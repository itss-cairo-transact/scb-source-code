* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
**---------INGY 22/07/2002---------**

*A ROUTINE TO CHECK IF ACCOUNT .2 NE A VALID INTRNAL ACCOUNT THEN DISPLAY ERROR MESSAGE
*A ROUTINE TO  EMPTY FIELDS CURRENCY.1 AND AMOUNT.LOCAL.LOCAL.1 AND AMOUNT.FCY.1 IF ACCOUNT.2 IS CHANGED
* MAKE AMOUNT.LOCAL.1 INPUT IF IT IS LOCAL CURRENCY & MAKE IT NOINPUT IF IT IS FOREIGN CURRENCY
* MAKE AMOUNT.FCY.1 INPUT IF IT IS FOREIGN CURRENCY & MAKE IT NOINPUT IF IT IS LOCAL CURRENCY

SUBROUTINE VVR.TT.ACCOUNT.2.WITHDRAW.FR.INTR

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


  IF COMI # R.NEW(TT.TE.ACCOUNT.2) THEN

    R.NEW (TT.TE.ACCOUNT.2) = ''
    R.NEW (TT.TE.CURRENCY.2) = ''
    R.NEW (TT.TE.AMOUNT.LOCAL.1) = ''
    R.NEW (TT.TE.AMOUNT.FCY.1) = ''
    R.NEW (TT.TE.NARRATIVE.2)<1,1> = ''

  END

 CATEGORY = ''

 CATEGORY = COMI[4,5]
 AC.BRANCH= COMI[9,2]
 AC.BRANCH = TRIM( AC.BRANCH, '0', 'L') ;* remove leading zeros

*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR ('CATEGORY':@FM:1,CATEGORY,CATE)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEGORY,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATE=R.ITSS.CATEGORY<1>
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR('ACCOUNT':@FM:3,COMI,TITLE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
TITLE=R.ITSS.ACCOUNT<3>

 IF NUM(COMI[1,3]) THEN     ETEXT = 'MUST.BE.INERNAL.ACCOUNT'
 ELSE
  IF AC.BRANCH # R.USER<EB.USE.DEPARTMENT.CODE> THEN ETEXT = 'MUST.BE.SAME.DEPARTMENT'
  ELSE
    IF COMI[4,5] = '10000' THEN
      
      R.NEW (TT.TE.ACCOUNT.2) = ''
      R.NEW (TT.TE.CURRENCY.2) = ''
      R.NEW (TT.TE.AMOUNT.LOCAL.1) = ''
      R.NEW (TT.TE.AMOUNT.FCY.1) = ''
      R.NEW (TT.TE.NARRATIVE.2)<1,1> = ''
      CALL REBUILD.SCREEN
      ETEXT = 'THIS.CATEGORY.IS.NOT.ALLOWED'
   END ELSE
       R.NEW(TT.TE.NARRATIVE.2)<1,1> = COMI[1,3]:'-':CATE:'-':TITLE
       R.NEW(TT.TE.CURRENCY.2) = COMI[1,3]
   END

 IF R.NEW(TT.TE.CURRENCY.2) = LCCY THEN
    T(TT.TE.AMOUNT.LOCAL.1)<3> =''
    T(TT.TE.AMOUNT.FCY.1)<3> ='NOINPUT'
 END ELSE
   T(TT.TE.AMOUNT.FCY.1)<3> =''
   T(TT.TE.AMOUNT.LOCAL.1)<3> ='NOINPUT'

 END
END
END
CALL REBUILD.SCREEN

 RETURN
 END
