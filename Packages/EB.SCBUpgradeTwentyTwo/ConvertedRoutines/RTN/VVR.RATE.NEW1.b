* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
    SUBROUTINE VVR.RATE.NEW1
*TO GET THE DEPOSIT RATE BASED ON AMOUNT,CURRENCY,CATEGORY

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL
    IF COMI THEN
*  IF MESSAGE # 'VAL' THEN
        IF COMI LT '21001'  OR COMI GT '21010' THEN

            ETEXT = '��� ������� ��� �����'
        END


        CURR =  R.NEW(LD.CURRENCY)
        AMT = R.NEW(LD.AMOUNT) + R.NEW(LD.AMOUNT.INCREASE)
* CATEG = R.NEW(LD.CATEGORY):R.NEW(LD.VALUE.DATE)
        CATEG = COMI : R.NEW(LD.VALUE.DATE)
        F.LD.LEVEL = '' ; FN.LD.LEVEL = 'F.SCB.LD.TYPE.LEVEL' ; R.LD.LEVEL = ''
        CALL OPF(FN.LD.LEVEL,F.LD.LEVEL)

        CALL F.READ(FN.LD.LEVEL, CATEG, R.LD.LEVEL, F.LD.LEVEL, ETEXT)
        LOCATE CURR IN R.LD.LEVEL<LDTL.CURRENCY,1> SETTING POS THEN
            VV = R.LD.LEVEL<LDTL.CURRENCY,1>
            EE = R.LD.LEVEL<LDTL.RATE,POS>
*Line [ 46 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT =  DCOUNT (R.LD.LEVEL<LDTL.AMT.FROM,POS>,@SM)
            FOR I = 1 TO TEMP.COUNT
                AMT.FROM = R.LD.LEVEL<LDTL.AMT.FROM,POS,I>
                AMT.TO = R.LD.LEVEL<LDTL.AMT.TO,POS,I>
                IF AMT GE AMT.FROM AND AMT LE AMT.TO THEN
** R.NEW(LD.INTEREST.RATE) = R.LD.LEVEL<LDTL.RATE,POS,I>
                    A = R.NEW(LD.INTEREST.RATE)
                    B = R.LD.LEVEL<LDTL.RATE,POS,I>
                    CALL REBUILD.SCREEN
                END

                IF TEMP.COUNT = I AND AMT GE R.LD.LEVEL<LDTL.AMT.FROM,POS,I> THEN
**    R.NEW(LD.INTEREST.RATE) = R.LD.LEVEL<LDTL.RATE,POS,I>
                    CALL REBUILD.SCREEN

                END
            NEXT I
        END
* END
        CALL VVR.LD.CUS.STAFF
    END
    RETURN
END
