* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*----------------15-7-2002 ZOZO SCB-----------------------*
*-----------------------------------------------------------------------------
* <Rating>513</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.ACCEPT.FR.BRN

*COMI = NARRATIVE.2 , IN THIS FIELD WE ENTER REFERENCE TRANSACTION NO.
* OF THE SENDING TELLER.ID WHO SEND THE CASH
*IF WE CAHANGE THE COMI VALUE , WE CLEAR THE FOLLOWING FIELDS ,
*AMOUNT.LOCAL.1 , AMOUNT.FCY.1 , TELLER.ID.2 (RECEIVER),DR.DENOM,DR.UNIT
*IF THE LENGTH OF THE COMI > 5 THEN ETEXT MESSAGE
*IF WE ENTER LESS THAN 5 DIGITS , COMI WILL BE COMPLETED BY ZEROS
*WE OPEN THE RECORD OF THE SENDING TELLER.ID.2 BY USING "TRANS.NO"
*TRANS.NO =TRANS.NO = 'TT' : XX :COMI
*COMI CONTAIN ONLY THE LAST 5 DIGITS WHICH IS THE REF. TRANSACTION
*XX : IS THE TODAY DATE BUT CONVERTED TO JULIAN DATE
*IF NO ERROR IN OPENING ,SO WE READ THE WHOLE RECORD
*CHECH IF THE TELLER.ID.1 WHICH IS INCLUDED IN THE RECORD WE READ
*R.TELLER<TT.TE.TELLER.ID.1> EQ THE TELLER.ID OF THE OPERATOR
* IF NOT EQ THEN ETEXT "ILLEGAL TELLER.ID'

*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*******************************************************************************************
    IF MESSAGE # 'VAL' THEN
        IF V$FUNCTION = 'I' THEN
            IF NOT (COMI) THEN GOTO PROGRAM.END
            IF COMI # R.NEW(TT.TE.NARRATIVE.2)<1,1> THEN GOSUB INITIAL1
            GOSUB INITIAL
            GOSUB TRANSACTION.NO
            GOSUB READ.TRANS.NO.OF.SENDING.BRN
        END
    END
    GOTO PROGRAM.END
*******************************************************************************************
INITIAL:
    ETEXT='' ; BRANCH2 =''
    FN.TELLER = 'FBNK.TELLER' ; F.TELLER ='' ; R.TELLER = ''
    GOSUB INITIAL1
    RETURN
*******************************************************************************************
INITIAL1:
    R.NEW(TT.TE.CURRENCY.1)= ''
    R.NEW(TT.TE.TELLER.ID.2)= ''
    R.NEW(TT.TE.AMOUNT.LOCAL.1)= ''
    R.NEW(TT.TE.AMOUNT.FCY.1)= ''
    R.NEW(TT.TE.DR.DENOM)= ''
    R.NEW(TT.TE.DR.UNIT)= ''
    RETURN
*******************************************************************************************
TRANSACTION.NO:
    IF LEN(COMI) > 5 THEN
        ETEXT = 'MUST.ENTER.ONLY.5.DIGITS'
    END ELSE
        COMI=FMT(COMI , 'R%5')
        GOSUB CONVERT.JULIAN.DATE
        XX = XX[3,5]
        TRANS.NO = 'TT' : XX :COMI
    END
    RETURN
*******************************************************************************************
READ.TRANS.NO.OF.SENDING.BRN:
    IF NOT(ETEXT) THEN
        CALL OPF(FN.TELLER,F.TELLER)
        CALL F.READ(FN.TELLER,TRANS.NO, R.TELLER, F.TELLER ,E1)
        IF NOT(E1) THEN
            GOSUB CHECK.TELLER.ID.2
            IF NOT(ETEXT) THEN
                R.NEW(TT.TE.CURRENCY.1)= R.TELLER<TT.TE.CURRENCY.1>
                R.NEW(TT.TE.TELLER.ID.2)= R.TELLER<TT.TE.TELLER.ID.2>
* R.NEW(TT.TE.TELLER.ID.1)= R.TELLER<TT.TE.TELLER.ID.1>
              * R.NEW(TT.TE.AMOUNT.LOCAL.1)= R.TELLER<TT.TE.AMOUNT.LOCAL.1>
              * R.NEW(TT.TE.AMOUNT.FCY.1)= R.TELLER<TT.TE.AMOUNT.FCY.1>
                R.NEW(TT.TE.DR.DENOM)= R.TELLER<TT.TE.DENOMINATION>
                R.NEW(TT.TE.DR.UNIT)= R.TELLER<TT.TE.UNIT>
**********************NESSREEN***************************************
                TELL.1 = R.TELLER<TT.TE.TELLER.ID.1>
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('TELLER.ID':@FM:TT.TID.USER,TELL.1,T.USER)
F.ITSS.TELLER.ID = 'FBNK.TELLER.ID'
FN.F.ITSS.TELLER.ID = ''
CALL OPF(F.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID)
CALL F.READ(F.ITSS.TELLER.ID,TELL.1,R.ITSS.TELLER.ID,FN.F.ITSS.TELLER.ID,ERROR.TELLER.ID)
T.USER=R.ITSS.TELLER.ID<TT.TID.USER>
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,T.USER,DEPT)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,T.USER,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                BR.NAME = FIELD(DEPT.NAME,'.',2)
                R.NEW(TT.TE.NARRATIVE.2) = BR.NAME
***************NESSREEN 14/09/2006***********************************
                IF R.TELLER<TT.TE.CURRENCY.1> = LCCY THEN
                   R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
                END ELSE
                   R.NEW(TT.TE.NARRATIVE.2)<1,2> = R.TELLER<TT.TE.AMOUNT.FCY.1>
                END
*********************************************************************

                *R.NEW(TT.TE.NARRATIVE.2)= R.TELLER<TT.TE.NARRATIVE.2>
*********************************************************************
                INS COMI BEFORE R.NEW(TT.TE.NARRATIVE.2)<1,1>
                GOSUB DEFAULT.ACCOUNT.2
                CALL REBUILD.SCREEN
            END
        END
        ELSE ETEXT = 'INVALID.TRANSACTION.NO'
    END
    RETURN
*********************************************************************************************
CONVERT.JULIAN.DATE:
    XX = ''
    CALL JULDATE(TODAY,XX )
    RETURN
**********************************************************************************************
DEFAULT.ACCOUNT.2:
    TRAN.CODE= R.NEW(TT.TE.TRANSACTION.CODE)
*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('TELLER.TRANSACTION':@FM:TT.TR.CAT.DEPT.CODE.2,TRAN.CODE,CATEG)
F.ITSS.TELLER.TRANSACTION = 'FBNK.TELLER.TRANSACTION'
FN.F.ITSS.TELLER.TRANSACTION = ''
CALL OPF(F.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION)
CALL F.READ(F.ITSS.TELLER.TRANSACTION,TRAN.CODE,R.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION,ERROR.TELLER.TRANSACTION)
CATEG=R.ITSS.TELLER.TRANSACTION<TT.TR.CAT.DEPT.CODE.2>
    IF NOT(ETEXT) THEN
*  R.NEW(TT.TE.ACCOUNT.2)= R.NEW(TT.TE.CURRENCY.1): CATEG : R.NEW(TT.TE.TELLER.ID.2)
    END
    RETURN
**********************************************************************************************
CHECK.TELLER.ID.2:
*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   * CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.OP)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.OP=R.ITSS.TELLER.USER<1>
   * IF R.TELLER<TT.TE.TELLER.ID.2> # TT.OP THEN ETEXT = '��� �� ��� ������'
   * IF R.TELLER<TT.TE.NARRATIVE.1> = 'Y' THEN ETEXT = '��� ������� ��� �� ���'
    RETURN
**********************************************************************************************
PROGRAM.END:
    RETURN
END
