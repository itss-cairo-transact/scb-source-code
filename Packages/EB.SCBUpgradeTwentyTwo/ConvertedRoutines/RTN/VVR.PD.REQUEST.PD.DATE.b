* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1198</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.PD.REQUEST.PD.DATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PD.REQUEST

    CHK.MONTH =  COMI[5,2]
    BEGIN CASE
    CASE CHK.MONTH EQ "01"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "02"
        END.DATE1 = 28
        END.DATE  = 29
        IF NOT(COMI[7,2] EQ END.DATE OR COMI[7,2] EQ END.DATE1) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "03"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "04"
        END.DATE = 30
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "05"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "06"
        END.DATE = 30
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "07"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "08"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "09"
        END.DATE = 30
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "10"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "11"
        END.DATE = 30
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'

    CASE CHK.MONTH EQ "12"
        END.DATE = 31
        IF NOT(COMI[7,2] EQ END.DATE) THEN ETEXT = '��� �� ���� ����� ���'
    END CASE

*IF NOT(COMI[7,2] EQ "31" OR COMI[7,2] EQ "30" OR COMI[7,2] EQ "28" OR COMI[7,2] EQ "29") THEN
*    ETEXT = '��� �� ���� ����� ���'
*END
    RETURN
END
