* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCCD.COLL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS';F.LDD ='';R.LDD = '';E=''
    CALL OPF(FN.LDD,F.LDD)
    IF R.NEW(COLL.COLLATERAL.CODE) EQ '108' THEN
        IF COMI # "" THEN
            R.NEW(COLL.APPLICATION.ID)= COMI
            CALL F.READ(FN.LDD,COMI,R.LDD,F.LDD,E)
            CD.AMT = R.LDD<LD.AMOUNT>

            R.NEW(COLL.NOMINAL.VALUE)   = CD.AMT
            NEW.AMT                     = ( CD.AMT * 0.9 ) * 0.9
            R.NEW(COLL.EXECUTION.VALUE) = NEW.AMT
            IF R.NEW(COLL.EXECUTION.VALUE) GT  NEW.AMT THEN
                ETEXT = " ������ �������� ��� �����"
                CALL STORE.END.ERROR
            END
            EX.VALUE  = R.NEW(COLL.EXECUTION.VALUE)
            COL.VALUE = R.NEW(COLL.LOCAL.REF)<1,COLR.COLL.AMOUNT>
            NET.COLL  = EX.VALUE - COL.VALUE

            IF NET.COLL LT 0 THEN NET.COLL = 0
            R.NEW(COLL.THIRD.PARTY.VALUE) =  NET.COLL

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
