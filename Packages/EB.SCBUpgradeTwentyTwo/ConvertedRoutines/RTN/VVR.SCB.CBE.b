* @ValidationCode : MjotMTk1MDg1Nzc6Q3AxMjUyOjE2NDIyNTQ4Mzc4NDc6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:53:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.SCB.CBE
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.FT.COMMISSION.TYPE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CUSTOMER
    
    $INSERT           I_F.SCB.BR.CUS
    $INSERT           I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BR.SLIPS
*------------- 2011/ 11/ 15 -ADD AS VIR on VERSION  ---------------
    IF PGM.VERSION NE ',SCB.CHQ.LCY.REG.0017' THEN
        IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'NO' ) THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '9943330010508001'
        END ELSE
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '99433300105085':ID.COMPANY[8,2]
        END
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> EQ '' THEN
*Line [ 44 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            E='ACCOUNT CBE IS '' ';CALL ERR;MESSAGE='REPEAT'
        END
    END
*------------------------------------------------------------------
RETURN
END
