* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>825</Rating>
*-----------------------------------------------------------------------------
**------------15.07.2002 ABEER HEGAZI-----------*

SUBROUTINE VVR.TT.CHECK.ACCOUNT

*ROUTINE TO:1- CHECK LENGTH OF THE ACCOUNT EQ 17.
*           2- CHECK NOT TO ACCEPT INTERNAL ACCOUNT.
*           3- CHECK IT'S A VALID CBE ACCOUNT,IT'S CATEGORY IS THE SAME WITH THE CATEGORY OF THE ACCOUNT.
*           4- THEN CHECK IF IT'S SECTOR IS THE SAME WITH THE SECTOR OF THE CUSTOMER(OWENER OF THE ACCOUNT).
*           5- IF IT'S SAME THEN DEFAULT NARRATIVE WITH SHORT NAME AND VALUE DATE WITH TODAY.
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLASS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

IF V$FUNCTION = 'I' THEN
 IF NUM(COMI) AND LEN(COMI) # 17 THEN
    ETEXT = 'ACCOUNT.LENGTH.MUST.EQ.17'
    GOTO PROG.END
 END ELSE
     IF NOT(NUM(COMI)) THEN
       ETEXT = 'INTERNAL.ACCOUNT.NOT.ALLOWED'
       GOTO PROG.END
     END ELSE
      GOSUB CHECK.ALL
      GOTO PROG.END
         END
     END
END

*******************************************************************************************************
CHECK.ALL:

    GOSUB INTIAL
    GOSUB CALL.OPF
    IF E THEN RETURN
    GOSUB CHECK.CATEGORY
    IF E THEN RETURN

RETURN
*******************************************************************************************************
INTIAL:

     ID1 = 'U-CBE' ; CATE = '' ;CUST='' ;CUST1='';E = ''
     FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
     FN.ACCOUNT.CLASS = 'F.ACCOUNT.CLASS' ; F.ACCOUNT.CLASS = '' ; R.ACCOUNT.CLASS = ''


RETURN
********************************************************************************************************
CALL.OPF:

  CUSTOMER = COMI [5,7] ;CUST1 =TRIM(CUSTOMER,'0','L'); CATEGORY = COMI [12,4]
*
*  CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUST1,CUST)
*  CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST1,SECTOR)
*  CALL DBR('ACCOUNT.CLASS':@FM:AC.CLS.CATEGORY,ID1,CATE)
*  CALL DBR('ACCOUNT.CLASS':@FM:AC.CLS.SECTOR,ID1,SECT)


   CALL OPF(FN.CUSTOMER,F.CUSTOMER)
   CALL F.READ(FN.CUSTOMER,CUST1, R.CUSTOMER, F.CUSTOMER ,E1)
   CUST   = R.CUSTOMER<EB.CUS.SHORT.NAME>
   SECTOR = R.CUSTOMER<EB.CUS.SECTOR>

   

   CALL OPF(FN.ACCOUNT.CLASS,F.ACCOUNT.CLASS)
   CALL F.READ(FN.ACCOUNT.CLASS,ID1, R.ACCOUNT.CLASS, F.ACCOUNT.CLASS ,E1)
   CATE = R.ACCOUNT.CLASS<AC.CLS.CATEGORY>
   SECT = R.ACCOUNT.CLASS<AC.CLS.SECTOR>

   


RETURN
*******************************************************************************************************
CHECK.CATEGORY:


 LOCATE CATEGORY IN CATE<1,1> SETTING J THEN  GOSUB CHECK.SECTOR ELSE ETEXT = 'IMPROPER.CATEGORY.FOR.CBE'

RETURN
********************************************************************************************************
CHECK.SECTOR:

 LOCATE SECTOR   IN SECT<1,1> SETTING J THEN GOSUB DEFAULTS ELSE ETEXT = 'IMPROPER.SECTOR.FOR.CBE'

RETURN
*******************************************************************************************************
 DEFAULTS:

 R.NEW( TT.TE.NARRATIVE.2) = CUST
 R.NEW( TT.TE.VALUE.DATE.2) = TODAY
 *R.NEW( TT.TE.EXPOSURE.DATE.2) = TODAY

 CALL REBUILD.SCREEN

RETURN
********************************************************************************************************
PROG.END:

RETURN
END
