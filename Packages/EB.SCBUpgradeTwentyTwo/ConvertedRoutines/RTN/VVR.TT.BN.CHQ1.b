* @ValidationCode : MjotNzA5OTc4MTgwOkNwMTI1MjoxNjQyMjU1NjQyODU0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 16:07:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE  VVR.TT.BN.CHQ1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FT.DR.CHQ
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
*Line [ 33 ] Adding $INCLUDE I_F.NUMERIC.CURRENCY, as it used on line 56  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
****END OF UPDATE 8/3/2016**************************

    F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
    ID= COMI:'.':R.NEW(TT.TE.CHEQUE.NUMBER)
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN

        CURR = COMI[9,2]
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****ID = 'SCB.':COMI:"-":R.NEW(TT.TE.CHEQUE.NUMBER)
        ID = 'SCB.':COMI:".":R.NEW(TT.TE.CHEQUE.NUMBER)
****CALL DBR("CHEQUES.PRESENTED":@FM:CHQ.PRE.REPRESENTED.COUNT,ID,COUN)
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR("CHEQUE.REGISTER.SUPPLEMENT":@FM:CC.CRS.DATE.PRESENTED,ID,COUN)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
COUN=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.PRESENTED>
****END OF UPDATE 8/3/2016**************************
        IF NOT(ETEXT) AND COUN = ""  AND R.COUNT<DR.CHQ.CHEQ.TYPE> = 2 THEN
            IF CURR =  '10' THEN
                R.NEW(TT.TE.AMOUNT.LOCAL.1) = R.COUNT<DR.CHQ.AMOUNT>
            END ELSE
                R.NEW(TT.TE.AMOUNT.FCY.1) = R.COUNT<DR.CHQ.AMOUNT>
            END
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CURR,CODE)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CURR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CODE=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
            R.NEW(TT.TE.CURRENCY.1) = CODE

* R.NEW(TT.TE.ACCOUNT.1) =  R.COUNT<DR.CHQ.NOS.ACCT>
            R.NEW(TT.TE.NARRATIVE.1) = COMI
            COMI = R.COUNT<DR.CHQ.DEBIT.ACCT>
* R.NEW(TT.TE.CHEQUE.NUMBER) =  R.COUNT<DR.CHQ.CHEQ.NO>
            CALL REBUILD.SCREEN
* IF V$FUNCTION = 'A' THEN
*  R.COUNT<DR.CHQ.CHEQ.STATUS> = 2
*  CALL F.WRITE(FN.COUNT,ID,R.COUNT)
*  CALL JOURNAL.UPDATE(ID)
* END
        END ELSE
            ETEXT = 'CHQ.STATUS.NOT.ALLOWED'
        END
    END

    CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
    CLOSE FN.COUNT


RETURN
END
