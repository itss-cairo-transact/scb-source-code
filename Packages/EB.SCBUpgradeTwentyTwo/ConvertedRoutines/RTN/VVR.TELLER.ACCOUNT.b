* @ValidationCode : MjoxNDYxMzY4OTE4OkNwMTI1MjoxNjQyMjU0OTM3MjYzOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:55:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>1037</Rating>
*-----------------------------------------------------------------------------
*--- 14/07/2002 RANIA KAMAL -------
*A ROUTINE TO CHECK THAT THE LENGHT OF ACCOUNT EQ 17 & MAKE SURE THAT IT IS NOT NOSTRO/INTERNAL A/C
*ALSO TO DEFAULT NARRATIVE & CURRENCY  ACCORDING TO ACCOUNT AND MAKE CURRENCY NOINPUT FIELD
*ALSO IF THE CURRENCY IS LOCAL AND ONLINE ACTUAL BALANCE <=0 THEN DEFAULT VALUE.DATE & EXPOSURE.DATE

SUBROUTINE VVR.TELLER.ACCOUNT

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 38 ] Adding $INCLUDE I_F.ACCOUNT for being used line 106 - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
***************************************************************************************************
    IF V$FUNCTION = 'I' THEN

        GOSUB INITIAL
        GOSUB ACCOUNT.CHECK
*IF E = 'Y' THEN RETURN
        GOSUB CURRENCY.CHECK
*IF E = 'Y' THEN RETURN
        GOSUB DATE.CHECK
*IF E = 'Y' THEN RETURN

    END
    GOTO PROGRAM.END
***************************************************************************************************
INITIAL:

*E = ''
    BRANCH = ''
    CURRENCY = ''
    CUSTOMER = ''
    CATEGORY = ''
    CUS.NAME = ''
    CURR.NAME = ''
    BALANCE = ''
    LLDATE = TODAY
    FOREIGNDATE = TODAY

RETURN
***************************************************************************************************
ACCOUNT.CHECK:

    BRANCH = COMI [1,2]
    CURR = COMI [3,2]
    CUSTOMER = COMI [5,7]
    CATEGORY = COMI [12,4]

    IF LEN(COMI) # 17 THEN E= 'Y' ; ETEXT = 'NOT.SUITABLE.ACCOUNT.LENGHT' ; R.NEW (TT.TE.ACCOUNT.1) = ''
    ELSE IF NOT(NUM(COMI)) THEN E= 'Y' ; ETEXT = 'THIS.IS.INTERNAL.ACCOUNT' ; R.NEW (TT.TE.ACCOUNT.1) = ''
    ELSE IF CATEGORY = '5000' THEN E= 'Y' ; ETEXT = 'THIS.IS.NOSTRO.ACCOUNT' ; R.NEW (TT.TE.ACCOUNT.1) = ''

    ELSE GOSUB ACCOUNT.DEFAULT
    CALL REBUILD.SCREEN

RETURN
**************************************************************************************************
ACCOUNT.DEFAULT:

*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'CUSTOMER':@FM:EB.CUS.SHORT.NAME, CUSTOMER , CUS.NAME)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSTOMER,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.NAME=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>
    R.NEW (TT.TE.NARRATIVE.2) = CUS.NAME
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE, CURR , CURR.NAME)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CURR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CURR.NAME=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
    R.NEW (TT.TE.CURRENCY.1) = CURR.NAME
    CALL REBUILD.SCREEN

RETURN
**************************************************************************************************
CURRENCY.CHECK:

    IF R.NEW (TT.TE.CURRENCY.1) = LCCY THEN T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT' ;T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
    ELSE T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT' ;T(TT.TE.AMOUNT.FCY.1)<3>=''

RETURN
* *************************************************************************************************
DATE.CHECK:

    CALL CDT('', LLDATE, '1W')
    CALL CDT('', FOREIGNDATE, '2W')

*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL, COMI ,BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BALANCE=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>

    IF R.NEW (TT.TE.CURRENCY.1) = LCCY AND BALANCE <= 0 THEN R.NEW (TT.TE.VALUE.DATE.1) = LLDATE ;R.NEW (TT.TE.VALUE.DATE.2) = LLDATE ; R.NEW (TT.TE.EXPOSURE.DATE.1) = LLDATE
    IF R.NEW (TT.TE.CURRENCY.1) # LCCY AND BALANCE <= 0 THEN R.NEW (TT.TE.VALUE.DATE.1) = LLDATE ;R.NEW (TT.TE.VALUE.DATE.2) = FOREIGNDATE ; R.NEW (TT.TE.EXPOSURE.DATE.1) = FOREIGNDATE

RETURN
**************************************************************************************************

PROGRAM.END:

RETURN
END
