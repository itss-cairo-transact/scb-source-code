* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
******NESSREEN-SCB 21/06/2006************
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.FAULT.VISA.ACC

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    IF MESSAGE = '' THEN
*****UPDATED BY NESSREEN AHMED 1/11/2011******************
        X = ''
        VISA.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.VISA.NUMBER>
        IF COMI NE VISA.NO THEN
            R.NEW(TT.TE.ACCOUNT.1) = ""
            T.ENRI<TT.TE.ACCOUNT.1> = ""
            R.NEW (TT.TE.CUSTOMER.1) = ""
            CALL REBUILD.SCREEN
        END
*****END OF UPDATE 1/11/2011******************************
****NESSREEN AHMED 09/01/2007*********
        IF COMI[1,4] # '4042' THEN ETEXT = '��� �� ���� ��� ������� ����' ; CALL STORE.END.ERROR
***************************************
        FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.VISA.APP = '' ; RETRY1 = '' ; E1 = ''
        T.SEL = 'SSELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO EQ ':COMI

        KEY.LIST=""
        SELECTED=""
        ER.MSG=""

        CUS = ''
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                KEY.TO.USE = KEY.LIST<I>
                CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                CALL F.READ(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
                VISA.ACC.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT>
            NEXT I
******UPDATED BY NESSREEN AHMED 1/11/2011******************
*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,VISA.ACC.NO,CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,VISA.ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
            R.NEW (TT.TE.CUSTOMER.1)      = CUS
            R.NEW (TT.TE.ACCOUNT.1)       = VISA.ACC.NO
            X= AC.LOCAL.REF
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:X,VISA.ACC.NO,AC.NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,VISA.ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.NAME=R.ITSS.ACCOUNT<X>
            AC.NAME = AC.NAME<1,ACLR.ARABIC.TITLE>
            T.ENRI<TT.TE.ACCOUNT.1> = AC.NAME
 ******END OF UPDATE 1/11/2011*****************************
            R.NEW (TT.TE.EXPOSURE.DATE.1) = TODAY
            R.NEW (TT.TE.VALUE.DATE.1)    = TODAY
            CALL REBUILD.SCREEN
        END ELSE
            ETEXT = "������ ����� ���� �����"  ;CALL STORE.END.OVERRIDE
        END
    END
    RETURN
END
