* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.WH.GET.COMM.CHRG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TXN.TYPE.CONDITION

*ERROR IN MULTI

    IF MESSAGE NE "VAL" THEN
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('SCB.WH.TXN.TYPE.CONDITION':@FM:SCB.WH.TXN.FT.TRANS,COMI,FT.TXN)
F.ITSS.SCB.WH.TXN.TYPE.CONDITION = 'F.SCB.WH.TXN.TYPE.CONDITION'
FN.F.ITSS.SCB.WH.TXN.TYPE.CONDITION = ''
CALL OPF(F.ITSS.SCB.WH.TXN.TYPE.CONDITION,FN.F.ITSS.SCB.WH.TXN.TYPE.CONDITION)
CALL F.READ(F.ITSS.SCB.WH.TXN.TYPE.CONDITION,COMI,R.ITSS.SCB.WH.TXN.TYPE.CONDITION,FN.F.ITSS.SCB.WH.TXN.TYPE.CONDITION,ERROR.SCB.WH.TXN.TYPE.CONDITION)
FT.TXN=R.ITSS.SCB.WH.TXN.TYPE.CONDITION<SCB.WH.TXN.FT.TRANS>

        T.SEL = "SELECT FBNK.FT.TXN.TYPE.CONDITION WITH @ID EQ ":FT.TXN
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        FN.TXN='FBNK.FT.TXN.TYPE.CONDITION' ;R.TXN='';F.TXN=''
        CALL OPF(FN.TXN,F.TXN)
        CALL F.READ(FN.TXN,KEY.LIST, R.TXN, F.TXN,E)

*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        FOR I =1 TO DCOUNT(R.TXN<FT6.COMM.TYPES>,@VM)
*TEXT = I ; CALL REM
*TEXT = R.TXN<FT6.COMM.TYPES,I> ; CALL REM

            R.NEW(SCB.WH.TRANS.COMMISSION.TYPE)<1,I> = R.TXN<FT6.COMM.TYPES,I>

*TEXT = R.NEW(SCB.WH.TRANS.COMMISSION.TYPE)<1,I> ; CALL REM
            CALL REBUILD.SCREEN
        NEXT I

*Line [ 58 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        FOR J =1 TO DCOUNT(R.TXN<FT6.CHARGE.TYPES>,@VM)
*TEXT = J ; CALL REM
*TEXT = R.TXN<FT6.CHARGE.TYPES,J> ; CALL REM

            R.NEW(SCB.WH.TRANS.CHARGE.TYPE)<1,J>      = R.TXN<FT6.CHARGE.TYPES,J>

*TEXT = R.NEW(SCB.WH.TRANS.CHARGE.TYPE)<1,J> ; CALL REM
            CALL REBUILD.SCREEN
        NEXT J

    END

    RETURN
END
