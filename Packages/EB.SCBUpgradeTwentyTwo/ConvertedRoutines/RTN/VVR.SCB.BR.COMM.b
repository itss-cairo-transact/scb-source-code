* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.BR.COMM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS

    PER = ".2"
    MIN = "10"
    MAX = "300"
    COMM.AMT = R.NEW(EB.BILL.REG.AMOUNT)*PER/100
    IF COMM.AMT LT MIN THEN
        COMM.AMT = MIN
    END ELSE
        IF COMM.AMT GT MAX THEN
            COMM.AMT = MAX
        END
    END
    CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")

***-------------20100805-----------------------------------------------------**

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
    SEC  = R.CUS<EB.CUS.SECTOR>
    IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = "BILLCOLL"
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = "EGP":COMM.AMT
    END ELSE
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""

    END

    FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
    CALL OPF(FN.BR.COM,F.BR.COM)
    BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
    CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)
    IF NOT(E11) THEN
        COM.AMT   = R.BR.COM<BR.CUS.COMM.AMT>
        COM.PR    = R.BR.COM<BR.CUS.COMM.PREC>
        CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>

        IF CHRG.AMT EQ 'YES' OR COM.AMT EQ 0 THEN

            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ""
        END
    END

***-------------20100805-----------------------------------------------------**
    RETURN

END
