* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.TT.FT.PL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    F.COUNT = '' ; FN.COUNT = 'FBNK.FUNDS.TRANSFER'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
    ID= COMI
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
*IF R.COUNT<DR.CHQ.CHEQ.STATUS> = 1 AND R.COUNT<DR.CHQ.CHEQ.TYPE> = 1 THEN
        IF R.COUNT<FT.DEBIT.CURRENCY> =  LCCY THEN
            R.NEW(TT.TE.AMOUNT.LOCAL.1) = R.COUNT<FT.DEBIT.AMOUNT>
        END ELSE
            R.NEW(TT.TE.AMOUNT.FCY.1) = R.COUNT<FT.DEBIT.AMOUNT>
        END
        R.NEW(TT.TE.CURRENCY.1) = R.COUNT<FT.DEBIT.CURRENCY>
        R.NEW(TT.TE.ACCOUNT.1) = R.COUNT<FT.DEBIT.ACCT.NO>
*  END
*ELSE
*    ETEXT = 'CHQ.STATUS.NOT.ALLOWED'
* END
    END

    CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
    CLOSE FN.COUNT
CALL REBUILD.SCREEN

    RETURN
END
