* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN AHMED 22/3/2015-----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.FLT.CUST.DT
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    ETEXT = ''    ; TEXT = ''

    IF MESSAGE =  '' THEN
        FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
        CALL OPF(FN.CUSTOMER,F.CUSTOMER)

        IF COMI # R.NEW(TT.TE.LOCAL.REF)<1,TTLR.BK.CUS> THEN
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ARABIC.NAME> = ''
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.DPST.NSN.NO> = ''
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHK.FLG> = ''
            CALL REBUILD.SCREEN
        END
        IF COMI THEN
            CUST = COMI
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E1)
            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME   = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NSN    = LOCAL.REF<1,CULR.NSN.NO>
            CUST.REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
            R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ARABIC.NAME> = CUST.NAME
            IF CUST.NSN # '' THEN
                R.NEW(TT.TE.LOCAL.REF)<1,TTLR.DPST.NSN.NO> = CUST.NSN
            END ELSE
                R.NEW(TT.TE.LOCAL.REF)<1,TTLR.DPST.NSN.NO> = CUST.REG.NO
            END
            CALL REBUILD.SCREEN
        END
        RETURN
    END
