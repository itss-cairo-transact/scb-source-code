* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
****RANIA******
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SWIFT.MSG.TO.PROD
*TO CHECK IF MSG.TO.PROD ENTERED IS NOT MATCEHD WITH RIGHT MESSAGE TO PROD
*IN TABLE SCB.LG.SWIFT.ACTION ERROR MESSAGE WILL BE DISPLAYED
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.SWIFT.ACTION

    IF MESSAGE = 'VAL' THEN

        SWIFTACTION =  R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('SCB.LG.SWIFT.ACTION':@FM:SCB.LG.DE.MAPPING.ID,SWIFTACTION,MAPKEY)
F.ITSS.SCB.LG.SWIFT.ACTION = 'F.SCB.LG.SWIFT.ACTION'
FN.F.ITSS.SCB.LG.SWIFT.ACTION = ''
CALL OPF(F.ITSS.SCB.LG.SWIFT.ACTION,FN.F.ITSS.SCB.LG.SWIFT.ACTION)
CALL F.READ(F.ITSS.SCB.LG.SWIFT.ACTION,SWIFTACTION,R.ITSS.SCB.LG.SWIFT.ACTION,FN.F.ITSS.SCB.LG.SWIFT.ACTION,ERROR.SCB.LG.SWIFT.ACTION)
MAPKEY=R.ITSS.SCB.LG.SWIFT.ACTION<SCB.LG.DE.MAPPING.ID>
        MAPKEY = MAPKEY[1,3]
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD> # MAPKEY THEN ETEXT = 'DOESNT.MATCH.ID'
    END

    RETURN
END
