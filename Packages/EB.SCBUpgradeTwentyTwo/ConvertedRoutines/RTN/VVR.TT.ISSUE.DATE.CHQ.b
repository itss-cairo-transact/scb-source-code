* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>198</Rating>
*-----------------------------------------------------------------------------
**---------BAKRY 21/07/2002---------**

    SUBROUTINE VVR.TT.ISSUE.DATE.CHQ


*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

*To Check if field Cheq Date is not valid date an error message will be displayed
*Also if the Cheq is not due yet an error message will be displayed

    ETEXT = ''

    IF COMI THEN
        MYDATE = COMI
****UPDATED BY NESSREEN AHMED 31/12/2008 ACCORDING TO NEW CHEQUE RULES********
***  CALL CDT('', MYDATE, '365C')
***  IF TODAY > MYDATE THEN  ETEXT='����� ����� ��� ����'
***IF COMI > TODAY THEN ETEXT='��� ����� ��� ����� ���'
        CALL CDT('', MYDATE, '180C')
       *** TEXT = 'MYDATE=':MYDATE ; CALL REM
        IF COMI > MYDATE THEN ETEXT = '��� ����� ��� ����� ���'

***************************************************************************************
    END
    CALL REBUILD.SCREEN
    RETURN
END
