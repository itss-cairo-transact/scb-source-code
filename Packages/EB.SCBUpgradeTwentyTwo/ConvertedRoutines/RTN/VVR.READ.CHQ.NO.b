* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
 **---------ZEAD_MOSTAFA---------**

 SUBROUTINE VVR.READ.CHQ.NO

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

 IF V$FUNCTION = 'I' THEN
 CHQ = "SCB.":R.NEW(TT.TE.ACCOUNT.2)

*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*      CALL DBR("CHEQUE.REGISTER":@FM:7.1,CHQ,CHQ.NO)
F.ITSS.CHEQUE.REGISTER = 'FBNK.CHEQUE.REGISTER'
FN.F.ITSS.CHEQUE.REGISTER = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER,FN.F.ITSS.CHEQUE.REGISTER)
CALL F.READ(F.ITSS.CHEQUE.REGISTER,CHQ,R.ITSS.CHEQUE.REGISTER,FN.F.ITSS.CHEQUE.REGISTER,ERROR.CHEQUE.REGISTER)
CHQ.NO=R.ITSS.CHEQUE.REGISTER<7.1>
      X = FIELD(CHQ.NO,"-",1)
      Y = FIELD(CHQ.NO,"-",2)
      IF COMI < X OR COMI > Y THEN ETEXT = 'OUT OF RANGE'

 END
  RETURN
  END
---------------
