* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>339</Rating>
*-----------------------------------------------------------------------------
**---------INGY 17/07/2002---------**



    SUBROUTINE VVR.TT.ACCOUNT.2.TRANSFER

*A ROUTINE TO :
* MAKE SURE IT IS NOT NOSTRO ACCOUNT
* MAKE SURE IT IS NOT INTERNAL ACCOUNT
*CHECK IF ACCOUNT.1 EQ ACCOUNT.2 THEN DISPLAY ERROR
*CHECK IF ACCOUNT.1 NE ACCOUNT.2 THEN
*CHECK IF CURRENCY OF ACCOUNT.2 NE THE CURRENCY OF ACCOUNT.1 THEN DISPLAY ERROR

*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT




* CATEGORY = ''
* CURR.ACC.2 = ''
*
* CUST.ID.2 = TRIM(COMI[5,7],"0","L")
* CUST.ID.1 = TRIM(R.NEW (TT.TE.ACCOUNT.1)[5,7],"0","L")
* CURR.ACC.2 = COMI [3,2]
* CATEGORY = COMI [12,4]

    CUST.ID.1= R.NEW (TT.TE.ACCOUNT.2)

*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('ACCOUNT':@FM:AC.SHORT.TITLE,CUST.ID.1,NAME1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CUST.ID.1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
NAME1=R.ITSS.ACCOUNT<AC.SHORT.TITLE>
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('ACCOUNT':@FM:AC.SHORT.TITLE,COMI,NAME2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
NAME2=R.ITSS.ACCOUNT<AC.SHORT.TITLE>

    IF COMI # R.NEW(TT.TE.ACCOUNT.1) THEN

****  R.NEW(TT.TE.CURRENCY.1) = ''
*   R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
*  R.NEW(TT.TE.AMOUNT.FCY.1) = ''
        R.NEW(TT.TE.ACCOUNT.1) = ''
        R.NEW(TT.TE.NARRATIVE.2) = ''
    END

    IF NOT(NUM(COMI)) THEN ETEXT='��� ����� ��������� �������� �����'
* ETEXT = 'Not.Allowed.For.Internal.Account'
    ELSE
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF 5000 LE CATEG AND CATEG LE 5999 THEN
            ETEXT = 'THIS.IS.NOSTRO.ACCOUNT'
            ETEXT='��� ����� ������� ��������'
        END ELSE
            IF R.NEW (TT.TE.ACCOUNT.2) = COMI THEN ETEXT='������ ��� �� ���� �����'
*ETEXT = 'THE.ACCOUNTS.MUST.BE.DIFFERENT'
            ELSE
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR.ACC.2)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.ACC.2=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,CUST.ID.1,CURR.ACC.1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CUST.ID.1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.ACC.1=R.ITSS.ACCOUNT<AC.CURRENCY>

                IF  CURR.ACC.2 # CURR.ACC.1 THEN ETEXT='���� �������� ��� �� ���� �����'
* ETEXT = 'THE.CURRENCY.OF.THE.ACCOUNTS.MUST.BE.EQUAL'

            END



            IF NAME1 AND NAME2 THEN

                R.NEW(TT.TE.NARRATIVE.2)<1,1> = NAME1
                R.NEW(TT.TE.NARRATIVE.2)<1,2> = NAME2
                CALL REBUILD.SCREEN
            END
        END
    END


    RETURN
END
