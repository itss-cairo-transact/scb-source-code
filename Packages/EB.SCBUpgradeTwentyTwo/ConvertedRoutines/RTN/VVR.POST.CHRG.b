* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*************MAHMOUD************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.POST.CHRG
*define charge amount
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    IF MESSAGE # 'VAL' THEN
        CHG.ID = "ACRESPOSTAC"
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID,ST.AMT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
ST.AMT=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID,CTG.ACC)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CTG.ACC=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

        CHG.ID1 = "ACRESMAILAC"
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,CHG.ID1,PS.AMT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID1,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
PS.AMT=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID1,CTG.ACC1)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID1,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CTG.ACC1=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

        IF R.NEW(FT.DEBIT.CURRENCY) EQ '' THEN
            R.NEW(FT.DEBIT.CURRENCY)='EGP'
        END
        DR.CUR = R.NEW(FT.DEBIT.CURRENCY)
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,DR.CUR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DR.CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
        IF COMI EQ 'YES' THEN
            R.NEW(FT.TRANSACTION.TYPE) = 'AC51'
            R.NEW(FT.LOCAL.REF)<1,5> = 'NO'
            IF DR.CUR NE 'EGP' THEN
                PST.AMT = PS.AMT<1,1> / MID.RATE<1,1>
                R.NEW(FT.DEBIT.AMOUNT)  = DROUND(PST.AMT,2)
            END ELSE
                R.NEW(FT.DEBIT.AMOUNT)  = PS.AMT<1,1>
            END
            R.NEW(FT.CREDIT.ACCT.NO) = 'PL':CTG.ACC1
            R.NEW(FT.CREDIT.CURRENCY) = DR.CUR
        END ELSE
            R.NEW(FT.TRANSACTION.TYPE) = 'AC50'
            R.NEW(FT.LOCAL.REF)<1,5>  = 'YES'
            IF DR.CUR NE 'EGP' THEN
                STT.AMT = ST.AMT<1,1> / MID.RATE<1,1>
                R.NEW(FT.DEBIT.AMOUNT)  = DROUND(STT.AMT,2)
            END ELSE
                R.NEW(FT.DEBIT.AMOUNT)  = ST.AMT<1,1>
            END
            R.NEW(FT.CREDIT.ACCT.NO) = 'PL':CTG.ACC
            R.NEW(FT.CREDIT.CURRENCY) = DR.CUR
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
