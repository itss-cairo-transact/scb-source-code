* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
**---------INGY 17/07/2002---------**

    SUBROUTINE VVR.TT.ACCOUNT.1.TRANSFER

*A ROUTINE TO :
* MAKE SURE IT IS NOT NOSTRO ACCOUNT
* MAKE SURE IT IS NOT INTERNAL ACCOUNT
* MAKE AMOUNT.LOCAL.1 INPUT IF IT IS LOCAL CURRENCY & MAKE IT NOINPUT IF IT IS FOREIGN CURRENCY
* MAKE AMOUNT.FCY.1 INPUT IF IT IS FOREIGN CURRENCY & MAKE IT NOINPUT IF IT IS LOCAL CURRENCY

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

    CURR.NAME = ''
    CATEGORY = ''
    CURRENCY = ''

    IF COMI # R.NEW(TT.TE.ACCOUNT.2) THEN

        R.NEW(TT.TE.CURRENCY.1) = ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
        R.NEW(TT.TE.AMOUNT.FCY.1) = ''
        R.NEW(TT.TE.ACCOUNT.1) = ''
        R.NEW(TT.TE.NARRATIVE.2) = ''
    END
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY, COMI, CATEG )
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>

    IF NOT(NUM(COMI)) THEN ETEXT ='��� ����� ��������� �������� �����'
*ETEXT='Not.Allowed.For.Internal.Account'
    ELSE
        IF 5000 LE CATEG AND CATEG LE 5999 THEN
*ETEXT = 'THIS.IS.NOSTRO.ACCOUNT'
            ETEXT='��� ����� ������� ������'
        END ELSE
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, COMI, CURR.NAME )
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.NAME=R.ITSS.ACCOUNT<AC.CURRENCY>
            IF CURR.NAME THEN R.NEW(TT.TE.CURRENCY.1) = CURR.NAME
            IF R.NEW(TT.TE.CURRENCY.2) = LCCY THEN
                T(TT.TE.AMOUNT.LOCAL.1)<3> =''
                T(TT.TE.AMOUNT.FCY.1)<3> ='NOINPUT'
            END ELSE
                T(TT.TE.AMOUNT.FCY.1)<3> =''
                T(TT.TE.AMOUNT.LOCAL.1)<3> ='NOINPUT'

            END
        END
    END
    CALL REBUILD.SCREEN


    RETURN
END
