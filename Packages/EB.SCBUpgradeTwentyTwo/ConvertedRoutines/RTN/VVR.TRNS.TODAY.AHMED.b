* @ValidationCode : MjozMjM2MTc3MDpDcDEyNTI6MTY0MjI1NTA4MTk0MjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Jan 2022 15:58:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.TRNS.TODAY.AHMED(ENQUIRY.DATA)

*    PROGRAM VVR.TRNS.TODAY

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FUND
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.TRANS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CONSOL.ENT.TODAY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON

    LOCATE "INPUTTER" IN ENQUIRY.DATA<2,1> SETTING INPUTT THEN
        INPTR = ENQUIRY.DATA<4,INPUTT>
    END
    LOCATE "BOOKING.DATE" IN ENQUIRY.DATA<2,1> SETTING B.DATE THEN
        BOK.DATE = ENQUIRY.DATA<4,B.DATE>

    END
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    T.SEL="SELECT F.SCB.TRANS.TODAY WITH BOOKING.DATE EQ ": BOK.DATE :" AND INPUTTER LIKE ": INPTR
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
* ACC.NO = FIELD(KEY.LIST<I>,'-',1)
* ACC.NO2 = FIELD(KEY.LIST<I+1>,'-',1)
* IF ACC.NO NE ACC.NO2 THEN
            ENQ<2,Z> = "@ID"
            ENQ<3,Z> = "EQ"
            ENQ<4,Z> = KEY.LIST<I>
            Z = Z+1

* END
        NEXT I
    END ELSE
        ENQ.ERROR = "NO ACCOUNT FOUND"
    END



    F.COUNT = '' ; FN.COUNT = 'F.SCB.TRANS.TODAY'; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)

    F.ACC = '' ; FN.ACC = 'FBNK.ACCOUNT'; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''

    CALL OPF( FN.STMT,F.STMT)
    F.ACCT.ENT = '' ; FN.ACCT.ENT = 'F.ACCT.ENT.TODAY'; R.ACCT.ENT = ''
    CALL OPF(FN.ACCT.ENT,F.ACCT.ENT)
*****UPDATED ON 6/6/2008 BY NESSREEN AHMED********************************
    F.FT = '' ; FN.FT = 'FBNK.FUNDS.TRANSFER' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    F.BR = '' ; FN.BR = 'FBNK.BILL.REGISTER' ; R.BR = ''
    CALL OPF(FN.BR,F.BR)
    F.WH = '' ; FN.WH = 'F.SCB.WH.TRANS' ; R.WH = ''
    CALL OPF(FN.WH,F.WH)
    F.LD = '' ; FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    ORD.BNK = ''
**************************************************************************
    T.SEL = "SELECT FBNK.ACCT.ENT.TODAY"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* CALL F.READ(FN.ACCT.ENT,'0110073410650101',R.ACCT.ENT,F.ACCT.ENT,READ.ERR)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.ACCT.ENT,KEY.LIST<I>,R.ACCT.ENT,F.ACCT.ENT,READ.ERR)
        PRINT KEY.LIST<I>
        PRINT R.ACCT.ENT
        X = 1 ; Y = 22
        FOR Z = X TO LEN(R.ACCT.ENT)
            CALL F.READ(FN.STMT,R.ACCT.ENT[Z,Y], R.STMT,F.STMT, ETEXT)
*PRINT R.ACCT.ENT[Z,Y]
            ID.NO = "SE*":R.STMT<AC.STE.SYSTEM.ID>:"*":R.STMT<AC.STE.ACCOUNT.OFFICER>:"*":R.ACCT.ENT[Z,Y]:"*":TODAY
            PRINT ID.NO
****UPDATED BY NESSREEN AHMED ON 25/06/2008***
            CALL F.READ(FN.COUNT,ID.NO,R.COUNT,F.COUNT, ETEXT)
            IF ETEXT THEN
***********************************************
                R.COUNT<TRANS.ACCOUNT.NUMBER> = R.STMT<AC.STE.ACCOUNT.NUMBER>
*Line [ 139 ] Update EB.ACCOUNT.TITLE.1 to AC.ACCOUNT.TITLE.1 - ITSS - R21 Upgrade - 2021-12-23
*CALL DBR('ACCOUNT':@FM:EB.ACCOUNT.TITLE.1,R.STMT<AC.STE.ACCOUNT.NUMBER>,TITLE)
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,R.STMT<AC.STE.ACCOUNT.NUMBER>,TITLE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.STMT<AC.STE.ACCOUNT.NUMBER>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
TITLE=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>

                R.COUNT<TRANS.NAME> = TITLE
                R.COUNT<TRANS.AMOUNT.LCY> = R.STMT<AC.STE.AMOUNT.LCY>
                R.COUNT<TRANS.TRANSACTION.CODE> = R.STMT<AC.STE.TRANSACTION.CODE>
                R.COUNT<TRANS.CUSTOMER.ID> = R.STMT<AC.STE.CUSTOMER.ID>
                R.COUNT<TRANS.ACCOUNT.OFFICER> = R.STMT<AC.STE.ACCOUNT.OFFICER>
                R.COUNT<TRANS.PRODUCT.CATEGORY> = R.STMT<AC.STE.PRODUCT.CATEGORY>
                R.COUNT<TRANS.VALUE.DATE> = R.STMT<AC.STE.VALUE.DATE>
                R.COUNT<TRANS.CURRENCY> = R.STMT<AC.STE.CURRENCY>
                R.COUNT<TRANS.AMOUNT.FCY> = R.STMT<AC.STE.AMOUNT.FCY>
                R.COUNT<TRANS.BOOKING.DATE> = R.STMT<AC.STE.BOOKING.DATE>
                R.COUNT<TRANS.EXCHANGE.RATE> = R.STMT<AC.STE.EXCHANGE.RATE>
                R.COUNT<TRANS.OUR.REFERENCE> = R.STMT<AC.STE.TRANS.REFERENCE>
                R.COUNT<TRANS.SYSTEM.ID> = R.STMT<AC.STE.SYSTEM.ID>
************UPDATED BY NESSREEN AHMED ON 6/6/2008***********************
                SYSTEM.ID = R.STMT<AC.STE.SYSTEM.ID>
                INP =  R.STMT<AC.STE.INPUTTER>
                BEGIN CASE
                    CASE SYSTEM.ID EQ 'FT'
                        ST.REF = R.STMT<AC.STE.TRANS.REFERENCE>
                        DBREF = ST.REF[1,12]
                        CALL F.READ(FN.FT,DBREF,R.FT,F.FT,FT.ERR)
                        TR.TYPE = R.FT<FT.TRANSACTION.TYPE>
                        DB.REF = R.FT<FT.DEBIT.THEIR.REF>

                        BEGIN CASE
                            CASE (TR.TYPE EQ 'AC22') OR (TR.TYPE EQ 'AC23') OR (TR.TYPE EQ 'AC25')
                                CALL F.READ(FN.BR,DB.REF,R.BR,F.BR,BR.ERR)
                                BR.INP = R.BR<EB.BILL.REG.INPUTTER>
                                BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = BR.INP
                                R.COUNT<TRANS.AUTHORISER> = BR.AUTH
                            CASE TR.TYPE EQ 'AC90'
                                ORD.BNK = R.FT<FT.ORDERING.BANK>
                                CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                                WH.INP = R.WH<SCB.WH.TRANS.INPUTTER>
                                WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = WH.INP
                                R.COUNT<TRANS.AUTHORISER> = WH.AUTH
                            CASE TR.TYPE EQ 'AC92'
                                ORD.BNK = R.FT<FT.ORDERING.BANK>
                                CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                                WH.INP = R.WH<SCB.WH.TRANS.INPUTTER>
                                WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = WH.INP
                                R.COUNT<TRANS.AUTHORISER> = WH.AUTH
                            CASE TR.TYPE EQ 'ACLG'
                                ORD.BNK = R.FT<FT.ORDERING.BANK>
                                CALL F.READ(FN.LD,ORD.BNK[1,12],R.LD,F.LD,LD.ERR)
                                LD.INP = R.LD<LD.INPUTTER>
                                LD.AUTH = R.LD<LD.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = LD.INP
                                R.COUNT<TRANS.AUTHORISER> = LD.AUTH
*********UPDATED BY NESSREEN IN 16/6/2008*********************
                            CASE TR.TYPE EQ 'AC21'
                                ORD.BNK = R.FT<FT.ORDERING.BANK>
                                CALL F.READ(FN.BR,ORD.BNK,R.BR,F.BR,BR.ERR)
                                BR.INP = R.BR<EB.BILL.REG.INPUTTER>
                                BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                                R.COUNT<TRANS.INPUTTER> = BR.INP
                                R.COUNT<TRANS.AUTHORISER> = BR.AUTH
***************************************************
                            CASE OTHERWISE
                                R.COUNT<TRANS.INPUTTER> = R.FT<FT.INPUTTER>
                                R.COUNT<TRANS.AUTHORISER> = R.FT<FT.AUTHORISER>
                        END CASE
*********UPDATED BY NESSREEN IN 16/6/2008******************************************
                    CASE SYSTEM.ID EQ 'CQ'
                        STT.REF = R.STMT<AC.STE.TRANS.REFERENCE>
                        DBREFS = STT.REF[1,12]
                        CALL F.READ(FN.BR,DBREFS,R.BR,F.BR,BR.ERR)
*******UPDATED BY NESSREEN AHMED 09/07/2008*******************
                        IF BR.ERR THEN
                            R.COUNT<TRANS.INPUTTER> = R.STMT<AC.STE.INPUTTER>
                            R.COUNT<TRANS.AUTHORISER> = R.STMT<AC.STE.AUTHORISER>
                        END ELSE
*******END OF UPDATED******************************************
                            BR.INP = R.BR<EB.BILL.REG.INPUTTER>
                            BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                            R.COUNT<TRANS.INPUTTER> = BR.INP
                            R.COUNT<TRANS.AUTHORISER> = BR.AUTH
                        END
                    CASE OTHERWISE
                        R.COUNT<TRANS.INPUTTER> = R.STMT<AC.STE.INPUTTER>
                        R.COUNT<TRANS.AUTHORISER> = R.STMT<AC.STE.AUTHORISER>
                END CASE
************************************************************************
                R.COUNT<TRANS.STMT.NO> = R.STMT<AC.STE.STMT.NO>
** R.COUNT<TRANS.INPUTTER> = R.STMT<AC.STE.INPUTTER>
                R.COUNT<TRANS.DATE.TIME> = R.STMT<AC.STE.DATE.TIME>
** R.COUNT<TRANS.AUTHORISER> = R.STMT<AC.STE.AUTHORISER>
                R.COUNT<TRANS.CRF.PROD.CAT> = R.STMT<AC.STE.CRF.PROD.CAT>
                R.COUNT<TRANS.REF> = R.ACCT.ENT[Z,Y]
*******UPDATED IN 1/6/2008 BY NESSREEN AHMED**********************
                R.COUNT<TRANS.COMPANY.CO>  = R.STMT<AC.STE.COMPANY.CODE>
******************************************************************
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****                WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
****                    PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
****                END
                CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
                CALL JOURNAL.UPDATE(ID.NO)
****END OF UPDATE 24/3/2016*****************************
            END     ;**END OF ETEXT ***
            Z = Z + 22
        NEXT Z
    NEXT I
***********************************************************************
*                                                                     *
*****************************CATEG ENTRY ******************************
*                                                                     *
***********************************************************************

    R.COUNT = ''

    FN.CATEG = 'FBNK.CATEG.ENTRY' ; F.CATEG = '' ; R.CATEG = ''

    CALL OPF( FN.CATEG,F.CATEG)
    F.CATEG.TODAY = '' ; FN.CATEG.TODAY = 'F.CATEG.ENT.TODAY'; R.CATEG.TODAY = ''
    CALL OPF(FN.CATEG.TODAY,F.CATEG.TODAY)

    T.SEL = "SELECT FBNK.CATEG.ENT.TODAY"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    CALL F.READ(FN.ACCT.ENT,'0110073410650101',R.ACCT.ENT,F.ACCT.ENT,READ.ERR)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CATEG.TODAY,KEY.LIST<I>,R.CATEG.TODAY,F.CATEG.TODAY,READ.ERR)
        PRINT KEY.LIST<I>
        PRINT R.CATEG.TODAY
        X = 1 ; Y = 22
        FOR Z = X TO LEN(R.CATEG.TODAY)

            CALL F.READ(FN.CATEG,R.CATEG.TODAY[Z,Y], R.CATEG,F.CATEG, ETEXT)
*PRINT R.ACCT.ENT[Z,Y]

            ID.NO = "CE*":R.CATEG<AC.CAT.SYSTEM.ID>:"*":R.CATEG<AC.CAT.ACCOUNT.OFFICER>:"*":R.CATEG.TODAY[Z,Y]:"*":TODAY
            PRINT ID.NO
***UPDATED BY NESSREN AHMED ON 25/6/2008************
            CALL F.READ(FN.COUNT,ID.NO, R.COUNT,F.COUNT, ETEXT)
            IF ETEXT THEN
*********

                IF R.CATEG<AC.CAT.TRANSACTION.CODE> NE 450 THEN

                    R.COUNT<TRANS.ACCOUNT.NUMBER> = R.CATEG<AC.CAT.ACCOUNT.NUMBER>
                    R.COUNT<TRANS.AMOUNT.LCY> = R.CATEG<AC.CAT.AMOUNT.LCY>
                    R.COUNT<TRANS.TRANSACTION.CODE> = R.CATEG<AC.CAT.TRANSACTION.CODE>
                    R.COUNT<TRANS.CUSTOMER.ID> = R.CATEG<AC.CAT.CUSTOMER.ID>
                    R.COUNT<TRANS.PL.CATEGORY> = R.CATEG<AC.CAT.PL.CATEGORY>
                    R.COUNT<TRANS.ACCOUNT.OFFICER> = R.CATEG<AC.CAT.ACCOUNT.OFFICER>
                    R.COUNT<TRANS.PRODUCT.CATEGORY> = R.CATEG<AC.CAT.PRODUCT.CATEGORY>
                    R.COUNT<TRANS.VALUE.DATE> = R.CATEG<AC.CAT.VALUE.DATE>
                    R.COUNT<TRANS.CURRENCY> = R.CATEG<AC.CAT.CURRENCY>
                    R.COUNT<TRANS.AMOUNT.FCY> = R.CATEG<AC.CAT.AMOUNT.FCY>
                    R.COUNT<TRANS.BOOKING.DATE> = R.CATEG<AC.STE.BOOKING.DATE>
                    R.COUNT<TRANS.EXCHANGE.RATE> = R.CATEG<AC.CAT.EXCHANGE.RATE>
                    R.COUNT<TRANS.OUR.REFERENCE> = R.CATEG<AC.CAT.TRANS.REFERENCE>
                    R.COUNT<TRANS.SYSTEM.ID> = R.CATEG<AC.CAT.SYSTEM.ID>
*********UPDATED BY NESSREEN AHMED ON 6/6/2008***********************
                    SYSTEM.ID.C = R.CATEG<AC.CAT.SYSTEM.ID>
                    BEGIN CASE
                        CASE SYSTEM.ID.C EQ 'FT'
                            CE.REF = R.CATEG<AC.CAT.TRANS.REFERENCE>
                            C.DB.REF = CE.REF[1,12]
                            CALL F.READ(FN.FT,C.DB.REF,R.FT,F.FT,FT.ERR)
                            TR.TYPE = R.FT<FT.TRANSACTION.TYPE>
                            DB.REF = R.FT<FT.DEBIT.THEIR.REF>
                            BEGIN CASE
                                CASE (TR.TYPE EQ 'AC22') OR (TR.TYPE EQ 'AC23') OR (TR.TYPE EQ 'AC25')
                                    CALL F.READ(FN.BR,DB.REF,R.BR,F.BR,BR.ERR)
                                    BR.INP = R.BR<EB.BILL.REG.INPUTTER>
                                    BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                                    R.COUNT<TRANS.INPUTTER> = BR.INP
                                    R.COUNT<TRANS.AUTHORISER> = BR.AUTH
                                CASE TR.TYPE EQ 'AC90'
                                    ORD.BNK = R.FT<FT.ORDERING.BANK>
                                    CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                                    WH.INP = R.WH<SCB.WH.TRANS.INPUTTER>
                                    WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                                    R.COUNT<TRANS.INPUTTER> = WH.INP
                                    R.COUNT<TRANS.AUTHORISER> = WH.AUTH
                                CASE TR.TYPE EQ 'AC92'
                                    ORD.BNK = R.FT<FT.ORDERING.BANK>
                                    CALL F.READ(FN.WH,ORD.BNK,R.WH,F.WH,WH.ERR)
                                    WH.INP = R.WH<SCB.WH.TRANS.INPUTTER>
                                    WH.AUTH = R.WH<SCB.WH.TRANS.AUTHORISER>
                                    R.COUNT<TRANS.INPUTTER> = WH.INP
                                    R.COUNT<TRANS.AUTHORISER> = WH.AUTH
                                CASE TR.TYPE EQ 'ACLG'
                                    ORD.BNK = R.FT<FT.ORDERING.BANK>
                                    CALL F.READ(FN.LD,ORD.BNK[1,12],R.LD,F.LD,LD.ERR)
                                    LD.INP = R.LD<LD.INPUTTER>
                                    LD.AUTH = R.LD<LD.AUTHORISER>
                                    R.COUNT<TRANS.INPUTTER> = LD.INP
                                    R.COUNT<TRANS.AUTHORISER> = LD.AUTH
*********UPDATED BY NESSREEN IN 16/6/2008*********************
                                CASE TR.TYPE EQ 'AC21'
                                    ORD.BNK = R.FT<FT.ORDERING.BANK>
                                    CALL F.READ(FN.BR,ORD.BNK,R.BR,F.BR,BR.ERR)
                                    BR.INP = R.BR<EB.BILL.REG.INPUTTER>
                                    BR.AUTH = R.BR<EB.BILL.REG.AUTHORISER>
                                    R.COUNT<TRANS.INPUTTER> = BR.INP
                                    R.COUNT<TRANS.AUTHORISER> = BR.AUTH
***************************************************
                                CASE OTHERWISE

                                    R.COUNT<TRANS.INPUTTER> = R.FT<FT.INPUTTER>
                                    R.COUNT<TRANS.AUTHORISER> = R.FT<FT.AUTHORISER>
                            END CASE
                        CASE OTHERWISE
                            R.COUNT<TRANS.INPUTTER> = R.CATEG<AC.CAT.INPUTTER>
                            R.COUNT<TRANS.AUTHORISER> = R.CATEG<AC.CAT.AUTHORISER>
                    END CASE
************************************************************************
                    R.COUNT<TRANS.STMT.NO> = R.CATEG<AC.CAT.STMT.NO>
**R.COUNT<TRANS.INPUTTER> = R.CATEG<AC.CAT.INPUTTER>
                    R.COUNT<TRANS.DATE.TIME> = R.CATEG<AC.CAT.DATE.TIME>
** R.COUNT<TRANS.AUTHORISER> = R.CATEG<AC.CAT.AUTHORISER>
                    R.COUNT<TRANS.CRF.PROD.CAT> = R.CATEG<AC.CAT.CRF.PROD.CAT>
                    R.COUNT<TRANS.REF> = R.CATEG.TODAY[Z,Y]
*******UPDATED IN 1/6/2008 BY NESSREEN AHMED**********************
                    R.COUNT<TRANS.COMPANY.CO>  = R.CATEG<AC.CAT.COMPANY.CODE>
******************************************************************
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****                    WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
****                        PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
****                    END
                    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
                    CALL JOURNAL.UPDATE(ID.NO)
****END OF UPDATE 24/3/2016*****************************

                END
            END     ;***END OF ETEXT****
            Z = Z + 22
        NEXT Z

    NEXT I
****************************************************************************************************************
    FN.CON = 'FBNK.CONSOL.ENT.TODAY' ; F.CON = '' ; R.CON = ''

    CALL OPF( FN.CON,F.CON)
    T.SEL = "SELECT FBNK.CONSOL.ENT.TODAY WITH TXN.CODE NE 'ACC'"
**********************************
    R.COUNT = ''
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CON,KEY.LIST<I>, R.CON,F.CON, ETEXT)
**        CALL F.READ(FN.CON,'LD0805600003',R.CON,F.CON, ETEXT)
* ID.NO = KEY.LIST<I>:"*":TODAY
            ID.NO = "CO*":R.CON<RE.CET.PRODUCT>:"*":R.CON<RE.CET.ACCOUNT.OFFICER>:"*":KEY.LIST<I>:"*":TODAY
            PRINT ID.NO
***UPDATED BY NESSREN AHMED ON 25/6/2008************
            CALL F.READ(FN.COUNT,ID.NO, R.COUNT,F.COUNT, ETEXT)
            IF ETEXT THEN
*********
                R.COUNT<TRANS.TRANSACTION.CODE> = R.CON<RE.CET.TXN.CODE> : ".":R.CON<RE.CET.TYPE>
                IF R.CON<RE.CET.LOCAL.DR> LT 0 THEN
                    R.COUNT<TRANS.AMOUNT.LCY> = R.CON<RE.CET.LOCAL.DR>
                END
                IF R.CON<RE.CET.LOCAL.CR> GT 0 THEN
                    R.COUNT<TRANS.AMOUNT.LCY> = R.CON<RE.CET.LOCAL.CR>
                END
                IF R.CON<RE.CET.FOREIGN.DR> LT 0 THEN
                    R.COUNT<TRANS.AMOUNT.FCY> = R.CON<RE.CET.FOREIGN.DR>
                END
                IF R.CON<RE.CET.FOREIGN.CR> GT 0 THEN
*****UPDATED BY NESSREEN AHMED ON 24/06/2008**********************
**  R.COUNT<TRANS.AMOUNT.FCY> = R.CON<RE.CET.LOCAL.CR>
                    R.COUNT<TRANS.AMOUNT.FCY> = R.CON<RE.CET.FOREIGN.CR>
*****END OF UPDATED************************************************
                END
*            TEXT = R.COUNT<TRANS.AMOUNT.LCY> ; CALL REM
                R.COUNT<TRANS.CUSTOMER.ID> = R.CON<RE.CET.CUSTOMER>
                R.COUNT<TRANS.ACCOUNT.OFFICER> = R.CON<RE.CET.ACCOUNT.OFFICER>
                R.COUNT<TRANS.PRODUCT.CATEGORY> = R.CON<RE.CET.PRODUCT.CATEGORY>
                R.COUNT<TRANS.VALUE.DATE> = R.CON<RE.CET.VALUE.DATE>
                R.COUNT<TRANS.CURRENCY> = R.CON<RE.CET.CURRENCY>
                R.COUNT<TRANS.BOOKING.DATE> = R.CON<RE.CET.BOOKING.DATE>
                R.COUNT<TRANS.EXCHANGE.RATE> = R.CON<RE.CET.EXCHANGE.RATE>
                R.COUNT<TRANS.OUR.REFERENCE> = R.CON<RE.CET.TXN.REF>
                R.COUNT<TRANS.SYSTEM.ID> = R.CON<RE.CET.PRODUCT>
                R.COUNT<TRANS.REF> = KEY.LIST<I>
******UPDATED ON 1/6/2008 BY NESSREEN****************************
                FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ;  E1 = ''
                FN.LDH = 'F.LD.LOANS.AND.DEPOSITS$HIS' ; F.LDH = '' ; R.LDH = '' ;  EH1 = ''
                FN.LC = 'F.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = '' ; E2 = ''
                FN.DR = 'F.DRAWINGS' ; F.DR = '' ; R.DR = '' ; E3 = ''
                PROD = R.CON<RE.CET.PRODUCT>
                IF PROD EQ 'LD' THEN
                    CALL OPF(FN.LD,F.LD)
                    CALL F.READ(FN.LD,R.CON<RE.CET.TXN.REF>,R.LD,F.LD,E1)
                    IF NOT(E1) THEN
                        INP = R.LD<LD.INPUTTER>
*Line [ 441 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        DD = DCOUNT(INP,@VM)
                        INP.LD = INP<1,DD>
                        AUTH.LD = R.LD<LD.AUTHORISER>
                        COMP.LD = R.LD<LD.CO.CODE>
                        R.COUNT<TRANS.INPUTTER> = INP.LD
                        R.COUNT<TRANS.AUTHORISER> = AUTH.LD
                        R.COUNT<TRANS.COMPANY.CO> = COMP.LD
                    END ELSE
                        CALL OPF(FN.LDH,F.LDH)
                        ID.LDH1 = R.CON<RE.CET.TXN.REF>:';1'
                        ID.LDH2 = R.CON<RE.CET.TXN.REF>:';2'
                        ID.LDH3 = R.CON<RE.CET.TXN.REF>:';3'
                        ID.LDH4 = R.CON<RE.CET.TXN.REF>:';4'
                        ID.LDH5 = R.CON<RE.CET.TXN.REF>:';5'
                        CALL F.READ(FN.LD,ID.LDH5,R.LD,F.LD,EH5)
                        IF EH5 THEN
                            CALL F.READ(FN.LDH,ID.LDH4,R.LDH,F.LDH,EH4)
                            IF EH4 THEN
                                CALL F.READ(FN.LDH,ID.LDH3,R.LDH,F.LDH,EH3)
                                IF EH3 THEN
                                    CALL F.READ(FN.LDH,ID.LDH2,R.LDH,F.LDH,EH2)
                                    IF EH2 THEN
                                        CALL F.READ(FN.LDH,ID.LDH1,R.LDH,F.LDH,EH1)
                                    END ELSE
                                        INP = R.LDH<LD.INPUTTER>
*Line [ 467 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                        DD = DCOUNT(INP,@VM)
                                        INP.LD = INP<1,DD>
                                        AUTH.LD = R.LDH<LD.AUTHORISER>
                                        COMP.LD = R.LDH<LD.CO.CODE>
                                        R.COUNT<TRANS.INPUTTER> = INP.LD
                                        R.COUNT<TRANS.AUTHORISER> = AUTH.LD
                                        R.COUNT<TRANS.COMPANY.CO> = COMP.LD
                                    END ;*END OF EH2
                                END ELSE
                                    INP = R.LDH<LD.INPUTTER>
*Line [ 478 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                    DD = DCOUNT(INP,@VM)
                                    INP.LD = INP<1,DD>
                                    AUTH.LD = R.LDH<LD.AUTHORISER>
                                    COMP.LD = R.LDH<LD.CO.CODE>
                                    R.COUNT<TRANS.INPUTTER> = INP.LD
                                    R.COUNT<TRANS.AUTHORISER> = AUTH.LD
                                    R.COUNT<TRANS.COMPANY.CO> = COMP.LD
                                END     ;*END OF EH3
                            END ELSE
                                INP = R.LDH<LD.INPUTTER>
*Line [ 489 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                                DD = DCOUNT(INP,@VM)
                                INP.LD = INP<1,DD>
                                AUTH.LD = R.LDH<LD.AUTHORISER>
                                COMP.LD = R.LDH<LD.CO.CODE>
                                R.COUNT<TRANS.INPUTTER> = INP.LD
                                R.COUNT<TRANS.AUTHORISER> = AUTH.LD
                                R.COUNT<TRANS.COMPANY.CO> = COMP.LD
                            END         ;* END OF EH4
                        END ELSE
                            INP = R.LDH<LD.INPUTTER>
*Line [ 500 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            DD = DCOUNT(INP,@VM)
                            INP.LD = INP<1,DD>
                            AUTH.LD = R.LDH<LD.AUTHORISER>
                            COMP.LD = R.LDH<LD.CO.CODE>
                            R.COUNT<TRANS.INPUTTER> = INP.LD
                            R.COUNT<TRANS.AUTHORISER> = AUTH.LD
                            R.COUNT<TRANS.COMPANY.CO> = COMP.LD
                        END   ;*END OF EH5
                    END
                END
****************************END OF UPDATED IN 20/07/2008*****************
                IF PROD EQ 'LCD' THEN
                    CALL OPF(FN.DR,F.DR)
                    CALL F.READ(FN.DR,R.CON<RE.CET.TXN.REF>,R.DR,F.DR,E3)
                    INP.DR = R.DR<TF.DR.INPUTTER>
                    AUTH.DR = R.DR<TF.DR.AUTHORISER>
                    COMP.DR = R.DR<TF.DR.CO.CODE>
                    R.COUNT<TRANS.INPUTTER> =  INP.DR
                    R.COUNT<TRANS.AUTHORISER> = AUTH.DR
                    R.COUNT<TRANS.COMPANY.CO> = COMP.DR
                END
                IF PROD EQ 'LCM' THEN
                    CALL OPF(FN.LC,F.LC)
                    CALL F.READ(FN.LC,R.CON<RE.CET.TXN.REF>,R.LC,F.LC,E2)
                    INP.LC = R.LC<TF.LC.INPUTTER>
                    AUTH.LC = R.LC<TF.LC.AUTHORISER>
                    COMP.LC = R.LC<TF.LC.CO.CODE>
                    R.COUNT<TRANS.INPUTTER> = INP.LC
                    R.COUNT<TRANS.AUTHORISER> = AUTH.LC
                    R.COUNT<TRANS.COMPANY.CO> = COMP.LC
                END
*****************************************************
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****                WRITE R.COUNT TO F.COUNT , ID.NO ON ERROR
****                    PRINT "CAN NOT WRITE RECORD ":ID.NO :" TO ":FN.COUNT
****                END
                CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
                CALL JOURNAL.UPDATE(ID.NO)
****END OF UPDATE 24/3/2016*****************************

*        CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
            END     ;***OF NOT(ETEXT)
        NEXT I
    END

*************************************************************
RETURN
END
