* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.CHQ

*1-CHECK IF CHEQUE ALREADY PRESENTED IN CHEQUES.PRESENTED
*2-CHECK IF THE TYPE OF CHEQUE AND ACCOUNT IS ALREADY PRESENTED
*3-CHECK IF CHEQUE IS STOPED OR NOT

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUES.PRESENTED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
****************MAIN PROGRAM************************
MAIN.PROG:
    IF MESSAGE NE 'VAL' THEN
        GOSUB INTIAL
        IF COMI THEN
            GOSUB CHQ.ISS.EMPTY
            IF SELECTED THEN
                GOSUB CHQ.NO.EXIST
                TETX = "ER": ER
                IF ER EQ 1 THEN
                    ER = ''
                  RETURN
                END ELSE
                    ER = ''
                    GOSUB CHQ.STOP
                 *   TEXT = "ERS": ERS
*                    IF ERS = 1 THEN
*                        ERS = ''
*                       RETURN
*                    END ELSE

*                        GOSUB CHQ.PRESENTED
*                        IF SELECTED THEN
*                    RETURN
*                       END
*               END
                END
            END
        END
        *TEXT = 'COMI':COMI ; CALL REM
        *TEXT = 'RNEW':R.NEW(FT.CHEQUE.NUMBER) ; CALL REM
        R.NEW(FT.CHEQUE.NUMBER) = COMI
        CALL REBUILD.SCREEN

        RETURN
    END
    *TEXT = 'COMI':COMI ; CALL REM
    *TEXT = 'RNEW':R.NEW(FT.CHEQUE.NUMBER) ; CALL REM
    R.NEW(FT.CHEQUE.NUMBER) = COMI
    CALL REBUILD.SCREEN
    RETURN
*************************************************************************************************
INTIAL:
    ETEXT= ''; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH =''
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS =''
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE =''
    CHQ.AMT =''

    RETURN

*********************************************************************************************************

CHQ.ISS.EMPTY:
*TEXT = 'CHQ.ISS.EMPTY' ;  CALL REM

    FN.CHQ.REG = 'FBNK.CHEQUE.REGISTER' ; F.CHQ.REG = ''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)

    CHQ.ID = 'SCB.':R.NEW(FT.DEBIT.ACCT.NO)
*TEXT = 'CHQ.ID': CHQ.ID ;  CALL REM

    T.SEL = "SELECT FBNK.CHEQUE.REGISTER WITH @ID EQ ":CHQ.ID
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
*TEXT = "SELECTED": SELECTED ; CALL REM
        CALL F.READ(FN.CHQ.REG,KEY.LIST,R.CHQ.REG,F.CHQ.REG,E1)
*TEXT = "E1": E1 ; CALL REM
        CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>
*TEXT = "CHQ.NOS": CHQ.NOS ; CALL REM
        CHQ.STOP    = R.CHQ.REG<CHEQUE.REG.STOPPED.CHQS>
*TEXT = "CHQ.STOP": CHQ.STOP ; CALL REM

    END ELSE
        ETEXT = '��� ����� �� ������ ��� �����' ;CALL STORE.END.ERROR
    END
    RETURN
***************************************************************************
CHQ.NO.EXIST:
    *TEXT = 'CHQ.NOS' ;  CALL REM
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    COUNTS1 =DCOUNT(CHQ.NOS,@VM)
    *TEXT = "COUNTS1": COUNTS1 ; CALL REM
    FOR I = 1 TO COUNTS1
        CHQ.LEN = LEN(CHQ.NOS<1,I>)
        IF CHQ.LEN THEN
            GOSUB CHECK.DASH.1
            *TEXT = "FIRST.NO": FIRST.NO ; CALL REM
            *TEXT = "LAST.NO": LAST.NO ; CALL REM
        END
        IF COMI LT FIRST.NO THEN
            ER = 1
            *TEXT = "LT FIRST.NO": ER ; CALL REM
            ETEXT = '����� ��� (&) �� ������ ���': COMI ;CALL STORE.END.ERROR
            I = COUNTS1
        END ELSE
            IF COMI GE FIRST.NO AND COMI LE LAST.NO THEN
                I = COUNTS1
                ER = 2
                *TEXT = "GE LE FIRST.NO": ER ; CALL REM
            END
        END
    NEXT
    IF ER = '' THEN
    ER = 1
    ETEXT =  '����� ��� (&) �� ������ ���': COMI ;CALL STORE.END.ERROR
*    TEXT = "NEXT": ER ; CALL REM
    END
    RETURN
***************************************************************************
CHQ.STOP:

*TEXT = 'CHQ.STOP' ;  CALL REM
*Line [ 153 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    COUNTS1 =DCOUNT(CHQ.STOP,@VM)
*TEXT = "COUNTS1": COUNTS1 ; CALL REM
    FOR I = 1 TO COUNTS1
        CHQ.LEN = LEN(CHQ.STOP<1,I>)
        IF CHQ.LEN THEN
            GOSUB CHECK.DASH

*TEXT = "FIRST.NO": FIRST.NO ; CALL REM
*TEXT = "LAST.NO": LAST.NO ; CALL REM
        END

        IF COMI LE LAST.NO AND COMI GE FIRST.NO THEN
            ETEXT = ''
            ETEXT = '����� ��� (&)�����' ; CALL STORE.END.ERROR
            GOSUB END.PROG
    ERS = 1
 I = COUNTS1
        END ELSE
            GOSUB CHQ.PRESENTED
        END
      FIRST.NO = ''
    LAST.NO = ''
    NEXT I

    RETURN
***************************************************************************
CHQ.PRESENTED:
*TEXT = 'CHQ.PRESENTED' ;  CALL REM

    FN.CHQ.PRESENT = 'F.CHEQUES.PRESENTED' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.DEBIT.ACCT.NO)
*TEXT = "SCB.CHQ.ID": SCB.CHQ.ID ; CALL REM
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT F.CHEQUES.PRESENTED WITH @ID EQ ":SCB.CHQ.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*TEXT = "SLE": SELECTED ; CALL REM

    IF SELECTED THEN
*TEXT  = "SELECTED": SELECTED ; CALL REM
        ETEXT = '����� ��� ����' ;CALL STORE.END.ERROR

        RETURN

    END
    CALL REBUILD.SCREEN
    RETURN
**************************************************************************
CHECK.DASH.1:
    *TEXT = "CHECK DASH.1" ; CALL REM
    FOR J =1 TO CHQ.LEN

        *TEXT = "CHQ.NOS":CHQ.NOS<1,I>
        A<J> = CHQ.NOS<1,I>[J,1]
        *TEXT = "CHQ.NOS":CHQ.NOS<1,I>[J,1]
        *TEXT = "A<J>":A<J>

        IF A<J> EQ '-' THEN
            FIRST.NO  = CHQ.NOS<1,I>[1,J-1]
            LAST.NO   = CHQ.NOS<1,I>[J+1,CHQ.LEN-J]
            X = 1
            *TEXT = "X1" : X ; CALL REM
            J = CHQ.LEN
        END
    NEXT J
    *TEXT = "X2" : X ; CALL REM
    IF X = 0 THEN
        FIRST.NO  = CHQ.NOS<1,I>
        LAST.NO   = CHQ.NOS<1,I>
    END
    X = 0
    A = ''
    RETURN

***************************************************************************
CHECK.DASH:
*TEXT = "CHECK DASH" ; CALL REM
    FOR J =1 TO CHQ.LEN
        *TEXT = "CHQ.STOP":CHQ.STOP<1,I>
        A<J> = CHQ.STOP<1,I>[J,1]
        *TEXT = "CHQ.STOP":CHQ.STOP<1,I>[J,1]
       * TEXT = "A<J>":A<J>

        IF A<J> EQ '-' THEN
            FIRST.NO  = CHQ.STOP<1,I>[1,J-1]
            LAST.NO   = CHQ.STOP<1,I>[J+1,CHQ.LEN-J]
            X = 1
*       TEXT = "X1" : X ; CALL REM
            J = CHQ.LEN
        END
    NEXT J
*TEXT = "X2" : X ; CALL REM
    IF X = 0 THEN
        FIRST.NO  = CHQ.STOP<1,I>
        LAST.NO   = CHQ.STOP<1,I>
    END
    X = 0
    A = ''
    RETURN
***************************************************************************
END.PROG:
    ETEXT = ''
    R.NEW(FT.CHEQUE.NUMBER) = COMI
    CALL REBUILD.SCREEN
    RETURN
 END
