* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*----- DINA_SCB ---- 14-7-2002

SUBROUTINE VVR.TT.CH.CURRENCY

* A ROUTIN TO EMPTY THE FIELDS AMOUNT.FCY.1 , AMOUNT.LOCAL.1, ACCOUNT.1 IF THE USER CHANGES THE CURRENCY
* TO CHECK IF THE CURRENCY IS LOCAL THEN MAKE AMOUNT.LOCAL.1 AN INPUT FIELD AND MAKE THE AMOUNT.FCY.1 A
* NOINPUT FIELD 
* TO CHECK IF THE CURRENCY IS FORIGN THEN MAKE AMOUNT.FCY.1 AN INPUT FIELD AND MAKE THE AMOUNT.LOCAL.1 A
* NOINPUT FIELD

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER


IF COMI # R.NEW(TT.TE.CURRENCY.1) THEN
 IF COMI = LCCY THEN
  T(TT.TE.AMOUNT.LOCAL.1)<3>=''
  T(TT.TE.AMOUNT.FCY.1)<3>='NOINPUT'
 END ELSE
   T(TT.TE.AMOUNT.FCY.1)<3>=''
   T(TT.TE.AMOUNT.LOCAL.1)<3>='NOINPUT'
  END
 R.NEW(TT.TE.CURRENCY.2)=''
 R.NEW(TT.TE.AMOUNT.FCY.1)= ''
 R.NEW(TT.TE.AMOUNT.LOCAL.1) = ''
 R.NEW(TT.TE.ACCOUNT.1) = ''
 R.NEW(TT.TE.ACCOUNT.2) = ''
 CALL REBUILD.SCREEN
END

RETURN
END
