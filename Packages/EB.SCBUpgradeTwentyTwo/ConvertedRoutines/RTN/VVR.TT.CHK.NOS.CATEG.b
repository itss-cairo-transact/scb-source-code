* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyTwo  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyTwo
*DONE
** ----- NESSREEN AHMED 08/03/2009
*-----------------------------------------------------------------------------
* <Rating>624</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.TT.CHK.NOS.CATEG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF COMI THEN
        ETEXT = ''
        IF  LEN(COMI) = 16  THEN
            GOSUB INTIAL
******Updated by Nessreen Ahmed 18/4/2016 for R15*************************
******      CALL DBR( 'AC.COLLATERAL':@FM:1,COMI,ACCFND)
****End of Update 18/4/2016 for R15 ****************************
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
            GOSUB CHECK.IF.NOS.OR.INT

        END ELSE
            ETEXT='��� �� ����� ������'
        END

    END

    GOTO PROGRAM.END

***********************************************************************************************************************

INTIAL:

*-------------TO INTIALIZE ALL VARIABLES -----------------------------------------*

    ETEXT = '' ; E = '' ; CURR.NAME1 = '' ; SHOR.NAME1 = ''


    RETURN
***********************************************************************************************************************

CHECK.IF.NOS.OR.INT:


*-------------TO CHECK IF IT IS NOSTRO OR INTERNAL ACCOUNT------------------------*

    IF COMI # R.NEW(TT.TE.ACCOUNT.1)<1,AV> THEN GOSUB CLEAN.FIELDS

    IF NOT(NUM(COMI)) THEN ETEXT='��� ����� ������ ������ ������'

    IF CATEG # '5010' THEN ETEXT='��� ������ ������� ������ ���'
*ETEXT = 'Not.Allowed.For.NOSTRO.Account'
    ELSE GOSUB GET.CUSNAME.CURR



    RETURN
* **********************************************************************************************************************

GET.CUSNAME.CURR:

    TEXT = 'COMI=':COMI ; CALL REM
    ETEXT = ''
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY,COMI,CURR.NAME1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.NAME1=R.ITSS.ACCOUNT<AC.CURRENCY>
* CALL DBR( 'NUMERIC.CURRENCY':@FM:1,COMI[9,2],CURR.NAME1)
    IF ETEXT THEN E = ETEXT

    R.NEW(TT.TE.CURRENCY.1) = CURR.NAME1
    R.NEW(TT.TE.CURRENCY.2) = CURR.NAME1

    ETEXT = ''

*CUST.NO = TRIM( COMI[ 5, 7], '0', 'L')

*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'ACCOUNT':@FM:AC.SHORT.TITLE,COMI,SHOR.NAME1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
SHOR.NAME1=R.ITSS.ACCOUNT<AC.SHORT.TITLE>
    IF ETEXT THEN E = ETEXT

  ***  R.NEW(TT.TE.NARRATIVE.2)<1,AV> = SHOR.NAME1

    GOSUB CHECK.IF.CURR.LOCAL


    RETURN

******************************************************************************************************************
CHECK.IF.CURR.LOCAL:
TEXT = 'CURR.NAME=':CURR.NAME1 ; CALL REM
    IF CURR.NAME1 = LCCY THEN

        T(TT.TE.AMOUNT.LOCAL.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
        T(TT.TE.AMOUNT.FCY.1)<3> = 'NOINPUT'

    END ELSE

        T(TT.TE.AMOUNT.FCY.1)<3> = ''
        R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
        T(TT.TE.AMOUNT.LOCAL.1)<3> = 'NOINPUT'

    END

    GOSUB DEF.DAT

    RETURN

**********************************************************************************************************************
DEF.DAT:

    R.NEW(TT.TE.VALUE.DATE.1) = TODAY
* R.NEW(TT.TE.EXPOSURE.DATE.2) = TODAY


    RETURN
***********************************************************************************************************************
CLEAN.FIELDS:

    R.NEW(TT.TE.ACCOUNT.1)<1,AV> = ''
    R.NEW(TT.TE.ACCOUNT.2) = ''
    R.NEW(TT.TE.CURRENCY.1)<1,AV>= ''
    R.NEW(TT.TE.CURRENCY.2)= ''
    R.NEW(TT.TE.AMOUNT.LOCAL.1)<1,AV> = ''
    R.NEW(TT.TE.AMOUNT.FCY.1)<1,AV> = ''
    R.NEW(TT.TE.NARRATIVE.2)<1,AV> = ''

    RETURN

***********************************************************************************************************************
PROGRAM.END:

    CALL REBUILD.SCREEN

    RETURN
END
