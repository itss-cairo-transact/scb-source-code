* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.CUS.POS.REV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    CUST.ID=R.NEW(LD.CUSTOMER.ID)
    CURR=R.NEW(LD.CURRENCY)
***    TEXT=CURR:'CURR.FROM.VERSION';CALL REM
    DAT=TODAY
    YEAR.MONTH=DAT[1,4]:DAT[5,2]
    ID=CUST.ID:".":YEAR.MONTH
***    TEXT=ID;CALL REM
    TXN.ISSUE=R.OLD(LD.AMOUNT)*-1
    TEXT=TXN.ISSUE:"REV";CALL REM
***    TEXT=TXN.ISSUE;CALL REM
* COUNT.AMT=DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
* FOR I=1 TO COUNT.AMT
    TOT.COM=R.OLD(LD.CHRG.AMOUNT)<1,1>*-1
*    TEXT=TOT.COM;CALL REM
    TXN.COMMISSION=TOT.COM
* NEXT I
    TXN.CANCEL='';TXN.CONFIS='';TXN.INC='';TXN.DEC=''
    CALL SCB.LG.CUS.POSITION(ID,CURR,TXN.ISSUE,TXN.CANCEL,TXN.INC,TXN.DEC,TXN.COMMISSION,TXN.CONFIS)
    RETURN
END
