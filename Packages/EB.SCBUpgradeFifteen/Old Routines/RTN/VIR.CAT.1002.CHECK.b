* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.CAT.1002.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*----------------------------------------------------------


    FN.ACC = 'FBNK.ACCOUNT'      ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

*Line [ 37 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.ACC = DCOUNT (R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
    FOR  NUMBER.ACC = 1 TO DCOUNT.ACC

        IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC> NE '' THEN
            IF R.NEW(INF.MLT.SIGN)<1,NUMBER.ACC> EQ 'CREDIT' THEN

                ACC.IDD = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>

                CALL F.READ(FN.ACC,ACC.IDD,R.ACC,F.ACC,ER1)
                ACC.CATG  = R.ACC<AC.CATEGORY>

                IF ACC.CATG EQ '1002'  THEN
                    E = "��� ����� ������� ��� ����� ������"
                    CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR

                END

                IF ACC.CATG EQ '1008'  THEN
                    E = "��� ����� ������� ��� ���� 1008"
                    CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR

                END

            END
        END

    NEXT  NUMBER.ACC

    RETURN
END
