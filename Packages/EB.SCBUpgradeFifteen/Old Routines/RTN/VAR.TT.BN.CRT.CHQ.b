* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VAR.TT.BN.CRT.CHQ
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ

   * TEXT = "YAAAAAAAAA" ; CALL REM
** TO WRITE IN SCB.FT.DR.CHQ THEN BRANCH
    F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
   *ID=R.NEW(TT.TE.NARRATIVE.1):'.':R.NEW(TT.TE.CHEQUE.NUMBER)
   * TEXT = "HIIIIIIIIII" ; CALL REM
    ID = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NEW.ACCT.OPENED>:'.':R.NEW(TT.TE.CHEQUE.NUMBER)
   * TEXT = "ID = ":ID ; CALL REM
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
        R.COUNT<DR.CHQ.CHEQ.STATUS> = 2
        R.COUNT<DR.CHQ.PAY.BRN> = R.NEW(TT.TE.DEPT.CODE)
        R.COUNT<DR.CHQ.TRANS.PAYMENT> = ID.NEW
        R.COUNT<DR.CHQ.PAY.DATE> = TODAY
      *  TEXT = "PAY.DATE":R.COUNT<DR.CHQ.PAY.DATE> ; CALL REM
 IF V$FUNCTION = 'R' THEN
     R.COUNT<DR.CHQ.CHEQ.STATUS> = 1
     R.COUNT<DR.CHQ.PAY.BRN> = ''
     R.COUNT<DR.CHQ.TRANS.PAYMENT> = ''
 END
        CALL F.WRITE(FN.COUNT,ID,R.COUNT)
      *  CALL JOURNAL.UPDATE(ID)
        CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
      *  CLOSE FN.COUNT
    END

    RETURN
END
