* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>178</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.OFS.BR.REV.CONT.TT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = '' ; ETEXT = ''
    CALL OPF( FN.BR,F.BR)

    BR.ID = ''
    IF V$FUNCTION = 'I' THEN
        BR.ID = R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
        IF BR.ID[1,2] EQ "BR" THEN
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)
        END ELSE
            T.SEL = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
            CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
            CALL F.READ( FN.BR,KEY.LIST, R.BR, F.BR, ETEXT)
            BR.ID = KEY.LIST
        END
        GOSUB COLLECT
    END
* ------------------------------------------------------------------ *
**                            REV                                   **
* ------------------------------------------------------------------ *

    IF V$FUNCTION = 'R' THEN

        BR.ID = R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
        IF R.NEW(TT.TE.NARRATIVE.1)<1,1,1>[1,2] EQ "BR" THEN
            CALL F.READ( FN.BR,R.NEW(TT.TE.NARRATIVE.1)<1,1,1>, R.BR, F.BR, ETEXT)
        END ELSE
            T.SEL = "SELECT FBNK.BILL.REGISTER WITH BILL.CHQ.NO EQ ": R.NEW(TT.TE.NARRATIVE.1)<1,1,1>
            CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
            CALL F.READ( FN.BR,KEY.LIST, R.BR, F.BR, ETEXT)
            BR.ID = KEY.LIST
        END
        GOSUB COLLECT.REV
    END

    RETURN
*----------
COLLECT:
*----------
    CATEG.CR.N = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>[11,4]
    CATEG.DR.N = "1":CATEG.CR.N
**  CO.CODE.N  = C$ID.COMPANY[6,4]
    CO.CODE.N  = R.BR<EB.BILL.REG.CO.CODE>[6,4]
    CURR.N     = R.BR<EB.BILL.REG.CURRENCY>
    DR.ACCT.N  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>
    CR.ACCT.N  = CURR.N:CATEG.DR.N:"0001":CO.CODE.N

*-----
* CR
*-----
    Y.ACCT.N = CR.ACCT.N
    Y.AMT.N  = R.BR<EB.BILL.REG.AMOUNT>

*CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT.N,ACC.OFFICER)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,Y.ACCT.N,AC.COMP)
    ACC.OFFICER  = AC.COMP[8,2]
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT.N,CATEG)
    GOSUB AC.STMT.ENTRY

*-----
* DR
*-----
    Y.ACCT.N = DR.ACCT.N
    Y.AMT.N  = R.BR<EB.BILL.REG.AMOUNT> * -1
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT.N,CATEG)
    GOSUB AC.STMT.ENTRY

    RETURN
*------------
COLLECT.REV:
*------------
    CATEG.CR.N = '' ; CATEG.DR.N = '' ; CO.CODE.N = '' ; CURR.N = '' ; DR.ACCT.N = '' ; CR.ACCT.N = ''
    CATEG.CR.N = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>[11,4]
    CATEG.DR.N = "1":CATEG.CR.N
**  CO.CODE.N  = C$ID.COMPANY[6,4]
    CO.CODE.N  = R.BR<EB.BILL.REG.CO.CODE>[6,4]
    CURR.N     = R.BR<EB.BILL.REG.CURRENCY>
    DR.ACCT.N  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CONT.ACCT>
    CR.ACCT.N  = CURR.N:CATEG.DR.N:"0001":CO.CODE.N
*----
* CR
*----
    Y.ACCT.N = ''   ; Y.AMT.N = '' ; CATEG.N = ''
    Y.ACCT.N = CR.ACCT.N
    Y.AMT.N  = R.BR<EB.BILL.REG.AMOUNT> * -1

    *CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT.N,ACC.OFFICER)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,Y.ACCT.N,AC.COMP)
    ACC.OFFICER  = AC.COMP[8,2]

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATEG)

    GOSUB AC.STMT.ENTRY


*----
* DR
*----
    Y.ACCT.N = ''   ; Y.AMT.N = '' ; CATEG = ''
    Y.ACCT.N = DR.ACCT.N
    Y.AMT.N  = R.BR<EB.BILL.REG.AMOUNT>
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT.N,CATEG)

    GOSUB AC.STMT.ENTRY

    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
*
    TEXT = "COMP= " : ID.COMPANY ; CALL REM

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.N
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '790'
    ENTRY<AC.STE.THEIR.REFERENCE>  = BR.ID
    ENTRY<AC.STE.TRANS.REFERENCE>  = BR.ID
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT.N
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR.N
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = BR.ID

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
*------------------------------------------------------------------------*
END
