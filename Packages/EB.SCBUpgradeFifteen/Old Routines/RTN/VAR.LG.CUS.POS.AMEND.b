* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
******ABEER 29/07/2003************
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.CUS.POS.AMEND

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
TEXT = "1" ; CALL REM
    CUST.ID=R.NEW(LD.CUSTOMER.ID)
    CURR=R.NEW(LD.CURRENCY)
    DAT=TODAY
    YEAR.MONTH=DAT[1,4]:DAT[5,2]
    ID=CUST.ID:".":YEAR.MONTH
*  COUNT.AMT=DCOUNT(R.NEW(LD.CHRG.AMOUNT),VM)
*  FOR I=1 TO COUNT.AMT
    TOT.COM=TOT.COM+R.NEW(LD.CHRG.AMOUNT)<1,1>
*    TEXT=TOT.COM;CALL REM
    TXN.COMMISSION=TOT.COM
* NEXT I
    TXN.ISSUE='';TXN.CANCEL='';TXN.CONFIS=''
***********************************************************************
    OLD.AMOUNT= R.OLD(LD.AMOUNT)
***    TEXT=OLD.AMOUNT;CALL REM
    NEW.AMOUNT=R.NEW(LD.AMOUNT)
***    TEXT=NEW.AMOUNT;CALL REM
    AMT.INC=NEW.AMOUNT-OLD.AMOUNT
**    TEXT=AMT.INC;CALL REM
    AMT.DEC=OLD.AMOUNT-NEW.AMOUNT
*************************************************************************
    IF NEW.AMOUNT GT OLD.AMOUNT THEN
        TXN.INC=AMT.INC
        TXN.DEC=''
    END ELSE
        IF OLD.AMOUNT GT NEW.AMOUNT THEN
            TXN.INC=''
            TXN.DEC=AMT.DEC
        END
    END
**********************************************************************************
    CALL SCB.LG.CUS.POSITION(ID,CURR,TXN.ISSUE,TXN.CANCEL,TXN.INC,TXN.DEC,TXN.COMMISSION,TXN.CONFIS)
TEXT = "2"  ; CALL REM
    RETURN
END
