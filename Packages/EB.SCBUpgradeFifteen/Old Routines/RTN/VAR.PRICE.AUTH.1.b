* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.PRICE.AUTH.1
*    PROGRAM VAR.PRICE.AUTH

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.INVEST.FUND
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND.PRICE

    EXECUTE 'COMO ON VAR.PRICE.AUTH.1'
    CRT 'EXECUTE XXXXXX'  
    GOSUB PROC 
    EXECUTE 'COMO OFF VAR.PRICE.AUTH.1'

    RETURN
PROC:
    className = 't24java1.T24Java1'
    methodName = '$CallGetPrice'
    TAB.PRICE = ''
    TAB.CODE = R.NEW(SCB.FP.CODE)
    TAB.DATE = ''
    CRT 'PRICE : ':TAB.PRICE
    PARAM = TAB.CODE 
    CRT 'PARAM : ':PARAM
    CALLJ className, methodName, PARAM SETTING RET ON ERROR GOTO errHandler
    CRT 'return form java : ':RET
    CHANGE '"' TO '' IN RET
    CRT 'LEN : ':LEN(RET)
    RET = RET[2,LEN(RET)-2]
    CRT 'RET AFTER MODIFY : ':RET
    TAB.PRICE = FIELD(RET,',',1)
    TAB.DATE = FIELD(RET,',',2)
    TAB.PRICE = FIELD(TAB.PRICE,':',2) 
    CRT 'PRICE AFTER ROUND : ':TAB.PRICE
    TAB.DATE = FIELD(TAB.DATE,':',2)
    CRT 'PRICE : ':TAB.PRICE
    CRT 'DATE : ':TAB.DATE

    R.NEW(SCB.FP.PRICE) = TAB.PRICE
    R.NEW(SCB.FP.IC.DATE) = TAB.DATE
    IF TAB.PRICE GE 0 THEN
        R.NEW(SCB.FP.REPLY.CODE) = ',RESPONS.CODE=A00'
        CRT 'RESPONSE CODE = A00'
    END
    ELSE
        R.NEW(SCB.FP.REPLY.CODE) = ',RESPONS.CODE=D12'
        CRT 'RESPONSE CODE = D12'
    END
    CRT "Received batch from Java : " : RET
    RETURN

errHandler:
    err = SYSTEM(0)
    CRT 'ERROR : ':err
    IF err = 2 THEN

        CRT "Cannot find the JVM.dll !"

        RETURN

    END

    IF err = 3 THEN

        CRT "Class " : className : "doesn't exist !"

        RETURN

    END

    IF err = 5 THEN

        CRT "Method " : methodName : "doesn't exist !"

        RETURN

    END
    RETURN
END
