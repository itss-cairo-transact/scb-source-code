* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* Create By Nessma Mohammad
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.WRT.TEMP

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT           I_F.SCB.AC.INVEST.EGP
    $INSERT           I_FT.LOCAL.REFS
*-----------------------------------------------
    FN.TMP = "F.SCB.AC.INVEST.EGP" ;  F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    TMP.ID = R.NEW(FT.CREDIT.THEIR.REF)
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ERR.TMP)
    R.TMP<AC.INV.LAST.RUN.DATE> = TODAY

*WRITE R.TMP TO F.TMP , TMP.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':TMP.ID:' TO FILE ':FN.TMP
*END
    CALL F.WRITE (FN.TMP, TMP.ID,R.TMP)
*----------------------------------------------
    RETURN
END
