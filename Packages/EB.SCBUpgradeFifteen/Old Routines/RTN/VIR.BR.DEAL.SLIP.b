* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* Create By Nahrawy
* Edit By Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.BR.DEAL.SLIP

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT            I_BR.LOCAL.REFS
*----------------------------------------
    VV = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE>
    XX = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA>
    IN.OUT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL>

    IF ( VV EQ 7 OR IN.OUT EQ 'NO' ) AND XX EQ '1' THEN
        CALL PRODUCE.DEAL.SLIP("BR.DOCET3.1")
    END
    RETURN
END
