* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>365</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BR.PAID.CHQ

**  TO WRITE ON TABLE THE PAID OLD CHEQUES NUMBERS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*-------------------------------------------------
    IF V$FUNCTION = 'A' THEN
        FN.P.CHQ  = 'F.SCB.P.CHEQ'       ; F.P.CHQ = '' ; R.P.CHQ = ''
        ETEXT     = ''
        FN.BR     = 'FBNK.BILL.REGISTER' ; F.BR    = '' ; R.BR    = ''
        CALL OPF( FN.BR,F.BR)

*Line [ 56 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        FOR H = 1 TO YY

            BR.ID      = R.NEW(SCB.BT.OUR.REFERENCE)<1,H>
            CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E11)
            RET.REASON = R.NEW(SCB.BT.RETURN.REASON)<1,H>

            IF RET.REASON EQ '' THEN
                BNK  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BANK>
                TEXT = "BANK = ":BNK ; CALL REM

                CHQ.NO   = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.NO>
                ACCT.NO  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT>
                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN1)
                MYBRN  = MYBRN1[2]
*-- EDIT BY NESSMA 2016/08/02
                MYBRN = TRIM(MYBRN, "0" , "L")
*-- END EDIT
                OLD.CHQ  = CHQ.NO:'.':MYBRN
                TEXT  =  "CHQ.NO = ":OLD.CHQ ; CALL REM
                ETEXT =  ""

                IF BNK EQ '0017'  THEN
                    CALL OPF( FN.P.CHQ,F.P.CHQ)
                    CALL F.READ(FN.P.CHQ, OLD.CHQ, R.P.CHQ, F.P.CHQ, ETEXT)

                    IF ETEXT THEN
                        R.P.CHQ<P.CHEQ.CHEQ.VAL>   = TODAY
                        R.P.CHQ<P.CHEQ.TRN.DAT>    = TODAY
                        R.P.CHQ<P.CHEQ.ACCOUNT.NO> = ACCT.NO
                        R.P.CHQ<P.CHEQ.OUR.REF>    = ID.NEW
                        R.P.CHQ<P.CHEQ.COMPANY.CO> = C$ID.COMPANY
                        CALL F.WRITE(FN.P.CHQ,OLD.CHQ,R.P.CHQ)
                    END ELSE
                        E = '��� �� ��� ��� ����� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
                    END
                END
            END
        NEXT H
    END
    RETURN
END
