* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.CHRG.PAY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.CHARGE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* UPDATE THE TABLE SCB.STMT.CHARGE
    COMP1 = ID.COMPANY
    XX1.FLG = ''
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS) NE 'RNAU' THEN
        F.COUNT = '' ; FN.COUNT = 'F.SCB.STMT.CHARGE' ; R.COUNT = ''
        CALL OPF(FN.COUNT,F.COUNT)
        CUS.ID = R.NEW(FT.DEBIT.CUSTOMER)
        IF R.NEW(FT.LOCAL.REF)<1,5> EQ 'YES' THEN
            XX1.FLG = 'POST.FLG'
        END ELSE
            XX1.FLG = 'STMT.FLG'
        END
        T.SEL  = "SELECT F.SCB.STMT.CHARGE WITH @ID LIKE ":CUS.ID:"... "
        T.SEL := " AND ":XX1.FLG:" EQ 'YES' "
        T.SEL := " AND CO.CODE EQ ":COMP1
        T.SEL := " BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CALL F.READ(FN.COUNT,KEY.LIST<1>,R.COUNT,F.COUNT,ERR.COUNT)
            IF R.NEW(FT.LOCAL.REF)<1,5> EQ 'YES' THEN
                R.COUNT<STM.POST.FLG> = ''
            END
            IF R.NEW(FT.LOCAL.REF)<1,25> EQ 'YES' THEN
                R.COUNT<STM.STMT.FLG> = ''
            END
            CALL F.WRITE(FN.COUNT,KEY.LIST<1>,R.COUNT)
            CLOSE F.COUNT
        END
    END
**************************************************************
    RETURN
END
