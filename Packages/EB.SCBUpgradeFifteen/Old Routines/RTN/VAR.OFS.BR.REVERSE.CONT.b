* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>146</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.OFS.BR.REVERSE.CONT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER



**********************************************************************************

    IF PGM.VERSION = ",SCB.CHQ.LCY.REG1.REVERSE" THEN
******
        FT.REF.CONT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.FT.REF.CONT>
        FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = '' ; ETEXT = ''
        CALL OPF( FN.FT,F.FT)
        DAT = TODAY
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.STATUS.DATE> EQ DAT THEN
            T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH @ID LIKE ":FT.REF.CONT:"..."
        END ELSE
            T.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH @ID LIKE ":FT.REF.CONT:"..."
        END
        CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
        IF SELECTED THEN
            FT.REF.CONT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.FT.REF.CONT>:";1"
            BR.ID       = ID.NEW
            GOSUB CREATE.REVERSE
        END ELSE
            FT.REF.CONT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.FT.REF.CONT>
            BR.ID       = ID.NEW
            GOSUB CREATE.REVERSE
        END
******
    END

**********************************************************************************

***    IF PGM.VERSION = ",SCB.BT.BATCH" OR PGM.VERSION = ",SCB.BT.BATCH1" OR PGM.VERSION = ",SCB.BT.BATCH.FCY" OR PGM.VERSION = ",SCB.BT.BATCH.FCY.NOT" OR PGM.VERSION = ",SCB.BT.BATCH.BILL" OR PGM.VERSION = ",SCB.BT.BATCH.CORRECT" OR  PGM.VERSION  =",SCB.BT.CLR.FCY" THEN

*Line [ 76 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BT.NUMBER = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
    FOR  I = 1 TO BT.NUMBER

        FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BRRR = '' ; ETEXT = ''
        CALL OPF( FN.BR,F.BR)
        T.SEL = "SELECT FBNK.BILL.REGISTER WITH @ID EQ ": R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
        CALL EB.READLIST( T.SEL, KEY.LIST1, '', SELECTED, ETEXT)
        CALL F.READ( FN.BR,KEY.LIST1, R.BRRR, F.BR, ETEXT)
******
        FT.REF.CONT = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.FT.REF.CONT>
        FN.FT = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT = '' ; ETEXT = ''
        CALL OPF( FN.FT,F.FT)
        T.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH @ID LIKE ":FT.REF.CONT:"..."
        CALL EB.READLIST( T.SEL, KEY.LIST, '', SELECTED, ETEXT)
        IF SELECTED THEN
            FT.REF.CONT = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.FT.REF.CONT>:";1"
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
*TEXT = FT.REF.CONT ; CALL REM
            GOSUB CREATE.REVERSE
        END ELSE
            FT.REF.CONT = R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.FT.REF.CONT>
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
*TEXT = BR.ID-FT.REF.CONT ; CALL REM
            GOSUB CREATE.REVERSE
        END
******
    NEXT I
***   END
    RETURN

**********************************************************************************
CREATE.REVERSE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE  = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR/R"
*** OFS.USER.INFO    = "/"
************HYTHAM********20090128*******************************
    COMP = C$ID.COMPANY
***   OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP

**************HYTHAM******20090128*******************************
    OFS.TRANS.ID     = FT.REF.CONT
    OFS.MESSAGE.DATA = ""
    COMMA            = ","
    DAT              = TODAY

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:FT.REF.CONT
    OFS.ID = "T":TNO:".":ID.NEW:".":BR.ID:".":"REV.CONT":"-":DAT

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E


    RETURN
END
**********************************************************************************
