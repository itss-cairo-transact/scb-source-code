* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>973</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.BS.CHRG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS

    IF R.NEW(SCB.BS.DRAWER)[1,3] NE '994' THEN
        IF R.NEW(SCB.BS.CHG.AMT) THEN
            IF R.NEW(SCB.BS.CHG.AMT) GT 0 THEN
*---------------------
                CATEG.CR   = R.NEW(SCB.BS.CUST.ACCT)[11,4]
                CURR       = R.NEW(SCB.BS.CURRENCY)
                DR.ACCT    = R.NEW(SCB.BS.CUST.ACCT)
                AMT        = R.NEW(SCB.BS.CHG.AMT)
                V.DATE     = R.NEW(SCB.BS.RECEIVE.DATE)
                CHR.TYP    = R.NEW(SCB.BS.CHG.TYPE)
                DRAWER     = R.NEW(SCB.BS.DRAWER)

                FN.CHRG = 'FBNK.FT.CHARGE.TYPE' ; F.CHRG = ''
                CALL OPF(FN.CHRG,F.CHRG)
                CALL F.READ(FN.CHRG,CHR.TYP,R.CHRG,F.CUR,ERR11)
                PL.CHRG = R.CHRG<FT5.CATEGORY.ACCOUNT>

                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,DR.ACCT,AC.COMP)
                ACC.OFFICER  = AC.COMP[8,2]

                IF CURR NE 'EGP' THEN
                    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
                    CALL OPF(FN.CUR,F.CUR)
                    CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
                    RATE   = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                END

                IF V$FUNCTION EQ 'A' THEN
                    CALL DBR('SCB.BR.SLIPS':@FM:SCB.BS.DRAWER,ID.NEW,BR.EXIT)
                    IF NOT(BR.EXIT) THEN
*-----
* CR
*-----
                        Y.ACCT = ''
                        IF CURR NE 'EGP' THEN
                            LCY.AMT = AMT * RATE
                            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                            FCY.AMT = AMT
                        END ELSE
                            LCY.AMT = AMT
                            FCY.AMT = ''
                        END
                        CATEG  = CATEG.CR
                        GOSUB AC.STMT.ENTRY
*----
* DR
*----
                        Y.ACCT = DR.ACCT
                        PL.COM = ''
                        IF CURR NE 'EGP' THEN
                            LCY.AMT = (AMT * RATE ) * -1
                            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                            FCY.AMT = AMT * -1
                        END ELSE
                            LCY.AMT = AMT * -1
                            FCY.AMT = ''
                        END
                        CATEG  = CATEG.CR

                        GOSUB AC.STMT.ENTRY
                    END
                END
                IF V$FUNCTION EQ 'R' THEN
*----
* CR
*----
                    Y.ACCT = DR.ACCT
                    PL.COM = ''
                    IF CURR NE 'EGP' THEN
                        LCY.AMT = AMT * RATE
                        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                        FCY.AMT = AMT
                    END ELSE
                        LCY.AMT = AMT
                        FCY.AMT = ''
                    END
                    CATEG  = CATEG.CR

                    GOSUB AC.STMT.ENTRY
*----
* DR
*----
                    Y.ACCT = ''
                    IF CURR NE 'EGP' THEN
                        LCY.AMT = (AMT * RATE ) * -1
                        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                        FCY.AMT = AMT * -1
                    END ELSE
                        LCY.AMT = AMT * -1
                        FCY.AMT = ''
                    END
                    CATEG  = CATEG.CR
                    GOSUB AC.STMT.ENTRY
                END
            END
        END
    END
    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
    ENTRY = ""
    MULTI.ENTRIES = ""

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '127'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.CHRG
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
*    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = DRAWER
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
