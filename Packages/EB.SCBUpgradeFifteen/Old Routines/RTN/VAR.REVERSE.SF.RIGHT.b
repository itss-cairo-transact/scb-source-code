* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>177</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.REVERSE.SF.RIGHT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*----------------------------------------
    FN.SF = 'F.SCB.SAFEKEEP'      ; F.SF = ''
    CALL OPF(FN.SF,F.SF)

    FN.SF.R = 'F.SCB.SAFEKEEP.RIGHT'      ; F.SF.R = ''
    CALL OPF(FN.SF.R,F.SF.R)
*---
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SAF.RIG.RECORD.STATUS)='RNAU' THEN
        SF.ID = R.NEW(SCB.SAF.RIG.BB.SAFE.KEEP.NO)
        CALL F.READ(FN.SF,SF.ID,R.SF ,F.SF,ERR.SF)

        R.SF<SCB.SAF.SAFF.USED>    = 'NO'
        R.SF<SCB.SAF.CUSTOMER.NO>  = ''
        R.SF<SCB.SAF.LEASING.DATE> = ''

        CALL F.WRITE(FN.SF,SF.ID,R.SF)

    END
*----------------------------------------
    RETURN
END
