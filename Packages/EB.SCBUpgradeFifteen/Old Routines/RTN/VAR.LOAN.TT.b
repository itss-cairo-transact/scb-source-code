* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.LOAN.TT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*----------------------------------------------
    ACCT = R.NEW(TT.TE.ACCOUNT.1)
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT,CATT)

    IF ( CATT EQ 1230 OR CATT EQ 1595 OR CATT EQ 1596 OR CATT EQ 1598 OR CATT EQ 1585 OR CATT EQ 1587 OR CATT EQ 1527 OR CATT EQ 1528 OR CATT EQ 1567 OR CATT EQ 1512 OR CATT EQ 1514 OR CATT EQ 1524 OR CATT EQ 1508 OR CATT EQ 1502 OR CATT EQ 1404 OR CATT EQ 1438 OR CATT EQ 1589) THEN
        FN.ACC = 'F.SCB.LOAN.AC' ;F.ACC = '' ; R.ACC = ''
        CALL OPF(FN.ACC,F.ACC)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""
        R.ACC<AC.LO.ACCOUNT.NO> = R.NEW(TT.TE.ACCOUNT.1)
        R.ACC<AC.LO.OLD.REF>    = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
        R.ACC<AC.LO.AMOUNT>     = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        R.ACC<AC.LO.CURRENCY>   = R.NEW(TT.TE.CURRENCY.1)

        IF R.NEW(TT.TE.CURRENCY.1) EQ 'EGP' THEN
            R.ACC<AC.LO.OUTSTAND.AMOUNT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        END ELSE
            R.ACC<AC.LO.OUTSTAND.AMOUNT> =  R.NEW(TT.TE.AMOUNT.FCY.1)
        END

        R.ACC<AC.LO.ISSUE.DATE> = TODAY
        R.ACC<AC.LO.MAT.DATE>   = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.MATURITY.DATE>
        R.ACC<AC.LO.STATUS>     = 'O'
        R.ACC<AC.LO.OTHER.REF>  = 'CHQ'

        ACCT =  R.NEW(TT.TE.ACCOUNT.1)
        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,ACCT,COMP)
        R.ACC<AC.LO.CO.CODE.1>  = COMP

        R.ACC<AC.LO.DATE.TIME.1> = R.NEW(TT.TE.DATE.TIME)<1,1>
        R.ACC<AC.LO.INPUTTER>    = R.NEW(TT.TE.INPUTTER)<1,1>
        R.ACC<AC.LO.AUTHORISER>  = R.NEW(TT.TE.AUTHORISER)


        ID.NO = R.NEW(TT.TE.ACCOUNT.1) :"-": ID.NEW
        R.ACC<AC.LO.OUR.REF> = ID.NEW

        NOO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.MATURITY.DATE>
        IF NOO THEN
            WRITE  R.ACC TO F.ACC , ID.NO  ON ERROR
                PRINT "CAN NOT WRITE RECORD":ID.NO:"TO" :FN.ACC
            END
        END

        IF V$FUNCTION = 'A' AND  R.NEW(TT.TE.RECORD.STATUS)='RNAU' THEN
            ID.NO = R.NEW(TT.TE.ACCOUNT.1) :"-": ID.NEW
            DELETE F.ACC , ID.NO
        END
    END
*---------------------------------------------------------------
    RETURN
END
