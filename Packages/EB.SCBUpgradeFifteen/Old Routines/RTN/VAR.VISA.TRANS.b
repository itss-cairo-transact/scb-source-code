* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>328</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.VISA.TRANS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES.NEW
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.DAILY.TRN
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CURR.CODES

*AN AUTHORIZED ROUTINE TO WRITE THE DAILY TRANSACTIONS FOR EVERY CUSTOMER

*********************************************************************************************************
    F.VISA.TRANS = '' ; FN.VISA.TRANS = 'F.SCB.VISA.TRANS' ; R.VISA.TRANS = '' ; E2 = '' ; RETRY2 = ''
    CALL OPF(FN.VISA.TRANS,F.VISA.TRANS)
*********************************************************************************************************
    TEXT = 'HI' ; CALL REM

    ORG.MSG = '' ; MSG.TYPE = '' ; PROC.CODE = '' ; DAILY.COMB = '' ; CODES.COMB = ''
    ORG.MSG.CODE = '' ; MSG.TYPE.CODE = '' ; PROC.CODE.CODE = '' ; DB.CR.CODE = '' ; REASON.CO.CODE = '' ; CODE.TO.USE = ''

    ORG.MSG.F = R.NEW(SCB.DAILY.ORG.MSG.TYPE)
*Line [ 58 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    TT = DCOUNT(ORG.MSG.F,@VM)
    TEXT = 'TT=':TT ; CALL REM
    ORG.MSG   = R.NEW(SCB.DAILY.ORG.MSG.TYPE)<1,TT>
*    TEXT = 'ORG.MSG=':ORG.MSG ; CALL REM
    MSG.TYPE = R.NEW(SCB.DAILY.MSG.TYPE)<1,TT>
*    TEXT = 'MSG=':MSG.TYPE ; CALL REM
    PROC.CODE = R.NEW(SCB.DAILY.PROCESS.CODE)<1,TT>
*    TEXT = 'PROC=':PROC.CODE ; CALL REM
**********NEW****************************************
    DB.CR = R.NEW(SCB.DAILY.DB.CR.FLAG)<1,TT>
    REASON.CO = R.NEW(SCB.DAILY.REASON.CODE)<1,TT>
*****************************************************
*    DAILY.COMB = ORG.MSG:MSG.TYPE:PROC.CODE
    DAILY.COMB = ORG.MSG:MSG.TYPE:PROC.CODE:DB.CR:REASON.CO
    TEXT = 'DACOMB=':DAILY.COMB ; CALL REM
*    N.SEL =  "SELECT F.SCB.VISA.CODES.NEW WITH ORG.MSG.TYPE EQ ":ORG.MSG:" AND MSG.TYPE EQ ":MSG.TYPE:" AND PROCESS.CODE EQ ":PROC.CODE
*        N.SEL := ' EVAL"ORG.MSG.TYPE':":'|':":'MSG.TYPE':":'|':":'PROCESS.CODE':"'

    N.SEL =  "SELECT F.SCB.VISA.CODES.NEW WITH ORG.MSG.TYPE EQ ":ORG.MSG:" AND MSG.TYPE EQ ":MSG.TYPE:" AND PROCESS.CODE EQ ":PROC.CODE:" AND FLAG EQ ":DB.CR:" AND REASON.CODE EQ ":REASON.CO
    KEY.LIST.2=""
    SELECTED.2=""
    ER.MSG.2=""

    CALL EB.READLIST(N.SEL,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
*       TEXT = 'SELECTED=':SELECTED.2 ; CALL REM
    F.VISA.CODE = '' ; FN.VISA.CODE = 'F.SCB.VISA.CODES.NEW' ; R.VISA.CODE = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.VISA.CODE,F.VISA.CODE)
    IF SELECTED.2 THEN
        FOR RR = 1 TO SELECTED.2
*            TEXT = 'KEYLIST=':KEY.LIST.2<RR> ; CALL REM
            CALL F.READU(FN.VISA.CODE, KEY.LIST.2<RR>, R.VISA.CODE, F.VISA.CODE, E3, RETRY3)
            ORG.MSG.CODE.T = R.VISA.CODE<SCB.VICD.ORG.MSG.TYPE>
*            TEXT = 'ORGMSG.SEL=':ORG.MSG.CODE.T ; CALL REM
*Line [ 92 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DCT = DCOUNT(ORG.MSG.CODE.T,@VM)
*            TEXT = 'DCT=':DCT ; CALL REM
            FOR WW = 1 TO DCT
                ORG.MSG.CODE   = R.VISA.CODE<SCB.VICD.ORG.MSG.TYPE,WW>
*                TEXT = 'ORG=':ORG.MSG.CODE ; CALL REM
                MSG.TYPE.CODE  = R.VISA.CODE<SCB.VICD.MSG.TYPE,WW>
*                TEXT = 'MSG=':MSG.TYPE.CODE ; CALL REM
                PROC.CODE.CODE = R.VISA.CODE<SCB.VICD.PROCESS.CODE,WW>
*               TEXT = 'PROC=':PROC.CODE.CODE ; CALL REM
***************************NEW********************************************
                DB.CR.CODE = R.VISA.CODE<SCB.VICD.FLAG,WW>
                REASON.CO.CODE = R.VISA.CODE<SCB.VICD.REASON.CODE,WW>
**************************************************************************
                CODES.COMB = ORG.MSG.CODE:MSG.TYPE.CODE:PROC.CODE.CODE:DB.CR.CODE:REASON.CO.CODE
                TEXT = 'CODCOM=':CODES.COMB ; CALL REM

                IF DAILY.COMB = CODES.COMB THEN
                    CODE.TO.USE = KEY.LIST.2<RR>
                    TEXT = 'CODE=':CODE.TO.USE ; CALL REM
                END
            NEXT WW
        NEXT RR
        TEXT = 'ID.NEW=':ID.NEW ; CALL REM
        CALL F.READU(FN.VISA.TRANS, ID.NEW, R.VISA.TRANS, F.VISA.TRANS, E2, RETRY2)
        IF NOT(E2) THEN
            TRANS.CODE = R.VISA.TRANS<SCB.TRANS.TRANS.CODE>
*Line [ 119 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DTC = DCOUNT(TRANS.CODE,@VM)
*       TEXT = 'DTCEXIST=':DTC ; CALL REM
            XX= DTC+1
            R.VISA.TRANS<SCB.TRANS.TRANS.CODE,XX>= CODE.TO.USE
TEXT= R.VISA.TRANS<SCB.TRANS.TRANS.CODE,XX> : CODE.TO.USE ;CALL REM
            R.VISA.TRANS<SCB.TRANS.TRANS.CURR,XX>= R.NEW(SCB.DAILY.BILL.CURR)<1,TT>
            R.VISA.TRANS<SCB.TRANS.TRANS.AMT,XX>= R.NEW(SCB.DAILY.BILL.AMT)<1,TT>
            R.VISA.TRANS<SCB.TRANS.POS.DATE,XX>= R.NEW(SCB.DAILY.POS.DATE)<1,TT>
            CALL F.WRITE(FN.VISA.TRANS,ID.NEW, R.VISA.TRANS)
            CALL JOURNAL.UPDATE(ID.NEW)
        END ELSE
            TRANS.CODE = R.VISA.TRANS<SCB.TRANS.TRANS.CODE>
*Line [ 132 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DTC = DCOUNT(TRANS.CODE,@VM)
*      TEXT = 'DTCNEW=':DTC ; CALL REM
            XX= DTC+1
            R.VISA.TRANS<SCB.TRANS.BRANCH.NUMBER> = R.NEW(SCB.DAILY.CARD.BR)
            R.VISA.TRANS<SCB.TRANS.CUST.NO> = R.NEW(SCB.DAILY.CUST.NO)
            R.VISA.TRANS<SCB.TRANS.CUST.NAME>= R.NEW(SCB.DAILY.CUST.NAME)
            R.VISA.TRANS<SCB.TRANS.CUST.ACCT>= R.NEW(SCB.DAILY.CUST.ACCT)
            R.VISA.TRANS<SCB.TRANS.TRANS.CODE,XX>= CODE.TO.USE
            R.VISA.TRANS<SCB.TRANS.TRANS.CURR,XX>= R.NEW(SCB.DAILY.BILL.CURR)<1,TT>
            R.VISA.TRANS<SCB.TRANS.TRANS.AMT,XX>= R.NEW(SCB.DAILY.BILL.AMT)<1,TT>
            R.VISA.TRANS<SCB.TRANS.POS.DATE,XX>= R.NEW(SCB.DAILY.POS.DATE)<1,TT>
            CALL F.WRITE(FN.VISA.TRANS,ID.NEW, R.VISA.TRANS)
            CALL JOURNAL.UPDATE(ID.NEW)
        END
    END
*  NEXT NN

    RETURN
END
