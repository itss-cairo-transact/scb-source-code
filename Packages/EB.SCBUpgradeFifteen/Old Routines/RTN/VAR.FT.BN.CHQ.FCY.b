* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.FT.BN.CHQ.FCY

*1-ROUTINE TO WRITE ON THE FILE(SCB.FT.DR.CHQ) ACCOURDING TO (FT) RECOURD
*  WITH FUNCTION AUTHORISE


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP1 = ID.COMPANY
    IF R.NEW(FT.RECORD.STATUS)[1,3] EQ 'INA' THEN
        F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
        CALL OPF(FN.COUNT,F.COUNT)
        ID.NO = R.NEW(FT.DEBIT.ACCT.NO):".":R.NEW(FT.CHEQUE.NUMBER)
        T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ": ID.NO
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* TEXT = "KEY.LIST=":KEY.LIST ; CALL REM
        IF KEY.LIST THEN
            E = 'THE CHEQUE ISSUED BEFORE' ; CALL ERR; MESSAGE='REPEAT'
        END

* TEXT = ID.NO ; CALL REM
        R.COUNT = ''
        R.COUNT<DR.CHQ.COMPANY.CO> = COMP1
        R.COUNT<DR.CHQ.DEBIT.ACCT> = R.NEW(FT.DEBIT.ACCT.NO)
        R.COUNT<DR.CHQ.CHEQ.NO> = R.NEW(FT.CHEQUE.NUMBER)
        R.COUNT<DR.CHQ.AMOUNT> = R.NEW(FT.DEBIT.AMOUNT)
        R.COUNT<DR.CHQ.CURRENCY> = R.NEW(FT.DEBIT.CURRENCY)
        R.COUNT<DR.CHQ.BEN,1> = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1>
        R.COUNT<DR.CHQ.BEN,2> = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,2>
        R.COUNT<DR.CHQ.CHEQ.TYPE> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>
        R.COUNT<DR.CHQ.NOS.ACCT> = R.NEW(FT.CREDIT.ACCT.NO)
        R.COUNT<DR.CHQ.TRANS.ISSUE> = ID.NEW
        R.COUNT<DR.CHQ.CHEQ.STATUS> = 1
        R.COUNT<DR.CHQ.CHEQ.DATE> = TODAY
        R.COUNT<DR.CHQ.ISSUE.BRN> = R.NEW(FT.DEPT.CODE)
        R.COUNT<DR.CHQ.OLD.CHEQUE.NO> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
        CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
*CALL JOURNAL.UPDATE(ID.NO)
        CLOSE F.COUNT
    END
******************************************************
    RETURN
END
