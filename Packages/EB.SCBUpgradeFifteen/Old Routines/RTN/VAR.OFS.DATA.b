* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>465</Rating>
*-----------------------------------------------------------------------------
*TO CREATE OFS MESSAGE FOR CREATING FT TRANSACTION
****** WAEL ****
    SUBROUTINE VAR.OFS.DATA

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER



    TEXT = "11" ; CALL REM
    LOCAL.REF = R.NEW(LD.LOCAL.REF)
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CUST = LOCAL.REF< 1,LDLR.THIRD.NUMBER>
* ORDER.BNK =R.NEW(LD.CUSTOMER.ID)
    ORDER.BNK = ID.NEW:'.':MYCODE:'.':MYTYPE


*---------------
    MYID = MYCODE:'.':MYTYPE

*    FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
*   CALL OPF(FN.LG,F.LG)
*   CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)

*  PL.CODE = R.LG<SCB.LG.CH.PL.CATEGORY>


    GOSUB OFS_MAKE
    RETURN

*========================== ISSUE ==========================
    OFS_MAKE:

    IF MYCODE = '1111' THEN
*------------ MARGIN --------------

        TRANS.TYPE = "ACLG"
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>

        IF PGM.VERSION EQ ',SCB.LG.ADVANCE' THEN
        END ELSE
            TRANS.TYPE = "ACLG"
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
            DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
            CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>
            GOSUB PRODUCE.MSG
        END
    END
*========================== OPERAION =============================
    IF MYCODE = '1251' THEN

        TRANS.TYPE = "ACLG"
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>

        GOSUB PRODUCE.MSG
    END
*------------------------
    IF MYCODE = '1252' THEN

        TRANS.TYPE = "ACLG"
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>
        GOSUB PRODUCE.MSG
    END
*================================ INWARD ===========================
*============================== CANCEL =============================
    IF (MYCODE >= '1301' AND MYCODE <= '1303') THEN

* --------  MARGIN --------
        TRANS.TYPE = "ACLG"
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        GOSUB PRODUCE.MSG
    END
*=========================== CONFISCATE ============================
    IF (MYCODE >= '1401' AND MYCODE <= '1407') THEN

*------------------
        CHK.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
        T.SEL = "SELECT FBNK.CHEQUES.PRESENTED WITH @ID LIKE ":'DRFT.':CHK.ACCT:"..."
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CHQ.NOM = 0
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                CURRENT = FIELD(KEY.LIST<I>,"-",2)
                IF CURRENT >= CHQ.NOM THEN CHQ.NOM = CURRENT +1
            NEXT I
        END
        R.NEW(LD.LOCAL.REF)< 1,LDLR.CHEQUE.NO> ="DRFT.":CHK.ACCT:".":CHQ.NOM
*-------------------
        IF R.NEW(LD.FIN.MAT.DATE) = TODAY THEN

            IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONFISC.AMT> = R.NEW(LD.AMOUNT) THEN

* -------------- MARGIN ----------
                TRANS.TYPE = "ACLG"
                VER.NAME = ""
                DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
                DB.AMT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
                DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
                CR.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                GOSUB PRODUCE.MSG
*-------------- DRAFT.CHEQUE ------
                TRANS.TYPE = "ACLG"
                VER.NAME = PGM.VERSION
                CHK.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
                CHQ.TYPE = '1 DRAFT- ��� ����� ����� �����'
                DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
                DB.AMT = R.NEW(LD.AMOUNT)
                DB.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                CR.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
                BENF.CUST = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>

                GOSUB PRODUCE.MSG

            END ELSE
* -------------- MARGIN ----------
                TRANS.TYPE = "ACLG"
                VER.NAME = ""
                DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
                DB.AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
                DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
                CR.ACCT   = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                GOSUB PRODUCE.MSG
*-------------- DRAFT.CHEQUE ------
                TRANS.TYPE = "ACLG"
                VER.NAME = PGM.VERSION
                CHK.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
                CHQ.TYPE = '1 DRAFT- ��� ����� ����� �����'
                DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
                DB.AMT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>
                DB.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                CR.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
                CHQ.NO = CHQ.NOM
                BENF.CUST = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>

                GOSUB PRODUCE.MSG

            END
        END ELSE
*---------- MARGIN -------------
            TRANS.TYPE = "ACLG"

            VER.NAME = ""
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            PER = R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>/100
            DB.AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>*PER
            DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            CR.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT> = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT> - DB.AMT
            CALL REBUILD.SCREEN
            GOSUB PRODUCE.MSG
*---------------- DRAFT.CHEQUE ----------

            TRANS.TYPE = "ACLG"

            VER.NAME = PGM.VERSION
            CHK.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
            CHQ.TYPE = '1 DRAFT- ��� ����� ����� �����'
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = ABS(R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>)
            DB.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            CR.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
            CHQ.NO = "TEST.":CHK.ACCT:".":CHQ.NOM
            CHQ.NO = CHQ.NOM
            BENF.CUST = LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>

            GOSUB PRODUCE.MSG

        END


    END
*============================ AMENDEMENT ===================
    IF MYCODE = '1241' OR MYCODE = '1242' OR (MYCODE > '1231' AND MYCODE <= '1235') THEN
*------------- MARGIN ------------
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> > R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> THEN
            TRANS.TYPE = "ACLG"
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            DB.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            CR.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>

            GOSUB PRODUCE.MSG
        END ELSE
            TRANS.TYPE = "ACLG"
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            DB.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            CR.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>

            GOSUB PRODUCE.MSG
        END

    END

    RETURN
*===========================================================
*CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST,CUST.BRN)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST,COMP.BOOK)
    CUST.BRN = COMP.BOOK[8,2]

PRODUCE.MSG:
    ORDER.BANK = ORDER.BNK
    PROFT.CUST = LOCAL.REF< 1,LDLR.THIRD.NUMBER>
    IF R.NEW( LD.LOCAL.REF)< 1,LDLR.VERSION.NAME> = ",SCB.LG.CANCELFINAL" THEN

***DB.DATE = TODAY
***CR.DATA = TODAY
        DB.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.INSTAL.DUE.DATE>
        CR.DATA=R.NEW(LD.LOCAL.REF)<1,LDLR.INSTAL.DUE.DATE>

    END ELSE
*IF MYCODE = '1241' OR MYCODE = '1242' THEN
* DB.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.MATUR.DATE>
*  CR.DATA = R.NEW(LD.LOCAL.REF)<1,LDLR.MATUR.DATE>
*END ELSE
        IF MYCODE = '1111' THEN
            DB.DATE = R.NEW(LD.VALUE.DATE)
            CR.DATA = R.NEW(LD.VALUE.DATE)
        END
* END
    END
*Line [ 264 ] Adding EB.SCBUpgradeFifteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeFifteen.VAR.LG.OFS( CHQ.TYPE,VER.NAME,TRANS.TYPE,DB.CUR,DB.AMT,PROFT.CUST,DB.DATE,CR.DATA,DB.ACCT,CR.ACCT,CHQ.NO,BENF.CUST,PROFIT.DEPT,ORDER.BANK)
    TEXT = "12"  ; CALL REM
    RETURN
*-------------------------
END
