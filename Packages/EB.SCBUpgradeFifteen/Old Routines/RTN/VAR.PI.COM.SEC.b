* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>498</Rating>
*-----------------------------------------------------------------------------
****NESSREEN AHMED-SCB 13/11/2005

    SUBROUTINE  VAR.PI.COM.SEC

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PERIODIC.INTEREST
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COM.CR.RATE.SEC

****ADDED ON 12/08/2008***********************************
***PRICE FOR EGP *****************************************
    M.SEL = "SELECT F.SCB.COM.CR.RATE.SEC WITH @ID LIKE 01EGP..."
    KEY.LIST.M=""
    SELECTED.M=""
    ER.MSG.M=""
    CALL EB.READLIST(M.SEL,KEY.LIST.M,"",SELECTED.M,ER.MSG.M)
    IF KEY.LIST.M THEN
        DIM AAE(SELECTED.M)
        FOR XG = 1 TO SELECTED.M
            DG1 = RIGHT(KEY.LIST.M<XG>,8)
            AAE(XG) = DG1
        NEXT XG
        TEMPEG = AAE(1)
        SORTEG1 = 1
        FOR XG = 2 TO SELECTED.M
            IF TEMPEG < AAE(XG)  THEN TEMPEG = AAE(XG); SORTEG1 =XG
        NEXT XG
        IDEG1 = KEY.LIST.M<SORTEG1>
       ** DATTEG = IDEG1[6,8]
        DATTEG = ID.NEW[6,8]
        TEXT = 'DATTEG=':DATTEG ; CALL REM
        GOSUB OPEN.FILE.EGP
    END ELSE
        TEXT = '�� ���� ����� ���� �������'
    END
****TO OPEN FILE*******************************************
OPEN.FILE.EGP:

    TEXT = 'EGP' ; CALL REM
    F.COMRAT = '' ; FN.COMRAT = 'F.SCB.COM.CR.RATE.SEC' ; R.COMRAT = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.COMRAT,F.COMRAT)

    FN.LD.TYPE = 'F.SCB.LD.TYPE.LEVEL' ; F.LD.TYPE = '' ; R.LD.TYPE = '' ; RETRY = '' ; E1 = ''
    CURR = 'EGP'
    CALL F.READU(FN.COMRAT,IDEG1, R.COMRAT, F.COMRAT ,E3, RETRY3)
    PCTEG = R.COMRAT<SCB.COM.RT.PERIOD>
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NS = DCOUNT(PCTEG,@VM)
    FOR YY = 1 TO NS
        CATEGEG = R.COMRAT<SCB.COM.RT.CATEGORY,YY>
        AMTFREG = R.COMRAT<SCB.COM.RT.AMT.FROM,YY>
        AMTTOEG = R.COMRAT<SCB.COM.RT.AMT.TO,YY>
*Line [ 79 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WX = DCOUNT(AMTFREG,@SM)
        FOR MME = 1 TO WX
            COM.AMTFREG = R.COMRAT<SCB.COM.RT.AMT.FROM,YY,MME>
            COM.AMTTOEG = R.COMRAT<SCB.COM.RT.AMT.TO,YY,MME>
            RATEEG = R.COMRAT<SCB.COM.RT.BID.RATE,YY,MME>
***
            KEY.TO.USE.EG = CATEGEG:DATTEG
            CALL OPF(FN.LD.TYPE,F.LD.TYPE)
            CALL F.READU(FN.LD.TYPE,KEY.TO.USE.EG, R.LD.TYPE, F.LD.TYPE ,E1, RETRY)
            IF NOT(E1) THEN
                LD.CURR.EG = R.LD.TYPE<LDTL.CURRENCY>
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WWEG = DCOUNT(LD.CURR.EG,@VM)
                LOCATE 'EGP' IN LD.CURR.EG<1,1> SETTING GG THEN
                    LD.AMTFREG = R.LD.TYPE<LDTL.AMT.FROM,GG>
                    LOCATE COM.AMTFREG IN LD.AMTFREG<1,1,1> SETTING SSG THEN
                        R.LD.TYPE<LDTL.RATE,GG,SSG> = RATEEG
                        CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE.EG,R.LD.TYPE)
                    END ELSE
*Line [ 99 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        DDE = DCOUNT(LD.AMTFREG,@SM)
                        R.LD.TYPE<LDTL.AMT.FROM,GG,DDE+1> = COM.AMTFREG
                        R.LD.TYPE<LDTL.AMT.TO,GG,DDE+1> = COM.AMTTOEG
                        R.LD.TYPE<LDTL.RATE,GG,DDE+1> = RATEEG
                        CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE.EG,R.LD.TYPE)
                    END
                END ELSE
                    R.LD.TYPE<LDTL.CURRENCY,WWEG+1> = 'EGP'
                    R.LD.TYPE<LDTL.AMT.FROM,WWEG+1,1> = COM.AMTFREG
                    R.LD.TYPE<LDTL.AMT.TO,WWEG+1,1> = COM.AMTTOEG
                    R.LD.TYPE<LDTL.RATE,WWEG+1,1> = RATEEG
                    CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE.EG,R.LD.TYPE)
                END
            END ELSE          ;*ELSE OF IF E1
                R.LD.TYPE<LDTL.CURRENCY,1> = 'EGP'
                R.LD.TYPE<LDTL.AMT.FROM,1,1> = COM.AMTFREG
                R.LD.TYPE<LDTL.AMT.TO,1,1> = COM.AMTTOEG
                R.LD.TYPE<LDTL.RATE,1,1> = RATEEG
                CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE.EG,R.LD.TYPE)
            END     ;* END OF IF E1
***
        NEXT MME
    NEXT YY

**********************************************************
***FORIEGN CURRENCY****************
    SER = '01'
    CUR1 = ID.NEW[3,3]
    ID = SER:CUR1
    T.SEL = "SELECT F.SCB.COM.CR.RATE.SEC WITH @ID LIKE ":ID:"..."
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        DIM AA(SELECTED)
        FOR X = 1 TO SELECTED
            D1 = RIGHT(KEY.LIST<X>,8)
            AA(X) = D1
        NEXT X
        TEMP = AA(1)
        SORT1 = 1
        FOR X = 2 TO SELECTED
            IF TEMP < AA(X)  THEN TEMP = AA(X); SORT1 =X
        NEXT X
        ID1 = KEY.LIST<SORT1>
       *** DATT = ID1[6,8]
        DATT = ID.NEW[6,8]
        GOSUB OPEN.FILE
    END ELSE
        TEXT = '�� ���� ����� ���� �������'
    END

OPEN.FILE:
**********************************************************
    F.COMRAT = '' ; FN.COMRAT = 'F.SCB.COM.CR.RATE.SEC' ; R.COMRAT = '' ; E3 = '' ; RETRY3 = ''
    CALL OPF(FN.COMRAT,F.COMRAT)

    FN.LD.TYPE = 'F.SCB.LD.TYPE.LEVEL' ; F.LD.TYPE = '' ; R.LD.TYPE = '' ; RETRY = '' ; E1 = ''
    CURR = ID.NEW[3,3]

    RP = R.NEW(PI.REST.PERIOD)
*Line [ 162 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NN = DCOUNT(RP,@VM)

    FOR I =1 TO NN
        SN = ''  ; BIDR = ''  ; SN2 = '' ; PCT = '' ; PCT2= '' ; CRAT = '' ; CRAT2 = ''
        REST.PER = R.NEW(PI.REST.PERIOD)<1,I>
        BIDR = R.NEW(PI.BID.RATE)<1,I>
**********************************************************
        CALL F.READU(FN.COMRAT,ID1, R.COMRAT, F.COMRAT ,E3, RETRY3)
        PCT = R.COMRAT<SCB.COM.RT.PERIOD>
        LOCATE REST.PER IN PCT<1,1> SETTING BB THEN
            CATEG = R.COMRAT<SCB.COM.RT.CATEGORY,BB>
            AMTFR = R.COMRAT<SCB.COM.RT.AMT.FROM,BB>
            AMTTO = R.COMRAT<SCB.COM.RT.AMT.TO,BB>
*Line [ 176 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            TT = DCOUNT(AMTFR,@SM)
            FOR MM = 1 TO TT
                COM.AMTFR = R.COMRAT<SCB.COM.RT.AMT.FROM,BB,MM>
                COM.AMTTO = R.COMRAT<SCB.COM.RT.AMT.TO,BB,MM>
                RATE = R.COMRAT<SCB.COM.RT.BID.RATE,BB,MM>
                SN = SSUB(BIDR, RATE)
******************************************************************
                KEY.TO.USE = CATEG:DATT
                CALL OPF(FN.LD.TYPE,F.LD.TYPE)
                CALL F.READU(FN.LD.TYPE,KEY.TO.USE, R.LD.TYPE, F.LD.TYPE ,E1, RETRY)
                IF NOT(E1) THEN
                    LD.CURR = R.LD.TYPE<LDTL.CURRENCY>
*Line [ 189 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    WW = DCOUNT(LD.CURR,@VM)
                    LOCATE CUR1 IN LD.CURR<1,1> SETTING CC THEN
                        LD.AMTFR = R.LD.TYPE<LDTL.AMT.FROM,CC>
**  TEXT = 'COM.AMTFR=':COM.AMTFR ; CALL REM
**  TEXT = 'LD.AMTFR=':LD.AMTFR ; CALL REM
                        LOCATE COM.AMTFR IN LD.AMTFR<1,1,1> SETTING SS THEN
                            R.LD.TYPE<LDTL.RATE,CC,SS> = SN
                            CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE,R.LD.TYPE)
**11/5/2006 CALL JOURNAL.UPDATE(KEY.TO.USE)
                        END ELSE
*Line [ 200 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                            DD = DCOUNT(LD.AMTFR,@SM)
                            R.LD.TYPE<LDTL.AMT.FROM,CC,DD+1> = COM.AMTFR
                            R.LD.TYPE<LDTL.AMT.TO,CC,DD+1> = COM.AMTTO
                            R.LD.TYPE<LDTL.RATE,CC,DD+1> = SN
                            CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE,R.LD.TYPE)
**11/5/2006  CALL JOURNAL.UPDATE(KEY.TO.USE)
                        END
                    END ELSE
                        R.LD.TYPE<LDTL.CURRENCY,WW+1> = CUR1
                        R.LD.TYPE<LDTL.AMT.FROM,WW+1,1> = COM.AMTFR
                        R.LD.TYPE<LDTL.AMT.TO,WW+1,1> = COM.AMTTO
                        R.LD.TYPE<LDTL.RATE,WW+1,1> = SN
                        CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE,R.LD.TYPE)
**11/5/2006 CALL JOURNAL.UPDATE(KEY.TO.USE)
                    END
                END ELSE      ;*ELSE OF IF E1
                    R.LD.TYPE<LDTL.CURRENCY,1> = CUR1
                    R.LD.TYPE<LDTL.AMT.FROM,1,1> = COM.AMTFR
                    R.LD.TYPE<LDTL.AMT.TO,1,1> = COM.AMTTO
                    R.LD.TYPE<LDTL.RATE,1,1> = SN
                    CALL F.WRITE(FN.LD.TYPE,KEY.TO.USE,R.LD.TYPE)
**11/5/2006 CALL JOURNAL.UPDATE(KEY.TO.USE)
                END ;* END OF IF E1
            NEXT MM
        END ELSE
            SN = '0'
        END
    NEXT I

    RETURN
END
