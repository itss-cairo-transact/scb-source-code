* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/08/20 ***
*******************************************
    SUBROUTINE VIR.CHK.GROUP.EXP
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
    COMP = ID.COMPANY

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.NEW(CG.CUSTOMER.ID),@VM)
    WS.CU.ALL = R.NEW(CG.CUSTOMER.ID)
    R.NEW(CG.OVERRIDE) = ''
    CALL REBUILD.SCREEN
    FOR ICU = 1 TO WS.COUNT
*       WS.CU.ID = R.NEW(CG.CUSTOMER.ID,ICU)
        WS.CU.ID = WS.CU.ALL<1,ICU>
        CALL F.READ(FN.CU,WS.CU.ID,R.CU,F.CU,ER.CU)
        IF NOT(ER.CU) THEN
            WS.LOCAL.REF    = R.CU<EB.CUS.LOCAL.REF>
            WS.CU.GROUP.NUM = WS.LOCAL.REF<1,CULR.GROUP.NUM>
            WS.CU.CREDIT    = WS.LOCAL.REF<1,CULR.CREDIT.CODE>
            WS.CU.RISK.R    = WS.LOCAL.REF<1,CULR.RISK.RATE>

            IF  WS.CU.RISK.R GT 1 THEN
                FINDSTR WS.CU.ID IN R.OLD(CG.CUSTOMER.ID) SETTING POS.CY THEN ELSE
                    TEXT = "Customer No. : ":WS.CU.ID
                    CALL STORE.OVERRIDE(CURR.NO)
                    TEXT = "Require an exception to include as Class-one"
                    CALL STORE.OVERRIDE(CURR.NO)
                END
            END
        END
    NEXT ICU
    RETURN
END
