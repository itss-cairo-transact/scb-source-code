* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>408</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.WH.MOD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.UNITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TXN.TYPE.CONDITION
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*-----------------------------------------------------
    CURR     = R.NEW(SCB.WH.TRANS.CURRENCY)
    NEW.AMT  = R.NEW(SCB.WH.TRANS.TOTAL.BALANCE)
    TXN.TYPE = R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE)

    TRANS.ID = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)
    CALL DBR ('SCB.WH.ITEMS':@FM:SCB.WH.IT.VALUE.BALANCE,TRANS.ID,OLD.AMT)
    AMT        = OLD.AMT - NEW.AMT
    BRANCH.NO  = R.NEW(SCB.WH.TRANS.BRANCH)
    IF LEN(BRANCH.NO) EQ 1 THEN
        BRANCH.NO = '0':BRANCH.NO
    END

*--- 2011/03/02 -----------
*CHECK INTERNAL ACCOUNT
*******INTERNAL.ACCT   = "EGP19030000100":BRANCH.NO
    REG.ID   = FIELD(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),'.',1)
    CATEG.NO = REG.ID[13,4]
    INTERNAL.ACCT = R.NEW(SCB.WH.TRANS.CURRENCY) :'1':CATEG.NO:'000100':BRANCH.NO
***********************
    CALL DBR('ACCOUNT':@FM:0,INTERNAL.ACCT,ACC.E)
    IF ACC.E EQ '' THEN
        E = "MISSING INTERNAL ACCOUNT"
        CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
    END ELSE
*--------------------------
        IF TXN.TYPE EQ "MOD" THEN
            IF OLD.AMT LE NEW.AMT THEN
                CR.ACCT  = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)[3,16]
****************DR.ACCT   = "EGP19030000100":BRANCH.NO

                REG.ID   = FIELD(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),'.',1)
                CATEG.NO = REG.ID[13,4]
                DR.ACCT  = R.NEW(SCB.WH.TRANS.CURRENCY) :'1':CATEG.NO:'000100':BRANCH.NO
            END ELSE
                DR.ACCT  = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)[3,16]
****************CR.ACCT  = "EGP19030000100":BRANCH.NO

                REG.ID   = FIELD(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),'.',1)
                CATEG.NO = REG.ID[13,4]
                CR.ACCT  = R.NEW(SCB.WH.TRANS.CURRENCY) :'1':CATEG.NO:'000100':BRANCH.NO
            END
            CATEG.CR  = CR.ACCT[11,4]
            CATEG.DR  = DR.ACCT[4,5]
            TXN.CODE  = '847'
        END

        ACC.OFFICER   = R.NEW(SCB.WH.TRANS.BRANCH)

        IF CURR NE 'EGP' THEN
            FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
            CALL OPF(FN.CUR,F.CUR)
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        END

**--------------------------

        IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)[1,3]='INA' THEN
*--------
*   CR
*--------
            Y.ACCT = CR.ACCT

            IF CURR NE 'EGP' THEN
                LCY.AMT = AMT * RATE
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMT
            END ELSE
                LCY.AMT = AMT
                FCY.AMT = ''
            END
            CATEG  = CATEG.CR
*--- EDIT 2011/04/12
            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
*-------------------
            GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
            Y.ACCT = DR.ACCT
            IF CURR NE 'EGP' THEN
                LCY.AMT = (AMT * RATE ) * -1
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMT * -1
            END ELSE
                LCY.AMT = AMT * -1
                FCY.AMT = ''
            END
            CATEG  = CATEG.DR
*--- EDIT 2011/04/12
            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
*-------------------
            GOSUB AC.STMT.ENTRY
        END

**                   -------------------------
**                              REV
**                   -------------------------

        IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='RNAU' THEN

*--------
*  CR
*--------
            Y.ACCT = DR.ACCT

            IF CURR NE 'EGP' THEN
                LCY.AMT = AMT * RATE
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMT
            END ELSE
                LCY.AMT = AMT
                FCY.AMT = ''
            END
            CATEG  = CATEG.CR
*--- EDIT 2011/04/12
            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
*-------------------
            GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
            Y.ACCT = CR.ACCT
            IF CURR NE 'EGP' THEN
                LCY.AMT = (AMT * RATE ) * -1
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMT * -1
            END ELSE
                LCY.AMT = AMT * -1
                FCY.AMT = ''
            END
            CATEG  = CATEG.DR
*--- EDIT 2011/04/12
            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
*-------------------
            GOSUB AC.STMT.ENTRY
        END

    END

    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
    ENTRY = ""
    MULTI.ENTRIES = ""

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TXN.CODE
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
    RETURN

END
