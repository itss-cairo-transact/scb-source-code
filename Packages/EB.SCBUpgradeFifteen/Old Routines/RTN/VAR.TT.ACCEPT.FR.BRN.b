* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VAR.TT.ACCEPT.FR.BRN
***TO GET THE FULL TRANSACTION NO
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ

    F.COUNT = '' ; FN.COUNT = 'FBNK.TELLER'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''

    XX = ''
    CALL JULDATE(TODAY,XX )
    VAR1 = R.NEW(TT.TE.NARRATIVE.2)<1,1>
    VAR1 =FMT(VAR1 , 'R%5')
    XX = XX[3,5]
    TRANS.NO = 'TT' : XX :VAR1


    CALL F.READU(FN.COUNT,TRANS.NO, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
        R.COUNT<TT.TE.NARRATIVE.1> = 'Y'
        CALL F.WRITE(FN.COUNT,TRANS.NO,R.COUNT)
        *CALL JOURNAL.UPDATE(TRANS.NO)
        CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
        CLOSE FN.COUNT
    END

    RETURN
END
