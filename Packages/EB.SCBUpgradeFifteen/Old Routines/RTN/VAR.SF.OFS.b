* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SF.OFS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.EXPIRY
*---------------------------------------------
    GOSUB INITIALISE
    GOSUB PROCESS
    RETURN
*---------------------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SF.EXP"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    SERIAL = 0
*DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
*NEW.FILE = "RENT.":TODAY:".":RND(10000)


* OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
*     CLOSESEQ V.FILE.IN
*     HUSH ON
*     EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
*     HUSH OFF
*     PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
* END

* OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
*     CREATE V.FILE.IN THEN
*         PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
*     END
*     ELSE
*         STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
*     END
* END
    RETURN
*----------------------------------------------------------
PROCESS:
*-------
    FN.EXP  = "F.SCB.SF.EXPIRY"    ; F.EXP  = ""
    CALL OPF(FN.EXP, F.EXP)

    FN.ACCT = "FBNK.ACCOUNT"       ; F.ACCT = ""
    CALL OPF(FN.ACCT,F.ACCT)

    SYS.DATE = TODAY
    CALL CDT('', SYS.DATE, '-1W')
*----------------------------------------------------------
    N.SEL  = "SELECT F.SCB.SF.EXPIRY WITH RENEW.DATE LE ":TODAY
    N.SEL := " AND RENEW.DATE GT ":SYS.DATE
    N.SEL := " AND SAFF.USED NE 'YES'"
    N.SEL := " BY @ID"
*   N.SEL  = "SELECT F.SCB.SF.EXPIRY WITH RENEW.DATE EQ '20210601' AND SAFF.USED NE 'YES'"
    CALL EB.READLIST(N.SEL,KEY.LIST,'',SELECTED,ERR)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            EXP.ID = KEY.LIST<II>
*    DD.ID  = FIELD(EXP.ID,'-',2)
            CALL F.READ(FN.EXP,EXP.ID, R.EXP, F.EXP, ETEXT)

            TRNS.TYPE = "AC"
            DR.ACC    = R.EXP<SF.EXP.ACCOUNT.NO>
            DD.ID     = R.EXP<SF.EXP.RENEW.DATE>
            CUR.CODE  = DR.ACC[9,2]
            CALL DBR ('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.CODE,DR.CUR)
**************UPDATED BY RIHAM YOUSSIF 20210501***********
*         AMT.D          =  R.EXP<SF.EXP.RENT.AMOUNT> + R.EXP<SF.EXP.COMP.AMOUNT>
            AMT.D          =  R.EXP<SF.EXP.COMP.AMOUNT>
**********************************************************
            DR.AMT         =  R.EXP<SF.EXP.RENT.AMOUNT> + R.EXP<SF.EXP.COMP.AMOUNT>
            DR.VAL.D       =  R.EXP<SF.EXP.RENEW.DATE>
            COMP           = R.EXP<SF.EXP.CO.CODE>
            COMP.NN        = R.EXP<SF.EXP.CO.CODE>
            COMP.CODE      = COMP[8,2]
*            OFS.USER.INFO  = "INPUTT" : COMP.CODE : "//" :COMP
            OFS.USER.INFO = "AUTO.CHRGE//":COMP

            FN.ACC = "FBNK.ACCOUNT"   ;  F.ACC = ""
            CALL OPF(FN.ACC,F.ACC)

            CALL F.READ(FN.ACC,DR.ACC, R.ACC, F.ACC, ETEXT.ACC)
            AMOUNT = R.ACC<AC.WORKING.BALANCE>

            IF AMT.D GT AMOUNT THEN
                CHECK.FLG = "NO"
            END ELSE
                CHECK.FLG = "YES"
            END

            CR.ACC    = "PL54015"
            CR.AMT    = R.EXP<SF.EXP.COMP.AMOUNT>
            CR.VAL.D  = R.EXP<SF.EXP.RENEW.DATE>
*  COM.CODE  = "DEBIT PLUS CHARGES"
*  COM.TYPE  = "CHQCOOLIN"
*  COM.AMT   = "EGP" : R.EXP<SF.EXP.RENT.AMOUNT>

            SERIAL = SERIAL + 1
            GOSUB BUILD.RECORD
*   GOSUB WRITE.EXP
***** SCB R15 UPG 20160703 - S
*            CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
        NEXT II
    END
    RETURN
*----------------------------------------------------
WRITE.EXP:
*---------
    FN.EXP = 'F.SCB.SF.EXPIRY'     ; F.EXP = ''
    CALL OPF( FN.EXP,F.EXP)
    CALL OPF(FN.EXP,FVAR.EXP)
    EXP.ID = KEY.LIST<II>
    CALL F.READ(FN.EXP,EXP.ID,R.TEMP,F.EXP,ERR.EXP)

    IF CHECK.FLG EQ 'YES' THEN
        DD =  R.TEMP<SF.EXP.RENEW.DATE>
        CALL ADD.MONTHS(DD,'12')

        R.TEMP<SF.EXP.RENEW.DATE> = DD
        R.TEMP<SF.EXP.SAFF.USED>  = ""
    END ELSE
        R.TEMP<SF.EXP.SAFF.USED>  = "NO"
    END

    CALL F.WRITE (FN.EXP,EXP.ID,R.TEMP)

    RETURN
*----------------------------------------------------
BUILD.RECORD:
*-------------
    COMMA     = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :DD.ID:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :COMP.NN:COMMA

    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :"EGP":COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :CR.AMT:COMMA

    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:16:1="      :",SF.EXP"


*    OFS.ID = "RENT.":TODAY:".":RND(10000)
    OFS.ID = "RENT.":TODAY:".":SERIAL

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

    RETURN
*---------------------------------------------------------------
END
