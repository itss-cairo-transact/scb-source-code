* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VIR.CD.PRIN.DR.ACC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    CD.CUS= R.NEW(LD.CUSTOMER.ID)
******************
    AC.DR = R.NEW(LD.DRAWDOWN.ACCOUNT)
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.DR,ACC.DR)

    IF CD.CUS NE ACC.DR  THEN
        ETEXT='DR CUST MUST BE SAME AS CD CUSTOMER';CALL STORE.END.ERROR
    END
****************
    AC.PRIN = R.NEW(LD.PRIN.LIQ.ACCT)
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.PRIN,ACC.PRIN)

    IF CD.CUS NE ACC.PRIN THEN
        ETEXT='PRIN LIQ CUST MUST BE SAME AS CD CUSTOMER';CALL STORE.END.ERROR
    END
*********
    AC.CHRG = R.NEW(LD.CHRG.LIQ.ACCT)
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.CHRG,ACC.CHRG)

    IF CD.CUS NE ACC.CHRG THEN
        ETEXT='CHRG CUST MUST BE SAME AS CD CUSTOMER';CALL STORE.END.ERROR
    END



    RETURN
END
