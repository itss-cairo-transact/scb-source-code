* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
****      BY MOHAMED SABRY 22/3/2010    ****
********************************************
    SUBROUTINE VAR.WRITE.USER.MOD
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.USER.MODIFICATION
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF


    F.USE = '' ; FN.USE = 'F.USER' ; R.USE = '' ; RETRY = ''
    CALL OPF(FN.USE,F.USE)

    F.UMOD = '' ; FN.UMOD = 'F.SCB.USER.MODIFICATION' ; R.UMOD = '' ; E.UMOD = '' ; RETRY2 = ''
    CALL OPF(FN.UMOD,F.UMOD)

    F.MUA.HIS = '' ; FN.MUA.HIS = 'F.SCB.MODIFY.USER.AUTHORITY$HIS' ; R.MUA.HIS = '' ; E.MUA.HIS = '' ; RETRY2.HIS = ''
    CALL OPF(FN.MUA.HIS,F.MUA.HIS)

**** By Khaled 2013/04/18 *******

    F.UMODA = '' ; FN.UMODA = 'F.SCB.MODIFY.USER.AUTHORITY' ; R.UMODA = '' ; E.UMODA = '' ; RETRY3 = ''
    CALL OPF(FN.UMODA,F.UMODA)
    IF PGM.VERSION EQ ',AUTO' THEN
        T.SEL2 = "SELECT ":FN.UMODA:" WITH USER.ID EQ ":ID.NEW:" BY DATE.TIME"
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
        IF SELECTED2 THEN
            CALL F.READ(FN.UMODA,KEY.LIST2<SELECTED2>,R.UMODA,F.UMODA,E3)
            WS.INPUTTER = R.UMODA<MUA.INPUTTER>
*** UPDATED BY Mohamed sabry 2013/05/30
            WS.OFS.USR = 'OFSADMIN'
            FINDSTR WS.OFS.USR IN WS.INPUTTER SETTING FPOS, VPOS THEN
                WS.ID.HIS = KEY.LIST2<SELECTED2>
                CALL F.READ.HISTORY(FN.MUA.HIS,WS.ID.HIS,R.MUA.HIS,F.MUA.HIS,E3.HIS)
                WS.INPUTTER   = R.MUA.HIS<MUA.INPUTTER>
                WS.AUTHORISER = R.MUA.HIS<MUA.AUTHORISER>
                WS.AUTHORISER = FIELD(WS.AUTHORISER,"_",2)
            END
*** End of update
        END
    END

*********************************

    WS.TIME           = TIME()
    WS.TIME           = OCONV(WS.TIME, "MTS")
    WS.DEPT.ATH       = R.USER<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
    IF APPLICATION EQ 'USER' THEN
        WS.UMOD.ID                   = APPLICATION:'*':ID.NEW:'*':TODAY:'*':WS.TIME
        R.UMOD<UMOD.SYS.ID>          = APPLICATION
        R.UMOD<UMOD.USER.ID>         = ID.NEW
        R.UMOD<UMOD.MOD.TIME>        = WS.TIME
        R.UMOD<UMOD.END.DATE>        = R.NEW(EB.USE.END.DATE.PROFILE)
        R.UMOD<UMOD.MOD.SYS.DATE>    = TODAY
        R.UMOD<UMOD.CURR.NO>         = R.NEW(EB.USE.CURR.NO)
        R.UMOD<UMOD.INPUTTER>        = R.NEW(EB.USE.INPUTTER)

        IF PGM.VERSION EQ ',AUTO' THEN
            R.UMOD<UMOD.INPUTTER>    = WS.INPUTTER
        END

        R.UMOD<UMOD.DATE.TIME>       = R.NEW(EB.USE.DATE.TIME)
        R.UMOD<UMOD.AUDITOR.CODE>    = WS.DEPT.ATH
*Line [ 99 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL DBR("USER.SIGN.ON.NAME":@FM:EB.USO.USER.ID,R.USER<EB.USE.SIGN.ON.NAME>,WS.USER.ID)


        R.UMOD<UMOD.AUTHORISER>      = WS.USER.ID
        IF WS.AUTHORISER NE '' THEN
            R.UMOD<UMOD.AUTHORISER> =  WS.AUTHORISER
        END
        R.UMOD<UMOD.AUDIT.DATE.TIME> = R.NEW(EB.USE.AUDIT.DATE.TIME)

        GOSUB PRINT.REPORT

        R.UMOD<UMOD.DEPT.CODE>       = WS.DEPT.USE
        R.UMOD<UMOD.CO.CODE>         = WS.COMPANY

        CALL F.WRITE(FN.UMOD,WS.UMOD.ID,R.UMOD)
    END

*** UPDATED BY MOHAMED SABRY 2012/05/16 *************************************************

    IF APPLICATION EQ 'USER.ABBREVIATION' THEN
        WS.UMOD.ID                   = APPLICATION:'*':ID.NEW:'*':TODAY:'*':WS.TIME
        R.UMOD<UMOD.SYS.ID>          = APPLICATION
        R.UMOD<UMOD.USER.ID>         = ID.NEW
        R.UMOD<UMOD.MOD.TIME>        = WS.TIME
        R.UMOD<UMOD.MOD.SYS.DATE>    = TODAY
        R.UMOD<UMOD.CURR.NO>         = R.NEW(EB.UAB.CURR.NO)
        R.UMOD<UMOD.INPUTTER>        = R.NEW(EB.UAB.INPUTTER)

        IF PGM.VERSION EQ ',AUTO' THEN
            R.UMOD<UMOD.INPUTTER>    = WS.INPUTTER
        END

        R.UMOD<UMOD.DATE.TIME>       = R.NEW(EB.UAB.DATE.TIME)
        R.UMOD<UMOD.AUDITOR.CODE>    = WS.DEPT.ATH
*Line [ 134 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL DBR("USER.SIGN.ON.NAME":@FM:EB.USO.USER.ID,R.USER<EB.USE.SIGN.ON.NAME>,WS.USER.ID)

        R.UMOD<UMOD.AUTHORISER>      = WS.USER.ID
        IF WS.AUTHORISER NE '' THEN
            R.UMOD<UMOD.AUTHORISER> =  WS.AUTHORISER
        END
        R.UMOD<UMOD.AUDIT.DATE.TIME> = R.NEW(EB.UAB.AUDIT.DATE.TIME)

        GOSUB PRINT.REPORT

        R.UMOD<UMOD.DEPT.CODE>       = WS.DEPT.USE
        R.UMOD<UMOD.CO.CODE>         = WS.COMPANY

        CALL F.WRITE(FN.UMOD,WS.UMOD.ID,R.UMOD)
    END

    IF APPLICATION EQ 'TELLER.ID' THEN
**** By Khaled 2013/04/18 *******
        WS.DAT = TODAY[3,6]:'...'
        IF PGM.VERSION EQ ',AUTO' THEN
            T.SEL2 = "SELECT ":FN.UMODA:" WITH TELLER.ID EQ ":ID.NEW:" AND DATE.TIME LIKE ":WS.DAT:" BY DATE.TIME"
            CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
            IF SELECTED2 THEN
                CALL F.READ(FN.UMODA,KEY.LIST2<SELECTED2>,R.UMODA,F.UMODA,E3)
                WS.INPUTTER = R.UMODA<MUA.INPUTTER>
*** UPDATED BY Mohamed sabry 2013/05/30
                WS.OFS.USR = 'OFSADMIN'
                FINDSTR WS.OFS.USR IN WS.INPUTTER SETTING FPOS, VPOS THEN
                    WS.ID.HIS = KEY.LIST2<SELECTED2>
                    CALL F.READ.HISTORY(FN.MUA.HIS,WS.ID.HIS,R.MUA.HIS,F.MUA.HIS,E3.HIS)
                    WS.INPUTTER   = R.MUA.HIS<MUA.INPUTTER>
                    WS.AUTHORISER = R.MUA.HIS<MUA.AUTHORISER>
                    WS.AUTHORISER = FIELD(WS.AUTHORISER,"_",2)
                END
***End of update
            END
        END
*********************************

        WS.UMOD.ID                   = APPLICATION:'*':R.NEW(TT.TID.USER):'*':TODAY:'*':WS.TIME:'*':ID.NEW
        R.UMOD<UMOD.SYS.ID>          = APPLICATION
        R.UMOD<UMOD.USER.ID>         = R.NEW(TT.TID.USER)
        R.UMOD<UMOD.MOD.TIME>        = WS.TIME
*       R.UMOD<UMOD.END.DATE>        = ID.NEW
        R.UMOD<UMOD.MOD.SYS.DATE>    = TODAY
        R.UMOD<UMOD.CURR.NO>         = R.NEW(TT.TID.CURR.NO)
        R.UMOD<UMOD.INPUTTER>        = R.NEW(TT.TID.INPUTTER)

        IF PGM.VERSION EQ ',AUTO' THEN
            R.UMOD<UMOD.INPUTTER>    = WS.INPUTTER
        END

        R.UMOD<UMOD.DATE.TIME>       = R.NEW(TT.TID.DATE.TIME)
        R.UMOD<UMOD.AUDITOR.CODE>    = WS.DEPT.ATH
*Line [ 189 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL DBR("USER.SIGN.ON.NAME":@FM:EB.USO.USER.ID,R.USER<EB.USE.SIGN.ON.NAME>,WS.USER.ID)

        R.UMOD<UMOD.AUTHORISER>      = WS.USER.ID
        IF WS.AUTHORISER NE '' THEN
            R.UMOD<UMOD.AUTHORISER> =  WS.AUTHORISER
        END
        R.UMOD<UMOD.AUDIT.DATE.TIME> = R.NEW(TT.TID.AUDIT.DATE.TIME)

        WS.INPUTTER = FIELD(R.UMODA<MUA.INPUTTER>,"_",2)
        GOSUB PRINT.REPORT

        R.UMOD<UMOD.DEPT.CODE>       = WS.DEPT.USE
        R.UMOD<UMOD.CO.CODE>         = WS.COMPANY

        CALL F.WRITE(FN.UMOD,WS.UMOD.ID,R.UMOD)
    END
*** END OF UPDATED BY MOHAMED SABRY 2012/05/16 ******************************************

    RETURN

*****************************************************************
PRINT.REPORT:

**** CHECK IF NEED TO PRINT REPORT  M.ELSAYED 2012/05/28 ********
*** UPDATED BY M.ELSAYED 2012/11/11  *************************************************
* CALL TXTINP('Print User Application ?', 8, 23, '1.1', FM:'N_Y')
*IF COMI[1,1] = 'Y' THEN

    CALL F.READ(FN.USE,R.UMOD<UMOD.USER.ID>, R.USE, F.USE, ETEXT)

    WS.SIGN.ON   = R.USE<EB.USE.SIGN.ON.NAME>
    WS.USER.NAME = R.USE<EB.USE.USER.NAME>
    WS.DEPT.USE  = R.USE<EB.USE.DEPARTMENT.CODE>

    WS.DEPT.USE2 = WS.DEPT.USE + 100
    WS.COMPANY = 'EG00100':WS.DEPT.USE2[2]


*** UPDATED BY MSABRY 2014/11/20
    IF WS.DEPT.ATH NE 2800 THEN
        RETURN
    END
*** END OF UPDATE
 
    REPORT.ID='VAR.WRITE.USER.MOD'
    CALL PRINTER.ON(REPORT.ID,'')

    YEAR = TODAY[1,4]
    MNTH = TODAY[5,2]
    DAY  = TODAY[7,2]
    DATE1 = YEAR:'/':MNTH:'/':DAY

    PR.HD  =REPORT.ID
    HEADING PR.HD
    PRINT
    PRINT "DATE :":DATE1
    PRINT
    PRINT "USER.ID :":R.UMOD<UMOD.USER.ID>
    PRINT
    PRINT
    PRINT "SIGN.ON :":WS.SIGN.ON
    PRINT
    PRINT
    PRINT "USER.NAME :":WS.USER.NAME
    PRINT
    PRINT "INPUTTER :":FIELD(R.UMOD<UMOD.INPUTTER><1,1>,"_",2)

    PRINT
*    PRINT "AUTHORISER :":WS.USER.ID
    PRINT "AUTHORISER :":R.UMOD<UMOD.AUTHORISER>
    PRINT
* PRINT R.NEW(EB.USE.DEPARTMENT.CODE)
    PRINT "TIME :":WS.TIME
    PRINT
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,WS.DEPT.USE,WS.DEPT.NAME)
    PRINT  "BRANCH :":FIELD(WS.DEPT.NAME,".",2)
    PRINT
    PRINT
    IF APPLICATION EQ 'TELLER.ID' THEN
        PRINT R.NEW(TT.TID.STATUS):' ':APPLICATION:' ':ID.NEW
    END ELSE
        PRINT APPLICATION
    END
    PRINT
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*** END OF UPDATED BY M.ELSAYED 2012/5/28 ******************************************
END
