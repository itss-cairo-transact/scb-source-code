* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.LC.BILL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.BILL.INDEX
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS


    ID = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.LC.LG.REF>
    F.COUNT = '' ; FN.COUNT = 'FBNK.LETTER.OF.CREDIT'
    F.LC.BILL = '' ; FN.LC.BILL = 'FBNK.SCB.LC.BILL.INDEX'

              CALL OPF(FN.COUNT,F.COUNT)
              CALL OPF(FN.LC.BILL,F.LC.BILL)
              R.COUNT = ""
              R.LC.BILL = ""
              R.COUNT<TF.LC.LOCAL.REF,LCLR.LC.LG.REF> = ID.NEW
              R.LC.BILL<SCB.BLDX.BILL.NUMBER> = R.NEW(TF.LC.LOCAL.REF)<1,LCLR.BL.NUMBER>



               CALL F.WRITE(FN.COUNT,ID, R.COUNT)
               CALL F.WRITE(FN.LC.BILL,ID.NEW,R.LC.BILL)
               CALL JOURNAL.UPDATE(ID)
               CALL JOURNAL.UPDATE(ID.NEW)

   CLOSE FN.COUNT
   CLOSE FN.LC.BILL

   RETURN
END
