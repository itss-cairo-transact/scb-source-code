* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
********************************************
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.DEPT.INPUT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE.INPUT

    F.SAMP  = '' ; FN.SAMP = 'F.SCB.DEPT.SAMPLE.INPUT' ; R.SAMP = '' ; E.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)
    F.SAMP1 = '' ; FN.SAMP1 = 'F.SCB.DEPT.SAMPLE1' ; R.SAMP1 = '' ; E.SAMP1 = ''
    CALL OPF(FN.SAMP1,F.SAMP1)

    WS.TIME                         = TIME()
    WS.TIME                         = OCONV(WS.TIME, "MTS")
    WS.UMOD.ID                      = APPLICATION:'*':ID.NEW:'*':TODAY:'*':WS.TIME
    R.SAMP<SAMP.INPUT>              = R.NEW(DEPT.SAMP.INPUT.NAME.HWALA)
*    R.SAMP<SAMP.INPUT>              = R.NEW(DEPT.SAMP.INPUT.NAME.WH)
    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,R.SAMP<SAMP.INPUT>,DEP1)
    R.SAMP<SAMP.AUTH>               = R.NEW(DEPT.SAMP.AUTH.NAME.HWALA)
*    R.SAMP<SAMP.AUTH>               = R.NEW(DEPT.SAMP.AUTH.NAME.WH)
    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,R.SAMP<SAMP.AUTH>,DEP2)
    R.SAMP<SAMP.FUNCTION>           = DEP1
    R.SAMP<SAMP.END.DATE>           = DEP2
    R.SAMP<SAMP.MOD.SYS.DATE>       = TODAY
    R.SAMP<SAMP.CO.CODE>            = R.NEW(DEPT.SAMP.CO.CODE)
    R.SAMP<SAMP.STAGE1.INP>         = R.NEW(DEPT.SAMP.FLG.INP1)
    R.SAMP<SAMP.STAGE1.AUTH>        = R.NEW(DEPT.SAMP.FLG.AUTH1)
    R.SAMP<SAMP.STAGE2.INP>         = R.NEW(DEPT.SAMP.FLG.INP2)
    R.SAMP<SAMP.STAGE2.AUTH>        = R.NEW(DEPT.SAMP.FLG.AUTH2)

    CALL F.WRITE(FN.SAMP,WS.UMOD.ID,R.SAMP)

* CALL JOURNAL.UPDATE(WS.UMOD.ID)

    RETURN
END
