* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
****ABEER
*-----------------------------------------------------------------------------
* <Rating>2510</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.TRNS.STMP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

******************
    MESSAGE  = ''
    ERR.MESS = ''
    AMT  = R.NEW(LD.AMOUNT)
***********************************
****************************************************
    FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
    CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
    CATG = R.ACC<AC.CATEGORY>
    IF CATG  NE '9090' AND CATG  NE '9091' THEN

****************************
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.REF.LD>  NE '1239' THEN
            IF V$FUNCTION EQ 'I' THEN
*------------------------  INP  ----------------------
                IF  R.NEW(LD.RECORD.STATUS)[1,3] EQ 'INA' THEN
                    MULTI.ENTRIES = ''
                    GOSUB AC.STMT.ENTRY
                    TYPE = 'DEL'
                    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
                    IF END.ERROR OR TEXT = "NO" THEN RETURN
                END
                MULTI.ENTRIES = ''
                GOSUB AC.STMT.ENTRY
                TYPE = 'VAL'
                CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
                IF END.ERROR OR TEXT = "NO" THEN RETURN
            END
*------------------------  DEL  ----------------------
            IF V$FUNCTION EQ 'D' THEN
                MULTI.ENTRIES = ''
                GOSUB AC.STMT.ENTRY
                TYPE = 'DEL'
                CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
                IF END.ERROR OR TEXT = "NO" THEN RETURN
            END
*------------------------  AUT ----------------------
            IF V$FUNCTION EQ 'A' THEN
                TYPE = 'AUT'
                CALL EB.ACCOUNTING("SYS",TYPE, "" , "" )
                IF END.ERROR OR TEXT = "NO" THEN RETURN
            END
*******************
        END
    END
    RETURN
*------------------------------
****************************
AC.STMT.ENTRY:
****************************************************************
    IF  R.NEW(LD.CUSTOMER.ID) NE '99499900'  THEN
************HYTHAM********20090318**********
        COMP = ID.COMPANY
        COM.CODE = COMP[8,2]
        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT,ACC.OFFICER)

**********************20081112*******************

        FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
        CALL OPF(FN.ACC,F.ACC)
        AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
        CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
        CATG = R.ACC<AC.CATEGORY>
        CUR = R.ACC<AC.CURRENCY>
        IF CUR EQ 'EGP' THEN
            DB.ACCT = R.NEW(LD.CHRG.LIQ.ACCT)
        END ELSE
            AC.ID =R.NEW(LD.LOCAL.REF)<1,LDLR.ACCT.STMP>
            DB.ACCT = AC.ID
        END
*****************
        PROFT.CUST =  R.NEW(LD.CUSTOMER.ID)

        CR.ACCT.STMP = 'EGP11023000100':ID.COMPANY[8,2]
        OP.CODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*****
        IF OP.CODE NE '' THEN
            OP.COD = OP.CODE
        END ELSE
            OP.COD =ID.NEW
        END
*****
        BENF.CUST = 'STAMP'
*--------
*   CR
*--------
        Y.ACCT.STMP = CR.ACCT.STMP
        LCY.AMT.CR.ST = '2.90'

*                 ----    1 CR  STMP ----

        ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.STMP
        ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
        ENTRY<AC.STE.TRANSACTION.CODE> = '47'
        ENTRY<AC.STE.THEIR.REFERENCE>  = OP.COD
        ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
        ENTRY<AC.STE.NARRATIVE>        = ""
        ENTRY<AC.STE.PL.CATEGORY>      = ""
        ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.CR.ST
        ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
        ENTRY<AC.STE.VALUE.DATE>       = TODAY
        ENTRY<AC.STE.CURRENCY>         = 'EGP'
        ENTRY<AC.STE.AMOUNT.FCY>       = ''
        ENTRY<AC.STE.EXCHANGE.RATE>    = ""
        ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
        ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
        ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
        ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
        ENTRY<AC.STE.BOOKING.DATE>     = TODAY
        ENTRY<AC.STE.CRF.TYPE>         = ""
        ENTRY<AC.STE.CRF.TXN.CODE>     = ""
        ENTRY<AC.STE.CRF.MAT.DATE>     = ""
        ENTRY<AC.STE.CHQ.TYPE>         = ""
        ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
        ENTRY<AC.STE.CUSTOMER.ID>      = ""
        ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
        ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
        ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

        MULTI.ENTRIES<-1> = LOWER(ENTRY)
        ENTRY = ""
*-------
*  DR
*-------
        Y.ACCT.DRSTMP = DB.ACCT
        LCY.AMT.DR.ST = '-2.90'

*                 ----    2   DR STMP----

*
        ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.DRSTMP
        ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
        ENTRY<AC.STE.TRANSACTION.CODE> = '48'
        ENTRY<AC.STE.THEIR.REFERENCE>  = OP.COD
        ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
        ENTRY<AC.STE.NARRATIVE>        = ""
        ENTRY<AC.STE.PL.CATEGORY>      = ""
        ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.DR.ST
        ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
        ENTRY<AC.STE.VALUE.DATE>       = TODAY
        ENTRY<AC.STE.CURRENCY>         = 'EGP'
        ENTRY<AC.STE.AMOUNT.FCY>       = ''
        ENTRY<AC.STE.EXCHANGE.RATE>    = ""
        ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
        ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
        ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
        ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
        ENTRY<AC.STE.BOOKING.DATE>     = TODAY
        ENTRY<AC.STE.CRF.TYPE>         = ""
        ENTRY<AC.STE.CRF.TXN.CODE>     = ""
        ENTRY<AC.STE.CRF.MAT.DATE>     = ""
        ENTRY<AC.STE.CHQ.TYPE>         = ""
        ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
        ENTRY<AC.STE.CUSTOMER.ID>      = ""
        ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
        ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
        ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)
        MULTI.ENTRIES<-1> = LOWER(ENTRY)
        ENTRY=""

    END
****************************************UPDATE***************
    RETURN
END
