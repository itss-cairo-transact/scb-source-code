* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>734</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.AC.HOLD.AMT
*A ROUTINE 1-TO CHECK THAT INPUTTERS FROM SAME DEPARTMENTS ONLY ARE ALLOWED TO CHANGE THE RECORD (VALIDATION MODE ONLY)
*          2-TO CHECK THAT HOLD.AMOUNT MUST BE ENTERED ALSO THAT ITS SUM IS SMALLER/EQUAL WORKING.BALANCE.

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    CALL DBR('ACCOUNT':@FM:AC.LOCAL.REF,ID.NEW,MYLOCAL)
    HLD.USER=MYLOCAL<1,ACLR.HOLD.USER>
    HLD.DETAILS=MYLOCAL<1,ACLR.HOLD.DETAILS>

*Line [ 40 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    HLD.NO=DCOUNT(HLD.USER,@SM)
*Line [ 42 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    HLD.NO.NEW=DCOUNT(R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER>,@SM)
*    IF R.NEW(AC.RECORD.STATUS) EQ 'INAU' THEN
    FOR I=1 TO HLD.NO
        IF HLD.NO EQ HLD.NO.NEW THEN
            IF R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.DETAILS,I>  NE  MYLOCAL<1,ACLR.HOLD.DETAILS,I> THEN LL=1
        END
    NEXT I
*   END
    IF HLD.NO GT HLD.NO.NEW THEN LL=1
************
    IF LL = 1 THEN
        TEXT = '���� ��� �� ������ ���� ��� �����';CALL REM ;CALL STORE.END.ERROR
*        CALL ERR ; MESSAGE = 'REPEAT'
    END
*************************************************************************
*Line [ 58 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.DATE>,@SM)
        IF R.NEW( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> NE  R.OLD( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> THEN
            IF R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> = '' THEN  YY = 1
            IF R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> = '0.00' THEN  ZZ = 1
            IF R.NEW( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> LT R.OLD( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> THEN
                IF R.NEW( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> NE '0.00' THEN KK = 1
            END
        END
    NEXT I

    IF YY = 1 THEN
        ETEXT = '���� �� ����� ����'
       * CALL STORE.END.ERROR
    END
* ETEXT = 'You.Should.Enter.Amount'
    IF ZZ = 1 THEN TEXT = '��� �� ����� ������' ; CALL STORE.OVERRIDE(R.NEW(AC.CURR.NO)+1)
    IF KK = 1 THEN TEXT = '��� �� ����� ���� ������';CALL STORE.OVERRIDE(R.NEW(AC.CURR.NO)+1)
*    CALL REBUILD.SCREEN
*****************************************************************************
*    CALL DBR('ACCOUNT':@FM:AC.LOCAL.REF,ID.NEW,MYLOCAL)
*    HLD.USER=MYLOCAL<1,ACLR.HOLD.USER>
*   HLD.DETAILS=MYLOCAL<1,ACLR.HOLD.DETAILS>

*   HLD.NO=DCOUNT(HLD.USER,SM)
*   HLD.NO.NEW=DCOUNT(R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER>,SM)
*    IF R.NEW(AC.RECORD.STATUS) EQ 'INAU' THEN
*   FOR I=1 TO HLD.NO
*       IF HLD.NO EQ HLD.NO.NEW THEN
*           IF R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.DETAILS,I>  NE  MYLOCAL<1,ACLR.HOLD.DETAILS,I> THEN LL=1

*      END
*  NEXT I
*   END
*   IF HLD.NO GT HLD.NO.NEW THEN LL=1
************
*   IF LL = 1 THEN
*       ETEXT = '12���� ��� �� ������ ���� ��� �����'
*       CALL ERR ; MESSAGE = 'REPEAT'
*   END
************
*****************************************************************************
    RETURN
END
