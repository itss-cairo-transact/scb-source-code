* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*----------------------------------------------------------------------*
* <Rating>209</Rating>
*----------------------------------------------------------------------*
    SUBROUTINE VAR.OFS.SF.ACCT

*   TO CREATE CONTEINGENT ACCOUNT FOR CUSTOMER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*---------------------------------------------------------------------*
    GOSUB INITIALISE

    REC.ID = ID.NEW
    CUS.ID = FIELD(REC.ID,'.',1)

    FN.CUS = "FBNK.CUSTOMER"  ;  F.CUS = ""
    CALL OPF(FN.CUS,F.CUS)
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER1)
    SEC  = R.CUS<EB.CUS.SECTOR>

    IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
        GOSUB BUILD.RECORD
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
    END
    RETURN
*----------------------------------------------------------------------*
INITIALISE:

    FN.OFS.SOURCE     = "F.OFS.SOURCE"
    F.OFS.SOURCE      = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK         = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN          = 0
    F.OFS.BK          = 0
    OFS.REC           = ""
    OFS.OPERATION     = "ACCOUNT"
    OFS.OPTIONS       = "BR"
    COMP              = C$ID.COMPANY
    COM.CODE          = COMP[8,2]
    OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.TRANS.ID      = ""
    OFS.MESSAGE.DATA  = ""
*----------------------------------------------------------------------*

    FN.ACC ='FBNK.ACCOUNT' ;R.ACC = '' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '' ; F.CUS.AC = ''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    RETURN
*----------------------------------------------------------------------*
BUILD.RECORD:
    COMMA       = ","
    DAT         = TODAY
    AC.SERL     = 1
    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(SCB.BS.CURRENCY),CURR)

**-----------------------------3050------------------------------------*
    CURS = 'EGP'
    REC.ID = ID.NEW
    CUS.ID = FIELD(REC.ID,'.',1)

    IF LEN(CUS.ID) EQ 7 THEN
        CUSS = "0":CUS.ID
    END ELSE
        CUSS = CUS.ID
    END

    CURR = "EGP"
    CUST.ACCT = CUSS:"10":"3050":"01"
    CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
    IF AC.COMP  = COMP  THEN
        CUST.ACCT.EXIST = ''
    END ELSE
        NEW.CUST.ACCT = CUST.ACCT
*-
        FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
        CALL OPF(FN.CUS.AC,F.CUS.AC)
        CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
        LOOP
            REMOVE ACC FROM R.CUS.AC  SETTING POS1
        WHILE ACC:POS1
            CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
            CATEG     = R.ACC<AC.CATEGORY>
            CURRR     = R.ACC<AC.CURRENCY>
            IF ( CURRR EQ CURS AND CATEG EQ 3050 ) THEN
                AC.SERL ++
                IF R.ACC<AC.CO.CODE> EQ COMP THEN
                    CUST.ACCT.EXIST   = ''
                    RETURN
                END
            END
        REPEAT
*-
        CUST.ACCT.EXIST   = 'XXX'
    END

    IF CUST.ACCT.EXIST THEN
        NEW.AC.SELR   = AC.SERL
        NEW.CUST.ACCT = CUSS:"10":"30500":NEW.AC.SELR
        CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
        OFS.TRANS.ID  = NEW.CUST.ACCT

        DAT = TODAY
        OFS.MESSAGE.DATA  = "CUSTOMER=":CUSS:COMMA
        OFS.MESSAGE.DATA := "CATEGORY=":"3050":COMMA
        OFS.MESSAGE.DATA := "CURRENCY=":"EGP"

        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA

        OFS.ID = OFS.TRANS.ID:"-":DAT
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    END

    RETURN
**-----------------------------3050------------------------------------*
END
