* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE

    SUBROUTINE VERSION.LIV.DIF
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDARD.SELECTION
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION

    GOSUB INIT
    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEADER
    GOSUB LOAD
    GOSUB PROC
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN

INIT:

    LIV.REC = '' ; LIV.DATE.TIME = ''
    HIS.REC = '' ; HIS.DATE.TIME = ''
    SS.REC = '' ; SS.ERR = ''
    FIELD.NAME = '' ; FIELD.COUNT = 0 ; VALID.FIELD = ''
    CURR.COUNT = 0 ; FIELD.NO = '' ; VAL.PROG = '' ; SINGLE.MULT = '' ; FIELD.NAME = ''
    SUB.FIELDS = '17':@FM:'18':@FM:'19':@FM:'20':@FM:'31':@FM:'32':@FM:'38':@FM:'42':@FM:'55'
    FN.SS = 'F.STANDARD.SELECTION' ; F.SS = ''
    CALL OPF(FN.SS,F.SS)
    FN.VER = 'F.VERSION' ; F.VER = '' ; VER.REC = '' ; VER.ERR = '' ; T.VER.SEL = ''
    KEY.VER.LIST = '' ; VER.SELECTED = '' ; VER.REC = '' ; VER.ERR.2 = ''
    CALL OPF(FN.VER,F.VER)
    FN.VER.HIS = 'F.VERSION$HIS' ; F.VER.HIS = '' ; VER.HIS.REC = '' ; VER.HIS.ERR = ''
    HIS.RECS.NO = ''
    CALL OPF(FN.VER.HIS,F.VER.HIS)
*Line [ 53 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    C.@FM = ''

    NOINPUT.ARR = 'TABLE.COLUMN':@VM:'TABLE.LINE':@VM:'MULTI.POSSIBLE':@VM:'VAL.ASSOC':@VM:'SUB.ASSOC'
    NOINPUT.ARR := @VM:'EXPOSE':@VM:'SERVICE':@VM:'ACTIVITY':@VM:'FUNCT':@VM:'EXPOSE.DESC':@VM:'OVERRIDE'
    NOINPUT.ARR := @VM:'RECORD.STATUS':@VM:'CURR.NO':@VM:'INPUTTER':@VM:'DATE.TIME':@VM:'AUTHORISER'
    NOINPUT.ARR := @VM:'CO.CODE':@VM:'DEPT.CODE':@VM:'AUDITOR.CODE':@VM:'AUDIT.DATE.TIME'

    TD = ''
    DIFF.ARR = '' ; RES.REC = ''  ; LIV.RES = '' ; HIS.RES = ''
    PRINTED.ID = ''
    VALUE.POS = ''
    PRINT.TEXT = ''
    REPORT.ID = 'P.FUNCTION'

    RETURN

LOAD:

    CALL F.READ(FN.SS,'VERSION',SS.REC,F.SS,SS.ERR)
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FIELD.COUNT = DCOUNT(SS.REC<SSL.SYS.FIELD.NAME>,@VM)
    FOR I=1 TO FIELD.COUNT
        FIELD.NO = SS.REC<SSL.SYS.FIELD.NO,I,1>
        VAL.PROG = SS.REC<SSL.SYS.VAL.PROG,I>
        SINGLE.MULT = SS.REC<SSL.SYS.SINGLE.MULT,I>
        FIELD.NAME = SS.REC<SSL.SYS.FIELD.NAME,I>

        IF FIELD.NO MATCH '0N' AND FIELD.NO NE '' AND FIELD.NO GT 0 THEN
*Line [ 82 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            FIND FIELD.NAME IN NOINPUT.ARR SETTING C.@FM ELSE
                CURR.COUNT = CURR.COUNT + 1
                VALID.FIELD<CURR.COUNT,1> = FIELD.NO
*Line [ 86 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                FIND FIELD.NO IN SUB.FIELDS SETTING C.@FM  THEN
*Line [ 88 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                    VALID.FIELD<CURR.COUNT,2> = '@SM'
                END
                ELSE
                    VALID.FIELD<CURR.COUNT,2> = SINGLE.MULT
                END
                VALID.FIELD<CURR.COUNT,3> = FIELD.NAME
            END
        END
    NEXT

    RETURN
PROC:

    TD = TODAY 
    TD = TD[3,6]

    T.VER.SEL = 'SELECT ':FN.VER:' WITH DATE.TIME LIKE ':TD:'... AND CURR.NO GT 1'
    CALL EB.READLIST(T.VER.SEL,KEY.VER.LIST, "",VER.SELECTED,VER.ERR)
    FOR K=1 TO VER.SELECTED
        CALL F.READ(FN.VER,KEY.VER.LIST<K>,VER.REC,F.VER,VER.ERR.2)
        HIS.RECS.NO = VER.REC<EB.VER.CURR.NO> - 1
        LIV.REC = VER.REC
        LIV.DATE.TIME = LIV.REC<EB.VER.DATE.TIME,1>
        CALL F.READ(FN.VER.HIS,KEY.VER.LIST<K>:';':HIS.RECS.NO,VER.HIS.REC,F.VER,VER.HIS.ERR)
        HIS.REC = VER.HIS.REC
        HIS.DATE.TIME = VER.HIS.REC<EB.VER.DATE.TIME,1>
        IF LIV.REC AND HIS.REC THEN
            LOOP WHILE HIS.RECS.NO GT 0 AND LIV.DATE.TIME[1,6] EQ TD DO
                GOSUB VER.DEF
                LIV.REC = HIS.REC
                LIV.DATE.TIME = HIS.DATE.TIME
                HIS.RECS.NO = HIS.RECS.NO - 1
                CALL F.READ(FN.VER.HIS,KEY.VER.LIST<K>:';':HIS.RECS.NO,VER.HIS.REC,F.VER,VER.HIS.ERR)
                HIS.REC = VER.HIS.REC
                HIS.DATE.TIME = VER.HIS.REC<EB.VER.DATE.TIME,1>
            REPEAT
        END
    NEXT
    RETURN


VER.DEF:

    LIV.FIELD = '' ; HIS.FIELD = '' ; FIELD.NO = ''
    POINT.FILED = '' ; OTHR.FIELD = '' ; FIELD.TYPE = ''
    MAX.ARR = ''

*Line [ 136 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    FOR I=1 TO DCOUNT(VALID.FIELD,@FM)
        FIELD.NO = VALID.FIELD<I,1>
        LIV.FIELD = LIV.REC<FIELD.NO>
        HIS.FIELD = HIS.REC<FIELD.NO>
        IF VALID.FIELD<I,2> EQ 'S' THEN
            IF LIV.FIELD NE HIS.FIELD THEN
                LIV.RES = LIV.FIELD
                HIS.RES = HIS.FIELD
                VALUE.POS = FIELD.NO
                GOSUB RES.ADD
            END
        END
        ELSE IF VALID.FIELD<I,2> EQ 'M' THEN

*Line [ 151 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            IF DCOUNT(LIV.FIELD,@VM) GE DCOUNT(HIS.FIELD,@VM) THEN
                POINT.FIELD = LIV.FIELD
                OTHR.FIELD = HIS.FIELD
                FIELD.TYPE = 'LIV'
            END
            ELSE
                POINT.FIELD = HIS.FIELD
                OTHR.FIELD = LIV.FIELD
                FIELD.TYPE = 'HIS'
            END
*Line [ 162 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FOR J = 1 TO DCOUNT(POINT.FIELD,@VM)
                IF POINT.FIELD<1,J> NE OTHR.FIELD<1,J> THEN
                    VALUE.POS = FIELD.NO:'.':J
                    IF FIELD.TYPE EQ 'LIV' THEN
                        LIV.RES = POINT.FIELD<1,J>
                        HIS.RES = OTHR.FIELD<1,J>
                        GOSUB RES.ADD

                    END
                    ELSE
                        LIV.RES = OTHR.FIELD<1,J>
                        HIS.RES = POINT.FIELD<1,J>
                        GOSUB RES.ADD
                    END
                END

            NEXT
        END
*Line [ 181 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        ELSE IF VALID.FIELD<I,2> EQ '@SM' THEN
*Line [ 183 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            MAX.ARR<1> = DCOUNT(LIV.FIELD,@VM)
*Line [ 185 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            MAX.ARR<2> = DCOUNT(HIS.FIELD,@VM)
            FOR J = 1 TO MAXIMUM(MAX.ARR)
*Line [ 188 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                IF DCOUNT(LIV.FIELD<1,J>,@SM) GE DCOUNT(HIS.FIELD<1,J>,@SM) THEN
                    POINT.FIELD = LIV.FIELD
                    OTHR.FIELD = HIS.FIELD
                    FIELD.TYPE = 'LIV'
                END
                ELSE
                    POINT.FIELD = HIS.FIELD
                    OTHR.FIELD = LIV.FIELD
                    FIELD.TYPE = 'HIS'
                END
*Line [ 199 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                FOR Z = 1 TO DCOUNT(POINT.FIELD<1,J>,@SM)
                    IF POINT.FIELD<1,J,Z> NE OTHR.FIELD<1,J,Z> THEN
                        VALUE.POS = FIELD.NO:'.':J:'.':Z
                        IF FIELD.TYPE EQ 'LIV' THEN
                            LIV.RES = POINT.FIELD<1,J,Z>
                            HIS.RES = OTHR.FIELD<1,J,Z>
                            GOSUB RES.ADD
                        END
                        ELSE
                            LIV.RES = OTHR.FIELD<1,J,Z>
                            HIS.RES = POINT.FIELD<1,J,Z>
                            GOSUB RES.ADD
                        END
                    END
                NEXT
            NEXT
        END
    NEXT
    RETURN

RES.ADD:

    ID = ''
    CONV.DATE = LIV.DATE.TIME
    IF PRINTED.ID NE KEY.VER.LIST<K> THEN
        PRINTED.ID = KEY.VER.LIST<K>
        ID = KEY.VER.LIST<K>
    END
    ELSE
        ID = ''
    END

    RES.REC =  ID:',':CONV.DATE:',':VALUE.POS:'  ':VALID.FIELD<I,3>:','
    RES.REC := LIV.RES:',':HIS.RES:','
    RES.REC := LIV.REC<EB.VER.INPUTTER>:',':LIV.REC<EB.VER.AUTHORISER>

    XX = SPACE(132)
    XX<1,1>[1,54] = ID
    IF ID NE '' THEN
        PRINT.TEXT = XX<1,1>
        GOSUB PRINT.FILE
        XX = SPACE(132)
        XX<1,1>[1,50] = '============================================'
        PRINT.TEXT = XX<1,1>
        GOSUB PRINT.FILE
    END

    XX = SPACE(132)
    XX<1,1>[3,50] = VALUE.POS:' ':VALID.FIELD<I,3>
    XX<1,1>[55,50] = LIV.RES
    XX<1,1>[107,16] = FIELD(LIV.REC<EB.VER.INPUTTER>,'_',2)
    XX<1,1>[125,6] = CONV.DATE[7,2]:':':CONV.DATE[9,2]
    PRINT.TEXT = XX<1,1>
    GOSUB PRINT.FILE

    XX = SPACE(132)
    XX<1,1>[55,50] = HIS.RES
    XX<1,1>[107,16] = FIELD(LIV.REC<EB.VER.AUTHORISER>,'_',2)

    PRINT.TEXT = XX<1,1>
    GOSUB PRINT.FILE

    XX = SPACE(132)
    XX<1,1>[2,130] = STR('-',132)
    PRINT.TEXT = XX<1,1>
    GOSUB PRINT.FILE

    DIFF.ARR<-1> = RES.REC
    RES.REC = ''
    CONV.DATE = ''
    RETURN

PRINT.HEADER:

    HD.DATE = TODAY[1,4]:'/':TODAY[5,2]:'/':TODAY[7,2]
    PRINT.TEXT =  SPACE(105):'Date : ':OCONV(DATE(),'D')
    PRINT.TEXT := "'L'":SPACE(105):'Time : ':OCONV(TIME(),'MT') 
    PRINT.TEXT := "'L'":SPACE(40):'Daily updates on T24 screens for ':HD.DATE
    PRINT.TEXT := "'L'":SPACE(40):'*******************************************'
    PRINT.TEXT := "'L'":'Screen name'
    PRINT.TEXT := "'L'"
    PRINT.TEXT := "'L'":SPACE(3):'Field name':SPACE(42):'New value'
    PRINT.TEXT := SPACE(43):'Inputter'
    PRINT.TEXT := "'L'":SPACE(55):'Old value'
    PRINT.TEXT := SPACE(43):'Authoriser'
    PRINT.TEXT := SPACE(8):'Time'
    PRINT.TEXT := "'L'":
    PRINT.TEXT := "'L'"::STR('_',132)
    HEADING PRINT.TEXT

*    PRINT

    RETURN

PRINT.FILE:

    PRINT PRINT.TEXT
    PRINT.TEXT = ''
    RETURN

END
