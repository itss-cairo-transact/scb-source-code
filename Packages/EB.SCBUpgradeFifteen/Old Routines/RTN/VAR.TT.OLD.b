* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.OLD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE

    ACCT.NO =R.NEW(TT.TE.ACCOUNT.2)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO,MYCURR)
    CHQ='SCB':'.':ACCT.NO:'...'

    T.SEL ="SELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN

        IF SELECTED EQ '0' THEN
            NO.ISSUED='1'
            CHQ.START='1'
        END ELSE
            CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST<SELECTED>,CHECK.ST.NO)
            CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.NUMBER.ISSUED,KEY.LIST<SELECTED>,NO.ISSUED)

            NEW.CHQ.STRT.NO= CHECK.ST.NO+NO.ISSUED
            NO.ISSUED='1'
            CHQ.START= NEW.CHQ.STRT.NO
        END
    END

    IF R.NEW(TT.TE.CHEQUE.NUMBER) NE '' THEN
        R.NEW(TT.TE.CHEQUE.NUMBER)= CHQ.START
        CALL REBUILD.SCREEN
    END
    IF MESSAGE EQ 'VAL' THEN
*Line [ 61 ] Adding EB.SCBUpgradeFifteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeFifteen.VAR.TT.OFS(ACCT.NO,NO.ISSUED,CHQ.START)
    END
    RETURN
END
