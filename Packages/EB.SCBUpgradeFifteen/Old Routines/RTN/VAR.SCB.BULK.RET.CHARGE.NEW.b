* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>420</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SCB.BULK.RET.CHARGE.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*Line [ 44 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.ACC = DCOUNT (R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
    FOR  NUMBER.ACC = 2 TO DCOUNT.ACC
*********

        IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC> NE '' THEN
            IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>[1,6] NE '994999' THEN
                FN.OFS.SOURCE ="F.OFS.SOURCE"
                F.OFS.SOURCE  = ""
                CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*                CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
                CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
                FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
                FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
                F.OFS.IN         = 0
                F.OFS.BK         = 0
                OFS.REC          = ""
                OFS.OPERATION    = "FUNDS.TRANSFER"
                OFS.OPTIONS      = "BR"
***OFS.USER.INFO    = "/"

*************HYTHAM********20090318**********
                COMP = C$ID.COMPANY
                COM.CODE = COMP[8,2]
                OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

                OFS.TRANS.ID     = ""
                OFS.MESSAGE.DATA = ""

**

                COMMA = ","
                DAT = TODAY
                OFS.MESSAGE.DATA = "DEBIT.CURRENCY=EGP"::COMMA
                OFS.MESSAGE.DATA := "CREDIT.CURRENCY=EGP"::COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DAT:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":DAT:COMMA
                OFS.MESSAGE.DATA := "TRANSACTION.TYPE=AC"::COMMA
***
                IF R.NEW(INF.MLT.AMOUNT.LCY)<1,NUMBER.ACC> LE 300 THEN
                    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=1"::COMMA
                END
                IF R.NEW(INF.MLT.AMOUNT.LCY)<1,NUMBER.ACC> GT 300 THEN
                    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=2"::COMMA
                END
***
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>:COMMA
                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=PL52014"::COMMA
                OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":R.USER<EB.USE.DEPARTMENT.CODE>:COMMA
                OFS.MESSAGE.DATA := "ORDERING.BANK=SCB"::COMMA
                OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=RET-CHARGE":COMMA
                OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=RET-CHARGE":COMMA
                OFS.MESSAGE.DATA := "COMMISSION.CODE=WAIVE"::COMMA
                OFS.MESSAGE.DATA := "CHARGE.CODE=WAIVE"::COMMA
**
                DAT = TODAY
                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OFS.ID = "T":TNO:".":R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>:"-":DAT
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*                SCB.OFS.SOURCE = "SCBOFFLINE"
*                SCB.OFS.ID = '' ; SCB.OPT = ''
*                CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*                END
*                CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
*********
            END
        END

    NEXT NUMBER.ACC

    RETURN
END
