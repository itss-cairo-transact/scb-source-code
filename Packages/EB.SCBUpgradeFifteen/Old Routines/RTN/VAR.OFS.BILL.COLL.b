* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.OFS.BILL.COLL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.STATUS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    DCOUNTBR.ID = ""
    NUMBERBR.ID = ""

*Line [ 52 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)
    FOR NUMBERBR.ID = 1 TO DCOUNTBR.ID
        IF R.NEW(SCB.BT.RETURN.REASON)<1,NUMBERBR.ID> EQ '' THEN
            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.AMOUNT,R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>,AMT)
            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.CURRENCY,R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>,CURR)
            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>,LOC.REF)
            DR.ACCT = LOC.REF<1,BRLR.CUST.ACCT>
            CR.ACCT = LOC.REF<1,BRLR.LIQ.ACCT>
            GOSUB COLLET.BILL

        END
    NEXT NUMBERBR.ID
    RETURN

**********************************************************
COLLET.BILL:
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
***OFS.USER.INFO    = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
***    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    COMMA = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC24":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":TODAY:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":TODAY:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>:COMMA
    OFS.MESSAGE.DATA :=  "PROFIT.CENTRE.DEPT=":R.USER<EB.USE.DEPARTMENT.CODE>:COMMA

**********************************HHHHH*****20081210****************
    BANK = LOC.REF<1,BRLR.BANK>

    IF BANK EQ '0017' THEN

        OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":"CONFIRMATION":COMMA
        OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":"CONFIRMATION":COMMA
    END
*********************************HHHH******************************
    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY
    OFS.ID = "T":TNO:".":R.NEW(SCB.BT.OUR.REFERENCE)<1,NUMBERBR.ID>:"-":DAT

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    RETURN

*******************************************************

END
