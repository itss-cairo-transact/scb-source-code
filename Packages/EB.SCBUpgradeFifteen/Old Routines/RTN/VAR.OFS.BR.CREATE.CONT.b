* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>290</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.OFS.BR.CREATE.CONT


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


*    TEXT = "HI" ; CALL REM
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE  = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
**** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
**** SCB R15 UPG 20160703 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "BR"
*** OFS.USER.INFO    = "/"

************HYTHAM********20090128*******************************
    COMP = ID.COMPANY
    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
**************HYTHAM******20090128*******************************

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    COMMA = ","

    CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,ID.NEW,LOC.REF)
    BR.EXIT = LOC.REF<1,BRLR.FT.REF.CONT>
*TEXT = "BR.EXIT= ":BR.EXIT   ; CALL REM
    IF NOT(BR.EXIT) THEN

        TXN.TYPE = "AC21"
        OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TXN.TYPE:COMMA
        BR.AMT=R.NEW(EB.BILL.REG.AMOUNT)
        OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": BR.AMT:COMMA
        BR.CUR=R.NEW(EB.BILL.REG.CURRENCY)
        OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": BR.CUR:COMMA
        BR.DEPT=R.USER<EB.USE.DEPARTMENT.CODE>
        OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":BR.DEPT:COMMA

*IF PGM.VERSION EQ ",SCB.CHQ.LCY.REG1" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG.CONV" OR PGM.VERSION EQ ",SCB.CHQ.OUT.OF.CLEARING" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG1.COLL" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG.CONV1" OR PGM.VERSION EQ ",SCB.CHQ.OUT.OF.CLEARING1" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG1.EXIST" THEN
*   CATEG.DR = "19002"
*END
*IF PGM.VERSION EQ ",SCB.BILLS" OR PGM.VERSION EQ ",SCB.BILLS.CONV" THEN
*   CATEG.DR = "19003"
*END
        CATEG = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT>[11,4]

        CATEG.DR = "1":CATEG
        BR.CUR  = R.NEW(EB.BILL.REG.CURRENCY)
        BR.DEPT = R.USER<EB.USE.DEPARTMENT.CODE>
        IF LEN(BR.DEPT) EQ 1 THEN
            BR.DEPT = 0:BR.DEPT
        END
***********UPDATED BY NESSREEN ON 19/6/2008*******************
**    BR.ACCT.DR = BR.CUR:CATEG.DR:"000100":BR.DEPT
        CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BR.ACCT.DR = BR.CUR:CATEG.DR:"0001":CO.CODE
**************************************************************
        OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":BR.ACCT.DR:COMMA
        OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":BR.ACCT.DR:COMMA

        BR.ACCT.CR=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT>
        IF NOT(BR.ACCT.CR) THEN RETURN
***************20081111*****************
*****    IF BR.ACCT.CR[1,2] EQ "99" THEN RETURN
**************20081111******************
        OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":BR.ACCT.CR:COMMA
        OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":BR.ACCT.CR:COMMA

        OFS.MESSAGE.DATA := "ORDERING.BANK=":ID.NEW:COMMA
        DAT = TODAY
        OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DAT:COMMA
        OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":DAT:COMMA
        OFS.MESSAGE.DATA := "COMMISSION.CODE=WAIVE"::COMMA
        OFS.MESSAGE.DATA := "CHARGE.CODE=WAIVE"::COMMA

        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
        OFS.ID = "T":TNO:".":ID.NEW:".":"CONT":"-":DAT
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    END
    RETURN
END
