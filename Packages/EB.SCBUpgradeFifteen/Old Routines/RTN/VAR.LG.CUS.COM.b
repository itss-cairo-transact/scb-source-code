* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.CUS.COM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.COMM


******************************
    FN.CUS='F.SCB.CUS.EXP';ID=R.NEW(LD.CUSTOMER.ID);R.CUS='';F.CUS=''
    CALL F.READ(FN.CUS,ID,R.CUS,F.CUS, ETEXT)
    LG.TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND>
    IF ETEXT EQ '' THEN
        LOCATE LG.TYPE IN R.CUS<CUS.EXP.TYPE,1>  SETTING Y THEN
            TEXT=R.CUS<CUS.EXP.PERCENT><1,Y>:'PER';CALL REM
            COM.PER = R.CUS<CUS.EXP.PERCENT><1,Y>
            COM.EXP= R.CUS<CUS.EXP.EXP.DAT><1,Y>
        END
    END
********************************
    LG.DATE=R.NEW(LD.DATE.TIME)
    ID.LG.DATE=ID.NEW:".":LG.DATE
    TEXT=ID.LG.DATE;CALL REM
    LG.AMT=R.NEW(LD.AMOUNT)

    FN.LG.COMM='F.SCB.LG.COMM';ID=ID.LG.DATE;R.LG.COMM='';F.LG.COMM=''
    CALL F.READ(FN.LG.COMM,ID.LG.DATE,R.LG.COMM,F.LG.COMM, ETEXT)

    R.LG.COMM<LG.COM.LG>=  ID.NEW
    R.LG.COMM<LG.COM.DATE>=   TODAY
    R.LG.COMM<LG.COM.CUSTOMER>= R.NEW(LD.CUSTOMER.ID)
    R.LG.COMM<LG.COM.COM.PER>=  COM.PER
    R.LG.COMM<LG.COM.COM.EXP.DAT>= COM.EXP
    R.LG.COMM<LG.COM.LG.TYPE>= LG.TYPE
    R.LG.COMM<LG.COM.LG.AMOUNT>= R.NEW(LD.AMOUNT)
    R.LG.COMM<LG.COM.OP.CODE>=  R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    R.LG.COMM<LG.COM.DR.ACCT>=  R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>

    R.LG.COMM<LG.COM.MARG.PERC> =  R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>
    R.LG.COMM<LG.COM.MARG.AMT>  =  R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
    R.LG.COMM<LG.COM.LG.EXP.DATE>= R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
    R.LG.COMM<LG.COM.LG.VALUE.DATE>=  R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
    R.LG.COMM<LG.COM.CURRENCY>=       R.NEW(LD.CURRENCY)
    R.LG.COMM<LG.COM.LIMIT.REF>=   R.NEW(LD.LIMIT.REFERENCE)
    R.LG.COMM<LG.COM.OLD.LG.NO>=R.NEW(LD.LOCAL.REF)<1,LDLR.OLD.NO>
    R.LG.COMM<LG.COM.END.COMM.DATE>=R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
*****************
*Line [ 76 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    COUNT.AMT=DCOUNT(R.NEW(LD.CHRG.AMOUNT),@VM)
    TEXT= COUNT.AMT:'COUNT'
    FOR I=1 TO COUNT.AMT
        R.LG.COMM<LG.COM.COMM.CODE,I>= R.NEW(LD.CHRG.CODE)<1,I>
        R.LG.COMM<LG.COM.COMM.AMT,I> =  R.NEW(LD.CHRG.AMOUNT)<1,I>
    NEXT I
**********************
    R.LG.COMM<LG.COM.COMM.ACCT> =  R.NEW(LD.CHRG.LIQ.ACCT)
    R.LG.COMM<LG.COM.AMOUNT.INC.DEC> = R.NEW(LD.AMOUNT.INCREASE)
    R.LG.COMM<LG.COM.CONFIS.AMT> = R.NEW(LD.LOCAL.REF)<1,LDLR.CONFISC.AMT>
    R.LG.COMM<LG.COM.CHEQUE.AMOUNT> =  R.NEW(LD.LOCAL.REF)<1,LDLR.CHQ.AMT.CUR>
    R.LG.COMM<LG.COM.CO.CODE>    =     R.NEW(LD.CO.CODE)

    CALL F.WRITE(FN.LG.COMM,ID.LG.DATE,R.LG.COMM)


    RETURN
END
