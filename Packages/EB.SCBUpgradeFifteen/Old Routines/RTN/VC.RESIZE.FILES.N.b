* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>457</Rating>
*-----------------------------------------------------------------------------
    PROGRAM VC.RESIZE.FILES.N
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE


    INCLUDE JBC.h

*Line [ 28 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPEN "VOC" TO F.VOC ELSE NULL

    SEL.CMD = "SSELECT VOC WITH ( @ID LIKE F... AND  @ID LIKE ...FBNK.CATEG... OR @ID LIKE ...FBNK.STMT... OR @ID LIKE ...FBNK.RE.CONSOL.SPEC.ENTRY... OR @ID LIKE ...FBNK.LD.LOANS.AND.DEPOSITS$HIS... OR @ID LIKE ...FBNK.FUNDS.TRANSFER$HIS... OR @ID LIKE ...FBNK.RE.SPEC.ENTRY.DETAIL... OR @ID LIKE ...FBNK.ACCT.ACTIVITY... OR @ID LIKE ...FBNK.EB.CONTRACT.BALANCES... OR @ID LIKE ...FBNK.CUSTOMER$HIS... OR @ID LIKE ...FBNK.ACCOUNT$HIS... ) AND F1 LIKE F... AND F2 NE '' AND @ID UNLIKE ...FBNK.STMT.PRINTED AND @ID UNLIKE ...FBNK.STMT.VAL.ENTRY AND @ID UNLIKE ...FBNK.STMT.E..."
    CALL EB.READLIST(SEL.CMD,FILE.LIST,"","","")
*    PRINT "DO YOU WANT TO RESIZE ALL FILES?"
*    INPUT RESIZE.ALL
    RESIZE.ALL = "Y"

    LOOP

        REMOVE FILE.ID FROM FILE.LIST SETTING MORE.IDS
    WHILE FILE.ID:MORE.IDS
*================= FOR DISTRIBUT FILES 15/02/2010 ===================
        FILENAME=''
        Curr.File=FILE.ID
        OPEN Curr.File TO f.Curr.File THEN
            STATUS f.stat FROM f.Curr.File THEN

                FileType    = f.stat<21>          ;*Hold the type of file. DISTRIB or J4
                FileSize    = f.stat<6>
                FileSep     = f.stat<23>
                FileModulo  = f.stat<22>
                File.Path   = f.stat<20>
            END ELSE
                f.stat = ''
            END
        END ELSE
            FileType = ''     ;* Ignore this file
        END
        IF FileType NE 'DISTRIB' THEN
            PRINT "FILETYPE :":FileType:FILE.ID
*================= FOR DISTRIBUT FILES 15/02/2010 ===================

            PRINT FILE.ID
            FILE.NAME = ""
            FILE.NAME<1> = FILE.ID
            FILE.NAME<2> = "NO.FATAL.ERROR"
            FILE.VAR = ""
            CALL OPF(FILE.NAME,FILE.VAR)
*            IF ETEXT THEN CONTINUE

*            IF IOCTL(FILE.VAR,JBC_COMMAND_GETFILENAME,FILENAME) ELSE
 *               CRT "IOCTL failed !!" ; EXIT(2)
  *          END

            EXE.CMD = "jrf -R ":FILE.ID
            EXECUTE EXE.CMD CAPTURING RESIZE.OUTPUT

            IF INDEX(RESIZE.OUTPUT,"Resize",1) THEN
                PRINT RESIZE.OUTPUT
                IF RESIZE.ALL = "Y" THEN
                    RESIZE.ANS = "Y"
                END ELSE
                    PRINT "DO YOU WANT TO RESIZE THIS FILE Y/N?"
                    INPUT RESIZE.ANS
                END
                IF RESIZE.ANS = "Y" THEN
                    NEW.MODULO = FIELD(FIELD(TRIM(RESIZE.OUTPUT)," ",14,1),".",1)
                    EXE.CMD = "jrf -S":NEW.MODULO:" ":FILE.ID
                    EXECUTE EXE.CMD
                END
            END
        END
    REPEAT

    STOP
END
