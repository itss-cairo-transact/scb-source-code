* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
****** WAEL ****
*-----------------------------------------------------------------------------
* <Rating>48</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.OFS(CHQ.TYPE,VER.NAME,TRANS.TYPE,DB.CUR,DB.AMT,PROFT.CUST,DB.DATE,CR.DATA,DB.ACCT,CR.ACCT,CHQ.NO,BENF.CUST,PROFIT.DEPT,ORDER.BANK)


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    GOSUB CREATE.FILE
*******************
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
*******************
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"

    IF VER.NAME = ",SCB.LG.CONFISCATE" THEN
        FN.CHQ.PRES = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRES = ''
        R.CHQ.PRES = ''
        CALL OPF(FN.CHQ.PRES,F.CHQ.PRES)
******************************
        COMP = C$ID.COMPANY
        COM.CODE = COMP[8,2]
        CHQ.AC = DB.CUR :"16151000100":COM.CODE

*******************************
        T.SEL ="SELECT F.SCB.FT.DR.CHQ WITH @ID LIKE ":CHQ.AC:"... BY CHEQ.NO"
        KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CHEQ.NO = FIELD(KEY.LIST<SELECTED>,".",2) + 1
        END ELSE
            CHEQ.NO = '1'
        END
        IF DB.CUR THEN
            OFS.OPTIONS = "SCB.LG.DRFT"
        END ELSE
            OFS.OPTIONS = "SCB.LG.DRFT"
        END
    END ELSE
        OFS.OPTIONS = "SCB"
    END

***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
    DB.AMT=FMT(DB.AMT,"R2#10")
**    OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.CUST=":PROFT.CUST:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATA:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    IF VER.NAME = ",SCB.LG.CONFISCATE" THEN
***  OFS.MESSAGE.DATA := "CHEQUE.NUMBER=":CHEQ.NO:COMMA
    END
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":PROFIT.DEPT:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":ORDER.BANK:COMMA
    OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
    OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE':COMMA
    OFS.MESSAGE.DATA := "LOCAL.REF:14:1=":CHQ.TYPE:COMMA
**    OFS.MESSAGE.DATA := "LOCAL.REF:14:2=":BENF.CUST:COMMA
    IF VER.NAME = ",SCB.LG.CONFISCATE" THEN
        OFS.MESSAGE.DATA := "LOCAL.REF:2:1=":BENF.CUST:COMMA
        OFS.MESSAGE.DATA := "BEN.CUSTOMER=":'LG'
    END ELSE
        OFS.MESSAGE.DATA := "LOCAL.REF:2:1=":"������":COMMA
        OFS.MESSAGE.DATA := "BEN.CUSTOMER=":BENF.CUST
    END


    RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(LD.DATE.TIME)

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    OFS.ID = "T":TNO:".":OPERATOR:"_LG":RND(10000):"_":D.T
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    RETURN
*--------------------------------------------------------
END
