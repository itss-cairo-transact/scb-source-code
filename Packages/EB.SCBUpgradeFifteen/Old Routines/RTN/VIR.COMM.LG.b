* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>379</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VIR.COMM.LG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP.AMT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.EXP.P
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.COMM.CORS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.CODE

    SHIFT ='' ; ROUND =''
    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)


    W.CUR           = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    W.EXP.DATE      = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
    W.ISSUE.DATE    = R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
    START.DATE      = W.ISSUE.DATE
    W.END.COMM.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
    COMM.NO         = 1
    COMM.AMT1       = 0
    AMOUNT.PLS      = 0
    NEW.COM.AMT     = 0
    NEW.COMM.NO     = 1
    NEW.END.COMM.DATE= ''
*                  ........................................
*----------------- To Calculate ( END.COMM.DATE & COMM.NO ) --------------------
*                  ........................................
*---------*
COMM.CALC:
*---------*
    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MAYLOC)
    OLD.EXP     = MAYLOC<1,LDLR.ACTUAL.EXP.DATE>
    OLD.MARG    = MAYLOC<1,LDLR.MARGIN.PERC>
    OLD.END.COM = MAYLOC<1,LDLR.END.COMM.DATE>
    IF OLD.END.COM  NE '' THEN
***    CALL CDT('EG', OLD.END.COM, '1')
    END
    IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
        DEAL.AMOUNT = R.NEW(LD.AMOUNT.INCREASE)
    END ELSE
        DEAL.AMOUNT = R.NEW(LD.AMOUNT) + R.NEW(LD.AMOUNT.INCREASE)
    END

    IF R.NEW(LD.AMOUNT.INCREASE) AND ( R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = OLD.EXP ) THEN

        W.EXP.DATE =  R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
    END

    NEW.PERC = R.NEW(LD.LOCAL.REF)<1,LDLR.PERC.EXP>
    FN.FT.COM= 'F.SCB.LD.CODE' ; F.FT.COM = ''
    CALL OPF(FN.FT.COM,F.FT.COM)

    ID.COM  = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,1>

    CALL F.READ(FN.FT.COM,ID.COM,R.FT.COM,F.FT.COM,E1)

    COM.RER = R.FT.COM<LD.CODE.PERC> / 100
    CUSS    = R.NEW(LD.CUSTOMER.ID)

    LG.TYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND>

**-----------------------                       --------------------**

    FN.CUR= 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
    CALL OPF(FN.CUR,F.CUR)

    CALL F.READ(FN.CUR,R.NEW(LD.CURRENCY), R.CUR, F.CUR ,E11)
    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
    W.CUR    = R.NEW(LD.CURRENCY)
    LOCATE W.CUR IN R.FT.COM<LD.CODE.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
    COM.RER = R.FT.COM<FT4.PERCENTAGE,J> / 100

    COM.MIN = R.FT.COM<LD.CODE.MIN.AMT,J>

*    IF R.NEW(LD.CURRENCY) EQ 'EGP' THEN
*    COM.MIN = R.FT.COM<LD.CODE.MIN.AMT>
*    END ELSE
*     IF R.NEW(LD.CURRENCY) EQ 'JPY' THEN
*        COM.MIN = (R.FT.COM<LD.CODE.MIN.AMT> * 100) / CUR.RATE
*   END ELSE
*      COM.MIN = R.FT.COM<LD.CODE.MIN.AMT> / CUR.RATE
*   END
* END
*  CALL EB.ROUND.AMOUNT ('USD',COM.MIN,'',"2")

    IF R.NEW(LD.AUTHORISER) = '' THEN
        START.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
    END  ELSE
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = ''   THEN
            START.DATE = TODAY
        END ELSE
            START.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            CALL CDT('EG', START.DATE, '1')
        END

        IF OLD.MARG = "100" THEN

            IF OLD.MARG >  R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>  THEN
                START.DATE = TODAY
            END
        END
    END

*-------        ------------------------20090405-------------------------*

    OP.CO   = MAYLOC<1,LDLR.OPERATION.CODE>
    MAR.PRC = MAYLOC<1,LDLR.MARGIN.PERC>
    LG.K    = MAYLOC<1,LDLR.LG.KIND>

    IF OP.CO EQ '1111' AND MAR.PRC EQ '' AND LG.K EQ 'AD' THEN
        START.DATE = TODAY
    END

*------------------------------20090405--------------------------*

    IF R.NEW(LD.AMOUNT.INCREASE) THEN
        BEGIN CASE
        CASE   R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> >  OLD.EXP
            NEW.START  = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
            AMOUNT.PLS = 1
            IF R.NEW(LD.AMOUNT.INCREASE) < 0  THEN
                DEAL.AMOUNT = R.NEW(LD.AMOUNT) + R.NEW(LD.AMOUNT.INCREASE)
            END
        CASE R.NEW(LD.AMOUNT.INCREASE) AND ( R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = OLD.EXP )
            W.EXP.DATE =  R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>
        CASE OTHERWISE
            START.DATE = TODAY
        END CASE
    END
*----------------------------------------------------------------------*

    HH.DATE = START.DATE
    TMP.NO = 0
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "YES-����� ��� �����" THEN
        START.DATE = SHIFT.DATE( START.DATE, '3M', 'UP')
    END ELSE
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "NO-����� �����" THEN
            FOR I = 1 TO 100
                START.DATE = SHIFT.DATE( START.DATE, '3M', 'UP')
                TMP.DATE = START.DATE
                IF TMP.DATE > W.EXP.DATE THEN
                    COMM.NO = I - TMP.NO ; I = 100
                END
            NEXT I
        END
    END
    CALL CDT('EG', START.DATE, '-1')
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "NO-����� �����" THEN
        START.DATE4 = SHIFT.DATE( HH.DATE, '12M', 'UP')
        CALL CDT('EG', START.DATE4, '-1')
        IF TMP.DATE LE  START.DATE4 THEN
            CALL CDT('EG', TMP.DATE, '-1')
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = TMP.DATE   ; CALL REBUILD.SCREEN
        END ELSE
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = START.DATE4 ; CALL REBUILD.SCREEN
        END
    END ELSE
        R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = START.DATE  ; CALL REBUILD.SCREEN
    END
    NEW.END.COMM.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>

    IF OLD.EXP THEN

        IF R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE> = OLD.EXP  AND OLD.END.COM GT TODAY  THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE>  = OLD.END.COM

        END ELSE
*----------------------------20090215--------------------------------*

            IF R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> LT TODAY THEN
                CALL CDT('EG', HH.DATE, '-1')
                IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "NO-����� �����" THEN

                    START.DATE4 = SHIFT.DATE( HH.DATE, '12M', 'UP')
                    R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = START.DATE4 ; CALL REBUILD.SCREEN
                END ELSE
                    START.DATE4 = SHIFT.DATE( HH.DATE, '3M', 'UP')
                    R.NEW(LD.LOCAL.REF)<1,LDLR.END.COMM.DATE> = START.DATE4 ; CALL REBUILD.SCREEN
                END
            END

        END
    END
*-------------------------------*20090215----------------------------*


    IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> = "100" THEN

        IF R.NEW(LD.CURRENCY) NE "EGP" THEN
            FN.CUR= 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
            CALL OPF(FN.CUR,F.CUR)

            CALL F.READ(FN.CUR,R.NEW(LD.CURRENCY), R.CUR, F.CUR ,E11)
            CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
            AMT30 = "30"
            COMM.AMTT  = AMT30 / CUR.RATE
            CALL EB.ROUND.AMOUNT ('USD',COMM.AMTT,'',"2")
            IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                COMM.AMTT  = COMM.AMTT * 100
                CALL EB.ROUND.AMOUNT ('USD',COMM.AMTT,'',"2")
            END
        END ELSE
            COMM.AMTT = "30"
        END
    END ELSE

*TEXT = "COMI = " : DEAL.AMOUNT ; CALL REM
*TEXT = "PER  = " : COM.RER     ; CALL REM

        COM.AMT = DEAL.AMOUNT * COM.RER
        IF COM.AMT < COM.MIN THEN
            COMM.AMTTT = COM.MIN
        END ELSE
            COMM.AMTTT = COM.AMT
        END
*-------------------------------------------------------------------*
*---------------------------  DATE & AMOUT -------------------------*
*-------------------------------------------------------------------*

        IF  AMOUNT.PLS = "1" THEN
            NEW.COM.AMT = R.NEW(LD.AMOUNT) * COM.RER
            IF NEW.COM.AMT < COM.MIN THEN
                NEW.COMM.AMTTT = COM.MIN
            END ELSE
                NEW.COMM.AMTTT = NEW.COM.AMT
            END

            FOR I = 1 TO 100
                NEW.START = SHIFT.DATE( NEW.START, '3M', 'UP')
                TMP.DATE = NEW.START

                NEW.EXP.DATE =   NEW.END.COMM.DATE
                IF TMP.DATE > NEW.EXP.DATE THEN
                    NEW.COMM.NO = I - TMP.NO ; I = 100
                END

            NEXT I
        END
        IF R.NEW(LD.CURRENCY) NE "EGP" THEN
            FN.CUR= 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
            CALL OPF(FN.CUR,F.CUR)

            CALL F.READ(FN.CUR,R.NEW(LD.CURRENCY), R.CUR, F.CUR ,E11)
            CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
            NEW.AMT1  = NEW.COMM.AMTTT / CUR.RATE
            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")
            IF  R.NEW(LD.CURRENCY) EQ "JPY" THEN
                NEW.AMT1  = NEW.COMM.AMTTT * 100
                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")
            END
        END ELSE
            NEW.AMT1 = NEW.COMM.AMTTT
        END

        NEW.COM.AMT = NEW.AMT1 * NEW.COMM.NO
*TEXT = "NEW1= " : NEW.AMT1 ; CALL REM
*TEXT = "NOM1= " : NEW.COMM.NO ; CALL REM
    END
*--------------------------- DATE & AMOUT ---------------------------*

    IF R.NEW(LD.CURRENCY) NE "EGP" AND R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.PERC> NE 100 THEN

        COMM.AMTT  = COMM.AMTTT
    END ELSE
        IF COMM.AMTTT NE '' THEN
            COMM.AMTT= COMM.AMTTT
        END
    END
*TEXT ="COM3=  " : COMM.AMTT ; CALL REM
*TEXT ="COMM.NO=  " : COMM.NO ; CALL REM
    COMISS.AMT =  COMM.AMTT * COMM.NO

** -------------------------------   --------------------------------- **

    CALL EB.ROUND.AMOUNT ('EGP',COMISS.AMT,'',"2")
    CALL EB.ROUND.AMOUNT ('EGP',COMM.AMTT,'',"2")
*TEXT = "COMISS.AMT" : COMM.AMTT ; CALL REM
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "YES-����� ��� �����" THEN
        COMISS.AMT = COMM.AMTT
    END

    IF R.NEW(LD.AMOUNT.INCREASE) < 0 THEN
* R.NEW(LD.CHRG.AMOUNT) =  COMISS.AMT
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT> = COMISS.AMT
    END
**-----------------

    FN.COM = 'F.SCB.LG.COMM.CORS' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

**-----------------
    NEW.COMMM = COMISS.AMT + NEW.COM.AMT

* R.NEW(LD.CHRG.AMOUNT)     = COMISS.AMT + NEW.COM.AMT
    R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT> = COMISS.AMT + NEW.COM.AMT
    IF R.NEW(LD.AMOUNT.INCREASE) > 0 THEN
* R.NEW(LD.CHRG.AMOUNT) = COMISS.AMT + NEW.COM.AMT
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT> = COMISS.AMT + NEW.COM.AMT
    END
    IF R.NEW(LD.AMOUNT.INCREASE) < 0 THEN
* R.NEW(LD.CHRG.AMOUNT) = COMISS.AMT
        R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT> = COMISS.AMT
    END

    COM.ID = ID.NEW :'-': TODAY
    CALL F.READ(FN.COM,COM.ID,R.COM,F.COM,READ.ERR)

***--------------------------          ------------------------------***
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "YES-����� ��� �����" THEN

        ST.DATE = '' ; ST.DATE = R.NEW(LD.VALUE.DATE)

        CHRG.CODE = R.NEW(LD.CHRG.CODE)
* CHRG.AMT  = R.NEW(LD.CHRG.AMOUNT)
        CHRG.AMT  = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.AMT>
        EN.DATE = R.NEW(LD.LOCAL.REF)<1,LDLR.ACTUAL.EXP.DATE>
        START.DATE = R.NEW(LD.VALUE.DATE)
        DIFF = "C"

        CALL CDD("",ST.DATE,EN.DATE,DIFF)
        SAM1 = DIFF / 90
        SAM2 = SAM1 + 0.9

***-----------------------------------------------*******

        FOR II = 1 TO 100
            START.DATE = SHIFT.DATE( START.DATE, '3M', 'UP')
            TMP.DATE   = START.DATE
            IF TMP.DATE > EN.DATE THEN

                PERIOD.NO = II -1  ; II = 100
            END

        NEXT II

        FIRST.INT.MON= R.NEW(LD.VALUE.DATE)
***------------------------------------------------******
        FOR I = 1 TO PERIOD.NO + 1

            ST.DATE = R.NEW(LD.VALUE.DATE)
            GOSUB PROCESS.FRQ.MONTHS
*******************************************
            FIRST.INT.MON=SHIFT.DATE(ST.DATE,'3M',UP)
******************************

        NEXT I
        FIRST.INT.MON.S = SHIFT.DATE(ST.DATE,'3M',UP)

        R.COM<LG.COM.S.CODE>      = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,3>
        R.COM<LG.COM.STAM.DATE>   = FIRST.INT.MON.S
        R.COM<LG.COM.STAM.AMOUNT> = "10"
        R.COM<LG.COM.STAM.STATUS> = 'O'

*WRITE  R.COM TO F.COM , COM.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":COM.ID:"TO" :FN.COM
*END
        CALL F.WRITE (FN.COM,COM.ID,R.COM)
    END ELSE

        IF R.NEW(LD.LOCAL.REF)<1,LDLR.DECREASE>  = "NO-����� �����" THEN
*************************UPDATE NI7OOOOO*****************************
            IF NEW.PERC NE ''  THEN
                COMISSS.AMT  =   COMISS.AMT * NEW.PERC
            END ELSE
                TEXT = "COMISS.AMT : " : COMISS.AMT ; CALL REM
                COMISSS.AMT  =   COMISS.AMT
            END
*************************UPDATE NI7OOOOO*****************************


            R.COM<LG.COM.COM.CODE>   = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,1>
            R.COM<LG.COM.COM.DATE>   = TODAY
            R.COM<LG.COM.COM.AMOUNT> = COMISSS.AMT
            R.COM<LG.COM.COM.STATUS> = 'O'
            R.COM<LG.COM.LG.STATUS>  = 'O'

            R.COM<LG.COM.SWF.CODE>   = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,2>
            R.COM<LG.COM.SWF.DATE>   = FIRST.INT.MON
            SW.CO                    = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,2>
            TEXT =  "SW=  " : SW.CO ; CALL REM
            CALL DBR('SCB.LD.CODE':@FM:LD.CODE.MIN.AMT,SW.CO,PL.AMT)
            SW.CO                      = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,2>
            CALL F.READ(FN.FT.COM,SW.CO,R.FT.COM,F.FT.COM,E1)
            R.COM<LG.COM.SWF.AMOUNT,I> = R.FT.COM<LD.CODE.MIN.AMT>
            R.COM<LG.COM.SWF.STATUS> = 'O'

        END

        R.COM<LG.COM.S.CODE>      = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,3>
        R.COM<LG.COM.STAM.DATE>   = FIRST.INT.MON.S
        R.COM<LG.COM.STAM.AMOUNT> = "10"
        R.COM<LG.COM.STAM.STATUS> = 'O'

*WRITE  R.COM TO F.COM , COM.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":COM.ID:"TO" :FN.COM
*END
        CALL F.WRITE (FN.COM,COM.ID,R.COM)
    END

***-----------------------------------------------------------

*------------------ PROCESS FREQUENCY IN MONTHS -------------------------------*
PROCESS.FRQ.MONTHS:
    IF NEW.PERC NE ''  THEN
        COMISSS.AMT  =   COMISS.AMT * NEW.PERC
    END
    R.COM<LG.COM.COM.CODE,I>   = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,1>
    R.COM<LG.COM.COM.DATE,I>   = FIRST.INT.MON
    R.COM<LG.COM.COM.AMOUNT,I> = COMISSS.AMT
    R.COM<LG.COM.COM.STATUS,I> = 'O'

    R.COM<LG.COM.SWF.CODE,I>   = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,2>
    R.COM<LG.COM.SWF.DATE,I>   = FIRST.INT.MON
    SW.CO                      = R.NEW(LD.LOCAL.REF)<1,LDLR.CHARGE.CODE,2>
    CALL F.READ(FN.FT.COM,SW.CO,R.FT.COM,F.FT.COM,E1)
    R.COM<LG.COM.SWF.AMOUNT,I> = R.FT.COM<LD.CODE.MIN.AMT>
    R.COM<LG.COM.SWF.STATUS,I> = 'O'

    ST.DATE = FIRST.INT.MON

    RETURN
***-----------------------------------------------------------


    CALL EB.ROUND.AMOUNT ('EGP',COMISS.AMT,'',"2")
    CALL REBUILD.SCREEN
    TEXT = "FINAL " ; CALL REM
    RETURN
END
