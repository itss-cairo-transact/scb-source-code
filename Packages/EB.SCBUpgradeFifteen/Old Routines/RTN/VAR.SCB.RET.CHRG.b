* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>235</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SCB.RET.CHRG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*----------------------------------------------------------

    FN.BR  = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF( FN.BR,F.BR)

    FN.CUS = 'FBNK.CUSTOMER'      ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.CUS = 'FBNK.CUSTOMER'      ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.ACC = DCOUNT (R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
    FOR  NUMBER.ACC = 2 TO DCOUNT.ACC

        IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC> NE '' THEN
            IF R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>[1,6] NE '994999' THEN
*******************8-10-2013
                ACCT.NO  = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>
                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUSS.IDD)
                CALL F.READ(FN.CUS,CUSS.IDD,R.CUS,F.CUS,ER1)
*******************8-10-2013
                SEC  = R.CUS<EB.CUS.SECTOR>
                IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN

                    IF R.NEW(INF.MLT.AMOUNT.LCY)<1,NUMBER.ACC> LE 300 THEN
                        AMOUNT= "2"
                    END
                    IF R.NEW(INF.MLT.AMOUNT.LCY)<1,NUMBER.ACC> GT 300 THEN
                        AMOUNT= "3"
                    END
                    ACC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>
                    *CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC,ACC.OFFICER)
                    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC,AC.COMP)
                    ACC.OFFICER  = AC.COMP[8,2]
                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC,CATEG)

                    CURR    = "EGP"
*----
* CR
*----
                    Y.ACCT       = ""
                    PL.COM       = 52014
                    Y.AMT        = AMOUNT
                    NEW.VAL.DATE = TODAY

                    GOSUB AC.STMT.ENTRY
*----
* DR
*----
                    Y.ACCT = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NUMBER.ACC>
                    PL.COM = ""
                    Y.AMT  = AMOUNT * -1
                    BR.ID  = R.NEW(INF.MLT.BILL.NO)


                    CALL F.READ(FN.BR,BR.ID, R.BR, F.BR, ETEXT)
****--------------------------------------------------
*         CALL CDT("",COL.DAT,'+1W')
*         NEW.VAL.DATE = COL.DAT
*
***-------------------------------------------------------
                    COL.DAT      = TODAY
                    COMP<1,1> = "EG0010040"
                    COMP<1,2> = "EG0010050"
                    ID.COMP   = ID.COMPANY
                    FINDSTR ID.COMP IN COMP SETTING IDD THEN
                        NEW.VAL.DATE = TODAY
                    END  ELSE
                        CALL CDT("",COL.DAT,'+1W')
                        NEW.VAL.DATE = COL.DAT
                    END
***----------------------------------------------------------


                    GOSUB AC.STMT.ENTRY
                END
            END
        END
    NEXT NUMBER.ACC

    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '825'
    ENTRY<AC.STE.THEIR.REFERENCE>  = "RET-CHARGE"
    ENTRY<AC.STE.TRANS.REFERENCE>  = "RET-CHARGE"
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.COM
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = NEW.VAL.DATE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

*    ENTRY<AC.STE.OVERRIDE>         = R.NEW(INF.MLT.OVERRIDE)
*    ENTRY<AC.STE.STMT.NO>          = R.NEW(INF.MLT.STMT.NO)

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
