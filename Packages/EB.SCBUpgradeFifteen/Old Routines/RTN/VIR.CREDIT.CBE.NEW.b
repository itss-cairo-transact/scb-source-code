* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>237</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.CREDIT.CBE.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE.NEW
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*********************** OPENING FILES *****************************
    FN.CPY  = "F.SCB.CREDIT.CBE.NEW"  ; F.CPY   = ""
    CALL OPF (FN.CPY,F.CPY)
    CBE.ID = ID.NEW
*------------------------------------------------------------------
    IF V$FUNCTION ='A' THEN
        CALL F.READ(FN.CPY,CBE.ID,R.CPY,F.CPY,E2)

********************** COPY NEW DATA ******************************
        R.CPY<SCB.C.CBE.BNK.DATE>  =  R.NEW(SCB.C.CBE.BNK.DATE)
        R.CPY<SCB.C.CBE.CBE.NO>    =  R.NEW(SCB.C.CBE.CBE.NO)
        R.CPY<SCB.C.CBE.TOT.US.1>  =  R.NEW(SCB.C.CBE.TOT.US.1)
        R.CPY<SCB.C.CBE.TOT.CR.1>  =  R.NEW(SCB.C.CBE.TOT.CR.1)
        R.CPY<SCB.C.CBE.TOT.US.2>  =  R.NEW(SCB.C.CBE.TOT.US.2)
        R.CPY<SCB.C.CBE.TOT.CR.2>  =  R.NEW(SCB.C.CBE.TOT.CR.2)
        R.CPY<SCB.C.CBE.TOT.US.3>  =  R.NEW(SCB.C.CBE.TOT.US.3)
        R.CPY<SCB.C.CBE.TOT.CR.3>  =  R.NEW(SCB.C.CBE.TOT.CR.3)
        R.CPY<SCB.C.CBE.TOT.US.4>  =  R.NEW(SCB.C.CBE.TOT.US.4)
        R.CPY<SCB.C.CBE.TOT.CR.4>  =  R.NEW(SCB.C.CBE.TOT.CR.4)
        R.CPY<SCB.C.CBE.TOT.US.5>  =  R.NEW(SCB.C.CBE.TOT.US.5)
        R.CPY<SCB.C.CBE.TOT.CR.5>  =  R.NEW(SCB.C.CBE.TOT.CR.5)
        R.CPY<SCB.C.CBE.TOT.US.6>  =  R.NEW(SCB.C.CBE.TOT.US.6)
        R.CPY<SCB.C.CBE.TOT.CR.6>  =  R.NEW(SCB.C.CBE.TOT.CR.6)
        R.CPY<SCB.C.CBE.TOT.US.7>  =  R.NEW(SCB.C.CBE.TOT.US.7)
        R.CPY<SCB.C.CBE.TOT.CR.7>  =  R.NEW(SCB.C.CBE.TOT.CR.7)
        R.CPY<SCB.C.CBE.TOT.US.8>  =  R.NEW(SCB.C.CBE.TOT.US.8)
        R.CPY<SCB.C.CBE.TOT.CR.8>  =  R.NEW(SCB.C.CBE.TOT.CR.8)
        R.CPY<SCB.C.CBE.TOT.US.9>  =  R.NEW(SCB.C.CBE.TOT.US.9)
        R.CPY<SCB.C.CBE.TOT.CR.9>  =  R.NEW(SCB.C.CBE.TOT.CR.9)
        R.CPY<SCB.C.CBE.TOT.US.10> =  R.NEW(SCB.C.CBE.TOT.US.10)
        R.CPY<SCB.C.CBE.TOT.CR.10> =  R.NEW(SCB.C.CBE.TOT.CR.10)
        R.CPY<SCB.C.CBE.TOT.US.11> =  R.NEW(SCB.C.CBE.TOT.US.11)
        R.CPY<SCB.C.CBE.TOT.CR.11> =  R.NEW(SCB.C.CBE.TOT.CR.11)
        R.CPY<SCB.C.CBE.TOT.US.12> =  R.NEW(SCB.C.CBE.TOT.US.12)
        R.CPY<SCB.C.CBE.TOT.CR.12> =  R.NEW(SCB.C.CBE.TOT.CR.12)
        R.CPY<SCB.C.CBE.TOT.US.99> =  R.NEW(SCB.C.CBE.TOT.US.99)
        R.CPY<SCB.C.CBE.TOT.CR.99> =  R.NEW(SCB.C.CBE.TOT.CR.99)
        R.CPY<SCB.C.CBE.TOT.US.13> =  R.NEW(SCB.C.CBE.TOT.US.13)
        R.CPY<SCB.C.CBE.TOT.CR.13> =  R.NEW(SCB.C.CBE.TOT.CR.13)
        R.CPY<SCB.C.CBE.CO.CODE>   =  ID.COMPANY

** R.CPY<SCB.C.CBE.CO.CODE>   =  R.NEW(SCB.C.CBE.CO.CODE)


        US.99 = R.NEW(SCB.C.CBE.TOT.US.99) ; CR.99 = R.NEW(SCB.C.CBE.TOT.CR.99)
        US.13 = R.NEW(SCB.C.CBE.TOT.US.13) ; CR.13 = R.NEW(SCB.C.CBE.TOT.CR.13)

        IF (US.99 GE 30 OR CR.99 GE 30 OR US.13 GE 30 OR CR.13 GE 30) THEN
            R.CPY<SCB.C.CBE.GE30.FLAG> = 'Y'
        END

        IF R.CPY<SCB.C.CBE.GE30.FLAG> EQ 'Y' THEN
            R.CPY<SCB.C.CBE.GE30.FLAG> = 'Y'
        END

        TOT.FLG = US.99 + CR.99 + US.13 + CR.13
        IF TOT.FLG EQ 0 THEN
            R.CPY<SCB.C.CBE.GE30.FLAG> = 'N'
        END

        CALL F.WRITE(FN.CPY,CBE.ID,R.CPY)

    END
    RETURN
END
