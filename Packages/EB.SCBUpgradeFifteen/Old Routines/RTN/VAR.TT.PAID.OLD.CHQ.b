* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
** ----- NESSREEN AHMED 10/4/2007 -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.PAID.OLD.CHQ
** TO WRITE ON TABLE THE PAID OLD CHEQUES NUMBERS
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION = 'A' AND  R.NEW(TT.TE.RECORD.STATUS)[1,3]='INA' THEN
        FN.P.CHQ = 'F.SCB.P.CHEQ' ; F.P.CHQ = '' ; R.P.CHQ = ''
        ETEXT = ''

        CHQ.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
*CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,AC.COMP)
        MYBRN  = AC.COMP[8,2]
*-- EDIT BY NESSMA 2016/08/02
        MYBRN = TRIM(MYBRN , "0" , "L")
*-- END EDIT
        OLD.CHQ = CHQ.NO:'.':MYBRN

        CALL OPF( FN.P.CHQ,F.P.CHQ)
        CALL F.READ( FN.P.CHQ,OLD.CHQ, R.P.CHQ, F.P.CHQ, ETEXT)
*****UPDATED BY NESSREEN AHMED 24/2/2010*************************
        IF ETEXT THEN
            R.P.CHQ<P.CHEQ.CHEQ.VAL> = R.NEW(TT.TE.NET.AMOUNT)
            R.P.CHQ<P.CHEQ.CHEQ.DAT> = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>
*******UPDATED BY NESSREEN AHMED 22/02/2009********************
**  R.P.CHQ<P.CHEQ.TRM.DAT> = R.NEW(TT.TE.VALUE.DATE.1)
            R.P.CHQ<P.CHEQ.TRN.DAT> = R.NEW(TT.TE.VALUE.DATE.1)
***************************************************************
            R.P.CHQ<P.CHEQ.ACCOUNT.NO> = ACCT.NO
            R.P.CHQ<P.CHEQ.OUR.REF> = ID.NEW
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
            COMP = C$ID.COMPANY
            R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP
*********************************************************************
            CALL F.WRITE(FN.P.CHQ,OLD.CHQ,R.P.CHQ)
        END ELSE
            E = '��� �� ��� ��� ����� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
****
    END
    RETURN
END
