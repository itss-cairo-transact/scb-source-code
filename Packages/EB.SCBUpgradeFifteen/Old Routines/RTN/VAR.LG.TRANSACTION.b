* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.TRANSACTION


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.TRANSACTION



    FN.LG = 'F.SCB.LG.TRANSACTION';R.LG='';F.LG='';EEE=''
    CALL F.READ(FN.LG,ID,R.LG,F.LG,EEE)

    LOCAL.REF = R.NEW(LD.LOCAL.REF)
    MYCODE=LOCAL.REF<1,LDLR.OPERATION.CODE>
    ID = ID.NEW:'.':TODAY
    R.LG<SCB.LGTR.CUSTOMER> = R.NEW(LD.CUSTOMER.ID)
    R.LG<SCB.LGTR.TYPE>              = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    R.LG<SCB.LGTR.OP.CODE>           = LOCAL.REF<1,LDLR.OPERATION.CODE>
    R.LG<SCB.LGTR.LIMIT.REF>         = R.NEW(LD.LIMIT.REFERENCE)
***************************************************************
*T.SEL ="SELECT FBNK.STMT.ENTRY WITH BOOKING.DATE EQ ":TODAY :" AND CURRENCY EQ ": R.NEW(LD.CURRENCY)
*CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*IF SELECTED THEN TEXT=KEY.LIST<1>;CALL REM
*FN.STMT ='FBNK.STMT.ENTRY' ;R.STMT='';F.STMT=''
*CALL OPF(FN.STMT,F.STMT)
*CALL F.READ(FN.STMT,KEY.LIST<1>, R.STMT, F.STMT,E)
*R.LG<SCB.LGTR.RATE.TODAY>        = R.STMT<AC.STE.EXCHANGE.RATE>

** MIDDLE RATE FROM CURRENCY TABLE **

    LD.CURR = R.NEW(LD.CURRENCY)
    IF LD.CURR EQ 'EGP' THEN
        R.LG<SCB.LGTR.RATE.TODAY> = '1'
    END ELSE
        IF LD.CURR NE 'EGP' THEN
            CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,LD.CURR,LD.RATE)
            R.LG<SCB.LGTR.RATE.TODAY> = LD.RATE
        END
    END
***************************************************************
    R.LG<SCB.LGTR.COMM.CURR>= R.NEW(LD.CURRENCY)

    LG.CHRG.AMT = R.NEW(LD.CHRG.AMOUNT)
*Line [ 83 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.CHRG.AMOUNT = DCOUNT(LG.CHRG.AMT,@SM)
    FOR I=1 TO DCOUNT.CHRG.AMOUNT
        R.LG<SCB.LGTR.COMM.AMOUNT,I> = R.NEW(LD.CHRG.AMOUNT)<1,I>
    NEXT I

    R.LG<SCB.LGTR.CURRENCY>          = R.NEW(LD.CURRENCY)
    R.LG<SCB.LGTR.AMOUNT>            = R.NEW(LD.AMOUNT)
    R.LG<SCB.LGTR.MARGIN.AMT>        = LOCAL.REF<1,LDLR.MARGIN.AMT>
    R.LG<SCB.LGTR.MARGIN.PERC>       = LOCAL.REF<1,LDLR.MARGIN.PERC>
    IF (MYCODE >= '1401' AND  MYCODE <= '1403') THEN
        R.LG<SCB.LGTR.MARGIN.ACCT>       = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    END ELSE
        R.LG<SCB.LGTR.MARGIN.ACCT>       = LOCAL.REF<1,LDLR.CREDIT.ACCT>
    END
    IF (MYCODE >= '1401' AND  MYCODE <= '1403') THEN
        R.LG<SCB.LGTR.DRADOWN.ACCT>      = LOCAL.REF<1,LDLR.CREDIT.ACCT>
    END ELSE
        R.LG<SCB.LGTR.DRADOWN.ACCT>      = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    END
    TAX.AMT = LOCAL.REF<1,LDLR.TAXES>
*Line [ 104 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.TAX.AMT = DCOUNT(TAX.AMT,@SM)
    FOR J=1 TO DCOUNT.TAX.AMT
        R.LG<SCB.LGTR.TAX.STAMP.AMT,J>    = LOCAL.REF<1,LDLR.TAXES,J>
        R.LG<SCB.LGTR.TAX.STAMP.CURR,J>   = LCCY
        CHARGE.ID = LOCAL.REF<1,LDLR.OPERATION.CODE>:".":LOCAL.REF<1,LDLR.PRODUCT.TYPE>
        CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.TAX.ACCOUNT,CHARGE.ID,INT.ACC)
        R.LG<SCB.LGTR.INTERNAL.ACCT,J>    = INT.ACC<1,J>
        R.LG<SCB.LGTR.DRADOWN.TAX.ACCT,J> = LOCAL.REF<1,LDLR.DEBIT.ACCT>
    NEXT J

    R.LG<SCB.LGTR.CHEQ.NO>           = LOCAL.REF<1,LDLR.CHEQUE.NO>
    R.LG<SCB.LGTR.CHEQ.AMT>          = LOCAL.REF<1,LDLR.CHQ.AMT.CUR>
    R.LG<SCB.LGTR.CHEQ.DATE>         = LOCAL.REF<1,LDLR.CHQ.DATE>

***************************************************************
*T.SEL1 = "SELECT FBNK.CATEG.ENTRY WITH TRANS.REFERENCE EQ ": ID.NEW
*KEY.LIST1="" ;SELECTED1="" ;ER.MSG1=""
*CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
*FN.CATEG ='FBNK.CATEG.ENTRY' ;R.CATEG='';F.CATEG=''
*CALL OPF(FN.CATEG,F.CATEG)
*CALL F.READ(FN.CATEG,KEY.LIST1, R.CATEG, F.CATEG,E)
*PL.CATEG = R.CATEG<AC.CAT.PL.CATEGORY>
*DCOUNT.PL.ACCT = DCOUNT(PL.CATEG,VM)
*FOR I=1 TO DCOUNT.PL.ACCT
*R.LG<SCB.LGTR.PROFIT.LOSS.ACCT,I>        = R.CATEG<AC.CAT.PL.CATEGORY,I>
*NEXT I

****LMM.CHARGE.CONDITIONS @ID----> CHARGE.CODE ====>> CATEGORY.CODE****
    ABEER = R.NEW(LD.CHRG.CODE)
*Line [ 134 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNT.PL.ACCT = DCOUNT(ABEER,@VM)
    FOR KK=1 TO DCOUNT.PL.ACCT
        CALL DBR('LMM.CHARGE.CONDITIONS':@FM:LD21.CATEGORY.CODE,ABEER<1,KK>,PL.ACCT)

        R.LG<SCB.LGTR.PROFIT.LOSS.ACCT,KK> = PL.ACCT
    NEXT KK

**********************CONFISCATION*****************************************

    CONFIS.AMT=LOCAL.REF<1,LDLR.CONFISC.AMT>
    R.LG<SCB.LGTR.CONFISC.AMT>=CONFIS.AMT

**********************AMEND************************************************



***************************************************************************
    CALL F.WRITE(FN.LG,ID,R.LG)
    TEXT='LG';CALL REM
    RETURN
END
