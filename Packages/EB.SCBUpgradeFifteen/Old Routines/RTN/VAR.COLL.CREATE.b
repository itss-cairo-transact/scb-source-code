* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>65</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.COLL.CREATE


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION EQ 'I' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
***** SCB R15 UPG 20160703 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
    END
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "COLLATERAL"
    OFS.OPTIONS="SCB.INP1"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
*******************************************************

*    TEXT = "ID" : NEW.FINAL.ID ; CALL REM
********DCOUNT
*Line [ 83 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    LD.NO =DCOUNT(R.NEW(COLL.NOTES),@VM)
*    TEXT = "NO" : LD.NO ; CALL REM
*******************************************************
    DDD = LD.NO -1
    FOR I = 1 TO DDD
        COMMA = ","

        TEMP.ID = ID.NEW
        NEW.ID.TMP = FIELD(TEMP.ID,'.',3)
        NEW.ID.TMP = NEW.ID.TMP + I
        NEW.FINAL.ID  = FIELD(TEMP.ID,'.',1):".":FIELD(TEMP.ID,'.',2):".":NEW.ID.TMP
*       TEXT = "ID" : NEW.FINAL.ID ; CALL REM

        OFS.MESSAGE.DATA =  "COLLATERAL.TYPE=":R.NEW(COLL.COLLATERAL.TYPE):COMMA
*TEXT = R.NEW(COLL.NOTES)<1,I> ; CALL REM
        OFS.MESSAGE.DATA :=  "APPLICATION.ID=":R.NEW(COLL.NOTES)<1,I>:COMMA
        OFS.MESSAGE.DATA := "COLLATERAL.CODE=":R.NEW(COLL.COLLATERAL.CODE):COMMA
        OFS.MESSAGE.DATA := "DESCRIPTION=":R.NEW(COLL.DESCRIPTION)
*        OFS.MESSAGE.DATA := "LOCAL.REF:5=":R.NEW(<COLL.LOCAL.REF><1,COLR.CUS.LIAB>)
        GOSUB CREATE.FILE
    NEXT I
    RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(LD.DATE.TIME)
    INPP = R.NEW(LD.INPUTTER)<1,1>


    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:NEW.FINAL.ID:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":INPP:RND(10000):"_":D.T ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

    RETURN
*--------------------------------------------------------
END
