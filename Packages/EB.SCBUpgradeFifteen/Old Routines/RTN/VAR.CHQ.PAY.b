* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>314</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CHQ.PAY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS


*    IF MESSAGE EQ "" THEN

*   CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,COMI,MYLOC)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''

    CALL OPF( FN.BR,F.BR)
    BR.ID = R.NEW(FT.BKCRAC.ORDERING.CUS)

    CALL F.READ( FN.BR, BR.ID , R.BR, F.BR, ETEXT)


    R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CHQ.PAY> = 'Y'

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
    CALL F.WRITE(FN.BR,BR.ID, R.BR)


*   END
    RETURN
END
