* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
********* ABEER 12/3/2003**************
*A Routine To Count No of Extension.
    SUBROUTINE VAR.LG.INW.EXTENSION.NO

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    MYCODE=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    IF  MYCODE='2231'OR MYCODE='2234' OR MYCODE='2235' THEN
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>='' THEN
            R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>='1'
        END ELSE
            COUNT.NO=R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>
            ADD.COUNT=COUNT.NO+1
            R.NEW(LD.LOCAL.REF)<1,LDLR.NO.OF.DAYS>=ADD.COUNT
        END
    END
    RETURN
END
