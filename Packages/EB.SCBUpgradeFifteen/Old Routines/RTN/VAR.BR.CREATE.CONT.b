* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BR.CREATE.CONT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*------------------------------------------
    CATEG.CR = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT>[11,4]
    CATEG.DR = "1":CATEG.CR
    CO.CODE  = ID.COMPANY[6,4]
    CURR     = R.NEW(EB.BILL.REG.CURRENCY)
    CR.ACCT  = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT>
    DR.ACCT  = CURR:CATEG.DR:"0001":CO.CODE
    AMT      = R.NEW(EB.BILL.REG.AMOUNT)
    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT,ACC.OFFICER)



    IF CURR NE 'EGP' THEN
        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    END
    IF V$FUNCTION = 'A' AND  R.NEW(EB.BILL.REG.RECORD.STATUS)='INAU' THEN
*--------
*   CR
*--------
        Y.ACCT = CR.ACCT

        IF CURR NE 'EGP' THEN
            LCY.AMT = AMT * RATE
            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
            FCY.AMT = AMT
        END ELSE
            LCY.AMT = AMT
            FCY.AMT = ''
        END
        CATEG  = CATEG.CR
        GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
        Y.ACCT = DR.ACCT
        IF CURR NE 'EGP' THEN
            LCY.AMT = (AMT * RATE ) * -1
            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
            FCY.AMT = AMT * -1
        END ELSE
            LCY.AMT = AMT * -1
            FCY.AMT = ''
        END
        CATEG  = CATEG.DR
        GOSUB AC.STMT.ENTRY
    END

**                   -------------------------
**                              REV
**                   -------------------------

    IF V$FUNCTION = 'A' AND  R.NEW(EB.BILL.REG.RECORD.STATUS)='RNAU' THEN

*--------
*  CR
*--------
        Y.ACCT = DR.ACCT

        IF CURR NE 'EGP' THEN
            LCY.AMT = AMT * RATE
            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
            FCY.AMT = AMT
        END ELSE
            LCY.AMT = AMT
            FCY.AMT = ''
        END
        CATEG  = CATEG.CR
        GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
        Y.ACCT = CR.ACCT
        IF CURR NE 'EGP' THEN
            LCY.AMT = (AMT * RATE ) * -1
            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
            FCY.AMT = AMT * -1
        END ELSE
            LCY.AMT = AMT * -1
            FCY.AMT = ''
        END
        CATEG  = CATEG.DR
        GOSUB AC.STMT.ENTRY
    END

    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '790'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
***TEXT = "END2" ; CALL REM
    RETURN
END
