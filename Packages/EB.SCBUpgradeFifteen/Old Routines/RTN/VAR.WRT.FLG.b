* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.WRT.FLG

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.CURRENCY
    $INSERT        I_F.SCB.BT.BATCH
    $INSERT        I_BR.LOCAL.REFS
    $INSERT        I_F.SCCB.BT.PARTIAL
*----------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='INAU' THEN
        FN.TMP = "F.SCCB.BT.PARTIAL" ; F.TMP = ""
        CALL OPF(FN.TMP , F.TMP)
        TMP.ID = ID.COMPANY:".":TODAY
        CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
        R.TMP<SCCB.BT.FLG.AUTH> = "YES"

*WRITE  R.TMP TO F.TMP , TMP.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":TMP.ID:"TO" :FN.TMP
*END
        CALL F.WRITE (FN.TMP,TMP.ID,R.TMP)
    END
*----------------------------------------------
    RETURN
END
