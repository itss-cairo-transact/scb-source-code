* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>47</Rating>
*-----------------------------------------------------------------------------
*******    SUBROUTINE VAR.TT.OFS(ACCT.NO,NO.ISSUED,CHQ.START)
    SUBROUTINE VAR.TT.OFS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    GOSUB CREATE.FILE
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E

    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "CHEQUE.ISSUE"
    OFS.OPTIONS = "SCB"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = 'SCB':'.':ACCT.NO
*  TEXT=OFS.TRANS.ID:'TRANS.ID';CALL REM
    OFS.MESSAGE.DATA = ""
**************************************
**********To Issue A Cheq Record In Cheque.Issue*********************
    ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
    CHQ='SCB':'.':ACCT.NO:'...'

    T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ETEXT=''

    IF SELECTED EQ '0' THEN
        NO.ISSUED = '1'
        CHQ.START = '1'
    END ELSE
        CALL DBR('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST<SELECTED>,CHECK.ST.NO)
        CALL DBR('CHEQUE.ISSUE':@FM:CHEQUE.IS.NUMBER.ISSUED,KEY.LIST<SELECTED>,NO.ISSUED)

        NEW.CHQ.STRT.NO = CHECK.ST.NO + NO.ISSUED
        NO.ISSUED = '1'
        CHQ.START = NEW.CHQ.STRT.NO
    END
**************************************

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","

    OFS.MESSAGE.DATA = "CHEQUE.STATUS=":'90':COMMA
    OFS.MESSAGE.DATA := "ISSUE.DATE=":TODAY:COMMA
    OFS.MESSAGE.DATA := "NUMBER.ISSUED=":NO.ISSUED:COMMA
*   OFS.MESSAGE.DATA := "CHARGES=":'0.00':COMMA
*   OFS.MESSAGE.DATA := "CHARGE.DATE=":'':COMMA
    OFS.MESSAGE.DATA := "CHQ.NO.START=":CHQ.START:COMMA
    OFS.MESSAGE.DATA := "WAIVE.CHARGES=":'YES'

    RETURN
*-------------------------------------------------------
CREATE.FILE:
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    IDENT="T":TNO:".":OPERATOR:"_TT":RND(10000)
    WRITE OFS.REC ON F.OFS.IN,IDENT ON ERROR TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160703 - S
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

    RETURN
*--------------------------------------------------------
END
