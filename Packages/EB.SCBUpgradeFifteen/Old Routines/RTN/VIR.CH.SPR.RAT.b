* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.CH.SPR.RAT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION

    FN.LD= 'F.LD.TXN.TYPE.CONDITION' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    CALL F.READ(FN.LD,R.NEW(LD.CATEGORY),R.LD,F.LD,E1)
    CUR = R.LD<LTTC.CURRENCY>
    LOCATE R.NEW(LD.CURRENCY) IN CUR<1,1> SETTING POS THEN
        IF R.NEW(LD.INTEREST.SPREAD) # R.LD<LTTC.INTEREST.SPREAD,POS> THEN
*TEXT = 'SPREAD.NOT.EQ.DEFULT'
            TEXT = '���� ������ ������� �������'
            CALL STORE.OVERRIDE(R.NEW(LD.CURR.NO)+1)
            AF = LD.INTEREST.SPREAD ; AV = 1 ; AS = 1
        END
    END
    CALL REBUILD.SCREEN

    RETURN
END
