* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CHQ.RTN.TRANS.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

****-----------------------------------------------------------------***

*TEXT = "MESSAGE = " : MESSAGE ; CALL REM

    DR.ACCT      = R.NEW(CHQ.RET.DEBIT.ACCT)
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DR.ACCT,CURR)
    BRANCH.NO    = ID.COMPANY[8,2]
    ACC.OFFICER  = BRANCH.NO

    IF R.NEW(CHQ.RET.DEBIT.ACCT) # '' THEN
        IF CURR EQ 'EGP' THEN
            LCY.AMTT = 20
            CALL EB.ROUND.AMOUNT ('EGP',LCY.AMTT,'',"2")

            FCY.AMTT = ''
        END
        IF CURR EQ 'USD' THEN
            FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
            CALL OPF(FN.CUR,F.CUR)
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

            LCY.AMTT = 5 * RATE
            CALL EB.ROUND.AMOUNT ('USD',LCY.AMTT,'',"2")
            FCY.AMTT = 5
        END

        BEGIN CASE
*****************************************HYTH(((((((((((((((((((((((((
* CASE (V$FUNCTION = 'I')
        CASE R.NEW(CHQ.RET.RECORD.STATUS) NE 'RNAU'
*****************************************HYTH(((((((((((((((((((((((((
*TEXT = 'INPUT' ; CALL REM
            TXN.TYPE = 'DR'
            TXN.CODE = '874'

            LCY.AMT  = LCY.AMTT * -1
*TEXT = 'LCY='  :LCY.AMT ; CALL REM
            FCY.AMT  = FCY.AMTT * -1

            IF FCY.AMT EQ 0 THEN
                FCY.AMT = ''
            END
            Y.ACCT   = DR.ACCT
            PL.ACCT  = ''

            GOSUB AC.STMT.ENTRY
*TEXT = 'AFTER' ; CALL REM
*TEXT = "FUN= " : V$FUNCTION ;  CALL REM
*TEXT = "STA= "  : R.NEW(CHQ.RET.RECORD.STATUS) ; CALL REM

* CASE ((V$FUNCTION = 'A') AND (R.NEW(CHQ.RET.RECORD.STATUS)[1,3]='INA' ))
*****UPDATED BY NESSREEN AHMED 29/5/2011******************************************
***** IF (  V$FUNCTION = 'A' AND R.NEW(CHQ.RET.RECORD.STATUS)='INAU' ) THEN
            IF (  V$FUNCTION = 'A' AND R.NEW(CHQ.RET.RECORD.STATUS)[1,3]='INA' ) THEN
*****END OF UPDATE 29/5/2011******************************************************
                LCY.AMT  = LCY.AMTT
                FCY.AMT  = FCY.AMTT
                TXN.TYPE = 'AD'
                TXN.CODE = '874'
                IF FCY.AMT EQ 0 THEN
                    FCY.AMT = ''
                END
                Y.ACCT   = ''
                PL.ACCT  = '52109'

                CATEG    = ''
*TEXT = "CATEG.AUTH " ; CALL REM

*TEXT = "H " ; CALL REM
                GOSUB AC.STMT.ENTRY.CATEG
            END
        CASE ( V$FUNCTION = 'A' AND R.NEW(CHQ.RET.RECORD.STATUS)='RNAU' )
** -------------------------
**        REV
** -------------------------
*-- SWAPED THE ACCOUNTS CR,DR --------------
            CR.ACCT  = R.NEW(CHQ.RET.DEBIT.ACCT)
            TXN.TYPE = 'AD'
            TXN.CODE = '874'
            IF FCY.AMT EQ 0 THEN
                FCY.AMT = ''
            END
            LCY.AMT  = LCY.AMTT
            FCY.AMT  = FCY.AMTT
            Y.ACCT   = CR.ACCT
            PL.ACCT  = ''
            GOSUB AC.STMT.ENTRY.CATEG
            Y.ACCT   = ''
*TEXT = "REV" ; CALL REM
            TXN.TYPE = 'DR'
            TXN.CODE = '874'

            LCY.AMT  = LCY.AMTT * -1
            FCY.AMT  = FCY.AMTT * -1

            IF FCY.AMT EQ 0 THEN
                FCY.AMT = ''
            END

            PL.ACCT  = '52109'
            GOSUB AC.STMT.ENTRY.CATEG

        END CASE
    END
    RETURN

AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY         = ""
    MULTI.ENTRIES = ""
*
*TEXT = 'STMT' ;CALL REM
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TXN.CODE
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.ACCT
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(CHQ.RET.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(CHQ.RET.STMT.NO)

    MULTI.ENTRIES<-1> = LOWER(ENTRY)

    IF ( V$FUNCTION EQ 'I'  AND R.NEW(CHQ.RET.CURR.NO) EQ '' ) THEN
        TYPE = 'VAL'
        MESSAGE = ''
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

        IF END.ERROR OR TEXT = "NO" THEN RETURN
*TEXT  =  "VAL" ; CALL REM

    END

*****UPDATED BY NESSREEN AHMED 2/5/2011**************************
*****IF V$FUNCTION EQ 'A' THEN
    IF V$FUNCTION = 'A' AND  R.NEW(CHQ.RET.RECORD.STATUS)[1,3]='INA' THEN
        TYPE = 'AUT'
        CALL EB.ACCOUNTING("SYS",TYPE,"","")

        IF END.ERROR OR TEXT = "NO" THEN RETURN

*TEXT  =  "AUT" ; CALL REM
    END
****END OF UPDATE 2/5/2011*************************
    IF V$FUNCTION EQ 'D' THEN

        TYPE = 'DEL'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
        IF END.ERROR OR TEXT = "NO" THEN RETURN
*TEXT = "DEL" ; CALL REM
    END

    RETURN

**--------------------------------------------------------------**
*--------------------------------
AC.STMT.ENTRY.CATEG:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY.C         = ""
    MULTI.ENTRIES.C = ""
*
    ENTRY.C<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY.C<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY.C<AC.STE.TRANSACTION.CODE> = TXN.CODE
    ENTRY.C<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY.C<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY.C<AC.STE.NARRATIVE>        = ""
    ENTRY.C<AC.STE.PL.CATEGORY>      = PL.ACCT
    ENTRY.C<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY.C<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY.C<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY.C<AC.STE.VALUE.DATE>       = TODAY
    ENTRY.C<AC.STE.CURRENCY>         = CURR
    ENTRY.C<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY.C<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY.C<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY.C<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY.C<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY.C<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY.C<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY.C<AC.STE.CRF.TYPE>         = ""
    ENTRY.C<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY.C<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY.C<AC.STE.CHQ.TYPE>         = ""
    ENTRY.C<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY.C<AC.STE.CUSTOMER.ID>      = ""
    ENTRY.C<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY.C<AC.STE.OVERRIDE>         = R.NEW(CHQ.RET.OVERRIDE)
    ENTRY.C<AC.STE.STMT.NO>          = R.NEW(CHQ.RET.STMT.NO)

    MULTI.ENTRIES.C<-1> = LOWER(ENTRY.C)
****UPDATED BY NESSREEN AHMED 02/05/2011*********
**** IF V$FUNCTION EQ 'A' THEN
*****************************************HYTH(((((((((((((((((((((((((
*IF V$FUNCTION EQ 'A' AND  R.NEW(CHQ.RET.RECORD.STATUS)[1,3]='INA' THEN

    IF V$FUNCTION EQ 'A' THEN
*****************************************HYTH(((((((((((((((((((((((((
        TYPE = 'SAO'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES.C,"")

        IF END.ERROR OR TEXT = "NO" THEN RETURN
*TEXT  =  "CATEG==    " : ; CALL REM
    END

    RETURN
**--------------------------------------------------------------**

END
