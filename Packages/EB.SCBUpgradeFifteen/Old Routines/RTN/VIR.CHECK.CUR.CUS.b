* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.CHECK.CUR.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC
*-------------------------------------------
    TEMP.ID = ID.NEW

    CUS.ID  = FIELD(TEMP.ID,'-',1)
    ACC.ID  = FIELD(TEMP.ID,'-',1)

    CALL DBR ('CUSTOMER':@FM:EB.CUS.NAME.1,CUS.ID,NAME.CU)
    IF NOT(NAME.CU) THEN
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,R.NEW(AC.LO.ACCOUNT.NO),CUS.ID)
    END
    AC.ID = R.NEW(AC.LO.ACCOUNT.NO)
*   TEXT = "AC= " : AC.ID ; CALL REM
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.ID,CU.NO)

    TEXT = CUS.ID :"*": CU.NO ; CALL REM

    IF CUS.ID NE CU.NO THEN
        TEXT = 'INVALID ACCOUNT' ; CALL REM
        CALL STORE.END.ERROR
    END

*--- CHECK CURRENCY
    CURR = R.NEW(AC.LO.ACCOUNT.NO)[9,2]
    CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(AC.LO.CURRENCY),CURR2)
    TEXT = CURR :"***": CURR2 ; CALL REM

    IF CURR NE CURR2 THEN
        TEXT = 'INVALID CURRENCY' ; CALL REM
        CALL STORE.END.ERROR
    END
*---------------------------------------------------
    RETURN
END
