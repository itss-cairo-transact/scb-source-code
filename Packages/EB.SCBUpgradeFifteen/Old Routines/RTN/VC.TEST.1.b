* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1497</Rating>
*-----------------------------------------------------------------------------
    PROGRAM VC.TEST.1
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.CONSOL.SPEC.ENTRY

*******************************UPDATED BY RIHAM R15**********************
*    OPEN "FBNK.STMT.ENTRY" TO F.SE ELSE NULL
*    OPEN "FBNK.CATEG.ENTRY" TO F.CE ELSE NULL
*    OPEN "FBNK.RE.CONSOL.SPEC.ENTRY" TO F.RE ELSE NULL
    CALL OPF("FBNK.STMT.ENTRY",F.SE)
    CALL OPF("FBNK.CATEG.ENTRY",F.CE)
    CALL OPF("FBNK.RE.CONSOL.SPEC.ENTRY",F.RE)
****************************************************************
*Line [ 35 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPENSEQ "STMT.ENTRY" TO SE ELSE NULL
*Line [ 37 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPENSEQ "CATEG.ENTRY" TO CE ELSE NULL
*Line [ 39 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPENSEQ "CONSOL.ENTRY" TO RE ELSE NULL

    SELECT F.SE

    LOOP
        READNEXT SE.ID ELSE SE.ID = ""
    WHILE SE.ID

*UPDATED BY NOHA HAMED 21/3/2016
*READ R.SE FROM F.SE,SE.ID ELSE NULL
        CALL F.READ ('FBNK.STMT.ENTRY', SE.ID, R.SE, F.SE, FILE.ERR)
        PL = SE.ID
        PL := ",":R.SE<AC.STE.ACCOUNT.NUMBER>
        PL := ",":R.SE<AC.STE.COMPANY.CODE>
        PL := ",":R.SE<AC.STE.CUSTOMER.ID>
        PL := ",":R.SE<AC.STE.VALUE.DATE>
        PL := ",":R.SE<AC.STE.CURRENCY>
        PL := ",":R.SE<AC.STE.AMOUNT.LCY>
        PL := ",":R.SE<AC.STE.AMOUNT.FCY>
        PL := ",":R.SE<AC.STE.TRANSACTION.CODE>
        IF R.SE<AC.STE.OUR.REFERENCE> <> "" THEN
            PL := ",":R.SE<AC.STE.OUR.REFERENCE>
        END ELSE
            PL := ",":R.SE<AC.STE.TRANS.REFERENCE>
        END
        PL := ",":R.SE<AC.STE.BOOKING.DATE>
        PL := ",":R.SE<AC.STE.CONSOL.KEY>
*        PRINT PL
*Line [ 68 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        WRITESEQ PL TO SE ELSE NULL
    REPEAT

    SELECT F.CE

    LOOP
        READNEXT CE.ID ELSE CE.ID = ""
    WHILE CE.ID
**        READ R.CE FROM F.CE,CE.ID ELSE NULL
CALL F.READ("FBNK.CATEG.ENTRY",CE.ID, R.CE,F.CE,ERR.CE)
        PL = CE.ID
        PL := ",":R.CE<AC.CAT.ACCOUNT.NUMBER>
        PL := ",":R.CE<AC.CAT.COMPANY.CODE>
        PL := ",":R.CE<AC.CAT.CUSTOMER.ID>
        PL := ",":R.CE<AC.CAT.VALUE.DATE>
        PL := ",":R.CE<AC.CAT.CURRENCY>
        PL := ",":R.CE<AC.CAT.AMOUNT.LCY>
        PL := ",":R.CE<AC.CAT.AMOUNT.FCY>
        PL := ",":R.CE<AC.CAT.TRANSACTION.CODE>
        IF R.CE<AC.CAT.OUR.REFERENCE> <> "" THEN
            PL := ",":R.CE<AC.CAT.OUR.REFERENCE>
        END ELSE
            PL := ",":R.CE<AC.CAT.TRANS.REFERENCE>
        END
        PL := ",":R.CE<AC.CAT.BOOKING.DATE>
        PL := ",":R.CE<AC.CAT.CONSOL.KEY>
*        PRINT PL
*Line [ 96 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        WRITESEQ PL TO CE ELSE NULL
    REPEAT

    SELECT F.RE

    LOOP
        READNEXT RE.ID ELSE RE.ID = ""
    WHILE RE.ID
**        READ R.RE FROM F.RE,RE.ID ELSE NULL
CALL F.READ("FBNK.RE.CONSOL.SPEC.ENTRY",RE.ID, R.RE,F.RE,ERR.RE)
        PL = RE.ID
        PL := ",":R.RE<RE.CSE.DEAL.NUMBER>
        PL := ",":R.RE<RE.CSE.COMPANY.CODE>
        PL := ",":R.RE<RE.CSE.CUSTOMER.ID>
        PL := ",":R.RE<RE.CSE.VALUE.DATE>
        PL := ",":R.RE<RE.CSE.CURRENCY>
        PL := ",":R.RE<RE.CSE.AMOUNT.LCY>
        PL := ",":R.RE<RE.CSE.AMOUNT.FCY>
        PL := ",":R.RE<RE.CSE.TRANSACTION.CODE>
        IF R.RE<RE.CSE.OUR.REFERENCE> <> "" THEN
            PL := ",":R.RE<RE.CSE.OUR.REFERENCE>
        END ELSE
            PL := ",":R.RE<RE.CSE.TRANS.REFERENCE>
        END
        PL := ",":R.RE<RE.CSE.BOOKING.DATE>
        PL := ",":R.RE<RE.CSE.CONSOL.KEY.TYPE>
*        PRINT PL
*Line [ 124 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        WRITESEQ PL TO RE ELSE NULL
    REPEAT

    STOP
END
