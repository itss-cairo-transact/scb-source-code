* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>404</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.EGP.ENTRY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION = 'A' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD

*------------------------------
INITIALISE:
        FN.OFS.SOURCE ="F.OFS.SOURCE"
        F.OFS.SOURCE = ""

        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*        CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
        FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
        F.OFS.IN         = 0
        F.OFS.BK         = 0
        OFS.REC          = ""
        OFS.OPERATION    = "FUNDS.TRANSFER"
        OFS.OPTIONS      = "SCB.AC.FCY.1"
***OFS.USER.INFO    = "/"

*************HYTHAM********20090318**********
        COMP = C$ID.COMPANY
        COM.CODE = COMP[8,2]
        OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

        OFS.TRANS.ID     = ""
        OFS.MESSAGE.DATA = ""

        RETURN
*----------------------------------------------------
BUILD.RECORD:
        COMMA = ","
        CR.CUR = R.NEW(FT.CREDIT.CURRENCY)
        DR.CUR = R.NEW(FT.DEBIT.CURRENCY)
******UPDATED IN 18/6/2008 BY NESSREEN ********
** AC.NO = "115000001"
        ACN = "115000099"
        CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        AC.NO = ACN:CO.CODE
*************************************************
**************************************************CRAETE FT BY OFS**********************************************
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":COMMA

        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":R.NEW(FT.CREDIT.CURRENCY):COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":R.NEW(FT.DEBIT.CURRENCY):COMMA

        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":CR.CUR:AC.NO:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":DR.CUR:AC.NO:COMMA

        OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":R.NEW(FT.CREDIT.AMOUNT):COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT=":R.NEW(FT.DEBIT.AMOUNT):COMMA

        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":R.NEW(FT.DEBIT.VALUE.DATE):COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":R.NEW(FT.CREDIT.VALUE.DATE):COMMA

        OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ID.NEW:COMMA

        F.PATH = FN.OFS.IN
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
        OFS.ID = "T":TNO:".":ID.NEW

        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

*TEXT = 'OK' ; CALL REM
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    END
    RETURN
************************************************************
END
