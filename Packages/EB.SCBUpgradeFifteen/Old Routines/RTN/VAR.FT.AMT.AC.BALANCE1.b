* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.AMT.AC.BALANCE1

*1-IF THE INPUT VALUE IS NOT EQ DEBIT AMOUNT THEN MAKE CREDIT.AMOUNT IS EMPTY
*2-CHECK IF WORKING.BALANCE HAVE A CREDIT BALANCE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    CU.NO = R.NEW(FT.DEBIT.CUSTOMER)
    CATEG = R.NEW(FT.DEBIT.ACCT.NO)[11,4]

    AC = R.NEW(FT.DEBIT.ACCT.NO)
*    IF CATEG EQ 1201 OR CATEG EQ 1202 THEN
    IF CATEG GE 1101 AND  CATEG LE 1599  THEN
        ***IF CATEG NE 1220 THEN
            CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,AC,LIM.REF)
            LIM = CU.NO :".":"0000":LIM.REF
* TEXT= "LIM":LIM ; CALL REM
            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
* TEXT= "AVL":AVL ; CALL REM
*       CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,AC,ONLINE.BALANCE)

            AMT = R.NEW(FT.AMOUNT.DEBITED)[4,LEN(R.NEW(FT.AMOUNT.DEBITED))]
* TEXT = "ONLINE.BALANCE" : ONLINE.BALANCE ; CALL REM
            ZERO = "0.00"
            BAL =  ONLINE.BALANCE + AVL
*   BALL =  BAL  + AMT
* TEXT = "BAL" : BAL ; CALL REM
*  TEXT = "BALL" : BALL ; CALL REM
* IF R.NEW(FT.AMOUNT.DEBITED)[4,LEN(R.NEW(FT.AMOUNT.DEBITED))] GT AVL THEN
            IF BAL LT ZERO THEN
          ***      E = 'AMOUNT IS NOT AVILABLE' ; CALL ERR; MESSAGE='REPEAT'
*     CALL FATAL.ERROR("AMOUNT IS NOT AVILABLE")
            END
        END ELSE
*TEXT = 'AMOUNT.DEBITED=':R.NEW(FT.AMOUNT.DEBITED) ; CALL REM
* END
            IF R.NEW(FT.AMOUNT.DEBITED) THEN
                DEBIT.ACC = R.NEW(FT.DEBIT.ACCT.NO)
                CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,DEBIT.ACC,WORK.BALANCE)
*TEXT ="FT.AMOUNT=":R.NEW(FT.AMOUNT.DEBITED)[4,LEN(R.NEW(FT.AMOUNT.DEBITED))] ;CALL REM
*TEXT ="WORK.BALANCE=":WORK.BALANCE ;CALL REM

                IF R.NEW(FT.AMOUNT.DEBITED)[4,LEN(R.NEW(FT.AMOUNT.DEBITED))] GT WORK.BALANCE THEN
* TEXT ="FT.AMOUNT=":R.NEW(FT.AMOUNT.DEBITED) ;CALL REM
*TEXT ="WORK.BALANCE=":WORK.BALANCE ;CALL REM
*              TEXT = 'AMOUNT IS NOT AVILABLE' ; CALL REM
*   CALL FATAL.ERROR("AMOUNT IS NOT AVILABLE")
* AF= FT.AMOUNT.DEBITED ; AV=1
                END
            END
        END
        RETURN
    END
