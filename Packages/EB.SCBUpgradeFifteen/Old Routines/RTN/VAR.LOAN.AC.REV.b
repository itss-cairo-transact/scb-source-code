* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>390</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LOAN.AC.REV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC

    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='RNA' THEN

        ETEXT   =''
        FN.LO   = 'F.SCB.LOAN.AC'    ; F.LO   = '' ; R.LO  = ''
        CALL OPF(FN.LO,F.LO)
        KEY.LIST= "" ; SELECTED = "" ; ER.MSG = ""

        REF       = R.NEW(FT.DEBIT.THEIR.REF)
        ACCT.FT   = R.NEW(FT.CREDIT.ACCT.NO):"-" :REF
        ACCT.TF   = R.NEW(FT.CREDIT.ACCT.NO)[1,8]:"-" :REF
        ACCT.TT   = R.NEW(FT.CREDIT.ACCT.NO):"-" :REF

        IF LEN(R.NEW(FT.CREDIT.CUSTOMER)) EQ 7 THEN
            ACCT.TO   = "...":R.NEW(FT.CREDIT.ACCT.NO)[2,9] : "..." : REF
        END
        IF LEN(R.NEW(FT.CREDIT.CUSTOMER)) EQ 8 THEN
            ACCT.TO   = "...":R.NEW(FT.CREDIT.ACCT.NO)[1,10] : "..." : REF
        END

        BEGIN CASE
        CASE  REF[1,2] EQ "TO"
            T.SEL = "SELECT F.SCB.LOAN.AC WITH @ID LIKE  ": ACCT.TO
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            CALL F.READ(FN.LO,KEY.LIST,R.LO,F.LO,READ.ERR)
            ID.LOAN = KEY.LIST
        CASE  REF[1,2] EQ "TF"
            CALL F.READ(FN.LO,ACCT.TF,R.LO,F.LO,READ.ERR)
            ID.LOAN = ACCT.TF
        CASE  REF[1,2] EQ "FT"
            CALL F.READ(FN.LO,ACCT.FT,R.LO,F.LO,READ.ERR)
            ID.LOAN = ACCT.FT
**************ADDED ON 30-7-2013
        CASE  REF[1,2] EQ "TT"
            CALL F.READ(FN.LO,ACCT.TT,R.LO,F.LO,READ.ERR)
            ID.LOAN = ACCT.FT
*********************************
        CASE OTHERWISE
            RETURN
        END CASE

        IF NOT(READ.ERR) THEN
*Line [ 73 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            FIND ID.NEW IN R.LO SETTING POS1,POS2,POS3 ELSE NULL
            IF POS1 THEN

                AMT    =  R.LO<AC.LO.OUTSTAND.AMOUNT> + R.NEW(FT.CREDIT.AMOUNT)

                IF AMT GT '0'  THEN
                    R.LO<AC.LO.STATUS>        = 'O'
                END

                R.LO<AC.LO.PAY.DATE,POS2>     = ""
                R.LO<AC.LO.FT.REF,POS2>       = ""
                R.LO< AC.LO.AMOUNT.PAID,POS2> = ""
                R.LO<AC.LO.OUTSTAND.AMOUNT>   = AMT

                *WRITE  R.LO TO F.LO , ID.LOAN ON ERROR
                 *   PRINT "CAN NOT WRITE RECORD":KEY.LIST :"TO" :F.LO
                *END
CALL F.WRITE (FN.LO, ID.LOAN,R.LO)
            END
        END
    END
    RETURN
END
