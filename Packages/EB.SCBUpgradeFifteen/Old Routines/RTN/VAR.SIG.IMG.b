* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SIG.IMG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

      IF R.NEW(IM.DOC.IMAGE) # '' THEN

            CALL DBR ("CUSTOMER":@FM:EB.CUS.LOCAL.REF,R.NEW(IM.DOC.IMAGE.REFERENCE),LOCAL.RF)
            ID = R.NEW(IM.DOC.IMAGE.REFERENCE)
            IF LOCAL.RF<1,15> EQ "NO-��" THEN
                F.COUNT = '' ; FN.COUNT = 'FBNK.CUSTOMER'
                CALL OPF(FN.COUNT,F.COUNT)
                R.COUNT = ''
                CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
                IF NOT(E) THEN
                    R.COUNT<40,15> = "YES-���"
                    CALL F.WRITE(FN.COUNT,ID,R.COUNT)
*                    CALL JOURNAL.UPDATE(ID)
                END
            END 
      END
RETURN
END
