* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1141</Rating>
*-----------------------------------------------------------------------------
*MODIFICATION DONE LINE 96 AND 141 BY ADDING INAO AND RNAO STATUS TO THE IF CONDITION
*AND ACTIVATED ON LIVE 11-3-2014(R10)
    SUBROUTINE VAR.PROV.AMT.DP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON

*************************************************
*DEBUG
    FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
    CALL OPF(FN.LCC,F.LCC)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
    ID.LC = ID.NEW[1,12]
    CALL F.READ(FN.LCC,ID.LC,R.LCC,F.LCC,READ.ERR)

*   CR.ACCT  = LC.REC(TF.LC.CREDIT.PROVIS.ACC)
    CR.ACCT  = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,CR.ACCT,CR.CURR)

****DR.ACCT  = R.LCC<TF.LC.LOCAL.REF><1,LCLR.TEMP.PROV.ACC>
    DR.ACCT  = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC>
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DR.ACCT,DR.CURR)

****HASHED ON 11-5-20011   CATEG.CR = LC.REC(TF.LC.CREDIT.PROVIS.ACC)[11,4]
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CR.ACCT,CATEG.CR)
    CALL DBR('DRAWINGS':@FM:TF.DR.DRAWING.TYPE,ID.NEW,DRAW.TYPE)

    DRW.TYPE= R.NEW(TF.DR.DRAWING.TYPE)
    DRW.PER = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>
    DRW.AMT =R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT>
    ACC.P  =    R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.P,CATEG.CR)

*    IF CATEG.CR EQ '3013' AND DRAW.TYPE EQ '' AND (DRW.PER NE '0' OR DRW.AMT NE '0') THEN
**    IF ( CATEG.CR EQ '3013' AND DRAW.TYPE EQ 'DP' ) THEN
    IF  DRW.TYPE EQ 'DP'   THEN
        TEXT='EB ACC';CALL REM
*************************************
**        CATEG.DR = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.TEMP.PROV.ACC>[11,4]
        CATEG.ACC.DR = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.TEMP.PROV.ACC>
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CATEG.ACC.DR,CATEG.DR)
        TEXT= CATEG.DR:'CATEG.DR':LC.REC(TF.LC.LOCAL.REF)<1,LCLR.TEMP.PROV.ACC>;CALL REM
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DR.ACCT,CURR)
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,CR.ACCT,CUR.CR)

        CO.CODE  = C$ID.COMPANY[6,4]

        FULL.UT = R.NEW(TF.DR.FULLY.UTILISED)
        IF FULL.UT EQ 'NO' THEN

            IF R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>  NE '' THEN
                AMT      = ( R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT> * R.NEW(TF.DR.DOCUMENT.AMOUNT) ) /100
            END ELSE
                IF R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT> NE '' THEN
                    AMT      = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT>
                END
            END

            IF (R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRAEING.PERCENT>  EQ '' AND R.NEW(TF.DR.LOCAL.REF)<1,DRLR.DRW.PROV.AMT> EQ '') THEN
                AMT  = ( R.LCC<TF.LC.PROVIS.PERCENT> * R.NEW(TF.DR.DOCUMENT.AMOUNT) ) /100
                TEXT=AMT:'AMT';CALL REM
            END
        END ELSE
            IF FULL.UT EQ 'Y' THEN
                IF R.NEW(TF.DR.LOCAL.REF)<1,DRLR.PROV.AMT.FULLY> NE '' THEN
** AMT = R.LCC<TF.LC.PRO.OUT.AMOUNT>
                    AMT=R.NEW(TF.DR.LOCAL.REF)<1,DRLR.PROV.AMT.FULLY>

                END
            END
        END
********************
*CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT,ACC.OFFICER)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,CR.ACCT,AC.COMP)
        ACC.OFFICER  = AC.COMP[8,2]

        IF CURR NE 'EGP' THEN
            FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
            CALL OPF(FN.CUR,F.CUR)
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        END
        IF CUR.CR NE 'EGP' THEN
            FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
            CALL OPF(FN.CUR,F.CUR)
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        END
        IF V$FUNCTION = 'A' THEN
            IF R.NEW(TF.DR.RECORD.STATUS) = 'INAU' OR R.NEW(TF.DR.RECORD.STATUS) = 'CNAU' OR R.NEW(TF.DR.RECORD.STATUS) = 'INAO' THEN
                IF (( CATEG.CR EQ '3013' AND DRAW.TYPE EQ '' )  AND ( DRW.PER NE '0' OR DRW.AMT NE '0' ) ) THEN
                    TEXT='TEST EB ';CALL REM
*--------
*   CR
*--------
                    Y.ACCT = CR.ACCT
                    CURR=CUR.CR
                    IF CUR.CR NE 'EGP' THEN
                        LCY.AMT = AMT * RATE
                        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                        FCY.AMT = AMT
                        TEXT=LCY.AMT:'---':FCY.AMT;CALL REM
                    END ELSE
                        LCY.AMT = AMT
                        FCY.AMT = ''
                        TEXT=LCY.AMT:'-LF':FCY.AMT;CALL REM
                    END
                    CATEG  = CATEG.CR
                    GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
                    Y.ACCT = DR.ACCT
                    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,DR.ACCT,CURR)
                    IF CURR NE 'EGP' THEN
                        LCY.AMT = (AMT * RATE ) * -1
                        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                        FCY.AMT = AMT * -1
                    END ELSE
                        LCY.AMT = AMT * -1
                        FCY.AMT = ''
                    END
                    CATEG  = CATEG.DR
                    GOSUB AC.STMT.ENTRY
                END
            END
        END

**                   -------------------------
**                              REV
**                   -------------------------

        IF V$FUNCTION = 'A' THEN
            IF  R.NEW(TF.DR.RECORD.STATUS)='RNAU' OR R.NEW(TF.DR.RECORD.STATUS)='RNAO' THEN
                IF ( (CATEG.CR EQ '3013'  ) AND (DRW.PER NE '0' OR DRW.AMT NE '0')) THEN
*--------
*  CR
*--------
***   Y.ACCT = R.LCC<TF.LC.CREDIT.PROVIS.ACC>
                    Y.ACCT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.LC.PROV.ACC>
                    TEXT = "AC1=  " : Y.ACCT ; CALL REM
                    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,Y.ACCT,CURR)
                    IF CURR NE 'EGP' THEN
                        LCY.AMT = AMT * RATE
                        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                        FCY.AMT = AMT
                    END ELSE
                        LCY.AMT = AMT
                        FCY.AMT = ''
                    END
                    CATEG  = CATEG.CR
                    GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
                    Y.ACCT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NEW.PROV.ACC>
                    TEXT = "AC2=  " : Y.ACCT ; CALL REM
                    CURR=CUR.CR
                    IF CUR.CR NE 'EGP' THEN
                        LCY.AMT = (AMT * RATE ) * -1
                        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                        FCY.AMT = AMT * -1
                    END ELSE
                        LCY.AMT = AMT * -1
                        FCY.AMT = ''
                    END
                    CATEG  = CATEG.DR
                    GOSUB AC.STMT.ENTRY
                END
            END
        END
    END
    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '890'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
***********************ADDED ON 11-5-2011
    RETURN
END
