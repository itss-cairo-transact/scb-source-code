* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>177</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SCB.SF.OFS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*---------------------------------------
    CUS.NO  = R.NEW(SCB.SF.TR.CUSTOMER.NO)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.NO,CUS.SEC)

    IF CUS.SEC NE '1100' AND CUS.SEC NE '1200' AND CUS.SEC NE '1300' AND CUS.SEC NE '1400' THEN
        IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "MRGN" THEN
            FLG.TRNS = 1
        END

        IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "LOST" THEN
            FLG.TRNS = 2
        END

        IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "EXPD" THEN
            FLG.TRNS = 3
        END

        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        GOSUB PRINT.DEAL.SLIP
    END
    RETURN

*------------------------------
PRINT.DEAL.SLIP:
*--------------
    CALL REPORT.SF.DEBIT
    CALL REPORT.SF.CREDIT
    RETURN
*------------------------------
INITIALISE:
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SF"
***OFS.USER.INFO    = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    DAT = TODAY

**************************************************CRAETE FT BY OFS**********************************************
*    CALL DBR ('SCB.WH.TXN.TYPE.CONDITION':@FM:SCB.WH.TXN.FT.TRANS,R.NEW(SCB.WH.TRANS.TRANSACTION.TYPE),FT.TXN)
*******MARGN
    IF FLG.TRNS =  1 THEN
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC40":COMMA
    END
********LOST
    IF FLG.TRNS =  2 THEN
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC42":COMMA
    END

********EXP
    IF FLG.TRNS =  3 THEN
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC41":COMMA
    END
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":R.NEW(SCB.SF.TR.MARGIN.VALUE):COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":R.NEW(SCB.SF.TR.VALUE.DATE):COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":R.NEW(SCB.SF.TR.VALUE.DATE):COMMA
    IF FLG.TRNS = 1 THEN
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT):COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":R.NEW(SCB.SF.TR.MARGIN.ACCT):COMMA
    END
    IF FLG.TRNS = 2 THEN
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":R.NEW(SCB.SF.TR.MARGIN.ACCT):COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":"PL54018":COMMA
    END
    IF FLG.TRNS = 3 THEN
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT):COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":R.NEW(SCB.SF.TR.MARGIN.ACCT):COMMA
    END
    OFS.MESSAGE.DATA :=  "PROFIT.CENTRE.DEPT=":R.USER<EB.USE.DEPARTMENT.CODE>:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":ID.NEW:COMMA
    IF FLG.TRNS = 1 THEN
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":R.NEW(SCB.SF.TR.MARGIN.ACCT):COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
    END
    IF FLG.TRNS = 2 THEN
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"PL54018":COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":R.NEW(SCB.SF.TR.MARGIN.ACCT)
    END
    IF FLG.TRNS = 3 THEN
        OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":R.NEW(SCB.SF.TR.MARGIN.ACCT):COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
    END
    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OFS.ID = "T":TNO:".":ID.NEW

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

*TEXT = 'OK' ; CALL REM
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    RETURN
************************************************************
END
