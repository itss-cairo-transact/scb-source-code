* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-41</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.UP.FT.DR.REV.CLR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

MAIN.PROG:
**    IF V$FUNCTION = 'R' THEN
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='RNA' THEN
        GOSUB INTIAL
        GOSUB DRAFT.CHQ.ISSUE
    END
    RETURN
*-----------------------------------------------------------------*
INTIAL:

    ETEXT        ='' ; E            = '' ; CHQ.NOS  = '' ; CHQ.RETURN   = '' ; CHQ.STOP    = '' ; LF = '' ; RH  ='' ;
    COUNTS1      ='' ; COUNTS11     = '' ; CHQ.STAT = '' ; CHQ.PAY.DATE = '' ; CHQ.PAY.BRN = '' ; ER = '' ; ERS ='' ;
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE = '' ; CHQ.BEN  = '' ; CHQ.REC.DATE = '' ;
    CHQ.AMT      =''

    RETURN
*------------------------------------------------------------------*
DRAFT.CHQ.ISSUE:

    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.DEBIT.ACCT.NO):".": R.NEW(FT.CHEQUE.NUMBER)
    KEY.LIST   = "" ; SELECTED = "" ;  ER.MSG = ""

    CALL F.READ(FN.CHQ.PRESENT,SCB.CHQ.ID,R.CHQ.PRESENT,F.CHQ.PRESENT,E1)
    IF NOT(E1) THEN
        R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS>   = "1"
        R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT> = ""
        R.CHQ.PRESENT<DR.CHQ.PAY.DATE>      = ""
        R.CHQ.PRESENT<DR.CHQ.PAY.BRN>       = ""

        KEY.LIST = SCB.CHQ.ID
        CALL F.WRITE(FN.CHQ.PRESENT,SCB.CHQ.ID,R.CHQ.PRESENT)
    END
    RETURN
*--------------------------------------------------------------------*
END
