* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BR.TRANSACTION (ID.FT,TXN.TYPE,BR.AMT,BR.CUR,BR.DEPT,BR.ACCT.DR,BR.ACCT.CR,ID.NO)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.TRANSACTION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS


    F.COUNT = '' ; FN.COUNT = 'F.SCB.BR.TRANSACTION'
    CALL OPF(FN.COUNT,F.COUNT)

*    ID.NO   = ID.NEW
   * TEXT    = ID.NO ; CALL REM
    R.COUNT = ''

    R.COUNT<SCB.BRT.FT.REFERENCE>      = ID.FT

    R.COUNT<SCB.BRT.TRANS.TYPE>        = TXN.TYPE
    R.COUNT<SCB.BRT.DEBIT.AMOUNT>      = BR.AMT
    R.COUNT<SCB.BRT.DEBIT.CURRENCY>    = BR.CUR
    R.COUNT<SCB.BRT.CREDIT.CURRENCY>   = BR.CUR
    R.COUNT<SCB.BRT.PROFIT.CENTRE.DEPT>= BR.DEPT
    R.COUNT<SCB.BRT.DEBIT.ACCT.NO>     = BR.ACCT.DR
    R.COUNT<SCB.BRT.ORDERING.BANK>     = BR.DEPT
    R.COUNT<SCB.BRT.CREDIT.ACCT.NO>    = BR.ACCT.CR
    R.COUNT<SCB.BRT.CREDIT.THEIR.REF>  = BR.ACCT.DR
    R.COUNT<SCB.BRT.DEBIT.THEIR.REF>   = ID.NO
    R.COUNT<SCB.BRT.DEBIT.VALUE.DATE>  = TODAY
    R.COUNT<SCB.BRT.CREDIT.VALUE.DATE> = TODAY

    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
    CALL JOURNAL.UPDATE(ID.NO)
    CLOSE F.COUNT

    RETURN
END
