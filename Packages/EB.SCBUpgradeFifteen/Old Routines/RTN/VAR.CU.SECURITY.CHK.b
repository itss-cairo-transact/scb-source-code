* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>167</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2012/03/18 ***
*******************************************

    SUBROUTINE VAR.CU.SECURITY.CHK
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.SECURITY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CU.DRMNT.INDEX

    GOSUB INITIATE
    GOSUB GET.DATA
    GOSUB AMEND.SECURITY.FILE
    GOSUB CHANG.VER.NAME
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.CU  = "FBNK.CUSTOMER"            ; F.CU   = ""
    FN.SCS = "F.SCB.CUSTOMER.SECURITY"  ; F.SCS  = "" ; R.SCS = ""
    FN.SCDI    = "F.SCB.CU.DRMNT.INDEX"          ; F.SCDI    = ""  ; R.SCDI   = ""

    CALL OPF (FN.SCDI,F.SCDI)
    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.SCS,F.SCS)
    RETURN
*-----------------------------------------------------
GET.DATA:
    WS.NATION           = R.NEW(EB.CUS.NATIONALITY)
    WS.POST.REST        = R.NEW(EB.CUS.POSTING.RESTRICT)
    WS.POST.REST.OLD    = R.OLD(EB.CUS.POSTING.RESTRICT)
    WS.CUSTOMER.STATUS  = R.NEW(EB.CUS.CUSTOMER.STATUS)
    WS.REC.STATUS       = R.NEW(EB.CUS.RECORD.STATUS)
    WS.PASSPORT.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.NUMBER>
    WS.NATIONAL.ID      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NSN.NO>
    WS.PLACE.ISSUE      = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.PLACE.ID.ISSUE>
    WS.REG.NO           = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.COM.REG.NO>
    WS.ID.TYPE          = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ID.TYPE>

    WS.ARABIC.NAME.1    = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ARABIC.NAME>
    WS.ARABIC.NAME.2    = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.ARABIC.NAME.2>

    WS.SECURITY.KEY.OLD = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.SECURITY.KEY>
    WS.SECURITY.KEY.NEW = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.SECURITY.KEY>

    WS.NEW.SECTOR.NEW   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>
    WS.NEW.SECTOR.OLD   = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.NEW.SECTOR>

    CALL DBR ('SCB.NEW.SECTOR':@FM:C.PROVE.CODE,WS.NEW.SECTOR.NEW,WS.PROVE.CODE.NEW)
    CALL DBR ('SCB.NEW.SECTOR':@FM:C.PROVE.CODE,WS.NEW.SECTOR.OLD,WS.PROVE.CODE.OLD)

    WS.DRMNT.CO.NEW   = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>
    WS.DRMNT.CO.OLD   = R.OLD(EB.CUS.LOCAL.REF)<1,CULR.DRMNT.CODE>

    WS.PROVE.CODE = WS.PROVE.CODE.NEW
    WS.NEW.SECTOR = WS.NEW.SECTOR.NEW

    RETURN
*-----------------------------------------------------
AMEND.SECURITY.FILE:

    IF WS.SECURITY.KEY.OLD NE WS.SECURITY.KEY.NEW THEN
*****-------------------------------------- ���� ��� ������� ������ ������� �������  --------------------------------*
        IF WS.SECURITY.KEY.OLD NE '' THEN
            CALL F.READ(FN.SCS,WS.SECURITY.KEY.OLD,R.SCS,F.SCS,E2)
            IF NOT(E2) THEN
*Line [ 98 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.SCS.COUNT  = DCOUNT((R.SCS<SCS.CUSTOMER.ID>),@VM)

                IF WS.SCS.COUNT EQ 1 THEN
                    CALL F.DELETE(FN.SCS,WS.SECURITY.KEY.OLD)
                END

                IF WS.SCS.COUNT GT 1 THEN
                    FOR I.SCS = 1 TO WS.SCS.COUNT
                        IF ID.NEW = R.SCS<SCS.CUSTOMER.ID,(I.SCS)> THEN
                            DEL R.SCS<SCS.CUSTOMER.ID,(I.SCS)>
                            DEL R.SCS<SCS.POSTING.RESTRICT,(I.SCS)>
                            CALL F.WRITE(FN.SCS,WS.SECURITY.KEY.OLD,R.SCS)
                            WS.SCS.COUNT --
                            I.SCS = WS.SCS.COUNT
                        END
                    NEXT I.SCS
                END
            END
        END
        IF WS.SECURITY.KEY.NEW NE '' THEN
            GOSUB AMEND.NEW.KEY
        END
    END
    IF WS.POST.REST.OLD NE WS.POST.REST THEN

        GOSUB AMEND.NEW.KEY

*** UPDATE BY MOHAMED SABRY 2013/11/03
        IF (( WS.POST.REST.OLD EQ 18 ) OR ( WS.POST.REST EQ 18 )) THEN
            GOSUB CREATE.DRMNT.POS.INDEX
        END
    END
***
*** UPDATE BY MOHAMED SABRY 2013/12/17
    IF WS.DRMNT.CO.OLD NE WS.DRMNT.CO.NEW THEN
        GOSUB CREATE.DRMNT.CO.INDEX
    END
***
    RETURN
*--------------------------------------------------------------------------------------------------------------------*
AMEND.NEW.KEY:
*****-------------------------------------  ������ ��� ������� ������ �� ������ -------------------------------------*
    IF WS.SECURITY.KEY.NEW NE '' THEN
        CALL F.READ(FN.SCS,WS.SECURITY.KEY.NEW,R.SCS,F.SCS,E3)
        IF NOT(E3) THEN
*Line [ 144 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.SCS.COUNT  = DCOUNT((R.SCS<SCS.CUSTOMER.ID>),@VM)
            FOR I.SCS  = 1 TO WS.SCS.COUNT
                IF ID.NEW = R.SCS<SCS.CUSTOMER.ID,(I.SCS)> THEN
                    R.SCS<SCS.POSTING.RESTRICT,(I.SCS)> = WS.POST.REST
                    CALL F.WRITE(FN.SCS,WS.SECURITY.KEY.NEW,R.SCS)
                    RETURN
                END
            NEXT I.SCS
            R.SCS<SCS.CUSTOMER.ID,WS.SCS.COUNT+1>        = ID.NEW
            R.SCS<SCS.POSTING.RESTRICT,WS.SCS.COUNT+1>   = WS.POST.REST
            CALL F.WRITE(FN.SCS,WS.SECURITY.KEY.NEW,R.SCS)
        END ELSE
            R.SCS<SCS.CHECK.FLAG>                        = 'Y'
            R.SCS<SCS.PROVE.CODE>                        = WS.PROVE.CODE
            R.SCS<SCS.CUSTOMER.ID,1>                     = ID.NEW
            R.SCS<SCS.POSTING.RESTRICT,1>                = WS.POST.REST
            R.SCS<SCS.ARABIC.NAME,1>                     = WS.ARABIC.NAME.1
            IF  WS.ARABIC.NAME.2 NE '' THEN
                R.SCS<SCS.ARABIC.NAME,2>                 = WS.ARABIC.NAME.2
            END
            CALL F.WRITE(FN.SCS,WS.SECURITY.KEY.NEW,R.SCS)
        END
    END
    RETURN
*-----------------------------------------------------
*** ADD BY MOHAMED SABRY 2013/11/03
*-----------------------------------------------------
CREATE.DRMNT.POS.INDEX:
*    WS.DATE            = R.NEW(EB.CUS.DATE.TIME)<1,1>
*    WS.DATE            = WS.DATE[1,6]
*    WS.CURR.DATE       = WS.DATE + 20000000
    WS.CURR.DATE       = TODAY
    WS.CURR.POS        = WS.POST.REST
    WS.CURR.CURR       = R.NEW(EB.CUS.CURR.NO)

    CALL F.READ(FN.SCDI,ID.NEW,R.SCDI,F.SCCI,E.SCDI)

*Line [ 182 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.SCDI<SCDI.POS.DATE>,@VM)
    WS.COUNT ++

    R.SCDI<SCDI.POS.DATE,WS.COUNT>       = WS.CURR.DATE
    R.SCDI<SCDI.POSTING,WS.COUNT>        = WS.CURR.POS
    R.SCDI<SCDI.DRMNT.CURR.NO,WS.COUNT>  = WS.CURR.CURR

    IF WS.CURR.POS EQ 18 THEN
        R.SCDI<SCDI.DRMNT.DATE>          = WS.CURR.DATE
    END ELSE
        R.SCDI<SCDI.DRMNT.DATE>          = ''
    END
    CALL F.WRITE(FN.SCDI,ID.NEW,R.SCDI)

    R.SCDI   = ''
    WS.COUNT = 0
    RETURN
*-----------------------------------------------------
*** ADD BY MOHAMED SABRY 2013/12/17
*-----------------------------------------------------
CREATE.DRMNT.CO.INDEX:
*    WS.DATE            = R.NEW(EB.CUS.DATE.TIME)<1,1>
*    WS.DATE            = WS.DATE[1,6]
*    WS.CURR.DATE       = WS.DATE + 20000000
    WS.CURR.DATE       = TODAY
    WS.CURR.CODE       = WS.DRMNT.CO.NEW
    WS.CURR.CURR       = R.NEW(EB.CUS.CURR.NO)

    CALL F.READ(FN.SCDI,ID.NEW,R.SCDI,F.SCCI,E.SCDI)

*Line [ 213 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT = DCOUNT(R.SCDI<SCDI.CODE.DATE>,@VM)
    WS.COUNT ++

    R.SCDI<SCDI.CODE.DATE,WS.COUNT>     = WS.CURR.DATE
    R.SCDI<SCDI.CODE,WS.COUNT>          = WS.CURR.CODE
    R.SCDI<SCDI.CODE.CURR.NO,WS.COUNT>  = WS.CURR.CURR

    IF WS.CURR.CODE EQ 1 THEN
        R.SCDI<SCDI.DRMNT.CODE.DATE>    = WS.CURR.DATE
    END ELSE
        R.SCDI<SCDI.DRMNT.CODE.DATE>    = ''
    END

    CALL F.WRITE(FN.SCDI,ID.NEW,R.SCDI)

    R.SCDI   = ''
    WS.COUNT = 0
    RETURN
*-----------------------------------------------------
*** ADD BY MOHAMED SABEY 2014/09/02
************************************
CHANG.VER.NAME:
    IF V$FUNCTION = 'A' THEN
        VERSION.NAME  = R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME>
        IF (VERSION.NAME EQ ",SCB.PRIVATE.SUEZ" AND PGM.VERSION ",SCB.PRIVATE.SUEZ") THEN
            R.NEW(EB.CUS.LOCAL.REF)<1,CULR.VERSION.NAME> = ",SCB.PRIVATE.UPDATE"
            CALL REBUILD.SCREEN
        END
    END
    RETURN
*-----------------------------------------------------
