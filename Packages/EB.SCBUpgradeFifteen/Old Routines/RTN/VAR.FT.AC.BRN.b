* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
 SUBROUTINE VAR.FT.AC.BRN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


 IF R.NEW(FT.DEBIT.ACCT.NO)[1,2] # R.NEW(FT.CREDIT.ACCT.NO)[1,2] THEN
     DIM R.FT(FT.AUDIT.DATE.TIME)
     MAT R.FT = ""
     ER.MSG = ""
     ID.FT = ""
     W.STATUS = "" 

      DEP = R.NEW(FT.DEBIT.ACCT.NO)[1,2]
      DEP2 = R.NEW(FT.CREDIT.ACCT.NO)[1,2]
      CALL DBR('CURRENCY':@FM:2,R.NEW(FT.DEBIT.CURRENCY),DRCURR)
      CALL DBR('CURRENCY':@FM:2,R.NEW(FT.CREDIT.CURRENCY),CRCURR)
      DRACCT = DEP:'400010':DRCURR:'2101':'01'
      CRACCT = DEP2:'400010':CRCURR:'2101':'01'

     R.FT(FT.TRANSACTION.TYPE) = "AC"
     R.FT(FT.DEBIT.AMOUNT) = R.NEW(FT.DEBIT.AMOUNT)
     R.FT(FT.DEBIT.CURRENCY) = R.NEW(FT.DEBIT.CURRENCY)
     R.FT(FT.CREDIT.CURRENCY) = R.NEW(FT.CREDIT.CURRENCY)
     R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(FT.DEBIT.VALUE.DATE)
     R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(FT.CREDIT.VALUE.DATE)
     R.FT(FT.DEBIT.ACCT.NO) = DRACCT
     R.FT(FT.CREDIT.ACCT.NO) = CRACCT

     CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
     IF W.STATUS # "L" THEN
        IF NOT(ER.MSG) THEN
           ER.MSG = "ERROR CREATING FT"
        END
    END
 END
  RETURN
  END
