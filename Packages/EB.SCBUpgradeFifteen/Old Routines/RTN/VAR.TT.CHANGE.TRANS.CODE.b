* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
** ----- NESSREEN  -----
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.CHANGE.TRANS.CODE
** TO CHANGE THE RESTRICT IN CUSTOMER IF TRANSACTION CODE EQ 55 56 57
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
**12/6/2006   CUST = R.NEW(TT.TE.CUSTOMER.2)
**IN PAR
    CUST = R.NEW(TT.TE.CUSTOMER.1)
**7/5/2006    NEWACC = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.NEW.ACCT.OPENED>
**7/5/2006    CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, NEWACC ,CUST)
    TRAN.COD = R.NEW(TT.TE.TRANSACTION.CODE)
    IF TRAN.COD = '10' OR TRAN.COD = '44' OR TRAN.COD = '45' THEN
        TEXT = 'REMOVE CUST REST' ; CALL REM
**7/5/2006 IF TRAN.COD = '55' OR TRAN.COD = '56' OR TRAN.COD = '57' THEN
        CALL OPF( FN.CUSTOMER,F.CUSTOMER)
        CALL F.READ( FN.CUSTOMER,CUST, R.CUSTOMER, F.CUSTOMER, ETEXT)
        R.CUSTOMER<EB.CUS.POSTING.RESTRICT> = ''
        CALL F.WRITE(FN.CUSTOMER,CUST,R.CUSTOMER)
    END
    RETURN
END
