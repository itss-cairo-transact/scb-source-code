* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 24/08/2003***********

SUBROUTINE VAR.LC.AMT.INFO

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.AMOUNT.INFO


FN.SCB.LC.AMOUNT.INFO = 'F.SCB.LC.AMOUNT.INFO' ; F.SCB.LC.AMOUNT.INFO = ''; R.SCB.LC.AMOUNT.INFO = '';
OLD.AMT = R.OLD(TF.LC.LC.AMOUNT)
NEW.AMT = R.NEW(TF.LC.LC.AMOUNT)

IF OLD.AMT # NEW.AMT THEN
  DIFF.AMT = SSUB(NEW.AMT,OLD.AMT)
* CALL F.READ(FN.SCB.LC.AMOUNT.INFO ,ID.NEW , R.SCB.LC.AMOUNT.INFO , F.SCB.LC.AMOUNT.INFO , E1)
 CALL OPF( FN.SCB.LC.AMOUNT.INFO,F.SCB.LC.AMOUNT.INFO)
 CALL F.READU(FN.SCB.LC.AMOUNT.INFO , ID.NEW , S.SCB.LC.AMOUNT.INFO , F.SCB.LC.AMOUNT.INFO , E1 , RETRY)
 * CALL DBR("SCB.LC.AMOUNT.INFO":@FM:SCB.LC.AMT.AMOUNT,ID.NEW,AMT)
 * CALL DBR("SCB.LC.AMOUNT.INFO":@FM:SCB.LC.AMT.AMEND.DATE,ID.NEW,DAT)
   AMT = S.SCB.LC.AMOUNT.INFO<SCB.LC.AMT.AMOUNT>
   DAT = S.SCB.LC.AMOUNT.INFO<SCB.LC.AMT.AMEND.DATE>
   IF NOT(AMT) THEN
   R.SCB.LC.AMOUNT.INFO<SCB.LC.AMT.AMOUNT> = DIFF.AMT
   R.SCB.LC.AMOUNT.INFO<SCB.LC.AMT.AMEND.DATE> = TODAY
   END ELSE
*Line [ 50 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
      DD = DCOUNT(AMT,@VM)
       FOR J = 1 TO DD
      AMNT<1,J> = AMT<1,J>
      DATT<1,J> = DAT<1,J>
        NEXT J
        DD = DD + 1
      AMNT<1,DD> = DIFF.AMT
      DATT<1,DD> = TODAY
      R.SCB.LC.AMOUNT.INFO<SCB.LC.AMT.AMOUNT> = AMNT
      R.SCB.LC.AMOUNT.INFO<SCB.LC.AMT.AMEND.DATE> = DATT


    END

     CALL F.WRITE(FN.SCB.LC.AMOUNT.INFO,ID.NEW,R.SCB.LC.AMOUNT.INFO)
     CALL JOURNAL.UPDATE(ID.NEW)
     CALL F.RELEASE(FN.SCB.LC.AMOUNT.INFO,ID.NEW,F.SCB.LC.AMOUNT.INFO)
END

RETURN
END
