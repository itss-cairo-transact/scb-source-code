* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-41</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.UP.FT.DR.CRT.CROSS

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

****************MAIN PROGRAM************************
* TEXT = "HI ABOUD" ; CALL REM
MAIN.PROG:
    GOSUB INTIAL
    GOSUB DRAFT.CHQ.ISSUE
   RETURN
*************************************************************************************************
INTIAL:
    ETEXT='' ; E ='' ; CHQ.NOS ='' ; CHQ.RETURN ='' ; CHQ.STOP ='' ; LF ='' ; RH ='' ;
    COUNTS1 ='' ; COUNTS11 ='' ; CHQ.STAT ='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS ='' ;
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE ='' ;
    CHQ.AMT =''
    RETURN
*********************************************************************************************************
DRAFT.CHQ.ISSUE:
   * TEXT = 'DRAFT.CHQ.ISSUE' ;  CALL REM

    FN.CHQ.PRESENT = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    SCB.CHQ.ID = R.NEW(FT.LOCAL.REF)<1,FTLR.ACCOUNT.NO>:".":R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
   * TEXT = "SCB.CHQ.ID": SCB.CHQ.ID ; CALL REM
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ":SCB.CHQ.ID

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

   * TEXT = "SLE": SELECTED ; CALL REM

   * TEXT = "DRAFT.CHQ.PAID" ; CALL REM
    CALL F.READ(FN.CHQ.PRESENT,KEY.LIST,R.CHQ.PRESENT,F.CHQ.PRESENT,E1)

    R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS> = "2"
   * TEXT = "R.CHQ": R.CHQ.PRESENT<DR.CHQ.CHEQ.STATUS> ; CALL REM
    R.CHQ.PRESENT<DR.CHQ.TRANS.PAYMENT> = ID.NEW
   * TEXT = "CHQ.TRNS": ID.NEW ; CALL REM
    R.CHQ.PRESENT<DR.CHQ.PAY.DATE> = R.NEW(FT.CREDIT.VALUE.DATE)
   * TEXT = "CHQ.PAY.DATE": R.CHQ.PRESENT<DR.CHQ.PAY.DATE> ; CALL REM
    R.CHQ.PRESENT<DR.CHQ.PAY.BRN> = R.USER< EB.USE.DEPARTMENT.CODE>
   * TEXT = "CHQ.PAY.BRN": R.USER< EB.USE.DEPARTMENT.CODE> ; CALL REM

*UPDATE.SCB.FT.DR.CHQ
   * TEXT = "UPDATE.SCB.FT.DR.CHQ";CALL REM
    KEY.LIST = SCB.CHQ.ID
    CALL F.WRITE(FN.CHQ.PRESENT,KEY.LIST,R.CHQ.PRESENT)
* CALL F.RELEASE(FN.SCB.FT.DR.CHQ,KEY.LIST,F.SCB.FT.DR.CHQ)

    RETURN

***************************************************************************
END
