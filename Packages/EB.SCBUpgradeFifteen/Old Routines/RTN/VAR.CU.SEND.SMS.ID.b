* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2014/09/03 ***
*******************************************

    SUBROUTINE VAR.CU.SEND.SMS.ID
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB GET.DATA
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.CU  = "FBNK.CUSTOMER"            ; F.CU   = ""

    RETURN
*-----------------------------------------------------
GET.DATA:
    TEXT = R.NEW(AC.CUSTOMER) ; CALL REM
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,R.NEW(AC.CUSTOMER),WS.LOCAL)
    TEXT = WS.LOCAL ; CALL REM
    WS.MOB.TEL          = WS.LOCAL<1,CULR.TELEPHONE>
    TEXT = WS.MOB.TEL ; CALL REM
    WS.CORP             = WS.LOCAL<1,CULR.CORPORATE.FLG>
    TEXT = WS.CORP ; CALL REM
    WS.BANK             = WS.LOCAL<1,CULR.CU.BANK>

    IF WS.CORP EQ 'YES' THEN
        IF WS.BANK EQ '0017' THEN
            GOSUB PRINT.SMS
        END
    END

    RETURN
*-----------------------------------------------------
PRINT.SMS:

    REPORT.ID='VAR.CU.SEND.SMS.ID'
    CALL PRINTER.ON(REPORT.ID,'')

    PR.HD  =REPORT.ID
    HEADING PR.HD

    TEXT = ID.NEW:',':WS.MOB.TEL<1,1,3> ; CALL REM
    PRINT ID.NEW:',':WS.MOB.TEL<1,1,3>
    RETURN
*-----------------------------------------------------
