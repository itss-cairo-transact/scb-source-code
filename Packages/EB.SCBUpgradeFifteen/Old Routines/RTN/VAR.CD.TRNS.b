* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1557</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CD.TRNS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*-------------------------------------------------------

    MESSAGE  = ''
    ERR.MESS = ''

    IF V$FUNCTION EQ 'I' THEN
*------------------------  INP  ----------------------
        IF  R.NEW(LD.RECORD.STATUS)[1,3] EQ 'INA' THEN
            MULTI.ENTRIES = ''
            GOSUB AC.STMT.ENTRY
            TYPE = 'DEL'
            CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
            IF END.ERROR OR TEXT = "NO" THEN RETURN

        END
        MULTI.ENTRIES = ''
        GOSUB AC.STMT.ENTRY
        TYPE = 'VAL'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END
*------------------------  DEL  ----------------------

    IF V$FUNCTION EQ 'D' THEN
        MULTI.ENTRIES = ''
        GOSUB AC.STMT.ENTRY
        TYPE = 'DEL'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END

*------------------------  AUT ----------------------

    IF V$FUNCTION EQ 'A' AND R.NEW(LD.RECORD.STATUS)[1,3]= 'INA' THEN
        TYPE = 'AUT'
        CALL EB.ACCOUNTING("SYS",TYPE, "" , "" )
        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END


**    IF V$FUNCTION EQ 'A' AND  R.NEW(LD.RECORD.STATUS)[1,3]= 'RNA' THEN
**
**   GOSUB AC.STMT.ENTRY.REV
** END


    RETURN


*------------------------------
AC.STMT.ENTRY:

    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]

    AMT      = R.NEW(LD.AMOUNT)
    LD.CATG  = R.NEW(LD.CATEGORY)
    CURR     = R.NEW(LD.CURRENCY)

    CR.DATA  = TODAY
    DB.DATE  = TODAY

*--------
*   CR
*--------
*                 ----    1 CR MARG   ----
    CR.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>

    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT,ACC.OFFICER)


    Y.ACCT.CR = CR.ACCT


    LCY.AMT.CR = AMT

    FCY.AMT.CR = ''


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.CR
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '401'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT> NE '' THEN
        ENTRY<AC.STE.NARRATIVE>    =  R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
    END
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.CR
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = CR.DATA
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.CR
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     =TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)


    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""
*-------
*  DR
*-------
    DB.ACCT    =  R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    Y.ACCT.DR = DB.ACCT


    LCY.AMT.DR =  AMT  * -1



    FCY.AMT.DR = ''

*****************
*    ----    2 DR MARG   ---

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.DR
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '401'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT> NE '' THEN
        ENTRY<AC.STE.NARRATIVE>    =  R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
    END

    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.DR
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = DB.DATE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.DR
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""

    RETURN


** ---------------------  Rev  -------------------------  **


AC.STMT.ENTRY.REV:


    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]

    AMT      = R.NEW(LD.AMOUNT)
    LD.CATG  = R.NEW(LD.CATEGORY)
    CURR     = R.NEW(LD.CURRENCY)

    CR.DATA  = TODAY
    DB.DATE  = TODAY

*--------
*   CR
*--------
*************
*                 ----    1 CR MARG   ----
    CR.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>

    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT,ACC.OFFICER)

    Y.ACCT.CR = CR.ACCT


    LCY.AMT.CR = AMT * -1

    FCY.AMT.CR = ''


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.CR
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '401'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT> NE '' THEN
        ENTRY<AC.STE.NARRATIVE>    =  R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
    END
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.CR
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = CR.DATA
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.CR
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     =TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)


    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""
*-------
*  DR
*-------
    DB.ACCT    =  R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    Y.ACCT.DR = DB.ACCT


    LCY.AMT.DR =  AMT



    FCY.AMT.DR = ''

*****************
*    ----    2 DR MARG   ---

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.DR
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '401'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT> NE '' THEN
        ENTRY<AC.STE.NARRATIVE>    =  R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
    END

    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.DR
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = DB.DATE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.DR
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""

    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
** ---------------------  Rev
END
