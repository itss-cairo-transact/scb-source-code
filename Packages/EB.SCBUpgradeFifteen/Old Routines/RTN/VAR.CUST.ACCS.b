* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
******************MAHMOUD 17/5/2012**********************
********COPIED AND UPDATED BY MAI 06/04/2020
*-----------------------------------------------------------------------------
* <Rating>1691</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CUST.ACCS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
    $INCLUDE T24.BP I_F.LETTER.OF.CREDIT          ;*TF.LC.
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
    $INCLUDE T24.BP I_F.NUMERIC.CURRENCY          ;*EB.NCN.CURRENCY.CODE
    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM         ;*RE.BCP.
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TSA.SERVICE
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP


    GOSUB INITIATE
*Line [ 81 ] Adding EB.SCBUpgradeFifteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
***    *EXECUTE "sh EIBHIS/s_eibhis.sh"
    RETURN
*===========================================
INITIATE:
*--------
    II = 0
    CC = 0
    CARDS.ARR = ''
    AST = ","
    CUSTOMER.ID = ID.NEW
    PATHNAME1 = "EIBHIS"
    **FILENAME1 = CUSTOMER.ID:"-REG.CSV"
    FILENAME1 = "REG-":CUSTOMER.ID:".CSV"

    OPENSEQ PATHNAME1 , FILENAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATHNAME1:' ':FILENAME1
        HUSH OFF
    END
    OPENSEQ PATHNAME1 , FILENAME1 TO BB ELSE
        CREATE BB THEN
            CRT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END
    VICARD.GRP = ' 101 102 104 105 201 202 204 205 '
    VISUPP.GRP = ' 110 111 112 113 210 211 212 213 '
***UPDATED BY NESSREEN AHMED 1/10/2019***
    MACARD.GRP = ' 501 502 504 505 601 602 604 605 701 702 704 705 1001 1004'
    MASUPP.GRP = ' 510 511 512 513 610 611 612 613 710 711 712 713'
***END OF UPDATE 1/10/2019*****************

    RETURN
*===========================================
CALLDB:
*------
*Master files

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ER.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = '' ; R.MM = '' ; ER.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.LC = "FBNK.LETTER.OF.CREDIT"      ; F.LC = "" ; R.LC = '' ; ER.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = "FBNK.DRAWINGS"      ; F.DR = "" ; R.DR = '' ; ER.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.COL   = "FBNK.COLLATERAL"         ; F.COL  = '' ;  R.COL = '' ; ER.COL = ''
    CALL OPF(FN.COL,F.COL)

    FN.COL.R = "FBNK.COLLATERAL.RIGHT"   ; F.COL.R  = '' ; R.COL.R = '' ; ER.COL.R = ''
    CALL OPF(FN.COL.R,F.COL.R)

    FN.LIM = "FBNK.LIMIT"                ; F.LIM = "" ; R.LIM = ''  ; ER.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; ER.CUR = '' ; ER.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.N.CUR = 'F.NUMERIC.CURRENCY' ; F.N.CUR = '' ; R.N.CUR = '' ; ER.N.CUR = '' ; ER.N.CUR = ''
    CALL OPF(FN.N.CUR,F.N.CUR)

    FN.CO = 'F.COMPANY' ; F.CO = '' ; R.CO = '' ; ER.CO = ''
    CALL OPF(FN.CO,F.CO)

    FN.CG = "F.SCB.CUSTOMER.GROUP"  ; F.CG  = "" ; R.CG = ""
    CALL OPF (FN.CG,F.CG)

    FN.CI = "FBNK.CARD.ISSUE"  ; F.CI  = "" ; R.CI = ""
    CALL OPF (FN.CI,F.CI)

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)


* Index files

* Index file for account
    FN.IND.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC = '' ; R.IND.AC = '' ; ER.IND.AC = ''
    CALL OPF(FN.IND.AC,F.IND.AC)

* Index file for account1
    FN.IND.AC1 = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC1 = '' ; R.IND.AC1 = '' ; ER.IND.AC1 = ''
    CALL OPF(FN.IND.AC1,F.IND.AC1)

*Index file for LD's , LG's and MM's
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

*Collateral (Received)  Index
    FN.IND.R.COL = "FBNK.COLLATERAL.RIGHT.CUST"   ; F.IND.R.COL = ""
    CALL OPF(FN.IND.R.COL,F.IND.R.COL)

*Collateral (Given)  Index
    FN.IND.G.COL = "FBNK.CUSTOMER.COLLATERAL"   ; F.IND.G.COL = ""
    CALL OPF(FN.IND.G.COL,F.IND.G.COL)

*Collateral Index by useing Collateral (Received)  Index or Collateral (Given)  Index
    FN.RIGHT.COL= "FBNK.RIGHT.COLLATERAL"      ; F.RIGHT.COL =""
    CALL OPF(FN.RIGHT.COL,F.RIGHT.COL)

*LETTER.OF.CREDIT Index !!! (import)
    FN.IND.LCA = "FBNK.LC.APPLICANT"         ; F.IND.LCA = ""
    CALL OPF(FN.IND.LCA,F.IND.LCA)

* FOR TEST LC.ISSUING.BANK
*    FN.IND.LCA = "FBNK.LC.ISSUING.BANK"         ; F.IND.LCA = ""

**LETTER.OF.CREDIT Index  !!! (exp)
    FN.IND.LCB = "FBNK.LC.BENEFICIARY"   ; F.IND.LCB = ""
    CALL OPF(FN.IND.LCB,F.IND.LCB)

*Index file for limit
    FN.IND.LI = "FBNK.LIMIT.LIABILITY"   ; F.IND.LI = ""
    CALL OPF(FN.IND.LI,F.IND.LI)

*Index file for Card Issue
    FN.IND.CI = "FBNK.CARD.ISSUE.ACCOUNT"   ; F.IND.CI = ""
    CALL OPF(FN.IND.CI,F.IND.CI)

    RETURN
*===========================================
PROCESS:
*---------
    CU.ID = ID.NEW
    II = 0
    CC = 0
    CARDS.ARR = ''
    CALL F.READ(FN.CU,CU.ID,R.CU,F.CU,ER.CU)
    CU.COM  = R.CU<EB.CUS.COMPANY.BOOK>
    CU.SEC  = R.CU<EB.CUS.SECTOR>
    CU.LCL  = R.CU<EB.CUS.LOCAL.REF>
    CU.NAME = CU.LCL<1,CULR.ARABIC.NAME>:" ":CU.LCL<1,CULR.ARABIC.NAME.2>
    CRD.STA = CU.LCL<1,CULR.CREDIT.STAT>
    CRD.COD = CU.LCL<1,CULR.CREDIT.CODE>
    EMP.NO  = CU.LCL<1,CULR.EMPLOEE.NO>
    SS.ON   = (1000000 + EMP.NO)[2,6]
    IF CRD.STA NE '' OR CRD.COD GE 110 THEN
       RETURN
    END
    GOSUB GET.AC
    GOSUB GET.LD
    GOSUB GET.LC.A
    GOSUB GET.LC.B
    GOSUB GET.LI
    IF CU.SEC EQ '1100' THEN
        GOSUB GET.CARDS.NO
    END
    RETURN
*===========================================
GET.AC:
*---------
    CALL F.READ(FN.IND.AC,CU.ID,R.IND.AC,F.IND.AC,ER.IND.AC)
    LOOP
        REMOVE ACCT.ID FROM R.IND.AC SETTING POS.IND.AC
    WHILE ACCT.ID:POS.IND.AC
        GOSUB GET.AC.DATA
    REPEAT
    RETURN
*===========================================
GET.AC.DATA:
*---------
    CALL F.READ(FN.AC,ACCT.ID,R.AC,F.AC,ER.AC)
    CUR.ID         = R.AC<AC.CURRENCY>
    CAT.ID         = R.AC<AC.CATEGORY>
    COMP1.ID       = R.AC<AC.CO.CODE>
    CUST.NO = FMT(CU.ID,"R%8")
    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
    SUB.ID  = ACCT.ID
*Line [ 264 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR CAT.ID:' ' IN "1710 1711 1721 1701 5000 5001 5081 5082 5083 5084 5085 5090 5091 1021 1022 5079 5080 21078 21079 " SETTING POS.NOSHOW THEN '' ELSE
**##        FINDSTR CAT.ID:' ' IN "1002 1407 1408 1413 1445 1455 " SETTING POS.SS THEN NULL ELSE
        GOSUB SEND.REC
**##        END
    END
    RETURN
*=============================================
GET.LD:
*---------
    CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            GOSUB GET.LD.DATA
        END
    REPEAT

    RETURN
*=============================================
GET.LD.DATA:
*---------
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
    REC.STA     = R.LD<LD.STATUS>
    IF REC.STA NE 'LIQ' THEN
        CAT.ID    = R.LD<LD.CATEGORY>
        CUR.ID    = R.LD<LD.CURRENCY>
        COMP1.ID  = R.LD<LD.CO.CODE>
        CUST.NO = FMT(CU.ID,"R%8")
        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
        SUB.ID  = CUST.NO:CUR.NUM:CAT.ID
        GOSUB SEND.REC
    END
    RETURN
*=============================================
GET.LC.A:
*---------
    CALL F.READ(FN.IND.LCA,CU.ID,R.IND.LCA,F.IND.LCA,ER.IND.LCA)
    LOOP
        REMOVE LCA.ID FROM R.IND.LCA SETTING POS.LCA
    WHILE LCA.ID:POS.LCA
        CALL F.READ(FN.LC,LCA.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                CAT.ID    = R.LC<TF.LC.CATEGORY.CODE>
                CUR.ID    = R.LC<TF.LC.LC.CURRENCY>
                COMP1.ID  = R.LC<TF.LC.CO.CODE>
                CUST.NO = FMT(CU.ID,"R%8")
                CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
                SUB.ID  = CUST.NO:CUR.NUM:CAT.ID
                GOSUB SEND.REC
            END
            CAT.ID    = R.LC<TF.LC.CATEGORY.CODE>
            DR.SUB.ID = LCA.ID
            GOSUB DR.REC
        END
    REPEAT
    RETURN
*=============================================
GET.LC.B:
*---------
    CALL F.READ(FN.IND.LCB,CU.ID,R.IND.LCB,F.IND.LCB,ER.IND.LCB)
    LOOP
        REMOVE LCB.ID FROM R.IND.LCB SETTING POS.LCB
    WHILE LCB.ID:POS.LCB
        CALL F.READ(FN.LC,LCB.ID,R.LC,F.LC,ER.LC)
        IF NOT(ER.LC) THEN
            WS.NEXT.DR  = R.LC<TF.LC.NEXT.DRAWING>
            IF R.LC<TF.LC.LIABILITY.AMT> GT 0 THEN
                CAT.ID    = R.LC<TF.LC.CATEGORY.CODE>
                CUR.ID    = R.LC<TF.LC.LC.CURRENCY>
                COMP1.ID  = R.LC<TF.LC.CO.CODE>
                CUST.NO = FMT(CU.ID,"R%8")
                CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
                SUB.ID  = CUST.NO:CUR.NUM:CAT.ID
                GOSUB SEND.REC
            END
            CAT.ID    = R.LC<TF.LC.CATEGORY.CODE>
            DR.SUB.ID = LCB.ID
            GOSUB DR.REC
        END
    REPEAT
    RETURN
*=============================================
DR.REC:
*--------
    WS.LC.GL  = CAT.ID
    WS.SRL.DR = ''
    WS.NEXT.DR = WS.NEXT.DR + 0
    FOR IDR = 1 TO WS.NEXT.DR
        WS.SRL.ID = ( IDR + 100 )[2,2]
        WS.DR.ID = DR.SUB.ID : WS.SRL.ID
        CALL F.READ(FN.DR,WS.DR.ID,R.DR,F.DR,ER.DR)
        IF NOT(ER.DR) THEN
            IF R.DR<TF.DR.MATURITY.REVIEW> GT TODAY THEN
                IF WS.LC.GL GE 23100 AND WS.LC.GL LE 23299 THEN
                    CAT.ID   = 50000
                END ELSE
                    CAT.ID   = 50001
                END
                CUR.ID   = R.DR<TF.DR.DRAW.CURRENCY>
                COMP1.ID = R.DR<TF.DR.CO.CODE>
                CUST.NO = FMT(CU.ID,"R%8")
                CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
                SUB.ID  = CUST.NO:CUR.NUM:CAT.ID
                GOSUB SEND.REC
            END
        END
    NEXT IDR
    WS.LC.GL = 0
    RETURN
*=============================================
GET.LI:
*---------
    CALL F.READ(FN.IND.LI,CU.ID,R.IND.LI,F.IND.LI,ER.IND.LI)
    LOOP
        REMOVE LI.ID FROM R.IND.LI SETTING POS.LI
    WHILE LI.ID:POS.LI
        IF FIELD(LI.ID,'.',4) EQ '' THEN
            CALL F.READ(FN.LIM,LI.ID,R.LIM,F.LIM,ER.LIM)
            WS.LI.PROD    = R.LIM<LI.LIMIT.PRODUCT>
            WS.PRO.ALL  = R.LIM<LI.PRODUCT.ALLOWED>
            IF  WS.PRO.ALL NE '' AND WS.LI.PROD NE 2000 AND WS.LI.PROD NE 5100 THEN
                WS.ON.LI.NO = DCOUNT(R.LIM<LI.ONLINE.LIMIT>,@VM)
                IF WS.LI.PROD NE 9700 AND WS.LI.PROD NE 9800 AND WS.LI.PROD NE 9900 THEN
                    CAT.ID    = 41000
                    CUR.ID    = R.LIM<LI.LIMIT.CURRENCY>
                    COMP1.ID  = R.LIM<LI.LOCAL.REF><1,LILR.COMPANY.BOOK>
                    CUST.NO = FMT(CU.ID,"R%8")
                    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
                    SUB.ID  = CUST.NO:CUR.NUM
                    GOSUB SEND.REC
                END
            END
        END
    REPEAT
    RETURN
*===========================================
GET.CARDS.NO:
*---------
    CALL F.READ(FN.IND.AC1,CU.ID,R.IND.AC1,F.IND.AC1,ER.IND.AC1)
    LOOP
        REMOVE ACCT.ID FROM R.IND.AC1 SETTING POS.IND.AC1
    WHILE ACCT.ID:POS.IND.AC1
        CALL F.READ(FN.IND.CI,ACCT.ID,R.IND.CI,F.IND.CI,ER.IND.CI)
        LOOP
            REMOVE CARD.NO FROM R.IND.CI SETTING POS.IND.CI
        WHILE CARD.NO:POS.IND.CI
            CARD.TYPE = FIELD(CARD.NO,'.',1)
****UPDATE BY NESSREEN AHMED 1/10/2019******
****IF CARD.TYPE EQ 'VICL' OR CARD.TYPE EQ 'VIGO' OR CARD.TYPE EQ 'MACL' OR CARD.TYPE EQ 'MAGO' THEN
            IF CARD.TYPE EQ 'VICL' OR CARD.TYPE EQ 'VIGO' OR CARD.TYPE EQ 'MACL' OR CARD.TYPE EQ 'MAGO' OR CARD.TYPE EQ 'MATI' OR CARD.TYPE EQ 'MAPL' OR CARD.TYPE EQ 'MACO' THEN
****END OF UPDATE 1/10/2019*****************
*Line [ 419 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CARD.NO IN CARDS.ARR SETTING POS.CARDS THEN '' ELSE
                    CALL F.READ(FN.CI,CARD.NO,R.CI,F.CI,ER.CI)
                    CUR.ID   = R.CI<CARD.IS.CURRENCY>
                    CI.LCL   = R.CI<CARD.IS.LOCAL.REF>
                    CARD.CODE = CI.LCL<1,LRCI.CARD.CODE>
                    CAT.ID   = '03'
*Line [ 426 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR CARD.CODE:' ' IN VICARD.GRP SETTING GR.POS1 THEN CAT.ID = '03' ELSE NULL
*Line [ 428 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR CARD.CODE:' ' IN VISUPP.GRP SETTING GR.POS1 THEN CAT.ID = '04' ELSE NULL
*Line [ 430 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR CARD.CODE:' ' IN MACARD.GRP SETTING GR.POS1 THEN CAT.ID = '17' ELSE NULL
*Line [ 432 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    FINDSTR CARD.CODE:' ' IN MASUPP.GRP SETTING GR.POS1 THEN CAT.ID = '18' ELSE NULL
                    COMP1.ID = R.CI<CARD.IS.CO.CODE>
                    CARD.EXP = R.CI<CARD.IS.EXPIRY.DATE>
                    CANCEL.DT= R.CI<CARD.IS.CANCELLATION.DATE>
                    IF CANCEL.DT EQ '' THEN
                        CC++
                        CARDS.ARR<CC> = CARD.NO
                        CUST.NO = FMT(CU.ID,"R%8")
                        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR.ID,CUR.NUM)
                        SUB.ID  = FIELD(CARD.NO,'.',2)
                        GOSUB SEND.REC
                    END
                END
            END
        REPEAT
    REPEAT
    RETURN
*=============================================
SEND.REC:
*---------
    II++
    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CAT.ID,CATEG.NAME)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP1.ID,COMP.NAME)

    RET.REC = II:AST:CUST.NO:AST:SUB.ID:AST:CUR.ID:AST:CU.NAME:AST:COMP1.ID:AST:CAT.ID

**    CRT RET.REC

    XX.DATA  = RET.REC
    WRITESEQ XX.DATA TO BB ELSE
        CRT " ERROR WRITE FILE "
    END
    CAT.ID = ''
    CUR.ID = ''
    SUB.ID = ''
    RETURN
*=============================================
END
