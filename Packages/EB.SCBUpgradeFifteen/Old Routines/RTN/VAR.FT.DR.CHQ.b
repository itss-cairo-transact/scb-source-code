* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>187</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.DR.CHQ

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP1 = ID.COMPANY
    IF ( R.NEW(FT.RECORD.STATUS)[1,3] EQ 'INA' OR R.NEW(FT.RECORD.STATUS)[1,3] EQ 'CNA' ) THEN

        F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
        CALL OPF(FN.COUNT,F.COUNT)

        ID.NO = R.NEW(FT.CREDIT.ACCT.NO):".":R.NEW(FT.CHEQUE.NUMBER)
        T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ": ID.NO
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            E = 'THE CHEQUE ISSUED BEFORE' ; CALL ERR; MESSAGE='REPEAT'
        END

        R.COUNT = ''
        R.COUNT<DR.CHQ.COMPANY.CO> = COMP1
        R.COUNT<DR.CHQ.DEBIT.ACCT> = R.NEW(FT.DEBIT.ACCT.NO)
        R.COUNT<DR.CHQ.CHEQ.NO>    = R.NEW(FT.CHEQUE.NUMBER)

        IF R.NEW(FT.DEBIT.AMOUNT) NE '' THEN
            R.COUNT<DR.CHQ.AMOUNT> = R.NEW(FT.DEBIT.AMOUNT)
        END ELSE
            R.COUNT<DR.CHQ.AMOUNT> = R.NEW(FT.CREDIT.AMOUNT)
        END

        R.COUNT<DR.CHQ.CURRENCY> = R.NEW(FT.CREDIT.CURRENCY)
        R.COUNT<DR.CHQ.BEN,1>    = R.NEW(FT.BEN.CUSTOMER)
        R.COUNT<DR.CHQ.BEN,2>    = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1>
        GOSUB CHQ.TYPE

        R.COUNT<DR.CHQ.CHEQ.TYPE>     = FIRST.NO
        R.COUNT<DR.CHQ.NOS.ACCT>      = R.NEW(FT.CREDIT.ACCT.NO)
        R.COUNT<DR.CHQ.TRANS.ISSUE>   = ID.NEW
        R.COUNT<DR.CHQ.CHEQ.STATUS>   = 1
        R.COUNT<DR.CHQ.CHEQ.TYPE>     = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>
        R.COUNT<DR.CHQ.ISSUE.BRN>     = R.NEW(FT.DEPT.CODE)
        R.COUNT<DR.CHQ.CHEQ.DATE>     = TODAY
        R.COUNT<DR.CHQ.OLD.CHEQUE.NO> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>

        CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
        CLOSE F.COUNT
    END
**************************************************************
CHQ.TYPE:
*---------
    FIRST.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>[1,1] 
    RETURN

**************************************************************
    RETURN
END
