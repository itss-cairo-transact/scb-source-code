* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>210</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.OFS.STMP
***    PROGRAM VAR.LG.OFS.STMP


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF  R.NEW(LD.CUSTOMER.ID) NE '99499900'  THEN

        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        GOSUB CREATE.FILE
*******************
***** SCB R15 UPG 20160703 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
*******************
    END
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"

    OFS.OPTIONS = "SCB.STAMP"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

***********************20081112*******************
    IF R.NEW(LD.CHRG.LIQ.ACCT)[9,2] EQ 10 THEN
        DB.ACCT = R.NEW(LD.CHRG.LIQ.ACCT)
    END ELSE

        FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
        F.CUS.ACC  = ""
        R.CUS.ACC = ""
        Y.CUS.ACC.ID   = ""
        CALL OPF( FN.CUS.ACC,F.CUS.ACC)
**** DEBUG

        FN.ACC = "FBNK.ACCOUNT"
        F.ACCOUNT  = ""
        R.ACCOUNT = ""
        CALL OPF( FN.ACC,F.ACCOUNT)
***    Y.ACC.ID = ""
        CUS.ID = R.NEW(LD.CUSTOMER.ID)
****    CUS.ID  = "1302906"
        CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ERR.CUST1)

        LOOP
            REMOVE AC.ID FROM R.CUS.ACC SETTING POS.CUST
        WHILE AC.ID:POS.CUST
            IF AC.ID[9,2] EQ 10 THEN
                CALL F.READ(FN.ACC,AC.ID,R.ACCOUNT,F.ACCOUNT,ERR.CUST)
                IF AC.ID[11,4] EQ  1001 THEN
                    IF R.ACCOUNT<AC.WORKING.BALANCE> GT 1 THEN
                        DB.ACCT = AC.ID
                        RETURN
                    END ELSE
                        DB.ACCT = AC.ID
                        RETURN
                    END
                END
            END
        REPEAT
    END
**************************************************
    RETURN
*----------------------------------------------------
BUILD.RECORD:
*****************

    LOCAL.REF = R.NEW(LD.LOCAL.REF)
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    ORDER.BNK = ID.NEW:'.':MYCODE:'.':MYTYPE
*****************
    COMMA = ","
    DB.AMT = '1'
    DB.CUR = 'EGP'
*****   CR.CUR = R.NEW(LD.CURRENCY)
    PROFT.CUST =  R.NEW(LD.CUSTOMER.ID)
**    DB.DATE = R.NEW(LD.VALUE.DATE)
**    CR.DATA = R.NEW(LD.VALUE.DATE)
    DB.DATE = TODAY
    CR.DATA = TODAY

***   DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
**--------------------HYTHAM ---- 20120326-----------
*   CR.ACCT = 'EGP11023000100':R.USER<EB.USE.COMPANY.CODE,1>[8,2]
    CR.ACCT = 'EGP11023000100':COM.CODE
**--------------------HYTHAM ---- 20120326-----------

    BENF.CUST = 'STAMP'

**    OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.CUST=":PROFT.CUST:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATA:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
**    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":PROFIT.DEPT:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":ORDER.BNK:COMMA
    OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
    OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE':COMMA
    OFS.MESSAGE.DATA := "LOCAL.REF:2:1=":"������":COMMA
**    OFS.MESSAGE.DATA := "BEN.CUSTOMER=":BENF.CUST

    RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(LD.DATE.TIME)

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    OFS.ID = "T":TNO:".":OPERATOR:"_LG":RND(10000):"_":D.T
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    RETURN
*--------------------------------------------------------
END
