* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.BR.INTERNAL.ACCT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

    CURR    = R.NEW(EB.BILL.REG.CURRENCY)
    DEPT.ID = R.USER<EB.USE.DEPARTMENT.CODE>
    IF LEN(DEPT.ID) EQ 1 THEN DEPT.ID = "000":DEPT.ID
    IF LEN(DEPT.ID) EQ 2 THEN DEPT.ID = "00":DEPT.ID
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> EQ "����� �����" THEN
        CATEG = "11311"
    END
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> EQ "�������� �����"  THEN
        CATEG = "11312"
    END
*EGP113110001
*TEXT = CURR:CATEG:DEPT.ID ; CALL REM
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.INTERNAL.ACCT> = CURR:CATEG:DEPT.ID
    CALL REBUILD.SCREEN

    RETURN
END
