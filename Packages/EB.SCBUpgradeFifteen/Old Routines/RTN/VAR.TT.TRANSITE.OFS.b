* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>53</Rating>
*-----------------------------------------------------------------------------
****** NESSREEN AHMED 7/9/2006 ****

    SUBROUTINE VAR.TT.TRANSITE.OFS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD.1
    GOSUB CREATE.FILE
* GOSUB BUILD.RECORD.2
* GOSUB CREATE.FILE
*******************
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
*******************
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>


    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS = "SCB"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD.1:
***********************************
    TRANS.TYPE = 'ACTT'
    DB.CUR = R.NEW(TT.TE.CURRENCY.1)
    IF DB.CUR = LCCY THEN
        DB.AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    END ELSE
        DB.AMT = R.NEW(TT.TE.AMOUNT.FCY.1)
    END
    PROFIT.DEPT = R.NEW(TT.TE.DEPT.CODE)
    DB.DATE = R.NEW(TT.TE.VALUE.DATE.1)
    CR.DATE = R.NEW(TT.TE.VALUE.DATE.1)

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME, PROFIT.DEPT , BRANCH)
    ORDER.BANK = FIELD(BRANCH,'.',2)

    ACCT.1 = R.NEW(TT.TE.ACCOUNT.1)
    TELL.1 = R.NEW(TT.TE.TELLER.ID.1)
    DB.ACCT = ACCT.1[1,6]:TELL.1[1,2]:ACCT.1[9,8]
*   TEXT = 'DB.ACCT=':DB.ACCT ; CALL REM
    CR.ACCT = DB.CUR:'10020':TELL.1
*   TEXT = 'CR.ACCT=':CR.ACCT ; CALL REM
**********************************

    COMMA = ","
    OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":PROFIT.DEPT:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":ORDER.BANK

    RETURN
*-------------------------------------------------------
*-------------------------------------------------------
BUILD.RECORD.2:
************************************
    TRANS.TYPE = 'ACTT'
    DB.CUR = R.NEW(TT.TE.CURRENCY.1)
    DB.AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    PROFIT.DEPT = R.NEW(TT.TE.DEPT.CODE)
    DB.DATE = R.NEW(TT.TE.VALUE.DATE.1)
    CR.DATE = R.NEW(TT.TE.VALUE.DATE.1)
************************************
    COMMA = ","
    OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":PROFIT.DEPT
* OFS.MESSAGE.DATA := "ORDERING.BANK=":ORDER.BANK:COMMA
* OFS.MESSAGE.DATA :=  "LOCAL.REF:14:1=":CHQ.TYPE:COMMA
* OFS.MESSAGE.DATA := "BEN.CUSTOMER=":BENF.CUST

    RETURN

*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(TT.TE.DATE.TIME)

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,"T":TNO:".":'YARABB':"_TT":RND(10000):"_":D.T ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

    RETURN
*--------------------------------------------------------
END
