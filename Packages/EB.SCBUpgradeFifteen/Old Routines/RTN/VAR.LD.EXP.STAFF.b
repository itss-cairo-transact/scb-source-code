* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LD.EXP.STAFF


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.EXP.STAFF
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*------------------------------
    IF R.NEW(LD.INTEREST.SPREAD) THEN
        CUS.ID = R.NEW(LD.CUSTOMER.ID)
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
*************************
        EMP.NO = LOCAL.REF<1,CULR.EMPLOEE.NO>
        TEXT = "EMP" : EMP.NO ; CALL REM

        FN.LD.EXP.STAFF ="F.SCB.LD.EXP.STAFF"

        F.LD.EXP.STAFF = ""
        CALL OPF(FN.LD.EXP.STAFF,F.LD.EXP.STAFF)
        CALL F.READ(FN.LD.EXP.STAFF,EMP.NO,LD.EXP.STAFF.REC,F.LD.EXP.STAFF,E1)

        IF E1 THEN
TEXT = "ZZ " ; CALL REM
            R.LD.EXP.STAFF<LDEXP.CUSTOMER.ID,1> = CUS.ID
            R.LD.EXP.STAFF<LDEXP.LD.NO,1> = ID.NEW
            R.LD.EXP.STAFF<LDEXP.CURRENCY,1> = R.NEW(LD.CURRENCY)
            R.LD.EXP.STAFF<LDEXP.AMOUNT,1> = R.NEW(LD.AMOUNT)
            R.LD.EXP.STAFF<LDEXP.FINAL.DATE,1> = R.NEW( LD.FIN.MAT.DATE)


            CALL F.WRITE(FN.LD.EXP.STAFF,EMP.NO,R.LD.EXP.STAFF)
        END ELSE
*Line [ 65 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            EXP.COUNT = DCOUNT(LD.EXP.STAFF.REC<LDEXP.AMOUNT>,@VM)
            MUL.NO = EXP.COUNT +1
            LD.EXP.STAFF.REC<LDEXP.CUSTOMER.ID,MUL.NO> = CUS.ID
            LD.EXP.STAFF.REC<LDEXP.LD.NO,MUL.NO> = ID.NEW
            LD.EXP.STAFF.REC<LDEXP.CURRENCY,MUL.NO> = R.NEW(LD.CURRENCY)
            LD.EXP.STAFF.REC<LDEXP.AMOUNT,MUL.NO> = R.NEW(LD.AMOUNT)
            LD.EXP.STAFF.REC<LDEXP.FINAL.DATE,MUL.NO> = R.NEW( LD.FIN.MAT.DATE)

            CALL F.WRITE(FN.LD.EXP.STAFF,EMP.NO,LD.EXP.STAFF.REC)
        END
    END
    RETURN
END
