* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LOAN.AC.CROSS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC

    ETEXT =''

    FN.LO = 'F.SCB.LOAN.AC' ; F.LO = '' ; R.LO = ''
    CALL OPF(FN.LO,F.LO)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    REF    = R.NEW(FT.DEBIT.THEIR.REF)
    ACCT   = R.NEW(FT.CREDIT.ACCT.NO)[1,8]
    AC.ID  = ACCT :"-" :REF
    AC.ID2 = ACCT[1,10] : "..." : REF

    IF REF[1,2] EQ "TO" THEN
        T.SEL = "SELECT F.SCB.LOAN.AC WITH @ID LIKE  ": AC.ID2
    END ELSE
        T.SEL = "SELECT F.SCB.LOAN.AC WITH @ID EQ ":AC.ID
    END

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.LO,KEY.LIST,R.LO,F.LO,READ.ERR)
    TEXT = "SELECTED " : SELECTED ; CALL REM
    R.LO<AC.LO.STATUS> = 'MOVED'
    LO.ID = KEY.LIST :";"

*WRITE  R.LO TO F.LO , LO.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":KEY.LIST :"TO" :F.LO
*END

    CALL F.WRITE(FN.LO, LO.ID,R.LO)
    IF REF[1,2] EQ "TO" THEN
        T.SEL.H = "SELECT F.SCB.LOAN.AC WITH STATUS NE 'MOVED' AND @ID LIKE  ": AC.ID2
    END ELSE
        T.SEL.H = "SELECT F.SCB.LOAN.AC WITH STATUS NE 'MOVED' AND @ID EQ ":AC.ID
    END

    CALL EB.READLIST(T.SEL.H,KEY.LIST.H,"",SELECTED.H,ER.MSG.H)
    CALL F.READ(FN.LO,KEY.LIST.H,R.LO,F.LO,READ.ERR)
    TEXT = "SELECTED " : SELECTED.H ; CALL REM

    CALL F.READ(FN.CUR,R.LO<AC.LO.CURRENCY>,R.CUR,F.CUR,READ.ERRRR)

**    RATEE = R.CUR<EB.CUR.MID.REVAL.RATE,1>
    RATEE = R.NEW(FT.CUSTOMER.RATE)
    NEW.AMT = R.LO<AC.LO.AMOUNT> * RATEE
    CALL EB.ROUND.AMOUNT ('EGP',NEW.AMT,'',"")

    R.LO<AC.LO.ACCOUNT.NO>      = R.NEW(FT.DEBIT.ACCT.NO)
    R.LO<AC.LO.ORG.CUR>         = R.LO<AC.LO.CURRENCY>
    R.LO<AC.LO.ORG.AMT>         = R.LO<AC.LO.AMOUNT>
    R.LO<AC.LO.OUTSTAND.AMOUNT> = R.NEW(FT.LOC.AMT.DEBITED)
    R.LO<AC.LO.CURRENCY>        = R.NEW(FT.DEBIT.CURRENCY)
    R.LO<AC.LO.AMOUNT>          = NEW.AMT

*WRITE  R.LO TO F.LO , KEY.LIST.H ON ERROR
*   PRINT "CAN NOT WRITE RECORD":KEY.LIST :"TO" :F.LO
*END
    CALL F.WRITE(FN.LO,KEY.LIST.H,R.LO)
    RETURN
END
