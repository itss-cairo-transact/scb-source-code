* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.MULTI.PAID.OLD.CHQ
** TO WRITE ON TABLE THE PAID OLD CHEQUES NUMBERS
*UPDATED BY NOHA HAMED 50160307

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION = 'A' AND  R.NEW(INF.MLT.RECORD.STATUS)[1,3]='INA' THEN

        FN.P.CHQ = 'F.SCB.P.CHEQ' ; F.P.CHQ = '' ; R.P.CHQ = ''
        ETEXT = ''

*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NO.M = DCOUNT(R.NEW(INF.MLT.CHEQUE.NUMBER),@VM)
        FOR K = 1 TO NO.M
            IF R.NEW(INF.MLT.CHEQUE.NUMBER)<1,K> NE '' THEN
                CHQ.NO  = R.NEW(INF.MLT.CHEQUE.NUMBER)<1,K>
                ACCT.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,K>
*CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,AC.COMP)
                MYBRN  = AC.COMP[8,2]
*-- EDIT BY NESSMA 2016/08/02
                MYBRN = TRIM(MYBRN , "0" , "L")
*-- END EDIT
                OLD.CHQ = CHQ.NO:'.':MYBRN
                CALL OPF( FN.P.CHQ,F.P.CHQ)
                CALL F.READ( FN.P.CHQ,OLD.CHQ, R.P.CHQ, F.P.CHQ, ETEXT)
******UPDATED BY NESSREEN AHMED 24/2/2010*******************************
                IF ETEXT THEN
                    IF R.NEW(INF.MLT.CURRENCY.MARKET)<1,K> NE "EGP" THEN
                        AMT = R.NEW(INF.MLT.AMOUNT.LCY)<1,K>
                    END ELSE
                        AMT = R.NEW(INF.MLT.AMOUNT.FCY)<1,K>
                    END
                    R.P.CHQ<P.CHEQ.CHEQ.VAL> = AMT
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
**  R.P.CHQ<P.CHEQ.TRM.DAT> = R.NEW(INF.MLT.VALUE.DATE)<1,K>
                    R.P.CHQ<P.CHEQ.TRN.DAT> = R.NEW(INF.MLT.VALUE.DATE)<1,K>
**********************************************************************
                    R.P.CHQ<P.CHEQ.ACCOUNT.NO> = ACCT.NO
                    R.P.CHQ<P.CHEQ.OUR.REF> = ID.NEW
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
                    COMP = C$ID.COMPANY
                    R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP
*********************************************************************
                    CALL F.WRITE(FN.P.CHQ,OLD.CHQ,R.P.CHQ)
                END ELSE
                    E = '��� �� ��� ��� ����� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
                END
****
            END
        NEXT K
*******************
    END
    RETURN
END
