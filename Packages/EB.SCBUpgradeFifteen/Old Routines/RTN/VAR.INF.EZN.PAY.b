* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-25</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.INF.EZN.PAY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_INF.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MALIA.EZN
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.TAX
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    COMP1 = ID.COMPANY

    FN.EZN = 'F.SCB.MALIA.EZN' ; F.EZN = '' ; R.EZN = ''
    CALL OPF(FN.EZN,F.EZN)
    FN.TAX = 'F.SCB.CUSTOMER.TAX' ; F.TAX = '' ; R.TAX = ''
    CALL OPF(FN.TAX,F.TAX)

    ACC.NO  = ''
    ACC.CAT = ''
    X1 = ''
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YCOUNT = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
    FOR XX = 1 TO YCOUNT
        ACC.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,XX>
        SIGN   = R.NEW(INF.MLT.SIGN)<1,XX>
        CURR   = R.NEW(INF.MLT.CURRENCY)<1,XX>
**        TEXT = ' ACC = ':ACC.NO ; CALL REM
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,ACC.CAT)
**        TEXT = ' CAT = ':ACC.CAT ; CALL REM
        IF ACC.CAT EQ '11214' AND SIGN EQ 'CREDIT' THEN
            GOSUB EZN.WRITE
        END
        IF ACC.CAT EQ '16523' AND SIGN EQ 'CREDIT' THEN
            IF R.NEW(INF.MLT.LOCAL.REF)<1,INLR.SUPP.CODE> THEN
                GOSUB TAX.WRITE
            END ELSE
                E = ' ��� ����� ��� ������ '
            END
        END
        ACC.NO = ''
    NEXT XX
    RETURN
**************************************************************
EZN.WRITE:
**********
* UPDATE THE TABLE SCB.MALIA.EZN
    ID.NO = R.NEW(INF.MLT.BILL.NO)
**    TEXT = 'EZN = ':ID.NO ; CALL REM
    CALL F.READ(FN.EZN,ID.NO,R.EZN,F.EZN,ERR.EZN)
    IF NOT(R.EZN) THEN
        E = ' ��������� ��� ���� ����� '
    END
*Line [ 79 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.M = DCOUNT(R.EZN<EZN.PAY.DATE>,@VM)
    X1    = NO.M + 1
    R.EZN<EZN.PAY.DATE,X1>     = TODAY
    R.EZN<EZN.TXN.REF,X1>      = ID.NEW
    IF CURR EQ 'EGP' THEN
        R.EZN<EZN.AMOUNT.PAID,X1>  = R.NEW(INF.MLT.AMOUNT.LCY)<1,XX>
        AMT    =  R.EZN<EZN.OUTSTAND.AMOUNT> - R.NEW(INF.MLT.AMOUNT.LCY)<1,XX>
    END ELSE
        R.EZN<EZN.AMOUNT.PAID,X1>  = R.NEW(INF.MLT.AMOUNT.FCY)<1,XX>
        AMT    =  R.EZN<EZN.OUTSTAND.AMOUNT> - R.NEW(INF.MLT.AMOUNT.FCY)<1,XX>
    END
    IF AMT NE 0 THEN
        E = ' ������ �� ����� ���� ����� '
    END ELSE
        R.EZN<EZN.STATUS>      = 'P'
        IF CURR EQ 'EGP' THEN
            R.EZN<EZN.OUTSTAND.AMOUNT> =  R.EZN<EZN.OUTSTAND.AMOUNT> - R.NEW(INF.MLT.AMOUNT.LCY)<1,XX>
        END ELSE
            R.EZN<EZN.OUTSTAND.AMOUNT> =  R.EZN<EZN.OUTSTAND.AMOUNT> - R.NEW(INF.MLT.AMOUNT.FCY)<1,XX>
        END
        CALL F.WRITE(FN.EZN,ID.NO,R.EZN)
**    CLOSE F.EZN
    END
    RETURN
**************************************************************
TAX.WRITE:
*---------
    IF LEN(XX) EQ 1 THEN
        TTX = '0':XX
    END
    ID.TAX = 'TX':ID.NEW[3,10]:TTX
    R.TAX<CUS.TAX.CUSTOMER.ID>      = R.NEW(INF.MLT.LOCAL.REF)<1,INLR.SUPP.CODE>
    R.TAX<CUS.TAX.TOTAL.AMOUNT>     = R.NEW(INF.MLT.GL.NUMBER)<1,XX>
    R.TAX<CUS.TAX.DISCOUNT.PERCENT> = R.NEW(INF.MLT.DISC.PERCENT)<1,XX>
    R.TAX<CUS.TAX.DISCOUNT.AMOUNT>  = R.NEW(INF.MLT.AMOUNT.LCY)<1,XX>
    R.TAX<CUS.TAX.VALUE.DATE>       = R.NEW(INF.MLT.VALUE.DATE)<1,XX>
    R.TAX<CUS.TAX.CO.CODE>          = COMP1
    CALL F.WRITE(FN.TAX,ID.TAX,R.TAX)
    CLOSE F.TAX
    RETURN
**************************************************************
END
