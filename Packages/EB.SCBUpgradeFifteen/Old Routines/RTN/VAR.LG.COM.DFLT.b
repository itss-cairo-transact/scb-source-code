* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.LG.COM.DFLT
*    PROGRAM VNC.LG.COM.DFLT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.COM

    IF V$FUNCTION EQ 'A' THEN
        ID.LG.ALL = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED,1>
        ID.LG = FIELD(ID.LG.ALL,'-',1)
        ID.LG.OLD = FIELD(ID.LG.ALL,'-',2)

        FN.LD='F.LD.LOANS.AND.DEPOSITS';F.LD='';R.LD=''
        CALL F.READ(FN.LD,ID.LG,R.LD,F.LD, ETEXT)
        LG.TYPS = R.LD<LD.LOCAL.REF,LDLR.PRODUCT.TYPE>
        LG.CUR =  R.LD<LD.CURRENCY>
******************************
        FN.LG.COM = 'F.SCB.LG.COM';F.LG.COM='';R.LG.COM=''

*Line [ 46 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        REC.COUNT   = DCOUNT(R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED>,@SM)

*   REC.COUNT= REC.COUNT - 1
        FOR I = 2  TO REC.COUNT
            IF R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED,I> NE '0' THEN
                AMT.DAT.1 = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED,I>
                LG.COM.DAT.1=FIELD(AMT.DAT.1,'-',2)
                LG.ID.COM = ID.LG:'-':LG.COM.DAT.1
                CALL F.READ(FN.LG.COM,LG.ID.COM,R.LG.COM,F.LG.COM, ETEXT1)
                TEXT= LG.ID.COM:'-COM';CALL REM
                R.LG.COM<LG.DATE>        =   LG.COM.DAT.1
                R.LG.COM<LG.DR.ACCT>     =   R.NEW(FT.DEBIT.ACCT.NO)
                R.LG.COM<LG.TYP>        =   LG.TYPS
                R.LG.COM<LG.CURRENCY>    =   R.NEW(FT.DEBIT.CURRENCY)
                R.LG.COM<LG.CR.ACCT>     =   R.NEW(FT.CREDIT.ACCT.NO)
                R.LG.COM<LG.OLD.NO>      =   ID.LG.OLD
**************
*Line [ 64 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DAYS.COUNT   = DCOUNT(R.LG.COM<LG.FT.DATE>,@VM)
                TEXT=DAYS.COUNT:'-COUNT';CALL REM
*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DAYS.COUNT   = DCOUNT(R.LG.COM<LG.FT.DATE>,@VM)
                IF DAYS.COUNT GE '1' THEN
                    IF R.LG.COM<LG.FT.DATE,DAYS.COUNT> EQ TODAY  THEN
                        DAYS.COUNT = DAYS.COUNT
                    END ELSE
                        DAYS.COUNT += 1
                    END
                END ELSE
                    DAYS.COUNT = 1
                END
*****************
*Line [ 79 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                FT.COUNT   = DCOUNT(R.LG.COM<LG.FT.AMT,DAYS.COUNT>,@SM)
                IF FT.COUNT GE '1' THEN
                    FT.COUNT +=1
                END ELSE
                    FT.COUNT = 1
                END

*****************
                R.LG.COM<LG.FT.AMT,DAYS.COUNT,FT.COUNT>  = R.NEW(FT.DEBIT.AMOUNT)
                R.LG.COM<LG.FT.ID,DAYS.COUNT,FT.COUNT>  = ID.NEW
                AMT.DAT.1=R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED,I>
                LG.COM.AMT.1=FIELD(AMT.DAT.1,'-',1)
                R.LG.COM<LG.COM.YEAR.AMT,DAYS.COUNT,FT.COUNT> =LG.COM.AMT.1
                R.LG.COM<LG.FT.DATE,DAYS.COUNT> = TODAY
                TEXT=DAYS.COUNT:'-COUNT';CALL REM
*****************
                R.LG.COM<LG.CO.CODE> = ID.COMPANY
                CALL F.WRITE(FN.LG.COM,LG.ID.COM,R.LG.COM)
            END
        NEXT I
*****************
    END
    RETURN
END
