* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* Create by Nessma 2011/03/15
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.BR.UPDATE.BS

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*---------------------------------------------
    FN.BS = "FBNK.SCB.BR.SLIPS"    ; F.BS = ""
    CALL OPF( FN.BS,F.BS)

    FN.BR = "FBNK.BILL.REGISTER$NAU"   ; F.BR = ""
    CALL OPF(FN.BR, F.BR)

    FN.BR.2 = "FBNK.BILL.REGISTER"   ; F.BR.2 = ""
    CALL OPF(FN.BR.2, F.BR.2)
*-----------------------------------------------
    BR.NUMBER  = ID.NEW
    BS.NUMBER  = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.SLIP.REFER>
    BR.POS     = ''

    CALL F.READ( FN.BS,BS.NUMBER, R.SLIPS, F.BS, ETEXT)

*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BR.CC  = DCOUNT(R.SLIPS<SCB.BS.BR.REFRENCE>,@VM)
    FOR KK = 1 TO BR.CC
        IF R.SLIPS<SCB.BS.BR.REFRENCE,KK> EQ ID.NEW THEN
            BR.POS = KK
        END
    NEXT KK

    IF BR.POS NE '' THEN
**** LOCATE BR.NUMBER IN R.SLIPS<SCB.BS.BR.REFRENCE> SETTING BR.POS THEN
        NEXT.CHRG = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
        R.SLIPS<SCB.BS.BR.CHRG2.AMT,BR.POS> = NEXT.CHRG
    END ELSE
*-- CALC CHRG2 FIRST
*-- ADD MULTI
*Line [ 69 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NNN = DCOUNT(R.SLIPS<SCB.BS.BR.REFRENCE>,@VM)
        NNN = NNN + 1
        R.SLIPS<SCB.BS.BR.REFRENCE,NNN>  = BR.NUMBER

        IF  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> NE '' THEN
            R.SLIPS<SCB.BS.BR.CHRG2.AMT,NNN> = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
        END ELSE
*COUNT PL
            R.SLIPS<SCB.BS.BR.CHRG2.AMT,NNN> = 0
        END
    END

    CALL F.WRITE(FN.BS,BS.NUMBER,R.SLIPS)

*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BR.COUNT = DCOUNT(R.SLIPS<SCB.BS.BR.REFRENCE>,@VM)
    TOT.AMT  = 0
    BR.THFOZ = 0
    COUNT.PL = 0

    FOR NN = 1 TO BR.COUNT
        BR.REF = R.SLIPS<SCB.BS.BR.REFRENCE,NN>
        CALL F.READ( FN.BR,BR.REF, R.BR, F.BR, ETEXT)
        IF NOT(ETEXT) THEN
            AMT.REF   = R.BR<EB.BILL.REG.AMOUNT>
            BR.CHRG.2 = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CH.CCY.AMT>
        END ELSE
            CALL F.READ( FN.BR.2,BR.REF, R.BR, F.BR.2, ETEXT)
            AMT.REF   = R.BR<EB.BILL.REG.AMOUNT>
            BR.CHRG.2 = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CH.CCY.AMT>
        END

        IF NN NE BR.POS THEN
            IF R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CH.CCY.AMT> GT 0 THEN
                COUNT.PL = COUNT.PL + 1
            END
        END

        IF NN EQ BR.POS THEN
            AMT.REF   = 0
            BR.CHRG.2 = 0
        END

        TOT.AMT  = TOT.AMT  + AMT.REF
        BR.THFOZ = BR.THFOZ + BR.CHRG.2

    NEXT NN
    TOT.AMT  = TOT.AMT  + R.NEW(EB.BILL.REG.AMOUNT)
    BR.THFOZ = BR.THFOZ + R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>

    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> GT 0 THEN
        COUNT.PL = COUNT.PL + 1
    END

    R.SLIPS<SCB.BS.REG.AMT>          = R.SLIPS<SCB.BS.TOTAL.AMT> - TOT.AMT
    R.SLIPS<SCB.BS.REG.NO>           = R.SLIPS<SCB.BS.TOTAL.NO>  - BR.COUNT
    R.SLIPS<SCB.BS.TOT.CHRG2.AMT>    = BR.THFOZ
***---------HYTHAM & NESMAAA  20111219-------------
*    FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' ; E11=''
*    CALL OPF(FN.BR.COM,F.BR.COM)
*    BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
*    CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)
*    IF NOT(E11) THEN
*        FEES   = R.BR.COM<BR.CUS.KEEP.FEES>
*    END 
*    IF FEES EQ '' THEN
*        CHRG = 3
*    END ELSE
*        CHRG = FEES
*    END
*    COUNT.PL = COUNT.PL * CHRG
***---------HYTHAM & NESMAAA  20111219-------------
    R.SLIPS<SCB.BS.BR.COUNT.PL>      = BR.THFOZ
    R.SLIPS<SCB.BS.BR.COUNT.INT.ACC> = ""

    CALL F.WRITE(FN.BS,BS.NUMBER,R.SLIPS)

*-------------------------------------------
    RETURN
END
