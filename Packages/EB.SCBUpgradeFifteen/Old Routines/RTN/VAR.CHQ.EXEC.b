* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CHQ.EXEC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP

    FN.CHQ = 'F.SCB.CHEQ.APPL' ; F.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    FN.STP = 'FBNK.PAYMENT.STOP' ; F.STP = ''
    CALL OPF(FN.STP,F.STP)

    WS.CHQ.TYPE.ALL = R.NEW(AC.PAY.PAYM.STOP.TYPE)
*Line [ 38 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.OF.TYPE      = DCOUNT(WS.CHQ.TYPE.ALL,@VM)

    FOR X = 1 TO NO.OF.TYPE
        WS.CHQ.START = R.NEW(AC.PAY.FIRST.CHEQUE.NO)<1,X>
        WS.CHQ.TYPE  = R.NEW(AC.PAY.PAYM.STOP.TYPE)<1,X>
        IF WS.CHQ.TYPE EQ '9' THEN

            T.SEL = "SELECT F.SCB.CHEQ.APPL WITH CHQ.NO.START EQ ":WS.CHQ.START
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    CALL F.READ(FN.CHQ,KEY.LIST<I>,R.CHQ,F.CHQ,E2)

                    R.CHQ<CHQA.CHEQ.KIND> = '9'
                    R.CHQ<CHQA.SEND.TO.CUST.DATE> = TODAY

                    CALL F.WRITE(FN.CHQ,KEY.LIST<I>,R.CHQ)

                NEXT I
            END
        END

    NEXT X

    RETURN
END
