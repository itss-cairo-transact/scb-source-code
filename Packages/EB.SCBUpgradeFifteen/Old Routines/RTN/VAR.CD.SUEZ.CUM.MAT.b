* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CD.SUEZ.CUM.MAT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER



******

    NEW.FILE ="SCCD.MAT.21101":ID.NEW
    OPENSEQ "SCCD" , NEW.FILE TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"SCCD":' ':NEW.FILE
        HUSH OFF
    END
    OPENSEQ "SCCD" , NEW.FILE TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCCD.MAT.21101 CREATED IN SCCD'
        END ELSE
            STOP 'Cannot create SCCD.CUM.MAT.99 File IN SCCD'
        END
    END
******
    OFS.MESSAGE.DATA = ""

***********
    DB.ACCT=R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>

    CR.ACCT =R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>
    AMT= R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT>
    MAT.DATE =TODAY
***********
    COMMA = ","
*    CALL F.READ('F.LD.LOANS.AND.DEPOSITS$NAU',ID.NEW,R.TERM,VF.LD.LOANS.AND.DEPOSITS,ER.MSG)
*******************************************************
    COMMA = ","
    LD.CODE = R.NEW(LD.CO.CODE)
    IDD = 'FUNDS.TRANSFER,SCCD.NONCUM,AUTO.CHRGE//':LD.CODE:','

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"ACSZ":','
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":','
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":"EGP":','
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:','
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:','
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:','
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":MAT.DATE:','
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":MAT.DATE:','
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ID.NEW:','
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":ID.NEW:','
    OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
    OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":','
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB.21101':','


*************
    MSG.DATA = IDD:",":OFS.MESSAGE.DATA
    WRITESEQ MSG.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*** COPY TO OFS **
    EXECUTE 'COPY FROM SCCD TO OFS.IN ':NEW.FILE
    EXECUTE 'DELETE ':"SCCD":' ':NEW.FILE




*************
    RETURN
END
