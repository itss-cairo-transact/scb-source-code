* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CHQ.CHARGE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.CHARGE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*----------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN
*----------------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "CHQ.CHARGE"
*------------------------------------------

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.CA = 'F.SCB.CHEQ.APPL' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CH = 'FBNK.CHEQUE.CHARGE' ; F.CH = ''
    CALL OPF(FN.CH,F.CH)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA   = ","
    V.DATE  = TODAY

*************************************************

    CALL F.READ(FN.CH,'SCB',R.CH,F.CH,E1)
    ACCT.NO.CR   = 'PL':R.CH<CHEQUE.CHG.PL.CATEGORY>
    COM.AMT      = R.CH<CHEQUE.CHG.ISSUE.CHG.AMT>
    CUS.NO       = R.NEW(CHQA.CUST.NO)
    ACCT.NO.DR   = R.NEW(CHQA.DB.ACCT)
    WS.CHQ.PAPER = R.NEW(CHQA.CHEQ.TYPE)
    WS.NO.BOOKS  = R.NEW(CHQA.NO.OF.BOOKS)

    AMT = WS.CHQ.PAPER * WS.NO.BOOKS * COM.AMT

    CALL F.READ(FN.CU,CUS.NO,R.CU,F.CU,E2)
    CUST.SECTOR.CODE = R.CU<EB.CUS.SECTOR>
    CUST.TARGET.CODE = R.CU<EB.CUS.TARGET>
    WS.ID.SER = ID.NEW[3]

* ETEXT = CUST.TARGET.CODE,"-"WS.ID.SER"
* CALL STORE.END.ERROR
*========
    DB.AMT1 = AMT
*========= ����� 50% ��������
    IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1300 OR CUST.SECTOR.CODE EQ 1400 THEN
        DB.AMT1 = AMT / 2
    END
*========= ����� 100% ��� ������ ����� ������ ����
    IF CUST.TARGET.CODE EQ 5700 AND WS.ID.SER EQ '001' AND WS.CHQ.PAPER EQ 25 THEN
       * AMT = WS.CHQ.PAPER * (WS.NO.BOOKS - 0.5) * COM.AMT
         AMT = WS.CHQ.PAPER * (WS.NO.BOOKS - 1) * COM.AMT
        DB.AMT1 = AMT
    END
*========= ����� 50% ����� �������
 *   IF CUST.TARGET.CODE EQ 5800 OR CUST.TARGET.CODE EQ 5850 CUST.TARGET.CODE EQ 5900 THEN
 *       AMT = WS.CHQ.PAPER * (WS.NO.BOOKS - 0.5) * COM.AMT
 *       DB.AMT1 = AMT
 *   END
*======================================

    CALL F.READ(FN.AC,ACCT.NO.DR,R.AC,F.AC,E3)
    DB.CUR = R.AC<AC.CURRENCY>
    CR.CUR = R.AC<AC.CURRENCY>
    CALL F.READ(FN.CUR,DB.CUR,R.CUR,F.CUR,E5)
    IF DB.CUR NE 'EGP' THEN
        WS.RATE = R.CUR<EB.CUR.BUY.RATE,1>
    END ELSE
        WS.RATE = 1
    END
    DB.AMT = DB.AMT1 / WS.RATE
    DB.AMT = DROUND(DB.AMT,'2')

    COMP   = R.AC<AC.CO.CODE>
    OFS.USER.INFO    = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
*------------------------------------------
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC54":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":DB.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":ACCT.NO.DR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":ACCT.NO.CR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ID.NEW:COMMA

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY
    OFS.ID = ID.NEW:"-":DAT

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

    RETURN
************************************************************
END
