* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.ACH.EXCP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.EXCP.RATE

    T.DATE      = TODAY
    EXCP.RATE   = R.NEW(ACH.EXCP.EXCP.RATE)
    FLAT.RATE   = R.NEW(ACH.EXCP.EXCP.FLAT.RATE)
    START.DATE  = R.NEW(ACH.EXCP.EXCP.START.DATE)
    EXPIRE.DATE = R.NEW(ACH.EXCP.EXCP.EXPIRE.DATE)
    MIN.AMT     = R.NEW(ACH.EXCP.EXCP.MIN.AMT)
    MAX.AMT     = R.NEW(ACH.EXCP.EXCP.MAX.AMT)
    IF V$FUNCTION = 'I' THEN
        CALL DBR("ACCOUNT":@FM:AC.CATEGORY,LIQ.ACCT,CATEG)
        IF (EXCP.RATE AND FLAT.RATE) THEN
            E = "��� ����� ������ �� ������ ���������� ��� "
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR

        END
        IF (MIN.AMT GE MAX.AMT) THEN
            E = "���������� ��� �� ���� ��� �� ������ "
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
        END
        IF (START.DATE LE T.DATE OR EXPIRE.DATE LE T.DATE) THEN
            E = "��� �� ���� ������� ���� �� ����� �����"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
        END
        IF (EXCP.RATE EQ '' AND FLAT.RATE EQ '') THEN
            E = " ��� ����� ������ �� ������ ����������"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR

        END
        IF (EXCP.RATE AND (MIN.AMT EQ '' OR MAX.AMT EQ '')) THEN
            E = "��� ����� ���� ������ ������� ������"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR

        END
    END
**********************************************************************
    RETURN
END
