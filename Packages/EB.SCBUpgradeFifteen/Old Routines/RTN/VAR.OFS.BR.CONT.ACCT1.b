* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.OFS.BR.CONT.ACCT1

*   TO CREATE CONTEINGENT ACCOUNT FOR BANK

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.OFS.SOURCE
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_F.NUMERIC.CURRENCY
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT           I_AC.LOCAL.REFS
    $INSERT           I_F.SCB.BR.SLIPS
    $INSERT           I_OFS.SOURCE.LOCAL.REFS
*----------------------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
    RETURN
*----------------------------------------------------
INITIALISE:
*-----------
    FN.OFS.SOURCE     = "F.OFS.SOURCE"
    F.OFS.SOURCE      = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK         = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN          = 0
    F.OFS.BK          = 0
    OFS.REC           = ""
    OFS.OPERATION     = "ACCOUNT"
    OFS.OPTIONS       = ""
    COMP              = ID.COMPANY
    COM.CODE          = COMP[8,2]
    OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.TRANS.ID      = ""
    OFS.MESSAGE.DATA  = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:
*------------
    COMMA    = ","
    DAT      = TODAY
    CURR     = R.NEW(SCB.BS.CURRENCY)


    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN

        DEPT = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BANK.ACCT = CURR:"190070001":DEPT
        CALL DBR('ACCOUNT':@FM:0,BANK.ACCT,BANK.ACCT.EXIST)
        IF NOT(BANK.ACCT.EXIST) THEN
            OFS.TRANS.ID = CURR:"190070001":DEPT
            OFS.MESSAGE.DATA = "MNEMONIC=":CURR:COM.CODE
            DAT = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "T":TNO:".":OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160703 - E

        END
    END


    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����" THEN

        DEPT = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BANK.ACCT = CURR:"190020001":DEPT
        CALL DBR('ACCOUNT':@FM:0,BANK.ACCT,BANK.ACCT.EXIST)
        IF NOT(BANK.ACCT.EXIST) THEN
            OFS.TRANS.ID = CURR:"190020001":DEPT
            OFS.MESSAGE.DATA = "MNEMONIC=":CURR:"CHQ1"
            DAT = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "T":TNO:".":OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160703 - E

        END
    END

    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����" THEN

        DEPT = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BANK.ACCT = CURR:"190030001":DEPT
        CALL DBR('ACCOUNT':@FM:0,BANK.ACCT,BANK.ACCT.EXIST)
        IF NOT(BANK.ACCT.EXIST) THEN
            OFS.TRANS.ID = CURR:"190030001":DEPT
            OFS.MESSAGE.DATA = "MNEMONIC=":CURR:"BILL1"
            DAT = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "T":TNO:".":OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160703 - E

        END
    END

    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ���� �����" THEN
        DEPT = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BANK.ACCT = CURR:"190050001":DEPT
        CALL DBR('ACCOUNT':@FM:0,BANK.ACCT,BANK.ACCT.EXIST)
        IF NOT(BANK.ACCT.EXIST) THEN
            OFS.TRANS.ID = CURR:"190050001":DEPT
            OFS.MESSAGE.DATA = "MNEMONIC=":CURR:"CHQ2"
            DAT = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "T":TNO:".":OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160703 - E

        END
    END

    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����" THEN

        DEPT = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BANK.ACCT = CURR:"190060001":DEPT
        CALL DBR('ACCOUNT':@FM:0,BANK.ACCT,BANK.ACCT.EXIST)
        IF NOT(BANK.ACCT.EXIST) THEN
            OFS.TRANS.ID = CURR:"190060001":DEPT
            OFS.MESSAGE.DATA = "MNEMONIC=":CURR:"BILL2"
            DAT = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "T":TNO:".":OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160703 - E

        END
    END

    FINDSTR '������' IN R.NEW(SCB.BS.OPERATION.TYPE) SETTING FMS,VMS THEN
        VV = "����� ������ ��� ������"
        DEPT = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        BANK.ACCT = CURR:"190090001":DEPT
        CALL DBR('ACCOUNT':@FM:0,BANK.ACCT,BANK.ACCT.EXIST)
        IF NOT(BANK.ACCT.EXIST) THEN

            OFS.TRANS.ID       = CURR:"190090001":DEPT
            OFS.MESSAGE.DATA   ="CATEGORY=19009":COMMA
            OFS.MESSAGE.DATA  :="MNEMONIC=":CURR:"BILL9":COMMA
            OFS.MESSAGE.DATA  :="CURRENCY=":CURR:COMMA
            OFS.MESSAGE.DATA  :="ACCOUNT.OFFICER=":COM.CODE:COMMA
            OFS.MESSAGE.DATA  :="ACCOUNT.TITLE.1=":VV:COMMA
            OFS.MESSAGE.DATA  :="SHORT.TITLE=":VV

            DAT     = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID  = "T":TNO:".":OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160703 - E

        END
    END ELSE
    END

    RETURN

***** SCB R15 UPG 20160703 - S
*--
OPM.PROCESS:
*--

*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*
    RETURN
***** SCB R15 UPG 20160703 - E
**********************************************************************************************************************
END
