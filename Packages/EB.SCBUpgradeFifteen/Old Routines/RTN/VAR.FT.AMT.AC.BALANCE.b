* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
***----INGY 18/04/2005----***

    SUBROUTINE VAR.FT.AMT.AC.BALANCE

*1-IF THE INPUT VALUE IS NOT EQ DEBIT AMOUNT THEN MAKE CREDIT.AMOUNT IS EMPTY
*2-CHECK IF WORKING.BALANCE HAVE A CREDIT BALANCE

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

* IF COMI # R.NEW(FT.DEBIT.AMOUNT) THEN
*  R.NEW(FT.CREDIT.AMOUNT) = ''
*  R.NEW(FT.COMMISSION.AMT)= ''
*  CALL REBUILD.SCREEN
* END
    IF R.NEW(FT.AMOUNT.DEBITED) THEN
        DEBIT.ACC = R.NEW(FT.DEBIT.ACCT.NO)
        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,DEBIT.ACC,WORK.BALANCE)
* TEXT ="FT.AMOUNT=":R.NEW(FT.AMOUNT.DEBITED)[4,LEN(R.NEW(FT.AMOUNT.DEBITED))] ;CALL REM
* TEXT ="WORK.BALANCE=":WORK.BALANCE ;CALL REM

        IF R.NEW(FT.AMOUNT.DEBITED)[4,LEN(R.NEW(FT.AMOUNT.DEBITED))] GT WORK.BALANCE THEN
           * TEXT ="FT.AMOUNT=":R.NEW(FT.AMOUNT.DEBITED) ;CALL REM
           * TEXT ="WORK.BALANCE=":WORK.BALANCE ;CALL REM
            TEXT = '���� ������ �� ����' ; CALL REM
            CALL FATAL.ERROR("AMOUNT IS NOT AVILABLE")
* AF= FT.AMOUNT.DEBITED ; AV=1
        END
    END
    RETURN
END
