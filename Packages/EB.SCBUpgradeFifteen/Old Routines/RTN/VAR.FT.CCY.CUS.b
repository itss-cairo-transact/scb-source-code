* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>631</Rating>             HYTHAM & M.ELSAYED
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.CCY.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.CCY.CUS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS


    FN.CCY   = 'F.SCB.FT.CCY.CUS'    ; F.CCY   = '' ; R.CCY  = ''
    CALL OPF(FN.CCY,F.CCY)
    KEY.LIST= "" ; SELECTED = "" ; ER.MSG = ""

    CUS       = R.NEW(FT.CREDIT.CUSTOMER)
    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='INA' THEN

*  TEXT.IN.OUT = ''
        ETEXT   =''

        CALL F.READ(FN.CUS,CUS,R.CUS,F.CUS,E)
        E = ''
        IF R.CUS<EB.CUS.NATIONALITY> EQ 'EG' THEN
            IF R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR> EQ '4650' THEN

                CALL DBR ('ACCOUNT':@FM: AC.ACCOUNT.TITLE.1,CUS.ACT,DR.NAME)
                CALL DBR ('ACCOUNT':@FM: AC.ACCOUNT.TITLE.1,CR.ACT,CR.NAME)


                CUS.ACT   = R.NEW(FT.CREDIT.ACCT.NO)
                CUR       = R.NEW(FT.CREDIT.CURRENCY)
                CR.ACT    = R.NEW(FT.CREDIT.ACCT.NO)
                REF       = ID.NEW
                REC.STATUS = R.NEW(FT.RECORD.STATUS)
**-----------------------------------
                COM.TYPE  = R.NEW(FT.COMMISSION.CODE)
*  TEXT      = "COM.TYPE   " : COM.TYPE ; CALL REM
                AMT       = R.NEW(FT.AMOUNT.CREDITED)[4,10]

                TOT.CHRG  = R.NEW(FT.TOTAL.CHARGE.AMOUNT)[4,10]

                COM.CR    = 'CREDIT LESS CHARGE'
* TEXT = "COM   " : COM.CR ; CALL REM
                IF COM.TYPE EQ COM.CR THEN
*TEXT = "YES" ; CALL REM
                    IF R.NEW(FT.AMOUNT.CREDITED)[1,3] EQ R.NEW(FT.TOTAL.CHARGE.AMOUNT)[1,3] THEN
                        AMT   = AMT + TOT.CHRG
                    END
                END

**----------------------------
                FT.TYPE   = "IN"
                BEN.CUS   = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT>

                IN.OUT    = R.NEW(FT.LOCAL.REF)<1,FTLR.INN.OUTT>

                IF IN.OUT EQ 'IN' THEN
                    IN.OUT.TXT = 'IN'
                END
                IF IN.OUT EQ 'OUT' THEN
                    IN.OUT.TXT = 'OUT'

                END

                R.CCY<CUS.CCY.CUS.NO>          = CUS
                R.CCY<CUS.CCY.CURRENCY>        = CUR
                R.CCY<CUS.CCY.ACCOUNT.NO>      = CUS.ACT
                R.CCY<CUS.CCY.AMOUNT>          = AMT
                R.CCY<CUS.CCY.OUR.REF>         = REF
                R.CCY<CUS.CCY.BOOK.DATE>       = TODAY
                R.CCY<CUS.CCY.FLAG>            = FT.TYPE
                R.CCY<CUS.CCY.COMP.CODE>       = ID.COMPANY
                R.CCY<CUS.CCY.CREDIT.AC.NAME>  = CR.NAME
                R.CCY<CUS.CCY.DEBIT.AC.NAME>  = DR.NAME
                R.CCY<CUS.CCY.BEN.CUS>        = BEN.CUS
                R.CCY<CUS.CCY.IN.OUT>         = IN.OUT.TXT

                IDD.1     = CUS:"-":CUR:"-":REF

*WRITE  R.CCY TO F.CCY , IDD.1  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":IDD.1:"TO" :F.CCY
*END
                CALL F.WRITE(FN.CCY,IDD.1, R.CCY)
                R.CCY = ''

                IDD.2 = CUS:"-":CUR
                CALL F.READ(FN.CCY,IDD.2,R.CCY,F.CCY,E)

                IF NOT(E) THEN
                    AMT.OLD = R.CCY<CUS.CCY.TOT.AMT>
                END

                IF FT.TYPE EQ 'IN'  THEN

                    NEW.AMT  = AMT.OLD + AMT
                END

                IF FT.TYPE EQ 'OUT'  THEN
                    NEW.AMT  = AMT.OLD - AMT
                END

                R.CCY<CUS.CCY.TOT.AMT>         = NEW.AMT
                R.CCY<CUS.CCY.CUS.NO>          = CUS
                R.CCY<CUS.CCY.CURRENCY>        = CUR
                R.CCY<CUS.CCY.FLAG>            = 'TOT'

*WRITE  R.CCY TO F.CCY , IDD.2  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":IDD.2:"TO" :F.CCY
*END
                CALL F.WRITE(FN.CCY,IDD.2, R.CCY)
            END
        END
    END
*======*
* REVE *
*======*
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='RNA' THEN

        CALL F.READ(FN.CUS,CUS,R.CUS,F.CUS,E)

        AMT       = R.NEW(FT.AMOUNT.CREDITED)[4,10]
        REF       = ID.NEW
        CUR       = R.NEW(FT.CREDIT.CURRENCY)


        IDD.1     = CUS:"-":CUR:"-":REF
        CALL F.READ(FN.CCY,IDD.1,R.CCY,F.CCY,E)

        R.CCY<CUS.CCY.REC.STATUS> = 'REVE'


*WRITE  R.CCY TO F.CCY , IDD.1  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":IDD.1:"TO" :F.CCY
*END
        CALL F.WRITE(FN.CCY,IDD.1, R.CCY)
        R.CCY = ''

        CUS.ACT   = R.NEW(FT.CREDIT.ACCT.NO)

        IDD.2 = CUS:"-":CUR
        TEXT = IDD.2 ; CALL REM

        CALL F.READ(FN.CCY,IDD.2,R.CCY,F.CCY,E)

        IF NOT(E) THEN
            AMT.OLD = R.CCY<CUS.CCY.TOT.AMT>
            TEXT = AMT.OLD ; CALL REM
        END

        FT.TYPE   = "IN"

        IF FT.TYPE EQ 'IN'  THEN
            NEW.AMT  = AMT.OLD - AMT
        END

        IF FT.TYPE EQ 'OUT'  THEN
            NEW.AMT  = AMT.OLD + AMT
        END


        TEXT = NEW.AMT ; CALL REM

        R.CCY<CUS.CCY.TOT.AMT>         = NEW.AMT
        R.CCY<CUS.CCY.CUS.NO>          = CUS
        R.CCY<CUS.CCY.CURRENCY>        = CUR
        R.CCY<CUS.CCY.FLAG>            = 'TOT'


*WRITE  R.CCY TO F.CCY , IDD.2  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":IDD.2:"TO" :F.CCY
*END
        CALL F.WRITE(FN.CCY,IDD.2, R.CCY)
    END

    TEXT = "AMTT=  " :  R.CCY<CUS.CCY.TOT.AMT>  ; CALL REM

    E = ''
    RETURN
END
