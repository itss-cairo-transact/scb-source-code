* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.USER.PRINTER.99

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.USER.SIGN.ON.NAME
*------------------------------------------------
    FN.SGN = "F.USER.SIGN.ON.NAME" ; F.SGN = ""
    CALL OPF(FN.SGN, F.SGN)

    FN.USR = "F.USER"              ; F.USR = ""
    CALL OPF(FN.USR, F.USR)
*------------------------------------------------
    IF V$FUNCTION = 'A' THEN
        TEXT = R.USER<EB.USE.PRINTER.FOR.RPTS> ; CALL REM
        TEXT = R.USER<EB.USE.SIGN.ON.NAME>     ; CALL REM

        USR.SGN.ID  = R.USER<EB.USE.SIGN.ON.NAME>
        CALL F.READ(FN.SGN,USR.SGN.ID,R.SGN,F.SGN,ER.SGN)
        USR.FILE.ID = R.SGN<EB.USO.USER.ID>

        CALL F.READ(FN.USR,USR.FILE.ID,R.USERR,F.USR,ER.USR)
        IF R.USERR<EB.USE.PRINTER.FOR.RPTS> EQ '' THEN
            E = '�� ���� ������� ����� ����� �������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*------------------------------------------------
    RETURN
END
