* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
*-- EDIT BY NESSMA & MENNA
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SF.KEEP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.EXPIRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*--------------------------------------------
    FN.SF = 'F.SCB.SAFEKEEP' ; R.SF=''      ; F.SF=''
    CALL OPF(FN.SF,F.SF)

    FN.AC = "FBNK.ACCOUNT" ; F.AC = ""
    CALL OPF(FN.AC, F.AC)

    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='INAU' THEN
        ID.SF = R.NEW(SCB.SF.TR.BB.SAFF.KEEP.NO)
        CALL F.READ(FN.SF,ID.SF,R.SF,F.SF,E1)

*Line [ 50 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NN = DCOUNT(R.SF<SCB.SAF.CUSTOMER.NO.2>,@VM)
        NN = NN + 1

        R.SF<SCB.SAF.KEY.CLOSE.DATE,NN> =  TODAY
        R.SF<SCB.SAF.TRANS.FT.REF.2,NN> =  ID.NEW
        R.SF<SCB.SAF.CUSTOMER.NO.2,NN>  =  R.NEW(SCB.SF.TR.CUSTOMER.NO)
        R.SF<SCB.SAF.SAFF.USED>         =  'NO'
        R.SF<SCB.SAF.LEASING.DATE>      =  ''
        R.SF<SCB.SAF.CUSTOMER.NO>       =  ''

        CALL F.WRITE (FN.SF, ID.SF,R.SF)
*--------------------------------------------------------
        ACC.ID = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        CALL F.READ(FN.AC,ACC.ID,R.AC,F.AC,E.AC)
*   CUS.AC = R.AC<AC.CUSTOMER>
        CUS.AC = R.NEW(SCB.SF.TR.CUSTOMER.NO)
        FN.TMP = "F.SCB.SF.EXPIRY" ; F.TMP = ""
        CALL OPF(FN.TMP, F.TMP)
        IF LEN(CUS.AC) EQ 7 THEN
            CUS.AC = "0":CUS.AC
        END

        NN.SEL = "SELECT F.SCB.SF.EXPIRY WITH @ID LIKE ":CUS.AC:"..."
        CALL EB.READLIST(NN.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            TEXT = "SELECTED = ":SELECTED
            FOR NN = 1 TO SELECTED
                CALL F.READ(FN.TMP,KEY.LIST<NN>,R.TMP,F.TMP,E.TMP)
                R.TMP<SF.EXP.STOP> = "NO"
                CALL F.WRITE(FN.TMP,KEY.LIST<NN>,R.TMP)
            NEXT NN
        END
    END
*---
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='RNAU' THEN
        ID.SF = R.NEW(SCB.SF.TR.BB.SAFF.KEEP.NO)
        CALL F.READ(FN.SF,ID.SF,R.SF,F.SF,E1)

*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NN = DCOUNT(R.SF<SCB.SAF.CUSTOMER.NO.2>,@VM)
        FOR II = 1 TO NN
            SF.REC = R.SF<SCB.SAF.TRANS.FT.REF.2,II>

            IF SF.REC EQ ID.NEW THEN
                DEL R.SF<SCB.SAF.TRANS.FT.REF.2,II>
                DEL R.SF<SCB.SAF.KEY.CLOSE.DATE,II>
                DEL R.SF<SCB.SAF.CUSTOMER.NO.2,II>

                CALL F.WRITE(FN.SF,ID.SF,R.SF)

            END
        NEXT II

        CUS.ID = R.NEW(SCB.SF.TR.CUSTOMER.NO)
        RENT.ID = CUS.ID :".": ID.SF

        FN.SF.R = "F.SCB.SAFEKEEP.RIGHT"   ; F.SF.R = ""
        CALL OPF(FN.SF.R,F.SF.R)
        CALL F.READ(FN.SF.R,RENT.ID,R.SF.R,F.SF.R,E1.R)
        RENT.DATE = R.SF.R<SCB.SAF.RIG.VALUE.DATE>

        R.SF<SCB.SAF.SAFF.USED>    =  'YES'
        R.SF<SCB.SAF.LEASING.DATE> =  RENT.DATE
        R.SF<SCB.SAF.CUSTOMER.NO>  =  CUS.ID

        CALL F.WRITE(FN.SF,ID.SF,R.SF)
    END
*------------------------
    RETURN
END
