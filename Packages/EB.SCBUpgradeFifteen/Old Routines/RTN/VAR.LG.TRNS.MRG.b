* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
****** WAEL ****
*-----------------------------------------------------------------------------
* <Rating>1161</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.TRNS.MRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


******************
    MESSAGE  = ''
    ERR.MESS = ''
******************************
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
***************************************CASE 9090*****
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,1> GT '0'  THEN
        FLG.AMT.MRG = '1'
    END ELSE
        FLG.AMT.MRG = '0'
    END
****************************************************
    FN.ACC= 'FBNK.ACCOUNT'; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    AC.ID = R.NEW(LD.CHRG.LIQ.ACCT)
    CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,E.AC)
    CATG = R.ACC<AC.CATEGORY>
    IF CATG  EQ '9090' OR CATG  EQ '9091' THEN
        IF FLG.AMT.MRG EQ '1' THEN
            IF V$FUNCTION EQ 'I' THEN
*------------------------  INP  ----------------------
                IF  R.NEW(LD.RECORD.STATUS)[1,3] EQ 'INA' THEN
                    MULTI.ENTRIES = ''
                    GOSUB AC.STMT.ENTRY
                    TYPE = 'DEL'
                    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
                    IF END.ERROR OR TEXT = "NO" THEN RETURN
                END
                MULTI.ENTRIES = ''
                GOSUB AC.STMT.ENTRY
                TYPE = 'VAL'
                CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
                IF END.ERROR OR TEXT = "NO" THEN RETURN
            END
*------------------------  DEL  ----------------------
            IF V$FUNCTION EQ 'D' THEN
                MULTI.ENTRIES = ''
                GOSUB AC.STMT.ENTRY
                TYPE = 'DEL'
                CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
                IF END.ERROR OR TEXT = "NO" THEN RETURN
            END
*------------------------  AUT ----------------------
            IF V$FUNCTION EQ 'A' THEN
                TYPE = 'AUT'
                CALL EB.ACCOUNTING("SYS",TYPE, "" , "" )
                IF END.ERROR OR TEXT = "NO" THEN RETURN
            END
        END
    END
    RETURN
*------------------------------
AC.STMT.ENTRY:

*Line [ 101 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    FOR K = 1 TO DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE>,@SM)
        AMT    = R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT,K>
*************
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.CODE,K> EQ '14' THEN
            TRNS.CODE.CR ='50'
            TRNS.CODE.DR = '49'
        END ELSE
            TRNS.CODE.CR ='875'
            TRNS.CODE.DR = '875'
        END
***************
        IF AMT NE '' THEN
            CURR   = R.NEW(LD.CURRENCY)
            IF CURR NE 'EGP' THEN
                FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
                CALL OPF(FN.CUR,F.CUR)
                CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
                RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
            END
*--------
*   CR
*--------
            IF CURR NE 'EGP' THEN
                LCY.AMT = AMT * RATE
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMT
            END ELSE
                LCY.AMT = AMT
                FCY.AMT = ''
            END
            IF CATG  EQ '9090' THEN
                Y.ACCT.CR = CURR:'19090000100':ID.COMPANY[8,2]
            END ELSE
                IF CATG  EQ '9091' THEN
                    Y.ACCT.CR = CURR:'19091000100':ID.COMPANY[8,2]
                END
            END
            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,Y.ACCT,CUS.ACC)
            PL.CAT = ''
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATG)
            CATEG  = CATG
            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)

            ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.CR
            ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
            ENTRY<AC.STE.TRANSACTION.CODE> = TRNS.CODE.CR
            ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
            ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
            ENTRY<AC.STE.NARRATIVE>        = ""
            ENTRY<AC.STE.PL.CATEGORY>      = PL.CAT
            ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
            ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
            ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
            ENTRY<AC.STE.VALUE.DATE>       = TODAY
            ENTRY<AC.STE.CURRENCY>         = CURR
            ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
            ENTRY<AC.STE.EXCHANGE.RATE>    = ""
            ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
            ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
            ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
            ENTRY<AC.STE.SYSTEM.ID>        = "LD"
            ENTRY<AC.STE.BOOKING.DATE>     = TODAY
            ENTRY<AC.STE.CRF.TYPE>         = ""
            ENTRY<AC.STE.CRF.TXN.CODE>     = ""
            ENTRY<AC.STE.CRF.MAT.DATE>     = ""
            ENTRY<AC.STE.CHQ.TYPE>         = ""
            ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
            ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
            ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
            ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
            ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

            MULTI.ENTRIES<-1> = LOWER(ENTRY)
            ENTRY = ""

*-------
*  DR
*-------
** Y.ACCT = R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
            Y.ACCT.DR=AC.ID
            IF CURR NE 'EGP' THEN
                LCY.AMT = (AMT * RATE ) * -1
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMT * -1
            END ELSE
                LCY.AMT = AMT * -1
                FCY.AMT = ''
            END
            PL.CAT = ''

            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATG)
            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,Y.ACCT,ACC.OFFICER)
            CATEG  = CATG
*------------------
            ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.DR
            ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
            ENTRY<AC.STE.TRANSACTION.CODE> = TRNS.CODE.DR
            ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
            ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
            ENTRY<AC.STE.NARRATIVE>        = ""
            ENTRY<AC.STE.PL.CATEGORY>      = PL.CAT
            ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
            ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
            ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
            ENTRY<AC.STE.VALUE.DATE>       = TODAY
            ENTRY<AC.STE.CURRENCY>         = CURR
            ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
            ENTRY<AC.STE.EXCHANGE.RATE>    = ""
            ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
            ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
            ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
            ENTRY<AC.STE.SYSTEM.ID>        = "LD"
            ENTRY<AC.STE.BOOKING.DATE>     = TODAY
            ENTRY<AC.STE.CRF.TYPE>         = ""
            ENTRY<AC.STE.CRF.TXN.CODE>     = ""
            ENTRY<AC.STE.CRF.MAT.DATE>     = ""
            ENTRY<AC.STE.CHQ.TYPE>         = ""
            ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
            ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
            ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
            ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
            ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

            MULTI.ENTRIES<-1> = LOWER(ENTRY)
        END
    NEXT K
****************************************************************************
    RETURN
END
