* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.OFS.BR.CONT.ACCT

*   TO CREATE CONTEINGENT ACCOUNT FOR CUSTOMER

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.OFS.SOURCE
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.CUSTOMER.ACCOUNT
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT           I_F.SCB.BR.SLIPS
    $INSERT           I_OFS.SOURCE.LOCAL.REFS
    $INSERT           I_AC.LOCAL.REFS
*----------------------------------------------------------------------*
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
    RETURN
*----------------------------------------------------------------------*
INITIALISE:
*----------
    FN.OFS.SOURCE     = "F.OFS.SOURCE"
    F.OFS.SOURCE      = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK         = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN          = 0
    F.OFS.BK          = 0
    OFS.REC           = ""
    OFS.OPERATION     = "ACCOUNT"
    OFS.OPTIONS       = "BR"
    COMP              = ID.COMPANY
    COM.CODE          = COMP[8,2]
    OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.TRANS.ID      = ""
    OFS.MESSAGE.DATA  = ""

    FN.ACC ='FBNK.ACCOUNT'              ; F.ACC   = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACC.H ='FBNK.ACCOUNT$HIS'        ; F.ACC.H = ''
    CALL OPF(FN.ACC.H,F.ACC.H)

    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT'  ; F.CUS.AC = ''
    CALL OPF(FN.CUS.AC,F.CUS.AC)
    RETURN
*----------------------------------------------------------------------*
CHECK.HIS:
*---------
    CHECK.ACCT = NEW.CUST.ACCT

    FOR NN = 1 TO 100
        CALL F.READ(FN.ACC.H,CHECK.ACCT:";1",R.ACC.H,F.ACC.H,ERR.H)
        IF ERR.H EQ "" THEN
            AC.SERL ++
            IF LEN(AC.SERL) EQ 1 THEN
                NEW.CUST.ACCT = CHECK.ACCT[1,15]:AC.SERL
            END
            IF LEN(AC.SERL) EQ 2 THEN
                NEW.CUST.ACCT = CHECK.ACCT[1,14]:AC.SERL
            END
            CHECK.ACCT = NEW.CUST.ACCT
        END
    NEXT NN
    NEW.CUST.ACCT = CHECK.ACCT
    RETURN
*----------------------------------------------------------------------*
BUILD.RECORD:
*------------
    COMMA       = ","
    DAT         = TODAY
    AC.SERL     = 1
    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(SCB.BS.CURRENCY),CURR)
**-----------------------------9007------------------------------------*
    CURS = R.NEW(SCB.BS.CURRENCY)

    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
        IF LEN(R.NEW(SCB.BS.DRAWER)) EQ 7 THEN
            CUSS = "0":R.NEW(SCB.BS.DRAWER)
        END ELSE
            CUSS = R.NEW(SCB.BS.DRAWER)
        END

        IF R.NEW(SCB.BS.DRAWER)  = '99433300' THEN
            CUST.ACCT = CUSS:CURR:"9007":COM.CODE
            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            NEW.CUST.ACCT = CUST.ACCT
        END ELSE
            CUST.ACCT = CUSS:CURR:"9007":"01"
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
            IF AC.COMP  = COMP  THEN
                CUST.ACCT.EXIST = ''
            END ELSE
*-
                FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
                CALL OPF(FN.CUS.AC,F.CUS.AC)
                CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
                LOOP
                    REMOVE ACC FROM R.CUS.AC  SETTING POS1
                WHILE ACC:POS1
                    CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                    CATEG     = R.ACC<AC.CATEGORY>
                    CURRR     = R.ACC<AC.CURRENCY>
                    IF ( CURRR EQ CURS AND CATEG EQ 9007 ) THEN
                        AC.SERL ++
                        IF R.ACC<AC.CO.CODE> EQ COMP THEN
                            CUST.ACCT.EXIST   = ''
                            RETURN
                        END
                    END
                REPEAT
*-
                CUST.ACCT.EXIST   = 'XXX'
            END
        END
        IF CUST.ACCT.EXIST THEN
            NEW.AC.SELR   = AC.SERL

            IF LEN(NEW.AC.SELR) EQ 1 THEN
                NEW.CUST.ACCT = CUSS:CURR:"90070":NEW.AC.SELR
            END ELSE
                IF LEN(NEW.AC.SELR) EQ 2 THEN
                    NEW.CUST.ACCT = CUSS:CURR:"9007":NEW.AC.SELR
                END
            END

            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            GOSUB CHECK.HIS

            OFS.TRANS.ID  = NEW.CUST.ACCT
            DAT           = TODAY

            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA
            OFS.ID  = OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
        END
    END
**-----------------------------9007------------------------------------*

**-----------------------------9002------------------------------------*
    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����" THEN
        IF LEN(R.NEW(SCB.BS.DRAWER)) EQ 7 THEN
            CUSS = "0":R.NEW(SCB.BS.DRAWER)
        END ELSE
            CUSS = R.NEW(SCB.BS.DRAWER)
        END
        IF R.NEW(SCB.BS.DRAWER)  = '99433300' THEN
            CUST.ACCT = CUSS:CURR:"9002":COM.CODE
            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            NEW.CUST.ACCT = CUST.ACCT
        END ELSE
            CUST.ACCT = CUSS:CURR:"9002":"01"
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
            IF AC.COMP  = COMP  THEN
                CUST.ACCT.EXIST = ''
            END ELSE
*-
                FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
                CALL OPF(FN.CUS.AC,F.CUS.AC)
                CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
                LOOP
                    REMOVE ACC FROM R.CUS.AC  SETTING POS1
                WHILE ACC:POS1
                    CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                    CATEG     = R.ACC<AC.CATEGORY>
                    CURRR     = R.ACC<AC.CURRENCY>
                    IF ( CURRR EQ CURS AND CATEG EQ 9002 ) THEN
                        AC.SERL ++

                        IF R.ACC<AC.CO.CODE> EQ COMP THEN
                            CUST.ACCT.EXIST   = ''
                            RETURN
                        END
                    END
                REPEAT
*-
                CUST.ACCT.EXIST   = 'XXX'
            END
        END
        IF CUST.ACCT.EXIST THEN
            NEW.AC.SELR   = AC.SERL
            IF LEN(NEW.AC.SELR) EQ 1 THEN
                NEW.CUST.ACCT = CUSS:CURR:"90020":NEW.AC.SELR
            END ELSE
                IF LEN(NEW.AC.SELR) EQ 2 THEN
                    NEW.CUST.ACCT = CUSS:CURR:"9002":NEW.AC.SELR
                END
            END

            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            GOSUB CHECK.HIS
            OFS.TRANS.ID  = NEW.CUST.ACCT

            DAT     = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA
            OFS.ID  =  OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
        END
    END
**-----------------------------9002------------------------------------*

**-----------------------------9003------------------------------------*

    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����" THEN
        IF LEN(R.NEW(SCB.BS.DRAWER)) EQ 7 THEN
            CUSS = "0":R.NEW(SCB.BS.DRAWER)
        END ELSE
            CUSS = R.NEW(SCB.BS.DRAWER)
        END
        IF R.NEW(SCB.BS.DRAWER)  = '99433300' THEN
            CUST.ACCT = CUSS:CURR:"9003":COM.CODE
            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            NEW.CUST.ACCT = CUST.ACCT
        END ELSE
            CUST.ACCT = CUSS:CURR:"9003":"01"
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
            IF AC.COMP  = COMP  THEN
                CUST.ACCT.EXIST = ''
            END ELSE
*-
                FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
                CALL OPF(FN.CUS.AC,F.CUS.AC)

                CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
                LOOP
                    REMOVE ACC FROM R.CUS.AC  SETTING POS1
                WHILE ACC:POS1
                    CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                    CATEG     = R.ACC<AC.CATEGORY>
                    CURRR     = R.ACC<AC.CURRENCY>
                    IF ( CURRR EQ CURS AND CATEG EQ 9003 ) THEN
                        AC.SERL ++

                        IF R.ACC<AC.CO.CODE> EQ COMP THEN
                            CUST.ACCT.EXIST   = ''
                            RETURN
                        END
                    END
                REPEAT
*-
                CUST.ACCT.EXIST   = 'XXX'
            END
        END
        IF CUST.ACCT.EXIST THEN
            NEW.AC.SELR  = AC.SERL
            IF LEN(NEW.AC.SELR) EQ 1 THEN
                NEW.CUST.ACCT = CUSS:CURR:"90030":NEW.AC.SELR
            END ELSE
                IF LEN(NEW.AC.SELR) EQ 2 THEN
                    NEW.CUST.ACCT = CUSS:CURR:"9003":NEW.AC.SELR
                END
            END

            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            GOSUB CHECK.HIS
            OFS.TRANS.ID  = NEW.CUST.ACCT

            DAT     = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA
            OFS.ID  = OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
        END
    END
**-----------------------------9003------------------------------------*

**-----------------------------9005------------------------------------*

    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ���� �����" THEN
        IF LEN(R.NEW(SCB.BS.DRAWER)) EQ 7 THEN
            CUSS = "0":R.NEW(SCB.BS.DRAWER)
        END ELSE
            CUSS = R.NEW(SCB.BS.DRAWER)
        END

        IF R.NEW(SCB.BS.DRAWER)  = '99433300' THEN
            CUST.ACCT = CUSS:CURR:"9005":COM.CODE
            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            NEW.CUST.ACCT = CUST.ACCT
        END ELSE
            CUST.ACCT = CUSS:CURR:"9005":"01"
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
            IF AC.COMP  = COMP  THEN
                CUST.ACCT.EXIST = ''
            END ELSE
*-
                FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
                CALL OPF(FN.CUS.AC,F.CUS.AC)
                CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
                LOOP
                    REMOVE ACC FROM R.CUS.AC  SETTING POS1
                WHILE ACC:POS1
                    CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                    CATEG     = R.ACC<AC.CATEGORY>
                    CURRR     = R.ACC<AC.CURRENCY>
                    IF ( CURRR EQ CURS AND CATEG EQ 9005 ) THEN
                        AC.SERL ++

                        IF R.ACC<AC.CO.CODE> EQ COMP THEN
                            CUST.ACCT.EXIST   = ''
                            RETURN
                        END
                    END
                REPEAT
*-
                CUST.ACCT.EXIST   = 'XXX'
            END
        END
        IF CUST.ACCT.EXIST THEN
            NEW.AC.SELR  = AC.SERL
            IF LEN(NEW.AC.SELR) EQ 1 THEN
                NEW.CUST.ACCT = CUSS:CURR:"90050":NEW.AC.SELR
            END ELSE
                IF LEN(NEW.AC.SELR) EQ 2 THEN
                    NEW.CUST.ACCT = CUSS:CURR:"9005":NEW.AC.SELR
                END
            END

            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            GOSUB CHECK.HIS
            OFS.TRANS.ID  = NEW.CUST.ACCT

            DAT     = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA
            OFS.ID  =  OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
        END
    END
**-----------------------------9005------------------------------------*
**-----------------------------9006------------------------------------*
    IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����" THEN
        IF LEN(R.NEW(SCB.BS.DRAWER)) EQ 7 THEN
            CUSS = "0":R.NEW(SCB.BS.DRAWER)
        END ELSE
            CUSS = R.NEW(SCB.BS.DRAWER)
        END

        IF R.NEW(SCB.BS.DRAWER)  = '99433300' THEN
            CUST.ACCT = CUSS:CURR:"9006":COM.CODE
            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            NEW.CUST.ACCT = CUST.ACCT
        END ELSE
            CUST.ACCT = CUSS:CURR:"9006":"01"
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
            IF AC.COMP  = COMP  THEN
                CUST.ACCT.EXIST = ''
            END ELSE
*-
                FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
                CALL OPF(FN.CUS.AC,F.CUS.AC)
                CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
                LOOP
                    REMOVE ACC FROM R.CUS.AC  SETTING POS1
                WHILE ACC:POS1
                    CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                    CATEG     = R.ACC<AC.CATEGORY>
                    CURRR     = R.ACC<AC.CURRENCY>
                    IF ( CURRR EQ CURS AND CATEG EQ 9006 ) THEN
                        AC.SERL ++
                        IF R.ACC<AC.CO.CODE> EQ COMP THEN
                            CUST.ACCT.EXIST   = ''
                            RETURN
                        END
                    END
                REPEAT
*-
                CUST.ACCT.EXIST   = 'XXX'
            END
        END
        IF CUST.ACCT.EXIST THEN
            NEW.AC.SELR = AC.SERL
            IF LEN(NEW.AC.SELR) EQ 1 THEN
                NEW.CUST.ACCT = CUSS:CURR:"90060":NEW.AC.SELR
            END ELSE
                IF LEN(NEW.AC.SELR) EQ 2 THEN
                    NEW.CUST.ACCT = CUSS:CURR:"9006":NEW.AC.SELR
                END
            END

            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            GOSUB CHECK.HIS
            OFS.TRANS.ID  = NEW.CUST.ACCT

            DAT     = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA
            OFS.ID  = OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
        END
    END
**-----------------------------9006------------------------------------*
**-----------------------------9009------------------------------------*
    FINDSTR '������' IN R.NEW(SCB.BS.OPERATION.TYPE) SETTING FMS,VMS THEN
        IF LEN(R.NEW(SCB.BS.DRAWER)) EQ 7 THEN
            CUSS = "0":R.NEW(SCB.BS.DRAWER)
        END ELSE
            CUSS = R.NEW(SCB.BS.DRAWER)
        END

        IF R.NEW(SCB.BS.DRAWER)  = '99433300' THEN
            CUST.ACCT = CUSS:CURR:"9009":COM.CODE
            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            NEW.CUST.ACCT = CUST.ACCT
        END ELSE
            CUST.ACCT = CUSS:CURR:"9009":"01"
            CALL DBR('ACCOUNT':@FM:AC.CO.CODE,CUST.ACCT,AC.COMP)
            IF AC.COMP  = COMP  THEN
                CUST.ACCT.EXIST = ''
            END ELSE
*-
                FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
                CALL OPF(FN.CUS.AC,F.CUS.AC)
                CALL F.READ( FN.CUS.AC,R.NEW(SCB.BS.DRAWER), R.CUS.AC, F.CUS.AC,ETEXT1)
                LOOP
                    REMOVE ACC FROM R.CUS.AC  SETTING POS1
                WHILE ACC:POS1
                    CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                    CATEG     = R.ACC<AC.CATEGORY>
                    CURRR     = R.ACC<AC.CURRENCY>
                    IF ( CURRR EQ CURS AND CATEG EQ 9009 ) THEN
                        AC.SERL ++
                        IF R.ACC<AC.CO.CODE> EQ COMP THEN
                            CUST.ACCT.EXIST   = ''
                            RETURN
                        END
                    END
                REPEAT
*-
                CUST.ACCT.EXIST   = 'XXX'
            END
        END
        IF CUST.ACCT.EXIST THEN
            TEXT = NEW.AC.SELR ; CALL REM
            NEW.AC.SELR = AC.SERL
            IF LEN(NEW.AC.SELR) EQ 1 THEN
                NEW.CUST.ACCT = CUSS:CURR:"90090":NEW.AC.SELR
            END
            IF LEN(NEW.AC.SELR) EQ 2 THEN
                NEW.CUST.ACCT = CUSS:CURR:"9009":NEW.AC.SELR
            END

            CALL DBR('ACCOUNT':@FM:0,CUST.ACCT,CUST.ACCT.EXIST)
            GOSUB CHECK.HIS
            OFS.TRANS.ID  = NEW.CUST.ACCT

            DAT     = TODAY
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA
            OFS.ID  = OFS.TRANS.ID:"-":DAT
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160703 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
        END
    END
    RETURN
**-----------------------------9009------------------------------------*
END
