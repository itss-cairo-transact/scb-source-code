* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-34</Rating>
*-----------------------------------------------------------------------------
********* WAEL   01-04-2003**************

SUBROUTINE VAR.LG.CONFIS

* TO MAKE AN FT TRANSACTION TO TRANSFER MONEY FROM
* CURRENCT.ACCT OF THE CUSTOMER TO MARGIN.ACCT
* ACCORDING TO THE TYPE OF CHANGE

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

  * YTEXT = "PLEASE INPUTT BENEFICIARY'S NAME: "
  * CALL TXTINP(YTEXT, 8, 22, "35", "A")
*BENEF = COMI
******IF ETEXT DO NOT PROCEED

*YTEXT = "PLEASE INPUTT CHEQUE NUMBER : "
*CALL TXTINP(YTEXT, 8, 22, "10", "")
**********IF ETEXT DO NOT PROCEED

*********EXTRA CHECK: SEE IF IT IS A VALID CHEQUE IN CHEQUE.ISSUE
* CHECK.NO = COMI
* YTEXT = "YOU HAVE INSERTED: BENEFICIARY NAME IS: ":BENEF:" AND CHECK.NO: ":CHECK.NO:" IS THIS INFORMATION CORRECT Y_NO"
* CALL TXTINP(YTEXT,8,22,2,FM:'Y_NO')

******IF ETEXT THEN EXIT IF NO ALSO EXIT
*******IF YES PROCEED

X = ""
X = 5
  IF X = 5 THEN
   * IF R.NEW(LD.FIN.MAT.DATE) = TODAY THEN

 *  IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONFISC.AMT> = R.NEW(LD.AMOUNT) THEN
  IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONFISC.AMT> < R.NEW(LD.AMOUNT) THEN
       AMT     = "5555.00" ;*R.NEW(LD.AMOUNT)
       DB.XX   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
       CR.XX   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>

       CHK.AMT = "5000.00" ;*R.NEW(LD.AMOUNT)
       CHK.DB  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
       CHK.CR  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>

       GOSUB TRANSFER
       GOSUB DRAFT.CHK

   END ELSE

       AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
       DB.XX   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
       CR.XX   = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>

       CHK.AMT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>
       CHK.DB  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
       CHK.CR  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>

       GOSUB TRANSFER
       GOSUB DRAFT.CHK
   END
END ELSE

       PER = R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>/100

        AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>*PER
        DB.XX   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
        CR.XX   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>

        CHK.AMT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>
        CHK.DB  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
        CHK.CR  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>

        GOSUB TRANSFER
        GOSUB DRAFT.CHK

END

RETURN
*=======================================================================
TRANSFER:

DIM R.FT(FT.AUDIT.DATE.TIME)

   MAT R.FT = "" ; ER.MSG = "" ; ID.FT = "" ; W.STATUS = ""

   R.FT(FT.TRANSACTION.TYPE)    = "ACLG"
    R.FT(FT.DEBIT.AMOUNT)        = AMT
     R.FT(FT.DEBIT.CURRENCY)      = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
      R.FT(FT.CREDIT.CURRENCY)     = R.NEW( LD.LOCAL.REF)< 1,LDLR.ACC.CUR>
       R.FT(FT.PROFIT.CENTRE.CUST)  = R.NEW(LD.CUSTOMER.ID)
        R.FT(FT.DEBIT.VALUE.DATE)    = R.NEW(LD.VALUE.DATE)
         R.FT(FT.CREDIT.VALUE.DATE)   = R.NEW(LD.VALUE.DATE)
          R.FT(FT.DEBIT.ACCT.NO)       = DB.XX
           R.FT(FT.CREDIT.ACCT.NO)      = CR.XX
           TEXT = "BAKRY= ":R.FT(FT.DEBIT.AMOUNT) ; CALL REM
   CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
   IF W.STATUS # "L" THEN
      IF NOT(ER.MSG) THEN
         ER.MSG = "ERROR CREATING FT"
      END
   END
TEXT = "MARGIN TRANSACTION " ; CALL REM
RETURN
*=======================================================================
DRAFT.CHK:
*DEBUG
DIM R.FT(FT.AUDIT.DATE.TIME)
   MAT R.FT = ""
   ER.MSG = ""
   ID.FT = ""
   W.STATUS = ""

   R.FT(FT.TRANSACTION.TYPE)    = "OD"
    R.FT(FT.DEBIT.AMOUNT)       = CHK.AMT
     R.FT(FT.DEBIT.CURRENCY)     = R.NEW(LD.CURRENCY)
      R.FT(FT.CREDIT.CURRENCY)    = R.NEW( LD.CURRENCY)
       R.FT(FT.PROFIT.CENTRE.CUST) = R.NEW(LD.CUSTOMER.ID)
        R.FT(FT.DEBIT.VALUE.DATE)   = R.NEW(LD.VALUE.DATE)
         R.FT(FT.CREDIT.VALUE.DATE)  = R.NEW(LD.VALUE.DATE)
          R.FT(FT.DEBIT.ACCT.NO)      = "0120001010100201"  ;*CHK.DB
           R.FT(FT.CREDIT.ACCT.NO)     = "0140001010200001" ;*CHK.CR
           R.FT(FT.CHEQUE.NUMBER)      = 2
           R.FT(FT.BEN.CUSTOMER)       = "MYLDKSLFK"
   CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
   IF W.STATUS # "L" THEN
      IF NOT(ER.MSG) THEN
         ER.MSG = "ERROR CREATING FT"
      END
   END
TEXT = "CHECK TRANSACTION " ; CALL REM
RETURN
*=======================================================================

END
