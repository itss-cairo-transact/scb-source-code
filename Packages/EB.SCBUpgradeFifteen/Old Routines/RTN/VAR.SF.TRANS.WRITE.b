* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SF.TRANS.WRITE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*----------------------------------------
    FN.SF = 'F.SCB.SAFEKEEP'      ; F.SF = ''
    CALL OPF(FN.SF,F.SF)

    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='INAU' THEN
        IF PGM.VERSION NE ',SCB.SF.TR.MARGIN' THEN
            ID.SF = R.NEW(SCB.SF.TR.BB.SAFF.KEEP.NO)

            CALL F.READ(FN.SF,ID.SF,R.SF,F.SF,E1)
*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NN = DCOUNT(R.SF<SCB.SAF.CUSTOMER.NO.1>,@VM)
            NN = NN + 1

            R.SF<SCB.SAF.TRANS.FT.REF.1,NN> = ID.NEW
            R.SF<SCB.SAF.RENEWAL.DATE,NN>   = TODAY
            R.SF<SCB.SAF.CUSTOMER.NO.1,NN>  = R.NEW(SCB.SF.TR.CUSTOMER.NO)

            R.SF<SCB.SAF.SAFF.USED>         = 'YES'

            CALL F.WRITE(FN.SF,ID.SF,R.SF)
        END
    END
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='RNAU' THEN
        ID.SF = R.NEW(SCB.SF.TR.BB.SAFF.KEEP.NO)
        CALL F.READ(FN.SF,ID.SF,R.SF,F.SF,E1)

*Line [ 59 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NN = DCOUNT(R.SF<SCB.SAF.CUSTOMER.NO.1>,@VM)
        FOR II = 1 TO NN
            SF.REC = R.SF<SCB.SAF.TRANS.FT.REF.1,II>
            IF SF.REC EQ ID.NEW THEN
                DEL R.SF<SCB.SAF.TRANS.FT.REF.1,II>
                DEL R.SF<SCB.SAF.RENEWAL.DATE,II>
                DEL R.SF<SCB.SAF.CUSTOMER.NO.1,II>

                CALL F.WRITE(FN.SF,ID.SF,R.SF)
            END
        NEXT II
    END
*----------------------------------------
    RETURN
END
