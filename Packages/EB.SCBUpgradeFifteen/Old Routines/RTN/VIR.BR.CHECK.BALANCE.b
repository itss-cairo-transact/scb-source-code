* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VIR.BR.CHECK.BALANCE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*------------------------------------------------
    IF V$FUNCTION = 'I' THEN
        LIQ.ACCT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,LIQ.ACCT,CATEG)

        IF CATEG EQ '6501' OR CATEG EQ '6502' OR CATEG EQ '6503' OR CATEG EQ '6504' OR CATEG EQ '6511' OR CATEG EQ '1001' THEN
            CALL DBR ('ACCOUNT':@FM:AC.WORKING.BALANCE,LIQ.ACCT,ACCT.BAL)
            BR.AMT   = R.NEW(EB.BILL.REG.AMOUNT)
            BR.CHRG  = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
            BR.COM   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
            BR.LEN   = LEN(BR.COM)
            BR.COM   = BR.COM[4,BR.LEN]
            TOT.AMT  = BR.COM + BR.CHRG
            ETEXT    = ""
            E        = ""

            IF TOT.AMT GT 0 THEN
                IF TOT.AMT GT ACCT.BAL THEN
                    E = '������ �� ����'
                    CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR
                END
            END
        END
    END
*---------------------------------------------------------------
    RETURN
END
