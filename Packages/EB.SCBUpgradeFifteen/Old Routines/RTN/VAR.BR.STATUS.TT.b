* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>370</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BR.STATUS.TT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.STATUS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.TT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS

    IF V$FUNCTION   = 'I' THEN
        DCOUNTBR.ID = ""
        NUMBERBR.ID = ""
*Line [ 53 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(BT.TT.OUR.REFERENCE),@VM)
        FOR NUMBERBR.ID = 1 TO DCOUNTBR.ID

            FN.BR  = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
            CALL OPF( FN.BR,F.BR)
            BR.ID = R.NEW(BT.TT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF NOT(ETEXT) THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>     = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  = 15
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>   = TODAY
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>     = TODAY
                CALL F.WRITE(FN.BR,BR.ID,R.BR)
            END
        NEXT NUMBERBR.ID

    END
    IF V$FUNCTION = 'R' THEN
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTBR.ID = DCOUNT (R.NEW(BT.TT.OUR.REFERENCE),@VM)
        FOR NUMBERBR.ID = 1 TO DCOUNTBR.ID

            FN.BR  = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
            CALL OPF( FN.BR,F.BR)
            BR.ID = R.NEW(BT.TT.OUR.REFERENCE)<1,NUMBERBR.ID>
            CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

            IF NOT(ETEXT) THEN
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PREV.STAT>

                MAT.EX = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT>
                PLACE  = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.PAY.PLACE>
                HH     = "+":PLACE:"W"
                CALL CDT('',MAT.EX,HH)
                R.BR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>     = MAT.EX

                CALL F.WRITE(FN.BR,BR.ID,R.BR)
            END
        NEXT NUMBERBR.ID
    END
    RETURN
END
