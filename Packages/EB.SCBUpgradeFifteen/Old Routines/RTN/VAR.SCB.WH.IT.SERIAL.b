* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SCB.WH.IT.SERIAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS.SERIAL


*    IF V$FUNCTION ='' THEN

        KEY.LIST = '' ; SELECTED.NO = '' ; ER.MSG = '' ; ETEXT = '';NEW.SER = '' ;SER.ID=''
        FN.SCB.WH.SERIAL = 'F.SCB.WH.ITEMS.SERIAL' ; F.SCB.WH.SERIAL = '' ; R.SCB.WH.SERIAL = ''

        CALL DBR('SCB.WH.ITEMS.SERIAL':@FM:SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16],SER.ID)

        IF SER.ID EQ '' THEN
            NEW.SER = 1
        END ELSE
            NEW.SER = SER.ID + 1
        END

        R.SCB.WH.SERIAL<SCB.WH.IT.SER.SERIAL.NO> = NEW.SER

        CALL OPF( FN.SCB.WH.SERIAL,F.SCB.WH.SERIAL)
        CALL F.WRITE(FN.SCB.WH.SERIAL,ID.NEW[3,16],R.SCB.WH.SERIAL)

    *END
*CLOSE F.SCB.WH.SERIAL
    RETURN
END
