* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LD.DEP.OPEN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TXN.DEP

*TO WRITE TRANSACTION IN SCB.TXN.DEP TABLE

    LD.ID = '' ; LD.ID = ID.NEW

    CALL OPF('F.LD.LOANS.AND.DEPOSITS', F.LD)

    CALL F.READ('F.LD.LOANS.AND.DEPOSITS',LD.ID, R.TERM,F.LD, ER.MSG)
    IF NOT(ER.MSG) THEN

        FN.LD='F.SCB.TXN.DEP';ID.NO=ID.NEW:'.':TODAY
        CALL OPF(FN.LD,F.LD)
        CALL F.READ(FN.LD,ID.NO,R.LD,F.LDD,ETEXT)

        R.LD<SCB.DEP.BRANCH>=R.TERM<LD.DEPT.CODE>
        R.LD<SCB.DEP.CATEGORY>=R.TERM<LD.CATEGORY>
        R.LD<SCB.DEP.DEBIT.ACCOUNT>= R.TERM<LD.DRAWDOWN.ACCOUNT>
        R.LD<SCB.DEP.DEBIT.CURRENCY>= R.TERM<LD.CURRENCY>
        R.LD<SCB.DEP.DEBIT.AMOUNT>=R.TERM<LD.AMOUNT>
        R.LD<SCB.DEP.CREDIT.ACCOUNT>=R.TERM<LD.CATEGORY>
        R.LD<SCB.DEP.CREDIT.AMOUNT>=R.TERM<LD.AMOUNT>
        R.LD<SCB.DEP.DESCRIPTION>='DEPISSUANCE'
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR("CURRENCY":@FM:EB.CUR.MID.REVAL.RATE,R.TERM<LD.CURRENCY>,EQUI)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.TERM<LD.CURRENCY>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
EQUI=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
        R.LD<SCB.DEP.DR.EQUIVALENT> = R.LD<SCB.DEP.DEBIT.AMOUNT>*EQUI
        R.LD<SCB.DEP.CR.EQUIVALENT> = R.LD<SCB.DEP.CREDIT.AMOUNT>*EQUI
*********************************************************
        DA.TI=R.TERM<LD.DATE.TIME>
        TIM=DA.TI[7,2]:":":DA.TI[9,2]
        DAT=DA.TI[1,6]
        R.LD<SCB.DEP.TXN.DATE>=DAT

        R.LD<SCB.DEP.TXN.TIME>=TIM

        SC.INP=R.TERM<LD.INPUTTER,1>
        SC.INP = FIELD(SC.INP,'_',2)

        R.LD<SCB.DEP.USER>=SC.INP
        R.LD<SCB.DEP.FILLER>=''

        CALL F.WRITE(FN.LD,ID.NO,R.LD)
    END

    RETURN

END
