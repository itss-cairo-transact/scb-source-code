* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*----------------------------------------------------------------------------
    SUBROUTINE VIR.CD.SERIAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.SERIAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    IF V$FUNCTION NE 'R' AND V$FUNCTION NE 'D' THEN

        BR.CODE       =  ID.COMPANY
        BR.CODE  =BR.CODE[8,2]
        CD.QUAN=R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
        IF CD.QUAN GT 1  THEN
            CD.QUAN=R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>-1
        END ELSE
            CD.QUAN = 0
        END


        FN.CD.SER     =  "F.SCB.CD.SERIAL"    ; F.CD.SER = ""
        CALL OPF(FN.CD.SER,F.CD.SER)
        CALL F.READ(FN.CD.SER,BR.CODE,R.CD.SER,F.CD.SER,ERR1)


        IF R.CD.SER<CD.SER.SERIAL.NO> NE ''  THEN

            START.SER =  R.CD.SER<CD.SER.SERIAL.NO>

            END.SER   =  R.CD.SER<CD.SER.SERIAL.NO> + CD.QUAN
            IF LEN(END.SER) LT 14 THEN
                END.SER = '0':END.SER
            END

            SCB.CD    =  R.CD.SER<CD.SER.SERIAL.NO>

            R.NEW(LD.LOCAL.REF)<1,LDLR.ORDER>      =  START.SER
            R.NEW(LD.LOCAL.REF)<1,LDLR.ARTICLE.NO> =  END.SER
            R.NEW(LD.LOCAL.REF)<1,LDLR.OLD.NO>     =  SCB.CD

*******************************************************
        END ELSE
**------------------------------------
**          BR.CODE

            BR.NO    = FMT(BR.CODE, "LZ12")
            STRT.NO  = FIELD(BR.NO,'.',1):1

            START.SER =  STRT.NO

            IF LEN(START.SER) LT 14 THEN
                START.SER = '0':START.SER
            END

            END.SER   =  START.SER + CD.QUAN
            IF LEN(END.SER) LT 14 THEN
                END.SER = '0':END.SER
            END

            SCB.CD    = START.SER

            R.NEW(LD.LOCAL.REF)<1,LDLR.ORDER>      =  START.SER
            R.NEW(LD.LOCAL.REF)<1,LDLR.ARTICLE.NO> =  END.SER
            R.NEW(LD.LOCAL.REF)<1,LDLR.OLD.NO>     =  SCB.CD

        END

        SER.NEW = R.NEW(LD.LOCAL.REF)<1,LDLR.ARTICLE.NO> + 1

        IF LEN(SER.NEW) LT 14 THEN
            SER.NEW = '0':SER.NEW
        END
        R.CD.SER<CD.SER.SERIAL.NO> = SER.NEW
**TEXT=R.CD.SER<CD.SER.SERIAL.NO>:'--';CALL REM
*WRITE  R.CD.SER TO F.CD.SER , BR.CODE ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.CODE:"TO" :FN.CD.SER
*END
        CALL F.WRITE (FN.CD.SER,BR.CODE,R.CD.SER)
    END
    RETURN
END
