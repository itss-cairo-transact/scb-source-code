* @ValidationCode : MjotMTA1MTA4NTM4NjpDcDEyNTI6MTY0MTk0MzQ1MDgwMTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 12 Jan 2022 01:24:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.OFS.USR.MOD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SMS.GROUP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*----------------------------------------

*** UPDATED BY MSABRY 2014/02/26 ***********
    IF R.NEW(MUA.RECORD.STATUS) EQ 'RNAU' THEN
        RETURN
    END
*** END OF UPDATE ***************************

    IF R.NEW(MUA.OVERRIDE) NE '' THEN
        WS.CHK.OVERRIDE    = 0
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.LVE.OVR.CNT     = DCOUNT(R.NEW(MUA.OVERRIDE),@VM)
        FOR ILVE.CLS = 1 TO WS.LVE.OVR.CNT
            WS.LVE.CLS         = FIELD(R.NEW(MUA.OVERRIDE)<1,ILVE.CLS>,"*",2)
            WS.LVE.CLS.AUTH    = FIELD(R.NEW(MUA.OVERRIDE)<1,ILVE.CLS>,"*",3)
            IF WS.LVE.CLS NE '' AND WS.LVE.CLS.AUTH EQ '' THEN
                RETURN
            END
*Line [57] should be ILVE.CLS insted of WS.LVE.OVR.CNT - ITSS - R21 Upgrade - 2021-12-26
        NEXT ILVE.CLS
*NEXT WS.LVE.OVR.CNT
    END
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*----------------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    COMMA = ","
*Line [ 68 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    WS.NULL = ''

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "USER"
    OFS.OPTIONS      = "AUTO"

    COMP  = ID.COMPANY
    AC.BR = COMP[2]
*    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.SMS = 'F.USER.SMS.GROUP' ; F.SMS = ''
    CALL OPF(FN.SMS,F.SMS)

    GOSUB GET.DATA
RETURN
*----------------------------------------------------
GET.DATA:
    WS.USR.ID        = R.NEW(MUA.USER.ID)
    WS.USR.CO        = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.USR.CO        = 'EG00100':WS.USR.CO[2,2]
    WS.DEPT.ACCT     = R.NEW(MUA.DEP.ACCT.CODE)

    WS.START         = R.NEW(MUA.START.DATE.PROFILE)
    WS.END           = R.NEW(MUA.END.DATE.PROFILE)

    WS.OVER.CLASS    = R.NEW(MUA.OVER.CLASS)
*Line [ 103 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CNT       = DCOUNT(WS.OVER.CLASS,@VM)

    WS.HMM.ID        = R.NEW(MUA.HMM.ID)
*Line [ 107 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.HMM.CNT       = DCOUNT(WS.HMM.ID,@VM)

    WS.FUN           = R.NEW(MUA.FUNCTION)
*Line [ 111 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.FUN.CNT       = DCOUNT(WS.FUN,@VM)

    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :WS.USR.CO
RETURN
*----------------------------------------------------
BUILD.RECORD:

    GOSUB EDIT.USR

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:WS.USR.ID:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY
*OFS.ID = "T":TNO:".USR-":ID.NEW:WS.USR.ID:"-":DAT
    OFS.ID = "T":TNO:".USR-":WS.USR.CO[2]:"-":ID.NEW

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")

RETURN
************************************************************
**-------------------------------------------------------------------
EDIT.USR:

    OFS.MESSAGE.DATA  =  "INIT.APPLICATION=":WS.HMM.ID<1,1>:COMMA

    GOSUB EDIT.USR.COMPANY
    GOSUB CHK.TIME

    OFS.MESSAGE.DATA :=  "START.DATE.PROFILE=":WS.START:COMMA
    OFS.MESSAGE.DATA :=  "END.DATE.PROFILE=":WS.END:COMMA

    OFS.MESSAGE.DATA :=  "COMPANY.RESTR:1:1=ALL":COMMA
    OFS.MESSAGE.DATA :=  "APPLICATION:1:1=ALL.PG.H":COMMA
    OFS.MESSAGE.DATA :=  "FUNCTION:1:1=E L S P":COMMA

    WS.COUNT = 1
    GOSUB GET.SMS
RETURN
**-------------------------------------------------------------------
GET.SMS:
    FOR ICNT = 1 TO WS.FUN.CNT
        WS.FUN<1,ICNT> = FIELD(WS.FUN<1,ICNT>,".",1)
        IF WS.FUN<1,ICNT> # 'S' THEN
            WS.SMS.ID = WS.FUN<1,ICNT>:WS.HMM.ID<1,ICNT>
            CALL F.READ(FN.SMS,WS.SMS.ID,R.SMS,F.SMS,ER.SMS)
            IF NOT(ER.SMS) THEN
                WS.COUNT++
                OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=":WS.USR.CO:COMMA
                OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=@":WS.SMS.ID:COMMA
*Line [ 164 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 166 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 168 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 170 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 172 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 174 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA
                GOSUB ADD.SP.HMM
            END
        END
    NEXT ICNT
    WS.COUNT++
    OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=ALL":COMMA
    OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=@LOCK.DEV.AREA":COMMA
*Line [ 183 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 185 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 187 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 189 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 191 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 193 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA

    WS.COUNT++
    OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:':1=ALL':COMMA
    OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=FUNDS.TRANSFER":COMMA
*Line [ 199 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
    OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=E L S P":COMMA
    OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=1":COMMA
    OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=NE":COMMA
    OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=AC12':COMMA
    OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=AC13":COMMA

    WS.COUNT++
    OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=ALL":COMMA
    OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=ALL.PG.M":COMMA
*Line [ 210 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 212 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 214 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 216 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 218 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 220 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA


*Line [ 226 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('USER':@FM:EB.USE.COMPANY.RESTR,WS.USR.ID,WS.USR.CO.RES)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.USR.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.USR.CO.RES=R.ITSS.USER<EB.USE.COMPANY.RESTR>
*Line [ 225 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.USR.CO.RES.CNT     = DCOUNT(WS.USR.CO.RES,@VM)
    IF WS.USR.CO.RES.CNT GT WS.COUNT THEN

*MSABRY 2014/02/25

*  FOR I.RES.CNT = WS.COUNT TO WS.USR.CO.RES.CNT
*      WS.COUNT ++
*      OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:':1=':'NULL':COMMA
*  NEXT I.RES.CNT
****
    END
*Line [ 245 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('USER':@FM:EB.USE.OVERRIDE.CLASS,WS.USR.ID,WS.USR.OVR.CLS)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.USR.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.USR.OVR.CLS=R.ITSS.USER<EB.USE.OVERRIDE.CLASS>
*Line [ 238 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.USR.OVR.CNT     = DCOUNT(WS.USR.OVR.CLS,@VM)

    IF WS.USR.OVR.CNT GT WS.OVR.CNT THEN
        FOR IOVR.CNT = 1 TO WS.OVR.CNT
            IF WS.OVER.CLASS<1,IOVR.CNT> NE '' THEN
                OFS.MESSAGE.DATA  :=  "OVERRIDE.CLASS:":IOVR.CNT:":1=":WS.OVER.CLASS<1,IOVR.CNT>:COMMA
            END ELSE
*MSABRY 2014/02/25
*                OFS.MESSAGE.DATA  :=  "OVERRIDE.CLASS:":IOVR.CNT:":1=":'NULL':COMMA
            END
        NEXT IOVR.CNT
        WS.OVR.CNT  ++
** MSABRY 2014/02/25
*   FOR IOVR.CNT = WS.OVR.CNT TO WS.USR.OVR.CNT
*       OFS.MESSAGE.DATA  :=  "OVERRIDE.CLASS:":IOVR.CNT:":1=":'NULL':COMMA
*   NEXT IOVR.CNT
    END ELSE
        FOR IOVR.CNT = 1 TO WS.OVR.CNT
*MSABRY 2014/02/25
            IF WS.OVER.CLASS<1,IOVR.CNT> NE '' THEN
                OFS.MESSAGE.DATA  :=  "OVERRIDE.CLASS:":IOVR.CNT:":1=":WS.OVER.CLASS<1,IOVR.CNT>:COMMA
            END
        NEXT IOVR.CNT
    END
    OFS.MESSAGE.DATA  :=  "SCB.DEPT.CODE=":WS.DEPT.ACCT:COMMA
RETURN
**-------------------------------------------------------------------
EDIT.USR.COMPANY:
    WS.MUA.COMPANY         = R.NEW(MUA.COMPANY.CODE)
*Line [ 268 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.MUA.COMPANY.CNT     = DCOUNT(WS.MUA.COMPANY,@VM)

    FOR ICOMPANY = 1 TO WS.MUA.COMPANY.CNT
        OFS.MESSAGE.DATA  :=  "COMPANY.CODE:":ICOMPANY:':1=':WS.MUA.COMPANY<1,ICOMPANY>:COMMA
    NEXT ICOMPANY

*Line [ 289 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('USER':@FM:EB.USE.COMPANY.CODE,WS.USR.ID,WS.USR.COMPANY)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.USR.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.USR.COMPANY=R.ITSS.USER<EB.USE.COMPANY.CODE>
*Line [ 276 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.USR.COMPANY.CNT     = DCOUNT(WS.USR.COMPANY,@VM)

    IF WS.USR.COMPANY.CNT GT  WS.MUA.COMPANY.CNT THEN
* MSABRY  2014/02/25
*        FOR I.CO.N = ICOMPANY TO WS.USR.COMPANY.CNT
*            I.CO.N ++
*            OFS.MESSAGE.DATA  :=  "COMPANY.CODE:":I.CO.N:':1=':'NULL':COMMA
*        NEXT I.CO.N
    END
RETURN
**-------------------------------------------------------------------
ADD.SP.HMM:
    IF WS.HMM.ID<1,ICNT>   = '433' THEN
        WS.COUNT++
        OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=":"EG0010099":COMMA
        OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=@":WS.SMS.ID:COMMA
*Line [ 293 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 295 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 297 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 299 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 301 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 303 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA
    END
    IF WS.HMM.ID<1,ICNT>   = '400' AND WS.FUN<1,ICNT> = 'I' THEN
        WS.SMS.ID = 'A400'
        WS.COUNT++
        OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=":WS.USR.CO:COMMA
        OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=@":WS.SMS.ID:COMMA
*Line [ 311 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 313 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 315 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 317 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 319 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 321 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA
    END
    IF WS.HMM.ID<1,ICNT>   = '400' AND WS.FUN<1,ICNT> = 'A' THEN
        WS.SMS.ID = 'I400'
        WS.COUNT++
        OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=":WS.USR.CO:COMMA
        OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=@":WS.SMS.ID:COMMA
*Line [ 329 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 331 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 333 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 335 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 337 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 339 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA
    END

*--- EDIT BY NESSMA 2016/06/01
    IF WS.HMM.ID<1,ICNT>   = '401' AND WS.FUN<1,ICNT> = 'A' THEN
        WS.SMS.ID = 'I401'
        WS.COUNT++
        OFS.MESSAGE.DATA  :=  "COMPANY.RESTR:":WS.COUNT:":1=":WS.USR.CO:COMMA
        OFS.MESSAGE.DATA  :=  "APPLICATION:":WS.COUNT:":1=@":WS.SMS.ID:COMMA
*Line [ 349 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "VERSION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 351 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FUNCTION:":WS.COUNT:":1=":'NULL':"":COMMA
*Line [ 353 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "FIELD.NO:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 355 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.COMPARISON:":WS.COUNT:":1=":'NULL':COMMA
*Line [ 357 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.FROM:":WS.COUNT:':1=':'NULL':COMMA
*Line [ 359 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        OFS.MESSAGE.DATA  :=  "DATA.TO:":WS.COUNT:":1=":'NULL':COMMA
    END
*--- END EDIT
RETURN
**-------------------------------------------------------------------
CHK.TIME:
*Line [ 386 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('USER':@FM:EB.USE.START.TIME,WS.USR.ID,WS.USR.STRT.TIME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.USR.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.USR.STRT.TIME=R.ITSS.USER<EB.USE.START.TIME>
*Line [ 393 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('USER':@FM:EB.USE.END.TIME,WS.USR.ID,WS.USR.END.TIME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.USR.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.USR.END.TIME=R.ITSS.USER<EB.USE.END.TIME>
* EB.USE.END.TIME
    IF  WS.USR.END.TIME<1,1> LT 1730 THEN
        OFS.MESSAGE.DATA :=  "START.TIME:1:1=08:00":COMMA
        OFS.MESSAGE.DATA :=  "END.TIME:1:1=17:30":COMMA
    END
RETURN
**-------------------------------------------------------------------
END
