* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CHK.PAY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*-----------------------------------------------

    FN.PAY = "F.PAYMENT.STOP"   ; F.PAY = ""
    CALL OPF(FN.PAY, F.PAY)

    FN.TMP = "F.SCB.FT.DR.CHQ"   ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    ACC.NO  = ID.NEW
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('PAYMENT.STOP$NAU':@FM:AC.PAY.MOD.PS.CHQ.NO,ID.NEW,CHQ.NOMM)
F.ITSS.PAYMENT.STOP$NAU = 'F.PAYMENT.STOP$NAU'
FN.F.ITSS.PAYMENT.STOP$NAU = ''
CALL OPF(F.ITSS.PAYMENT.STOP$NAU,FN.F.ITSS.PAYMENT.STOP$NAU)
CALL F.READ(F.ITSS.PAYMENT.STOP$NAU,ID.NEW,R.ITSS.PAYMENT.STOP$NAU,FN.F.ITSS.PAYMENT.STOP$NAU,ERROR.PAYMENT.STOP$NAU)
CHQ.NOMM=R.ITSS.PAYMENT.STOP$NAU<AC.PAY.MOD.PS.CHQ.NO>

    AAM      = DCOUNT(CHQ.NOMM,@AM)
    ASM      = DCOUNT(CHQ.NOMM,@SM)
    AVM      = DCOUNT(CHQ.NOMM,@VM)
    AFM      = DCOUNT(CHQ.NOMM,@FM)

    FOR KK = 1 TO AVM
        CHEQ.X  = CHQ.NOMM<1,KK>
        TEMP.ID = ACC.NO :".": CHEQ.X
        TEXT = "TEMP.ID":TEMP.ID ; CALL REM
        CALL F.READ(FN.TMP,TEMP.ID,R.TEMP,F.TMP,ERR.TMP)
        IF NOT(ERR.TMP) THEN
            PREV.STAT = R.TEMP<DR.CHQ.PREV.STATUS>
            CRNT.STAT = R.TEMP<DR.CHQ.CHEQ.STATUS>

            R.TEMP<DR.CHQ.PREV.STATUS> = CRNT.STAT
            R.TEMP<DR.CHQ.CHEQ.STATUS> = PREV.STAT
            TEXT = "CRNT.STAT":CRNT.STAT ; CALL REM
            TEXT = "PREV.STAT":PREV.STAT ; CALL REM
*WRITE  R.TEMP TO F.TMP , TEMP.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD"
*END
***********MODIFIED ON 2016-06-15 ABEER
***            CALL F.WRITE(FN.TEMP,TEMP.ID, R.TEMP)
***
            CALL F.WRITE(FN.TMP,TEMP.ID, R.TEMP)
****

        END ELSE
            ETEXT = "�� ���� ��� ����� �����"
            CALL STORE.END.ERROR
            KK = AVM
        END
    NEXT KK
    RETURN
END
