* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.PAID.OLD.CHQ

**  TO WRITE ON TABLE THE PAID OLD CHEQUES NUMBERS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

***    IF V$FUNCTION # 'R' THEN
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='INA' THEN
        FN.P.CHQ = 'F.SCB.P.CHEQ' ; F.P.CHQ = '' ; R.P.CHQ = ''
        ETEXT    = ''

        CHQ.NO   = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
        ACCT.NO  = R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
MYBRN1=R.ITSS.ACCOUNT<AC.CO.CODE>
        MYBRN = MYBRN1[2]

*-- EDIT BY NESSMA 2016/08/02
        MYBRN = TRIM(MYBRN , "0" , "L")
*-- END EDIT

        OLD.CHQ  = CHQ.NO:'.':MYBRN
        CALL OPF( FN.P.CHQ,F.P.CHQ)
        CALL F.READ( FN.P.CHQ,OLD.CHQ, R.P.CHQ, F.P.CHQ, ETEXT)
****UPDATED BY NESSREEN AHMED 24/2/2010***************************
        IF ETEXT THEN
            R.P.CHQ<P.CHEQ.CHEQ.VAL> = R.NEW(FT.DEBIT.AMOUNT)

***UPDATED BY NESSREEN AHMED ON 22/02/2009*******************
**  R.P.CHQ<P.CHEQ.TRM.DAT> = R.NEW(FT.DEBIT.VALUE.DATE)
            R.P.CHQ<P.CHEQ.TRN.DAT> = R.NEW(FT.DEBIT.VALUE.DATE)
**************************************************************
            R.P.CHQ<P.CHEQ.ACCOUNT.NO> = ACCT.NO
            R.P.CHQ<P.CHEQ.OUR.REF>  = ID.NEW
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
            COMP1 = C$ID.COMPANY
            R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP1
*********************************************************************
            CALL F.WRITE(FN.P.CHQ,OLD.CHQ,R.P.CHQ)
        END ELSE
            E = '��� �� ��� ��� ����� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'

        END
    END
    RETURN
END
