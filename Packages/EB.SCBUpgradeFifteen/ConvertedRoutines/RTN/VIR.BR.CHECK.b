* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.BR.CHECK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

************************CONTINGENT NOT ALLOWED************************

    LIQ.ACCT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR("ACCOUNT":@FM:AC.CATEGORY,LIQ.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LIQ.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF ( CATEG NE 3020 AND CATEG NE 3206  AND CATEG NE 3207  AND CATEG NE 3208 )THEN
    IF (CATEG >= 9000 AND CATEG <= 9999) OR (CATEG >= 3000 AND CATEG <= 3199)  OR (CATEG >= 3206 AND CATEG <= 3999) THEN
        TEXT  = "��� ����� ��������� ��������"  ; CALL REM
        ETEXT = "��� ����� ��������� ��������"  ; CALL STORE.END.ERROR
    END

************************CHECK CURRENCY WITH DEBIT & CREDIT************

    DEBIT.ACCT  = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
    IF DEBIT.ACCT[1,3] EQ "EGP" THEN
        DB.CURR1     = DEBIT.ACCT[1,3]
*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR("CURRENCY":@FM:EB.CUR.NUMERIC.CCY.CODE,DB.CURR1,DB.CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,DB.CURR1,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
DB.CURR=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
    END ELSE
        DB.CURR     = DEBIT.ACCT[9,2]
    END

    CREDIT.ACCT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>
    CR.CURR     = CREDIT.ACCT[9,2]

*   IF DB.CURR NE CR.CURR THEN
*      ETEXT = "��� ������ �������" ; CALL STORE.END.ERROR
*   END

**********************************************************************
    CURR         = R.NEW(EB.BILL.REG.CURRENCY)
    COM.AMT      = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>

    IF COM.AMT THEN
        COM.AMT.CURR = COM.AMT[1,3]

        IF COM.AMT.CURR NE CURR THEN
            ETEXT = "��� ������ ���� �������" ; CALL STORE.END.ERROR
        END
    END

    CHG.AMT      = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>

    IF CHG.AMT THEN
        CHG.AMT.CURR = CHG.AMT[1,3]

        *IF CHG.AMT.CURR NE CURR THEN
           * ETEXT = "��� ������ ���� ��������" ; CALL STORE.END.ERROR
       * END
    END

**********************************************************************

    RETURN
END
