* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.LG.OUT.COMM

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.COMM.CORS


    FN.COM= 'F.SCB.LG.COMM.CORS';F.COM='';R.COM = '';E11=''
    CALL OPF(FN.COM,F.COM)
    COM.ID = ID.NEW :'-': TODAY
    CALL F.READ(FN.COM,COM.ID,R.COM,F.COM ,E11)

    LG.CHRG.AMT = R.NEW(LD.CHRG.AMOUNT)
*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AMT.COUNT = DCOUNT(LG.CHRG.AMT,@VM)
    TEXT=AMT.COUNT;CALL REM
    R.COM<LG.COM.CURRENCY> = R.NEW(LD.CURRENCY)
    FOR I = 1 TO AMT.COUNT
        R.COM<LG.COM.COM.CODE,I>   = R.NEW(LD.CHRG.CODE)<1,I>
        R.COM<LG.COM.COM.DATE,I>   = TODAY
        R.COM<LG.COM.COM.AMOUNT,I> = R.NEW(LD.CHRG.AMOUNT)<1,I>
        R.COM<LG.COM.COM.STATUS,I> = 'O'
        CALL F.WRITE(FN.COM,COM.ID,R.COM)
    NEXT I
    RETURN
END
