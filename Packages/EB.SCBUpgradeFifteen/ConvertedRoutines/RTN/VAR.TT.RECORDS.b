* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
***-----NESSREEN 03/05/2005----***
***TO GEAT THE TRANSACTION FOR TELLER
    SUBROUTINE VAR.TT.RECORDS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TXN.TELLER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    F.COUNT = '' ; FN.COUNT = 'F.SCB.TXN.TELLER' ; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
    ID.NO = ID.NEW
    TRANS.COD = R.NEW(TT.TE.TRANSACTION.CODE)
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'TELLER.TRANSACTION':@FM:TT.TR.DESC, TRANS.COD, DESC2)
F.ITSS.TELLER.TRANSACTION = 'FBNK.TELLER.TRANSACTION'
FN.F.ITSS.TELLER.TRANSACTION = ''
CALL OPF(F.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION)
CALL F.READ(F.ITSS.TELLER.TRANSACTION,TRANS.COD,R.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION,ERROR.TELLER.TRANSACTION)
DESC2=R.ITSS.TELLER.TRANSACTION<TT.TR.DESC>
*********************************************************************
****************TRANSACTION DESCRIPTION******************************
*********************************************************************
*R.COUNT<SCB.TXN.TT.DESCRIOTION> = DESC
    BEGIN CASE
    CASE TRANS.COD = '56' OR TRANS.COD = '55' OR TRANS.COD ='57' OR TRANS.COD ='9' OR TRANS.COD ='42' OR TRANS.COD ='37' OR TRANS.COD ='38' OR TRANS.COD ='39' OR TRANS.COD ='87' OR TRANS.COD ='7' OR TRANS.COD ='32' OR TRANS.COD ='33'
        DESC = 'Tdeposit'
    CASE TRANS.COD = '2' OR TRANS.COD ='41' OR TRANS.COD ='43' OR TRANS.COD ='40' OR TRANS.COD ='86' OR TRANS.COD ='85' OR TRANS.COD ='84' OR  TRANS.COD ='82' OR TRANS.COD ='28' OR TRANS.COD ='8' OR TRANS.COD ='36' OR TRANS.COD ='34'
        DESC = 'Twithdrawal'
    CASE TRANS.COD = '15' OR TRANS.COD ='4' OR TRANS.COD ='5' OR TRANS.COD ='901' OR TRANS.COD ='1' OR TRANS.COD ='16' OR TRANS.COD ='3'
        DESC = 'Ttransfer'
    CASE TRANS.COD = '23' OR TRANS.COD ='24'
        DESC = 'Tbuy&sellccy'
    END CASE
    R.COUNT<SCB.TXN.TT.DESCRIOTION> = DESC
    R.COUNT<SCB.TXN.TT.TRANS.CODE> = R.NEW(TT.TE.TRANSACTION.CODE)
**********************************************************************
***************DEBIT ENTRIES SIDE*************************************
**********************************************************************
**            ( IN CASE OF WITHDRAWAL)                               *
**********************************************************************
    IF TRANS.COD = '2' OR TRANS.COD ='41' OR TRANS.COD ='43' OR TRANS.COD ='40' OR TRANS.COD ='86' OR TRANS.COD ='85' OR TRANS.COD ='85' OR TRANS.COD ='84' OR  TRANS.COD ='82' OR TRANS.COD ='28' OR TRANS.COD ='8' OR TRANS.COD ='36' OR TRANS.COD ='34' THEN
        R.COUNT<SCB.TXN.TT.DEBIT.ACCOUNT> = R.NEW(TT.TE.ACCOUNT.2)
        R.COUNT<SCB.TXN.TT.DEBIT.CURRENCY> = R.NEW(TT.TE.CURRENCY.2)
        DR.ACC = R.NEW(TT.TE.ACCOUNT.2)
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, DR.ACC, DR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        R.COUNT<SCB.TXN.TT.DEBIT.CATEGORY> = DR.CATEG
        IF R.NEW(TT.TE.CURRENCY.2) = 'EGP' THEN
            R.COUNT<SCB.TXN.TT.DEBIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.LOCAL.2)
            R.COUNT<SCB.TXN.TT.DR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.2)
        END ELSE
            R.COUNT<SCB.TXN.TT.DEBIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.FCY.2)
            R.COUNT<SCB.TXN.TT.RATE> = R.NEW(TT.TE.RATE.2)
            R.COUNT<SCB.TXN.TT.DR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.2)
        END
    END ELSE
***********************************************************************
**             (OTHERWISE)                                            *
***********************************************************************
        R.COUNT<SCB.TXN.TT.DEBIT.ACCOUNT> = R.NEW(TT.TE.ACCOUNT.1)
        R.COUNT<SCB.TXN.TT.DEBIT.CURRENCY> = R.NEW(TT.TE.CURRENCY.1)
        DR.ACC = R.NEW(TT.TE.ACCOUNT.1)
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, DR.ACC, DR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        R.COUNT<SCB.TXN.TT.DEBIT.CATEGORY> = DR.CATEG
        IF R.NEW(TT.TE.CURRENCY.1) = 'EGP' THEN
            R.COUNT<SCB.TXN.TT.DEBIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
            R.COUNT<SCB.TXN.TT.DR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        END ELSE
            R.COUNT<SCB.TXN.TT.DEBIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.FCY.1)
            R.COUNT<SCB.TXN.TT.RATE> = R.NEW(TT.TE.RATE.1)
            R.COUNT<SCB.TXN.TT.DR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        END
    END
***********************************************************************
***************CREDIT ENTRIES SIDE*************************************
***********************************************************************
**            ( IN CASE OF WITHDRAWAL)                               *
**********************************************************************
    IF TRANS.COD = '2' OR TRANS.COD ='41' OR TRANS.COD ='43' OR TRANS.COD ='40' OR TRANS.COD ='86' OR TRANS.COD ='85' OR TRANS.COD ='85' OR TRANS.COD ='84' OR  TRANS.COD ='82' OR TRANS.COD ='28' OR TRANS.COD ='8' OR TRANS.COD ='36' OR TRANS.COD ='34' THEN
        R.COUNT<SCB.TXN.TT.CREDIT.CURRENCY> = R.NEW(TT.TE.CURRENCY.1)
        CR.ACC = R.NEW(TT.TE.ACCOUNT.1)
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, CR.ACC, CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        R.COUNT<SCB.TXN.TT.CREDIT.CATEGORY> = CR.CATEG
        R.COUNT<SCB.TXN.TT.CREDIT.ACCOUNT> = CR.ACC
        IF R.NEW(TT.TE.CURRENCY.1) = 'EGP' THEN
            R.COUNT<SCB.TXN.TT.CREDIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
            R.COUNT<SCB.TXN.TT.CR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        END ELSE
            R.COUNT<SCB.TXN.TT.CREDIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.FCY.1)
            R.COUNT<SCB.TXN.TT.RATE> = R.NEW(TT.TE.RATE.1)
            R.COUNT<SCB.TXN.TT.CR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        END
        R.COUNT<SCB.TXN.TT.VALUE.DATE> = R.NEW(TT.TE.VALUE.DATE.1)
    END ELSE
***********************************************************************
**             (OTHERWISE)                                            *
***********************************************************************
        R.COUNT<SCB.TXN.TT.CREDIT.CURRENCY> = R.NEW(TT.TE.CURRENCY.2)
        CR.ACC = R.NEW(TT.TE.ACCOUNT.2)
*Line [ 148 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, CR.ACC, CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        R.COUNT<SCB.TXN.TT.CREDIT.CATEGORY> = CR.CATEG
        R.COUNT<SCB.TXN.TT.CREDIT.ACCOUNT> = CR.ACC
        IF R.NEW(TT.TE.CURRENCY.2) = 'EGP' THEN
            R.COUNT<SCB.TXN.TT.CREDIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.LOCAL.2)
            R.COUNT<SCB.TXN.TT.CR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.2)
        END ELSE
            R.COUNT<SCB.TXN.TT.CREDIT.AMOUNT> = R.NEW(TT.TE.AMOUNT.FCY.2)
            R.COUNT<SCB.TXN.TT.RATE> = R.NEW(TT.TE.RATE.2)
            R.COUNT<SCB.TXN.TT.CR.EQUIVALENT> = R.NEW(TT.TE.AMOUNT.LOCAL.2)
        END
        R.COUNT<SCB.TXN.TT.VALUE.DATE> = R.NEW(TT.TE.VALUE.DATE.2)
    END
***********************************************************************
*****************AUDITING DETAILS**************************************
***********************************************************************
    TT.DD.TT = R.NEW(TT.TE.DATE.TIME)
    DD = TT.DD.TT[5,2]
    MM = TT.DD.TT[3,2]
    YY = TT.DD.TT[1,2]
   * TT.DATE = DD:MM:YY
     TT.DATE = TODAY
    *TEXT = 'DATE=':TT.DATE ; CALL REM
    R.COUNT<SCB.TXN.TT.TRANS.DATE> = TT.DATE
    R.COUNT<SCB.TXN.TT.TRANS.TIME> = TT.DD.TT[7,2]:":":TT.DD.TT[9,2]
    TT.INP = R.NEW(TT.TE.INPUTTER)
    TT.INPU = FIELD(TT.INP,'_',2)
    R.COUNT<SCB.TXN.TT.USER> = TT.INPU
    R.COUNT<SCB.TXN.TT.BRANCH.CODE> = R.NEW(TT.TE.DEPT.CODE)
***********************************************************************
    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
   * CALL JOURNAL.UPDATE(ID.NO)
    CLOSE F.COUNT
    RETURN
END
