* @ValidationCode : MjotMTA3NDI2OTE1MTpDcDEyNTI6MTY0MjMyNDIzNjU4Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Jan 2022 11:10:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.SCB.WH.IT.SERIAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.ITEMS.SERIAL


*    IF V$FUNCTION ='' THEN

    KEY.LIST = '' ; SELECTED.NO = '' ; ER.MSG = '' ; ETEXT = '';NEW.SER = '' ;SER.ID=''
    FN.SCB.WH.SERIAL = 'F.SCB.WH.ITEMS.SERIAL' ; F.SCB.WH.SERIAL = '' ; R.SCB.WH.SERIAL = ''

*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('SCB.WH.ITEMS.SERIAL':@FM:SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16],SER.ID)
    F.ITSS.SCB.WH.ITEMS.SERIAL = 'F.SCB.WH.ITEMS.SERIAL'
    FN.F.ITSS.SCB.WH.ITEMS.SERIAL = ''
    CALL OPF(F.ITSS.SCB.WH.ITEMS.SERIAL,FN.F.ITSS.SCB.WH.ITEMS.SERIAL)
    CALL F.READ(F.ITSS.SCB.WH.ITEMS.SERIAL,R.ITSS.SCB.WH.ITEMS.SERIAL,FN.F.ITSS.SCB.WH.ITEMS.SERIAL,ERROR.SCB.WH.ITEMS.SERIAL)
    SER.ID=R.ITSS.SCB.WH.ITEMS.SERIAL<SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16]>

    IF SER.ID EQ '' THEN
        NEW.SER = 1
    END ELSE
        NEW.SER = SER.ID + 1
    END

    R.SCB.WH.SERIAL<SCB.WH.IT.SER.SERIAL.NO> = NEW.SER

    CALL OPF( FN.SCB.WH.SERIAL,F.SCB.WH.SERIAL)
    CALL F.WRITE(FN.SCB.WH.SERIAL,ID.NEW[3,16],R.SCB.WH.SERIAL)

*END
*CLOSE F.SCB.WH.SERIAL
RETURN
END
