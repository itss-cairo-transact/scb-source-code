* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VAR.FT.BN.CHQ1
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
    IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "1 DRAFT- ��� ����� ��������" THEN
        ID = R.NEW(FT.DEBIT.ACCT.NO):'.':R.NEW(FT.CHEQUE.NUMBER)
    END
    IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "4 CHEQUE-��� � �����" THEN

        ID=R.NEW(FT.DEBIT.THEIR.REF):'.':R.NEW(FT.CHEQUE.NUMBER)
    END
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
        R.COUNT<DR.CHQ.CHEQ.STATUS> = 3
        R.COUNT<DR.CHQ.PAY.BRN> = R.NEW(FT.DEPT.CODE)
        R.COUNT<DR.CHQ.TRANS.PAYMENT> = ID.NEW
        CALL F.WRITE(FN.COUNT,ID,R.COUNT)
        CALL JOURNAL.UPDATE(ID)
        CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
        CLOSE FN.COUNT
    END
    RETURN
END
