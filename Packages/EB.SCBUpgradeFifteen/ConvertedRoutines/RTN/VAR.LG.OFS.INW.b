* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
****** WAEL ****
*-----------------------------------------------------------------------------
* <Rating>69</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.OFS.INW

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    GOSUB CREATE.FILE

    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.LG",OFS.SOURCE.REC,F.OFS.SOURCE,'')

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
*TEXT = FN.OFS.IN ; CALL REM

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"

    OFS.OPTIONS = "SCB.ZEAD"


***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","

***************
    TRANS.TYPE = 'ACLG'
    DB.CUR = 'EGP'
    DB.AMT = '30.10'
    PROFT.CUST = 'CUST'
    DB.DATE = R.NEW(LD.VALUE.DATE)
    CR.DATA = R.NEW(LD.VALUE.DATE)
    DB.ACCT = 'PL52055'
    CR.ACCT = 'EGP110230101'
    PROFIT.DEPT ='13'
    ORDER.BANK ='SCB'
    BENF.CUST ='CUST'
***************
    OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":PROFIT.DEPT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATA:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":ORDER.BANK


    RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(LD.DATE.TIME)
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OPERATOR:"_LG":RND(10000):"_":D.T ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

    RETURN
*--------------------------------------------------------
END
