* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* CREATE BY HYTHAM
* EDIT BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.CHQ.DELETE.BR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*--------------------------------------------------------------
    FN.BS = "FBNK.SCB.BR.SLIPS"     ; F.BS = ""
    CALL OPF(FN.BS ,F.BS)

    BS.NUMBER    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.SLIP.REFER>
    CALL F.READ(FN.BS,BS.NUMBER,R.BS,F.BS,BS.ERR)
    CUS.CHRG     = R.BS<SCB.BS.TOT.CHRG2.AMT>
    PL.CHRG      = R.BS<SCB.BS.BR.COUNT.PL>
    INT.AC.CHRG  = R.BS<SCB.BS.BR.COUNT.INT.ACC>
*-------------------- (^^;) -----------------------*
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('BILL.REGISTER':@FM:0,ID.NEW,BR.TEST)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,ID.NEW,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
BR.TEST=R.ITSS.BILL.REGISTER<0>
    IF BR.TEST EQ '' THEN
        IF V$FUNCTION = 'D' AND  R.NEW(EB.BILL.REG.RECORD.STATUS)='INAU' THEN

*Line [ 62 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            BR.COUNT2 = DCOUNT(R.BS<SCB.BS.BR.REFRENCE>,@VM)
            FOR NN2 = 1 TO BR.COUNT2
                IF ID.NEW EQ R.BS<SCB.BS.BR.REFRENCE,NN2> THEN
                    DEL R.BS<SCB.BS.BR.REFRENCE,NN2>
                    DEL R.BS<SCB.BS.BR.CHRG2.AMT,NN2>
                    CALL F.WRITE(FN.BS,BS.NUMBER,R.BS)
                    NN2 = BR.COUNT2
                END
            NEXT NN2
            GOSUB BS.UPDATE
        END
    END
*--------------------------------
    RETURN
*--------------------------------
BS.UPDATE:
    FN.BR = "FBNK.BILL.REGISTER$NAU"   ; F.BR = ""
    CALL OPF(FN.BR, F.BR)

    CALL F.READ( FN.BS,BS.NUMBER, R.SLIPS, F.BS, ETEXT.BS)
*Line [ 83 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BR.COUNT  = DCOUNT(R.SLIPS<SCB.BS.BR.REFRENCE>,@VM)
    BR.TOT    = 0
    CHRG.TOT  = 0
    COUNT.PL  = 0
*---- 2011/05/30 ---
    FOR II = 1 TO BR.COUNT
        BR.NUM = R.SLIPS<SCB.BS.BR.REFRENCE,II>
        CALL F.READ( FN.BR,BR.NUM, R.BR, F.BR, ETEXT.BR)
        BR.AMT  = R.BR<EB.BILL.REG.AMOUNT>
        BR.CHRG = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CH.CCY.AMT>

        IF ETEXT.BR THEN
            FN.BR.2 = "FBNK.BILL.REGISTER"   ; F.BR.2 = ""
            CALL OPF(FN.BR.2, F.BR.2)
            CALL F.READ(FN.BR.2,BR.NUM, R.BR.2, F.BR.2, ETEXT.BR.2)
            BR.AMT  = R.BR.2<EB.BILL.REG.AMOUNT>
            BR.CHRG = R.BR.2<EB.BILL.REG.LOCAL.REF><1,BRLR.CH.CCY.AMT>
        END

        BR.TOT   = BR.TOT   + BR.AMT
        CHRG.TOT = CHRG.TOT + BR.CHRG


        IF BR.CHRG GT 0 THEN
            COUNT.PL = COUNT.PL + 1
        END
    NEXT II
*-------------------
    TEXT = "CHRG.TOT= ":CHRG.TOT ; CALL REM

    R.SLIPS<SCB.BS.REG.AMT>       = R.SLIPS<SCB.BS.TOTAL.AMT> - BR.TOT
    R.SLIPS<SCB.BS.REG.NO>        = R.SLIPS<SCB.BS.TOTAL.NO>  - BR.COUNT
    R.SLIPS<SCB.BS.TOT.CHRG2.AMT> = CHRG.TOT

    COUNT.PL = COUNT.PL * 5
    R.SLIPS<SCB.BS.BR.COUNT.PL>      = COUNT.PL
    R.SLIPS<SCB.BS.BR.COUNT.INT.ACC> = CHRG.TOT - COUNT.PL

    CALL F.WRITE(FN.BS,BS.NUMBER,R.SLIPS)
    RETURN
END
