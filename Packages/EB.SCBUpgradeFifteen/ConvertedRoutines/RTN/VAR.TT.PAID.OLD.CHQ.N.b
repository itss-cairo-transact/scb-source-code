* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
** ----- NESSREEN AHMED 21/3/2010 -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.PAID.OLD.CHQ.N
** TO WRITE ON TABLE THE PAID OLD CHEQUES NUMBERS
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


    FN.P.CHQ = 'F.SCB.P.CHEQ' ; F.P.CHQ = '' ; R.P.CHQ = ''
    ETEXT = ''

****UPDATED BY NESSREEN AHMED 24/03/2010**********************
    IF V$FUNCTION # 'R' THEN
****END OF UPDATE ********************************************
        CHQ.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        *CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
MYBRN=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
***Updated by Nessreen Ahmed 29/7/2016**********
***     MYBRN  = AC.COMP[8,2]
        BR  = AC.COMP[8,2]
        MYBRN = TRIM(BR, "0" , "L")
        OLD.CHQ = CHQ.NO:'.':MYBRN
***end of updated 29/7/2016*************************
        CALL OPF( FN.P.CHQ,F.P.CHQ)
        CALL F.READ( FN.P.CHQ,OLD.CHQ, R.P.CHQ, F.P.CHQ, ETEXT)
*****UPDATED BY NESSREEN AHMED 24/2/2010*************************
        IF ETEXT THEN
            R.P.CHQ<P.CHEQ.CHEQ.VAL> = R.NEW(TT.TE.NET.AMOUNT)
            R.P.CHQ<P.CHEQ.CHEQ.DAT> = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>
*******UPDATED BY NESSREEN AHMED 22/02/2009********************
**  R.P.CHQ<P.CHEQ.TRM.DAT> = R.NEW(TT.TE.VALUE.DATE.1)
            R.P.CHQ<P.CHEQ.TRN.DAT> = R.NEW(TT.TE.VALUE.DATE.1)
***************************************************************
            R.P.CHQ<P.CHEQ.ACCOUNT.NO> = ACCT.NO
            R.P.CHQ<P.CHEQ.OUR.REF> = ID.NEW
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
            COMP = C$ID.COMPANY
            R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP
*********************************************************************
            CALL F.WRITE(FN.P.CHQ,OLD.CHQ,R.P.CHQ)
        END ELSE
            E = '��� �� ��� ��� ����� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
        END
****
    END   ;***END OF IF V$FUNCTION # 'R' THEN
***********
    RETURN
END
