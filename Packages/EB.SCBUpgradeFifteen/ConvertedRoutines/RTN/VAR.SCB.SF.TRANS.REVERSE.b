* @ValidationCode : MjotOTE2MjAzMzc6Q3AxMjUyOjE2NDIzMjMzNjIwMzU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 16 Jan 2022 10:56:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>177</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.SCB.SF.TRANS.REVERSE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SF.TRANS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Adding I_F.ACCOUNT  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*-----------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='RNAU' THEN

        IF LEN(R.NEW(SCB.SF.TR.CUSTOMER.NO)) = 7 THEN
            TMP.CLAS = R.NEW(SCB.SF.TR.CUSTOMER.NO)[2,1]
        END ELSE
            TMP.CLAS = R.NEW(SCB.SF.TR.CUSTOMER.NO)[3,1]
        END
        IF TMP.CLAS # 1 THEN
            IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "MRGN" THEN
                FLG.TRNS = 1
            END

            IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "EXPD" THEN
                FLG.TRNS = 3
            END

            GOSUB BUILD.RECORD
        END
    END

RETURN
**----------------------------------------------------------------**
BUILD.RECORD:

    CURR               = 'EGP'
    AMT                = R.NEW(SCB.SF.TR.MARGIN.VALUE)
    DATEE              = R.NEW(SCB.SF.TR.VALUE.DATE)

** - - - - - - - - - - -  - - - ** 1 ** - - - - - - - - - - - -- - - -  **
    IF FLG.TRNS = 1 THEN
        Y.ACCT         = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        Y.AMT          = AMT
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        GOSUB  AC.STMT.ENTRY

        Y.ACCT         = R.NEW(SCB.SF.TR.MARGIN.ACCT)
        Y.AMT          = AMT  * -1
        CATEG          = R.NEW(SCB.SF.TR.MARGIN.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        GOSUB  AC.STMT.ENTRY
    END
** - - - - - - - - - - -  - - - ** 1 ** - - - - - - - - - - - -- - - -  **
** - - - - - - - - - - -  - - - ** 3 ** - - - - - - - - - - - -- - - -  **
    IF FLG.TRNS = 3 THEN
        Y.ACCT         = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        Y.AMT          = AMT * -1
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        GOSUB  AC.STMT.ENTRY

        Y.ACCT         = R.NEW(SCB.SF.TR.MARGIN.ACCT)
        Y.AMT          = AMT
        CATEG          = R.NEW(SCB.SF.TR.MARGIN.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        GOSUB  AC.STMT.ENTRY
    END
** - - - - - - - - - - -  - - - ** 3 ** - - - - - - - - - - - -- - - -  **
    ACC.OFFICER        = R.USER<EB.USE.DEPARTMENT.CODE>

RETURN
**----------------------------------------------------------------**

AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*                                                         *
    ENTRY = ""
    MULTI.ENTRIES = ""


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '987'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.CAT
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = DATEE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = Y.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "IC4"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("IC4",TYPE,MULTI.ENTRIES,"")

RETURN

END
