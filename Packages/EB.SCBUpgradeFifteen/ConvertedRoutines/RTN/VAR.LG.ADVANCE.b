* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>380</Rating>
*-----------------------------------------------------------------------------
******* WAEL ********

    SUBROUTINE VAR.LG.ADVANCE

* TO MAKE AN FT TRANSACTION TO TRANSFER MONEY FROM
* CURRENCT.ACCT OF THE CUSTOMER TO TAX.ACCOUNT

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.BK
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    DIM R.FT(FT.AUDIT.DATE.TIME)
    MAT R.FT = ""
    ER.MSG = ""
    ID.FT = ""
    W.STATUS = ""
    CUS = R.NEW( LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>

    MYTAX = ''
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
*=============================================
    FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
    CALL OPF(FN.LG,F.LG)
    CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)

    MYTAX.ACC1 = R.LG<SCB.LG.CH.TAX.ACCOUNT,1>
    MYTAX1 = R.LG<SCB.LG.CH.TAX.AMOUNT,1>
    MYTAX.ACC2 = R.LG<SCB.LG.CH.TAX.ACCOUNT,2>
    MYTAX2 = R.LG<SCB.LG.CH.TAX.AMOUNT,2>

    GOSUB FIRST
    GOSUB SECOND
    RETURN

*===============================================
FIRST:

    R.FT(FT.TRANSACTION.TYPE) = "ACLG"
    R.FT(FT.DEBIT.AMOUNT) = MYTAX1
    R.FT(FT.DEBIT.CURRENCY) = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    R.FT(FT.CREDIT.CURRENCY) = R.NEW( LD.LOCAL.REF)< 1,LDLR.ACC.CUR>
    R.FT(FT.PROFIT.CENTRE.CUST) = R.NEW( LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
    R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.DEBIT.ACCT.NO) = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUS,CUS.BRN)
*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS,CUS.BRN)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.BRN=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUS.BRN = CUS.BRN[2]
    CUS.BRN = TRIM(CUS.BRN,"0","L")

    IF CUS.BRN = 1 THEN
        R.FT(FT.CREDIT.ACCT.NO) = MYTAX.ACC1
    END ELSE
        IF LEN (CUS.BRN)>1 THEN MYTAXBRN[9,2]=CUS.BRN
        IF LEN (CUS.BRN)=1 THEN MYTAXBRN[9,2]="0":CUS.BRN
        MYTAXBRN = MYTAX.ACC1[1,9]:CUS.BRN:MYTAX.ACC1[11,2]
        R.FT(FT.CREDIT.ACCT.NO) = MYTAXBRN
    END

    CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
    IF W.STATUS # "L" THEN
        IF NOT(ER.MSG) THEN
            ER.MSG = "ERROR CREATING FT"
        END
    END

    RETURN
*================================================================
SECOND:

    R.FT(FT.TRANSACTION.TYPE) = "ACLG"
    R.FT(FT.DEBIT.AMOUNT) = MYTAX2
    R.FT(FT.DEBIT.CURRENCY) = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    R.FT(FT.CREDIT.CURRENCY) = R.NEW( LD.LOCAL.REF)< 1,LDLR.ACC.CUR>
    R.FT(FT.PROFIT.CENTRE.CUST) = R.NEW( LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
    R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.DEBIT.ACCT.NO) = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUS,CUS.BRN)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.BRN=R.ITSS.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
    IF CUS.BRN = 1 THEN
        R.FT(FT.CREDIT.ACCT.NO) = MYTAX.ACC2
    END ELSE
        IF LEN (CUS.BRN)>1 THEN MYTAXBRN[9,2]=CUS.BRN
        IF LEN (CUS.BRN)=1 THEN MYTAXBRN[9,2]="0":CUS.BRN
        MYTAXBRN = MYTAX.ACC2[1,9]:CUS.BRN:MYTAX.ACC2[11,2]
        R.FT(FT.CREDIT.ACCT.NO) = MYTAXBRN
    END

    CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
    IF W.STATUS # "L" THEN
        IF NOT(ER.MSG) THEN
            ER.MSG = "ERROR CREATING FT"
        END
    END


    RETURN
*=============================================================

END
