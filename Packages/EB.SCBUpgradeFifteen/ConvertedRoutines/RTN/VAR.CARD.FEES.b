* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*---- CREATE BY NESSMA
    SUBROUTINE VAR.CARD.FEES

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CARD.FEES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CARD.FEES.ALL
*-------------------------------------------------
    CUSTOMER.ID = FIELD(ID.NEW , '.' , 1 )
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.TARGET,CUSTOMER.ID,TARGT)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSTOMER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
TARGT=R.ITSS.CUSTOMER<EB.CUS.TARGET>

****UPDATED BY NESSREEN AHMED 30/01/2020********
**** IF TARGT NE 8 THEN
    CARD.ACTION = R.NEW(SCB.VISA.CARD.ACTION)
    CUS.PAY     = R.NEW(SCB.VISA.PAYROLL.CUST)
    CARD.TYPE  = R.NEW(SCB.VISA.CARD.TYPE)
    IF (TARGT EQ 8 OR TARGT EQ 30 )AND CARD.ACTION EQ 1 AND (CARD.TYPE EQ 801 OR CARD.TYPE EQ 810) THEN
****END OF UPDATE 30/01/2020********************
***UPDATED BY NESSREEN AHMED 19/2/2020******************************************************
        CH.FEE = 'NO'
    END ELSE
        CH.FEE = 'YES'
    END
    IF CH.FEE = 'YES' THEN
****END OF UPDATE 19/2/2020**********************************************************************
        GOSUB INIT
        GOSUB PROCESS
    END
    RETURN
*------------------------------------------------
INIT:
*----
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS   = "CARD.FEES2"
    COMP          = C$ID.COMPANY
    COMP.CODE     = COMP[8,2]

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "CARD.FEES.":ID.NEW
    COMMA    = ","

    RETURN
*---------------------------------------------------
OFS.RECORD:
*----------
    COMP          = ID.COMPANY
    OFS.USER.INFO = "AUTO.CHRGE//":COMP
    TRNS.TYPE     = "AC52"
    DR.ACC        = R.NEW(SCB.VISA.FEES.DEBIT.ACCT)
    DR.CUR        = "EGP"
    CR.ACC        = "PL57040"
    AMOUNT        = R.NEW(SCB.VISA.FEES.AMT)
    IF AMOUNT GT 0 THEN
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE="    :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :DR.CUR:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :AMOUNT:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE="   :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA
        OFS.MESSAGE.DATA :=  "NOTE.DEBITED="        :ID.NEW:COMMA
        OFS.MESSAGE.DATA :=  "VERSION.NAME="        :",CARD.FEES2"

        OFS.REC  = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA
        OFS.REC := COMMA:OFS.MESSAGE.DATA
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN,NEW.FILE ON ERROR  TEXT = " ERROR ";CALL REM
    END

    RETURN
*----------------------------------------------
PROCESS:
*-------
    FN.CF = "F.SCB.ATM.CARD.FEES"   ; F.CF = "" ; R.CF = ""
    CALL OPF(FN.CF, F.CF)

    FN.CF2 = "F.SCB.CARD.FEES.ALL"  ; F.CF2 = "" ; R.CF2 = ""
    CALL OPF(FN.CF2, F.CF2)
*----------------------------------------------
    APP.FEES.ID = ""
    APP.FEES.ID = R.NEW(SCB.VISA.CARD.PRODUCT):".":R.NEW(SCB.VISA.CARD.TYPE)

    TMP.ID = ID.NEW
    CALL F.READ(FN.CF2,TMP.ID,R.CF2,F.CF2,E.CF2)
    IF E.CF2 THEN
        R.CF2<FEES.ALL.APP.ATM.ID>  = ID.NEW
        R.CF2<FEES.ALL.APP.FEES.ID> = APP.FEES.ID
        R.CF2<FEES.ALL.AMOUNT>      = R.NEW(SCB.VISA.FEES.AMT)
        R.CF2<FEES.ALL.DB.ACCT>     = R.NEW(SCB.VISA.FEES.DEBIT.ACCT)
        R.CF2<FEES.ALL.CR.ACCT>     = "PL57040"
        R.CF2<FEES.ALL.FLAG>        = "NO"
        R.CF2<FEES.ALL.COMPANY.ID>  = ID.COMPANY
        CALL F.WRITE(FN.CF2 ,TMP.ID, R.CF2)

        ACC.BAL = ""
*Line [ 144 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,R.NEW(SCB.VISA.FEES.DEBIT.ACCT),ACC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(SCB.VISA.FEES.DEBIT.ACCT),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BAL=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,R.NEW(SCB.VISA.FEES.DEBIT.ACCT),ACC.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(SCB.VISA.FEES.DEBIT.ACCT),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF ACC.CAT GE 6500 AND ACC.CAT LE 6599 THEN
            IF ACC.BAL LT R.NEW(SCB.VISA.FEES.AMT) THEN
                CALL EOD.ATM.CARD.FEES(ID.NEW)
            END ELSE
                GOSUB OFS.RECORD
            END
        END ELSE
            GOSUB OFS.RECORD
        END
    END ELSE
        TEXT = "EXCUTE BEFORE" ; CALL REM
    END
    RETURN
*------------------------------------------------------------------
END
