* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VIR.BR.CHECK.BALANCE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*------------------------------------------------
    IF V$FUNCTION = 'I' THEN
        LIQ.ACCT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,LIQ.ACCT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LIQ.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>

        IF CATEG EQ '6501' OR CATEG EQ '6502' OR CATEG EQ '6503' OR CATEG EQ '6504' OR CATEG EQ '6511' OR CATEG EQ '1001' THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('ACCOUNT':@FM:AC.WORKING.BALANCE,LIQ.ACCT,ACCT.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,LIQ.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACCT.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
            BR.AMT   = R.NEW(EB.BILL.REG.AMOUNT)
            BR.CHRG  = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
            BR.COM   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
            BR.LEN   = LEN(BR.COM)
            BR.COM   = BR.COM[4,BR.LEN]
            TOT.AMT  = BR.COM + BR.CHRG
            ETEXT    = ""
            E        = ""

            IF TOT.AMT GT 0 THEN
                IF TOT.AMT GT ACCT.BAL THEN
                    E = '������ �� ����'
                    CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR
                END
            END
        END
    END
*---------------------------------------------------------------
    RETURN
END
