* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* CREATED BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CHQ.YEAR.CHRG.NEW.MOD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*--------------------------------------------
    SSS = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE>
    DEPT.CODE = R.USER<EB.USE.DEPARTMENT.CODE>
    IF LEN(DEPT.CODE) EQ 1 THEN
        DEPT.CODE = "0":DEPT.CODE
    END
*--------------------------------------------
    FN.BS = "FBNK.SCB.BR.SLIPS"     ; F.BS = ""
    CALL OPF(FN.BS ,F.BS)

    BS.NUMBER = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.SLIP.REFER>
    OUR.REF   = BS.NUMBER

    CALL F.READ(FN.BS,BS.NUMBER,R.BS,F.BS,BS.ERR)
    CUS.CHRG     = R.BS<SCB.BS.TOT.CHRG2.AMT>
    PL.CHRG      = R.BS<SCB.BS.BR.COUNT.PL>
    INT.AC.CHRG  = R.BS<SCB.BS.BR.COUNT.INT.ACC>
*-------------------- (^^;) -----------------------*
    IF V$FUNCTION = 'A' THEN
        T.SEL  = "SELECT FBNK.BILL.REGISTER$NAU WITH SLIP.REFER EQ ":BS.NUMBER
        T.SEL := " AND @ID NE ": ID.NEW
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.NO.CHQ> EQ '1' AND SELECTED THEN
            E = "ALL BRs MUST BE AUTHORISED FIRST "
            CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
*----------------- (^^;) -----------------------*
            IF V$FUNCTION = 'A' AND R.NEW(EB.BILL.REG.RECORD.STATUS)='INAU' THEN
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.NO.CHQ> EQ '1' THEN
                    CATEG.CR     = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>[11,4]
                    CURR         = R.NEW(EB.BILL.REG.CURRENCY)

                    DR.ACCT      = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
                    DR.AMTT      = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
                    AMT          = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
                    V.DATE       = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>
*                   COM.TYP      = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>
                    COM.TYP      = "BILCHARG"
                    BR.CUS       = R.NEW(EB.BILL.REG.DRAWER)

                    FN.COM = 'FBNK.FT.CHARGE.TYPE' ; F.COM = ''
                    CALL OPF(FN.COM,F.COM)

                    CALL F.READ(FN.COM,COM.TYP,R.COM,F.COM,ERR111)
                    PL.COM = R.COM<FT5.CATEGORY.ACCOUNT>
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,DR.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>

                    IF CURR NE 'EGP' THEN
                        FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
                        CALL OPF(FN.CUR,F.CUR)
                        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
                        RATE   = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    END
***----------------------------- PL.CHRG  --------------------------------***
                    IF PL.CHRG GT 0 THEN
*----*
* CR *
*----*
                        TODP1 = TODAY
                        Y.ACCT = ''
                        IF CURR NE 'EGP' THEN
                            LCY.AMT  = PL.CHRG * RATE
                            FCY.AMT  = PL.CHRG
                            CUR.RATE = RATE
                        END ELSE
                            LCY.AMT = PL.CHRG
                            FCY.AMT = ''
                            CUR.RATE= ''
                        END
                        CATEG  = CATEG.CR
                        GOSUB AC.STMT.ENTRY
                    END
****---------------------------- INT.AC.CHRG  ----------------------------****
                    IF INT.AC.CHRG GT 0 THEN
*                 Y.ACCT = CURR:'16109000100':ID.COMPANY[8,2]
                        Y.ACCT    = CURR:'16109000100':DEPT.CODE

                        IF CURR NE 'EGP' THEN
                            LCY.AMT = INT.AC.CHRG * RATE
                            FCY.AMT = INT.AC.CHRG
                            CUR.RATE= RATE
                        END ELSE
                            LCY.AMT = INT.AC.CHRG
                            FCY.AMT = ''
                            CUR.RATE= ''
                        END
                        CATEG  = CATEG.CR
                        PL.COM = ''
                        GOSUB AC.STMT.ENTRY
                    END
*-- END CR
*------------------------------- CUS.CHRG -------------------------
*-----
* DR
*-----
                    IF CUS.CHRG GT 0 THEN
                        TODP1  = TODAY
                        Y.ACCT = DR.ACCT
                        IF CURR NE 'EGP' THEN
                            LCY.AMT = CUS.CHRG * RATE * -1
                            FCY.AMT = CUS.CHRG * -1
                            CUR.RATE= RATE
                        END ELSE
                            LCY.AMT = CUS.CHRG * -1
                            FCY.AMT = ''
                            CUR.RATE= ''
                        END
                        CATEG  = CATEG.CR

                        GOSUB AC.STMT.ENTRY
                    END
*-- END DR
                END ;*END IF NUMBER OF CHCK
            END     ;*END OF AUTHORISE

            IF V$FUNCTION = 'A' AND  R.NEW(EB.BILL.REG.RECORD.STATUS)='RNAU' THEN
*-----
* CR REV
*-----
                IF CUS.CHRG GT 0 THEN
                    TODP1  = TODAY
                    Y.ACCT = DR.ACCT
                    IF CURR NE 'EGP' THEN
                        LCY.AMT = CUS.CHRG * RATE
                        FCY.AMT = CUS.CHRG
                        CUR.RATE= RATE
                    END ELSE
                        LCY.AMT = CUS.CHRG
                        FCY.AMT = ''
                        CUR.RATE= ''
                    END
                    CATEG  = CATEG.CR

                    GOSUB AC.STMT.ENTRY
                END
*-- END CR
*----*
* DR REV
*----*
                IF PL.CHRG GT 0 THEN
                    TODP1 = TODAY
                    Y.ACCT = ''
                    IF CURR NE 'EGP' THEN
                        LCY.AMT  = PL.CHRG * RATE * -1
                        FCY.AMT  = PL.CHRG * -1
                        LCY.AMT  = LCY.AMT * R.COM<FT5.FLAT.AMT>
                        FCY.AMT  = FCY.AMT * R.COM<FT5.FLAT.AMT>
                        CUR.RATE = RATE
                    END ELSE
                        LCY.AMT = PL.CHRG * -1
                        LCY.AMT = LCY.AMT * R.COM<FT5.FLAT.AMT>
                        FCY.AMT = ''
                        CUR.RATE= ''
                    END
                    CATEG  = CATEG.CR
                    GOSUB AC.STMT.ENTRY
                END
****---------------------------- INT.AC.CHRG  ----------------------------****
                IF INT.AC.CHRG GT 0 THEN
*                    Y.ACCT = CURR:'16109000100':ID.COMPANY[8,2]
                    Y.ACCT    = CURR:'16109000100':DEPT.CODE

                    IF CURR NE 'EGP' THEN
                        LCY.AMT = INT.AC.CHRG * RATE * -1
                        FCY.AMT = INT.AC.CHRG * -1
                        LCY.AMT = LCY.AMT * R.COM<FT5.FLAT.AMT>
                        FCY.AMT = FCY.AMT * R.COM<FT5.FLAT.AMT>
                        CUR.RATE= RATE
                    END ELSE
                        LCY.AMT = INT.AC.CHRG * -1
                        LCY.AMT = LCY.AMT * R.COM<FT5.FLAT.AMT>
                        FCY.AMT = ''
                        CUR.RATE= ''
                    END
                    CATEG  = CATEG.CR
                    PL.COM = ''
                    GOSUB AC.STMT.ENTRY
                END
*-- END DR
            END     ;*END OF REVERSE
        END         ;* END OF A
*--------------------------------
        RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
        ENTRY = ""
        MULTI.ENTRIES = ""

        ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
*        ENTRY<AC.STE.COMPANY.CODE>     = "EG00100": DEPT.CODE
        ENTRY<AC.STE.COMPANY.CODE>     = "EG00100": ID.COMPANY[8,2]
        IF SSS EQ 7 THEN
            ENTRY<AC.STE.TRANSACTION.CODE> = '870'
        END
        IF SSS EQ 6 OR SSS EQ 10 THEN
            ENTRY<AC.STE.TRANSACTION.CODE> = '374'
        END

        ENTRY<AC.STE.THEIR.REFERENCE>  = OUR.REF
        ENTRY<AC.STE.TRANS.REFERENCE>  = OUR.REF
        ENTRY<AC.STE.NARRATIVE>        = ""
        ENTRY<AC.STE.PL.CATEGORY>      = PL.COM
        ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
        ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
        ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
        ENTRY<AC.STE.VALUE.DATE>       = TODP1
        ENTRY<AC.STE.CURRENCY>         = CURR
        ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
        ENTRY<AC.STE.EXCHANGE.RATE>    = CUR.RATE
        ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
        ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
        ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
        ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
        ENTRY<AC.STE.BOOKING.DATE>     = TODAY
        ENTRY<AC.STE.CRF.TYPE>         = ""
        ENTRY<AC.STE.CRF.TXN.CODE>     = ""
        ENTRY<AC.STE.CRF.MAT.DATE>     = ""
        ENTRY<AC.STE.CHQ.TYPE>         = ""
        ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
*        ENTRY<AC.STE.CUSTOMER.ID>      = ""
        ENTRY<AC.STE.CUSTOMER.ID>      = BR.CUS
        ENTRY<AC.STE.OUR.REFERENCE>    = BS.NUMBER

        MULTI.ENTRIES<-1> = LOWER(ENTRY)
        TYPE = 'SAO'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
        RETURN
    END
