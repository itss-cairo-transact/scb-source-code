* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CREAT.CC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION


    FN.SLIPS = 'FBNK.CHEQUE.COLLECTION' ; F.SLIPS = '' ; R.SLIPS = ''

    CALL OPF( FN.SLIPS,F.SLIPS)
    CALL F.READ( FN.SLIPS,SLIP.REF, R.SLIPS, F.SLIPS, ETEXT)

    CC.ID = ID.NEW[3,10]
    CC.ID4 = "CC":CC.ID
    CC.ID2 = CC.ID4[1,7]
    CC.ID3 = CC.ID4[9,4]
    CC.ID1 = CC.ID2:"1":CC.ID3


    R.SLIPS<CHQ.COL.TXN.ID> = ID.NEW

    R.SLIPS<CHQ.COL.CREDIT.ACC.NO> = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>

    R.SLIPS<CHQ.COL.AMOUNT> = R.NEW(EB.BILL.REG.AMOUNT)

    R.SLIPS<CHQ.COL.ORIG.VALUE.DATE> = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

    R.SLIPS<CHQ.COL.SUSP.POSTED.TO> = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>

    R.SLIPS<CHQ.COL.CURRENCY> = R.NEW(EB.BILL.REG.CURRENCY)

    R.SLIPS<CHQ.COL.CHQ.STATUS> = 'DEPOSITED'

    R.SLIPS<CHQ.COL.STATUS.DATE> = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

    R.SLIPS<CHQ.COL.EXPOSURE.DATE> = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

    R.SLIPS<CHQ.COL.CURR.NO>  = R.NEW(EB.BILL.REG.CURR.NO)

    R.SLIPS<CHQ.COL.INPUTTER> = R.NEW(EB.BILL.REG.INPUTTER)

    R.SLIPS<CHQ.COL.DATE.TIME> = R.NEW(EB.BILL.REG.DATE.TIME)

    R.SLIPS<CHQ.COL.AUTHORISER> = R.NEW(EB.BILL.REG.AUTHORISER)

    R.SLIPS<CHQ.COL.CO.CODE> = R.NEW(EB.BILL.REG.CO.CODE)

    R.SLIPS<CHQ.COL.DEPT.CODE> = R.NEW(EB.BILL.REG.DEPT.CODE)

    CALL F.WRITE(FN.SLIPS,CC.ID1,R.SLIPS)

    RETURN
END
