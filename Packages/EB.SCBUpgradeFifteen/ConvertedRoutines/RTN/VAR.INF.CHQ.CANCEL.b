* @ValidationCode : MjoxMzkxMTQ3ODIwOkNwMTI1MjoxNjQ4NTQ0NzQzMzI0Ok1vdW5pcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Mar 2022 11:05:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>19</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.INF.CHQ.CANCEL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_INF.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUSTOMER.TAX
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    COMP1 = ID.COMPANY
    FN.COUNT = 'F.SCB.FT.DR.CHQ' ; F.COUNT = '' ; R.COUNT = ''
    CALL OPF(FN.COUNT,F.COUNT)
    FN.TAX = 'F.SCB.CUSTOMER.TAX' ; F.TAX = '' ; R.TAX = ''
    CALL OPF(FN.TAX,F.TAX)
    ACC.NO = ''
    IF V$FUNCTION = 'A' AND  R.NEW(INF.MLT.RECORD.STATUS) NE 'RNAU' THEN
*Line [ 43 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YCOUNT = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
        FOR XX = 1 TO YCOUNT
            ACC.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,XX>
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:2,ACC.NO,ACC.CAT)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            ACC.CAT=R.ITSS.ACCOUNT<2>
            IF ACC.CAT EQ '16151' AND (R.NEW(INF.MLT.SIGN)<1,XX>) EQ 'DEBIT' THEN
                GOSUB CHQ.CANCEL
            END
            IF (ACC.CAT EQ '16523' OR ACC.CAT EQ '16528' OR ACC.CAT EQ '16522' OR ACC.CAT EQ '16510') THEN
                IF (R.NEW(INF.MLT.SIGN)<1,XX>) EQ 'DEBIT' THEN
                    GOSUB TAX.REV
                END
            END
        NEXT XX
    END
**************************************************************
CHQ.CANCEL:
**********
* UPDATE THE TABLE SCB.FT.DR.CHQ
    ID.NO = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,XX>:".":R.NEW(INF.MLT.CHEQUE.NUMBER)<1,XX>
    CALL F.READ(FN.COUNT,ID.NO,R.COUNT,F.COUNT,ERR.CHQ)
    IF NOT(ERR.CHQ) THEN
        
*Line [ 43 ] SUPP.NAME instead of SUPP.SUPP.NAME  - ITSS - R21 Upgrade - 2021-12-26
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.SUPPLIER':@FM:SUPP.NAME,R.NEW(INF.MLT.LOCAL.REF)<1,INLR.SUPP.CODE>,SUPP.NAME)
        F.ITSS.SCB.SUPPLIER = 'F.SCB.SUPPLIER'
        FN.F.ITSS.SCB.SUPPLIER = ''
        CALL OPF(F.ITSS.SCB.SUPPLIER,FN.F.ITSS.SCB.SUPPLIER)
        CALL F.READ(F.ITSS.SCB.SUPPLIER,INLR.SUPP.CODE,R.ITSS.SCB.SUPPLIER,FN.F.ITSS.SCB.SUPPLIER,ERROR.SCB.SUPPLIER)
        SUPP.NAME=R.ITSS.SCB.SUPPLIER<@FM:SUPP.NAME,R.NEW(INF.MLT.LOCAL.REF)><1>
        R.COUNT<DR.CHQ.CHEQ.STATUS>   = 3
        CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
        CLOSE F.COUNT
    END
RETURN
**************************************************************
TAX.REV:
**********
    IF LEN(XX) EQ 1 THEN
        TTX = '0':XX
    END
    ID.TAX = 'TX':ID.NEW[3,10]:'01'
    R.TAX<CUS.TAX.CUSTOMER.ID>      = R.NEW(INF.MLT.LOCAL.REF)<1,INLR.SUPP.CODE>
    R.TAX<CUS.TAX.TOTAL.AMOUNT>     = (R.NEW(INF.MLT.GL.NUMBER)<1,XX>) * -1
    R.TAX<CUS.TAX.DISCOUNT.PERCENT> = (R.NEW(INF.MLT.DISC.PERCENT)<1,XX>) * -1
    R.TAX<CUS.TAX.DISCOUNT.AMOUNT>  = (R.NEW(INF.MLT.AMOUNT.LCY)<1,XX>) * -1
    R.TAX<CUS.TAX.VALUE.DATE>       = R.NEW(INF.MLT.VALUE.DATE)<1,XX>
    R.TAX<CUS.TAX.CO.CODE>          = COMP1
    CALL F.WRITE(FN.TAX,ID.TAX,R.TAX)
    CLOSE F.TAX
RETURN
**************************************************************
RETURN
END
