* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.DR.CHQ.PL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* UPDATE THE TABLE SCB.FT.DR.CHQ
    COMP1 = ID.COMPANY
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS) NE 'RNAU' THEN

        F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
        CALL OPF(FN.COUNT,F.COUNT)
        ID.NO = R.NEW(FT.CREDIT.ACCT.NO):".":R.NEW(FT.CHEQUE.NUMBER)
        T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ": ID.NO
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* TEXT = "KEY.LIST=":KEY.LIST ; CALL REM
        IF KEY.LIST THEN
            E = 'THE CHEQUE ISSUED BEFORE' ; CALL ERR; MESSAGE='REPEAT'
        END
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,R.NEW(FT.CREDIT.ACCT.NO),CR.AC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.CREDIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.AC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
*TEXT = ID.NO ; CALL REM
        R.COUNT = ''
        R.COUNT<DR.CHQ.COMPANY.CO> = CR.AC.COM
        R.COUNT<DR.CHQ.DEBIT.ACCT> = R.NEW(FT.DEBIT.ACCT.NO)
        R.COUNT<DR.CHQ.CHEQ.NO> = R.NEW(FT.CHEQUE.NUMBER)
        CRD.AMT = R.NEW(FT.AMOUNT.CREDITED)[4,19]
        TEXT = "CRD.AMT":CRD.AMT ; CALL REM
        R.COUNT<DR.CHQ.AMOUNT> = CRD.AMT
        R.COUNT<DR.CHQ.CURRENCY> = R.NEW(FT.CREDIT.CURRENCY)
        R.COUNT<DR.CHQ.BEN,1> = R.NEW(FT.BEN.CUSTOMER)
        R.COUNT<DR.CHQ.BEN,2> = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1>
        GOSUB CHQ.TYPE
        R.COUNT<DR.CHQ.CHEQ.TYPE> = FIRST.NO
        R.COUNT<DR.CHQ.NOS.ACCT> = R.NEW(FT.CREDIT.ACCT.NO)
        R.COUNT<DR.CHQ.TRANS.ISSUE> = ID.NEW
        R.COUNT<DR.CHQ.CHEQ.STATUS> = 1
        R.COUNT<DR.CHQ.CHEQ.TYPE> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>
* TEXT=R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>;CALL REM
        R.COUNT<DR.CHQ.ISSUE.BRN> = R.NEW(FT.DEPT.CODE)
        R.COUNT<DR.CHQ.CHEQ.DATE> = TODAY
        R.COUNT<DR.CHQ.OLD.CHEQUE.NO> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
*TEXT = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> ; CALL REM
        CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
*  TEXT = 'WRITE' ; CALL REM
* CALL JOURNAL.UPDATE(ID.NO)
        CLOSE F.COUNT
* TEXT = 'CLOSE' ; CALL REM
**************************************************************
CHQ.TYPE:
        FIRST.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>[1,1]
* TEXT = "FIRST.NO": FIRST.NO ; CALL REM

        RETURN
**************************************************************
    END
    RETURN
END
