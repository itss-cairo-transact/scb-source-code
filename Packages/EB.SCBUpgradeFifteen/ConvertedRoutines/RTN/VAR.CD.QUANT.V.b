* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>63</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CD.QUANT.V

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP.USER =ID.COMPANY

    IF V$FUNCTION EQ 'A' THEN
        IF R.NEW(LD.RECORD.STATUS) NE 'RNAU' THEN
            GOSUB INITIALISE
            GOSUB BUILD.RECORD
***** SCB R15 UPG 20160703 - S
*                CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
        END
    END

    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "LD.LOANS.AND.DEPOSITS"
    OFS.OPTIONS="SCB.CD.QUAN1"
*** OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    D.T = R.NEW(LD.DATE.TIME)
    INPP = R.NEW(LD.INPUTTER)<1,1>

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
*   NEW.FILE = "T":TNO:".":INPP:RND(10000):"_":D.T
    NEW.FILE = ID.NEW:".":RND(10000)
*   NEW.FILE = "T":TNO:".":"CD'S"

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END
    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME

        END
    END


    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
*******************************************************
    CALL F.READ('F.LD.LOANS.AND.DEPOSITS$NAU',ID.NEW,R.TERM,VF.LD.LOANS.AND.DEPOSITS, ER.MSG)
    CD.QUAN=R.TERM<LD.LOCAL.REF><1,LDLR.CD.QUANTITY>
    NO.CD=CD.QUAN-1
*******************************************************
    DIM ZZZ(NO.CD)
    FOR I = 1 TO NO.CD
        COMMA = ","

        OFS.MESSAGE.DATA =  "CUSTOMER.ID=":R.TERM<LD.CUSTOMER.ID>:COMMA
        OFS.MESSAGE.DATA := "CURRENCY=":R.TERM<LD.CURRENCY>:COMMA
        OFS.MESSAGE.DATA := "AMOUNT=":R.TERM<LD.AMOUNT>:COMMA
**1-4-2014        OFS.MESSAGE.DATA := "CURRENCY.MARKET=":'1':COMMA
        OFS.MESSAGE.DATA := "VALUE.DATE=":R.TERM<LD.VALUE.DATE>:COMMA
        OFS.MESSAGE.DATA := "FIN.MAT.DATE=":R.TERM<LD.FIN.MAT.DATE>:COMMA
        OFS.MESSAGE.DATA := "CATEGORY=":R.TERM<LD.CATEGORY>:COMMA
        OFS.MESSAGE.DATA := "DRAWDOWN.ACCOUNT=":R.TERM<LD.DRAWDOWN.ACCOUNT>:COMMA
        OFS.MESSAGE.DATA := "INT.RATE.TYPE=":'3':COMMA
        OFS.MESSAGE.DATA := "INTEREST.BASIS=":'E':COMMA
        OFS.MESSAGE.DATA := "INT.PAYMT.METHOD=":'1':COMMA
        OFS.MESSAGE.DATA := "INTEREST.KEY=":R.TERM<LD.INTEREST.KEY>:COMMA
        OFS.MESSAGE.DATA := "DEFINE.SCHEDS=":'YES':COMMA
        OFS.MESSAGE.DATA := "AUTO.SCHEDS=":'NO':COMMA
        OFS.MESSAGE.DATA := "MATURE.AT.SOD=":'YES':COMMA
        OFS.MESSAGE.DATA := "INT.LIQ.ACCT=":R.TERM<LD.INT.LIQ.ACCT>:COMMA
**************************
        VER.NAME=R.TERM<LD.LOCAL.REF><1,LDLR.VERSION.NAME>
        VER.NAME = FIELD(VER.NAME,',',2)
        OFS.MESSAGE.DATA := "LOCAL.REF:6:1=?":VER.NAME:COMMA
***************************************************
        OFS.MESSAGE.DATA := "LOCAL.REF:54:1=":R.TERM<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>:COMMA
        OFS.MESSAGE.DATA := "LOCAL.REF:83:1=":R.TERM<LD.LOCAL.REF><1,LDLR.CD.TYPE>:COMMA
        OFS.MESSAGE.DATA := "LOCAL.REF:32:1=":R.TERM<LD.LOCAL.REF><1,LDLR.IN.RESPECT.OF>
        ZZZ(I) = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
        WRITESEQ ZZZ(I) TO V.FILE.IN ELSE
            PRINT  'CAN NOT WRITE LINE ':ZZZ(I)
        END

*        GOSUB CREATE.FILE
    NEXT I
    RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(LD.DATE.TIME)
    INPP = R.NEW(LD.INPUTTER)<1,1>


    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
*WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":INPP:RND(10000):"_":D.T ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
*WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.NEW:".":RND(10000) ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    RETURN
*--------------------------------------------------------
END
