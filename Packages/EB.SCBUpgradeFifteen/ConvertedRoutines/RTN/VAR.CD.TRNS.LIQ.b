* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CD.TRNS.LIQ
*WAITING A NEW TRANSACTION CODE TO EDIT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*-------------------------------------------------------

    MESSAGE  = ''
    ERR.MESS = ''

    IF V$FUNCTION EQ 'I' THEN
*------------------------  INP  ----------------------
        IF  R.NEW(LD.RECORD.STATUS)[1,3] EQ 'INA' THEN
            MULTI.ENTRIES = ''
            GOSUB AC.STMT.ENTRY
            TYPE = 'DEL'
            CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
            IF END.ERROR OR TEXT = "NO" THEN RETURN

        END
        MULTI.ENTRIES = ''
        GOSUB AC.STMT.ENTRY
        TYPE = 'VAL'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END
*------------------------  DEL  ----------------------

    IF V$FUNCTION EQ 'D' THEN
        MULTI.ENTRIES = ''
        GOSUB AC.STMT.ENTRY
        TYPE = 'DEL'
        CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")
        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END

*------------------------  AUT ----------------------

    IF V$FUNCTION EQ 'A' AND R.NEW(LD.RECORD.STATUS)[1,3]= 'INA' THEN
        TYPE = 'AUT'
        CALL EB.ACCOUNTING("SYS",TYPE, "" , "" )
        IF END.ERROR OR TEXT = "NO" THEN RETURN
    END




    RETURN


*------------------------------
AC.STMT.ENTRY:

    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]

**    AMT      = R.NEW(LD.AMOUNT)
    AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.LIQ.AMT>
    LD.CATG  = R.NEW(LD.CATEGORY)
    CURR     = R.NEW(LD.CURRENCY)

    CR.DATA  = TODAY
    DB.DATE  = TODAY

*--------
*   CR  TRANSACTION FOR CUSTOMER
*--------
*                 ----    1 CR    ----
**FOR ISSUANCE    CR.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>
    CR.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.ACCT.STMP>

*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
    Y.ACCT.CR = CR.ACCT


    LCY.AMT.CR = AMT

    FCY.AMT.CR = ''


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.CR
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '420'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT> NE '' THEN
        ENTRY<AC.STE.NARRATIVE>    =  R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
    END
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.CR
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = CR.DATA
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.CR
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     =TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)


    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""
*-------
*  DR  TRANSACTION FOR EGP11005000100099
*-------
******ISSUANCE    DB.ACCT    =  R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    DB.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>
    Y.ACCT.DR = DB.ACCT


    LCY.AMT.DR =  AMT  * -1



    FCY.AMT.DR = ''

*****************
*    ----    2 DR   ---

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.DR
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '420'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW

    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT> NE '' THEN
        ENTRY<AC.STE.NARRATIVE>    =  R.NEW(LD.LOCAL.REF)<1,LDLR.CR.NOSTRO.ACCT>
    END

    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.DR
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = DB.DATE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.DR
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""
      ********************************* COMM CR DR TRANSACTION

      **    AMT      = R.NEW(LD.AMOUNT)
    AMT.COM = R.NEW(LD.LOCAL.REF)<1,LDLR.MRG.CHRG.AMT>
    LD.CATG  = R.NEW(LD.CATEGORY)
    CURR     = R.NEW(LD.CURRENCY)

    CR.DATA  = TODAY
    DB.DATE  = TODAY

*--------
*   CR TRANSACTION FOR COMMISSION FOR PL52890
*--------
*                 ----    1 CR    ----
**FOR ISSUANCE    CR.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>
    CR.ACCT.COM    = '52890'

*Line [ 227 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CR.ACCT.COM,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CR.ACCT.COM,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
    Y.ACCT.CR.COM = CR.ACCT.COM


    LCY.AMT.CR.COM = AMT.COM

    FCY.AMT.CR.COM = ''


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ''
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '420'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.PL.CATEGORY>      = Y.ACCT.CR.COM
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.CR.COM
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = CR.DATA
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.CR.COM
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)


    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""
*-------
*  DR  TRANSACTION FOR COMMISSION FOR EGP1100500010099
*-------
******ISSUANCE    DB.ACCT    =  R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    DB.ACCT.COM    = R.NEW(LD.LOCAL.REF)<1,LDLR.CREDIT.ACCT>
    Y.ACCT.DR.COM = DB.ACCT.COM


    LCY.AMT.DR.COM =  AMT.COM  * -1



    FCY.AMT.DR.COM = ''

*****************
*    ----    2 DR   ---

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT.DR.COM
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '420'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT.DR.COM
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.VALUE.DATE>       = DB.DATE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT.DR.COM
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = ACC.OFFICER
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW
    ENTRY<AC.STE.CRF.PROD.CAT>     = LD.CATG
    ENTRY<AC.STE.OVERRIDE>         = R.NEW(LD.OVERRIDE)
    ENTRY<AC.STE.STMT.NO>          = R.NEW(LD.STMT.NO)

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    ENTRY = ""

      **********************************
    RETURN
END
