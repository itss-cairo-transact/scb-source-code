* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-42</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.DR.PROV.ACCT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON


    IF V$FUNCTION EQ 'A' OR V$FUNCTION EQ 'R' THEN

        FN.LCC = 'FBNK.LETTER.OF.CREDIT' ;F.LCC = '' ; R.LCC = ''
        CALL OPF(FN.LCC,F.LCC)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""
        ID.LC = ID.NEW[1,12]
        CALL F.READ(FN.LCC,ID.LC,R.LCC,F.LCC,READ.ERR)


        LOC.REF = R.LCC<TF.LC.LOCAL.REF>

        TEXT = LOC.REF<1,LCLR.TEMP.PROV.ACC>:'TEST';CALL REM
        TEXT = LC.REC(TF.LC.CREDIT.PROVIS.ACC):'PROV.ACC';CALL REM
        TEXT = LC.REC(TF.LC.LOCAL.REF)<1,LCLR.TEMP.PROV.ACC>:'TEMP';CALL REM
**--------------------20110627
*     IF LOC.REF<1,LCLR.TEMP.PROV.ACC> NE '' THEN
*      R.LCC<TF.LC.CREDIT.PROVIS.ACC> = LOC.REF<1,LCLR.TEMP.PROV.ACC>
*     END
**--------------------20110627
        IF LOC.REF<1,LCLR.LOCAL.PERCENT>  NE '' THEN
            R.LCC<TF.LC.PROVIS.PERCENT> = LOC.REF<1,LCLR.LOCAL.PERCENT>
        END
        IF LOC.REF<1,LCLR.LOCAL.PROV>  NE '' THEN
            R.LCC<TF.LC.PROVIS.AMOUNT>=LOC.REF<1,LCLR.LOCAL.PROV>
        END


        R.LCC<TF.LC.LOCAL.REF,LCLR.TEMP.PROV.ACC> = ''
        R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PERCENT> = ''
        R.LCC<TF.LC.LOCAL.REF,LCLR.LOCAL.PROV>  = ''



        TEXT=R.LCC<TF.LC.CREDIT.PROVIS.ACC>:'CREDIT ACCT'

*WRITE  R.LCC TO F.LCC ,ID.LC ON ERROR
*   PRINT "CAN NOT WRITE RECORD":KEY.LIST:"TO" :FN.LCC
*END
        CALL F.WRITE(FN.LCC,ID.LC, R.LCC)
    END
    RETURN
END
