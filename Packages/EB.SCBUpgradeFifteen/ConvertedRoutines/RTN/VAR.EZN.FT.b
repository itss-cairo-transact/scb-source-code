* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.EZN.FT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MALIA.EZN
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    ACCT=  R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT,CATT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATT=R.ITSS.ACCOUNT<AC.CATEGORY>

    IF CATT EQ 11214 THEN
        FN.EZN = 'F.SCB.MALIA.EZN' ;F.EZN = '' ; R.EZN = ''
        CALL OPF(FN.EZN,F.EZN)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""

        ID.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.OPERATION.CODE>

        IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3] = 'INA' THEN
            CALL F.READ(FN.EZN,ID.NO,R.EZN,F.EZN,ERR1)
            IF R.EZN THEN
                E = ' �� ����� ����� �� ��� '
            END
            R.EZN<EZN.ACCOUNT.NO> = R.NEW(FT.DEBIT.ACCT.NO)
            R.EZN<EZN.OLD.REF>    = R.NEW(FT.LOCAL.REF)<1,FTLR.OPERATION.CODE>
            R.EZN<EZN.AMOUNT>     = R.NEW(FT.DEBIT.AMOUNT)
            R.EZN<EZN.OUTSTAND.AMOUNT> = R.NEW(FT.DEBIT.AMOUNT)
            R.EZN<EZN.EMP.NO>     = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED>
            R.EZN<EZN.ISSUE.DATE> = R.NEW(FT.PROCESSING.DATE)
            R.EZN<EZN.STATUS>     = 'O'
            R.EZN<EZN.CURRENCY>   = R.NEW(FT.DEBIT.CURRENCY)
            R.EZN<EZN.INPUTTER>    = R.NEW(FT.INPUTTER)<1,1>
            R.EZN<EZN.AUTHORISER>  = R.NEW(FT.AUTHORISER)
            R.EZN<EZN.OTHER.REF>   = "EZN"
            R.EZN<EZN.OUR.REF> = ID.NEW

*WRITE  R.EZN TO F.EZN , ID.NO  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":ID.NO:"TO" :FN.EZN
*END
*            CALL F.WRITE(FN.RZN,ID.NO, R.RZN)
            CALL F.WRITE(FN.EZN,ID.NO,R.EZN)
        END
        IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3] = 'RNA' THEN
            ID.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.OPERATION.CODE>
*DELETE F.EZN , ID.NO
            CALL F.DELETE (FN.EZN , ID.NO)
        END
        CALL REBUILD.SCREEN
    END
************************************************************
    RETURN
END
