* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.WRT.TT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.TT
*----------------------------------------
    FN.TMP = "F.SCB.SUEZ.TT" ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    FN.TT  = "FBNK.TELLER"   ; F.TT  = ""
    CALL OPF(FN.TT, F.TT)

    TT.NO  = ID.NEW
    CALL F.READ(FN.TMP,TT.NO,R.TMP,F.TMP,E.TMP)
    TT.AMOUNT = R.NEW(TT.TE.AMOUNT.LOCAL.1)

    IF V$FUNCTION = 'I' THEN
        R.TMP<SUEZ.TT.AMOUNT>      = TT.AMOUNT
        R.TMP<SUEZ.TT.OUTSTANDING> = TT.AMOUNT
        CALL F.WRITE(FN.TMP,TT.NO,R.TMP)
    END

    IF V$FUNCTION = 'R' THEN
        R.TMP<SUEZ.TT.AMOUNT>      = -1
        R.TMP<SUEZ.TT.OUTSTANDING> = -1
        CALL F.WRITE(FN.TMP,TT.NO,R.TMP)
    END
*----------------------------------------
    RETURN
END
