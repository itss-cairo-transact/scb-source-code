* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LOAN.AC.PAY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC
*-------------------------------------------------------
    ETEXT   =''
    FN.LO   = 'F.SCB.LOAN.AC'    ; F.LO   = '' ; R.LO  = ''
    CALL OPF(FN.LO,F.LO)

    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS)[1,3]='INA' THEN
        KEY.LIST  = "" ; SELECTED = "" ; ER.MSG = ""
        REF       = R.NEW(FT.DEBIT.THEIR.REF)
        DB.ACCT   = R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DB.ACCT,DB.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DB.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
        TEMP.ID = "...": DB.CUST : "..." : REF : "..."

        T.SEL = "SELECT F.SCB.LOAN.AC WITH @ID LIKE  ": TEMP.ID
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED EQ 1 THEN
            CALL F.READ(FN.LO,KEY.LIST,R.LO,F.LO,READ.ERR)
            ID.LOAN = KEY.LIST<1>
            CALL F.READ(FN.LO,ID.LOAN,R.LO,F.LO,READ.ERR)

*Line [ 52 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.M = DCOUNT(R.LO<AC.LO.PAY.DATE>,@VM)
            X    = NO.M + '1'
            R.LO<AC.LO.PAY.DATE,X>      = TODAY
            R.LO<AC.LO.FT.REF,X>        = ID.NEW
            R.LO< AC.LO.AMOUNT.PAID,X>  = R.NEW(FT.CREDIT.AMOUNT)

            AMT    =  R.LO<AC.LO.OUTSTAND.AMOUNT> - R.NEW(FT.CREDIT.AMOUNT)
            IF AMT = '0'  THEN
                R.LO<AC.LO.STATUS>      = 'P'
            END
            R.LO<AC.LO.OUTSTAND.AMOUNT> =  R.LO<AC.LO.OUTSTAND.AMOUNT> - R.NEW(FT.CREDIT.AMOUNT)

*WRITE  R.LO TO F.LO , ID.LOAN  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":ID.LOAN:"TO" :F.LO
*END
            CALL F.WRITE (FN.LO, ID.LOAN,R.LO)
        END
    END
    RETURN
END
