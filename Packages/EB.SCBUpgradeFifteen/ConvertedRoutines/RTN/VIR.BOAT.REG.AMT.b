* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VIR.BOAT.REG.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*----------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*----------------------------------------
INITIALISE:

    FN.BOT = 'F.SCB.BOAT.TOTAL' ; F.BOT = ''
    CALL OPF(FN.BOT,F.BOT)

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    BOT.ID  = R.NEW(BO.BOT.ID)
    BOAT.ID = R.NEW(BO.BOAT.CODE)
    CUR     = R.NEW(BO.CURRENCY)
    WS.AC   = R.NEW(BO.ACCOUNT.NO)

    IF V$FUNCTION = 'I' THEN

        IF WS.AC EQ '' THEN
            IF CUR EQ 'EGP' THEN
                R.NEW(BO.AMOUNT) = R.NEW(BO.AMOUNT) - 10

                TEXT = '�� ����� ������ ������ ����� ������� ' ; CALL REM
            END

            IF CUR EQ 'USD' THEN
                R.NEW(BO.AMOUNT) = R.NEW(BO.AMOUNT) - 2
                TEXT =  '�� ����� ������ ������ ����� ������� ' ; CALL REM
            END
        END

        T.SEL = "SELECT ":FN.BOT:" WITH @ID EQ ":BOT.ID:" AND BOAT.CODE EQ ":BOAT.ID:" AND CURRENCY EQ ":CUR
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN

            CALL F.READ(FN.BOT,KEY.LIST,R.BOT,F.BOT,E1)
            BTT = R.BOT<BOT.BOAT.CODE>

*Line [ 78 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE BOAT.ID IN BTT<1,1> SETTING POS ELSE NULL

            R.BOT<BOT.REG.CUS,POS> -= 1

            IF WS.AC EQ '' THEN
                IF CUR EQ 'EGP' THEN
                    R.BOT<BOT.REG.AMT,POS> -= ( R.NEW(BO.AMOUNT) + 10 )
                END
                IF CUR EQ 'USD' THEN
                    R.BOT<BOT.REG.AMT,POS> -= ( R.NEW(BO.AMOUNT) + 2 )
                END
            END ELSE
                R.BOT<BOT.REG.AMT,POS> -= R.NEW(BO.AMOUNT)
            END

            CALL F.WRITE(FN.BOT,KEY.LIST,R.BOT)

        END

        CALL F.READ(FN.BOT,BOT.ID,R.BOT,F.BOT,E1)
*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAMT = DCOUNT(R.BOT<BOT.REG.AMT>,@VM)
        FOR I = 1 TO DAMT
            AMT.REG += R.BOT<BOT.REG.AMT><1,I>
        NEXT I
*----------------------------------------------
*Line [ 105 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCUS = DCOUNT(R.BOT<BOT.REG.CUS>,@VM)
        FOR X = 1 TO DCUS
            CUS.REG += R.BOT<BOT.REG.CUS><1,X>
        NEXT X
**-----------------
        IF AMT.REG EQ 0 AND CUS.REG EQ 0 THEN
            R.BOT<BOT.STATUS> = '2'
            CALL F.WRITE(FN.BOT,KEY.LIST,R.BOT)
        END
    END
*************************************************
    IF V$FUNCTION = 'D' THEN
        T.SEL = "SELECT ":FN.BOT:" WITH @ID EQ ":BOT.ID:" AND BOAT.CODE EQ ":BOAT.ID:" AND CURRENCY EQ ":CUR
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CALL F.READ(FN.BOT,KEY.LIST,R.BOT,F.BOT,E1)
            BTT = R.BOT<BOT.BOAT.CODE>
*Line [ 123 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE BOAT.ID IN BTT<1,1> SETTING POS ELSE NULL
            R.BOT<BOT.REG.CUS,POS> += 1

            IF WS.AC EQ '' THEN
                IF CUR EQ 'EGP' THEN
                    R.BOT<BOT.REG.AMT,POS> += ( R.NEW(BO.AMOUNT) + 10 )
                END
                IF CUR EQ 'USD' THEN
                    R.BOT<BOT.REG.AMT,POS> += ( R.NEW(BO.AMOUNT) + 2 )
                END
            END ELSE
                R.BOT<BOT.REG.AMT,POS> += R.NEW(BO.AMOUNT)
            END

            CALL F.WRITE(FN.BOT,KEY.LIST,R.BOT)
        END
        CALL F.READ(FN.BOT,BOT.ID,R.BOT,F.BOT,E1)
*Line [ 141 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DAMT = DCOUNT(R.BOT<BOT.REG.AMT>,@VM)
        FOR I = 1 TO DAMT
            AMT.REG += R.BOT<BOT.REG.AMT><1,I>
        NEXT I
*----------------------------------------------
*Line [ 147 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCUS = DCOUNT(R.BOT<BOT.REG.CUS>,@VM)
        FOR X = 1 TO DCUS
            CUS.REG += R.BOT<BOT.REG.CUS><1,X>
        NEXT X
**-----------------
        IF AMT.REG NE 0 AND CUS.REG NE 0 THEN
            R.BOT<BOT.STATUS> = '1'
            CALL F.WRITE(FN.BOT,KEY.LIST,R.BOT)
        END
    END
*************************************************

    RETURN
************************************************************
END
