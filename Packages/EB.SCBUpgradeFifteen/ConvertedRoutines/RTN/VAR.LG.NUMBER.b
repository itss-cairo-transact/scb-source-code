* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
********* WAEL **************

SUBROUTINE VAR.LG.NUMBER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

IF V$FUNCTION = 'A' THEN
      NEW.NUM = R.NEW( LD.LOCAL.REF)<1,LDLR.LG.NUMBER,1> ; NEW.NUM[2,1] = 'D'
      T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":NEW.NUM:"..."
      CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
      IF KEY.LIST THEN
            FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = '';R.LD=''
            CALL OPF(FN.LD,F.LD)
            MYLD = NEW.NUM:";":SELECTED
            LAST = R.NEW( LD.LOCAL.REF)<1,LDLR.LG.NUMBER,2>
                CALL F.READ(FN.LD,MYLD,R.LD,F.LD,E1)
                XX = R.LD<LD.LOCAL.REF,LDLR.LG.NUMBER,2>
*----- SUBROUTINE TO UPDATE/WRITE IN HISTORY FILE -----
            IF XX = NEW.NUM THEN
                R.LD<LD.LOCAL.REF,LDLR.LG.NUMBER,2>= ID.NEW
                CALL F.WRITE(FN.LD,MYLD,R.LD)
                CALL JOURNAL.UPDATE(MYLD)
            END ELSE
                MYLD = XX
                FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = '';R.LD=''
                CALL OPF(FN.LD,F.LD)
                CALL F.READ(FN.LD,MYLD,R.LD,F.LD,E1)
                R.LD<LD.LOCAL.REF,LDLR.LG.NUMBER,2>= ID.NEW
                CALL F.WRITE(FN.LD,MYLD,R.LD)
                CALL JOURNAL.UPDATE(MYLD)
            END

      END
*---------------------- FT.TRANSACTION --------------------------------------

* DIM R.FT(FT.AUDIT.DATE.TIME)
*    MAT R.FT = ""
*    ER.MSG = ""
*    ID.FT = ""
*    W.STATUS = ""
*
*    R.FT(FT.TRANSACTION.TYPE) = "ACLG"
*    R.FT(FT.DEBIT.AMOUNT) = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
*    R.FT(FT.DEBIT.CURRENCY) = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
*    R.FT(FT.CREDIT.CURRENCY) = R.NEW( LD.LOCAL.REF)< 1,LDLR.ACC.CUR>
*    R.FT(FT.PROFIT.CENTRE.CUST) = R.NEW( LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
*    R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
*    R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
*    R.FT(FT.DEBIT.ACCT.NO) = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>   ;  * FOR AC
*    R.FT(FT.CREDIT.ACCT.NO) = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
*
*
*
*    CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
*    IF W.STATUS # "L" THEN
*       IF NOT(ER.MSG) THEN
*          ER.MSG = "ERROR CREATING FT"
*       END
*    END

*------------------------------------------------------------
END

RETURN
END
