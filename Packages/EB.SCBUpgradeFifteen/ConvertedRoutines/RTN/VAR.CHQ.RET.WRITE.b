* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*---------------------------------
* CREATE BY NESSMA ON 2013/04/24
*---------------------------------
    SUBROUTINE VAR.CHQ.RET.WRITE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW
*---------------------------------------------------------------
    FN.CHQ = 'F.SCB.CHQ.RETURN.NEW'     ; F.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    CHQ.ID = R.NEW(FT.DEBIT.THEIR.REF)
    CALL F.READ(FN.CHQ,CHQ.ID,R.TEMP,F.CHQ,ERR.CHQ)
    IF ERR.CHQ THEN
        ETEXT = "NOT VALID REFERENCE"
        CALL STORE.END.ERROR
    END ELSE
        R.TEMP<CHQ.RET.DEBIT.ACCT>  = R.NEW(FT.DEBIT.ACCT.NO)
*WRITE R.TEMP TO F.CHQ , CHQ.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':CHQ.ID:' TO FILE ':FN.CHQ
*END
        CALL F.WRITE(FN.CHQ,CHQ.ID, R.TEMP)
    END
*-------------------------------------------------------------------
    RETURN
END
