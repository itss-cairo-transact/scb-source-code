* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SF.WRITE.EXP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.EXPIRY
*--------------------------------------------
    FN.EXP = 'F.SCB.SF.EXPIRY'     ; F.EXP = ''
    CALL OPF( FN.EXP,F.EXP)

    EXP.ID = R.NEW(FT.DEBIT.ACCT.NO) : "-": R.NEW(FT.DEBIT.THEIR.REF)

    CALL F.READ(FN.EXP,EXP.ID,R.TEMP,F.EXP,ERR.EXP)
    R.TEMP<SF.EXP.SAFF.USED>  = 'YES'
  *  RENT.AMT  = R.TEMP<SF.EXP.RENT.AMOUNT>
    COMP.AMT  = R.TEMP<SF.EXP.COMP.AMOUNT>
    RENT.DD   = R.TEMP<SF.EXP.RENT.DATE>
    COMPANY.R = R.TEMP<SF.EXP.CO.CODE>

*WRITE R.TEMP TO F.EXP , EXP.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':EXP.ID:' TO FILE ':FN.EXP
*END
    CALL F.WRITE (FN.EXP,EXP.ID,R.TEMP)
*--- NEW RECORD

    DD =  R.NEW(FT.DEBIT.THEIR.REF)

*--- EDIT ON 2011-07-14 BY NESSMA
*    IF PGM.VERSION EQ ',SCB.SF.MANUAL' THEN

    IF DD EQ '' THEN
        DD = R.NEW(FT.DEBIT.VALUE.DATE)
    END
*------------------ NEW RECORD ---------------------------
    CALL ADD.MONTHS(DD,'12')
    EXP.ID = R.NEW(FT.DEBIT.ACCT.NO) : "-" : DD
    CALL F.READ(FN.EXP,EXP.ID,R.TEMP,F.EXP,ERR.EXP)

    R.TEMP<SF.EXP.RENEW.DATE>  = DD
    R.TEMP<SF.EXP.RENT.DATE>   = DD
    R.TEMP<SF.EXP.SAFF.USED>   = 'NO'
    R.TEMP<SF.EXP.ACCOUNT.NO>  = R.NEW(FT.DEBIT.ACCT.NO)

    COMP.AMT = R.NEW(FT.CREDIT.AMOUNT)
*    RENT.AMT = R.NEW(FT.CREDIT.AMOUNT) * 0.14

    R.TEMP<SF.EXP.COMP.AMOUNT> = COMP.AMT
*    R.TEMP<SF.EXP.RENT.AMOUNT> = RENT.AMT

    IF COMPANY.R EQ '' THEN
        COMPANY.R = R.NEW(FT.CO.CODE)
    END
    R.TEMP<SF.EXP.CO.CODE>     = COMPANY.R

*WRITE R.TEMP TO F.EXP , EXP.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':EXP.ID:' TO FILE ':FN.EXP
*END
    CALL F.WRITE (FN.EXP,EXP.ID,R.TEMP)
*-------------------------------------------------------------------
    RETURN
END
