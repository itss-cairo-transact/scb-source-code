* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.REV.CHEQ
********NESSREEN AHMED 22/4/2007***

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    IF V$FUNCTION ='R' THEN
        CHQ.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
*CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        MYBRNM  = AC.COMP[8,2]
***UPDATED BY NESSREEN AHMED 20/9/2016****
        MYBRN = TRIM(MYBRNM, "0" , "L")
***END OF UPDATE**************************
        KEY.TO.USE =  CHQ.NO:".":MYBRN
        INPUT.BUFFER =C.U:' SCB.P.CHEQ,SCB R ':KEY.TO.USE
    END
****UPDATED BY NESSREEN AHMED 1/10/2012********************
    IF V$FUNCTION = 'A' AND R.NEW(TT.TE.RECORD.STATUS)[1,3]='RNA' THEN
        CHQ.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.1)
*CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        MYBRNM  = AC.COMP[8,2]
***UPDATED BY NESSREEN AHMED 20/9/2016****
        MYBRN = TRIM(MYBRNM, "0" , "L")
***UPDATED BY NESSREEN AHMED 20/9/2016****
        KEY.TO.USE =  CHQ.NO:".":MYBRN
****13/8/2014*** INPUT.BUFFER =C.U:' SCB.P.CHEQ,SCB A ':KEY.TO.USE
        INPUT.BUFFER =C.U:' SCB.P.CHEQ,SCB R ':KEY.TO.USE
****END OF UPDATE 13/8/2014****
    END
****END OF UPDATE 1/10/2012********************************
    RETURN
END
