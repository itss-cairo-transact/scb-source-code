* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>180</Rating>
*-----------------------------------------------------------------------------
 SUBROUTINE VAR.TT.FT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

    DIM R.FT(FT.AUDIT.DATE.TIME)
    MAT R.FT = ""
    ER.MSG = ""
    ID.FT = ""
    W.STATUS = ""


    IF R.NEW(TT.TE.CURRENCY.1) = LCCY THEN AMMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    ELSE AMMT = R.NEW(TT.TE.AMOUNT.FCY.1)

     DEP = R.USER<EB.USE.DEPARTMENT.CODE>
     DEP = STR('0', 2- LEN(DEP) ):DEP
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*     CALL DBR('CURRENCY':@FM:2,R.NEW(TT.TE.CURRENCY.1),CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.NEW(TT.TE.CURRENCY.1),R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<2>
     CRACCT = DEP:'400010':CURR:'2101':'01'
     DEP2 = R.NEW(TT.TE.ACCOUNT.1)[1,2]
     DRACCT = DEP2:'400010':CURR:'2101':'01'

    R.FT(FT.TRANSACTION.TYPE) = "AC"
    R.FT(FT.DEBIT.AMOUNT) = AMMT
    R.FT(FT.DEBIT.CURRENCY) = R.NEW(TT.TE.CURRENCY.1)
    R.FT(FT.CREDIT.CURRENCY) = R.NEW(TT.TE.CURRENCY.1)
    R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(TT.TE.VALUE.DATE)
    R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(TT.TE.VALUE.DATE)
    R.FT(FT.DEBIT.ACCT.NO) = DRACCT
    R.FT(FT.CREDIT.ACCT.NO) = CRACCT

    CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
    IF W.STATUS # "L" THEN
       IF NOT(ER.MSG) THEN
          ER.MSG = "ERROR CREATING FT"
       END
   END

 RETURN
 END
