* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*********ABEER*********************
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE VAR.LG.INWARD.CONFIRMED

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CONFIRMED

* DAT=R.NEW(LD.LOCAL.REF)<1,LDLR.ISSUE.DATE>
    DAT=R.NEW(LD.LOCAL.REF)<1,LDLR.MATUR.DATE>
    FN.SCB.LG.CONFIRMED='F.SCB.LG.CONFIRMED';ID=ID.NEW:".":DAT;R.CHRG='';F.CHRG=''
    CALL F.READ( FN.SCB.LG.CONFIRMED,ID,R.CHRG,F.CHRG, ETEXT)
*******************************************************************
* IF NOT(ETEXT) THEN
    IF   R.NEW(LD.CHRG.CODE) THEN
        R.CHRG<SCB.CONFIRMED>='YES'
        CALL F.WRITE(FN.SCB.LG.CONFIRMED,ID,R.CHRG)

    END
*END
    RETURN
END
