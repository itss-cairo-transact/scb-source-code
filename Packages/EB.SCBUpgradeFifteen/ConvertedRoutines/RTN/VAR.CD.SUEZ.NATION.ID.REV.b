* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CD.SUEZ.NATION.ID.REV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*--------------------------------------------------

    R.TEMP      = ""
    COMMA       = ","
    TOT.LD.AMT  = R.NEW(LD.AMOUNT)
**********MODIFIED
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.CHK.FLG>  EQ '�����'  THEN
        NATIONAL.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>
    END ELSE
        NATIONAL.ID = R.NEW(LD.LOCAL.REF)<1,LDLR.EBAN>:'.':R.NEW(LD.LOCAL.REF)<1,LDLR.PLACE.ID.ISSUE>
    END
********MODIFIED
    FN.TEMP     = "F.SCB.CD.NATIONAL.ID"    ; F.TEMP = ""
    CALL OPF(FN.TEMP,F.TEMP)
    CALL F.READ(FN.TEMP,NATIONAL.ID,R.TEMP,F.TEMP,ERR1)
    BAL      = R.TEMP<SCB.CD.BALANCE>

    IF V$FUNCTION EQ 'D' THEN
        TOT.BAL  = BAL + TOT.LD.AMT
*TEXT=TOT.BAL:'D';CALL REM
    END
    IF V$FUNCTION EQ 'R' THEN
        TOT.BAL  = BAL - TOT.LD.AMT
        TEXT=TOT.BAL:'TOT BAL';CALL REM
    END
    R.TEMP<SCB.CD.BALANCE> = TOT.BAL

*WRITE  R.TEMP TO F.TEMP , NATIONAL.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":NATIONAL.ID:"TO" :FN.TEMP
* END
    CALL F.WRITE(FN.TEMP,NATIONAL.ID, R.TEMP)

    RETURN
END
