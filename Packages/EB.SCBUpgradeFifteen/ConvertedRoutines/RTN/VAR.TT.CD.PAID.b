* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.TT.CD.PAID

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.NATIONAL.ID
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    FN.CD.NSN = 'F.SCB.CD.NATIONAL.ID' ; F.CD.NSN = '' ; R.CD.NSN = ''
    CALL OPF(FN.CD.NSN,F.CD.NSN)

    REC.ID = ''
*****TRACING****
    DIR.NAME = '&SAVEDLISTS&'
    REC.ID = ID.NEW
    TEXT = 'REC.ID=':ID.NEW ; CALL REM
    NEW.FILE = "TT.CD_":REC.ID:"_":TODAY
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END

****************
    LD.REC = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CD.NUMBER>
    IF V$FUNCTION # 'R' THEN
        VISA.DATA = V$FUNCTION
        VISA.DATA := "|LD.REC="
        VISA.DATA := LD.REC
        CALL F.READ( FN.CD.NSN,LD.REC, R.CD.NSN, F.CD.NSN,ERR)
        LD.STAT = R.CD.NSN<SCB.CD.LD.STATUS,1>
        VISA.DATA := "|LD.STAT="
        VISA.DATA := LD.STAT
        IF LD.STAT NE "PAID" THEN
            TT.REC = ID.NEW
            VISA.DATA := "|TT.REC="
            VISA.DATA := TT.REC
            BR.CO    = R.NEW(TT.TE.DEPT.CODE)
            TT.AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
            VISA.DATA := "|TT.AMT="
            VISA.DATA := TT.AMT
****UPDATED BY NESSREEN AHMED 31/8/2019***********
** R.CD.NSN<SCB.CD.AMOUNT> = TT.AMT
** R.CD.NSN<SCB.CD.LD.NO> = TT.REC
            R.CD.NSN<SCB.CD.AMOUNT,1> = TT.AMT
            R.CD.NSN<SCB.CD.LD.NO,1> = TT.REC
            R.CD.NSN<SCB.CD.LD.STATUS,1> = "PAID"
            CALL F.WRITE(FN.CD.NSN,LD.REC,R.CD.NSN)
            WRITESEQ VISA.DATA TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':VISA.DATA
            END
        END
    END ELSE
*****In case of Reverse record******************
        CALL F.READ( FN.CD.NSN,LD.REC, R.CD.NSN, F.CD.NSN,ERR)
        IF NOT(ERR) THEN
*R.CD.NSN<SCB.CD.LD.NO,1> = ""
            R.CD.NSN<SCB.CD.LD.STATUS,1> = "REVERSED"
            CALL F.WRITE(FN.CD.NSN,LD.REC,R.CD.NSN)
        END
    END
    RETURN
END
