* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*******ABEER 6/04/2003*******************
*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LG.INWARD.CHRG
*A Routine To Default chrg code&Amt In SCB.LG.CONFIRMED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CONFIRMED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

***********Transfering FT For Stamp***************
    DIM R.FT(FT.AUDIT.DATE.TIME)
    MAT R.FT = ""
    ER.MSG = ""
    ID.FT = ""
    W.STATUS = ""
    TAX.AMOUNT=''
    CR.CCY=R.NEW( LD.LOCAL.REF)< 1,LDLR.ACC.CUR>
    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE
*************************************************************************
    FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
    CALL OPF(FN.LG,F.LG)
    CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)

    TAX.ACCOUNT1 = R.LG<SCB.LG.CH.TAX.ACCOUNT,1>
    TAX.AMOUNT1  = R.LG<SCB.LG.CH.TAX.AMOUNT,1>
    TAX.ACCOUNT2 = R.LG<SCB.LG.CH.TAX.ACCOUNT,2>
    TAX.AMOUNT2  = R.LG<SCB.LG.CH.TAX.AMOUNT,2>
    PL.CODE=R.LG<SCB.LG.CH.PL.CATEGORY>
    TEXT = PL.CODE ; CALL REM
***********************************************************
    GOSUB TAX.ONE
    GOSUB TAX.SECOND
    RETURN
************************************************************
TAX.ONE:
    R.FT(FT.TRANSACTION.TYPE) = "ACLG"
    R.FT(FT.DEBIT.AMOUNT) =TAX.AMOUNT1
    R.FT(FT.DEBIT.ACCT.NO) =PL.CODE
    R.FT(FT.CREDIT.ACCT.NO)= TAX.ACCOUNT1
    R.FT(FT.DEBIT.CURRENCY) = "EGP"
    R.FT(FT.CREDIT.CURRENCY) = "EGP"
    R.FT(FT.PROFIT.CENTRE.DEPT) = "1"
    R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.ORDERING.BANK)= R.NEW(LD.CUSTOMER.ID)
    GOSUB INFORM.FT
    RETURN
**************************************************************************
TAX.SECOND:
    R.FT(FT.TRANSACTION.TYPE) = "ACLG"
    R.FT(FT.DEBIT.AMOUNT) =TAX.AMOUNT2
    R.FT(FT.DEBIT.ACCT.NO) =PL.CODE
    R.FT(FT.CREDIT.ACCT.NO)= TAX.ACCOUNT2
    R.FT(FT.DEBIT.CURRENCY) = "EGP"
    R.FT(FT.CREDIT.CURRENCY) = "EGP"
    R.FT(FT.PROFIT.CENTRE.DEPT) = "1"
    R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
    R.FT(FT.ORDERING.BANK)= R.NEW(LD.CUSTOMER.ID)
    GOSUB INFORM.FT
    RETURN
**************************************************************************
INFORM.FT:
    CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
    IF W.STATUS # "L" THEN
        IF NOT(ER.MSG) THEN
            ER.MSG = "ERROR CREATING FT"
        END
    END
    RETURN
**************************************************
END
