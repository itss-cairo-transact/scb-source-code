* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
***************CREATED BY MAHMOUD MAGDY 2018/07/09
    SUBROUTINE VIR.CHQ.VALIDATE.2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ


    ID = ID.NEW
    ID.CHEQ = FIELD(ID,'.',2)
    CHEQ.NO = R.NEW(DR.CHQ.CHEQ.NO)
    OLD.CHEQ.NO = R.NEW(DR.CHQ.OLD.CHEQUE.NO)



    IF ID.CHEQ NE CHEQ.NO OR ID.CHEQ NE OLD.CHEQ.NO THEN
        E = 'INVALID MATCHING FOR CHEQUE NUMBERS'
        CALL ERR ;  MESSAGE ='REPEAT'
        CALL STORE.END.ERROR
    END

END
