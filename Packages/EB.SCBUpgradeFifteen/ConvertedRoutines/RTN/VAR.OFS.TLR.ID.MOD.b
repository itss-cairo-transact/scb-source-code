* @ValidationCode : MjotNjgyNTE3Nzk5OkNwMTI1MjoxNjQxOTQzMjk0MjY2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 12 Jan 2022 01:21:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-80</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.OFS.TLR.ID.MOD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER.ID
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*----------------------------------------

*** UPDATED BY MSABRY 2014/02/26 ***********
    IF R.NEW(MUA.RECORD.STATUS) EQ 'RNAU' THEN
        RETURN
    END
*** END OF UPDATE ***************************

    IF R.NEW(MUA.OVERRIDE) NE '' THEN
        WS.CHK.OVERRIDE    = 0
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.LVE.OVR.CNT     = DCOUNT(R.NEW(MUA.OVERRIDE),@VM)
        FOR ILVE.CLS = 1 TO WS.LVE.OVR.CNT
            WS.LVE.CLS         = FIELD(R.NEW(MUA.OVERRIDE)<1,ILVE.CLS>,"*",2)
            WS.LVE.CLS.AUTH    = FIELD(R.NEW(MUA.OVERRIDE)<1,ILVE.CLS>,"*",3)
            IF WS.LVE.CLS NE '' AND WS.LVE.CLS.AUTH EQ '' THEN
                RETURN
            END
*Line [57] should be ILVE.CLS insted of WS.LVE.OVR.CNT - ITSS - R21 Upgrade - 2021-12-26
        NEXT ILVE.CLS
* NEXT  WS.LVE.OVR.CNT
    END

    GOSUB INITIALISE
    GOSUB GET.DATA
    GOSUB GET.TLR.ID.OLD
    GOSUB BUILD.RECORD
RETURN
*----------------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    FN.TUSR   = "FBNK.TELLER.USER"  ; F.TUSR  = "" ; R.TUSR = ""
    CALL OPF (FN.TUSR,F.TUSR)

    FN.TT.ID   = "FBNK.TELLER.ID"  ; F.TT.ID  = "" ; R.TT.ID = ""
    CALL OPF (FN.TT.ID,F.TT.ID)

    COMMA = ","

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "TELLER.ID"
    OFS.OPTIONS      = "AUTO"

    COMP  = ID.COMPANY
    AC.BR = COMP[2]
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    WS.TLR.ID.OLD = ''

RETURN
*----------------------------------------------------
GET.DATA:
    WS.USR.ID        = R.NEW(MUA.USER.ID)

    WS.USR.CO        = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.USR.CO        = 'EG00100':WS.USR.CO[2,2]
    WS.DEPT.ACCT     = R.NEW(MUA.DEP.ACCT.CODE)

    WS.TLR.ID        = R.NEW(MUA.TELLER.ID)

    WS.START         = R.NEW(MUA.START.DATE.PROFILE)
    WS.END           = R.NEW(MUA.END.DATE.PROFILE)

    WS.OVER.CLASS    = R.NEW(MUA.OVER.CLASS)
*Line [ 114 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CNT       = DCOUNT(WS.OVER.CLASS,@VM)

    WS.HMM.ID        = R.NEW(MUA.HMM.ID)
*Line [ 118 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.HMM.CNT       = DCOUNT(WS.HMM.ID,@VM)

    WS.FUN           = R.NEW(MUA.FUNCTION)
*Line [ 122 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.FUN.CNT       = DCOUNT(WS.FUN,@VM)

    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :WS.USR.CO

    WS.OFS.ID = "T":TNO:".TLR-":WS.USR.CO[2]:"-":ID.NEW
RETURN
*----------------------------------------------------
BUILD.RECORD:

    IF  WS.TLR.ID # WS.TLR.ID.OLD  THEN

        OPENSEQ "../bnk.data/OFS/OFS.IN" , WS.OFS.ID TO BB THEN
            CLOSESEQ BB
            HUSH ON
            EXECUTE 'DELETE ':"../bnk.data/OFS/OFS.IN":' ':WS.OFS.ID
            HUSH OFF
        END
        OPENSEQ "../bnk.data/OFS/OFS.IN" , WS.OFS.ID TO BB ELSE
            CREATE BB THEN
            END ELSE
            END
        END
        IF WS.TLR.ID.OLD NE '' THEN
            GOSUB CLOSE.TLR
        END
        IF WS.TLR.ID NE '' THEN
            GOSUB OPEN.TLR
        END
    END
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E

RETURN
************************************************************
GET.TLR.ID.OLD:
    CALL F.READ(FN.TUSR,WS.USR.ID,R.TUSR,F.TUSR,ER.TUSR)
    LOOP
        REMOVE WS.TELLER.ID FROM R.TUSR SETTING POS.TUSR
    WHILE WS.TELLER.ID:POS.TUSR

        CALL F.READ(FN.TT.ID,WS.TELLER.ID,R.TT.ID,F.TT.ID,ER.TT.ID)

        WS.STATUS = R.TT.ID<TT.TID.STATUS>
        IF WS.STATUS = 'OPEN' THEN
            WS.TLR.ID.OLD = WS.TELLER.ID
            RETURN
        END
    REPEAT

RETURN
**-------------------------------------------------------------------
CLOSE.TLR:

    WS.TLR.OFS.ID = WS.TLR.ID.OLD
    WS.COUNT      = 0

    WS.COUNT++
    OFS.MESSAGE.DATA   =  "STATUS:":WS.COUNT:":1=":"CLOSE":COMMA
    GOSUB RUN.TLR.OFS
    WS.TLR.OFS.ID = ''
RETURN
**-------------------------------------------------------------------
OPEN.TLR:
    WS.TLR.OFS.ID = WS.TLR.ID
    WS.COUNT      = 0

    WS.COUNT++
    OFS.MESSAGE.DATA   =  "STATUS:":WS.COUNT:":1=":"OPEN":COMMA
    OFS.MESSAGE.DATA  :=  "USER:":WS.COUNT:":1=":WS.USR.ID:COMMA
    GOSUB RUN.TLR.OFS
    WS.TLR.OFS.ID = ''
RETURN
**-------------------------------------------------------------------
RUN.TLR.OFS:
    F.PATH  = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:WS.TLR.OFS.ID:COMMA:OFS.MESSAGE.DATA

    WRITESEQ OFS.REC TO BB ELSE
        TEXT = " ERROR WRITE FILE " ; CALL REM
    END

*    OFS.ID   = "T":TNO:".TLR-":ID.NEW:'-':WS.TLR.OFS.ID:'-':WS.USR.ID
*    OFS.ID   = "TLR-":ID.NEW:'-':WS.TLR.OFS.ID:'-':WS.USR.ID

*    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
*    WRITE OFS.REC ON F.OFS.IN, WS.OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
*    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
*    WRITE OFS.REC ON F.OFS.BK, WS.OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")

***** SCB R15 UPG 20160703 - S
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

RETURN
************************************************************
END
