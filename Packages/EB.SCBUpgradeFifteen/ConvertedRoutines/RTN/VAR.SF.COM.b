* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SF.COM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*----------------------------------------------------
    DEBIT.ACCT = '' ; DEBIT.CUST.NO  = '' ; REF=''
    CREDIT.ACCT= '' ; CREDIT.ACCT.NO = '' ; R.DOC.PRO=''
    CHARGE     = '' ; COMMISSION     = '' ; NO.CHARGE = 0 ; NO.COMM = 0

    FN.CUR = 'FBNK.CURRENCY'          ; F.CUR = ''; R.CUR = '' ; E11=''
    CALL OPF( FN.CUR,F.CUR)
    FN.CH = 'FBNK.FT.CHARGE.TYPE'     ; F.CH  = ''; R.CH  = '' ; ERR2 =''
    CALL OPF(FN.CH,F.CH)
    FN.CO = 'FBNK.FT.COMMISSION.TYPE' ; F.CO  = ''; R.CO  = '' ; ERR3 =''
    CALL OPF(FN.CO,F.CO)

    DEBIT.ACCT   = R.NEW(SCB.SF.TR.ACCT.CHRG.COMM)
    TEXT = DEBIT.ACCT ;  CALL REM

    IF DEBIT.ACCT NE '' THEN
*CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.OFFICER,DEBIT.ACCT,ACC.OFFICER)
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,DEBIT.ACCT,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        ACC.OFFICER  = AC.COMP[8,2]

        DEBIT.CATEG  = DEBIT.ACCT[11,4]
*------ CHRG 1  --- EGP ---
        COM.AMT = R.NEW(SCB.SF.TR.COMMISSION.AMT)
        CALL EB.ROUND.AMOUNT ('EGP',COM.AMT,'',"2")
        COM.TYPE     = R.NEW(SCB.SF.TR.COMMISSION.TYPE)
        CALL F.READ( FN.CO,COM.TYPE, R.CO, F.CO, E1)
        CREDIT.ACCT  = R.CO<FT4.CATEGORY.ACCOUNT>[1,14]:"99"
*----- CHRG 2   --- PL ---
        COM.AMT2      = R.NEW(SCB.SF.TR.COMMISSION.AMT2)
        CALL EB.ROUND.AMOUNT ('EGP',COM.AMT2,'',"2")
        COM.TYPE2     = R.NEW(SCB.SF.TR.COMMISSION.TYPE2)
        CALL F.READ( FN.CO,COM.TYPE2, R.CO, F.CO, E1)
        CREDIT.ACCT2  = R.CO<FT4.CATEGORY.ACCOUNT>
*-------------------------
        CREDIT.CATEG = DEBIT.CATEG
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY ,DEBIT.ACCT,CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
        AMOUNT.FCY.D  = ''
        AMOUNT.FCY.C  = ''
        AMOUNT.FCY.C2 = ''
   *     DR.AMOUNT     = COM.AMT + COM.AMT2
         DR.AMOUNT     = COM.AMT2
******* GET RATE ***********
        IF CURR NE 'EGP' THEN
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

      *      AMOUNT.FCY.D  = ( COM.AMT + COM.AMT2 ) * RATE
             AMOUNT.FCY.D  = ( COM.AMT2) * RATE
            CALL EB.ROUND.AMOUNT ('EGP',AMOUNT.FCY.D,'',"2")

     *       AMOUNT.FCY.C = COM.AMT * RATE
     *       CALL EB.ROUND.AMOUNT ('EGP',AMOUNT.FCY.C,'',"2")

           AMOUNT.FCY.C2 = COM.AMT2 * RATE
            CALL EB.ROUND.AMOUNT ('EGP',AMOUNT.FCY.C2,'',"2")
        END
********************CHECK IF IT IS INPUT OR REVERSE FUNCTION*********
        IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='INAU' THEN
            TRANS.NEW = '781'
            GOSUB AC.STMT.ENTRY.D

            TRANS.NEW = '783'
       *     GOSUB AC.STMT.ENTRY.C

*--- EDIT 2011/04/12
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,DEBIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------

            TRANS.NEW = '784'
            GOSUB AC.STMT.ENTRY.C2
        END
        IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='RNAU' THEN
            TRANS.NEW = '783'
            GOSUB AC.STMT.ENTRY.R.D

*--- EDIT 2011/04/12
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,DEBIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------

            TRANS.NEW = '784'
            GOSUB AC.STMT.ENTRY.R.D2

            TRANS.NEW = '781'
      *      GOSUB AC.STMT.ENTRY.R.C
        END
        GOSUB PRINT.DEAL.SLIP
    END
    RETURN
**********************************************************************
PRINT.DEAL.SLIP:
*---------------
    CALL REPORT.SF.DEBIT.PL
    CALL REPORT.SF.CREDIT.PL1
    CALL REPORT.SF.CREDIT.PL2
    RETURN
**********************************************************************
AC.STMT.ENTRY.D:
*---------------
    ENTRY         = ""
    MULTI.ENTRIES = ""

  *  COM.AMT.D     = ( COM.AMT + COM.AMT2 ) * -1
     COM.AMT.D     = ( COM.AMT2 ) * -1
    AMOUNT.FCY.D  = AMOUNT.FCY.D * -1
    IF AMOUNT.FCY.D EQ 0 THEN
        AMOUNT.FCY.D = ''
    END

*--- EDIT 2011/04/12
*Line [ 180 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,DEBIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = DEBIT.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRANS.NEW
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = DEBIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY.D NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.D
    END
    IF AMOUNT.FCY.D EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

AC.STMT.ENTRY.C:

    ENTRY         = ""
    MULTI.ENTRIES = ""
*--- EDIT 2011/04/12
*Line [ 236 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,CREDIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CREDIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = CREDIT.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRANS.NEW
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CREDIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY.C EQ 0 THEN
        AMOUNT.FCY.C = ''
    END

    IF AMOUNT.FCY.C NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.C
    END
    IF AMOUNT.FCY.C EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
************************************************************
AC.STMT.ENTRY.C2:

    ENTRY         = ""
    MULTI.ENTRIES = ""

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ''
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRANS.NEW
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = CREDIT.ACCT2
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CREDIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY.C2 EQ 0 THEN
        AMOUNT.FCY.C2 = ''
    END

    IF AMOUNT.FCY.C2 NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT2
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.C2
    END
    IF AMOUNT.FCY.C2 EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT2
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
************************************************************
AC.STMT.ENTRY.R.D:

    ENTRY         = ""
    MULTI.ENTRIES = ""

    COM.AMT       = COM.AMT * -1
    AMOUNT.FCY.C  = AMOUNT.FCY.C
    IF AMOUNT.FCY.C EQ 0 THEN
        AMOUNT.FCY.C = ''
    END
*--- EDIT 2011/04/12
*Line [ 350 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,CREDIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CREDIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = CREDIT.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRANS.NEW
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CREDIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY.C NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.C
    END
    IF AMOUNT.FCY.C EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
************************************************************
AC.STMT.ENTRY.R.D2:
    ENTRY         = ""
    MULTI.ENTRIES = ""

    COM.AMT2      = COM.AMT2      * -1
    AMOUNT.FCY.C2 = AMOUNT.FCY.C2 * -1
    IF AMOUNT.FCY.C2 EQ 0 THEN
        AMOUNT.FCY.C2 = ''
    END

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ''
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRANS.NEW
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = CREDIT.ACCT2
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CREDIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY.C2 NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT2
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.C2
    END
    IF AMOUNT.FCY.C2 EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT2
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
************************************************************

AC.STMT.ENTRY.R.C:

    ENTRY         = ""
    MULTI.ENTRIES = ""
*--- EDIT 2011/04/12
*Line [ 456 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,DEBIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = DEBIT.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRANS.NEW
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = DEBIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY.D EQ 0 THEN
        AMOUNT.FCY.D = ''
    END

    IF AMOUNT.FCY.D NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = DR.AMOUNT
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.D
    END
    IF AMOUNT.FCY.D EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = DR.AMOUNT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

END
