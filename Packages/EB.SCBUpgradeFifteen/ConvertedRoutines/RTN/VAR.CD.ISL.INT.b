* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*------------------------MAI OCT 2018-----------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CD.ISL.INT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    CD.QUAN        = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
    CD.RECOVERED   = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.NO>
    IF CD.RECOVERED LE CD.QUAN THEN
        REMAINING.CD = CD.QUAN  - CD.RECOVERED
    END
*IF  REMAINING.CD EQ '0' THEN
TEXT = 'CD.QUAN=':CD.QUAN ; CALL REM;
    IF CD.QUAN EQ '0' THEN
        CURR   = R.NEW(LD.CURRENCY)
        AMT    = R.NEW(LD.TOT.INTEREST.AMT)

*IF CURR NE 'EGP' THEN
*    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
*    CALL OPF(FN.CUR,F.CUR)
*    CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
*    RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
*END
**----------------------------------------------------------------**
****                        DEB.INT                             ****
**----------------------------------------------------------------**
*--------
*   CR
*--------

* IF CURR NE 'EGP' THEN
*     LCY.AMT = AMT * RATE
*     CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
*     FCY.AMT = AMT
* END ELSE
        LCY.AMT = AMT
        FCY.AMT = ''
*END

        PL.CAT = '50010'
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,R.NEW(LD.INT.LIQ.ACCT),CATG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(LD.INT.LIQ.ACCT),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATG=R.ITSS.ACCOUNT<AC.CATEGORY>

        CATEG  = CATG
*Line [ 83 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,R.NEW(LD.INT.LIQ.ACCT),ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(LD.INT.LIQ.ACCT),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
        FLG = 'CR'
        GOSUB AC.STMT.ENTRY
*-------
*  DR
*-------
        Y.ACCT = R.NEW(LD.INT.LIQ.ACCT)
*IF CURR NE 'EGP' THEN
*    LCY.AMT = (AMT * RATE ) * -1
*    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
*    FCY.AMT = AMT * -1
*END ELSE
        LCY.AMT = AMT * -1
        FCY.AMT = ''
*END
        PL.CAT = ''

*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,R.NEW(LD.INT.LIQ.ACCT),CATG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(LD.INT.LIQ.ACCT),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATG=R.ITSS.ACCOUNT<AC.CATEGORY>
        CATEG  = CATG
        FLG = 'DR'
        GOSUB AC.STMT.ENTRY
    END
    RETURN
**********************************************************
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
*
    TEXT = 'FLAG=':FLG;CALL REM;
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    IF FLG EQ 'CR' THEN
        ENTRY<AC.STE.TRANSACTION.CODE> = '879'
    END
    IF FLG EQ 'DR' THEN
        ENTRY<AC.STE.TRANSACTION.CODE> = '878'
    END
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.CAT
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "LD"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("LD",TYPE,MULTI.ENTRIES,"")
* END
    RETURN
END
