* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2011/06/22 ***
*******************************************
    SUBROUTINE VAR.R.GROUP.IN.CU
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*****WRITTEN BY MOHAMED SABRY 21/6/2011 SCB*****
***************************************
    F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = '' ; E1 = '' ; RETRY2 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

*Line [ 42 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT.NEW = DCOUNT(R.NEW(CG.CUSTOMER.ID),@VM)
*Line [ 44 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.COUNT.OLD = DCOUNT(R.OLD(CG.CUSTOMER.ID),@VM)

    FOR I.OLD = 1 TO WS.COUNT.OLD
        WS.CL.REF.OLD = R.OLD(CG.CUSTOMER.ID)<1,I.OLD>
        LOCATE WS.CL.REF.OLD IN R.NEW(CG.CUSTOMER.ID)<1,1> SETTING YCUS.POS THEN
*Line [ 50 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            NULL
        END ELSE
            CALL F.READ(FN.CUSTOMER,WS.CL.REF.OLD,R.CUSTOMER,F.CUSTOMER,E1)
*** UPDATED BY MOHAMED SABRY 2014/10/2
            *DEL R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.GROUP.NUM>
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.GROUP.NUM> = ''
*** END OF UPDATE
            TEXT = WS.CL.REF.OLD :" WILL REMOVE FROM GROUP ":ID.NEW ; CALL REM
            CALL F.WRITE(FN.CUSTOMER,WS.CL.REF.OLD,R.CUSTOMER)
        END
    NEXT I.OLD

    FOR I = 1 TO WS.COUNT.NEW
        WS.CL.REF = R.NEW(CG.CUSTOMER.ID)<1,I>
        CALL F.READ(FN.CUSTOMER,WS.CL.REF,R.CUSTOMER,F.CUSTOMER,E1)
        IF R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.GROUP.NUM> # ID.NEW THEN
            R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.GROUP.NUM> =  ID.NEW
            CALL F.WRITE(FN.CUSTOMER,WS.CL.REF,R.CUSTOMER)
            CALL REBUILD.SCREEN
        END
    NEXT I

    RETURN
END
