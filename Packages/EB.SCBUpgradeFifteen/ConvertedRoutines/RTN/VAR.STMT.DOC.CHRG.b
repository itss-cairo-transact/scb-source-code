* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1398</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.STMT.DOC.CHRG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY



    EXECUTE 'COMO ON VAR.STMT.DOC.CHRG'
    DEBIT.ACCT = '' ; DEBIT.CUST.NO  = '' ; REF=''
    CREDIT.ACCT= '' ; CREDIT.ACCT.NO = '' ; R.DOC.PRO=''
    CHARGE     = '' ; COMMISSION     = '' ; NO.CHARGE = 0 ; NO.COMM = 0
    CUS.ID     = ''

    FN.CUR = 'FBNK.CURRENCY'          ; F.CUR = ''; R.CUR = '' ; E11=''
    CALL OPF( FN.CUR,F.CUR)
    FN.CH = 'FBNK.FT.CHARGE.TYPE'     ; F.CH  = ''; R.CH  = '' ; ERR2 =''
    CALL OPF(FN.CH,F.CH)
    FN.CO = 'FBNK.FT.COMMISSION.TYPE' ; F.CO  = ''; R.CO  = '' ; ERR3 =''
    CALL OPF(FN.CO,F.CO)


    DEBIT.ACCT   = R.NEW(DOC.PRO.DEBIT.ACCT)
    DEBIT.ACCT.2 = R.NEW(DOC.PRO.COMM.ACCT)
    CUS.ID       = R.NEW(DOC.PRO.CUSTOMER.ID)
    CRT 'CUS.ID : ':CUS.ID
    DEBIT.CURR   = DEBIT.ACCT[9,2]
    DEBIT.CURR.2 = DEBIT.ACCT.2[9,2]
    DEBIT.CATEG  = DEBIT.ACCT[11,4]
    DEBIT.CATEG.2= DEBIT.ACCT.2[11,4]

*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,DEBIT.ACCT,DEBIT.CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DEBIT.CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    *CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.OFFICER,DEBIT.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,DEBIT.ACCT,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
    ACC.OFFICER = AC.COMP[8,2]
    ACC.OFFICER = ACC.OFFICER + 0

    CHARGE      = R.NEW(DOC.PRO.CHARGE.TYPE)
    COMMISSION  = R.NEW(DOC.PRO.COMMISSION.TYPE)
*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.CHARGE   = DCOUNT(CHARGE,@VM)
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.COMM     = DCOUNT(COMMISSION,@VM)
    ALL.AMOUNT  = R.NEW(DOC.PRO.CHARGE.AMOUNT)

    IF DEBIT.ACCT NE '' THEN
        FOR I =1 TO NO.CHARGE
            AMOUNT = 0
            AMOUNT = ALL.AMOUNT<1,I>
            IF AMOUNT NE '' AND AMOUNT NE 0  THEN
                ACCT    = DEBIT.ACCT
                AMOUNT.FCY = 0
                CATEG   = DEBIT.CATEG
                CHARGE.TYPE = CHARGE<1,I>
                TEXT = CHARGE.TYPE:"/":AMOUNT; CALL REM
                CALL F.READ(FN.CH,CHARGE.TYPE,R.CH,F.CH,ERR2)
                CREDIT.CH.ACCT = R.CH<FT5.CATEGORY.ACCOUNT>
                ACCT.LEN       = LEN(CREDIT.CH.ACCT)
                IF ACCT.LEN > 10 THEN
                    ACCT.LEN       = ACCT.LEN - 2
                    CREDIT.CH.ACCT = CREDIT.CH.ACCT[1,ACCT.LEN]:ID.COMPANY[8,2]
                    CREDIT.ACCT.NO = CREDIT.CH.ACCT
                    PL.ACCT.NO     = ''
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CREDIT.ACCT.NO,CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CREDIT.ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                END
                ELSE
                    PL.ACCT.NO     = CREDIT.CH.ACCT
                    CR.CATEG       = DEBIT.CATEG
                END
                CALL EB.ROUND.AMOUNT ('EGP',AMOUNT,'',"2")

****************************GET CURRENCY*****************************
                T.SEL = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ " : DEBIT.CURR
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF KEY.LIST THEN
                    FIN.CURR = KEY.LIST<1>
                END ELSE
                    FIN.CURR = 'EGP'
                END
                IF FIN.CURR NE 'EGP' THEN
                    CALL F.READ(FN.CUR,FIN.CURR,R.CUR,F.CUR,ERR1)
                    RATE        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    AMOUNT.FCY  = AMOUNT * RATE
                    CALL EB.ROUND.AMOUNT ('EGP',AMOUNT.FCY,'',"2")
                END

********************CHECK IF IT IS INPUT OR REVERSE FUNCTION*********

                IF V$FUNCTION = 'A' AND  R.NEW(DOC.PRO.RECORD.STATUS)='INAU' THEN
                    GOSUB AC.STMT.ENTRY.D
                    GOSUB AC.STMT.ENTRY.C
                END
                IF V$FUNCTION = 'A' AND  R.NEW(DOC.PRO.RECORD.STATUS)='RNAU' THEN

                    GOSUB AC.STMT.ENTRY.R.D
                    GOSUB AC.STMT.ENTRY.R.C

                END
*********************************************************************
            END
        NEXT I
    END
    IF DEBIT.ACCT.2 NE '' THEN
        FOR J =1 TO NO.COMM
            ACCT    = DEBIT.ACCT.2
            AMOUNT  = 0; AMOUNT.FCY = 0
            CATEG   = DEBIT.CATEG.2
            AMOUNT  = R.NEW(DOC.PRO.COMMISSION.AMOUNT)
            IF AMOUNT NE '' AND AMOUNT NE 0 THEN
                COMM.TYPE = COMMISSION<1,J>
                CALL F.READ(FN.CO,COMM.TYPE,R.CO,F.CO,ERR3)
                CREDIT.COM.ACCT = R.CO<FT4.CATEGORY.ACCOUNT>
*Line [ 186 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT ,COMM.TYPE,CREDIT.COM.ACCT)
F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
FN.F.ITSS.FT.COMMISSION.TYPE = ''
CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,COMM.TYPE,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
CREDIT.COM.ACCT=R.ITSS.FT.COMMISSION.TYPE<FT4.CATEGORY.ACCOUNT>
                IF LEN(CREDIT.COM.ACCT) > 10 THEN
                    CREDIT.ACCT.NO = CREDIT.COM.ACCT
                    PL.ACCT.NO     = ''
*Line [ 196 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CREDIT.ACCT.NO,CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CREDIT.ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                END
                ELSE
                    CREDIT.ACCT.NO = ''
                    PL.ACCT.NO     = CREDIT.COM.ACCT
                    CR.CATEG       = DEBIT.CATEG.2
                END
                CALL EB.ROUND.AMOUNT ('EGP',AMOUNT,'',"2")

****************************GET CURRENCY*****************************
                T.SEL = "SELECT FBNK.CURRENCY WITH NUMERIC.CCY.CODE EQ " : DEBIT.CURR.2
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF KEY.LIST THEN
                    FIN.CURR = KEY.LIST<1>
                END ELSE
                    FIN.CURR = 'EGP'
                END
                IF FIN.CURR NE 'EGP' THEN
                    CALL F.READ(FN.CUR,FIN.CURR,R.CUR,F.CUR,ERR1)
                    RATE        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    AMOUNT.FCY  = AMOUNT * RATE
                    CALL EB.ROUND.AMOUNT ('EGP',AMOUNT.FCY,'',"2")
                END

********************CHECK IF IT IS INPUT OR REVERSE FUNCTION*********

                IF V$FUNCTION = 'A' AND  R.NEW(DOC.PRO.RECORD.STATUS)='INAU' THEN
                    GOSUB AC.STMT.ENTRY.D

                    GOSUB AC.STMT.ENTRY.C
                END
                IF V$FUNCTION = 'A' AND  R.NEW(DOC.PRO.RECORD.STATUS)='RNAU' THEN
                    GOSUB AC.STMT.ENTRY.R.D
                    GOSUB AC.STMT.ENTRY.R.C
                END
            END
        NEXT J
    END
    EXECUTE 'COMO OFF VAR.STMT.DOC.CHRG'
    RETURN
**********************************************************************


AC.STMT.ENTRY.D:
    ENTRY         = ""
    MULTI.ENTRIES = ""

    AMOUNT.D        = AMOUNT * -1
    AMOUNT.FCY.D    = AMOUNT.FCY * -1

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '780'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = FIN.CURR

    IF AMOUNT.FCY NE 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT.FCY.D
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.D
    END
    IF AMOUNT.FCY EQ 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    CRT '======================'
    CRT 'AC.STMT.ENTRY.D'
    CRT MULTI.ENTRIES
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

AC.STMT.ENTRY.C:

    ENTRY         = ""
    MULTI.ENTRIES = ""


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = CREDIT.ACCT.NO
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '780'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.ACCT.NO
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CR.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = FIN.CURR

    IF AMOUNT.FCY NE 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT.FCY
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT
    END
    IF AMOUNT.FCY EQ 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
*    ENTRY<AC.STE.SYSTEM.ID>        = "AC"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
*    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ID
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    CRT '======================'
    CRT 'AC.STMT.ENTRY.C'
    CRT MULTI.ENTRIES

    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************
AC.STMT.ENTRY.R.D:

    ENTRY         = ""
    MULTI.ENTRIES = ""

    AMOUNT.D        = AMOUNT * -1
    AMOUNT.FCY.D    = AMOUNT.FCY * -1

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = CREDIT.ACCT.NO
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '780'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.ACCT.NO
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CR.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = FIN.CURR

    IF AMOUNT.FCY NE 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT.FCY.D
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.D
    END
    IF AMOUNT.FCY EQ 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
*    ENTRY<AC.STE.SYSTEM.ID>        = "AC"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
*    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ID
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CRT '======================'
    CRT 'AC.STMT.ENTRY.R.D'
    CRT MULTI.ENTRIES

    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

AC.STMT.ENTRY.R.C:

    ENTRY         = ""
    MULTI.ENTRIES = ""


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '780'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = FIN.CURR

    IF AMOUNT.FCY NE 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT.FCY
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT
    END
    IF AMOUNT.FCY EQ 0 THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = AMOUNT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    CRT '======================'
    CRT 'AC.STMT.ENTRY.R.C'
    CRT MULTI.ENTRIES

    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

END
