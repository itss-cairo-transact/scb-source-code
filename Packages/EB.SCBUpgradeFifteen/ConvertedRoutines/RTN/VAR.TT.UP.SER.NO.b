* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
****06/06/2010 NESSREEN AHMED ************
*-----------------------------------------------------------------------------
* <Rating>279</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.UP.SER.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.SER.NO
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    ETEXT = ''   ; SRN = '' ; SER.STAT = '' ; SER.NO = ''
    FN.P.SER.NO = 'F.SCB.P.SER.NO' ; F.P.SER.NO = '' ; R.P.SER.NO = '' ; SR.ERR = ''
    CURR = '' ; SER.AMT = '' ; TRN.DAT = '' ; ACCT.NO = '' ; COMP.CODE = ''
    DEP.CO = ''

    IF V$FUNCTION # 'R' THEN

        SER.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.SER.NO>

        CALL OPF( FN.P.SER.NO,F.P.SER.NO)
        CALL F.READ( FN.P.SER.NO,SER.NO, R.P.SER.NO, F.P.SER.NO,SR.ERR)

        IF SR.ERR THEN
            TT.REF = ID.NEW
            CURR      = R.NEW(TT.TE.CURRENCY.1)
            SER.AMT   = R.NEW(TT.TE.NET.AMOUNT)
            TRN.DAT   = R.NEW(TT.TE.AUTH.DATE)
            ACCT.NO   = R.NEW(TT.TE.ACCOUNT.1)
            COMP.CODE = R.NEW(TT.TE.CO.CODE)
            DEP.CO    = R.NEW(TT.TE.DEPT.CODE)

            R.P.SER.NO<PSN.REF.NO>     = TT.REF
            R.P.SER.NO<PSN.CURR>       = CURR
            R.P.SER.NO<PSN.TRN.DAT>    = TRN.DAT
            R.P.SER.NO<PSN.ACCT.NO>    = ACCT.NO
            R.P.SER.NO<PSN.AMOUNT.NO>  = SER.AMT
            R.P.SER.NO<PSN.SER.NO>     = SER.NO
            R.P.SER.NO<PSN.COMPANY.CO> = COMP.CODE
            R.P.SER.NO<PSN.SER.STATUS> = "PAID"

            CALL F.WRITE(FN.P.SER.NO,SER.NO,R.P.SER.NO)
        END ELSE
*****UPDATED BY NESSREEN AHMED 13/12/2010|*******************************
            IF R.P.SER.NO<PSN.SER.STATUS> = "PAID" THEN
                E = '��� �� ��� ��� ������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
****UPDATED BY NESSREEN AHMED 29/7/2012*******************************
                IF R.P.SER.NO<PSN.SER.STATUS> = "REVERSED" THEN
                    TT.REF    = ID.NEW
                    CURR      = R.NEW(TT.TE.CURRENCY.1)
                    SER.AMT   = R.NEW(TT.TE.NET.AMOUNT)
                    TRN.DAT   = R.NEW(TT.TE.AUTH.DATE)
                    ACCT.NO   = R.NEW(TT.TE.ACCOUNT.1)
                    COMP.CODE = R.NEW(TT.TE.CO.CODE)
                    DEP.CO    = R.NEW(TT.TE.DEPT.CODE)

                    R.P.SER.NO<PSN.REF.NO>     = TT.REF
                    R.P.SER.NO<PSN.CURR>       = CURR
                    R.P.SER.NO<PSN.TRN.DAT>    = TRN.DAT
                    R.P.SER.NO<PSN.ACCT.NO>    = ACCT.NO
                    R.P.SER.NO<PSN.AMOUNT.NO>  = SER.AMT
                    R.P.SER.NO<PSN.SER.NO>     = SER.NO
                    R.P.SER.NO<PSN.COMPANY.CO> = COMP.CODE
                    R.P.SER.NO<PSN.SER.STATUS> = "PAID"

                    CALL F.WRITE(FN.P.SER.NO,SER.NO,R.P.SER.NO)
                END
            END
****END OF UPDATE 29/7/2012*************************************
*****END OF UPDATE 13/12/2010************************************************
        END ; ****END OF IF SR.ERR*****
    END ELSE
**###########IF FUNCTION = 'R'##########################
        SER.NO = R.NEW(TT.TE.LOCAL.REF)<1,TTLR.SER.NO>
        CALL OPF( FN.P.SER.NO,F.P.SER.NO)
        CALL F.READ( FN.P.SER.NO,SER.NO, R.P.SER.NO, F.P.SER.NO,SR.ERR)
        IF NOT(SR.ERR) THEN
            R.P.SER.NO<PSN.SER.STATUS> = "REVERSED"
            CALL F.WRITE(FN.P.SER.NO,SER.NO,R.P.SER.NO)
        END
    END
    RETURN
END
