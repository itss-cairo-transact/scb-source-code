* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>212</Rating>
*-----------------------------------------------------------------------------
**** RANIA *********
*TO CREATE SWIFT MESSAGE
    SUBROUTINE VAR.LG.SWIFT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.SWIFT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.AMEND.NUMBER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.769


    IF R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> NE '' THEN
*************** Updating Amend.No If Msg is 767 ************************
        IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> = '767' THEN
            FN.SCB.LG.AMEND.NUMBER='F.SCB.LG.AMEND.NUMBER';ID=ID.NEW;R.AMEND='';F.AMEND=''
            CALL F.READ( FN.SCB.LG.AMEND.NUMBER,ID, R.AMEND, F.AMEND, ETEXT)
            IF ETEXT THEN
                R.AMEND<SCB.AMEND.NO> = '1'
            END ELSE
                MYAMEND =  R.AMEND<SCB.AMEND.NO>
                MYAMEND = MYAMEND + 1
                R.AMEND<SCB.AMEND.NO>=MYAMEND
            END
            AMEND.NO= R.AMEND<SCB.AMEND.NO>
            CALL F.WRITE(FN.SCB.LG.AMEND.NUMBER,ID,R.AMEND)
*CALL JOURNAL.UPDATE(ID)
        END
********** Getting Amount.Increase & Amt.V.Date If Msg is 769 ********
*IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> = '769' THEN
* REDUCE.AMT=LINK.DATA<ABS(R.NEW(LD.AMOUNT.INCREASE))>
*TEXT=REDUCE.AMT ;CALL REM
*REDUCE.DATE=LINK.DATA<ABS(R.NEW(LD.AMT.V.DATE))>
*TEXT=REDUCE.AMT ;CALL REM
*END
        IF  R.NEW( LD.LOCAL.REF)< 1,LDLR.MSG.TO.PROD> = '769' THEN
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('SCB.LG.769':@FM:SCB.AMOUNT.INCREASE,ID.NEW,REDUCE.AMT)
F.ITSS.SCB.LG.769 = 'F.SCB.LG.769'
FN.F.ITSS.SCB.LG.769 = ''
CALL OPF(F.ITSS.SCB.LG.769,FN.F.ITSS.SCB.LG.769)
CALL F.READ(F.ITSS.SCB.LG.769,ID.NEW,R.ITSS.SCB.LG.769,FN.F.ITSS.SCB.LG.769,ERROR.SCB.LG.769)
REDUCE.AMT=R.ITSS.SCB.LG.769<SCB.AMOUNT.INCREASE>
            *TEXT = REDUCE.AMT ;CALL REM
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('SCB.LG.769':@FM:SCB.AMT.V.DATE,ID.NEW,REDUCE.DATE)
F.ITSS.SCB.LG.769 = 'F.SCB.LG.769'
FN.F.ITSS.SCB.LG.769 = ''
CALL OPF(F.ITSS.SCB.LG.769,FN.F.ITSS.SCB.LG.769)
CALL F.READ(F.ITSS.SCB.LG.769,ID.NEW,R.ITSS.SCB.LG.769,FN.F.ITSS.SCB.LG.769,ERROR.SCB.LG.769)
REDUCE.DATE=R.ITSS.SCB.LG.769<SCB.AMT.V.DATE>
            *TEXT = REDUCE.DATE ;CALL REM
        END
*************** Calling SCB.LG.PROD.SWIFT To draw Message *************
        STORE.DEL.REFS = ""
        ER.MSG = ""
        CALL SCB.LG.PROD.SWIFT(MAT R.NEW,ID.NEW,AMEND.NO,REDUCE.AMT,REDUCE.DATE,STORE.DEL.REFS,ER.MSG)
        *TEXT = STORE.DEL.REFS ;CALL REM

**************** Updating SCB.LG.Swift With Delivery Ref **************
        DAT= R.NEW(LD.AGREEMENT.DATE)
        FN.SCB.LG.SWIFT='F.SCB.LG.SWIFT';ID=ID.NEW:".":DAT;R.SWIFT='';F.SWIFT=''
        CALL F.READ( FN.SCB.LG.SWIFT,ID, R.SWIFT, F.SWIFT, ETEXT)
        IF ETEXT THEN
            R.SWIFT<SCB.DELIVERY.REF> = STORE.DEL.REFS
        END ELSE
            MYDEV = R.SWIFT<SCB.DELIVERY.REF>
            MYCOUNT = DCOUNT(MYDEV,@VM)
            MYCOUNT = MYCOUNT +1
            R.SWIFT<SCB.DELIVERY.REF,MYCOUNT> = STORE.DEL.REFS
        END

************************************************************************
        CALL F.WRITE(FN.SCB.LG.SWIFT,ID,R.SWIFT)
*CALL JOURNAL.UPDATE(ID)

************************************************************************
    END
    RETURN
END
