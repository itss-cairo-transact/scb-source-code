* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.OFS.HR.1407

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*--------------------------------------------------------------*
    COMP = ID.COMPANY

***CHECK ID
    FN.AC = 'FBNK.ACCOUNT$NAU' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,ID.NEW,R.AC,F.AC,E2)
    CURR.NO = R.AC<AC.CURR.NO>
    IF CURR.NO EQ 1 THEN
        ACC.ID   = ID.NEW
        ACC.CATEG = ACC.ID[11,4]
        IF ACC.CATEG EQ 1407 THEN
            GOSUB INITIALISE
            GOSUB BUILD.RECORD
        END ELSE
            TEXT = "ERROR IN CATEGORY, MUST BE 1407 " ; CALL REM
        END
    END
    RETURN
***********************************************
INITIALISE:
    OFS.MESSAGE.DATA = ""
    MSG.DATA = ""
    COMMA = ","
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.AC = 'FBNK.ACCOUNT$NAU' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)
    OPENSEQ "../bnk.data/OFS/OFS.IN" , "ACCOUNT.":ID.NEW TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"../bnk.data/OFS/OFS.IN":' ':"ACCOUNT.":ID.NEW
        HUSH OFF
    END
    OPENSEQ "../bnk.data/OFS/OFS.IN" , "ACCOUNT.":ID.NEW TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ACCOUNT CREATED IN ../bnk.data/OFS/OFS.IN'
        END ELSE
            STOP 'Cannot create ACCOUNT File IN ../bnk.data/OFS/OFS.IN'
        END
    END
    RETURN
***********************************************
BUILD.RECORD:
    CALL F.READ(FN.AC,ID.NEW,R.AC,F.AC,E1)
    AC.ID = ID.NEW

    CATEG.ORG = R.AC<AC.CATEGORY>

    CUR         = R.AC<AC.CURRENCY>
    OPN.DATE    = R.AC<AC.OPENING.DATE>
    *ACT.OFFICER = R.AC<AC.ACCOUNT.OFFICER>
    ACT.OFF = R.AC<AC.CO.CODE>
    ACT.OFFICER = ACT.OFF[8,2]
    TITLE       = R.AC<AC.ACCOUNT.TITLE.1>
    SHRT.TITLE  = R.AC<AC.SHORT.TITLE>

    IDD = 'ACCOUNT,SCB2,':R.USER<EB.USE.SIGN.ON.NAME>:'//':COMP:',':AC.ID
*------------------------------------------------------------------------------
    OFS.MESSAGE.DATA   ="CATEGORY=":CATEG.ORG:COMMA
    OFS.MESSAGE.DATA  :="CURRENCY=":CUR:COMMA
    OFS.MESSAGE.DATA  :="OPENING.DATE=":OPN.DATE:COMMA
    OFS.MESSAGE.DATA  :="ACCOUNT.OFFICER=":ACT.OFFICER:COMMA
    OFS.MESSAGE.DATA  :="ACCOUNT.TITLE.1=":TITLE:COMMA
    OFS.MESSAGE.DATA  :="SHORT.TITLE=":SHRT.TITLE

    MSG.DATA = IDD:",":OFS.MESSAGE.DATA
*------------------------------------------------------------------------------
    WRITESEQ MSG.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------------------------------------------------------
    RETURN
END
