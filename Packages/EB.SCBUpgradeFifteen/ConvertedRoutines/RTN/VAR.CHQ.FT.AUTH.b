* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CHQ.FT.AUTH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*----------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN
*----------------------------------------
INITIALISE:

    FN.CA = 'F.SCB.CHEQ.APPL' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    FN.FT= 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    FT.REF = R.NEW(FT.DEBIT.THEIR.REF)
    CALL F.READ(FN.CA,FT.REF,R.CA,F.CA,E2)

    IF V$FUNCTION = 'A' THEN
        R.CA<CHQA.CHARGE.DATE> = TODAY
        CALL F.WRITE(FN.CA,FT.REF,R.CA)
    END

    IF V$FUNCTION = 'D' THEN
        R.CA<CHQA.CHARGE.DATE> = ''
        CALL F.WRITE(FN.CA,FT.REF,R.CA)
    END

    RETURN
************************************************************
END
