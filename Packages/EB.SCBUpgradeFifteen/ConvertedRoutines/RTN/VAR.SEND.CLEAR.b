* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SEND.CLEAR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.STATUS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.BR  = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF( FN.BR,F.BR)

    FN.BS   = 'FBNK.SCB.BR.SLIPS' ; F.BS = '' ; R.BS = ''
    CALL OPF( FN.BS,F.BS)

    DCOUNTBR.ID = ""
    NUMBERBR.ID = ""

*Line [ 61 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BLB.CHQ.REG.ID),@VM)
    FOR  NUMBERBR.ID = 1 TO DCOUNTBR.ID

        BR.ID  = R.NEW(SCB.BLB.CHQ.REG.ID)<1,NUMBERBR.ID>
        CALL F.READ( FN.BR,BR.ID, R.BR, F.BR, ETEXT)

        R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>  = R.NEW(SCB.BLB.USR.STATUS)
        R.BR<EB.BILL.REG.LOCAL.REF,BRLR.STATUS.DATE>   = TODAY

*WRITE  R.BR TO F.BR , BR.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BR.ID:"TO" :FN.BR
*END
        CALL F.WRITE (FN.BR, BR.ID,R.BR)

        BS.ID = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.SLIP.REFER>
        CALL F.READ( FN.BS,BS.ID, R.BS, F.BS, ETEXT)
        R.BS<SCB.BS.TOTAL.AMT> -= R.BR<EB.BILL.REG.AMOUNT>
        R.BS<SCB.BS.TOTAL.NO>  -= 1

*WRITE  R.BS TO F.BS , BS.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":BS.ID:"TO" :FN.BS
*END
        CALL F.WRITE (FN.BS, BS.ID,R.BS)
    NEXT NUMBERBR.ID
    RETURN
END
