* @ValidationCode : MjotMTgxNjI2MTkxMTpDcDEyNTI6MTY0MTk0MzcxMzc5MTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 12 Jan 2022 01:28:33
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* Create By Nessma Mohammad
*-----------------------------------------------------------------------------
SUBROUTINE VAR.WRT.TEMP

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_F.ACCOUNT
    $INSERT  I_F.FUNDS.TRANSFER
    $INSERT           I_F.SCB.AC.INVEST.EGP
    $INSERT           I_FT.LOCAL.REFS
*-----------------------------------------------
    FN.TMP = "F.SCB.AC.INVEST.EGP" ;  F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    TMP.ID = R.NEW(FT.CREDIT.THEIR.REF)
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ERR.TMP)
    R.TMP<AC.INV.LAST.RUN.DATE> = TODAY

*WRITE R.TMP TO F.TMP , TMP.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':TMP.ID:' TO FILE ':FN.TMP
*END
    CALL F.WRITE (FN.TMP, TMP.ID,R.TMP)
*----------------------------------------------
RETURN
END
