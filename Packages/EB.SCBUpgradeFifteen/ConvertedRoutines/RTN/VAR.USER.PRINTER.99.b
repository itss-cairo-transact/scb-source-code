* @ValidationCode : MjotMTIzMDYyMTEwMjpDcDEyNTI6MTY0MTk0MzU1ODc2ODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 12 Jan 2022 01:25:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
SUBROUTINE VAR.USER.PRINTER.99

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_USER.ENV.COMMON
    $INSERT  I_F.USER
    $INSERT  I_F.USER.SIGN.ON.NAME
*------------------------------------------------
    FN.SGN = "F.USER.SIGN.ON.NAME" ; F.SGN = ""
    CALL OPF(FN.SGN, F.SGN)

    FN.USR = "F.USER"              ; F.USR = ""
    CALL OPF(FN.USR, F.USR)
*------------------------------------------------
    IF V$FUNCTION = 'A' THEN
        TEXT = R.USER<EB.USE.PRINTER.FOR.RPTS> ; CALL REM
        TEXT = R.USER<EB.USE.SIGN.ON.NAME>     ; CALL REM

        USR.SGN.ID  = R.USER<EB.USE.SIGN.ON.NAME>
        CALL F.READ(FN.SGN,USR.SGN.ID,R.SGN,F.SGN,ER.SGN)
        USR.FILE.ID = R.SGN<EB.USO.USER.ID>

        CALL F.READ(FN.USR,USR.FILE.ID,R.USERR,F.USR,ER.USR)
        IF R.USERR<EB.USE.PRINTER.FOR.RPTS> EQ '' THEN
            E = '�� ���� ������� ����� ����� �������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*------------------------------------------------
RETURN
END
