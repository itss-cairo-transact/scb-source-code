* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>70</Rating>
*-----------------------------------------------------------------------------
****** NESSREEN AHMED 13/9/2006 ****

    SUBROUTINE VAR.TT.COMM.OFS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    GOSUB CREATE.FILE
*******************
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*******************
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>


    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS = "SCB"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:
***********************************
    TRANS.TYPE = 'ACTT'
    DB.CUR = R.NEW(TT.TE.CURRENCY.1)
    DB.AMT.COM = R.NEW(TT.TE.NARRATIVE.1)

    IF DB.CUR = LCCY THEN
        AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    END ELSE
        AMT = R.NEW(TT.TE.AMOUNT.FCY.1)
    END

    DB.AMT = SUBS(AMT, DB.AMT.COM)
    PROFIT.DEPT = R.NEW(TT.TE.DEPT.CODE)
    DB.DATE = R.NEW(TT.TE.VALUE.DATE.1)
    CR.DATE = R.NEW(TT.TE.VALUE.DATE.1)

    DB.ACCT = R.NEW(TT.TE.ACCOUNT.2)
    CR.ACCT = 'PL52114'
    ORDER.BANK = 'SUEZ CANAL'
**********************************

    COMMA = ","
    OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TRANS.TYPE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": DB.CUR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": DB.AMT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DB.DATE:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":CR.DATE:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":PROFIT.DEPT:COMMA
    OFS.MESSAGE.DATA := "ORDERING.BANK=":ORDER.BANK

    RETURN

*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(TT.TE.DATE.TIME)

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,"T":TNO:".":'COMM':"_TT":RND(10000):"_":D.T ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

    RETURN
*--------------------------------------------------------
END
