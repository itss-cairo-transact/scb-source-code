* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.LOAN.AC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.AC
*-------------------------------------------
    COMP = ID.COMPANY
    ACCT = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT,CATT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATT=R.ITSS.ACCOUNT<AC.CATEGORY>

    LC.ID = ID.NEW[1,12]
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('LETTER.OF.CREDIT':@FM:TF.LC.OLD.LC.NUMBER,LC.ID,OLD.REF)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,LC.ID,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
OLD.REF=R.ITSS.LETTER.OF.CREDIT<TF.LC.OLD.LC.NUMBER>

    IF ( CATT EQ 1230 OR CATT EQ 1595 OR CATT EQ 1596 OR CATT EQ 1598 OR CATT EQ 1585 OR CATT EQ 1587 OR CATT EQ 1512 OR CATT EQ 1579 OR CATT EQ 1508 OR CATT EQ 1527 OR CATT EQ 1528 OR CATT EQ 1567 OR CATT EQ 1404 OR CATT EQ 1438 OR CATT EQ 1589) THEN
        FN.ACC = 'F.SCB.LOAN.AC' ;F.ACC = '' ; R.ACC = ''
        CALL OPF(FN.ACC,F.ACC)
        KEY.LIST="" ; SELECTED="" ; ER.MSG=""

        R.ACC<AC.LO.ACCOUNT.NO>      = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
        R.ACC<AC.LO.OLD.REF>         = OLD.REF
        R.ACC<AC.LO.AMOUNT>          = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.MARG.AMOUNT>
        R.ACC<AC.LO.OUTSTAND.AMOUNT> = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.MARG.AMOUNT>
        NOO                          = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NO.DAY>

        IF R.NEW(TF.DR.DRAWING.TYPE) = 'DP' OR R.NEW(TF.DR.DRAWING.TYPE) = 'AC' THEN
            DAYY = R.NEW(TF.DR.MATURITY.REVIEW)
        END
        IF R.NEW(TF.DR.MATURITY.REVIEW) EQ '' THEN
            DAYY= R.NEW(TF.DR.VALUE.DATE)
        END ELSE
            DAYY = R.NEW(TF.DR.MATURITY.REVIEW)
        END

        R.ACC<AC.LO.ISSUE.DATE> = DAYY
        R.ACC<AC.LO.MAT.DATE>   = NOO
        R.ACC<AC.LO.STATUS>     = 'O'

*Line [ 85 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT,CURRR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURRR=R.ITSS.ACCOUNT<AC.CURRENCY>
        R.ACC<AC.LO.CURRENCY> = CURRR

        ACCT = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,ACCT,COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        R.ACC<AC.LO.CO.CODE.1>   = COMP
        R.ACC<AC.LO.DATE.TIME.1> = R.NEW(TF.DR.DATE.TIME)<1,1>
        R.ACC<AC.LO.INPUTTER>    = R.NEW(TF.DR.INPUTTER)<1,1>
        R.ACC<AC.LO.AUTHORISER>  = R.NEW(TF.DR.AUTHORISER)
        ID.NO = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)[1,8] :"-": ID.NEW

        R.ACC<AC.LO.OUR.REF>   = ID.NEW
        R.ACC<AC.LO.CO.CODE.1> =  COMP

        IF NOO THEN
*WRITE  R.ACC TO F.ACC , ID.NO  ON ERROR
*   PRINT "CAN NOT WRITE RECORD":ID.NO:"TO" :FN.ACC
*END
            CALL F.WRITE (FN.ACC, ID.NO, R.ACC)
        END
        IF V$FUNCTION = 'A' AND  R.NEW(TF.DR.RECORD.STATUS)='RNAU' THEN
            ID.NO = R.NEW(TF.DR.DRAWDOWN.ACCOUNT)[1,8] :"-": ID.NEW
*******************************UPDATED BY RIHAM R15**********************
*   DELETE F.ACC , ID.NO
            CALL F.DELETE (FN.ACC,ID.NO)

        END
    END
*-----------------------------------------------------------------
    RETURN
END
