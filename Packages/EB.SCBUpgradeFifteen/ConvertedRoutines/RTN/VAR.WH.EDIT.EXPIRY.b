* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.WH.EDIT.EXPIRY

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.EXPIRY
*-------------------------------------------------
    FN.WH.ITEMS = 'F.SCB.WH.ITEMS'    ; F.WH.ITEMS = ''
    CALL OPF( FN.WH.ITEMS,F.WH.ITEMS)

    FN.EXP = 'F.SCB.WH.EXPIRY'        ; F.EXP      = ''
    CALL OPF(FN.EXP,F.EXP)

    FN.TRNS = "F.SCB.WH.TRANS"        ; F.TRNS = ""
    CALL OPF(FN.TRNS, F.TRNS)
*******************************UPDATED BY RIHAM R15**********************
*    OPEN FN.EXP TO FVAR.EXP ELSE

*       TEXT = "ERROR OPEN FILE" ; CALL REM
*        RETURN
*    END
*-------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='INA2' THEN
*Line [ 53 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DECOUNT.ITEM = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)
        FOR I = 1 TO DECOUNT.ITEM
            R.WH   = ''
            WH.ID  = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I> :'-': R.NEW(SCB.WH.TRANS.VALUE.DATE)<1,I>
            CALL F.READ(FN.EXP, WH.ID , R.WH , F.EXP, ETEXT)

            TRNS.ID = R.WH<EXP.REF.WH.ID>
            TEXT = "TRNS.ID= " : TRNS.ID ; CALL REM

            CALL F.READ(FN.TRNS, TRNS.ID , R.TRNS , F.TRNS, E.TRNS)
*Line [ 64 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NN.ITEMS = DCOUNT(R.TRNS<SCB.WH.TRANS.WH.ACCT.NO.ITEM>,@VM)

            FOR NN = 1 TO NN.ITEMS
                IF R.TRNS<SCB.WH.TRANS.WH.ACCT.NO.ITEM><1,NN> EQ R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I> THEN
                    NO.UNTS = R.TRNS<SCB.WH.TRANS.NO.OF.UNITS><1,NN>
                END
            NEXT NN
            TEXT = "UNITS= ": NO.UNTS  ; CALL REM

            NEW.AMT = NO.UNTS * R.NEW(SCB.WH.TRANS.UNIT.PRICE)<1,I>
            R.WH<EXP.VALUE.BALANCE> = NEW.AMT

*WRITE R.WH TO F.EXP , WH.ID  ON ERROR
*   STOP 'CAN NOT WRITE RECORD ':WH.ID:' TO FILE ':FN.EXP
*END
            CALL F.WRITE (FN.EXP,WH.ID,R.WH)
        NEXT I
    END
*-----------------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='RNAU' THEN
*Line [ 85 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DECOUNT.ITEM = DCOUNT(R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM),@VM)
        FOR I = 1 TO DECOUNT.ITEM
            R.WH<EXP.VALUE.DATE>  = R.NEW(SCB.WH.TRANS.VALUE.DATE)<1,I>
            ACCT.ITEM   = R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,I>
            WH.ID       = ACCT.ITEM:'-':R.WH<EXP.VALUE.DATE>
*******************************UPDATED BY RIHAM R15**********************

*            DELETE FVAR.EXP, WH.ID
            CALL F.DELETE (FN.EXP,WH.ID)
        NEXT I
    END
*----------------------------------------------------------
    RETURN
END
