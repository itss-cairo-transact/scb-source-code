* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CHQ.COM

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS

    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> THEN
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>[4,4] GT 0 THEN

            CATEG.CR   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>[11,4]
            CURR       = R.NEW(EB.BILL.REG.CURRENCY)
            DR.ACCT    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
            DR.AMTT    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
            LEN.AMT    = LEN(R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>)
            LEN.NEW    = LEN.AMT - 3
            AMT        = DR.AMTT[4,LEN.NEW]
            V.DATE     = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>
            COM.TYP    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>
            DRW.CUS    = R.NEW(EB.BILL.REG.DRAWER)

            FN.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.COM = ''
            CALL OPF(FN.COM,F.COM)
            CALL F.READ(FN.COM,COM.TYP,R.COM,F.COM,ERR111)
            PL.COM = R.COM<FT4.CATEGORY.ACCOUNT>

*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,DR.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DR.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>

            IF CURR NE 'EGP' THEN
                FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
                CALL OPF(FN.CUR,F.CUR)
                CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
                RATE   = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
            END
            IF V$FUNCTION = 'A' AND R.NEW(EB.BILL.REG.RECORD.STATUS)='INAU' THEN

*----*
* CR *
*----*
                Y.ACCT = ''
                IF CURR NE 'EGP' THEN
                    LCY.AMT = AMT * RATE
                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                    FCY.AMT = AMT
                END ELSE
                    LCY.AMT = AMT
                    FCY.AMT = ''
                END
                CATEG  = CATEG.CR

                GOSUB AC.STMT.ENTRY
*----*
* DR *
*----*
                Y.ACCT = DR.ACCT
                PL.COM = ''
                IF CURR NE 'EGP' THEN
                    LCY.AMT = (AMT * RATE ) * -1
                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                    FCY.AMT = AMT * -1
                END ELSE
                    LCY.AMT = AMT * -1
                    FCY.AMT = ''
                END
                CATEG  = CATEG.CR
                GOSUB AC.STMT.ENTRY
            END


**                   -------------------------
**                              REV
**                   -------------------------

            IF V$FUNCTION = 'A' AND  R.NEW(EB.BILL.REG.RECORD.STATUS)='RNAU' THEN

*-----
* CR
*-----
                Y.ACCT = DR.ACCT
                IF CURR NE 'EGP' THEN
                    LCY.AMT = AMT * RATE
                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                    FCY.AMT = AMT
                END ELSE
                    LCY.AMT = AMT
                    FCY.AMT = ''
                END
                CATEG  = CATEG.CR

*----
* DR
*----
                Y.ACCT = ""

                IF CURR NE 'EGP' THEN
                    LCY.AMT = (AMT * RATE ) * -1
                    CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                    FCY.AMT = AMT * -1
                END ELSE
                    LCY.AMT = AMT * -1
                    FCY.AMT = ''
                END
                CATEG  = CATEG.CR

                GOSUB AC.STMT.ENTRY
            END
        END
    END
    RETURN
*--------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""

*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '792'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.COM
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
*    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = DRW.CUS
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN
END
