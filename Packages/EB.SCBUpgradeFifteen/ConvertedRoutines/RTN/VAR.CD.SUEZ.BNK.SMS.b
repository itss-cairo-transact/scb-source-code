* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CD.SUEZ.BNK.SMS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    FN.CU  = "FBNK.CUSTOMER"            ; F.CU   = ""
    CALL OPF(FN.CU,F.CU)

    CALL F.READ(FN.CU,R.NEW(LD.CUSTOMER.ID),R.CU,F.CU,ERRCU)
    WS.MOB.TEL          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>
    COR.FLG             =R.CU<EB.CUS.LOCAL.REF><1,CULR.CORPORATE.FLG>
*    TEXT=COR.FLG;CALL REM
    IF COR.FLG EQ 'YES' THEN
        REPORT.ID='VAR.CD.SUEZ.BNK.SMS'

        CALL PRINTER.ON(REPORT.ID,'')

        PR.HD  =REPORT.ID
*    HEADING PR.HD

        PRINT PR.HD:ID.NEW:',':WS.MOB.TEL<1,1,3>

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
*-----------------------------------------------------
    RETURN
END
