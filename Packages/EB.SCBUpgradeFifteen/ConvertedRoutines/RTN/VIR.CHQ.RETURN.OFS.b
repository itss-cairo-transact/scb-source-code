* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>149</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VIR.CHQ.RETURN.OFS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
**********************************
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "SCB.CHQ.RETURN"
    OFS.OPTIONS = "INP"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""
    COMMA = ","
*CUST = ''

*Line [ 66 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('THE SIGNATURE IS CORRECT', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
***********************CHECK BALAMCE
        LOCK.AMT1 = ''
        ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT.NO, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LOCK.NO=DCOUNT(LOCK.AMT,@VM)
        FOR I=1 TO LOCK.NO
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
            LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
        NEXT I
        NET.BAL = BAL - LOCK.AMT1
        CATEG = ACCT.NO[11,4]
        IF R.NEW(TT.TE.AMOUNT.LOCAL.1) THEN
            AMT.LOC = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        END ELSE
            AMT.LOC = R.NEW(TT.TE.AMOUNT.FCY.1)
        END
        IF AMT.LOC GT NET.BAL THEN
************************************
*Line [ 89 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
            CALL TXTINP('THE AMOUNT IS NOT AVILABLE DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
*     CUST = ''
            IF COMI[1,1] = 'Y' THEN
                ETEXT = 'AMOUNT NOT AVILABLE'
                AF= TT.TE.AMOUNT.LOCAL.1 ; AV=1
                CALL STORE.END.ERROR

*        CUST = R.NEW(TT.TE.CUSTOMER.1)
                CURR = R.NEW(TT.TE.CURRENCY.1)
                ACCT = R.NEW(TT.TE.ACCOUNT.1)
                AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
                CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
                NRTV = R.NEW(TT.TE.NARRATIVE.2)
                STOP.TYPE = '20'
                CHEQUE.TYPE = 'SCB'
                STOP.DATE = TODAY
            END ELSE
                ETEXT = 'AMOUNT NOT AVILABLE'
                CALL STORE.END.ERROR
            END
        END
    END ELSE
*Line [ 113 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL TXTINP('DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
        IF COMI[1,1] = 'Y' THEN
            ETEXT = 'AMOUNT NOT AVILABLE'
            AF= TT.TE.AMOUNT.LOCAL.1 ; AV=1
            CALL STORE.END.ERROR

***************************************
*   CUST = R.NEW(TT.TE.CUSTOMER.1)
            CURR = R.NEW(TT.TE.CURRENCY.1)
            ACCT = R.NEW(TT.TE.ACCOUNT.1)
            AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
            AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
            CHQ.NO = R.NEW(TT.TE.CHEQUE.NUMBER)
            NRTV = R.NEW(TT.TE.NARRATIVE.2)
            STOP.TYPE = '21'
            CHEQUE.TYPE = 'SCB'
            STOP.DATE = TODAY
        END ELSE
            GOSUB ERR.BAL
* R.NEW(TT.TE.NARRATIVE.2) = 'AMOUNT IS NOT AVILABLE'
        END
    END


    MUL.NO = 1

*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR( 'SCB.CHQ.RETURN':@FM:SCB.CHQR.PAYM.STOP.TYPE, ACCT,ST.TYPE)
F.ITSS.SCB.CHQ.RETURN = 'F.SCB.CHQ.RETURN'
FN.F.ITSS.SCB.CHQ.RETURN = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN,FN.F.ITSS.SCB.CHQ.RETURN)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN,ACCT,R.ITSS.SCB.CHQ.RETURN,FN.F.ITSS.SCB.CHQ.RETURN,ERROR.SCB.CHQ.RETURN)
ST.TYPE=R.ITSS.SCB.CHQ.RETURN<SCB.CHQR.PAYM.STOP.TYPE>
    TEXT = ST.TYPE ; CALL REM
    IF ST.TYPE THEN

*Line [ 144 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        MULL.NOO=DCOUNT(ST.TYPE,@VM)
        TEXT = MULL.NOO ; CALL REM
        MUL.NO = MUL.NO + MULL.NOO

    END

    IF CURR THEN
* OFS.MESSAGE.DATA =  "CUSTOMER=":CUST:COMMA
*OFS.MESSAGE.DATA :=  "CURRENCY=":CURR:COMMA
        OFS.MESSAGE.DATA :=  "PAYM.STOP.TYPE:":MUL.NO:"=":STOP.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "FIRST.CHEQUE.NO:":MUL.NO:"=":CHQ.NO:COMMA
*        OFS.MESSAGE.DATA :=  "LAST.CHEQUE.NO=":CHQ.NO:COMMA
        OFS.MESSAGE.DATA :=  "CHEQUE.TYPE:":MUL.NO:"=":CHEQUE.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "STOP.DATE:":MUL.NO:"=":STOP.DATE:COMMA
        IF AMT.LCY THEN
            OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.LCY:COMMA
        END ELSE
            OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT.FCY:COMMA
        END
        OFS.MESSAGE.DATA :=  "BENEFICIARY:":MUL.NO:"=":NRTV


        ID.KEY.LIST= ACCT
        TEXT = "ID.KEY.LIST = ":ID.KEY.LIST ; CALL REM
        F.PATH = FN.OFS.IN
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ID.KEY.LIST:COMMA:OFS.MESSAGE.DATA
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.KEY.LIST:"-":TODAY ON ERROR
            TEXT = " ERROR "
            CALL REM
        END
***** SCB R15 UPG 20160703 - S
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

*DEBUG
***** SCB R15 UPG 20160703 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
******************************
*   R.NEW(TT.TE.NARRATIVE.2) = 'AMOUNT IS NOT AVILABLE'
    END
    RETURN
*****************************************
ERR.BAL:
    IF R.NEW(TT.TE.AMOUNT.LOCAL.1)  THEN
        ETEXT = 'AMOUNT IS NOT AVILABLE'
        AF= TT.TE.AMOUNT.LOCAL.1 ; AV=1
*        CALL TXT( ETEXT)
        CALL STORE.END.ERROR
    END ELSE
        ETEXT = 'AMOUNT IS NOT AVILABLE'
        AF= TT.TE.AMOUNT.FCY.1 ; AV=1
        CALL TXT( ETEXT)
*   CALL STORE.END.ERROR
    END
    RETURN
END
