* @ValidationCode : MjotMTk1NDk3MzY0NjpDcDEyNTI6MTY0ODU0NDk4MjQwNzpNb3VuaXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Mar 2022 11:09:42
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.OFS.UA.MOD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.ABBREVIATION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.HELPTEXT.MAINMENU
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*----------------------------------------

*** UPDATED BY MSABRY 2014/02/26 ***********
    IF R.NEW(MUA.RECORD.STATUS) EQ 'RNAU' THEN
        RETURN
    END
*** END OF UPDATE ***************************

    IF R.NEW(MUA.OVERRIDE) NE '' THEN
        WS.CHK.OVERRIDE    = 0
*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.LVE.OVR.CNT     = DCOUNT(R.NEW(MUA.OVERRIDE),@VM)
        FOR ILVE.CLS = 1 TO WS.LVE.OVR.CNT
            WS.LVE.CLS         = FIELD(R.NEW(MUA.OVERRIDE)<1,ILVE.CLS>,"*",2)
            WS.LVE.CLS.AUTH    = FIELD(R.NEW(MUA.OVERRIDE)<1,ILVE.CLS>,"*",3)
            IF WS.LVE.CLS NE '' AND WS.LVE.CLS.AUTH EQ '' THEN
                RETURN
            END
*Line [59] should be ILVE.CLS insted of WS.LVE.OVR.CNT - ITSS - R21 Upgrade - 2021-12-26
        NEXT ILVE.CLS
*NEXT WS.LVE.OVR.CNT
    END

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*----------------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    COMMA = ","

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "USER.ABBREVIATION"
    OFS.OPTIONS      = "AUTO"

    COMP  = ID.COMPANY
    AC.BR = COMP[2]
*    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    GOSUB GET.DATA
RETURN
*----------------------------------------------------
GET.DATA:
    WS.USR.ID        = R.NEW(MUA.USER.ID)
    WS.USR.CO        = R.NEW(MUA.DEPARTMENT.CODE) + 100
    WS.USR.CO        = 'EG00100':WS.USR.CO[2,2]
    WS.DEPT.ACCT     = R.NEW(MUA.DEP.ACCT.CODE)

    WS.START         = R.NEW(MUA.START.DATE.PROFILE)
    WS.END           = R.NEW(MUA.END.DATE.PROFILE)

    WS.OVER.CLASS    = R.NEW(MUA.OVER.CLASS)
*Line [ 104 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CNT       = DCOUNT(WS.OVER.CLASS,@VM)

    WS.HMM.ID        = R.NEW(MUA.HMM.ID)
*Line [ 108 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.HMM.CNT       = DCOUNT(WS.HMM.ID,@VM)

    WS.FUN           = R.NEW(MUA.FUNCTION)
*Line [ 112 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.FUN.CNT       = DCOUNT(WS.FUN,@VM)

    OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :WS.USR.CO
RETURN
*----------------------------------------------------
BUILD.RECORD:

    GOSUB EDIT.UA

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:WS.USR.ID:COMMA:OFS.MESSAGE.DATA
    DAT = TODAY
*   OFS.ID = "T":TNO:".UA-":ID.NEW:WS.USR.ID:"-":DAT
    OFS.ID = "T":TNO:".UA-":WS.USR.CO[2]:"-":ID.NEW

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

RETURN
************************************************************
**-------------------------------------------------------------------
EDIT.UA:
    WS.COUNT = 0
    FOR ICNT = 1 TO WS.FUN.CNT
        WS.FUN<1,ICNT> = FIELD(WS.FUN<1,ICNT>,".",1)
*Line [ 153 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('HELPTEXT.MAINMENU':@FM:EB.MME.TITLE,WS.HMM.ID<1,ICNT>,WS.HMM.TITLE)
        F.ITSS.HELPTEXT.MAINMENU = 'F.HELPTEXT.MAINMENU'
        FN.F.ITSS.HELPTEXT.MAINMENU = ''
        CALL OPF(F.ITSS.HELPTEXT.MAINMENU,FN.F.ITSS.HELPTEXT.MAINMENU)
        CALL F.READ(F.ITSS.HELPTEXT.MAINMENU,ICNT,R.ITSS.HELPTEXT.MAINMENU,FN.F.ITSS.HELPTEXT.MAINMENU,ERROR.HELPTEXT.MAINMENU)
        WS.HMM.TITLE=R.ITSS.HELPTEXT.MAINMENU<@FM:EB.MME.TITLE,WS.HMM.ID><1>

        WS.COUNT++
        IF WS.COUNT = 1 THEN
            OFS.MESSAGE.DATA   =  "ABBREVIATION:":WS.COUNT:":1=":WS.HMM.TITLE:COMMA
            OFS.MESSAGE.DATA  :=  "ORIGINAL.TEXT:":WS.COUNT:":1=":WS.HMM.ID<1,ICNT>:COMMA
        END ELSE
            OFS.MESSAGE.DATA  : = "ABBREVIATION:":WS.COUNT:":1=":WS.HMM.TITLE:COMMA
            OFS.MESSAGE.DATA  :=  "ORIGINAL.TEXT:":WS.COUNT:":1=":WS.HMM.ID<1,ICNT>:COMMA
        END
    NEXT ICNT

*Line [ 165 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('USER.ABBREVIATION':@FM:EB.UAB.ABBREVIATION,WS.USR.ID,WS.UA.ABB)
    F.ITSS.USER.ABBREVIATION = 'F.USER.ABBREVIATION'
    FN.F.ITSS.USER.ABBREVIATION = ''
    CALL OPF(F.ITSS.USER.ABBREVIATION,FN.F.ITSS.USER.ABBREVIATION)
    CALL F.READ(F.ITSS.USER.ABBREVIATION,WS.USR.ID,R.ITSS.USER.ABBREVIATION,FN.F.ITSS.USER.ABBREVIATION,ERROR.USER.ABBREVIATION)
    WS.UA.ABB=R.ITSS.USER.ABBREVIATION<EB.UAB.ABBREVIATION>
*Line [ 164 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.UA.ABB.CNT     = DCOUNT(WS.UA.ABB,@VM)

*MSABRY 2014/02/25
*    IF WS.UA.ABB.CNT GT WS.COUNT THEN
*        FOR ICNT = WS.COUNT TO WS.UA.ABB.CNT
*            WS.COUNT ++
*            OFS.MESSAGE.DATA  : = "ABBREVIATION:":WS.COUNT:":1=":'NULL':COMMA
*            OFS.MESSAGE.DATA  :=  "ORIGINAL.TEXT:":WS.COUNT:":1=":'NULL':COMMA
*        NEXT ICNT
*    END

RETURN
**-------------------------------------------------------------------
END
