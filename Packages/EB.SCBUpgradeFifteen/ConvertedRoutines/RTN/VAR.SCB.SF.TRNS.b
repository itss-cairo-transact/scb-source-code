* @ValidationCode : MjotMTkwMzU1OTA4MDpDcDEyNTI6MTY0MjMyMzM3OTM0Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Jan 2022 10:56:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>177</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.SCB.SF.TRNS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SF.TRANS
*Line [ 35 ] Adding I_F.ACCOUNT  - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*---------------------------------------------
    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
*---------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.SF.TR.RECORD.STATUS)='INAU' THEN
        CUS.NO = R.NEW(SCB.SF.TR.CUSTOMER.NO) ;  CUS.SECTOR = ""
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.NO,CUS.SECTOR)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        CUS.SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
        TEXT = "SEC= ":CUS.SECTOR   ; CALL REM
        IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN
            IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "MRGN" THEN
                FLG.TRNS = 1
            END

            IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "LOST" THEN
                FLG.TRNS = 2
            END

            IF R.NEW(SCB.SF.TR.TRANSACTION.TYPE) = "EXPD" THEN
                FLG.TRNS = 3
            END

            GOSUB BUILD.RECORD
            GOSUB PRINT.DEAL.SLIP
        END ELSE
            TEXT =  "������ ����� ���� ������"
            CALL REM
        END
    END
RETURN
**----------------------------------------------------------------**
PRINT.DEAL.SLIP:
*---------------
    IF FLG.TRNS = 1 THEN
        CALL REPORT.SF.DEBIT
        CALL REPORT.SF.CREDIT
    END
RETURN
**----------------------------------------------------------------**
BUILD.RECORD:
*-------------
    CURR               = 'EGP'
    AMT                = R.NEW(SCB.SF.TR.MARGIN.VALUE)
    DATEE              = R.NEW(SCB.SF.TR.VALUE.DATE)

** - - - - - - - - - - -  - - - ** 1 ** - - - - - - - - - - - -- - - -  **
    IF FLG.TRNS = 1 THEN
        Y.ACCT         = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        Y.AMT          = AMT * -1
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = ""
*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
        TRNS.COD = '987'
        GOSUB  AC.STMT.ENTRY

        Y.ACCT         = R.NEW(SCB.SF.TR.MARGIN.ACCT)
        Y.AMT          = AMT
        CATEG          = R.NEW(SCB.SF.TR.MARGIN.ACCT)[11,4]
        PL.CAT         = ""
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
        TRNS.COD = '987'
        GOSUB  AC.STMT.ENTRY
*---------- KEY INS.

        Y.ACCT         = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        Y.AMT          = 1500 * -1
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = ""
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
        CALL F.READ(FN.CUS,R.NEW(SCB.SF.TR.CUSTOMER.NO),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>

        TRNS.COD = '988'
        GOSUB  AC.STMT.ENTRY

        Y.ACCT         = R.NEW(SCB.SF.TR.MARGIN.ACCT)
        Y.AMT          = 1500
        CATEG          = R.NEW(SCB.SF.TR.MARGIN.ACCT)[11,4]
        PL.CAT         = ""
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>

        TRNS.COD = '988'
        GOSUB  AC.STMT.ENTRY
    END
** - - - - - - - - - - -  - - - ** 1 ** - - - - - - - - - - - -- - - -  **

** - - - - - - - - - - -  - - - ** 2 ** - - - - - - - - - - - -- - - -  **
    IF FLG.TRNS = 2 THEN

        Y.ACCT         = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        Y.AMT          = AMT  * -1
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        TRNS.COD = '987'
        GOSUB  AC.STMT.ENTRY
        Y.ACCT         = ""
        Y.AMT          = AMT
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = "54018"

    END
** - - - - - - - - - - -  - - - ** 2 ** - - - - - - - - - - - -- - - -  **

** - - - - - - - - - - -  - - - ** 3 ** - - - - - - - - - - - -- - - -  **

    IF FLG.TRNS = 3 THEN

        Y.ACCT         = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)
        Y.AMT          = AMT
        CATEG          = R.NEW(SCB.SF.TR.CUST.DEBIT.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        TRNS.COD = '987'
        GOSUB  AC.STMT.ENTRY

        Y.ACCT         = R.NEW(SCB.SF.TR.MARGIN.ACCT)
        Y.AMT          = AMT * -1
        CATEG          = R.NEW(SCB.SF.TR.MARGIN.ACCT)[11,4]
        PL.CAT         = ""
*--- EDIT 2011/04/12
*Line [ 201 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,Y.ACCT,CUS.ACC)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
        TRNS.COD = '987'
        GOSUB AC.STMT.ENTRY
** - - - - - - - - - - -  - - - ** 3 ** - - - - - - - - - - - -- - - -  **
    END
    ACC.OFFICER        = R.USER<EB.USE.DEPARTMENT.CODE>


RETURN


**----------------------------------------------------------------**

AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*                                                         *
    ENTRY = ""
    MULTI.ENTRIES = ""


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = TRNS.COD
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = PL.CAT
    ENTRY<AC.STE.AMOUNT.LCY>       = Y.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = DATEE
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = Y.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "IC4"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("IC4",TYPE,MULTI.ENTRIES,"")

RETURN

END
