* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
****** WAEL ****

SUBROUTINE VAR.LG.FINAL

* TO MAKE AN FT TRANSACTION TO TRANSFER MONEY FROM
* CURRENCT.ACCT OF THE CUSTOMER TO MARGIN.ACCT

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

   DIM R.FT(FT.AUDIT.DATE.TIME)
   MAT R.FT = ""
   ER.MSG = ""
   ID.FT = ""
   W.STATUS = ""

   R.FT(FT.TRANSACTION.TYPE) = "ACLG"
   R.FT(FT.DEBIT.AMOUNT) = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
   R.FT(FT.DEBIT.CURRENCY) = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
   R.FT(FT.CREDIT.CURRENCY) = R.NEW( LD.LOCAL.REF)< 1,LDLR.ACC.CUR>
   R.FT(FT.PROFIT.CENTRE.CUST) = R.NEW( LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
   R.FT(FT.DEBIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
   R.FT(FT.CREDIT.VALUE.DATE) = R.NEW(LD.VALUE.DATE)
   R.FT(FT.DEBIT.ACCT.NO) = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
   R.FT(FT.CREDIT.ACCT.NO) = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>

   CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
   IF W.STATUS # "L" THEN
      IF NOT(ER.MSG) THEN
         ER.MSG = "ERROR CREATING FT"
      END
   END

RETURN
END
