* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
***-----INGY 24/04/2005----***
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.RECORDS

*1-ROUTINE TO WRITE ON THE FILE(SCB.FUND) ACCOURDING TO (FT) RECOURD
*  WITH FUNCTION AUTHORISE

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS


    F.COUNT = '' ; FN.COUNT = 'F.SCB.FUND'
    CALL OPF(FN.COUNT,F.COUNT)
    ID.NO = ID.NEW
   * TEXT = ID.NO ; CALL REM
    R.COUNT = ''
    IF R.NEW(FT.TRANSACTION.TYPE) = 'OT' THEN
        R.COUNT<SCB.FT.DESCRIOTION> = 'OUTGOING PAYMENT BY SWIFT'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'IT' THEN
        R.COUNT<SCB.FT.DESCRIOTION> = 'OUTGOING PAYMENT BY SWIFT'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'IM' THEN
        R.COUNT<SCB.FT.DESCRIOTION> = 'OUTGOING PAYMENT BY MAIL'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'AC' THEN
        R.COUNT<SCB.FT.DESCRIOTION> = 'TRANSFER BETWEEN ACCOUNTS'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'OD' OR R.NEW(FT.TRANSACTION.TYPE) = 'OC' THEN
        R.COUNT<SCB.FT.DESCRIOTION> = 'DRAFT CHEQUES'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'OD01' OR R.NEW(FT.TRANSACTION.TYPE) = 'OC01' THEN
        R.COUNT<SCB.FT.DESCRIOTION> = 'CERTIFIED CHEQUES'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'AC' AND R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME> = ',SCB.CHQ' THEN
         R.COUNT<SCB.FT.DESCRIOTION> = 'PAID DRAFT CHEQUE'
     END


    R.COUNT<SCB.FT.TRANS.CODE> = R.NEW(FT.TRANSACTION.TYPE)
    R.COUNT<SCB.FT.DEBIT.ACCOUNT> = R.NEW(FT.DEBIT.ACCT.NO)
    R.COUNT<SCB.FT.DEBIT.CURRENCY> = R.NEW(FT.DEBIT.CURRENCY)
    R.COUNT<SCB.FT.DEBIT.CATEGORY> = R.NEW(FT.DEBIT.ACCT.NO)[11,4]
    R.COUNT<SCB.FT.DEBIT.AMOUNT> = R.NEW(FT.AMOUNT.DEBITED)
* R.COUNT<DEBIT.ACC> = R.NEW(FT.DEBIT.VALUE.DATE)
    R.COUNT<SCB.FT.DEBIT.BRANCH> = R.NEW(FT.LOCAL.REF)<1,FTLR.DEBIT.BRANCH>

    R.COUNT<SCB.FT.CREDIT.ACCOUNT> = R.NEW(FT.CREDIT.ACCT.NO)
    R.COUNT<SCB.FT.CREDIT.CURRENCY> = R.NEW(FT.CREDIT.CURRENCY)
    R.COUNT<SCB.FT.CREDIT.CATEGORY> = R.NEW(FT.CREDIT.ACCT.NO)[11,4]
    R.COUNT<SCB.FT.CREDIT.AMOUNT> = R.NEW(FT.AMOUNT.CREDITED)
    R.COUNT<SCB.FT.CREDIT.BRANCH> = R.NEW(FT.LOCAL.REF)<1,FTLR.CREDIT.BRANCH>
*    R.COUNT<CREDIT.ACC> = R.NEW(FT.CREDIT.VALUE.DATE)
    FT.DD.TT = R.NEW(FT.DATE.TIME)
    R.COUNT<SCB.FT.VALUE.DATE> = TODAY
    R.COUNT<SCB.FT.VALUE.TIME> = FT.DD.TT[7,2]:":":FT.DD.TT[9,2]
    FT.INP = R.NEW(FT.INPUTTER)
    FT.INPU = FIELD(FT.INP,'_',2)
    R.COUNT<SCB.FT.USER> = FT.INPU
    R.COUNT<SCB.FT.COMMISSION.AMT> = R.NEW(FT.COMMISSION.AMT)

*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(R.NEW(FT.CHARGE.AMT),@VM)
    FOR I = 1 TO DD
        R.COUNT<SCB.FT.CHARGE.AMT,I> = R.NEW(FT.CHARGE.AMT)<1,I>
    NEXT I

    IF R.NEW(FT.TRANSACTION.TYPE) = 'OT' AND R.NEW(FT.COMMISSION.CODE) THEN
        R.COUNT<SCB.FT.COMMISSION.DESC> = 'OUTCOMMISSION'
    END

*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(R.NEW(FT.CHARGE.AMT),@VM)

    IF R.NEW(FT.TRANSACTION.TYPE) = 'IT' AND R.NEW(FT.COMMISSION.CODE) THEN
        R.COUNT<SCB.FT.COMMISSION.DESC> = 'INCOMMISSION'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'IM' AND R.NEW(FT.COMMISSION.CODE) THEN
        R.COUNT<SCB.FT.COMMISSION.DESC> = 'INCOMMISSION'
    END

    IF R.NEW(FT.TRANSACTION.TYPE) = 'OT' AND R.NEW(FT.CHARGE.CODE) THEN
        R.COUNT<SCB.FT.CHARGE.DESC> = 'OUTEXPENSES'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'IT' AND R.NEW(FT.CHARGE.CODE) THEN
        R.COUNT<SCB.FT.CHARGE.DESC> = 'INEXPENSES'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'IM' AND R.NEW(FT.CHARGE.CODE) THEN
        R.COUNT<SCB.FT.CHARGE.DESC> = 'INEXPENSES'
    END
    IF R.NEW(FT.TRANSACTION.TYPE) = 'AC' AND R.NEW(FT.CHARGE.CODE) THEN
        R.COUNT<SCB.FT.CHARGE.DESC> = 'TRANSFER EXPENSES'
    END
    IF R.NEW(FT.DEBIT.CURRENCY) = 'EGP' THEN
        FT.DB.EQUIV = R.NEW(FT.AMOUNT.DEBITED)[4,20] * 1
        R.COUNT<SCB.FT.DR.EQUIVALENT> = FT.DB.EQUIV
    END ELSE
*Line [ 125 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR("CURRENCY":@FM:EB.CUR.MID.REVAL.RATE,R.NEW(FT.DEBIT.CURRENCY),RR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.NEW(FT.DEBIT.CURRENCY),R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
RR=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
        FT.DB.EQUIV = R.NEW(FT.AMOUNT.DEBITED)[4,20] * RR
*     FT.DB.EQUIV1 = FIELD(FT.DB.EQUIV,'.',1)
*      FT.DB.EQUIV2 = FIELD(FT.DB.EQUIV,'.',2)[1,2]
*      DB.EQUIV = FT.DB.EQUIV1:'.':FT.DB.EQUIV2
        R.COUNT<SCB.FT.DR.EQUIVALENT> = FT.DB.EQUIV
    END

    IF R.NEW(FT.CREDIT.CURRENCY) = 'EGP' THEN
        FT.CR.EQUIV = R.NEW(FT.AMOUNT.CREDITED)[4,20] * 1
        R.COUNT<SCB.FT.CR.EQUIVALENT> = FT.CR.EQUIV
    END ELSE
*Line [ 143 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR("CURRENCY":@FM:EB.CUR.MID.REVAL.RATE,R.NEW(FT.CREDIT.CURRENCY),RR1)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.NEW(FT.CREDIT.CURRENCY),R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
RR1=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
        FT.CR.EQUIV = R.NEW(FT.AMOUNT.CREDITED)[4,20] * RR1
*      FT.CR.EQUIV1 = FIELD(FT.CR.EQUIV,'.',1)
*      FT.CR.EQUIV2 = FIELD(FT.CR.EQUIV,'.',2)[1,2]
*      CR.EQUIV = FT.CR.EQUIV1:'.':FT.CR.EQUIV2
        R.COUNT<SCB.FT.CR.EQUIVALENT> = FT.CR.EQUIV
    END

    CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
*   CALL JOURNAL.UPDATE(ID.NO)
*   CLOSE F.COUNT

    RETURN
END
