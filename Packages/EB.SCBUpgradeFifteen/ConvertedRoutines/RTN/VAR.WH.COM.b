* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-43</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.WH.COM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*----------------------------------------------------
    DEBIT.ACCT = '' ; DEBIT.CUST.NO  = '' ; REF=''
    CREDIT.ACCT= '' ; CREDIT.ACCT.NO = '' ; R.DOC.PRO=''
    CHARGE     = '' ; COMMISSION     = '' ; NO.CHARGE = 0 ; NO.COMM = 0

    FN.CUR = 'FBNK.CURRENCY'          ; F.CUR = ''; R.CUR = '' ; E11=''
    CALL OPF( FN.CUR,F.CUR)
    FN.CH = 'FBNK.FT.CHARGE.TYPE'     ; F.CH  = ''; R.CH  = '' ; ERR2 =''
    CALL OPF(FN.CH,F.CH)
    FN.CO = 'FBNK.FT.COMMISSION.TYPE' ; F.CO  = ''; R.CO  = '' ; ERR3 =''
    CALL OPF(FN.CO,F.CO)

    DEBIT.ACCT   = R.NEW(SCB.WH.TRANS.ACCT.CHRG.COMM)

    IF DEBIT.ACCT NE '' THEN
*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        *CALL DBR('ACCOUNT':@FM: AC.ACCOUNT.OFFICER,DEBIT.ACCT,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,DEBIT.ACCT,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        ACC.OFFICER  = AC.COMP[8,2]

        DEBIT.CATEG  = DEBIT.ACCT[11,4]
        COM.AMT      = R.NEW(SCB.WH.TRANS.COMMISSION.AMT)
        CALL EB.ROUND.AMOUNT ('EGP',COM.AMT,'',"2")
        COM.TYPE     = R.NEW(SCB.WH.TRANS.COMMISSION.TYPE)
        CALL F.READ( FN.CO,COM.TYPE, R.CO, F.CO, E1)
        CREDIT.ACCT  = R.CO<FT4.CATEGORY.ACCOUNT>
*PL.ACCT.NO   = ''
        CREDIT.CATEG = DEBIT.CATEG
        CURR         = R.NEW(SCB.WH.TRANS.CURRENCY)

******* GET RATE ***********
        IF CURR NE 'EGP' THEN
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE        = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
            AMOUNT.FCY  = COM.AMT * RATE
            CALL EB.ROUND.AMOUNT ('EGP',AMOUNT.FCY,'',"2")
        END

*--- EDIT 2011/04/12
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM: AC.CUSTOMER,DEBIT.ACCT,CUS.ACC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.ACC=R.ITSS.ACCOUNT<AC.CUSTOMER>
*-------------------
********************CHECK IF IT IS INPUT OR REVERSE FUNCTION*********
        IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='INA2' THEN
            GOSUB AC.STMT.ENTRY.D
            GOSUB AC.STMT.ENTRY.C
        END
        IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='RNA2' THEN
            GOSUB AC.STMT.ENTRY.R.D
            GOSUB AC.STMT.ENTRY.R.C

        END
    END
*********************************************************************
    RETURN
**********************************************************************


AC.STMT.ENTRY.D:
    ENTRY         = ""
    MULTI.ENTRIES = ""

    COM.AMT.D     = COM.AMT * -1
    AMOUNT.FCY.D  = AMOUNT.FCY * -1

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = DEBIT.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '868'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = DEBIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.D
    END
    IF AMOUNT.FCY EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

AC.STMT.ENTRY.C:

    ENTRY         = ""
    MULTI.ENTRIES = ""


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ''
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '868'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = CREDIT.ACCT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CREDIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY
    END
    IF AMOUNT.FCY EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************
AC.STMT.ENTRY.R.D:

    ENTRY         = ""
    MULTI.ENTRIES = ""

    COM.AMT.D        = COM.AMT * -1
    AMOUNT.FCY.D    = AMOUNT.FCY * -1

    ENTRY<AC.STE.ACCOUNT.NUMBER>   = ''
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '868'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = CREDIT.ACCT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CREDIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY.D
    END
    IF AMOUNT.FCY EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT.D
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

AC.STMT.ENTRY.R.C:

    ENTRY         = ""
    MULTI.ENTRIES = ""


    ENTRY<AC.STE.ACCOUNT.NUMBER>   = DEBIT.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    ENTRY<AC.STE.TRANSACTION.CODE> = '868'
    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ''
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = DEBIT.CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR

    IF AMOUNT.FCY NE '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = AMOUNT.FCY
    END
    IF AMOUNT.FCY EQ '' THEN
        ENTRY<AC.STE.AMOUNT.LCY>   = COM.AMT
        ENTRY<AC.STE.AMOUNT.FCY>   = ''
    END

    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = CUS.ACC
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

    RETURN

************************************************************

END
