* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>78</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.COMMIT.OFSS


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    TEXT = "A" ; CALL REM
    IF R.NEW(LD.CONVERSION.TYPE) EQ "MID" THEN
        TEXT = "B" ; CALL REM
        GOSUB INITIALISE
        TEXT = "C" ; CALL REM
        GOSUB BUILD.RECORD
        TEXT = "D" ; CALL REM
    END

    RETURN

*------------------------------
INITIALISE:
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "LD.LOANS.AND.DEPOSITS"
*OFS.OPTIONS = "SCB.COMMIT1.H"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    DAT = TODAY

**************************************************CRAETE DEPOSIT BY OFS**********************************************

    OFS.MESSAGE.DATA  =  "CUSTOMER.ID=":R.NEW(LD.CUSTOMER.ID):COMMA
    OFS.MESSAGE.DATA :=  "CURRENCY=":R.NEW(LD.CURRENCY):COMMA
    OFS.MESSAGE.DATA :=  "AMOUNT=":R.NEW(LD.AMOUNT):COMMA
    OFS.MESSAGE.DATA :=  "BUS.DAY.DEFN=":R.NEW(LD.BUS.DAY.DEFN):COMMA
    OFS.MESSAGE.DATA :=  "VALUE.DATE=":R.NEW(LD.VALUE.DATE):COMMA
    OFS.MESSAGE.DATA :=  "FIN.MAT.DATE=":R.NEW(LD.FIN.MAT.DATE):COMMA
    OFS.MESSAGE.DATA :=  "CATEGORY=":21095:COMMA
    OFS.MESSAGE.DATA :=  "INT.RATE.TYPE=":R.NEW(LD.INT.RATE.TYPE):COMMA
    OFS.MESSAGE.DATA :=  "INTEREST.RATE=10":COMMA
    OFS.MESSAGE.DATA :=  "INT.PAYMT.METHOD=1":COMMA
    OFS.MESSAGE.DATA :=  "INTEREST.BASIS=B":COMMA
    OFS.MESSAGE.DATA :=  "L.C.U.TYPE=REVOLVING":COMMA
    OFS.MESSAGE.DATA :=  "STA........Y.N=NO":COMMA
    OFS.MESSAGE.DATA :=  "AUTO.SCHEDS=NO":COMMA
    OFS.MESSAGE.DATA :=  "DEFINE.SCHEDS=YES":COMMA
    OFS.MESSAGE.DATA :=  "INT.LIQ.ACCT=":R.NEW(LD.INT.LIQ.ACCT):COMMA
    OFS.MESSAGE.DATA :=  "CHRG.LIQ.ACCT=":R.NEW(LD.CHRG.LIQ.ACCT):COMMA
    OFS.MESSAGE.DATA :=  "LIQUIDATION.MODE=":R.NEW(LD.LIQUIDATION.MODE):COMMA

    OFS.MESSAGE.DATA :=  "LOCAL.REF:59:1=":ID.NEW

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    TEXT =  " OFS.ERR": OFS.ERR ; CALL REM
    TEXT = "F.IN":F.OFS.IN ; CALL REM
    TEXT = FN.OFS.IN ; CALL REM
    TEXT = ID.NEW:"-":DAT ; CALL REM
*    WRITE OFS.REC ON F.OFS.IN, ID.NEW:"-":DAT :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    WRITE OFS.REC ON F.OFS.IN, "ZZZ" :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    TEXT = 'OK' ; CALL REM
**************************************************************************************************************
****************************************************VESHAL.TSA************
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
*************************************************************************
    RETURN
*END
