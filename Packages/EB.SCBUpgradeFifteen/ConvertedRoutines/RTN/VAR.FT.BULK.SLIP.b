* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VAR.FT.BULK.SLIP
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT

DIM R.SAVE(FBC.AUDIT.DATE.TIME)
MAT R.SAVE = ""
MAT R.SAVE = MAT R.NEW
R.SAVE(FBC.ORDERING.ACCT) = R.NEW(FBC.ORDERING.ACCT)
R.SAVE(FBC.AMOUNT) = R.NEW(FBC.AMOUNT)
R.SAVE(FBC.DR.VALUE.DATE) = R.NEW(FBC.DR.VALUE.DATE)
FOR I = 1 TO R.NEW(FBC.COMMON.REFERENCE)
 
 R.NEW(FBC.ORDERING.ACCT) = ""
 R.NEW(FBC.ORDERING.ACCT) = R.SAVE(FBC.ORDERING.ACCT)<1,I>
 R.NEW(FBC.AMOUNT) = ""
 R.NEW(FBC.AMOUNT) =  R.SAVE(FBC.AMOUNT)<1,I>
 R.NEW(FBC.DR.VALUE.DATE) = ""
 R.NEW(FBC.DR.VALUE.DATE) = R.SAVE(FBC.DR.VALUE.DATE)<1,I>
 CALL PRODUCE.DEAL.SLIP("FT.BULK.DR")

NEXT I

R.NEW(FBC.ORDERING.ACCT) = R.SAVE(FBC.ORDERING.ACCT)
R.NEW(FBC.AMOUNT) = R.SAVE(FBC.AMOUNT)
R.NEW(FBC.DR.VALUE.DATE) = R.SAVE(FBC.DR.VALUE.DATE)

   RETURN

END
