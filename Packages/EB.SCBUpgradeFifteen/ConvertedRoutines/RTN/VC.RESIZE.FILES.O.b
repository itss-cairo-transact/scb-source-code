* @ValidationCode : MjoxMTcwOTE3Nzc6Q3AxMjUyOjE2NDIyODA4ODM1MTA6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Jan 2022 23:08:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
PROGRAM VC.RESIZE.FILES.O
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE


*INCLUDE JBC.h

*Line [ 28 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPEN "VOC" TO F.VOC ELSE NULL

    SEL.CMD = "SSELECT VOC WITH ( @ID LIKE F... AND @ID UNLIKE ...FBNK.CATEG... AND @ID UNLIKE ...FBNK.STMT... AND @ID UNLIKE ...FBNK.RE.CONSOL.SPEC.ENTRY... AND @ID UNLIKE ...FBNK.LD.LOANS.AND.DEPOSITS$HIS... AND @ID UNLIKE ...FBNK.FUNDS.TRANSFER$HIS... AND @ID UNLIKE ...FBNK.STMT.PRINTED... AND @ID UNLIKE ...FBNK.RE.CONSOL.STMT.ENT.KEY... AND @ID UNLIKE ...FBNK.STMT.VAL.ENTRY... AND @ID UNLIKE ...FBNK.RE.CONSOL.SPEC.ENT.KEY... AND @ID UNLIKE ...FBNK.RE.SPEC.ENTRY.DETAIL... AND @ID UNLIKE ...FBNK.ACCT.ACTIVITY... AND @ID UNLIKE ...FBNK.EB.CONTRACT.BALANCES... AND @ID UNLIKE ...FBNK.CUSTOMER$HIS... AND @ID UNLIKE ...FBNK.ACCOUNT$HIS... AND @ID UNLIKE ...FBNK.RE.SPEC.ENTRY.XREF... AND @ID UNLIKE ...FBNK.CATEG.ENT.ACTIVITY... AND @ID UNLIKE ...FBNK.RE.STAT.LINE.BAL... AND @ID UNLIKE ...FBNK.RE.CONSOL.PROFIT... AND @ID UNLIKE ...FBNK.RE.CONSOL.SPEC.ENT.KEY... ) AND F1 LIKE F... AND F2 NE ''"
    CALL EB.READLIST(SEL.CMD,FILE.LIST,"","","")

    PRINT "DO YOU WANT TO RESIZE ALL FILES?":FILE.LIST
    INPUT RESIZE.ALL
    RESIZE.ALL = "Y"

    LOOP

        REMOVE FILE.ID FROM FILE.LIST SETTING MORE.IDS

    WHILE FILE.ID:MORE.IDS
        FILENAME=''
        PRINT FILE.ID
        FILE.NAME = ""
        FILE.NAME<1> = FILE.ID
        FILE.NAME<2> = "NO.FATAL.ERROR"
        FILE.VAR = ""
        CALL OPF(FILE.NAME,FILE.VAR)

        IF ETEXT THEN CONTINUE

        IF IOCTL(FILE.VAR,JBC_COMMAND_GETFILENAME,FILENAME) ELSE
            CRT "IOCTL failed !!" ; EXIT(2)
        END

        EXE.CMD = "jrf -R ":FILENAME
        EXECUTE EXE.CMD CAPTURING RESIZE.OUTPUT

        IF INDEX(RESIZE.OUTPUT,"Resize",1) THEN
            PRINT RESIZE.OUTPUT
            IF RESIZE.ALL = "Y" THEN
                RESIZE.ANS = "Y"
            END ELSE
                PRINT "DO YOU WANT TO RESIZE THIS FILE Y/N?"
                INPUT RESIZE.ANS
            END
            IF RESIZE.ANS = "Y" THEN
                NEW.MODULO = FIELD(FIELD(TRIM(RESIZE.OUTPUT)," ",14,1),".",1)
                EXE.CMD = "jrf -S":NEW.MODULO:" ":FILENAME
                EXECUTE EXE.CMD
            END
        END

    REPEAT

    STOP
END
