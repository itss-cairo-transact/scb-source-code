* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>875</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.CHQ.REG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
********************
    FN.BR.COM= 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
    CALL OPF(FN.BR.COM,F.BR.COM)
    BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
**   TEXT = "CUSS" : BILL.CUS ; CALL REM
    CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)

    COM.AMT   = R.BR.COM<BR.CUS.COMM.AMT>
    COM.PR    = R.BR.COM<BR.CUS.COMM.PREC>
    CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>

**   TEXT  = "COM.PR" : COM.PR ; CALL REM
**   TEXT  = "COM.AMT" : COM.AMT ; CALL REM
**   TEXT  = "CHRG.AMT" : CHRG.AMT ; CALL REM
    IF CHRG.AMT NE 0 THEN
        IF COM.AMT EQ 0 THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = "EGP0.00"
        END

********************
**********************HYTHAM******20081230
        IF R.NEW(EB.BILL.REG.DRAWER)[1,3] EQ '994'  THEN

            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ''
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> = ''

        END
*****************************************
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
*------------------------------
INITIALISE:
        FN.OFS.SOURCE ="F.OFS.SOURCE"
        F.OFS.SOURCE = ""

        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - S
        FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
        F.OFS.IN         = 0
        F.OFS.BK         = 0
        OFS.REC          = ""
        OFS.OPERATION    = "FUNDS.TRANSFER"
        OFS.OPTIONS      = "BR"
*** OFS.USER.INFO    = "/"
************HYTHAM********20090128*******************************
        COMP = C$ID.COMPANY
        OFS.USER.INFO = R.USER<EB.USE.SIGN.ON.NAME>:"/":"/" :COMP
**************HYTHAM******20090128*******************************
        OFS.TRANS.ID     = ""
        OFS.MESSAGE.DATA = ""

        RETURN
*----------------------------------------------------
BUILD.RECORD:
        COMMA = ","

**************************************************CRAETE FT BY OFS**********************************************
        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC22":COMMA

        BR.CUR=R.NEW(EB.BILL.REG.CURRENCY)
        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":BR.CUR:COMMA

        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":BR.CUR:COMMA

        BR.ACCT.DR=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":BR.ACCT.DR:COMMA

        BR.ACCT.CR = "PL52014"
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":BR.ACCT.CR:COMMA

        DR.AMTT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>
        DR.AMT = DR.AMTT[4,2]
        OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DR.AMT:COMMA

        V.DATE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>
        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA

***    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ "7"   THEN
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ "7" AND R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994'  THEN
            PER = ".2"
            MIN = "10"
            MAX = "300"
            COMM.AMT = (R.NEW(EB.BILL.REG.AMOUNT)*PER/100)
            IF COMM.AMT LT MIN THEN
                COMM.AMT = MIN
            END ELSE
                IF COMM.AMT GT MAX THEN
                    COMM.AMT = MAX
                END
            END
            ZZ = FIELD(COMM.AMT,'.',1)
            XX = FIELD(COMM.AMT,'.',2)
            YY = XX[1,2]
            FF = ZZ:".":YY

            IF COM.PR THEN
                FF = FF * COM.PR
            END

            DD = BR.CUR:FF
***  END
*********************************
            IF R.NEW(EB.BILL.REG.DRAWER) NE '1304848' THEN
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> NE 'EGP0.00' THEN
                    IF COM.AMT AND COM.AMT NE 0 THEN
                        DD = BR.CUR:COM.AMT
                    END
                    OFS.MESSAGE.DATA := "COMMISSION.TYPE=BILLCOLL"::COMMA
                    OFS.MESSAGE.DATA := "COMMISSION.AMT=":DD:COMMA

                    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> THEN
                        OFS.MESSAGE.DATA := "COMMISSION.TYPE=":R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>:COMMA
                    END

                    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> THEN
                        OFS.MESSAGE.DATA := "COMMISSION.AMT=":R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>:COMMA
                    END
                END
            END
        END
*********************************
**    IF  COM.PR = R.BR.COM<BR.CUS.COM.PERCENTAGE> THEN
**        OFS.MESSAGE.DATA := "COMMISSION.AMT=":COM.PR:COMMA
**    END
***************
        OFS.MESSAGE.DATA :=  "ORDERING.BANK=":ID.NEW:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":ID.NEW:COMMA

        F.PATH = FN.OFS.IN
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
        DAT = TODAY
        OFS.ID = "T":TNO:".":ID.NEW:"-":DAT

        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    END
    RETURN
************************************************************
END
