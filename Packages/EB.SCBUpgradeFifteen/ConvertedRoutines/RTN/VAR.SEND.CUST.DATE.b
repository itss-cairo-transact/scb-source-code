* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.SEND.CUST.DATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*----------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
*----------------------------------------
INITIALISE:

    FN.CA = 'F.SCB.CHEQ.APPL' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    FN.CH = 'FBNK.CHEQUE.ISSUE' ; F.CH = ''
    CALL OPF(FN.CH,F.CH)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    RETURN
*----------------------------------------------------
BUILD.RECORD:
*--------------
    IF V$FUNCTION = 'A' THEN
        WS.CHQ.START = R.NEW(CHEQUE.IS.CHQ.NO.START)
        ACC.NO = FIELD(ID.NEW,'.',2)

        CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,E1)
        AC.CUST.NO = R.AC<AC.CUSTOMER>

        T.SEL = "SELECT F.SCB.CHEQ.APPL WITH CUST.NO EQ ":AC.CUST.NO:" AND CHQ.NO.START EQ ":WS.CHQ.START
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CALL F.READ(FN.CA,KEY.LIST,R.CA,F.CA,E1)

            WS.RECV.DATE = R.CA<CHQA.BRN.RECV.DATE>
            IF WS.RECV.DATE EQ '' THEN
                E = 'THIS RECORD NOT RECEVIED FROM OPERATION CENTER'
*** SCB UPG 20160623 - S
*                CALL ERR ; MESSAGE = 'REPEAT'
*                E = '' ; CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END ELSE
                R.CA<CHQA.SEND.TO.CUST.DATE> = TODAY
                CALL F.WRITE(FN.CA,KEY.LIST,R.CA)
            END

        END ELSE
            E = '����� ������ ��� �����'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END
    RETURN
************************************************************
END
