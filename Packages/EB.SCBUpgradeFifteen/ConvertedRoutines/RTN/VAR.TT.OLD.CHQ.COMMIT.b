* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
****23/10/2003 ABEER *************
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.TT.OLD.CHQ.COMMIT
**To Issue A Cheq Record In Cheque.Issue
**AND CHECK  THE Cheque IS ISSUE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF V$FUNCTION = 'A' AND  R.NEW(TT.TE.RECORD.STATUS)[1,3]='INA' THEN

**N** OLD.CHQ.NO=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.NO>
        OLD.CHQ.NO=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>
        ACCT.NO =R.NEW(TT.TE.ACCOUNT.2)
*CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,AC.COMP)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        MYBRN  = AC.COMP[8,2]
*-- EDIT BY NESSMA 2016/08/02
        MYBRN = TRIM(MYBRN , "0" , "L")
*-- END EDIT

        PAY.CHQ.ID= OLD.CHQ.NO:'.':MYBRN

*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO,MYCURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
MYCURR=R.ITSS.ACCOUNT<AC.CURRENCY>
        CHQ='SCB':'.':ACCT.NO:'...'

*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('PAYMENT.STOP':@FM:AC.PAY.FIRST.CHEQUE.NO,ACCT.NO,MYCHECK)
F.ITSS.PAYMENT.STOP = 'FBNK.PAYMENT.STOP'
FN.F.ITSS.PAYMENT.STOP = ''
CALL OPF(F.ITSS.PAYMENT.STOP,FN.F.ITSS.PAYMENT.STOP)
CALL F.READ(F.ITSS.PAYMENT.STOP,ACCT.NO,R.ITSS.PAYMENT.STOP,FN.F.ITSS.PAYMENT.STOP,ERROR.PAYMENT.STOP)
MYCHECK=R.ITSS.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCNO)
F.ITSS.SCB.P.CHEQ = 'F.SCB.P.CHEQ'
FN.F.ITSS.SCB.P.CHEQ = ''
CALL OPF(F.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ)
CALL F.READ(F.ITSS.SCB.P.CHEQ,PAY.CHQ.ID,R.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ,ERROR.SCB.P.CHEQ)
MYACCNO=R.ITSS.SCB.P.CHEQ<P.CHEQ.ACCOUNT.NO>
**********To Issue A Cheq Record In Cheque.Issue*******************
**N** IF R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.NO>  THEN
        IF R.NEW(TT.TE.LOCAL.REF)<1,TTLR.CHEQUE.NO>  THEN
            T.SEL ="SELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

            IF SELECTED EQ '0' THEN
                NO.ISSUED='1'
                CHQ.START='1'
            END ELSE
*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST<SELECTED>,CHECK.ST.NO)
F.ITSS.CHEQUE.ISSUE = 'FBNK.CHEQUE.ISSUE'
FN.F.ITSS.CHEQUE.ISSUE = ''
CALL OPF(F.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE)
CALL F.READ(F.ITSS.CHEQUE.ISSUE,KEY.LIST<SELECTED>,R.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE,ERROR.CHEQUE.ISSUE)
CHECK.ST.NO=R.ITSS.CHEQUE.ISSUE<CHEQUE.IS.CHQ.NO.START>
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*                CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.NUMBER.ISSUED,KEY.LIST<SELECTED>,NO.ISSUED)
F.ITSS.CHEQUE.ISSUE = 'FBNK.CHEQUE.ISSUE'
FN.F.ITSS.CHEQUE.ISSUE = ''
CALL OPF(F.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE)
CALL F.READ(F.ITSS.CHEQUE.ISSUE,KEY.LIST<SELECTED>,R.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE,ERROR.CHEQUE.ISSUE)
NO.ISSUED=R.ITSS.CHEQUE.ISSUE<CHEQUE.IS.NUMBER.ISSUED>

                NEW.CHQ.STRT.NO= CHECK.ST.NO+NO.ISSUED
                NO.ISSUED='1'
                CHQ.START= NEW.CHQ.STRT.NO
            END
**********************************************************************
*        FN.P.CHQ='F.SCB.P.CHEQ';ID=PAY.CHQ.ID;R.P.CHQ='';F.P.CHQ=''
*        CALL F.READ(FN.P.CHQ,ID,R.P.CHQ,F.P.CHQ,ETEXT)
*        R.P.CHQ<P.CHEQ.CHEQ.VAL>=R.NEW(TT.TE.AMOUNT.LOCAL.1)

*        R.P.CHQ<P.CHEQ.CHEQ.DAT>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>
*        R.P.CHQ<P.CHEQ.TRM.DAT>=TODAY
*        R.P.CHQ<P.CHEQ.ACCOUNT.NO>=R.NEW(TT.TE.ACCOUNT.2)
*        R.P.CHQ<P.CHEQ.OLD.KEY>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.ACCT>
*        CALL F.WRITE(FN.P.CHQ,ID,R.P.CHQ)
*******************************************************************
****UPDATED BY NESSREEN AHMED 16/03/2009*****************
*** CALL VAR.TT.OFS(ACCT.NO,NO.ISSUED,CHQ.START)
********************************************************************
            T.SEL1 ="SELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG)
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST1<SELECTED1>,CHQ.LAST)
F.ITSS.CHEQUE.ISSUE = 'FBNK.CHEQUE.ISSUE'
FN.F.ITSS.CHEQUE.ISSUE = ''
CALL OPF(F.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE)
CALL F.READ(F.ITSS.CHEQUE.ISSUE,KEY.LIST1<SELECTED1>,R.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE,ERROR.CHEQUE.ISSUE)
CHQ.LAST=R.ITSS.CHEQUE.ISSUE<CHEQUE.IS.CHQ.NO.START>

            IF CHQ.LAST LT CHQ.START THEN
                FN.P.CHQ='F.SCB.P.CHEQ';ID=PAY.CHQ.ID;R.P.CHQ='';F.P.CHQ=''
                CALL F.READ(FN.P.CHQ,ID,R.P.CHQ,F.P.CHQ,ETEXT)
******UPDATED BY NESSREEN AHMED 24/2/2010********************************
                IF ETEXT THEN
                    R.P.CHQ<P.CHEQ.CHEQ.VAL>=R.NEW(TT.TE.AMOUNT.LOCAL.1)
                    R.P.CHQ<P.CHEQ.CHEQ.DAT>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.ISSUE.DATE>
****UPDATED BY NESSREEN AHMED 16/03/2009*****************
**  R.P.CHQ<P.CHEQ.TRM.DAT>=TODAY
                    R.P.CHQ<P.CHEQ.TRN.DAT>=TODAY
**********************************************************
                    R.P.CHQ<P.CHEQ.ACCOUNT.NO>=R.NEW(TT.TE.ACCOUNT.2)
* R.P.CHQ<P.CHEQ.OLD.KEY>=R.NEW(TT.TE.LOCAL.REF)<1,TTLR.OLD.CHQ.ACCT>
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
                    COMP = C$ID.COMPANY
                    R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP
*********************************************************************
                    CALL F.WRITE(FN.P.CHQ,ID,R.P.CHQ)
                END ELSE
                    E = '��� �� ��� ��� ����� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
                END
****
            END ELSE
                E='Must.Issue.Cheque.First';CALL ERR ;MESSAGE = 'REPEAT'
            END

********************************************************************
        END

    END
    RETURN
END
