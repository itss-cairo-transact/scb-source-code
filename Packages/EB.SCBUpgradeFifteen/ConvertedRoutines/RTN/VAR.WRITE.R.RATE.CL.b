* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1812</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.WRITE.R.RATE.CL
*  PROGRAM OFS.CORRES.STATUS
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*****WRITTEN BY MOHAMED SABRY 22/3/2010 SCB*****
***************************************
    F.CUSTOMER = '' ; FN.CUSTOMER = 'FBNK.CUSTOMER' ; R.CUSTOMER = '' ; E1 = '' ; RETRY2 = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    WS.CL.REF         = R.NEW(RM.CUSTOMER.ID)
    WS.CL.RISK.RATE   = DROUND(R.NEW(RM.TOT.CUS),'0')

    CALL F.READ(FN.CUSTOMER,WS.CL.REF,R.CUSTOMER,F.CUSTOMER,E1)
    R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.RISK.RATE> =  WS.CL.RISK.RATE
    CALL F.WRITE(FN.CUSTOMER,WS.CL.REF,R.CUSTOMER)
    CALL REBUILD.SCREEN
* CALL JOURNAL.UPDATE(WS.CL.REF)

    RETURN
END
