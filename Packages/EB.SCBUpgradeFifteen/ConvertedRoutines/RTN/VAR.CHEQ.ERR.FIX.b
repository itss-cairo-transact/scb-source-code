* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.CHEQ.ERR.FIX

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.ERR.TYPE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*----------------------------------------
    FN.CHQ = 'F.SCB.CHEQ.APPL' ; F.CHQ = ''
    CALL OPF(FN.CHQ,F.CHQ)

    FN.USR = 'F.USER' ; F.USR = ''
    CALL OPF(FN.USR,F.USR)

    FN.USR.SG = 'F.USER.SIGN.ON.NAME' ; F.USR.SG = ''
    CALL OPF(FN.USR.SG,F.USR.SG)
*----------------------------------------
    WS.USR.SIGN = R.USER<EB.USE.SIGN.ON.NAME>
    CALL F.READ(FN.USR.SG,WS.USR.SIGN,R.USR.SG,F.USR.SG,E3)
    WS.USR.ID = R.USR.SG<EB.USO.USER.ID>

    CALL F.READ(FN.USR,WS.USR.ID,R.USR,F.USR,E2)
    WS.USR.DEP = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>

*----------------------------------------
    WS.ERR.CODE = R.NEW(CHQA.ERROR.TYPE)

    WS.CHQ.ID = ID.NEW

    CALL F.READ(FN.CHQ,WS.CHQ.ID,R.CHQ,F.CHQ,E1)
*------------------------------------------

**    IF V$FUNCTION = 'I' THEN
**        IF WS.ERR.CODE EQ '1' THEN
**            IF WS.USR.DEP EQ '5008' THEN
**                ETEXT = '��� �������� ��� ����� ���' ; CALL  STORE.END.ERROR
**            END ELSE
**                WS.CHARGE.DATE = R.NEW(CHQA.CHARGE.DATE)
**                IF WS.CHARGE.DATE NE '' THEN
**                    ETEXT = '�� ����� ������� �� ���' ; CALL STORE.END.ERROR
**                END
**            END
**        END

**        IF WS.ERR.CODE EQ '2' THEN
**            IF WS.USR.DEP NE '5008' THEN
**                ETEXT = '��� �������� ��� ���� ����� ������� ���' ; CALL STORE.END.ERROR
**            END
**        END

**        IF WS.ERR.CODE EQ '3' THEN
**            IF WS.USR.DEP EQ '5008' THEN
**                ETEXT = '��� �������� ��� ����� ���' ; CALL  STORE.END.ERROR
**            END
**        END

**        IF WS.ERR.CODE EQ '4' THEN
**            IF WS.USR.DEP EQ '5008' THEN
**                ETEXT = '��� �������� ��� ����� ���' ; CALL  STORE.END.ERROR
**            END
**        END
**    END
*-----------------------------------------------------------
**    IF V$FUNCTION = 'A' THEN
    IF WS.ERR.CODE EQ '1' THEN
        IF WS.USR.DEP EQ '5008' THEN
            ETEXT = '��� �������� ��� ����� ���' ; CALL  STORE.END.ERROR
        END ELSE
            WS.CHARGE.DATE = R.NEW(CHQA.CHARGE.DATE)
            IF WS.CHARGE.DATE NE '' THEN
                ETEXT = '�� ����� ������� �� ���' ; CALL STORE.END.ERROR
            END ELSE
                R.NEW(CHQA.CHARGE.DATE) = TODAY
            END
        END
    END

    IF WS.ERR.CODE EQ '2' THEN
        IF WS.USR.DEP NE '5008' THEN
            ETEXT = '��� �������� ��� ���� ����� ������� ���' ; CALL STORE.END.ERROR
        END ELSE
            R.NEW(CHQA.SEND.TO.BRN.DATE) = ''
        END
    END

    IF WS.ERR.CODE EQ '3' THEN
        IF WS.USR.DEP EQ '5008' THEN
            ETEXT = '��� �������� ��� ����� ���' ; CALL  STORE.END.ERROR
        END ELSE
            R.NEW(CHQA.PRINT.VAUT.DATE) = ''
        END
    END

    IF WS.ERR.CODE EQ '4' THEN
        IF WS.USR.DEP EQ '5008' THEN
            ETEXT = '��� �������� ��� ����� ���' ; CALL  STORE.END.ERROR
        END ELSE
            R.NEW(CHQA.SEND.TO.CUST.DATE) = TODAY
            R.NEW(CHQA.PRINT.VAUT.DATE)   = ''
        END
    END
**    END
    RETURN
************************************************************
END
