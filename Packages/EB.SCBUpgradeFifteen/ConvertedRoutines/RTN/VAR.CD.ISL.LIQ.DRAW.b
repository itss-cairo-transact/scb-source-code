* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*------------------------MAI DEC 2018-----------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.CD.ISL.LIQ.DRAW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


*Line [ 41 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR("LD.LOANS.AND.DEPOSITS":@FM:LD.DRAWDOWN.ACCOUNT,ID.NEW,DRAWDOWN.LIQ.AC)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
DRAWDOWN.LIQ.AC=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.DRAWDOWN.ACCOUNT>
* TEXT = "CUSTOMER DRAWLIQ.ACCT = ": DRAWDOWN.LIQ.AC ; CALL REM;
* TEXT = "CURRENT LIQ ACCT = ": R.NEW(LD.DRAWDOWN.ACCOUNT);CALL REM;
    CUST.DRAW.LIQ.AC = DRAWDOWN.LIQ.AC
    CD.QUAN        = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
    CD.RECOVERED   = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.NO>
    IF CD.RECOVERED LE CD.QUAN THEN
        REMAINING.CD = CD.QUAN  - CD.RECOVERED
    END
    IF  REMAINING.CD GT '0' THEN
        CURR   = R.NEW(LD.CURRENCY)
        AMT    =ABS(R.NEW(LD.AMOUNT.INCREASE))
*   TEXT = 'AMOUNT=': AMT;CALL REM;
**----------------------------------------------------------------**
****                        DEB.INT                             ****
**----------------------------------------------------------------**
*--------
*   CR
*--------
        LCY.AMT = AMT
        FCY.AMT = ''
        Y.ACCT = CUST.DRAW.LIQ.AC
*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,CUST.DRAW.LIQ.AC,CATG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CUST.DRAW.LIQ.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATG=R.ITSS.ACCOUNT<AC.CATEGORY>

        CATEG  = CATG
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,CUST.DRAW.LIQ.AC,ACC.OFFICER)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CUST.DRAW.LIQ.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OFFICER=R.ITSS.ACCOUNT<AC.ACCOUNT.OFFICER>
*    TEXT = 'CR ACCOUNT=':Y.ACCT;CALL REM;
        FLG = 'CR'
        GOSUB AC.STMT.ENTRY
*        TEXT = "FALG IN CR = ": FLAG.D;CALL REM;
*-------
*  DR
*-------
        Y.ACCT = R.NEW(LD.DRAWDOWN.ACCOUNT)
        LCY.AMT = AMT * -1
        FCY.AMT = ''

        PL.CAT = ''

*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,Y.ACCT,CATG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,Y.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATG=R.ITSS.ACCOUNT<AC.CATEGORY>
        CATEG  = CATG
        TEXT = 'DR ACCOUNT=':Y.ACCT;CALL REM;
        FLG = 'DR'
        GOSUB AC.STMT.ENTRY
        RETURN
    END
**********************************************************
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
*
    TEXT = 'FLAG=':FLG;CALL REM;
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = ID.COMPANY
    IF FLG EQ 'CR' THEN
        ENTRY<AC.STE.TRANSACTION.CODE> = '879'
    END
    IF FLG EQ 'DR' THEN
        ENTRY<AC.STE.TRANSACTION.CODE> = '878'
    END

    ENTRY<AC.STE.THEIR.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.TRANS.REFERENCE>  = ID.NEW
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEG
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CURR
    ENTRY<AC.STE.AMOUNT.FCY>       = ""
    ENTRY<AC.STE.EXCHANGE.RATE>    = ""
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "LD"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    CALL EB.ACCOUNTING("LD",TYPE,MULTI.ENTRIES,"")
*END
    RETURN
END
