* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*----------------------------------------------
* CREATED BY RIHAM YOUSSEF 28/7/2020
    SUBROUTINE VAR.FT.INVEST.FUND
*----------------------------------------------

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SMS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS



    FN.SMS  = 'F.SCB.SMS'    ; F.SMS   = '' ; R.SMS  = ''
    CALL OPF(FN.SMS,F.SMS)

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.FT   = 'FBNK.FUNDS.TRANSFER'    ; F.FT   = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    className = 'smsapp.SmsApp'
    methodName = '$callSms'

*---------------SUB-------------

    IF V$FUNCTION = 'I' AND R.NEW(FT.DEBIT.ACCT.NO)[1,8] EQ 'EGP19016' THEN
        AC.NO   = R.NEW(FT.CREDIT.ACCT.NO)
        DEB.CUS = R.NEW(FT.CREDIT.CUSTOMER)
        DEB.AMT   = R.NEW(FT.DEBIT.AMOUNT)
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SMS.1,DEB.CUS,TELE.NO)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,DEB.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
TELE.NO=R.ITSS.CUSTOMER<EB.CUS.SMS.1>

        CALL F.READ(FN.SMS,'INVEST.FUND',R.SMS,F.SMS,E.SMS)
        STAT = R.SMS<SMS.STATUS>
        URL  = R.SMS<SMS.URL>
        LOCATE 'DB' IN STAT<1,1> SETTING POS THEN
            STAT.1   = R.SMS<SMS.STATUS,POS>
            STRING.1 = R.SMS<SMS.STRING,POS>
*Line [ 70 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CHANGE @SM TO ' ' IN STRING.1
            STRING.1 = STRING.1 :' ' : DEB.AMT

        END

    END
*------------------RED------------

    IF V$FUNCTION = 'I' AND R.NEW(FT.CREDIT.ACCT.NO)[1,8] EQ 'EGP19016'  THEN
        AC.NO     = R.NEW(FT.DEBIT.ACCT.NO)
        DEB.CUS   = R.NEW(FT.DEBIT.CUSTOMER)
        DEB.AMT   = R.NEW(FT.DEBIT.AMOUNT)
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SMS.1,DEB.CUS,TELE.NO)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,DEB.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
TELE.NO=R.ITSS.CUSTOMER<EB.CUS.SMS.1>


        CALL F.READ(FN.SMS,'INVEST.FUND',R.SMS,F.SMS,E.SMS)
        STAT = R.SMS<SMS.STATUS>
        URL  = R.SMS<SMS.URL>
        LOCATE 'CR' IN STAT<1,1> SETTING POS THEN
            STAT.1 = R.SMS<SMS.STATUS,POS>
            STRING.1 = R.SMS<SMS.STRING,POS>
*Line [ 92 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CHANGE @SM TO ' ' IN STRING.1
            STRING.1 = STRING.1 :' ' : DEB.AMT
        END

    END

*------------------REVE------------

    IF V$FUNCTION = 'R' AND R.NEW(FT.DEBIT.ACCT.NO)[1,8] EQ 'EGP19016'  THEN
*    IF V$FUNCTION = 'R' AND R.NEW(FT.CREDIT.ACCT.NO) THEN
        AC.NO     = R.NEW(FT.CREDIT.ACCT.NO)
        DEB.CUS   = R.NEW(FT.CREDIT.CUSTOMER)
        DEB.AMT   = R.NEW(FT.DEBIT.AMOUNT)
*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.SMS.1,DEB.CUS,TELE.NO)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,DEB.CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
TELE.NO=R.ITSS.CUSTOMER<EB.CUS.SMS.1>

        CALL F.READ(FN.SMS,'INVEST.FUND',R.SMS,F.SMS,E.SMS)
        STAT = R.SMS<SMS.STATUS>
        URL  = R.SMS<SMS.URL>
        LOCATE 'REVE' IN STAT<1,1> SETTING POS THEN
            STAT.1   = R.SMS<SMS.STATUS,POS>
            STRING.1 = R.SMS<SMS.STRING,POS>
*Line [ 114 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CHANGE @SM TO ' ' IN STRING.1
            STRING.1 = STRING.1 :' ' : DEB.AMT
        END

    END
    EXECUTE 'COMO ON VAR.FT.INVEST.FUND'
    PARAM = AC.NO:'::':TELE.NO:'::':STRING.1:'::':STAT.1:'::':URL
    CRT 'PARAM : ':PARAM
    CALLJ className, methodName, PARAM SETTING RET ON ERROR GOTO errHandler
    CRT "Received batch from Java : " : RET
    EXECUTE 'COMO OFF VAR.FT.INVEST.FUND'
    RETURN

errHandler:
    err = SYSTEM(0)
    CRT 'ERROR : ':err
    IF err = 2 THEN

        CRT "Cannot find the JVM.dll !"

        RETURN

    END

    IF err = 3 THEN

        CRT "Class " : className : "doesn't exist !"

        RETURN

    END

    IF err = 5 THEN

        CRT "Method " : methodName : "doesn't exist !"

        RETURN

    END
    RETURN

*--------------------------------------
END
