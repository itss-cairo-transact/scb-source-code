* @ValidationCode : MjoxNDEwNTQxODk6Q3AxMjUyOjE2NDE5NDM5MzI3MDE6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 12 Jan 2022 01:32:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* Create By Nahrawy
* Edit By Nessma
*-----------------------------------------------------------------------------
SUBROUTINE VIR.BR.DEAL.SLIP

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_F.BILL.REGISTER
    $INSERT            I_BR.LOCAL.REFS
*----------------------------------------
    VV = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE>
    XX = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA>
    IN.OUT = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL>

    IF ( VV EQ 7 OR IN.OUT EQ 'NO' ) AND XX EQ '1' THEN
        CALL PRODUCE.DEAL.SLIP("BR.DOCET3.1")
    END
RETURN
END
