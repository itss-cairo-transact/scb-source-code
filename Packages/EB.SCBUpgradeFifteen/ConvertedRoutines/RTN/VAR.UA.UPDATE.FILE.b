* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
** ----- NESSREEN AHMED 29/2/2012 -----
*-----------------------------------------------------------------------------
* <Rating>-7</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.UA.UPDATE.FILE
** TO WRITE ON USER.ABBREVIATION LOCAL TABLE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.USER.ABBREVIATION
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON


    FN.SCBUA = 'F.SCB.USER.ABBREVIATION' ; F.SCBUA = '' ; R.SCBUA = ''
    ETEXT = ''

    COMP = C$ID.COMPANY
    INPUTT  = R.NEW(EB.UAB.INPUTTER)
    DATIME  = R.NEW(EB.UAB.DATE.TIME)
    TIMEXX = DATIME[7,4]
    COMP    = R.NEW(EB.UAB.CO.CODE)
    USR     = ID.NEW
    DATEE   = TODAY

    SS = '' ; NN = ''
    T.SEL = "SELECT F.SCB.USER.ABBREVIATION WITH USER EQ " : USR : " AND UPD.DATE EQ " : DATEE
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF NOT(SELECTED) THEN
        SS = SS+1
        KEY.ID = USR:"-":DATEE:"-":SS
        CALL F.READ( FN.SCBUA,KEY.ID, R.SCBUA, F.SCBUA, ERR.UA)
        R.SCBUA<SCB.UAB.USER> = USR
        R.SCBUA<SCB.UAB.UPD.CURR.NO> = SS
        R.SCBUA<SCB.UAB.UPD.DATE> = TODAY
        R.SCBUA<SCB.UAB.UPD.TIME> = TIMEXX
        R.SCBUA<SCB.UAB.UPD.COMPANY> = COMP
        R.SCBUA<SCB.UAB.UPD.INPUTTER> = INPUTT
        R.SCBUA<SCB.UAB.UPD.AUTHORISER> = OPERATOR

        ABBREV  = R.NEW(EB.UAB.ABBREVIATION)
        ORG.TXT = R.NEW(EB.UAB.ORIGINAL.TEXT)
*Line [ 72 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCT = DCOUNT(ORG.TXT,@VM)
        FOR DD = 1 TO DCT
            R.SCBUA<SCB.UAB.ABBREVIATION,DD> = ABBREV<1,DD>
            R.SCBUA<SCB.UAB.ORIGINAL.TEXT,DD> = ORG.TXT<1,DD>
        NEXT DD
        CALL F.WRITE(FN.SCBUA,KEY.ID,R.SCBUA)
    END ELSE
        NN = SELECTED+1
        KEY.ID = USR:"-":DATEE:"-":NN
        CALL F.READ( FN.SCBUA,KEY.ID, R.SCBUA, F.SCBUA, ERR.UA)
        R.SCBUA<SCB.UAB.USER> = USR
        R.SCBUA<SCB.UAB.UPD.CURR.NO> = NN
        R.SCBUA<SCB.UAB.UPD.DATE> = TODAY
        R.SCBUA<SCB.UAB.UPD.TIME> = TIMEXX
        R.SCBUA<SCB.UAB.UPD.COMPANY> = COMP
        R.SCBUA<SCB.UAB.UPD.INPUTTER> = INPUTT
        R.SCBUA<SCB.UAB.UPD.AUTHORISER> = OPERATOR

        ABBREV  = R.NEW(EB.UAB.ABBREVIATION)
        ORG.TXT = R.NEW(EB.UAB.ORIGINAL.TEXT)
*Line [ 93 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCT = DCOUNT(ORG.TXT,@VM)
        FOR DD = 1 TO DCT
            R.SCBUA<SCB.UAB.ABBREVIATION,DD> = ABBREV<1,DD>
            R.SCBUA<SCB.UAB.ORIGINAL.TEXT,DD> = ORG.TXT<1,DD>
        NEXT DD
        CALL F.WRITE(FN.SCBUA,KEY.ID,R.SCBUA)

    END
***********
    RETURN
END
