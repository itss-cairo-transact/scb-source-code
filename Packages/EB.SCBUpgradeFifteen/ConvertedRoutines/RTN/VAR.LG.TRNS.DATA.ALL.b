* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>357</Rating>
*-----------------------------------------------------------------------------
****** WAEL ****
    SUBROUTINE VAR.LG.TRNS.DATA.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER


**TEXT = 'ALL';CALL REM
    LOCAL.REF = R.NEW(LD.LOCAL.REF)
    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    CUST = LOCAL.REF< 1,LDLR.THIRD.NUMBER>
    MARG.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
    RECORD.STAT = R.NEW(LD.RECORD.STATUS)

*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,ID.NEW,MYLOCAL)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ID.NEW,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
MYLOCAL=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF>
    MARG.OLD=MYLOCAL<1,LDLR.MARGIN.AMT>
*---------------
    IF MYCODE EQ 1271 THEN
        FLG.STMP = '1'
    END
    GOSUB MAKE.REC
    RETURN
*========================== ISSUE ==========================
MAKE.REC:

    IF MYCODE = '1111' THEN
*------------ MARGIN --------------

        IF PGM.VERSION NE ',SCB.LG.ADVANCE.EB' THEN

            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
            DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
            CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>

            IF DB.AMT = '0' THEN
                FLG.AMT = '0'
            END ELSE
                FLG.AMT = '1'
            END

            FLG.STMP= 1

        END ELSE
            FLG.AMT = 0
            FLG.STMP= 1
        END
        GOSUB PRODUCE.MSG
    END
*========================== OPERAION =============================
    IF MYCODE = '1251' THEN
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>
        IF DB.AMT = '0' THEN
            FLG.AMT = '0'
        END ELSE
            FLG.AMT = '1'
        END
        FLG.STMP='0'
        GOSUB PRODUCE.MSG
    END
*------------------------
    IF MYCODE = '1252' THEN
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>
        IF DB.AMT = '0' THEN
            FLG.AMT = '0'
        END ELSE
            FLG.AMT = '1'
        END
        FLG.STMP='0'
        GOSUB PRODUCE.MSG
    END
*============================== CANCEL =============================
    IF (MYCODE >= '1301' AND MYCODE <= '1303') THEN

* --------  MARGIN --------
        DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
        DB.AMT = LOCAL.REF<1,LDLR.MARGIN.AMT>
        DB.ACCT = LOCAL.REF< 1,LDLR.CREDIT.ACCT>
        CR.ACCT = LOCAL.REF< 1,LDLR.DEBIT.ACCT>
        TEXT=DB.AMT:'DRAMT';CALL REM
        IF DB.AMT = '0' THEN
            FLG.AMT = '0'
            TEXT=DB.AMT:'DB.AMT':FLG.AMT;CALL REM
        END ELSE
            FLG.AMT = '1'
        END
        IF MYCODE EQ '1302' OR MYCODE EQ '1303' THEN
            FLG.STMP ='1'
        END ELSE
            FLG.STMP='0'
        END
        GOSUB PRODUCE.MSG
    END
*=========================== CONFISCATE ============================
    IF (MYCODE >= '1401' AND MYCODE <= '1407') THEN
*-----------------
**        TEXT=MYCODE:'CODE';CALL REM
*-------------------
        IF R.NEW(LD.FIN.MAT.DATE) = TODAY THEN
            IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONFISC.AMT> = R.NEW(LD.AMOUNT) THEN
* -------------- MARGIN ----------
                DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
                DB.AMT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
                DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
                CR.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                IF DB.AMT = '0' THEN
                    FLG.AMT = '0'
                END ELSE
                    FLG.AMT = '1'
                END
                FLG.STMP= '1'
                GOSUB PRODUCE.MSG
            END ELSE
* -------------- MARGIN ----------
                DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
                DB.AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
                DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
                CR.ACCT   = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                IF DB.AMT = '0' THEN
                    FLG.AMT = '0'
                END ELSE
                    FLG.AMT = '1'
                END
                FLG.STMP= '1'
                GOSUB PRODUCE.MSG
            END
        END ELSE
*---------- MARGIN -------------

            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            PER = R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>/100
            DB.AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>*PER
            DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            CR.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
*****2013-6-16 R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT> = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT> - DB.AMT
            IF DB.AMT = '0' THEN
                FLG.AMT = '0'
            END ELSE
                FLG.AMT = '1'
            END
            FLG.STMP= '1'
            GOSUB PRODUCE.MSG
        END
    END
*============================ AMENDEMENT ===================
    IF MYCODE = '1241' OR MYCODE = '1242' OR (MYCODE > '1231' AND MYCODE <= '1235') THEN
*------------- MARGIN ------------
        IF  MYCODE EQ '1232' OR MYCODE EQ '1234' OR MYCODE EQ '1233' OR MYCODE EQ '1235' THEN
            FLG.STMP= '1'
        END ELSE
            FLG.STMP = '0'
        END
        IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> > R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> THEN
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            DB.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            CR.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            IF DB.AMT = '0' THEN
                FLG.AMT = '0'
            END ELSE
                FLG.AMT = '1'
            END
            GOSUB PRODUCE.MSG
        END ELSE
            DB.CUR = LOCAL.REF<1,LDLR.ACC.CUR>
            DB.AMT = R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT> - R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
            DB.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            CR.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            IF DB.AMT = '0' THEN
                FLG.AMT = '0'
            END ELSE
                FLG.AMT = '1'
            END
            GOSUB PRODUCE.MSG
        END
    END
    RETURN
*===========================================================
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST,CUST.BRN)
*Line [ 221 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST,CUST.BRN)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.BRN=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CUST.BRN = CUST.BRN[2]
    CUST.BRN = TRIM(CUST.BRN,"0","L")

PRODUCE.MSG:

    PROFT.CUST = LOCAL.REF< 1,LDLR.THIRD.NUMBER>
*    IF R.NEW( LD.LOCAL.REF)< 1,LDLR.VERSION.NAME> = ",SCB.LG.CANCELFINAL" THEN
    IF (MYCODE >= '1301' AND MYCODE <= '1303') THEN
        DB.DATE=R.NEW(LD.LOCAL.REF)<1,LDLR.INSTAL.DUE.DATE>
        TEXT=DB.DATE:'DATE';CALL REM
        CR.DATA=R.NEW(LD.LOCAL.REF)<1,LDLR.INSTAL.DUE.DATE>
    END ELSE
        IF MYCODE = '1111' THEN
            DB.DATE = R.NEW(LD.VALUE.DATE)
            CR.DATA = R.NEW(LD.VALUE.DATE)
        END
    END
*Line [ 233 ] Adding EB.SCBUpgradeFifteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeFifteen.VAR.LG.TRNS.ALL(CHQ.TYPE,VER.NAME,DB.CUR,DB.AMT,PROFT.CUST,DB.DATE,CR.DATA,DB.ACCT,CR.ACCT,CHQ.NO,BENF.CUST,PROFIT.DEPT,ORDER.BANK,FLG.STMP,FLG.AMT)
    RETURN
*-------------------------
END
