* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.BR.EX.UPDATE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.EXT
*---------------------------------------------
    FN.BR.EX = 'F.SCB.BR.EXT' ; F.BR.EX     = '' ; R.BR.EX = '' ; ETEXT.EX = ''
    CALL OPF(FN.BR.EX,F.BR.EX)

    BR.EX.ID   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> :"-":R.NEW(EB.BILL.REG.CO.CODE)

    CALL F.READ(FN.BR.EX,BR.EX.ID,R.BR.EX,F.BR.EX,ETEXT.EX)
*Line [ 38 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BR.NO      = DCOUNT(R.BR.EX<BR.EX.OUR.REFERENCE>,@VM)
    BR.NOO     = BR.NO + 1

    R.BR.EX<BR.EX.OUR.REFERENCE,BR.NOO> = ID.NEW

*WRITE R.BR.EX TO F.BR.EX , BR.EX.ID  ON ERROR
*   TEXT = "CAN NOT WRITE RECORD ":ID.NEW :" TO ":FN.BR.EX ; CALL REM
*END
    CALL F.WRITE(FN.BR,BR.ID, R.BR)
    RETURN

END
