* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>212</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.FT.DR.CHQ.TAX

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.TAX
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

* UPDATE THE TABLE SCB.FT.DR.CHQ
    COMP1 = ID.COMPANY
    IF V$FUNCTION = 'A' AND  R.NEW(FT.RECORD.STATUS) NE 'RNAU' THEN

        F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
        CALL OPF(FN.COUNT,F.COUNT)
        F.TAX = '' ; FN.TAX = 'F.SCB.CUSTOMER.TAX'
        CALL OPF(FN.TAX,F.TAX)
        FN.FT.COMM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COMM = '' ; R.FT.COMM = ''
        CALL OPF(FN.FT.COMM,F.FT.COMM)

        Y.DESC.TAX = ''
        ID.NO = R.NEW(FT.CREDIT.ACCT.NO):".":R.NEW(FT.CHEQUE.NUMBER)
        T.SEL = "SELECT F.SCB.FT.DR.CHQ WITH @ID EQ ": ID.NO
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            E = 'THE CHEQUE ISSUED BEFORE' ; CALL ERR; MESSAGE='REPEAT'
        END
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*        CALL DBR('ACCOUNT':@FM:AC.CO.CODE,R.NEW(FT.CREDIT.ACCT.NO),CR.AC.COM)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.CREDIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.AC.COM=R.ITSS.ACCOUNT<AC.CO.CODE>
        R.COUNT = ''
        R.COUNT<DR.CHQ.COMPANY.CO> = CR.AC.COM
        R.COUNT<DR.CHQ.DEBIT.ACCT> = R.NEW(FT.DEBIT.ACCT.NO)
        R.COUNT<DR.CHQ.CHEQ.NO> = R.NEW(FT.CHEQUE.NUMBER)
        CRD.AMT = R.NEW(FT.AMOUNT.CREDITED)[4,19]
        R.COUNT<DR.CHQ.AMOUNT> = CRD.AMT
        R.COUNT<DR.CHQ.CURRENCY> = R.NEW(FT.CREDIT.CURRENCY)
        R.COUNT<DR.CHQ.BEN,1> = R.NEW(FT.BEN.CUSTOMER)
        R.COUNT<DR.CHQ.BEN,2> = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS,1>
        GOSUB CHQ.TYPE
        R.COUNT<DR.CHQ.CHEQ.TYPE> = FIRST.NO
        R.COUNT<DR.CHQ.NOS.ACCT> = R.NEW(FT.CREDIT.ACCT.NO)
        R.COUNT<DR.CHQ.TRANS.ISSUE> = ID.NEW
        R.COUNT<DR.CHQ.CHEQ.STATUS> = 1
        R.COUNT<DR.CHQ.CHEQ.TYPE> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>
        R.COUNT<DR.CHQ.ISSUE.BRN> = R.NEW(FT.DEPT.CODE)
        R.COUNT<DR.CHQ.CHEQ.DATE> = TODAY
        R.COUNT<DR.CHQ.OLD.CHEQUE.NO> = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
        CALL F.WRITE(FN.COUNT,ID.NO,R.COUNT)
        TEXT = "CHQ = ":ID.NO ; CALL REM
* CALL JOURNAL.UPDATE(ID.NO)
        CLOSE F.COUNT
        XX1 = DCOUNT(R.NEW(FT.COMMISSION.TYPE),@VM)
        FOR XX = 1 TO XX1
            GOSUB TAX.WRITE
        NEXT XX
    END
RETURN
**************************************************************
CHQ.TYPE:
    FIRST.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>[1,1]
    RETURN
**************************************************************
TAX.WRITE:
**********
    IF LEN(XX) EQ 1 THEN
        TTX = '0':XX
    END
    ID.TAX = 'TX':ID.NEW[3,10]:TTX
    TEXT = "TAX = ":ID.TAX ; CALL REM
    R.TAX<CUS.TAX.CUSTOMER.ID>      = R.NEW(FT.LOCAL.REF)<1,FTLR.SUPP.CODE>
    R.TAX<CUS.TAX.TOTAL.AMOUNT>     = R.NEW(FT.DEBIT.AMOUNT)
    DESC.TAX.CODE = R.NEW(FT.COMMISSION.TYPE)<1,XX>

**    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.PERCENTAGE,DESC.TAX.CODE,DESC.TAX)

    CALL F.READ(FN.FT.COMM,DESC.TAX.CODE,R.FT.COMM,F.FT.COMM,ERR.FT.COMM)
    DESC.TAX = R.FT.COMM<FT4.PERCENTAGE,1>

    PSS = DCOUNT(DESC.TAX,@SM)
    FOR DDS = 1 TO PSS
        Y.DESC.TAX = DESC.TAX<1,1,DDS>
        IF Y.DESC.TAX NE 0 THEN
            DDS = PSS
        END
    NEXT DDS
    R.TAX<CUS.TAX.DISCOUNT.PERCENT> = Y.DESC.TAX
    LCC = LEN(R.NEW(FT.COMMISSION.AMT)<1,XX>)
    R.TAX<CUS.TAX.DISCOUNT.AMOUNT>  = R.NEW(FT.COMMISSION.AMT)<1,XX>[4,LCC]
    R.TAX<CUS.TAX.VALUE.DATE>       = TODAY
    R.TAX<CUS.TAX.CO.CODE>          = COMP1
    CALL F.WRITE(FN.TAX,ID.TAX,R.TAX)
    CLOSE F.TAX
    RETURN
**************************************************************
    RETURN
END
