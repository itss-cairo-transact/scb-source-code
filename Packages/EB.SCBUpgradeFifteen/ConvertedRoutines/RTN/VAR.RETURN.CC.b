* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.RETURN.CC


*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.COLLECTION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

*Line [ 34 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DCOUNTBR.ID = DCOUNT (R.NEW(SCB.BT.OUR.REFERENCE),@VM)

    FOR I = 1 TO DCOUNTBR.ID

        BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>

        FN.CC = 'FBNK.CHEQUE.COLLECTION' ; F.CC = '' ; R.CC = ''
        T.SEL = "SELECT FBNK.CHEQUE.COLLECTION WITH TXN.ID EQ ": BR.ID
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN

            CALL OPF( FN.CC,F.CC)
            CALL F.READ( FN.CC,KEY.LIST, R.CC, F.CC, ETEXT)
*          IF R.NEW(SCB.BT.RETURN.REASON)<1,I> THEN
*         CC.AC = "EGP112570001":R.USER<EB.USE.COMPANY.CODE,1>[6,4]
*               R.CC<CHQ.COL.NARRATIVE,1> = R.CC<CHQ.COL.SUSP.POSTED.TO>
*              R.CC<CHQ.COL.NARRATIVE,2> = R.CC<CHQ.COL.CREDIT.ACC.NO>
*             R.CC<CHQ.COL.CREDIT.ACC.NO> = CC.AC
*            R.CC<CHQ.COL.SUSP.POSTED.TO> = CC.AC
*           CALL F.WRITE(FN.CC,KEY.LIST,R.CC)
*      END
        END ELSE
*-------------------------- UPDATE AT 16/06/2008 ----------------------
            KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,BR.ID,LOC.REF)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,BR.ID,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
LOC.REF=R.ITSS.BILL.REGISTER<EB.BILL.REG.LOCAL.REF>
            IF LOC.REF<1,BRLR.CC.NO>[1,2] = "CC" THEN
                CC.ID = LOC.REF<1,BRLR.CC.NO>
                CALL OPF( FN.CC,F.CC)
                CALL F.READ( FN.CC,CC.ID, R.CC, F.CC, ETEXT)

*   IF R.NEW(SCB.BT.RETURN.REASON)<1,I> THEN
*         CC.AC = "EGP112570001":R.USER<EB.USE.COMPANY.CODE,1>[6,4]
*        R.CC<CHQ.COL.NARRATIVE,1> = R.CC<CHQ.COL.SUSP.POSTED.TO>
*       R.CC<CHQ.COL.NARRATIVE,2> = R.CC<CHQ.COL.CREDIT.ACC.NO>
*      R.CC<CHQ.COL.CREDIT.ACC.NO> = CC.AC
*     R.CC<CHQ.COL.SUSP.POSTED.TO> = CC.AC
*    CALL F.WRITE(FN.CC,CC.ID,R.CC)
*   END
                IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,I>) THEN
                    AC.NEW = LOC.REF<1,BRLR.CUST.ACCT>
                    R.CC<CHQ.COL.SUSP.POSTED.TO> = AC.NEW
                    CALL F.WRITE(FN.CC,CC.ID,R.CC)

                END

            END ELSE

                FT.ID = LOC.REF<1,BRLR.FT.REFERENCE>

                T.SEL = "SELECT FBNK.CHEQUE.COLLECTION WITH TXN.ID EQ ": FT.ID
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF SELECTED THEN

                    CALL OPF( FN.CC,F.CC)
                    CALL F.READ( FN.CC,KEY.LIST, R.CC, F.CC, ETEXT)

*        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> THEN
*           CC.AC = "EGP112570001":R.USER<EB.USE.COMPANY.CODE,1>[6,4]
*          R.CC<CHQ.COL.NARRATIVE,1> = R.CC<CHQ.COL.SUSP.POSTED.TO>
*         R.CC<CHQ.COL.NARRATIVE,2> = R.CC<CHQ.COL.CREDIT.ACC.NO>
*        R.CC<CHQ.COL.CREDIT.ACC.NO> = CC.AC
*                  R.CC<CHQ.COL.SUSP.POSTED.TO> = CC.AC
*                 CALL F.WRITE(FN.CC,KEY.LIST,R.CC)
*            END
                    IF NOT(R.NEW(SCB.BT.RETURN.REASON)<1,I>) THEN
                        AC.NEW = LOC.REF<1,BRLR.CUST.ACCT>
                        R.CC<CHQ.COL.SUSP.POSTED.TO> = AC.NEW
                        CALL F.WRITE(FN.CC,CC.ID,R.CC)

                    END
                END
            END
        END
*-------------------------- UPDATE AT 16/06/2008 ----------------------
    NEXT I

    RETURN
END
