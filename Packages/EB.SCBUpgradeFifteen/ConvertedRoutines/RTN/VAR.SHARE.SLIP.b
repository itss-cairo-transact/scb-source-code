* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
    SUBROUTINE VAR.SHARE.SLIP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SHARE.DATA
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*========================================================================
INITIATE:
**    REPORT.ID='VAR.SHARE.SLIP'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY

    RETURN
*========================================================================
PROCESS:
*---------------------

    CUS.ID      = R.NEW(SD.CUSTOMER.CODE)
    CUS.NAME    = R.NEW(SD.CUSTOMER.NAME)
    CUS.NAT     = R.NEW(SD.NATIONALITY)
    CUS.ADDRESS = R.NEW(SD.ADDRESS)
    CUS.IDF     = R.NEW(SD.CUSTOMER.IDF)
    CUS.REG     = R.NEW(SD.CUST.REG.NO)
    CUS.TYPE    = R.NEW(SD.CUSTOMER.TYPE)
    CUS.TEL     = R.NEW(SD.TEL.NO)
    CUS.KEEPER  = R.NEW(SD.KEEPER)
    SH.COUNT    = R.NEW(SD.SHARE.COUNT)
    SH.AMT      = R.NEW(SD.SHARE.AMT)
    SH.VAL      = R.NEW(SD.SHARE.VALUE)
    PAY.WAY     = R.NEW(SD.PAYMENT.WAY)
    CHQ.NO      = R.NEW(SD.CHQ.NO)
    DB.ACCT     = R.NEW(SD.DEBIT.ACCOUNT)
    AMT.LET     = R.NEW(SD.AMT.LETTER)


    XX1  = SPACE(132)  ; XX6  = SPACE(132)
    XX2  = SPACE(132)  ; XX7  = SPACE(132)
    XX3  = SPACE(132)  ; XX8  = SPACE(132)
    XX4  = SPACE(132)  ; XX9  = SPACE(132)
    XX5  = SPACE(132)
*--------------------------------------------------------------

    XX1<1,1>[2,15]  = '��� �������'
    XX1<1,1>[25,35] = ': ':CUS.NAME
    XX1<1,1>[70,15] = "�������"
    XX1<1,1>[90,15] = ': ':CUS.NAT

    XX2<1,1>[2,15]  = "����� �������"
    XX2<1,1>[25,60] = ': ':CUS.ADDRESS

    XX3<1,1>[2,19]  = "����� ����� ������"
    XX3<1,1>[25,18] = ': ':CUS.IDF
    XX3<1,1>[70,19] = "��� ����� �������"
    XX3<1,1>[90,18] = ': ':CUS.REG

    XX4<1,1>[2,15]  = "��� ��������"
    XX4<1,1>[25,15] = ': ':CUS.TEL
    XX4<1,1>[70,19] = "���� �����"
    XX4<1,1>[90,19] = ': ':CUS.KEEPER

    XX5<1,1>[2,15]  = "���  �������"
    XX5<1,1>[25,15] = ': ':CUS.ID
    XX5<1,1>[70,15] = "��� �������"
    XX5<1,1>[90,15] = ': ':CUS.TYPE

    XX6<1,1>[2,15]  = "��� ������"
    XX6<1,1>[25,15] = ': ':SH.COUNT
    XX6<1,1>[70,15] = "���� ������"
    XX6<1,1>[90,15] = ': ':SH.AMT

    XX7<1,1>[2,15]  = "������ ���� ��������"
    XX7<1,1>[25,15] = ': ':SH.VAL

    XX8<1,1>[2,15]  = "����� �����"
    XX8<1,1>[25,15] = ': ':PAY.WAY
    IF PAY.WAY EQ '��� �����' THEN
        XX8<1,1>[70,18] = "��� �����"
        XX8<1,1>[90,18] = ': ':CHQ.NO
    END
    IF PAY.WAY EQ  '�����' THEN
        XX8<1,1>[70,18] =  "��� ������"
        XX8<1,1>[90,18] =  ': ':DB.ACCT
    END

    XX9<1,1>[2,35]  = '������ �������'
    XX9<1,1>[25,90] = ' :':AMT.LET


*-------------------------------------------
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":BRANCH

   * PR.HD :="'L'":SPACE(50):"��� ������ �� ���� ��� ��� ���� ������� �������"
     PR.HD :="'L'":SPACE(50):"�������� �� ����� ��� ��� ���� ����� ��������� �����������"
    PR.HD :="'L'":SPACE(60):"-------------------------------- "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
*===============================================================
    RETURN
END
