* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>59</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.WH.COLL
*TO UPDATE COLLARTAL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER


    IF V$FUNCTION EQ 'A' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        GOSUB CREATE.FILE
***** SCB R15 UPG 20160703 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160703 - E


    END
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "COLLATERAL"
    OFS.OPTIONS = "COLL.WH"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]

    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    D.T = R.NEW(SCB.WH.TRANS.DATE.TIME)
    INPP = R.NEW(SCB.WH.TRANS.INPUTTER)<1,1>

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>

    NEW.FILE = ID.NEW:".":RND(10000)


    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END
    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME

        END
    END


    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","
*******************************************************
    ACCT.NO=R.NEW(SCB.WH.TRANS.WH.ACCT.NO.ITEM)<1,1>
    ACCT.NO=ACCT.NO[1,18]
*    TEXT=ACCT.NO:'ACCT.NO';CALL REM


*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-01-15
*    CALL DBR('SCB.WH.REGISTER':@FM:SCB.WH.COLLACTRAL.ID,ACCT.NO,COLL.ID)
F.ITSS.SCB.WH.REGISTER = 'F.SCB.WH.REGISTER'
FN.F.ITSS.SCB.WH.REGISTER = ''
CALL OPF(F.ITSS.SCB.WH.REGISTER,FN.F.ITSS.SCB.WH.REGISTER)
CALL F.READ(F.ITSS.SCB.WH.REGISTER,ACCT.NO,R.ITSS.SCB.WH.REGISTER,FN.F.ITSS.SCB.WH.REGISTER,ERROR.SCB.WH.REGISTER)
COLL.ID=R.ITSS.SCB.WH.REGISTER<SCB.WH.COLLACTRAL.ID>
    COLL.ID=COLL.ID<1,1>
*    TEXT=COLL.ID;CALL REM
*******************************************************
    COMMA = ","

    OFS.MESSAGE.DATA =  "NOTES=":ID.NEW:COMMA
    OFS.MESSAGE.DAA :=  "COLLATERAL.CODE=":'102'

    ZZZ = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COLL.ID:COMMA:OFS.MESSAGE.DATA
*-- 2011/01/27
*    ZZZ := COMMA:"COLLATERAL.CODE = 301"

    WRITESEQ ZZZ TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZZ
    END
    RETURN
*-------------------------------------------------------
CREATE.FILE:

    D.T = R.NEW(SCB.WH.TRANS.DATE.TIME)
    INPP = R.NEW(SCB.WH.TRANS.INPUTTER)<1,1>

    OFS.REC = ZZZ
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":COLL.ID:".":RND(10000) ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
    RETURN
*--------------------------------------------------------
END
