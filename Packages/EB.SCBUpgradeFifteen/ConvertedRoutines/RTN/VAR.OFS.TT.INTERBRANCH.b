* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeFifteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeFifteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>507</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VAR.OFS.TT.INTERBRANCH

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*TEXT = "VAR.OFS.TT.INTERBRANCH" ; CALL REM

    IF R.NEW(TT.TE.ACCOUNT.1)[1,2] NE R.NEW( TT.TE.ACCOUNT.2)[9,2] THEN
**************************************************************
        TEXT = "INTERBRANCH WILL RUN" ; CALL REM
        FN.OFS.SOURCE = "F.OFS.SOURCE"
        F.OFS.SOURCE  = ""
        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160703 - S
*        CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E

        FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
        F.OFS.IN         = 0
        F.OFS.BK         = 0
        OFS.REC          = ""
        OFS.OPERATION    = "FUNDS.TRANSFER"
        OFS.OPTIONS      = "BR"
***OFS.USER.INFO    = "/"

*************HYTHAM********20090318**********
        COMP = C$ID.COMPANY
        COM.CODE = COMP[8,2]
        OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

        OFS.TRANS.ID     = ""
        OFS.MESSAGE.DATA = ""
        COMMA = ","

**************************************************************

        TXN.TYPE = "AC"
        OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":TXN.TYPE:COMMA

        TT.CUR = R.NEW(TT.TE.CURRENCY.1)
        IF TT.CUR EQ LCCY THEN
            TT.AMT = R.NEW(TT.TE.AMOUNT.LOCAL.2)
        END ELSE
            TT.AMT = R.NEW(TT.TE.AMOUNT.FCY.2)
        END

        OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": TT.AMT:COMMA

        OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": TT.CUR:COMMA

        BR.DEPT=R.USER<EB.USE.DEPARTMENT.CODE>
        OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":BR.DEPT:COMMA
***
        CATEG = "12800"
*TT.DEPT = R.USER<EB.USE.DEPARTMENT.CODE>
*IF LEN(BR.DEPT) EQ 1 THEN
*    BR.DEPT = 0:BR.DEPT
*END

*** DEPOSIT
        IF R.NEW(TT.TE.DR.CR.MARKER) EQ "CREDIT" THEN
            TT.ACCT.DR = TT.CUR:CATEG:"00":R.NEW(TT.TE.ACCOUNT.1)[1,2]
            OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":TT.ACCT.DR:COMMA
            OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":TT.ACCT.DR:COMMA
            TT.ACCT.CR = TT.CUR:CATEG:"00":R.NEW( TT.TE.ACCOUNT.2)[9,2]
            OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":TT.ACCT.CR:COMMA
            OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":TT.ACCT.CR:COMMA
        END
*** WITHDRAW
        IF R.NEW(TT.TE.DR.CR.MARKER) EQ "DEBIT" THEN
            TT.ACCT.DR = TT.CUR:CATEG:"00":R.NEW( TT.TE.ACCOUNT.2)[9,2]
            OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":TT.ACCT.DR:COMMA
            OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":TT.ACCT.DR:COMMA
            TT.ACCT.CR = TT.CUR:CATEG:"00":R.NEW(TT.TE.ACCOUNT.1)[1,2]
            OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":TT.ACCT.CR:COMMA
            OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":TT.ACCT.CR:COMMA
        END
***
        OFS.MESSAGE.DATA := "ORDERING.BANK=":ID.NEW:COMMA
        DAT = TODAY
        OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DAT:COMMA
        OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":DAT:COMMA
        OFS.MESSAGE.DATA := "COMMISSION.CODE=WAIVE"::COMMA
        OFS.MESSAGE.DATA := "CHARGE.CODE=WAIVE"::COMMA

**************************************************************

        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
        OFS.ID = "T":TNO:".":ID.NEW:".":"INTERBRANCH":"-":DAT

        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E

**************************************************************
    END
    RETURN
END
