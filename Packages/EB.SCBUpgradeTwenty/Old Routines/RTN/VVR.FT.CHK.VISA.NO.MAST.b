* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*****NESSREEN-SCB  09/01/2007************
*-----------------------------------------------------------------------------
* <Rating>188</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CHK.VISA.NO.MAST
*A ROUTINE TO CHK THAT VISA NO IS MASTER AND ITS LENGTH EQ 16 AND IT IS AN EXIST CARD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

****NESSREEN AHMED 24/11/2008*********
    ETEXT = ''

    IF LEN(COMI) # 16 THEN ETEXT = '��� �� ��� �����' ; CALL STORE.END.ERROR
****UPDATED BY NESSREEN AHMED 28/11/2013********************** 
****  IF COMI[1,2] # '52' THEN ETEXT = '��� �� ���� ��� ������ �����' ; CALL STORE.END.ERROR
**UPDATED BY NESSREEN AHMED 24/8/2019 *********************************************************
**      IF (COMI[1,8] # '52571500' AND COMI[1,8] # '52481500') THEN ETEXT = '����� ��� ����� ������� �������' ; CALL STORE.END.ERROR
     IF (COMI[1,6] # '525715' AND COMI[1,6] # '524815' AND COMI[1,6] # '537055' AND COMI[1,6] # '534686') THEN ETEXT = '����� ��� ����� ������� �������' ; CALL STORE.END.ERROR
**END OF UPDATE 24/8/2019***********************************************************************
    ETEXT = ''
*****UPDATED BY NESSREEN AHMED 13/11/2011*****************
*****CARD.NO = R.NEW( FT.LOCAL.REF)<1, FTLR.VISA.NO>******
    IF MESSAGE EQ 'VAL' THEN
        CARD.NO = R.NEW( FT.LOCAL.REF)<1, FTLR.VISA.NO>
    END ELSE
        CARD.NO = COMI
    END
*****END OF UPDATE 13/11/2011*****************************
*--------------------------

    ACC.NO = R.NEW(FT.CREDIT.ACCT.NO)
    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.VISA.APP = '' ; RETRY1 = '' ; E1 = ''
    T.SEL = 'SELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO EQ ':CARD.NO

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF NOT(SELECTED) THEN
        ETEXT = '�� ���� ����� ���� �����'  ; CALL STORE.END.ERROR
    END ELSE
        KEY.TO.USE = KEY.LIST<1>
        CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
        CALL F.READ(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
        VISA.ACC.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT>
****UPDATED BY NESSREEN AHMED 24/10/2011****************************
        R.NEW(FT.CREDIT.ACCT.NO)= VISA.ACC.NO
        CALL REBUILD.SCREEN
**    IF VISA.ACC.NO # ACC.NO THEN
**       ETEXT = '���� ������ ��� ���� ������� � ��� ������'  ; CALL STORE.END.ERROR
**    END
****END OF UPDATE 24/10/2011****************************************
    END
****UPDATED BY NESSREEN AHMED 24/10/2011****************************
    IF MESSAGE = 'VAL' THEN
        DB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)
        ACC.NO = R.NEW(FT.CREDIT.ACCT.NO)
        IF DB.ACCT = ACC.NO THEN
            ETEXT = '���� ����� �� ���� �������'  ; CALL STORE.END.ERROR
        END
    END
****END OF UPDATE 24/10/2011****************************************
    RETURN
END
