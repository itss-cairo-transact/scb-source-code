* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LAST.AMT.BSTO

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BULK.STO

IF AV >  R.NEW(BST.DESCRIPTION) THEN ETEXT = "ERROR IN EXPAND"
ELSE
IF AV = R.NEW(BST.DESCRIPTION) THEN
NO1 = R.NEW(BST.DESCRIPTION)
NO = NO1 - 1
FOR I = 1 TO NO
SMM =SMM +  R.NEW(BST.CURRENT.AMT.BAL)<1,I>
NEXT I
LAST.AMT = R.NEW(BST.TOTAL.DEBIT.AMOUNT) - SMM
IF LAST.AMT <= 0 THEN ETEXT = "ERROR.IN.AMOUNT"
ELSE
R.NEW(BST.CURRENT.AMT.BAL)<1,NO1> = LAST.AMT
CALL REBUILD.SCREEN
END
END 
END
RETURN
END
 
