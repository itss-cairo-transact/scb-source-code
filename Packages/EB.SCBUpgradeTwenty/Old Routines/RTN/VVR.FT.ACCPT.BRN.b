* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*----------------13/02/2010 NESSREEN AHMED-----------------------*
*-----------------------------------------------------------------------------
* <Rating>513</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.ACCPT.BRN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
**    TEXT = 'COMP' ; CALL REM
*******************************************************************************************
    IF MESSAGE = '' THEN

        ETEXT='' ; BRANCH2 =''
        FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT ='' ; R.FT = ''
        FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H ='' ; R.FT.H = ''
        CALL OPF(FN.FT,F.FT)
        CALL OPF(FN.FT.H,F.FT.H)

        IF COMI THEN
            CALL F.READ(FN.FT,COMI, R.FT, F.FT ,E1)
            IF NOT(E1) THEN
                R.NEW(FT.DEBIT.CURRENCY)= R.FT<FT.DEBIT.CURRENCY>
                BR = COMP[8,2]
                CCO = COMP[6,4]
                LOCAL.REF = R.FT<FT.LOCAL.REF>
                RECV.BR = LOCAL.REF<1,FTLR.BRANCH.NO>
                DB.ACCT = R.FT<FT.DEBIT.CURRENCY> :'10000':BR:'99':CCO
                TEXT = 'DB=':DB.ACCT ; CALL REM
                R.NEW(FT.DEBIT.ACCT.NO)= DB.ACCT
                R.NEW(FT.CREDIT.CURRENCY)= R.FT<FT.CREDIT.CURRENCY>
                R.NEW(FT.CREDIT.ACCT.NO) = R.FT<FT.DEBIT.ACCT.NO>
                R.NEW(FT.DEBIT.AMOUNT) = R.FT<FT.DEBIT.AMOUNT>
            END ELSE
                IF E1 THEN
                    FT.H = ''  ;
                    FT.H = COMI:';1'
                    CALL F.READ(FN.FT.H,FT.H, R.FT.H, F.FT.H ,E2)
                    IF NOT(E2) THEN
                        R.NEW(FT.DEBIT.CURRENCY)= R.FT.H<FT.DEBIT.CURRENCY>
                        BR = COMP[8,2]
                        CCO = COMP[6,4]
                        LOCAL.REF = R.FT<FT.LOCAL.REF>
                        DB.ACCT = R.FT.H<FT.DEBIT.CURRENCY> :'10000':BR:'99':CCO
                        R.NEW(FT.DEBIT.ACCT.NO)= DB.ACCT
                        R.NEW(FT.CREDIT.CURRENCY)= R.FT.H<FT.CREDIT.CURRENCY>
                        R.NEW(FT.CREDIT.ACCT.NO) = R.FT.H<FT.DEBIT.ACCT.NO>
                        R.NEW(FT.DEBIT.AMOUNT) = R.FT.H<FT.DEBIT.AMOUNT>
**END ELSE
**    ETEXT = "��� ������� ��� ���" ; CALL STORE.END.ERROR
**END
                    END ELSE
                        ETEXT = '��� �� ��� �������' ; CALL STORE.END.ERROR
                    END       ;*IF NOT(E2)
                END ;*IF E1
            END     ;*END ELSE IF NOT(E1)
        END         ;* IF COMI
        CALL REBUILD.SCREEN
    END   ;* IF MESSAGE

    RETURN
END
