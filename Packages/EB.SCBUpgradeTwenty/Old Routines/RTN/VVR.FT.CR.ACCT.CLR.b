* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CR.ACCT.CLR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    ETEXT = ''

    IF MESSAGE = "VAL" THEN

        COMP       = C$ID.COMPANY
        COM.CODE   = COMP[8,2]
        USR.DEP    = "1700":COM.CODE
        AC         = R.NEW(FT.CREDIT.ACCT.NO)
        AC.1       = '99433300105082':COM.CODE
        AC.2       = '99433300105083':COM.CODE

        IF NOT(AC EQ AC.1 OR AC EQ AC.2 ) THEN
            ETEXT = "������ ��� ����"
        END

        IF COMI = R.NEW(FT.DEBIT.ACCT.NO) THEN
            ETEXT = '���� �� ���� ���� ������� ����� �� ���� �����'
        END ELSE
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,R.NEW(FT.DEBIT.ACCT.NO),DB.CURR)
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CR.CURR)
            IF  DB.CURR # CR.CURR   THEN
                ETEXT = '���� ���� ������� ������ ����� ���� �����'
            END
        END

        CALL REBUILD.SCREEN
    END
    RETURN
END
