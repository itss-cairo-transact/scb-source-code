* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
* --- BAKRY - SCB --- 2017/08/23
*    PROGRAM WORDS.ENGLISH
    SUBROUTINE WORDS.ENGLISH(IN.AMOUNT,OUT.AMOUNT,LINE.LENGTH,NO.OF.LINES,ER.MSG)
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DE.WORDS
*    INPUT IN.AMOUNT ;*   <<-- Input Amount
    INTEGER = FIELD(IN.AMOUNT,'.',1)
    FRACTION = FIELD(IN.AMOUNT,'.',2)
    AMT.INT = ''
    COUNTRY.CODE = 'GB'
    LENGTH = ''
    ER = ''
    CALL DE.O.PRINT.WORDS(INTEGER,AMT.INT,COUNTRY.CODE,LENGTH,1,ER)
    CONVERT '*' TO ' ' IN AMT.INT
    IF FRACTION THEN
 *       OUT.AMOUNT = AMT.INT:' ':FRACTION:'/100':' ONLY'
      OUT.AMOUNT = 'ONLY ':AMT.INT:' ':FRACTION:'/100'
END
    ELSE
        OUT.AMOUNT = 'ONLY ':AMT.INT
    END
END
