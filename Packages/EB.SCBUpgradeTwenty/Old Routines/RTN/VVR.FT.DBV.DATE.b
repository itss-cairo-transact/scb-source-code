* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
**---------NESSREEN AHMED 12/10/2009---------**
    SUBROUTINE VVR.FT.DBV.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.DATE

    ETEXT = ''
    DB.ACCT = '' ; WB = '' ; T.DAT = '' ; DD.DAT = '' ; S.DAT = '' ; CATEG = ''
    CHK.BAL = ''  ; KEY.ID = ''
    DAT.FR = '' ; DAT.TO  = '' ; SAL.DAT = '' ; DB.DAT.FR = '' ; DB.DAT.TO = ''

    DB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)
    FN.ACCT = 'F.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; RETRY = '' ; E1 = ''
    CALL OPF(FN.ACCT,F.ACCT)
    CALL F.READ(FN.ACCT, DB.ACCT, R.ACCT, F.ACCT, E3)
    CATEG = R.ACCT<AC.CATEGORY>
********************************************
    PR.DATE = R.NEW(FT.PROCESSING.DATE)
    T.DAT = TODAY
    DD.DAT = T.DAT[1,6]
  **  S.DAT = DD.DAT:26
    IF CATEG EQ '1002' THEN
        TEXT = '1002' ; CALL REM
        DB.AMT = R.NEW(FT.DEBIT.AMOUNT)
        W.B.D = R.ACCT<AC.WORKING.BALANCE>
        ABS.WB = ABS(W.B.D)
        CHK.BAL = DB.AMT + W.B.D
*****UPDATED BY NESSREEN AHMED 06/12/2009**********
**      DB.DAT = PR.DATE[7,2]
        FN.PDT = 'F.SCB.VISA.PAY.DATE' ; F.PDT = '' ; R.PDT = '' ; RETRY.PDT = '' ; E.PDT = ''
        CALL OPF(FN.PDT,F.PDT)
        KEY.ID = DD.DAT
       ** TEXT = 'KEY.ID=':KEY.ID ; CALL REM
        CALL F.READ(FN.PDT, KEY.ID, R.PDT, F.PDT, E.PDT)
        DAT.FR  = R.PDT<VPD.DATE.FROM>
        DAT.TO  = R.PDT<VPD.DATE.TO>
        SAL.DAT = R.PDT<VPD.SAL.DATE>
        **TEXT = 'DAT.FR=':DAT.FR ; CALL REM
        **TEXT = 'DAT.TO=':DAT.TO ; CALL REM
        **TEXT = 'SAL.DAT= ':SAL.DAT ; CALL REM
        DB.DAT.FR  = DAT.FR[7,2]
      **  TEXT = 'DB.DAT.FR=':DB.DAT.FR ; CALL REM
        DB.DAT.TO = DAT.TO[7,2]
        DB.DAT.N = TODAY[7,2]
**        IF DB.DAT < '21' OR DB.DAT > '22' THEN

        IF (DB.DAT.N < DB.DAT.FR) OR (DB.DAT.N > DB.DAT.TO) THEN
            IF DB.AMT > CHK.BAL THEN
                TEXT = 'OVER' ; CALL REM
                ETEXT = '��� ����� ������� �� ��� ������' ; CALL STORE.END.ERROR
 ****UPDATED BY NESSREEN AHMED 27/4/2010******************
          ***  END
            END ELSE
               R.NEW(FT.DEBIT.VALUE.DATE) = TODAY
               CALL REBUILD.SCREEN
*****END OF UPDATED 27/4/2010********************************
            END
        END ELSE
            IF V$FUNCTION NE 'R' THEN
********UPDATE BY NESSREEN AHMED 6/12/2009******************
**    CALL CDT('',S.DAT,"-1W")
**    R.NEW(FT.DEBIT.VALUE.DATE) = S.DAT
                 TEXT = 'SD=':SAL.DAT ; CALL REM
                R.NEW(FT.DEBIT.VALUE.DATE) = SAL.DAT
                CALL REBUILD.SCREEN
            END

        END         ;* IF DB.DAT < '21' OR DB.DAT > '22'
    END   ;* IF CATEG EQ '1002'
    RETURN
END
