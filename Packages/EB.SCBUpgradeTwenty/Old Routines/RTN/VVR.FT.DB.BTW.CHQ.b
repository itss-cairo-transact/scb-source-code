* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***----INGY----***
*-----------------------------------------------------------------------------
* <Rating>90</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.DB.BTW.CHQ

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

*CHECK IF CREDIT.ACCT # INTERNAL ACCOUNT


    IF V$FUNCTION = 'I' THEN
        ACC = COMI
*        TEXT = "1" :ACC ; CALL REM
        IF ACC[1,2] NE 'PL' THEN
            CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
**** TEXT = "ERR" : POST ; CALL REM
            CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
            IF POST # "" THEN
                ETEXT = DESC ;CALL STORE.END.ERROR
            END ELSE
                IF COMI # R.NEW(FT.DEBIT.ACCT.NO) THEN
*  R.NEW(FT.CREDIT.CUSTOMER) = ''
*R.NEW(FT.CREDIT.ACCT.NO) = ''
*  R.NEW(FT.CREDIT.CURRENCY)  = ''
                END
*IF NUM(COMI[1,3]) THEN
                CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
                IF (CATEG >= 9000 AND CATEG <= 9999)  THEN ETEXT = '��� ����� ��������� ��������'
*   R.NEW(FT.CHARGES.ACCT.NO)= COMI[1,8]:10:COMI[11,6]
*   END ELSE
*      ETEXT = '��� ����� ��������� ����� �����'
* END
                CALL REBUILD.SCREEN
            END
        END


******************************MODIFIED ON 22/3/2011
        IF COMI[1,2] NE 'PL' THEN
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,MYCATEG)
            IF MYCATEG GE 6501 AND MYCATEG LE 6504 THEN
                ETEXT = "SAVING NOT ALLOWED" ;CALL STORE.END.ERROR
            END
        END
*****************************

    END
    RETURN
END
