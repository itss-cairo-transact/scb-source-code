* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CR.DRFT.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*-----------------------------------------------
    IF MESSAGE # 'VAL' THEN
        IF V$FUNCTION = "I" THEN

            FN.CHQ.PRES = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRES = ''
            R.CHQ.PRES  = ''
            CALL OPF(FN.CHQ.PRES,F.CHQ.PRES)

**          T.SEL    = "SELECT F.SCB.FT.DR.CHQ CHEQ.NO WITH @ID LIKE ":COMI:"... "
            T.SEL    = "SELECT F.SCB.FT.DR.CHQ CHEQ.NO WITH COMPANY.CO"
            T.SEL   := " EQ ":ID.COMPANY
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

            IF SELECTED THEN
                CHEQ.NO = MAXIMUM(KEY.LIST) + 1
                R.NEW(FT.CHEQUE.NUMBER)               = CHEQ.NO
                R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = CHEQ.NO
            END ELSE
                R.NEW(FT.CHEQUE.NUMBER) = '1'
                R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = CHEQ.NO

            END

            VER.NAM = R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>
            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
            CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)

            IF CURR # LCCY  AND VER.NAM = ",SCB.DRFT.ABO" THEN
                ETEXT = 'ONLY.LOCAL.CURR'
            END
            ACTT = COMI[4,5]

            IF ACTT NE 16151 THEN
                ETEXT = "Categ.Not.Allowed"
            END
        END
        CALL REBUILD.SCREEN
    END
*---------------------------------------------------------
    RETURN
END
