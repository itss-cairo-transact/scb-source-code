* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>540</Rating>
*-----------------------------------------------------------------------------
**********DINA***********

    SUBROUTINE VVR.FT.DRAFT.EGP

*If we enter a no in the field then the routine check if the account has a valid chq book in the cheque.register
*Table. and make sure that the entered no is withen the range and in the right turn

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF V$FUNCTION = "I" THEN

        T.SEL = "SELECT FBNK.CHEQUE.REGISTER WITH @ID LIKE  ": '...':R.NEW(FT.CREDIT.ACCT.NO)
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF KEY.LIST THEN
            F.CHQ = '' ; FN.CHQ = 'FBNK.CHEQUE.REGISTER'
            CALL OPF(FN.CHQ,F.CHQ)
            R.CHQ = ''
            FOR I = 1 TO SELECTED
                CALL F.READ( FN.CHQ, KEY.LIST<I>, R.CHQ, F.CHQ, READ.ERROR)
                CHQ.NUM = R.CHQ<CHEQUE.REG.CHEQUE.NOS>
                X = FIELD(CHQ.NUM,"-",1)
                Y = FIELD(CHQ.NUM,"-",2)
                IF COMI < X OR COMI > Y THEN ETEXT = 'OUT OF RANGE'
                IF  R.CHQ<CHEQUE.REG.STOPPED.CHQS> = COMI  OR R.CHQ<CHEQUE.REG.PRESENTED.CHQS> = COMI THEN ETEXT= 'WRONG' ELSE ER =""
            NEXT I
        END ELSE
            ETEXT = 'CHEQ.NUM.NOT.FOUND'
        END
        CLOSE F.CHQ
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = '...':R.NEW(FT.CREDIT.ACCT.NO):'-':COMI
    CHQ.ID = '...':R.NEW(FT.CREDIT.ACCT.NO):'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED,CHQ.ID,PRESENT)
 CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED,CHQ.ID,PRESENT)
****END OF UPDATE 7/3/2016*****************************
* IF ETEXT THEN ETEXT = '' ELSE ETEXT = 'WRONG.PRESENTED'
        IF PRESENT THEN ETEXT = '�� ���� �� ���'
    END
*  T.SEL = "SELECT FBNK.CHEQUES.PRESENTED WITH @ID  LIKE ": '...':R.NEW(FT.CREDIT.ACCT.NO):"..."

*  CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*  CHQ.NO = 0
* IF KEY.LIST THEN
*    FOR I = 1 TO SELECTED
*       CURRENT = FIELD(KEY.LIST<I>,"-",2)
*       IF CURRENT >= CHQ.NO THEN CHQ.NO = CURRENT +1
*  NEXT I

* IF COMI # CHQ.NO  THEN ETEXT = 'NUM MUST BE ' :CHQ.NO

* END


    RETURN
END
