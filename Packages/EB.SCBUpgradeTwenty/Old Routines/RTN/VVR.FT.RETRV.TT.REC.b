* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>500</Rating>
*-----------------------------------------------------------------------------
***----NESSREEN AHMED 12/09/2006----***

    SUBROUTINE VVR.FT.RETRV.TT.REC

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    IF MESSAGE = '' THEN
        TT.REF = COMI:'...'
        TEXT = 'TT.REF=':TT.REF ; CALL REM
        T.SEL =  "SELECT FBNK.TELLER$HIS WITH @ID LIKE ":TT.REF
        KEY.LIST=""
        SELECTED=""
        ER.MSG=""

        FN.TT = 'F.TELLER$HIS' ; F.TT = '' ; R.TT = '' ; RETRY1 = '' ; E1 = ''

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        TEXT = 'SELECTED=':SELECTED  ; CALL REM
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                KEY.TO.USE = KEY.LIST<I>
                CALL OPF(FN.TT,F.TT)
                CALL F.READU(FN.TT,  KEY.TO.USE, R.TT, F.TT, E1, RETRY1)
                TRANS.CO = R.TT<TT.TE.TRANSACTION.CODE>
                IF TRANS.CO # 4 THEN ETEXT = 'Should.Be.Cash.Transite.Tran'
                DB.ACCT  = R.TT<TT.TE.ACCOUNT.2>
                TEXT = 'DB.ACCT=':DB.ACCT ; CALL REM
                DB.CURR  = R.TT<TT.TE.CURRENCY.1>
                TEXT = 'DB.CURR=':DB.CURR ; CALL REM
                IF DB.CURR = LCCY THEN
                    DB.AMT   = R.TT<TT.TE.AMOUNT.LOCAL.1>
                END ELSE
                    DB.AMT   = R.TT<TT.TE.AMOUNT.FCY.1>
                END
                TEXT = 'DB.AMT=':DB.AMT ; CALL REM
                CR.ACCT  = R.TT<TT.TE.ACCOUNT.1>
                TEXT = 'CR.ACCT=':CR.ACCT ; CALL REM
                DEPT.COD = R.TT<TT.TE.DEPT.CODE>
                TEXT = 'DEP=':DEPT.COD ; CALL REM
                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME, DEPT.COD , BRANCH)
                TR.BR = FIELD(BRANCH,'.',2)
            NEXT I

            R.NEW(FT.DEBIT.ACCT.NO)   = DB.ACCT
            R.NEW(FT.DEBIT.CURRENCY)  = DB.CURR
            R.NEW(FT.DEBIT.AMOUNT)    = DB.AMT
            R.NEW(FT.CREDIT.ACCT.NO)  = CR.ACCT
            R.NEW(FT.DEBIT.VALUE.DATE)= TODAY
            R.NEW(FT.ORDERING.BANK)   = TR.BR

            CALL REBUILD.SCREEN
**********************************************
        END
    END
    RETURN
END
