* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>799</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CURR2.EGP
*If account with foriegn currency error message will appear
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF V$FUNCTION = "I" THEN
**********OLD************
*IF NUM(COMI) THEN ETEXT = 'MUST.BE.INTERNAL.ACCOUNT'
*ELSE
*************************
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
        IF CURR # LCCY AND PGM.VERSION = ",SCB.DRACHQEGP2" THEN ETEXT = 'ONLY.LOCAL.CURR'
        ELSE  IF CURR =  LCCY AND PGM.VERSION = ",SCB.DRACHQFCY2" THEN ETEXT = 'ONLY.FORIEGN.CURRENCY'
        IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "1 DRAFT- ��� ����� ��������" THEN
            IF CATEG # 5101 THEN ETEXT = "Categ.Not.Allowed"
            END ELSE  IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "4 CHEQUE-��� � �����" THEN
                IF CATEG # 5102 THEN ETEXT = "Categ.Not.Allowed"
                END ELSE  IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "5 ORDER.PAY -��� �����" THEN
                    IF CATEG # 2105 THEN ETEXT = "Categ.Not.Allowed"

                END
                RETURN
            END
