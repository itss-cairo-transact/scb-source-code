* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

    SUBROUTINE VVR.LC.APPLICANT.NAME
* A ROUTINE TO DEFAULT THE FIRST MULTIVALUE WITH THE CUSTOMER NAME

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4  ; CALL REM
    END
    IF MESSAGE # 'VAL' THEN
        IF V$FUNCTION='I' THEN
            IF COMI THEN
                FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
                CALL OPF( FN.CU,F.CU)
                CALL F.READ( FN.CU, COMI, R.CU, F.CU, ETEXT)
                IF NOT(ETEXT) THEN
                    CU.NAME   = R.CU<EB.CUS.NAME.1>
                    CU.STREET = R.CU<EB.CUS.STREET>
                    CU.TOWN   = R.CU<EB.CUS.TOWN.COUNTRY>
                END
                CLOSE F.CU
************************

                R.NEW(TF.LC.APPLICANT)<1,1> = CU.NAME
                R.NEW(TF.LC.APPLICANT)<1,2> = CU.STREET
                R.NEW(TF.LC.APPLICANT)<1,3> = CU.TOWN

                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
