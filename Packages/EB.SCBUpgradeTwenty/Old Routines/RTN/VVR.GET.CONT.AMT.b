* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.GET.CONT.AMT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MORTG.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    COMP = ID.COMPANY

    FN.MOC = 'F.SCB.MORTG.CUSTOMER' ; F.MOC = ''
    CALL OPF(FN.MOC,F.MOC)

    WS.TODAY     = TODAY
    WS.DATE      = COMI
    WS.CODE      = R.NEW(MOC.MORTGAGES.CODE)<1,AV>
    WS.END.MORTG = R.NEW(MOC.MORTG.END.DATE)<1,AV>
    WS.AMT.VAL   = R.NEW(MOC.LAST.MORTG.VAL)<1,AV>
    WS.AMT       = R.NEW(MOC.MORTGAGES.AMT)<1,AV>

    WS.YY        = WS.TODAY[1,4]
    WS.MM.DD     = WS.TODAY[5,4]
    WS.YY.3      = WS.YY - 3


    WS.CHK.DATE = WS.YY.3 : WS.MM.DD

    IF WS.END.MORTG GT TODAY THEN
* IF WS.CODE EQ 1 THEN
*     WS.CONT.AMT = WS.AMT.VAL * 0.65
* END

        IF WS.CODE EQ 1 THEN
            WS.CONT.AMT = WS.AMT.VAL * 0.25
            IF WS.CONT.AMT GT WS.AMT THEN
                WS.CONT.AMT = WS.AMT
            END
        END

        IF WS.CODE EQ 2 THEN
            WS.CONT.AMT = WS.AMT.VAL * 0.50
            IF WS.CONT.AMT GT WS.AMT THEN
                WS.CONT.AMT = WS.AMT
            END
        END

        IF WS.CODE EQ 3 THEN
            WS.CONT.AMT = WS.AMT.VAL * 0.38
            IF WS.CONT.AMT GT WS.AMT THEN
                WS.CONT.AMT = WS.AMT
            END
        END

        R.NEW(MOC.CONTRACT.AMT)<1,AV> = WS.CONT.AMT

        CALL REBUILD.SCREEN
    END
  *  IF WS.DATE LE WS.CHK.DATE THEN
  *      R.NEW(MOC.CONTRACT.AMT)<1,AV> = 0
  *      CALL REBUILD.SCREEN
  *  END

    WS.DATE.YY =  WS.DATE[1,4] + 3
    WS.DATE.MD =  WS.DATE[5,4]


    R.NEW(MOC.EXP.MORTG.DATE)<1,AV> = WS.DATE.YY : WS.DATE.MD

    CALL REBUILD.SCREEN

    RETURN
END
