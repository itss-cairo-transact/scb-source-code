* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
******** UPDATED BY INGY_SCB**********
*-----------------------------------------------------------------------------
* <Rating>500</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.CHECK.CUR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

IF COMI THEN


    CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, COMI ,CR.CATEG)
    IF ETEXT THEN E = ETEXT

    CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, COMI ,CR.CURR)
    IF ETEXT THEN E = ETEXT

    IF CR.CATEG < 5000 OR CR.CATEG > 5999 THEN ETEXT = 'Only.Nostro.ACCOUNT' ; RETURN


   IF R.NEW(FT.DEBIT.ACCT.NO) THEN

         CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY,R.NEW(FT.DEBIT.ACCT.NO) ,DB.CURR)
         IF ETEXT THEN E = ETEXT

         IF CR.CURR # DB.CURR THEN ETEXT = 'SHOULD.BE.EQ.DB.CURR'; RETURN

   END

END


RETURN
END
