* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE

*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.EMPTY.CREDIT.CUST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*TO EMPTY CREDIT.CURRENCY,FT.CREDIT.ACCT.NO,FT.CREDIT.AMOUNT,FT.CREDIT.THEIR.REF AND ASSIGN USER BRANCH 

    IF COMI # R.NEW(FT.CREDIT.CUSTOMER) THEN
        R.NEW(FT.CREDIT.CUSTOMER)=''
        R.NEW(FT.CREDIT.ACCT.NO) = ''
        R.NEW(FT.CREDIT.AMOUNT)=''
        R.NEW(FT.CREDIT.THEIR.REF)=''
        R.NEW(FT.PROFIT.CENTRE.DEPT)=''

        CUSLEN=LEN(COMI)
        IF CUSLEN = 7 THEN
            BRN = COMI[1,1]
        END ELSE
            BRN = COMI[1,2]
        END
        R.NEW(FT.PROFIT.CENTRE.DEPT)=BRN
        CALL REBUILD.SCREEN
    END



    RETURN
END
