* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>683</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CHQ.PRES

*TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 8/3/2016**************************
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
****UPDATED BY NESSREEN AHMED 19/8/2017*************
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP.TYPE
****END OF UPDATE 19/8/2017*************************
**--------------------------------------------


    CUS.ID  = R.NEW(FT.DEBIT.CUSTOMER)
    CHQ.NO  = R.NEW(FT.CHEQUE.NUMBER)
    IF R.NEW(FT.CHEQUE.NUMBER) EQ ''  THEN CHQ.NO = COMI
    FLAG    = 0

    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;     R.CUS.AC = '' ;  F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    FN.CHQ.AC ='FBNK.CHEQUE.ISSUE.ACCOUNT' ; R.CHQ.AC = '' ;  F.CHQ.AC=''
    CALL OPF(FN.CHQ.AC,F.CHQ.AC)

    FN.CHQ.REG ='FBNK.CHEQUE.REGISTER' ; R.CHQ.REG = ''    ;  F.CHQ.REG   =''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)

*****UPDATED BY NESSREEN AHMED 19/8/2017********************
    FN.CHQ.REG.SUP ='FBNK.CHEQUE.REGISTER.SUPPLEMENT' ; R.CHQ.REG.SUP = ''    ;  F.CHQ.REG.SUP   =''
    CALL OPF(FN.CHQ.REG.SUP,F.CHQ.REG.SUP)
*****END OF UPDATE 19/8/2017********************************

*==========a)To Check IF Cheq Is Issued ===============================================
    T.SEL ="SELECT FBNK.CHEQUE.REGISTER WITH CUSTOMER EQ ": CUS.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CHQ.REG,KEY.LIST<I>, R.CHQ.REG, F.CHQ.REG , ERR.CHQ)
            CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>

*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT(CHQ.NOS,@VM)
            FOR X = 1 TO DD
                CHN  = CHQ.NOS<1,X>
                SS = COUNT(CHN, "-")
                IF SS > 0 THEN
                    ST.NO   = FIELD(CHN ,"-", 1)
                    ED.NO   = FIELD(CHN ,"-", 2)
                END ELSE
                    ST.NO   = CHN
                    ED.NO   = CHN
                END
                IF (CHQ.NO GE ST.NO) AND (CHQ.NO LE ED.NO) THEN
                    FLAG = "1"
                END ELSE
                END
            NEXT X
        NEXT I
    END

    ETEXT   = ''
    IF FLAG = "0" THEN
        ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
    END
    ETEXT = ''

**-------------------------
    IF COMI  THEN
        ACCT.NO    = R.NEW(FT.DEBIT.ACCT.NO)
************UPDATED BY RIHAM R15***************

*        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN1)
        CUS.BR = MYBRN1[8,2]

        MYBRN = TRIM(CUS.BR, "0" , "L")
*****************************************************
        PAY.CHQ.ID = COMI:'.':MYBRN
        CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCN)

        IF MYACCN THEN
            ETEXT='��� ����� �� ����';CALL STORE.END.ERROR
        END ELSE
            CHQ.NO = TRIM(COMI, "0" , "L")
**Updated by Nessreen Ahmed 5/9/2016 For R15*****
****UPDATED BY NESSREEN AHMED 8/3/2016 for R15****
****        CHQ.ID.STOP = ACCT.NO:"*": CHQ.NO
****        CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY, CHQ.ID.STOP, CURR)
***         CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.AC)
            KEY.ID = "...":ACCT.NO:".":CHQ.NO
            N.SEL = "SELECT FBNK.CHEQUE.REGISTER.SUPPLEMENT WITH @ID LIKE ": KEY.ID :" AND STATUS EQ STOPPED "
            KEY.LIST.N=""
            SELECTED.N=""
            ER.MSG.N=""
            CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
           IF SELECTED.N THEN


**    IF CURR THEN
**End of Update 5/9/2016**********
****UPDATED BY NESSREEN AHMED 19/8/2017*******************
***ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
                CALL F.READ( FN.CHQ.REG.SUP,KEY.LIST.N, R.CHQ.REG.SUP, F.CHQ.REG.SUP,ERR.SUP)


               STOP.TYPE = R.CHQ.REG.SUP<CC.CRS.PAYM.STOP.TYPE>
                CALL DBR ('PAYMENT.STOP.TYPE':@FM:AC.PAT.DESCRIPTION,STOP.TYPE,STOP.DESC)
                ETEXT = "����� ��� (&) ����� ":@FM:COMI :' * ':STOP.DESC
                CALL STORE.END.ERROR
****END OF UPDATE  19/8/2017*******************
            END ELSE
                KEY.LIST.DR = ""  ;  ER.MSG = ""  ; SELECTED.DR = ""
                T.SEL.DR = "SELECT F.SCB.FT.DR.CHQ WITH  CHEQ.TYPE LIKE ...CHE... AND CHEQ.NO EQ ":COMI:" OR OLD.CHEQUE.NO EQ ": COMI
                CALL EB.READLIST(T.SEL.DR,KEY.LIST.DR,"",SELECTED.DR,ER.MSG)
                IF SELECTED.DR THEN
                    FN.CHQ.PRES = "F.SCB.FT.DR.CHQ"  ; F.CHQ.PRES = ""
                    CALL OPF(FN.CHQ.PRES, F.CHQ.PRES)
                    CALL F.READ(FN.CHQ.PRES,KEY.LIST.DR<1>, R.CHQ.REG, F.CHQ.REG , CHQ.ERR)
*--- EDIT BY NESSMA 2015/1/11
                    CHQ.STAT = R.CHQ.REG<DR.CHQ.CHEQ.STATUS>
                    TEXT = "CHEQ.STAT = ":CHQ.STAT ; CALL REM
                    IF CHQ.STAT NE 3 THEN
                        ETEXT= "���� ��� ����� ����� ���� �����"  ; CALL STORE.END.ERR
                    END
                END
                F.COUNT   = '' ; FN.COUNT = 'F.AC.LOCKED.EVENTS'
                CALL OPF(FN.COUNT,F.COUNT)
                LOCAL.REF = FN.COUNT<AC.LCK.LOCAL.REF>
                CHQ.NO    = LOCAL.REF<1,AC.LCK.LOC.CHQ.NO>

                T.SEL = "SELECT FBNK.AC.LOCKED.EVENTS WITH CHQ.NO EQ ":COMI:" AND ACCOUNT.NUMBER EQ ":ACCT.NO
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                IF SELECTED THEN
                    ETEXT = "��� ����� ���� ����" ;CALL STORE.END.ERROR
                END
            END
        END
    END
*-----------------------------------------
    OLD.CHQ.NO = COMI
    IF MESSAGE EQ 'VAL' THEN
        CALL VAR.FT.OFS
        COMI = OLD.CHQ.NO
        R.NEW(FT.CHEQUE.NUMBER) = COMI
        CALL REBUILD.SCREEN
    END

    ETEXT = ''
    R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = COMI
    CALL REBUILD.SCREEN
    RETURN
END
