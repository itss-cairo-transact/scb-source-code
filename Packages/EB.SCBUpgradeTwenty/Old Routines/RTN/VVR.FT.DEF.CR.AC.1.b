* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
***----INGY14/03/2005----***
*****EDITED BY MAI SAAD 18/02/2018
    SUBROUTINE VVR.FT.DEF.CR.AC.1
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*To Default Credit account by 994999:Brn.no:Currency:510105
    IF V$FUNCTION = "I" THEN
        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(FT.CREDIT.CURRENCY),CUR)
        CURR = R.NEW(FT.CREDIT.CURRENCY)
        BRN.NO = COMI
*R.NEW(FT.CREDIT.ACCT.NO)= '994999':BRN.NO:CUR:'510101'
***UPDATED BY NESSREEN AHMED 25/5/2008**********
*         R.NEW(FT.CREDIT.ACCT.NO)= CURR:'161510001'
***UPODATED BY NESSREN AHMED 07/07/2008*********
**        R.NEW(FT.CREDIT.ACCT.NO)= CURR:'1615100010001'
        CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        R.NEW(FT.CREDIT.ACCT.NO)= CURR:'161510001':CO.CODE
*****************UPDATED BY MAI SAAD 08/02/2018************
        COMP = C$ID.COMPANY
        COM.CODE      = COMP[8,2]
        IF COM.CODE[1,1]  EQ '0' THEN
            COM.CODE  = COMP[9,1]
        END ELSE
            COM.CODE  = COMP[8,2]
        END

        DEBIT.ACC = R.NEW(FT.DEBIT.ACCT.NO)
        IF (DEBIT.ACC[1,2] EQ 'PL') THEN
            PL.CAT = DEBIT.ACC[3,5]
            IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                IF (COMP NE 'EG0010099') THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
                END ELSE
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                        TEXT ='��� ����� ��� �������' C;CALL REM
                    END
                END
            END ELSE
                IF (COMP NE 'EG0010099') THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END ELSE
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '99'
                    END
                END
            END
        END ELSE
            IF (COMP EQ 'EG0010099') THEN
                IF(R.NEW(FT.PROFIT.CENTRE.DEPT) = '') THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END
            END ELSE
                R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
            END
        END
***********************************
        CALL REBUILD.SCREEN
    END
    RETURN
END
