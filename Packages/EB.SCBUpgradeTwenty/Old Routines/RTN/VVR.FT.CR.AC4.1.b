* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
***-----INGY----***

    SUBROUTINE VVR.FT.CR.AC4.1


*1-DEBIT ACCOUNT MUST BE DEFEREANT CREDIT ACCOUNT
*2-INTERNAL ACCOUNT IS NOT ALLOWED
*3-CURRENCY IN DEBIT ACCOUNT MUST BE EQ CURRENCY IN CREDIT ACCOUNT



*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF MESSAGE = "VAL" THEN

        IF COMI = R.NEW(FT.DEBIT.ACCT.NO) THEN
            ETEXT = '���� �� ���� ���� ������� ����� �� ���� �����'
        END ELSE
* IF NOT(NUM(COMI)) THEN
* ETEXT = '��� ����� ��������� ����� �����'
*END ELSE
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
*            IF (CATEG >= 5000 AND CATEG <= 5999) OR (CATEG >= 2000 AND CATEG <= 2999) THEN ETEXT = '�������� ������� �������� � ��������'
*           ELSE
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,R.NEW(FT.DEBIT.ACCT.NO),DB.CURR)
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CR.CURR)
            IF  DB.CURR # CR.CURR   THEN
                ETEXT = '���� ���� ������� ������ ����� ���� �����'
            END
*          END
        END
* END
        CALL REBUILD.SCREEN
    END
    RETURN
END
