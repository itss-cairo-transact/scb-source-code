* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.FT.LG.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

*Line [ 27 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    REC.COUNT   = DCOUNT(R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED>,@SM)
    FOR I = 2  TO REC.COUNT
        IF R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED,I> NE '0' THEN
            AMT.DAT = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED,I>
            LG.COM=FIELD(AMT.DAT,'-',1)
            TEXT=LG.COM:'-LG.COM';CALL REM
            LG.COM.ALL += LG.COM
            TEXT=LG.COM.ALL:'-EACH';CALL REM
        END
    NEXT I
    TEXT=LG.COM.ALL:'-ALL';CALL REM
    R.NEW(FT.DEBIT.AMOUNT) = LG.COM.ALL
    IF MESSAGE EQ 'VAL' THEN
        IF R.NEW(FT.DEBIT.AMOUNT)  NE LG.COM.ALL THEN
E='WRONG DEBIT AMOUNT';CALL STORE.END.ERROR
        END
    END
    CALL REBUILD.SCREEN
    RETURN
END
