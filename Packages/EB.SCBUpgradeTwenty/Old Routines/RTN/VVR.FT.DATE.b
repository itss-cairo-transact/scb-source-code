* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
**---------INGY 18/04/2002---------**

    SUBROUTINE VVR.FT.DATE

*1-CREDIT VALUE DATE MUST EQ DEBIT VALUE DATE
*2-NOT ALLOWED THE VALUE DATE LESS THAN TODAY


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*-----------------------------------------
    ETEXT = ''
    IF COMI THEN
        DB.ACCT = '' ; WB = ''
        DB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)
        IF DB.ACCT[11,4] EQ '1002' THEN
*            TEXT = '1002' ; CALL REM
            DB.AMT = R.NEW(FT.DEBIT.AMOUNT)
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE, DB.ACCT, WB)
            ABS.WB = ABS(WB)
            IF DB.AMT > ABS.WB THEN
                DB.DAT = COMI[7,2]
                ETEXT = "������ �� ����" ; CALL STORE.END.ERROR
*    IF DB.DAT < '21' OR DB.DAT > '22' THEN
*        ETEXT = '��� ����� ������� �� ��� ����� �' ; CALL STORE.END.ERROR
*    END
            END
        END
    END
    RETURN
END
