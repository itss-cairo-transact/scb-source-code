* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>886</Rating>
*-----------------------------------------------------------------------------
*** DINA & INGY*****

SUBROUTINE VVR.FT.OUTFOREIGN
*a routine to make sure :
                      * that the debit account is not a vostro or nostro account
                      * it is not a bank sector
                      * if the debit account is customer then default commission.code & charge.code with debit plus charges
                      * else default commission.code & charge.code with waive
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.TYPE.CATEG
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

IF V$FUNCTION = 'I' THEN
IF MESSAGE # 'VAL' THEN
IF COMI THEN
 *CALL DBR("ACCOUNT":@FM:AC.CURRENCY,COMI,CURR)
 *IF CURR = LCCY THEN ETEXT = 'Only.Foreign.Currency'
 *ELSE
 IF NUM(COMI) THEN
 IF R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> THEN ETEXT = "Must.Be.Internal.Acct"
 ELSE
   CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)

  IF (CATEG >= 2000 AND CATEG <= 2999  OR  CATEG >= 5000 AND CATEG <= 5999) THEN ETEXT = 'Not.Allowed.For.Nostro.Or.Vostro'
  ELSE
    CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,COMI,CUS)
    CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, CUS, SEC )
  * CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, TRIM(COMI[5,7],"0","L"), SEC )
  IF (SEC >= 3000 AND SEC <= 3999) THEN ETEXT = 'Not.Allowed.For.Bank'
  ELSE
  CALL DBR( 'CUSTOMER':@FM:EB.CUS.NAME.1,CUS, CUS.NAME )
  *CALL DBR( 'CUSTOMER':@FM:EB.CUS.NAME.1,TRIM(COMI[5,7],"0","L"), CUS.NAME )
  R.NEW(FT.COMMISSION.CODE) = "DEBIT PLUS CHARGES"
  R.NEW(FT.CHARGE.CODE) = "DEBIT PLUS CHARGES"
  R.NEW(FT.ORDERING.CUST)= CUS.NAME
  R.NEW(FT.CHARGES.ACCT.NO)= COMI
  CALL REBUILD.SCREEN
  END
  END
  END
END ELSE
  *CALL DBR ('SCB.CHQ.TYPE.CATEG':@FM:CC.CATEGORY,11,ACCT.CATEG)
  DEP = R.USER<EB.USE.DEPARTMENT.CODE>
  DEP = STR('0', 2- LEN(DEP) ):DEP
  ACCT = R.NEW(FT.DEBIT.CURRENCY):'17303':DEP:'01'
  IF COMI # ACCT THEN ETEXT = 'This.Account.Is.Not.Allowed'
  ELSE
  R.NEW(FT.COMMISSION.CODE) = "WAIVE"
  R.NEW(FT.CHARGE.CODE) = "WAIVE"
  R.NEW(FT.CHARGES.ACCT.NO)= ''
  R.NEW(FT.ORDERING.CUST)= ''
  CALL REBUILD.SCREEN
  END
  END

END
END
END
RETURN
END
