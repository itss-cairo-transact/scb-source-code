* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
***----INGY----***

    SUBROUTINE VVR.FT.CUBR.CR

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 ; CALL REM
    END


    IF V$FUNCTION = "I" THEN
        IF COMI # R.NEW(FT.CREDIT.CUSTOMER) THEN
            R.NEW(FT.CREDIT.CURRENCY) = ''
            R.NEW(FT.CREDIT.ACCT.NO) = ''

        END
*IF COMI THEN
*CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI,SEC)
*IF SEC # '3400' THEN
*    ETEXT = '������ ���� �� ���� ��� ���� ����'
*END
*END
    END

    RETURN
END
