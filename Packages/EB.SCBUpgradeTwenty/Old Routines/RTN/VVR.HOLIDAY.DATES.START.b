* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.HOLIDAY.DATES.START

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMPLOYEE.HOLIDAY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    FN.EH = 'F.SCB.EMPLOYEE.HOLIDAY' ; F.EH = ''
    CALL OPF(FN.EH,F.EH)

    DATE.FROM = COMI
    WS.YEAR   = R.NEW(EH.YEAR)
    WS.TYPE   = R.NEW(EH.TYPE)<1,AV>
*****************
*Line [ 36 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    N.COUNT = DCOUNT(R.NEW(EH.START.DATE),@VM)
    FOR I = 1 TO N.COUNT
        WS.DATE.FROM = R.NEW(EH.START.DATE)<1,I>
        WS.DATE.TO   = R.NEW(EH.END.DATE)<1,I>
        IF I NE AV THEN
            IF COMI GE WS.DATE.FROM AND COMI LE WS.DATE.TO THEN
                ETEXT = '���� ����� �� ���� ��������'
                CALL STORE.END.ERROR
            END
        END
    NEXT I
*****************
    CUR = 'EG00'
    CALL AWD(CUR,DATE.FROM,AA)
    IF AA EQ 'H' THEN
        ETEXT = '������� ���� �� ������ ����� �����'
        CALL STORE.END.ERROR
    END ELSE
        IF DATE.FROM[1,4] NE WS.YEAR THEN
            ETEXT = '������� ��� ����� ���� �����'
            CALL STORE.END.ERROR
        END
        IF MESSAGE NE 'VAL' THEN
            R.NEW(EH.INPUT.DATE)<1,AV> = TODAY
        END
        CALL REBUILD.SCREEN
    END


    RETURN
END
