* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
************* WAEL ***********

    SUBROUTINE VVR.FT.WAVE.CHARGE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

** IF CHARGE = WAIVE THEN (CHARGE.AMT / CHARGE.TYPE )NOINPUT




    IF COMI = "WAIVE" THEN
        T(FT.CHARGE.AMT)<3>='NOINPUT'
        T(FT.CHARGE.TYPE)<3>='NOINPUT'
    END ELSE
        T(FT.CHARGE.AMT)<3>=' '
        T(FT.CHARGE.TYPE)<3>=' '
    END

* IF COMI = "DEBIT PLUS CHARGES" THEN R.NEW(FT.CHARGES.ACCT.NO)= R.NEW(FT.DEBIT.ACCT.NO)
*             CALL REBUILD.SCREEN
*             IF COMI = "CREDIT LESS CHARGES" THEN R.NEW(FT.CHARGES.ACCT.NO)= R.NEW(FT.CREDIT.ACCT.NO)
*             CALL REBUILD.SCREEN


    RETURN
END
