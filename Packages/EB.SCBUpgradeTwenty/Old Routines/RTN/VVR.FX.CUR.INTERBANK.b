* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.FX.CUR.INTERBANK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    IF MESSAGE NE 'VAL' THEN
        CUR = COMI

        IF CUR EQ 'USD' THEN
            R.NEW(FT.CREDIT.ACCT.NO)  = '9943330010508001'
            R.NEW(FT.CREDIT.CURRENCY) = 'EGP'
            R.NEW(FT.DEBIT.ACCT.NO)   = '9943330020508001'
        END

        IF CUR EQ 'EGP' THEN
            R.NEW(FT.CREDIT.ACCT.NO)  = '9943330020508001'
            R.NEW(FT.CREDIT.CURRENCY) = 'USD'
            R.NEW(FT.DEBIT.ACCT.NO)   = '9943330010508001'
        END

        CALL REBUILD.SCREEN
    END
    RETURN
END
