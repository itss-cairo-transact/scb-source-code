* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
**------- INGY -------**

SUBROUTINE VVR.FT.CHQ.NO
* A ROUTINE :
           *TO MAKE SURE IF CHEQUE.TYPE = 5 BILLS OR 6 CUSTOMER.CHEQUE OR 7 CUSTOMER.ORDER THEN MAKE CHEQUE.NUMBER NOINPUT
           *DEFAULT DEBIT.THEIR.REF WITH COMI
           *DEFAULT CHARGES.ACCT.NO WITH DEBIT.ACCT.NO
           *DEFAULT COMMISSION.CODE & CHARGE.CODE = WAIVE

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

IF COMI  THEN
   IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = '7 CUSTOMER.CHEQUE-��� ����' THEN
  ETEXT = 'Input.Is.Not.Allowed'

   END ELSE
IF (R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> ='6 BILLS-�������') OR ( R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> ='8 CUSTOMER.ORDER-��� ����') THEN
   R.NEW(FT.CHARGES.ACCT.NO)= R.NEW(FT.DEBIT.ACCT.NO)
    R.NEW(FT.COMMISSION.CODE)= 'DEBIT PLUS CHARGES'
    R.NEW(FT.CHARGE.CODE)= 'DEBIT PLUS CHARGES'
     CALL REBUILD.SCREEN

   END ELSE
    R.NEW(FT.DEBIT.THEIR.REF)= COMI
    R.NEW(FT.COMMISSION.CODE)= 'WAIVE'
    R.NEW(FT.CHARGE.CODE)= 'WAIVE'
      CALL REBUILD.SCREEN


END
END
END

RETURN
END
