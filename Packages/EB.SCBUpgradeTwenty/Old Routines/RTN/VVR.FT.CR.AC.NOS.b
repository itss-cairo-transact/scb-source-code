* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
***-----INGY----***

    SUBROUTINE VVR.FT.CR.AC.NOS

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

* CHECK IF DEBIT.ACCT # CREDIT.ACCT
* CHECK IF CREDIT.ACCT NOT INTERNAL ACCOUNT
* CHECK IF DEBIT.CURR # CREDIT.CURR

    IF COMI = R.NEW(FT.DEBIT.ACCT.NO) THEN

        ETEXT = '���� �� ���� ���� ������� ����� �� ���� �����'
    END ELSE

        IF NOT(NUM(COMI)) THEN
            ETEXT = '��� ����� ��������� ���������'
        END ELSE
        *    R.NEW(FT.CHARGES.ACCT.NO) = COMI
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)

            IF CATEG >= 2000 AND CATEG <= 2999 THEN
                ETEXT = 'NO.VOSTRO.ACCOUNTS'
            END  ELSE
                IF CATEG >= 5000 AND CATEG <= 5999 THEN
*    IF CATEG # 5010 THEN ETEXT = '���� �� ���� ���� ���'
                END
            END

            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,R.NEW(FT.DEBIT.ACCT.NO),CATEG1)

            IF CATEG1 >= 1000 AND CATEG1 <= 2000 THEN

                IF CATEG >= 1000 AND CATEG <= 2000 THEN
                    ETEXT = "���� ����� ��� �� ���� ����"  ;CALL STORE.END.ERROR
                END
            END ELSE
                IF CATEG1 >= 5000 AND CATEG1 <= 5999 THEN
                    IF CATEG >= 5000 AND CATEG <= 5999 THEN
                        ETEXT = "���� ����� ��� �� ���� ���"

                    END

                END
            END

            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,R.NEW(FT.DEBIT.ACCT.NO),DB.CURR)
            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CR.CURR)
            IF  DB.CURR # CR.CURR   THEN
                ETEXT = '���� ���� ����� ���� �� ����� ���� ���� ������'
            END
        END
    END
    CALL REBUILD.SCREEN

    RETURN
END
