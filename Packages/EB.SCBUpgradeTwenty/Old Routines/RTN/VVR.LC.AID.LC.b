* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VVR.LC.AID.LC
*A ROUTINE TO CHECK:
                  * IF LCLR.AIDLC IS CHANGED THEN EMPTY TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE,
                  * TF.LC.DOC.1ST.COPIES, TF.LC.DOC.2ND.COPIES
                  * IF LCLR.TERMS & TF.LC.LC.TYPE HAVE VALUES THEN DEFAULT TF.LC.CLAUSES.TEXT,TF.LC.DOCUMENT.CODE
                  * WITH SCB.LCD.TERM:LCLR.AID


*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.ADVICE.TEXT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.CLAUSES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS


IF V$FUNCTION='I' AND MESSAGE # "VAL" THEN
 FN.SCB.LC.DOCTERMS = 'F.SCB.LC.DOCTERMS' ; F.SCB.LC.DOCTERMS = '' ; R.SCB.LC.DOCTERMS = ''
 DOC.ID = R.NEW(TF.LC.LC.TYPE):'.': COMI

  IF COMI # R.NEW(TF.LC.LOCAL.REF)<1,LCLR.AIDLC>  THEN
   R.NEW(TF.LC.CLAUSES.TEXT)   = ''
   R.NEW(TF.LC.DOCUMENT.CODE)  = ''
   R.NEW(TF.LC.DOC.1ST.COPIES) = ''
   R.NEW(TF.LC.DOC.2ND.COPIES) = ''
   R.NEW(TF.LC.DOCUMENT.TXT)  = ''
   END 

  IF R.NEW(TF.LC.LC.TYPE) AND R.NEW(TF.LC.LOCAL.REF)<1,LCLR.TERMS> THEN
  CALL OPF( FN.SCB.LC.DOCTERMS,F.SCB.LC.DOCTERMS)
  CALL F.READ( FN.SCB.LC.DOCTERMS, DOC.ID, R.SCB.LC.DOCTERMS, F.SCB.LC.DOCTERMS,ETEXT)
      TRM = R.SCB.LC.DOCTERMS<SCB.LCD.TERM>
      LOCATE R.NEW(TF.LC.LOCAL.REF)<1,LCLR.TERMS> IN TRM<1,1> SETTING POS THEN 
*Line [ 62 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
      CO.DOC = DCOUNT(R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS>,@SM)
*Line [ 64 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
      CO.ADD = DCOUNT(R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS>,@SM)
      FOR I =1 TO CO.DOC
      R.NEW(TF.LC.DOCUMENT.CODE)<1,I> = R.SCB.LC.DOCTERMS<SCB.LCD.DOC.CODE,POS,I>
      CALL DBR("LC.ADVICE.TEXT":@FM:TF.AD.NARRATIVE,R.NEW(TF.LC.DOCUMENT.CODE)<1,I>,NARR)
*Line [ 69 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
       RR= DCOUNT(NARR,@VM)
       FOR Y = 1 TO RR
       R.NEW(TF.LC.DOCUMENT.TXT)<1,I,Y> = NARR<1,Y>
       CLOSE F.SCB.LC.DOCTERMS
       NEXT Y
       NEXT I
        FOR CC = 1 TO CO.ADD
      CALL DBR("LC.CLAUSES":@FM:LC.CL.DESCR,R.SCB.LC.DOCTERMS<SCB.LCD.ADD.CONDS,POS,CC>,DESC)
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
      DD = DCOUNT (DESC,@VM)
      TT=''
      FOR J = 1 TO DD
      TT := ' ': DESC<1,J>
      NEXT J
         R.NEW(TF.LC.CLAUSES.TEXT)<1,CC> = TRIM(TT)
      NEXT CC


      END
     END
    END
    RETURN
    END
