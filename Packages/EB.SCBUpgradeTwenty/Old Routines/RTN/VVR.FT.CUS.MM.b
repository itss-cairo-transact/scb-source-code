* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>248</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CUS.MM
*a routine to empty fields credit.customer & debit.amount & credit.amount with every change of the
*debit.customer
*routine defaults field ordering.cust with customer.name
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

    IF V$FUNCTION = "I" THEN
        IF COMI THEN
            CUU = COMI
            CALL DBR('CUSTOMER':@FM: EB.CUS.POSTING.RESTRICT,CUU,POST)
            CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
            IF POST # "" THEN
         **       ETEXT = DESC ;CALL STORE.END.ERROR
            END ELSE


                IF LEN(COMI) = 7 THEN
                    IF COMI[2,1] = 6 THEN

                        ETEXT = '���� �� ���� ���� �����' ; CALL STORE.END.ERROR

                    END
                END ELSE
                    IF COMI[3,1] = 6 THEN
                        ETEXT = '���� �� ���� ���� �����' ; CALL STORE.END.ERROR
                    END
                END

                IF COMI # R.NEW(FT.DEBIT.CUSTOMER) THEN
                    R.NEW(FT.DEBIT.ACCT.NO) = ""
                    R.NEW(FT.DEBIT.AMOUNT) = ""
                    R.NEW(FT.CREDIT.AMOUNT) = ""
                END
                CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,COMI,CU.NAME)
                R.NEW(FT.ORDERING.CUST) = CU.NAME<1>
                R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> = R.USER< EB.USE.DEPARTMENT.CODE>
* TEXT = "BRN = ":R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> ; CALL REM

                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
