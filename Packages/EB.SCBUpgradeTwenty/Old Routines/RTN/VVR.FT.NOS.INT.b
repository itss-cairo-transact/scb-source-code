* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***----INGY----***
*-----------------------------------------------------------------------------
* <Rating>346</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.NOS.INT

*1-CHECK IF A NEW VALUE IN DEBIT.CUSTOMER THEN MAKE:
*  CREDIT.ACCT.NO  = EMPTY
*  CREDIT.CURRENCY = EMPTY
*2-CHECK IF CATEG < 5000 OR CATEG > 5999 THEN MUST NOSTRO ACCOUNT BRANCH ACCOUNT
*3-CATEG 10000 IS NOT ALLOWED



*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
    IF V$FUNCTION = 'I' THEN
        ACC = COMI
        *TEXT = "1" :ACC ; CALL REM
        CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
        *TEXT = "POST" :POST ; CALL REM
        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
        *TEXT = "DESC" :DESC ; CALL REM
        IF POST # "" THEN
            ETEXT = DESC ;CALL STORE.END.ERROR
        END ELSE

            IF COMI # R.NEW(FT.DEBIT.ACCT.NO) THEN
                R.NEW(FT.CREDIT.ACCT.NO) = ''
                R.NEW(FT.CREDIT.CURRENCY)  = ''
            END
            IF NUM(COMI[1,3]) THEN
                CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
                IF CATEG < 5000 OR CATEG > 5999 THEN ETEXT = '���� �� ���� ����  ���'
* IF CATEG # 5010 THEN ETEXT = '���� �� ���� ��� ���� �����'
            END ELSE
                IF NOT(NUM(COMI[1,3])) THEN
                    IF COMI[4,5] = 10000 THEN ETEXT = '��� ����� ���� �������'
                END
            END
        END
    END
    RETURN
END
