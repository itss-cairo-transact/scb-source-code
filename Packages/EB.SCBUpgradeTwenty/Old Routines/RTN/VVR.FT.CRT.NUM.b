* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>347</Rating>
*-----------------------------------------------------------------------------
***------INGY 16/03/2005------***

    SUBROUTINE VVR.FT.CRT.NUM

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ

** CHECK IF THE CHEQUE ISSUED
**CHECK IF THE CHEQUE STOPPED
**CHECK IF THE CHEQUE PRESENTED

***------------CHEQUE.ISSUE---------------***
    FT.DR.CHQ.ID = R.NEW(FT.DEBIT.ACCT.NO):'.':COMI
    CALL DBR('SCB.FT.DR.CHQ':@FM:DR.CHQ.CHEQ.NO,FT.DR.CHQ.ID,CHQ.NO)
    IF CHQ.NO = COMI THEN
        ETEXT = '�������� �� ������ �� ���' ; CALL STORE.END.ERROR
    END
***------------CHEQUE.REGISTER------------***
    T.SEL = "SELECT FBNK.CHEQUE.REGISTER WITH @ID LIKE  ": '...':R.NEW(FT.DEBIT.ACCT.NO)
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        F.CHQ = '' ; FN.CHQ = 'FBNK.CHEQUE.REGISTER'
        CALL OPF(FN.CHQ,F.CHQ)
        R.CHQ = ''
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CHQ, KEY.LIST<I>, R.CHQ, F.CHQ, READ.ERROR)
            CHQ.NUM = R.CHQ<CHEQUE.REG.CHEQUE.NOS>
            X = FIELD(CHQ.NUM,"-",1)
            Y = FIELD(CHQ.NUM,"-",2)
            IF COMI GT Y AND COMI # X THEN
                ETEXT = 'OUT OF RANGE' ;  CALL STORE.END.ERROR
            END
            IF COMI < X  THEN
                ETEXT = 'OUT OF RANGE' ;  CALL STORE.END.ERROR
            END
*Line [ 69 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            STP = DCOUNT(R.CHQ<CHEQUE.REG.STOPPED.CHQS>,@VM)
            FOR J=1 TO STP
                STP.CHQ= R.CHQ<CHEQUE.REG.STOPPED.CHQS><1,J>
                IF COUNT(STP.CHQ,'-') GT 0 THEN
                    Z = FIELD(STP.CHQ,"-",1)
                    W = FIELD(STP.CHQ,"-",2)
                    IF COMI GE Z AND COMI LE W THEN
                        ETEXT = 'CHEQUE STOPPED'  ;  CALL STORE.END.ERROR
                    END
                END ELSE
                    IF COUNT(STP.CHQ,'-') EQ 0 THEN
                        IF STP.CHQ EQ COMI THEN
                            ETEXT = 'CHEQUE STOPPED'  ;  CALL STORE.END.ERROR
                        END
                    END
                END

            NEXT J

            IF  R.CHQ<CHEQUE.REG.PRESENTED.CHQS> = COMI THEN ETEXT = 'CHEQUE PRESENTED' ;  CALL STORE.END.ERROR
        NEXT I
    END ELSE
        ETEXT = 'CHEQ.NUM.NOT.FOUND'
    END
    CLOSE F.CHQ
***------------CHEQUES.PRESENTED------------***
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = 'SCB':'.':R.NEW(FT.DEBIT.ACCT.NO):'-':COMI
    CHQ.ID = 'SCB':'.':R.NEW(FT.DEBIT.ACCT.NO):'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED,CHQ.ID,PRESENT)
    CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED,CHQ.ID,PRESENT)
****END OF UPDATE 7/3/2016*****************************
    IF PRESENT THEN
        ETEXT = '�� ���� �� ���' ; CALL STORE.END.ERROR
    END ELSE
        ETEXT = ''
    END


    RETURN
END
