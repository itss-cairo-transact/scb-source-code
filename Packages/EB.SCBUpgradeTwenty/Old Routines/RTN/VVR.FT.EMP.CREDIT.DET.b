* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*************NESSREEN-SCB************
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.EMP.CREDIT.DET

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

*To Check if we change Credit Customer then empty Credit Acc. No , Credit Currency ,
*Credit.Amount , Cheque.No and Ordering.Cust
* Also to bring Customer Name by using the entered Customer ID


IF COMI # R.NEW(FT.CREDIT.CUSTOMER) THEN

  R.NEW(FT.CREDIT.ACCT.NO) = ''
  *R.NEW(FT.CREDIT.CURRENCY) = ''
  R.NEW(FT.CREDIT.AMOUNT) = ''
  R.NEW(FT.LOCAL.REF)<1, FTLR.CHEQUE.NO> = ''
  R.NEW(FT.ORDERING.CUST) = ''

END

 CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,COMI,CUST.NAME)
 R.NEW(FT.ORDERING.CUST) = CUST.NAME

 CALL REBUILD.SCREEN

RETURN
END
