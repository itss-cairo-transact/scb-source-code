* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*** DINA & INGY*****
*-----------------------------------------------------------------------------
* <Rating>445</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.CR.ACC.CHECK
*routine make sure that the currency of account is a foriegn currency
*comi must be different than debit account
*comi must be nostro account
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


IF COMI THEN
 CALL DBR("ACCOUNT":@FM:AC.CURRENCY,COMI,CURR)
 CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)

 IF CURR = LCCY THEN ETEXT = 'Only.Foreign.Currency'
 ELSE
 IF NOT (NUM(COMI[1,3])) THEN ETEXT = 'Only.For.Nostro.Account'

 ELSE
 IF CATEG < 5000 OR CATEG > 5999 THEN ETEXT = 'Only.For.Nostro.Account'

END

* IF R.NEW(FT.DEBIT.ACCT.NO)THEN
*            IF NUM(R.NEW(FT.DEBIT.ACCT.NO)[1,3]) THEN
*             IF COMI[3,2] # R.NEW(FT.DEBIT.ACCT.NO)[3,2] THEN ETEXT = 'Must.Be.Same.Currency'
*         END ELSE
*
*
*             IF CURR # R.NEW(FT.DEBIT.ACCT.NO)[1,3] THEN ETEXT = 'Must.Be.Same.Currency'
*          END


IF COMI = R.NEW(FT.DEBIT.ACCT.NO) THEN ETEXT = 'Accounts.Must.Be.Different'
END
END

RETURN
END
