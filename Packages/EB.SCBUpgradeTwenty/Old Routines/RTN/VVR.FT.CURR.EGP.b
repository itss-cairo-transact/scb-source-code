* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE

*-----------------------------------------------------------------------------
* <Rating>392</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.CURR.EGP
*To make sure that the category of the entered account is not nostro or vostro
*If the commision and charge not waive the routine will default charges.account.no with comi
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

 IF V$FUNCTION = "I" THEN
 IF NOT(COMI) THEN ETEXT = 'INPUT.IS.MANDATORY'
 ELSE
 IF NUM(COMI) THEN
 CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
 TEXT = CURR ; CALL REM
 IF CURR # 'EGP' THEN
 ETEXT = 'MUST.BE.LOCAL.CURRENCY'
 END
 CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
 IF CATEG >= 5000 AND CATEG <= 5999 OR (CATEG >= 2000 AND CATEG <= 2999) THEN ETEXT = 'Not.Allowed.For.Nostro.Or.Vostro'
****************************************************
* IF LEFT(COMI,2) # 'PL' THEN
* CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
* IF CURR # LCCY THEN ETEXT = 'ONLY.LOCAL.CURR'
*  IF NUM(COMI) THEN
  IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "2 DRAFT-��� ����� ���� �������" THEN

  ETEXT = 'MUST.BE.INTERNAL.ACCT'
  END
END
*  END ELSE
*   R.NEW(FT.COMMISSION.CODE) = 'WAIVE'
*   R.NEW(FT.CHARGE.CODE) = 'WAIVE'
*   END
*  END ELSE
  ELSE
  IF R.NEW(FT.COMMISSION.CODE) = 'DEBIT PLUS CHARGES' AND R.NEW(FT.CHARGE.CODE) = 'DEBIT PLUS CHARGES' THEN

  R.NEW(FT.CHARGES.ACCT.NO) = COMI
  CALL REBUILD.SCREEN
  END
  
  END
END
END
RETURN
END
