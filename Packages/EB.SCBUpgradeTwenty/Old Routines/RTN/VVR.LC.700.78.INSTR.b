* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*****NESSREEN AHMED**********
*-----------------------------------------------------------------------------
* <Rating>147</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.LC.700.78.INSTR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DEFAULT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

    IF V$FUNCTION='I' THEN
        R.SCB.LC.DEFAULT = ''
        CALL OPF('F.SCB.LC.DEFAULT',F.SCB.LC.DEFAULT)
*READ R.SCB.LC.DEFAULT FROM F.SCB.LC.DEFAULT,"DEFAULT" THEN
        CALL F.READ ('F.SCB.LC.DEFAULT',"DEFAULT", R.SCB.LC.DEFAULT, F.SCB.LC.DEFAULT, FILE.ERR)
        DE.INST.COD = R.SCB.LC.DEFAULT<SCB.LCV.78.INSRTUCT.COD>
        LC.LOCAL.REF = R.SCB.LC.DEFAULT<TF.LC.LOCAL.REF>
        LC.COD = LC.LOCAL.REF<1,LCLR.700.INST.COD>
        IF COMI THEN
            IF COMI # LC.COD THEN
                R.NEW(TF.LC.INSTRUCTIONS) = ''
* TEXT = 'HI' ; CALL REM
            END
            LOCATE COMI IN DE.INST.COD<1,1> SETTING POS1 THEN
                DE.INST.TXT = R.SCB.LC.DEFAULT<SCB.LCV.78.INSRTUCT.TEXT,POS1>
*Line [ 48 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                TT = DCOUNT(DE.INST.TXT, @SM)
                FOR I = 1 TO TT
                    R.NEW(TF.LC.INSTRUCTIONS)<1,I> = DE.INST.TXT<1,I>
*  TEXT = 'R.NEW.SM=':DE.INST.TXT<1,1,I> ; CALL REM
                    R.NEW(TF.LC.INSTRUCTIONS)<1,I> = DE.INST.TXT<1,1,I>
* CALL REBUILD.SCREEN
                NEXT I
            END

    END
END
CALL REBUILD.SCREEN

RETURN
END
