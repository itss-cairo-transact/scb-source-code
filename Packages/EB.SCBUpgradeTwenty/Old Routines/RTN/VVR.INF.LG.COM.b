* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.INF.LG.COM
*    PROGRAM VVR.INF.LG.COM

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.COMM.CORS

    IF MESSAGE # 'VAL' THEN
*************************
        LD.ID = COMI
        FN.COM= 'F.SCB.LG.COMM.CORS';F.COM='';R.COM = '';EEEE=''
        CALL OPF(FN.COM,F.COM)

        YTEXT = "Enter TRANS DATE :  "
        CALL TXTINP(YTEXT, 8, 22, "10", "A")

        TRANS.DAT = COMI

        COM.ID = LD.ID:'-':TRANS.DAT
        COMI = LD.ID
************************
        CALL F.READ(FN.COM,COM.ID,R.COM,F.COM,EEEE)
        LG.CHRG = R.COM<LG.COM.COM.AMOUNT>
*Line [ 48 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        AMT.COUNT = DCOUNT(LG.CHRG,@VM)
        IF EEEE THEN
            ETEXT = 'WRONG LG';CALL STORE.END.ERROR
        END
********************
        FN.LD= 'F.LD.LOANS.AND.DEPOSITS';F.LD='';R.LD = '';EE=''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD ,EE)
        CREDIT.ACCT=R.LD<LD.CHRG.LIQ.ACCT>
        CURR=R.LD<LD.CURRENCY>
************************
        FOR K = 1 TO AMT.COUNT
            L=K+1
            R.NEW(INF.MLT.SIGN)<1,L> = 'CREDIT'
            R.NEW(INF.MLT.TXN.CODE)<1,L> = 79
            R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,L> = CREDIT.ACCT
            R.NEW(INF.MLT.CURRENCY)<1,L>=CURR

            IF CURR EQ 'EGP' THEN
                R.NEW(INF.MLT.AMOUNT.LCY)<1,L> = R.COM<LG.COM.COM.AMOUNT><1,K>

            END ELSE
                R.NEW(INF.MLT.AMOUNT.FCY)<1,L> = R.COM<LG.COM.COM.AMOUNT><1,K>
            END
*            CALL REBUILD.SCREEN
        NEXT K
        R.NEW(INF.MLT.OUR.REFERENCE)<1,1>=TRANS.DAT
**************************
        CALL REBUILD.SCREEN
    END
    RETURN
END
