* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.FT.RETURN.ALL.AMT
*To make fields Debit.Customer & Credit.customer in input mode
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETURNEE

    FN.GOV = 'F.SCB.RETURNEE'; F.GOV = ''
    CALL OPF(FN.GOV,F.GOV)
    TOT.AMT = 0

    IF V$FUNCTION = "I" THEN

        CUST    = R.NEW(FT.DEBIT.CUSTOMER)
        T.DATE  = TODAY
        LOAN.NO = COMI
        T.SEL ="SELECT F.SCB.RETURNEE WITH PAY.CODE2 EQ 1 AND INSTALLEMENT.DATE LE ":T.DATE:" AND CUSTOMER.NO LIKE ...":CUST:" AND LOAN.NO EQ ":LOAN.NO
*       TEXT = T.SEL; CALL REM
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.GOV,KEY.LIST<I>,R.GOV, F.GOV, ETEXT1)
            TOT.AMT = TOT.AMT + R.GOV<RETURN.INSTALEEMNT.AMT>
        NEXT I
        R.NEW(FT.DEBIT.AMOUNT)   = TOT.AMT
        CALL REBUILD.SCREEN
    END
    RETURN
END
