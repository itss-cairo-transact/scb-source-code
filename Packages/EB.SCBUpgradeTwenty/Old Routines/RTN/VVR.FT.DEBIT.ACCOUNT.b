* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
**---------INGY ---------**


SUBROUTINE VVR.FT.DEBIT.ACCOUNT

*A ROUTINE TO CHECK:
                  * IF CATEGORY BETWEEN 5000 & 5999 (NOSTRO ACCOUNT) THEN DISPLAY ERROR
                  * IF CATEGORY BETWEEN 2000 & 2999 (VOSTRO ACCOUNT) THEN DISPLAY ERROR
                  

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

CATEG = ''


IF COMI  THEN
  CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)

    IF 5000 LE CATEG AND CATEG LE 5999 THEN
     ETEXT = 'Can.Not.Be.Nostro.Account'
    END ELSE
       IF 2000 LE CATEG AND CATEG LE 2999 THEN
        ETEXT = 'Can.Not.Be.Vostro.Account'
       END 
        END
END

RETURN
END
