* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*---------------------------------------------
*----------RIHAM YOUSSEF 28/11/2013---------**
    SUBROUTINE VVR.FT.BEN
*---------------------------------------------
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------
    IF COMI = 'BEN' THEN
        R.NEW(FT.COMMISSION.CODE) = 'CREDIT LESS CHARGES'
        R.NEW(FT.CHARGE.CODE)     = 'CREDIT LESS CHARGES'
    END
    IF COMI = 'OUR' THEN
        R.NEW(FT.COMMISSION.CODE) = 'DEBIT PLUS CHARGES'
        R.NEW(FT.CHARGE.CODE)     = 'DEBIT PLUS CHARGES'
    END

    IF COMI = 'SHA' THEN
        R.NEW(FT.COMMISSION.CODE) = 'DEBIT PLUS CHARES'
        R.NEW(FT.CHARGE.CODE)     = 'DEBIT PLUS CHARES'
   END
    IF NOT(COMI) THEN
        R.NEW(FT.COMMISSION.CODE) = ''
        R.NEW(FT.CHARGE.CODE)     = ''
        R.NEW(FT.BEN.OUR.CHARGES) = ''
    END


    CALL REBUILD.SCREEN
    RETURN
END
