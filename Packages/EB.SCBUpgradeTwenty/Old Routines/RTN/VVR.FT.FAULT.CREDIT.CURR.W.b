* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
* ----- NESSREEN AHMED -----*

*1-To Default value of Credit currency using the DEBIT Currency
*2-To Empty DEBIT.ACCT.NO,DEBIT.AMOUNT,CREDIT.ACCT.NO and CREDIT.CURRENCY if u change credit currency

    SUBROUTINE VVR.FT.FAULT.CREDIT.CURR.W

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
         CR = ''
    IF COMI THEN

                *WAGDY*

        CR = COMI

        R.NEW(FT.CREDIT.CURRENCY)= CR

        CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CR,CRNO)

*        TEXT = "CUR IS ": CR ; CALL REM
        CO = ID.COMPANY
        CO.NO = CO[8,2]
        ACC.NO = "99400500": CRNO :"5090":CO.NO
        R.NEW(FT.CREDIT.ACCT.NO) = ACC.NO
        CALL REBUILD.SCREEN

        CALL REBUILD.SCREEN

    END



    RETURN
END
