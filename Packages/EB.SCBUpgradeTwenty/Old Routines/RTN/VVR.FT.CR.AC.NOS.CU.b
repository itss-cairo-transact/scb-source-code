* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***************DINA_SCB*************
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CR.AC.NOS.CU

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF MESSAGE # 'VAL' THEN
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,R.NEW(FT.DEBIT.ACCT.NO),DRCATEG)
        IF (CATEG >= 5000 AND CATEG <= 5999) AND COMI[3,6] = "499999" THEN
            R.NEW(FT.COMMISSION.CODE)= "DEBIT PLUS CHARGES"
            R.NEW(FT.CHARGE.CODE) = "DEBIT PLUS CHARGES"
            R.NEW(FT.COMMISSION.TYPE)=""
            R.NEW(FT.COMMISSION.AMT) = ""
            R.NEW(FT.CHARGE.TYPE) = ""
            R.NEW(FT.CHARGE.AMT) = ""

        END ELSE
            IF (DRCATEG >= 5000 AND CATEG <=5999) AND R.NEW(FT.DEBIT.ACCT.NO)[3,6] = "499999" THEN
                IF R.NEW(FT.DEBIT.CURRENCY) EQ "EGP" THEN
                    R.NEW(FT.COMMISSION.CODE)= "WAIVE"
                    R.NEW(FT.COMMISSION.TYPE)=""
                    R.NEW(FT.COMMISSION.AMT) = ""
                    R.NEW(FT.CHARGES.ACCT.NO)=""
                    R.NEW(FT.CHARGE.CODE) = "WAIVE"
                    R.NEW(FT.CHARGE.TYPE) = ""
                    R.NEW(FT.CHARGE.AMT) = ""
                END ELSE
                    R.NEW(FT.COMMISSION.CODE)= "CREDIT LESS CHARGES"
                    R.NEW(FT.CHARGE.CODE) = "CREDIT LESS CHARGES"
                    R.NEW(FT.COMMISSION.TYPE)=""
                    R.NEW(FT.COMMISSION.AMT) = ""
                    R.NEW(FT.CHARGE.TYPE) = ""
                    R.NEW(FT.CHARGE.AMT) = ""

                END
            END
        END

        CALL REBUILD.SCREEN

        RETURN
    END
