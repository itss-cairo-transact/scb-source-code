* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
******NESSREEN-SCB 12/01/2010************
*-----------------------------------------------------------------------------
* <Rating>148</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CHK.VISA.ACC.SETT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS


    IF COMI[1,4] # '4042' THEN ETEXT = '��� �� ���� ��� ������� ����' ; CALL STORE.END.ERROR

    CARD.NO = COMI

    FN.CARD.ISSUE = 'F.CARD.ISSUE' ; F.CARD.ISSUE = '' ; R.VISA.APP = '' ; RETRY1 = '' ; E1 = ''
    T.SEL = 'SELECT FBNK.CARD.ISSUE WITH VISA.VISA.NO EQ ':CARD.NO

    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF NOT(SELECTED) THEN
        ETEXT = '�� ���� ����� ���� �����'  ; CALL STORE.END.ERROR
    END ELSE
        KEY.TO.USE = KEY.LIST<1>
        CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
        CALL F.READ(FN.CARD.ISSUE,  KEY.TO.USE, R.CARD.ISSUE, F.CARD.ISSUE, E1)
        VISA.ACC.NO = R.CARD.ISSUE<CARD.IS.ACCOUNT>
*****UPDATED BY NESSREEN AHMED 31/10/2011*******************************
****UPDATED BY NESSREEN AHMED 24/10/2011****************************
**24/10/2011***R.NEW(FT.CREDIT.ACCT.NO)= VISA.ACC.NO
        R.NEW(FT.DEBIT.ACCT.NO)= VISA.ACC.NO
*****END OF UPDATE 31/10/2011****************************
        CALL REBUILD.SCREEN
****END OF UPDATE 24/10/2011**********************
**    IF VISA.ACC.NO # ACC.NO THEN
**       ETEXT = '���� ������ ��� ���� ������� � ��� ������'  ; CALL STORE.END.ERROR
**    END
****END OF UPDATE 24/10/2011****************************************
    END
****UPDATED BY NESSREEN AHMED 24/10/2011****************************
    IF MESSAGE = 'VAL' THEN
        DB.ACCT = R.NEW(FT.DEBIT.ACCT.NO)
        ACC.NO = R.NEW(FT.CREDIT.ACCT.NO)
        IF DB.ACCT = ACC.NO THEN
            ETEXT = '���� ����� �� ���� �������'  ; CALL STORE.END.ERROR
        END
    END
****END OF UPDATE 24/10/2011****************************************
    RETURN
END
