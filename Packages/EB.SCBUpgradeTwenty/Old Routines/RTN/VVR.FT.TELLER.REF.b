* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>295</Rating>
*-----------------------------------------------------------------------------

**********WAEL*************
* UPDATED BY DINA - INGY *

SUBROUTINE VVR.FT.TELLER.REF
*ROUTINE MAKE SURE THAT THE TELLER REF IS ENTERED WITH INTERNAL DEBIT.ACCOUNT
*THE REF MUST BE A VALID TRANSACTION IN THE TELLER
*THE CREDIT.ACCOUNT OF THAT TRANS MUST BE WITH SPECIAL CATEGORY
*AND ACCOUNT MUST BE DR.ACCOUNT



*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


IF COMI THEN
IF R.NEW(FT.DEBIT.CUSTOMER) THEN R.NEW(FT.DEBIT.CUSTOMER) = '' ; CALL REBUILD.SCREEN
*IF NUM(R.NEW(FT.DEBIT.ACCT.NO)) THEN ETEXT = 'Input.Not.Allowed'
*ELSE
F.COUNT = '' ; FN.COUNT = 'FBNK.TELLER' ; R.COUNT = ''
CALL OPF(FN.COUNT,F.COUNT)
CALL F.READ( FN.COUNT, COMI, R.COUNT, F.COUNT, TT.ERR)
IF TT.ERR THEN ETEXT = 'Teller.Reference.Does.Not.Exist'
ELSE
CUR  = R.COUNT<TT.TE.CURRENCY.1>
ACCT = R.COUNT<TT.TE.ACCOUNT.1>
AMT.FCY = R.COUNT<TT.TE.AMOUNT.FCY.1>
AMT.LCY = R.COUNT<TT.TE.AMOUNT.LOCAL.1>
* AMT.LCY = R.COUNT<TT.TE.NARRATIVE.1>
*AMT.FCY = R.COUNT<TT.TE.NARRATIVE.1>
CLOSE FN.COUNT
DEP = R.USER<EB.USE.DEPARTMENT.CODE>
  DEP = STR('0', 2- LEN(DEP) ):DEP
*IF ACCT[4,9] # '17303':DEP:'01' THEN ETEXT = 'Wrong.Account.No'
*ELSE
IF CUR = LCCY THEN R.NEW(FT.DEBIT.AMOUNT) = AMT.LCY
ELSE
R.NEW(FT.DEBIT.AMOUNT) = AMT.FCY
END
 R.NEW(FT.DEBIT.CURRENCY) = CUR
  R.NEW(FT.DEBIT.ACCT.NO) = ACCT
  R.NEW(FT.COMMISSION.CODE) = "WAIVE"
  R.NEW(FT.CHARGE.CODE) = "WAIVE"
  R.NEW(FT.CHARGES.ACCT.NO)= ''
  R.NEW(FT.ORDERING.CUST)= ''
   CALL REBUILD.SCREEN
END
END
*END
*END
*END
* ELSE
   * IF NOT(NUM(R.NEW(FT.DEBIT.ACCT.NO)[1,3])) THEN ETEXT ='You.Must.Enter.Value'
*END

RETURN
END
