* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------


    SUBROUTINE VVR.FT.DEBIT.CURR.EST

*1-MAKE THE DEBIT CURRENCY EQ CREDIT CURRENCY

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    IF LEN(R.NEW(FT.CREDIT.CUSTOMER))= 7 THEN
        CU.NO = "0":R.NEW(FT.CREDIT.CUSTOMER)
    END ELSE
        CU.NO = R.NEW(FT.CREDIT.CUSTOMER)
    END

    CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,COMI,CCY.NO)

    IF COMI THEN
        R.NEW(FT.CREDIT.CURRENCY) = COMI
        R.NEW(FT.CREDIT.ACCT.NO) = CU.NO:CCY.NO:"300101"
    END

    CALL REBUILD.SCREEN

    RETURN
END
