* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***----INGY----***
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.DB.AC.INT

*1-CHECK IF THE NEW VALUE IN DEBIT.ACCT NOT EQ OLD VALUE THEN MAKE:
*CREDIT.CUSTOMER = EMPTY
*CREDIT.ACCT.NO  = EMPTY
*CREDIT.CURRENCY = EMPTY
*2-CHECK IF CATEGORY >= 5000 AND CATEG <= 5999 THEN NOSTRO.OR.VOSTRO ACCT IS NOT ALLOWED

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    INCLUDE T24.BP I_F.POSTING.RESTRICT

    IF V$FUNCTION = 'I' THEN
        ACC = COMI
        CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
        IF POST # "" THEN
            ETEXT = DESC ;CALL STORE.END.ERROR
        END ELSE

            IF COMI # R.NEW(FT.DEBIT.ACCT.NO) THEN
                R.NEW(FT.CREDIT.CUSTOMER) = ''
                R.NEW(FT.CREDIT.ACCT.NO) = ''
                R.NEW(FT.CREDIT.CURRENCY)  = ''
            END
            IF NUM(COMI[1,3]) THEN
                CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
                IF (CATEG >= 5000 AND CATEG <= 5999) OR (CATEG >= 2000 AND CATEG <= 2999) THEN ETEXT = 'NO.NOSTRO.OR.VOSTRO'
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
