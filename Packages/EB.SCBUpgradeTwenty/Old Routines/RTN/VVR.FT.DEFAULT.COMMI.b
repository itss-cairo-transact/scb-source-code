* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
***------INGY 13/03/2005------***

    SUBROUTINE VVR.FT.DEFAULT.COMMI

*TO DEFUALT COMMISSION TYPE WITH THE TYPE DEFINED IN THE TABLE
*FT.TXN.TYPE.CONDITION FOR THE TYPE OF TRANSACTION

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    IF COMI THEN
        IF COMI = "DEBIT PLUS CHARGES"  THEN
            CALL DBR('FT.TXN.TYPE.CONDITION':@FM:FT6.COMM.TYPES,R.NEW(FT.TRANSACTION.TYPE),COMM)
            IF COMM THEN
*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                XX= DCOUNT(COMM , @VM)
                FOR I = 1 TO XX
                    R.NEW(FT.COMMISSION.TYPE)<1,I> = COMM<1,I>
                NEXT I
                CALL REBUILD.SCREEN
            END
        END

    END

    RETURN
END
