* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***----INGY----***
*-----------------------------------------------------------------------------
* <Rating>-6</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.IT.CRCU

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

*To empty the Credit.Acct.No if the user chages the Credit Customer
*To check that the Customer's sector is BANK OR IT PRODUCE AN ERROR MESSAGE

    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    STO5=POSS<1,5>
    STO6=POSS<1,6>
    STO7=POSS<1,7>
    STO8=POSS<1,8>
    STO9=POSS<1,9>
    STO10=POSS<1,10>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
    END
*----------------
*----------------

    IF MESSAGE NE 'VAL' THEN
        IF COMI # R.NEW(FT.CREDIT.CUSTOMER) THEN
            R.NEW(FT.CREDIT.ACCT.NO) = ''
*    R.NEW(FT.CREDIT.CURRENCY) = ''

            CALL REBUILD.SCREEN
        END
        CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI, SEC)
* IF (SEC >= 3000 AND SEC <= 3999) THEN ETEXT = '��� ����� ������ ����'
*    CALL DBR('SCB.VER.IND.SEC.LEG':@FM:VISL.SECTOR,',SCB.BANK',SEC1)
*    LOCATE SEC IN SEC1<1,1,1> SETTING M THEN  ETEXT = '��� ����� ������ ����'
    END

    RETURN
END
