* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE

*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.GOV.REG


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE

    IF COMI THEN
*Line [ 33 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.GEO.SEC,COMI,LOC)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,COMI,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
LOC=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.GEO.SEC>

        R.NEW(EB.CUS.TOWN.COUNTRY)<1,1> = LOC
        R.NEW(EB.CUS.TOWN.COUNTRY)<1,2> = LOC
    END

    RETURN
END
