* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
* ----- NESSREEN AHMED 21/10/2002,  -----

*To check that it is internal account
*To check that the account department is the same as the user department

SUBROUTINE VVR.FT.CHK.CREDIT.ACC

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

IF COMI THEN

   IF NUM(COMI[1,3]) THEN ETEXT = 'Not.Allowed.For.Int'; RETURN

   IF COMI[4,5] = 10000 THEN ETEXT = 'NOT.ALLOWED.FOR.THAT.GATEGORY' ; RETURN

   ID.DEPT = TRIM( COMI[9,2], '0', 'L') ;* remove leading zeros
   IF R.USER<EB.USE.DEPARTMENT.CODE> # ID.DEPT THEN ETEXT = 'SHOULD.ONLY.FROM.UR.DEPT'; RETURN

END



RETURN
END
