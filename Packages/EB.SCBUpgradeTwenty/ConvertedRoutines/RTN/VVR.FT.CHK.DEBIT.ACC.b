* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>392</Rating>
*-----------------------------------------------------------------------------
** ----- NESSREEN AHMED 21/10/2002,  -----

*1-To check that it is internal account or PL account
*2-To check that the account department is the same as the user department

    SUBROUTINE VVR.FT.CHK.DEBIT.ACC

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

*    IF  COMI[1,2] = 'PL' THEN R.NEW(FT.ORDERING.CUST) = COMI ; CALL REBUILD.SCREEN ; RETURN

    ACC = COMI
*TEXT = "1" :ACC ; CALL REM
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
POST=R.ITSS.ACCOUNT<AC.POSTING.RESTRICT>
    *TEXT = "POST" :POST ; CALL REM
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
    *TEXT = "DESC" :DESC ; CALL REM
    IF POST # "" THEN
        ETEXT = DESC ;CALL STORE.END.ERROR
    END ELSE

        IF NOT(NUM(COMI[1,3])) THEN
            IF  COMI[1,2] # 'PL' THEN

                IF COMI[4,5] = 10000 THEN ETEXT = '��� ����� ���� �������' ; RETURN

                ID.DEPT = TRIM( COMI[9,2], '0', 'L')        ;* remove leading zeros
*  ID.DEPT = TRIM( COMI[11,2], '0', 'L')     ;* remove leading zeros
                IF R.USER<EB.USE.DEPARTMENT.CODE> # ID.DEPT THEN ETEXT = '���� �� ���� �� ��� �����' ; RETURN
            END
        END ELSE ETEXT = '���� �� ���� ���� ��� ���� �� ����� � �����'

* R.NEW(FT.ORDERING.CUST) = COMI
        CALL REBUILD.SCREEN
    END
    RETURN
END
