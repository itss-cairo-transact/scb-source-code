* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.FT.EZN.CHECK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MALIA.EZN
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    COMP1 = ID.COMPANY

    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
***************************************
INITIATE:
*--------
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.EZN = 'F.SCB.MALIA.EZN' ; F.EZN = '' ; R.EZN = ''
    CALL OPF(FN.EZN,F.EZN)
    STAT = ''
    RETURN
***************************************
PROCESS:
*-------
    IF COMI THEN
        CALL F.READ(FN.EZN,COMI,R.EZN,F.EZN,ERRR1)
        STAT = R.EZN<EZN.STATUS>
        IF STAT NE '' THEN
            ETEXT = ' �� �����  ����� �� ��� '
*** ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
    CALL REBUILD.SCREEN
    RETURN
***************************************
END
