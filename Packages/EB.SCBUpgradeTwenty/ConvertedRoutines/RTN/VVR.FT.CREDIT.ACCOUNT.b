* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
**---------INGY ---------**


SUBROUTINE VVR.FT.CREDIT.ACCOUNT

*A ROUTINE TO CHECK:
                  * IF CATEGORY BETWEEN 5000 & 5999 (NOSTRO ACCOUNT) THEN DISPLAY ERROR
                  * IF CATEGORY BETWEEN 2000 & 2999 (VOSTRO ACCOUNT) THEN DISPLAY ERROR
                  * IF DEBIT.ACCOUNT EQ CREDIT.ACCOUNT THEN DISPLAY ERROR


*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


CATEG = ''

IF COMI  THEN
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
  IF 5000 LE CATEG AND CATEG LE 5999 THEN
   ETEXT = 'This.Is.Nostro.Account'
  END ELSE
   IF 2000 LE CATEG AND CATEG LE 2999 THEN
    ETEXT = 'This.Is.Vostro.Account'
    END ELSE
    IF COMI = R.NEW(FT.DEBIT.ACCT.NO) THEN ETEXT = 'Accounts.Must.Be.Different'
    END
  END
END


RETURN
END
