* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.INF.TRE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    IF MESSAGE # 'VAL' THEN
        ZZ = COMI +1
        IF COMI AND NOT(R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,ZZ>) THEN
            R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,ZZ> = ''
*Line [ 31 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
            FOR K = 2 TO NO.M
                R.NEW(INF.MLT.SIGN)<1,K> = ''
                R.NEW(INF.MLT.TXN.CODE)<1,K> = ''
                R.NEW(INF.MLT.CURRENCY.MARKET)<1,K> = 1

            NEXT K
        END

    END
    CALL REBUILD.SCREEN
    RETURN
END
