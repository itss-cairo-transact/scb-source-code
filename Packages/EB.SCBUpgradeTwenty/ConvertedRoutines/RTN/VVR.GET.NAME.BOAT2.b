* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.GET.NAME.BOAT2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.DATA
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    FN.BOD = 'F.SCB.BOAT.DATA' ; F.BOD = ''
    CALL OPF(FN.BOD,F.BOD)

    CALL F.READ(FN.BOD,COMI,R.BOD,F.BOD,E1)
    IF NOT(E1) THEN
        R.NEW(BOT.BOAT.NAME)<1,AV> = R.BOD<BOD.BOAT.NAME>
    END ELSE
        ETEXT = "������ ������ ��� �����"
    END

    IF COMP NE 'EG0010020' AND COMI EQ '50' THEN
        ETEXT = "��� ����� ����� �����"
    END

    CALL REBUILD.SCREEN

    RETURN
END
