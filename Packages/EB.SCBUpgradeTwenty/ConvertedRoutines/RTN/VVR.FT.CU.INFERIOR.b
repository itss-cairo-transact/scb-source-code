* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>149</Rating>
*-----------------------------------------------------------------------------
***----INGY14/03/2005----***

    SUBROUTINE VVR.FT.CU.INFERIOR

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

*To check that if the Cheque Type is Draft but not for our customers then the customer's class should be inferior

    IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>  = '2 DRAFT-��� ����� ���� �������' THEN
        IF LEN(R.NEW(FT.DEBIT.CUSTOMER)) = '7' THEN
            IF R.NEW(FT.DEBIT.CUSTOMER)[2,1] # '6' THEN
                ETEXT = '���� �� ���� ���� ����'
            END
        END ELSE
            IF LEN(R.NEW(FT.DEBIT.CUSTOMER)) = '8' THEN
                IF R.NEW(FT.DEBIT.CUSTOMER)[2,1] # '6' THEN ETEXT = '���� �� ���� ���� ����'
            END
        END
    END
    RETURN
END
