* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.INF.TAX.PERCENT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_INF.LOCAL.REFS

    IF MESSAGE # 'VAL' THEN
        TAX.AMT  = R.NEW(INF.MLT.GL.NUMBER)<1,AV>
        ACCT.NO  = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,AV>
        AC.SIGN  = R.NEW(INF.MLT.SIGN)<1,AV>
        IF COMI THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:2,ACCT.NO,AC.CATT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.CATT=R.ITSS.ACCOUNT<2>
            IF AC.SIGN EQ 'CREDIT' AND (AC.CATT EQ '16522' OR AC.CATT EQ '16523' OR AC.CATT EQ '16528' OR AC.CATT EQ '16510' OR AC.CATT EQ '16536') THEN
                R.NEW(INF.MLT.AMOUNT.LCY)<1,AV> = COMI * TAX.AMT / 100
            END ELSE
                AF = INF.MLT.GL.NUMBER  ;* AV = XX1 ; AS = 1
                ETEXT = 'NOT ALLOWED FOR THIS ACCOUNT'
                CALL STORE.END.ERROR
            END
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
