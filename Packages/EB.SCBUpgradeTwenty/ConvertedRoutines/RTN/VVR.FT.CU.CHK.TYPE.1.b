* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>142</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CU.CHK.TYPE.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>  = '2 DRAFT-��� ����� ���� �������' THEN


        IF LEN(R.NEW(FT.DEBIT.CUSTOMER)) = '7' THEN
            IF R.NEW(FT.DEBIT.CUSTOMER)[2,1] # '6' THEN
                ETEXT = '���� �� ���� ���� ����'
            END
        END ELSE
            IF LEN(R.NEW(FT.DEBIT.CUSTOMER)) = '8' THEN
                IF R.NEW(FT.DEBIT.CUSTOMER)[3,1] # '6' THEN ETEXT = '���� �� ���� ���� ����'
            END
        END
    END

*  IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>  = '3 DRAFT-��� ����� ��� �� ���������' THEN
*  R.NEW(FT.DEBIT.ACCT.NO) = "PL"
*  T(FT.DEBIT.CUSTOMER)<3> ='NOINPUT'
* CALL REBUILD.SCREEN
*  END

* IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>  = '4 DRAFT-��� ����� ��� �� ���� �����' THEN
*   R.NEW(FT.DEBIT.ACCT.NO) = R.NEW(FT.DEBIT.CURRENCY)
*  T(FT.DEBIT.CUSTOMER)<3> ='NOINPUT'
* CALL REBUILD.SCREEN
* END

    RETURN
END
