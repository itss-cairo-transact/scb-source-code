* @ValidationCode : MjoxOTk4NzcwMTIwOkNwMTI1MjoxNjQ1MDA2NDk1Nzc3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 16 Feb 2022 12:14:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>886</Rating>
*-----------------------------------------------------------------------------
************* WAEL ***********
*******UPDATED BY DINA & INGY*******

SUBROUTINE VVR.FT.OUTLOCAL
*A ROUTINE TO MAKE SURE :
* IF TELLER NOT EQUAL NULL THEN DEBIT ACCOUNT MUST BE INTERNAL
* IT'S LOCAL CURRENCY
* THAT THE DEBIT ACCOUNT IS NOT A VOSTRO OR NOSTRO ACCOUNT
* IT IS NOT A BANK SECTOR
* IF THE DEBIT ACCOUNT IS CUSTOMER THEN DEFAULT COMMISSION.CODE & CHARGE.CODE WITH DEBIT PLUS CHARGES
* ELSE DEFAULT COMMISSION.CODE & CHARGE.CODE WITH WAIVE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHQ.TYPE.CATEG
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS


    IF V$FUNCTION = 'I' THEN
        IF MESSAGE # 'VAL' THEN
            IF COMI THEN
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* *CALL DBR("ACCOUNT":@FM:AC.CURRENCY,COMI,CURR)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*IF CURR # LCCY THEN ETEXT = 'Only.Local.Currency'
*ELSE
                IF NUM(COMI) THEN
                    IF R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> THEN ETEXT = "Must.Be.Internal.Acct"
                    ELSE
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
                        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                        FN.F.ITSS.ACCOUNT = ''
                        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                        CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                        CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                        IF (CATEG >= 2000 AND CATEG <= 2999  OR  CATEG >= 5000 AND CATEG <= 5999) THEN ETEXT = 'Not.Allowed.For.Nostro.Or.Vostro'
                        ELSE
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,COMI,CUS)
                            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                            FN.F.ITSS.ACCOUNT = ''
                            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                            CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                            CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, CUS, SEC )
                            F.ITSS.CUSTOMER = 'F.CUSTOMER'
                            FN.F.ITSS.CUSTOMER = ''
                            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                            CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                            SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  *CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, TRIM(COMI[5,7],"0","L"), SEC )
                            F.ITSS.CUSTOMER = 'F.CUSTOMER'
                            FN.F.ITSS.CUSTOMER = ''
                            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                            CALL F.READ(F.ITSS.CUSTOMER,"L",R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                            SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR,TRIM(COMI[5,7],"0")>
                            IF (SEC >= 3000 AND SEC <= 3999) THEN ETEXT = 'Not.Allowed.For.Bank'
                            ELSE
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  CALL DBR( 'CUSTOMER':@FM:EB.CUS.NAME.1, CUS, CUS.NAME )
                                F.ITSS.CUSTOMER = 'F.CUSTOMER'
                                FN.F.ITSS.CUSTOMER = ''
                                CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                                CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                                CUS.NAME=R.ITSS.CUSTOMER<EB.CUS.NAME.1>
*  CALL DBR( 'CUSTOMER':@FM:EB.CUS.NAME.1,TRIM(COMI[5,7],"0","L"), CUS.NAME )
                                R.NEW(FT.COMMISSION.CODE) = "DEBIT PLUS CHARGES"
                                R.NEW(FT.CHARGE.CODE) = "DEBIT PLUS CHARGES"
                                R.NEW(FT.ORDERING.CUST)= CUS.NAME
                                R.NEW(FT.CHARGES.ACCT.NO)= COMI
                                CALL REBUILD.SCREEN
                            END
                        END
                    END
                END ELSE
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  *CALL DBR ('SCB.CHQ.TYPE.CATEG':@FM:CC.CATEGORY,10,ACCT.CATEG)
                    F.ITSS.SCB.CHQ.TYPE.CATEG = 'F.SCB.CHQ.TYPE.CATEG'
                    FN.F.ITSS.SCB.CHQ.TYPE.CATEG = ''
                    CALL OPF(F.ITSS.SCB.CHQ.TYPE.CATEG,FN.F.ITSS.SCB.CHQ.TYPE.CATEG)
                    CALL F.READ(F.ITSS.SCB.CHQ.TYPE.CATEG,10,R.ITSS.SCB.CHQ.TYPE.CATEG,FN.F.ITSS.SCB.CHQ.TYPE.CATEG,ERROR.SCB.CHQ.TYPE.CATEG)
                    ACCT.CATEG=R.ITSS.SCB.CHQ.TYPE.CATEG<CC.CATEGORY>
                    DEP = R.USER<EB.USE.DEPARTMENT.CODE>
                    DEP = STR('0', 2- LEN(DEP) ):DEP
                    ACCT = LCCY:'17303':DEP:'01'
                    IF COMI # ACCT THEN ETEXT = 'This.Account.Is.Not.Allowed'

                    ELSE
                        R.NEW(FT.COMMISSION.CODE) = "WAIVE"
                        R.NEW(FT.CHARGE.CODE) = "WAIVE"
                        R.NEW(FT.CHARGES.ACCT.NO)= ''
                        R.NEW(FT.ORDERING.CUST)= ''
                        CALL REBUILD.SCREEN
                    END
                END

            END
        END
    END

RETURN
END
