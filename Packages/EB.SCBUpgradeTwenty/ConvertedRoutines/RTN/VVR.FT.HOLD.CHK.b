* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
** ----- INGY-----**

    SUBROUTINE VVR.FT.HOLD.CHK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF MESSAGE # 'VAL' THEN
        IF COMI # R.NEW(FT.DEBIT.AMOUNT) THEN
*     R.NEW(FT.CREDIT.AMOUNT)= ''
            ETEXT = '��� ����� ������'
        END
        CALL REBUILD.SCREEN
    END
    IF MESSAGE # 'VAL' THEN

        ACCT =  R.NEW(FT.DEBIT.ACCT.NO)
*      IF ACCT[11,4] # '1201' THEN
        IF NOT( ACCT[11,4] LE 1500 AND ACCT[11,4] GE 1590)THEN
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LOCK.NO=DCOUNT(LOCK.AMT,@VM)
        FOR I=1 TO LOCK.NO
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
            LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
        NEXT I
        NET.BAL=BAL-LOCK.AMT1

        IF COMI GT NET.BAL THEN
            ETEXT = '������ �� ����'
        END
    END
*  END ELSE
*  R.NEW(FT.CREDIT.AMOUNT)= R.NEW(FT.DEBIT.AMOUNT)
END
RETURN
END
