* @ValidationCode : MjoxODg1Mzk3MTYxOkNwMTI1MjoxNjQxNzM4NDg4MTc3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 16:28:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>178</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.CHQ.RETURN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PAYMENT.STOP
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CHEQUE.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.POSTING.RESTRICT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 47 ] Adding I_F.SCB.CHQ.RETURN - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHQ.RETURN

*********
*TEXT='RETURN';CALL REM
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160703 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160703 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "SCB.CHQ.RETURN"
    OFS.OPTIONS = "INP"
***OFS.USER.INFO = "/"

*************HYTHAM********20090318**********
    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP
*************HYTHAM********20090318**********

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""
    COMMA = ","

*AMT = R.NEW(TT.TE.AMOUNT.LOCAL.1)
    AMT = R.NEW(FT.DEBIT.AMOUNT)
*Line [ 79 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('THE SIGNATURE IS CORRECT', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
***********************CHECK BALANCE
        LOCK.AMT1 = ''
* ACCT.NO = R.NEW(TT.TE.ACCOUNT.1)
        ACCT.NO = R.NEW(FT.DEBIT.ACCT.NO)
*   TEXT = "ACCT.NO=":ACCT.NO ; CALL REM
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.WORKING.BALANCE, ACCT.NO, BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
*   TEXT = "BAL=":BAL ; CALL REM
*Line [ 95 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 90 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LOCK.NO=DCOUNT(LOCK.AMT,@VM)
*        FOR I=1 TO LOCK.NO
*            CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT.NO,LOCK.AMT)
*          LOCK.AMT1=LOCK.AMT1+LOCK.AMT<1,I>
*     NEXT I
        NET.BAL=BAL-LOCK.AMT<1,LOCK.NO>
        CATEG = ACCT.NO[11,4]
** IF R.NEW(TT.TE.AMOUNT.LOCAL.1) THEN
*  IF R.NEW(TT.TE.CURRENCY.1) EQ 'EGP' THEN
** AMT.LOC = R.NEW(TT.TE.AMOUNT.LOCAL.1)
        AMT.LOC = AMT
* END ELSE
* AMT.LOC = R.NEW(TT.TE.AMOUNT.FCY.1)
* END
**********
*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCT.NO,AC.CAT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AC.CAT=R.ITSS.ACCOUNT<AC.CATEGORY>

*   IF AC.CAT # '1201' AND AC.CAT # '1202' THEN
        IF NOT( AC.CAT GE 1101 AND AC.CAT LE 1590 ) THEN
**********
            IF AMT.LOC GT NET.BAL THEN
************************************
*Line [ 113 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                CALL TXTINP('THE AMOUNT IS NOT AVILABLE DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
*     CUST = ''
                IF COMI[1,1] = 'Y' THEN
                    ETEXT = 'AMOUNT NOT AVILABLE'
                    CALL STORE.END.ERROR


                    CURR = R.NEW(FT.DEBIT.CURRENCY)
                    ACCT = R.NEW(FT.DEBIT.ACCT.NO)
**    AMT.LCY = R.NEW(TT.TE.AMOUNT.LOCAL.1)
* AMT.LCY = AMT
* AMT.FCY = R.NEW(TT.TE.AMOUNT.FCY.1)
                    CHQ.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
*NRTV = R.NEW(TT.TE.NARRATIVE.2)
                    NRTV = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS>
                    STOP.TYPE = '20'
                    CHEQUE.TYPE = 'OSCB'
                    STOP.DATE = TODAY
                END ELSE
                    ETEXT = 'AMOUNT NOT AVILABLE'
                    CALL STORE.END.ERROR

                END
            END
*************
        END
**************
    END ELSE
*Line [ 142 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL TXTINP('DO YOU WANT TO MAKE A RETURN ?', 8, 23, '1.1', @FM:'Y_N')
        IF COMI[1,1] = 'Y' THEN
            ETEXT = 'AMOUNT NOT AVILABLE'
            CALL STORE.END.ERROR

***************************************
            CURR = R.NEW(FT.DEBIT.CURRENCY)
            ACCT = R.NEW(FT.DEBIT.ACCT.NO)
            CHQ.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO>
            NRTV = R.NEW(FT.LOCAL.REF)<1,FTLR.BENEFICIARY.CUS>
            STOP.TYPE = '21'
            CHEQUE.TYPE = 'OSCB'
            STOP.DATE = TODAY
        END ELSE
            ETEXT = 'AMOUNT NOT AVILABLE'
            CALL STORE.END.ERROR

        END
    END

    MUL.NO = 1
*Line [ 182 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR( 'SCB.CHQ.RETURN':@FM:SCB.CHQR.PAYM.STOP.TYPE, ACCT,ST.TYPE)
F.ITSS.SCB.CHQ.RETURN = 'F.SCB.CHQ.RETURN'
FN.F.ITSS.SCB.CHQ.RETURN = ''
CALL OPF(F.ITSS.SCB.CHQ.RETURN,FN.F.ITSS.SCB.CHQ.RETURN)
CALL F.READ(F.ITSS.SCB.CHQ.RETURN,ACCT,R.ITSS.SCB.CHQ.RETURN,FN.F.ITSS.SCB.CHQ.RETURN,ERROR.SCB.CHQ.RETURN)
ST.TYPE=R.ITSS.SCB.CHQ.RETURN<SCB.CHQR.PAYM.STOP.TYPE>
*  TEXT = ST.TYPE ; CALL REM
    IF ST.TYPE THEN

*Line [ 168 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        MULL.NOO=DCOUNT(ST.TYPE,@VM)
*    TEXT = MULL.NOO ; CALL REM
        MUL.NO = MUL.NO + MULL.NOO

    END

    IF CURR THEN
* OFS.MESSAGE.DATA =  "CUSTOMER=":CUST:COMMA
*OFS.MESSAGE.DATA :=  "CURRENCY=":CURR:COMMA
        OFS.MESSAGE.DATA :=  "PAYM.STOP.TYPE:":MUL.NO:"=":STOP.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "FIRST.CHEQUE.NO:":MUL.NO:"=":CHQ.NO:COMMA
*OFS.MESSAGE.DATA :=  "LAST.CHEQUE.NO=":CHQ.NO:COMMA
        OFS.MESSAGE.DATA :=  "CHEQUE.TYPE:":MUL.NO:"=":CHEQUE.TYPE:COMMA
        OFS.MESSAGE.DATA :=  "STOP.DATE:":MUL.NO:"=":STOP.DATE:COMMA
        OFS.MESSAGE.DATA :=  "AMOUNT:":MUL.NO:"=":AMT:COMMA
        OFS.MESSAGE.DATA :=  "BENEFICIARY:":MUL.NO:"=":NRTV


        ID.KEY.LIST= ACCT
        TEXT = "ID.KEY.LIST = ":ID.KEY.LIST ; CALL REM
        F.PATH = FN.OFS.IN
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:ID.KEY.LIST:COMMA:OFS.MESSAGE.DATA
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.KEY.LIST:"-":TODAY ON ERROR
            TEXT = " ERROR "
            CALL REM
        END
***** SCB R15 UPG 20160703 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*        SCB.OFS.SOURCE = "SCBOFFLINE"
*        SCB.OFS.ID = '' ; SCB.OPT = ''
*        CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*        END
*        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160703 - E
******************************
    END
*****************************************
*********
RETURN
END
