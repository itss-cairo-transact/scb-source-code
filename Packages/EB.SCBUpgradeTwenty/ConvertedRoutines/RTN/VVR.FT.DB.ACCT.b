* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*---------------------EDITED BY MAI SAAD 27/03/2018--------------------------------------------------------
    SUBROUTINE VVR.FT.DB.ACCT
*1-CHECK IF DEBIT.ACCT IS A (PL)ACCOUNT THEN CREDIT.ACCT MUST BE A CUSTOMER ACCOUNT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF MESSAGE NE 'VAL' THEN
        ACC = COMI
**        CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
**        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
*        IF POST # "" THEN
**            ETEXT = DESC ;CALL STORE.END.ERROR
**        END ELSE

        IF AF = FT.DEBIT.ACCT.NO THEN
            IF COMI AND R.NEW(FT.CREDIT.ACCT.NO) THEN
                IF COMI[1,2] EQ 'PL' AND R.NEW(FT.CREDIT.ACCT.NO)[1,2] = 'PL' THEN
                    ETEXT = '���� �� ���� ���� ����'
                END
                IF NUM(COMI) AND NUM(R.NEW(FT.CREDIT.ACCT.NO)) THEN
                    ETEXT = '���� �� ���� ���� ����� ������'
                END
            END
        END

        IF AF = FT.CREDIT.ACCT.NO THEN
            IF COMI AND R.NEW(FT.DEBIT.ACCT.NO) THEN
                IF COMI[1,2] EQ 'PL' AND R.NEW(FT.DEBIT.ACCT.NO)[1,2] = 'PL' THEN
                    ETEXT = '���� �� ���� ���� ����'
                END
                IF NUM(COMI) AND NUM(R.NEW(FT.DEBIT.ACCT.NO)) THEN
                    ETEXT = '���� �� ���� ���� ����� ������'
                END
            END
        END
    END
*************ADDED BY MAI SAAD 27/03/2018*****************
    COMP = C$ID.COMPANY
    COM.CODE      = COMP[8,2]
    IF COM.CODE[1,1]  EQ '0' THEN
        COM.CODE  = COMP[9,1]
    END ELSE
        COM.CODE  = COMP[8,2]
    END
    IF MESSAGE EQ 'VAL' THEN
        DB.ACC  = R.NEW(FT.DEBIT.ACCT.NO)
        CR.ACC  = R.NEW(FT.CREDIT.ACCT.NO)
        IF LEN(DB.ACC)<16 OR LEN(CR.ACC)<16 THEN
            IF DB.ACC[1,2] EQ 'PL' THEN
                PL.CAT = DB.ACC[3,5]
                IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                    IF  COMP NE 'EG0010099' THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                    END
                END ELSE
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END
            END ELSE
                IF CR.ACC[1,2] EQ 'PL' THEN
                    PL.CAT.C = CR.ACC[3,5]
                    IF (PL.CAT.C GE 52880 AND PL.CAT.C LE 52900) OR (PL.CAT.C GE 56001 AND PL.CAT.C LE 56019) OR (PL.CAT.C GE 60000 AND PL.CAT.C LE 69998 ) OR PL.CAT.C EQ 54036 THEN
                        IF  COMP NE 'EG0010099' THEN
                            R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
                        END ELSE
                            R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                        END
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                    END
                END
            END
        END ELSE
            R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
        END
    END
**END
    RETURN
END
