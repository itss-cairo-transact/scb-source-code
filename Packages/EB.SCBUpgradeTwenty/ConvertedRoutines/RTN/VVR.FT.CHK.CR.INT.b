* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
** ----- INGY30/03/2004 -----
**EDITED BY MAI.SAAD 13/03/2018
*To check that it is internal account or PL account
*To check that the account department is the same as the user department

    SUBROUTINE VVR.FT.CHK.CR.INT

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*  IF  COMI[1,2] = 'PL' THEN R.NEW(FT.ORDERING.CUST) = COMI ; CALL REBUILD.SCREEN ; RETURN
    IF NOT(NUM(COMI[1,3])) THEN
        IF  COMI[1,2] # 'PL' THEN
            IF COMI[4,5] = 10000 THEN ETEXT = '��� ����� ���� �������' ; RETURN

            ID.DEPT = TRIM( COMI[9,2], '0', 'L')  ;* remove leading zeros
*       ID.DEPT = TRIM( COMI[11,2], '0', 'L') ;* remove leading zeros
*IF R.USER<EB.USE.DEPARTMENT.CODE> # ID.DEPT THEN ETEXT = '���� �� ���� �� ��� �����' ; RETURN
        END
    END ELSE ETEXT = '���� �� ���� ���� ��� ���� �� ����� � �����'

***MAI******
    IF MESSAGE EQ 'VAL' THEN
*Line [ 50 ] Adding EB.SCBUpgradeTwenty. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        CALL EB.SCBUpgradeTwenty.VVR.FT.PROFT.CENTRE
    END
    CALL REBUILD.SCREEN
    RETURN
END
