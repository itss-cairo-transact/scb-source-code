* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>41</Rating>
*-----------------------------------------------------------------------------
***-----INGY----***

    SUBROUTINE VVR.FT.CR.AC.NOS.CBE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF COMI = R.NEW(FT.DEBIT.ACCT.NO) THEN

        ETEXT = '���� �� ���� ���� ������� ����� �� ���� �����'
    END ELSE

        IF NOT(NUM(COMI)) THEN
            ETEXT = '��� ����� ��������� ���������'
        END ELSE
*    R.NEW(FT.CHARGES.ACCT.NO) = COMI
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>

            IF CATEG >= 2000 AND CATEG <= 2999 THEN
                ETEXT = 'NO.VOSTRO.ACCOUNTS'
            END  ELSE
                IF CATEG >= 5000 AND CATEG <= 5999 THEN
*    IF CATEG # 5010 THEN ETEXT = '���� �� ���� ���� ���'
                END
            END

*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR('ACCOUNT':@FM:AC.CATEGORY,R.NEW(FT.DEBIT.ACCT.NO),CATEG1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG1=R.ITSS.ACCOUNT<AC.CATEGORY>

* IF CATEG1 >= 1000 AND CATEG1 <= 2000 THEN

*     IF CATEG >= 1000 AND CATEG <= 2000 THEN
*        ETEXT = "���� ����� ��� �� ���� ����"  ;CALL STORE.END.ERROR
*   END
* END ELSE
*    IF CATEG1 >= 5000 AND CATEG1 <= 5999 THEN
*       IF CATEG >= 5000 AND CATEG <= 5999 THEN
*          ETEXT = "���� ����� ��� �� ���� ���"
*
*    END

* END
*  END

*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,R.NEW(FT.DEBIT.ACCT.NO),DB.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DB.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CR.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
            IF  DB.CURR # CR.CURR   THEN
                ETEXT = '���� ���� ����� ���� �� ����� ���� ���� ������'
            END
        END
    END
    CALL REBUILD.SCREEN

    RETURN
END
