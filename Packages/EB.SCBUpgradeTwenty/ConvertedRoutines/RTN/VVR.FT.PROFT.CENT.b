* @ValidationCode : MjoxMTk1MjYxMjk1OkNwMTI1MjoxNjQ1MDA2NjI0NzI1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 16 Feb 2022 12:17:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0

* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*****CREATED BY MAI SAAD 11/02/2018*******
$PACKAGE EB.SCBUpgradeTwenty
SUBROUTINE  VVR.FT.PROFT.CENT
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY
    COM.CODE      = COMP[8,2]
    IF COM.CODE[1,1]  EQ '0' THEN
        COM.CODE  = COMP[9,1]
    END ELSE
        COM.CODE  = COMP[8,2]
    END
    CREDIT.ACC = COMI
    DEBIT.ACC =  R.NEW(FT.DEBIT.ACCT.NO)
    IF V$FUNCTION = 'I' THEN
        IF (LEN(DEBIT.ACC)) < 16 THEN
            PL.CAT = DEBIT.ACC[3,5]
            IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                IF (COMP EQ 'EG0010099') THEN
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                        TEXT ='��� ����� ��� �������' C;CALL REM
                    END
                END ELSE
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT) # 9940 THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
                    END
                END
            END ELSE
                IF (COMP EQ 'EG0010099') THEN
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT) = '' THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '99'
                    END
                END ELSE
** IF R.NEW(FT.PROFIT.CENTRE.DEPT) = 9940 THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = '99'
** END
                END
            END
        END
        IF (LEN(CREDIT.ACC)) < 16 THEN
            PL.CAT.C = CREDIT.ACC[3,5]
            IF (PL.CAT.C GE 52880 AND PL.CAT.C LE 52900) OR (PL.CAT.C GE 56001 AND PL.CAT.C LE 56019) OR (PL.CAT.C GE 60000 AND PL.CAT.C LE 69998 ) OR PL.CAT.C EQ 54036 THEN
                IF (COMP EQ 'EG0010099') THEN
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                        TEXT ='���� ����� ��� �������' C;CALL REM
                    END
                END ELSE
**    IF R.NEW(FT.PROFIT.CENTRE.DEPT) # 9940 THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
**    END
                END
            END ELSE
                IF (COMP EQ 'EG0010099') THEN
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT) = '' THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = 99
                    END
                END ELSE
**   IF R.NEW(FT.PROFIT.CENTRE.DEPT) = 9940 THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = 99
**   END
                END
            END
        END
    END

RETURN
END
