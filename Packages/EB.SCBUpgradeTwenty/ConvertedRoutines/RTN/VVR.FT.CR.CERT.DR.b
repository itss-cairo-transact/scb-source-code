* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
***----INGY15/03/2005----***

    SUBROUTINE VVR.FT.CR.CERT.DR

** TO PL CREATE ACCOUNT MASK ACCORDING TO CURRENCY


*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN
        IF V$FUNCTION = "I" THEN
            IF COMI THEN
*  R.NEW(FT.CREDIT.CURRENCY) = COMI
                CUR1 = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR1,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR1,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
                BRN.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF>
*  CUR = COMI
*  TEXT = "CUR=":CUR ;CALL REM
                CURR = R.NEW(FT.DEBIT.CURRENCY)
********UPDATED BY NESSREEN ON 07/07/2008************************
**  R.NEW(FT.DEBIT.ACCT.NO)= COMI:'1615200010001'
                CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
                R.NEW(FT.DEBIT.ACCT.NO)= COMI:'161520001':CO.CODE
*******************************************************************
* CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
