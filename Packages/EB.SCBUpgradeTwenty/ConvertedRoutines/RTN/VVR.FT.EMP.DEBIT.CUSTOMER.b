* @ValidationCode : MjozMzI4MDM0NTpDcDEyNTI6MTY0NTAwNjI2MTI5MTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 16 Feb 2022 12:11:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
*------------INGY---------*

SUBROUTINE VVR.FT.EMP.DEBIT.CUSTOMER

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG




    IF COMI # R.NEW(FT.DEBIT.CUSTOMER) THEN
        R.NEW(FT.CREDIT.CUSTOMER)=''
        R.NEW(FT.CREDIT.CURRENCY)=''
        R.NEW(FT.DEBIT.CURRENCY)=''
        R.NEW(FT.DEBIT.ACCT.NO) = ''
        R.NEW(FT.CREDIT.ACCT.NO) = ''
        R.NEW(FT.CREDIT.AMOUNT)=''
        R.NEW(FT.DEBIT.AMOUNT)=''
        R.NEW(FT.DEBIT.THEIR.REF)=''
        R.NEW(FT.CREDIT.THEIR.REF)=''

        CALL REBUILD.SCREEN
    END

    IF COMI THEN
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI, SEC)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*CALL DBR ('SCB.VER.IND.SEC.LEG':@FM:VISL.SECTOR,',SCB.BANK',SEC1)
        F.ITSS.SCB.VER.IND.SEC.LEG = 'F.SCB.VER.IND.SEC.LEG'
        FN.F.ITSS.SCB.VER.IND.SEC.LEG = ''
        CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG)
        CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG,SCB.BANK,R.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG,ERROR.SCB.VER.IND.SEC.LEG)
        SEC1=R.ITSS.SCB.VER.IND.SEC.LEG<VISL.SECTOR>
        LOCATE SEC IN SEC1<1,1,1> SETTING M THEN  ETEXT = 'SECTOR.OF.CUSTOMER.NOT.ALLOWED'
    END

RETURN
END
