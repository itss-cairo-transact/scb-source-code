* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*------------------------------------------------------------------------
* <Rating>-8</Rating>
*-------------------------------------------------------------------------
****MAI SAAD 13/03/2018*********
    SUBROUTINE VVR.FT.PROFT.CENTRE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

** IF V$FUNCTION = 'I' THEN
    IF MESSAGE EQ 'VAL' THEN
        COMP = C$ID.COMPANY
        COM.CODE      = COMP[8,2]
        IF COM.CODE[1,1]  EQ '0' THEN
            COM.CODE  = COMP[9,1]
        END ELSE
            COM.CODE  = COMP[8,2]
        END

        DB.ACC  = R.NEW(FT.DEBIT.ACCT.NO)
        IF (LEN(DB.ACC) < 16 ) THEN
            IF DB.ACC[1,2] EQ 'PL' THEN
                PL.CAT = DB.ACC[3,5]
                IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                    IF (COMP EQ 'EG0010099') THEN
**   IF R.NEW(FT.PROFIT.CENTRE.DEPT) = '' THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '99'
** END
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
                    END
                END ELSE
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END
            END
        END
***********************CREDIT*********************
        CR.ACC  = R.NEW(FT.CREDIT.ACCT.NO)
        IF (LEN(CR.ACC) < 16 ) THEN
            IF CR.ACC[1,2] EQ 'PL' THEN
                PL.CAT.C = CR.ACC[3,5]
                IF (PL.CAT.C GE 52880 AND PL.CAT.C LE 52900) OR (PL.CAT.C GE 56001 AND PL.CAT.C LE 56019) OR (PL.CAT.C GE 60000 AND PL.CAT.C LE 69998 ) OR PL.CAT.C EQ 54036 THEN
                    IF (COMP EQ 'EG0010099') THEN
**            IF R.NEW(FT.PROFIT.CENTRE.DEPT) = '' THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '99'
**            END
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '7070'
                    END
                END ELSE
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END
            END
        END
        RETURN
    END
** END
END
