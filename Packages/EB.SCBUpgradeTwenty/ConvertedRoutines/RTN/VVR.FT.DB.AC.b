* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*******************MAHMOUD 13/2/2012***********************
    SUBROUTINE VVR.FT.DB.AC

*1-CHECK IF DEBIT.ACCT IS A (PL)ACCOUNT THEN CREDIT.ACCT MUST BE A CUSTOMER ACCOUNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
FN.ACC = "FBNK.ACCOUNT" ; F.ACC = "" ; R.ACC = "" ; ER.ACC = ""
CALL OPF(FN.ACC,F.ACC)

    IF MESSAGE NE 'VAL' THEN
        ACC = COMI
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ER.ACC)
        POST = R.ACC<AC.POSTING.RESTRICT>
        CUST = R.ACC<AC.CUSTOMER>
        CURR = R.ACC<AC.CURRENCY>
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
        IF POST # "" THEN
            ETEXT = DESC ;CALL STORE.END.ERROR
        END ELSE
            R.NEW(FT.DEBIT.CUSTOMER) = CUST
            R.NEW(FT.DEBIT.CURRENCY) = CURR
            R.NEW(FT.CREDIT.CURRENCY) = CURR
            IF AF = FT.DEBIT.ACCT.NO THEN
                IF COMI AND R.NEW(FT.CREDIT.ACCT.NO) THEN
                    IF COMI[1,2] EQ 'PL' AND R.NEW(FT.CREDIT.ACCT.NO)[1,2] = 'PL' THEN
                        ETEXT = '���� �� ���� ���� ����'
                    END
                    IF NUM(COMI) AND NUM(R.NEW(FT.CREDIT.ACCT.NO)) THEN
                        ETEXT = '���� �� ���� ���� ����� ������'
                    END

                END

            END

            IF AF = FT.CREDIT.ACCT.NO THEN
                IF COMI AND R.NEW(FT.DEBIT.ACCT.NO) THEN
                    IF COMI[1,2] EQ 'PL' AND R.NEW(FT.DEBIT.ACCT.NO)[1,2] = 'PL' THEN
                        ETEXT = '���� �� ���� ���� ����'
                    END
                    IF NUM(COMI) AND NUM(R.NEW(FT.DEBIT.ACCT.NO)) THEN
                        ETEXT = '���� �� ���� ���� ����� ������'
                    END
                END
            END

        END
    END
    RETURN
END
