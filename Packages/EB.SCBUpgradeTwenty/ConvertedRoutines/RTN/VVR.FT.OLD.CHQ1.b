* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
****23/10/2003 ABEER *************
*-----------------------------------------------------------------------------
* <Rating>641</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.OLD.CHQ1

** To Cheqck IF Cheq Is Stopped
** To Check IF Cheq Is Paid
** To Issue A Cheq Record In Cheque.Issue
** To Issue A Cheq Record In Cheque.Issue

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF MESSAGE NE 'VAL' THEN
        IF COMI THEN
            ACCT.NO =R.NEW(FT.DEBIT.ACCT.NO)
*****************UPDATED BY RIHAM R15************
*            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,MYBRN)
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACCT.NO,MYBRN1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
MYBRN1=R.ITSS.ACCOUNT<AC.CO.CODE>
            CUS.BR = MYBRN1[8,2]
            MYBRN = TRIM(CUS.BR, "0" , "L")
*********************************************
            PAY.CHQ.ID= COMI:'.':MYBRN
            CHQ.ISSUE.ID='SCB':'.':ACCT.NO:'...'

*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACCT.NO,MYCURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
MYCURR=R.ITSS.ACCOUNT<AC.CURRENCY>
            CHQ='SCB':'.':ACCT.NO:'...'
************To Cheqck IF Cheq Is Stopped *************************
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('PAYMENT.STOP':@FM:AC.PAY.FIRST.CHEQUE.NO,ACCT.NO,MYCHECK)
F.ITSS.PAYMENT.STOP = 'FBNK.PAYMENT.STOP'
FN.F.ITSS.PAYMENT.STOP = ''
CALL OPF(F.ITSS.PAYMENT.STOP,FN.F.ITSS.PAYMENT.STOP)
CALL F.READ(F.ITSS.PAYMENT.STOP,ACCT.NO,R.ITSS.PAYMENT.STOP,FN.F.ITSS.PAYMENT.STOP,ERROR.PAYMENT.STOP)
MYCHECK=R.ITSS.PAYMENT.STOP<AC.PAY.FIRST.CHEQUE.NO>
            LOCATE COMI IN  MYCHECK<1,1>  SETTING I THEN
                ETEXT='This.Cheque.Already.Stopped'
            END ELSE
*********** To Check IF Cheq Is Paid ******************************
*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCNO)
F.ITSS.SCB.P.CHEQ = 'F.SCB.P.CHEQ'
FN.F.ITSS.SCB.P.CHEQ = ''
CALL OPF(F.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ)
CALL F.READ(F.ITSS.SCB.P.CHEQ,PAY.CHQ.ID,R.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ,ERROR.SCB.P.CHEQ)
MYACCNO=R.ITSS.SCB.P.CHEQ<P.CHEQ.ACCOUNT.NO>
                IF NOT(ETEXT) THEN
                    ETEXT='This.Cheque.Aleardy.Paid'
                END  ELSE
**********To Issue A Cheq Record In Cheque.Issue*********************
                    ETEXT=''
                    T.SEL ="SSELECT FBNK.CHEQUE.ISSUE WITH @ID LIKE ": CHQ
                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)


                    IF SELECTED EQ '0' THEN
                        NO.ISSUED='1'
                        CHQ.START='1'
                    END ELSE
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                        CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.CHQ.NO.START,KEY.LIST<SELECTED>,CHECK.ST.NO)
F.ITSS.CHEQUE.ISSUE = 'FBNK.CHEQUE.ISSUE'
FN.F.ITSS.CHEQUE.ISSUE = ''
CALL OPF(F.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE)
CALL F.READ(F.ITSS.CHEQUE.ISSUE,KEY.LIST<SELECTED>,R.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE,ERROR.CHEQUE.ISSUE)
CHECK.ST.NO=R.ITSS.CHEQUE.ISSUE<CHEQUE.IS.CHQ.NO.START>
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                        CALL DBR ('CHEQUE.ISSUE':@FM:CHEQUE.IS.NUMBER.ISSUED,KEY.LIST<SELECTED>,NO.ISSUED)
F.ITSS.CHEQUE.ISSUE = 'FBNK.CHEQUE.ISSUE'
FN.F.ITSS.CHEQUE.ISSUE = ''
CALL OPF(F.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE)
CALL F.READ(F.ITSS.CHEQUE.ISSUE,KEY.LIST<SELECTED>,R.ITSS.CHEQUE.ISSUE,FN.F.ITSS.CHEQUE.ISSUE,ERROR.CHEQUE.ISSUE)
NO.ISSUED=R.ITSS.CHEQUE.ISSUE<CHEQUE.IS.NUMBER.ISSUED>

                        NEW.CHQ.STRT.NO= CHECK.ST.NO+NO.ISSUED
                        NO.ISSUED='1'
                        CHQ.START= NEW.CHQ.STRT.NO
                    END

*********************************************************************
                    IF R.NEW(FT.CHEQUE.NUMBER) EQ '' THEN
                        R.NEW(FT.CHEQUE.NUMBER)= CHQ.START
                        T(FT.CHEQUE.NUMBER)<3> = 'NOINPUT'
                        CALL REBUILD.SCREEN
                    END
                    ELSE
                        IF R.NEW(FT.CHEQUE.NUMBER) NE '' AND MESSAGE NE 'VAL' THEN
                            ETEXT='No.Input.Is.Allowed.In.Cheq.NO'
                            CALL REBUILD.SCREEN
                        END
                    END
**********************************************************************
* IF MESSAGE EQ 'VAL' THEN
                    IF NOT(ETEXT) THEN
                        FN.P.CHQ='F.SCB.P.CHEQ';ID=PAY.CHQ.ID;R.P.CHQ='';F.P.CHQ=''
                        CALL F.READ(FN.P.CHQ,ID,R.P.CHQ,F.P.CHQ,ETEXT)
                        R.P.CHQ<P.CHEQ.CHEQ.VAL>=R.NEW(FT.DEBIT.AMOUNT)
                        R.P.CHQ<P.CHEQ.ACCOUNT.NO>=R.NEW(FT.DEBIT.ACCT.NO)
                        R.P.CHQ<P.CHEQ.OLD.KEY>=COMI
****UPDATED BY NESSREEN AHMED 16/03/2009*****************************
                        COMP = C$ID.COMPANY
                        R.P.CHQ<P.CHEQ.COMPANY.CO> = COMP
*********************************************************************
                        CALL F.WRITE(FN.P.CHQ,ID,R.P.CHQ)
************UPDATED BY NESSREEN 16/03/2009******************************************
**    CALL VAR.FT.OFS1(ACCT.NO,NO.ISSUED,CHQ.START)
**********************************************************************
                        R.NEW(FT.CHEQUE.NUMBER)= CHQ.START
                        TEXT=CHQ.START:'CHQ.START';CALL REM
********************************************************************
                    END
* END
********************************************************************
                END
            END
        END
    END
    RETURN
END
