* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.DR.AMOUNT
*The routine make sure that the entered amount is the same as in the teller transaction if the field teller
*ref is entered
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS


    IF R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> THEN
        IF R.NEW(FT.DEBIT.CURRENCY)= 'EGP' THEN
            CALL DBR( 'TELLER':@FM:TT.TE.AMOUNT.LOCAL.1, R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF>,AMOUNT)
        END ELSE
            CALL DBR( 'TELLER':@FM:TT.TE.AMOUNT.FCY.1, R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF>,AMOUNT)
        END
        IF COMI # AMOUNT  THEN ETEXT = 'WRONG'

    END
    IF COMI # R.NEW(FT.DEBIT.AMOUNT) THEN
        R.NEW(FT.CREDIT.AMOUNT) = ''
    END
    IF COMI THEN
        DEBIT.ACC = R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,DEBIT.ACC,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
        IF COMI GT WORK.BALANCE THEN
            ETEXT = '���� ������ ������'
        END
    END


    RETURN
END
