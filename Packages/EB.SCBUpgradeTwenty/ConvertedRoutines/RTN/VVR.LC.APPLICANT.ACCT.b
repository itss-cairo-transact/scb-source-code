* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
***********INGY&DALIA-SCB 11/05/2003***********

    SUBROUTINE VVR.LC.APPLICANT.ACCT
* A ROUTINE TO DEFAULT THE FIRST MULTIVALUE WITH THE CUSTOMER NAME

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

    APPL.ACCT = R.NEW(TF.LC.APPLICANT.ACC)
*    TEXT = "APPL.ACCT":APPL.ACCT ; CALL REM
    ACCT.NO  = R.NEW(TF.LC.APPLICANT.ACC)[1,10]
    SERIAL   = R.NEW(TF.LC.APPLICANT.ACC)[15,2]

   TEXT = 'YAAAARAB' ; CALL REM
***02/01/2007 NESSREEN AHMED *****
    IF MESSAGE = '' THEN
**********************************
*IF MESSAGE = '' THEN
*  IF PGM.VERSION # ",SCB.LIP" THEN
*     R.NEW(TF.LC.PROVIS.ACC) = APPL.ACCT
*    R.NEW(TF.LC.CREDIT.PROVIS.ACC) = ACCT.NO:'1076':SERIAL
*END
        IF R.NEW(TF.LC.WAIVE.CHARGES) = 'NO' THEN
*Line [ 46 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            KK = DCOUNT(R.NEW(TF.LC.CHARGE.CODE),@VM)
            FOR I = 1 TO KK

                R.NEW(TF.LC.CHARGE.ACCT)<1,I> = APPL.ACCT
           *  TEXT = "CHARGE.ACCT":R.NEW(TF.LC.CHARGE.ACCT)<1,I> ; CALL REM
                R.NEW(TF.LC.CHARGE.STATUS)<1,I> = '2'
            NEXT I
            CALL REBUILD.SCREEN ; P = 0
* IF MESSAGE = 'VAL' THEN
*   IF R.NEW(TF.LC.PROVISION) = 'NO' THEN
*    R.NEW(TF.LC.PROVIS.ACC) = ''
*   R.NEW(TF.LC.CREDIT.PROVIS.ACC) = ''
*END
*CALL REBUILD.SCREEN ; P = 0
        END

* END
    END
    RETURN
END
