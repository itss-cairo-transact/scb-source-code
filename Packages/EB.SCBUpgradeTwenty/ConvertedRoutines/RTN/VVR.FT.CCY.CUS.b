* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CCY.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.CCY.CUS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    IF COMI = LCCY THEN ETEXT = '���� ������ ���'
    ETEXT    = ''
    FN.CCY   = 'F.SCB.FT.CCY.CUS'    ; F.CCY   = '' ; R.CCY  = ''
    CALL OPF(FN.CCY,F.CCY)
    KEY.LIST= "" ; SELECTED = "" ; ER.MSG = ""

    CUS       = R.NEW(FT.DEBIT.CUSTOMER)
    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    CALL F.READ(FN.CUS,CUS,R.CUS,F.CUS,E)

    IF R.CUS<EB.CUS.NATIONALITY> EQ 'EG' THEN
        IF R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR> EQ '4650' THEN


            CUR       = COMI
            REF       = ID.NEW
            AMT       = R.NEW(FT.AMOUNT.CREDITED)[4,10]
            FT.TYPE   = "OUT"

            IDD.2 = CUS:"-":CUR
            CALL F.READ(FN.CCY,IDD.2,R.CCY,F.CCY,E)

            OLD.AMT = R.CCY<CUS.CCY.TOT.AMT>

            TEXT = " ���� ������ �� �������� = "  : OLD.AMT ; CALL REM
        END
    END

    RETURN
END
