* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.DEF.GOODS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

*To Default Credit account by 994999:Brn.no:Currency:510105
    IF V$FUNCTION = "I" THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(FT.CREDIT.CURRENCY),CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.NEW(FT.CREDIT.CURRENCY),R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
        BRN.NO = COMI
        CURR = R.NEW(FT.DEBIT.CURRENCY)
********UPDATED BY NESSREEN AHMED 07/07/2008*****************
**  R.NEW(FT.CREDIT.ACCT.NO)= CURR:'1615100010001'
        CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
        R.NEW(FT.CREDIT.ACCT.NO)= CURR:'161510001':CO.CODE
*************************************************************
***************ADDED BY MAI 07/02/2018**********
        COMP = C$ID.COMPANY
        COM.CODE      = COMP[8,2]
        IF COM.CODE[1,1]  EQ '0' THEN
            COM.CODE  = COMP[9,1]
        END ELSE
            COM.CODE  = COMP[8,2]
        END

        DEBIT.ACC = R.NEW(FT.DEBIT.ACCT.NO)
        IF (DEBIT.ACC[1,2] EQ 'PL') THEN
            PL.CAT = DEBIT.ACC[3,5]
            IF (PL.CAT GE 52880 AND PL.CAT LE 52900) OR (PL.CAT GE 56001 AND PL.CAT LE 56019) OR (PL.CAT GE 60000 AND PL.CAT LE 69998 ) OR PL.CAT EQ 54036 THEN
                IF (COMP NE 'EG0010099') THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = 7070
                END ELSE
                    IF R.NEW(FT.PROFIT.CENTRE.DEPT)= '' THEN
                        TEXT ='��� ����� ��� �������' C;CALL REM
                    END
                END
            END ELSE
                IF (COMP NE 'EG0010099') THEN
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END ELSE
                    IF(R.NEW(FT.PROFIT.CENTRE.DEPT) = '') THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = '99'
                    END
                END
            END
        END ELSE
            IF (COMI # '') THEN
                IF COMI[1,1] = '0' THEN
                    ACCT.OFF = COMI[2,1]
                    IF (COMP EQ 'EG0010099') THEN
                        IF(R.NEW(FT.PROFIT.CENTRE.DEPT) = '') THEN
                            R.NEW(FT.PROFIT.CENTRE.DEPT) = ACCT.OFF
                        END
                    END ELSE
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = ACCT.OFF
                    END
                END ELSE
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COMI
                END
            END ELSE
                IF (COMP EQ 'EG0010099') THEN
                    IF(R.NEW(FT.PROFIT.CENTRE.DEPT) = '') THEN
                        R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                    END
                END ELSE
                    R.NEW(FT.PROFIT.CENTRE.DEPT) = COM.CODE
                END
            END
        END
*****************************
    END
    CALL REBUILD.SCREEN

    RETURN
END
