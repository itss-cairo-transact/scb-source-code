* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***----INGY----***
*-----------------------------------------------------------------------------
* <Rating>598</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CR.IT.TELEX

*check if the currency in the Debit.Acc.No is the same as Credit.Acc.No or not and
* to check that the Debit.Acc.No is not same as Credit.Acc.No
*to check that if it is internal account it is not an teller transaction
*to check that it is not nostro or vostro accounts

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF COMI THEN

*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, COMI ,CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF ETEXT THEN E = ETEXT

*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, COMI ,CR.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
        IF ETEXT THEN E = ETEXT

        IF NUM(COMI[1,3]) THEN
           *IF CR.CATEG >= 2000 AND CR.CATEG <= 2999  OR  CR.CATEG >= 5000 AND CR.CATEG <= 5999 THEN ETEXT = '��� ����� ������� �������� � ��������' ; RETURN
            IF CR.CURR = 'EGP' THEN
                R.NEW(FT.COMMISSION.CODE) = 'WAIVE'
                R.NEW(FT.CHARGE.CODE) = 'WAIVE'

            END ELSE
                R.NEW(FT.COMMISSION.CODE) = 'CREDIT LESS CHARGE'
                R.NEW(FT.CHARGE.CODE) = 'WAIVE'
            END
        END ELSE
            IF CR.CATEG = 10000 THEN ETEXT = '��� ����� ������� �������' ; RETURN
            R.NEW(FT.COMMISSION.CODE) = 'WAIVE'
            R.NEW(FT.CHARGE.CODE) = 'WAIVE'
        END


        IF R.NEW(FT.DEBIT.ACCT.NO) THEN

*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, R.NEW(FT.DEBIT.ACCT.NO) ,DB.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DB.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
            IF ETEXT THEN E = ETEXT

*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, R.NEW(FT.DEBIT.ACCT.NO) ,DB.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DB.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
            IF ETEXT THEN E = ETEXT


            IF COMI=R.NEW(FT.DEBIT.ACCT.NO) THEN ETEXT = '���� �� ���� ����� �� ���� �����' ; RETURN ;
   * IF CR.CURR # DB.CURR THEN ETEXT = '���� ������� ���� �� ��� ���� �����' ; RETURN
    *    END

    END
    CALL REBUILD.SCREEN

    RETURN
END
