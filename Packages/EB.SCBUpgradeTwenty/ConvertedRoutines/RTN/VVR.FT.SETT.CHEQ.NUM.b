* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>246</Rating>
*-----------------------------------------------------------------------------
***------INGY 15/03/2005------***

    SUBROUTINE VVR.FT.SETT.CHEQ.NUM
*1- TO CHEACK IF THE CHEQUE IS PRESENTED OR NOT
*2- TO CHEACK IF THE CHEQUE IS ALREADY PAIED

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ

***------------CHEQUE.ISSUE---------------***
    FT.DR.CHQ.ID = R.NEW(FT.CREDIT.ACCT.NO):'.':COMI
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('SCB.FT.DR.CHQ':@FM:DR.CHQ.CHEQ.NO,FT.DR.CHQ.ID,CHQ.NO)
F.ITSS.SCB.FT.DR.CHQ = 'F.SCB.FT.DR.CHQ'
FN.F.ITSS.SCB.FT.DR.CHQ = ''
CALL OPF(F.ITSS.SCB.FT.DR.CHQ,FN.F.ITSS.SCB.FT.DR.CHQ)
CALL F.READ(F.ITSS.SCB.FT.DR.CHQ,FT.DR.CHQ.ID,R.ITSS.SCB.FT.DR.CHQ,FN.F.ITSS.SCB.FT.DR.CHQ,ERROR.SCB.FT.DR.CHQ)
CHQ.NO=R.ITSS.SCB.FT.DR.CHQ<DR.CHQ.CHEQ.NO>
    IF CHQ.NO = COMI THEN
        ETEXT = '�������� �� ������ �� ���' ; CALL STORE.END.ERROR
    END
***------------CHEQUE.REGISTER------------***
    T.SEL = "SELECT FBNK.CHEQUE.REGISTER WITH @ID LIKE  ": '...':R.NEW(FT.CREDIT.ACCT.NO)
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        F.CHQ = '' ; FN.CHQ = 'FBNK.CHEQUE.REGISTER'
        CALL OPF(FN.CHQ,F.CHQ)
        R.CHQ = ''
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CHQ, KEY.LIST<I>, R.CHQ, F.CHQ, READ.ERROR)
            CHQ.NUM = R.CHQ<CHEQUE.REG.CHEQUE.NOS><1,I>

            X = FIELD(CHQ.NUM,"-",1)
            Y = FIELD(CHQ.NUM,"-",2)
            IF COUNT(CHQ.NUM,'-') GT 0 THEN

                IF COMI LT X OR COMI GT Y  THEN
                    ETEXT='�� ��� ������ ���';CALL STORE.END.ERROR
                END
            END
* IF COMI GT Y AND COMI # X THEN
*     ETEXT = 'OUT OF RANGE' ;  CALL STORE.END.ERROR
* END
* IF COMI < X  THEN
*     ETEXT = 'OUT OF RANGE' ;  CALL STORE.END.ERROR
* END
            IF  R.CHQ<CHEQUE.REG.STOPPED.CHQS> = COMI   THEN ETEXT = 'CHEQUE STOPPED'  ;  CALL STORE.END.ERROR
            IF  R.CHQ<CHEQUE.REG.PRESENTED.CHQS> = COMI THEN ETEXT = 'CHEQUE PRESENTED' ;  CALL STORE.END.ERROR
        NEXT I
    END ELSE
        ETEXT = 'CHEQ.NUM.NOT.FOUND'
    END
    CLOSE F.CHQ
***------------CHEQUES.PRESENTED------------***
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = 'SETT':'.':R.NEW(FT.CREDIT.ACCT.NO):'-':COMI
    CHQ.ID = 'SETT':'.':R.NEW(FT.CREDIT.ACCT.NO):'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED,CHQ.ID,PRESENT)
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED,CHQ.ID,PRESENT)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
PRESENT=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.PRESENTED>
****END OF UPDATE 7/3/2016*****************************
    IF PRESENT THEN
        ETEXT = '�� ���� �� ���' ; CALL STORE.END.ERROR
    END ELSE
        ETEXT = ''
    END


    RETURN
END
