* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
*********DINA********
***------INGY 13/03/2005------***
*to defualt charge type with the type defined in the table
*FT.TXN.TYPE.CONDITION for the type of transaction

    SUBROUTINE VVR.FT.TRANS.CHARGE

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    IF V$FUNCTION = 'I' AND MESSAGE NE 'VAL' THEN
        IF COMI = "DEBIT PLUS CHARGES" OR COMI = "CREDIT LESS CHARGES" THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('FT.TXN.TYPE.CONDITION':@FM:FT6.CHARGE.TYPES,R.NEW(FT.TRANSACTION.TYPE),CHR)
F.ITSS.FT.TXN.TYPE.CONDITION = 'F.FT.TXN.TYPE.CONDITION'
FN.F.ITSS.FT.TXN.TYPE.CONDITION = ''
CALL OPF(F.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION)
CALL F.READ(F.ITSS.FT.TXN.TYPE.CONDITION,R.NEW(FT.TRANSACTION.TYPE),R.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION,ERROR.FT.TXN.TYPE.CONDITION)
CHR=R.ITSS.FT.TXN.TYPE.CONDITION<FT6.CHARGE.TYPES>
            IF CHR THEN
*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                XX= DCOUNT(CHR , @VM)
                FOR I = 1 TO XX
                    R.NEW(FT.CHARGE.TYPE)<1,I> = CHR<1,I>
                NEXT I
                CALL REBUILD.SCREEN
            END
            IF NUM(R.NEW(FT.DEBIT.ACCT.NO)) THEN
                R.NEW(FT.CHARGES.ACCT.NO) = R.NEW(FT.DEBIT.ACCT.NO)[1,8]:'10':R.NEW(FT.DEBIT.ACCT.NO)[11,6]
            END
        END
    END


    RETURN
END
