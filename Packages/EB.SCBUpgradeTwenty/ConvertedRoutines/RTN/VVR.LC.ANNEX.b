* @ValidationCode : MjozNTQ3MDI3NzE6Q3AxMjUyOjE2NDUwMDY5MjMzNjA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Feb 2022 12:22:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>350</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VVR.LC.ANNEX
* A ROUTINE TO MAKE SURE THAT THE ID OF LCLR.ANNEX11 IS NEW

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LC.ANNEX.INDEX
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LC.LOCAL.REFS

    IF MESSAGE = 'VAL' THEN
        IF COMI  THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   CALL DBR("LETTER.OF.CREDIT":@FM:TF.LC.LOCAL.REF,ID.NEW[1,12],LOCAL)
            F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
            FN.F.ITSS.LETTER.OF.CREDIT = ''
            CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
            CALL F.READ(F.ITSS.LETTER.OF.CREDIT,12,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
            LOCAL=R.ITSS.LETTER.OF.CREDIT<TF.LC.LOCAL.REF,ID.NEW[1,12]>
            LL = LOCAL<1,LCLR.LOCAL.FOREIGN >
            IF LL = 'Foreign' THEN
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR("SCB.LC.ANNEX.INDEX":@FM:SCB.LC.ANDX.LC.ID,COMI,ID)
                F.ITSS.SCB.LC.ANNEX.INDEX = 'F.SCB.LC.ANNEX.INDEX'
                FN.F.ITSS.SCB.LC.ANNEX.INDEX = ''
                CALL OPF(F.ITSS.SCB.LC.ANNEX.INDEX,FN.F.ITSS.SCB.LC.ANNEX.INDEX)
                CALL F.READ(F.ITSS.SCB.LC.ANNEX.INDEX,COMI,R.ITSS.SCB.LC.ANNEX.INDEX,FN.F.ITSS.SCB.LC.ANNEX.INDEX,ERROR.SCB.LC.ANNEX.INDEX)
                ID=R.ITSS.SCB.LC.ANNEX.INDEX<SCB.LC.ANDX.LC.ID>
                IF ETEXT THEN ETEXT = ""
                ELSE
                    IF ID.NEW # ID THEN ETEXT = 'Annex.Must.Be.New'
                END
            END  ELSE
                COMI = ''
                CALL REBUILD.SCREEN
            END
        END
    END
RETURN
END
