* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.GET.NAME.BOAT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.DATA
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER

    FN.BOD = 'F.SCB.BOAT.TOTAL' ; F.BOD = ''
    CALL OPF(FN.BOD,F.BOD)
    BOT.ID = R.NEW(BO.BOT.ID)
    CUR = R.NEW(BO.CURRENCY)

    T.SEL = "SELECT ":FN.BOD:" WITH @ID EQ ":BOT.ID:" AND BOAT.CODE EQ ":COMI:" AND CURRENCY EQ ":CUR
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.BOD,KEY.LIST,R.BOD,F.BOD,E1)
        BTT = R.BOD<BOT.BOAT.CODE>

*Line [ 41 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE COMI IN BTT<1,1> SETTING POS ELSE NULL
        IF R.BOD<BOT.REG.CUS,POS> EQ 0 AND R.BOD<BOT.REG.AMT,POS> EQ 0 THEN
            ETEXT = "�� ����� ������ �������"
        END ELSE
            R.NEW(BO.BOAT.NAME) = R.BOD<BOT.BOAT.NAME,POS>
            R.NEW(BO.REG.CUST)  = R.BOD<BOT.REG.CUS,POS>
            R.NEW(BO.REG.AMT)   = R.BOD<BOT.REG.AMT,POS>
        END
    END ELSE
        ETEXT = "��� ������ ��� ����� ��������"
    END

    CALL REBUILD.SCREEN

    RETURN
END
