* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>89</Rating>
*-----------------------------------------------------------------------------
***----INGY14/03/2005----***

    SUBROUTINE VVR.FCY.COMM

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION


** IF CURRENCY # EGP THEN DEFULT COMMISSION.AMT

    COMM1 = ''
    CUR = R.NEW(FT.DEBIT.CURRENCY)
    IF CUR # 'EGP' THEN
        TRAN = R.NEW(FT.TRANSACTION.TYPE)
        AMT  = R.NEW(FT.DEBIT.AMOUNT)
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('FT.TXN.TYPE.CONDITION':@FM:FT6.COMM.TYPES,TRAN,COMM)
F.ITSS.FT.TXN.TYPE.CONDITION = 'F.FT.TXN.TYPE.CONDITION'
FN.F.ITSS.FT.TXN.TYPE.CONDITION = ''
CALL OPF(F.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION)
CALL F.READ(F.ITSS.FT.TXN.TYPE.CONDITION,TRAN,R.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION,ERROR.FT.TXN.TYPE.CONDITION)
COMM=R.ITSS.FT.TXN.TYPE.CONDITION<FT6.COMM.TYPES>
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.PERCENTAGE,COMM,PER)
F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
FN.F.ITSS.FT.COMMISSION.TYPE = ''
CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,COMM,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
PER=R.ITSS.FT.COMMISSION.TYPE<FT4.PERCENTAGE>
        IF PER THEN
            PER1 = PER<1,1>
            COMM1 = SMUL(PER1,AMT)
            IF R.NEW(FT.COMMISSION.AMT) THEN
                IF COMM1 < '5.00' THEN
                    R.NEW(FT.COMMISSION.AMT) = CUR:'5.00'

                END ELSE
                    R.NEW(FT.COMMISSION.AMT) = CUR:COMM1
                END
            END
        END

    END

    RETURN
END
