* @ValidationCode : MjoxMjk2MTMxMDk3OkNwMTI1MjoxNjQxNDAwNTI2MzQxOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 05 Jan 2022 18:35:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.BRN.1
* TO PL CREATE ACCOUNT MASK ACCORDING TO CURRENCY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS

    IF MESSAGE # "VAL" THEN
        IF V$FUNCTION = "I" THEN
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE, COMI ,CUR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,COMI,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
*     TEXT = 'COMI': COMI ; CALL REM
*    TEXT = 'CUR': CUR ; CALL REM
            BRN.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF>
*BRN.NO = COMI
* R.NEW(FT.DEBIT.ACCT.NO)= '994999':BRN.NO:CUR:'510101'
**********UPDATED BY NESSREEN ON 06/07/2008**********************
**   R.NEW(FT.DEBIT.ACCT.NO)= COMI:'1615100010001'
            CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
            R.NEW(FT.DEBIT.ACCT.NO)= COMI:'161510001':CO.CODE
******************************************************************
            CALL REBUILD.SCREEN
        END
    END
RETURN
END
