* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*******************************NI7OOOOOO*************
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.INPUTTER2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER


* INPUTT = R.NEW(DEPT.SAMP.INPUTTER)
* INP    = FIELD(INPUTT,'_',2)
* AUTHI  = R.USER<EB.USE.SIGN.ON.NAME>
* CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTHI,AUTH)
    OVER = R.NEW(DEPT.SAMP.OVERRIDE)
* FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
*     R.NEW(DEPT.SAMP.AUTH.NAME.HWALA)  = AUTH
*     R.NEW(DEPT.SAMP.INPUT.NAME.HWALA) = INP
* END
*INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>
*CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)
*CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)

    FINDSTR 'MAR2' IN OVER SETTING FMS,VMS THEN

        INPUTT2 = R.NEW(DEPT.SAMP.INPUTTER)

*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD.INP  = DCOUNT(INPUTT2,@VM)
        FOR I = 1 TO DD.INP
            INP.NEW = R.NEW(DEPT.SAMP.INPUTTER)<1,I>
            INP2    = FIELD(INP.NEW,'_',2)
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP2,DEPT.CODE)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INP2,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT.CODE=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
            IF DEPT.CODE EQ 99 THEN
                R.NEW(DEPT.SAMP.INPUT.NAME.HWALA.MAR) = INP2
                TEXT = "3" : R.NEW(DEPT.SAMP.INPUT.NAME.HWALA.MAR)
            END
            IF DEPT.CODE EQ 99 THEN
                R.NEW(DEPT.SAMP.INPUT.NAME.WH.MAR) = INP2
                TEXT = "3" : R.NEW(DEPT.SAMP.INPUT.NAME.WH.MAR)
            END

        NEXT I
        AUTHI2  = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTHI2,AUTH2)
F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
FN.F.ITSS.USER.SIGN.ON.NAME = ''
CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTHI2,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
AUTH2=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
        R.NEW(DEPT.SAMP.AUTH2.NAME.HWALA.MAR)  = AUTH2
        R.NEW(DEPT.SAMP.AUTH2.NAME.WH.MAR)  = AUTH2
    END

RETURN
END
