* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.GET.HMM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MAINMENU
    COMP = ID.COMPANY

    FN.UA = 'F.USER.ABBREVIATION' ; F.UA = ''
    CALL OPF(FN.UA,F.UA)

    WS.TODAY     = TODAY
    IF NUM(COMI) THEN
        IF COMP NE 'EG0010099' THEN
            IF (( COMI LT 400 ) OR ( COMI GT 499 )) THEN
                ETEXT = '�� ���� ������� ��� ������� ������'
                CALL STORE.END.ERROR
            END ELSE
                WS.ID.HMM  = COMI

*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR ('HELPTEXT.MAINMENU':@FM:EB.MME.TITLE,WS.ID.HMM,WS.HMM.TITLE)
F.ITSS.HELPTEXT.MAINMENU = 'F.HELPTEXT.MAINMENU'
FN.F.ITSS.HELPTEXT.MAINMENU = ''
CALL OPF(F.ITSS.HELPTEXT.MAINMENU,FN.F.ITSS.HELPTEXT.MAINMENU)
CALL F.READ(F.ITSS.HELPTEXT.MAINMENU,WS.ID.HMM,R.ITSS.HELPTEXT.MAINMENU,FN.F.ITSS.HELPTEXT.MAINMENU,ERROR.HELPTEXT.MAINMENU)
WS.HMM.TITLE=R.ITSS.HELPTEXT.MAINMENU<EB.MME.TITLE>
                WS.HMM.ID = '?' : WS.ID.HMM

                R.NEW(EB.UAB.ORIGINAL.TEXT)<1,AV> = WS.HMM.ID
                CALL REBUILD.SCREEN
                COMI = WS.HMM.TITLE
                CALL REBUILD.SCREEN
            END
        END ELSE

            WS.ID.HMM  = COMI

*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('HELPTEXT.MAINMENU':@FM:EB.MME.TITLE,WS.ID.HMM,WS.HMM.TITLE)
F.ITSS.HELPTEXT.MAINMENU = 'F.HELPTEXT.MAINMENU'
FN.F.ITSS.HELPTEXT.MAINMENU = ''
CALL OPF(F.ITSS.HELPTEXT.MAINMENU,FN.F.ITSS.HELPTEXT.MAINMENU)
CALL F.READ(F.ITSS.HELPTEXT.MAINMENU,WS.ID.HMM,R.ITSS.HELPTEXT.MAINMENU,FN.F.ITSS.HELPTEXT.MAINMENU,ERROR.HELPTEXT.MAINMENU)
WS.HMM.TITLE=R.ITSS.HELPTEXT.MAINMENU<EB.MME.TITLE>
            WS.HMM.ID = '?' : WS.ID.HMM

            R.NEW(EB.UAB.ORIGINAL.TEXT)<1,AV> = WS.HMM.ID
            CALL REBUILD.SCREEN
            COMI = WS.HMM.TITLE
            CALL REBUILD.SCREEN
        END
        RETURN
    END
