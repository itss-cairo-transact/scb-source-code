* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
    SUBROUTINE VVR.GET.SERIAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COL.CHEQ
*-------------------------------------------
    IF COMI THEN
        FN.TMP = "F.SCB.COL.CHEQ" ; F.TMP = ""
        CALL OPF(FN.TMP, F.TMP)
        CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ERR)
        CUS.ID = FIELD(ID.NEW,".",1)
        T.SEL = "" ; SELECTED = "" ; KEY.LIST = "" ; ER.MSG = ""
        T.SEL = "SELECT ":FN.TMP:" WITH CUST.ID LIKE ":CUS.ID:"... BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            TMP.ID      = KEY.LIST<SELECTED>
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('SCB.COL.CHEQ':@FM:COL.END.SER,TMP.ID,LAST.SERIAL)
F.ITSS.SCB.COL.CHEQ = 'F.SCB.COL.CHEQ'
FN.F.ITSS.SCB.COL.CHEQ = ''
CALL OPF(F.ITSS.SCB.COL.CHEQ,FN.F.ITSS.SCB.COL.CHEQ)
CALL F.READ(F.ITSS.SCB.COL.CHEQ,TMP.ID,R.ITSS.SCB.COL.CHEQ,FN.F.ITSS.SCB.COL.CHEQ,ERROR.SCB.COL.CHEQ)
LAST.SERIAL=R.ITSS.SCB.COL.CHEQ<COL.END.SER>
            LAST.SERIAL = FIELD(LAST.SERIAL,".",2)
            VAR.1 = LAST.SERIAL + 1
            VAR.2 = LAST.SERIAL + COMI
            VAR.1.F = FMT(VAR.1, "R%4")
            VAR.2.F = FMT(VAR.2, "R%4")
            R.NEW(COL.START.SER) = VAR.1
            R.NEW(COL.END.SER)   = VAR.2
            R.NEW(COL.START.SER) = CUS.ID :".": VAR.1.F
            R.NEW(COL.END.SER)   = CUS.ID :".": VAR.2.F
            CALL REBUILD.SCREEN
        END ELSE
            R.NEW(COL.START.SER) = 1
            R.NEW(COL.END.SER)   = COMI - 1

            R.NEW(COL.START.SER) = CUS.ID :".": "0001"
            VAR.3 = COMI
            VAR.3.F = FMT(VAR.3, "R%4")
            R.NEW(COL.END.SER)   = CUS.ID :".": VAR.3.F
            CALL REBUILD.SCREEN
        END
    END
    CALL REBUILD.SCREEN
*-------------------------------------------
    RETURN
END
