* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 07/04/2003***********

SUBROUTINE VVR.LC.AMEND.EXP.DATE
*A ROUTINE TO ADD TF.LC.ADVICE.EXPIRY.DATE & SCB.LCD.EXP.PERIOD & DEFAULT THE RESULT IN TF.LC.EXPIRY.DATE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.AMENDMENTS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

ID = ID.NEW[1,12]

IF COMI THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   CALL DBR("LETTER.OF.CREDIT":@FM:TF.LC.LC.TYPE,ID,TYPE)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,ID,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
TYPE=R.ITSS.LETTER.OF.CREDIT<TF.LC.LC.TYPE>
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR("LETTER.OF.CREDIT":@FM:TF.LC.LOCAL.REF,ID,AID)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,ID,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
AID=R.ITSS.LETTER.OF.CREDIT<TF.LC.LOCAL.REF>

 IF TYPE THEN
   DOC.ID = TYPE:'.': AID<1,LCLR.AIDLC>
*Line [ 57 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   CALL DBR("SCB.LC.DOCTERMS":@FM:SCB.LCD.EXP.PERIOD,DOC.ID,DAT)
F.ITSS.SCB.LC.DOCTERMS = 'F.SCB.LC.DOCTERMS'
FN.F.ITSS.SCB.LC.DOCTERMS = ''
CALL OPF(F.ITSS.SCB.LC.DOCTERMS,FN.F.ITSS.SCB.LC.DOCTERMS)
CALL F.READ(F.ITSS.SCB.LC.DOCTERMS,DOC.ID,R.ITSS.SCB.LC.DOCTERMS,FN.F.ITSS.SCB.LC.DOCTERMS,ERROR.SCB.LC.DOCTERMS)
DAT=R.ITSS.SCB.LC.DOCTERMS<SCB.LCD.EXP.PERIOD>
  EXP.DAT = COMI
   IF DAT THEN
    CALL CDT ('',EXP.DAT,DAT)
    R.NEW(LC.AMD.EXPIRY.DATE) = EXP.DAT 
  END
 END
END
CALL REBUILD.SCREEN

RETURN
END
