* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
**------- INGY -------**

SUBROUTINE VVR.FT.CHQ.TYPE
* A ROUTINE TO CHECK IF THE CHEQUE.TYPE CHANGED THEN EMPTY ( DEBIT.CUSTOMER,CREDIT.CUSTOMER,DEBIT.ACCT.NO,CREDIT.ACCT.NO
* DEBIT.AMOUNT,CREDIT.AMOUNT,DEBIT.THEIR.REF,CREDIT.THEIR.REF,CHEQUE.NO,CHEQUE.NUMBER)

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

IF V$FUNCTION = "I" THEN
  IF COMI # R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> THEN
    R.NEW(FT.DEBIT.CUSTOMER) = ""
    R.NEW(FT.CREDIT.CUSTOMER) = ""
    R.NEW(FT.DEBIT.ACCT.NO) = ""
    R.NEW(FT.CREDIT.ACCT.NO) = ""
    R.NEW(FT.DEBIT.AMOUNT) = ""
    R.NEW(FT.CREDIT.AMOUNT) = ""
    R.NEW(FT.DEBIT.THEIR.REF) = ""
    R.NEW(FT.CREDIT.THEIR.REF) = ""
    R.NEW(FT.CHEQUE.NUMBER) = ""
    R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.NO> = ""
    CALL REBUILD.SCREEN
  END
 
END


RETURN
END
