* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*************DINA_SCB***********
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.ACCOUNT
*nostro or vostro account not allowed
*teller account not allowed
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

 IF V$FUNCTION = 'I' THEN
 IF COMI # R.NEW(FT.DEBIT.ACCT.NO) THEN
    R.NEW(FT.CREDIT.ACCT.NO) = ''
    R.NEW(FT.CREDIT.CURRENCY)  = ''
 END
 IF NUM(COMI[1,3]) THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
  IF (CATEG >= 5000 AND CATEG <= 5999) OR (CATEG >= 2000 AND CATEG <= 2999) THEN ETEXT = 'NO.NOSTRO.OR.VOSTRO'
  ELSE
   *CURRENCY = COMI [3,2]
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, COMI, CURR.NAME )
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR.NAME=R.ITSS.ACCOUNT<AC.CURRENCY>
  END
 END ELSE
 CURR.NAME = COMI[1,3]
 IF COMI[4,5] = 10000 THEN ETEXT = 'NOT.FOR.TELLER.ACCOUNT'
 END
 R.NEW(FT.DEBIT.CURRENCY) = CURR.NAME
 *R.NEW(FT.CHARGES.ACCT.NO)= COMI
 CALL REBUILD.SCREEN
END
RETURN
END
