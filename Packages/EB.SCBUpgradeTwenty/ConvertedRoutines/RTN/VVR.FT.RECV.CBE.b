* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*----------------28/02/2010 NESSREEN AHMED-----------------------*
*-----------------------------------------------------------------------------
* <Rating>513</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.RECV.CBE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
*******************************************************************************************

    ETEXT='' ; BRANCH2 =''
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT ='' ; R.FT = ''
    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H ='' ; R.FT.H = ''
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.FT.H,F.FT.H)

    IF COMI THEN
        BR = COMP[8,2]
        CCO = COMP[6,4]
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR( 'CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE, COMI, CUR.COD)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,COMI,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.COD=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
        CR.ACCT = '99433300':CUR.COD:'508001'
        DB.ACCT = COMI :'1000099990099'
        R.NEW(FT.DEBIT.ACCT.NO)= DB.ACCT
        R.NEW(FT.CREDIT.CURRENCY)= COMI
        R.NEW(FT.CREDIT.ACCT.NO) = CR.ACCT
        CALL REBUILD.SCREEN
    END

    RETURN
END
