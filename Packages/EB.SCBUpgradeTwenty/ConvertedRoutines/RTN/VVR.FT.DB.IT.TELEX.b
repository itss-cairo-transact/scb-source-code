* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>800</Rating>
*-----------------------------------------------------------------------------
***----INGY----***

    SUBROUTINE VVR.FT.DB.IT.TELEX

* To check if Debit.Acc.No is only Nostro or Vostro account then check if the
* currency in the Credit.Acc.No is the same as Debit.Acc.No or not and
* to check that the Credit.Acc.No is not same as Debit.Acc.No


*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT


    IF COMI THEN
        ACC = COMI
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
POST=R.ITSS.ACCOUNT<AC.POSTING.RESTRICT>
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
        IF POST # "" THEN
            ETEXT = DESC ;CALL STORE.END.ERROR
        END ELSE


            FN.ACCOUNT = 'F.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = '' ; RETRY = '' ; E1 = ''
            CALL OPF(FN.ACCOUNT,F.ACCOUNT)
            CALL F.READU(FN.ACCOUNT,COMI, R.ACCOUNT, F.ACCOUNT ,E1,RETRY)
            IF NOT(E1) THEN
                DB.CATEG = R.ACCOUNT<AC.CATEGORY>
                DB.CURR = R.ACCOUNT<AC.CURRENCY>
                DB.CUST = R.ACCOUNT<AC.CUSTOMER>
            END
            CALL F.RELEASE(FN.ACCOUNT,COMI,F.ACCOUNT)

            IF NOT (NUM(COMI)) THEN ETEXT = '��� ����� ��������� ��������' ; RETURN
            IF NOT (DB.CATEG >= 2000 AND DB.CATEG <= 2999  OR  DB.CATEG >= 5000 AND DB.CATEG <= 5999) THEN ETEXT = '������ �������� ��������� ���' ; RETURN

            IF R.NEW(FT.CREDIT.ACCT.NO) THEN

*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, COMI ,CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                IF ETEXT THEN E = ETEXT

*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, COMI ,CR.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
                IF ETEXT THEN E = ETEXT

                IF DB.CURR # CR.CURR THEN ETEXT = '���� �� ���� ���� ����� ��������'


                IF COMI=R.NEW(FT.CREDIT.ACCT.NO) THEN ETEXT = '���� ����� � ������� ���� �� ���� �����'

            END

*  R.NEW(FT.ORDERING.BANK)= DB.CUST

            CALL REBUILD.SCREEN

        END
    END
    RETURN
END
