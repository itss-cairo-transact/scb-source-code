* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>586</Rating>
*-----------------------------------------------------------------------------
***----INGY 05/04/2005----***

    SUBROUTINE VVR.FT.DB.ACOT
*1-CHEACK IF CATEG >= 2000 AND CATEG <= 2999  OR CATEG >= 5000 AND CATEG <= 5999
* THEN Nostro.Or.Vostro ACCOUNT IS NOT ALLOWED
*2-CHEACK IF SEC >= 3000 AND SEC <= 3999 THEN Not.Allowed.For.Bank
*3-SET THE CHARGE AND COMMISSION ACCOURDING TO SECTOR


*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.TYPE.CATEG
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)


    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
            IF COMI THEN
                ACC = COMI
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
POST=R.ITSS.ACCOUNT<AC.POSTING.RESTRICT>
*Line [ 62 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
                IF POST # "" THEN
                    ETEXT = DESC ;CALL STORE.END.ERROR
                END ELSE

                    IF R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> THEN ETEXT = "��� ����� ��������"
                    ELSE
                        IF NUM(COMI) THEN
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                            CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                            IF (CATEG > 2000 AND CATEG <= 2999  OR  CATEG >= 5000 AND CATEG <= 5999) THEN
***    ETEXT = 'Not.Allowed.For.Nostro.Or.Vostro'
                            END
                            ELSE
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                                CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,COMI,CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                                CALL DBR( 'CUSTOMER':@FM:EB.CUS.SECTOR, CUS, SEC )
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
* IF (SEC >= 3000 AND SEC <= 3999) THEN ETEXT = 'Not.Allowed.For.Bank'
* ELSE
**  CALL DBR( 'CUSTOMER':@FM:EB.CUS.NAME.1,CUS,CUS.NAME)
                                CALL F.READ(FN.CU,CUS,R.CU,F.CU,E1)
                                CUS.NAME = R.CU<EB.CUS.NAME.1><1,1>
                                R.NEW(FT.ORDERING.CUST)= CUS.NAME
*     R.NEW(FT.CHARGE.CODE) = "DEBIT PLUS CHARGES"
                                R.NEW(FT.CHARGES.ACCT.NO)= COMI
*   CALL REBUILD.SCREEN
* END
                            END
                        END  ELSE
                            R.NEW(FT.COMMISSION.CODE) = "WAIVE"
                            R.NEW(FT.CHARGE.CODE) = "WAIVE"
                            DEP = R.USER<EB.USE.DEPARTMENT.CODE>
                            DEP = STR('0', 2- LEN(DEP) ):DEP
                            ACCT = LCCY:'17303':DEP:'01'
* IF COMI # ACCT THEN ETEXT = '��� ����� �������� ��� ������'

                        END
                    END
                END
            END
        END

        RETURN
    END
