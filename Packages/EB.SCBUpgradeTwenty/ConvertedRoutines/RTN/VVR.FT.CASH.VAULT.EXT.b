* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VVR.FT.CASH.VAULT.EXT
* If the func is input then the field debit.value.date & the field credit.value.date defaulted to today

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------------------
*    COMP = ID.COMPANY

*   BR = COMP[8,2]
*   BRC = COMP[6,4]

    IF COMI THEN
*     CR.ACCT = COMI:'10000':BR:'99':BRC
*      R.NEW(FT.DEBIT.ACCT.NO)= COMI:'11140000100':BR
*      R.NEW(FT.CREDIT.CURRENCY) = COMI
*      R.NEW(FT.CREDIT.ACCT.NO) =  CR.ACCT

        IF V$FUNCTION = 'I' THEN
            R.NEW(FT.DEBIT.ACCT.NO)  = 'EGP1139400010099'
            R.NEW(FT.CREDIT.ACCT.NO) = 'EGP1000099990099'
        END

        CALL REBUILD.SCREEN
    END

*-------------------------------------------------------
    RETURN
END
