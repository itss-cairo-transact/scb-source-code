* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
**************************NI7OOOOOOOOOOOOO
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FLAG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1

    IF R.NEW(DEPT.SAMP.AMT.HWALA) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.HWALA) = 1
        TEXT = "FLG 1  :": R.NEW(DEPT.SAMP.FLG.HWALA) ; CALL REM
    END
    IF R.NEW(DEPT.SAMP.AMT.WH) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.WH) = 1
        TEXT = "FLG 1  :": R.NEW(DEPT.SAMP.FLG.WH) ; CALL REM
    END


    IF R.NEW(DEPT.SAMP.AMT.LG11) NE '' AND R.NEW(DEPT.SAMP.NO.EXTENTION.LG22) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.LG11) = 3
        TEXT = "FLG 2  :": R.NEW(DEPT.SAMP.FLG.LG11) ; CALL REM
    END

    IF R.NEW(DEPT.SAMP.AMT.LG22) NE '' AND R.NEW(DEPT.SAMP.FOR.BENFITS.LG11) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.LG22) = 2
        TEXT = "FLG 3  :": R.NEW(DEPT.SAMP.FLG.LG22) ; CALL REM
    END

    IF R.NEW(DEPT.SAMP.AMT.TEL) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.TEL) = 4
    END

    IF R.NEW(DEPT.SAMP.AMT.TF2) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.TF2) = 5
    END

    IF R.NEW(DEPT.SAMP.AMT.TF1) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.TF1) = 6
    END

    IF R.NEW(DEPT.SAMP.AMT.TF3) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.TF3) = 7
    END

    IF R.NEW(DEPT.SAMP.AMT.BR) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.BR) = 8
    END

    IF R.NEW(DEPT.SAMP.AMT.AC) NE '' THEN
        R.NEW(DEPT.SAMP.FLG.AC) = 9
    END

    IF R.NEW(DEPT.SAMP.AMT.TEL) AND R.NEW(DEPT.SAMP.AMT.HWALA) AND R.NEW(DEPT.SAMP.AMT.WH) AND R.NEW(DEPT.SAMP.AMT.LG11) AND R.NEW(DEPT.SAMP.AMT.LG22) AND R.NEW(DEPT.SAMP.AMT.AC) EQ '' AND R.NEW(DEPT.SAMP.AMT.BR) EQ '' AND R.NEW(DEPT.SAMP.AMT.TF3) EQ '' AND R.NEW(DEPT.SAMP.AMT.TF1) EQ '' AND R.NEW(DEPT.SAMP.AMT.TF2) EQ '' THEN
        R.NEW(DEPT.SAMP.FLG.TWSYA) = 10
    END

    RETURN
END
