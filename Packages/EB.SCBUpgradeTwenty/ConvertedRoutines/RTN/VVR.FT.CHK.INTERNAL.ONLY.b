* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*************NESSREEN-SCB************
*-----------------------------------------------------------------------------
* <Rating>298</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.CHK.INTERNAL.ONLY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*To check that Debit Acc.is AN Internal Account and not Teller Transaction nor PL Transactions

ETEXT = ''
IF V$FUNCTION = "I" AND MESSAGE # 'VAL' THEN

 CURR = R.NEW(FT.DEBIT.CURRENCY)
 BRN = R.USER< EB.USE.DEPARTMENT.CODE>
 BRN = STR('0', 2- LEN(BRN) ):BRN
* COMI = CURR:"17001":BRN:"01"
 *CALL REBUILD.SCREEN
 IF NUM(COMI)  THEN ETEXT = 'SHOULD.BE.INTERNAL'; RETURN
 IF COMI[1,2] = 'PL' THEN ETEXT = 'NOT.ALLOWED.FOR.PL.TRANS' ; RETURN
 IF COMI[4,5] = 10000 THEN ETEXT = 'NOT.ALLOWED.FOR.TELLER.TRANSACTION'

END

RETURN
END
