* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*------------INGY---------*

SUBROUTINE VVR.FT.EMPTY.DEBIT.CUSTOMER
*A ROUTINE TO EMPTY :CREDIT.CUSTOMER,CREDIT.CUSTOMER,CREDIT.CURRENCY,CREDIT.ACCT.NO,CREDIT.AMOUNT,CREDIT.THEIR.REF
*                   DEBIT.CUSTOMER,DEBIT.CURRENCY,DEBIT.ACCT.NO,DEBIT.AMOUNT,DEBIT.THEIR.REF
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER




IF COMI # R.NEW(FT.DEBIT.CUSTOMER) THEN
           R.NEW(FT.CREDIT.CUSTOMER)=''
           R.NEW(FT.CREDIT.CURRENCY)=''
           R.NEW(FT.DEBIT.CURRENCY)=''
           R.NEW(FT.DEBIT.ACCT.NO) = ''
           R.NEW(FT.CREDIT.ACCT.NO) = ''
           R.NEW(FT.CREDIT.AMOUNT)=''
           R.NEW(FT.DEBIT.AMOUNT)=''
           R.NEW(FT.DEBIT.THEIR.REF)=''
           R.NEW(FT.CREDIT.THEIR.REF)=''

   CALL REBUILD.SCREEN
  END



 RETURN
END
