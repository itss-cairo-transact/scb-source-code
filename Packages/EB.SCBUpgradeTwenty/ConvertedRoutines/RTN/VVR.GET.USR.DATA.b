* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>-31</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2013/02/06 ***
*******************************************

    SUBROUTINE VVR.GET.USR.DATA
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HELPTEXT.MAINMENU
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACTIVITY

*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    IF MESSAGE # 'VAL' THEN
        GOSUB INITIATE
        GOSUB GET.USR.DATA
        GOSUB GET.TELLER.DATA

        WS.USR.CO = R.NEW(MUA.DEPARTMENT.CODE)
        WS.MOD.USR = WS.COMP[2]
        IF WS.MOD.USR # WS.USR.CO THEN
            ETEXT = '�� ���� ����� ���� �� ���� �����'
            CALL STORE.END.ERROR
            RETURN
        END
        IF WS.USR.CO EQ '99' OR WS.USR.CO EQ '88' THEN
            ETEXT = '��� ����� �������� �� ��� ������'
            CALL STORE.END.ERROR
            RETURN
        END
        CALL F.READ(FN.ACTV,COMI,R.ACTV,F.ACTV,E.ACTV)
        IF NOT(E.ACTV) THEN
            ETEXT = '���� ������ �� ��� ������� ������ ������'
            CALL STORE.END.ERROR
        END

        CALL F.READ(FN.USRNAU,COMI,R.USRNAU,F.USRNAU,E.USRNAU)
        IF NOT(E.USRNAU) THEN
            ETEXT = "��� �������� ��� ������� ���� ������� ����"
            CALL STORE.END.ERROR
        END
    END
    CALL REBUILD.SCREEN
    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.USR  = "F.USER"               ; F.USR   = ""
    CALL OPF (FN.USR,F.USR)

    FN.USRNAU  = "F.USER$NAU"               ; F.USRNAU   = ""
    CALL OPF (FN.USRNAU,F.USRNAU)

    FN.UA   = "F.USER.ABBREVIATION"  ; F.UA  = "" ; R.UA = ""
    CALL OPF (FN.UA,F.UA)

    FN.TUSR   = "FBNK.TELLER.USER"  ; F.TUSR  = "" ; R.TUSR = ""
    CALL OPF (FN.TUSR,F.TUSR)

    FN.TT.ID   = "FBNK.TELLER.ID"  ; F.TT.ID  = "" ; R.TT.ID = ""
    CALL OPF (FN.TT.ID,F.TT.ID)

    FN.ACTV   = 'F.ACTIVITY'          ; F.ACTV = ''
    CALL OPF(FN.ACTV,F.ACTV)

    RETURN
*-----------------------------------------------------
GET.USR.DATA:

    WS.USER.ID = COMI
    CALL F.READ(FN.USR,WS.USER.ID,R.USR,F.USR,ER.USR)

    R.NEW(MUA.SIGN.ON.NAME)         = R.USR<EB.USE.SIGN.ON.NAME>
    R.NEW(MUA.COMPANY.CODE)         = R.USR<EB.USE.COMPANY.CODE>
    R.NEW(MUA.DEPARTMENT.CODE)      = R.USR<EB.USE.DEPARTMENT.CODE>
    R.NEW(MUA.DEP.ACCT.CODE)        = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
    R.OLD(MUA.DEP.ACCT.CODE)        = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
    R.NEW(MUA.USER.BNK.POSITION)    = R.USR<EB.USE.LOCAL.REF><1,USER.STAFF.RANK>
    R.NEW(MUA.START.DATE.PROFILE)   = R.USR<EB.USE.START.DATE.PROFILE>
    R.NEW(MUA.END.DATE.PROFILE)     = R.USR<EB.USE.END.DATE.PROFILE>
    R.NEW(MUA.OVER.CLASS)           = R.USR<EB.USE.OVERRIDE.CLASS>

    RETURN
*-----------------------------------------------------
GET.TELLER.DATA:
    R.NEW(MUA.TELLER.ID) = ''
    CALL F.READ(FN.TUSR,WS.USER.ID,R.TUSR,F.TUSR,ER.TUSR)
    LOOP
        REMOVE WS.TELLER.ID FROM R.TUSR SETTING POS.TUSR
    WHILE WS.TELLER.ID:POS.TUSR

        CALL F.READ(FN.TT.ID,WS.TELLER.ID,R.TT.ID,F.TT.ID,ER.TT.ID)

        WS.STATUS = R.TT.ID<TT.TID.STATUS>
        IF WS.STATUS = 'OPEN' THEN
            R.NEW(MUA.TELLER.ID) = WS.TELLER.ID
            R.OLD(MUA.TELLER.ID) = WS.TELLER.ID
            RETURN
        END
    REPEAT

    RETURN
*-----------------------------------------------------
