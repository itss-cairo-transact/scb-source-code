* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>33</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CR.DRFT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

*To check if version is to issue Draft cheque by  Local Currency then the
*allowed currency should be Local
* and the allowed category should be 5101
    IF MESSAGE # 'VAL' THEN
        IF V$FUNCTION = "I" THEN
***************************************
            FN.CHQ.PRES = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRES = ''
            R.CHQ.PRES = ''
            CALL OPF(FN.CHQ.PRES,F.CHQ.PRES)
********************HHHHHH***20081114****************
***  T.SEL ="SELECT F.SCB.FT.DR.CHQ WITH @ID LIKE ":COMI:"... BY CHEQ.NO"
            T.SEL ="SELECT F.SCB.FT.DR.CHQ WITH CHEQ.NO LE 10000000 AND  @ID LIKE ":COMI:"... BY CHEQ.NO"
*********************HHHHH*********************
*TEXT = T.SEL ; CALL REM
            KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                CHEQ.NO = FIELD(KEY.LIST<SELECTED>,".",2) + 1
*  TEXT = "CHEQ.NO=":CHEQ.NO ; CALL REM
                R.NEW(FT.CHEQUE.NUMBER) = CHEQ.NO
            END ELSE
                R.NEW(FT.CHEQUE.NUMBER) = '1'
            END
*************************************

            VER.NAM = R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
            IF CURR # LCCY  AND VER.NAM = ",SCB.DRFT.ABO" THEN
                ETEXT = 'ONLY.LOCAL.CURR'
            END
* IF CURR =  LCCY AND VER.NAM = ",SCB.DRFT.ABO" THEN ETEXT = 'ONLY.FORIEGN.CURRENCY'
* END
* IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "1 DRAFT- ��� ����� ��������" THEN

            ACTT = COMI[4,5]
*TEXT = "CATEG":ACTT ; CALL REM

            IF ACTT NE 16151 THEN
                ETEXT = "Categ.Not.Allowed"
            END
            CALL REBUILD.SCREEN

        END
* CALL REBUILD.SCREEN
    END
    RETURN
END
