* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
**----INGY14/03/2005----**
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CR.BNK
*If account with foriegn currency error message will appear
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF V$FUNCTION = "I" THEN
        VER.NAM = R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
        TEXT = CURR ; CALL REM
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
        IF CURR = LCCY AND VER.NAM = ",SCB.BNKCHQFCY"  THEN

            ETEXT = '���� ����� ���'
        END  ELSE
            IF CURR # LCCY AND VER.NAM = ",SCB.BNKCHQEGP" THEN
                ETEXT = '���� ������ ���'
            END
        END
        IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> = "5 CHEQUE-��� � �����" THEN
            IF CATEG # 5102 THEN ETEXT = "Categ.Not.Allowed"
        END

    END
    RETURN
END
