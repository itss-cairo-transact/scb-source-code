* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.CHQ.PRESENTED

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************

    IF V$FUNCTION = 'I' THEN
        IF COMI  THEN
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = 'DRF.':R.NEW(FT.CREDIT.ACCT.NO):'-':COMI
            CHQ.ID = 'DRF.':R.NEW(FT.CREDIT.ACCT.NO):'.':COMI
****CALL DBR ('CHEQUES.PRESENTED':@FM:CHQ.PRE.DATE.PRESENTED,CHQ.ID,PRESENT)
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.PRESENTED,CHQ.ID,PRESENT)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
PRESENT=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.PRESENTED>
****END OF UPDATE 7/3/2016*****************************
            IF ETEXT THEN ETEXT = '' ELSE ETEXT = 'WRONG'
        END
    END
    RETURN
END
