* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>793</Rating>
*-----------------------------------------------------------------------------
*******Nessreen Ahmed - SCB*******************

SUBROUTINE VVR.FT.CHK.NOSTRO.INTERNAL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*To check that Crediit Acc.is not an Internal Account nor a Nostro Acc or Vostro Acc.
*To check that the Currency in the Credit acc is the same Currency as Debit.Currency
*To check that the Customer in the Acc. is equal to the Credit Customer
*To Default Charge.Acc.No with the Credit.Acc.No

IF V$FUNCTION = "I" THEN

   IF COMI THEN

*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*      CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, COMI ,CR.CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
      IF ETEXT THEN E = ETEXT

*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*      CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, COMI ,CR.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CR.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
      IF ETEXT THEN E = ETEXT

      IF CR.CATEG >4999 AND CR.CATEG < 6000 THEN ETEXT = 'NOT.ALLOWED.NOSTRO'; RETURN
      IF CR.CATEG >1999 AND CR.CATEG <3000 THEN ETEXT = 'NOT.ALLOWED.VOSTRO' ; RETURN
      IF NOT(NUM(COMI)) THEN ETEXT = 'SHOULDNOT.BE.INTERNAL' ; RETURN

      IF R.NEW(FT.DEBIT.ACCT.NO) THEN

*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*         * CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, R.NEW(FT.DEBIT.ACCT.NO) ,DB.CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DB.CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>
*          IF ETEXT THEN E = ETEXT

*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*         CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY,R.NEW(FT.DEBIT.ACCT.NO) ,DB.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,R.NEW(FT.DEBIT.ACCT.NO),R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
DB.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
         IF ETEXT THEN E = ETEXT

         IF CR.CURR # DB.CURR THEN ETEXT = 'SHOULD.BE.EQ.DR.CURR'; RETURN
*          IF CR.CUST # DB.CUST THEN ETEXT = 'SHOULD.BE.EQ.CR.CUSTOMER'

      END

      R.NEW(FT.CHARGES.ACCT.NO) = COMI

    CALL REBUILD.SCREEN
   END

END

RETURN
END
