* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.FT.BN.CHQ

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    F.COUNT = '' ; FN.COUNT = 'F.SCB.FT.DR.CHQ'
    CALL OPF(FN.COUNT,F.COUNT)
    R.COUNT = ''
    ID= R.NEW(FT.DEBIT.ACCT.NO):'.':COMI 
    CALL F.READU(FN.COUNT,ID, R.COUNT, F.COUNT ,E, RETRY)
    IF NOT(E) THEN
        IF R.COUNT<DR.CHQ.CHEQ.STATUS> = 1 AND R.COUNT<DR.CHQ.CHEQ.TYPE> = 2 AND  R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>  =  "4 CHEQUE-��� � �����" THEN
            R.NEW(FT.DEBIT.CURRENCY) =  R.COUNT<DR.CHQ.CURRENCY>
            R.NEW(FT.DEBIT.AMOUNT) = R.COUNT<DR.CHQ.AMOUNT>
            R.NEW(FT.CREDIT.ACCT.NO) =  R.COUNT<DR.CHQ.DEBIT.ACCT>
            R.NEW(FT.DEBIT.THEIR.REF) =  R.NEW(FT.DEBIT.ACCT.NO)
            R.NEW(FT.DEBIT.ACCT.NO) = R.COUNT<DR.CHQ.NOS.ACCT>
            R.NEW(FT.CREDIT.CURRENCY) =  R.COUNT<DR.CHQ.CURRENCY>
            CALL REBUILD.SCREEN 
        END ELSE
            IF R.COUNT<DR.CHQ.CHEQ.STATUS> = 1 AND R.COUNT<DR.CHQ.CHEQ.TYPE> = 1 AND  R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE>  = "1 DRAFT- ��� ����� ��������"  THEN
                R.NEW(FT.DEBIT.CURRENCY) =  R.COUNT<DR.CHQ.CURRENCY>
                R.NEW(FT.DEBIT.AMOUNT) = R.COUNT<DR.CHQ.AMOUNT>
                R.NEW(FT.CREDIT.ACCT.NO) =  R.COUNT<DR.CHQ.DEBIT.ACCT>
                R.NEW(FT.DEBIT.ACCT.NO) = R.COUNT<DR.CHQ.NOS.ACCT>
                R.NEW(FT.CREDIT.CURRENCY) =  R.COUNT<DR.CHQ.CURRENCY>
            END ELSE ETEXT = 'TYPE.MISS.MATCH'
        END
    END

    CALL F.RELEASE(FN.COUNT,ID,F.COUNT)
    CLOSE FN.COUNT


    RETURN
END
