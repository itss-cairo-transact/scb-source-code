* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>272</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VVR.FT.CHQ.PRES.UNRETURN

*   TO CHECK IF CHECK ALREADY PRESENTED IN CHEQUES.PRESENTED

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*** $INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 7/3/2016**************************
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF

    ETEXT = ''

    FOR I = 1 TO 100

        ACCT.NO    = R.NEW(FT.DEBIT.ACCT.NO)
        PAY.CHQ.ID = COMI:'.': I
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCN)
F.ITSS.SCB.P.CHEQ = 'F.SCB.P.CHEQ'
FN.F.ITSS.SCB.P.CHEQ = ''
CALL OPF(F.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ)
CALL F.READ(F.ITSS.SCB.P.CHEQ,PAY.CHQ.ID,R.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ,ERROR.SCB.P.CHEQ)
MYACCN=R.ITSS.SCB.P.CHEQ<P.CHEQ.ACCOUNT.NO>
        IF MYACCN THEN
            ETEXT='��� ����� �� ����' ; CALL STORE.END.ERROR
            CALL REBUILD.SCREEN
            I = 100
        END
    NEXT I

    IF NOT(MYACCN) THEN

*   -----------------------------------------------------------    *
*  CHQ.ID.STOP = ACCT.NO:"*": COMI
*  CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY, CHQ.ID.STOP, CURR)
*  ETEXT = ''
*  IF CURR THEN
*      ETEXT = "����� ��� (&) ����� ":@FM:COMI ;CALL STORE.END.ERROR
*  END
*  CALL REBUILD.SCREEN



***----------------

*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.AC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.AC=R.ITSS.ACCOUNT<AC.CUSTOMER>
        FN.CUAC = 'F.CUSTOMER.ACCOUNT' ; F.CUAC = '' ; R.CUAC = ''  ; E1.CUAC = ''
        CALL OPF(FN.CUAC,F.CUAC)

        CALL F.READ(FN.CUAC, CUST.AC , R.CUAC, F.CUAC, E1.CUAC)
        LOOP
            REMOVE ACC.ID FROM R.CUAC SETTING SUBVAL
        WHILE ACC.ID:SUBVAL

**           CHQ.ID = ACC.ID:'*':COMI
            CHQ.NO = TRIM(COMI, "0" , "L")
****UPDATED BY NESSREEN AHMED 7/3/2016 for R15****
****CHQ.ID = ACC.ID:'*':CHQ.NO
****CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID,CHQ.STOP)
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,ACC.ID,CHQ.TYPE)
F.ITSS.CHEQUE.TYPE.ACCOUNT = 'FBNK.CHEQUE.TYPE.ACCOUNT'
FN.F.ITSS.CHEQUE.TYPE.ACCOUNT = ''
CALL OPF(F.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT)
CALL F.READ(F.ITSS.CHEQUE.TYPE.ACCOUNT,ACC.ID,R.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT,ERROR.CHEQUE.TYPE.ACCOUNT)
CHQ.TYPE=R.ITSS.CHEQUE.TYPE.ACCOUNT<CHQ.TYP.CHEQUE.TYPE>
            TY = CHQ.TYPE
*Line [ 102 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT (TY,@VM)
            FOR X = 1 TO DD
                CHQ.ID = CHQ.TYPE<1,X>:'.':ACC.ID:'.':CHQ.NO
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CHQ.STOP)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
CHQ.STOP=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.STOPPED>
                IF CHQ.STOP THEN
                    ETEXT='��� ����� �����'
                    CALL STORE.END.ERROR
                END
            NEXT X
        REPEAT

***---------------
    END

*   ------------------------------------------------------------   *

    ERR.MSG = ''
    ETEXT = ''
    FN.CHQ.PRES = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRES = ''
    R.CHQ.PRES  = ''
    CALL OPF(FN.CHQ.PRES,F.CHQ.PRES)
**  --------------------------------------------------------------  **
**             UPDATED BY NESSREEN AHMED ON 26/08/2009              **
**  --------------------------------------------------------------  **
*TEXT = "X" ; CALL REM
    KEY.LIST.DR = ""  ;  ER.MSG = ""  ; SELECTED.DR = ""

    T.SEL.DR  = "SELECT F.SCB.FT.DR.CHQ WITH (CHEQ.NO EQ ":COMI:" OR OLD.CHEQUE.NO EQ ": COMI:")"
    T.SEL.DR := " AND CHEQ.TYPE LIKE ...DRFT... "
    CALL EB.READLIST(T.SEL.DR,KEY.LIST.DR,"",SELECTED.DR,ER.MSG)
*TEXT = 'SELECTED.DR=':SELECTED.DR ; CALL REM
    IF SELECTED.DR THEN
        ETEXT= "���� ��� ����� ����� ���� �����"  ; CALL STORE.END.ERROR
    END
*    ------------------------------------------------------------    *
*TEXT = "Y" ; CALL REM
    SELECTED.LK = ""
    F.COUNT     = '' ;  FN.COUNT = 'F.AC.LOCKED.EVENTS'
    CALL OPF(FN.COUNT,F.COUNT)
    LOCAL.REF = FN.COUNT<AC.LCK.LOCAL.REF>
    CHQ.NO    = LOCAL.REF<1,AC.LCK.LOC.CHQ.NO>
    T.SEL     = "SELECT FBNK.AC.LOCKED.EVENTS WITH CHQ.NO EQ ":COMI:" AND ACCOUNT.NUMBER EQ ":ACCT.NO
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED.LK,ER.MSG)

    IF SELECTED.LK THEN
        ETEXT = "��� ����� ���� ����" ;CALL STORE.END.ERROR
    END

    R.NEW(FT.CHEQUE.NUMBER) = COMI
    CALL REBUILD.SCREEN

**  END
*   ------------------------------------------------------------    *

**-----------------------  CHECK  ISSUE ----------------
*TEXT = "H" ; CALL REM

    CUS.ID  = R.NEW(FT.DEBIT.CUSTOMER)
    CHQ.NO  = R.NEW(FT.CHEQUE.NUMBER)
    IF R.NEW(FT.CHEQUE.NUMBER) EQ ''  THEN CHQ.NO = COMI
    FLAG    = 0


    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;     R.CUS.AC = '' ;  F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    FN.CHQ.AC ='FBNK.CHEQUE.ISSUE.ACCOUNT' ; R.CHQ.AC = '' ;  F.CHQ.AC=''
    CALL OPF(FN.CHQ.AC,F.CHQ.AC)

    FN.CHQ.REG ='FBNK.CHEQUE.REGISTER' ; R.CHQ.REG = ''    ;  F.CHQ.REG   =''
    CALL OPF(FN.CHQ.REG,F.CHQ.REG)

*==========a)To Check IF Cheq Is Issued ===============================================
    T.SEL ="SELECT FBNK.CHEQUE.REGISTER WITH CUSTOMER EQ ": CUS.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
*TEXT = 'SEL=':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CHQ.REG,KEY.LIST<I>, R.CHQ.REG, F.CHQ.REG , ERR.CHQ)
            CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>

*Line [ 184 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DD = DCOUNT(CHQ.NOS,@VM)
            FOR X = 1 TO DD
                CHN  = CHQ.NOS<1,X>
                SS = COUNT(CHN, "-")
                IF SS > 0 THEN
                    ST.NO   = FIELD(CHN ,"-", 1)
                    ED.NO   = FIELD(CHN ,"-", 2)
                END ELSE
                    ST.NO   = CHN
                    ED.NO   = CHN
                END
                IF (CHQ.NO GE ST.NO) AND (CHQ.NO LE ED.NO) THEN
                    FLAG = "1"
*TEXT = 'RANGE' ; CALL REM
                END ELSE
                END
            NEXT X
        NEXT I
    END
    ETEXT = ''
    IF FLAG = "0" THEN
        ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
    END
    ETEXT = ''


**-------------------

    RETURN
END
