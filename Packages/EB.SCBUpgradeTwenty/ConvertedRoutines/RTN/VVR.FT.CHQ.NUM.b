* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>350</Rating>
*-----------------------------------------------------------------------------
**------- INGY -------**

SUBROUTINE VVR.FT.CHQ.NUM
* A ROUTINE :
           *TO MAKE SURE IF CHEQUE.TYPE = 5 BILLS OR 6 CUSTOMER.CHEQUE OR 7 CUSTOMER.ORDER THEN MAKE CHEQUE.NO NOINPUT
           *DEFAULT DEBIT.THEIR.REF WITH COMI
           *DEFAULT CHARGES.ACCT.NO WITH DEBIT.ACCT.NO
           *DEFAULT COMMISSION.CODE & CHARGE.CODE = DEBIT PLUS CHARGES


*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

IF COMI THEN


IF R.NEW(FT.LOCAL.REF)<1,FTLR.CHEQUE.TYPE> # '7 CUSTOMER.CHEQUE-��� ����' THEN
   ETEXT = 'INPUT.IS.NOT.ALLOWED'
END ELSE
     T.SEL = "SELECT FBNK.CHEQUE.REGISTER WITH @ID LIKE  ": '...':R.NEW(FT.DEBIT.ACCT.NO)
      CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

 
 IF KEY.LIST THEN
 F.CHQ = '' ; FN.CHQ = 'FBNK.CHEQUE.REGISTER'
 CALL OPF(FN.CHQ,F.CHQ)
 R.CHQ = ''
 FOR I = 1 TO SELECTED
 CALL F.READ( FN.CHQ, KEY.LIST<I>, R.CHQ, F.CHQ, READ.ERROR)

 CHQ.NUM = R.CHQ<CHEQUE.REG.CHEQUE.NOS>
 X = FIELD(CHQ.NUM,"-",1)
 Y = FIELD(CHQ.NUM,"-",2)
 IF COMI < X OR COMI > Y THEN ETEXT = 'OUT OF RANGE';RETURN
 IF  R.CHQ<CHEQUE.REG.STOPPED.CHQS> = COMI  OR R.CHQ<CHEQUE.REG.PRESENTED.CHQS> = COMI THEN ETEXT= 'WRONG' ;RETURN
NEXT I
     R.NEW(FT.DEBIT.THEIR.REF)= COMI
     R.NEW(FT.CHARGES.ACCT.NO)= R.NEW(FT.DEBIT.ACCT.NO)
     R.NEW(FT.COMMISSION.CODE)= 'DEBIT PLUS CHARGES'
     R.NEW(FT.CHARGE.CODE)= 'DEBIT PLUS CHARGES'
      CALL REBUILD.SCREEN

END ELSE
 ETEXT = 'CHEQ.NUM.NOT.FOUND'
 END
 CLOSE F.CHQ
 END


END



RETURN
END
