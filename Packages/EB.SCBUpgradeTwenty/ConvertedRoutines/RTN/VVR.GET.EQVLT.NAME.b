* @ValidationCode : MjoxNDc2ODU1ODEzOkNwMTI1MjoxNjQxNDAyODIzNjM4OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 05 Jan 2022 19:13:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
*Create By Nessma Mohammad
*-----------------------------------------------------------------------------
SUBROUTINE VVR.GET.EQVLT.NAME
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.ACCOUNT
    $INSERT        I_F.SCB.AC.BONUS.BNK
*--------------------------------------------
*Line [ 28 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,COMI,ACCT.NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACCT.NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
    R.NEW(BONUS.EQUIVALENT.NAME) = ACCT.NAME
    CALL REBUILD.SCREEN
*--------------------------------------------
RETURN
END
