* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***************DINA_SCB*************
*-----------------------------------------------------------------------------
* <Rating>596</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.ACCOUNT.CRIDIT
*If debit account is internal account then credit account can't be internal
*account can't be nostro or vostro or teller account
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    IF NOT(NUM(COMI)) THEN
    IF NOT(NUM(R.NEW(FT.DEBIT.ACCT.NO))) THEN ETEXT = 'MUST.BE.CUS.ACCT'
    ELSE
    CURRENCY = COMI[1,3]
    IF CURRENCY # R.NEW(FT.DEBIT.CURRENCY) THEN MM = 'Y'
    IF COMI[4,5] = 10000 THEN ETEXT = 'NOT.FOR.TELLER.ACCOUNT'
    END
    END ELSE
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
    IF (CATEG >= 5000 AND CATEG <= 5999) OR (CATEG >= 2000 AND CATEG <= 2999) THEN ETEXT = 'NO.NOSTRO.OR.VOSTRO'
    ELSE
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*     CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURRENCY)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURRENCY=R.ITSS.ACCOUNT<AC.CURRENCY>
     * CURRENCY = COMI [3,2]
*      CALL DBR( 'NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE, CURRENCY, CURR.NAME )
     IF NOT(NUM(R.NEW(FT.DEBIT.ACCT.NO))) THEN IF CURRENCY # R.NEW(FT.DEBIT.CURRENCY) THEN MM = 'Y'
    END
    END
 *IF MM = 'Y' THEN ETEXT = 'ACCOUNTS.MUST.BE.THE.SAME.CURR'
 BRN.CR = COMI[1,2]
 BRN.DR = R.NEW(FT.DEBIT.ACCT.NO)[1,2]
 CUS.CR = COMI[3,6]
 CUS.DR = R.NEW(FT.DEBIT.ACCT.NO)[3,6]
 IF BRN.CR = BRN.DR OR CUS.CR = CUS.DR THEN
 R.NEW(FT.COMMISSION.CODE)= "WAIVE"
 R.NEW(FT.COMMISSION.TYPE)=""
 R.NEW(FT.COMMISSION.AMT) = ""
 R.NEW(FT.CHARGES.ACCT.NO)=""
 R.NEW(FT.CHARGE.CODE) = "WAIVE"
 R.NEW(FT.CHARGE.TYPE) = ""
 R.NEW(FT.CHARGE.AMT) = ""
 *CALL REBUILD.SCREEN
 END
 IF BRN.CR # BRN.DR AND CUS.CR # CUS.DR THEN
 R.NEW(FT.COMMISSION.CODE)= "DEBIT.PLUS.CHARGES"
 R.NEW(FT.CHARGE.CODE) = "DEBIT.PLUS.CHARGES"
 *TEXT = "CR.BRN": BRN.CR :" DR.BRN": BRN.DR ; CALL REM
 *CALL REBUILD.SCREEN
 END
 R.NEW(FT.CREDIT.CURRENCY) = CURRENCY
 CALL REBUILD.SCREEN

RETURN
END
