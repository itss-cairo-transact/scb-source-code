* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***----INGY----***
*-----------------------------------------------------------------------------
* <Rating>45</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.DB.AC.NOS.CBE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF V$FUNCTION = 'I' THEN
        IF COMI # R.NEW(FT.DEBIT.ACCT.NO) THEN
            R.NEW(FT.CREDIT.CUSTOMER) = ''
            R.NEW(FT.CREDIT.ACCT.NO) = ''
* R.NEW(FT.CREDIT.CURRENCY)  = ''
        END

        IF NUM(COMI[1,3]) THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>


            IF CATEG >= 2000 AND CATEG <= 2999 THEN
                ETEXT = 'NO.VOSTRO.ACCOUNTS'
            END  ELSE
                IF CATEG >= 5000 AND CATEG <= 5999 THEN
*   IF CATEG # 5010 THEN ETEXT = '������ ���� ���'
                END
            END
            IF COMI[1,2] = '91'  THEN
* AND CATEG = 5010 THEN
                R.NEW(FT.CHARGE.CODE) = 'WAIVE'
            END ELSE
*   R.NEW(FT.CHARGE.CODE) = 'DEBIT PLUS CHARGE'
*   R.NEW(FT.CHARGES.ACCT.NO) = COMI
            END

        END ELSE
            ETEXT = '��� ����� ��������� ����� �����'
        END
        CALL REBUILD.SCREEN
        RETURN
    END
