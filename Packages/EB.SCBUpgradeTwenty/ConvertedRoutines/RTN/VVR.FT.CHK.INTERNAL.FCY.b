* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>196</Rating>
*-----------------------------------------------------------------------------
*---------- INGY ------------*

SUBROUTINE VVR.FT.CHK.INTERNAL.FCY
*A ROUTINE:
         * TO MAKE SURE IT IS NOT NOSTRO.ACCOUNT & MAKE SURE IT IS FOREIGN.CURRENCY
         * DEFAULT CHARGES.ACCT.NO WITH COMI


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

IF V$FUNCTION = "I" THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
 IF CURR = LCCY THEN ETEXT = 'MUST.Foregn.Currency'
   *IF (CATEG GE 5000 AND CATEG LE 5999) OR (CATEG GE 2000 AND CATEG LE 2999) THEN
    * ETEXT = 'Cannot.Be.Nostro.Or.Vostro.Account'
    IF CATEG < 5000 OR CATEG > 5999 THEN ETEXT = 'Only.Nostro.Account'
*END
*R.NEW(FT.CHARGES.ACCT.NO) = COMI
*END
*END

    *IF (CATEG GE 5000 AND CATEG LE 5999) OR (CATEG GE 2000 AND CATEG LE 2999) THEN
     *ETEXT = 'Cannot.Be.Nostro.Or.Vostro.Account'
     *END

RETURN
END
