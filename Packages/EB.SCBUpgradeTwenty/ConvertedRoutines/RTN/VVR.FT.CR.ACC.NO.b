* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>350</Rating>
*-----------------------------------------------------------------------------

************* WAEL ***********
**** UPDATED BY DINA & INGY****


SUBROUTINE VVR.FT.CR.ACC.NO

*A ROUTINE TO MAKE SURE:
                       * IT'S A LOCAL CURRENCY
                       * THAT THE CREDIT ACCOUNT IS NOT INTERNAL ACCOUNT
                       * THAT THE CREDIT ACCOUNT IS A NOSTRO ACCOUNT


*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

IF COMI THEN
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR("ACCOUNT":@FM:AC.CURRENCY,COMI,CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
* CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>

 IF CURR # LCCY THEN ETEXT = 'Only.Local.Currency'
 ELSE
 IF NOT(NUM(COMI)) THEN ETEXT = 'Internal.Account.Not.Allowed'
 ELSE
 IF CATEG >= 5000 AND CATEG <= 5999 OR (CATEG >= 2000 AND CATEG <= 2999)THEN RETURN
 ELSE  ETEXT = 'Only.For.Nostro.Account'
  
END
END
END


RETURN
END
