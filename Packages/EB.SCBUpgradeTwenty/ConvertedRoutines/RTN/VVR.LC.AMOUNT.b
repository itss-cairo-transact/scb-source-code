* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
***********INGY-SCB 06/04/2003***********

SUBROUTINE VVR.LC.AMOUNT
* A ROUTINE TO CHECK IF TF.LC.LC.AMOUNT IS CHANGED THEN EMPTY TF.LC.DB.PROV.AMOUNT,TF.LC.PROVIS.PERCENT,
* TF.LC.PROVIS.AMOUNT,TF.LC.PRO.OUT.AMOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

IF V$FUNCTION='I' THEN
IF COMI # R.NEW(TF.LC.LC.AMOUNT) THEN
   R.NEW(TF.LC.DB.PROV.AMOUNT) = ''
   R.NEW(TF.LC.PROVIS.PERCENT) = ''
   R.NEW(TF.LC.PROVIS.AMOUNT) = ''
   R.NEW(TF.LC.PRO.OUT.AMOUNT) = ''
END
CALL REBUILD.SCREEN
END

RETURN
END
