* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>331</Rating>
*-----------------------------------------------------------------------------
***----INGY 06/04/2005----***

    SUBROUTINE VVR.FT.DB.ACOT1

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT

*TO CHECK IF NOT NOSTRO ACCOUNT , NOT INPUT THE ACCOUNT AND DEFAULT ORDERING.CUST,CHARGE.CODE,COMMISSION.CODE,DEBIT.CURRENCY AND ASSIGN USER BRANCH

    IF V$FUNCTION = 'I' THEN
        IF MESSAGE NE 'VAL' THEN
            IF COMI THEN

                R.NEW(FT.ORD.CUST.ACCT) = COMI

                ACC = COMI
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('ACCOUNT':@FM:AC.POSTING.RESTRICT,ACC,POST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
POST=R.ITSS.ACCOUNT<AC.POSTING.RESTRICT>
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,POST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
                IF POST # "" THEN
                    ETEXT = DESC ;CALL STORE.END.ERROR
                END ELSE

                    IF R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF> THEN
                        ETEXT = "��� ����� ��������"
                    END ELSE
                        IF NUM(COMI) THEN
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                            CALL DBR("ACCOUNT":@FM:AC.CATEGORY,COMI,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,COMI,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                            IF (CATEG >= 2000 AND CATEG <= 2999)  THEN
*  ETEXT = '��� ����� ������� ��������'
*OR  CATEG >= 5000 AND CATEG <= 5999) THEN ETEXT = 'Not.Allowed.For.Nostro.Or.Vostro'
                            END ELSE
*CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,COMI,CUS)
*CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR, CUS, SEC)
*IF (SEC >= 3000 AND SEC <= 3999) THEN ETEXT = 'Not.Allowed.For.Bank'

*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*                                CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1, CUS, CUS.NAME)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.NAME=R.ITSS.CUSTOMER<EB.CUS.NAME.1>
                                R.NEW(FT.ORDERING.CUST)= CUS.NAME
*  R.NEW(FT.CHARGE.CODE) = "DEBIT PLUS CHARGES"
* R.NEW(FT.COMMISSION.CODE) = "DEBIT PLUS CHARGES"
*R.NEW(FT.CHARGES.ACCT.NO)= COMI
*CALL REBUILD.SCREEN
                            END
                        END
                    END
* R.NEW(FT.COMMISSION.CODE) = "WAIVE"
* R.NEW(FT.CHARGE.CODE) = "WAIVE"
                    DEP = R.USER<EB.USE.DEPARTMENT.CODE>
                    CC = R.NEW(FT.DEBIT.CURRENCY)
                    DEP = STR('0', 2- LEN(DEP) ):DEP
                    ACCT = CC:'17303':DEP:'01'
* IF COMI # ACCT THEN ETEXT = '��� ����� �������� ��� ������'

* END
                END
            END
*END

*CALL REBUILD.SCREEN
        END
        RETURN
    END
