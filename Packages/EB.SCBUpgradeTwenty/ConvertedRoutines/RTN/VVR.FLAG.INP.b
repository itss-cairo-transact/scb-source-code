* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*************************NI7OOOOOOOOOOOOOOOOO**************************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FLAG.INP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1

    OVER = R.NEW(DEPT.SAMP.OVERRIDE)
    FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
        R.NEW(DEPT.SAMP.FLG.INP1)   = 1
        R.NEW(DEPT.SAMP.FLG.AUTH1)  = 1
    END

    FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
        FINDSTR 'MAR2' IN OVER SETTING FMS,VMS THEN
            R.NEW(DEPT.SAMP.FLG.INP2)  = 2
            R.NEW(DEPT.SAMP.FLG.AUTH2) = 2
        END
    END

    FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
        FINDSTR 'MAR2' IN OVER SETTING FMS,VMS THEN
            FINDSTR 'MAR3' IN OVER SETTING FMS,VMS THEN
                R.NEW(DEPT.SAMP.FLG.INP3)  = 3
                R.NEW(DEPT.SAMP.FLG.AUTH3) = 3
            END
        END
    END

    RETURN
END
