* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>144</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.INT.DR.CUR

*1-CHECK IF CREDIT.ACCT IS A (PL)ACCOUNT THEN THE CREDIT.CURRENCY MUST BE A LOCAL CURRENCY

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    IF MESSAGE EQ 'VAL' THEN
        IF COMI THEN

            IF R.NEW(FT.CREDIT.ACCT.NO)[1,2] EQ 'PL' THEN
            ***    IF COMI NE 'EGP' THEN ETEXT = '���� ������� �������� ���� ���'
                CALL REBUILD.SCREEN
*  R.NEW(FT.DEBIT.ACCT.NO) = ''
*  R.NEW(FT.DEBIT.AMOUNT) = ''
*  R.NEW(FT.CREDIT.ACCT.NO) = ''
*  COMI.ENRI<FT.CREDIT.CURRENCY> = ''
*  R.NEW(FT.CREDIT.CURRENCY) = COMI
*  COMI.ENRI<FT.CREDIT.CURRENCY> = COMI

            END

        END

    END


    RETURN
END
