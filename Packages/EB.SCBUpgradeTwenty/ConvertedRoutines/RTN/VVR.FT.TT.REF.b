* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*-----------------------------------------------------------------------------
* <Rating>299</Rating>
*-----------------------------------------------------------------------------
***----INGY 05/04/2005----***

    SUBROUTINE VVR.FT.TT.REF

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*TO CHECK NOT INPUT DEBIT.CUSTOMER , IF TELLER REFERANCE NUMBER EXIT ,DEFAULT CURRENCY.1, ACCOUNT.1, AMOUNT.FCY.1, AMOUNT.LOCAL.1, DEBIT.CURRENCY, DEBIT.ACCT.NO, ASSIGN USER BRANCH , NO INPUT DEBIT.CURRENCY,DEBIT.ACCT.NO,IF LOCAL CURRENCY ASSIGN DEBIT.AMOUNT WITH LOCAL & MAKE DEBIT.AMOUNT NO INPUT ,IF FORIGN CURRENCY ASSIGN DEBIT.AMOUNT WITH FORIGN & MAKE DEBIT.AMOUNT NO INPUT


    IF COMI THEN
        IF R.NEW(FT.DEBIT.CUSTOMER) THEN  ETEXT = '��� ����� ��������'
        ELSE
            FN.COUNT = 'FBNK.TELLER' ; R.COUNT = ''  ; F.COUNT = ''
            CALL OPF(FN.COUNT,F.COUNT)
            CALL F.READ( FN.COUNT, COMI, R.COUNT, F.COUNT, TT.ERR)
            IF TT.ERR THEN ETEXT = '��� ������� ��� �����'
            ELSE
                CUR  = R.COUNT<TT.TE.CURRENCY.1>
                ACCT = R.COUNT<TT.TE.ACCOUNT.1>
                AMT.FCY = R.COUNT<TT.TE.AMOUNT.FCY.1>
                AMT.LCY = R.COUNT<TT.TE.AMOUNT.LOCAL.1>
*CLOSE FN.COUNT
                DEP = R.USER<EB.USE.DEPARTMENT.CODE>
                DEP = STR('0', 2- LEN(DEP) ):DEP
                IF CUR = LCCY THEN
                    R.NEW(FT.DEBIT.AMOUNT) = AMT.LCY
                    T(FT.DEBIT.AMOUNT)<3> = "NOINPUT"
                END ELSE
                    R.NEW(FT.DEBIT.AMOUNT) = AMT.FCY
                    T(FT.DEBIT.AMOUNT)<3> = "NOINPUT"
                END
                R.NEW(FT.DEBIT.CURRENCY) = CUR
                T(FT.DEBIT.CURRENCY)<3> = "NOINPUT"
                R.NEW(FT.DEBIT.ACCT.NO) = ACCT
                T(FT.DEBIT.ACCT.NO)<3> = "NOINPUT"
                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
