* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
***********DALIA-SCB 08/04/2003***********
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.LC.ANNEX11

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.ANNEX.INDEX
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS

IF COMI THEN
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*  CALL DBR("SCB.LC.ANNEX.INDEX":@FM:SCB.LC.ANDX.LC.ID,COMI,ANEX)
F.ITSS.SCB.LC.ANNEX.INDEX = 'F.SCB.LC.ANNEX.INDEX'
FN.F.ITSS.SCB.LC.ANNEX.INDEX = ''
CALL OPF(F.ITSS.SCB.LC.ANNEX.INDEX,FN.F.ITSS.SCB.LC.ANNEX.INDEX)
CALL F.READ(F.ITSS.SCB.LC.ANNEX.INDEX,COMI,R.ITSS.SCB.LC.ANNEX.INDEX,FN.F.ITSS.SCB.LC.ANNEX.INDEX,ERROR.SCB.LC.ANNEX.INDEX)
ANEX=R.ITSS.SCB.LC.ANNEX.INDEX<SCB.LC.ANDX.LC.ID>
    IF ANEX THEN ETEXT='MUST.BE.NEW.VALUE'
 END
 IF R.OLD(TF.LC.LOCAL.REF)<1,LCLR.ANNEX11> AND NOT(COMI) THEN
 ETEXT = 'YOU.MUST.ENTER.VALUE'
 END

  IF COMI THEN
  
  IF R.OLD(TF.LC.LOCAL.REF)<1,LCLR.ANNEX11> = '' THEN ETEXT= 'INPUT.IS.NOT.ALLOWEWD'


END
RETURN
END
