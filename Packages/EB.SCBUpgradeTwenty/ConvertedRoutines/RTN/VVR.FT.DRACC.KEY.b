* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
**---------NESSREEN-SCB---------**
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.DRACC.KEY

* IF The Func IS INPUT Then  Default the field  DEBIT.CURRENCY with THE FIELD CREDIT.CURRENCY
*And Create DEBIT.ACCOUNT Key by taking the Credit Curr:Category:User Dep:Serial.no
*And if we change THE CREDIT.CURRENCY then empty the following Fields:
*DEBIT.ACCOUNT, DEBIT.CURRENCY, CREDIT.ACCOUNT, CREDIT.CUSTOMER AND CREDIT.ACCOUNT

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.TYPE.CATEG

IF COMI THEN
    IF COMI # R.NEW(FT.CREDIT.CURRENCY) THEN
      R.NEW(FT.DEBIT.ACCT.NO) = ''
      R.NEW(FT.DEBIT.CURRENCY) = ''
      T.ENRI<FT.DEBIT.CURRENCY> = ''
      R.NEW(FT.CREDIT.ACCT.NO) = ''
     *  R.NEW(FT.CREDIT.CUSTOMER) = ''
*       R.NEW(FT.CREDIT.AMOUNT) = ''
   END
   R.NEW(FT.DEBIT.CURRENCY)= COMI
   CODE = 9
   DEP.COD = R.USER< EB.USE.DEPARTMENT.CODE>
   DEP.COD = STR('0', 2- LEN(DEP.COD) ):DEP.COD
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*   CALL DBR("SCB.CHQ.TYPE.CATEG":@FM:CC.CATEGORY,CODE,CATEG2)
F.ITSS.SCB.CHQ.TYPE.CATEG = 'F.SCB.CHQ.TYPE.CATEG'
FN.F.ITSS.SCB.CHQ.TYPE.CATEG = ''
CALL OPF(F.ITSS.SCB.CHQ.TYPE.CATEG,FN.F.ITSS.SCB.CHQ.TYPE.CATEG)
CALL F.READ(F.ITSS.SCB.CHQ.TYPE.CATEG,CODE,R.ITSS.SCB.CHQ.TYPE.CATEG,FN.F.ITSS.SCB.CHQ.TYPE.CATEG,ERROR.SCB.CHQ.TYPE.CATEG)
CATEG2=R.ITSS.SCB.CHQ.TYPE.CATEG<CC.CATEGORY>
      IF NOT(ETEXT) THEN R.NEW(FT.DEBIT.ACCT.NO) = COMI:CATEG2:DEP.COD:'01'

CALL REBUILD.SCREEN

END

RETURN
END
