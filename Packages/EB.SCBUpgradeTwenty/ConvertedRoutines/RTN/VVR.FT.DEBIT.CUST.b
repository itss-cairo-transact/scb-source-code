* @ValidationCode : MjoxNTk3NzU5NzM2OkNwMTI1MjoxNjQ1MDA2MTcwMjM0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 16 Feb 2022 12:09:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
*************DINA-SCB************
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.FT.DEBIT.CUST
*not allowed customer with sector bank
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.POSTING.RESTRICT

*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    POSS=R.ITSS.CUSTOMER<EB.CUS.TEXT>
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    STO5=POSS<1,5>
    STO6=POSS<1,6>
    STO7=POSS<1,7>
    STO8=POSS<1,8>
    STO9=POSS<1,9>
    STO10=POSS<1,10>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
    END

    ETEXT =''

    CUU = COMI
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('CUSTOMER':@FM: EB.CUS.POSTING.RESTRICT,CUU,POST)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUU,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    POST=R.ITSS.CUSTOMER<EB.CUS.POSTING.RESTRICT>
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST,DESC)
    F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
    FN.F.ITSS.POSTING.RESTRICT = ''
    CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
    CALL F.READ(F.ITSS.POSTING.RESTRICT,POST,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
    DESC=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>
    IF POST # "" AND POST # 10 THEN
**        ETEXT = DESC ;CALL STORE.END.ERROR
    END ELSE

        IF COMI THEN
            R.NEW(FT.ORDERING.BANK) = 'SCB'
        END

        IF COMI THEN
*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,COMI, SEC)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            SEC=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('SCB.VER.IND.SEC.LEG':@FM:VISL.SECTOR,',SCB.BANK',SEC1)
            F.ITSS.SCB.VER.IND.SEC.LEG = 'F.SCB.VER.IND.SEC.LEG'
            FN.F.ITSS.SCB.VER.IND.SEC.LEG = ''
            CALL OPF(F.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG)
            CALL F.READ(F.ITSS.SCB.VER.IND.SEC.LEG,SCB.BANK,R.ITSS.SCB.VER.IND.SEC.LEG,FN.F.ITSS.SCB.VER.IND.SEC.LEG,ERROR.SCB.VER.IND.SEC.LEG)
            SEC1=R.ITSS.SCB.VER.IND.SEC.LEG<VISL.SECTOR>
            LOCATE SEC IN SEC1<1,1,1> SETTING M THEN  ETEXT = 'SECTOR.OF.CUSTOMER.NOT.ALLOWED'
        END
    END
RETURN
END
