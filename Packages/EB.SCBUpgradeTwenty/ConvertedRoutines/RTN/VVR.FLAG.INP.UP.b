* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
**********************NI7OOOOOOOOOOOOO******************
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FLAG.INP.UP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1


    OVER     = R.NEW(DEPT.SAMP.OVERRIDE)
    REC.STAT = R.NEW(DEPT.SAMP.RECORD.STATUS)
    XX       = R.NEW(DEPT.SAMP.FLG.INP1)
    YY       = R.NEW(DEPT.SAMP.FLG.INP2)
    ZZ       = R.NEW(DEPT.SAMP.FLG.INP3)

    INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)
F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
FN.F.ITSS.USER.SIGN.ON.NAME = ''
CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,INPUTTER,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
INP=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT.CODE=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>


    IF XX = 1 AND YY = '' AND REC.STAT EQ 'INAU' THEN
        IF DEPT.CODE EQ 99 THEN
            E = '��� ������� �� ����� ����'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END
    IF XX = 1 AND YY = 2 AND REC.STAT EQ 'INAU' THEN
        IF DEPT.CODE NE 99 THEN
            E = '��� ������� �� ����� �������� ����'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    IF XX = 1 AND YY = 2 AND ZZ = 3 AND REC.STAT EQ 'INAO' THEN
        IF DEPT.CODE NE 99 THEN
*    E = '��� ������� �� ����� �������� ������ ���� �� 250000' ;* CALL ERR  ;* MESSAGE = 'REPEAT'
        END
    END

    RETURN
END
