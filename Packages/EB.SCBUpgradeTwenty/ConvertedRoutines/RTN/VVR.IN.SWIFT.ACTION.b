* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwenty  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwenty
*DONE
****** RANIA ******
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.IN.SWIFT.ACTION

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.SWIFT.ACTION

    IF NOT(COMI)  THEN

        MYKIND=R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>

        IF MYKIND EQ 'INFINAL' OR MYKIND EQ 'INAD' OR MYKIND EQ 'INBB' THEN
            COMI=R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-16
*            CALL DBR ('SCB.LG.SWIFT.ACTION':@FM:SCB.LG.DE.MAPPING.ID,COMI,MAPKEY)
F.ITSS.SCB.LG.SWIFT.ACTION = 'F.SCB.LG.SWIFT.ACTION'
FN.F.ITSS.SCB.LG.SWIFT.ACTION = ''
CALL OPF(F.ITSS.SCB.LG.SWIFT.ACTION,FN.F.ITSS.SCB.LG.SWIFT.ACTION)
CALL F.READ(F.ITSS.SCB.LG.SWIFT.ACTION,COMI,R.ITSS.SCB.LG.SWIFT.ACTION,FN.F.ITSS.SCB.LG.SWIFT.ACTION,ERROR.SCB.LG.SWIFT.ACTION)
MAPKEY=R.ITSS.SCB.LG.SWIFT.ACTION<SCB.LG.DE.MAPPING.ID>
            MAPKEY = MAPKEY[1,3]
            R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD> = MAPKEY
            CALL REBUILD.SCREEN
        END ELSE
            ETEXT='You.Cant.Produce.Swift'
            CALL REBUILD.SCREEN
        END

    END

    RETURN
END
