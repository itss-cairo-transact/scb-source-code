* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SCB.FATCA.TOT.PRCNT.CAL

* This is attached as input routine in entity FCSI versions
* To validate the PERCENTAGE id total percentage is greater than 100
*-----------------------------------------


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO


*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS

    RETURN
*----------------------------------------

INITIALIZE:
*----------

    Y.TOT.PRCNT.VAL = R.NEW(FA.FI.PRCNT.OWNERSHIP)
    ETEXT = ''

    RETURN

PROCESS:
*-------

*Line [ 55 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    Y.PRCNT.CNT = DCOUNT(Y.TOT.PRCNT.VAL,@VM)

    FOR CNT1 = 1 TO Y.PRCNT.CNT
        TOTAL.PRCNT += Y.TOT.PRCNT.VAL<1,CNT1>
        IF TOTAL.PRCNT GT '100' THEN
            AF = FA.FI.PRCNT.OWNERSHIP
            ETEXT = 'FA-TOTAL.PRCNT.GT.100'
            CALL STORE.END.ERROR
            IF ETEXT THEN RETURN
        END

    NEXT CNT1

    RETURN

END
