* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SCB.FATCA.JOBO.OWNER.HOLDER.REF

*----------------------------------------

*This routine is a validation routine for field CUSTOMER.ID to default the HOLDER.REF value
*and to check if the entered BO/SO CUSTOMER.ID is a FATCA customer are not
*Attached as validation routine to customer.id field in entity versions

*-----------------------------------------

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO

*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS

    RETURN
*----------------------------------------

INITIALIZE:
*---------

    FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO = 'F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO'
    F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO = ''
    CALL OPF(FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO)

    SEL.CMD = ''            ; SEL.LIST = ''
    JOBO.OWNER.ID.LIST = '' ; JOBO.OWNER.ID = ''
    JOBO.COUNT = ''         ; FCSI.ID = ''
    HOLDER.POS = ''         ;  ETEXT = ''
    FCSI.ID = ID.NEW

    RETURN

PROCESS:
*-------

    SEL.CMD = "SELECT ": FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'','',SEL.ERR)
    CONVERT @FM TO @VM IN SEL.LIST
    IF COMI NE '' THEN
        JOBO.OWNER.ID = COMI
        JOBO.OWNER.ID.LIST = R.NEW(FA.FI.CUSTOMER.ID)

        FINDSTR JOBO.OWNER.ID IN SEL.LIST SETTING HOLDER.POS THEN
            R.NEW(FA.FI.HOLDER.REF)<1,1> = FCSI.ID : "-" : 1
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            MULTI.POS = DCOUNT(R.NEW(FA.FI.HOLDER.REF),@VM)
            FOR I = 1 TO MULTI.POS
                R.NEW(FA.FI.HOLDER.REF)<1,I> = FCSI.ID : "-" : I
                R.NEW(FA.FI.LEGAL.ENTITY.TYPE)<1,I> = 'CONT'
            NEXT I
        END ELSE
            AF = FA.FI.CUSTOMER.ID
            ETEXT = "FA-NOT.A.FATCA.CUSTOMER"
            CALL STORE.END.ERROR
        END
    END

    RETURN

END
