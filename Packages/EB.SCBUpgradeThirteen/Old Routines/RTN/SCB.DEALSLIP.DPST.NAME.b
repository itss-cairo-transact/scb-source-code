* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
***************************************************************************************************************************
 SUBROUTINE SCB.DEALSLIP.DPST.NAME(TT.ID)
***************************************************************************************************************************

* Description       : This routine is used to display the local field value (Depositor Name) of  teller record in Deal SLIP
*                   : This routine is copied from SCB.DEALSLIP.ARAB.NAME routine and updated by Nessreen Ahmed 13/10/2020
* Attached To       : DEAL.DLIP.FORMAT>TT.DEPOSIT.LST3
* In & Out Argument : TT.ID

***************************************************************************************************************************

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    TEXT = 'HI' ; CALL REM
    IF TT.ID THEN
        GOSUB PROCESS
    END
    RETURN

*--
PROCESS:
*--

    FN.TT = "FBNK.TELLER"
    F.TT = '' ; R.TT = '' ; ERR.TT = ''
    CALL OPF(FN.TT,F.TT)

     FN.TT.H = "FBNK.TELLER$HIS"
     F.TT.H = '' ; R.TT.H = '' ; ERR.TT.H = ''
     CALL OPF(FN.TT.H,F.TT.H)

    CALL F.READ(FN.TT,TT.ID,R.TT,F.TT,ERR.TT)
    TEXT = 'TT.ID=' :TT.ID ; CALL REM
    IF R.TT THEN
        LOCAL.TT.VAL = ''
        LOCAL.TT.VAL = R.TT<TT.TE.LOCAL.REF>
        TT.ID = LOCAL.TT.VAL<1,TTLR.ARABIC.NAME>
        TEXT = 'TT.ID2=':TT.ID ; CALL REM
    END
    RETURN

END
