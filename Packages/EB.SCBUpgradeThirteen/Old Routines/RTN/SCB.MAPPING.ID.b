* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.MAPPING.ID(INCOMING,MAPPING.ID)
*-----------------------------------------------------------------------------
* Subroutine Type : PROCEDURE
* Attached to     : INTRF.MAPPING
* Attached as     :
* Primary Purpose : Form Mapping Id
*
*-----------------------------------------------------------------------------
* Incoming :
*-----------
* Outgoing :
*-----------------------------------------------------------------------------
* Developed for ATM Framework ISO 8583-87/93 message standards
* Developer: Gpack ATM
*-----------------------------------------------------------------------------
* Modification History :
*
* 16/12/13 - Ref 866817 / Task 866826
*            Remove dependency with RAD tables. Replaced call to
*            RAD.LOG.MSG with ATM.MSG.LOG
*
*-----------------------------------------------------------------------------

*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AT.ISO.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ATM.PARAMETER


    GOSUB PROCESS

    RETURN          ;* Program RETURN

PROCESS:
*-------*

    MAPPING.ID = "ISO"
    R.ATM.PARAMETER=AT$ATM.PARAMETER.REC
    INTRF.MSG.ID = R.ATM.PARAMETER<ATM.PARA.MSG.ID>

    MESG.TYPE.IND = AT$INCOMING.ISO.REQ(0)
    PROCESSING.CODE = AT$INCOMING.ISO.REQ(3)

    NETWORK.MSG.IND = AT$INCOMING.ISO.REQ(70)
*


    MAPPING.ID := MESG.TYPE.IND

    IF  PROCESSING.CODE EQ '' THEN
        MAPPING.ID:= NETWORK.MSG.IND
    END ELSE

        MAPPING.ID:= PROCESSING.CODE


    END
    IF MESG.TYPE.IND NE "0420" THEN
        IF PROCESSING.CODE[1,2] EQ '20' AND NOT(AT$INCOMING.ISO.REQ(57)) THEN MAPPING.ID = 'ISO0200999999'
    END


    RETURN          ;* from PROCESS

END
