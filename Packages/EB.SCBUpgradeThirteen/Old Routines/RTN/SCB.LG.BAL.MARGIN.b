* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.LG.BAL.MARGIN

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SCB.LG.BAL.MARGIN'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.CUR= 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    CUS.SEL = "SELECT FBNK.CUSTOMER"
    CUS.KEY.LIST ="" ; CUS.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUS.SEL,CUS.KEY.LIST,"",CUS.SELECTED,ER.MSG)

    CUR.SEL = "SELECT FBNK.CURRENCY"
    CUR.KEY.LIST ="" ; CUR.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUR.SEL,CUR.KEY.LIST,"",CUR.SELECTED,ER.MSG)

    FOR K = 1 TO CUR.SELECTED
        T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ ":21096:" AND CURRENCY EQ ":CUR.KEY.LIST<K>
        KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED > 0 THEN GOSUB GET.RECORDS
    NEXT K

    RETURN
*-----------------------------------
GET.RECORDS:
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LOCAL.REF = R.LD<LD.LOCAL.REF>
            LG.AMT<I> =R.LD<LD.AMOUNT>
            LG.CUST.ID<I>=R.LD<LD.CUSTOMER.ID>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LG.CUST.ID<I>,LOCAL.REF)
            NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUR<I>=R.LD<LD.CURRENCY>
            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR<I>,CURR)

            MARG<I>=LOCAL.REF<1,LDLR.MARGIN.AMT>
            IF CUR<1> # CUR<I> THEN TOT.LG = TOT.LG + LG.AMT<I>
            IF CUR<1> # CUR<I> THEN TOT.MARG = TOT.MARG + MARG<I>
**************TO GET EGQ FOR LG AMOUNT AND MARGIN******************
            LAMT = "" ;LMARG=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = ""
            MARKET = '1'
            IF CUR<I> # LCCY THEN
                CALL MIDDLE.RATE.CONV.CHECK(TOT.LG,CUR<I>,RATE,MARKET,LAMT,DIF.AMT,DIF.RATE)
                CALL MIDDLE.RATE.CONV.CHECK(TOT.MARG,CUR<I>,RATE,MARKET,LMARG,DIF.AMT,DIF.RATE)
            END
************************************************************
        NEXT I
        PRINT SPACE(1):CUS.KEY.LIST<K>:SPACE(5):NAME :SPACE(5):CURR:SPACE(5):TOT.LG:SPACE(5):LAMT:SPACE(5):TOT.MARG:SPACE(5):LMARG
        PRINT
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):" ����� � ������ � ���������"
    PR.HD :="'L'":SPACE(28):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"������":SPACE(5):"�����":SPACE(5):"������":SPACE(5):"������ ��������":SPACE(5):"����� ��������":SPACE(5):"������ ���������":SPACE(5):"����� ���������"
    PR.HD :="'L'":SPACE(1):STR('_',70)

    HEADING PR.HD
    RETURN
*==============================================================
END
