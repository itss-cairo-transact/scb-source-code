* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>894</Rating>
*-----------------------------------------------------------------------------
* Version 5 11/10/99  GLOBUS Release No. G10.1.02 28/10/99

SUBROUTINE SCB.INW.103.FILE

* 01/07/2002 -N.Betsias, INFORMER SA Greece-
* New table created in order to store information for MT103.
* These information will be used from FT.IN.PROCESSING.3 routine
* in order to be able to produce a FT txn.
*
* UPDATED BY P.KARAKITSOS - 22/02/03
* SO AS TO INCLUDE FIELD INSTRCTN.CODE
*
* UPDATED BY P.KARAKITSOS - 22/02/03
* SO AS TO INCLUDE FIELD FOR.BRANCH
*
* UPDATED BY P.KARAKITSOS - 25/02/03
* SO AS TO INCLUDE FIELD PEG.IND (ONLY FOR EGYPT)
*
* UPDATED BY P.KARAKITSOS - 08/03/03
* SO AS TO INCLUDE FIELD CREDIT.ACCT.NO
*
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*
* PK 22/02/03 START
*
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*
* PK - 22/02/03 END
*

*************************************************************************

      GOSUB DEFINE.PARAMETERS

      IF LEN(V$FUNCTION) GT 1 THEN
         GOTO V$EXIT
      END

      CALL MATRIX.UPDATE

      GOSUB INITIALISE                   ; * Special Initialising

*************************************************************************

* Main Program Loop

      LOOP

         CALL RECORDID.INPUT

      UNTIL (MESSAGE EQ 'RET')

         V$ERROR = ''

         IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION         ; * Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
               CALL FUNCTION.DISPLAY
               V$FUNCTION = ''
            END

         END ELSE

REM >       GOSUB CHECK.ID                  ;* Special Editing of ID
REM >       IF ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
               GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

REM >       GOSUB CHECK.RECORD              ;* Special Editing of Record
REM >       IF ERROR THEN GOTO MAIN.REPEAT

REM >       GOSUB PROCESS.DISPLAY           ;* For Display applications

            LOOP
               GOSUB PROCESS.FIELDS      ; * ) For Input
               GOSUB PROCESS.MESSAGE     ; * ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

         END

MAIN.REPEAT:
      REPEAT

V$EXIT:
      RETURN                             ; * From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

      LOOP
         IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
               CALL FIELD.MULTI.INPUT
            END ELSE
               CALL FIELD.MULTI.DISPLAY
            END
         END ELSE
            IF FILE.TYPE EQ 'I' THEN
               CALL FIELD.INPUT
            END ELSE
               CALL FIELD.DISPLAY
            END
         END

      WHILE NOT(MESSAGE)

REM >    GOSUB CHECK.FIELDS              ;* Special Field Editing

         IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

      REPEAT

      RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

      IF MESSAGE = 'DEFAULT' THEN
         MESSAGE = 'ERROR'               ; * Force the processing back
         IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
REM >       GOSUB CROSS.VALIDATION
         END
      END

      IF MESSAGE = 'PREVIEW' THEN
         MESSAGE = 'ERROR'               ; * Force the processing back
         IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
REM >       GOSUB CROSS.VALIDATION
REM >       IF NOT(ERROR) THEN
REM >          GOSUB DELIVERY.PREVIEW
REM >       END
         END
      END

      IF MESSAGE EQ 'VAL' THEN
         MESSAGE = ''
         BEGIN CASE
            CASE V$FUNCTION EQ 'D'
REM >          GOSUB CHECK.DELETE              ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
REM >          GOSUB CHECK.REVERSAL            ;* Special Reversal checks
            CASE OTHERWISE
REM >          GOSUB CROSS.VALIDATION          ;* Special Cross Validation
REM >          IF NOT(ERROR) THEN
REM >             GOSUB OVERRIDES
REM >          END
         END CASE
REM >    IF NOT(ERROR) THEN
REM >       GOSUB BEFORE.UNAU.WRITE         ;* Special Processing before write
REM >    END
         IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
REM >       IF MESSAGE NE "ERROR" THEN
REM >          GOSUB AFTER.UNAU.WRITE          ;* Special Processing after write
REM >       END
         END

      END

      IF MESSAGE EQ 'AUT' THEN
REM >    GOSUB AUTH.CROSS.VALIDATION          ;* Special Cross Validation
REM >    IF NOT(ERROR) THEN
REM >       GOSUB BEFORE.AUTH.WRITE         ;* Special Processing before write
REM >    END

         IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

REM >       IF MESSAGE NE "ERROR" THEN
REM >          GOSUB AFTER.AUTH.WRITE          ;* Special Processing after write
REM >       END
         END

      END

      RETURN

*************************************************************************

PROCESS.DISPLAY:

* Display the record fields.

      IF SCREEN.MODE EQ 'MULTI' THEN
         CALL FIELD.MULTI.DISPLAY
      END ELSE
         CALL FIELD.DISPLAY
      END

      RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

      IF E THEN V$ERROR = 1

      RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


      RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS
      IF E THEN
         T.SEQU = "IFLD"
         CALL ERR
      END

      RETURN

*************************************************************************

CROSS.VALIDATION:

*
      V$ERROR = ''
      ETEXT = ''
      TEXT = ''
*
REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*

      RETURN                             ; * Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
      V$ERROR = ''
      ETEXT = ''
      TEXT = ''
REM > CALL XX.OVERRIDE
*

*
      IF TEXT = "NO" THEN                ; * Said NO to override
         V$ERROR = 1
         MESSAGE = "ERROR"               ; * Back to field input

      END
      RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


      RETURN

*************************************************************************

CHECK.DELETE:


      RETURN

*************************************************************************

CHECK.REVERSAL:


      RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

      IF TEXT = "NO" THEN                ; * Said No to override
         CALL TRANSACTION.ABORT          ; * Cancel current transaction
         V$ERROR = 1
         MESSAGE = "ERROR"               ; * Back to field input
         RETURN
      END

*
* Additional updates should be performed here
*
REM > CALL XX...



      RETURN

*************************************************************************

AFTER.UNAU.WRITE:


      RETURN

*************************************************************************

AFTER.AUTH.WRITE:


      RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

      BEGIN CASE
         CASE R.NEW(V-8)[1,3] = "INA"    ; * Record status
REM > CALL XX.AUTHORISATION
         CASE R.NEW(V-8)[1,3] = "RNA"    ; * Record status
REM > CALL XX.REVERSAL

      END CASE
*
* If there are any OVERRIDES a call to EXCEPTION.LOG should be made
*
* IF R.NEW(V-9) THEN
*    EXCEP.CODE = "110" ; EXCEP.MESSAGE = "OVERRIDE CONDITION"
*    GOSUB EXCEPTION.MESSAGE
* END
*

      RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

      IF INDEX('V',V$FUNCTION,1) THEN
         E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
         CALL ERR
         V$FUNCTION = ''
      END

      RETURN

*************************************************************************
*
EXCEPTION.MESSAGE:
*
      CALL EXCEPTION.LOG("U",
         APP.CODE,
         APPLICATION,
         APPLICATION,
         EXCEP.CODE,
         "",
         FULL.FNAME,
         ID.NEW,
         R.NEW(V-7),
         EXCEP.MESSAGE,
         ACCT.OFFICER)
*
      RETURN

*************************************************************************

INITIALISE:

      APP.CODE = ""                      ; * Set to product code ; e.g FT, FX
      ACCT.OFFICER = ""                  ; * Used in call to EXCEPTION. Should be relevant A/O
      EXCEP.CODE = ""

      RETURN

*************************************************************************

DEFINE.PARAMETERS:* SEE 'I_RULES' FOR DESCRIPTIONS *

      MAT F = "" ; MAT N = "" ; MAT T = ""
      MAT CHECKFILE = "" ; MAT CONCATFILE = ""
      ID.CHECKFILE = "" ; ID.CONCATFILE = ""

      ID.F = "SW.TXN.REF" ; ID.N = "50" ; ID.T = "A"

      Z=0

      Z+=1 ; F(Z) = "DEBIT.THEIR.REF" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "DEBIT.VALUE.DATE" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "DEBIT.CURRENCY" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "DEBIT.AMOUNT" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "INS.CCY.AMT" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "EXC.RATE" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.ORDERING.CUS" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "TELEX.FROM.CUST" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.ORDERING.BK" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.SEND.CORR.BK" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.REC.CORR.BK" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "INTEM.INST" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.ACCT.WITH.BK" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "IN.BEN.ACCT.NO" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.BEN.CUSTOMER" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.PAY.DETAILS" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "FOR.BK.CHR.COM" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.IN.BK.TO.BK" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.DEBIT.ACC" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "XX.INSTRCTN.CODE" ; N(Z) = "35" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "FOR.BRANCH" ; N(Z) = "4" ; T(Z) = ""
*Line [ 455 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME
      Z+=1 ; F(Z) = "PEG.IND" ; N(Z) = "3" ; T(Z) = "A"
      Z+=1 ; F(Z) = "RECEIVE.DATE" ; N(Z) = "25" ; T(Z) = "ANY"
      Z+=1 ; F(Z) = "CREDIT.ACCT.NO" ; N(Z) = "35" ; T(Z) = "ANY"
*
      Z+=1 ; F(Z) = "RESERVED.09" ; N(Z) = "35" ; T(Z) = "ANY" 
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.08" ; N(Z) = "35" ; T(Z) = "ANY" 
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.07" ; N(Z) = "35" ; T(Z) = "ANY" 
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.06" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.05" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.04" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.03" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.02" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.01" ; N(Z) = "35" ; T(Z) = "ANY" 
      T(Z)<3> = 'NOINPUT'

      V = Z + 9

      RETURN

*************************************************************************

   END
