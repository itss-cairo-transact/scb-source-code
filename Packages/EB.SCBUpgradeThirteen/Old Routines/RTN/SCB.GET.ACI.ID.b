* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.GET.ACI.ID(ACC.ID,ACI.ID.CODE)

*******MAHMOUD ELHAWARY 11/10/2010***********************
* to get the ACI id for the given account               *
*********************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY

    II = 0
    DTT = TODAY[1,6]:"01"
    CR.RATE = ''

    FN.ACC  = 'FBNK.ACCOUNT'  ; F.ACC  = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ACI  = 'FBNK.ACCOUNT.CREDIT.INT'  ; F.ACI  = '' ; R.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,EACC)
    OPEN.DAT = R.ACC<AC.OPENING.DATE>
    OP.DAT = OPEN.DAT[1,6]:"01"

    LOOP
    WHILE DTT GE OP.DAT
        ACI.ID = ACC.ID:"-":DTT
        CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,ECAA)
        IF R.ACI THEN
           ACI.ID.CODE = ACI.ID
           RETURN
        END
        CALL ADD.MONTHS(DTT,-1)
    REPEAT
    RETURN
