* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_OFS.MESSAGE.SERVICE.COMMON
    $INSERT T24.BP I_GTS.COMMON
    $INSERT T24.BP I_F.OFS.SOURCE
    $INSERT T24.BP I_GTS.COMMON
    $INSERT T24.BP I_F.TSA.SERVICE
    $INSERT T24.BP I_F.SPF
    $INSERT T24.BP I_IO.EQUATE
*

    MSG.TYPE = "OFS"
    OFS.HEADER = '' ; OFS.DATA = ''
    CALL OFS.PARSER.OFS(MSG.TYPE,OFS.HEADER,OFS.DATA,SCB.OFS.MESSAGE)
*
    OPERATOR.SAVE = ""
    USER.ID = ''; COMP.ID = ''
    USER.ID = OFS.HEADER<7>
    COMP.ID = OFS.HEADER<9>

*    USER.ID = SCB.OFS.MESSAGE["//EG00",1,1][",",3,1]
*    COMP.ID = SCB.OFS.MESSAGE[INDEX(SCB.OFS.MESSAGE,"EG001",1),9]

*    IF COMP.ID[1,2] NE "EG" THEN
*        COMP.ID = "EG0010001"
*    END
*
    IF COMP.ID[1,2] EQ "EG" THEN
        CALL LOAD.COMPANY(COMP.ID)
    END

    IF USER.ID THEN
        R.SON = ""
        USER.OPTION = ""
        FN.USER.SON = "F.USER.SIGN.ON.NAME"
        FP.USER.SON = ""
        CALL OPF(FN.USER.SON,FP.USER.SON)
        CALL F.READ(FN.USER.SON,USER.ID,R.SON,FP.USER.SON,"")
        USER.OPTION = ""
        IF R.SON THEN
            USER.OPTION = R.SON<1>
        END
*
        OPERATOR.SAVE = OPERATOR
        OPERATOR = USER.OPTION
    END
*
    SCB.OFS.SOURCE = "SCBONLINE"
*
    FN.OFS = "F.OFS.SOURCE"
    F.OFS = ''
    CALL OPF(FN.OFS,F.OFS)
*
*
    OFS$SOURCE.ID = SCB.OFS.SOURCE
*
    ERR.OFSS = ''
    CALL F.READ(FN.OFS,OFS$SOURCE.ID,OFS$SOURCE.REC,F.OFS,ERR.OFSS)
*
    TTYPE.BKP = TTYPE
    TTYPE = "EBS-JBASE"
    RESP = ""
    SCB.OFS.ID = '' ; SCB.OPT = ''
    HUSH ON
    CALL OFS.CALL.BULK.MANAGER(SCB.OFS.SOURCE,SCB.OFS.MESSAGE,RESP,TXN.COMMIT)
    HUSH OFF
    SCB.OFS.MESSAGE = RESP
*
    TTYPE = TTYPE.BKP
    IF OPERATOR.SAVE THEN
        OPERATOR = OPERATOR.SAVE
    END
*
    RETURN
END
