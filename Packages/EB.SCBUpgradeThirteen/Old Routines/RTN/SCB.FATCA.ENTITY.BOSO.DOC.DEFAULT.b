* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-52</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SCB.FATCA.ENTITY.BOSO.DOC.DEFAULT

* This is attached as input routine in entity FCSI versions
* To default the document details for BO/SO
*-----------------------------------------


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO


*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS
    CALL REBUILD.SCREEN

    RETURN
*----------------------------------------

INITIALIZE:
*----------

    FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO = 'F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO'
    F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO = ''
    CALL OPF(FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO)

    BOSO.OWNER.ID = ''   ; BOSO.OWNER.CNT = ''
    BOSO.OWNER.LIST = '' ; BOSO.CNT = ''
    BOSO.ROLE = ''       ; BOSO.PRCNT = ''
    ROLE.FOUND.POS = ''  ; R.BOSO.FCSI = ''
    BOSO.FORM.OWNER = '' ; BOSO.HOLDER.REF = ''
    BOSO.FORM.TYPE = ''  ; TYPE.INDX = ''
    CNT = ''             ; BOSO.FORM = ''
    EXIST.FORM.OWNER = ''
    ROLE.TYPE.LIST = 'Signatory(BEN)':@VM:'Signatory(SUB)':@VM:'POAentity':@VM:'SoleOwner'

    RETURN

PROCESS:
*-------

    BOSO.OWNER.LIST = R.NEW(FA.FI.CUSTOMER.ID)
*Line [ 68 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    BOSO.OWNER.CNT = DCOUNT(BOSO.OWNER.LIST,@VM)

    FOR BOSO.CNT = 1 TO BOSO.OWNER.CNT
        BOSO.OWNER.ID = BOSO.OWNER.LIST<1,BOSO.CNT>

        BOSO.ROLE = R.NEW(FA.FI.ROLE.TYPE)<1,BOSO.CNT>
        BOSO.PRCNT = R.NEW(FA.FI.PRCNT.OWNERSHIP)<1,BOSO.CNT>
        IF BOSO.ROLE MATCHES ROLE.TYPE.LIST AND BOSO.PRCNT LE '10' THEN
            GOSUB GET.OWNER.DOC.DETAILS
        END ELSE
            IF BOSO.PRCNT GT '10' THEN
                GOSUB GET.OWNER.DOC.DETAILS
            END
        END

    NEXT BOSO.CNT

    RETURN


GET.OWNER.DOC.DETAILS:
*---------------------
    CALL F.READ(FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,BOSO.OWNER.ID,R.BOSO.FCSI,F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,BOSO.ERR)

    R.NEW(FA.FI.HOLDER.TIN)<1,BOSO.CNT> = R.BOSO.FCSI<FA.FI.TIN.CODE><1,1>
    R.NEW(FA.FI.HOLD.TIN.COUNTRY)<1,BOSO.CNT> = R.BOSO.FCSI<FA.FI.TIN.COUNTRY><1,1>
    R.NEW(FA.FI.HOLD.ADDR.COUNTRY)<1,BOSO.CNT> = R.BOSO.FCSI<FA.FI.ADDR.COUNTRY><1,1>
    BOSO.HOLDER.REF = R.NEW(FA.FI.HOLDER.REF)<1,BOSO.CNT>
*    CNT = FIELD(BOSO.HOLDER.REF,'-',2)
    EXIST.FORM.OWNER = R.NEW(FA.FI.FORM.OWNER)
*Line [ 99 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DOC.CNT = DCOUNT(EXIST.FORM.OWNER,@VM)
    IF EXIST.FORM.OWNER EQ '' THEN
        R.NEW(FA.FI.FORM.OWNER)<1,1> = BOSO.HOLDER.REF
        CNT = 1
    END ELSE
        FIND BOSO.HOLDER.REF IN EXIST.FORM.OWNER SETTING HOLD.REF.POS THEN
            RETURN
        END ELSE
            R.NEW(FA.FI.FORM.OWNER)<1,-1> = BOSO.HOLDER.REF
            CNT = DOC.CNT + 1
        END
    END
    BOSO.FORM.OWNER = R.BOSO.FCSI<FA.FI.FORM.OWNER>
    AF = '' ; AV = ''
    FIND BOSO.OWNER.ID IN BOSO.FORM.OWNER SETTING AF,AV THEN
        OW.POS = AV
    END
    IF OW.POS THEN
        TYPE.INDX = ''
        BOSO.FORM.TYPE = R.BOSO.FCSI<FA.FI.FORM.TYPE><1,OW.POS>
*Line [ 120 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        FORM.TYPE.COUNT = DCOUNT(BOSO.FORM.TYPE,@SM)
        LOOP
            REMOVE BOSO.FORM FROM BOSO.FORM.TYPE SETTING TYPE.POS
            TYPE.INDX += 1
            FORM.ID.LIST = R.BOSO.FCSI<FA.FI.FORM.ID><1,OW.POS,TYPE.INDX>
            FORM.REQ.DATES = R.BOSO.FCSI<FA.FI.REQ.DATE><1,OW.POS,TYPE.INDX>
            FORM.REC.DATES = R.BOSO.FCSI<FA.FI.RECV.DATE><1,OW.POS,TYPE.INDX>
            FORM.CUT.DATES = R.BOSO.FCSI<FA.FI.CUT.OFF.DATE><1,OW.POS,TYPE.INDX>
            FORM.EXP.DATES = R.BOSO.FCSI<FA.FI.EXP.DATE><1,OW.POS,TYPE.INDX>
            FORM.TOT.STATUS = R.BOSO.FCSI<FA.FI.FATCA.STATUS>
            GOSUB WRITE.FORM.DETS
        UNTIL NOT(TYPE.POS)
        REPEAT
    END
    RETURN

WRITE.FORM.DETS:
*---------------

    R.NEW(FA.FI.FORM.TYPE)<1,CNT,TYPE.INDX> = BOSO.FORM
    R.NEW(FA.FI.FORM.ID)<1,CNT,TYPE.INDX> = FORM.ID.LIST
    R.NEW(FA.FI.REQ.DATE)<1,CNT,TYPE.INDX> = FORM.REQ.DATES
    R.NEW(FA.FI.RECV.DATE)<1,CNT,TYPE.INDX> = FORM.REC.DATES
    R.NEW(FA.FI.RECV.DATE)<1,CNT,TYPE.INDX> = FORM.REC.DATES
    R.NEW(FA.FI.CUT.OFF.DATE)<1,CNT,TYPE.INDX> = FORM.CUT.DATES
    R.NEW(FA.FI.EXP.DATE)<1,CNT,TYPE.INDX> = FORM.EXP.DATES
    IF FORM.TOT.STATUS EQ 'US.ACCOUNT' OR FORM.TOT.STATUS EQ 'US.ACCOUNT.JOINT' THEN
        R.NEW(FA.FI.BEN.SUBS.OWNER) = 'YES'
    END
    RETURN
END
