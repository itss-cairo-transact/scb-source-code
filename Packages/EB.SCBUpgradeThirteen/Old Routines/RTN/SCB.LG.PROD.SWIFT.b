* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>951</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.LG.PROD.SWIFT(MAT R.FIELDS,LD.ID,AMEND.NO,REDUCE.AMT,REDUCE.DATE,STORE.DEL.REFS,ER.MSG)
*
* WRITTEN BY P.KARAKITSOS - 03/05/03
* SO AS TO PRODUCE SWIFT MESSAGES FOR LGs ACCORDING TO THE ACTIVITY PASSED
* THIS ROUTINE IS CALLED AT AUTHORISATION STAGE
*
*
* INCOMING --> R.NEW (ARRAY OF LD)
*          --> AMEND.NO (IN CASE OF MT767)
*          --> LD.ID (ID.NEW)
*          --> REDUCE.AMT (IN CASE OF MT769)
*          --> REDUCE.DATE (IN CASE OF MT769)
*
* OUTGOING --> ER.MSG (IF MESSAGE CANNOT BE GENERATED)
*          --> STORE.DEL.REFS (DELIVERY REFERENCES OF PRODUCED MESSAGES)
*
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.SWIFT.ACTION
*
   MAT R.NEW = MAT R.FIELDS
*
   IF R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD> & R.NEW(LD.LOCAL.REF)<1,LDLR.SEN.REC.BANK> THEN
      GOSUB GET.DE.MAPPING
      IF NOT(ETEXT) THEN
         ER.MSG = "" ; I = 0 ; SPLIT.77 = 150 ; * VALUE FOR CALCULATION OF SEQUENCE OF MESSAGES
*Line [ 54 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
         FOR I = 1 TO DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD>,@VM) UNTIL ER.MSG
            GOSUB PRODUCE.MESSAGE
         NEXT I
      END
      ELSE ER.MSG = ETEXT
   END
*
   GOTO PROGRAM.END
***************************************************************************************************************************
PRODUCE.MESSAGE:
*
   LOCATE.MAP.KEYS = ""
   REMAP.CNT = 0
*Line [ 68 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
   FOR REMAP.CNT = 1 TO DCOUNT(MAP.KEYS,@VM)
      LOCATE.MAP.KEYS<REMAP.CNT> = MAP.KEYS<1,REMAP.CNT>[1,3]
   NEXT REMAP.CNT
*
   FOUND.POS = 0
   LOCATE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,I> IN LOCATE.MAP.KEYS<1> SETTING FOUND.POS ELSE
*Line [ 75 ] Add @VM Instead Of VM & Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      ER.MSG = "CANNOT FIND & IN &":@FM:R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,I>:@VM:"SAIB.LG.SWIFT.ACTION"
   END
*
   IF NOT(ER.MSG) THEN
      R.1 = "" ; STORE.DEL.REFS = ""
      BEGIN CASE
*
         CASE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,I> = "760"
            GOSUB BUILD.760
*
         CASE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,I> = "767"
            GOSUB BUILD.767
*
         CASE R.NEW(LD.LOCAL.REF)<1,LDLR.MSG.TO.PROD,I> = "769"
            GOSUB BUILD.769
*
      END CASE
   END
*
   RETURN
***************************************************************************************************************************
CALL.APP.HANDOFF:
*
   DEL.REF = ""
*
*TEXT = "MAP.KEYS<1,FOUND.POS>" ; CALL REM
   CALL APPLICATION.HANDOFF(R.1,"","","","","","","","",
      MAP.KEYS<1,FOUND.POS>,DEL.REF,ER.MSG)
*
   IF NOT(ER.MSG) THEN STORE.DEL.REFS<SEQ.CNT> = DEL.REF

TEXT = DEL.REF ; CALL REM
*
   RETURN
***************************************************************************************************************************
GET.DE.MAPPING:
*
   ETEXT = "" ; MAP.KEYS = ""
*Line [ 114 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
   CALL DBR("SCB.LG.SWIFT.ACTION":@FM:SCB.LG.DE.MAPPING.ID,R.NEW(LD.LOCAL.REF)<1,LDLR.SWIFT.ACTION>,MAP.KEYS)
*
   RETURN
***************************************************************************************************************************
BUILD.760:
*
   R.1<4> = R.NEW(LD.AGREEMENT.DATE)[3,8] ; * STATIC INFO, FIELD 30 OF SWIFT
*
   GOSUB COMMON.760.767.PROCESS
*
   RETURN
***************************************************************************************************************************
BUILD.769:
*
   IF NOT(REDUCE.AMT) THEN
      ER.MSG = "MISSING REDUCE AMOUNT FOR MT769"
      RETURN
   END
   ELSE
      IF NOT(REDUCE.DATE) THEN
         ER.MSG = "MISSING REDUCE DATE FOR MT769"
         RETURN
      END
   END
*
   GOSUB MANDATORY.FIELDS
*
   R.1<22> = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.NUMBER,1> ; * FIELD 20 OF SWIFT
   R.1<24> = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.REF.ID> ; * FIELD 21 OF SWIFT
   R.1<25> = R.NEW(LD.CHRG.LIQ.ACCT) ; * FIELD 25 OF SWIFT
   R.1<4> = REDUCE.DATE ; * FIELD 30 OF SWIFT
   TEMP.VAR = "" ; TEMP.VAR = R.NEW(LD.CURRENCY):REDUCE.AMT
   IF NOT(INDEX(TEMP.VAR,".",1)) THEN TEMP.VAR := ","
   ELSE CONVERT "." TO "," IN TEMP.VAR
   R.1<26> = TEMP.VAR ; * FIELD 33B OF SWIFT
   TEM.VAR = "" ; TEMP.VAR = R.NEW(LD.COMMT.AVAIL.AMT)
   CONVERT "." TO "," IN TEMP.VAR
   R.1<27> = R.NEW(LD.CURRENCY):TEMP.VAR ; * FIELD 34B OF SWIFT
   SW.SEND.CNT = 0
*Line [ 154 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
   IF DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>,@SM) > 1 THEN
*Line [ 156 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
      FOR SW.SEND.CNT = 1 TO DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>,@SM)
         R.1<5,SW.SEND.CNT> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO,SW.SEND.CNT> ; * FIELD 72 OF SWIFT
      NEXT SW.SEND.CNT
   END
   ELSE
*Line [ 162 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
      IF DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>,@SM) = 1 THEN R.1<5> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>
   END
*
   GOSUB CALL.APP.HANDOFF
*
   RETURN
***************************************************************************************************************************
COUNT.FIELD:
*
*Line [ 172 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
   SW.GTEE.NO = 0 ; SW.GTEE.NO = DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.SW.GTEE.DET>,@SM)
   NO.OF.GTEE.MTS = 0 ; SEQ.MSG = 0
*
   RETURN
***************************************************************************************************************************
BUILD.767:
*
   IF NOT(AMEND.NO) THEN
      ER.MSG = "MISSING AMENDMENT NUMBER FOR MT767"
      RETURN
   END
*
   R.1<23> = AMEND.NO ; * STATIC INFO, FIELD 26E OF SWIFT
   R.1<24> = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.REF.ID> ; * STATIC INFO, FIELD 21 OF SWIFT
   R.1<21> = R.NEW(LD.AGREEMENT.DATE)[3,8] ; * STATIC INFO, FIELD 31C OF SWIFT
   R.1<4> = TODAY ; * STATIC INFO, FIELD 30 OF SWIFT
*
   GOSUB COMMON.760.767.PROCESS
*
   RETURN
***************************************************************************************************************************
FIND.SEQUENCE:
*
* FOR FIELD 77C OF SWIFT THERE IS A NEW 760 MESSAGE GENERATED WHEN THERE ARE MORE THAN 150 LINES (E.G 151,301,451 ETC)
*
   IF SW.GTEE.NO =< SPLIT.77 THEN
      NO.OF.GTEE.MTS = 1
   END
   ELSE
      IF MOD(SW.GTEE.NO,SPLIT.77) > 0 THEN NO.OF.GTEE.MTS = INT(SW.GTEE.NO / SPLIT.77) + 1
      ELSE NO.OF.GTEE.MTS = SW.GTEE.NO / SPLIT.77
   END
*
   SEQ.MSG = NO.OF.GTEE.MTS
*
   RETURN
***************************************************************************************************************************
SW.FIELD.77C:
*
   IF SW.GTEE.NO > 1 THEN
      INT.CNT.77 = 0
*
      BEGIN CASE
*
         CASE NO.OF.GTEE.MTS > SEQ.CNT
            FOR INT.CNT.77 = 1 TO SPLIT.77
               R.1<3,INT.CNT.77> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.GTEE.DET,PROCESS.CNT.77 + INT.CNT.77>
            NEXT INT.CNT.77
            PROCESS.CNT.77 += SPLIT.77
*
         CASE NO.OF.GTEE.MTS = SEQ.CNT
            FOR INT.CNT.77 = 1 TO SW.GTEE.NO - PROCESS.CNT.77
               R.1<3,INT.CNT.77> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.GTEE.DET,PROCESS.CNT.77 + INT.CNT.77>
            NEXT INT.CNT.77
*
         CASE OTHERWISE
            R.1<3> = ""
*
      END CASE
*
   END
   ELSE
      IF SEQ.CNT = 1 THEN R.1<3> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.GTEE.DET> ; * VERY RARE CASE
   END
*
   RETURN
***************************************************************************************************************************
MANDATORY.FIELDS:
*
   R.1<8> = R.USER<EB.USE.COMPANY.CODE,1>
   R.1<7> = R.NEW(LD.LOCAL.REF)<1,LDLR.SEN.REC.BANK>
   R.1<9> = R.USER<EB.USE.COMPANY.CODE,1>
   R.1<11> = R.USER<EB.USE.DEPARTMENT.CODE>
   R.1<10> = R.USER<EB.USE.LANGUAGE>
   R.1<2> = ID.NEW
*
   RETURN
***************************************************************************************************************************
COMMON.760.767.PROCESS:
*
   GOSUB COUNT.FIELD
   GOSUB FIND.SEQUENCE
   GOSUB MANDATORY.FIELDS
*
   R.1<22> = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.NUMBER,1> ; * STATIC INFO, FIELD 20 OF SWIFT
   R.1<6> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.FUR.ID> ; * STATIC INFO, FIELD 23 OF SWIFT
*Line [ 259 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
   IF DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>,@SM) > 1 THEN
      SEND.CNT = 0
*Line [ 262 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
      FOR SEND.CNT = 1 TO DCOUNT(R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>,@SM)
         R.1<5,SEND.CNT> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO,SEND.CNT> ; * STATIC INFO, FIELD 72 OF SWIFT
      NEXT SEND.CNT
   END
   ELSE
      IF R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO> THEN R.1<5> = R.NEW(LD.LOCAL.REF)<1,LDLR.SW.SEND.INFO>
   END
*
   SEQ.CNT = 0 ; PROCESS.CNT.77 = 0
*
   FOR SEQ.CNT = 1 TO SEQ.MSG UNTIL ER.MSG
*
      IF SEQ.CNT > 1 THEN R.1<5> = ""
      R.1<1> = SEQ.CNT:"/":SEQ.MSG
      GOSUB SW.FIELD.77C
*
      GOSUB CALL.APP.HANDOFF
*
   NEXT SEQ.CNT
*
***************************************************************************************************************************
PROGRAM.END:
*
   RETURN
END
