* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.LD.EDIT(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
**************************************************
    FN.NAME = R.ENQ<2>
    T.SEL = "SELECT FBNK.":FN.NAME:" WITH DRAWDOWN.ISSUE.PRC NE REIMBURSE.AMOUNT "

    ENQ.LP  = '' ; OPER.VAL = ''

*Line [ 41 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR ENQ.LP = 1 TO DCOUNT(ENQ<2>,@VM)
        IF ENQ<3,ENQ.LP> = 'LK' THEN
            OPER.VAL = 'LIKE'
        END
        IF ENQ<3,ENQ.LP> = 'UL' THEN
            OPER.VAL = 'UNLIKE'
        END
        IF ENQ<3,ENQ.LP> # 'LK' AND  ENQ<3,ENQ.LP> # 'UL' THEN
            OPER.VAL = ENQ<3,ENQ.LP>
        END
        T.SEL := ' AND ':ENQ<2,ENQ.LP>:' ':OPER.VAL:' ':ENQ<4,ENQ.LP>

    NEXT ENQ.LP
*    T.SEL := ' AND VERSION.NAME EQ ':VER.NAME
    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
    IF SELECTED >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ<2> = '@ID'
        ENQ<3> = 'EQ'
        ENQ<4> = 'DUUMY'
    END

    RETURN
END
