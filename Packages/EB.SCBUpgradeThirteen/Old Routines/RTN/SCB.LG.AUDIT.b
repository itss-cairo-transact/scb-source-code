* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
**** CREATE BY NESSMA IN 01/02/2010 ****
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.LG.AUDIT

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.LD.LOANS.AND.DEPOSITS
    $INSERT           I_LD.LOCAL.REFS
*----------------------------------------------------------
    EOF        = ''
    BB.DATA    = ''
    COMP       = ID.COMPANY

    FN.LD   = "FBNK.LD.LOANS.AND.DEPOSITS"  ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.COMP = "F.COMPANY"                   ; F.VOMP  = ''
    CALL OPF(FN.COMP,F.COMP)
*----------------------------------------------------------
    COMP.ID   = COMP
    TOD.DATE  = TODAY
    TOD.MONTH = TOD.DATE[5,2]
    TOD.YEAR  = TOD.DATE[7,2]

    OPENSEQ "&SAVEDLISTS&" , "LTOG-":TOD.MONTH:TOD.YEAR:"-":COMP.ID:".CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'CANNOT CREATE File IN &SAVEDLISTS&'
        END
    END
*----------------------------------------------------------------
    T.SEL   = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CO.CODE EQ ": COMP.ID :" AND CATEGORY EQ 21096"
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED2,ERR)

    IF SELECTED2 THEN
        FOR NNN = 1 TO SELECTED2
            CALL F.READ( FN.LD,KEY.LIST<NNN>, R.LD, F.LD, Y.LD.ERR)
            PRODUCT.TYPE     = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
            CUSTOMER.ID      = R.LD<LD.CUSTOMER.ID>
            AMOUNT           = R.LD<LD.AMOUNT>
            CURRENCY         = R.LD<LD.CURRENCY>
            MARGIN.PERC      = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.PERC>
            VALUE.DATE       = R.LD<LD.VALUE.DATE>
            ACTUAL.EXP.DATE  = R.LD<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
            MARGIN.AMT       = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
            OLD.NO           = R.LD<LD.LOCAL.REF><1,LDLR.OLD.NO>

            BB.DATA  = PRODUCT.TYPE:','
            BB.DATA := CUSTOMER.ID:','
            BB.DATA := KEY.LIST<NNN>:','
            BB.DATA := AMOUNT:','
            BB.DATA := CURRENCY:','
            BB.DATA := MARGIN.PERC:','
            BB.DATA := VALUE.DATE:','
            BB.DATA := ACTUAL.EXP.DATE:','
            BB.DATA := MARGIN.AMT:','
            BB.DATA := OLD.NO:','

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE ":COMP.ID
            END
        NEXT NNN
    END

    CLOSESEQ BB
*---------------------
    TEXT = "FILE CREATED IN SAVEDLISTS" ; CALL REM
    TEXT = "LTOG-":TOD.MONTH:TOD.YEAR:"-":COMP.ID:".CSV"  ; CALL REM

    RETURN
END
