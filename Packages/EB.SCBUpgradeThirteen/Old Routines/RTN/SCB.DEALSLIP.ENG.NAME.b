* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.DEALSLIP.ENG.NAME(CUS.VAL)

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.CUSTOMER

    IF CUS.VAL THEN
        GOSUB PROCESS
    END
    RETURN
*----------------------------------------------
PROCESS:
*-------
    FN.CUS = "FBNK.CUSTOMER" ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    R.CUS   = '' ;  ERR.CUS = ''

    CALL F.READ(FN.CUS,CUS.VAL,R.CUS,F.CUS,ERR.CUS)

    IF R.CUS THEN
        CUS.VAL = R.CUS<EB.CUS.SHORT.NAME>
    END
    RETURN
END
