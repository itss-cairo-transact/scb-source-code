* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2420</Rating>
*-----------------------------------------------------------------------------
******ABEER 29/06/2003*************
******UPDATED AT  13/07/2003*******
    SUBROUTINE  SCB.LG.CUS.POSITION(ID,CURR,TXN.ISSUE,TXN.CANCEL,TXN.INC,TXN.DEC,TXN.COMMISSION,TXN.CONFIS)

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CUST.POSITION

  *  TEXT=CURR:'CURRRRRRR';CALL REM
    CUST.ID=FIELD(ID,'.',1)
    TODAY.MONTH=FIELD(ID,'.',2)
    ID.TODAY=CUST.ID:".":"..."
**********************************************************************************************************
    T.SEL = "SELECT F.SCB.LG.CUST.POSITION WITH @ID LIKE ": ID.TODAY
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            FN.LG.CUS.POS.ALL = 'F.SCB.LG.CUST.POSITION' ;R.CUS.ALL='';F.CUS.ALL=''
            CALL F.READ(FN.LG.CUS.POS.ALL,KEY.LIST<I>,R.CUS.ALL,F.CUS.ALL,E1)
            COUNT.DATE.2=R.CUS.ALL<SCB.TXN.DATE>
*Line [ 46 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COUNT.D.C=DCOUNT(COUNT.DATE.2,@VM)
            FOR J = 1 TO COUNT.D.C
                LOCATE CURR IN R.CUS.ALL<SCB.TXN.CURRENCY,J,1>  SETTING Y THEN
*                    IF CURR EQ R.CUS.ALL<SCB.TXN.CURRENCY><1,J,Y> THEN  TEMP=R.CUS.ALL<SCB.TXN.CURRENCY><1,J,Y>
                    *TEXT=R.CUS.ALL<SCB.TXN.CURRENCY><1,J,Y>:'RIGHTCURRENCY';CALL REM
                    *TEXT=R.CUS.ALL<SCB.TXN.TOT.BALANCE><1,J,Y>:'RICHTBAL.FOR.CURRENCY';CALL REM
                    TOT.BAL.4.LAST.VALUE=R.CUS.ALL<SCB.TXN.TOT.BALANCE><1,J,Y>
                    FLAG=1
                END
            NEXT J
        NEXT I
    END
*********************************************************************************************************
    FN.LG.CUS.POS='F.SCB.LG.CUST.POSITION';ID.SELECTED=KEY.LIST<SELECTED>;R.CUS.SELECTED='';F.CUS.SELECTED=''
    CALL F.READ( FN.LG.CUS.POS,ID.SELECTED, R.CUS.SELECTED, F.CUS.SELECTED, ETEXT)
    LAST.MONTH=FIELD(ID.SELECTED,'.',2)
***********************************************************
    TXN.SELECTED.DATE=R.CUS.SELECTED<SCB.TXN.DATE>
    *TEXT=TXN.SELECTED.DATE:'SELECTED.DATE';CALL REM
*Line [ 66 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    COUNT.D = DCOUNT(TXN.SELECTED.DATE,@VM)
    *TEXT=COUNT.D:'COUNT.D';CALL REM
    TOT.EQU.BAL=R.CUS.SELECTED<SCB.TOT.EQU.BALANCE><1,COUNT.D>
    *TEXT=TOT.EQU.BAL:'TOTEQU.BAL';CALL REM
    TXN.SELECTED.CURR=R.CUS.SELECTED<SCB.TXN.CURRENCY><1,COUNT.D>
    *TEXT=TXN.SELECTED.CURR:'SELECTED.CURR';CALL REM
*********************************************************************************************************
    FN.LG.CUS.POS='F.SCB.LG.CUST.POSITION';ID=ID;R.CUS='';F.CUS=''
    CALL F.READ( FN.LG.CUS.POS,ID, R.CUS, F.CUS, ETEXT)
**************GET ALL VALUES FOR DATE AND AMOUNTS*******************************************************
    TXN.DATE.E=R.CUS<SCB.TXN.DATE>
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    COUNT.DATE=DCOUNT(TXN.DATE.E,@VM)
*  TEXT=COUNT.DATE;CALL REM
    NEW.VALUE=COUNT.DATE+1
* TEXT=NEW.VALUE;CALL REM
********************************************************************************
    IF NOT(SELECTED) THEN
        R.CUS<SCB.TXN.DATE>=TODAY
        *TEXT='NEW.CUSTOMER';CALL REM
        R.CUS<SCB.TXN.CURRENCY>=CURR
*  TEXT=CURR;CALL REM
        R.CUS<SCB.TXN.INCREASE>=TXN.ISSUE
*  TEXT=TXN.ISSUE;CALL REM
        IF TXN.CANCEL='' THEN TXN.CANCEL= 0; R.CUS<SCB.TXN.DECREASE>=TXN.CANCEL
*  TEXT=TXN.DEC;CALL REM
        R.CUS<SCB.TXN.COMMISSION>=TXN.COMMISSION
*  TEXT=TXN.COMMISSION;CALL REM
        IF TXN.CONFIS='' THEN TXN.CONFIS= 0 ;R.CUS<SCB.TXN.CONFISCATION>=TXN.CONFIS
*  TEXT=TXN.CONFIS;CALL REM
        R.CUS<SCB.TXN.TOT.BALANCE>=TXN.ISSUE
****************To Get Equivalent For Commission&Balance******************
        LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = ""
        UNEQU.COMM= R.CUS<SCB.TXN.COMMISSION>
        UNEQU.BAL=R.CUS<SCB.TXN.TOT.BALANCE>
        MARKET = '1'
        IF CURR # LCCY THEN
            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.BAL,CURR,RATE,MARKET,LBAL,DIF.AMT,DIF.RATE)

            R.CUS<SCB.TOT.EQU.COMM>= LCOMM
            R.CUS<SCB.TOT.EQU.BALANCE>=LBAL
        END ELSE
            R.CUS<SCB.TOT.EQU.COMM>=UNEQU.COMM
            R.CUS<SCB.TOT.EQU.BALANCE>=UNEQU.BAL
        END
        CALL F.WRITE(FN.LG.CUS.POS,ID,R.CUS)
    END ELSE
        IF SELECTED THEN
            IF  LAST.MONTH GE TODAY.MONTH THEN
*********************************************************************
                TXN.SELECTED.DATE=R.CUS.SELECTED<SCB.TXN.DATE>
                *TEXT=TXN.SELECTED.DATE:'SELECTED.DATE';CALL REM
*Line [ 120 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                COUNT.D = DCOUNT(TXN.SELECTED.DATE,@VM)
                *TEXT=COUNT.D:'COUNT.D';CALL REM
                TOT.EQU.BAL=R.CUS.SELECTED<SCB.TOT.EQU.BALANCE><1,COUNT.D>
                TXN.SELECTED.CURR=R.CUS.SELECTED<SCB.TXN.CURRENCY><1,COUNT.D>
*********************************************************************
*   LOCATE TODAY  IN  TXN.DATE.E<1,1> SETTING I  THEN
                LOCATE TODAY  IN  TXN.SELECTED.DATE<1,1> SETTING I  THEN
*   TXN.CURR.E= R.CUS<SCB.TXN.CURRENCY>
                    TXN.CURR.E = R.CUS.SELECTED <SCB.TXN.CURRENCY>
                    *TEXT=TXN.CURR.E:'CURRENCY FOR TODAY';CALL REM
*  COUNT.CURR.E=DCOUNT(TXN.CURR.E,SM)
* NEW.CURR.VALUE=COUNT.CURR.E+1
* TEXT=COUNT.CURR.E:'NO.CURR';CALL REM
                    LOCATE CURR IN  TXN.CURR.E<1,I,1>   SETTING J  THEN
************************SAMEMONTH.SAMEDAY.SAMECURR****************************************
                        *TEXT='SAMEMONTH.SAMEDAY.SAMECURR';CALL REM
                        TXN.CURR.COMM.E=R.CUS<SCB.TXN.COMMISSION,I,J>
                        TXN.EQU.COMM.E=R.CUS<SCB.TOT.EQU.COMM,I,J>
                        TXN.CURR.CONFIS.E=R.CUS<SCB.TXN.CONFISCATION,I,J>
                        TXN.CURR.TOT.E=R.CUS<SCB.TXN.TOT.BALANCE,I,J>
                        TXN.EQU.BAL.E=R.CUS<SCB.TOT.EQU.BALANCE,I>
                        *TEXT=TXN.EQU.BAL.E:'EXISITING EGU BAL';CALL REM
***********************************************************************
                        NEW.TXN.INC=R.CUS<SCB.TXN.INCREASE,I,J>+TXN.ISSUE+TXN.INC
                        R.CUS<SCB.TXN.INCREASE,I,J>=NEW.TXN.INC

                        *TEXT=I;CALL REM
                        *TEXT=J;CALL REM
                        NEW.TXN.DEC=R.CUS<SCB.TXN.DECREASE,I,J>+TXN.CANCEL+TXN.DEC
                        R.CUS<SCB.TXN.DECREASE,I,J>=NEW.TXN.DEC

                        R.CUS<SCB.TXN.COMMISSION,I,J>=TXN.CURR.COMM.E+TXN.COMMISSION
                        IF TXN.CONFIS='' THEN
                            TXN.CONFIS= 0
                            R.CUS<SCB.TXN.CONFISCATION,I,J>=TXN.CURR.CONFIS.E+TXN.CONFIS
                        END ELSE
                            R.CUS<SCB.TXN.CONFISCATION,I,J>=TXN.CURR.CONFIS.E+TXN.CONFIS
                        END
                        TXN.DEC.CANCEL.CONFIS=TXN.CANCEL+TXN.DEC+TXN.CONFIS
                        R.CUS<SCB.TXN.TOT.BALANCE,I,J>=TXN.CURR.TOT.E+TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS
****************************************************************
                        LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = ""

                        UNEQU.COMM=R.CUS<SCB.TXN.COMMISSION,I,J>
                        *TEXT=UNEQU.COMM:'COMM.1';CALL REM
*                       UNEQU.BAL=R.CUS<SCB.TXN.TOT.BALANCE,I,J>
                        UNEQU.BAL=TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS
                        MARKET = '1'
                        IF CURR # LCCY THEN
                            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.BAL,CURR,RATE,MARKET,LBAL,DIF.AMT,DIF.RATE)

                            R.CUS<SCB.TOT.EQU.COMM,I,J>=LCOMM
                            R.CUS<SCB.TOT.EQU.BALANCE,I>=TOT.EQU.BAL+LBAL
                        END ELSE
                            R.CUS<SCB.TOT.EQU.COMM,I,J>=UNEQU.COMM
                            *TEXT=R.CUS<SCB.TOT.EQU.COMM,I,J>:'EQU.COMM';CALL REM
                            R.CUS<SCB.TOT.EQU.BALANCE,I>=TOT.EQU.BAL+UNEQU.BAL
                        END
                        CALL F.WRITE(FN.LG.CUS.POS,ID,R.CUS)
***************************************************************************
                    END ELSE
                        TXN.CURR.E= R.CUS<SCB.TXN.CURRENCY,I>
                        *TEXT=TXN.CURR.E:'CURRENCY FOR TODAY';CALL REM
*Line [ 185 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                        COUNT.CURR.E=DCOUNT(TXN.CURR.E,@SM)
                        NEW.CURR.VALUE=COUNT.CURR.E+1
                        *TEXT= NEW.CURR.VALUE:'NEW.CURR.VALUE';CALL REM
*************************************
                        *TEXT='SAMEMONTH.SAMEDAY.NEWCURR.OR.EXISITING.CURR';CALL REM
                        R.CUS<SCB.TXN.CURRENCY,I,NEW.CURR.VALUE>=CURR
                        R.CUS<SCB.TXN.INCREASE,I,NEW.CURR.VALUE>=TXN.ISSUE+TXN.INC
                        R.CUS<SCB.TXN.DECREASE,I,NEW.CURR.VALUE>=TXN.CANCEL+TXN.DEC
                        R.CUS<SCB.TXN.CONFISCATION,I,NEW.CURR.VALUE>=TXN.CONFIS
                        R.CUS<SCB.TXN.COMMISSION,I,NEW.CURR.VALUE>=TXN.COMMISSION
                        TXN.DEC.CANCEL.CONFIS=TXN.CANCEL+TXN.DEC+TXN.CONFIS
                        IF    FLAG=1 THEN
                            *TEXT='SAMEMONTH.SAMEDAY.SAMECURR.4.PREVIOUS.DAYS.OR.MONTH';CALL REM
                            R.CUS<SCB.TXN.TOT.BALANCE,I,NEW.CURR.VALUE>=TOT.BAL.4.LAST.VALUE+TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS
                        END ELSE
                            *TEXT='SAMEMONTH.SAMEDAY.NEWCURR';CALL REM
                            R.CUS<SCB.TXN.TOT.BALANCE,I,NEW.CURR.VALUE>=TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS
                        END
************************************************************
                        LCOMM="" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE=''
                        UNEQU.COMM=R.CUS<SCB.TXN.COMMISSION,I,NEW.CURR.VALUE>
* UNEQU.BAL=R.CUS<SCB.TXN.TOT.BALANCE,I,NEW.CURR.VALUE>
                        UNEQU.BAL=TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS
                        MARKET = '1'

                        IF CURR # LCCY THEN
                            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                            CALL MIDDLE.RATE.CONV.CHECK(UNEQU.BAL,CURR,RATE,MARKET,LBAL,DIF.AMT,DIF.RATE)
                            R.CUS<SCB.TOT.EQU.COMM,I,NEW.CURR.VALUE>=LCOMM
                            R.CUS<SCB.TOT.EQU.BALANCE,I>=TOT.EQU.BAL+LBAL
                        END ELSE
                            R.CUS<SCB.TOT.EQU.COMM,I,NEW.CURR.VALUE>=UNEQU.COMM
                            R.CUS<SCB.TOT.EQU.BALANCE,I>=TOT.EQU.BAL+UNEQU.BAL
                        END
************************************************************
                        CALL F.WRITE(FN.LG.CUS.POS,ID,R.CUS)
                    END
                END  ELSE
*  TEXT='SAMEMONTH.NEWDAY.NEWCURR';CALL REM
*  TXN.DATE.E=R.CUS<SCB.TXN.DATE>
*  COUNT.DATE=DCOUNT(TXN.DATE.E,VM)
*  NEW.VALUE=COUNT.DATE+1
*  TEXT=NEW.VALUE:'NEW.VALUE';CALL REM

                    R.CUS<SCB.TXN.DATE,NEW.VALUE>=TODAY
                    R.CUS<SCB.TXN.CURRENCY,NEW.VALUE,1>=CURR
                    R.CUS<SCB.TXN.INCREASE,NEW.VALUE,1>=TXN.ISSUE+TXN.INC
                    R.CUS<SCB.TXN.DECREASE,NEW.VALUE,1>=TXN.CANCEL+TXN.DEC
                    R.CUS<SCB.TXN.COMMISSION,NEW.VALUE,1>=TXN.COMMISSION
                    IF TXN.CONFIS='' THEN TXN.CONFIS= 0 ;R.CUS<SCB.TXN.CONFISCATION,NEW.VALUE,1>=TXN.CONFIS
                    TXN.DEC.CANCEL.CONFIS.NEW= R.CUS<SCB.TXN.DECREASE,NEW.VALUE,1>+R.CUS<SCB.TXN.CONFISCATION,NEW.VALUE,1>
                    IF FLAG = 1 THEN
                        R.CUS<SCB.TXN.TOT.BALANCE,NEW.VALUE,1>=TOT.BAL.4.LAST.VALUE+ R.CUS<SCB.TXN.INCREASE,NEW.VALUE>-TXN.DEC.CANCEL.CONFIS.NEW
                    END ELSE
                        R.CUS<SCB.TXN.TOT.BALANCE,NEW.VALUE,1>=R.CUS<SCB.TXN.INCREASE,NEW.VALUE>-TXN.DEC.CANCEL.CONFIS.NEW
                    END
*******************************************************
                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE=''
                    UNEQU.COMM=R.CUS<SCB.TXN.COMMISSION,NEW.VALUE,1>
*                    UNEQU.BAL=R.CUS<SCB.TXN.TOT.BALANCE,NEW.VALUE,1>
                    UNEQU.BAL=TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS.NEW
                    MARKET = '1'

                    IF CURR # LCCY THEN
                        CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                        CALL MIDDLE.RATE.CONV.CHECK(UNEQU.BAL,CURR,RATE,MARKET,LBAL,DIF.AMT,DIF.RATE)
                        R.CUS<SCB.TOT.EQU.COMM,NEW.VALUE,1>=LCOMM
                        R.CUS<SCB.TOT.EQU.BALANCE,NEW.VALUE>=TOT.EQU.BAL+LBAL
                    END ELSE
                        R.CUS<SCB.TOT.EQU.COMM,NEW.VALUE,1>=UNEQU.COMM
                        R.CUS<SCB.TOT.EQU.BALANCE,NEW.VALUE>=TOT.EQU.BAL+UNEQU.BAL
                    END
********************************************************
                    CALL F.WRITE(FN.LG.CUS.POS,ID,R.CUS)
                END
            END ELSE
*********************NEWMONTH.NEWDAY.NEWOREXISTING.CURRENCY*********
                IF  LAST.MONTH LT TODAY.MONTH THEN
                    R.CUS.NEW<SCB.TXN.DATE>=TODAY
                    R.CUS.NEW<SCB.TXN.CURRENCY>=CURR
                    R.CUS.NEW<SCB.TXN.INCREASE>=TXN.ISSUE+TXN.INC
                    R.CUS.NEW<SCB.TXN.DECREASE>=TXN.CANCEL+TXN.DEC
                    R.CUS.NEW<SCB.TXN.COMMISSION>=TXN.COMMISSION
                    IF TXN.CONFIS='' THEN TXN.CONFIS= 0; R.CUS.NEW<SCB.TXN.CONFISCATION>=TXN.CONFIS
                    *TEXT='NEW.MONTH.NEW.DATE.NEW.CURR';CALL REM
                    TXN.DEC.CANCEL.CONFIS.MONTH=R.CUS.NEW<SCB.TXN.DECREASE>+R.CUS.NEW<SCB.TXN.CONFISCATION>
************************************************************************
                    IF FLAG = 1 THEN
                        R.CUS.NEW<SCB.TXN.TOT.BALANCE>= TOT.BAL.4.LAST.VALUE+R.CUS.NEW<SCB.TXN.INCREASE>-TXN.DEC.CANCEL.CONFIS.MONTH
                    END ELSE
                        R.CUS.NEW<SCB.TXN.TOT.BALANCE>= R.CUS.NEW<SCB.TXN.INCREASE>-TXN.DEC.CANCEL.CONFIS.MONTH
                    END
***************************************************************************


                    LCOMM = "" ;LBAL=''; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = ""
                    UNEQU.COMM=R.CUS.NEW<SCB.TXN.COMMISSION>
*  UNEQU.BAL=R.CUS.NEW<SCB.TXN.TOT.BALANCE>
                    UNEQU.BAL=TXN.ISSUE+TXN.INC-TXN.DEC.CANCEL.CONFIS.MONTH
                    MARKET = '1'

                    IF CURR # LCCY THEN
                        CALL MIDDLE.RATE.CONV.CHECK(UNEQU.COMM,CURR,RATE,MARKET,LCOMM,DIF.AMT,DIF.RATE)
                        CALL MIDDLE.RATE.CONV.CHECK(UNEQU.BAL,CURR,RATE,MARKET,LBAL,DIF.AMT,DIF.RATE)
                        R.CUS.NEW<SCB.TOT.EQU.COMM> =LCOMM
                        R.CUS.NEW<SCB.TOT.EQU.BALANCE>=TOT.EQU.BAL+LBAL
                    END ELSE
                        R.CUS.NEW<SCB.TOT.EQU.COMM>= UNEQU.COMM
                        R.CUS.NEW<SCB.TOT.EQU.BALANCE>=TOT.EQU.BAL+UNEQU.BAL
                    END
                    CALL F.WRITE(FN.LG.CUS.POS,ID,R.CUS.NEW)
                END
            END
        END
    END
TEXT='FF';CALL REM
    RETURN
END
