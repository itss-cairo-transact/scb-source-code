* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-51</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SCB.FATCA.JOBO.DOC.DEFAULT

* This is attached as validation routine to CUSTOMER.ID in individual FCSI versions
* To default the document details for JO/BO/SO owners and to default REQ.DATE for primary customer
*-----------------------------------------


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO


*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS

    RETURN
*----------------------------------------

INITIALIZE:
*----------

    FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO = 'F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO'
    F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO = ''
    CALL OPF(FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO)

    FCSI.ID = ID.NEW       ; AV.POS = ''
    TYPE.INDX = ''         ; EXIST.FORM.OWNER = ''
    SEL.CMD = ''           ; SEL.LIST = ''
    JO.HOLDER.REF = ''     ; OW.POS = ''
    FORM.TYPE.COUNT = ''

    RETURN

PROCESS:
*-------

    SEL.CMD = "SELECT ": FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'','',SEL.ERR)
    CONVERT @FM TO @VM IN SEL.LIST
    IF COMI NE '' THEN

        JOBO.OWNER.ID = COMI
        FINDSTR JOBO.OWNER.ID IN SEL.LIST SETTING HOLDER.POS THEN
            R.NEW(FA.FI.HOLDER.REF)<1,AV> = FCSI.ID : "-" : AV
            JO.HOLDER.REF = R.NEW(FA.FI.HOLDER.REF)<1,AV>
            R.NEW(FA.FI.LEGAL.ENTITY.TYPE)<1,AV> = 'CONT'
            GOSUB DEFAULT.VALUE
        END ELSE
            AF = FA.FI.CUSTOMER.ID
            ETEXT = "FA-NOT.A.FATCA.CUSTOMER"
            CALL STORE.END.ERROR
        END
    END
    RETURN

DEFAULT.VALUE:
*-------------

    IF NOT(ETEXT) THEN
        CALL F.READ(FN.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,JOBO.OWNER.ID,R.JOBO.FCSI,F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO,JOBO.ERR)
        CNT = AV
        R.NEW(FA.FI.HOLDER.TIN)<1,AV> = R.JOBO.FCSI<FA.FI.TIN.CODE><1,1>
        R.NEW(FA.FI.HOLD.TIN.COUNTRY)<1,AV> = R.JOBO.FCSI<FA.FI.TIN.COUNTRY><1,1>
        R.NEW(FA.FI.HOLD.ADDR.COUNTRY)<1,AV> = R.JOBO.FCSI<FA.FI.ADDR.COUNTRY><1,1>
        EXIST.FORM.OWNER = R.NEW(FA.FI.FORM.OWNER)
        IF EXIST.FORM.OWNER EQ '' THEN
            R.NEW(FA.FI.FORM.OWNER)<1,1> = JO.HOLDER.REF
            GOSUB GET.FATCA.DETAILS
        END ELSE
            FIND JO.HOLDER.REF IN EXIST.FORM.OWNER SETTING HOLD.REF.POS THEN
                RETURN
            END ELSE
                R.NEW(FA.FI.FORM.OWNER)<1,-1> = JO.HOLDER.REF
                GOSUB GET.FATCA.DETAILS
            END
        END
    END
    RETURN

GET.FATCA.DETAILS:
*-----------------

    JOBO.FORM.OWNER = R.JOBO.FCSI<FA.FI.FORM.OWNER>

    FIND JOBO.OWNER.ID IN JOBO.FORM.OWNER SETTING OW.FIELD,OW.POS THEN

        JOBO.FORM.TYPE = R.JOBO.FCSI<FA.FI.FORM.TYPE><1,OW.POS>
*Line [ 113 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        FORM.TYPE.COUNT = DCOUNT(JOBO.FORM.TYPE,@SM)
        LOOP
            REMOVE JO.FORM.TYPE FROM JOBO.FORM.TYPE SETTING TYPE.POS
            TYPE.INDX += 1
            FORM.ID.LIST = R.JOBO.FCSI<FA.FI.FORM.ID><1,OW.POS,TYPE.INDX>
            FORM.REQ.DATES = R.JOBO.FCSI<FA.FI.REQ.DATE><1,OW.POS,TYPE.INDX>
            FORM.REC.DATES = R.JOBO.FCSI<FA.FI.RECV.DATE><1,OW.POS,TYPE.INDX>
            FORM.CUT.DATES = R.JOBO.FCSI<FA.FI.CUT.OFF.DATE><1,OW.POS,TYPE.INDX>
            FORM.EXP.DATES = R.JOBO.FCSI<FA.FI.EXP.DATE><1,OW.POS,TYPE.INDX>
            GOSUB WRITE.FORM.DETS
        UNTIL NOT(TYPE.POS)
        REPEAT
    END

    RETURN

WRITE.FORM.DETS:
*---------------

    R.NEW(FA.FI.FORM.TYPE)<1,CNT,TYPE.INDX> = JO.FORM.TYPE
    R.NEW(FA.FI.FORM.ID)<1,CNT,TYPE.INDX> = FORM.ID.LIST
    R.NEW(FA.FI.REQ.DATE)<1,CNT,TYPE.INDX> = FORM.REQ.DATES
    R.NEW(FA.FI.RECV.DATE)<1,CNT,TYPE.INDX> = FORM.REC.DATES
    R.NEW(FA.FI.CUT.OFF.DATE)<1,CNT,TYPE.INDX> = FORM.CUT.DATES
    R.NEW(FA.FI.EXP.DATE)<1,CNT,TYPE.INDX> = FORM.EXP.DATES

    RETURN
END
