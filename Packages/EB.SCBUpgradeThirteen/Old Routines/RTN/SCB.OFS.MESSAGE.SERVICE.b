* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.OFS.MESSAGE.SERVICE(OMQ.REC.ID)

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_OFS.MESSAGE.SERVICE.COMMON
    $INSERT T24.BP I_GTS.COMMON
    $INSERT T24.BP I_F.OFS.SOURCE
    $INSERT T24.BP I_GTS.COMMON
    $INSERT T24.BP I_F.TSA.SERVICE
    $INSERT T24.BP I_F.SPF
    $INSERT T24.BP I_IO.EQUATE
    $INSERT TEMENOS.BP I_SCB.OFS.MESSAGE.SERVICE.COMMON

*Line [ 29 ] Adding EB.SCBUpgradeThirteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeThirteen.OFS.MESSAGE.SERVICE(OMQ.REC.ID)

    RETURN
END
