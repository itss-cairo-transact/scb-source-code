* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1651</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

    SUBROUTINE SCB.ISCO.CF

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PARMS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.TYPE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUPPLIER.TYPE
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PAYMENT.TYPE
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

*       IF 21000 > ID.NEW[1,5]  OR ID.NEW[1,5] > 21300 THEN
*         E = 'This.Is.Wrong.Category' ; CALL ERR ; MESSAGE = 'REPEAT'
*       END
*         CURR = ''
*        CALL DBR('CURRENCY':@FM:@ID,ID.NEW[6,3],CURR)
*         IF ETEXT THEN E = 'This.Is.Wrong.CURRENCY' ; CALL ERR ; MESSAGE = 'REPEAT'
*         MM = ''
*         MM = TODAY
*         IF NOT(ID.NEW[9,11]) THEN ID.NEW[9,11] = MM
*_________________________________________________________________________________________

    IF E THEN CALL ERR ; V$ERROR = 1

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

*  FOR I = 1 TO DCOUNT(R.NEW(SCB.PAR.MIN.AMT),VM)
*         J = I - 1
*            IF R.NEW(SCB.PARMS.MIN.AMT)<1,I> < R.NEW(SCB.PARMS.MIN.AMT)<1,J> THEN
*             ETEXT = 'Enter.Smaller.Amount'
*            END
*
*        NEXT I

    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL

*     FOR I = 2 TO DCOUNT(R.NEW(SCB.PAR.MIN.AMT),VM)
*         J = I - 1
*            IF R.NEW(SCB.PARMS.MIN.AMT)<1,I> < R.NEW(SCB.PARMS.MIN.AMT)<1,J> THEN
*             ETEXT = 'Enter.Smaller.Amount'
*            END
*
*        NEXT I
*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
    RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


    RETURN

*************************************************************************

CHECK.DELETE:


    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************
DELIVERY.PREVIEW:

    RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



    RETURN

*************************************************************************

AFTER.UNAU.WRITE:


    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
    CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
    CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

    RETURN

*************************************************************************

INITIALISE:

    RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE = ""
    ID.CONCATFILE = ""

    ID.F  = "CF.DATE.SERIAL"; ID.N  = "30"; ID.T  = "A"
*    ID.T< 4> = 'R##### ### DDDDDDDD'

    Z = 0

    Z+=1 ; F(Z)  = "SEG.IDENT"             ; N(Z) = "4"      ; T(Z) = "A"
    Z+=1 ; F(Z)  = "DATA.PROV.ID"          ; N(Z) = "10"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "PRE.DATA.PROV.ID"      ; N(Z) = "10"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "DATA.PROV.BRANCH.ID"   ; N(Z) = "10"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "PRE.DATA.BRANCH.ID"    ; N(Z) = "10"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CF.ACC.NO"             ; N(Z) = "25"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CONSENT.STATUS"        ; N(Z) = "3"      ; T(Z) = "A"
    Z+=1 ; F(Z)  = "CONSENT.EXPIRE.DATE"   ; N(Z) = "8"      ; T(Z) = "D"
    Z+=1 ; F(Z)  = "PRIMARY.CARD.NO"       ; N(Z) = "16"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "PRE.CF.ACC.NO"         ; N(Z) = "25"     ; T(Z) = "A"
    Z+=1 ; F(Z)  = "PRE.PRIMARY.CARD.NO"   ; N(Z) = "16"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "APPROVAL.DATE"         ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "APPROVAL.AMOUNT"       ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "AMT.INSTALL"           ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "HIGHEST.CREDIT"        ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "CURRENCY"              ; N(Z) = "3"      ; T(Z)= "A"
    *CHECKFILE(Z) = "CURRENCY":FM:3
    Z+=1 ; F(Z)  = "LIABILITY.IND"         ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "CF.TYPE"               ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "TERM.OF.LOAN"          ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "REPAYMENT.TYPE"        ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "FIRST.DISBURSE.DATE"   ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "CF.ACCT.BALANCE"       ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "LAST.AMT.PAID"         ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "AMT.OVERDUE"           ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "NDPD"                  ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "NO.OF.PAYMENTS"        ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "ASSET.CLASS"           ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "ACC.STATUS"            ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "LAST.PAY.RECEIVE.DATE" ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "CF.AMENDMENT.DATE"     ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "CF.SETTLEMENT.DATE"    ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "PROTEST.NOTIFY"        ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "PROTEST.NOTIFY.DATE"   ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "AMT.WRITTEN.OFF"       ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "REASON.WRITTEN.OFF"    ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "AMT.FORGIVEN"          ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "REASON.AMT.FORGIVEN"   ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "ACCT.CLOSING.DATE"     ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "CLOSURE.REASON"        ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "COMMENT"               ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "SUIT.FIELD.STATUS"     ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "SUIT.REF.NO"           ; N(Z) = "16"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "SUIT.AMT"              ; N(Z) = "20"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "COURT"                 ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "COURT.LOCATION"        ; N(Z) = "60"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "COURT.DECREE"          ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "SUIT.DATE"             ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "DECREE.DATE"           ; N(Z) = "8"      ; T(Z)= "D"
    Z+=1 ; F(Z)  = "LEGAL.ACTION.ORIGIN"   ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "DISPUTE.NO"            ; N(Z) = "16"     ; T(Z)= "A"
    Z+=1 ; F(Z)  = "DISPUTE.STATUS"        ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "TRANS.TYPE.CODE"       ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "FILLER"                ; N(Z) = "1"      ; T(Z)= "A"
    Z+=1 ; F(Z)  = "NOTIFIED.ACCT"         ; N(Z) = "3"      ; T(Z)= "A"
    Z+=1 ; F(Z)= "RESERVED10"              ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED9"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED8"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED7"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"               ; N(Z) = "35"     ; T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9
    RETURN

*************************************************************************

END
