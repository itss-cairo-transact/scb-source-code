* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>934</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.GEN.400.FT(ERR.MSG)
*
* WRITTEN BY P.KARAKITSOS - 23/02/03
* SO AS TO GENERATE FTs FROM INCOMING MT400 MESSAGES
*
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AGENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.INW.400.FILE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DE.ADDRESS.BR
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*
   GOSUB SELECT.MT400
   IF KEY.LIST THEN
      GOSUB OPEN.FILES
      GOSUB READ.400.FILE
   END
*
   GOTO PROGRAM.END
***********************************************************************************************************************
SELECT.MT400:
*
   T.SEL = "SELECT F.SCB.INW.400.FILE"
   KEY.LIST = ""
   SELECTED = ""
*
   CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG)
   ERR.MSG = ""
*
   RETURN
***********************************************************************************************************************
READ.400.FILE:
*
   I = 0
   FOR I = 1 TO SELECTED UNTIL ERR.MSG
*
      R.SCB = ""
      CALL F.READU(FN.SCB,KEY.LIST<I>,R.SCB,F.SCB,ERR.MSG,"")
      IF NOT(ERR.MSG) THEN GOSUB BUILD.FT
      ELSE ERR.MSG = "MISSING SCB.INW.400.FILE ID = ":KEY.LIST<I>
*
   NEXT I
*
   RETURN
***********************************************************************************************************************
BUILD.FT:
*
   ID.FT = ""
   CALL GET.APPL.NEXT.ID("FBNK.FUNDS.TRANSFER","FT",ID.FT,ERR.MSG)
   IF NOT(ERR.MSG) & ID.FT THEN
*
      R.FT = "" ; TXN.CCY = "" ; TXN.CCY = R.SCB<SCB.400.CREDIT.CURRENCY>
      GOSUB FIND.THE.ACCOUNT
*
      R.FT<FT.TRANSACTION.TYPE> = "IT"
      R.FT<FT.DEBIT.THEIR.REF> = R.SCB<SCB.400.DEBIT.THEIR.REF>
      R.FT<FT.CREDIT.THEIR.REF> = R.SCB<SCB.400.ADD.REF>
      YAMT = "" ; YAMT = R.SCB<SCB.400.CREDIT.AMOUNT>
      IF NOT(NUM(YAMT)) THEN
         CONVERT ',' TO '.' IN YAMT
      END
      R.FT<FT.DEBIT.AMOUNT> = YAMT
      IF R.SCB<SCB.400.CREDIT.VALUE.DATE> THEN
         R.FT<FT.CREDIT.VALUE.DATE> = TODAY[1,2]:R.SCB<SCB.400.CREDIT.VALUE.DATE>
      END
      R.FT<FT.CREDIT.CURRENCY> = R.SCB<SCB.400.CREDIT.CURRENCY>
*
      IF R.SCB<SCB.400.PEG.IND> THEN R.FT<FT.LOCAL.REF,FTLR.PEG.IND> = "FIN"
*
      T.CUST = "" ; T.CUST = R.SCB<SCB.400.TELEX.FROM.CUST>
      GOSUB FIND.T.CUST
      R.FT<FT.TELEX.FROM.CUST> = T.NAME
*
      R.FT<FT.INWARD.PAY.TYPE> = "MT400"
      R.FT<FT.IN.ORDERING.BANK> = R.SCB<SCB.400.IN.ORDERING.BK>
      R.FT<FT.IN.BEN.BANK> = R.SCB<SCB.400.IN.BEN.BANK>
      R.FT<FT.IN.REC.CORR.BK> = R.SCB<SCB.400.IN.REC.CORR.BK>
      R.FT<FT.IN.SEND.CORR.BK> = R.SCB<SCB.400.IN.SEND.CORR.BK>
      R.FT<FT.IN.BK.TO.BK.INFO> = R.SCB<SCB.400.IN.BK.TO.BK>
      R.FT<FT.IN.ACCT.WITH.BANK> = R.SCB<SCB.400.ACCT.WITH.BK>
      R.FT<FT.LOCAL.REF,FTLR.RECEIV.DATE> = R.SCB<SCB.400.RECEIVE.DATE>
      R.FT<FT.LOCAL.REF,FTLR.INS.CCY.AMT> = R.SCB<SCB.400.AMT.CCY.COLL>
*
      INT.CNT = 0
*Line [ 119 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
      FOR INT.CNT = 1 TO DCOUNT(R.SCB<SCB.400.FOR.BK.CHR.COM>,@VM)
         R.FT<FT.LOCAL.REF,FTLR.FOR.BK.CHR.COM,INT.CNT> = R.SCB<SCB.400.FOR.BK.CHR.COM,INT.CNT>
      NEXT INT.CNT
*
      R.FT<FT.CREDIT.COMP.CODE> = ID.COMPANY
      R.FT<FT.DEBIT.COMP.CODE> = ID.COMPANY
      R.FT<FT.RECORD.STATUS> = "IHLD"
*
      TIME.STAMP = TIMEDATE()
      R.FT<FT.CURR.NO> = 1
      R.FT<FT.INPUTTER> = TNO:"_SWIFT400"
      X = OCONV(DATE(),"D-")
      X = X[9,2]:X[1,2]:X[4,2]:TIME.STAMP[1,2]:TIME.STAMP[4,2]
      R.FT<FT.DATE.TIME> = X
      R.FT<FT.CO.CODE> = ID.COMPANY
*
*  POSSIBLE AUTO - ROUTING
*
      IF R.SCB<SCB.400.FOR.BRANCH> THEN R.FT<FT.DEPT.CODE> = R.SCB<SCB.400.FOR.BRANCH>
      ELSE R.FT<FT.DEPT.CODE> = R.USER<EB.USE.DEPARTMENT.CODE>
*
      CALL F.DELETE(FN.SCB,KEY.LIST<I>)
      CALL F.RELEASE(FN.SCB,KEY.LIST<I>,F.SCB)
      CALL F.WRITE(FN.FT,ID.FT,R.FT)
      CALL JOURNAL.UPDATE(ID.FT)
*
   END
*
   RETURN
***********************************************************************************************************************
OPEN.FILES:
*
   FN.SCB = "F.SCB.INW.400.FILE" ; F.SCB = ""
   CALL OPF(FN.SCB,F.SCB)
*
   FN.FT = "F.FUNDS.TRANSFER$NAU" ; F.FT = ""
   CALL OPF(FN.FT,F.FT)
*
RETURN
***********************************************************************************************************************
FIND.T.CUST:
*
   T.NAME = ''
   T.SEL2 = "SELECT F":R.COMPANY(EB.COM.MNEMONIC):".AGENCY WITH"
   T.SEL2 := " SWIFT.CONF.ADDR = '":T.CUST:"'"
   KEY.LIST2 = '' ; SELECTED2 = '' ; S.VR2 = ''
   CALL EB.READLIST(T.SEL2, KEY.LIST2, '', SELECTED2, S.VR2) 
   IF KEY.LIST2 THEN
      S.NAME = ''; ETEXT = '' 
*Line [ 169 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CALL DBR("CUSTOMER":@FM:EB.CUS.SHORT.NAME, KEY.LIST2<1>, S.NAME)
      IF NOT(ETEXT) & S.NAME THEN
         T.NAME = S.NAME
      END
   END
*
RETURN
***********************************************************************************************************************
FIND.THE.ACCOUNT:
*
  DR.ACCOUNT = ''
  I.A = 1 ; E.FLAG = '' ; DEBIT.ACC = R.SCB<SCB.400.DEBIT.ACC>
*Line [ 182 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
  LOOP WHILE I.A <= DCOUNT(DEBIT.ACC, @VM) & NOT(E.FLAG)
    T.SEL.TEMP = "SELECT F":R.COMPANY(3):".AGENCY WITH"
    T.SEL.TEMP := " SWIFT.CONF.ADDR = '":DEBIT.ACC<1, I.A>:"'"
    KEY.LIST.TEMP = '' ; SELECTED.TEMP = '' ; S.VR = ''
    CALL EB.READLIST(T.SEL.TEMP,KEY.LIST.TEMP,'',SELECTED.TEMP, S.VR)
    IF KEY.LIST.TEMP THEN
       J = 1
       LOOP WHILE J <= SELECTED & NOT(E.FLAG)
          ETEXT = '' ; OUR.ACC = ''
*Line [ 192 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
          CALL DBR("AGENCY":@FM:EB.AG.NOSTRO.ACCT.NO,KEY.LIST.TEMP<1>,OUR.ACC)
          IF NOT(ETEXT) & OUR.ACC THEN
             G = 1
*Line [ 196 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
             LOOP WHILE G <= DCOUNT(OUR.ACC, @VM) & NOT(E.FLAG)
                ETEXT = '' ; AC.CCY = ''
*Line [ 199 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                CALL DBR("ACCOUNT":@FM:AC.CURRENCY, OUR.ACC<1, G>, AC.CCY)
                IF NOT(ETEXT) THEN
                   IF AC.CCY = TXN.CCY THEN
                      DR.ACCOUNT = OUR.ACC<1, G>
                      E.FLAG = 'TRUE'
                   END
                END
                G += 1
             REPEAT
          END 
          J += 1
       REPEAT
    END
    I.A += 1
  REPEAT
*
  IF DR.ACCOUNT THEN
     R.NEW(FT.DEBIT.ACCT.NO) = DR.ACCOUNT
  END
*
  ETEXT = ""
*
  RETURN
***********************************************************************************************************************
PROGRAM.END:
*
   RETURN
END
