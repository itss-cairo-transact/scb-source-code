* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
******************************************NI7OOOOOOOOOOOOOOOO****************
    SUBROUTINE SCB.DR1(ENQ)
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR

    FN.CUS='FBNK.CUSTOMER'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

    FN.STMT='FBNK.STMT.ACCT.DR'
    F.STMT=''
    R.STMT=''
    CALL OPF(FN.STMT,F.STMT)

    DATEE = '' ; YEAR1 = '' ; MONTH1= ''

    CALL DBR( 'DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
***  DATEE = '20081130'
*    YYYY = DATEE[1,4]
*    MM   = DATEE[5,2]
    CC   = DATEE[1,6]
* T.SEL = "SELECT FBNK.STMT.ACCT.CR WITH @ID LIKE 0130004910100101-20081130  AND @ID UNLIKE 011... AND @ID UNLIKE 012... AND @ID UNLIKE ...6501... AND @ID UNLIKE ...6502... AND @ID UNLIKE ...6503... AND @ID UNLIKE ...1000... AND  @ID UNLIKE ...1100... "
*****T.SEL = "SELECT FBNK.STMT.ACCT.CR WITH @ID LIKE ...-20090331  AND @ID UNLIKE 011... AND @ID UNLIKE 012... AND @ID UNLIKE ...6501... AND @ID UNLIKE ...6502... AND @ID UNLIKE ...6503... AND @ID UNLIKE ...1000... AND  @ID UNLIKE ...1100... AND @ID UNLIKE ...9090... "
    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID EQ 0130432020140101-20090430 AND @ID UNLIKE ...9090... "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM

    CALL F.READ(FN.STMT,KEY.LIST,R.STMT,F.STMT,E2)
***    CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)
    TOT.INTEREST  = R.STMT<IC.STMDR.TOTAL.INTEREST>
    LAST.PERIODIC = R.STMT<IC.STMDR.PERIOD.LAST.DATE>
    TEXT = TOT.INTEREST ; CALL REM
    TEXT = LAST.PERIODIC ; CALL REM
    ENQ.LP   = '' ;

    MYID  = FIELD(KEY.LIST,'-',1)
    VV    = MYID[11,4]
    CUS   = MYID[2,7]
***TEXT  = "MYID = :": MYID ; CALL REM
***TEXT  = "VV   = :": VV   ; CALL REM
    ACC.NO = FIELD(MYID,'-',1)
    TEXT = "ACC.NO : " : ACC.NO ; CALL REM
*Line [ 74 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL DBR ('ACCOUNT' ,@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
*Line [ 76 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL DBR ('CUSTOMER',@FM:EB.CUS.SECTOR,CUST.ID,SECNO)
***TEXT = " SECNO = " :SECNO
*** CALL DBR ('SECTOR':@FM:EB.SEC.DESCRIPTION,SECNO,SECNAME)
***SECTORNAME=SECNAME<2,2>
***  TEXT = " SECTORNAME = " : SECNAME<2,2>
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            IF VV NE 1000 OR VV NE 1101 AND VV NE 6501 AND VV NE 6502 AND VV NE 6503 AND VV NE 6504 AND VV NE 1000 AND VV NE 1100 AND VV NE 1200  AND SECNO NE 1000 AND SECNO NE 1100 AND SECNO NE 1100 AND SECNO NE 1200  AND SECNO NE 1300 THEN
***** IF VV[11,4] NE 6501 AND VV[11,4] NE 6502 AND VV[11,4] NE 6503 AND VV[11,4] NE 6504 AND VV[11,4] NE 1000 AND VV [11,4] NE 1100 AND VV[11,4] NE 1200 AND VV[11,4] NE 1002 AND VV[11,4] NE 1710 VV[11,4] NE 1711  AND SEC NE 1100 AND SEC NE 1200 AND SEC 1300 AND SEC NE '' THEN
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
            END
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
