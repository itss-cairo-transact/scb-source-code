* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
***************************************************************************************************************************
    SUBROUTINE SCB.DEALSLIP.ARAB.NAME(CUS.VAL)
***************************************************************************************************************************

* Description       : This routine is used to display the local field value (ARABIC.NAME) of  customer record in Deal SLIP
*                   : This routine is written during R15 Upgrade since Local field values failed to display in deal slip
* Attached To       : DEAL.DLIP.FORMAT>TT.DEPOSIT.LST3
* In & Out Argument : CUS.VAL

***************************************************************************************************************************

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT TEMENOS.BP I_CU.LOCAL.REFS

    IF CUS.VAL THEN
        GOSUB PROCESS
    END
    RETURN

*--
PROCESS:
*--

    FN.CUS = "F.CUSTOMER"
    F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    R.CUS = ''
    ERR.CUS = ''
    CALL F.READ(FN.CUS,CUS.VAL,R.CUS,F.CUS,ERR.CUS)
    IF R.CUS THEN
        LOCAL.CUS.VAL = ''
        LOCAL.CUS.VAL = R.CUS<EB.CUS.LOCAL.REF>
        CUS.VAL = LOCAL.CUS.VAL<1,CULR.ARABIC.NAME>
*       CUS.VAL = LOCAL.CUS.VAL<1,CULR.TAX.EXEMPTION>
*       CUS.VAL = R.CUS<EB.CUS.SECTOR>
*       CUS.VAL = R.CUS<EB.CUS.TEXT,1>
    END
    RETURN

END
