* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.GUID.ID
    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.AC.LOCKED.EVENTS
    $INSERT T24.BP I_AT.ISO.COMMON

    Y.57 = AT$INCOMING.ISO.REQ(57)
    IF Y.57 THEN
        Y.57.LEN = LEN(Y.57)
        DE.57 = Y.57[7,Y.57.LEN-6]
        CALL GET.LOC.REF('AC.LOCKED.EVENTS','GUID',LRF.POS)

        R.NEW(AC.LCK.LOCAL.REF)<1,LRF.POS> = DE.57
    END
    RETURN
