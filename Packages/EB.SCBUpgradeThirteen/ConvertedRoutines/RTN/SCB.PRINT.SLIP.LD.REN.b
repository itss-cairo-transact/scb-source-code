* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMzQ0ODU6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-15</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.PRINT.SLIP.LD.REN


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.RENEW.METHOD
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.TXN.TYPE.CONDITION
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.REN.LD.A",OFS.SOURCE.REC,F.OFS.SOURCE,'')

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.OUT = OFS.SOURCE.REC<OFS.SRC.OUT.QUEUE.DIR>
    F.OFS.IN = 0

*    OFS.REC = ""
*   OFS.OPERATION = "COLLATERAL"
*  OFS.USER.INFO = "ZEAD03/123456"
* OFS.TRANS.ID = ""
*OFS.MESSAGE.DATA = ""

* COMMA = ","

*    DEBUG
    T.SEL = "SELECT ":FN.OFS.OUT:" WITH @ID LIKE ...-":TODAY:"-A"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FN.OFS.OUT= FN.OFS.OUT ; F.OFS.OUT = '' ; R.OFS.OUT = ''
    CALL OPF( FN.OFS.OUT,F.OFS.OUT)

    IF KEY.LIST THEN
*        DEBUG
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.OFS.OUT,KEY.LIST<I>, R.OFS.OUT,F.OFS.OUT, ETEXT)
            Z = FIELD(R.OFS.OUT,"/",1)
            YY = KEY.LIST<I>

            CALL SCB.REPRINT.SLIP.LD.REN(Z)

        NEXT I
    END
    TEXT ="��� ������� �����" ; CALL REM

    RETURN
**************************************************************************************************************
END
