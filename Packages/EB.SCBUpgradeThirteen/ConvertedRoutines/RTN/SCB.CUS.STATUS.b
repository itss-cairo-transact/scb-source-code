* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMTc4MjM6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:41:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>68</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.CUS.STATUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN
*-----------------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"
    F.OFS.SOURCE  = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "CUSTOMER"
*--- EDIT BY NESSMA
    OFS.OPTIONS   = "SCB21"
*--- END EDIT
    COMP          = C$ID.COMPANY
    COM.CODE      = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP

    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""
    RETURN
*-----------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = "," ; KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    TEMP.DATE = TODAY[1,4]
    AFTR.DATE = TODAY[7,2]
    NEW.DATE  = TEMP.DATE-21:TODAY[5,2]:AFTR.DATE

    T.SEL  = "SELECT FBNK.CUSTOMER WITH ( POSTING.RESTRICT NE 99 AND POSTING.RESTRICT NE 90 AND POSTING.RESTRICT NE 91 ) "
    T.SEL := "AND CUSTOMER.STATUS EQ 22 "
    T.SEL := "AND (SECTOR EQ 1100 OR SECTOR EQ 1200 OR SECTOR EQ 1300 OR "
    T.SEL := "SECTOR EQ 1400 OR SECTOR EQ 2000) AND BIRTH.INCORP.DATE "
    T.SEL := "LE " : NEW.DATE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            OFS.MESSAGE.DATA  = "POSTING.RESTRICT=":21:","
            OFS.MESSAGE.DATA := "CUSTOMER.STATUS=":2
            OFS.TRANS.ID  = KEY.LIST <I>
            ID.KEY.LIST   = KEY.LIST<I>
            F.PATH        = FN.OFS.IN
            OFS.REC       = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA

            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, "MINOR-":KEY.LIST<I>:"-":TODAY :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
        NEXT I
    END
    RETURN
END
