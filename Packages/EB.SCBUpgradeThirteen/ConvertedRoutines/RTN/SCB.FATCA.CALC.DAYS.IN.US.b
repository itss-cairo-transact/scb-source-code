* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjIzOTg6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SCB.FATCA.CALC.DAYS.IN.US

*-----------------------------------------
*Subroutine to calculate number of days spent by the individual in US for
*current year, last year and previous to last year and accordingly change the citizenship to US
*-----------------------------------------

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO

*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS

    RETURN
*----------------------------------------

INITIALIZE:
*---------

    CUR.POS = '' ; CUR.YR.VALUE = ''
    LAS.POS = '' ; LAS.YR.VALUE = ''
    PRE.POS = '' ; PRE.YR.VALUE = ''
    TOT.LAS.YR.VALUE = '' ; TOT.PRE.YR.VALUE = ''
    TOTAL.DAY.IN.US = ''

    CALL GET.LOC.REF("FATCA.CUSTOMER.SUPPLEMENTARY.INFO","IN.US.CURR.YR",CUR.POS)
    CALL GET.LOC.REF("FATCA.CUSTOMER.SUPPLEMENTARY.INFO","IN.US.LAST.YR",LAS.POS)
    CALL GET.LOC.REF("FATCA.CUSTOMER.SUPPLEMENTARY.INFO","IN.US.PRE.LA.YR",PRE.POS)

    RETURN

PROCESS:
*------

    LAS.YR.VALUE = R.NEW(FA.FI.LOCAL.REF)<1,LAS.POS>
    PRE.YR.VALUE = R.NEW(FA.FI.LOCAL.REF)<1,PRE.POS>

    TOT.LAS.YR.VALUE = DROUND((LAS.YR.VALUE/'3'),2)
    TOT.PRE.YR.VALUE = DROUND((PRE.YR.VALUE/'6'),2)

    IF COMI NE '' THEN
        CUR.YR.VALUE = COMI

        IF CUR.YR.VALUE GE '31' THEN
            TOTAL.DAY.IN.US = DROUND((CUR.YR.VALUE + TOT.LAS.YR.VALUE + TOT.PRE.YR.VALUE),2)
            IF TOTAL.DAY.IN.US GE '183' THEN
                R.NEW(FA.FI.GREENCARD) = 'As of substantial presence Test Calculation'
            END
        END
    END

    RETURN

END
