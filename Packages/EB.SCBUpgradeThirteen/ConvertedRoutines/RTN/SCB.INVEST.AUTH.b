* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjU1Nzk6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.INVEST.AUTH
*    PROGRAM SCB.INVEST.AUTH
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.INVEST.FUND

    EXECUTE 'COMO ON SCB.INVEST.AUTH'
    CRT 'EXECUTE XXXXXX'
    CRT 'V&FUNCTION' : V$FUNCTION 
    GOSUB PROC 
    EXECUTE 'COMO OFF SCB.INVEST.AUTH'

    RETURN
PROC:
    className = 't24java1.T24Java1'
    methodName = '$CallWebService'
    X.STATUS =  '' ; X.STATUS.DESC = '' ; X.RET = '' 
    CUS.ID = R.NEW(SCB.INV.CUSTOMER.ID)
    CLIENT.ACC = R.NEW(SCB.INV.CLIENT.AC.NO)
    SCI.DATE = R.NEW(SCB.INV.DATE.TIME)<1,1>
    SCI.AMT = R.NEW(SCB.INV.TOTAL.AMOUNT)
    SCI.TYPE = R.NEW(SCB.INV.RTN.TYPE)
    SCI.DATE = '20':SCI.DATE[1,2]:'-':SCI.DATE[3,2]:'-':SCI.DATE[5,2]:' ':SCI.DATE[7,2]:':':SCI.DATE[9,2]:':00.000'
    CRT 'CUS.ID : ':CUS.ID:' / CLIENT.ACC : ':SCI.DATE:' / SCI.AMT : ':SCI.AMT
    PARAM = ID.NEW:'::':SCI.AMT:'::':SCI.DATE:'::':CLIENT.ACC:'::':SCI.TYPE 
    CRT 'PARAM : ':PARAM 
    CALLJ className, methodName, PARAM SETTING RET ON ERROR GOTO errHandler 
    R.NEW(SCB.INV.API.RESPONSE.CODE) = RET
    X.RET = RET
    CHANGE '"' TO '' IN X.RET
    X.RET = X.RET[2,LEN(X.RET)-2]
    CRT 'RESPONSE CODE : ':X.RET
    X.STATUS = FIELD(X.RET,',',1)
    X.STATUS.DESC = FIELD(X.RET,',',2)
    X.STATUS = FIELD(X.STATUS,':',2)
    X.STATUS.DESC = FIELD(X.STATUS.DESC,':',2)
    CRT 'STATUS DESC : ':X.STATUS.DESC:' / STATUS : ':X.STATUS
    IF X.STATUS.DESC EQ 'Pending' AND X.STATUS EQ '1' THEN
        R.NEW(SCB.INV.REPLY.CODE) = ',RESPONS.CODE=A00,'
        CRT 'RESPONSE.CODE : 00'
    END
    ELSE
        R.NEW(SCB.INV.REPLY.CODE) = ',RESPONS.CODE=D12,'
        CRT 'RESPONSE.CODE : 12'
    END
    CRT "Received batch from Java : " : RET


    RETURN
errHandler:
    err = SYSTEM(0)
    CRT 'ERROR : ':err
    IF err = 2 THEN

        CRT "Cannot find the JVM.dll !"

        RETURN

    END

    IF err = 3 THEN

        CRT "Class " : className : "doesn't exist !"

        RETURN

    END

    IF err = 5 THEN

        CRT "Method " : methodName : "doesn't exist !"

        RETURN

    END
    RETURN
END
