* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjM0OTg6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>375</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.GEN.103.FT(MY.ID, ID.INWARD, ID.FT, ERR.MSG)

* 02/07/2002 - N.Betsias, INFORMER S.A Greece
* New subroutine created in order to create FT txns for MT103.
* This program read the SCB.INW.103.FILE (updated while ALLIANCE.IN is
* running).
* This program called from FT.IN.PROCESSING.3
*
* UPDATED BY P.KARAKITSOS - 22/02/03
* SO AS TO 1) ENABLE AUTO - ROUTING IF POSSIBLE. IF THE BANK HAS ONLY ONE SWIFT
* ADDRESS FOR ALL BRANCHES (E.G BARCLAYS) THEN AUTO - ROUTING CANNOT BE APPLIED (NOT NEEDED ACTUALLY)
* 2) CREATE NEW FIELD INSTRCTN.CODE
* 3) UPDATE LOCAL FIELD PEG.IND (ONLY FOR EGYPT)
*
* UPDATED BY P.KARAKITSOS - 08/03/03
* SO AS TO DEFAULT FIELD CREDIT.ACCT.NO IF POSSIBLE
*
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AGENCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.INW.103.FILE
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*
* PK - 22/02/03 START
*
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DE.ADDRESS.BR
*
* PK - 22/02/03 END
*
ERR.MSG = ''
GOSUB OPEN.FILES
GOSUB READ.103.FILE
IF NOT(ERR.MSG) THEN
   GOSUB BUILD.FT.PAYMENT
END

GOTO PROGRAM.END

**************************************************************************
READ.103.FILE:

   CALL F.READU(FN.SCB.INW.103.FILE, MY.ID, R.SCB,
   F.SCB.INW.103.FILE, ERR.MSG, '')

RETURN
**************************************************************************

**************************************************************************
OPEN.FILES:

   FN.SCB.INW.103.FILE = 'F.SCB.INW.103.FILE' ; F.SCB.INW.103.FILE = ''
   CALL OPF(FN.SCB.INW.103.FILE, F.SCB.INW.103.FILE)

   Y.FUNDS.TRANSFER = 'F.FUNDS.TRANSFER' ; F.FUNDS.TRANSFER = ''
   CALL OPF(Y.FUNDS.TRANSFER,F.FUNDS.TRANSFER)

   Y.FUNDS.TRANSFER$NAU = 'F.FUNDS.TRANSFER$NAU' ; F.FUNDS.TRANSFER$NAU = ''
   CALL OPF(Y.FUNDS.TRANSFER$NAU,F.FUNDS.TRANSFER$NAU)

RETURN
***************************************************************************

***************************************************************************
BUILD.FT.PAYMENT:

 R.NEW.REC.SIZE = FT.AUDIT.DATE.TIME
 MAT R.NEW = ""

* APP.IND = 'INW'
* CALL FT.GENERATE.ID(APP.IND,ID.FT)

  ER.MSG = '' ; ID.FT = ""
* APP.IND = 'INW'
* CALL FT.GENERATE.ID(APP.IND,ID.FT)

 CALL GET.APPL.NEXT.ID("FBNK.FUNDS.TRANSFER", "FT", ID.FT, ER.MSG)

 R.NEW(FT.TRANSACTION.TYPE) = 'IT'

 TXN.CCY = R.SCB<SCB.INW.DEBIT.CURRENCY>
 GOSUB FIND.THE.ACCOUNT

 R.NEW(FT.DEBIT.THEIR.REF) = R.SCB<SCB.INW.DEBIT.THEIR.REF>
 IF LEN(R.SCB<SCB.INW.DEBIT.VALUE.DATE>) = 6 THEN
    CALL EB.CHECK.DATE(R.SCB<SCB.INW.DEBIT.VALUE.DATE>)
 END
 R.NEW(FT.DEBIT.VALUE.DATE) = TODAY[1,2]:R.SCB<SCB.INW.DEBIT.VALUE.DATE>
 YCCY = R.SCB<SCB.INW.DEBIT.CURRENCY>
 YAMT = R.SCB<SCB.INW.DEBIT.AMOUNT>
 *CALL SC.FORMAT.CCY.AMT(YCCY, YAMT)
 R.NEW(FT.DEBIT.CURRENCY) = YCCY
 IF NOT(NUM(YAMT)) THEN
    CONVERT ',' TO '.' IN YAMT
 END
 R.NEW(FT.DEBIT.AMOUNT) = YAMT
 R.NEW(FT.INWARD.PAY.TYPE) = "MT103"
 R.NEW(FT.DELIVERY.INREF) = ID.INWARD
 R.NEW(FT.LOCAL.REF)<1, FTLR.INS.CCY.AMT> = R.SCB<SCB.INW.INS.CCY.AMT>
 R.NEW(FT.LOCAL.REF)<1, FTLR.EXC.RATE> = R.SCB<SCB.INW.EXC.RATE>
 
 R.NEW(FT.IN.ORDERING.CUST) = R.SCB<SCB.INW.IN.ORDERING.CUS>

*Line [ 135 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
 IF DCOUNT(R.NEW(FT.IN.ORDERING.CUST), @VM) = 5 THEN
    R.NEW(FT.IN.ORDERING.CUST)<1,1> = ''
    DEL R.NEW(FT.IN.ORDERING.CUST)<1,1>
 END

 R.NEW(FT.ORDERING.CUST) = R.NEW(FT.IN.ORDERING.CUST)

 T.CUST = R.SCB<SCB.INW.TELEX.FROM.CUST>
 GOSUB FIND.T.CUST
 R.NEW(FT.TELEX.FROM.CUST) = T.NAME

 R.NEW(FT.IN.ORDERING.BANK) = R.SCB<SCB.INW.IN.ORDERING.BK>
*** N.B 23/09/2002
*Line [ 149 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
IF DCOUNT(R.NEW(FT.IN.ORDERING.BANK), @VM) = 5 THEN
   R.NEW(FT.IN.ORDERING.BANK)<1,1> = ''
   DEL R.NEW(FT.IN.ORDERING.BANK)<1,1>
END

 R.NEW(FT.IN.SEND.CORR.BK) = R.SCB<SCB.INW.IN.SEND.CORR.BK>
 R.NEW(FT.IN.REC.CORR.BK) = R.SCB<SCB.INW.IN.REC.CORR.BK>
 R.NEW(FT.LOCAL.REF)<1, FTLR.INTEM.INST> = R.SCB<SCB.INW.INTEM.INST>
 R.NEW(FT.IN.ACCT.WITH.BANK) = R.SCB<SCB.INW.ACCT.WITH.BK>
 R.NEW(FT.IN.BEN.ACCT.NO) = R.SCB<SCB.INW.IN.BEN.ACCT.NO>
 R.NEW(FT.IN.BEN.CUSTOMER) = R.SCB<SCB.INW.IN.BEN.CUSTOMER>
 R.NEW(FT.IN.PAYMENT.DETAILS) = R.SCB<SCB.INW.IN.PAY.DETAILS>
 R.NEW(FT.LOCAL.REF)<1, FTLR.FOR.BK.CHR.COM,1> = R.SCB<SCB.INW.FOR.BK.CHR.COM>
 R.NEW(FT.IN.BK.TO.BK.INFO) = R.SCB<SCB.INW.IN.BK.TO.BK>
 R.NEW(FT.LOCAL.REF)<1, FTLR.RECEIV.DATE> = R.SCB<SCB.INW.RECEIVE.DATE>

 R.NEW(FT.CREDIT.COMP.CODE) = ID.COMPANY
 R.NEW(FT.DEBIT.COMP.CODE) = ID.COMPANY
*
* PK - 22/02/03 START
* IF 57A OF SWIFT HAS VALUE THEN AUTO - ROUTING MAY BE ENABLED
*
* R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER<EB.USE.DEPARTMENT.CODE> 
*
  IF R.SCB<SCB.INW.FOR.BRANCH> THEN
     R.NEW(FT.PROFIT.CENTRE.DEPT) = R.SCB<SCB.INW.FOR.BRANCH>
     R.NEW(FT.DEPT.CODE) = R.SCB<SCB.INW.FOR.BRANCH>
  END
  ELSE
     R.NEW(FT.PROFIT.CENTRE.DEPT) = R.USER<EB.USE.DEPARTMENT.CODE>
     R.NEW(FT.DEPT.CODE) = R.USER<EB.USE.DEPARTMENT.CODE>
  END
  R.NEW(FT.INSTRCTN.CODE) = R.SCB<SCB.INW.INSTRCTN.CODE>
  IF R.SCB<SCB.INW.PEG.IND> THEN R.NEW(FT.LOCAL.REF)<1,FTLR.PEG.IND> = R.SCB<SCB.INW.PEG.IND>
*
* PK - 22/02/03 END
*
*
* PK - 08/03/03 START
*
  IF R.SCB<SCB.INW.CREDIT.ACCT.NO> THEN R.NEW(FT.CREDIT.ACCT.NO) = R.SCB<SCB.INW.CREDIT.ACCT.NO>
*
* PK - 08/03/03 END
*
 R.NEW(FT.RECORD.STATUS) = 'IHLD'

 TIME.STAMP = TIMEDATE()
 R.NEW(FT.CURR.NO) = 1
 R.NEW(FT.INPUTTER) = TNO:'_':"SWIFT103"
 X=OCONV(DATE(),"D-")
 X=X[9,2]:X[1,2]:X[4,2]:TIME.STAMP[1,2]:TIME.STAMP[4,2]
 R.NEW(FT.DATE.TIME)=X
*
* PK - 22/02/03 R.NEW(FT.DEPT.CODE)=R.USER<EB.USE.DEPARTMENT.CODE>
*
 R.NEW(FT.CO.CODE)=ID.COMPANY

 CALL F.DELETE(FN.SCB.INW.103.FILE, MY.ID)

** RELEASE F.SCB.INW.103.FILE, MY.ID
CALL F.RELEASE(F.SCB.INW.103.FILE, MY.ID,F.SCB.INW.103.FILE)
 
  CALL F.MATWRITE(Y.FUNDS.TRANSFER$NAU,ID.FT,
       MAT R.NEW,R.NEW.REC.SIZE)

RETURN
****************************************************************************

****************************************************************************
FIND.T.CUST:

   T.NAME = ''
   T.SEL2 = "SELECT F":R.COMPANY(3):".AGENCY WITH"
   T.SEL2 := " SWIFT.CONF.ADDR = '":T.CUST:"'"
   KEY.LIST2 = '' ; SELECTED2 = '' ; S.VR2 = ''
   CALL EB.READLIST(T.SEL2, KEY.LIST2, '', SELECTED2, S.VR2) 
   IF KEY.LIST2 THEN
      S.NAME = ''; ETEXT = '' 
*Line [ 228 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CALL DBR("CUSTOMER":@FM:EB.CUS.SHORT.NAME, KEY.LIST2<1>, S.NAME)
      IF NOT(ETEXT) & S.NAME THEN
         T.NAME = S.NAME
      END
   END

RETURN
****************************************************************************

****************************************************************************
FIND.THE.ACCOUNT:

  DR.ACCOUNT = ''
  I.A = 1 ; E.FLAG = '' ; DEBIT.ACC = R.SCB<SCB.INW.DEBIT.ACC>
*Line [ 243 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
  LOOP WHILE I.A <= DCOUNT(DEBIT.ACC, @VM) & NOT(E.FLAG)
    T.SEL = "SELECT F":R.COMPANY(3):".AGENCY WITH"
    T.SEL := " SWIFT.CONF.ADDR = '":DEBIT.ACC<1, I.A>:"'"
    KEY.LIST = '' ; SELECTED = '' ; S.VR = ''
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED, S.VR)
    IF KEY.LIST THEN
       J = 1
       LOOP WHILE J <= SELECTED & NOT(E.FLAG)
          ETEXT = '' ; OUR.ACC = ''
*Line [ 253 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
          CALL DBR("AGENCY":@FM:EB.AG.NOSTRO.ACCT.NO,KEY.LIST<1>,OUR.ACC)
          IF NOT(ETEXT) & OUR.ACC THEN
             G = 1
*Line [ 257 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
             LOOP WHILE G <= DCOUNT(OUR.ACC, @VM) & NOT(E.FLAG)
                ETEXT = '' ; AC.CCY = ''
*Line [ 260 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
                CALL DBR("ACCOUNT":@FM:AC.CURRENCY, OUR.ACC<1, G>, AC.CCY)
                IF NOT(ETEXT) THEN
                   IF AC.CCY = TXN.CCY THEN
                      DR.ACCOUNT = OUR.ACC<1, G>
                      E.FLAG = 'TRUE'
                   END
                END
                G += 1
             REPEAT
          END 
          J += 1
       REPEAT
    END
    I.A += 1
  REPEAT

  IF DR.ACCOUNT THEN
     R.NEW(FT.DEBIT.ACCT.NO) = DR.ACCOUNT
  END

RETURN
****************************************************************************


PROGRAM.END:

RETURN

END
