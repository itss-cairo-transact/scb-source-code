* @ValidationCode : MjotOTQzNzgzNjQwOkNwMTI1MjoxNjQxODA4ODU4NDkzOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:00:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
***************************************************************************************************************************
SUBROUTINE SCB.DEALSLIP.ARAB.NAME(CUS.VAL)
***************************************************************************************************************************

* Description       : This routine is used to display the local field value (ARABIC.NAME) of  customer record in Deal SLIP
*                   : This routine is written during R15 Upgrade since Local field values failed to display in deal slip
* Attached To       : DEAL.DLIP.FORMAT>TT.DEPOSIT.LST3
* In & Out Argument : CUS.VAL

***************************************************************************************************************************
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_CU.LOCAL.REFS

    IF CUS.VAL THEN
        GOSUB PROCESS
    END
RETURN

*--
PROCESS:
*--

    FN.CUS = "F.CUSTOMER"
    F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    R.CUS = ''
    ERR.CUS = ''
    CALL F.READ(FN.CUS,CUS.VAL,R.CUS,F.CUS,ERR.CUS)
    IF R.CUS THEN
        LOCAL.CUS.VAL = ''
        LOCAL.CUS.VAL = R.CUS<EB.CUS.LOCAL.REF>
        CUS.VAL = LOCAL.CUS.VAL<1,CULR.ARABIC.NAME>
*       CUS.VAL = LOCAL.CUS.VAL<1,CULR.TAX.EXEMPTION>
*       CUS.VAL = R.CUS<EB.CUS.SECTOR>
*       CUS.VAL = R.CUS<EB.CUS.TEXT,1>
    END
RETURN

END
