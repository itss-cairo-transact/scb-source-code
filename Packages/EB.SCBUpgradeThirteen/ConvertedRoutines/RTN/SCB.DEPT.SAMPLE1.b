* @ValidationCode : Mjo5NTc1MDc4NTU6Q3AxMjUyOjE2NDE4MDg4NTg5MDI6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:00:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>11422</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

SUBROUTINE SCB.DEPT.SAMPLE1

******************************************************************
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.PARMS
*----------------------------------------------------------
    COMP = ID.COMPANY
    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE          ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL        ;* Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION      ;* Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

    IF ID.NEW[1,2] NE "CL" THEN
        E = '��� ����� ���� �� ��� F3' ; CALL ERR  ; MESSAGE = 'REPEAT '
    END

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

*_________________________________________________________________________________________
    E = ''
    V$ERROR = ''
    HEADER = ''
    HEADER.LINE = 0
    HEADER.LINE = 0


    CALL UPDATE.HEADER( HEADER)
* --- --- ---

*_________________________________________________________________________________________

    IF E THEN CALL ERR ; V$ERROR = 1

RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.

RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL

    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************

CHECK.DELETE:


RETURN

*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************
DELIVERY.PREVIEW:

RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:


RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
        CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

RETURN

*************************************************************************
INITIALISE:
RETURN
*************************************************************************
DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = ""          ; MAT N = ""           ;  MAT T = ""
    MAT CHECKFILE = ""  ; MAT CONCATFILE = ""
    ID.CHECKFILE  = ""
    ID.CONCATFILE = ""

    ID.F  = "CATEGORY"; ID.N  = "16"; ID.T  = "A"
    Z = 0

    Z+=1 ; F(Z) = "XX.LL.DESCRIPTION"   ;  N(Z) = "100" ; T(Z) = "TEXT"
    Z+=1 ; F(Z) = "TF.DATE" ;  N(Z) = "10" ; T(Z) = "D"
********************* FT ***********
** ��� �����
    Z+=1 ; F(Z)  = "DEPT.NO.HWALA" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 410 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
** ��� �������
    Z+=1 ; F(Z)  = "FT.TYPE"  ; N(Z) = "60"
    T(Z) = ""    ; T(Z)<2> = "�����-��� ���-����� ��� ����� ����� -����� ��� ����� - ����� ��� �����"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.HWALA "   ; N(Z) = "10" ; T(Z) = "ANY"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.HWALA" ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.HWALA" ; N(Z) = "100" ; T(Z) = "ANY"
**���� �������
    Z+=1 ; F(Z) = "AMT.HWALA"    ; N(Z) = "20"  ; T(Z) = "AMT"
**����
    Z+=1 ; F(Z) = "SUP.HWALA"    ; N(Z) = "60"  ; T(Z) = "ANY"
**���� ���
    Z+=1 ; F(Z) = "ACC.NAMES2.HWALA" ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 427 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
**����
    Z+=1 ; F(Z) = "CUS.HWALA" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 431 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**����
    Z+=1 ; F(Z) = "XX.PURPOSE.HWALA"    ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.HWALA"      ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.HWALA"  ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.HWALA"   ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.HWALA"      ; N(Z) = "20"  ; T(Z) = ""
    T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
** ������ �������
    Z+=1 ; F(Z) = "XX.NOTES.HWALA.MAR"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.HWALA" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.HWALA"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.HWALA.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.HWALA.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH2.NAME.HWALA.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.HWALA.MANG" ; N(Z) = "100" ; T(Z) = "ANY"
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.HWALA"    ; N(Z) = "20"      ; T(Z) = ""       ; T(Z)<2> = "���_�����_�����"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.HWALA"    ; N(Z) = "8"   ; T(Z) = "A"
    T(Z)<2> = ""
*Line [ 463 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
*************************LG11 *******************************
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.LG11" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 468 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
** ��� ������
    Z+=1 ; F(Z)  = "LG11.TYPE"          ; N(Z) = "35"  ; T(Z) = ""       ; T(Z)<2> = "�������_�����_���� ����_���� ��� ����"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.LG11 "     ; N(Z) = "10"  ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.LG11"         ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.LG11"         ; N(Z) = "100" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.LG11"     ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 481 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
**������
    Z+=1 ; F(Z) = "AMT.LG11"          ; N(Z) = "10" ; T(Z) = "AMT"
**���� ���
    Z+=1 ; F(Z) = "END.DATE.LG11"     ; N(Z) = "10" ; T(Z) = "D"
**�����
    Z+=1 ; F(Z) = "FOR.BENFITS.LG11"  ; N(Z) = "100" ; T(Z) = "A"
**����
    Z+=1 ; F(Z) = "XX.PURPOSE.LG11"   ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.LG11"     ; N(Z) = "100" ; T(Z) = "ANY"
**��� ������ ������
    Z+=1 ; F(Z) = "OLD.LG11.NO"         ; N(Z) = "20" ; T(Z) = "ANY"
**��� ������ ������
    Z+=1 ; F(Z) = "NEW.LG11.NO"         ; N(Z) = "20" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.LG11"   ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.LG11"    ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.LG11"       ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.LG11"            ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 505 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.LG11.MAR"   ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.LG11"  ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.LG11"   ; N(Z) = "100" ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.LG11"      ; N(Z) = "8"   ; T(Z) = "A" ;  T(Z)<2> = ""
*Line [ 515 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.LG11"      ; N(Z) = "20"
    T(Z) = ""    ; T(Z)<2> = "���_�����_�����"
******************************* LG 22 ********************************
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.LG22" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 523 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
** ��� ������
    Z+=1 ; F(Z)  = "LG22.TYPE"        ; N(Z) = "5"    ; T(Z) = ""   ; T(Z)<2> = "�������-�����-���� �����"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.LG22 "   ; N(Z) = "10"   ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.LG22"       ; N(Z) = "100" ; T(Z) = "A"
**�����
    Z+=1 ; F(Z) = "FOR.BENFITS.LG22"  ; N(Z) = "100" ; T(Z) = "A"
**���� ���
    Z+=1 ; F(Z) = "END.DATE.LG22"     ; N(Z) = "15"  ; T(Z) = "D"
**���� ������
    Z+=1 ; F(Z) = "AMT.LG22"          ; N(Z) = "10"  ; T(Z) = "AMT"
**��� ������ ������
    Z+=1 ; F(Z) = "OLD.LG22.NO"       ; N(Z) = "20"  ; T(Z) = "ANY"
**��� ������ ������
    Z+=1 ; F(Z) = "NEW.LG22.NO"       ; N(Z) = "20"  ; T(Z) = "ANY"
**��� ���� ����
    Z+=1 ; F(Z) = "NO.EXTENTION.LG22" ; N(Z) = "10" ; T(Z) = "ANY"
**���� ��
    Z+=1 ; F(Z) = "ISSUE.DATE.LG22"   ; N(Z) = "15" ; T(Z) = "D"
**���� �������
    Z+=1 ; F(Z) = "AMT.OMOLA.LG22"    ; N(Z) = "20"
    T(Z) = "AMT"
**���� �������� ��� ���
    Z+=1 ; F(Z) = "EXT.DATE.LG22"     ; N(Z) = "15" ; T(Z) = "D"
**����
    Z+=1 ; F(Z) = "XX.PURPOSE.LG22"   ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.LG22"     ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.LG22" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.LG22"  ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.LG22"     ; N(Z) = "20"  ; T(Z) = "" ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.LG22" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 563 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.LG22.MAR"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.LG22" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.LG22"  ; N(Z) = "100" ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.LG22"     ; N(Z) = "8"   ; T(Z) = "A" ; T(Z)<2> = ""
*Line [ 573 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.LG22"     ; N(Z) = "20"  ; T(Z) = ""
    T(Z)<2> = "���_�����_�����"
****************************** TT *****************
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.TEL"      ;  N(Z) = "10"
    T(Z) = "ANY"
*Line [ 582 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.TEL "   ; N(Z) = "10" ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.TEL"       ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.TEL"       ; N(Z) = "100" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.TEL"   ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 593 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
**���� ����� TELLER
    Z+=1 ; F(Z) = "AMT.TEL"          ; N(Z) = "100" ; T(Z) = "AMT"
**����
    Z+=1 ; F(Z) = "XX.PURPOSE.TEL"   ; N(Z) = "100" ; T(Z) = "ANY"
**����
    Z+=1 ; F(Z) = "SUP.TEL"          ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TEL"     ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.TEL" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.TEL"  ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.TEL"     ; N(Z) = "100"  ; T(Z) = ""  ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.TEL" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 611 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TEL.MAR"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.TEL" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.TEL" ; N(Z) = "100" ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.TEL"    ; N(Z) = "8"      ; T(Z) = "A" ; T(Z)<2> = ""
*Line [ 621 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.TEL"    ; N(Z) = "20"
    T(Z) = ""    ; T(Z)<2> = "���_�����_�����"
**********************************TF1***************************
** ��� �����
    Z+=1 ; F(Z)  = "DEPT.NO.TF1"      ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 629 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.TF1 "   ; N(Z) = "10" ; T(Z) = "ANY"
* CHECKFILE(Z)  = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ��� ��������
    Z+=1 ; F(Z) = "TF1.TYPE"         ; N(Z) = "20"  ; T(Z) = ""
    T(Z)<2> = "�����_������� ������"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.TF1"     ; N(Z) = "10"  ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.TF1"     ; N(Z) = "10" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.TF1" ; N(Z) = "20" ; T(Z) = "ANY"
*Line [ 643 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
*����
    Z+=1 ; F(Z) = "AMT.TF1"        ; N(Z) = "10" ; T(Z) = "AMT"
**��� ��������
    Z+=1 ; F(Z) = "OLD.NO.TF1"     ; N(Z) = "20" ; T(Z) = "AMT"
**���� ��� ��������
    Z+=1 ; F(Z) = "PERC.TF1"       ; N(Z) = "10" ; T(Z) = "A"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TF1" ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.TF1" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.TF1" ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.TF1"  ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.TF1" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 661 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**��� ��������
    Z+=1 ; F(Z) = "OLD.NO.TF1.MAR"  ; N(Z) = "20" ; T(Z) = "AMT"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.TF1" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.TF1" ; N(Z) = "100"  ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.TF1"      ; N(Z) = "8"  ; T(Z) = "A"
    T(Z)<2> = ""
*Line [ 672 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.TF1"      ; N(Z) = "20" ; T(Z) = ""
    T(Z)<2> = "���_�����_�����"
********************************* TF2 ********************************
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.TF2" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 680 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
** ��� ������
    Z+=1 ; F(Z)  = "TF2.TYPE"          ; N(Z) = "70"      ; T(Z) = ""       ; T(Z)<2> = "��� �������� �������_2����� �������� �������_3������� �����_4������� �� ����� �������� �����_5���� �������� ���_6���� �����_7����� ���� �����_8������� ������� �����_9��� ������ �������_10����_11�������� �����"

    Z+=1 ; F(Z) = "BRANCH.NO.TF2 "   ; N(Z) = "10" ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.TF2" ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.TF2" ; N(Z) = "100" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.TF2" ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 693 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
*����
    Z+=1 ; F(Z) = "AMT.TF2"      ; N(Z) = "10" ; T(Z) = "AMT"
**������� �������
    Z+=1 ; F(Z) = "AMT.LOC.TF2"  ; N(Z) = "10" ; T(Z) = "AMT"
**��������
    Z+=1 ; F(Z) = "FOR.EXPORT.TF2" ; N(Z) = "10" ; T(Z) = "AMT"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TF2" ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.TF2" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.TF2" ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.TF2"  ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.TF2" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 711 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TF2.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.TF2" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.TF2" ; N(Z) = "100" ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.TF2"       ; N(Z) = "8"
    T(Z) = "A" ; T(Z)<2> = ""
*Line [ 722 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.TF2"       ; N(Z) = "20"      ; T(Z) = ""
; T(Z)<2> = "���_�����_�����"
**************************TF3******************
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.TF3" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 730 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
** ��� ������
    Z+=1 ; F(Z)  = "TF3.TYPE"          ; N(Z) = "100"      ; T(Z) = ""       ; T(Z)<2> = "����� ���� ������_����� ����� ������_����� �����"

**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.TF3 "   ; N(Z) = "10" ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.TF3" ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.TF3" ; N(Z) = "100" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.TF3" ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 744 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
*����
    Z+=1 ; F(Z) = "AMT.TF3" ; N(Z) = "10" ; T(Z) = "ANY"
**�� ��������
    Z+=1 ; F(Z) = "DATE.TF3" ; N(Z) = "15" ; T(Z) = "D"
**��� ��������
    Z+=1 ; F(Z) = "OLD.NO.TF3" ; N(Z) = "20" ; T(Z) = "AMT"
**�� ���
    Z+=1 ; F(Z) = "MONTH.TF3" ; N(Z) = "15" ; T(Z) = "D"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TF3" ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.TF3" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.TF3" ; N(Z) = "20"      ; T(Z) = ""       ; T(Z)<2> = "������_�����_����_����"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.TF3"  ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.TF3" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 764 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TF3.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.TF3" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.TF3" ; N(Z) = "100" ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.TF3"             ; N(Z) = "8"      ; T(Z) = "A" ; T(Z)<2> = ""
*Line [ 774 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.TF3"          ; N(Z) = "20"      ; T(Z) = ""       ; T(Z)<2> = "���_�����_�����"
******************************BR**
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.BR" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 781 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.BR "   ; N(Z) = "10" ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.BR" ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.BR" ; N(Z) = "100" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.BR" ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 792 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
*����
    Z+=1 ; F(Z) = "AMT.BR" ; N(Z) = "10" ; T(Z) = "AMT"
**����
    Z+=1 ; F(Z) = "SUP.BR"  ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.BR" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.BR" ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.BR"  ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.BR" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 806 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.BR" ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.BR.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.BR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.BR" ; N(Z) = "100" ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.BR"             ; N(Z) = "8"      ; T(Z) = "A" ; T(Z)<2> = ""
*Line [ 818 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.BR"          ; N(Z) = "20"      ; T(Z) = ""
    T(Z)<2> = "���_�����_�����"

****************************ACCOUNT***************
** ��� �����
    Z+=1 ; F(Z) = "DEPT.NO.AC" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 827 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.AC "   ; N(Z) = "10" ; T(Z) = "ANY"
*  CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.AC" ; N(Z) = "10" ; T(Z) = "A"
** ����� 2
    Z+=1 ; F(Z) = "NAMES2.AC" ; N(Z) = "10" ; T(Z) = "A"
** ���� ������
    Z+=1 ; F(Z) = "ACC.NAMES2.AC" ; N(Z) = "20" ; T(Z) = "ANY"
*Line [ 838 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
*����
    Z+=1 ; F(Z) = "AMT.AC" ; N(Z) = "10" ; T(Z) = "AMT"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.AC" ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.AC" ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.AC"  ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**����
    Z+=1 ; F(Z) = "CUS.AC" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 850 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.AC" ; N(Z) = "50" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.AC.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.AC" ; N(Z) = "100"    ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.AC"  ; N(Z) = "100"    ; T(Z) = "ANY"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.AC"     ; N(Z) = "8"      ; T(Z) = "A" ; T(Z)<2> = ""
*Line [ 862 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.AC"     ; N(Z) = "20"      ; T(Z) = ""       ; T(Z)<2> = "���_�����_�����"

************** TAWSYA ***********
    Z+=1 ; F(Z) = "DEPT.NO.FIN" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 869 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.FIN "      ; N(Z) = "10" ; T(Z) = "ANY"
* CHECKFILE(Z) = "DEPT.ACCT.OFFICER":FM:EB.DAO.NAME:FM:"L"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.FIN"    ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.FIN"     ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.TWSYA"      ; N(Z) = "20"      ; T(Z) = ""    ; T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TWSYA"      ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.TWSYA.MAR"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.TWSYA" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.TWSYA"  ; N(Z) = "100" ; T(Z) = "ANY"
*********************FLAGS******************
**FLAG HWALA
    Z+=1 ; F(Z) = "FLG.HWALA" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG LG ISSUE
    Z+=1 ; F(Z) = "FLG.LG11" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG LG MAD
    Z+=1 ; F(Z) = "FLG.LG22" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG TELLER
    Z+=1 ; F(Z) = "FLG.TEL" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG LC ISSUE
    Z+=1 ; F(Z) = "FLG.TF2" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG LC TSLEM
    Z+=1 ; F(Z) = "FLG.TF1" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG LC SDAD
    Z+=1 ; F(Z) = "FLG.TF3" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG BR
    Z+=1 ; F(Z) = "FLG.BR" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG AC
    Z+=1 ; F(Z) = "FLG.AC" ; N(Z) = "5" ; T(Z) = "ANY"
**FLAG TWSYA
    Z+=1 ; F(Z) = "FLG.TWSYA" ; N(Z) = "5" ; T(Z) = "ANY"
***********************FLAG FOR AUTHORISE AND INPUTTER

    Z+=1 ; F(Z) = "FLG.INP1"  ; N(Z) = "5" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "FLG.AUTH1" ; N(Z) = "5" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "FLG.INP2"  ; N(Z) = "5" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "FLG.AUTH2" ; N(Z) = "5" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "FLG.INP3"  ; N(Z) = "5" ; T(Z) = "ANY"
    Z+=1 ; F(Z) = "FLG.AUTH3" ; N(Z) = "5" ; T(Z) = "ANY"
** ��� �������
    Z+=1 ; F(Z)= "LG11.OPERATION"             ;  N(Z) = "35"    ;  T(Z)= ""  ; T(Z)<2> = "�����_��_�����_����� ���� ����"
********************* WH ***********
** ��� �����
    Z+=1 ; F(Z)  = "DEPT.NO.WH" ;  N(Z) = "10" ; T(Z) = "ANY"
*Line [ 922 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "DEPT.ACCT.OFFICER":@FM:EB.DAO.NAME:@FM:"L"
**��� �����
    Z+=1 ; F(Z) = "BRANCH.NO.WH "   ; N(Z) = "10" ; T(Z) = "ANY"
** ������ 1
    Z+=1 ; F(Z) = "NAMES1.WH" ; N(Z) = "100" ; T(Z) = "A"
** ������ 2
    Z+=1 ; F(Z) = "NAMES2.WH" ; N(Z) = "100" ; T(Z) = "ANY"
**���� �������
    Z+=1 ; F(Z) = "AMT.WH"    ; N(Z) = "20"  ; T(Z) = "AMT"
**����
    Z+=1 ; F(Z) = "SUP.WH"    ; N(Z) = "60"  ; T(Z) = ""
    T(Z)<2> = "�����_�����"
**���� ���
    Z+=1 ; F(Z) = "ACC.NAMES2.WH" ; N(Z) = "100" ; T(Z) = "ANY"
*Line [ 937 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "ACCOUNT":@FM:AC.ACCOUNT.TITLE.1:@FM:"L"
**����
    Z+=1 ; F(Z) = "CUS.WH" ; N(Z) = "8" ; T(Z) = "ANY"
*Line [ 941 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:EB.CUS.NAME.1:@FM:"L"
**����
    Z+=1 ; F(Z) = "XX.PURPOSE.WH"    ; N(Z) = "100" ; T(Z) = "ANY"
**�������
    Z+=1 ; F(Z) = "XX.NOTES.WH"      ; N(Z) = "100" ; T(Z) = "ANY"
**����� �����
    Z+=1 ; F(Z) = "REQUEST.DATE.WH"  ;  N(Z) = "10" ; T(Z) = "D"
**����� ����
    Z+=1 ; F(Z) = "REPLAY.DATE.WH"   ;  N(Z) = "10" ; T(Z) = "D"
**REQUEST
    Z+=1 ; F(Z)  = "REQ.STA.WH"      ; N(Z) = "20"  ; T(Z) = ""
    T(Z)<2> = "WAIT OF APPROVED_APPROVED_REJECT "
** ������ �������
    Z+=1 ; F(Z) = "XX.NOTES.WH.MAR"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.WH" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.WH"  ; N(Z) = "100" ; T(Z) = "ANY"
** input
    Z+=1 ; F(Z) = "XX.INPUT.NAME.WH.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.WH.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH2.NAME.WH.MAR" ; N(Z) = "100" ; T(Z) = "ANY"
** AUTH
    Z+=1 ; F(Z) = "XX.AUTH.NAME.WH.MANG" ; N(Z) = "100" ; T(Z) = "ANY"
** TYPE
    Z+=1 ; F(Z)  = "TYPE.AMT.WH"    ; N(Z) = "20"      ; T(Z) = ""       ; T(Z)<2> = "���_�����_�����"
** CUR
    Z+=1 ; F(Z)  = "CURRENCY.WH"    ; N(Z) = "8"   ; T(Z) = "A"
    T(Z)<2> = ""
*Line [ 973 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CURRENCY":@FM:3
***************************************************
    Z+=1 ; F(Z)= "RESERVED9"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED8"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED7"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "XX.OVERRIDE"             ;  N(Z) = "50"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    V = Z + 9
RETURN

*************************************************************************

END
