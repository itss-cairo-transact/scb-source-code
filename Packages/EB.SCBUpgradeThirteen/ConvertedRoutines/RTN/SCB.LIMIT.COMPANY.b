* @ValidationCode : MjotMTExMTk3Njk2MzpDcDEyNTI6MTY0ODU0MzY4MTE3NjpNb3VuaXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Mar 2022 10:48:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
SUBROUTINE SCB.LIMIT.COMPANY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LI.LOCAL.REFS

    LI.LOCAL.REFS = ""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LI = 'FBNK.LIMIT' ; F.LI = ''
    CALL OPF(FN.LI,F.LI)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    T.SEL = "SELECT ":FN.LI:" WITH COMPANY.BOOK UNLIKE EG..."

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            COMP.BOOK = ''
            CALL F.READ(FN.LI,KEY.LIST<I>,R.LI,F.LI,E1)
            CUST.NO = FIELD(KEY.LIST<I>,".",1)
*CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.NO,COMP.BOOK)
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - yyyy-MM-dd
            F.ITSS.TBL.NAME = 'FBNK.CUSTOMER'
            FN.F.ITSS.TBL.NAME = ''
            CALL OPF(F.ITSS.TBL.NAME,FN.F.ITSS.TBL.NAME)
            CALL F.READ(F.ITSS.TBL.NAME,CUST.NO,R.ITSS.TBL.NAME,FN.F.ITSS.TBL.NAME,ERROR.TBL.NAME)
            COMP.BOOK=R.ITSS.TBL.NAME<EB.CUS.COMPANY.BOOK>
            R.LI<LI.LOCAL.REF,LILR.COMPANY.BOOK> = COMP.BOOK
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
***  WRITE  R.LI TO F.LI , KEY.LIST<I> ON ERROR
***      PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.LI
***  END
            CALL F.WRITE(FN.LI,KEY.LIST<I>, R.LI)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)
***************
        NEXT I
    END
RETURN
END
