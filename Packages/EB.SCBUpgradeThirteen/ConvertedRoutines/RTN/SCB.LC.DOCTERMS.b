* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjY1Mjc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1701</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

      SUBROUTINE SCB.LC.DOCTERMS

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LC.DOCTERMS

*************************************************************************

      GOSUB DEFINE.PARAMETERS

      IF LEN(V$FUNCTION) GT 1 THEN
         GOTO V$EXIT
      END

      CALL MATRIX.UPDATE

      GOSUB INITIALISE                   ; * Special Initialising

*************************************************************************

* Main Program Loop

      LOOP

         CALL RECORDID.INPUT

      UNTIL (MESSAGE EQ 'RET')

         V$ERROR = ''

         IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION         ; * Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
               CALL FUNCTION.DISPLAY
               V$FUNCTION = ''
            END

         END ELSE

            GOSUB CHECK.ID               ; * Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
               GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD           ; * Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
               GOSUB PROCESS.FIELDS      ; * ) For Input
               GOSUB PROCESS.MESSAGE     ; * ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

         END

MAIN.REPEAT:
      REPEAT

V$EXIT:
      RETURN                             ; * From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

      LOOP
         IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
               CALL FIELD.MULTI.INPUT
            END ELSE
               CALL FIELD.MULTI.DISPLAY
            END
         END ELSE
            IF FILE.TYPE EQ 'I' THEN
               CALL FIELD.INPUT
            END ELSE
               CALL FIELD.DISPLAY
            END
         END

      WHILE NOT(MESSAGE)

         GOSUB CHECK.FIELDS              ; * Special Field Editing

         IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

      REPEAT

      RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

      IF MESSAGE = 'DEFAULT' THEN
         MESSAGE = 'ERROR'               ; * Force the processing back
         IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
         END
      END

      IF MESSAGE = 'PREVIEW' THEN
         MESSAGE = 'ERROR'               ; * Force the processing back
         IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
         END
      END

      IF MESSAGE EQ 'VAL' THEN
         MESSAGE = ''
         BEGIN CASE
            CASE V$FUNCTION EQ 'D'
               GOSUB CHECK.DELETE        ; * Special Deletion checks
            CASE V$FUNCTION EQ 'R'
               GOSUB CHECK.REVERSAL      ; * Special Reversal checks
            CASE OTHERWISE
               GOSUB CROSS.VALIDATION    ; * Special Cross Validation
               IF NOT(V$ERROR) THEN
                  GOSUB OVERRIDES
               END
         END CASE
         IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE      ; * Special Processing before write
         END
         IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
               GOSUB AFTER.UNAU.WRITE    ; * Special Processing after write
            END
         END

      END

      IF MESSAGE EQ 'AUT' THEN
         GOSUB AUTH.CROSS.VALIDATION     ; * Special Cross Validation
         IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE      ; * Special Processing before write
         END

         IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
               GOSUB AFTER.AUTH.WRITE    ; * Special Processing after write
            END
         END

      END

      RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.
  ID.CH = FIELD(ID.NEW,".",1)
  TY.CH = FIELD(ID.NEW,".",2)
   IF COUNT( ID.NEW, '.') # 1 THEN E = 'ID MUST BE LCTYPE [.] YES OR NO'
    ELSE CALL DBR("LC.TYPES":@FM:1,ID.CH,TYP) ; IF ETEXT THEN E = "INVALID LC TYPE"
      ELSE IF ( TY.CH # 'YES' AND TY.CH # 'NO' ) THEN E = "INVALID AID"
       IF E THEN  CALL ERR ; MESSAGE = 'REPEAT'
        IF E THEN V$ERROR = 1

      RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


      RETURN

*************************************************************************

CHECK.FIELDS:

REM > CALL XX.CHECK.FIELDS
      IF E THEN
         T.SEQU = "IFLD"
         CALL ERR
      END

      RETURN

*************************************************************************

CROSS.VALIDATION:

*

      V$ERROR = ''
      ETEXT = ''
      TEXT = ''
*
I = 0
*Line [ 243 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(SCB.LCD.TERM),@VM) UNTIL ETEXT
    LOCATE R.NEW(SCB.LCD.TERM)<1,I> IN R.NEW(SCB.LCD.TERM)<1,1> SETTING JJ THEN
    IF I # JJ THEN
     ETEXT = "DUPLICATE VALUE"
     AF = SCB.LCD.TERM ; AV = I
     CALL STORE.END.ERROR
    END
    END
       J = 0
*Line [ 253 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
       FOR J = 1 TO DCOUNT(R.NEW(SCB.LCD.DOC.CODE)<1,I>,@SM) UNTIL ETEXT
          M = 0
          ETEXT = ""
         LOCATE R.NEW(SCB.LCD.DOC.CODE)<1,I,J> IN R.NEW(SCB.LCD.DOC.CODE)<1,I,1> SETTING M THEN
             IF J # M  THEN
               ETEXT = "DUPLICATE VALUE"
                AF = SCB.LCD.DOC.CODE ; AV = I ; AS = J
              CALL STORE.END.ERROR
          END
          END

      NEXT J
*Line [ 266 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
      FOR Y = 1 TO DCOUNT(R.NEW(SCB.LCD.ADD.CONDS)<1,I>,@SM) UNTIL ETEXT

           LOCATE R.NEW(SCB.LCD.ADD.CONDS)<1,I,Y> IN R.NEW(SCB.LCD.ADD.CONDS)<1,I,1> SETTING NN THEN
              IF Y # NN THEN
                ETEXT = "DUPLICATE VALUE"
                  AF = SCB.LCD.ADD.CONDS ; AV = I ; AS = Y
                  CALL STORE.END.ERROR
              END
           END
     NEXT Y
    NEXT I
    *#################################################################
    KK1 = LEN(R.NEW(SCB.LCD.EXP.PERIOD))
    IF R.NEW(SCB.LCD.EXP.PERIOD)[KK1,1] # "W"  THEN
    IF R.NEW(SCB.LCD.EXP.PERIOD)[KK1,1] # "C"  THEN
       ETEXT = "INVALID VALUE"
       AF = SCB.LCD.EXP.PERIOD ; AV = 1 ; AS = 1
       CALL STORE.END.ERROR
       END
        END
    *#################################################################
    KK2 = LEN(R.NEW(SCB.LCD.LAST.SHIP.TO.ADV))
    IF R.NEW(SCB.LCD.LAST.SHIP.TO.ADV)[KK2,1] # "W" THEN
    IF R.NEW(SCB.LCD.LAST.SHIP.TO.ADV)[KK2,1] # "C" THEN
       ETEXT = "INVALID VALUE"
       AF = SCB.LCD.LAST.SHIP.TO.ADV ; AV = 1 ; AS = 1
       CALL STORE.END.ERROR
         END
        END
     *#################################################################
    KK3 = LEN(R.NEW(SCB.LCD.CLOSE.PERIOD))
    IF R.NEW(SCB.LCD.CLOSE.PERIOD)[KK3,1] # "W" THEN
    IF R.NEW(SCB.LCD.CLOSE.PERIOD)[KK3,1] # "C" THEN
       ETEXT = "INVALID VALUE"
       AF = SCB.LCD.CLOSE.PERIOD ; AV = 1 ; AS = 1
       CALL STORE.END.ERROR
        END
        END
    *#################################################################

REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
      IF END.ERROR THEN
         A = 1
      LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
         T.SEQU = A
         V$ERROR = 1
         MESSAGE = 'ERROR'
      END
      RETURN                             ; * Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
      V$ERROR = ''
      ETEXT = ''
      TEXT = ''
REM > CALL XX.OVERRIDE
*

*
      IF TEXT = "NO" THEN                ; * Said NO to override
         V$ERROR = 1
         MESSAGE = "ERROR"               ; * Back to field input

      END
      RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


      RETURN

*************************************************************************

CHECK.DELETE:


      RETURN

*************************************************************************

CHECK.REVERSAL:


      RETURN

*************************************************************************
DELIVERY.PREVIEW:

      RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

      IF TEXT = "NO" THEN                ; * Said No to override
         CALL TRANSACTION.ABORT          ; * Cancel current transaction
         V$ERROR = 1
         MESSAGE = "ERROR"               ; * Back to field input
         RETURN
      END

*
* Additional updates should be performed here
*
REM > CALL XX...



      RETURN

*************************************************************************

AFTER.UNAU.WRITE:


      RETURN

*************************************************************************

AFTER.AUTH.WRITE:


      RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

      BEGIN CASE
         CASE R.NEW(V-8)[1,3] = "INA"    ; * Record status
REM > CALL XX.AUTHORISATION
         CASE R.NEW(V-8)[1,3] = "RNA"    ; * Record status
REM > CALL XX.REVERSAL

      END CASE

      RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

      IF INDEX('V',V$FUNCTION,1) THEN
         E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
         CALL ERR
         V$FUNCTION = ''
      END

      RETURN

*************************************************************************

INITIALISE:

      RETURN

*************************************************************************

DEFINE.PARAMETERS:* SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS
 MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
      ID.CHECKFILE = ""
       ID.CONCATFILE = ""

    ID.F  = "LC.TYPE.AID"; ID.N  = "8"; ID.T  = "A"
    Z = 0

    Z+=1 ; F(Z)= "XX<TERM" ; N(Z) = "9.1"     ; T(Z)= "A"
*Line [ 452 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "SCB.LC.TERMS":@FM:1:@FM:"L"

    Z+=1 ; F(Z)= "XX-XX.DOC.CODE" ; N(Z) = "10.1"     ; T(Z)= "A"
*Line [ 456 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "LC.ADVICE.TEXT":@FM:1

    Z+=1 ; F(Z)= "XX>XX.ADD.CONDS"   ; N(Z) = "6.1"   ; T(Z)= "A"
*Line [ 460 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "LC.CLAUSES":@FM:1

    Z+=1 ; F(Z)= "EXP.PERIOD" ; N(Z) = "3.1"     ; T(Z)= "A"

    Z+=1 ; F(Z)= "LAST.SHIP.TO.ADV" ; N(Z) = "3.1"     ; T(Z)= "A"

    Z+=1 ; F(Z)= "CLOSE.PERIOD" ; N(Z) = "3.1"     ; T(Z)= "A"

    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9

      RETURN

*************************************************************************

   END
