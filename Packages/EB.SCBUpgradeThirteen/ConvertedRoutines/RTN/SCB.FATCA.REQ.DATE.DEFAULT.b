* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjI2OTc6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SCB.FATCA.REQ.DATE.DEFAULT

* This is attached as validation routine to field REQ.DATE in both individual and entity FCSI versions
* To validate the and default the REQ.DATE
*-----------------------------------------


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO


*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS

    RETURN
*----------------------------------------

INITIALIZE:
*----------

    FORM.REQ.DATE = COMI
    ETEXT = ''

    RETURN

PROCESS:
*-------

    IF FORM.REQ.DATE NE '' AND FORM.REQ.DATE GT TODAY THEN
        AS = FA.FI.REQ.DATE
        ETEXT = 'FA-REQ.DATE.GT.TODAY'
        CALL STORE.END.ERROR
    END

    RETURN
END
