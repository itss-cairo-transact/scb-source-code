* @ValidationCode : MjotMzEzMjA3ODQyOkNwMTI1MjoxNjQxODA4ODY0NDI0OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:01:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
**** CREATE BY NESSMA IN 01/02/2010 ****
*-----------------------------------------------------------------------------
SUBROUTINE SCB.LG.AUDIT
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT    I_LD.LOCAL.REFS
*----------------------------------------------------------
    EOF        = ''
    BB.DATA    = ''
    COMP       = ID.COMPANY

    FN.LD   = "FBNK.LD.LOANS.AND.DEPOSITS"  ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.COMP = "F.COMPANY"                   ; F.VOMP  = ''
    CALL OPF(FN.COMP,F.COMP)
*----------------------------------------------------------
    COMP.ID   = COMP
    TOD.DATE  = TODAY
    TOD.MONTH = TOD.DATE[5,2]
    TOD.YEAR  = TOD.DATE[7,2]

    OPENSEQ "&SAVEDLISTS&" , "LTOG-":TOD.MONTH:TOD.YEAR:"-":COMP.ID:".CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'CANNOT CREATE File IN &SAVEDLISTS&'
        END
    END
*----------------------------------------------------------------
    T.SEL   = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CO.CODE EQ ": COMP.ID :" AND CATEGORY EQ 21096"
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED2,ERR)

    IF SELECTED2 THEN
        FOR NNN = 1 TO SELECTED2
            CALL F.READ( FN.LD,KEY.LIST<NNN>, R.LD, F.LD, Y.LD.ERR)
            PRODUCT.TYPE     = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
            CUSTOMER.ID      = R.LD<LD.CUSTOMER.ID>
            AMOUNT           = R.LD<LD.AMOUNT>
            CURRENCY         = R.LD<LD.CURRENCY>
            MARGIN.PERC      = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.PERC>
            VALUE.DATE       = R.LD<LD.VALUE.DATE>
            ACTUAL.EXP.DATE  = R.LD<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
            MARGIN.AMT       = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>
            OLD.NO           = R.LD<LD.LOCAL.REF><1,LDLR.OLD.NO>

            BB.DATA  = PRODUCT.TYPE:','
            BB.DATA := CUSTOMER.ID:','
            BB.DATA := KEY.LIST<NNN>:','
            BB.DATA := AMOUNT:','
            BB.DATA := CURRENCY:','
            BB.DATA := MARGIN.PERC:','
            BB.DATA := VALUE.DATE:','
            BB.DATA := ACTUAL.EXP.DATE:','
            BB.DATA := MARGIN.AMT:','
            BB.DATA := OLD.NO:','

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE ":COMP.ID
            END
        NEXT NNN
    END

    CLOSESEQ BB
*---------------------
    TEXT = "FILE CREATED IN SAVEDLISTS" ; CALL REM
    TEXT = "LTOG-":TOD.MONTH:TOD.YEAR:"-":COMP.ID:".CSV"  ; CALL REM

RETURN
END
