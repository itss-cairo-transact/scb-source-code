* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMTgxODk6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:41:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2013</Rating>
*-----------------------------------------------------------------------------
** ----- 17.03.2003  BAKRY SCB -----

    SUBROUTINE SCB.CUST.AM.1

* CUSTOMER mask * BB-CLASS-SERIAL
* BB - Branch code * CLASS = PRIVATE,BANK,STAFF,CORPPORATE,THIRD - SERIAL  Customer sequential number / CLASS / BRANCH
* User input for new record must be CCXXXXXXXGGGG other parts of ID will be defaulted

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.SERIAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG.1
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF LEVEL.STATUS THEN RETURN
    IF MESSAGE = 'REPEAT' THEN RETURN
    IF APPLICATION # 'CUSTOMER' THEN RETURN

    IF COMI< 3> THEN RETURN   ;* auto new id not supported
    IF V$FUNCTION = 'I' THEN
        GOSUB INTIAL
        LEN.COMI = LEN( COMI)

        IF LEN.COMI = 0 THEN
            CALL DBR('CUSTOMER$NAU':@FM:1,COMI,CHK.ID)
            IF NOT(CHK.ID) THEN ETEXT = '' ; CALL DBR('CUSTOMER':@FM:1,COMI,CHK.ID)
            IF NOT(CHK.ID) THEN ETEXT = '' ; GOSUB CUST.ID.NEW
            IF CHK.ID THEN GOSUB CUST.EXISTING.ID
        END ELSE GOSUB CUST.EXISTING.ID
    END
    IF E THEN CALL ERR ; MESSAGE = 'REPEAT'

    RETURN

***********************************************************************************************************************

INTIAL:

    ETEXT= ''; LEN.COMI= ''; EXIST= '' ; E ='';E1='' ; MYDUMMY ='' ; FOUND = 0 ; CUST.SECTOR = ''; SER.NO = '0' ; RETRY=''
    FN.ACCT.GEN.CONDITION = 'FBNK.ACCT.GEN.CONDITION' ; F.ACCT.GEN.CONDITION = '' ; CCY.NAME = ''
    FN.SCB.CUST.SERIAL = 'F.SCB.CUST.SERIAL' ; F.SCB.CUST.SERIAL = '' ; R.SCB.CUST.SERIAL = ''

    RETURN
***************************************************************************************************************
CUST.ID.NEW:
    GOSUB GET.CUST.CLASS
    IF E THEN RETURN
    GOSUB GET.AND.ADD.BRANCH
    IF E THEN RETURN
    GOSUB GET.SERIAL.NO
    IF E THEN RETURN
    GOSUB PUT.SCB.CUST.SERIAL.ID
    IF E THEN RETURN
    GOSUB CHECK.UNAUTH.ID
    IF E THEN RETURN
    RETURN
**************************************************************************************************
GET.CUST.CLASS:
    CALL DBR('SCB.VER.IND.SEC.LEG.1':@FM:VISL.CUST.CLASS,PGM.VERSION,CUST.CLASS)
    IF ETEXT THEN E = ETEXT
    RETURN
**************************************************************************************************
GET.AND.ADD.BRANCH:
* -- BRANCH --

***Updated by Nessreen Ahmed 16/7/2019***
*** ID.BRANCH = R.USER< EB.USE.DEPARTMENT.CODE>
    ID.BRANCH.N = ID.COMPANY[2]
    ID.BRANCH   = TRIM(ID.BRANCH.N,"0","L")
***End of Update 16/7/2019***************

    IF LEN( ID.BRANCH) > 2 THEN E = 'BRANCH (&) NOT SUPPORTED' : @FM : ID.BRANCH

    IF E THEN RETURN

* -- complete ID --

    ID.NEW.BASE =  ID.BRANCH : CUST.CLASS
    RETURN

**************************************************************************************************
GET.SERIAL.NO:
    CALL OPF(FN.SCB.CUST.SERIAL,F.SCB.CUST.SERIAL)
    CALL F.READU(FN.SCB.CUST.SERIAL,ID.NEW.BASE, R.SCB.CUST.SERIAL, F.SCB.CUST.SERIAL ,E1, RETRY)

    IF NOT(E1) THEN  SER.NO = R.SCB.CUST.SERIAL<SCB.CU.SER.SERIAL.NO> +1
    ELSE SER.NO = SER.NO + 1

    SER.NO = STR('0', 5- LEN(SER.NO) ):SER.NO
    ID.NEW = ID.NEW.BASE :SER.NO
    COMI = ID.NEW
    RETURN
*************************************************************************************************
PUT.SCB.CUST.SERIAL.ID: 
*---------------------
    R.SCB.CUST.SERIAL<SCB.CU.SER.SERIAL.NO> = SER.NO
    R.SCB.CUST.SERIAL<SCB.CU.SER.ID.NO> = ID.NEW
    R.SCB.CUST.SERIAL<SCB.CU.SER.USER.NAME> = OPERATOR
    CALL F.WRITE(FN.SCB.CUST.SERIAL,ID.NEW.BASE,R.SCB.CUST.SERIAL)
    CALL JOURNAL.UPDATE(ID.NEW.BASE)

    CALL F.RELEASE(FN.SCB.CUST.SERIAL,ID.NEW.BASE,F.SCB.CUST.SERIAL)
    CLOSE F.SCB.CUST.SERIAL
    RETURN

*************************************************************************************************
CHECK.UNAUTH.ID:
* -- verify unauthorised ID lock --

    CALL DBR( 'CUSTOMER$NAU':@FM:1, ID.NEW, MYDUMMY)
    IF ETEXT THEN ETEXT = ''
    ELSE E = 'UNAUTHORISED RECORD (&) WITH SAME ID EXISTS' : @FM : ID.NEW
    RETURN
****************************************************************************************************

CUST.EXISTING.ID:
    CALL DBR( 'CUSTOMER':@FM:1, COMI, MYDUMMY)
   **TEXT = "COMI":COMI  ; CALL REM
    IF ETEXT THEN
        ETEXT = ''
        CALL DBR( 'CUSTOMER$NAU':@FM:1, COMI, MYDUMMY)
*** STOPED BY MOHAMED SABRY 2014/09/04
*        IF ETEXT THEN ETEXT = '' ; E = 'CUSTOMER (&) DOES NOT EXIST' : @FM : COMI

    END
    RETURN

****************************************************************************************************

END
