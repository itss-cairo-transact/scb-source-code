* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMzQzNTY6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-40</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SCB.POST.LIST.FILE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.STMT.HANDOFF
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POST.LIST
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
**************************************************
CALLDB:
*------
    FN.HDF = "FBNK.AC.STMT.HANDOFF" ; F.HDF = '' ; R.HDF = ''
    CALL OPF(FN.HDF,F.HDF)
    FN.PST = "F.SCB.POST.LIST" ; F.PST = '' ; R.PST = ''
    CALL OPF(FN.PST,F.PST)
    FN.CUS = "FBNK.CUSTOMER" ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    K.LIST ="" ; SELECTED ="" ; ER.MSG =""
    RETURN
**************************************************
INITIATE:
*-------
    EXECUTE "CLEAR-FILE F.SCB.POST.LIST"
    TT = TODAY
    TTY = TT[1,4]
    TTM = TT[5,2]
    TTD = TT[7,2]
    TTX = TTY : TTM : '01'
    CALL CDT("",TTX,'-1W')

    RETURN
**************************************************
PROCESS:
*-------
    T.SEL  ="SELECT ":FN.HDF:" WITH TO.DATE EQ ":TTX
    T.SEL :=" AND CUSTOMER NE ''"
    T.SEL :=" BY COMPANY.CODE BY GOV.CODE BY REG.CODE BY CUSTOMER"
    KK1 = 0
    CALL EB.READLIST(T.SEL,K.LIST,'',SELECTED,ER.MSG)
    IF SELECTED THEN
        ACCC = ''
        CUST = ''
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.HDF,K.LIST<I>,R.HDF,F.HDF,ER.MSG1)
            CUST = R.HDF<AC.STH.CUSTOMER>
            AC.COM = R.HDF<AC.STH.COMPANY.CODE>
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,CUS.LCL)
            CUS.GOV = CUS.LCL<1,CULR.GOVERNORATE>
            CUS.REG = CUS.LCL<1,CULR.REGION>
            IF CUST # ACCC THEN
                CALL F.READ(FN.PST,CUST,R.PST,F.PST,ERR.PST)
                R.PST<POST.COMPANY.CODE> = AC.COM
                R.PST<POST.GOV.CODE>     = CUS.GOV
                R.PST<POST.REG.CODE>     = CUS.REG
                R.PST<POST.STMT.DATE>    = TTX
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**   WRITE R.PST TO F.PST , CUST ON ERROR
**     PRINT "CAN NOT WRITE RECORD ":CUST:" TO ":FN.PST
** END
 CALL F.WRITE(FN.PST,CUST, R.PST)
 CALL JOURNAL.UPDATE(CUST)
*************
                CRT @(10,10):CUST
                ACCC = CUST
            END
        NEXT I
    END
    RETURN
**************************************************
END
