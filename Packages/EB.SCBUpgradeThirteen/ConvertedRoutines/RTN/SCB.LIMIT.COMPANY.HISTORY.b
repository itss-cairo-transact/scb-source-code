* @ValidationCode : MjotMTIwOTIxOTg0OkNwMTI1MjoxNjQ4NTQzNjY0Njc1Ok1vdW5pcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Mar 2022 10:47:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
SUBROUTINE SCB.LIMIT.COMPANY.HISTORY
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LI.LOCAL.REFS

    LI.LOCAL.REFS = ""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LI = 'FBNK.LIMIT$HIS' ; F.LI = ''
    CALL OPF(FN.LI,F.LI)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    TD = TODAY[3,6]:"..."
    T.SEL = "SELECT ":FN.LI:" WITH DATE.TIME LIKE ":TD:" AND COMPANY.BOOK UNLIKE EG..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            COMP.BOOK = ''
            CALL F.READ(FN.LI,KEY.LIST<I>,R.LI,F.LI,E1)
            CUST.NO = FIELD(KEY.LIST<I>,".",1)
*CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.NO,COMP.BOOK)
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-21
            F.ITSS.TBL.NAME = 'FBNK.CUSTOMER'
            FN.F.ITSS.TBL.NAME = ''
            CALL OPF(F.ITSS.TBL.NAME,FN.F.ITSS.TBL.NAME)
            CALL F.READ(F.ITSS.TBL.NAME,CUST.NO,R.ITSS.TBL.NAME,FN.F.ITSS.TBL.NAME,ERROR.TBL.NAME)
            COMP.BOOK=R.ITSS.TBL.NAME<EB.CUS.COMPANY.BOOK>
            R.LI<LI.LOCAL.REF,LILR.COMPANY.BOOK> = COMP.BOOK
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**WRITE  R.LI TO F.LI , KEY.LIST<I> ON ERROR
**  PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.LI
**END
            CALL F.WRITE(FN.LI,KEY.LIST<I>,R.LI)
*************
        NEXT I
    END
RETURN
END
