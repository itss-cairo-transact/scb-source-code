* @ValidationCode : MjotMjA3NDQzMzM1OTpDcDEyNTI6MTY0NTQ0MDExNjI2NDpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 21 Feb 2022 12:41:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>793</Rating>
*-----------------------------------------------------------------------------
*** SUBROUTINE SCB.GL.CUR.CREATE.PL2

PROGRAM SCB.GL.CURR.CREATE.PL2

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.GL.CUR
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.GL.CURR
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.GL.CUR.BAL
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY
***-----------------------------------------------------------------
    BAL.EGP          = 0
    BAL.USD          = 0
    BAL.USD.CCY      = 0
    BAL.GBP          = 0
    BAL.GBP.CCY      = 0
    BAL.JPY          = 0
    BAL.JPY.CCY      = 0
    BAL.CHF          = 0
    BAL.CHF.CCY      = 0
    BAL.SAR          = 0
    BAL.SAR.CCY      = 0
    BAL.EUR          = 0
    BAL.EUR.CCY      = 0
    BAL.OTH          = 0
    BAL.OTH1         = 0
    BAL.OTH.CCY      = 0
    KEY.LIST         = ''
    KEY.LIST2        = ''
    KEY.LIST3        = ''
    SELECTED         = ''
    SELECTED2        = ''
    SELECTED3        = ''

    FN.COMP = 'F.COMPANY'     ;  F.COMP = '' ; R.COMP  = '' ; ETEXT = ''
    CALL OPF( FN.COMP,F.COMP)

*    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM' ; F.CUR  = '' ; R.CUR = '' ; ETEXT.EX = ''
    FN.CUR  = 'F.SBD.CURRENCY' ; F.CUR  = '' ; R.CUR = '' ; ETEXT.EX = ''
    CALL OPF(FN.CUR,F.CUR)

    FN.GL = 'F.SCB.GL.CURR' ;  F.GL = '' ; R.GL  = '' ; ETEXT = ''
    CALL OPF( FN.GL,F.GL)

    FN.LIN.BAL = 'FBNK.RE.STAT.LINE.BAL' ;  F.LIN.BAL = '' ; R.LIN.BAL  = '' ; ETEXT = ''
    CALL OPF(FN.LIN.BAL,F.LIN.BAL)

    FN.GL.BAL  = 'F.SCB.GL.CUR.BAL'  ; F.GL.BAL  = '' ; R.GL.BAL = '' ; ETEXT.EX1 = ''
    CALL OPF(FN.GL.BAL,F.GL.BAL)

    FN.CCY  = 'FBNK.CURRENCY' ; F.CCY = '' ; R.CCY= '' ; ETEXT.EX1 = ''
    CALL OPF(FN.CCY,F.CCY)

***----------------------------------------------------------------------
    COMP = ID.COMPANY
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,COMP,TD)

******* T.SEL = "SELECT F.SCB.GL.CURR WITH @ID NE 104 AND @ID NE 206 AND @ID NE 2000"
    T.SEL = "SELECT F.SCB.GL.CURR WITH @ID EQ 102 OR @ID EQ 208 AND @ID NE 2000"
********* T.SEL = "SELECT F.SCB.GL.CURR WITH @ID EQ 85"
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ER.MSG)
***PRINT SELECTED
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.GL,KEY.LIST<I>,R.GL,F.GL,ETEXT)


*Line [ 119 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LIN.NO  = DCOUNT(R.GL<GL.CURR.LINE.NO>,@VM)
        FOR II  = 1 TO  LIN.NO
            T.SEL2 = "SELECT F.COMPANY WITH @ID NE 'EG0010088' BY @ID "
            CALL EB.READLIST(T.SEL2,KEY.LIST2,'',SELECTED2,ER.MSG2)

            FOR X = 1 TO SELECTED2
                COMPO = KEY.LIST2<X>

                T.SEL3 = "SELECT FBNK.CURRENCY "
                CALL EB.READLIST(T.SEL3,KEY.LIST3,'',SELECTED3,ER.MSG3)
                FOR C = 1 TO SELECTED3

                    ID.1   = FIELD(R.GL<GL.CURR.LINE.NO,II>,'.',1)
                    ID.2   = FIELD(R.GL<GL.CURR.LINE.NO,II>,'.',2)
                    ID.3   = KEY.LIST3<C>
                    ID.4   = TD
*TEMP
                    ID.5   = KEY.LIST2<X>
****-------------------------------------
                    IF KEY.LIST3<C> NE 'EGP' THEN
                        CALL F.READ(FN.CCY,KEY.LIST3<C>,R.CCY,F.CCY,E1)
                        CUR.RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
                        IF KEY.LIST3<C> EQ 'JPY' THEN
                            CUR.RATE = ( CUR.RATE / 100 )
                        END
                    END ELSE
                        CUR.RATE = 1
                    END

*                    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
                    CURR = KEY.LIST3<C>
*                    LOCATE CURR IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
*                    CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
**PRINT  CURR
**PRINT "XXX" : CUR.RATE
                    CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ECAA)
                    CUR.RATE = R.CUR<SBD.CURR.MID.RATE>
                    IF CURR EQ 'JPY' THEN
                        CUR.RATE = ( CUR.RATE / 100 )
                    END
****-----------------------------------
                    BAL.ID = ID.1:"-":ID.2:"-":ID.3:"-":ID.4:"*":ID.5

                    CALL F.READ(FN.LIN.BAL,BAL.ID,R.LIN.BAL,F.LIN.BAL,ETEXT4)

** IF NOT(ETEXT4) THEN

                    BEGIN CASE
*CASE KEY.LIST3<C> = 'EGP'
* BAL.EGP  = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.EGP

                        CASE KEY.LIST3<C> = 'USD'
                            BAL.USD       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.USD
                            BAL.USD.CCY   = ( R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE ) + BAL.USD.CCY

                        CASE KEY.LIST3<C> = 'GBP'

                            BAL.GBP       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.GBP
                            BAL.GBP.CCY   = (R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE ) + BAL.GBP.CCY

                        CASE KEY.LIST3<C> = 'JPY'

                            BAL.JPY       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.JPY
                            BAL.JPY.CCY   = (R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE )+ BAL.JPY.CCY

                        CASE KEY.LIST3<C> = 'CHF'

                            BAL.CHF       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.CHF
                            BAL.CHF.CCY   = (R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE )+ BAL.CHF.CCY

                        CASE KEY.LIST3<C> = 'SAR'

                            BAL.SAR       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.SAR
                            BAL.SAR.CCY   = (R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE ) + BAL.SAR.CCY

                        CASE KEY.LIST3<C> = 'EUR'

                            BAL.EUR       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.EUR
                            BAL.EUR.CCY   = (R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE ) + BAL.EUR.CCY

                        CASE OTHERWISE

                            CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,ECAA)
                            CURR = 'USD'
*LOCATE CURR IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
*CUR.RATE1    = R.CUR<RE.BCP.RATE,POS>
                            CUR.RATE1     = R.CUR<SBD.CURR.MID.RATE>
                            BAL.OTH       = R.LIN.BAL<RE.SLB.CLOSING.BAL> + BAL.OTH
                            BAL.OTH.CCY   = (R.LIN.BAL<RE.SLB.CLOSING.BAL> * CUR.RATE)+ BAL.OTH.CCY
****  BAL.OTH       = BAL.OTH.CCY * CUR.RATE1

                    END CASE

***  END
                NEXT C

            NEXT X

        NEXT II
**-------------------------------------------------------------------**
        R.GL.BAL<GL.BAL.DESC>           = R.GL<GL.CURR.DESC>
        R.GL.BAL<GL.BAL.FLAG>           = R.GL<GL.CURR.FLAG>
        R.GL.BAL<GL.BAL.NEWFLAG>        = R.GL<GL.CURR.NEWFLAG>
        R.GL.BAL<GL.BAL.NEWNO>          = R.GL<GL.CURR.NEWNO>
        R.GL.BAL<GL.BAL.NEWMARKZY>      = R.GL<GL.CURR.NEWMARKZY>
**-------------------------------------------------------------------**
***  R.GL.BAL<GL.BAL.EGP>            = BAL.EGP
        R.GL.BAL<GL.BAL.USD>            = BAL.USD
        R.GL.BAL<GL.BAL.USD.CCY>        = BAL.USD.CCY
        R.GL.BAL<GL.BAL.GBP>            = BAL.GBP
        R.GL.BAL<GL.BAL.GBP.CCY>        = BAL.GBP.CCY
        R.GL.BAL<GL.BAL.JPY>            = BAL.JPY
        R.GL.BAL<GL.BAL.JPY.CCY>        = BAL.JPY.CCY
        R.GL.BAL<GL.BAL.CHF>            = BAL.CHF
        R.GL.BAL<GL.BAL.CHF.CCY>        = BAL.CHF.CCY
        R.GL.BAL<GL.BAL.SAR>            = BAL.SAR
        R.GL.BAL<GL.BAL.SAR.CCY>        = BAL.SAR.CCY
        R.GL.BAL<GL.BAL.EUR>            = BAL.EUR
        R.GL.BAL<GL.BAL.EUR.CCY>        = BAL.EUR.CCY
        R.GL.BAL<GL.BAL.OTHER>          = BAL.OTH
        R.GL.BAL<GL.BAL.OTHER.CCY>      = BAL.OTH.CCY

**-------------------------------------------------------------------**

        GL.CUR.ID = KEY.LIST<I> :"-":TD
*TEMP
**  GL.CUR.ID = KEY.LIST<I> :"-":"20100502"
**WRITE R.GL.BAL  TO F.GL.BAL , GL.CUR.ID ON ERROR
**    TEXT  = "CAN NOT WRITE RECORD ":GL.CUR.ID :" TO ":FN.GL.BAL ; CALL REM
**END
        CALL F.WRITE(FN.GL.BAL,GL.CUR.ID,R.GL.BAL)
        CALL JOURNAL.UPDATE(GL.CUR.ID)
**--------------------------------------------------------------------**

        BAL.EGP          = 0
        BAL.USD          = 0
        BAL.USD.CCY      = 0
        BAL.GBP          = 0
        BAL.GBP.CCY      = 0
        BAL.JPY          = 0
        BAL.JPY.CCY      = 0
        BAL.CHF          = 0
        BAL.CHF.CCY      = 0
        BAL.SAR          = 0
        BAL.SAR.CCY      = 0
        BAL.EUR          = 0
        BAL.EUR.CCY      = 0
        BAL.OTH          = 0
        BAL.OTH1         = 0
        BAL.OTH.CCY      = 0

**--------------------------------------------------------------------**
***PRINT I
    NEXT I

RETURN

END
