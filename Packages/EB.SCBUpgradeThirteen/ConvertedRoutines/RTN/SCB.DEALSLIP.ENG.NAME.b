* @ValidationCode : MjoxNzQxNTA2Mjk1OkNwMTI1MjoxNjQxODA4ODU4NTg3OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:00:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
SUBROUTINE SCB.DEALSLIP.ENG.NAME(CUS.VAL)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.CUSTOMER

    IF CUS.VAL THEN
        GOSUB PROCESS
    END
RETURN
*----------------------------------------------
PROCESS:
*-------
    FN.CUS = "FBNK.CUSTOMER" ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    R.CUS   = '' ;  ERR.CUS = ''

    CALL F.READ(FN.CUS,CUS.VAL,R.CUS,F.CUS,ERR.CUS)

    IF R.CUS THEN
        CUS.VAL = R.CUS<EB.CUS.SHORT.NAME>
    END
RETURN
END
