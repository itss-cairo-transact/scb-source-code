* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjU3OTQ6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1446</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

      SUBROUTINE SCB.INW.950.FILE

* WRITTEN BY P.KARAKITSOS - 10/03/03
* SO AS TO STORE INCOMING MT950 MESSAGES

******************************************************************

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE


*************************************************************************

      GOSUB DEFINE.PARAMETERS

      IF LEN(V$FUNCTION) GT 1 THEN
         GOTO V$EXIT
      END

      CALL MATRIX.UPDATE

      GOSUB INITIALISE                   ; * Special Initialising

*************************************************************************

* Main Program Loop

      LOOP

         CALL RECORDID.INPUT

      UNTIL (MESSAGE EQ 'RET')

         V$ERROR = ''

         IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION         ; * Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
               CALL FUNCTION.DISPLAY
               V$FUNCTION = ''
            END

         END ELSE

            GOSUB CHECK.ID               ; * Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
               GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD           ; * Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
               GOSUB PROCESS.FIELDS      ; * ) For Input
               GOSUB PROCESS.MESSAGE     ; * ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

         END

MAIN.REPEAT:
      REPEAT

V$EXIT:
      RETURN                             ; * From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

      LOOP
         IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
               CALL FIELD.MULTI.INPUT
            END ELSE
               CALL FIELD.MULTI.DISPLAY
            END
         END ELSE
            IF FILE.TYPE EQ 'I' THEN
               CALL FIELD.INPUT
            END ELSE
               CALL FIELD.DISPLAY
            END
         END

      WHILE NOT(MESSAGE)

         GOSUB CHECK.FIELDS              ; * Special Field Editing

         IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

      REPEAT

      RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

      IF MESSAGE = 'DEFAULT' THEN
         MESSAGE = 'ERROR'               ; * Force the processing back
         IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
         END
      END

      IF MESSAGE = 'PREVIEW' THEN
         MESSAGE = 'ERROR'               ; * Force the processing back
         IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
         END
      END

      IF MESSAGE EQ 'VAL' THEN
         MESSAGE = ''
         BEGIN CASE
            CASE V$FUNCTION EQ 'D'
               GOSUB CHECK.DELETE        ; * Special Deletion checks
            CASE V$FUNCTION EQ 'R'
               GOSUB CHECK.REVERSAL      ; * Special Reversal checks
            CASE OTHERWISE
               GOSUB CROSS.VALIDATION    ; * Special Cross Validation
               IF NOT(V$ERROR) THEN
                  GOSUB OVERRIDES
               END
         END CASE
         IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE      ; * Special Processing before write
         END
         IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
               GOSUB AFTER.UNAU.WRITE    ; * Special Processing after write
            END
         END

      END

      IF MESSAGE EQ 'AUT' THEN
         GOSUB AUTH.CROSS.VALIDATION     ; * Special Cross Validation
         IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE      ; * Special Processing before write
         END

         IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
               GOSUB AFTER.AUTH.WRITE    ; * Special Processing after write
            END
         END

      END

      RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

      IF E THEN V$ERROR = 1

      RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


      RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS
      IF E THEN
         T.SEQU = "IFLD"
         CALL ERR
      END

      RETURN

*************************************************************************

CROSS.VALIDATION:

*
      V$ERROR = ''
      ETEXT = ''
      TEXT = ''
*
REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
      IF END.ERROR THEN
         A = 1
      LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
         T.SEQU = A
         V$ERROR = 1
         MESSAGE = 'ERROR'
      END
      RETURN                             ; * Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
      V$ERROR = ''
      ETEXT = ''
      TEXT = ''
REM > CALL XX.OVERRIDE
*

*
      IF TEXT = "NO" THEN                ; * Said NO to override
         V$ERROR = 1
         MESSAGE = "ERROR"               ; * Back to field input

      END
      RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


      RETURN

*************************************************************************

CHECK.DELETE:


      RETURN

*************************************************************************

CHECK.REVERSAL:


      RETURN

*************************************************************************
DELIVERY.PREVIEW:

      RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

      IF TEXT = "NO" THEN                ; * Said No to override
         CALL TRANSACTION.ABORT          ; * Cancel current transaction
         V$ERROR = 1
         MESSAGE = "ERROR"               ; * Back to field input
         RETURN
      END

*
* Additional updates should be performed here
*
REM > CALL XX...



      RETURN

*************************************************************************

AFTER.UNAU.WRITE:


      RETURN

*************************************************************************

AFTER.AUTH.WRITE:


      RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

      BEGIN CASE
         CASE R.NEW(V-8)[1,3] = "INA"    ; * Record status
REM > CALL XX.AUTHORISATION
         CASE R.NEW(V-8)[1,3] = "RNA"    ; * Record status
REM > CALL XX.REVERSAL

      END CASE

      RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

      IF INDEX('V',V$FUNCTION,1) THEN
         E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
         CALL ERR
         V$FUNCTION = ''
      END

      RETURN

*************************************************************************

INITIALISE:

      RETURN

*************************************************************************

DEFINE.PARAMETERS:* SEE 'I_RULES' FOR DESCRIPTIONS *

       MAT F = '' ; MAT N = '' ; MAT T = ''
       MAT CHECKFILE = '' ; MAT CONCATFILE = ''
       ID.CHECKFILE = '' ; ID.CONCATFILE = ''
    
       ID.F = "SW.TXN.REF" ; ID.N = "12" ; ID.T = "A"
*
       Z = 0
*
       Z+= 1 ; F(Z) = "REFERENCE" ; N(Z) = "35" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "ACCOUNT.REF" ; N(Z) = "40" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "NO.OF.STAT.SEND" ; N(Z) = "5" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "DAILY.SEQUENCE" ; N(Z) = "5" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "FIRST.DB.CR" ; N(Z) = "1" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "FIRST.DATE" ; N(Z) = "8" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "FIRST.CCY" ; N(Z) = "3" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "FIRST.BALANCE" ; N(Z) = "20" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "INTERM.DB.CR" ; N(Z) = "1" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "INTERM.DATE" ; N(Z) = "8" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "INTERM.CCY" ; N(Z) = "3" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "INTERM.BALANCE" ; N(Z) = "20" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX<DATE" ; N(Z) = "8" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-ENTRY.DATE" ; N(Z) = "4" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-D.C.RC.RD" ; N(Z) = "2" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-LAST.CCY.CHAR" ; N(Z) = "1" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-AMOUNT" ; N(Z) = "20" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-COMMENTS" ; N(Z) = "35" ; T(Z) = "ANY"
       Z+= 1 ; F(Z) = "XX-TXN.TYPE" ; N(Z) = "4" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-OUR.REF" ; N(Z) = "30" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX-THEIR.REF" ; N(Z) = "30" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX>ADD.DETAILS" ; N(Z) = "35" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "CLOSE.DB.CR" ; N(Z) = "1" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "CLOSE.DATE" ; N(Z) = "8" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "CLOSE.CCY" ; N(Z) = "3" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "CLOSE.BALANCE" ; N(Z) = "20" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "AVAIL.DB.CR" ; N(Z) = "1" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "AVAIL.DATE" ; N(Z) = "8" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "AVAIL.CCY" ; N(Z) = "3" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "AVAIL.BALANCE" ; N(Z) = "20" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "XX.USER.REMARKS" ; N(Z) = "35" ; T(Z) = "ANY"
       Z+= 1 ; F(Z) = "RESERVED.2" ; N(Z) = "35" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
       Z+= 1 ; F(Z) = "RESERVED.3" ; N(Z) = "35" ; T(Z) = "ANY"
       T(Z)<3> = "NOINPUT"
*
V = Z+9
*
REM > CALL XX.FIELD.DEFINITIONS
*
      RETURN
*
*************************************************************************

   END
