* @ValidationCode : MjoxNDczMzAyMzMxOkNwMTI1MjoxNjQ2MDM5NzE5NDA1OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Feb 2022 11:15:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>73</Rating>
*-----------------------------------------------------------------------------
****  PROGRAM  SCB.DRMNT.OFS(Y.CUST.ID)
SUBROUTINE   SCB.DRMNT.OFS(Y.CUST.ID)



*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

RETURN
***********************************************
INITIALISE:
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "CUSTOMER"
**   THIS VER. TO 0 AUTH.  OR MAKE IT  1 AUTH.
    OFS.OPTIONS = "SCB.DRMNT.OFS"
**    OFS.OPTIONS = "SCB.OLD.CAT"
**    OFS.OPTIONS = ""
**

* OFS.USER.INFO = ""
    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""
RETURN
***********************************************
BUILD.RECORD:
    COMMA = ","
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    NUL = "."

    SYS.DATE = TODAY



    CALL F.READ(FN.CU,Y.CUST.ID,R.CU,F.CU,E1)


**************************************************************************
    COMP = R.CU<EB.CUS.CO.CODE>
    COMP = R.CU<EB.CUS.COMPANY.BOOK>
    INP.NO = FMT(COMP[8,2],"R%2")
***MSABRY 2013/09.22
*    INP.CD = "INPUTT":INP.NO
    INP.CD = "OFSITTRNS"

    X.DATE = TODAY
    POST.CODE = 18
    DRMNT.CODE = 1
    OFS.MESSAGE.DATA   = "POSTING.RESTRICT=":POST.CODE
    OFS.MESSAGE.DATA  := ",LOCAL.REF:100=":X.DATE
    OFS.MESSAGE.DATA  := ",LOCAL.REF:101=":DRMNT.CODE
*************************************************************************************************
    OFS.USER.INFO = INP.CD:"//":COMP

    OFS.TRANS.ID  = Y.CUST.ID
    ID.KEY.LIST = Y.CUST.ID
    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA

*    PRINT OFS.REC

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1

*   WRITE OFS.REC ON F.OFS.IN,Y.CUST.ID:ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    WRITE OFS.REC ON F.OFS.IN,'DRMNT-':Y.CUST.ID:ON ERROR  TEXT = " ERROR ";CALL REM ; STOP


RETURN

END
