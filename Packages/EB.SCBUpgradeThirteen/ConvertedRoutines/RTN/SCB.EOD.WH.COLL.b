* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjE3Njk6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>60</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.EOD.WH.COLL
*   PROGRAM SCB.EOD.WH.COLL
*TO UPDATE COLLARTAL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.EXPIRY
*------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "COLLATERAL"
    OFS.OPTIONS = "COLL.WH"
***OFS.USER.INFO = "/"

    COMP     = ID.COMPANY
    COM.CODE = COMP[8,2]

    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    INPP             = "INPUTT01"

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>


    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","

    FN.EXP = "F.SCB.WH.EXPIRY" ; F.EXP = ""
    CALL OPF(FN.EXP, F.EXP)
    T.SEL  = "SELECT ":FN.EXP:" WITH EXPIRY.DATE LE " : TODAY
    T.SEL := " AND DATE.ACTUAL.EXP EQ " : TODAY
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            ACCT.NO= FIELD(KEY.LIST<II>,'.',1)

            CALL DBR('SCB.WH.REGISTER':@FM:SCB.WH.COLLACTRAL.ID,ACCT.NO,COLL.ID)
            COLL.ID=COLL.ID<1,1>

            COMMA = ","

            OFS.MESSAGE.DATA =  "NOTES=":KEY.LIST<II>:COMMA
            OFS.MESSAGE.DATA :=  "COLLATERAL.CODE=":'102'

            OFS.TRANS.ID  = KEY.LIST<II>

            F.PATH = FN.OFS.IN
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COLL.ID:COMMA:OFS.MESSAGE.DATA
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, "WH-":KEY.LIST<II> :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160628 - S
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E

        NEXT II
    END
    RETURN

*-------------------------------------------------------
END
