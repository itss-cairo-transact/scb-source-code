* @ValidationCode : MjotMTY0MTg5NzEzOTpDcDEyNTI6MTY0MTgxMjE4Mzc3NzpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:56:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*----------------------------------NI7OOOOOOOOOO-------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>589</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.INTER.DR2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES

*------------------------------------------------------------------------
    COMP = ID.COMPANY
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCB.INTER.DR2'
    CALL PRINTER.ON(REPORT.ID,'')

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.LPE.DATE)

RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DR='FBNK.STMT.ACCT.DR';F.DR=''
    CALL OPF(FN.DR,F.DR)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')

*YTEXT = "Enter Account No. : "
*CALL TXTINP(YTEXT, 8, 22, "16", "A")
*ID = COMI:"-":DAT
*------------------------------------------------------------------------
    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY NE 6501 AND CATEGORY NE 6502 AND CATEGORY NE 6503 AND CATEGORY NE 6504 AND CATEGORY NE 1000 AND CATEGORY NE 1100 AND CATEGORY NE 1200 AND CATEGORY NE 1221 AND CATEGORY NE 1222 AND CATEGORY NE 1223 AND CATEGORY NE 1225 AND CATEGORY NE 1226 AND CATEGORY NE 1227 AND CATEGORY NE 9090 AND CATEGORY NE 1201 AND CUSTOMER NE '' BY CO.CODE BY CUSTOMER"
****T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY NE 6501 AND CATEGORY NE 6502 AND CATEGORY NE 6503 AND CATEGORY NE 6504 AND CATEGORY NE 1000 AND CATEGORY NE 1100 AND CATEGORY NE 1200 AND CATEGORY NE 1221 AND CATEGORY NE 1222 AND CATEGORY NE 1223 AND CATEGORY NE 1225 AND CATEGORY NE 1226 AND CATEGORY NE 1227 AND CATEGORY NE 9090 AND CATEGORY NE 1201 AND CUSTOMER NE '' AND CO.CODE EQ " : COMP
****T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...20090228 AND TOTAL.INTEREST NE '' AND @ID UNLIKE ...9090..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
****TEXT = 'SELECTED = ':SELECTED ; CALL REM
****TEXT = "KEY.LIST[11,4] = " : KEY.LIST[11,4] ; CALL REM
        FOR I = 1 TO SELECTED
*            XX = KEY.LIST<I>:'-20120430'
            XX = KEY.LIST<I>:'-':WS.LPE.DATE
            CALL F.READ(FN.DR,XX,R.DR,F.DR,E2)
            TOT.INT = R.DR<IC.STMDR.TOTAL.INTEREST>
            DR.ID  = XX
            ACC.NO = FIELD(DR.ID,'-',1)
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
            CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SEC)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF3)
            STAT  = LOCAL.REF3<1,CULR.CREDIT.STAT>

            IF SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND TOT.INT NE '' AND STAT EQ '' AND CAT.ID NE 1408 THEN
* IF ACC.NO[11,4] NE 6501 AND ACC.NO[11,4] NE 6502 THEN
                DAT = FIELD(DR.ID,'-',2)
                INT.DATE   = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
                INT.DATE2  = DAT[5,2]:'/':DAT[1,4]

                CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)

                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>

                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF1)
                CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
                CUST.ADDRESS1= LOCAL.REF1<1,CULR.GOVERNORATE>
                CUST.ADDRESS2= LOCAL.REF1<1,CULR.REGION>

                CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
                CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)

                IF CUST.ADDRESS1 = 98 THEN
                    CUST.ADD2 = ''
                END
                IF CUST.ADDRESS2 = 998 THEN
                    CUST.ADD1 = ''
                END
                IF CUST.ADDRESS1 = 999 THEN
                    CUST.ADD2 = ''
                END
                IF CUST.ADDRESS2 = 999 THEN
                    CUST.ADD1 = ''
                END

                CATEG.ID  = ACC.NO[11,4]
                CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
*************************UPDATED BY RIHAM R15 ***************
*              CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,COMP.BOOK)
                CUS.BR = COMP.BOOK[8,2]
                AC.OFICER = TRIM(CUS.BR, "0" , "L")

                CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
                CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
                DR.AMOUNT = R.DR<IC.STMDR.TOTAL.INTEREST>
                IN.AMOUNT = DR.AMOUNT
                HIGH.BAL=R.DR<IC.STMDR.TOTAL.CHARGE>
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                TOT = DR.AMOUNT + HIGH.BAL
*------------------------------------------------------------------------

                XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX10 = SPACE(132)
                XX1  = SPACE(132)  ; XX4  = SPACE(132)
                XX2  = SPACE(132)  ; XX5  = SPACE(132)
                XX6  = SPACE(132)  ; XX7  = SPACE(132)
                XX8  = SPACE(132)  ; XX9  = SPACE(132)

                XX1<1,1>[3,35]  = CUST.NAME
                XX<1,1>[3,35]   = CUST.NAME1
                XX2<1,1>[3,35]  = CUST.ADDRESS
                XX3<1,1>[3,35]  = CUST.ADD1
                XX4<1,1>[3,35]  = CUST.ADD2
                XX8<1,1>[3,15]  = DR.AMOUNT
                XX1<1,1>[59,15] = ACC.NO
                XX<1,1>[59,15]  = CATEG
                XX2<1,1>[59,15] = CUR
                XX9<1,1>[3,15]  = HIGH.BAL
                XX10<1,1>[3,15] = TOT
*-------------------------------------------------------------------
                YYBRN  = FIELD(BRANCH,'.',2)
                DAT   = TODAY
*** DATY  = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
*                DATY   = "2012/04/30"
                DATY   = WS.LPE.DATE[1,4]:'/':WS.LPE.DATE[5,2]:'/':WS.LPE.DATE[7,2]

                T.DAY  =  DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

                PR.HD  ="'L'":SPACE(1)
                PR.HD :="'L'":DATY:SPACE(20) : YYBRN
                PR.HD :="'L'":" "
                PR.HD :="'L'":" "
                PRINT
                HEADING PR.HD
*------------------------------------------------------------------
                PRINT XX1<1,1>
                PRINT XX<1,1>
* PRINT STR(' ',82)
                PRINT XX2<1,1>
                PRINT XX3<1,1>
                PRINT XX4<1,1>
                PRINT STR(' ',82)
                PRINT STR(' ',82)
                PRINT XX8<1,1>
* PRINT STR(' ',82)
                PRINT XX9<1,1>
                PRINT STR(' ',82)
                PRINT XX10<1,1>
*   PRINT STR('=',82)
            END
* HEADING PR.HD
        NEXT I
    END
*===============================================================
RETURN
END
