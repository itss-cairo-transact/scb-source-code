* @ValidationCode : MjoxMDMwMDIzNDQ3OkNwMTI1MjoxNjQxODA4ODU5OTI4OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:00:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-53</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.EOD.WH.EXP
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.CATEG.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.NUMERIC.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_OFS.SOURCE.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_F.SCB.WH.REGISTER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_F.SCB.WH.ITEMS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_F.SCB.WH.UNITS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_F.SCB.WH.TXN.TYPE.CONDITION
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_F.SCB.WH.TRANS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT           I_F.SCB.WH.EXPIRY
*--------------------------------------------------
    GOSUB INITIAL
RETURN
*--------------------------------------------------
INITIAL:
*-------
    WS.COMP    = ID.COMPANY
    SYS.DATE   = TODAY

    FN.EXP = "F.SCB.WH.EXPIRY" ; F.EXP = ""
    CALL OPF(FN.EXP, F.EXP)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    RATE = 1
    ACC.OFFICER = WS.COMP[8,2]
    IF ACC.OFFICER[1,1] EQ '0' THEN
        ACC.OFFICER = ACC.OFFICER[2,1]
    END
    GOSUB PROCESS
RETURN
*--------------------------------------------------
PROCESS:
*-------
    T.SEL  = "SELECT ":FN.EXP:" WITH EXPIRY.DATE LT ":SYS.DATE
    T.SEL := " AND EXP.FLAG NE 'YES'"
    T.SEL := " AND CO.CODE EQ ":WS.COMP:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            EXP.ID = KEY.LIST<II>
            CALL F.READ(FN.EXP,EXP.ID, R.EXP, F.EXP, ETEXT)
            DEBIT.ACCT  = R.EXP<EXP.DEBIT.ACCT>
            CREDIT.ACCT = R.EXP<EXP.CREDIT.ACCT>
            AMOUNT      = R.EXP<EXP.VALUE.BALANCE>
            EXP.DATE    = R.EXP<EXP.EXPIRY.DATE>
            CUR.CODE    = DEBIT.ACCT[9,2]
            CATEGORY    = DEBIT.ACCT[11,4]
            CALL DBR ('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.CODE,CUR.NAME)
            CURRENCY    = CUR.NAME
            CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
            RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

            IF CUR.NAME NE 'EGP' THEN
                LCY.AMT = AMOUNT * RATE
                CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
                FCY.AMT = AMOUNT
            END ELSE
                LCY.AMT = AMOUNT
                FCY.AMT = ''
            END

            Y.ACCT    = CREDIT.ACCT
            TXN.CODE  = '846'
            GOSUB AC.STMT.ENTRY

            Y.ACCT    = DEBIT.ACCT
            TXN.CODE  = '846'
            LCY.AMT = LCY.AMT * -1

            IF FCY.AMT NE '' THEN
                FCY.AMT = FCY.AMT * -1
            END

            GOSUB AC.STMT.ENTRY
            GOSUB WRITE.EXP
        NEXT II
    END
RETURN
*----------------------------------------------------------
WRITE.EXP:
*---------
    FN.EXP = 'F.SCB.WH.EXPIRY'     ; F.EXP = ''
    CALL OPF( FN.EXP,F.EXP)

*OPEN FN.EXP TO FVAR.EXP ELSE
*    TEXT = "ERROR OPEN FILE"   ; CALL REM
*    RETURN
*END

    FVAR.EXP = ""
    CALL OPF(FN.EXP,FVAR.EXP)

    WH.ID = KEY.LIST<II>
    CALL F.READ(FN.EXP,WH.ID,R.WH,F.EXP,ERR.EXP)
    R.WH<EXP.EXP.FLAG>        = "YES"
    R.WH<EXP.DATE.ACTUAL.EXP> = TODAY

**WRITE R.WH TO FVAR.EXP , WH.ID  ON ERROR
**    STOP 'CAN NOT WRITE RECORD ':WH.ID:' TO FILE ':FN.EXP
**END
    CALL F.WRITE(FN.EXP,WH.ID,R.WH)
RETURN
*----------------------------------------------------------
AC.STMT.ENTRY:
***********************************************************
*  Build base STMT.ENTRY entry fields.                    *
***********************************************************
*
    ENTRY = ""
    MULTI.ENTRIES = ""
*
    ENTRY<AC.STE.ACCOUNT.NUMBER>   = Y.ACCT
    ENTRY<AC.STE.COMPANY.CODE>     = WS.COMP
    ENTRY<AC.STE.TRANSACTION.CODE> = TXN.CODE
    ENTRY<AC.STE.THEIR.REFERENCE>  = "EXP.":EXP.DATE
    ENTRY<AC.STE.TRANS.REFERENCE>  = "EXP.":EXP.DATE
    ENTRY<AC.STE.NARRATIVE>        = ""
    ENTRY<AC.STE.PL.CATEGORY>      = ""
    ENTRY<AC.STE.AMOUNT.LCY>       = LCY.AMT
    ENTRY<AC.STE.ACCOUNT.OFFICER>  = ACC.OFFICER
    ENTRY<AC.STE.PRODUCT.CATEGORY> = CATEGORY
    ENTRY<AC.STE.VALUE.DATE>       = TODAY
    ENTRY<AC.STE.CURRENCY>         = CUR.NAME
    ENTRY<AC.STE.AMOUNT.FCY>       = FCY.AMT
    ENTRY<AC.STE.EXCHANGE.RATE>    = RATE
    ENTRY<AC.STE.POSITION.TYPE>    = 'TR'
    ENTRY<AC.STE.CURRENCY.MARKET>  = '1'
    ENTRY<AC.STE.DEPARTMENT.CODE>  = "1"
    ENTRY<AC.STE.SYSTEM.ID>        = "SYS"
    ENTRY<AC.STE.BOOKING.DATE>     = TODAY
    ENTRY<AC.STE.CRF.TYPE>         = ""
    ENTRY<AC.STE.CRF.TXN.CODE>     = ""
    ENTRY<AC.STE.CRF.MAT.DATE>     = ""
    ENTRY<AC.STE.CHQ.TYPE>         = ""
    ENTRY<AC.STE.CHEQUE.NUMBER>    = ""
    ENTRY<AC.STE.CUSTOMER.ID>      = ""
    ENTRY<AC.STE.OUR.REFERENCE>    = ID.NEW

    MULTI.ENTRIES<-1> = LOWER(ENTRY)
    TYPE = 'SAO'
    V = 21
    CALL EB.ACCOUNTING("SYS",TYPE,MULTI.ENTRIES,"")

RETURN
END
