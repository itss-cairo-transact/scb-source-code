* @ValidationCode : MjotNDQ4OTU0OTY5OkNwMTI1MjoxNjQ2MDM5NzE5MzE5OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Feb 2022 11:15:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
SUBROUTINE SCB.DRMNT.FILES

******************************************************************

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE          ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL        ;* Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION      ;* Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.
    E = ''
    V$ERROR = ''
    IF E THEN V$ERROR = 1

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    COMP = ID.COMPANY

    CUS.ID     = FIELD(ID.NEW,"-",1)
    STATUS.CHK = FIELD(ID.NEW,"-",2)
    DATE.CHK   = FIELD(ID.NEW,"-",3)

    CHK.LN = LEN(DATE.CHK)
    IF CHK.LN NE '8' THEN
        E = "Invalid Date"
        CALL ERR ; MESSAGE = 'REPEAT'
    END

    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
    IF E1 THEN
        E = 'Invalid Customer No.'
        CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        EMP.COMP = R.CU<EB.CUS.COMPANY.BOOK>

        IF COMP NE EMP.COMP THEN
            E = "Invalid Customer Branch"
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
    IF STATUS.CHK NE 'A' AND STATUS.CHK NE 'D' THEN
        E = "Invalid Customer Status - Must be A or D"
        CALL ERR ; MESSAGE = 'REPEAT'
    END
RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS

    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

RETURN

*************************************************************************

CROSS.VALIDATION:

    V$ERROR = ''
    ETEXT = ''
    TEXT = ''

REM > CALL XX.CROSSVAL

    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:

*  Overrides should reside here.

    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE

    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************

CHECK.DELETE:


RETURN

*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************
DELIVERY.PREVIEW:

RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:


RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
        CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

RETURN

*************************************************************************

INITIALISE:

RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = "" ; MAT N = "" ; MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
    ID.CHECKFILE  = "" ; ID.CONCATFILE  = ""

    ID.F  = "" ; ID.N  = "19" ; ID.T  = "A"

*************************************************************************
    Z = 0

    Z+=1 ; F(Z)  = "CUST.NO"               ;  N(Z) = "8"  ; T(Z) = ""
*Line [ 412 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "CUSTOMER":@FM:2:@FM:"L"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "CUST.NAME"             ;  N(Z) = "35" ; T(Z) = "A"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "BRANCH.NO"             ;  N(Z) = "9"  ; T(Z) = "A"
*Line [ 418 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CHECKFILE(Z) = "COMPANY":@FM:1:@FM:"L"
    T(Z)<3>      = 'NOINPUT'

    Z+=1 ; F(Z)  = "SIGN.COUNT.SYS"        ;  N(Z) = "10"   ; T(Z) = ""
    T(Z)<3>      = 'NOINPUT'

    Z+=1 ; F(Z)  = "SIGN.COUNT.FILE"     ;  N(Z) = "10"  ; T(Z) = ""
    Z+=1 ; F(Z)  = "XX.FILE.CONTENT"     ;  N(Z) = "35"  ; T(Z) = "A"
    Z+=1 ; F(Z)  = "XX.CONTENT.MISSING"  ;  N(Z) = "35"  ; T(Z) = "A"

    Z+=1 ; F(Z)  = "NOTES"            ;  N(Z) = "35"  ; T(Z) = "A"

    Z+=1 ; F(Z)  = "STATUS.DATE"      ;  N(Z) = "8"   ; T(Z) = "D"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "SEND.DATE"        ;  N(Z) = "8"   ; T(Z) = "D"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "RECV.DATE"        ;  N(Z) = "8"   ; T(Z) = "D"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "INPUTTER.NO"      ;  N(Z) = "8"   ; T(Z) = "A"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "INPUTTER.NAME"    ;  N(Z) = "11"  ; T(Z) = "A"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "AUTH.NO"          ;  N(Z) = "8"   ; T(Z) = "A"
    T(Z)<3>      = 'NOINPUT'
    Z+=1 ; F(Z)  = "AUTH.NAME"        ;  N(Z) = "11"  ; T(Z) = "A"
    T(Z)<3>      = 'NOINPUT'

    Z+=1 ; F(Z)= "DRMNT.DATE"        ; N(Z) = "35" ;  T(Z)= "D"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED7"         ; N(Z) = "35" ;  T(Z)= "D"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"         ; N(Z) = "35" ;  T(Z)= "D"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"         ; N(Z) = "35" ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"         ; N(Z) = "35" ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"         ; N(Z) = "35" ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"         ; N(Z) = "35" ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"         ; N(Z) = "35" ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9

RETURN
*************************************************************************
END
