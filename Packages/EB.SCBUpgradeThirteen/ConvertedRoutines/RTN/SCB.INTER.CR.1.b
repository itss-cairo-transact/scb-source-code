* @ValidationCode : MjoxMDM0MTI1OTQ4OkNwMTI1MjoxNjQxODEyMTgzMzgyOkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:56:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*****************************************NI7OOOOOOOOOOOO************************
*-----------------------------------------------------------------------------
* <Rating>625</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.INTER.CR.1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.CR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='SCB.INTER.CR.1'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.CR='FBNK.STMT.ACCT.CR';F.CR=''
    CALL OPF(FN.CR,F.CR)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')

    COMP = ID.COMPANY
    TEXT = COMP ; CALL REM
*YTEXT = "Enter Account No. : "
*CALL TXTINP(YTEXT, 8, 22, "16", "A")
*ID = COMI:"-":DAT
*------------------------------------------------------------------------
*********** T.SEL="SELECT FBNK.STMT.ACCT.CR WITH @ID LIKE ...-20100131 AND TOTAL.INTEREST NE '' AND @ID UNLIKE ...9090... "
***********UPDATED BY MAHMOUD 2/3/2010 **********************
***    T.SEL="SELECT FBNK.STMT.ACCT.CR WITH @ID IN ('0110125310100101-20100131' '0110125410100101-20100131') AND TOTAL.INTEREST NE '' "
    T.SEL="SELECT FBNK.STMT.ACCT.CR WITH @ID EQ 0110125310100101-":DAT:" 0110125410100101-":DAT:" AND TOTAL.INTEREST NE '' "
*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        TEXT = 'SELECTED = ':SELECTED ; CALL REM
**** TEXT = "KEY.LIST[11,4] = " : KEY.LIST[11,4] ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CR,KEY.LIST<I>,R.CR,F.CR,E2)
            DR.ID  = KEY.LIST<I>
            ACC.NO = FIELD(DR.ID,'-',1)
            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COM)
            IF AC.COM = COMP THEN
                CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
                CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,SEC)
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF3)
                STAT  = LOCAL.REF3<1,CULR.CREDIT.STAT>
                CUS.NO = ACC.NO[2,7]
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
                CREDIT.STATUS = LOCAL.REF<1,CULR.CREDIT.STAT>
*** IF ACC.NO NE '1310119510100101' AND ACC.NO[11,4] NE 6501 AND ACC.NO[11,4] NE 6502 AND ACC.NO[11,4] NE 6503 AND ACC.NO[11,4] NE 6504 AND ACC.NO[11,4] NE 1000 AND ACC.NO[11,4] NE 1100 AND ACC.NO[11,4] NE 1200 AND ACC.NO[11,4] NE 1002 AND ACC.NO[11,4] NE 1710 ACC.NO[11,4] NE 1711  AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1100 AND SEC NE '' AND STAT EQ '' THEN
                DAT  = FIELD(DR.ID,'-',2)
                INT.DATE   = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
                INT.DATE2  = DAT[5,2]:'/':DAT[1,4]
                CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUST.ID)
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
                CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
                CUST.NAME1   = LOCAL.REF<1,CULR.ARABIC.NAME.2>
                TEXT = CUST.NAME ; CALL REM
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF1)
                CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
                CUST.ADDRESS1= LOCAL.REF1<1,CULR.GOVERNORATE>
                CUST.ADDRESS2= LOCAL.REF1<1,CULR.REGION>
                CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
                CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)

                IF CUST.ADDRESS1 = 98 THEN
                    CUST.ADD1 = ''
                END
                IF CUST.ADDRESS2 = 998 THEN
                    CUST.ADD2 = ''
                END
                IF CUST.ADDRESS1 = 999 THEN
                    CUST.ADD1 = ''
                END
                IF CUST.ADDRESS2 = 999 THEN
                    CUST.ADD2 = ''
                END


                CATEG.ID  = ACC.NO[11,4]
                CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
***********UPDATED BY RIHAM R15 ******************
*                CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH)
                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,COMP.BOOK)
                CUS.BR = COMP.BOOK[8,2]
                AC.OFICER = TRIM(CUS.BR, "0" , "L")
                CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
*                CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
*               CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
**********************************************************************************
* CATEG.ID  = ACC.NO[11,4]
* CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
                CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
*  TEXT = " NNNN " ; CALL REM
                CR.AMOUNT = R.CR<IC.STMCR.TOTAL.INTEREST>
                IN.AMOUNT = CR.AMOUNT
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*------------------------------------------------------------------------

                XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX10 = SPACE(132)
                XX1  = SPACE(132)  ; XX4  = SPACE(132)
                XX2  = SPACE(132)  ; XX5  = SPACE(132)
                XX6  = SPACE(132)  ; XX7  = SPACE(132)
                XX8  = SPACE(132)  ; XX9  = SPACE(132)

                XX1<1,1>[3,35]  = CUST.NAME
                XX<1,1>[3,35]   = CUST.NAME1
                XX2<1,1>[3,35]  = CUST.ADDRESS
                XX3<1,1>[3,35]  = CUST.ADD2
                XX4<1,1>[3,35]  = CUST.ADD1

                XX<1,1>[45,15]  = '������     : '
                XX<1,1>[59,15]  = CR.AMOUNT
****TEXT = "ACC.NO" : ACC.NO ; CALL REM
                XX1<1,1>[45,15] = '��� ������ : '
                XX1<1,1>[59,15] = ACC.NO
                XX2<1,1>[45,15] = ' ��� ������ : '
                XX2<1,1>[59,15] = CATEG
                XX3<1,1>[45,15] = '������     : '
                XX3<1,1>[59,15] = CUR
                XX4<1,1>[45,15] = '����� ���� : '
                XX4<1,1>[59,15] = INT.DATE
                XX8<1,1>[3,35]  = '������ ������� : '
                XX8<1,1>[20,15] = OUT.AMOUNT : CUR
                XX9<1,1>[3,15]  = '������         : '
                XX9<1,1>[20,15] = '���� ������� ������� �������� �� ��� ' : INT.DATE2
****XX10<1,1>[65,15] = '�� ��� ���� ������'

*            XX<1,1>[3,35]  = CUST.NAME
*            XX<1,1>[45,15] = ACC.NO

*            XX1<1,1>[3,35]  = CUST.ADDRESS

*            XX2<1,1>[3,15] = '����� �������� ���� ����� ������� ��  ':INT.DATE:' ':CUR


*            XX3<1,1>[3,15] = '���'
*            XX3<1,1>[25,15] = '�����'
*            XX3<1,1>[45,15] = '������'

*            XX4<1,1>[25,15] = CR.AMOUNT
*            XX4<1,1>[45,15] = '���� ������ ������� ����� ������� ����'
*            XX5<1,1>[45,15] = '������� ������� �������� �� ���  ':INT.DATE2

*            XX6<1,1>[3,15]  = '��������'
                XX6<1,1>[20,15] = CR.AMOUNT
*-------------------------------------------------------------------
                YYBRN  = FIELD(BRANCH,'.',2)
                DATY   = TODAY
                T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
                PR.HD  ="'L'":SPACE(1):"��� ���� ������"
                PR.HD :="'L'":"������� : ":T.DAY:SPACE(20):"����� ������� �������":SPACE(10):"����� :" : YYBRN
                PR.HD :="'L'":"------------------------------------------------------------------------------------"
*            PR.HD :="'L'":"����� : ":YYBRN:SPACE(20):"����� �������"
*            PR.HD :="'L'":"����� ������� �������"
                PR.HD :="'L'":" "
                PR.HD :="'L'":" "
                PRINT
                HEADING PR.HD
*------------------------------------------------------------------
                PRINT XX1<1,1>
                PRINT XX<1,1>
* PRINT STR(' ',82)
                PRINT XX2<1,1>
                PRINT XX3<1,1>
                PRINT XX4<1,1>
                PRINT STR(' ',82)
                PRINT STR(' ',82)
                PRINT XX8<1,1>
                PRINT STR(' ',82)
                PRINT XX9<1,1>
*   PRINT STR('=',82)
                PRINT XX10<1,1>
*   PRINT STR('=',82)
** END
            END
        NEXT I
*****TEXT = "NNNNN"; CALL REM
    END
*===============================================================
RETURN
END
