* @ValidationCode : MjotNDg5MzMxMTQ3OkNwMTI1MjoxNjQxODA4ODYwMjE1OkRFTEw6LTE6LTE6MDoxOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:01:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>119</Rating>
*-----------------------------------------------------------------------------

SUBROUTINE SCB.FATCA.CUT.OFF.DEFAULT

* This is attached as input routine to in both individual and entity FCSI versions
* To default the CUT.OFF.DATE filed
*-----------------------------------------


*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FATCA.PARAMETER

*-----------------------------------------

    GOSUB INITIALIZE

    GOSUB PROCESS

RETURN
*----------------------------------------

INITIALIZE:
*----------

    FN.FATCA.PARAMETER = 'F.FATCA.PARAMETER'
    F.FATCA.PARAMETER = ''

    CALL OPF(FN.FATCA.PARAMETER,F.FATCA.PARAMETER)

    COMPANY.ID = R.COMPANY(EB.COM.FINANCIAL.COM)
    R.FATCA.PARAMETER = ''
    PARAM.EFFECTIVE = ''     ; PARAM.NEW.CLNT.DAYS = ''
    PARAM.HIGH.VAL.DAYS = '' ; Y.REQ.DATE = ''
    OWNER.POS = ''           ; Y.REQ.SM.CNT = ''
    Y.CUT.OFF.DATE = ''      ; FORM.NUMBER = ''
    FORM.NUMBER.CNT = ''     ; POS.CNT = ''
    R.COMPANY.REC = ''

RETURN

PROCESS:
*-------

    CALL F.READ(FN.FATCA.PARAMETER,COMPANY.ID,R.COMPANY.REC,F.FATCA.PARAMETER,COMP.ERR)
    IF NOT(COMP.ERR) THEN
        PARAM.EFFECTIVE = R.COMPANY.REC<FA.FP.EFFECTIVE.DATE>
        PARAM.NEW.CLNT.DAYS = R.COMPANY.REC<FA.FP.NEW.CLNT.DOC.DAYS>
        PARAM.HIGH.VAL.DAYS = R.COMPANY.REC<FA.FP.HIGH.VALUE.DAYS>
    END

    IF R.NEW(FA.FI.CLIENT.TYPE) EQ "HIGH.VALUE.ACCOUNT" THEN
        PARAM.DAYS = PARAM.HIGH.VAL.DAYS
    END ELSE
        PARAM.DAYS = PARAM.NEW.CLNT.DAYS
    END

    FORM.NUMBER = R.NEW(FA.FI.FORM.OWNER)
*Line [ 84 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FORM.NUMBER.CNT = DCOUNT(FORM.NUMBER,@VM)
    IF FORM.NUMBER AND FORM.NUMBER EQ ID.NEW THEN
        LOOP
            REMOVE OWNER FROM FORM.NUMBER SETTING OWNER.POS
            POS.CNT = 1
*Line [ 90 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            Y.REQ.SM.CNT = DCOUNT(R.NEW(FA.FI.REQ.DATE)<1,POS.CNT>,@SM)
            
            FOR REQ.CNT = 1 TO Y.REQ.SM.CNT
                Y.REC.DATE.VALUE = R.NEW(FA.FI.RECV.DATE)<1,POS.CNT,REQ.CNT>
                Y.REQ.DATE.VALUE = R.NEW(FA.FI.REQ.DATE)<1,POS.CNT,REQ.CNT>
                IF Y.REC.DATE.VALUE THEN
                    R.NEW(FA.FI.CUT.OFF.DATE)<1,POS.CNT,REQ.CNT> = ''
                END
                IF Y.REQ.DATE.VALUE EQ '' THEN
                    Y.REQ.DATE.VALUE = TODAY
                    R.NEW(FA.FI.REQ.DATE)<1,POS.CNT,REQ.CNT> = Y.REQ.DATE.VALUE
                    GOSUB CALCULATE.CUT.OFF.DATE
                END ELSE
                    GOSUB CALCULATE.CUT.OFF.DATE
                END
            NEXT REQ.CNT
        UNTIL NOT(OWNER.POS)
        REPEAT
    END

RETURN

CALCULATE.CUT.OFF.DATE:
*----------------------

    IF PARAM.DAYS AND Y.REC.DATE.VALUE EQ '' THEN
        CALL CDT("", Y.REQ.DATE.VALUE, "+":PARAM.DAYS:"C")
        R.NEW(FA.FI.CUT.OFF.DATE)<1,POS.CNT,REQ.CNT> = Y.REQ.DATE.VALUE
    END

RETURN
END
