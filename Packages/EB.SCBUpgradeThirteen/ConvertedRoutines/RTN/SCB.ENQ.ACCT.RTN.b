* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2NDE3MjEzMjE2NzU6REVMTDotMTotMTowOjE6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:42:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
    SUBROUTINE SCB.ENQ.ACCT.RTN(Y.ID.LIST)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ALTERNATE.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INTRF.MAPPING
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AT.ISO.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ATM.PARAMETER

    GOSUB INITIALISE
    GOSUB GET.ACCT

    RETURN
*
*--------------------------------------------------------------------------------------------------------------------------------------------------------
INITIALISE:
*--------------------------------------------------------------------------------------------------------------------------------------------------------

    Y.ID.LIST = ''
    Y.ERR =''
    ISO.RESP =''

    RETURN

*--------------------------------------------------------------------------------------------------------------------------------------------------------
GET.ACCT:
*--------------------------------------------------------------------------------------------------------------------------------------------------------

    SEL.ACCOUNT = 'ACCOUNT'

    FIND SEL.ACCOUNT IN ENQ.SELECTION SETTING Y.AC.POS1,Y.AC.POS ELSE
        Y.AC.POS =''
    END

    IF Y.AC.POS THEN
        FLD.CNT = DCOUNT(ENQ.SELECTION<4,Y.AC.POS>,' ')
        II =1
        LOOP
        WHILE II LE FLD.CNT
            Y.ACCT.NO =FIELD(ENQ.SELECTION<4,Y.AC.POS>,' ',II)
            IF Y.ACCT.NO THEN
                CHANGE '"' TO '' IN Y.ACCT.NO
                FN.ACCOUNT = 'F.ACCOUNT'
                FV.ACCOUNT = ''
                CALL OPF(FN.ACCOUNT,FV.ACCOUNT)   ;*open after changing company
                FN.ALT.ACCOUNT="F.ALTERNATE.ACCOUNT"
                F.ALT.ACCOUNT=""
                CALL OPF(FN.ALT.ACCOUNT,F.ALT.ACCOUNT)
                GOSUB PROCESS
            END
            II+=1
        REPEAT
    END ELSE
        GTSERROR = 'ACCOUNT NUMBER MISSING'
        ENQ.ERROR = 'ACCOUNT NUMBER MISSING'

        RETURN
    END

    RETURN          ;*From initialise

*--------------------------------------------------------------------------------------------------------------------------------------------------------
PROCESS:
*--------------------------------------------------------------------------------------------------------------------------------------------------------

    IF Y.ACCT.NO THEN
        CACHE.OFF =1
        GOSUB READ.ACCT
        CACHE.OFF = 0
        IF R.ACCT THEN
            GOSUB GET.UNIQUE.ID
            Y.ID.LIST<-1> = 'ACCOUNT FOUND'
        END ELSE
            Y.ID.LIST<-1> = 'ACCOUNT RECORD NOT FOUND'
        END
    END

    RETURN          ;*From process





*--------------------------------------------------------------------------------------------------------------------------------------------------------
GET.UNIQUE.ID:
*--------------------------------------------------------------------------------------------------------------------------------------------------------

    CALL ALLOCATE.UNIQUE.TIME(UNIQUE.TIME)
    CHANGE '.' TO '' IN UNIQUE.TIME
    LEN.UNIQUE.TIME = LEN(UNIQUE.TIME) -6
    Y.UNIQUE.ID = UNIQUE.TIME[LEN.UNIQUE.TIME,6]

    RETURN          ;*From GET.UNIQUE.ID

*--------------------------------------------------------------------------------------------------------------------------------------------------------
READ.ACCT:
*--------------------------------------------------------------------------------------------------------------------------------------------------------

    CALL F.READ(FN.ACCOUNT,Y.ACCT.NO,R.ACCT,FV.ACCOUNT,AC.ER)
    IF NOT(R.ACCT) THEN
        CALL F.READ(FN.ALT.ACCOUNT,Y.ACCT.NO,R.AT.ACCT,F.ALT.ACCOUNT,ALT.ERR)
        Y.ACCT.NO=R.AT.ACCT<AAC.GLOBUS.ACCT.NUMBER>
        CALL F.READ(FN.ACCOUNT,Y.ACCT.NO,R.ACCT,FV.ACCOUNT,AC.ER)
    END

    RETURN


END
