* @ValidationCode : MjotMTYxMDc5NzI2OTpDcDEyNTI6MTY0MTgwODg2MTg5NjpERUxMOi0xOi0xOjA6MTpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 10 Jan 2022 12:01:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : DELL
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : true
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
* @ValidationInfo : Copyright Temenos Headquarters SA 1993-2021. All rights reserved.
*Line [ 13 ] Add Package EB.SCBUpgradeThirteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThirteen
*DONE
SUBROUTINE SCB.GUID.ID
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_F.AC.LOCKED.EVENTS
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2022-01-09
    $INSERT I_AT.ISO.COMMON

    Y.57 = AT$INCOMING.ISO.REQ(57)
    IF Y.57 THEN
        Y.57.LEN = LEN(Y.57)
        DE.57 = Y.57[7,Y.57.LEN-6]
        CALL GET.LOC.REF('AC.LOCKED.EVENTS','GUID',LRF.POS)

        R.NEW(AC.LCK.LOCAL.REF)<1,LRF.POS> = DE.57
    END
RETURN
