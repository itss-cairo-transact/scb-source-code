* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>94</Rating>
*-----------------------------------------------------------------------------
*--- 21/08/2002 RANIA KAMAL -------
*A Routine to check that this teller not a Vault or Head Vault

    SUBROUTINE VNC.TT.TELLERCHECK

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

    CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)

    IF V$FUNCTION = 'I' THEN

**   IF INDEX(TT.NO,'98',1) > 1 THEN E = '��� ����� �������' ;CALL ERR ;MESSAGE = 'REPEAT'
**    IF INDEX(TT.NO,'99',1) > 1 THEN E = '��� ����� �������';CALL ERR ;MESSAGE = 'REPEAT'
*E = 'NOT.ALLOWED.TO.ENTER
    END

    IF V$FUNCTION = 'R' THEN

**   IF INDEX(TT.NO,'98',1) < 1 THEN E = '��� ����� �������' ;CALL ERR ;MESSAGE = 'REPEAT'
****NESSREEN AHMED 12/10/2006***************************
* TEXT = 'TT.NO=':TT.NO ; CALL REM
        TELL.1 = R.NEW(TT.TE.TELLER.ID.1)
*TEXT = 'TELL.1=':TELL.1 ; CALL REM
        IF TT.NO # TELL.1 THEN E = '��� �� ���� ���� ������� �� ���� ���� ��������' ; CALL ERR ; MESSAGE = 'REPEAT'
*******************************************************
    END

    RETURN
END
