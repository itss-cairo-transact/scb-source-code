* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
*---14/07/2002 RANIA KAMAL -------

*THE ROUTINE TO CHECK IF THE OPERATION IN TELLER$NAU THEN
*IF THE DEPARTMENT.CODE OF THE INPUTTER NOT EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*DISPLAY ERROR
*IF THE DEPARTMENT.CODE OF THE INPUTTER  EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
* CHECK IF THE TRANSACTION.CODE NOT EQUAL '9' DISPLAY ERROR


SUBROUTINE VNC.TT.CHECK

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

MYCODE = ''
MYTRANS= ''
INPUTTER.CODE=''
DEPT.CODE = ''

IF V$FUNCTION='I' THEN

MYCODE = FIELD(R.NEW(TT.TE.INPUTTER<1, 1>),'_', 2)

CALL DBR('TELLER$NAU':@FM:TT.TE.TELLER.ID.1, ID.NEW ,MYTRANS)
  IF NOT(ETEXT) THEN
   CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, MYCODE, DEPT.CODE)

     IF R.USER<EB.USE.DEPARTMENT.CODE> # DEPT.CODE THEN
      E = 'WRONG.DEPTARTMENT'
       CALL ERR ; MESSAGE = 'REPEAT';
     END ELSE

        IF R.NEW(TT.TE.TRANSACTION.CODE) # 9 THEN
          E='TRANSACTION.CODE.EQ.9'
          CALL ERR ; MESSAGE = 'REPEAT';
        END
     END
  END
END


RETURN
END
