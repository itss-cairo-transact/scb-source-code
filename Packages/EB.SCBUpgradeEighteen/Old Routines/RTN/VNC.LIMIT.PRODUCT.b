* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
************ WAEL ************
*-----------------------------------------------------------------------------
* <Rating>400</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.LIMIT.PRODUCT

* PREVENT USER FROM MODIFYING
* AN EXSISTING RECORD OR CUSTOMER
* THAT IS A BANK
* OPEN THE RECORD FORM THE VERSION
* IT WAS CREATED
* DEFALUT COUNTRY.RISK TO "EG"
* DEFALUT COUNTRY.PERCENT TO "100"


*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

IF V$FUNCTION = 'I' THEN
*----------------------------------------------------
   CALL DBR( 'LIMIT':@FM:LI.LIMIT.CURRENCY,ID.NEW,CURR)
   IF NOT(ETEXT) THEN
       E = 'Must.Be.Opened.From.Modification.             Screen' ; CALL ERR ;MESSAGE = 'REPEAT'
   END ELSE
       R.NEW(LI.COUNTRY.OF.RISK) = 'EG' ; R.NEW(LI.COUNTRY.PERCENT) = '100'
   END
*-------------------------------------------------

IF INDEX(PGM.VERSION,"BK",1) > 1 THEN
   NN = FIELD(ID.NEW,'.',1)
   CALL DBR ( 'CUSTOMER':@FM:EB.CUS.SECTOR,NN,SEC)
   IF NOT(SEC >= 3000 AND SEC <= 3999 ) THEN E = 'Must.Be.Bank' ; CALL ERR ;MESSAGE = 'REPEAT'
END ELSE
   NN = FIELD(ID.NEW,'.',1)
   CALL DBR ( 'CUSTOMER':@FM:EB.CUS.SECTOR,NN,SEC)
   IF (SEC >= 3000 AND SEC <= 3999 ) THEN E = 'Must.Be.Not.Bank' ; CALL ERR ;MESSAGE = 'REPEAT'
END
*------------------------------------------------
   IF INDEX(PGM.VERSION,"GLOBAL",1) > 1 THEN
      WW = FIELD(ID.NEW,'.',2)
      IF INDEX(WW,'0000',1) < 1 THEN E = 'Must.Be.Product.Version' ; CALL ERR ;MESSAGE = 'REPEAT'
   END

   IF INDEX(PGM.VERSION,"PRODUCT",1) > 1 THEN
      WW = FIELD(ID.NEW,'.',2)
      IF INDEX(WW,'0000',1) > 1 THEN E = 'Must.Be.Global.Version' ; CALL ERR ;MESSAGE = 'REPEAT'
   END
*-----------------------------------------------
END

RETURN
END
