* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.USER.CHK.99

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.USER
    $INSERT           I_BR.LOCAL.REFS
*--------------------------------------------
    FLGH  = 0
    CALL DBR('USER$NAU':@FM:EB.USE.RECORD.STATUS,ID.NEW,STAT)
    IF STAT THEN
        E = 'Please Authorise First ' ; CALL ERR ; MESSAGE = 'REPEAT'
    END

    IF V$FUNCTION = 'I' THEN
        IF PGM.VERSION = ',DEPT.CODE' THEN
            IF ID.NEW # 'SCB.2313' THEN
                FLG = 1
            END
            IF ID.NEW # 'SCB.1325' THEN
                FLG = 1
            END
            IF ID.NEW # 'SCB.14621' THEN
                FLG = 1
            END
            IF ID.NEW # 'SCB.14583' THEN
                FLG = 1
            END
        END
        IF FLG EQ 1 THEN
            E = '�� ���� ������� ���� ��������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*----------------------------------------------------
    RETURN
END
