* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
*----- WAEL  21/07/2002----

SUBROUTINE VNC.TT.TELLER.ID

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER


IF V$FUNCTION = 'I' THEN

IF NOT(R.NEW(TT.TE.TELLER.ID.1)) THEN
IF NOT(R.NEW(TT.TE.TELLER.ID.2)) THEN



  CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,OPERATOR,DEPT)
  CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
  R.NEW(TT.TE.NARRATIVE.2) = DEPT.NAME

*==================================================================================
  CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)

X = ''
X = 0:DEPT:99

IF TT.NO # X  THEN E = 'YOU ARE NOT ALLOWED TO ENTER' ; CALL ERR ; MESSAGE = 'REPEAT'



  R.NEW(TT.TE.TRANSACTION.CODE) = 1
  R.NEW(TT.TE.TELLER.ID.1) = TT.NO
  R.NEW(TT.TE.TELLER.ID.2) = 0:DEPT:98


END
END
END

RETURN
END
