* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
* --- BAKRY - SCB --- 31/03/2003
*-----------------------------------------------------------------------------
* <Rating>2894</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE WORDS.ARABIC.NESS(IN.AMOUNT,OUT.AMOUNT,LINE.LENGTH,NO.OF.LINES,ER.MSG)

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DE.WORDS
*-------------------------------------------------
    IF IN.AMOUNT THEN
*#####################################################################################
        FRACTION=' '
        CH.SP = ' '
        OUT.WORD = ' '

        FIRST.THREE = ' ' ; SECOND.THREE = ' ' ; THIRD.THREE = ' ' ; FOURTH.THREE = ' '
        R.WORDS = ' ' ; AMOUNT3 = ' ' ;       AMOUNT2 = ' ' ; AMOUNT1 = ' '
        TAFK = ''
*#####################################################################################
        DIM CH.AMT(12)        ;* Make Dimension Array
        FOR I = 1 TO 12       ;* To Use It For Amount
            CH.AMT(I) = 0
        NEXT I
*#####################################################################################
        SAVE.IN.AMOUNT = IN.AMOUNT
*#####################################################################################
        LANGUAGE='AR'         ;* Read The Record
        F.DE.WORDS='' ; CALL OPF('F.DE.WORDS',F.DE.WORDS)   ;* From DE.WORDS Table
*******************************UPDATED BY RIHAM R15**********************

*        READ R.WORDS FROM F.DE.WORDS,LANGUAGE THEN
        CALL F.READ ('F.DE.WORDS',LANGUAGE,R.WORDS,F.DE.WORDS,ERR)
*#####################################################################################
        IN.AMOUNT = INT(IN.AMOUNT)      ;* Trim The Fraction From Amount
        IN.AMOUNT = ABS(IN.AMOUNT)      ;* Trim Badge Negative From Amount
*#####################################################################################
        LEN.AMT = LEN(IN.AMOUNT)        ;* Filling The
        POS = LEN.AMT         ;* Dimension Array
        FOR I = 1 TO LEN.AMT  ;* By Amount
            IF NUM(IN.AMOUNT[POS,1]) THEN
                CH.AMT(I) = IN.AMOUNT[POS,1]
                POS = POS - 1
            END
        NEXT I
*#####################################################################################
        WAW = R.WORDS<DE.WRD.AND>
        GOSUB BEGIN.WORK
        GOSUB FORMAT.WORD.TO.FIT
 *   END
END
GOSUB END.PROGRAM
RETURN
*************************************************************************************************
BEGIN.WORK:
*------------
*FIRST.THREE
*------------
IF CH.AMT(1) # 0 OR CH.AMT(2) # 0 OR CH.AMT(3) # 0 THEN
    AMT1 = CH.AMT(1) ; AMT2 = CH.AMT(2) ; AMT3 = CH.AMT(3)
    GOSUB CALC.THREE
    AMT1= 0 ; AMT2 = 0 ; AMT3 = 0
    FIRST.THREE = AMOUNT3 : ' ' : AMOUNT2 : ' ' : AMOUNT1
    AMOUNT3 = ' ' ; AMOUNT2 = ' ' ; AMOUNT1 = ' '
    AMT1 = 0 ; AMT2 = 0 ; AMT3 = 0
END
*************************************************************************************************
*SECOND.THREE
*------------
IF CH.AMT(4) # 0 OR CH.AMT(5) # 0 OR CH.AMT(6) # 0 THEN
    AMT1 = CH.AMT(4) ; AMT2 = CH.AMT(5) ; AMT3 = CH.AMT(6)
*-------------------------------------------------*
    IF CH.AMT(4) = '1' AND CH.AMT(5) = '0' AND CH.AMT(6) = '0' THEN SECOND.THREE = R.WORDS<DE.WRD.THOUSAND,1>
    ELSE IF CH.AMT(4) = '2' AND CH.AMT(5) = '0' AND CH.AMT(6) = '0' THEN SECOND.THREE = R.WORDS<DE.WRD.THOUSAND,2>
    ELSE IF CH.AMT(4) > '2' AND CH.AMT(5) = '0' AND CH.AMT(4) # '0' AND CH.AMT(6) = 0 THEN SECOND.THREE = R.WORDS<DE.WRD.UNITS,CH.AMT(4)> :' ': R.WORDS<DE.WRD.THOUSAND,3>
    ELSE IF CH.AMT(5) = '1' AND CH.AMT(4) = '0' AND CH.AMT(6) = '0' THEN SECOND.THREE = R.WORDS<DE.WRD.TENS,CH.AMT(5)>:' ':R.WORDS<DE.WRD.THOUSAND,3>
    ELSE
        GOSUB CALC.THREE
        AMT1= 0 ; AMT2 = 0 ; AMT3 = 0
        SECOND.THREE = AMOUNT3 : ' ' : AMOUNT2 : ' ' : AMOUNT1:' ': R.WORDS<DE.WRD.THOUSAND,1>
        AMOUNT3 = ' ' ; AMOUNT2 = ' ' ; AMOUNT1 = ' '
    END
    IF FIRST.THREE # ' ' AND SECOND.THREE # ' '  THEN SECOND.THREE = SECOND.THREE :' ': WAW
END
*************************************************************************************************
*THIRD.THREE
*------------
IF CH.AMT(7) # 0 OR CH.AMT(8) # 0 OR CH.AMT(9) # 0 THEN
    AMT1 = CH.AMT(7) ; AMT2 = CH.AMT(8) ; AMT3 = CH.AMT(9)
*-------------------------------------------------*
    IF CH.AMT(7) = '1'  AND CH.AMT(8) = '0' AND CH.AMT(9) = 0 THEN THIRD.THREE = R.WORDS<DE.WRD.MILLION,1>
    ELSE IF  CH.AMT(7) > '2' AND CH.AMT(8) = '0' AND CH.AMT(9) = 0 THEN THIRD.THREE = R.WORDS<DE.WRD.UNITS,CH.AMT(7)> : ' ' :R.WORDS<DE.WRD.MILLION,2>
    ELSE IF  CH.AMT(7) = '0' AND CH.AMT(8) = '1' AND CH.AMT(9) = 0 THEN THIRD.THREE = R.WORDS<DE.WRD.TENS,CH.AMT(8)> : ' ' :R.WORDS<DE.WRD.MILLION,2>
    ELSE
        GOSUB CALC.THREE
        AMT1= 0 ; AMT2 = 0 ; AMT3 = 0
        THIRD.THREE = AMOUNT3 : ' ' : AMOUNT2 : ' ' : AMOUNT1:' ':R.WORDS<DE.WRD.MILLION,1>
        AMOUNT3 = ' ' ; AMOUNT2 = ' ' ; AMOUNT1 = ' '
    END
    IF THIRD.THREE # ' ' AND ( FIRST.THREE # ' ' OR SECOND.THREE # ' ' ) THEN THIRD.THREE = THIRD.THREE : ' ':WAW
END

*************************************************************************************************
*FOURTH.THREE
*------------
IF CH.AMT(10) # 0 OR CH.AMT(11) # 0 OR CH.AMT(12) # 0 THEN
    AMT1 = CH.AMT(10) ; AMT2 = CH.AMT(11) ; AMT3 = CH.AMT(12)
*-------------------------------------------------*
    IF CH.AMT(10) = '1'  AND CH.AMT(11) = '0' AND CH.AMT(12) = 0 AND CH.AMT(10) # 0 THEN FOURTH.THREE = R.WORDS<DE.WRD.BILLION,1>
    ELSE
        GOSUB CALC.THREE
        AMT1= 0 ; AMT2 = 0 ; AMT3 = 0
        FOURTH.THREE = AMOUNT3 : ' ' : AMOUNT2 : ' ' : AMOUNT1:' ':R.WORDS<DE.WRD.BILLION,1>
        AMOUNT3 = ' ' ; AMOUNT2 = ' ' ; AMOUNT1 = ' '
    END
    IF FOURTH.THREE  # ' ' AND ( THIRD.THREE # ' ' OR FIRST.THREE # ' ' OR SECOND.THREE # ' ' ) THEN FOURTH.THREE = FOURTH.THREE : ' ':WAW
END
RETURN
*************************************************************************************************
CALC.THREE:
*----------
*--------  UPDATED AT 20090707 BY BAKRY ------------------------------
*     IF AMT1 # '0' AND AMT2 = '0' AND CH.AMT(2) # '1'  THEN AMOUNT1 = R.WORDS<DE.WRD.UNITS,AMT1>
*---------------------------------------------------------------------
IF AMT1 # '0' AND AMT2 = '0'  THEN AMOUNT1 = R.WORDS<DE.WRD.UNITS,AMT1>
*---------- END OF UPDATE 20090707   ----------------------------------
IF AMT1 # '0' AND AMT2 = '1' THEN AMOUNT1 = R.WORDS<DE.WRD.TEENS,AMT1>

IF AMT2 >= '1' AND AMT1 = '0' THEN AMOUNT2 = R.WORDS<DE.WRD.TENS,AMT2>
IF AMT2 # '0' AND AMT2 # '1'  AND AMT1 # '0'  THEN AMOUNT2 = R.WORDS<DE.WRD.UNITS,AMT1>:' ':WAW:R.WORDS<DE.WRD.TENS,AMT2>
IF AMT1 = '0' AND AMT2 = '1'  THEN AMOUNT2 = R.WORDS<DE.WRD.TENS,AMT2>
IF AMT3 NE 0 THEN
    HUNDRED = ''
    FOR I = 1 TO 9
        HUNDRED<1,I> = ''
    NEXT I
    HUNDRED<1,1> = R.WORDS<DE.WRD.HUNDRED,1>
    HUNDRED<1,2> = R.WORDS<DE.WRD.HUNDRED,2>
    HUNDRED<1,3> = '��������'
    HUNDRED<1,4> = '��������'
    HUNDRED<1,5> = '�������'
    HUNDRED<1,6> = '������'
    HUNDRED<1,7> = '�������'
    HUNDRED<1,8> = '��������'
    HUNDRED<1,9> = '�������'

END ELSE
    HUNDRED = ' '
END
AMOUNT3 = HUNDRED<1,AMT3>
*     IF AMT3 = '1' THEN AMOUNT3 = R.WORDS<DE.WRD.HUNDRED,AMT3>
***    IF AMT3 = '2' THEN AMOUNT3 = R.WORDS<DE.WRD.HUNDRED,2>
****     IF AMT3 > '2' THEN AMOUNT3 = R.WORDS<DE.WRD.UNITS,AMT3> : R.WORDS<DE.WRD.HUNDRED,1>
IF AMOUNT3 # ' ' AND ( AMT2 # '0' OR AMT1 # '0' ) THEN
    AMOUNT3 = AMOUNT3 :' ': WAW
END
RETURN
*************************************************************************************************
FORMAT.WORD.TO.FIT:
*------------
IF NOT(LINE.LENGTH) THEN LINE.LENGTH = 65
IN.AMOUNT = SAVE.IN.AMOUNT
IF INDEX(IN.AMOUNT,".",1) AND IN.AMOUNT[2] # "00" THEN FRACTION = WAW:' ':'100/':IN.AMOUNT[2]       ;* The Fraction
IF INDEX(IN.AMOUNT,".",1) THEN TAFK = "��� "
WORDS = TAFK:FOURTH.THREE:' ':THIRD.THREE:' ':SECOND.THREE:' ':FIRST.THREE:' ':FRACTION
WORDS = TRIM(WORDS):SPACE(1)  ;* Trim More Spaces
NO.OF.LINES = 0

*         FOR I = 1 TO LEN (WORDS) STEP LINE.LENGTH
*
*         NO.OF.LINES += 1
*         *OUT.AMOUNT<1,DCOUNT(OUT.AMOUNT<1>, VM) + 1> = TRIM(WORDS[I, LINE.LENGTH])
*         OUT.AMOUNT<1,NO.OF.LINES> = TRIM(WORDS[I, LINE.LENGTH])
*         NEXT I
KK2 = 1 ; KK3 = LINE.LENGTH ; KK1 = ''
LOOP UNTIL KK1 = 'EXIT'
    NO.OF.LINES += 1 ; KK4 = LINE.LENGTH
    KK3 = LINE.LENGTH * NO.OF.LINES - CH.SP
    CH.SP1 = 0
    LOOP UNTIL WORDS[KK3,1] = SPACE(1)
        KK3 -= 1
        CH.SP += 1
        CH.SP1 += 1
    REPEAT
    KK4 = ( KK3 - KK2 ) + 1
    OUT.WORD<1,NO.OF.LINES> = TRIM(WORDS[KK2,KK4])
    KK3 = LINE.LENGTH * NO.OF.LINES - CH.SP

    KK2 = KK3 + 1
    IF NOT(OUT.WORD<1,NO.OF.LINES>) THEN DEL OUT.WORD<1,NO.OF.LINES> ; KK1 = 'EXIT'
REPEAT
OUT.AMOUNT = OUT.WORD
*************************************************************************************************

END.PROGRAM:
RETURN
END
