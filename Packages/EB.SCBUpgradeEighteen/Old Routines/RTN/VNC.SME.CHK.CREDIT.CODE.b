* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SME.CHK.CREDIT.CODE

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT        I_FT.LOCAL.REFS
    $INSERT        I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

*------------------------------------------------
    CUS.NO = ID.NEW
    FN.USR = "F.USER"  ; F.USR =""
    CALL OPF(FN.USR, F.USR)

    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,CUS.DATA)
    CREDIT.CODE = CUS.DATA<1,CULR.CREDIT.CODE>

    IF (CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120) THEN
        USR = R.USER<EB.USE.SIGN.ON.NAME>
        CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,USR,USR.ID)

        CALL F.READ(FN.USR,USR.ID, R.USR, F.USR, E.USR)
        DEPT.CODE = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>

        IF DEPT.CODE NE '9905' THEN
            E = '�� ���� ������� �� ��� ������' ; CALL ERR  ; MESSAGE = 'REPEAT'
        END
    END

*------------------------------------------------
    RETURN
END
