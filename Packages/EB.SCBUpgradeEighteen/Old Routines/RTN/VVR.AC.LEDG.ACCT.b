* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>96</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AC.LEDG.ACCT

*20021126 SHOULD BE BLANK
*MSABRY 2011/5/11 STOP THE ROUTINE AND EMPTY FLD. ( LINK.TO LIMIT ) IN ROUTINE 'VVR.AC.LI.CHK' After refer to the Mr. Bakry

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
* TO CLEAR THE LIMIT REF IN ACCOUNT
* TO CLEAR LINK TO LIMIT IN ACCOUNT
*    IF MESSAGE = 'VAL' THEN
*        COMI = ''
*        ETEXT = ''
*************** BAKRY ********************************
*        CALL DBR('ACCOUNT':@FM:1,ID.NEW,CHEX)
*        IF ETEXT THEN
*            ETEXT = ''
*            IF R.NEW(AC.LIMIT.REF) THEN R.NEW(AC.LIMIT.REF) = ''
*        END
*        IF R.NEW(AC.LINK.TO.LIMIT)THEN R.NEW(AC.LINK.TO.LIMIT) = ''
*************** BAKRY ********************************

*    END
    RETURN
END
