* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AC.MARG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*   IF MESSAGE EQ ' ' THEN

    ETEXT = ""
    IF MESSAGE NE 'VAL' THEN
*   APPL.ACCT  = R.NEW(TF.LC.APPLICANT.ACC)
        IF COMI THEN
            R.NEW(TF.LC.PROVIS.ACC) = COMI
            CU = COMI[1,10]
*        TEXT = "CU "  : CU ; CALL REM
            AC.MARG = CU : "300501"
            R.NEW(TF.LC.CREDIT.PROVIS.ACC)=AC.MARG
*       TEXT = "AC.MARG" : AC.MARG ; CALL REM
            CALL REBUILD.SCREEN ; P = 0
        END
*END
        RETURN
    END
END
