* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
********NESSREEN AHMED 16/7/2009*******
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.VISA.PAY.DATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.DATE

    E = '' ; TDATE = '' ; TYEAR = '' ; YYYY = '' ; MM = '' ; MM1 = '' ; DD.DATE = '' ; S.DAT = ''

    IF V$FUNCTION = 'I'  THEN
        IF LEN(ID.NEW) # 6 THEN
            E = '��� �� �������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            YYYY = ID.NEW[1,4]
            MM = ID.NEW[5,2]
            MM1 = TRIM (MM, '0', 'L')
            TDATE = TODAY
            TYEAR = TDATE[1,4]
            TMON  = TDATE[5,2]

*------------------------------------------
            DD.DAT = TDATE[1,6]
            S.DAT = DD.DAT:26
            CALL CDT('',S.DAT,"-1W")
*------------------------------------------
            IF YYYY # TYEAR THEN
                E = '��� �� �����' ; CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF MM # TMON THEN
                E = '��� �� �����' ; CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF YYYY = TYEAR AND MM = TMON THEN
                R.NEW(VPD.YEAR)  = YYYY
                R.NEW(VPD.MONTH) = MM1
                R.NEW(VPD.SAL.DATE) = S.DAT
            END
        END
    END
    RETURN
END
