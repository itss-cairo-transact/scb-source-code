* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BS.CUS.CHRG

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS.CUR
*-----------------------------------------------
    FN.BS = "F.SCB.BR.SLIPS" ; F.BS = ""
    CALL OPF(FN.BS, F.BS)
    BS.ID = ID.NEW
    IF R.NEW(SCB.BS.CO.CODE) NE '' THEN
        CALL F.READ(FN.BS, BS.ID, R.BS, F.BS, BS.ERROR)
        BR.EXIT = R.BS<SCB.BS.DRAWER>
        NOM     = R.BS<SCB.BS.TOTAL.NO>

    END
    IF BR.EXIT THEN
        IF COMI NE NOM THEN
            ETEXT = "�� ���� �������"
            CALL STORE.END.ERROR
        END

    END
    IF MESSAGE # 'VAL' THEN
*----------------------------
* CALC  CHRG
*----------------------------
        TEXT  = ''
        ETEXT = ''
        HH    = R.NEW(SCB.BS.DRAWER)[1,3]
        IF HH NE '994' THEN
            FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
            CALL OPF(FN.CUS,F.CUS)
            CALL F.READ(FN.CUS,R.NEW(SCB.BS.DRAWER),R.CUS,F.CUS,ER1)
            SEC  = R.CUS<EB.CUS.SECTOR>
            IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                IF COMI THEN
                    CURR      = R.NEW(SCB.BS.CURRENCY)
                    IF R.NEW(SCB.BS.CURRENCY) EQ 'EGP' THEN
                        FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
                        CALL OPF(FN.BR.COM,F.BR.COM)
                        BILL.CUS  = R.NEW(SCB.BS.DRAWER)
                        CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)

                        IF NOT(E11) THEN
                            CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>
                            CHRG.AMTT = R.BR.COM<BR.CUS.CHRGE.AMT>

                            BEGIN CASE
                            CASE CHRG.AMT EQ 'YES'
                                R.NEW(SCB.BS.CHG.TYPE) = ""
                                R.NEW(SCB.BS.CHG.AMT)  = ""

                            CASE CHRG.AMTT NE ''
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����"  OR R.NEW(SCB.BS.OPERATION.TYPE) EQ  "� ���� �����"  THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                    R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMTT * COMI
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                    R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMTT * COMI
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����"  OR  R.NEW(SCB.BS.OPERATION.TYPE)  EQ  "�������� �����" THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "DISCBILPST"
                                    R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMTT * COMI
                                END
                            CASE OTHERWISE
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����"  OR R.NEW(SCB.BS.OPERATION.TYPE) EQ  "� ���� �����"  THEN
                                    R.NEW(SCB.BS.CHG.TYPE) =  "CHQCHPOST"
                                    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                    R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMT<1,1> * COMI
                                END

                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                    R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMT * COMI
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����"  OR  R.NEW(SCB.BS.OPERATION.TYPE)  EQ  "�������� �����" THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "DISCBILPST"
                                    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                    R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMT<1,1> * COMI
                                END
                            END CASE
                        END ELSE
                            IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����"  OR R.NEW(SCB.BS.OPERATION.TYPE) EQ  "� ���� �����"  THEN
                                R.NEW(SCB.BS.CHG.TYPE) =  "CHQCHPOST"
                                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMT<1,1> * COMI
                            END
                            IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
                                R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMT * COMI
                            END
                            IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����"  OR  R.NEW(SCB.BS.OPERATION.TYPE)  EQ  "�������� �����" THEN
                                R.NEW(SCB.BS.CHG.TYPE) = "DISCBILPST"
                                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMT <1,1> * COMI
                            END
                        END
                    END ELSE
                        FN.BR.COM = 'F.SCB.BR.CUS.CUR' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
                        CALL OPF(FN.BR.COM,F.BR.COM)
                        BILL.CUS  = R.NEW(SCB.BS.DRAWER)
                        CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)

                        IF NOT(E11) THEN
                            LOCATE CURR IN R.BR.COM SETTING SET.POS THEN
                                BS.FEES = R.BR.COM<BR.CUS.CUR.KEEP.FEES,SET.POS>
                            END

                            IF BS.FEES EQ 0 THEN
                                R.NEW(SCB.BS.CHG.TYPE) = ""
                                R.NEW(SCB.BS.CHG.AMT)  = ""
                            END
                            IF BS.FEES NE '' THEN
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����"  OR R.NEW(SCB.BS.OPERATION.TYPE) EQ  "� ���� �����"  THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����"  OR  R.NEW(SCB.BS.OPERATION.TYPE)  EQ  "�������� �����" THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "DISCBILPST"
                                END
                                R.NEW(SCB.BS.CHG.AMT)  = BS.FEES * COMI
                            END
                            IF BS.FEES EQ '' THEN
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����"  OR R.NEW(SCB.BS.OPERATION.TYPE) EQ  "� ���� �����"  THEN
                                    R.NEW(SCB.BS.CHG.TYPE) =  "CHQCHPOST"
                                    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                    R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMT<1,1> * COMI
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                    R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMT<1,1> * COMI
                                END
                                IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����"  OR  R.NEW(SCB.BS.OPERATION.TYPE)  EQ  "�������� �����" THEN
                                    R.NEW(SCB.BS.CHG.TYPE) = "DISCBILPST"
                                    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                    R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMT<1,1> * COMI
                                END
                            END
                        END ELSE
                            IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "����� �����"  OR R.NEW(SCB.BS.OPERATION.TYPE) EQ  "� ���� �����"  THEN
                                R.NEW(SCB.BS.CHG.TYPE) =  "CHQCHPOST"
                                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMT<1,1> * COMI
                            END
                            IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "� ����� ���� " THEN
                                R.NEW(SCB.BS.CHG.TYPE) = "CHQCHPOST"
                                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                R.NEW(SCB.BS.CHG.AMT)  = CHRG.AMT<1,1> * COMI
                            END
                            IF R.NEW(SCB.BS.OPERATION.TYPE) EQ "�������� �����"  OR  R.NEW(SCB.BS.OPERATION.TYPE)  EQ  "�������� �����" THEN
                                R.NEW(SCB.BS.CHG.TYPE) = "DISCBILPST"
                                CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,R.NEW(SCB.BS.CHG.TYPE),CHRG.AMT)
                                R.NEW(SCB.BS.CHG.AMT)  =  CHRG.AMT<1,1> * COMI
                            END
                        END
                    END
                END
                CALL REBUILD.SCREEN
            END
        END
    END
*-----------------------------------------------------------
    RETURN
END
