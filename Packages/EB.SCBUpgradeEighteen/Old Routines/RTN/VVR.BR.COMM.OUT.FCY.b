* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* COPIED FROM VVR.SCB.BR.COMM.OUT
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.COMM.OUT.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS.CUR
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*------------------------------------------------------
    IF  R.NEW(EB.BILL.REG.CURRENCY) NE 'EGP' THEN
        CURR   = R.NEW(EB.BILL.REG.CURRENCY)
        COM.ID = "BILLCOLL2"

        FN.FT.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
        CALL OPF(FN.FT.COM,F.FT.COM)
        CALL F.READ(FN.FT.COM,COM.ID,R.FT.COM,F.FT.COM,E1)

        LOCATE "EGP" IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
        PER      = R.FT.COM<FT4.PERCENTAGE,J>
        DFLT.MIN = R.FT.COM<FT4.MINIMUM.AMT,J>
        DFLT.MAX = R.FT.COM<FT4.MAXIMUM.AMT,J>

        FN.CUR = "FBNK.CURRENCY" ; F.CUR = ""
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR, R.CUR, F.CUR ,E11)
        CUR.RATE   = R.CUR<EB.CUR.MID.REVAL.RATE,1>
        EQVLNT.MAX = DFLT.MAX / CUR.RATE
        EQVLNT.MIN = DFLT.MIN / CUR.RATE
        MAX        = EQVLNT.MAX
        MIN        = EQVLNT.MIN

        COMM.AMT = R.NEW(EB.BILL.REG.AMOUNT)*PER/100
        IF COMM.AMT LT MIN THEN
            COMM.AMT = MIN
        END ELSE
            IF COMM.AMT GT MAX THEN
                COMM.AMT = MAX
            END
        END
        CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR:COMM.AMT

        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>
        IF ( SEC NE 1100 AND SEC NE 1200 ) THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR:COMM.AMT
        END ELSE
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
        END

        FN.BR.COM = 'F.SCB.BR.CUS.CUR' ; F.BR.COM = '' ; R.BR.COM = '' ; E11=''
        CALL OPF(FN.BR.COM,F.BR.COM)
        BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
        CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)
        IF NOT(E11) THEN
            COM.AMT   = R.BR.COM<BR.CUS.CUR.COMM.AMT>
            COM.PR    = R.BR.COM<BR.CUS.CUR.COMM.PREC>
            CHRG.AMT  = R.BR.COM<BR.CUS.CUR.CHRG.AMT>

            IF CHRG.AMT EQ 'YES' OR COM.AMT EQ 0 THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                CALL REBUILD.SCREEN
            END ELSE
                FN.FT.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
                CALL OPF(FN.FT.COM,F.FT.COM)
                CALL F.READ(FN.FT.COM,COM.ID,R.FT.COM,F.FT.COM,E1)

                LOCATE CURR IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                PER  = R.FT.COM<FT4.PERCENTAGE,J> * COM.PR
                MIN  = R.FT.COM<FT4.MINIMUM.AMT,J>
                MAX  = R.FT.COM<FT4.MAXIMUM.AMT,J> * COM.PR

                COMM.AMT = (COM.AMT*PER/100)

                IF COMM.AMT LT MIN THEN
                    COMM.AMT = MIN
                END ELSE
                    IF COMM.AMT GT MAX THEN
                        COMM.AMT = MAX
                    END
                END
                CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
            END
            IF COM.AMT GT 0 THEN
                COMM.AMT = COM.AMT
            END
            COMM.AMT = CURR:COMM.AMT
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
            CALL REBUILD.SCREEN
        END ELSE
*----- ABOVE
        END
        CALL REBUILD.SCREEN
    END
*----------------------------------------------------------------------
    RETURN
END
