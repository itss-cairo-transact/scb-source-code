* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.WH.REGISTER

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.REGISTER
*-------------------------------------------
*    IF V$FUNCTION EQ 'A' OR V$FUNCTION EQ 'I' THEN
    COM.CODE = ID.COMPANY
    CO.CODE  = R.NEW(SCB.WH.CO.CODE)

    IF CO.CODE NE '' THEN
        IF CO.CODE NE COM.CODE THEN
            E = '���� �� ��� �����' ; CALL ERR ; MESSAGE ='REPEAT'
            CALL STORE.END.ERROR
        END
    END
*-------------------------------------------
    IF ID.NEW[3,1] = "0" THEN
        R.NEW(SCB.WH.CUSTOMER)      = ID.NEW[4,7]
    END ELSE
        R.NEW(SCB.WH.CUSTOMER)      = ID.NEW[3,8]
    END

    CALL DBR("NUMERIC.CURRENCY":@FM:EB.NCN.CURRENCY.CODE,ID.NEW[11,2],CURR)

    R.NEW(SCB.WH.CURRENCY)        = CURR
    R.NEW(SCB.WH.CATEGORY)        = ID.NEW[13,4]
    R.NEW(SCB.WH.WHAREHOUSE.NO)   = ID.NEW[17,2]
    IF ID.NEW[3,1] = "0" THEN
        R.NEW(SCB.WH.BRANCH)          = ID.NEW[4,1]
    END ELSE
        R.NEW(SCB.WH.BRANCH)          = ID.NEW[3,2]
    END
    R.NEW(SCB.WH.VERSION.NAME)    = PGM.VERSION
*    R.NEW(SCB.WH.WH.OPENING.DATE) = TODAY

    RETURN
END
