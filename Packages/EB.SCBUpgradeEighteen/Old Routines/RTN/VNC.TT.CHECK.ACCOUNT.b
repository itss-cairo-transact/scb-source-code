* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
**------------15.07.2002 ABEER HEGAZI-----------*

    SUBROUTINE VNC.TT.CHECK.ACCOUNT

*ROUTINE TO:1-  TO CHECK THAT ONLY VOLT CAN ENTER THIS VERSION .
*           2-  DEFAULT NARRATIVE.2 WITH SHORT NAME AND VALUE.DATE WITH TODAY.

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)

    IF V$FUNCTION = 'I' THEN
        IF TT.NO[3,2] # '99' THEN
            E = '��� ����� ��������' ;CALL ERR ;MESSAGE = 'REPEAT'
        END  ELSE
          *******NESSREEN 30/8/2006****************
          ** CUST = '' ;ACCT= '0140000410500401'
          ** CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCT,CUST)
          ** CALL DBR('CUSTOMER':@FM:EB.CUS.NAME.1,CUST,CUST.NAME)
          **  R.NEW( TT.TE.NARRATIVE.2) = CUST.NAME
            R.NEW( TT.TE.VALUE.DATE.1) = TODAY
        END
    END

    RETURN
END
