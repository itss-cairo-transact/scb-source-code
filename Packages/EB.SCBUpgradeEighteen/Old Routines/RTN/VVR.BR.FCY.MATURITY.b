* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* EDIT BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.FCY.MATURITY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*--------------------------------------------
    MAT.DATE      = COMI
    NEXT.MAT.DATE = TODAY
    RECIVE.DATE   = TODAY
    CALL CDT('',NEXT.MAT.DATE,"+1W")

    DAY.NOM       = OCONV(DATE(),"DW")
    COMP          = C$ID.COMPANY
    USR.DEP       = "1700":COMP[8,2]
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)

    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = LOCT

    IF MAT.DATE LE NEXT.MAT.DATE THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 6
        IF LOCT EQ '1'  THEN
            IF (DAY.NOM EQ 7 OR DAY.NOM EQ 2 ) THEN
                CALL CDT('',RECIVE.DATE,"+2W")
            END
            IF (DAY.NOM EQ 1 OR DAY.NOM EQ 3 OR DAY.NOM EQ 4  ) THEN
                CALL CDT('',RECIVE.DATE,"+1W")
            END
        END ELSE
            IF (DAY.NOM EQ 7 OR DAY.NOM EQ 2 OR DAY.NOM EQ 3 ) THEN
                CALL CDT('',RECIVE.DATE,"+2W")
            END
            IF ( DAY.NOM EQ 1 OR DAY.NOM EQ 4 )  THEN
                CALL CDT('',RECIVE.DATE,"+3W")
            END
        END

        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = RECIVE.DATE
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ""
    END
    IF MAT.DATE GT NEXT.MAT.DATE THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 7
        CALL CDT('EG00', MAT.DATE , '+1W')
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE
        CALL CDT('EG00', MAT.DATE , '+':LOCT:'W')
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = MAT.DATE
    END

    CALL REBUILD.SCREEN
    RETURN
END
