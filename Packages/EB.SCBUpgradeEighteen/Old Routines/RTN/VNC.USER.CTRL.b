* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
****      BY MUHAMMAD ELSAYED 24/10/2012    ****
***                             �������� �������� ���� ��� 1941
********************************************
    SUBROUTINE VNC.USER.CTRL
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET

*  IF V$FUNCTION = "A" THEN

    CALL DBR ('USER$NAU':@FM:EB.USE.INPUTTER,ID.NEW,WS.INP.ID)
    IF NOT(ETEXT) AND V$FUNCTION NE "S" THEN
        WS.INP.ID = WS.INP.ID<1,1>
        WS.INP.ID = FIELD(WS.INP.ID,'_',2)


        CALL DBR ('USER':@FM:EB.USE.LOCAL.REF,WS.INP.ID,WS.INP.LOCAL)
        WS.INP.DEP = WS.INP.LOCAL<1,USER.SCB.DEPT.CODE>

        WS.SIGN.ON.NAME = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 48 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL DBR("USER.SIGN.ON.NAME":@FM:EB.USO.USER.ID,WS.SIGN.ON.NAME,WS.AUTH.ID)

        CALL DBR ('USER':@FM:EB.USE.LOCAL.REF,WS.AUTH.ID,WS.AUTH.LOCAL)
        WS.AUTH.DEP = WS.AUTH.LOCAL<1,USER.SCB.DEPT.CODE>

        IF WS.INP.DEP NE WS.AUTH.DEP THEN

            E =  '���� ������ ��� ������� ���� ���� ��������'
*** SCB UPG 20160619 - S
*            CALL ERR ; MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160619 - E


        END
    END

*  END

    RETURN
END
