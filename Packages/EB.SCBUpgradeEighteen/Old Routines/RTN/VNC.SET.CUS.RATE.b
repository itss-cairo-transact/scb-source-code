* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*** WAGDY ***
    SUBROUTINE VNC.SET.CUS.RATE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COUNTRY.LOCAL.REFS


*    IF V$FUNCTION = "I" THEN
        XCUS = FIELD(ID.NEW,".",1)
*        TEXT = XCUS ; CALL REM
        CALL DBR('CUSTOMER': @FM:EB.CUS.LOCAL.REF,XCUS,XXXX)
        CUSRATE =  XXXX<1,CULR.RATEING>
        CALL DBR('CUSTOMER': @FM:EB.CUS.NATIONALITY,XCUS,NATI)
        CALL DBR('COUNTRY': @FM:EB.COU.LOCAL.REF,NATI,YYY)
        CONRATE = YYY<1,CUNT.RATEING>
 *        TEXT = CUSRATE :"  " : CONRATE ; CALL REM
        R.NEW(LI.LOCAL.REF)<1,LILR.CUSTOMER.RATING> = CUSRATE
        R.NEW(LI.LOCAL.REF)<1,LILR.COUNTRY.RATING> = CONRATE

 *   END
    RETURN
END
