* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
****** UPDATED BY INGY*****

    SUBROUTINE VNC.STO.DEFAULT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDING.ORDER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT


    IF V$FUNCTION = 'I' THEN

        ID = FIELD(ID.NEW, ".", 1)
         CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, ID, CURR.NAME)


       * CALL DBR( 'NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE, CURRENCY, CURR.NAME )

        IF CURR.NAME # LCCY THEN
            E= 'Only.Local.Currency' ;CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            CUS = TRIM (ID.NEW[5,7], '0', 'L')
            CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUS,XXX)
            R.NEW(STO.ORDERING.CUST) = XXX
        END

    END
    RETURN
END
