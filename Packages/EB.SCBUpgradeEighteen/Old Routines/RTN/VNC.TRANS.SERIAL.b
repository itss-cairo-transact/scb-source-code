* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.TRANS.SERIAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE

* Created by Noha Hamed
    CURR    = R.NEW(CBE.PAY.CURR.NO)
    IF V$FUNCTION EQ 'I' AND CURR EQ '' THEN
        AREA   = R.NEW(CBE.PAY.ARERAN.CODE)
        FUNTP  = COMI
        SELECTED = 0

        T.SEL  = "SELECT F.SCB.MONTHLY.PAY.CBE WITH TYPE.FILE EQ 1 "

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        NO     = SELECTED + 1
        LENGHT = LEN(NO)
        LED.ZERO = 6 - LENGHT

        FOR I = 1 TO LED.ZERO
            SERIAL = SERIAL:"0"
        NEXT I
        TEMP = SERIAL:NO

        Y.SEL  = "SELECT F.SCB.MONTHLY.PAY.CBE WITH TYPE.FILE EQ 1 AND TRNDET.SER EQ ":SERIAL = TEMP
        CALL EB.READLIST(Y.SEL,KEY.LIST2,"",SELECTED2,ER.MSG)

        IF SELECTED2 THEN
            NO     = NO + 1
            SERIAL = SERIAL:NO
        END
        ELSE
            SERIAL = TEMP
        END

        R.NEW(CBE.PAY.TRNDET.SER) = SERIAL
    END
    RETURN
END
