* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
******************************************************
***  CREATED BY MOHAMED SABRY 2010-10-17
******************************************************
    SUBROUTINE VNC.UA.COMP.CHK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACTIVITY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

    FN.HMM    = 'F.HELPTEXT.MAINMENU' ; F.HMM = '' ; R.HMM = ''
    FN.ACTV   = 'F.ACTIVITY'          ; F.ACTV = ''
    CALL OPF(FN.ACTV,F.ACTV)

    WS.INP.COMP    = ID.COMPANY
    WS.INP.SCB.DEP = R.USER<EB.USE.LOCAL.REF>
    WS.INP.SCB.DEP = WS.INP.SCB.DEP<1,USER.SCB.DEPT.CODE>
    WS.MOD.USER    = ID.NEW

*----------------------------------
*UPDATE BY MOHAMED SABRY 2012/02/06
    IF V$FUNCTION = 'A' THEN
        IF WS.INP.SCB.DEP EQ 2900 OR WS.INP.SCB.DEP EQ 5100 THEN
            CALL F.READ(FN.ACTV,WS.MOD.USER,R.ACTV,F.ACTV,E.ACTV)
            IF NOT(E.ACTV) THEN
                E = '���� ������ �� ��� ������� ������ ������'
*** SCB UPG 20160623 - S
*                CALL ERR ; MESSAGE = 'REPEAT'
*                E=''; CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END
        END
    END
*----------------------------------

    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C' THEN
        IF WS.INP.SCB.DEP EQ 2900 OR WS.INP.SCB.DEP EQ 5100 THEN
            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,WS.MOD.USER,WS.MOD.DEP.CODE)
*----------------------------------
*UPDATE BY MOHAMED SABRY 2012/01/30
            CALL F.READ(FN.ACTV,WS.MOD.USER,R.ACTV,F.ACTV,E.ACTV)
            IF NOT(E.ACTV) THEN
                E = '���� ������ �� ��� ������� ������ ������'
*** SCB UPG 20160623 - S
*                CALL ERR ; MESSAGE = 'REPEAT'
*                E=''; CALL ERR ; MESSAGE = 'REPEAT'
                CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
            END

            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,WS.MOD.USER,LOCAL.REF)

            CALL DBR('USER$NAU':@FM:EB.USE.LOCAL.REF,WS.MOD.USER,LOCAL.REF.NAU)
            WS.SCB.DEPT     = LOCAL.REF<1,USER.SCB.DEPT.CODE>
            WS.SCB.DEPT.NAU = LOCAL.REF.NAU<1,USER.SCB.DEPT.CODE>
            IF NOT(ETEXT) THEN
                IF WS.SCB.DEPT EQ '5100' OR WS.SCB.DEPT.NAU EQ '5100' THEN
                    IF WS.SCB.DEPT # WS.SCB.DEPT.NAU THEN
                        E = '���� ����� ��� �������� ����'
*** SCB UPG 20160623 - S
*                        CALL ERR ; MESSAGE = 'REPEAT'
*                        E=''; CALL ERR ; MESSAGE = 'REPEAT'
                        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                    END
                END
            END
*TEXT = WS.SCB.DEPT ; CALL REM
            IF WS.SCB.DEPT EQ 5100 THEN
                GOSUB EX.ALL.HMM
            END
*----------------------------------
            WS.DEP.LENS = LEN(WS.MOD.DEP.CODE)

            IF WS.DEP.LENS EQ 1 THEN
                WS.MOD.COMP = 'EG001000':WS.MOD.DEP.CODE
            END

            IF WS.DEP.LENS EQ 2 THEN
                WS.MOD.COMP = 'EG00100':WS.MOD.DEP.CODE
            END
            IF WS.MOD.DEP.CODE # 88 THEN
                IF WS.INP.COMP # WS.MOD.COMP  THEN

                    E = '��� ����� ������ ������ �� ���� �����'
*** SCB UPG 20160623 - S
*                    CALL ERR ; MESSAGE = 'REPEAT'
*                    E=''; CALL ERR ; MESSAGE = 'REPEAT'
                    CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                END
            END

**** UPDATED BY MOHAMED SABRY 2012/03/25     *************************
*            IF WS.SCB.DEPT EQ 5101 AND WS.MOD.DEP.CODE EQ 99 THEN
*                GOSUB EX.ALL.HMM
*            END
**** END OF UPDAT BY MOHAMED SABRY 2012/03/25 *************************
        END ELSE
            E = '��� �������� ����� ����� ����� �� ����� ���'
*** SCB UPG 20160623 - S
*            CALL ERR ; MESSAGE = 'REPEAT'
*            E=''; CALL ERR ; MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
        RETURN
*----------------------------------------------------------------------------------
*UPDATE BY MOHAMED SABRY 2012/01/30
*----------------------------------
EX.ALL.HMM:
        GOSUB CLEAR.UA
        WS.TEXT.UA.NO = 0
        T.SEL1 = "SELECT ":FN.HMM:" WITH @ID GE 400 AND @ID LT 499 WITHOUT @ID IN ( 404 425 427 428 429 432 435 439 447 461 462 480 493 497 ) BY @ID "

        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG2)
        IF SELECTED1 THEN
            FOR IHMM = 1 TO SELECTED1
                WS.TEXT.UA.NO ++
                R.NEW(EB.UAB.ABBREVIATION)<1,WS.TEXT.UA.NO>  =     KEY.LIST1<IHMM>
                R.NEW(EB.UAB.ORIGINAL.TEXT)<1,WS.TEXT.UA.NO> = "?":KEY.LIST1<IHMM>
            NEXT IHMM
        END
        RETURN
*----------------------------------------------------------------------------------
CLEAR.UA:
*Line [ 159 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = DCOUNT(R.NEW(EB.UAB.ABBREVIATION),@VM)
        FOR ICOUNT = 1 TO WS.COUNT
            DEL R.NEW(EB.UAB.ABBREVIATION)<1,ICOUNT>
            DEL R.NEW(EB.UAB.ORIGINAL.TEXT)<1,ICOUNT>
            ICOUNT --
            WS.COUNT --
        NEXT ICOUNT
        RETURN
*----------------------------------------------------------------------------------

    END
