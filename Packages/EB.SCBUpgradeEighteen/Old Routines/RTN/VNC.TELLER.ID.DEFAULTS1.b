* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>550</Rating>
*-----------------------------------------------------------------------------
*----- WAEL  21/07/2002----

    SUBROUTINE VNC.TELLER.ID.DEFAULTS1

*DEFAULT (NARRATIVE.2) WITH BRANCH NAME
*DEFAULT (TELLER.ID.1) WITH TELLER ID OF THE USER (VAULT)
*DEFAULT (TELLER.ID.2) WITH TELLER ID OF THE DESTINATION TELLER (HEAD TELLER)
*CHECK IF YOU ARE THE CORRECT USER (VAULT) OR NOT


*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER


    IF V$FUNCTION = 'I' THEN

        IF NOT(R.NEW(TT.TE.TELLER.ID.1)) THEN
            IF NOT(R.NEW(TT.TE.TELLER.ID.2)) THEN



                CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,OPERATOR,DEPT)
                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
                R.NEW(TT.TE.NARRATIVE.2) = DEPT.NAME

*==================================================================================
***UPDATED BY NESSREEN AHMED 09/02/2010********************************
***  CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
                T.SEL  = "SELECT FBNK.TELLER.ID WITH USER EQ ":OPERATOR :" AND STATUS EQ OPEN "
                CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ERR1)
                TEXT = 'SELECTED=':SELECTED ; CALL REM
                TT.NO = KEY.LIST<1>
***END OF UPDATED 09/02/2010***********************************************
                IF INDEX(TT.NO,'99',1) < 1 THEN E = '��� ����� �������'


                R.NEW(TT.TE.TELLER.ID.1) = TT.NO


                IF LEN (DEPT) < 2 THEN
                    R.NEW(TT.TE.TELLER.ID.2) = 0:DEPT:98
                END

                IF LEN (DEPT) > 1 THEN
                    R.NEW(TT.TE.TELLER.ID.2) = DEPT:98
                END


            END
        END
    END
*=======================================================================================
    IF V$FUNCTION ='A' THEN
        IF INDEX(TT.NO,'98',1) > 1 THEN E = '��� ����� �������'
    END


    IF V$FUNCTION ='R' THEN
        IF INDEX(TT.NO,'98',1) > 1 THEN E = '��� ����� �������'
    END

*=======================================================================================
    IF E THEN CALL ERR ;MESSAGE = 'REPEAT'




    RETURN
END
