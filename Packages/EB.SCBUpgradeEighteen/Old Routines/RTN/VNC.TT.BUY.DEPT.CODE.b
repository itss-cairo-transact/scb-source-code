* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------

**---------INGY 10/07/2002---------**
**--------UPDATE BY DALIA 12/07/2002----**

SUBROUTINE VNC.TT.BUY.DEPT.CODE

*THE VERSION HAS ZERO AUTHORIZED BUT IT CAN PUT IN IHLD STATUS (IHLD)
*THE ROUTINE TO CHECK IF THE OPERATION IN TELLER$NAU (IHLD) THEN
*IF THE DEPARTMENT.CODE OF THE INPUTTER NOT EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*DISPLAY ERROR
*IF THE DEPARTMENT.CODE OF THE INPUTTER  EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
* CHECK IF THE TT.TR.SHORT.DESC (TELLER.TRANSACTION) WHICH RELATED TO VER.NAME ACCOURDING TO
*TRANSACTION.CODE NOT EQUAL PGM.VERSION DISPLAY ERROR

*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION

VER.NAME = ''
INPUTT = ''
BRANCH = ''
TRANS.ID= R.NEW(TT.TE.TRANSACTION.CODE)
 IF V$FUNCTION ='I' THEN
   CALL DBR('TELLER$NAU':@FM:TT.TE.INPUTTER, ID.NEW ,INPUTT)
      IF NOT(ETEXT) THEN
      CALL GET.INPUTTER.BRANCH(BRANCH,INPUTT)
          IF R.USER<EB.USE.DEPARTMENT.CODE> # BRANCH THEN
           E = 'THIS.OPER.FROM.OTHER.BRANCH'
            CALL ERR ;MESSAGE = 'REPEAT'

          END ELSE
          CALL DBR('TELLER.TRANSACTION':@FM:TT.TR.SHORT.DESC,TRANS.ID,VER.NAME)
            IF NOT(ETEXT) THEN
                   IF VER.NAME<1,1> # PGM.VERSION THEN

                   E = 'YOU MUST RETRIVE THIS RECORD FROM VERSION & ' : @FM : VER.NAME<1,1>
                     CALL ERR ; MESSAGE = 'REPEAT'

                    END
               END
         END

      END
END
  RETURN
END
