* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BR.CHECK.MATURITY.UPD

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.FT.CHARGE.TYPE
    $INSERT T24.BP I_F.FT.COMMISSION.TYPE
    $INSERT           I_F.SCB.BR.PAY.PLACE
    $INSERT           I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BANK.BRANCH
    $INSERT           I_F.SCB.BR.CUS
*----------------------------------------------
    CUS.LIST  = "13300312 13300188 13300246 13300246 13300289"
    CUS.LIST := " 13300339 13300399 13300400"

    CUS.NOO = R.NEW(EB.BILL.REG.DRAWER)
    CUR.T   = R.NEW(EB.BILL.REG.CURRENCY)
    IN.O    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL>
    AMTT    = R.NEW(EB.BILL.REG.AMOUNT)

    IF COMI THEN    ;* maturity date local-25
        MAT.DATE       = COMI
        NEXT.MAT.DATE  = TODAY
        COMP           = C$ID.COMPANY
        USR.DEP        = "1700":COMP[8,2]
        CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)

        DAL.TODAY      = TODAY
        IF LOCT EQ '1'  THEN
            CALL CDT('EG00',NEXT.MAT.DATE,"+1W")
        END ELSE
            CALL CDT('EG00',NEXT.MAT.DATE,"+2W")
        END

        IF MAT.DATE LE NEXT.MAT.DATE THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 6
        END
        IF MAT.DATE GT NEXT.MAT.DATE THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 7
        END
    END
    IF IN.O EQ 'YES' THEN
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> NE 'NO' THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
        END

        CHRG.AMT  = ""
        Y.OUTPUT  = 'NO'
        IF R.NEW(EB.BILL.REG.CURRENCY) NE 'EGP' THEN
*Line [ 72 ] Adding EB.SCBUpgradeEighteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            CALL EB.SCBUpgradeEighteen.VVR.BR.CHECK.MATURITY.ROT(Y.OUTPUT)
        END

        IF ( R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP') AND (Y.OUTPUT EQ 'NO') THEN
            IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'YES' ) THEN
                IF MESSAGE # 'VAL' THEN
                    IF COMI NE R.NEW(BRLR.MAT.DATE) THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                    END
                END
                GOSUB INTIAL

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 9 THEN
                    GOSUB OUT.OF.CLEARING
                    RETURN
                END

                IF COMI THEN  ;* maturity date local-25
                    MAT.DATE       = COMI
                    NEXT.MAT.DATE  = TODAY
                    COMP           = C$ID.COMPANY
                    USR.DEP        = "1700":COMP[8,2]
                    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)

                    DAL.TODAY      = TODAY
                    IF LOCT EQ '1'  THEN
                        CALL CDT('EG00',NEXT.MAT.DATE,"+1W")
                    END ELSE
                        CALL CDT('EG00',NEXT.MAT.DATE,"+2W")
                    END

                    IF MAT.DATE LE NEXT.MAT.DATE THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 6
                    END
                    IF MAT.DATE GT NEXT.MAT.DATE THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 7
                    END

                    FN.CUS = 'FBNK.CUSTOMER'    ; F.CUS = ''
                    CALL OPF(FN.CUS,F.CUS)
                    CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
                    SEC  = R.CUS<EB.CUS.SECTOR>
                    CURR = R.NEW(EB.BILL.REG.CURRENCY)
                    AMTT = R.NEW(EB.BILL.REG.AMOUNT)

                    FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
                    CALL OPF(FN.BR.COM,F.BR.COM)

                    BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
                    CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)

                    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 7 THEN
                        IF COMI THEN
                            IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN
                                IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                                    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE '0017' THEN
                                        IF NOT(E11) THEN
                                            COM.AMT   = R.BR.COM<BR.CUS.COMM.AMT>
                                            COM.PR    = R.BR.COM<BR.CUS.COMM.PREC>
                                            CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>
                                            CHRG.AMT6 = R.BR.COM<BR.CUS.CHRG.AMT.6>

                                            IF CHRG.AMT EQ 'YES' OR COM.AMT EQ 0 THEN
                                                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> NE 'NO' THEN
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                                                END

                                                IF CHRG.AMT EQ 'YES' THEN
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = ""
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ""

                                                    CALL REBUILD.SCREEN
                                                END
                                            END ELSE
                                                IF CHRG.AMT6 THEN
                                                    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 6 AND IN.O EQ ' YES' THEN
                                                        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                                                            CHRG.ID = "BILCHARG6"
                                                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG.AMT6
                                                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = CHRG.ID
                                                        END
                                                    END
                                                END ELSE
*--- CALC NORMAL CHARGE
                                                END
                                                CALL REBUILD.SCREEN
                                                IF COM.PR THEN
                                                    COM.ID    = "BILLCOLL"
                                                    FN.FT.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
                                                    CALL OPF(FN.FT.COM,F.FT.COM)
                                                    CALL F.READ(FN.FT.COM,COM.ID,R.FT.COM,F.FT.COM,E1)

                                                    LOCATE CURR IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                                                    PER  = R.FT.COM<FT4.PERCENTAGE,J> * COM.PR
                                                    MIN  = R.FT.COM<FT4.MINIMUM.AMT,J>
                                                    MAX  = R.FT.COM<FT4.MAXIMUM.AMT,J>

                                                    IF R.NEW(EB.BILL.REG.DRAWER) EQ '2300227' AND TODAY LE '20171221' THEN
                                                        MAX = "100"
                                                    END
                                                    COMM.AMT = (COM.AMT*PER/100)

                                                    IF COMM.AMT LT MIN THEN
                                                        COMM.AMT = MIN
                                                    END ELSE
                                                        IF COMM.AMT GT MAX THEN
                                                            COMM.AMT = MAX
                                                        END
                                                    END
                                                    CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
                                                    IF COM.AMT GT 0 THEN
                                                        COMM.AMT = COM.AMT
                                                    END
                                                    COM.ID   = "BILLCOLL"
                                                    COMM.AMT = CURR:COMM.AMT
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
                                                END ELSE
                                                    COM.ID    = "BILLCOLL"
                                                    FN.FT.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
                                                    CALL OPF(FN.FT.COM,F.FT.COM)
                                                    CALL F.READ(FN.FT.COM,COM.ID,R.FT.COM,F.FT.COM,E1)

                                                    LOCATE CURR IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                                                    PER  = R.FT.COM<FT4.PERCENTAGE,J>
                                                    MIN  = R.FT.COM<FT4.MINIMUM.AMT,J>
                                                    MAX  = R.FT.COM<FT4.MAXIMUM.AMT,J>

                                                    IF R.NEW(EB.BILL.REG.DRAWER) EQ '2300227' AND TODAY LE '20171221' THEN
                                                        MAX = "100"
                                                    END
                                                    COMM.AMT = (AMTT*PER/100)

                                                    IF COMM.AMT LT MIN THEN
                                                        COMM.AMT = MIN
                                                    END ELSE
                                                        IF COMM.AMT GT MAX THEN
                                                            COMM.AMT = MAX
                                                        END
                                                    END
                                                    CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
                                                    IF COM.AMT GT 0 THEN
                                                        COMM.AMT = COM.AMT
                                                    END
                                                    COM.ID   = "BILLCOLL"
                                                    COMM.AMT = CURR:COMM.AMT
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
                                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
                                                END

*                                                IF COM.AMT GT 0 THEN
*                                                    COMM.AMT = COM.AMT
*                                                END
*                                                COM.ID   = "BILLCOLL"
*                                                COMM.AMT = CURR:COMM.AMT
*                                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
*                                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
                                            END
                                        END ELSE
************************************* NO EXP
                                            COM.ID    = "BILLCOLL"
                                            FN.FT.COM = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
                                            CALL OPF(FN.FT.COM,F.FT.COM)
                                            CALL F.READ(FN.FT.COM,COM.ID,R.FT.COM,F.FT.COM,E1)
                                            LOCATE CURR IN R.FT.COM<FT4.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                                            PER  = R.FT.COM<FT4.PERCENTAGE,J>
                                            MIN  = R.FT.COM<FT4.MINIMUM.AMT,J>
                                            MAX  = R.FT.COM<FT4.MAXIMUM.AMT,J>
                                            IF R.NEW(EB.BILL.REG.DRAWER) EQ '2300227' AND TODAY LE '20171221' THEN
                                                MAX = "100"
                                            END
                                            COMM.AMT = (AMTT*PER/100)
                                            IF COMM.AMT LT MIN THEN
                                                COMM.AMT = MIN
                                            END ELSE
                                                IF COMM.AMT GT MAX THEN
                                                    COMM.AMT = MAX
                                                END
                                            END
                                            CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
                                            COMM.AMT = CURR:COMM.AMT
                                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = COM.ID
                                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
                                        END
                                    END
                                END
                            END ELSE
                                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> NE 'NO' THEN
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
                                END
                                CALL REBUILD.SCREEN
                            END
                        END

                        IF CHRG.AMT EQ 'YES' THEN
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
                            CALL REBUILD.SCREEN
                        END
                        CALL REBUILD.SCREEN
                    END ELSE
                        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 6 AND IN.O EQ ' YES' THEN
                            FLG = "NO"
                            FINDSTR CUS.NOO IN CUS.LIST SETTING FND THEN
                                FLG = "YES"
                            END
                            IF FLG EQ "NO" THEN
                                FN.CHRG = "FBNK.FT.CHARGE.TYPE" ; F.CHRG = ""
                                CALL OPF(FN.CHRG, F.CHRG)
                                CHRG.ID = "BILCHARG6"
                                CALL F.READ(FN.CHRG,CHRG.ID,R.FT.CHRG,F.CHRG,E123)
                                LOCATE "EGP" IN R.FT.CHRG<FT5.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                                CHRG = R.FT.CHRG<FT5.FLAT.AMT,J>
                                IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = CHRG.ID
                                END
                                CALL REBUILD.SCREEN
                            END
                        END
                    END
                    IF MAT.DATE GT NEXT.MAT.DATE THEN
                        GOSUB CALC.DATE.S.PP
                        GOSUB CALC.DATE.COLLECTION
                    END ELSE
                        GOSUB CALC.DATE.SESSION
                        IF NOT(ETEXT) THEN
                            GOSUB CALC.DATE.COLLECTION
                        END
                    END
                END
            END
        END
        IF MAT.DATE GT NEXT.MAT.DATE THEN
            GOSUB CALC.DATE.S.PP
            GOSUB CALC.DATE.COLLECTION
        END ELSE
            GOSUB CALC.DATE.SESSION
            IF NOT(ETEXT) THEN
                GOSUB CALC.DATE.COLLECTION
            END
        END
        IF ( R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP') AND (Y.OUTPUT EQ 'NO') THEN
*-------------WAGDY-20100610---------------
            IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN
                CALL F.READ(FN.BR.COM,BILL.CUS,R.EXP,F.BR.COM,E11)
                IF NOT(E11) THEN
                    FEES   = R.EXP<BR.CUS.KEEP.FEES>
                    CHRG6  = R.EXP<BR.CUS.CHRG.AMT.6>
                END
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 7 THEN
                    IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
**-------------------------FEES.EXP ----------------------
                        MAT.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
                        REC.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

                        DAYS.BET = 'C'
                        CALL CDD("",REC.DT,MAT.DT,DAYS.BET)

                        YEE  = DAYS.BET / 365
                        WW   = INT(DAYS.BET/365)

                        IF FEES EQ '' THEN
                            FN.CHRG = "FBNK.FT.CHARGE.TYPE" ; F.CHRG = ""
                            CALL OPF(FN.CHRG, F.CHRG)
                            CHRG.ID = "BILCHARG"
                            CALL F.READ(FN.CHRG,CHRG.ID,R.FT.CHRG,F.CHRG,E123)
                            LOCATE "EGP" IN R.FT.CHRG<FT5.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                            CHRG = R.FT.CHRG<FT5.FLAT.AMT,J>
                        END ELSE
                            CHRG = FEES
                        END

                        IF CUR.T EQ 'EGP' THEN
                            IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = "BILCHARG"
                                CALL REBUILD.SCREEN
                            END
                        END

                        IF WW GE 1 THEN
                            IF YEE GT WW THEN
                                CHRG = ( WW * CHRG ) + CHRG
                            END ELSE
                                CHRG = CHRG
                            END
                            IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = "BILCHARG"
                            END
                            CALL REBUILD.SCREEN
                        END
                    END
                    IF CHRG.AMT EQ 'YES' THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = ""
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ""
                        CALL REBUILD.SCREEN
                    END
                END ELSE
                    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 6 THEN
                        IF CHRG6 EQ '' THEN
                            FN.CHRG = "FBNK.FT.CHARGE.TYPE" ; F.CHRG = ""
                            CALL OPF(FN.CHRG, F.CHRG)
                            CHRG.ID = "BILCHARG6"
                            CALL F.READ(FN.CHRG,CHRG.ID,R.FT.CHRG,F.CHRG,E123)
                            LOCATE "EGP" IN R.FT.CHRG<FT5.CURRENCY,1>  SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'
                            CHRG = R.FT.CHRG<FT5.FLAT.AMT,J>
                        END ELSE
                            CHRG.ID = "BILCHARG6"
                            CHRG    = CHRG6
                        END

                        IF CUR.T EQ 'EGP' THEN
                            IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = CHRG.ID
                                CALL REBUILD.SCREEN
                            END
                        END
                    END
                END
            END
            CALL REBUILD.SCREEN

            IF R.NEW(EB.BILL.REG.DRAWER)[1,3] EQ '994' THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
            END
            CALL REBUILD.SCREEN

            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ '0017' THEN
*   IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> NE 'NO' THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
*   END
            END
        END
    END
*---- 2019/03/03
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ '0017' THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""
    END
    CALL REBUILD.SCREEN
    RETURN
*--------------------------------------------------------------
INTIAL:
    MAT.DATE = '' ; MAT.DATE1 = '' ; DIFF = '' ; DAT1 = '' ; PAY.CODE = ''
    PAY.DAYS = ''
    RETURN
*----------------------------
CALC.DATE.SESSION:
    T.DAY = TODAY

    XX  = ''
    CALL JULDATE(TODAY,XX )
    XX.DAT = XX - 999
    XX1 = ''
    CALL JULDATE(MAT.DATE,XX1 )

    IF XX1 LT XX.DAT THEN
        ETEXT = "����� ������� ����� ��� ���� ���"
    END ELSE

        IF LOCT EQ '1'  THEN
            CALL CDT('',T.DAY,"+1W")
        END ELSE
            CALL CDT('',T.DAY,"+2W")
        END
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = T.DAY

        CALL REBUILD.SCREEN
    END
    RETURN
*------------------------------------------------------------------
CALC.DATE.S.PP:
    XX.N      = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
    MAT.DATE1 = COMI

    CALL AWD("EG00",MAT.DATE1,HOL)
    IF HOL EQ "H" THEN
        CALL CDT("EG00",MAT.DATE1,"+1W")
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
    END ELSE
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
    END
    CALL REBUILD.SCREEN
    RETURN
*------------------------------------------------------------------
CALC.DATE.COLLECTION:
    XX.N = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
    DAT1 = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
    PAY.CODE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE>
    IF PAY.CODE EQ 0 THEN
        PAY.DAYS = 0
    END ELSE
        PAY.DAYS = PAY.CODE
    END

    CALL CDT('EG00', DAT1, '+':PAY.DAYS:'W')
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE> = DAT1
    CALL REBUILD.SCREEN
    RETURN
*----------------------------------
OUT.OF.CLEARING:
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
    RETURN
*----------------------------------
END
