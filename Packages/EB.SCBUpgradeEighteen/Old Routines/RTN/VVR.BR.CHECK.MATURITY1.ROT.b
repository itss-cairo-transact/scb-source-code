* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BR.CHECK.MATURITY1.ROT(Y.OUTPUT)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS.CUR
*------------------------------------------
    SHIFT ='' ; ROUND =''
    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)


    GOSUB INTIAL
    ETEXT = ''  ; VAL.DAT = ''
    VAL.DAT = COMI
    IF VAL.DAT LE TODAY THEN
        ETEXT = '��� �� ���� ����� ���� ���� �� ����� ����� ' ; CALL STORE.END.ERROR
    END ELSE
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 9 THEN
            GOSUB OUT.OF.CLEARING
            RETURN
        END

        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 10 THEN
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.DR.CITY> THEN
                CALL AWD("EG00",COMI,HOL)
                IF HOL EQ "H" THEN
                    ETEXT = "��� ����� �����"
                END
            END

            GOSUB BILLS
            RETURN
        END

        IF COMI THEN
            MAT.DATE = COMI
            IF MAT.DATE GT TODAY THEN
                GOSUB CALC.DATE.S.PP
                GOSUB CALC.DATE.COLLECTION
            END ELSE
                GOSUB CALC.DATE.SESSION
                IF NOT(ETEXT) THEN
                    GOSUB CALC.DATE.COLLECTION
                END
            END
        END

        RETURN
*---------------------------------------------------------------------
INTIAL:
        MAT.DATE = '' ; MAT.DATE1 = '' ; DIFF = '' ; DAT1 = '' ; PAY.CODE = ''
        PAY.DAYS = ''
        RETURN

CALC.DATE.SESSION:
*------------------
        T.DAY = TODAY

        XX  = ''
        CALL JULDATE(TODAY,XX )
        XX1 = ''
        CALL JULDATE(MAT.DATE,XX1 )
        DIFF = XX - XX1
        IF DIFF GT 366 THEN
            ETEXT = "����� ������� ����� ��� ���� ���"
        END ELSE
            IF R.USER<EB.USE.DEPARTMENT.CODE> NE 1 THEN
                CALL CDT('',T.DAY,"+2W")
            END ELSE
                CALL CDT('',T.DAY,"+1W")
            END
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = T.DAY
            CALL REBUILD.SCREEN
        END
        RETURN

CALC.DATE.S.PP:
*--------------
        MAT.DATE1 = COMI

        IF R.USER<EB.USE.DEPARTMENT.CODE> NE 1 THEN
            CALL CDT('',MAT.DATE1,"+2W")
        END ELSE
            CALL CDT('',MAT.DATE1,"+1W")
        END
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
        CALL REBUILD.SCREEN

        RETURN
*-------------------------------------------------------------------
CALC.DATE.COLLECTION:
*--------------------
        DAT1 = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
        PAY.CODE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE>
        IF PAY.CODE EQ 0 THEN
            PAY.DAYS = 0
        END ELSE
            PAY.DAYS = (PAY.CODE * 2) - 1
        END

        CALL CDT('', DAT1, '+':PAY.DAYS:'W')

        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE> = DAT1

        CALL REBUILD.SCREEN
        RETURN
**************************
OUT.OF.CLEARING:
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
        RETURN
**************************
BILLS:
        MAT.DATE = COMI
        XX  = ''
        CALL JULDATE(TODAY,XX )
        XX.DAT = XX - 999
        XX1 = ''
        CALL JULDATE(MAT.DATE,XX1 )

        IF XX1 LT XX.DAT THEN
            ETEXT = "����� ������� ����� ��� ���� ���"
        END ELSE
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ 0017 THEN
                SCB.BRANCH.NO = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR>[5,2]
                SCB.BRANCH.NO = TRIM( SCB.BRANCH.NO, '0', 'L')        ;* remove leading zeros
                INPUTTER.BRANCH.NO = R.USER<EB.USE.DEPARTMENT.CODE>
*---------------------------------------------------------------
                IF SCB.BRANCH.NO EQ INPUTTER.BRANCH.NO THEN
                    IF COMI LT TODAY THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = TODAY
                        CALL REBUILD.SCREEN
                    END ELSE
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = COMI
                        CALL REBUILD.SCREEN
                    END
                END ELSE 
                    MAT.DAT = COMI
                    CALL CDT('', MAT.DAT, '-30C')
                    IF MAT.DAT LT TODAY THEN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = TODAY
                        CALL REBUILD.SCREEN
                    END ELSE
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DAT
                        CALL REBUILD.SCREEN
                    END 
                END 
            END ELSE
                MAT.DAT = COMI
                CALL CDT('', MAT.DAT, '-30C')
                IF MAT.DAT LT TODAY THEN
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = TODAY
                    CALL REBUILD.SCREEN
                END ELSE
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DAT
                    CALL REBUILD.SCREEN
                END
            END
        END
    END
*-------------WAGDY-20100610---------------

    IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN
        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,E22)
        SEC  = R.CUS<EB.CUS.SECTOR>
        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN
            MAT.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
            REC.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

            START.DATE = SHIFT.DATE( REC.DT, '1M', 'UP')
            DAYS.BET   = 'C'
            CALL CDD("",REC.DT,MAT.DT,DAYS.BET)

            YEE = DAYS.BET / 365
            WW  = INT(DAYS.BET/365)

            MAT.DT.1 = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MAT.DATE>
            IF COMI GT START.DATE THEN
                CHRG = 3
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> = CHRG
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = "BILCHARG"
                CALL REBUILD.SCREEN
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ""
                CALL REBUILD.SCREEN
            END

            IF WW GE 1 THEN
                IF YEE GT WW THEN
                    CHRG = ( WW * 3 ) + 3
                END ELSE
                    CHRG = WW
                END
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> = CHRG
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = "BILCHARG"
                CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
