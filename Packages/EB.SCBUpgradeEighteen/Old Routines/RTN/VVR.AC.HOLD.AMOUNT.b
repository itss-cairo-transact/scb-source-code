* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>243</Rating>
*-----------------------------------------------------------------------------
** ----- 17.06.2002 ABEER (MRA GROUP) -----

    SUBROUTINE VVR.AC.HOLD.AMOUNT
*A ROUTINE 1-TO CHECK THAT INPUTTERS FROM SAME DEPARTMENTS ONLY ARE ALLOWED TO CHANGE THE RECORD (VALIDATION MODE ONLY)
*          2-TO CHECK THAT HOLD.AMOUNT MUST BE ENTERED ALSO THAT ITS SUM IS SMALLER/EQUAL WORKING.BALANCE.

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*------ (1)

    IF MESSAGE NE 'VAL' THEN
        DEPT.NAME = ''
        DEPT = R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER,AS>
        US = R.USER<EB.USE.SIGN.ON.NAME>
        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,DEPT.NAME)
        IF COMI GT 0 THEN
*       ----------  GET THE DEPRTMENT.NAME FOR THE UPDATED RECORD -----------

*       ----------  GET THE DEPRTMENT.NAME FOR THE USER -----------

            IF DEPT  NE US:'-':DEPT.NAME THEN
*  ETEXT = 'You.Not.Authorise.To.Update.This.Record'
                ETEXT = '��� �� ������ �� ���� �� ��� ����� '
                CALL STORE.END.ERROR
            END ELSE
                IF COMI NE R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,AS> THEN
                    ETEXT='��� ����� ������'
                    R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,AS>=COMI
                    CALL REBUILD.SCREEN
                END
                R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.DATE,AS>=TODAY
                CALL REBUILD.SCREEN
            END

        END  ELSE
            IF COMI EQ '0.00' THEN
                IF DEPT  NE US:'-':DEPT.NAME  THEN
                    ETEXT = '��� �� ������ �� ���� �� ��� ����� '
                    CALL STORE.END.ERROR
                END ELSE
                    R.NEW(AC.LOCAL.REF)<1,ACLR.EXPIRY.DATE,AS>=TODAY
                    CALL REBUILD.SCREEN
                END
            END ELSE
* ETEXT = 'You.Should.Enter.Amount'
                ETEXT = '���� �� ����� ������'
                CALL STORE.END.ERROR
            END

        END
        CALL REBUILD.SCREEN
*------ (2)
    END ELSE

        AMT = ''

*Line [ 86 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        FOR I = 1 TO DCOUNT(R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.DATE>,@SM)

            IF R.NEW(AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> = '' THEN  YY = 1
*             R.NEW( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I> = COMI
            AMT = AMT + R.NEW( AC.LOCAL.REF)<1,ACLR.HOLD.AMOUNT,I>
        NEXT I

        IF YY = 1 THEN
* ETEXT = 'You.Should.Enter.Amount'
            ETEXT = '���� �� ����� ������'
            CALL STORE.END.ERROR
        END

        IF AMT GT R.NEW(AC.WORKING.BALANCE) THEN
            R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.AMOUNT,AS> = R.OLD(AC.LOCAL.REF)<1, ACLR.HOLD.AMOUNT,AS>
*  ETEXT = 'Total.Amounts.Greater.Than.Available.Balance'
            ETEXT = '������ ������ ���� �� ������ ������'
            CALL STORE.END.ERROR
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
