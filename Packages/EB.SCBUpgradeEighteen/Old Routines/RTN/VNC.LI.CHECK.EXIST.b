* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
***** WAEL *****

SUBROUTINE VNC.LI.CHECK.EXIST

* PREVENT USER FORM INPUTING A NEW RECORD
* PREVENT USER FROM MODIFYING A RECORD
* FOR A CUSTOMER THAT IS BANK

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

IF V$FUNCTION = 'I' THEN

XX = FIELD(ID.NEW,'.',1)
CALL DBR ( 'CUSTOMER':@FM:EB.CUS.SECTOR,XX,SEC)
IF (SEC >= 3000 AND SEC <= 3999 ) THEN E = 'MUST.BE.NOT.BANK' ; CALL ERR ;MESSAGE = 'REPEAT'


  CALL DBR('LIMIT':@FM:LI.LIMIT.CURRENCY,ID.NEW,DUMMY)
  IF ETEXT THEN E = 'MUST.BE.EXSISTING.RECORD' ;CALL ERR ;MESSAGE = 'REPEAT'

END




RETURN
END
