* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AC.COLL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY

    IF COMI # "" THEN

        IF R.NEW(COLL.COLLATERAL.CODE) EQ "101" OR R.NEW(COLL.COLLATERAL.CODE) EQ "103" THEN
            ETEXT = "��� ����� ��������"
            CALL STORE.END.ERROR
        END ELSE

            R.NEW(COLL.APPLICATION.ID)= COMI
            CALL REBUILD.SCREEN
        END
        ACCT = COMI
        CURR = ACCT[9,2]
        CALL DBR ('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CURR,CUR.DESC)
        R.NEW(COLL.CURRENCY) = CUR.DESC
        CALL REBUILD.SCREEN

        IF COMI # "" THEN
            R.NEW(COLL.APPLICATION.ID)= COMI

            CALL DBR("ACCOUNT":@FM:AC.ONLINE.ACTUAL.BAL,COMI,AMT)
            R.NEW(COLL.NOMINAL.VALUE) = AMT

            CALL DBR("ACCOUNT":@FM:AC.CURRENCY,COMI,CCY)
            R.NEW(COLL.CURRENCY) = CCY

            CALL REBUILD.SCREEN
        END

    END


    RETURN
END
