* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*******************************NI7OOOOOO*************
*-----------------------------------------------------------------------------
* <Rating>581</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AUTH

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY



    FN.SAMP = 'F.SCB.DEPT.SAMPLE1' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)


    T.SEL = "SELECT F.SCB.DEPT.SAMPLE1 WITH @ID EQ ":ID.NEW
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*    IF SELECTED THEN
*        E = '����� ���� �� ���' ; CALL ERR  ; MESSAGE = 'REPEAT '
*    END

    INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)

* R.NEW(DEPT.SAMP.INPUT.NAME.HWALA)<1,1> = INP

    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)

    OVER = R.NEW(DEPT.SAMP.OVERRIDE)
    IF OVER NE '' AND DEPT.CODE NE 99  AND (V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'D') AND R.NEW(DEPT.SAMP.RECORD.STATUS) EQ 'INAO' THEN
**   IF OVER NE '' AND R.NEW(DEPT.SAMP.RECORD.STATUS) EQ 'INAO' AND (V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'D') THEN
        E='��� ����� �������� �� �����'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
    END
    IF ID.NEW THEN
    END ELSE
        IF COMP NE R.NEW(DEPT.SAMP.CO.CODE) THEN
            E='��� ����� ���� ��� �� ���� ������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

* FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
*    FINDSTR 'MAR2' IN OVER SETTING FMS,VMS THEN
*       IF V$FUNCTION EQ 'I' THEN
*          E='���� �� ����� ����� �� �����';CALL ERR;MESSAGE='REPEAT'
*     END
* END
* END

    FINDSTR 'MAR4' IN OVER SETTING FMS,VMS THEN
        IF V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'A' THEN
            E='���� �� �����  �� ������� �� ������ �������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    FINDSTR 'MAR5' IN OVER SETTING FMS,VMS THEN
        IF V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'A' THEN
            E='���� �� �����  �� ������� �� ������ �������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

***############### ADDED BY MAHMOUD 12/6/2012 ###############**********

    IF V$FUNCTION = 'A' THEN
        FN.CUR  = 'FBNK.CURRENCY'  ; F.CUR  = '' ; R.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CASH.C = '10'
        AMT.ALL = 0
        IF R.NEW(DEPT.SAMP.AMT.HWALA) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.HWALA)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.HWALA) * CUR.RATE
        END
        IF R.NEW(DEPT.SAMP.AMT.WH) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.WH)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.WH) * CUR.RATE
        END

        IF R.NEW(DEPT.SAMP.AMT.LG11) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.LG11)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.LG11) * CUR.RATE
        END
        IF R.NEW(DEPT.SAMP.AMT.LG22) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.LG22)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.LG22) * CUR.RATE
        END
        IF R.NEW(DEPT.SAMP.AMT.TEL) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.TEL)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.TEL) * CUR.RATE
        END
        IF R.NEW(DEPT.SAMP.AMT.TF2) NE '' THEN
            TEXT = R.NEW(DEPT.SAMP.AMT.TF2) ; CALL REM
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.TF2)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.TF2) * CUR.RATE
        END
        IF R.NEW(DEPT.SAMP.AMT.BR) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.BR)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.BR) * CUR.RATE
        END
        IF R.NEW(DEPT.SAMP.AMT.AC) NE '' THEN
            CUR.COD = R.NEW(DEPT.SAMP.CURRENCY.AC)
            GOSUB GET.RATE.CUR
            AMT.ALL += R.NEW(DEPT.SAMP.AMT.AC) * CUR.RATE
        END
        IF PGM.VERSION EQ ",HWALA11" AND AMT.ALL GT 250000 AND DEPT.CODE EQ 99 AND ( R.NEW(DEPT.SAMP.TYPE.AMT.HWALA) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.LG11) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.LG22) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.TF1) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.TEL) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.BR) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.AC) EQ '���' ) THEN
            E = '������ ������� �� ��� ������ ������ ���� �� 250000'
        END

        IF PGM.VERSION EQ ",HWALA11.250" AND AMT.ALL LE 250000 AND DEPT.CODE EQ 99 AND ( R.NEW(DEPT.SAMP.TYPE.AMT.HWALA) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.LG11) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.LG22) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.TF1) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.TEL) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.BR) EQ '���' OR R.NEW(DEPT.SAMP.TYPE.AMT.AC) EQ '���' ) THEN
            E = '������ ������� �� ��� ������ ������ ��� �� �� ����� 250000'
        END

    END
    RETURN
*************************************************************************
GET.RATE.CUR:
*------------
    IF CUR.COD NE LCCY THEN
        CALL F.READ(FN.CUR,CUR.COD,R.CUR,F.CUR,ECAA)
        LOCATE CASH.C IN R.CUR<EB.CUR.CURRENCY.MARKET,1> SETTING POS THEN CUR.RATE  = R.CUR<EB.CUR.SELL.RATE,POS> ELSE
            CUR.RATE  = R.CUR<EB.CUR.SELL.RATE,1>
        END
*Line [ 173 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        IF CUR.COD EQ 'JPY' THEN CUR.RATE = CUR.RATE / 100 ELSE NULL
    END ELSE
        CUR.RATE = 1
    END
    RETURN
*************************************************************************
***###############        END OF ADD          ###############**********
END
