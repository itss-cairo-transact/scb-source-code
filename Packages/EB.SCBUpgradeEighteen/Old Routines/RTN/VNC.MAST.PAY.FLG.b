* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
***NESSREEN AHMED 14/07/2019**************
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.MAST.PAY.FLG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MAST.PAY.FLAG

    CO.CODE = ID.COMPANY
    YY.MONTH = TODAY[1,6]
    ID.NEW = CO.CODE :'.':YY.MONTH

    IF V$FUNCTION EQ 'I' THEN
        CALL DBR('SCB.MAST.PAY.FLAG':@FM:PAML.PAYMENT.FLAG,ID.NEW,PAY.FLG)
        IF PAY.FLG EQ 'YES' THEN
            E ='Already Paid'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END ELSE
            CALL DBR('SCB.MAST.PAY.FLAG$NAU':@FM:PAML.PAYMENT.FLAG,ID.NEW,PAY.FLG)
            IF V$FUNCTION NE 'D' THEN
                IF PAY.FLG = 'YES' THEN
                    E ='CANT AMEND UNAUTHORIZED RECORD'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                END ELSE
                    DEPT.CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>

                    DEPT.CODE= FIELD(ID.NEW,'.',1)
                    DEPT.CODE.ID=DEPT.CODE[8,2]

                    IF DEPT.CODE.ID NE DEPT.CODE.USER THEN
                        E ='Not Your Branch'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                    END  ELSE
                        R.NEW(PAML.COMPANY.CO)=CO.CODE
                        R.NEW(PAML.TRANS.DATE)=TODAY
                        R.NEW(PAML.YEAR.MM)= YY.MONTH
                        R.NEW(PAML.PAYMENT.FLAG)='YES'
                    END
                END
            END     ;*IF FUNCTION NE 'D'
        END
    END
    IF V$FUNCTION EQ 'A' THEN
        DEPT.CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>
****UPDATED BY NESSREN AHMED 17/3/2012*************
        DEPT.CODE.REC = R.NEW(PAML.DEPT.CODE)
***   DEPT.CODE= FIELD(ID.NEW,'.',1)
***        DEPT.CODE.ID=DEPT.CODE[8,2]
        DEPT.CODE.ID = DEPT.CODE.REC
****END OF UPDATE 17/3/2012***************************
        IF DEPT.CODE.ID NE DEPT.CODE.USER THEN
            E ='Not Your Branch'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END
*******UPDATED BY NESSREEN AHMED 17/4/2012*************************
    IF V$FUNCTION EQ 'D' THEN
        DEPT.CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>
        DEPT.CODE.REC = R.NEW(PAML.DEPT.CODE)
        DEPT.CODE.ID = DEPT.CODE.REC
        IF DEPT.CODE.ID NE DEPT.CODE.USER THEN
            E ='Not Your Branch'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END   ;*IF FUNCTION EQ 'D'
*******END OF UPDATE 17/4/2012**************************************
    RETURN
END
