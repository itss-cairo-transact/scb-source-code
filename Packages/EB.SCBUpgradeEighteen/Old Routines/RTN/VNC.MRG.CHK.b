* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
** ----- MOHAMED SABRY 28/4/2010 --------------------------
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.MRG.CHK

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    ETEXT = ""

    IF V$FUNCTION = 'I' OR V$FUNCTION = 'A' THEN
        IF ID.NEW[1,3] = '994' THEN
            E = '��� ����� ������ ����� ������'
            CALL ERR ; MESSAGE = 'REPEAT'
        END

        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,ID.NEW,LOCAL.REF)
        WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
* IF (( WS.CREDIT.CODE NE 100 ) OR ( WS.CREDIT.CODE NE 110 ) OR ( WS.CREDIT.CODE EQ 120 )) THEN
**** STOPED BY MOHAMED SABRY 2012/03/05   ********
       * IF  WS.CREDIT.CODE LT 100  THEN
       *     E = '������ ��� �� ���� ���� ������'
       *     CALL ERR ; MESSAGE = 'REPEAT'
       * END
**************************************************

        CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,ID.NEW,CO.BOOK)
        IF  CO.BOOK NE COMP THEN
            E = '��� ������ ��� ���� ���� �����'
            CALL ERR ; MESSAGE = 'REPEAT'
        END

        CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,ID.NEW,POS.RES)
        IF POS.RES GT 89 THEN
            E = '������ ����'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
    END

    RETURN
END
