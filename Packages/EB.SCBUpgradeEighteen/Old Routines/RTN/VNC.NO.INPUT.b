* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
********************************MR.SAIZO*************************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.NO.INPUT


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME

    INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>

    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)
    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)
    CALL DBR('USER':@FM:EB.USE.INIT.APPLICATION.CODE,INP,INIT.APP)


    IF DEPT.CODE NE '99' AND INIT.APP NE '?460' THEN
        T(DEPT.SAMP.DESCRIPTION)<3> = 'NOINPUT'
        T(DEPT.SAMP.TF.DATE)<3> = 'NOINPUT'
        T(DEPT.SAMP.DEPT.NO.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.DEPT.NO.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.FT.TYPE)<3> = 'NOINPUT'
        T(DEPT.SAMP.BRANCH.NO.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.BRANCH.NO.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES1.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES1.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES2.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES2.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.AMT.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.AMT.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.SUP.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.SUP.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.CUS.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.CUS.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.PURPOSE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.PURPOSE.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQUEST.DATE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQUEST.DATE.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.HWALA.MAR)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.WH.MAR)<3> = 'NOINPUT'
        T(DEPT.SAMP.TYPE.AMT.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.TYPE.AMT.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.CURRENCY.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.CURRENCY.WH)<3> = 'NOINPUT'

    END
    IF DEPT.CODE NE '99' AND INIT.APP EQ '?460' THEN
        T(DEPT.SAMP.DESCRIPTION)<3> = 'NOINPUT'
        T(DEPT.SAMP.TF.DATE)<3> = 'NOINPUT'
        T(DEPT.SAMP.DEPT.NO.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.DEPT.NO.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.FT.TYPE)<3> = 'NOINPUT'
        T(DEPT.SAMP.BRANCH.NO.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.BRANCH.NO.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES1.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES1.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES2.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.NAMES2.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.AMT.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.AMT.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.SUP.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.SUP.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.ACC.NAMES2.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.CUS.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.CUS.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.PURPOSE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.PURPOSE.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQUEST.DATE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQUEST.DATE.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REPLAY.DATE.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.REQ.STA.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.HWALA.MAR)<3> = 'NOINPUT'
        T(DEPT.SAMP.NOTES.WH.MAR)<3> = 'NOINPUT'
        T(DEPT.SAMP.TYPE.AMT.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.TYPE.AMT.WH)<3> = 'NOINPUT'
        T(DEPT.SAMP.CURRENCY.HWALA)<3> = 'NOINPUT'
        T(DEPT.SAMP.CURRENCY.WH)<3> = 'NOINPUT'


    END
    IF DEPT.CODE NE '99' AND ( INIT.APP EQ '?421' OR INIT.APP EQ '?422' ) THEN
    END
    RETURN
END
