* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.PWD.CHECK.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*-------------------------------------------
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF (FN.USR, F.USR)
*-------------------------------------------
    IF V$FUNCTION = 'I' THEN
        ETEXT = ''
        CALL DBR('USER':@FM:EB.USE.LOCAL.REF,ID.NEW,DEPT.CO)
        DEPT.CO = DEPT.CO<1,USER.SCB.DEPT.CODE>
        IF DEPT.CO EQ '2700' THEN
*** EDIT BY NESSMA ON 2012/07/03 BY Mr.Bakry Request
*       E = 'NOT ALLOW TO RESET THIS USER'
*       CALL ERR ; MESSAGE = 'REPEAT'
*** END EDIT
        END
*----------------       -----------------
        ETEXT = ''  ; STT = '' ; STT.N = ''
        CALL DBR('PASSWORD.RESET':@FM:EB.PWR.USER.ATTEMPT,ID.NEW,STT)
        IF NOT(ETEXT) THEN
*-- Edit By Nessma Mohammad on 2012/03/25
*-- Based on Mr.Sharef request ---------*
*            E = '�� ����� ��� �������� �� ���'
*            CALL ERR ; MESSAGE = 'REPEAT'
        END
*----------------       -----------------
        ETEXT = ''
        CALL DBR('PASSWORD.RESET$NAU':@FM:EB.PWR.USER.ATTEMPT,ID.NEW,STT.N)
        IF NOT(ETEXT) THEN
            E = '��� ����� ��� �������� ������� �����'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*----------------       -----------------
        ETEXT = ''
        CALL DBR('USER':@FM:EB.USE.USER.NAME,ID.NEW,USER.NAME)
        IF ETEXT THEN
            E = '���� �� ���� ��� ������ ����'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*----------------        --------------
*EDIT BY NESSMA ON 2011/10/10 ---------
        CALL DBR('USER$NAU':@FM:EB.USE.USER.NAME,ID.NEW,USER.NAME)
        IF NOT(ETEXT) THEN
            E = '��� ����� �������� ����'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*----------
        CALL DBR('USER':@FM:EB.USE.END.DATE.PROFILE,ID.NEW,USR.DAT.PRF)
        IF USR.DAT.PRF LT TODAY THEN
            E = '����� ������ �������� ������'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*--------------------------------------
        CALL DBR('USER':@FM:EB.USE.END.TIME,ID.NEW,USR.DAT.PRF)
        IF USR.DAT.PRF LT '1730' THEN
            E = 'TIME'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
    END

    RETURN
END
