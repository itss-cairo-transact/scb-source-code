* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*******************************NI7OOOOOOOOOOOOOOOO****************
    SUBROUTINE VNC.STO.COMM.25

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDING.ORDER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    FN.AC = 'FBNK.ACCOUNT'
    F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER'
    F.CU  = ''
    CALL OPF(FN.CU,F.CU)


    ID = ID.NEW
    XX = FIELD(ID,'.',1)

    TEXT = XX ; CALL REM
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,XX,CUS.ID)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.ID,SEC.NO)
    TEXT = SEC.NO ; CALL REM
    IF (SEC.NO LT 1000 OR SEC.NO GT 1400) THEN
**     TEXT = "INTO IF " ; CALL REM
        FREQ  = COMI
        FREQ2 = FREQ[4]
        FREQ3 = FREQ2[1,2]
        FREQ4 = FREQ2[3,4]
        DAYS = '+':FREQ4:'C'
        FREQ5 = FREQ[1,8]

        IF FREQ2 NE 'SNSS' THEN
            R.NEW(STO.COMM.FREQUENCY) = FREQ
            NN  = FREQ3[1]
            CALL ADD.MONTHS(FREQ5,NN)
            R.NEW(STO.COMM.END.DATE) = FREQ5
        END
        IF FREQ2 EQ 'SNSS' THEN
            R.NEW(STO.COMM.FREQUENCY) = FREQ
            R.NEW(STO.COMM.END.DATE) = FREQ5
        END
    END
    IF (SEC.NO GE 1000 AND SEC.NO LE 1400) THEN
      *  TEXT = "ELSE " ; CALL REM
        R.NEW(STO.COMMISSION.CODE)          = 'WAIVE'
        R.NEW(STO.COMM.FREQUENCY)           = ''
        R.NEW(STO.COMM.END.DATE)            = ''
        R.NEW(STO.COMMISSION.AMT)           = ''
    END

    RETURN
END
