* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.LIMIT.AHMED
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    IF COMI EQ '' THEN
        ETEXT = ' U MUST ENTER LIMIT ' ; CALL STORE.END.ERROR
*        CALL REBUILD.SCREEN
    END

*    IF  COMI[7,1] EQ '0' THEN
*        ETEXT = ' ENTER CORRECT LIMIT ' ; CALL STORE.END.ERROR
*        CALL REBUILD.SCREEN
*    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.TYPE> EQ 'C-Normal Margin Rate' AND COMI[1,4] NE 2510 THEN
        ETEXT = ' LIMIT MUST BE 2510' ; CALL STORE.END.ERROR
*        CALL REBUILD.SCREEN
    END
    IF  R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.TYPE> EQ 'D-Deposit Margin Rate' AND COMI[1,4] NE 2520 THEN
        ETEXT = ' LIMIT MUST BE 2520' ; CALL STORE.END.ERROR
*        CALL REBUILD.SCREEN
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.TYPE> EQ 'N-Fully Coverd' AND COMI[1,4] NE 2505 THEN

        ETEXT = ' LIMIT MUST BE 2505' ; CALL STORE.END.ERROR
        CALL REBUILD.SCREEN
    END
    IF R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.TYPE> EQ 'S-Special Margin Rate' AND COMI[1,4] NE 2505 THEN
        ETEXT = ' LIMIT MUST BE 2505' ; CALL STORE.END.ERROR
        CALL REBUILD.SCREEN
    END
    RETURN
END
