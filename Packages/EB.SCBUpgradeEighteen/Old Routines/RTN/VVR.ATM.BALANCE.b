* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>570</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.BALANCE
*-----------------------------------------------------------------------------
* Modification History:
*
* 3-Oct-2016 - SCBUPG20161002
* In R15 the validation routines are triggered twice, once during
* field validation while the other being at transaction commit.
* Since R15 raises UNAUTH entries during INPUT stage, it's best
* the validations are placed in transaction commit stage. we've
* used CALCULATE.CHARGE to get the total charges per ATM transactions
* to prevent system putting FT in INAO for charges (unauthorized overdraft)
*-----------------------------------------------------------------------------------
*1-IF THE INPUT VALUE IS NOT EQ DEBIT AMOUNT THEN MAKE CREDIT.AMOUNT IS EMPTY
*2-CHECK IF WORKING.BALANCE HAVE A CREDIT BALANCE

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
***** SCB R15 UPG ***** - S
    IF MESSAGE NE "VAL" THEN RETURN
***** SCB R15 UPG ***** - E
    IF R.NEW(FT.TRANSACTION.TYPE) EQ 'AC3B' THEN
        ACCC = R.NEW(FT.DEBIT.THEIR.REF)
    END ELSE
        ACCC = R.NEW(FT.DEBIT.ACCT.NO)
    END

    CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACCC,CCURR)
    IF CCURR EQ '' THEN
        E = 'RESPONS.CODE=76' :",ATM.123.REF.NO=" : R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1, FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID> ;  CALL ERR; MESSAGE='REPEAT'
    END ELSE

        TEXT = "DAREEEE=   "  : R.NEW(FT.DATE.TIME) ; CALL REM
        TEXT =  R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> ;CALL REM

        IF COMI = '' THEN
            E = 'RESPONS.CODE=20' : ",ATM.123.REF.NO=":  R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1, FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>   ; CALL ERR; MESSAGE='REPEAT'
        END

        CU.NO = R.NEW(FT.DEBIT.CUSTOMER)
        IF R.NEW(FT.DEBIT.THEIR.REF)[11,4] = 1535 THEN
            CATEG = R.NEW(FT.DEBIT.THEIR.REF)[11,4]
        END ELSE
            CATEG = R.NEW(FT.DEBIT.ACCT.NO)[11,4]

        END
        ACT = ACCC  ;*R.NEW(FT.DEBIT.ACCT.NO)
        IF CATEG NE 5001 THEN

            AC = ACCC         ;*R.NEW(FT.DEBIT.ACCT.NO)
            IF CATEG EQ 1535 THEN
                AVL = 0
                CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,AC,LIM.REF)
                LIM  = CU.NO :".":"0000":LIM.REF
                CALL DBR('LIMIT':@FM:LI.EXPIRY.DATE,LIM,SCB.EXP.DAT)
                IF SCB.EXP.DAT GT TODAY THEN
                    CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
                END
            END ELSE
                AVL = 0
            END
********************************
*----SCBUPG20161002 - S
            CUST.ID = R.NEW(FT.DEBIT.CUSTOMER)
            DEAL.AMT = R.NEW(FT.DEBIT.AMOUNT)
            DEAL.CCY = R.NEW(FT.DEBIT.CURRENCY)
            CCY.MKT = R.NEW(FT.CURRENCY.MKT.DR)
            FT.CHARGE.CODES = ""
            FT.CHARGE.CODES = R.NEW(50)
            TOT.CHARGE.LCY = "0"
            TOT.CHARGE.FCY = "0"
*------ UPDATE BY BAKRY 20161011 - S
            IF R.NEW(FT.COMMISSION.TYPE) THEN
                FT.CHARGE.CODES<1,-1> = R.NEW(FT.COMMISSION.TYPE)
            END
            CALL CALCULATE.CHARGE(CUST.ID,DEAL.AMT,DEAL.CCY,CCY.MKT,"","","",FT.CHARGE.CODES,"",TOT.CHARGE.LCY,TOT.CHARGE.FCY)
*------ UPDATE BY BAKRY 20161011 - E
*----SCBUPG20161002 - E
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = COMI
            ZERO = "0.00"
            BAL  =  WORK.BALANCE
*--------------UPDATED BY RIHAM 20180711---------------
            CALL DBR('ACCOUNT':@FM:AC.AVAILABLE.BAL,AC,AVL.BALANCE)
*Line [ 118 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AVAL.NO        = DCOUNT(AVL.BALANCE,@VM)
            BAL.AVAL = AVL.BALANCE<1,AVAL.NO>
            IF BAL.AVAL = '' OR BAL.AVAL = 0 THEN BAL.AVAL = BAL
****************************************************************************
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
*Line [ 124 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO = DCOUNT(LOCK.AMT,@VM)
            BLOCK    = LOCK.AMT<1,LOCK.NO>
*  ADD TO FIX ERRORFOR BLOCK ( BAKRY 20120913 )
            IF BLOCK LT 0 THEN BLOCK = 0
*  END OF ADD ( BAKRY 20120913 )
            BAL.BLOCK = BAL - BLOCK
            BAL.BLOCK.AVAL = BAL.AVAL - BLOCK
            IF R.NEW(FT.TRANSACTION.TYPE) EQ 'AC3B' THEN

                NET.BAL = BAL.BLOCK + AVL
                NET.BAL.AVAL = BAL.BLOCK.AVAL + AVL
            END ELSE
                NET.BAL = BAL.BLOCK - AMT  + AVL
                NET.BAL.AVAL = BAL.BLOCK.AVAL - AMT + AVL
            END
*-------SCBUPG20161002 - S
            IF R.NEW(FT.TRANSACTION.TYPE) NE 'AC3A' AND R.NEW(FT.TRANSACTION.TYPE) NE 'AC3B' AND R.NEW(FT.TRANSACTION.TYPE) NE 'AC32' THEN
                NET.BAL = NET.BAL - TOT.CHARGE.LCY
                NET.BAL.AVAL = NET.BAL.AVAL - TOT.CHARGE.LCY
            END
*-------SCBUPG20161002 - E
*---------------------MODIFY REMINING.BAL/20190527----------------
*            CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUST.ID,CUS.SECTOR)
*            IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400'  THEN
*---------------------MODIFY LOAN TYPE 20190701-----------------
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF)
            LOAN = LOC.REF<1,CULR.LOAN.TYPE>
*Line [ 152 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            POS = DCOUNT(LOAN,@SM)
            IF POS GT 0 THEN
                FOR X = 1 TO POS
                    LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>
*                    IF LOAN.TYPE EQ '103' OR LOAN.TYPE EQ '104' OR LOAN.TYPE EQ '105' OR LOAN.TYPE EQ '109' THEN
                    IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                        X = POS
                        IF NET.BAL.AVAL LT NET.BAL THEN
                            R.NEW(FT.LOCAL.REF)<1,FTLR.REMINING.BAL.NO> = NET.BAL.AVAL
                        END ELSE
                            R.NEW(FT.LOCAL.REF)<1,FTLR.REMINING.BAL.NO> = NET.BAL
                        END
                    END ELSE
                        R.NEW(FT.LOCAL.REF)<1,FTLR.REMINING.BAL.NO> = NET.BAL
                    END
                NEXT X
            END ELSE
                R.NEW(FT.LOCAL.REF)<1,FTLR.REMINING.BAL.NO> = NET.BAL
            END

            CALL REBUILD.SCREEN


            IF V$FUNCTION = 'R' THEN
                BAL.REV= NET.BAL + COMI
                E = "REMINING.BAL:1:1=":BAL.REV
            END


            IF NET.BAL LT ZERO THEN
                IF R.NEW(FT.TRANSACTION.TYPE) NE 'AC3B' THEN
                    E = 'RESPONS.CODE=51' : ",ATM.123.REF.NO=":  R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1, FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>   ; CALL ERR; MESSAGE='REPEAT'
                END
            END ELSE
*---------------Check Aval
* IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400'  THEN

                LOAN = LOC.REF<1,CULR.LOAN.TYPE>
*Line [ 191 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                POS = DCOUNT(LOAN,@SM)
                FOR X = 1 TO POS
                    LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>

*                    IF LOAN.TYPE EQ '103' OR LOAN.TYPE EQ '104' OR LOAN.TYPE EQ '105' OR LOAN.TYPE EQ '109' THEN
                    IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                        X = POS
                        IF NET.BAL.AVAL LT ZERO THEN

                            IF R.NEW(FT.TRANSACTION.TYPE) NE 'AC3B' THEN
                                E = 'RESPONS.CODE=51' : ",ATM.123.REF.NO=":  R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1, FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>   ; CALL ERR; MESSAGE='REPEAT'
                            END
                        END
                    END
                NEXT X
*-----------------------------------------
            END
        END
    END
**********************************

    RETURN
END
