* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
**------------ WAEL ---------------*

    SUBROUTINE VNC.MOVE.DATA

* TO DEFAULT THE (COLLATERAL.CODE)
* THAT HAS BEEN TAKEN FROM THE VERSION
* (COLLATERAL.RIGHT,SCB.INP)


*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.CODE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COLL.SCB.COMMON

    IF V$FUNCTION = 'I' THEN

        XX =''
        XX = WS.LINK.DATA<1,15>
        R.NEW(COLL.COLLATERAL.CODE) = XX

        CALL DBR('COLLATERAL.CODE':@FM:COLL.CODE.COLLATERAL.TYPE,XX,COL.TYPE)
        CALL DBR('COLLATERAL.CODE':@FM:COLL.CODE.DESCRIPTION,XX,COL.DESC)
        R.NEW(COLL.COLLATERAL.TYPE) = COL.TYPE
        R.NEW(COLL.DESCRIPTION) = COL.DESC
***************************              ************************************************
*Line [ 49 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LD.NO =DCOUNT(R.NEW(COLL.NOTES),@VM)
        DDD = LD.NO
        FOR I = 1 TO DDD
            R.NEW(COLL.NOTES)<1,I> = ''
        NEXT I

    END

    RETURN
END
