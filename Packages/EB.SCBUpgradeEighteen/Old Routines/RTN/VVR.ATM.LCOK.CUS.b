* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>97</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.LCOK.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
**  $INCLUDE           I_F.SCB.ATM.REC.REJECT

    IF MESSAGE NE "VAL" THEN RETURN
    ETEXT =''
    CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,COMI,CUS.ID)
    CUS = CUS.ID
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,COMI,CCURR)
    IF CCURR EQ "" THEN
        E=',RESPONS.CODE=05':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
    END

    CALL DBR('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,CUS,POST)
    IF POST # "" THEN
        E=',RESPONS.CODE=50':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
    END

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,LOCAL.REF)
    WS.DRMNT.CODE = LOCAL.REF<1,CULR.DRMNT.CODE>

    IF WS.DRMNT.CODE EQ '1' THEN
        E=',RESPONS.CODE=12':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
    END

    RETURN
END
