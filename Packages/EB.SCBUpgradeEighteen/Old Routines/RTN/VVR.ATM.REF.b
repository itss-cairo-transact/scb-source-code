* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.REF

*1-CHECK IF A NEW VALUE IN DEBIT.ACCT.NO THEN MAKE:
*  DEBIT.ACCT.NO   = EMPTY
*  CREDIT.ACCT.NO  = EMPTY
*  CREDIT.CUSTOMER = EMPTY
*2-MAKE CREDIT.CUSTOMER EQ DEBIT.CUSTOMER
*3-CHECK THE SECTOR OF CUSTOMER

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VER.IND.SEC.LEG
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
**    $INCLUDE           I_F.SCB.ATM.REC.REJECT
    ETEXT =''

***** SCB R15 UPG ***** - S
    IF MESSAGE NE 'VAL' THEN RETURN
***** SCB R15 UPG ***** - E

    IF R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> = '' THEN
        E = 'RESPONS.CODE=20' :",ATM.123.REF.NO=" : R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1,FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>   ; CALL ERR ; MESSAGE = 'REPEAT'
**************************************
    END

    RETURN
END
