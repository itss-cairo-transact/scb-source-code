* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BR.DUBLICATE.0017

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*----------------------------------------------------
    FN.BANK = 'F.SCB.BANK.BRANCH'  ;  F.BANK  = ''
    CALL OPF (FN.BANK,F.BANK)

    BB = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR>
    VV = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>

    CALL F.READ(FN.BANK,COMI,R.BANK,F.BANK,E1)
    BANKNO =  R.BANK<SCB.BAB.BANK.NO>

    IF R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP' THEN
        IF BANKNO NE VV THEN
            ETEXT = "��� �� ���� ��� �� ���� �����" ; CALL STORE.END.ERROR
        END
    END


    VV = COMI[5,2]
    IF R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP' THEN
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ 0017 AND COMI[1,4] EQ '1700' THEN
            FN.ACC = 'FBNK.ACCOUNT';F.ACC='';R.ACC = '';E1=''
            CALL OPF(FN.ACC,F.ACC)
            ACCC = "EGP12007000100":VV
            CALL F.READ(FN.ACC,ACCC, R.ACC, F.ACC ,E1)
        END
    END
    IF MESSAGE NE "VAL" THEN
        GOSUB DUBLICATE.CHQ.NAU
        IF FLAG NE 1 THEN GOSUB GET.PAY.PLACE
        RETURN
*---------------------------------
DUBLICATE.CHQ.NAU:
        IF COMI THEN

            T.SEL1= '';KEY.LIST1 = '';SELECTED1 = '';ERR.MSG1 = ''

            CHQ.NO    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO>
            BANK.NO   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>
            BRANCH.NO = COMI

            T.SEL = "SSELECT FBNK.BILL.REGISTER$NAU WITH BILL.CHQ.NO EQ ": CHQ.NO :" AND BANK EQ ": BANK.NO :" AND BANK.BR EQ ": BRANCH.NO
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG1 )
            IF SELECTED THEN
                FLAG = 1
                ETEXT = "����� ���� �� ��� � ��� ����" ; CALL STORE.END.ERROR
            END
        END
        RETURN
*-------------------------------
GET.PAY.PLACE:

        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = ''

        FN.BRANCH = 'F.SCB.BANK.BRANCH';F.BRANCH='';R.BRANCH = '';E=''
        CALL OPF(FN.BRANCH,F.BRANCH)

        CALL F.READ(FN.BRANCH,COMI, R.BRANCH, F.BRANCH ,E)

        IF (E)  THEN
            ETEXT="�� ���� ���� ����� ��" ;RETURN
        END ELSE
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = R.BRANCH<SCB.BAB.LOCATION>
        END

        CALL REBUILD.SCREEN
    END

    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR> # COMI THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
        CALL REBUILD.SCREEN
    END
    RETURN
*--------------------------------
END
