* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.CHG
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
    IF COMI THEN


        IF COMI EQ 'AC3A' OR COMI EQ 'AC3B' OR COMI EQ 'AC32' THEN
            IF COMI EQ 'AC3A' OR COMI EQ 'AC32' THEN
                R.NEW(FT.CHARGES.ACCT.NO) = 'EGP1124300010099'
                DEL R.NEW(FT.CHARGE.TYPE)<1,2>
            END
            IF COMI EQ 'AC3B' THEN
                R.NEW(FT.DEBIT.THEIR.REF) = R.NEW(FT.DEBIT.ACCT.NO)
*                R.NEW(FT.CHARGE.CODE) = 'WAIVE'
                R.NEW(FT.CHARGE.TYPE) = ''
*                R.NEW(FT.CHARGE.CODE) = 'DEBIT PLUS CHARGES'
                R.NEW(FT.DEBIT.ACCT.NO) = 'EGP1124300010099'
*                R.NEW(FT.CHARGES.ACCT.NO) = 'EGP1124300010099'
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
