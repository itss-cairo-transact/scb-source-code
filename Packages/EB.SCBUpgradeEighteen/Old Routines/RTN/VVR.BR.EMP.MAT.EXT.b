* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>68</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.EMP.MAT.EXT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
    $INSERT            I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*------------------------------------------------------
    IF MESSAGE # 'VAL' THEN
        COMP     = ID.COMPANY
        COM.CODE = COMP[8,2]
        TEXT     = ''
        ETEXT    = ''
        FLAG     = 0
        FALGG    = 0
        FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC= ''
        CALL OPF(FN.CU.AC,F.CU.AC)

        FN.ACC ='FBNK.ACCOUNT' ; R.ACC=''; F.ACC=''
        CALL OPF(FN.ACC,F.ACC)


        IF COMI # R.NEW(EB.BILL.REG.DRAWER) THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
            CALL REBUILD.SCREEN ; P = 0
        END

        IF COMI THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT> = ''

            IF COMI EQ '99433300' OR COMI EQ '99499900' THEN

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "����� �����" THEN
                    CATEG   = '9002'
                    GOSUB CHECK.ACC
                END

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "�������� �����" THEN
                    CATEG  = '9003'
                    GOSUB CHECK.ACC
                END

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "� ���� �����" THEN
                    CATEG  =  '9005'
                    GOSUB CHECK.ACC
                END

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "�������� �����" THEN
                    CATEG  = '9006'
                    GOSUB CHECK.ACC
                END
            END ELSE
*-------------------------
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "����� �����" THEN
                    CATEG  = '9002'
                    GOSUB CHECK.ACC1
                END

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "�������� �����" THEN
                    CATEG  = '9003'
                    GOSUB CHECK.ACC1
                END

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "� ���� �����" THEN
                    CATEG  = '9005'
                    GOSUB CHECK.ACC1
                END

                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "�������� �����" THEN
                    CATEG  = '9006'
                    GOSUB CHECK.ACC1
                END

**                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> = "� ����� ���� " THEN
                IF TRIM(R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE>) = "� ����� ����" THEN
                    CATEG  = '9007'
                    GOSUB CHECK.ACC1
                END

                FINDSTR '������' IN R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE> SETTING FMS,VMS THEN
                    CATEG  = '9009'
                    GOSUB CHECK.ACC1
                END

            END

            IF  FLAG NE '1' THEN
                ETEXT='There.Is.No.Contingent.Account'
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT> = ACC
            END

            CALL REBUILD.SCREEN

        END
    END
    RETURN

*-----------------
CHECK.CU.ACC:

    CALL F.READ(FN.CU.AC,COMI,R.CU.AC,F.CU.AC,ERR1)
    CURR    = R.NEW(EB.BILL.REG.CURRENCY)
    LOOP
        REMOVE ACC FROM R.CU.AC  SETTING POS
    WHILE ACC:POS
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CURR    = R.ACC<AC.CURRENCY>
        AC.CATEG   = R.ACC<AC.CATEGORY>
        IF CURR EQ AC.CURR THEN

            IF ( AC.CATEG EQ 1001 OR AC.CATEG EQ 6501 OR AC.CATEG EQ 6502 OR AC.CATEG EQ 6503 OR AC.CATEG EQ 6504 OR AC.CATEG EQ 5010 OR AC.CATEG EQ 5000 OR AC.CATEG EQ 5004 OR AC.CATEG EQ 2000 OR AC.CATEG EQ 2001 OR AC.CATEG EQ 1014 OR AC.CATEG EQ 1223 ) THEN
                FLAGG = 2
                RETURN
            END

        END
    REPEAT

    RETURN
*-----------------
*-----------------
CHECK.ACC:

    CALL F.READ(FN.CU.AC,COMI,R.CU.AC,F.CU.AC,ERR1)
    CURR    = R.NEW(EB.BILL.REG.CURRENCY)
    LOOP
        REMOVE ACC FROM R.CU.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CURR    = R.ACC<AC.CURRENCY>
        AC.CATEG   = R.ACC<AC.CATEGORY>
        COMP.CO    = R.ACC<AC.CO.CODE>
        IF (CURR EQ AC.CURR AND CATEG EQ AC.CATEG AND COMP EQ COMP.CO ) THEN

            FLAG = 1
            RETURN
        END
    REPEAT

    RETURN
*-----------------
CHECK.ACC1:
    CALL F.READ(FN.CU.AC,COMI,R.CU.AC,F.CU.AC,ERR1)
    CURR  = R.NEW(EB.BILL.REG.CURRENCY)
    LOOP
        REMOVE ACC FROM R.CU.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CURR    = R.ACC<AC.CURRENCY>
        AC.CATEG   = R.ACC<AC.CATEGORY>
        COMP.CO    = R.ACC<AC.CO.CODE>
        IF (CURR EQ AC.CURR AND CATEG EQ AC.CATEG AND COMP EQ COMP.CO ) THEN
            FLAG = 1
            RETURN
        END
    REPEAT
    RETURN
*-----------------
END
