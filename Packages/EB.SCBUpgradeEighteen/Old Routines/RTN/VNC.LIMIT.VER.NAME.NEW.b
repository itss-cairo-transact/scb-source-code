* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
********** WAEL ************

* DEFAULT VERSION NAME TO PREVENT
* USER TO OPEN OR AUTHORISE
* FROM ANOTHER VERSION

SUBROUTINE VNC.LIMIT.VER.NAME.NEW

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS


IF V$FUNCTION = 'I' THEN

         IF R.NEW(LI.LOCAL.REF)< 1, LILR.VERSION.NAME> = '' THEN
             R.NEW(LI.LOCAL.REF)< 1, LILR.VERSION.NAME> = PGM.VERSION
         END ELSE

           VERSION.NAME =  R.NEW( LI.LOCAL.REF)< 1, LILR.VERSION.NAME>

*           IF VERSION.NAME # PGM.VERSION:"MOD" THEN
 *              E = 'YOU.MUST.RETREIVE.RECORD.FROM &' : @FM : VERSION.NAME:"MOD"  ;CALL ERR ; MESSAGE = 'REPEAT'
  *            END
          END

END

RETURN
END
