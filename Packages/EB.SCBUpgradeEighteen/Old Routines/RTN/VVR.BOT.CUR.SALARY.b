* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BOT.CUR.SALARY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER

    FN.BOT = 'F.SCB.BOAT.TOTAL' ; F.BOT = ''
    CALL OPF(FN.BOT,F.BOT)

    CALL F.READ(FN.BOT,COMI,R.BOT,F.BOT,E1)
    BOT.ST = R.BOT<BOT.STATUS>
    IF NOT(E1) THEN
        IF BOT.ST EQ '1' THEN
            R.NEW(BO.CURRENCY)    = R.BOT<BOT.CURRENCY>
            R.NEW(BO.SALARY.DATE) = R.BOT<BOT.SALARY.DATE>
        END ELSE
            ETEXT = "�� ����� ������� �� ���"
        END
    END ELSE
        ETEXT = "������ ������� ��� �����"
    END

    CALL REBUILD.SCREEN

    RETURN
END
