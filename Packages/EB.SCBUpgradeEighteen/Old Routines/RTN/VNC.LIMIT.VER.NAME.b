* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
********** WAEL ************

* DEFAULT VERSION NAME TO PREVENT
* USER TO OPEN OR AUTHORISE
* FROM ANOTHER VERSION

    SUBROUTINE VNC.LIMIT.VER.NAME

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP


    IF V$FUNCTION = 'I' OR V$FUNCTION = 'C' THEN
************************************
**** ADD BY MOHAMED SABRY 2010-10-21
************************************

        WS.CUS.ID    = FIELD(ID.NEW,".",1)
        WS.LI.REF.ID = FIELD(ID.NEW,".",2)
        WS.LI.REF.ID = WS.LI.REF.ID + 0
        WS.BANK.CHK = WS.CUS.ID[1,3]
        IF WS.BANK.CHK NE 994 THEN
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.CUS.ID,LOCAL.REF)
            WS.CREDIT.CODE = LOCAL.REF<1,CULR.CREDIT.CODE>
            WS.GROUP.NUM   = LOCAL.REF<1,CULR.GROUP.NUM>
            IF WS.CREDIT.CODE LT 100 THEN
                E = '��� �� ���� ���� ������'
                CALL ERR ; MESSAGE = 'REPEAT'
            END

**** ADD BY MOHAMED SABRY 2011-09-12
************************************

            IF  WS.LI.REF.ID EQ 5100 OR WS.LI.REF.ID EQ 5105 OR WS.LI.REF.ID EQ 5110 OR WS.LI.REF.ID EQ 5120 THEN
                E = '�� ���� ����� ��� ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF  WS.LI.REF.ID EQ 5130 OR WS.LI.REF.ID EQ 5140 OR WS.LI.REF.ID EQ 5150 OR WS.LI.REF.ID EQ 5160 THEN
                E = '�� ���� ����� ��� ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF  WS.LI.REF.ID EQ 5180 OR WS.LI.REF.ID EQ 5190 THEN
                E = '�� ���� ����� ��� ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF  WS.LI.REF.ID EQ 2000 OR WS.LI.REF.ID EQ 2060 OR WS.LI.REF.ID EQ 2010 OR WS.LI.REF.ID EQ 2020 THEN
                E = '�� ���� ����� ��� ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
            IF  WS.LI.REF.ID EQ 2030 OR WS.LI.REF.ID EQ 2040 OR WS.LI.REF.ID EQ 2050 THEN
                E = '�� ���� ����� ��� ����'
                CALL ERR ; MESSAGE = 'REPEAT'
            END

            GOSUB CHK.NAU.GROUP.LI

**************************************
        END
*---------------------------------------

        IF R.NEW(LI.LOCAL.REF)< 1, LILR.VERSION.NAME> = '' THEN
            R.NEW(LI.LOCAL.REF)< 1, LILR.VERSION.NAME> = PGM.VERSION
        END ELSE

            VERSION.NAME =  R.NEW( LI.LOCAL.REF)< 1, LILR.VERSION.NAME>

*           IF VERSION.NAME # PGM.VERSION:"MOD" THEN
*              E = 'YOU.MUST.RETREIVE.RECORD.FROM &' : @FM : VERSION.NAME:"MOD"  ;CALL ERR ; MESSAGE = 'REPEAT'
*            END
        END

    END

    RETURN
**************************************
CHK.NAU.GROUP.LI:
    FN.CG = 'F.SCB.CUSTOMER.GROUP' ; F.CG = '' ; R.CU = '' ; ER.CG = ''
    CALL OPF(FN.CG,F.CG)

    FN.LI = "FBNK.LIMIT$NAU"    ; F.LI = "" ; R.LI = ''  ; ER.LI = ''
    CALL OPF(FN.LI,F.LI)

    CALL F.READ(FN.CG,WS.GROUP.NUM,R.CG,F.CG,ER.CG)
    WS.LI.GROUP.CHK = R.CG<CG.LIMIT.CHK>
    IF WS.LI.GROUP.CHK EQ 'Y' THEN
        WS.CG.CUS.ID   = R.CG<CG.CUSTOMER.ID>
*Line [ 117 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.CG.CUS.C    = DCOUNT(WS.CG.CUS.ID,@VM)
        FOR ICG.CUS    = 1 TO WS.CG.CUS.C
            WS.CG.CUS  = WS.CG.CUS.ID<1,ICG.CUS>
            T.SEL = "SELECT ":FN.LI:" WITH @ID LIKE ":WS.CG.CUS:"... BY @ID"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR Y = 1 TO SELECTED
                    CALL F.READ(FN.LI,KEY.LIST<Y>,R.LI,F.LI,ER.LI)
                    IF FIELD(KEY.LIST<Y>,'.',4) EQ '' THEN
                        WS.LI.PROD  = R.LI<LI.LIMIT.PRODUCT>
                        WS.LI.CY    = R.LI<LI.LIMIT.CURRENCY>
                        WS.PRO.ALL  = R.LI<LI.PRODUCT.ALLOWED>
                        WS.LI.AMT   = R.LI<LI.ONLINE.LIMIT>
                        IF  WS.PRO.ALL NE '' AND WS.LI.PROD NE 2000 AND WS.LI.PROD NE 5100 THEN
                            IF WS.LI.PROD NE 9700 AND WS.LI.PROD NE 9800 AND WS.LI.PROD NE 9900 THEN
                                IF WS.LI.PROD NE 400 AND WS.LI.PROD NE 500 AND WS.LI.PROD NE 600 AND WS.LI.PROD NE 700 THEN
                                    IF  KEY.LIST<Y> NE ID.NEW THEN
                                        E = '��� ����� ���� ���':KEY.LIST<Y>
                                        CALL ERR ; MESSAGE = 'REPEAT'
                                        RETURN
                                    END
                                END
                            END
                        END
                    END
                NEXT Y
            END
        NEXT ICG.CUS
    END

    RETURN
**************************************

END
