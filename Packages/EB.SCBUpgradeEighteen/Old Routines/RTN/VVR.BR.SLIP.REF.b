* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BR.SLIP.REF

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*---------------------------------------------------
    FN.BR.SLIP ='FBNK.SCB.BR.SLIPS' ;R.BR.SLIP ='';F.BR.SLIP =''
    CALL OPF(FN.BR.SLIP,F.BR.SLIP)
    CALL F.READ(FN.BR.SLIP,COMI, R.BR.SLIP, F.BR.SLIP,E)
    IF R.BR.SLIP<SCB.BS.CO.CODE> NE ID.COMPANY THEN ETEXT = "INVALID COMPANY"

    IF MESSAGE NE 'VAL' THEN
        IF PGM.VERSION = ",SCB.CHQ.OUT.OF.CLEARING" OR PGM.VERSION = ",SCB.CHQ.LCY.REG1" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG.CONV" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG1.COLL1" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG1.COLL.RET" OR PGM.VERSION EQ ",SCB.CHQ.LCY.REG.CONV1" OR PGM.VERSION = ",SCB.CHQ.OUT.OF.CLEARING1" OR PGM.VERSION = ",SCB.CHQ.LCY.REG1.EXIST" OR PGM.VERSION = ",SCB.CHQ.LCY.REG.H" THEN
            IF R.BR.SLIP<SCB.BS.OPERATION.TYPE> EQ "�������� �����"  THEN
                ETEXT = "��� ������� �� ���� ����������"
                RETURN
            END
        END
        IF PGM.VERSION = ",SCB.BILLS" OR PGM.VERSION = ",SCB.BILLS.CONV" THEN
            IF R.BR.SLIP<SCB.BS.OPERATION.TYPE> EQ "����� �����" THEN
                ETEXT = "��� ������� �� ���� �������"
                RETURN
            END
        END

        IF R.BR.SLIP<SCB.BS.REG.AMT> EQ 0 AND R.BR.SLIP<SCB.BS.REG.NO> EQ 0 THEN
            ETEXT = "�� ����� ������� �������"
            RETURN
        END ELSE
            R.NEW(EB.BILL.REG.DRAWER)                            = R.BR.SLIP<SCB.BS.DRAWER>
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE>  = R.BR.SLIP<SCB.BS.OPERATION.TYPE>
            R.NEW(EB.BILL.REG.CURRENCY)                          = R.BR.SLIP<SCB.BS.CURRENCY>
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.TOT.AMT>         = R.BR.SLIP<SCB.BS.REG.AMT>
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.NO.CHQ>          = R.BR.SLIP<SCB.BS.REG.NO>
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>        = R.BR.SLIP<SCB.BS.CUST.ACCT>
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.DR.NAME>         = R.BR.SLIP<SCB.BS.NOTES>
            CALL REBUILD.SCREEN
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
