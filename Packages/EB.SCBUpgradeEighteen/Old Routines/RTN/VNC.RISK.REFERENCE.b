* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.RISK.REFERENCE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
* Created by Noha Hamed
* INF.MULTI.TXN
* 12 LC
* 14 DRAWINGS

    FN.TT  = 'FBNK.TELLER'           ; F.TT = '' ; R.TT = ''
    CALL OPF(FN.TT,F.TT)
    FN.FT  = 'FBNK.FUNDS.TRANSFER'   ; F.FT = '' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.LC  = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    FN.DR  = 'FBNK.DRAWINGS'         ; F.DR = '' ; R.DR = ''
    CALL OPF(FN.DR,F.DR)
    FN.IN  = 'F.INF.MULTI.TXN'    ; F.IN = '' ; R.IN = ''
    CALL OPF(FN.IN,F.IN)



    IF MESSAGE = '' THEN

        IF V$FUNCTION EQ 'I' THEN
            REF = COMI
            REF.TYPE = REF[1,2]

            IF REF.TYPE = 'TT' THEN
                FLAG = 'NO'
                CALL DBR('TELLER':@FM:TT.TE.CURRENCY.1,REF,CURR)

                IF CURR EQ '' THEN
                    FLAG   = 'YES'
                    TT.REF = REF:";1"
                    CALL DBR('TELLER$HIS':@FM:TT.TE.CURRENCY.1,TT.REF,CURR)
                END
                IF CURR EQ 'EGP' AND FLAG EQ 'NO' THEN
                    CALL DBR('TELLER':@FM:TT.TE.AMOUNT.LOCAL.1,REF,LOCAL.AMT)
                END
                ELSE IF CURR NE 'EGP' AND CURR NE '' AND FLAG EQ 'NO' THEN
                    CALL DBR('TELLER':@FM:TT.TE.AMOUNT.FCY.1,REF,LOCAL.AMT)
                END
                ELSE IF CURR EQ 'EGP' AND FLAG EQ 'YES' THEN
                    CALL DBR('TELLER$HIS':@FM:TT.TE.AMOUNT.LOCAL.1,TT.REF,LOCAL.AMT)
                END
                ELSE IF CURR NE 'EGP' AND CURR NE '' AND FLAG EQ 'YES' THEN
                    CALL DBR('TELLER$HIS':@FM:TT.TE.AMOUNT.FCY.1,TT.REF,LOCAL.AMT)
                END

                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT) = LOCAL.AMT

            END

            IF REF.TYPE = 'FT' THEN
                CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.CURRENCY,REF,CURR)
***CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.AMOUNT,REF,LOCAL.AMT)
                CALL DBR('FUNDS.TRANSFER':@FM:FT.AMOUNT.DEBITED[4,10],REF,LOCAL.AMT)

                IF CURR EQ '' THEN
                    FT.REF = REF:";1"
                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.CURRENCY,FT.REF,CURR)
***CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.AMOUNT,FT.REF,LOCAL.AMT)
                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.AMOUNT.DEBITED[4,10],FT.REF,LOCAL.AMT)
                END

                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT) = LOCAL.AMT

            END

            IF REF.TYPE = 'TF' THEN
                IF LEN(REF) EQ 12 THEN
                    CALL DBR('LETTER.OF.CREDIT':@FM:TF.LC.LC.CURRENCY,REF,CURR)
                    CALL DBR('LETTER.OF.CREDIT':@FM:TF.LC.LC.AMOUNT,REF,LOCAL.AMT)
                END
                IF LEN(REF) EQ 14 THEN
                    CALL DBR('DRAWINGS':@FM:TF.DR.DRAW.CURRENCY,REF,CURR)
                    CALL DBR('DRAWINGS':@FM:TF.DR.DOCUMENT.AMOUNT,REF,LOCAL.AMT)

                END
                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT)      = LOCAL.AMT

            END

            IF REF.TYPE = 'IN' THEN
                CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.CURRENCY,REF,CURR.M)
                CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.AMOUNT.LCY,REF,LOCAL.AMT.M)
                CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.AMOUNT.FCY,REF,LOCAL.AMT.F.M)

                CURR         = CURR.M<1,1>
                LOCAL.AMT.1  = LOCAL.AMT.M<1,1>
                LOCAL.AMT.2  = LOCAL.AMT.F.M<1,1>

                IF CURR EQ 'EGP' THEN
                    LOCAL.AMT = LOCAL.AMT.1
                END
                ELSE
                    LOCAL.AMT = LOCAL.AMT.2
                END

                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT)      = LOCAL.AMT

            END

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
