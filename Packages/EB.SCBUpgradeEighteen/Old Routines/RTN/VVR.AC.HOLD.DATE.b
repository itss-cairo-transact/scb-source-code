* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>199</Rating>
*-----------------------------------------------------------------------------
** ----- 17.06.2002 RANIA (MRA GROUP) -----

    SUBROUTINE VVR.AC.HOLD.DATE
*A ROUTINE 1- TO DEFAULT HOLD DATE WITH TODAY'S DATE .
*          2- TO UPDATE HOLD.USER WITH THE (SIGN.ON.NAME OF THE USER - DEPT NAME )
*          3- TO UPDATE FROM.DATE AND LOCKED.AMOUNT WITH HOLD.DATE AND HOLD.AMOUNT(CROSS VALIDATION)

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    IF MESSAGE NE 'VAL' THEN

*---- (1)

        IF NOT(COMI) THEN COMI = TODAY
        IF COMI # TODAY AND NOT(R.OLD( AC.LOCAL.REF)<1, ACLR.HOLD.DATE,AS>) THEN
        * ETEXT = "Error.In.Date"
          ETEXT = "��� �� �������"
          CALL STORE.END.ERROR
        END
*---- (2)

*Line [ 51 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        MYCOUNT = DCOUNT(R.NEW( AC.LOCAL.REF)<1, ACLR.HOLD.DATE>,@SM)
        IF MYCOUNT = 0 THEN MYCOUNT = 1

        IF NOT(R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER,AS>) THEN
            DEPT.NAME = ''
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,DEPT.NAME)
            R.NEW( AC.LOCAL.REF)<1,ACLR.HOLD.USER,AS> = R.USER<EB.USE.SIGN.ON.NAME> : '-' : DEPT.NAME
            CALL REBUILD.SCREEN
        END

*---- (3)
    END ELSE

*Line [ 65 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        FOR J = 1 TO DCOUNT(R.NEW( AC.FROM.DATE),@SM)
            DEL R.NEW( AC.FROM.DATE)<1,J>
            DEL R.NEW( AC.LOCKED.AMOUNT)<1,J>
        NEXT J

*Line [ 71 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        FOR I = 1 TO DCOUNT(R.NEW( AC.LOCAL.REF)<1, ACLR.HOLD.DATE>,@SM)
            R.NEW( AC.FROM.DATE)<1,I> = R.NEW (AC.LOCAL.REF)<1, ACLR.HOLD.DATE,I>
            R.NEW( AC.LOCKED.AMOUNT)<1,I> = R.NEW (AC.LOCAL.REF)<1, ACLR.HOLD.AMOUNT,I>
        NEXT I
        CALL REBUILD.SCREEN

    END

    RETURN
END
