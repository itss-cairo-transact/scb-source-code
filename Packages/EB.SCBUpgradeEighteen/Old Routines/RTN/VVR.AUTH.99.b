* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*******************************NI7OOOOOO*************
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AUTH.99

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY



    FN.SAMP = 'F.SCB.DEPT.SAMPLE1' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)


    T.SEL = "SELECT F.SCB.DEPT.SAMPLE1 WITH @ID EQ ":ID.NEW
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        E = '����� ���� �� ���'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
    END

    INPUTTER = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,INPUTTER,INP)

* R.NEW(DEPT.SAMP.INPUT.NAME.HWALA)<1,1> = INP

    CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,DEPT.CODE)

    OVER = R.NEW(DEPT.SAMP.OVERRIDE)
    IF OVER NE '' AND DEPT.CODE EQ 99  AND (V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'D') AND R.NEW(DEPT.SAMP.RECORD.STATUS) EQ 'INAO' THEN
**   IF OVER NE '' AND R.NEW(DEPT.SAMP.RECORD.STATUS) EQ 'INAO' AND (V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'D') THEN
        E='��� ����� �������� �� �����'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
    END
    IF ID.NEW THEN
    END ELSE
        IF COMP NE R.NEW(DEPT.SAMP.CO.CODE) THEN
            E='��� ����� ���� ��� �� ���� ������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

* FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
*    FINDSTR 'MAR2' IN OVER SETTING FMS,VMS THEN
*       IF V$FUNCTION EQ 'I' THEN
*          E='���� �� ����� ����� �� �����';CALL ERR;MESSAGE='REPEAT'
*     END
* END
* END

    FINDSTR 'MARR' IN OVER SETTING FMS,VMS THEN
        IF V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'A' THEN
            E='���� �� �����  �� ������� �� ������ �������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    FINDSTR 'MAR2' IN OVER SETTING FMS,VMS THEN
        IF V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'A' THEN
            E='���� �� �����  �� ������� �� ������ �������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    FINDSTR 'MAR3' IN OVER SETTING FMS,VMS THEN
        IF V$FUNCTION EQ 'I' OR V$FUNCTION EQ 'A' THEN
            E='���� �� �����  �� ������� �� ������ �������'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    RETURN
END
