* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
**---------Noha Hamed---------**

    SUBROUTINE VR.TEL.INP.RTN



*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.SERVICE

    MANDATE.CODE = R.NEW(TEL.MANDATE.CODE)
    ACTIVE.FROM  = R.NEW(TEL.ACTIVE.FROM)
    ACTIVE.TO    = R.NEW(TEL.ACTIVE.TO)

    IF MANDATE.CODE EQ 'C' AND ACTIVE.FROM NE '' THEN
        AF=8
        ETEXT = "You must not enter active date when cancellation"
        CALL STORE.END.ERROR
    END

    IF MANDATE.CODE EQ 'Y' AND ACTIVE.TO NE '' THEN
        AF=9
        ETEXT="You must not enter end date when addition"
        CALL STORE.END.ERROR
    END
    IF MANDATE.CODE EQ 'C' AND ACTIVE.TO EQ '' THEN
        AF=9
        ETEXT = "You must enter end date when cancellation"
        CALL STORE.END.ERROR
    END

    IF MANDATE.CODE EQ 'Y' AND ACTIVE.FROM  EQ '' THEN
        AF=8
        ETEXT="You must not enter active date when addition"
        CALL STORE.END.ERROR
    END

    RETURN
END
