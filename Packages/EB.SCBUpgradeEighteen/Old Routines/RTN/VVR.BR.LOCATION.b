* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.LOCATION

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK


    IF COMI THEN
***********************************************
* IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 9 THEN
*     RETURN
* END
***********************************************
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = ''
        T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        T.SEL = "SSELECT F.SCB.BANK.BRANCH WITH @ID EQ ": COMI
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
        IF NOT(SELECTED) THEN
            ETEXT="�� ���� ���� ����� ��" ;RETURN
        END ELSE
            FN.BRANCH = 'F.SCB.BANK.BRANCH';F.BRANCH='';R.BRANCH = '';E=''
            CALL OPF(FN.BRANCH,F.BRANCH)
            CALL F.READ(FN.BRANCH,KEY.LIST, R.BRANCH, F.BRANCH ,E)
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = R.BRANCH<SCB.BAB.LOCATION>
        END
        CALL REBUILD.SCREEN
    END
    RETURN
END
