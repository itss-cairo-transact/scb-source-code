* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-------------------------------------------------------------------------
* Create By Nessma Mohammad
*-------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.AC.BONUS

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT        I_F.SCB.AC.BONUS.BNK
*--------------------------------------
    IF V$FUNCTION = "I" THEN
*** R.NEW(BONUS.ACCOUNT.NUMBER) = ID.NEW
        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ID.NEW,ACCT.NAME)
        R.NEW(BONUS.ACCOUNT.NAME)   = ACCT.NAME
    END
*--------------------------------------
    RETURN
END
