* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.MM.PLACE1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

    E = ''

    IF V$FUNCTION = 'I' THEN

        CALL DBR('MM.MONEY.MARKET':@FM:1,ID.NEW,CHK.ID)
        CALL DBR('MM.MONEY.MARKET':@FM:MM.CATEGORY,ID.NEW,CATEG)
        IF NOT(CHK.ID) OR (CATEG NE 21075 AND CATEG NE 21076 AND CATEG NE 21078) THEN
            E = 'THE ID MUST BE EXIST OR PLACEMENT'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
***        R.NEW(MM.MATURITY.DATE) = TODAY
    END

    RETURN
END
