* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
****NESSREEN AHMED 12/4/2012***********************************************
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.VISA.IGNORE.PAY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.MNTLY.PAY.AMT

    CO.CODE = ID.COMPANY
    YY.MONTH = TODAY[1,6]

    TEXT = 'USR.CO=' :CO.CODE  ; CALL REM
    TEXT = 'TODAY='  :YY.MONTH ; CALL REM

    FN.MNT.PAY = 'F.SCB.VISA.MNTLY.PAY.AMT' ; F.MNT.PAY = '' ; R.MNT.PAY = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.MNT.PAY,F.MNT.PAY)
    CALL F.READ(FN.MNT.PAY, ID.NEW , R.MNT.PAY, F.MNT.PAY, E1)

    REC.CO.CODE  = R.MNT.PAY<MPA.COMPANY.CO>
    REC.YY.MONTH = R.MNT.PAY<MPA.YEAR.MONTH>

    TEXT = 'REC.CO':REC.CO.CODE   ; CALL REM
    TEXT = 'REC.DAT':REC.YY.MONTH  ; CALL REM

    IF V$FUNCTION EQ 'I' THEN
        IF CO.CODE # REC.CO.CODE THEN
            E ='�� ���� ������� ����� ��� ���'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
        IF YY.MONTH # REC.YY.MONTH THEN
            E ='�� ���� ������� ��� ��� ����'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END
    IF V$FUNCTION EQ 'A' THEN
        IF CO.CODE # REC.CO.CODE THEN
            E ='�� ���� ������� ����� ��� ���'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END

    RETURN
END
