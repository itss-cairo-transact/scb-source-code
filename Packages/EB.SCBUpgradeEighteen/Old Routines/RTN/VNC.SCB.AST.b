* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>283</Rating>
*-----------------------------------------------------------------------------
** ----- 17.6.2002 (MRA GROUP) -----

    SUBROUTINE VNC.SCB.AST
*OLD NAME :VNC.AC.CUST.DEFAULTS.SAVING

*ROUTINE 1- TO CHECK THAT THIS RECORD IS RETREIVED FROM SAME VERSION IT IS INPUTTED FROM,ALSO
*           TO CHECK FOR EXISTING ACCOUNTS THAT BELONG TO THE SAME BRANCH AND THAT IT IS NOT
*           A CLOSURE ACCOUNT.                                                         ...MOHAMED...
*
*        2- TO CHECK THAT THE ID HAVE NUMERIC CHARACTERS ONLY.                        ... RANIA ...
*        3- TO DEFAULT LIMIT.REF & LINK TO LIMIT ACCORDING TO DEBIT ACCOUNTS.         ... RANIA ...
*        4- TO DEFAULT DEBIT.CREDIT WITH CREDIT.                                      ... ABEER ...
*        5- TO DEFAULT THE PASSBOOK FIELD TO 'YES' WITH ACCOUNT TYPE = SAVING.        ... ABEER ...


*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*----- (1)
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'A' THEN

        VERSION.NAME =  R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> ; ETEXT = ''
        CALL DBR( 'ACCOUNT$NAU':@FM:AC.LOCAL.REF, ID.NEW, LOCAL.REF)
        VER.NAME = LOCAL.REF<1, ACLR.VERSION.NAME>
************UPDATED BY RIHAM R15************************

*        CALL DBR( 'ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ID.NEW, ACC.OFF)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ID.NEW,COMP.BOOK)
        CUS.BR = COMP.BOOK[8,2]
        AC.OFICER = TRIM(CUS.BR, "0" , "L")

        IF NOT(ETEXT) THEN
*            IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN

            IF AC.OFICER NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
*   E='Only.Function.See.Is.Allowed'
                E='��� ����� ��������';CALL ERR;MESSAGE='REPEAT'
                V$FUNCTION = 'S'
            END
        END
*          IF NOT(ETEXT) AND VERSION.NAME # PGM.VERSION THEN
*          E = 'You.Must.Retreive.Record.From &' : @FM : VERSION.NAME
*          GOSUB MYERROR
*          END

** IF R.NEW(AC.ACCOUNT.OFFICER) AND R.NEW(AC.ACCOUNT.OFFICER) NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
**         E = 'This.Account.From.Other.Branch' ; GOSUB MYERROR
**     END

        IF  R.NEW(AC.POSTING.RESTRICT) GE '90' THEN
            E = 'This.Is.Closure.Account'
            E = '��� ������ ����' ; GOSUB MYERROR
        END

    END

*------------------------
    IF V$FUNCTION = 'I' THEN

        CATEGORY = ID.NEW [11,4]

*------(2)
* E = 'Bad.Account.Number'
        IF NOT(NUM(ID.NEW)) THEN E = '��� �� ��� ������' ; GOSUB MYERROR

*----- (3)
****************29-10-2004
*IF ( CATEGORY > 6000 AND CATEGORY < 6599 ) THEN
        IF PGM.VERSION EQ ",SCB.AST1" OR PGM.VERSION EQ ",SCB.AST1.UPDATE" OR PGM.VERSION EQ ",SCB.AST2" OR PGM.VERSION EQ ",SCB.AST2.UPDATE"  OR PGM.VERSION EQ ",STAFF.LOAN" OR PGM.VERSION EQ ",SCB.AST3" OR PGM.VERSION EQ ",SCB.AST3.UPDATE" THEN
            R.NEW (AC.LIMIT.REF) = ''
            R.NEW (AC.LINK.TO.LIMIT) = 'NO'

        END ELSE

*E= 'category not in range 6000 to 6599';CALL ERR;MESSAGE='REPEAT'
            E= 'ERROR IN VERSION NAME';CALL ERR;MESSAGE='REPEAT'

        END

*----- (4)

        IF NOT (R.NEW( AC.LOCAL.REF)< 1, ACLR.DEBIT.CREDIT>) THEN
            R.NEW( AC.LOCAL.REF)< 1, ACLR.DEBIT.CREDIT> = 'C'
        END

*----- (5)
****************29-10-2004
*IF R.NEW(AC.CATEGORY)
        IF ( CATEGORY > 6000 AND CATEGORY < 6599 ) THEN R.NEW(AC.PASSBOOK) = 'NO'
        ELSE  R.NEW(AC.PASSBOOK) = 'NO'

*-----

    END

    RETURN

MYERROR:
    CALL ERR ; MESSAGE = 'REPEAT'

    RETURN


*-----
END
