* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.ACC
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID

***** SCB R15 UPG ***** - S
    IF MESSAGE NE 'VAL' THEN RETURN
***** SCB R15 UPG ***** - E

*   IF R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>[1,4] = "SCAN" AND R.NEW(FT.CHARGE.CODE) = "WAIVE" THEN
    IF R.NEW(FT.CHARGE.CODE) = "WAIVE" THEN
        COMI = ''
        CALL REBUILD.SCREEN
        TERM.ID = R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>
*Line [ 40 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        CALL DBR("SCB.ATM.TERMINAL.ID":@FM:SCB.ATM.ACCOUNT.NUMBER,TERM.ID,TERM.ACC)
        COMI = TERM.ACC
    END

*    IF R.NEW(FT.TRANSACTION.TYPE) EQ 'AC33' OR R.NEW(FT.TRANSACTION.TYPE) EQ 'AC36' OR R.NEW(FT.TRANSACTION.TYPE) EQ 'AC31' THEN
    IF R.NEW(FT.TRANSACTION.TYPE) EQ 'AC36' OR R.NEW(FT.TRANSACTION.TYPE) EQ 'AC31' THEN
        COMI = "EGP1131300010099"
    END
***** S- UPD BAKRY 20161117 ********
    IF R.NEW(FT.TRANSACTION.TYPE) EQ 'AC33' THEN
        COMI = "EGP1611500010099"
    END
***** E- UPD BAKRY 20161117 ********


    RETURN
END
