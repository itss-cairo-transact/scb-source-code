* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.MARGIN.RATE
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*------------------------------------------------------------------------
    RECORD.N  = ''
    RECORD.N  = ID.NEW
    RECORD.N  = RECORD.N[3,3]
    GROUP.ID  = ID.NEW[1,2]

    IF GROUP.ID EQ 55 OR GROUP.ID EQ 56 THEN
        IF RECORD.N NE 'EGP' THEN
            R.NEW(IC.GCI.CR.MARGIN.RATE) = 0.125
            R.NEW(IC.GCI.CR.MARGIN.OPER)     = "ADD"
        END
    END ELSE
        IF RECORD.N EQ 'EGP' THEN
*            R.NEW(IC.GCI.CR.MARGIN.RATE) = 2
        END ELSE
            R.NEW(IC.GCI.CR.MARGIN.RATE) = 0.125
            R.NEW(IC.GCI.CR.MARGIN.OPER)     = "ADD"
        END
    END

    R.NEW(IC.GCI.CR.MIN.BAL.ST.DTE)  = 3
    R.NEW(IC.GCI.INTEREST.DAY.BASIS) = "E"
    R.NEW(IC.GCI.CR.ACCR.OPEN.AC) = "NO"

    RETURN
END
