* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE

*** CREATE BY NESSMA 2012/04/03
    SUBROUTINE VNC.LOCK.CODE

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT        I_CU.LOCAL.REFS
*-----------------------------------------------
    IF R.NEW(EB.CUS.POSTING.RESTRICT) EQ '' THEN
        R.NEW(EB.CUS.POSTING.RESTRICT) = '1'
    END ELSE
        E   = "�� ����  �������"
        CALL ERR
        MESSAGE = 'REPEAT'
        ETEXT   = "�� ���� ������� "
        CALL STORE.END.ERROR
    END
*-----------------------------------------------
    RETURN
END
