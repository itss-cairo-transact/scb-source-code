* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
***************************NI7OOOOOOOOOOOOOOOO********************
    SUBROUTINE VVR.AMT.LD

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.TYPE = 'F.SCB.LD.TYPE.LEVEL'        ; F.TYPE = ''
    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.TYPE,F.TYPE)

    IF V$FUNCTION = 'I' THEN
* ID = ID.NEW
* CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.AMOUNT,ID,AMTLD)

        AMTLD  = R.NEW(LD.AMOUNT)
        AMTINC = R.NEW(LD.AMOUNT.INCREASE)
        CATEG  = R.NEW(LD.CATEGORY)
        CUR    = R.NEW(LD.CURRENCY)
        VAL    = R.NEW(LD.VALUE.DATE)
        IF CATEG EQ 21001 AND CUR EQ 'EGP' THEN
*          TEXT = "IF NO 1 " ; CALL REM
            IDLEVEL = CATEG : VAL
*          TEXT = "IDLEVEL " : IDLEVEL ; CALL REM
            CALL F.READ(FN.TYPE,IDLEVEL,R.TYPE,F.TYPE,E2)
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CURTYPE = R.TYPE<LDTL.CURRENCY,@VM>
*Line [ 49 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR IN CURTYPE  SETTING POS11 ELSE NULL
            AMTTYPE = R.TYPE<LDTL.AMT.FROM,POS11>
*            TEXT = AMTTYPE ; CALL  REM
            IF AMTLD NE AMTTYPE THEN
*TEXT = AMTLD ; CALL REM
*TEXT = AMTTYPE ; CALL REM

                E = 'U CANT UPDAT' ;CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
        RETURN
    END
