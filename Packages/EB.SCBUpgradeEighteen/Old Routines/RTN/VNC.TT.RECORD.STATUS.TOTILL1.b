* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
**************DINA-SCB*************

    SUBROUTINE VNC.TT.RECORD.STATUS.TOTILL1
*the routine make sure that the teller.2 in the transaction the
*one who make authorisation
*else error message will appear
*only teller1 who allowed to make any another function
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID

** FN.TT.ID  = 'FBNK.TELLER.USER' ; F.TT.ID = '' ; R.TT.ID = ''
** CALL OPF(FN.TT.ID,F.TT.ID)
** CALL F.READ( FN.TT.ID,OPERATOR, R.TT.ID,F.TT.ID, ETEXT1)

    T.SEL  = "SELECT FBNK.TELLER.ID WITH USER EQ ":OPERATOR :" AND STATUS EQ OPEN "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ERR1)
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    TEL.USR = KEY.LIST<1>
    CALL DBR ('TELLER$NAU':@FM:TT.TE.RECORD.STATUS,ID.NEW,STAT)
    IF NOT(ETEXT) THEN
        IF V$FUNCTION = 'A' THEN
            IF STAT = 'RNAU' OR STAT = 'INAU' THEN
**  IF R.NEW(TT.TE.TELLER.ID.2) # TEL.ID THEN
                IF R.NEW(TT.TE.TELLER.ID.2) # TEL.USR THEN
                    E = '��� ����� ��������'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                END
            END
        END
    END
    IF V$FUNCTION # 'A' THEN
**  IF R.NEW(TT.TE.TELLER.ID.1) # TEL.USR[3,2] # 98 THEN
        IF TEL.USR[3,2] # 98 THEN
            E = '����� ��� ������'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END


    RETURN
END
