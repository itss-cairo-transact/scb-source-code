* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
***** 13/6/2002 MONTASER & DINA & INGY SCB *****
*A ROUTINE TO CHECK IF THE ACCOUNT TO BE CLOSED IS LINKED TO ANOTHER MODULE (PD,MM,LD,SC)
*AND CHECK IF THE ACCOUNT HAS UNCLEARED ENTRIES
*OR BLOCKED (HOLD) AMOUNT

    SUBROUTINE VNC.SCB.AC.CLOSURE.CHECK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EMP.INT.2.PERM
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.EXPIRY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.EXPIRY
*----------------------------------------------------
    GOSUB INTIAL
    GOSUB SELECT.INITIAL
    GOSUB CHECK.INT.ACCT
    GOSUB UNAUTHOR.ACT
    GOSUB CHECK.DEPARTMENT.CODE

*--- EDIT BY NESSMA 2013/05/29
    GOSUB CHECK.SAFEKEEP
    GOSUB CHECK.WH.EXPIRY
*-----------------------------
* EDIT BY MAHMOUD MAGDY 2018/05/20
    GOSUB CHECK.BILL.REG
*-----------------------------
*MSABRY 2011/04/20
*GOSUB PD.PAYMENT
*-----------------------
*GOSUB SECURITY
    GOSUB ACCT
    GOSUB CONTRACT
    GOSUB MSG
    GOTO END.RTN
**************************************************************
CHECK.BILL.REG:
    ACCT.NO = ID.NEW   ; BR.SELECTED  = ""
    FN.BR = "FBNK.BILL.REGISTER" ; F.BR = ""
    CALL OPF(FN.BR, F.BR)
    B.SEL  = "SELECT ":FN.BR:" WITH ( LIQ.ACCT EQ ":ACCT.NO
    B.SEL := " OR CONT.ACCT EQ ":ACCT.NO
    B.SEL := " OR CUST.ACCT EQ ":ACCT.NO
    B.SEL := " ) AND BILL.CHQ.STA IN (1 2 3 4 5 6 16 18) "
    CALL EB.READLIST(B.SEL,B.KEY.LIST,"",BR.SELECTED,B.ER.MSG)

    IF BR.SELECTED THEN
        E = "��� ������ ������ �� ��� ":"  ":BR.SELECTED:"  ":" �����/�������� �������"
*        E = '��� ����� �������'
        CALL ERR ;  MESSAGE ='REPEAT'
    END

    RETURN
**************************************************************
CHECK.SAFEKEEP:
*--------------
    IF V$FUNCTION = 'I' THEN
        FN.SF = 'F.SCB.SF.EXPIRY'  ; F.SF = ''
        CALL OPF(FN.SF, F.SF)

        ACCT  = ID.NEW
        T.SEL = 'SELECT ':FN.SF:' WITH ACCOUNT.NO EQ ': ACCT
        KEY.LIST="" ; SELECTED="";  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.SF,KEY.LIST<I>,R.SF,F.SF,ER.SF)
                IF R.SF<SF.EXP.SAFF.USED> EQ 'NO' THEN
                    E = '��� ������ ������ �� ����� ����� �������'
                    CALL ERR ; MESSAGE ='REPEAT'
                END
            NEXT I
        END
*-----------
        T.SEL = 'SELECT ':FN.SF:' WITH @ID LIKE ': ACCT:'...'
        KEY.LIST="" ; SELECTED="";  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.SF,KEY.LIST<I>,R.SF,F.SF,ER.SF)
                IF R.SF<SF.EXP.SAFF.USED> EQ 'NO' THEN
                    E = '��� ������ ������ �� ����� ����� �������'
                    CALL ERR ; MESSAGE ='REPEAT'
                END
            NEXT I
        END
    END
    RETURN
*----------------------------------------------------------------------
CHECK.WH.EXPIRY:
*--------------
    IF V$FUNCTION = 'I' THEN
        FN.WH = 'F.SCB.WH.EXPIRY'  ; F.WH = ''
        CALL OPF(FN.WH, F.WH)

        ACCT  = ID.NEW
        T.SEL = "SELECT ":FN.WH:" WITH DEBIT.ACCT EQ ": ACCT :" AND EXP.FLAG EQ '' "
        KEY.LIST="" ; SELECTED="";  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED.WH,ER.MSG)
        IF SELECTED.WH THEN
            E = "��� ������ ������ �� ������� ���������"
            CALL ERR ; MESSAGE ='REPEAT'
        END
    END
    RETURN
*----------------------------------------------------------------------
SELECT.INITIAL:
*--------------
    KEY.LIST = '';SELECTED = '';ERR.MSG = ''
    RETURN
*----------------------------------------------------------------------
INTIAL:
*------
    MES.LD = '';MES.MM = '';MES.PD = '';MES.SC = '';MES.HLD = '';MES.UNCL = ''
    KEY.LIST = '';SELECTED = '';ERR.MSG ='';E='';MESSAGE=''; DEPT='';DEPT.NAME=''
    POST='';ACT.BAL='';CLEAR.BAL='';UNCLEAR = '';HOLD1='';NATH='';INT.ACCT ='';CLOSED=''
    FN.ACCOUNT = 'FBNK.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = ''

*MSABRY 2013/02/01
*------------------
    FN.PERM = "F.SCB.EMP.INT.2.PERM" ; F.PERM = '' ; R.PERM = ''
    CALL OPF (FN.PERM,F.PERM)
    RETURN
**********************************************************************************************
UNAUTHOR.ACT:
    IF V$FUNCTION = 'I' THEN
        R.NEW( AC.ACL.POSTING.RESTRICT) = '90'
        CALL DBR( 'ACCOUNT$NAU':@FM:AC.MNEMONIC, ID.NEW , NATH)
        IF NOT(ETEXT) THEN NATH = 'Y'
    END
    RETURN
**********************************************************************************************
CHECK.INT.ACCT:
    IF NOT(NUM(ID.NEW)) THEN INT.ACCT='Y'

    RETURN

**********************************************************************************************
CHECK.DEPARTMENT.CODE:

    IF V$FUNCTION = 'I' THEN
*MSABRY 2013/02/01
*------------------
        CALL F.READ(FN.PERM,ID.NEW,R.PERM,F.PERM,ER.PERM)
        IF NOT(ER.PERM) THEN
*Line [ 179 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.COUNT.INT = DCOUNT(R.PERM<EMP.PERM.INT>,@VM)
            WS.TOT.AC.INT = 0
            IF WS.COUNT.INT GE 1 THEN
                FOR I.COUNT = 1 TO WS.COUNT.INT
                    WS.TOT.AC.INT +=  R.PERM<EMP.PERM.INT><1,I.COUNT>
                NEXT I.COUNT

                IF WS.TOT.AC.INT GT 0 THEN
                    E = "����� ���� ������� ���� ������� ��������"
                    CALL ERR ; MESSAGE = 'REPEAT'
                END
            END
        END

*END OF UPDATE MSABRY ------------------
        CALL DBR('ACCOUNT.CLOSURE':@FM:AC.ACL.POSTING.RESTRICT,ID.NEW,POST)
        IF POST  THEN CLOSED = 'Y'
        DEPT = TRIM( ID.NEW[1,2], '0', 'L')       ;* remove leading zeros
        IF DEPT NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
            DEPART.I = 'Y'
        END
    END
    IF V$FUNCTION = 'A' THEN
        DEPT = TRIM( ID.NEW[1,2], '0', 'L')       ;* remove leading zeros
        IF DEPT NE R.USER<EB.USE.DEPARTMENT.CODE> THEN
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
            DEPART.A = 'Y'
        END
    END
    RETURN
**********************************************************************************************
*MSABRY 2011/04/20
*PD.PAYMENT:
*    T.SEL = 'SELECT FBNK.PD.PAYMENT.DUE WITH ORIG.STLMNT.ACT EQ ': ID.NEW
*    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
*    IF KEY.LIST THEN
*        MES.PD = 'PD,'
*  MES.PD = ','
*        GOSUB SELECT.INITIAL
*    END
*    RETURN
***********************************************************************************************
*SECURITY:
*CALL DBR('SC':@FM:SC.REF.ACCOUNT,ID.NEW,SC.ACCT)
*IF SC.ACCT THEN
*MES.SC = 'SC,'
*END
*RETURN

***********************************************************************************************
ACCT:
    CALL OPF( FN.ACCOUNT,F.ACCOUNT)
    CALL F.READ( FN.ACCOUNT, ID.NEW, R.ACCOUNT, F.ACCOUNT, ETEXT)
    IF ETEXT THEN ETEXT = '' ; E = 'ERROR READING ACCOUNT (&)' : @FM :ID.NEW
    ELSE
        ACT.BAL   = R.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
        CLEAR.BAL = R.ACCOUNT<AC.ONLINE.CLEARED.BAL>
        UNCLEAR   = ACT.BAL - CLEAR.BAL
        HOLD1     = R.ACCOUNT<AC.LOCAL.REF,6,1>

        IF  UNCLEAR NE 0  THEN
            MES.UNCL = '���� ����' : UNCLEAR
        END
        IF HOLD1 THEN
            MESG.HLD = '���� ���� ,'
        END
    END
    CLOSE F.ACCOUNT
    RETURN
***********************************************************************************************
CONTRACT:

    T.SEL = 'SELECT FBNK.MM.MONEY.MARKET WITH DRAWDOWN.ACCOUNT EQ ': ID.NEW
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
    IF KEY.LIST THEN
        MES.MM = 'CONTRACT MM,'
        GOSUB SELECT.INITIAL
    END

*-- EDIT BY NESSMA 2016/03/16
    KEY.LIST.LD = '';SELECTED.LD = '';ERR.MSG116=''
    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH"
    T.SEL := " DRAWDOWN.ACCOUNT EQ " : ID.NEW
    T.SEL := " OR PRIN.LIQ.ACCT EQ " : ID.NEW
    T.SEL := " OR BENEF.PRIN.ACCT EQ " : ID.NEW
    T.SEL := " OR INT.LIQ.ACCT EQ " : ID.NEW
    T.SEL := " OR BEN.INT.ACCT EQ " : ID.NEW
    T.SEL := " OR COM.LIQ.ACCT EQ " : ID.NEW
    T.SEL := " OR CHRG.LIQ.ACCT EQ " : ID.NEW
    T.SEL := " OR PRINC.DIS.ACCT EQ " : ID.NEW
    T.SEL := " OR FEE.PAY.ACCOUNT EQ " : ID.NEW
    T.SEL := " OR DEBIT.ACCT EQ " : ID.NEW
    T.SEL := " OR CREDIT.ACCT EQ " : ID.NEW
    T.SEL := " OR CR.NOSTRO.ACCT EQ " : ID.NEW
    T.SEL := " OR OLD.ACCT EQ " : ID.NEW
    T.SEL := " OR OLD.ACCT.NO EQ " : ID.NEW
    T.SEL := " OR ACCT.STMP EQ " : ID.NEW
    T.SEL := " OR ACCT.ANOTHER.BN EQ " : ID.NEW

    CALL EB.READLIST(T.SEL,KEY.LIST.LD,"",SELECTED.LD,ERR.MSG116)
******MODIFIED BY ABEER 05-03-2017
    FN.LD = 'F.LD.LOANS.AND.DEPOSITS'  ; F.LD = ''
    CALL OPF(FN.LD, F.LD)

    IF SELECTED.LD THEN
        FOR JJ = 1 TO SELECTED.LD
            CALL F.READ(FN.LD,KEY.LIST.LD<JJ>,R.LD,F.LD,ER.LD)
            IF R.LD<LD.STATUS> NE 'LIQ' THEN
                MES.LD = 'CONTRACT LD,'
                GOSUB SELECT.INITIAL
            END
        NEXT JJ
    END
****END OF MODIFICATION
    RETURN
***********************************************************************************************
MSG:
    IF DEPART.I THEN
        RETURN
    END
    IF MES.HLD OR MES.UNCL OR MES.PD OR MES.MM OR MES.LD THEN
        E = '������ ��� ����':MES.HLD :MES.UNCL :MES.PD :MES.MM :MES.LD
        GOSUB ER
        RETURN
    END
    IF NATH THEN
        E =''
        GOSUB ER
        RETURN
    END
    IF INT.ACCT THEN
        E = '��� ����� �������� ���������'
        GOSUB ER
        RETURN
    END
    IF CLOSED THEN
        E = '������ �� ����'
        GOSUB ER
        RETURN
    END
    IF DEPART.A THEN
        RETURN
    END
    RETURN
***********************************************************************************************
ER:
    CALL ERR
    MESSAGE ='REPEAT'
    GOTO END.RTN
    RETURN
************************************************************************************************
END.RTN:
    RETURN
************************************************************************************************
END
