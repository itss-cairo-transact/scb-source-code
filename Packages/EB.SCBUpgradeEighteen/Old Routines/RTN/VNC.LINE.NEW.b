* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LINE.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE

**    ID.DESC  = FIELD(R.NEW(RE.SRL.DESC)<1,1>,'-',2)
    ID.DESC1 = FIELD(ID.NEW,'.',2)
    R.NEW(RE.SRL.DESC)<1,1>  = ID.DESC1

*    R.NEW(RE.SRL.DESC)<1,2>  = ''
*    R.NEW(RE.SRL.PROFT.CURRENCY)= ''
***    R.NEW(RE.SRL.DESC)<1,1> = ID.DESC1:"-":ID.DESC
**    R.NEW(RE.SRL.TOTAL.ACCUM) = '1'
***    R.NEW(RE.SRL.DESC)<1,2> = ID.DESC1:"-":ID.DESC
****       R.NEW(RE.SRL.DESC)<1,1> = FIELD(R.NEW(RE.SRL.DESC)<1,1>,'-',1) : "-" : FIELD(R.NEW(RE.SRL.DESC)<1,1>,'-',2)
    CALL REBUILD.SCREEN
    RETURN
END
