* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>952</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.WH.ITEMS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS.SERIAL
*------------------------------------------------
    IF V$FUNCTION = "R" THEN
        E = "�� ���� �������"
        CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
    END
*------------------------------------------------
    CALL DBR('SCB.WH.ITEMS.SERIAL':@FM:SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16],SER.ID)
    IF V$FUNCTION = "I" THEN
*---- 2011/03/01 BY NESSMA ----
        CALL DBR('SCB.WH.ITEMS':@FM:0,ID.NEW,E.SER)
        IF E.SER NE '' THEN
            E = "�� ���� ������� �� ��� ������"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
        END
*---
        MY.SER = FIELD(ID.NEW,'.',2)
        MY.SER = TRIM(MY.SER , "0", "L")

        IF MY.SER GT SER.ID THEN

            IF SER.ID EQ '' THEN
                SER.ID = 1
                ID.NEW = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
            END ELSE
                SER.ID  = SER.ID + 1
                ID.NEW  = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
            END
            CALL DBR('SCB.WH.ITEMS':@FM:0,ID.NEW,ITEM.EXIST)
            IF ITEM.EXIST EQ '' THEN
                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[4,7]
                END ELSE
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[3,8]
                END


                CALL DBR("NUMERIC.CURRENCY":@FM:EB.NCN.CURRENCY.CODE,ID.NEW[11,2],CURR)
                R.NEW(SCB.WH.IT.CURRENCY)        = CURR
                R.NEW(SCB.WH.IT.CATEGORY)        = ID.NEW[13,4]
                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[4,1]
                END ELSE
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[3,2]
                END


                R.NEW(SCB.WH.IT.WHAREHOUSE.NO)   = ID.NEW[17,2]
                R.NEW(SCB.WH.IT.VERSION.NAME)    = PGM.VERSION

*R.NEW(SCB.WH.IT.DESCRIPTION)     = ""
*R.NEW(SCB.WH.IT.UNITS.BALANCE)   = ""
*R.NEW(SCB.WH.IT.VALUE.BALANCE)   = ""
*R.NEW(SCB.WH.IT.UNIT.OF.STORE)   = ""
*R.NEW(SCB.WH.IT.UNIT.PRICE)      = ""
                R.NEW(SCB.WH.IT.WH.OPENING.DATE) = TODAY
*R.NEW(SCB.WH.IT.EXPIRING.DATE)   = ""
*R.NEW(SCB.WH.IT.TRANSACTION.TYPE)= ""
*R.NEW(SCB.WH.IT.NO.OF.UNITS)     = ""
*R.NEW(SCB.WH.IT.VALUE.DATE)      = ""
            END
        END ELSE
*--------------------- NOT BY HAND
            IF MY.SER EQ '' THEN
                CALL DBR('SCB.WH.ITEMS.SERIAL':@FM:SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16],SER.ID)

                IF SER.ID EQ '' THEN
                    SER.ID = 1
                    ID.NEW = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
                END ELSE
                    SER.ID = SER.ID + 1
                    ID.NEW = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
*---- 2010/03/10 EDITED BY NESSMA
                    CALL DBR('SCB.WH.ITEMS$NAU':@FM:0,ID.NEW,ERR.ITEM)
                    IF ERR.ITEM NE '' THEN
                        E = '���� ����� ����� ����'
                        CALL ERR;MESSAGE='REPEAT'
                        CALL STORE.END.ERROR
                    END
*----
                END

                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[4,7]
                END ELSE
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[3,8]
                END

                CALL DBR("NUMERIC.CURRENCY":@FM:EB.NCN.CURRENCY.CODE,ID.NEW[11,2],CURR)
                R.NEW(SCB.WH.IT.CURRENCY)        = CURR
                R.NEW(SCB.WH.IT.CATEGORY)        = ID.NEW[13,4]
                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[4,1]
                END ELSE
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[3,2]
                END

                R.NEW(SCB.WH.IT.WHAREHOUSE.NO)   = ID.NEW[17,2]
                R.NEW(SCB.WH.IT.VERSION.NAME)    = PGM.VERSION

                R.NEW(SCB.WH.IT.WH.OPENING.DATE) = TODAY
            END
        END
**********************
        WH.ACCT = FIELD(ID.NEW,'.',1)
        MY.ACCT = WH.ACCT[3,16]

        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,MY.ACCT,CURR)
        IF CURR EQ '' THEN
            E = "��� ������ ��� �����"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
        END
    END
    RETURN
END
