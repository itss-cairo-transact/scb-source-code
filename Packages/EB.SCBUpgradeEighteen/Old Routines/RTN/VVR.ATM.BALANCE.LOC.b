* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>236</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.BALANCE.LOC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
**  $INCLUDE           I_F.SCB.ATM.REC.REJECT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF



    IF MESSAGE NE "VAL" THEN RETURN
    ACCC = R.NEW(AC.LCK.ACCOUNT.NUMBER)
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCC,CCURR)
    IF CCURR EQ "" THEN
        E=',RESPONS.CODE=20':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
    END

*    IF R.NEW(AC.LCK.LOCKED.AMOUNT) = '' THEN
    IF COMI = '' THEN
        E=',RESPONS.CODE=05':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
    END

    IF CCURR NE "" THEN
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,R.NEW(AC.LCK.ACCOUNT.NUMBER),CU.NO)
        CATEG = R.NEW(AC.LCK.ACCOUNT.NUMBER)[11,4]
        ACT   = R.NEW(AC.LCK.ACCOUNT.NUMBER)
        AC    = R.NEW(AC.LCK.ACCOUNT.NUMBER)
        IF (CATEG GE 1101 AND CATEG LE 1590 ) THEN
            CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,AC,LIM.REF)
            LIM  = CU.NO :".":"0000":LIM.REF
            CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = COMI
            ZERO = "0.00"
            BAL  =  WORK.BALANCE + AVL
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            BLOCK =LOCK.AMT<1,LOCK.NO>
            BAL.BLOCK = BAL - BLOCK
            NET.BAL = BAL.BLOCK - AMT
            IF NET.BAL LT ZERO THEN
                E=',RESPONS.CODE=51':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
            END
        END ELSE
********************************
            CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
            AMT  = COMI
            ZERO = "0.00"
            BAL  =  WORK.BALANCE
***************UPDATED BY RIHAM 20190410************
            CALL DBR('ACCOUNT':@FM:AC.AVAILABLE.BAL,AC,AVL.BALANCE)
*Line [ 90 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AVAL.NO        = DCOUNT(AVL.BALANCE,@VM)
            BAL.AVAL = AVL.BALANCE<1,AVAL.NO>
            IF BAL.AVAL = '' OR BAL.AVAL = 0 THEN BAL.AVAL = BAL
****************************************************
            CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
*Line [ 96 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            BLOCK =LOCK.AMT<1,LOCK.NO>
            BAL.BLOCK = BAL - BLOCK
            BAL.BLOCK.AVAL = BAL.AVAL - BLOCK
            NET.BAL = BAL.BLOCK - AMT
            NET.BAL.AVAL = BAL.BLOCK.AVAL - AMT
            IF NET.BAL LT ZERO THEN
                E=',RESPONS.CODE=51':",ATM.123.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>:",RETRIVAL.REF.NO=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.RETRIVAL.REF.NO>:",TERMINAL.ID=":R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.TERMINAL.ID>
            END ELSE

*****************20190414**************************
*      CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CU.NO,CUS.SECTOR)
*      IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN
*********************20190702*******************
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CU.NO,LOC.REF)
                LOAN = LOC.REF<1,CULR.LOAN.TYPE>
*Line [ 113 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                POS = DCOUNT(LOAN,@SM)
                FOR X = 1 TO POS
                    LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>

*                    IF LOAN.TYPE EQ '103' OR LOAN.TYPE EQ '104' OR LOAN.TYPE EQ '105' OR LOAN.TYPE EQ '109' THEN
                    IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                        X = POS

                        IF NET.BAL.AVAL LT ZERO THEN
                            E = 'RESPONS.CODE=51' : ",ATM.123.REF.NO=":  R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1, FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID>   ; CALL ERR; MESSAGE='REPEAT'
                        END
                    END
                NEXT X
            END
        END
**---------------------------------------------- ADD BY BAKRY TO RETREV REMAINING BALANCE -----------------------------------
        IF NET.BAL GT ZERO THEN
*#########################################################################################
            WS.ADD.DATA = R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ADDITIONAL.DATA>
            IF WS.ADD.DATA EQ 'Int' THEN
                WS.AMOUNT   = R.NEW(AC.LCK.LOCKED.AMOUNT)
                WS.PER      = 1.5 / 100

                WS.AMOUNT.PER = WS.AMOUNT * WS.PER
                WS.AMOUNT.PER = DROUND(WS.AMOUNT.PER,'2')
**********************20190702**************************************
*                IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN

                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CU.NO,LOC.REF)
                LOAN = LOC.REF<1,CULR.LOAN.TYPE>
*Line [ 144 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                POS = DCOUNT(LOAN,@SM)
                IF POS GT 0 THEN
                    FOR X = 1 TO POS

                        LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>
*                        IF LOAN.TYPE EQ '103' OR LOAN.TYPE EQ '104' OR LOAN.TYPE EQ '105' OR LOAN.TYPE EQ '109' THEN
                        IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                            X = POS
                            IF NET.BAL.AVAL LT NET.BAL THEN

                                R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL.AVAL - WS.AMOUNT.PER
                            END ELSE
                                R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL - WS.AMOUNT.PER
                            END
                        END ELSE
                            R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL - WS.AMOUNT.PER
                        END
                    NEXT X
                END ELSE
                    R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL - WS.AMOUNT.PER
                END

            END ELSE
*=========================================================================================
********************20190702*****************************

*IF CUS.SECTOR NE '1100' AND CUS.SECTOR NE '1200' AND CUS.SECTOR NE '1300' AND CUS.SECTOR NE '1400' THEN

                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CU.NO,LOC.REF)
                LOAN = LOC.REF<1,CULR.LOAN.TYPE>
*Line [ 175 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                POS = DCOUNT(LOAN,@VM)
                IF POS GT 0 THEN
                    FOR X = 1 TO POS
                        LOAN.TYPE = LOC.REF<1,CULR.LOAN.TYPE,X>
*                            IF LOAN.TYPE EQ '103' OR LOAN.TYPE EQ '104' OR LOAN.TYPE EQ '105' OR LOAN.TYPE EQ '109' THEN
                        IF ( LOAN.TYPE GE '101' AND LOAN.TYPE LE '1104' ) THEN
                            IF NET.BAL.AVAL LT NET.BAL THEN
                                R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL.AVAL
                            END ELSE
                                R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL
                            END
                        END ELSE
                            R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL
                        END
                    NEXT X
                END ELSE
                    R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.REMINING.BAL> = NET.BAL
                END

            END
            IF V$FUNCTION = 'R' THEN
                BAL.REV= NET.BAL + R.NEW(AC.LCK.LOCKED.AMOUNT)
                E = "REMINING.BAL:1:1=":BAL.REV
            END
        END
**--------------------------------------------------END OF ADD -------------------------------------------------------------
        R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.EBC.AUTH.NO> = R.NEW(AC.LCK.LOCAL.REF)<1,AC.LCK.LOC.ATM.123.REF.NO>[6]
        CALL REBUILD.SCREEN
**----------------------------------------------
**********************************
        RETURN
    END
