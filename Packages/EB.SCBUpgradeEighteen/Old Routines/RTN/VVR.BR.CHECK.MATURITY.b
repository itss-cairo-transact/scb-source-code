* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2361</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.CHECK.MATURITY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS


    IF  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE  "9902" THEN
        IF MESSAGE # 'VAL' THEN
            IF COMI NE R.NEW(BRLR.MAT.DATE) THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
            END
        END
        GOSUB INTIAL

        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 9 THEN
            GOSUB OUT.OF.CLEARING
            RETURN
        END

        IF COMI THEN

            MAT.DATE       = COMI
            NEXT.MAT.DATE  = TODAY
            COMP           = C$ID.COMPANY
            USR.DEP        = "1700":COMP[8,2]
            CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)

            DAL.TODAY = TODAY
            IF LOCT EQ '1'  THEN
                CALL CDT('EG00',NEXT.MAT.DATE,"+1W")
            END ELSE
                CALL CDT('EG00',NEXT.MAT.DATE,"+2W")
            END

            IF MAT.DATE LE NEXT.MAT.DATE THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 6
            END
            IF MAT.DATE GT NEXT.MAT.DATE THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> =7
            END
**---------
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 7 THEN
*----------------------------
* CALC THE COMM
*----------------------------
**   IF MESSAGE # 'VAL' THEN ; ----2011/11/15--- HHYTHM & NESMAA
                IF COMI THEN
                    IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN
**  IF COMI THEN

                        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
                        CALL OPF(FN.CUS,F.CUS)
                        CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
                        SEC  = R.CUS<EB.CUS.SECTOR>
                        IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN

                            CURR      = R.NEW(EB.BILL.REG.CURRENCY)
                            AMTT      = R.NEW(EB.BILL.REG.AMOUNT)

                            FN.BR.COM = 'F.SCB.BR.CUS' ; F.BR.COM = '' ; R.BR.COM = '' E11=''
                            CALL OPF(FN.BR.COM,F.BR.COM)
                            BILL.CUS = R.NEW(EB.BILL.REG.DRAWER)
                            CALL F.READ(FN.BR.COM,BILL.CUS,R.BR.COM,F.BR.COM,E11)
                            IF NOT(E11) THEN
                                COM.AMT   = R.BR.COM<BR.CUS.COMM.AMT>
                                COM.PR    = R.BR.COM<BR.CUS.COMM.PREC>
                                CHRG.AMT  = R.BR.COM<BR.CUS.CHRG.AMT>
                               * TEXT =  COM.PR ; CALL REM

                                IF CHRG.AMT EQ 'YES' OR COM.AMT EQ 0 THEN

                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""

                                    IF CHRG.AMT EQ 'YES' THEN
*TEXT = "HH" ; CALL REM
                                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""

                                        CALL REBUILD.SCREEN
                                    END
                                END ELSE
                                    IF COM.PR THEN
                                        *TEXT = COM.PR  ; CALL REM

                                        PER = ".2"  * COM.PR
                                        MIN = "10"
                                        MAX = "300" * COM.PR

                                        COMM.AMT = (AMTT*PER/100)
                                        IF COMM.AMT LT MIN THEN
                                            COMM.AMT = MIN
                                        END ELSE
                                            IF COMM.AMT GT MAX THEN
                                                COMM.AMT = MAX
                                            END
                                        END
                                        CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
                                    END

                                    IF COM.AMT GT 0 THEN
                                        COMM.AMT = COM.AMT
                                    END
                                    COMM.AMT = CURR:COMM.AMT
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = "BILLCOLL"
                                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
                                END
                            END ELSE

                                PER = ".2"
                                MIN = "10"
                                MAX = "300"
                                COMM.AMT = (AMTT*PER/100)
                                IF COMM.AMT LT MIN THEN
                                    COMM.AMT = MIN
                                END ELSE
                                    IF COMM.AMT GT MAX THEN
                                        COMM.AMT = MAX
                                    END
                                END
                                CALL EB.ROUND.AMOUNT ('EGP',COMM.AMT,'',"2")
                                COMM.AMT = CURR:COMM.AMT
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = "BILLCOLL"
                                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = COMM.AMT
                            END
                        END
                    END
                END
** END ; ----2011/11/15--- HHYTHM & NESMAA
                IF CHRG.AMT EQ 'YES' THEN
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>    = ""
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""

                END

                CALL REBUILD.SCREEN


            END
**---------

            IF MAT.DATE GT NEXT.MAT.DATE THEN
                GOSUB CALC.DATE.S.PP
                GOSUB CALC.DATE.COLLECTION
            END ELSE
                GOSUB CALC.DATE.SESSION
                IF NOT(ETEXT) THEN
                    GOSUB CALC.DATE.COLLECTION
                END
            END
        END
    END
*-------------WAGDY-20100610---------------
**   IF MESSAGE # 'VAL' THEN ; ----2011/11/15--- HHYTHM & NESSMA

    IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN
**  IF COMI THEN

        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
        CALL OPF(FN.CUS,F.CUS)
        CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
        SEC  = R.CUS<EB.CUS.SECTOR>
        IF   R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 7 THEN
            IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN

                MAT.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
                REC.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

                IF  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE  "9902" THEN
                    DAYS.BET = 'C'
                    CALL CDD("",REC.DT,MAT.DT,DAYS.BET)
                END
**TEXT = "DAYS.BET : " : DAYS.BET ; CALL REM
                YEE = DAYS.BET / 365
                WW  = INT(DAYS.BET/365)

*TEXT = "YEE : " : YEE ; CALL REM
                CHRG = 3

**TEXT = "CHRG : " : CHRG ; CALL REM
*TEXT = "WW   : " : WW   ; CALL REM

                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = "BILCHARG"

                CALL REBUILD.SCREEN


                IF WW GE 1 THEN
                    IF YEE GT WW THEN
                        CHRG = ( WW * 3 ) + 3
*TEXT = "CHRGW :": CHRG ; CALL REM
                    END ELSE
                        CHRG = WW
                    END
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = CHRG
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = "BILCHARG"
                    CALL REBUILD.SCREEN
                END
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>  = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = ""
                CALL REBUILD.SCREEN
            END
            IF CHRG.AMT EQ 'YES' THEN
*TEXT = "HH" ; CALL REM
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ""

***TEXT = "CH.CCY.AMT : " : CH.CCY.AMT ; CALL REM
**TEXT = "CHARGE.TYPE : " : CHARGE.TYPE ; CALL REM
                CALL REBUILD.SCREEN
            END
        END
    END
**    END ; ----2011/11/15--- HHYTHM & NESSMA
    RETURN
*-----------------------------
INTIAL:
    MAT.DATE = '' ; MAT.DATE1 = '' ; DIFF = '' ; DAT1 = '' ; PAY.CODE = ''
    PAY.DAYS = ''
    RETURN

*----------------------------
CALC.DATE.SESSION:
    T.DAY = TODAY

    XX  = ''
    CALL JULDATE(TODAY,XX )
    XX.DAT = XX - 999
    XX1 = ''
    CALL JULDATE(MAT.DATE,XX1 )

    IF XX1 LT XX.DAT THEN
        ETEXT = "����� ������� ����� ��� ���� ���"
    END ELSE

        IF LOCT EQ '1'  THEN
            CALL CDT('',T.DAY,"+1W")
        END ELSE
            CALL CDT('',T.DAY,"+2W")
        END
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = T.DAY

        CALL REBUILD.SCREEN
    END
    RETURN

*------------------------

CALC.DATE.S.PP:

    MAT.DATE1 = COMI

    CALL AWD("EG00",MAT.DATE1,HOL)
    IF HOL EQ "H" THEN
        CALL CDT("EG00",MAT.DATE1,"+1W")
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
    END ELSE
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
    END
    CALL REBUILD.SCREEN

    RETURN

*--------------------------
CALC.DATE.COLLECTION:

    DAT1 = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
    PAY.CODE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE>
    IF PAY.CODE EQ 0 THEN
        PAY.DAYS = 0
    END ELSE
        PAY.DAYS = PAY.CODE
    END

    CALL CDT('EG00', DAT1, '+':PAY.DAYS:'W')

    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE> = DAT1

    CALL REBUILD.SCREEN
    RETURN

*----------------------------------
OUT.OF.CLEARING:
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
    RETURN
*----------------------------------

END
