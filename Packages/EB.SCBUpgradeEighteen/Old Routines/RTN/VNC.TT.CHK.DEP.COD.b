* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>244</Rating>
*-----------------------------------------------------------------------------
*---NESSREEN AHMED 14/07/2002-------

SUBROUTINE VNC.TT.CHK.DEP.COD

*CHECK IF THE OPERATION IN TELLER$NAU  THEN
*IF THE DEPARTMENT.CODE OF THE INPUTTER NOT EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*THEN DISPLAY ERROR
*IF THE DEPARTMENT.CODE OF THE INPUTTER  EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*CHECK IF THE TRANSACTION.CODE NOT EQUAL '5' DISPLAY ERROR

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID




INPUTT = ''
BRANCH = ''
TRANS.ID= R.NEW(TT.TE.TRANSACTION.CODE)
IF V$FUNCTION ='I' THEN

   CALL DBR('TELLER$NAU':@FM:TT.TE.TELLER.ID.1, ID.NEW ,INPUTT)
*    CALL DBR('TELLER$NAU':@FM:TT.TE.INPUTTER, ID.NEW ,INPUTT)
      IF NOT(ETEXT) THEN

*******************************************************************************
             TEL.ID = ''
             TEL.ID = R.NEW(TT.TE.TELLER.ID.1)
             CALL DBR('TELLER.ID':@FM:TT.TID.USER,TEL.ID , TEL.USER)
              IF ETEXT THEN RETURN
             CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, TEL.USER , TEL.DEPT)
              IF ETEXT THEN RETURN
             IF TEL.DEPT # R.USER<EB.USE.DEPARTMENT.CODE> THEN
                 E = 'SHOULD.ONLY.ENTER.FROM.UR.DEPT'
                 CALL ERR ;MESSAGE = 'REPEAT'
             END ELSE
************************************************************************************************
     *       CALL GET.INPUTTER.BRANCH(BRANCH,INPUTT)
*             IF R.USER<EB.USE.DEPARTMENT.CODE> # BRANCH THEN
*               E = 'SHOULD.ONLY.ENTER.FROM.UR.DEPT'
*               CALL ERR ;MESSAGE = 'REPEAT'
*             END ELSE
************************************************************************************************
               IF R.NEW(TT.TE.TRANSACTION.CODE) # 5 THEN
                   CALL GET.TELLER.TRN.DESC(TRANS.ID,TRANS.DESC)
                   E = 'YOU MUST RETRIVE THIS RECORD FROM VERSION & ':@FM:TRANS.DESC
                   CALL ERR ; MESSAGE = 'REPEAT'
               END
             END

      END
END

RETURN
END
