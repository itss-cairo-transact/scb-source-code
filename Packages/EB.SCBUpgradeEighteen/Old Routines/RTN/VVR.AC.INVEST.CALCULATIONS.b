* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AC.INVEST.CALCULATIONS
********* FOR THE FIRST TIME *********888
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS


*********************to get data from screen and currency of acct****
    IF COMI NE '' THEN

        NO.OF.BONDS = R.NEW(AC.LOCAL.REF)<1,ACLR.NO.OF.BONDS>
        PRICE.EVAL   = COMI
        PREV.BAL    = R.NEW(AC.LOCAL.REF)<1,ACLR.PREV.BAL>

        EVAL.BAL    = NO.OF.BONDS * PRICE.EVAL
        R.NEW(AC.LOCAL.REF)<1,ACLR.EVAL.BAL> = EVAL.BAL
        EVAL.BAL = EVAL.BAL * -1
        R.NEW(AC.LOCAL.REF)<1,ACLR.REVALUATION> = EVAL.BAL - PREV.BAL
        CALL REBUILD.SCREEN
    END ELSE

        AF = 20
        AV = 27
        ETEXT = "���� �� ����� ��� �������"
        CALL STORE.END.ERROR
    END
    RETURN
***********************************************
END
