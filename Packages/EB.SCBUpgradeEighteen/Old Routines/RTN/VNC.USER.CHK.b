* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.USER.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

***********UPDATAED BY MAHMOUD 1/7/2010********************************
*    IF V$FUNCTION = 'I' AND  ( R.NEW(EB.USE.RECORD.STATUS)='INAU' OR R.NEW(EB.USE.RECORD.STATUS)='IHLD' ) THEN
    CALL DBR('USER$NAU':@FM:EB.USE.RECORD.STATUS,ID.NEW,STAT)
    IF STAT THEN
*        E = 'Cant Access Record '; CALL ERR ; MESSAGE = 'REPEAT'
        E = 'Please Authorise First ' ; CALL ERR ; MESSAGE = 'REPEAT'
    END
***********************************************************************
*** ADD BY MOHAMED SABRY 2011/10/13
***********************************************************************
    IF V$FUNCTION = 'I' THEN
        IF PGM.VERSION = ',DEPT.CODE' THEN
            IF ID.NEW # 'SCB.4855' THEN
                E = '�� ���� ����� ��� ������'
                CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
    END
    RETURN
END
