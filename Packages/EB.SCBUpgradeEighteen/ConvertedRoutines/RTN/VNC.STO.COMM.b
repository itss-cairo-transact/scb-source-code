* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.STO.COMM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDING.ORDER

**    XX = R.NEW(STO.COMMISSION.AMT)<1,1>
    IF R.NEW(STO.COMMISSION.CODE)<1,1> NE 'WAIVE' THEN
        XX = R.NEW(STO.COMMISSION.AMT)<1,1>
        R.NEW(STO.COMMISSION.AMT)<1,1> = 'EGP25.00'
        R.NEW(STO.COMMISSION.CODE)<1,1>= 'DEBIT PLUS CHARGES'
        R.NEW(STO.COMMISSION.TYPE)<1,1>= 'STANDCOM'
        XX = 'EGP25.00'
    END
    IF R.NEW(STO.COMMISSION.CODE)<1,1> EQ 'WAIVE'  THEN
        XX = R.NEW(STO.COMMISSION.AMT)<1,1>
        R.NEW(STO.COMMISSION.AMT)<1,1> = ''
        R.NEW(STO.COMMISSION.CODE)<1,1>= ''
        R.NEW(STO.COMMISSION.TYPE)<1,1>= ''
        XX = ''
    END
    RETURN
END
