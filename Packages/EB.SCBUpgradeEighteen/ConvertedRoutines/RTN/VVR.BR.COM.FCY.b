* @ValidationCode : MjoyMDc4NDk5ODEzOkNwMTI1MjoxNjQ0OTMyNDMwNzU5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:40:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
SUBROUTINE VVR.BR.COM.FCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK
*-----------------------------------------------
*-- COM TYPE DEFAULT AT VERION (CHQCOLFOR)
    IF MESSAGE NE 'VAL' THEN
        ETEXT    = ''
        E        = ''
        CURR     = R.NEW(EB.BILL.REG.CURRENCY)
        COM.TYPE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>
        BILL.AMT = R.NEW(EB.BILL.REG.AMOUNT)
        FN.COMM  = 'FBNK.FT.COMMISSION.TYPE';F.COMM='';R.COMM = '';E1=''
        CALL OPF(FN.COMM,F.COMM)
        CALL F.READ(FN.COMM,COM.TYPE, R.COMM, F.COMM ,E1)
*        COM.CUR = R.COMM<FT4.CURRENCY,1>

        LOCATE CURR IN R.COMM<FT4.CURRENCY,1> SETTING J ELSE ETEXT = 'ERROR IN CUR & PARAMETER'

        COM.PER = R.COMM<FT4.PERCENTAGE,J>
        COM.MIN = R.COMM<FT4.MINIMUM.AMT,J>
        COM.MAX = R.COMM<FT4.MAXIMUM.AMT,J>

        IF R.NEW(EB.BILL.REG.DRAWER) EQ '2300227' AND TODAY LE '20171221' THEN
            COM.MAX = 100
        END
        IF CURR NE "USD" THEN
            FN.CUR  = 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
            CALL OPF(FN.CUR,F.CUR)

            CALL F.READ(FN.CUR,CURR, R.CUR, F.CUR ,E11)
            CUR.RATE  = R.CUR<EB.CUR.MID.REVAL.RATE,1>

            CALL F.READ(FN.CUR,"USD", R.CUR1, F.CUR ,E11)
            CUR.RATE1 = R.CUR1<EB.CUR.MID.REVAL.RATE,1>
            COM.AMT   = ((BILL.AMT * CUR.RATE ) / CUR.RATE1 )* COM.PER / 100



            BEGIN CASE
                CASE COM.AMT LE COM.MIN
                    COMM.AMTT = COM.MIN
                    COMM.AMTT = ((COMM.AMTT * CUR.RATE1)/CUR.RATE)
                CASE COM.AMT GE COM.MAX
                    COMM.AMTT = COM.MAX
                    COMM.AMTT = ((COMM.AMTT * CUR.RATE1)/CUR.RATE)
                CASE  OTHERWISE
                    COMM.AMTT = (BILL.AMT * COM.PER) / 100
            END CASE
        END ELSE
            COM.AMT =  (BILL.AMT * COM.PER) / 100

            BEGIN CASE
                CASE COM.AMT LE COM.MIN
                    COMM.AMTT = COM.MIN
                CASE COM.AMT GE COM.MAX
                    COMM.AMTT = COM.MAX
                CASE  OTHERWISE
                    COMM.AMTT = COM.AMT
            END CASE
        END

        CALL EB.ROUND.AMOUNT ('USD',COMM.AMTT,'',"")
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR:COMM.AMTT
        ETEXT = ''
        E     = ''
        CALL REBUILD.SCREEN
    END
*----------------------------------------------------------------------*
RETURN
END
