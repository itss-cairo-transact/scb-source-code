* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>908</Rating>
*-----------------------------------------------------------------------------
**---------INGY 10/07/2002---------**
**--------UPDATE BY DALIA 12/07/2002----**

    SUBROUTINE VNC.TT.VER.NAME.DEPT.CODE

*the version has zero authorized but it can put in ihld status (ihld)
*the routine to check if the operation in teller$nau (ihld) then
*if the department.code of the inputter not equal branch of user (r.user<eb.use.department.code>)
*display error
*if the department.code of the inputter  equal branch of user (r.user<eb.use.department.code>)
* check if the tt.tr.short.desc (teller.transaction) which related to ver.name accourding to
*transaction.code not equal pgm.version display error

*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.TRANSACTION

    VER.NAME = ''
    INPUTT = ''
    BRANCH = ''
    E = ''

    IF V$FUNCTION ='I' THEN
        IF PGM.VERSION NE ",SCB.PASSBOOK" THEN
            GOSUB CHECK.BRANCH
            IF E THEN GOTO END.RTN
            GOSUB CHECK.VERSION
        END
        IF E THEN GOTO END.RTN
        IF PGM.VERSION = ',SCB.SENDCCY.TO.BRN1' THEN GOSUB CHECK.VAULT

    END
    GOTO END.RTN
************************************************************************************
CHECK.BRANCH:
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('TELLER$NAU':@FM:TT.TE.INPUTTER, ID.NEW ,INPUTT)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
INPUTT=R.ITSS.TELLER$NAU<TT.TE.INPUTTER>
    IF NOT(ETEXT) THEN
        CALL GET.INPUTTER.BRANCH(BRANCH,INPUTT)
        IF R.USER<EB.USE.DEPARTMENT.CODE> # BRANCH THEN
*     E = 'THIS OPER FROM OTHER BRANCH'
           E = '��� ������� �� ��� ���'
        END
    END ELSE
        T.SEL = 'SELECT FBNK.TELLER.TRANSACTION WITH SHORT.DESC EQ ': PGM.VERSION
        KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
        IF KEY.LIST THEN R.NEW(TT.TE.TRANSACTION.CODE) = KEY.LIST<SELECTED>
        ELSE E='��� ������ ��� �����'
*E = 'TRANSACTION DOES NOT EXISIT'
        RETURN
************************************************************************************
CHECK.VERSION:
        IF INPUTT THEN
            TRANS.ID= R.NEW(TT.TE.TRANSACTION.CODE)
*Line [ 86 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('TELLER.TRANSACTION':@FM:TT.TR.SHORT.DESC,TRANS.ID,VER.NAME)
F.ITSS.TELLER.TRANSACTION = 'FBNK.TELLER.TRANSACTION'
FN.F.ITSS.TELLER.TRANSACTION = ''
CALL OPF(F.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION)
CALL F.READ(F.ITSS.TELLER.TRANSACTION,TRANS.ID,R.ITSS.TELLER.TRANSACTION,FN.F.ITSS.TELLER.TRANSACTION,ERROR.TELLER.TRANSACTION)
VER.NAME=R.ITSS.TELLER.TRANSACTION<TT.TR.SHORT.DESC>
            IF NOT(ETEXT) THEN
                IF VER.NAME # PGM.VERSION THEN
                    E = ' ���� �� ������� ��� ����� �� ���� &': @FM : VER.NAME
*       E = 'YOU MUST RETRIVE THIS RECORD FROM VERSION & ' : @FM : VER.NAME


                END
            END


        END

        RETURN
*********************************************************************************************
CHECK.VAULT:
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('TELLER.USER':@FM:1,OPERATOR,USE.ID)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
USE.ID=R.ITSS.TELLER.USER<1>
        IF USE.ID[3,2] # 99 THEN
            E = '��� �����'
        END
        RETURN

**********************************************************************************************
END.RTN:
        IF E THEN CALL ERR ;MESSAGE = 'REPEAT'


        RETURN
    END

END
