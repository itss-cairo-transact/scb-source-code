* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
****CREATED BY NESSMA
    SUBROUTINE VNC.WRITE.KEST.DET

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.KEST.DETAILS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.KEST.LOANS
*-----------------------------------------------------
    GOSUB INTIALIZE
    GOSUB INPUT.DATA
    GOSUB WRITE.DATA
*-----------------------------------------------------
INTIALIZE:
    FN.DETAIL   = 'F.SCB.KEST.DETAILS'  ; F.DETAIL  = '' ; R.DETAIL  = ''
    FN.LOANS    = 'F.SCB.KEST.LOANS'    ; F.LOANS   = '' ; R.LOANS   = ''
    CALL OPF(FN.DETAIL,F.DETAIL)
    CALL OPF(FN.LOANS,F.LOANS)
    RETURN
*----------*
INPUT.DATA:
*REF.ID = ID.NEW
    REF.ID = R.NEW(KEST.OUR.REF)
    ACC.ID = R.NEW(KEST.ACCOUNT.NO)
    OFFSET = R.NEW(KEST.PERIODIC.INSTALLMENT)
    I_LOOP = R.NEW(KEST.NO.OF.INSTALLMENT)
    AMOUNT = R.NEW(KEST.INSTALLMENT.AMOUNT)
    STRT.D = R.NEW(KEST.START.DATE)
    REF.NO = REF.ID
    RETURN
*----------*
WRITE.DATA:
    T.SEL = "SELECT F.SCB.KEST.DETAILS BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.DETAIL,KEY.LIST<SELECTED>,R.DETAIL,F.DETAIL,ER)
    REF.ID = R.DETAIL<KST.DTAIL.OUR.REF>
    NN     = 1
    MY.ID  = NN + REF.ID
    CALL F.READ(FN.DETAIL,MY.ID,R.DETAIL,F.DETAIL,ER)
    R.DETAIL<KST.DTAIL.OUR.REF>      = MY.ID
    R.DETAIL<KST.DTAIL.KEST.DATE>    = STRT.D
    R.DETAIL<KST.DTAIL.KEST.AMOUNT>  = AMOUNT
    R.DETAIL<KST.DTAIL.HIS.REFRENCE> = REF.NO
    R.DETAIL<KST.DTAIL.STATUS>       = 'N'
*WRITE  R.DETAIL TO F.DETAIL , MY.ID ON ERROR
************PRINT "CAN NOT WRITE RECORD"
*END
    CALL F.WRITE (FN.DETAIL,MY.ID,R.DETAIL)
*-----
*-----
    FOR NN = 2 TO I_LOOP
        MY.ID = NN + REF.ID
        CALL F.READ(FN.DETAIL,MY.ID,R.DETAIL,F.DETAIL,ER11)
        R.DETAIL<KST.DTAIL.OUR.REF>      = MY.ID
        CALL ADD.MONTHS(STRT.D,OFFSET)
        R.DETAIL<KST.DTAIL.KEST.DATE>    = STRT.D
        R.DETAIL<KST.DTAIL.KEST.AMOUNT>  = AMOUNT
        R.DETAIL<KST.DTAIL.HIS.REFRENCE> = REF.NO
        R.DETAIL<KST.DTAIL.STATUS>       = 'N'
*WRITE  R.DETAIL TO F.DETAIL , MY.ID ON ERROR
************PRINT "CAN NOT WRITE RECORD"
*END
        CALL F.WRITE (FN.DETAIL,MY.ID,R.DETAIL)
    NEXT NN
    RETURN
*-----------------------------------------------------
    RETURN
END
