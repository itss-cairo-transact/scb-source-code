* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>400</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.PD.REQUEST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PD.REQUEST

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN


    IF LEN(ID.NEW) = 16 THEN ID.NEW = ID.NEW :"-":TODAY

*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SCB.PD.REQUEST':@FM:SCB.PD.PD.DATE,ID.NEW,RECORD.EXIT)
F.ITSS.SCB.PD.REQUEST = 'F.SCB.PD.REQUEST'
FN.F.ITSS.SCB.PD.REQUEST = ''
CALL OPF(F.ITSS.SCB.PD.REQUEST,FN.F.ITSS.SCB.PD.REQUEST)
CALL F.READ(F.ITSS.SCB.PD.REQUEST,ID.NEW,R.ITSS.SCB.PD.REQUEST,FN.F.ITSS.SCB.PD.REQUEST,ERROR.SCB.PD.REQUEST)
RECORD.EXIT=R.ITSS.SCB.PD.REQUEST<SCB.PD.PD.DATE>

    IF RECORD.EXIT THEN
        E = "������ ���� �� ���"  ; CALL ERR; MESSAGE = 'REPEAT'
        RETURN
    END ELSE
        CATEG = ID.NEW[11,4]
*       IF NOT(CATEG EQ "1201" OR CATEG EQ "1202") THEN
        IF NOT(CATEG GE "1101" AND CATEG LE "1599") THEN
            E = "��� �� ���� GE 1101 �� LE 1590"  ; CALL ERR; MESSAGE = 'REPEAT'
        END
        R.NEW(SCB.PD.RECORD.DATE) = TODAY
        R.NEW(SCB.PD.COUNTER)     = 1
        R.NEW(SCB.PD.STATUS)      = "PD"
        R.NEW(SCB.PD.STATUS.DATE) = TODAY
    END

    RETURN
END
