* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>343</Rating>
*-----------------------------------------------------------------------------
***** WAEL *** 21/7/2002

SUBROUTINE VNC.RECORD.CHECK

*IF RECORD.STATUS IS IN HOLD THEN NO ONE CAN OPEN THE RECORD EXCEPT THE INPUTTER
*IF RECORD.STATUS IS NOT AUTHORISED THEN NO ONE CAN AUTHORISE EXCEPT THE HEAD TELLER
*NO ONE CAN REVERSE THE RECORD EXCEPT THE INPUTTER

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

GOSUB  INITIAL:
GOSUB  PR1
GOSUB  PR2
GOTO END.PROG

INITIAL:
*==================================================================================================
XX= ''
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER$NAU':@FM:TT.TE.RECORD.STATUS,ID.NEW,XX)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
XX=R.ITSS.TELLER$NAU<TT.TE.RECORD.STATUS>
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.OP)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.OP=R.ITSS.TELLER.USER<1>
*TEXT = 'TT.OP=':TT.OP ; CALL REM
*====================GET THE TELLER.ID OF THE INPUTTER================================
X = ''
*Line [ 65 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER$NAU':@FM:TT.TE.INPUTTER,ID.NEW,Y)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
Y=R.ITSS.TELLER$NAU<TT.TE.INPUTTER>
X = FIELD(Y,'_', 2)
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,X,TT.INP)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,X,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.INP=R.ITSS.TELLER.USER<1>
*TEXT = 'INP=' :TT.INP ; CALL REM
*=====================  LIVE FILE ==============================================================
****18/7/2005 NESSREEN
* CALL DBR ('TELLER':@FM:TT.TE.INPUTTER,ID.NEW,DD)
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR('TELLER':@FM:TT.TE.AUTHORISER,ID.NEW,DD)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ID.NEW,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
DD=R.ITSS.TELLER<TT.TE.AUTHORISER>
X = FIELD(DD,'_', 2)
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,X,TT.INP2)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,X,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.INP2=R.ITSS.TELLER.USER<1>
*TEXT = 'AUTH=':TT.INP2 ; CALL REM
RETURN
*====================================================================================

PR1:
IF XX = 'IHLD'  THEN
 IF TT.INP # TT.OP THEN E = '��� �� ���� ������'
END

RETURN
*====================================================================================================
PR2:

    IF (XX ='INAU' OR XX='RNAU') AND V$FUNCTION # 'A' AND TT.INP # TT.OP THEN
    E = '��� ����� ����������'
    END
      *TEXT = 'TELLER.ID.2=':R.NEW(TT.TE.TELLER.ID.2) ; CALL REM
      IF XX = 'INAU' AND V$FUNCTION = 'A' AND R.NEW(TT.TE.TELLER.ID.2) # TT.OP THEN
       E = '��� ����� �������� ����� ��� ����� ������'
     END




*===============================================================================================
   IF V$FUNCTION = 'R' AND TT.OP # TT.INP2 THEN
      E = 'YOU MUST REVERSE THIS RECORD FROM ' 
   END
*===============================================
  IF V$FUNCTION ='A' AND XX ='RNAU' THEN
     IF R.NEW(TT.TE.TELLER.ID.2) # TT.OP THEN E = '��� ����� �������� ������ ��� ����� ������'
  END

RETURN
*======================================================================================================

END.PROG:
IF E THEN CALL ERR ;MESSAGE = 'REPEAT'


RETURN
END
