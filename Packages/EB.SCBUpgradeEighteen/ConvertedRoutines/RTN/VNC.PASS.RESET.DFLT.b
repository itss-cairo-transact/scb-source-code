* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
****************** WAGDY MOUNIR *************************
    SUBROUTINE VNC.PASS.RESET.DFLT
*  PROGRAM    VNC.PASS.RESET.DFLT

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ENQUIRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.IMP.CODES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROJECT.IMP.STATUS


*   EB.PWR.USER.PW.ATTEMPT
*  EB.PWR.USER.RESET

    P.USER = ''

*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SCB.PROJECT.IMP.STATUS': @FM:WN.PASS.RESET.USER,ID.NEW,P.USER)
F.ITSS.SCB.PROJECT.IMP.STATUS = 'F.SCB.PROJECT.IMP.STATUS'
FN.F.ITSS.SCB.PROJECT.IMP.STATUS = ''
CALL OPF(F.ITSS.SCB.PROJECT.IMP.STATUS,FN.F.ITSS.SCB.PROJECT.IMP.STATUS)
CALL F.READ(F.ITSS.SCB.PROJECT.IMP.STATUS,ID.NEW,R.ITSS.SCB.PROJECT.IMP.STATUS,FN.F.ITSS.SCB.PROJECT.IMP.STATUS,ERROR.SCB.PROJECT.IMP.STATUS)
P.USER=R.ITSS.SCB.PROJECT.IMP.STATUS<WN.PASS.RESET.USER>
    TEXT = "USER IS ": P.USER ; CALL REM

*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SCB.PROJECT.IMP.STATUS': @FM:WN.REQUIRED.TASK,ID.NEW,FLAG)
F.ITSS.SCB.PROJECT.IMP.STATUS = 'F.SCB.PROJECT.IMP.STATUS'
FN.F.ITSS.SCB.PROJECT.IMP.STATUS = ''
CALL OPF(F.ITSS.SCB.PROJECT.IMP.STATUS,FN.F.ITSS.SCB.PROJECT.IMP.STATUS)
CALL F.READ(F.ITSS.SCB.PROJECT.IMP.STATUS,ID.NEW,R.ITSS.SCB.PROJECT.IMP.STATUS,FN.F.ITSS.SCB.PROJECT.IMP.STATUS,ERROR.SCB.PROJECT.IMP.STATUS)
FLAG=R.ITSS.SCB.PROJECT.IMP.STATUS<WN.REQUIRED.TASK>
    TEXT = "FLAG EQ ": FLAG ; CALL REM
    IF FLAG EQ 80001 THEN
        R.NEW(EB.PWR.USER.RESET)  = P.USER
    END
    IF FLAG EQ 80002 THEN
        R.NEW( EB.PWR.USER.PW.ATTEMPT)  = P.USER
    END



    RETURN
END
