* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.FT.OPEN

*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_COMMON
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_EQUATE
*Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.USER
    $INSERT           I_FT.LOCAL.REFS
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.SCB.BT.BATCH
*------------------------------------------------
*    DEBUG
*    TEXT = PGM.VERSION:'-':R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME>; CALL REM
    IF V$FUNCTION = 'A' THEN
        IF PGM.VERSION # R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME> THEN
            IF R.NEW(FT.LOCAL.REF)<1,FTLR.VERSION.NAME> NE ',SF.EXP' AND PGM.VERSION NE ',SCB.SF.MANUAL' THEN
*** SCB UPG 20160619 - S
*E = '��� ������� �� ��� ���� �������' ; CALL ERR  ; MESSAGE = 'REPEAT'
                E = '��� ������� �� ��� ���� �������' ;CALL STORE.END.ERROR
*** SCB UPG 20160619 - E
*                E = "Use the same version" ; CALL ERR ; MESSAGE = 'REPEAT'
            END
        END
    END
*------------------------------------------------
    RETURN
END
