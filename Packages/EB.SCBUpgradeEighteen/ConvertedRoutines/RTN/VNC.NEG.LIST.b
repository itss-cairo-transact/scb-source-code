* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.NEG.LIST

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CBE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.EH = 'F.SCB.VISA.CBE' ; F.EH = ''
    CALL OPF(FN.EH,F.EH)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    EMP.ID = FIELD(ID.NEW,".",1)
    EH.ID  = EMP.ID:"..."

    DAT = FIELD(ID.NEW,".",2):'01'
    CALL LAST.DAY(DAT)

    CALL F.READ(FN.EH,ID.NEW,R.EH,F.EH,E2)
    IF E2 THEN
        T.SEL = "SELECT ":FN.EH:" WITH @ID LIKE ":EH.ID:" BY @ID"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF NOT(SELECTED) THEN

            CALL F.READ(FN.CU,EMP.ID,R.CU,F.CU,E1)

            R.NEW(CBE.CUSTOMER)       = EMP.ID
            R.NEW(CBE.REP.DATE)       = DAT
            R.NEW(CBE.BRANCH)         = R.CU<EB.CUS.COMPANY.BOOK>
            R.NEW(CBE.CUST.ACCT)      = ''
            R.NEW(CBE.LATE.CODE)      = ''
            R.NEW(CBE.CUS.BIRTH.DATE) = R.CU<EB.CUS.BIRTH.INCORP.DATE>
            R.NEW(CBE.ID.NO)          = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>
            IF R.NEW(CBE.ID.NO) NE '' THEN
                R.NEW(CBE.CARD.TYPE)      = '1'
            END ELSE
                R.NEW(CBE.CARD.TYPE)      = ''
            END
            R.NEW(CBE.ID.ISS.DATE)    = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.ISSUE.DATE>
            R.NEW(CBE.ID.EXP.DATE)    = R.CU<EB.CUS.LOCAL.REF><1,CULR.ID.EXPIRY.DATE>
            R.NEW(CBE.WORK.TEL)       = ''
            R.NEW(CBE.HOME.TEL)       = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
            R.NEW(CBE.MOB.TEL)        = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>
            R.NEW(CBE.CARD.ISS.DATE)  = ''
            R.NEW(CBE.CARD.CURRENCY)  = ''
            R.NEW(CBE.LIMIT)          = ''
            R.NEW(CBE.STOP.DATE)      = ''
            R.NEW(CBE.USED.LIMIT)     = ''
            R.NEW(CBE.CARD.NO)        = ''
            CUS.SEX                   = R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER>
            IF CUS.SEX EQ 'M-���' THEN
                R.NEW(CBE.CU.SEX)     = '1'
            END ELSE
                R.NEW(CBE.CU.SEX)     = '2'
            END
            R.NEW(CBE.CU.NAME)        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            R.NEW(CBE.BIRTH.PLACE)    = ''
            R.NEW(CBE.CU.ADDRESS)     = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
            R.NEW(CBE.JOB.DESC)       = R.CU<EB.CUS.LOCAL.REF><1,CULR.JOB.DESCRIPTION>
            R.NEW(CBE.ID.ISS.PLACE)   = ''
            R.NEW(CBE.EMAIL)          = R.CU<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
            R.NEW(CBE.CUSTOMER.TYPE)  = ''
            R.NEW(CBE.CUSTOMER.STATE) = ''
            R.NEW(CBE.LOAN.KIND)      = ''

        END ELSE

            CALL F.READ(FN.EH,KEY.LIST<SELECTED>,R.EH,F.EH,E5)
            R.NEW(CBE.CUSTOMER)       = EMP.ID
            R.NEW(CBE.REP.DATE)       = DAT
            R.NEW(CBE.BRANCH)         = R.EH<CBE.BRANCH>
            R.NEW(CBE.CUST.ACCT)      = R.EH<CBE.CUST.ACCT>
            R.NEW(CBE.LATE.CODE)      = R.EH<CBE.LATE.CODE>
            R.NEW(CBE.CUS.BIRTH.DATE) = R.EH<CBE.CUS.BIRTH.DATE>
            R.NEW(CBE.CARD.TYPE)      = R.EH<CBE.CARD.TYPE>
            R.NEW(CBE.ID.NO)          = R.EH<CBE.ID.NO>
            R.NEW(CBE.ID.ISS.DATE)    = R.EH<CBE.ID.ISS.DATE>
            R.NEW(CBE.ID.EXP.DATE)    = R.EH<CBE.ID.EXP.DATE>
            R.NEW(CBE.WORK.TEL)       = R.EH<CBE.WORK.TEL>
            R.NEW(CBE.HOME.TEL)       = R.EH<CBE.HOME.TEL>
            R.NEW(CBE.MOB.TEL)        = R.EH<CBE.MOB.TEL>
            R.NEW(CBE.CARD.ISS.DATE)  = R.EH<CBE.CARD.ISS.DATE>
            R.NEW(CBE.CARD.CURRENCY)  = R.EH<CBE.CARD.CURRENCY>
            R.NEW(CBE.LIMIT)          = R.EH<CBE.LIMIT>
            R.NEW(CBE.STOP.DATE)      = R.EH<CBE.STOP.DATE>
            R.NEW(CBE.USED.LIMIT)     = R.EH<CBE.USED.LIMIT>
            R.NEW(CBE.CARD.NO)        = R.EH<CBE.CARD.NO>
            R.NEW(CBE.CU.SEX)         = R.EH<CBE.CU.SEX>
            R.NEW(CBE.CU.NAME)        = R.EH<CBE.CU.NAME>
            R.NEW(CBE.BIRTH.PLACE)    = R.EH<CBE.BIRTH.PLACE>
            R.NEW(CBE.CU.ADDRESS)     = R.EH<CBE.CU.ADDRESS>
            R.NEW(CBE.JOB.DESC)       = R.EH<CBE.JOB.DESC>
            R.NEW(CBE.ID.ISS.PLACE)   = R.EH<CBE.ID.ISS.PLACE>
            R.NEW(CBE.EMAIL)          = R.EH<CBE.EMAIL>
            R.NEW(CBE.CUSTOMER.TYPE)  = R.EH<CBE.CUSTOMER.TYPE>
            R.NEW(CBE.CUSTOMER.STATE) = R.EH<CBE.CUSTOMER.STATE>
            R.NEW(CBE.LOAN.KIND)      = R.EH<CBE.LOAN.KIND>
        END
    END
    RETURN
END
