* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------


** ----- 9.07.2002 MONTASER SCB -----
SUBROUTINE VNC.TT.TRN23

*IN 
* CHECK I
*E

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS



TRN.ID = 23
IF V$FUNCTION ='I' THEN
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*   CALL DBR( 'TELLER$NAU':@FM:TT.TE.INPUTTER, ID.NEW , MYYIIDD)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
MYYIIDD=R.ITSS.TELLER$NAU<TT.TE.INPUTTER>
   IF NOT(ETEXT) THEN ;* 'EXIST IN TELLER$NAU'
      CALL GET.INPUTTER.BRANCH( BRANCH, MYYIIDD)
      IF R.USER< EB.USE.DEPARTMENT.CODE> # BRANCH THEN
         E = 'THIS RECORD IS NOT RELATED TO YOUR BRANCH'
         CALL ERR ; MESSAGE = 'REPEAT'
      END
  * CHECK FOR TRANSACTION.CODE
      IF R.NEW(TT.TE.TRANSACTION.CODE) # TRN.ID THEN
         CALL GET.TELLER.TRN.DESC( TRN.ID, TRN.DESC )
         E = 'YOU MUST RETRIVE THIS RECORD FROM VERSION ' : TRN.DESC
         CALL ERR ; MESSAGE = 'REPEAT'
      END
   END
END

RETURN
END
