* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.ATM.CHK.DRMNT

*Line [ 18 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_COMMON
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.CUSTOMER
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*----------------------------------------
    IF MESSAGE EQ 'VAL' THEN
        FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
        CALL OPF(FN.CU,F.CU)

        CUS.ID = COMI

        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

        WS.DRMNT.CODE   = R.CU<EB.CUS.LOCAL.REF><1,CULR.DRMNT.CODE>
        WS.POSTING.CODE = R.CU<EB.CUS.POSTING.RESTRICT>

        IF WS.DRMNT.CODE EQ '1' THEN
            ETEXT = 'No Postings Allowed'
            CALL STORE.END.ERROR
        END

        IF WS.POSTING.CODE NE '' THEN
            ETEXT = 'No Postings Allowed'
            CALL STORE.END.ERROR
        END


    END

    RETURN
END
