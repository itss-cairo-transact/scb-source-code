* @ValidationCode : MjotMTgxODA3MTE4OkNwMTI1MjoxNjQ0OTMyMjMxODYxOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:37:11
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*****NESSREEN AHMED 5/12/2017***********
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.TT.FAULT.WAIVE
*- To defult Waive.Charge field by NO if User's branch is not from exceptional branch , and YES if it is from exceptional branch.

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BRANCH.COM.EXCEP

    COMP = C$ID.COMPANY

    IF V$FUNCTION = "I" THEN
        TEXT = 'COMP=':COMP ; CALL REM
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.BRANCH.COM.EXCEP':@FM:EXCEPTION.DATE, COMP ,EXCP.D)
        F.ITSS.SCB.BRANCH.COM.EXCEP = 'F.SCB.BRANCH.COM.EXCEP'
        FN.F.ITSS.SCB.BRANCH.COM.EXCEP = ''
        CALL OPF(F.ITSS.SCB.BRANCH.COM.EXCEP,FN.F.ITSS.SCB.BRANCH.COM.EXCEP)
        CALL F.READ(F.ITSS.SCB.BRANCH.COM.EXCEP,COMP,R.ITSS.SCB.BRANCH.COM.EXCEP,FN.F.ITSS.SCB.BRANCH.COM.EXCEP,ERROR.SCB.BRANCH.COM.EXCEP)
        EXCP.D=R.ITSS.SCB.BRANCH.COM.EXCEP<EXCEPTION.DATE>
        EXCEPTION.DATE = ''
*Line [ 44 ] Intialize "EXCEPTION.DATE"  - ITSS - R21 Upgrade - 2022-02-15
        IF NOT(ETEXT) THEN
            R.NEW(TT.TE.WAIVE.CHARGES) = 'YES'
        END ELSE
            R.NEW(TT.TE.WAIVE.CHARGES) = 'NO'
        END
    END

RETURN
END
