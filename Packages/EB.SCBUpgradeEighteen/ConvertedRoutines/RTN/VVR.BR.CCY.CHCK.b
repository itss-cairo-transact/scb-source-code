* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BR.CCY.CHCK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BR.CCY


    BB=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BNK.BRANCH>
    J = ''
    FN.BANK = 'F.SCB.BANK.BR.CCY'  ;  F.BANK  = ''
    CALL OPF (FN.BANK,F.BANK)

    VV=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>

    CALL F.READ(FN.BANK,COMI,R.BANK,F.BANK,E1)

    BANKNO =  R.BANK<SCB.BAB.BANK.NO>
    W.CUR  = R.NEW(EB.BILL.REG.CURRENCY)
    LOCATE W.CUR IN R.BANK<BR.CCY.CURR,1> SETTING J  ELSE ETEXT = 'ERROR IN CURR '

    BR.CCY = R.BANK<BR.CCY.CURR,J>
    IF BANKNO NE VV THEN
        ETEXT = "��� ����� ��� ����� �� ������" ; CALL STORE.END.ERROR
    END

    RETURN

END
