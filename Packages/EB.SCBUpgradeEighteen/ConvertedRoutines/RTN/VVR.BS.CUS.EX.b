* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.BS.CUS.EX

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.SLIPS
*----------------------------------------------
    POSS = ""
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
POSS=R.ITSS.CUSTOMER<EB.CUS.TEXT>

    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    STO5=POSS<1,5>
    STO6=POSS<1,6>
    STO7=POSS<1,7>
    STO8=POSS<1,8>
    STO9=POSS<1,9>
    STO10=POSS<1,10>

    IF POSS NE '' THEN
        TEXT = "������� �������": STO1 : STO2 : STO3 : STO4 :STO5 :STO6 :STO7 :STO8 :STO9 :STO10 ; CALL REM
    END
*------------------------------------------------------------
    IF COMI NE R.NEW(SCB.BS.DRAWER) THEN
        R.NEW(SCB.BS.CHG.TYPE)        = ""
        R.NEW(SCB.BS.CHG.AMT)         = ""
        R.NEW(SCB.BS.OPERATION.TYPE)  = ""
        R.NEW(SCB.BS.CURRENCY)        = ""
        R.NEW(SCB.BS.TOTAL.AMT)       = ""
        R.NEW(SCB.BS.TOTAL.NO)        = ""
        CALL REBUILD.SCREEN
    END
*--------- ADD BY NESSMA ON 2018/06/26
    IF COMI[1,3] NE '994' THEN
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,COMI,CUS.CO)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.CO=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
***        IF CUS.CO NE ID.COMPANY THEN ETEXT = "INVALID COMPANY"
    END
    CALL REBUILD.SCREEN
*------------------------------------------------------------
    RETURN
END
