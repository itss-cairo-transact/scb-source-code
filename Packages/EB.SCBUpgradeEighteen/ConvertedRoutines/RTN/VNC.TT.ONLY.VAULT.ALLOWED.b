* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
*----- NESSREEN----

SUBROUTINE VNC.TT.ONLY.VAULT.ALLOWED

*CHECK IF YOU ARE THE CORRECT USER (VAULT) OR NOT

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER


IF V$FUNCTION = 'I' THEN

*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.NO=R.ITSS.TELLER.USER<1>
IF INDEX(TT.NO,'99',1) < 1 THEN E = '��� ����� �������'

END
*===================================================================

 IF V$FUNCTION ='R' THEN
   IF INDEX(TT.NO,'99',1) < 1 THEN E = '��� ����� �������'
 END

*=======================================================================================
IF E THEN CALL ERR ;MESSAGE = 'REPEAT'


RETURN
END
