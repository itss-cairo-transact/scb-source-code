* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.RISK.REFERENCE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
* Created by Noha Hamed
* INF.MULTI.TXN
* 12 LC
* 14 DRAWINGS

    FN.TT  = 'FBNK.TELLER'           ; F.TT = '' ; R.TT = ''
    CALL OPF(FN.TT,F.TT)
    FN.FT  = 'FBNK.FUNDS.TRANSFER'   ; F.FT = '' ; R.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.LC  = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    FN.DR  = 'FBNK.DRAWINGS'         ; F.DR = '' ; R.DR = ''
    CALL OPF(FN.DR,F.DR)
    FN.IN  = 'F.INF.MULTI.TXN'    ; F.IN = '' ; R.IN = ''
    CALL OPF(FN.IN,F.IN)



    IF MESSAGE = '' THEN

        IF V$FUNCTION EQ 'I' THEN
            REF = COMI
            REF.TYPE = REF[1,2]

            IF REF.TYPE = 'TT' THEN
                FLAG = 'NO'
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('TELLER':@FM:TT.TE.CURRENCY.1,REF,CURR)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,REF,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
CURR=R.ITSS.TELLER<TT.TE.CURRENCY.1>

                IF CURR EQ '' THEN
                    FLAG   = 'YES'
                    TT.REF = REF:";1"
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('TELLER$HIS':@FM:TT.TE.CURRENCY.1,TT.REF,CURR)
F.ITSS.TELLER$HIS = 'F.TELLER$HIS'
FN.F.ITSS.TELLER$HIS = ''
CALL OPF(F.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS)
CALL F.READ(F.ITSS.TELLER$HIS,TT.REF,R.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS,ERROR.TELLER$HIS)
CURR=R.ITSS.TELLER$HIS<TT.TE.CURRENCY.1>
                END
                IF CURR EQ 'EGP' AND FLAG EQ 'NO' THEN
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('TELLER':@FM:TT.TE.AMOUNT.LOCAL.1,REF,LOCAL.AMT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,REF,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
LOCAL.AMT=R.ITSS.TELLER<TT.TE.AMOUNT.LOCAL.1>
                END
                ELSE IF CURR NE 'EGP' AND CURR NE '' AND FLAG EQ 'NO' THEN
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('TELLER':@FM:TT.TE.AMOUNT.FCY.1,REF,LOCAL.AMT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,REF,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
LOCAL.AMT=R.ITSS.TELLER<TT.TE.AMOUNT.FCY.1>
                END
                ELSE IF CURR EQ 'EGP' AND FLAG EQ 'YES' THEN
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('TELLER$HIS':@FM:TT.TE.AMOUNT.LOCAL.1,TT.REF,LOCAL.AMT)
F.ITSS.TELLER$HIS = 'F.TELLER$HIS'
FN.F.ITSS.TELLER$HIS = ''
CALL OPF(F.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS)
CALL F.READ(F.ITSS.TELLER$HIS,TT.REF,R.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS,ERROR.TELLER$HIS)
LOCAL.AMT=R.ITSS.TELLER$HIS<TT.TE.AMOUNT.LOCAL.1>
                END
                ELSE IF CURR NE 'EGP' AND CURR NE '' AND FLAG EQ 'YES' THEN
*Line [ 115 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('TELLER$HIS':@FM:TT.TE.AMOUNT.FCY.1,TT.REF,LOCAL.AMT)
F.ITSS.TELLER$HIS = 'F.TELLER$HIS'
FN.F.ITSS.TELLER$HIS = ''
CALL OPF(F.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS)
CALL F.READ(F.ITSS.TELLER$HIS,TT.REF,R.ITSS.TELLER$HIS,FN.F.ITSS.TELLER$HIS,ERROR.TELLER$HIS)
LOCAL.AMT=R.ITSS.TELLER$HIS<TT.TE.AMOUNT.FCY.1>
                END

                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT) = LOCAL.AMT

            END

            IF REF.TYPE = 'FT' THEN
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.CURRENCY,REF,CURR)
F.ITSS.FUNDS.TRANSFER = 'FBNK.FUNDS.TRANSFER'
FN.F.ITSS.FUNDS.TRANSFER = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER)
CALL F.READ(F.ITSS.FUNDS.TRANSFER,REF,R.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER,ERROR.FUNDS.TRANSFER)
CURR=R.ITSS.FUNDS.TRANSFER<FT.DEBIT.CURRENCY>
***CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.AMOUNT,REF,LOCAL.AMT)
*Line [ 138 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('FUNDS.TRANSFER':@FM:FT.AMOUNT.DEBITED[4,10],REF,LOCAL.AMT)
F.ITSS.FUNDS.TRANSFER = 'FBNK.FUNDS.TRANSFER'
FN.F.ITSS.FUNDS.TRANSFER = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER)
CALL F.READ(F.ITSS.FUNDS.TRANSFER,REF,R.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER,ERROR.FUNDS.TRANSFER)
LOCAL.AMT=R.ITSS.FUNDS.TRANSFER<FT.AMOUNT.DEBITED[4,10]>

                IF CURR EQ '' THEN
                    FT.REF = REF:";1"
*Line [ 148 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.CURRENCY,FT.REF,CURR)
F.ITSS.FUNDS.TRANSFER$HIS = 'F.FUNDS.TRANSFER$HIS'
FN.F.ITSS.FUNDS.TRANSFER$HIS = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS)
CALL F.READ(F.ITSS.FUNDS.TRANSFER$HIS,FT.REF,R.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS,ERROR.FUNDS.TRANSFER$HIS)
CURR=R.ITSS.FUNDS.TRANSFER$HIS<FT.DEBIT.CURRENCY>
***CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.AMOUNT,FT.REF,LOCAL.AMT)
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.AMOUNT.DEBITED[4,10],FT.REF,LOCAL.AMT)
F.ITSS.FUNDS.TRANSFER$HIS = 'F.FUNDS.TRANSFER$HIS'
FN.F.ITSS.FUNDS.TRANSFER$HIS = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS)
CALL F.READ(F.ITSS.FUNDS.TRANSFER$HIS,FT.REF,R.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS,ERROR.FUNDS.TRANSFER$HIS)
LOCAL.AMT=R.ITSS.FUNDS.TRANSFER$HIS<FT.AMOUNT.DEBITED[4,10]>
                END

                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT) = LOCAL.AMT

            END

            IF REF.TYPE = 'TF' THEN
                IF LEN(REF) EQ 12 THEN
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('LETTER.OF.CREDIT':@FM:TF.LC.LC.CURRENCY,REF,CURR)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,REF,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
CURR=R.ITSS.LETTER.OF.CREDIT<TF.LC.LC.CURRENCY>
*Line [ 179 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('LETTER.OF.CREDIT':@FM:TF.LC.LC.AMOUNT,REF,LOCAL.AMT)
F.ITSS.LETTER.OF.CREDIT = 'FBNK.LETTER.OF.CREDIT'
FN.F.ITSS.LETTER.OF.CREDIT = ''
CALL OPF(F.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT)
CALL F.READ(F.ITSS.LETTER.OF.CREDIT,REF,R.ITSS.LETTER.OF.CREDIT,FN.F.ITSS.LETTER.OF.CREDIT,ERROR.LETTER.OF.CREDIT)
LOCAL.AMT=R.ITSS.LETTER.OF.CREDIT<TF.LC.LC.AMOUNT>
                END
                IF LEN(REF) EQ 14 THEN
*Line [ 188 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('DRAWINGS':@FM:TF.DR.DRAW.CURRENCY,REF,CURR)
F.ITSS.DRAWINGS = 'FBNK.DRAWINGS'
FN.F.ITSS.DRAWINGS = ''
CALL OPF(F.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS)
CALL F.READ(F.ITSS.DRAWINGS,REF,R.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS,ERROR.DRAWINGS)
CURR=R.ITSS.DRAWINGS<TF.DR.DRAW.CURRENCY>
*Line [ 195 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('DRAWINGS':@FM:TF.DR.DOCUMENT.AMOUNT,REF,LOCAL.AMT)
F.ITSS.DRAWINGS = 'FBNK.DRAWINGS'
FN.F.ITSS.DRAWINGS = ''
CALL OPF(F.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS)
CALL F.READ(F.ITSS.DRAWINGS,REF,R.ITSS.DRAWINGS,FN.F.ITSS.DRAWINGS,ERROR.DRAWINGS)
LOCAL.AMT=R.ITSS.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>

                END
                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT)      = LOCAL.AMT

            END

            IF REF.TYPE = 'IN' THEN
*Line [ 210 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.CURRENCY,REF,CURR.M)
F.ITSS.INF.MULTI.TXN = 'F.INF.MULTI.TXN'
FN.F.ITSS.INF.MULTI.TXN = ''
CALL OPF(F.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN)
CALL F.READ(F.ITSS.INF.MULTI.TXN,REF,R.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN,ERROR.INF.MULTI.TXN)
CURR.M=R.ITSS.INF.MULTI.TXN<INF.MLT.CURRENCY>
*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.AMOUNT.LCY,REF,LOCAL.AMT.M)
F.ITSS.INF.MULTI.TXN = 'F.INF.MULTI.TXN'
FN.F.ITSS.INF.MULTI.TXN = ''
CALL OPF(F.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN)
CALL F.READ(F.ITSS.INF.MULTI.TXN,REF,R.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN,ERROR.INF.MULTI.TXN)
LOCAL.AMT.M=R.ITSS.INF.MULTI.TXN<INF.MLT.AMOUNT.LCY>
*Line [ 224 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('INF.MULTI.TXN':@FM:INF.MLT.AMOUNT.FCY,REF,LOCAL.AMT.F.M)
F.ITSS.INF.MULTI.TXN = 'F.INF.MULTI.TXN'
FN.F.ITSS.INF.MULTI.TXN = ''
CALL OPF(F.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN)
CALL F.READ(F.ITSS.INF.MULTI.TXN,REF,R.ITSS.INF.MULTI.TXN,FN.F.ITSS.INF.MULTI.TXN,ERROR.INF.MULTI.TXN)
LOCAL.AMT.F.M=R.ITSS.INF.MULTI.TXN<INF.MLT.AMOUNT.FCY>

                CURR         = CURR.M<1,1>
                LOCAL.AMT.1  = LOCAL.AMT.M<1,1>
                LOCAL.AMT.2  = LOCAL.AMT.F.M<1,1>

                IF CURR EQ 'EGP' THEN
                    LOCAL.AMT = LOCAL.AMT.1
                END
                ELSE
                    LOCAL.AMT = LOCAL.AMT.2
                END

                R.NEW(CBE.PAY.CUR.CODE) = CURR
                R.NEW(CBE.PAY.AMT)      = LOCAL.AMT

            END

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
