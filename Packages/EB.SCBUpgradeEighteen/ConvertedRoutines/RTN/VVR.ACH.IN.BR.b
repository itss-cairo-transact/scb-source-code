* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
***----NOHA----***
*-----------------------------------------------------------------------------
* <Rating>95</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ACH.IN.BR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    IF MESSAGE NE 'VAL' THEN
*Line [ 42 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.ACH.PACS008':@FM:PACS8.AMOUNT,COMI,ACH.AMT)
F.ITSS.SCB.ACH.PACS008 = 'F.SCB.ACH.PACS008'
FN.F.ITSS.SCB.ACH.PACS008 = ''
CALL OPF(F.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008)
CALL F.READ(F.ITSS.SCB.ACH.PACS008,COMI,R.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008,ERROR.SCB.ACH.PACS008)
ACH.AMT=R.ITSS.SCB.ACH.PACS008<PACS8.AMOUNT>
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.ACH.PACS008':@FM:PACS8.TRN.DATE,COMI,TRN.DATE)
F.ITSS.SCB.ACH.PACS008 = 'F.SCB.ACH.PACS008'
FN.F.ITSS.SCB.ACH.PACS008 = ''
CALL OPF(F.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008)
CALL F.READ(F.ITSS.SCB.ACH.PACS008,COMI,R.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008,ERROR.SCB.ACH.PACS008)
TRN.DATE=R.ITSS.SCB.ACH.PACS008<PACS8.TRN.DATE>
*Line [ 56 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.ACH.PACS008':@FM:PACS8.REMITTANCE.INFO1,COMI,BILL.NO)
F.ITSS.SCB.ACH.PACS008 = 'F.SCB.ACH.PACS008'
FN.F.ITSS.SCB.ACH.PACS008 = ''
CALL OPF(F.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008)
CALL F.READ(F.ITSS.SCB.ACH.PACS008,COMI,R.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008,ERROR.SCB.ACH.PACS008)
BILL.NO=R.ITSS.SCB.ACH.PACS008<PACS8.REMITTANCE.INFO1>
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.ACH.PACS008':@FM:PACS8.PURPOSE,COMI,PURPOSE)
F.ITSS.SCB.ACH.PACS008 = 'F.SCB.ACH.PACS008'
FN.F.ITSS.SCB.ACH.PACS008 = ''
CALL OPF(F.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008)
CALL F.READ(F.ITSS.SCB.ACH.PACS008,COMI,R.ITSS.SCB.ACH.PACS008,FN.F.ITSS.SCB.ACH.PACS008,ERROR.SCB.ACH.PACS008)
PURPOSE=R.ITSS.SCB.ACH.PACS008<PACS8.PURPOSE>

        IF PURPOSE EQ 'COLL' THEN
            IF TRN.DATE NE '' THEN ETEXT = '����� �� ������� �� ���' ; RETURN
            IF ACH.AMT EQ '' THEN  ETEXT = '��� �� ��� �������' ; RETURN

            IF ACH.AMT NE '' THEN
                R.NEW(SCB.BT.OUR.REFERENCE)<1,1> =  BILL.NO
                R.NEW(SCB.BT.RESERVED.FIELD10) = ACH.AMT
                CALL REBUILD.SCREEN
            END
        END ELSE
            ETEXT = '��� ������� ���� �����' ; RETURN
        END
    END
    RETURN
END
