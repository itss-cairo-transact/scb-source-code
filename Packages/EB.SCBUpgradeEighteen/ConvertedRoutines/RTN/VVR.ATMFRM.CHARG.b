* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATMFRM.CHARG
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID

    IF COMI THEN

        IF COMI[4] EQ 'Free' THEN
            R.NEW(FT.CHARGES.ACCT.NO) = 'EGP1124300010099'
            R.NEW(FT.CHARGE.TYPE)<1,1> = 'ATM123CHRG'
        END

        IF COMI[3] EQ '123' THEN
            R.NEW(FT.CHARGES.ACCT.NO) = 'EGP1124300010099'
            R.NEW(FT.CHARGE.TYPE)<1,1> = 'ATM123CHRG'
*            R.NEW(FT.CHARGE.TYPE)<1,2> = 'ATMSCBCHRG'
        END

        IF COMI[5] EQ 'Local' THEN
            R.NEW(FT.CHARGE.TYPE)<1,1> = 'ATM123CHLOC'
            R.NEW(FT.CHARGE.TYPE)<1,2> = 'ATMSCBCHLOC'
        END


        IF COMI[3] EQ 'Int' THEN
            R.NEW(FT.CHARGE.TYPE)<1,1> = 'ATMSCBINTR'
            R.NEW(FT.COMMISSION.TYPE)<1,1> = 'ATMSCBINTRC'
        END

        CALL REBUILD.SCREEN
    END
    RETURN
END
