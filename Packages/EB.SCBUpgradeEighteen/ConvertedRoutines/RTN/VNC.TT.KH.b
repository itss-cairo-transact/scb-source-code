* @ValidationCode : MjoxNDgwNTEyMTQ3OkNwMTI1MjoxNjQ0OTMzMzU5MjQ2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:55:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
*---------WAEL 15/07/2002------*

SUBROUTINE VNC.TT.KH

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER

*=====================================================================================

    IF V$FUNCTION ='I' THEN
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR('TELLER$NAU':@FM:TT.TE.INPUTTER, ID.NEW ,DUMMY)
        F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
        FN.F.ITSS.TELLER$NAU = ''
        CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
        CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
        DUMMY=R.ITSS.TELLER$NAU<TT.TE.INPUTTER>

        IF NOT(ETEXT) THEN

            GOSUB INIT
            GOSUB PR2
            GOSUB PR3

        END
*=====================================================================================
INIT:

        X = ''
        DEPT.CODE = ''
        XX = ''
        XP = ''
        Y = ''


        RETURN

*==============================================================================
PR2:
        IF R.NEW(TT.TE.TRANSACTION.CODE) # 12 THEN
            E='TRANSACTION.CODE SHOULD BE EQUAL 12'
            CALL ERR ; MESSAGE = 'REPEAT';
        END

        RETURN
*==============================================================================
PR3:
        X = FIELD(R.NEW(TT.TE.INPUTTER),'_', 2)
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*              CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,X,DEPT.CODE)
        F.ITSS.USER = 'F.USER'
        FN.F.ITSS.USER = ''
        CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
        CALL F.READ(F.ITSS.USER,X,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
        DEPT.CODE=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>

        IF R.USER<EB.USE.DEPARTMENT.CODE> # DEPT.CODE THEN
            E = 'THIS USER FROM ANOTHER BRANCH'
            CALL ERR ;MESSAGE = 'REPEAT'
        END
        RETURN
*==============================================================================
    END

RETURN
END
