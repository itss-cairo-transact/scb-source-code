* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>344</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.COMM

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK

    IF MESSAGE = '' THEN

**        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> THEN
        IF COMI THEN
            CURR     = R.NEW(EB.BILL.REG.CURRENCY)
            COM.TYPE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>
            BILL.AMT = R.NEW(EB.BILL.REG.AMOUNT)

            FN.COMM  = 'FBNK.FT.COMMISSION.TYPE';F.COMM='';R.COMM = '';E1=''
            CALL OPF(FN.COMM,F.COMM)
            CALL F.READ(FN.COMM,COM.TYPE, R.COMM, F.COMM ,E1)

            COM.CUR = R.COMM<FT4.CURRENCY>
            COM.PER = R.COMM<FT4.PERCENTAGE>
            COM.MIN = R.COMM<FT4.MINIMUM.AMT>
            COM.MAX = R.COMM<FT4.MAXIMUM.AMT>

            IF CURR NE "USD" THEN

                FN.CUR  = 'FBNK.CURRENCY';F.CUR='';R.CUR = '';E11=''
                CALL OPF(FN.CUR,F.CUR)

                CALL F.READ(FN.CUR,CURR, R.CUR, F.CUR ,E11)
                CUR.RATE  = R.CUR<EB.CUR.MID.REVAL.RATE,1>

                CALL F.READ(FN.CUR,"USD", R.CUR1, F.CUR ,E11)
                CUR.RATE1 = R.CUR1<EB.CUR.MID.REVAL.RATE,1>

                COM.AMT   = ((BILL.AMT * CUR.RATE1 ) / CUR.RATE )* COM.PER / 100

                BEGIN CASE
                CASE COM.AMT LE COM.MIN
                    COMM.AMTT = COM.MIN
                CASE COM.AMT GE COM.MAX
                    COMM.AMTT = COM.MAX
                CASE  OTHERWISE
                    COMM.AMTT = (BILL.AMT * COM.PER) / 100
                END CASE

            END ELSE

                COM.AMT =  (BILL.AMT * COM.PER) / 100

                BEGIN CASE

                CASE COM.AMT LE COM.MIN
                    COMM.AMTT = COM.MIN
                CASE COM.AMT GE COM.MAX
                    COMM.AMTT = COM.MAX
                CASE  OTHERWISE
                    COMM.AMTT = COM.AMT
                END CASE

            END
            CALL EB.ROUND.AMOUNT ('USD',COMM.AMTT,'',"")
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = CURR:COMM.AMTT

        END
        CALL REBUILD.SCREEN
    END
    RETURN
*----------------------------------------------------------------------*
END
