* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
* DEFAULT VERSION NAME TO PREVENT
* USER TO OPEN OR AUTHORISE
* FROM ANOTHER VERSION

    SUBROUTINE VNC.SF.RIGHT.VER.NAME

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP.RIGHT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*----------------------------------------------------------
    IF V$FUNCTION = 'I' THEN
        R.NEW(SCB.SAF.RIG.VERSION.NAME) = PGM.VERSION

*------- 2011/03/31 ---
        REC.ID = ID.NEW
        CUS.ID = FIELD(REC.ID,'.',1)
        SF.ID  = FIELD(REC.ID,'.',2)

        FN.SF = "F.SCB.SAFEKEEP"  ; F.SF = ""
        CALL OPF(FN.SF,F.SF)

        CALL F.READ(FN.SF,SF.ID,R.SF,F.SF,E.SF)
        USED.FLG = R.SF<SCB.SAF.SAFF.USED>
        CUST.SF  = R.SF<SCB.SAF.CUSTOMER.NO>

        IF USED.FLG EQ 'YES' AND (CUS.ID NE CUST.SF) THEN
            E = "ALREADY USED FOR " : CUST.SF
            CALL ERR ; MESSAGE = 'REPEAT'
        END
*---
    END
*-----------------------------------------------------------
    RETURN
END
