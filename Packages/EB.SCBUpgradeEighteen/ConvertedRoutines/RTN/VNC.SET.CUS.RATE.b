* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*** WAGDY ***
    SUBROUTINE VNC.SET.CUS.RATE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LI.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COUNTRY.LOCAL.REFS


*    IF V$FUNCTION = "I" THEN
        XCUS = FIELD(ID.NEW,".",1)
*        TEXT = XCUS ; CALL REM
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CUSTOMER': @FM:EB.CUS.LOCAL.REF,XCUS,XXXX)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,XCUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
XXXX=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUSRATE =  XXXX<1,CULR.RATEING>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CUSTOMER': @FM:EB.CUS.NATIONALITY,XCUS,NATI)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,XCUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
NATI=R.ITSS.CUSTOMER<EB.CUS.NATIONALITY>
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('COUNTRY': @FM:EB.COU.LOCAL.REF,NATI,YYY)
F.ITSS.COUNTRY = 'F.COUNTRY'
FN.F.ITSS.COUNTRY = ''
CALL OPF(F.ITSS.COUNTRY,FN.F.ITSS.COUNTRY)
CALL F.READ(F.ITSS.COUNTRY,NATI,R.ITSS.COUNTRY,FN.F.ITSS.COUNTRY,ERROR.COUNTRY)
YYY=R.ITSS.COUNTRY<EB.COU.LOCAL.REF>
        CONRATE = YYY<1,CUNT.RATEING>
 *        TEXT = CUSRATE :"  " : CONRATE ; CALL REM
        R.NEW(LI.LOCAL.REF)<1,LILR.CUSTOMER.RATING> = CUSRATE
        R.NEW(LI.LOCAL.REF)<1,LILR.COUNTRY.RATING> = CONRATE

 *   END
    RETURN
END
