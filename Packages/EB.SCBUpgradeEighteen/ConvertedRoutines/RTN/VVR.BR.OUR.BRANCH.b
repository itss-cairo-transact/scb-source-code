* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-3</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.OUR.BRANCH

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    $INSERT            I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> EQ '' THEN

        BB=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR>
        VV = COMI[5,2]
        BRN.CODE  = C$ID.COMPANY[8,2]
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ '0017' AND COMI NE '1700':BRN.CODE THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = 994999 : VV : 10 : 501001
        END
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE '0017' THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = "9943330010508001"
        END
        CALL REBUILD.SCREEN
    END
****************************NI7OOOOOOOOOOOO****************8
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR> # COMI THEN
*  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE> = ''
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
* R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.STATUS.DATE>  = ''
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
* R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MAT.DATE>     = ''
        CALL REBUILD.SCREEN
    END
***********************************************************

    RETURN
***********************************************
END
