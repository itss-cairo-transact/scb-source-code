* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
** ----- 21.06.2018 NESSREEN AHMED -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.ATM.NO.FREE.TR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.FEE.EXCP

    NO.FREE.TR = ''
    ETEXT = ''
    IF COMI THEN
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR( 'SCB.CUS.FEE.EXCP':@FM:FEE.FREE.123WITHDRAWAL, COMI, NO.FREE.TR)
F.ITSS.SCB.CUS.FEE.EXCP = 'F.SCB.CUS.FEE.EXCP'
FN.F.ITSS.SCB.CUS.FEE.EXCP = ''
CALL OPF(F.ITSS.SCB.CUS.FEE.EXCP,FN.F.ITSS.SCB.CUS.FEE.EXCP)
CALL F.READ(F.ITSS.SCB.CUS.FEE.EXCP,COMI,R.ITSS.SCB.CUS.FEE.EXCP,FN.F.ITSS.SCB.CUS.FEE.EXCP,ERROR.SCB.CUS.FEE.EXCP)
NO.FREE.TR=R.ITSS.SCB.CUS.FEE.EXCP<FEE.FREE.123WITHDRAWAL>
        R.NEW(SCB.VISA.NO.FREE.TRANS) =  NO.FREE.TR
        CALL REBUILD.SCREEN
    END
    IF MESSAGE EQ "VAL" THEN
        IF COMI  = '' THEN
* IF R.NEW(SCB.VISA.PAYROLL.CUST) = ''
            IF R.NEW(SCB.VISA.CARD.TYPE) GE 901 THEN
                ETEXT = '��� ����� ��� ������'
                CALL STORE.END.ERROR
            END
        END
    END

    RETURN
END
