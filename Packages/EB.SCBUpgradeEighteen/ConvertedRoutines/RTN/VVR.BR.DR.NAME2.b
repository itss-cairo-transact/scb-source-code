* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>49</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.DR.NAME2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT

    IF MESSAGE NE 'VAL' THEN
        IF R.NEW(EB.BILL.REG.DRAWER) EQ COMI THEN
            ETEXT = '��� �������� �� ��� ������� ����'
        END ELSE

            FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CU.AC= ''
            CALL OPF(FN.CU.AC,F.CU.AC)

            FN.ACC ='FBNK.ACCOUNT' ; R.ACC=''; F.ACC=''
            CALL OPF(FN.ACC,F.ACC)

            CALL F.READ(FN.CU.AC,COMI,R.CU.AC,F.CU.AC,ERR1)
            CURR    = R.NEW(EB.BILL.REG.CURRENCY)
            LOOP
                REMOVE ACC FROM R.CU.AC  SETTING POS1
            WHILE ACC:POS1
                CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
                AC.CURR    = R.ACC<AC.CURRENCY>
                AC.CATEG   = R.ACC<AC.CATEGORY>
                IF (CURR EQ AC.CURR AND AC.CATEG EQ '1001' ) THEN
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = ""
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = ACC
                    RETURN
                END
            REPEAT
*--------------------------
*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,COMI,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            IF LOCAL.REF THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.DR.NAME>    = LOCAL.REF<1,CULR.ARABIC.NAME>
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.DR.ADDRESS> = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
            END
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
