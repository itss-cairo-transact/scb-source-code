* @ValidationCode : MjotMTMzNDg2OTgyMjpDcDEyNTI6MTY0NDkzMzMxNzM0NDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:55:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
**------------10.07.2002 DALIA AHMED----------*

SUBROUTINE VNC.TELLER.DEPT.CODE
*THE VERSION HAS ZERO AUTHORIZED BUT IT CAN PUT IN IHLD STATUS (IHLD)
*THE ROUTINE TO CHECK IF THE OPERATION IN TELLER$NAU (IHLD) THEN
*IF THE DEPARTMENT.CODE OF THE INPUTTER NOT EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*DISPLAY ERROR
*IF THE DEPARTMENT.CODE OF THE INPUTTER  EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
* CHECK IF THE TRANSACTION.CODE NOT EQUAL '23' DISPLAY ERROR

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER

    Y=''
    INPUTTER.CODE=''

    IF V$FUNCTION='I' THEN
        X = FIELD(R.NEW(TT.TE.INPUTTER),'_', 2)

*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('TELLER$NAU':@FM:TT.TE.TELLER.ID.1, ID.NEW ,Y)
        F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
        FN.F.ITSS.TELLER$NAU = ''
        CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
        CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
        Y=R.ITSS.TELLER$NAU<TT.TE.TELLER.ID.1>
        IF NOT(ETEXT) THEN
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,X,INPUTTER.CODE)
            F.ITSS.USER = 'F.USER'
            FN.F.ITSS.USER = ''
            CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
            CALL F.READ(F.ITSS.USER,X,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
            INPUTTER.CODE=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
   
            IF R.USER<EB.USE.DEPARTMENT.CODE> # INPUTTER.CODE THEN
                E = 'THIS.OPER.FROM.OTHER.BRANCH'
                CALL ERR ; MESSAGE = 'REPEAT';
            END ELSE

                IF R.NEW(TT.TE.TRANSACTION.CODE) # 23 THEN
                    E='WRONG.TRANSACTION.CODE'
                    CALL ERR ; MESSAGE = 'REPEAT';
                END
            END
        END
    END
RETURN
END
