* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.MANDATE.REF

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.TEL.SERVICE

* Created by Noha Hamed

    COMP.CODE=ID.COMPANY
    COMP= COMP.CODE[7,3]


    IF V$FUNCTION = 'I'  THEN
        ZEROS=''
        T.SEL="SELECT FBNK.SCB.CUST.TEL.SERVICE"
        CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)
        NO.OF.RECORDS=SELECTED+1
        MANDATE.REF='SUZ':COMP
        LEN.NO=LEN(NO.OF.RECORDS)
        FOR I=LEN.NO TO 13
            ZEROS=ZEROS:'0'
        NEXT I
        MANDATE.REF=MANDATE.REF:ZEROS:NO.OF.RECORDS
        R.NEW(TEL.MANDATE.REF)=MANDATE.REF

    END
    RETURN
END
