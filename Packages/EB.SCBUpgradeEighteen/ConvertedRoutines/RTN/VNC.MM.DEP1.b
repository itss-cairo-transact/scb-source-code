* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.MM.DEP1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

    E = ''

    IF V$FUNCTION = 'I' THEN

*Line [ 32 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('MM.MONEY.MARKET':@FM:1,ID.NEW,CHK.ID)
F.ITSS.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET'
FN.F.ITSS.MM.MONEY.MARKET = ''
CALL OPF(F.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET)
CALL F.READ(F.ITSS.MM.MONEY.MARKET,ID.NEW,R.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET,ERROR.MM.MONEY.MARKET)
CHK.ID=R.ITSS.MM.MONEY.MARKET<1>
*Line [ 39 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('MM.MONEY.MARKET':@FM:MM.CATEGORY,ID.NEW,CATEG)
F.ITSS.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET'
FN.F.ITSS.MM.MONEY.MARKET = ''
CALL OPF(F.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET)
CALL F.READ(F.ITSS.MM.MONEY.MARKET,ID.NEW,R.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET,ERROR.MM.MONEY.MARKET)
CATEG=R.ITSS.MM.MONEY.MARKET<MM.CATEGORY>
        IF NOT(CHK.ID) OR (CATEG NE 21030 AND CATEG NE 21031) THEN
            E = 'THE ID MUST BE EXIST OR DEPOSITS'
            CALL ERR ; MESSAGE = 'REPEAT'
        END
***        R.NEW(MM.MATURITY.DATE) = TODAY
    END

    RETURN
END
