* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SME.CHK.CREDIT.CODE

*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_COMMON
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_EQUATE
*Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 31 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.CUSTOMER
    $INSERT        I_FT.LOCAL.REFS
    $INSERT        I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

*------------------------------------------------
    CUS.NO = ID.NEW
    FN.USR = "F.USER"  ; F.USR =""
    CALL OPF(FN.USR, F.USR)

*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,CUS.DATA)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.DATA=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CREDIT.CODE = CUS.DATA<1,CULR.CREDIT.CODE>

    IF (CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120) THEN
        USR = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,USR,USR.ID)
F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
FN.F.ITSS.USER.SIGN.ON.NAME = ''
CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,USR,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
USR.ID=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>

        CALL F.READ(FN.USR,USR.ID, R.USR, F.USR, E.USR)
        DEPT.CODE = R.USR<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>

        IF DEPT.CODE NE '9905' THEN
            E = '�� ���� ������� �� ��� ������' ; CALL ERR  ; MESSAGE = 'REPEAT'
        END
    END

*------------------------------------------------
    RETURN
END
