* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>646</Rating>
*-----------------------------------------------------------------------------

******** WAEL **********

    SUBROUTINE VRR.OFS.DATA

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    MYCODE = R.NEW(LD.LOCAL.REF)<1,LDLR.OPERATION.CODE>
    MYTYPE = R.NEW(LD.LOCAL.REF)<1,LDLR.PRODUCT.TYPE>
    CUST = R.NEW(LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
    ORDER.BNK = R.NEW(LD.CUSTOMER.ID)

*---------------
    MYID = MYCODE:'.':MYTYPE

    FN.LG = 'F.SCB.LG.CHARGE' ; F.LG = '' ; R.LG = ''
    CALL OPF(FN.LG,F.LG)
    CALL F.READ(FN.LG,MYID,R.LG,F.LG,E)
    PL.CODE = R.LG<SCB.LG.CH.PL.CATEGORY>


    GOSUB OFS_MAKE
RETURN 
*========================== ISSUE ==========================
OFS_MAKE:

IF MYCODE = '1111' THEN
*------------ MARGIN --------------

        TRANS.TYPE = "ACLG"
        DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
        DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
        DB.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
        CR.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
        ORDER.BNK = R.NEW(LD.CUSTOMER.ID)

        IF MYTYPE = "ADVANCE" THEN 
           ORDER.BNK = R.NEW(LD.CUSTOMER.ID)
           GOSUB TAX.MSG
        END ELSE 
           ORDER.BNK = R.NEW(LD.CUSTOMER.ID)
           GOSUB TAX.MSG

           TRANS.TYPE = "ACLG"
           DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
           DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
           DB.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
           CR.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>

           GOSUB PRODUCE.MSG
       END
END
*========================== OPERAION =============================
 IF MYCODE = '1251' THEN

         TRANS.TYPE = "ACLG"
         DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
         DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
         DB.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
         CR.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>

         GOSUB PRODUCE.MSG   
         GOSUB TAX.MSG
 END
                   *------------------------
IF MYCODE = '1252' THEN
 
          TRANS.TYPE = "ACLG"
          DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
          DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
          DB.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
          CR.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
          GOSUB PRODUCE.MSG
END
*================================ INWARD ===========================
    IF MYCODE = 2111 THEN GOSUB TAX.MSG
*============================== CANCEL =============================
    IF (MYCODE >= '1301' AND MYCODE <= '1303') THEN

* --------  MARGIN --------
        TRANS.TYPE = "ACLG"
        DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
        DB.AMT = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
        DB.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
        CR.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
        GOSUB PRODUCE.MSG
    END
*=========================== CONFISCATE ============================
    IF (MYCODE >= '1401' AND MYCODE <= '1403') THEN
*------------------
        CHK.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>

        T.SEL = "SELECT FBNK.CHEQUES.PRESENTED WITH @ID LIKE ":'TEST.':CHK.ACCT:"..."
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CHQ.NOM = 0
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                CURRENT = FIELD(KEY.LIST<I>,"-",2)
                IF CURRENT >= CHQ.NOM THEN CHQ.NOM = CURRENT +1
            NEXT I
        END
*R.NEW(LD.LOCAL.REF)< 1,LDLR.CHQEQUE.NO> ="TEST.":CHK.ACCT:".":CHQ.NOM
*-------------------
        IF R.NEW(LD.FIN.MAT.DATE) = TODAY THEN

            IF R.NEW(LD.LOCAL.REF)<1,LDLR.CONFISC.AMT> = R.NEW(LD.AMOUNT) THEN

* -------------- MARGIN ----------
                TRANS.TYPE = "ACLG"
                DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
                DB.AMT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
                DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
                CR.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                GOSUB PRODUCE.MSG
*-------------- DRAFT.CHEQUE ------
                TRANS.TYPE = "ACLG"
                DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
                DB.AMT = R.NEW(LD.AMOUNT)
                DB.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                CR.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
*CHQ.NO = CHQ.NOM
*BENF.CUST = LOCAL.REF<1,LDLR.IN.FAVOR.OF>

                GOSUB PRODUCE.MSG

            END ELSE
* -------------- MARGIN ----------
                TRANS.TYPE = "ACLG"
                DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
                DB.AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT>
                DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
                CR.ACCT   = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                GOSUB PRODUCE.MSG
*-------------- DRAFT.CHEQUE ------
                TRANS.TYPE = "ACLG"
                DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
                DB.AMT = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>
                DB.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                CR.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
*CHQ.NO = CHQ.NOM
*BENF.CUST = LOCAL.REF<1,LDLR.IN.FAVOR.OF>

                GOSUB PRODUCE.MSG

            END
        END ELSE
*---------- MARGIN -------------
            TRANS.TYPE = "ACLG"
            DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
            PER = R.NEW( LD.LOCAL.REF)<1,LDLR.MARGIN.PERC>/100
            DB.AMT     = R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>*PER
            DB.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            CR.ACCT   = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT> = R.NEW(LD.LOCAL.REF)< 1,LDLR.MARGIN.AMT> - DB.AMT
            CALL REBUILD.SCREEN
            GOSUB PRODUCE.MSG
*---------------- DRAFT.CHEQUE ----------
            TRANS.TYPE = "ACLG"
            DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
            DB.AMT = ABS(R.NEW(LD.LOCAL.REF)< 1,LDLR.CONFISC.AMT>)
            DB.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            CR.ACCT  = R.NEW(LD.LOCAL.REF)< 1,LDLR.CR.NOSTRO.ACCT>
*  CHQ.NO = "TEST.":CHK.ACCT:".":CHQ.NOM
*  CHQ.NO = CHQ.NOM
* BENF.CUST = LOCAL.REF<1,LDLR.IN.FAVOR.OF>

            GOSUB PRODUCE.MSG

        END


    END
*============================ AMENDEMENT ===================
    IF MYCODE = '1241' OR MYCODE = '1242' OR (MYCODE >= '1231' AND MYCODE <= '1235') THEN
*************
  * T.SEL = "SSELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":ID.NEW:";..."
  * CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

  * FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = ''
  * CALL OPF(FN.LD,F.LD)
  * CALL F.READ(FN.LD,ID.NEW:";":SELECTED,R.LD,F.LD,E1)

  * LOCAL.REF = R.LD<LD.LOCAL.REF>
  * OLD.MARG = LOCAL.REF<1,LDLR.MARGIN.AMT>
   OLD.MARG = R.OLD(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
   NEW.MARG = R.NEW(LD.LOCAL.REF)<1,LDLR.MARGIN.AMT>
*************
*------------- MARGIN ------------
        IF NEW.MARG > OLD.MARG THEN
            TRANS.TYPE = "ACLG"
            DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
            DB.AMT = NEW.MARG - OLD.MARG
            DB.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>
            CR.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            ORDER.BANK = ID.NEW

             GOSUB PRODUCE.MSG
        END ELSE
            TRANS.TYPE = "ACLG"
            DB.CUR = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
            DB.AMT = OLD.MARG - NEW.MARG
            ORDER.BANK = ID.NEW
            DB.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
            CR.ACCT = R.NEW( LD.LOCAL.REF)< 1,LDLR.CREDIT.ACCT>


            GOSUB PRODUCE.MSG
        END

    END

    IF MYCODE = '1271' THEN GOSUB TAX.MSG
    RETURN
*===========================================================
TAX.MSG:
*--------------- TAX  ----------------
*Line [ 246 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
TAX.CONT = DCOUNT(R.LG<SCB.LG.CH.TAX.AMOUNT>, @VM)
    IF TAX.CONT >= 1 THEN
        FOR I = 1 TO TAX.CONT

            MYTAX.ACC1 = R.LG<SCB.LG.CH.TAX.ACCOUNT,I>
            MYTAX1 = R.LG<SCB.LG.CH.TAX.AMOUNT,I>

            IF MYTAX1 > 0 THEN

*Line [ 256 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST,CUST.BRN)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.BRN=R.ITSS.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
              *  IF CUST.BRN = 1 THEN
                  *  CR.ACCT = MYTAX.ACC1
              *  END ELSE
                  *  IF LEN (CUST.BRN)>1 THEN MYTAXBRN[9,2]=CUST.BRN
                  *  IF LEN (CUST.BRN)=1 THEN MYTAXBRN[9,2]="0":CUST.BRN
                  *  MYTAXBRN = MYTAX.ACC1[1,9]:CUST.BRN:MYTAX.ACC1[11,2]
                  *  CR.ACCT = MYTAXBRN
              *  END

                TRANS.TYPE = "ACLG"
                DB.CUR = "EGP"
                DB.AMT = MYTAX1
                CR.ACCT = MYTAX.ACC1 
                ORDER.BANK = R.NEW(LD.CUSTOMER.ID)

                IF MYCODE = 2111 THEN
                    CR.ACCT = MYTAX.ACC1
                    DB.ACCT = PL.CODE
                    PROFIT.DEPT = "1" 
                    ORDER.BANK = ORDER.BNK
                END ELSE 
                   * ORDER.BANK = R.NEW(LD.CUSTOMER.ID)
                     ORDER.BANK = ID.NEW
                    DB.ACCT = R.NEW(LD.LOCAL.REF)< 1,LDLR.DEBIT.ACCT>
                END

                GOSUB PRODUCE.MSG
            END
NEXT I
END

RETURN
*-----------------------------
PRODUCE.MSG:

    PROFT.CUST = R.NEW(LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
    DB.DATE = R.NEW(LD.VALUE.DATE)
    CR.DATA = R.NEW(LD.VALUE.DATE)
*Line [ 295 ] Adding EB.SCBUpgradeEighteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEighteen.VRR.LG.OFS( TRANS.TYPE,DB.CUR,DB.AMT,PROFT.CUST,DB.DATE,CR.DATA,DB.ACCT,CR.ACCT,CHQ.NO,BENF.CUST,PROFIT.DEPT,ORDER.BANK)

    RETURN
*-------------------------
END
