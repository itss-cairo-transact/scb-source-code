* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
** ----- 03.06.2002 MOHAMED.MAHMOUD -----

    SUBROUTINE VNC.SCB.AC.VER.NAME
*OLD NAME :VNC.AC.VER.NAME

*IN THE INPUT FUNCTION MADE THE FIELD VERSION.NAME  EQUAL THE DEFAULT (PGM.VERSION)
*CHECK IF VERSION.NAME NE PGM.VERSION PRODUCE ERROR MSG
*E MSG INCLUDE & THAT REFER TO  THE RIGHT VERSION NAME

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*****************
    IF PGM.VERSION EQ ",SCB.CUSTAC.UPDATE" THEN
        FN.AC.NAU = "FBNK.ACCOUNT$NAU"
        F.AC.NAU  = ""

        CALL OPF(FN.AC.NAU,F.AC.NAU)
        CALL F.READ(FN.AC.NAU,ID.NEW,R.AC.NAU,F.AC.NAU,ETEXT.NAU)
********
        IF ETEXT.NAU THEN
            FN.AC ="FBNK.ACCOUNT"
            F.AC = ""

            CALL OPF(FN.AC,F.AC)
            CALL F.READ(FN.AC,ID.NEW,R.AC,F.AC,ETEXT)
            IF ETEXT THEN E = "������ ��� �����" ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    END
*****************
    IF V$FUNCTION ='I'  THEN
        VERSION.NAME =  R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME>
        IF NOT(VERSION.NAME) THEN
            R.NEW( AC.LOCAL.REF)< 1, ACLR.VERSION.NAME> = PGM.VERSION
        END

    END

    RETURN
END
