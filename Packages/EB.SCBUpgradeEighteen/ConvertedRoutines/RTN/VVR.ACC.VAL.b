* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*----------------MAI-------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  VVR.ACC.VAL
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER

    COMP = C$ID.COMPANY
    ETEXT = '' ; ACC.NO = ''

    IF COMI THEN
        F.ATM.AC = '' ; FN.ATM.AC = 'F.SCB.ATM.TERMINAL.ID' ; R.ATM.AC = '' ; E1 = '' ; RETRY1 = ''
        CALL OPF(FN.ATM.AC,F.ATM.AC)

        ATM.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.NUMBER>
        TEXT = 'ATM.NO=':ATM.NO ; CALL REM

*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE, COMI,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,COMI,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

        CALL F.READ(FN.ATM.AC,ATM.NO, R.ATM.AC, F.ATM.AC, E1)
        IF COMI EQ 'EGP' THEN
            ATM.ACC.NO = R.ATM.AC<SCB.ATM.ACCOUNT.NUMBER>

            R.NEW(FT.DEBIT.ACCT.NO) = ATM.ACC.NO
            TEXT = 'ATM.ACC.NO=':ATM.ACC.NO ; CALL REM
        END ELSE
              R.NEW(FT.DEBIT.ACCT.NO)=''
        END
        R.NEW(FT.CREDIT.ACCT.NO) = 'EGP1139400010099'

        CALL REBUILD.SCREEN
    END

    RETURN
END
