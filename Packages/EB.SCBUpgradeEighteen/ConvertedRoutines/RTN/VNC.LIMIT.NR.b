* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.LIMIT.NR

* PREVENT USER FROM MODIFYING
* AN EXSISTING RECORD OR CUSTOMER
* THAT IS A BANK
* OPEN THE RECORD FORM THE VERSION
* IT WAS CREATED
* DEFALUT COUNTRY.RISK TO "EG"
* DEFALUT COUNTRY.PERCENT TO "100"


*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT.REFERENCE

    IF V$FUNCTION = 'I' THEN
        WW = FIELD(ID.NEW,'.',2)
        LIM.REF=TRIM(WW,"0","L")
*        TEXT =" REF ": LIM.REF; CALL REM
*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('LIMIT.REFERENCE':@FM:LI.REF.REDUCING.LIMIT,LIM.REF,RE)
F.ITSS.LIMIT.REFERENCE = 'F.LIMIT.REFERENCE'
FN.F.ITSS.LIMIT.REFERENCE = ''
CALL OPF(F.ITSS.LIMIT.REFERENCE,FN.F.ITSS.LIMIT.REFERENCE)
CALL F.READ(F.ITSS.LIMIT.REFERENCE,LIM.REF,R.ITSS.LIMIT.REFERENCE,FN.F.ITSS.LIMIT.REFERENCE,ERROR.LIMIT.REFERENCE)
RE=R.ITSS.LIMIT.REFERENCE<LI.REF.REDUCING.LIMIT>
*       TEXT =" RE": RE ; CALL REM
        IF RE EQ "Y" THEN
****     E = 'THIS IS NOT Reducing Limit ' ; CALL ERR ;MESSAGE = 'REPEAT'
        END
    END
    RETURN
END
