* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>359</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AMT.BALANCE

*1-IF THE INPUT VALUE IS NOT EQ DEBIT AMOUNT THEN MAKE CREDIT.AMOUNT IS EMPTY
*2-CHECK IF WORKING.BALANCE HAVE A CREDIT BALANCE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    IF R.NEW(FT.DEBIT.ACCT.NO)[1,2] NE 'PL' THEN
        IF MESSAGE EQ '' THEN
            CU.NO = R.NEW(FT.DEBIT.CUSTOMER)
*******UPDATED BY NESSREEN AHMED 28/5/2008******
**   CATEG = R.NEW(FT.DEBIT.ACCT.NO)[11,4]
            ACT = R.NEW(FT.DEBIT.ACCT.NO)
*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACT,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
            IF (CATEG NE 5001) AND (CATEG NE 10101) THEN
**  IF CATEG NE 5001 THEN
                AC = R.NEW(FT.DEBIT.ACCT.NO)
*            IF CATEG EQ 1202 OR CATEG EQ 1201 THEN
                IF (CATEG GE 1101 AND CATEG LE 1590 ) THEN
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,AC,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
                    IF LEN(LIM.REF) EQ '6' THEN
                        LIM  = CU.NO :".":"0000":LIM.REF
                    END ELSE
                        LIM  = CU.NO :".":"00":LIM.REF
                    END
                    TEXT = "LIM":LIM ; CALL REM
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM,AVL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVL=R.ITSS.LIMIT<LI.AVAIL.AMT>
                    TEXT = "AVL":AVL ; CALL REM
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                    AMT  = COMI
*TEXT = "COMI" : COMI ; CALL REM
*TEXT = "W.BALANCE" : WORK.BALANCE ; CALL REM
                    ZERO = "0.00"
                    BAL  =  WORK.BALANCE + AVL
*TEXT = "BAL" : BAL ; CALL REM
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
                    TEXT = "LOOK.AMT" : LOCK.AMT ; CALL REM
*Line [ 67 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    LOCK.NO=DCOUNT(LOCK.AMT,@VM)
*TEXT = "LOCK.NO" : LOCK.NO ; CALL REM
                    BLOCK =LOCK.AMT<1,LOCK.NO>
*TEXT = "BLOCK" : BLOCK ; CALL REM

                    BAL.BLOCK = BAL - BLOCK
                    NET.BAL = BAL.BLOCK - AMT
*TEXT = "NET.BAL" : NET.BAL ; CALL REM

*************MAHMOUD 15/6/2009 *********************
*                IF NET.BAL LT ZERO THEN
*                    E = 'AMOUNT IS NOT AVILABLE' ; CALL ERR; MESSAGE='REPEAT'
*                END
****************************************************

                END ELSE
********************************
*Line [ 115 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,AC,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                    AMT  = COMI
*TEXT = "COMI" : COMI ; CALL REM
*TEXT = "W.BALANCE" : WORK.BALANCE ; CALL REM
                    ZERO = "0.00"
                    BAL  =  WORK.BALANCE
*TEXT = "BAL" : BAL ; CALL REM

*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,AC,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 94 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    LOCK.NO=DCOUNT(LOCK.AMT,@VM)
                    BLOCK =LOCK.AMT<1,LOCK.NO>
*TEXT = "BLOCK" : BLOCK ; CALL REM
                    BAL.BLOCK = BAL - BLOCK
                    NET.BAL = BAL.BLOCK - AMT
*TEXT = "NET.BAL" : NET.BAL ; CALL REM

*************MAHMOUD 15/6/2009 *********************
*                IF NET.BAL LT ZERO THEN
*                    E = 'AMOUNT IS NOT AVILABLE' ; CALL ERR; MESSAGE='REPEAT'
*                END
****************************************************
                END
            END
**********************************
        END
    END
    RETURN
END
