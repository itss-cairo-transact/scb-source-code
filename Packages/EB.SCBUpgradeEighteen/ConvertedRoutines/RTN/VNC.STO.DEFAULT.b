* @ValidationCode : MjotNTg4MjkxNjQzOkNwMTI1MjoxNjQ0OTMyMDQ0NDQ2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:34:04
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
****** UPDATED BY INGY*****

SUBROUTINE VNC.STO.DEFAULT

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STANDING.ORDER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT


    IF V$FUNCTION = 'I' THEN

        ID = FIELD(ID.NEW, ".", 1)
*Line [ 38 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*         CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY, ID, CURR.NAME)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CURR.NAME=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 45 ] Intialize "EB.NCN.CURRENCY.CODE" - ITSS - R21 Upgrade - 2022-02-15
        EB.NCN.CURRENCY.CODE = ''
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*       * CALL DBR( 'NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE, CURRENCY, CURR.NAME )
        F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
        FN.F.ITSS.NUMERIC.CURRENCY = ''
        CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
        CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CURRENCY,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
        CURR.NAME=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>

        IF CURR.NAME # LCCY THEN
            E= 'Only.Local.Currency' ;CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            CUS = TRIM (ID.NEW[5,7], '0', 'L')
*Line [ 59 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUS,XXX)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            XXX=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>
            R.NEW(STO.ORDERING.CUST) = XXX
        END

    END
RETURN
END
