* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>93</Rating>
*-----------------------------------------------------------------------------
** ----- 17.06.2002 MOHAMED MAHMOUD (MRA GROUP) -----

    SUBROUTINE VVR.AC.HOLD.EXPIRY.DATE
*A ROUTINE 1- TO CHECK THAT EXPIRY.DATE IS GREATER THAN TODAY AND HOLD.DATE.
*          2- TO CHECK THAT INPUTTERS FROM SAME DEPARTMENTS ONLY ARE ALLOWED TO CHANGE THE RECORD.
*          3- TO CHECK THAT EXPIRY.DATE IS MANDATORY IF HOLD.DATE IS ENTERED

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*---- (1)
    IF MESSAGE NE 'VAL' THEN

        DEPT = R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER,AS>
        DEPT = FIELD(DEPT,'-',2,1)
        DEPT.NAME = ''
        IF DEPT # '' THEN
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            IF  DEPT NE DEPT.NAME THEN
                R.NEW(AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE,AS>=R.OLD(AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE,AS>
* ETEXT = 'You.Not.Authorise.To.Update.This.Record (&)'
                ETEXT = '��� �� ������ �� ���� �� ��� ����� (&)' : @FM : DEPT
                CALL STORE.END.ERROR
            END
        END
        IF COMI THEN
* TEXT ="COMI" ; CALL REM
            IF COMI LT TODAY THEN
                R.NEW(AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE,AS>=R.OLD(AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE,AS>
                ETEXT = 'The.Date.Must.Be.GE.Today.&.Hold.Date'
                ETEXT = '������� ���� �� ���� ���� �� �� ����� ����� ����� � ����� �����'
                CALL STORE.END.ERROR
            END
*---- (2)
        END ELSE
* ETEXT = 'You.Must.Enter.Expiry.Date'
            COMI='20991231'
          *  ETEXT = '���� �� ����� ����� ������ ������'
          *  CALL STORE.END.ERROR

        END
    END ELSE

*---- (3)
        XX = R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER>
* TEXT = "XX = ":XX ; CALL REM
*Line [ 75 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        FOR II = 1 TO DCOUNT(R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.DATE>,@SM)
            IF R.NEW(AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE,II>='' THEN YY = 1
        NEXT II
        IF YY = 1 THEN
*   ETEXT = 'You.Must.Enter.Expiry.Date'
            ETEXT = '���� �� ����� ����� ������ ������'
            CALL STORE.END.ERROR
        END
    END
    RETURN
END
