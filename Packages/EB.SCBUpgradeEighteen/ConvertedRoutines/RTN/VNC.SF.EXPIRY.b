* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VNC.SF.EXPIRY

*Line [ 18 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_COMMON
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_EQUATE
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-15
$INSERT I_F.ACCOUNT
    $INSERT           I_F.SCB.SF.EXPIRY
*-----------------------------------------
    IF V$FUNCTION = 'I' THEN
        IF R.NEW(SF.EXP.VERSION.NAME) = '' THEN
            R.NEW(SF.EXP.VERSION.NAME) = PGM.VERSION
        END

        EXP.ID = ID.NEW
        SEP    = EXP.ID[17,1]
        IF SEP NE '-' THEN
            E = "MUST WRITE "-" BETWEEN ACCOUNT NO AND RENT DATE"
            CALL ERR ; MESSAGE = 'REPEAT'
        END

        ACCT.NO  = FIELD(EXP.ID,'-',1)
        EXP.DATE = FIELD(EXP.ID,'-',2)

        FN.ACC = "FBNK.ACCOUNT"   ; F.ACC = ""
        CALL OPF(FN.ACC,F.ACC)
        CALL F.READ(FN.ACC,ACCT.NO,R.ACC,F.ACC,ERR)

        IF PGM.VERSION EQ ",INSERT.DATA" THEN
            IF ERR THEN
                E = "MISSING ACCOUNT"
                CALL ERR ; MESSAGE = 'REPEAT'
            END ELSE
                R.NEW(SF.EXP.ACCOUNT.NO) = ACCT.NO
                R.NEW(SF.EXP.RENEW.DATE) = EXP.DATE
                R.NEW(SF.EXP.RENT.DATE)  = EXP.DATE
            END
        END
        CALL REBUILD.SCREEN
*-----------------------------------------------
        RETURN
    END
