* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.ITEMS.R

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.ITEMS
*--------------------------------------
    COM.CODE = ID.COMPANY
    CO.CODE  = R.NEW(SCB.WH.IT.CO.CODE)
    IF CO.CODE NE '' THEN
        IF CO.CODE NE COM.CODE THEN
            E = '���� �� ��� �����' ;
*** SCB UPG 20160623 - S
* CALL ERR ; MESSAGE ='REPEAT'
*** SCB UPG 20160623 - E
            CALL STORE.END.ERROR
        END
    END
*---- 2011/03/01 BY NESSMA ----
    IF V$FUNCTION = "I" THEN
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.WH.ITEMS':@FM:0,ID.NEW,E.SER)
F.ITSS.SCB.WH.ITEMS = 'F.SCB.WH.ITEMS'
FN.F.ITSS.SCB.WH.ITEMS = ''
CALL OPF(F.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS)
CALL F.READ(F.ITSS.SCB.WH.ITEMS,ID.NEW,R.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS,ERROR.SCB.WH.ITEMS)
E.SER=R.ITSS.SCB.WH.ITEMS<0>
        IF E.SER EQ '' THEN
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('SCB.WH.ITEMS$NAU':@FM:0,ID.NEW,ERR)
F.ITSS.SCB.WH.ITEMS$NAU = 'F.SCB.WH.ITEMS$NAU'
FN.F.ITSS.SCB.WH.ITEMS$NAU = ''
CALL OPF(F.ITSS.SCB.WH.ITEMS$NAU,FN.F.ITSS.SCB.WH.ITEMS$NAU)
CALL F.READ(F.ITSS.SCB.WH.ITEMS$NAU,ID.NEW,R.ITSS.SCB.WH.ITEMS$NAU,FN.F.ITSS.SCB.WH.ITEMS$NAU,ERROR.SCB.WH.ITEMS$NAU)
ERR=R.ITSS.SCB.WH.ITEMS$NAU<0>
            IF ERR EQ '' AND PGM.VERSION EQ ',SCB.WH.ITEMS.UPDATE' THEN
                E = "RECORD NOT EXIST"
*** SCB UPG 20160623 - S
*                CALL ERR;MESSAGE='REPEAT'
*** SCB UPG 20160623 - E
                CALL STORE.END.ERROR
            END
        END
    END
*------------------------------
    IF V$FUNCTION = "R" THEN
        E  = "�� ���� �������"
*** SCB UPG 20160623 - S
*        CALL ERR ; MESSAGE='REPEAT'
*** SCB UPG 20160623 - E
        CALL STORE.END.ERROR
    END
*--------------------------------------
    RETURN
END
