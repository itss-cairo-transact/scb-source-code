* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1691</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.CHK.MAT.MOD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*   $INCLUDE           I_F.SCB.EXCEPTION.CUST
*----------------------------------------------
    CHRG.TYP = ""
    CHRG.AMT = ""
    IF  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE  "9902" THEN
        IF MESSAGE # 'VAL' THEN
            IF COMI NE R.NEW(BRLR.MAT.DATE) THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE> = ""
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> = ""
            END
        END
        GOSUB INTIAL

        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 9 THEN
            GOSUB OUT.OF.CLEARING
            RETURN
        END
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 7 THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA> = 1
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = 'BILCHARGUPD'
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> = 'EGP10'
            R.NEW(EB.BILL.REG.START.DATE) = ''
            CALL REBUILD.SCREEN
        END
        IF COMI THEN
*TEXT = COMI ; CALL REM
            MAT.DATE       = COMI
            NEXT.MAT.DATE  = TODAY
            COMP           = C$ID.COMPANY
            USR.DEP        = "1700":COMP[8,2]
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
FN.F.ITSS.SCB.BANK.BRANCH = ''
CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
CALL F.READ(F.ITSS.SCB.BANK.BRANCH,USR.DEP,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
LOCT=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.LOCATION>

            DAL.TODAY = TODAY
            IF LOCT EQ '1'  THEN
                CALL CDT('EG00',NEXT.MAT.DATE,"+1W")
            END ELSE
                CALL CDT('EG00',NEXT.MAT.DATE,"+2W")
            END

            XX.CHEQ = ""
            B.DATE  = ''

            IF MAT.DATE GT NEXT.MAT.DATE THEN
                B.DATE  = 'XX'
            END
            IF MAT.DATE LE NEXT.MAT.DATE THEN
                B.DATE  = ''
            END
            IF B.DATE EQ 'XX' THEN
*TEXT ="H1" ; CALL REM
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE>  = 10 THEN
*TEXT = "H2" ; CALL REM
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA> = 2
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = 'BILCHARGUPD'
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = 'EGP10'
                    R.NEW(EB.BILL.REG.START.DATE) = ''
                    CALL REBUILD.SCREEN
                END
            END ELSE
                IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE>  = 10 THEN
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA> = R.OLD(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.STA>
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE>  = ''
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT>   = ''
                    R.NEW(EB.BILL.REG.START.DATE) = ''
                    CALL REBUILD.SCREEN
                END
            END
            TYP = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE>
            IF ( TYP EQ 6 OR TYP EQ 7 ) THEN
                IF MAT.DATE LE NEXT.MAT.DATE THEN
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 6
                    XX.CHEQ = 6
                END
                IF MAT.DATE GT NEXT.MAT.DATE THEN
                    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> =7
                    B.DATE  = 'XX'
                    XX.CHEQ = 7
                END
            END
*TEXT = "B= " : B.DATE ; CALL REM
**---------
            IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 7 THEN
*----------------------------
                IF MAT.DATE GT NEXT.MAT.DATE THEN
                    GOSUB CALC.DATE.S.PP
                    GOSUB CALC.DATE.COLLECTION
                END ELSE
                    GOSUB CALC.DATE.SESSION
                    IF NOT(ETEXT) THEN
                        GOSUB CALC.DATE.COLLECTION
                    END
                END
            END
        END
*---------------------------------------------------------*
        IF MESSAGE # 'VAL' THEN

            IF R.NEW(EB.BILL.REG.DRAWER)[1,3] NE '994' THEN

                FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = ''
                CALL OPF(FN.CUS,F.CUS)

                CALL F.READ(FN.CUS,R.NEW(EB.BILL.REG.DRAWER),R.CUS,F.CUS,ER1)
                SEC  = R.CUS<EB.CUS.SECTOR>
                IF   R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> EQ 7 THEN
                    IF ( SEC NE 1100 AND SEC NE 1200 AND SEC NE 1300 AND SEC NE 1400 ) THEN

                        MAT.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
                        REC.DT   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

                        IF  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> NE  "9902" THEN
                            DAYS.BET = 'C'
                            CALL CDD("",REC.DT,MAT.DT,DAYS.BET)
                        END
*-----------------------------------------------------------------
                        YEE  = DAYS.BET / 365
                        WW   = INT(DAYS.BET/365)

*-----------------------------
INTIAL:
                        MAT.DATE = '' ; MAT.DATE1 = '' ; DIFF = '' ; DAT1 = '' ; PAY.CODE = ''
                        PAY.DAYS = ''
                        RETURN

*----------------------------
CALC.DATE.SESSION:
                        T.DAY = TODAY

                        XX  = ''
                        CALL JULDATE(TODAY,XX )
                        XX.DAT = XX - 999
                        XX1 = ''
                        CALL JULDATE(MAT.DATE,XX1 )

                        IF XX1 LT XX.DAT THEN
                            ETEXT = "����� ������� ����� ��� ���� ���"
                        END ELSE

                            IF LOCT EQ '1'  THEN
                                CALL CDT('',T.DAY,"+1W")
                            END ELSE
                                CALL CDT('',T.DAY,"+2W")
                            END
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = T.DAY

                            CALL REBUILD.SCREEN
                        END
                        RETURN

*------------------------

CALC.DATE.S.PP:

                        MAT.DATE1 = COMI

                        CALL AWD("EG00",MAT.DATE1,HOL)
                        IF HOL EQ "H" THEN
                            CALL CDT("EG00",MAT.DATE1,"+1W")
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
                        END ELSE
                            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE1
                        END
                        CALL REBUILD.SCREEN

                        RETURN

*--------------------------
CALC.DATE.COLLECTION:

                        DAT1 = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT>
                        PAY.CODE = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE>
                        IF PAY.CODE EQ 0 THEN
                            PAY.DAYS = 0
                        END ELSE
                            PAY.DAYS = PAY.CODE
                        END

                        CALL CDT('EG00', DAT1, '+':PAY.DAYS:'W')

                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE> = DAT1

                        CALL REBUILD.SCREEN
                        RETURN
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CHARGE.TYPE> = 'BILCHARGUPD'
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CH.CCY.AMT> = 'EGP10'
*   TEXT = CHRG.TYP ; CALL REM
*----------------------------------
OUT.OF.CLEARING:
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
                        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
                        RETURN
*----------------------------------

                    END
                END
            END
        END
    END
END
