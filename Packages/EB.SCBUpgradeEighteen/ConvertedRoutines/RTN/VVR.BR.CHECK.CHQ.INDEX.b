* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.CHECK.CHQ.INDEX

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.NUMBER.INDEX

    IF MESSAGE = 'VAL' THEN
        IF COMI AND R.NEW(BRLR.BIL.CHQ.TYPE) THEN
            ID.CHQ.INDEX = R.NEW(BRLR.BILL.CHQ.NO):".":R.NEW(BRLR.BIL.CHQ.TYPE)
*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('SCB.CHQ.NUMBER.INDEX':@FM:SCB.CNI.BILL.REGISTER.ID,ID.CHQ.INDEX,REG.ID)
F.ITSS.SCB.CHQ.NUMBER.INDEX = 'F.SCB.CHQ.NUMBER.INDEX'
FN.F.ITSS.SCB.CHQ.NUMBER.INDEX = ''
CALL OPF(F.ITSS.SCB.CHQ.NUMBER.INDEX,FN.F.ITSS.SCB.CHQ.NUMBER.INDEX)
CALL F.READ(F.ITSS.SCB.CHQ.NUMBER.INDEX,ID.CHQ.INDEX,R.ITSS.SCB.CHQ.NUMBER.INDEX,FN.F.ITSS.SCB.CHQ.NUMBER.INDEX,ERROR.SCB.CHQ.NUMBER.INDEX)
REG.ID=R.ITSS.SCB.CHQ.NUMBER.INDEX<SCB.CNI.BILL.REGISTER.ID>
            IF REG.ID THEN ETEXT = '��� ����� �� ������ �� ��� '
            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
