* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*******NESSREEN AHMED 16/3/2011*******
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.VISA.APP.SER

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP

    SER.NO = ''
    CUST.ID1= ID.NEW
**    TEXT = 'ID.NEW=':ID.NEW ; CALL REM
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR( 'SCB.VISA.APP':@FM:SCB.VISA.CARD.TYPE, ID.NEW , CARD.TYP)
F.ITSS.SCB.VISA.APP = 'F.SCB.VISA.APP'
FN.F.ITSS.SCB.VISA.APP = ''
CALL OPF(F.ITSS.SCB.VISA.APP,FN.F.ITSS.SCB.VISA.APP)
CALL F.READ(F.ITSS.SCB.VISA.APP,ID.NEW,R.ITSS.SCB.VISA.APP,FN.F.ITSS.SCB.VISA.APP,ERROR.SCB.VISA.APP)
CARD.TYP=R.ITSS.SCB.VISA.APP<SCB.VISA.CARD.TYPE>
    IF CARD.TYP THEN
        E = '��� �� ���� ��� ����'
        CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        CUST.ID = FIELD(CUST.ID1,'.',1)
        ID.NN = CUST.ID:"..."
        T.SEL =  "SELECT F.SCB.VISA.APP WITH @ID LIKE ": ID.NN

        KEY.LIST=""
        SELECTED=""
        ER.MSG=""

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
  **      TEXT = 'SELECTED=':SELECTED ; CALL REM
        SER.NO = SELECTED+1
        KEY.TO.USE = CUST.ID:'.':SER.NO
  **      TEXT = 'KEY=':KEY.TO.USE ; CALL REM
        ID.NEW =  KEY.TO.USE
    END
    RETURN
END
