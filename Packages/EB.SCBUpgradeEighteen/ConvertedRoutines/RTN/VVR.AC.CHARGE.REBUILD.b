* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
    SUBROUTINE VVR.AC.CHARGE.REBUILD

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.CHARGE.REQUEST
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.CHG = 'FBNK.FT.CHARGE.TYPE' ; F.CHG = ''
    CALL OPF(FN.CHG,F.CHG)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    DB.ACCT    = R.NEW(CHG.DEBIT.ACCOUNT)
    CALL F.READ(FN.AC,DB.ACCT,R.AC,F.AC,E3)

    ACCT.CATEG = R.AC<AC.CATEGORY>
    WS.CUS.ID  = R.AC<AC.CUSTOMER>
    WS.CCY     = R.AC<AC.CURRENCY>

    CALL F.READ(FN.CU,WS.CUS.ID,R.CU,F.CU,E2)
    WS.SECTOR = R.CU<EB.CUS.SECTOR>
    WS.TARGET = R.CU<EB.CUS.TARGET>

    IF ACCT.CATEG EQ 1001 OR ACCT.CATEG EQ 1004 OR ACCT.CATEG EQ 1006 OR ACCT.CATEG EQ 1019 OR ACCT.CATEG EQ 1099 OR ACCT.CATEG EQ 1535 OR ACCT.CATEG EQ 1560 THEN
        R.NEW(CHG.CHARGE.CODE)<1,1> = 'ACCURCHRG'
    END

    IF ACCT.CATEG GE 6501 AND ACCT.CATEG LE 6517 THEN
        R.NEW(CHG.CHARGE.CODE)<1,1> = 'ACSAVCHRG'
    END

    IF PGM.VERSION EQ ',SCB' THEN
        IF WS.SECTOR EQ '2000' OR WS.SECTOR EQ '1100' OR WS.SECTOR EQ '1200' OR WS.SECTOR EQ '1300' OR WS.SECTOR EQ '1400' THEN
            CHG.ID = 'ACSTAMPRET'
        END ELSE
            CHG.ID = 'ACSTAMPCOR'
        END

        CALL F.READ(FN.CHG,CHG.ID,R.CHG,F.CHG,E1)
        CHG.AMT = R.CHG<FT5.FLAT.AMT>

        CUST.ID  = WS.CUS.ID
        DEAL.AMT = 0
        DEAL.CCY = WS.CCY
        CCY.MKT = "1"
        FT.CHARGE.CODES = ""
        FT.CHARGE.CODES = CHG.ID
        TOT.CHARGE.LCY = "0"
        TOT.CHARGE.FCY = "0"

        CALL CALCULATE.CHARGE(CUST.ID,DEAL.AMT,DEAL.CCY,CCY.MKT,"","","",FT.CHARGE.CODES,"",TOT.CHARGE.LCY,TOT.CHARGE.FCY)

        IF WS.CCY EQ 'EGP' THEN
            WS.CHG.AMT = TOT.CHARGE.LCY
        END ELSE
            WS.CHG.AMT = TOT.CHARGE.FCY
        END

        R.NEW(CHG.CHARGE.AMOUNT)<1,2> = WS.CHG.AMT
        R.NEW(CHG.CHARGE.CODE)<1,2>   = CHG.ID
    END

    IF WS.TARGET EQ 5700 OR WS.TARGET EQ 5800 OR WS.TARGET EQ 5850 OR WS.TARGET EQ 5860 OR WS.TARGET EQ 5870 OR WS.TARGET EQ 5900 THEN
        DEL R.NEW(CHG.CHARGE.CODE)<1,1>
    END

    CALL REBUILD.SCREEN

    RETURN
END
