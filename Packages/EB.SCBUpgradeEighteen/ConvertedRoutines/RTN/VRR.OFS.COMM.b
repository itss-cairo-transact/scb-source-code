* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>266</Rating>
*-----------------------------------------------------------------------------

******** WAEL **********

    SUBROUTINE VRR.OFS.COMM

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.CHARGE.CONDITIONS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

    T.SEL = "SSELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":ID.NEW:";..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    SEL.NO = SELECTED - 1
    CALL F.READ(FN.LD,ID.NEW:";":SEL.NO,R.LD,F.LD,E1)

    LOCAL.REF = R.LD<LD.LOCAL.REF>
    OLD.MARG = LOCAL.REF<1,LDLR.MARGIN.AMT>

    MYCODE = LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE = LOCAL.REF<1,LDLR.PRODUCT.TYPE>

*Line [ 55 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    XXX = DCOUNT(R.LD<LD.CHRG.AMOUNT>,@VM)
FOR I = 1 TO XXX
        CODE = R.LD<LD.CHRG.CODE><1,I>
        CHARGE.AMT = R.LD<LD.CHRG.AMOUNT><1,I>
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR("LMM.CHARGE.CONDITIONS":@FM:LD21.CATEGORY.CODE,CODE,CHARGE.ACCT)  
F.ITSS.LMM.CHARGE.CONDITIONS = 'F.LMM.CHARGE.CONDITIONS'
FN.F.ITSS.LMM.CHARGE.CONDITIONS = ''
CALL OPF(F.ITSS.LMM.CHARGE.CONDITIONS,FN.F.ITSS.LMM.CHARGE.CONDITIONS)
CALL F.READ(F.ITSS.LMM.CHARGE.CONDITIONS,CODE,R.ITSS.LMM.CHARGE.CONDITIONS,FN.F.ITSS.LMM.CHARGE.CONDITIONS,ERROR.LMM.CHARGE.CONDITIONS)
CHARGE.ACCT=R.ITSS.LMM.CHARGE.CONDITIONS<LD21.CATEGORY.CODE>
        GOSUB OFS_MAKE
NEXT I

RETURN
*========================== OPERATION ==========================
OFS_MAKE:
 IF MYCODE = '1251' THEN GOSUB PRODUCE.MSG

 IF MYCODE = '1252' THEN GOSUB PRODUCE.MSG
 
 IF MYCODE = '1241' OR MYCODE = '1242' OR (MYCODE >= '1231' AND MYCODE <= '1235') THEN GOSUB PRODUCE.MSG

 RETURN
*===========================================================
PRODUCE.MSG:

    TRANS.TYPE = "ACLG"
    DB.CUR     = R.NEW(LD.LOCAL.REF)<1,LDLR.ACC.CUR>
    DB.AMT     = CHARGE.AMT
    DB.ACCT    = R.NEW(LD.LOCAL.REF)<1,LDLR.DEBIT.ACCT>
    CR.ACCT    = "PL":CHARGE.ACCT
   * PROFT.CUST = R.NEW(LD.LOCAL.REF)< 1,LDLR.THIRD.NUMBER>
    DB.DATE    = R.NEW(LD.VALUE.DATE)
    CR.DATE    = R.NEW(LD.VALUE.DATE)
   * BENF.CUST  = R.NEW(LD.LOCAL.REF)<1,LDLR.THIRD.NUMBER>
   * ORDER.BNK  = R.NEW(LD.CUSTOMER.ID)
    ORDER.BANK = ID.NEW
    CHQ.NO = ''
    PROFIT.DEPT = R.USER<EB.USE.DEPARTMENT.CODE>

*Line [ 91 ] Adding EB.SCBUpgradeEighteen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    CALL EB.SCBUpgradeEighteen.VRR.LG.OFS( TRANS.TYPE,DB.CUR,DB.AMT,PROFT.CUST,DB.DATE,CR.DATE,DB.ACCT,CR.ACCT,CHQ.NO,BENF.CUST,PROFIT.DEPT,ORDER.BANK)
*-------------------------
RETURN
END
