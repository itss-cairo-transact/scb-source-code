* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>96</Rating>
*-----------------------------------------------------------------------------
****************** WAEL *************
*A Routine To Check That Updating This Record Will be only from same Inputter,
*Also To check If Rate is entered Then Allow Only Function See

SUBROUTINE VNC.MM.CHK.INPUTTER

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
**************************************************************************************************

   IF V$FUNCTION = 'I' THEN
     *R.NEW(MM.MIS.ACCT.OFFICER) = R.USER<EB.USE.DEPARTMENT.CODE>
      *IF R.NEW(MM.INPUTTER) # '' THEN
        * XX = R.NEW(MM.INPUTTER)
         *IF FIELD(XX,'_', 2) # R.USER<EB.USE.USER.NAME> THEN E = 'ONLY.INPUTTER.CAN.UPDATE' ; CALL ERR ; MESSAGE = 'REPEAT'

*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('MM.MONEY.MARKET':@FM:MM.INTEREST.RATE ,ID.NEW,DUMMY)
F.ITSS.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET'
FN.F.ITSS.MM.MONEY.MARKET = ''
CALL OPF(F.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET)
CALL F.READ(F.ITSS.MM.MONEY.MARKET,ID.NEW,R.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET,ERROR.MM.MONEY.MARKET)
DUMMY=R.ITSS.MM.MONEY.MARKET<MM.INTEREST.RATE>
        IF NOT(ETEXT) THEN E = 'ONLY.SEE.FUNCTION.ALLOWED' ;CALL ERR ;MESSAGE = 'REPEAT'
      *END
   END

****************************************************************************************************
RETURN
END
