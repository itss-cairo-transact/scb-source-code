* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>583</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.MLAD.ID.2

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM.2
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.TEMP.2


    FN.ML  = 'F.SCB.MLAD.FILE.PARAM.2' ; F.ML = '' ; R.ML = ''
    CALL OPF( FN.ML,F.ML)

    FN.MLAD.TEMP = "F.SCB.MLAD.FILE.TEMP.2"
    F.MLAD.TEMP  = ""
    R.MLAD.TEMP  = ""
    Y.MLAD.TEMP.ID   = ""
    CALL OPF( FN.MLAD.TEMP,F.MLAD.TEMP)

    IF V$FUNCTION EQ 'I' THEN
        MLAD.TYPE = R.NEW(MLPAR.MLAD.TYPE)
        LINE.NO   = R.NEW(MLPAR.MLAD.LINE.NO)
        GENLED.NO = R.NEW(MLPAR.GENLED.LINE.NO)
        CAT.FROM  = R.NEW(MLPAR.CATEGORY.FROM)
        CAT.TO    = R.NEW(MLPAR.CATEGORY.TO)
        MAT.DATE  = R.NEW(MLPAR.MATURITY.DATE)
*------------------
        ID.NEW = OCONV(ID.NEW,"MCT")
        X = OCONV('X',"MCT")
        Y = OCONV('Y',"MCT")

        MLAD.TYPE = FIELD(ID.NEW,'-',1,1)
        LINE.NO   = FIELD(ID.NEW,'-',2,1)
        SEL  = FIELD(ID.NEW,'-',3,1)

***        TEXT = "ID.AFT= "  : ID.NEW ; CALL REM

        IF MLAD.TYPE EQ X THEN
            GOTO MLAD.TYPE.OK
        END

        IF MLAD.TYPE EQ Y THEN
            GOTO MLAD.TYPE.OK
        END

        AF = 1
        ETEXT = "������� ��� ����"
        CALL STORE.END.ERROR

****    RETURN
*------------------
MLAD.TYPE.OK:
        IF SEL GE '0001'  AND SEL LE '9999' THEN
            CALL F.READ(FN.ML,ID.NEW,R.ML,F.ML,ERR.ML)
            IF ERR.ML EQ '' THEN
                GOTO PARAM.FOUND
            END
        END

PARAM.NOT.FOUND:
        T.SEL   = "SELECT F.SCB.MLAD.FILE.PARAM.2 WITH MLAD.TYPE EQ ":MLAD.TYPE:" AND MLAD.LINE.NO EQ ":LINE.NO
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
        IF SELECTED THEN
            SEL = SELECTED + 1
        END
        ELSE
            SEL = 1
        END

PARAM.FOUND:
        SEL = FMT(SEL,"R%4")
        R.NEW(MLPAR.MLAD.SERIAL) = SEL

        RECORD.NO = MLAD.TYPE:"-":LINE.NO:"-":R.NEW(MLPAR.MLAD.SERIAL)

        R.NEW(MLPAR.MLAD.RECORD.NO) = RECORD.NO


        ID.NEW = RECORD.NO

        R.NEW(MLPAR.MLAD.TYPE)    = FIELD(RECORD.NO,'-',1,1)
        R.NEW(MLPAR.MLAD.LINE.NO) = FIELD(RECORD.NO,'-',2,1)

        Y.MLAD.TEMP.ID = MLAD.TYPE:"-":LINE.NO

        CALL F.READ(FN.MLAD.TEMP,Y.MLAD.TEMP.ID,R.MLAD.TEMP,F.MLAD.TEMP,ERR.MLAD.TEMP)

        IF ERR.MLAD.TEMP   THEN
            AF = 1
            ETEXT = "������� ��� ����"
            CALL STORE.END.ERROR
        END


        N.DES   = R.MLAD.TEMP<MLTEMP.DESCRIPTION>


        R.NEW(MLPAR.TEMP.DESC) = N.DES
***        END

        RETURN

    END
