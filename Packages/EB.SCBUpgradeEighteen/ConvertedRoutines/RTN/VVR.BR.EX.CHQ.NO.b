* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE

*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.EX.CHQ.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.NUMBER.INDEX


    IF COMI THEN

*Line [ 36 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.BR.NUMBER.INDEX':@FM:1,COMI,CHQNO)
F.ITSS.SCB.BR.NUMBER.INDEX = 'F.SCB.BR.NUMBER.INDEX'
FN.F.ITSS.SCB.BR.NUMBER.INDEX = ''
CALL OPF(F.ITSS.SCB.BR.NUMBER.INDEX,FN.F.ITSS.SCB.BR.NUMBER.INDEX)
CALL F.READ(F.ITSS.SCB.BR.NUMBER.INDEX,COMI,R.ITSS.SCB.BR.NUMBER.INDEX,FN.F.ITSS.SCB.BR.NUMBER.INDEX,ERROR.SCB.BR.NUMBER.INDEX)
CHQNO=R.ITSS.SCB.BR.NUMBER.INDEX<1>
        IF NOT(ETEXT) THEN ETEXT = 'SHLD.BE.NEW.CHQ.NO'
        ELSE  ETEXT = ''

    END
    RETURN
END
