* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>348</Rating>
*-----------------------------------------------------------------------------
*********** WAEL *********

SUBROUTINE VNC.MM.CHK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

TEXT=TODAY ;CALL REM
IF V$FUNCTION = 'I' THEN
IF PGM.VERSION = ',SCB.ROLLOVER'  THEN
   IF R.NEW( MM.VALUE.DATE) # '' THEN
     IF R.NEW( MM.VALUE.DATE) < TODAY THEN E = 'VALUE.DATE.MUST.BE.TODAY'; CALL ERR ; MESSAGE = 'REPEAT'
  END
END

*CALL DBR('MM.MONEY.MARKET':@FM:MM.INTEREST.RATE ,ID.NEW,DUMMY)
*IF NOT(ETEXT) THEN E = 'ONLY.SEE.FUNCTION.ALLOWED' ;CALL ERR ;MESSAGE = 'REPEAT'

R.NEW(MM.INCR.EFF.DATE)= TODAY
IF R.NEW(MM.CURRENCY) # ''  THEN
   IF R.NEW(MM.CURRENCY) = LCCY THEN E = 'LOCAL.CURRENCY.NOT.ALLOWED'; CALL ERR ; MESSAGE = 'REPEAT'
 XX = R.NEW(MM.INPUTTER)
 IF FIELD(XX,'_', 2) # R.USER<EB.USE.USER.NAME> THEN E = 'ONLY.INPUTTER.CAN.UPDATE'
CALL ERR ; MESSAGE = 'REPEAT'
END
END

RETURN
END
