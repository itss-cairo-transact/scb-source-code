* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
*----- DINA_SCB ---- 18-7-2002

SUBROUTINE VNC.TT.FAULT.ID1.TO.TILL

* A routine to default the field teller.id.1 with the teller.id of the user
* and to default the field narrative.2 with the name of the user branch

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID

IF V$FUNCTION = 'I' THEN
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
* CALL DBR ('TELLER':@FM:TT.TE.RECORD.STATUS,ID.NEW,STAT)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ID.NEW,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
STAT=R.ITSS.TELLER<TT.TE.RECORD.STATUS>
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
* CALL DBR ('TELLER$NAU':@FM:TT.TE.RECORD.STATUS,ID.NEW,STAT)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
STAT=R.ITSS.TELLER$NAU<TT.TE.RECORD.STATUS>
  IF NOT(STAT) THEN
****UPDATED BY NESSREEN AHMED 17/11/2009
***  CALL DBR('TELLER.USER':@FM:1,OPERATOR,ID)
***  R.NEW(TT.TE.TELLER.ID.1) = ID
    T.SEL  = "SELECT FBNK.TELLER.ID WITH USER EQ ":OPERATOR :" AND STATUS EQ OPEN "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ERR1)
    TEL.USR = KEY.LIST<1>
    R.NEW(TT.TE.TELLER.ID.1) = TEL.USR

   DEP=R.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*   CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEP,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEP,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
   R.NEW(TT.TE.NARRATIVE.2) = DEPT.NAME
  END
END

RETURN
END
