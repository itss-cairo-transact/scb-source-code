* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.VISA.PAY.FLG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.FLAG

    CO.CODE = ID.COMPANY
    YY.MONTH = TODAY[1,6]
    ID.NEW = CO.CODE :'.':YY.MONTH

    IF V$FUNCTION EQ 'I' THEN
*Line [ 35 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.VISA.PAY.FLAG':@FM:PAFL.PAYMENT.FLAG,ID.NEW,PAY.FLG)
F.ITSS.SCB.VISA.PAY.FLAG = 'F.SCB.VISA.PAY.FLAG'
FN.F.ITSS.SCB.VISA.PAY.FLAG = ''
CALL OPF(F.ITSS.SCB.VISA.PAY.FLAG,FN.F.ITSS.SCB.VISA.PAY.FLAG)
CALL F.READ(F.ITSS.SCB.VISA.PAY.FLAG,ID.NEW,R.ITSS.SCB.VISA.PAY.FLAG,FN.F.ITSS.SCB.VISA.PAY.FLAG,ERROR.SCB.VISA.PAY.FLAG)
PAY.FLG=R.ITSS.SCB.VISA.PAY.FLAG<PAFL.PAYMENT.FLAG>
        IF PAY.FLG EQ 'YES' THEN
            E ='Already Paid'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END ELSE
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('SCB.VISA.PAY.FLAG$NAU':@FM:PAFL.PAYMENT.FLAG,ID.NEW,PAY.FLG)
F.ITSS.SCB.VISA.PAY.FLAG$NAU = 'F.SCB.VISA.PAY.FLAG$NAU'
FN.F.ITSS.SCB.VISA.PAY.FLAG$NAU = ''
CALL OPF(F.ITSS.SCB.VISA.PAY.FLAG$NAU,FN.F.ITSS.SCB.VISA.PAY.FLAG$NAU)
CALL F.READ(F.ITSS.SCB.VISA.PAY.FLAG$NAU,ID.NEW,R.ITSS.SCB.VISA.PAY.FLAG$NAU,FN.F.ITSS.SCB.VISA.PAY.FLAG$NAU,ERROR.SCB.VISA.PAY.FLAG$NAU)
PAY.FLG=R.ITSS.SCB.VISA.PAY.FLAG$NAU<PAFL.PAYMENT.FLAG>
            IF V$FUNCTION NE 'D' THEN
                IF PAY.FLG = 'YES' THEN
                    E ='CANT AMEND UNAUTHORIZED RECORD'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                    CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                END ELSE
                    DEPT.CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>

                    DEPT.CODE= FIELD(ID.NEW,'.',1)
                    DEPT.CODE.ID=DEPT.CODE[8,2]

                    IF DEPT.CODE.ID NE DEPT.CODE.USER THEN
                        E ='Not Your Branch'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
                        CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
                    END  ELSE
                        R.NEW(PAFL.COMPANY.CO)=CO.CODE
                        R.NEW(PAFL.TRANS.DATE)=TODAY
                        R.NEW(PAFL.YEAR.MM)= YY.MONTH
                        R.NEW(PAFL.PAYMENT.FLAG)='YES'
                    END
                END
            END     ;*IF FUNCTION NE 'D'
        END
    END
    IF V$FUNCTION EQ 'A' THEN
        DEPT.CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>
****UPDATED BY NESSREN AHMED 17/3/2012*************
        DEPT.CODE.REC = R.NEW(PAFL.DEPT.CODE)
***   DEPT.CODE= FIELD(ID.NEW,'.',1)
***        DEPT.CODE.ID=DEPT.CODE[8,2]
        DEPT.CODE.ID = DEPT.CODE.REC
****END OF UPDATE 17/3/2012***************************
        IF DEPT.CODE.ID NE DEPT.CODE.USER THEN
            E ='Not Your Branch'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END
*******UPDATED BY NESSREEN AHMED 17/4/2012*************************
    IF V$FUNCTION EQ 'D' THEN
        DEPT.CODE.USER = R.USER<EB.USE.DEPARTMENT.CODE>
        DEPT.CODE.REC = R.NEW(PAFL.DEPT.CODE)
        DEPT.CODE.ID = DEPT.CODE.REC
        IF DEPT.CODE.ID NE DEPT.CODE.USER THEN
            E ='Not Your Branch'
*** SCB UPG 20160623 - S
*   CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160623 - E
        END
    END   ;*IF FUNCTION EQ 'D'
*******END OF UPDATE 17/4/2012**************************************
    RETURN
END
