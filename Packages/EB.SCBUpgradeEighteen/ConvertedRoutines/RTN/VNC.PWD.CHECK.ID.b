* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.PWD.CHECK.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*-------------------------------------------
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF (FN.USR, F.USR)
*-------------------------------------------
    IF V$FUNCTION = 'I' THEN
        ETEXT = ''
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('USER':@FM:EB.USE.LOCAL.REF,ID.NEW,DEPT.CO)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,ID.NEW,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT.CO=R.ITSS.USER<EB.USE.LOCAL.REF>
        DEPT.CO = DEPT.CO<1,USER.SCB.DEPT.CODE>
        IF DEPT.CO EQ '2700' THEN
*** EDIT BY NESSMA ON 2012/07/03 BY Mr.Bakry Request
*       E = 'NOT ALLOW TO RESET THIS USER'
*       CALL ERR ; MESSAGE = 'REPEAT'
*** END EDIT
        END
*----------------       -----------------
        ETEXT = ''  ; STT = '' ; STT.N = ''
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('PASSWORD.RESET':@FM:EB.PWR.USER.ATTEMPT,ID.NEW,STT)
F.ITSS.PASSWORD.RESET = 'F.PASSWORD.RESET'
FN.F.ITSS.PASSWORD.RESET = ''
CALL OPF(F.ITSS.PASSWORD.RESET,FN.F.ITSS.PASSWORD.RESET)
CALL F.READ(F.ITSS.PASSWORD.RESET,ID.NEW,R.ITSS.PASSWORD.RESET,FN.F.ITSS.PASSWORD.RESET,ERROR.PASSWORD.RESET)
STT=R.ITSS.PASSWORD.RESET<EB.PWR.USER.ATTEMPT>
        IF NOT(ETEXT) THEN
*-- Edit By Nessma Mohammad on 2012/03/25
*-- Based on Mr.Sharef request ---------*
*            E = '�� ����� ��� �������� �� ���'
*            CALL ERR ; MESSAGE = 'REPEAT'
        END
*----------------       -----------------
        ETEXT = ''
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('PASSWORD.RESET$NAU':@FM:EB.PWR.USER.ATTEMPT,ID.NEW,STT.N)
F.ITSS.PASSWORD.RESET$NAU = 'F.PASSWORD.RESET$NAU'
FN.F.ITSS.PASSWORD.RESET$NAU = ''
CALL OPF(F.ITSS.PASSWORD.RESET$NAU,FN.F.ITSS.PASSWORD.RESET$NAU)
CALL F.READ(F.ITSS.PASSWORD.RESET$NAU,ID.NEW,R.ITSS.PASSWORD.RESET$NAU,FN.F.ITSS.PASSWORD.RESET$NAU,ERROR.PASSWORD.RESET$NAU)
STT.N=R.ITSS.PASSWORD.RESET$NAU<EB.PWR.USER.ATTEMPT>
        IF NOT(ETEXT) THEN
            E = '��� ����� ��� �������� ������� �����'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*----------------       -----------------
        ETEXT = ''
*Line [ 82 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('USER':@FM:EB.USE.USER.NAME,ID.NEW,USER.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,ID.NEW,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USER.NAME=R.ITSS.USER<EB.USE.USER.NAME>
        IF ETEXT THEN
            E = '���� �� ���� ��� ������ ����'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*----------------        --------------
*EDIT BY NESSMA ON 2011/10/10 ---------
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('USER$NAU':@FM:EB.USE.USER.NAME,ID.NEW,USER.NAME)
F.ITSS.USER$NAU = 'F.USER$NAU'
FN.F.ITSS.USER$NAU = ''
CALL OPF(F.ITSS.USER$NAU,FN.F.ITSS.USER$NAU)
CALL F.READ(F.ITSS.USER$NAU,ID.NEW,R.ITSS.USER$NAU,FN.F.ITSS.USER$NAU,ERROR.USER$NAU)
USER.NAME=R.ITSS.USER$NAU<EB.USE.USER.NAME>
        IF NOT(ETEXT) THEN
            E = '��� ����� �������� ����'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*----------
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('USER':@FM:EB.USE.END.DATE.PROFILE,ID.NEW,USR.DAT.PRF)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,ID.NEW,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USR.DAT.PRF=R.ITSS.USER<EB.USE.END.DATE.PROFILE>
        IF USR.DAT.PRF LT TODAY THEN
            E = '����� ������ �������� ������'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
*--------------------------------------
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('USER':@FM:EB.USE.END.TIME,ID.NEW,USR.DAT.PRF)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,ID.NEW,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USR.DAT.PRF=R.ITSS.USER<EB.USE.END.TIME>
        IF USR.DAT.PRF LT '1730' THEN
            E = 'TIME'
            CALL ERR ; MESSAGE = 'REPEAT'
            RETURN
        END
    END

    RETURN
END
