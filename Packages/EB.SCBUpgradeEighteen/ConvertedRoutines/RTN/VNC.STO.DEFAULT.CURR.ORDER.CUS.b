* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
**---------INGY ---------**

SUBROUTINE VNC.STO.DEFAULT.CURR.ORDER.CUS

* A ROUTINE TO DEFAULT THE FIELD CURRENCY & ORDERING.CUST FROM ID

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDING.ORDER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER


CURR = ID.NEW[3,2]
CUS = TRIM (ID.NEW[5,7], '0', 'L')

*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR( 'NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CURR,CUR)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CURR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
IF CUR THEN R.NEW(STO.CURRENCY) = CUR

*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUS,XXX)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
XXX=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>
R.NEW(STO.ORDERING.CUST) = XXX

RETURN
END

         
         
