* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>44</Rating>
*-----------------------------------------------------------------------------
**---------NESSREEN AHMED 15/03/2010---------**

    SUBROUTINE VNC.TT.CHK.REC.BR

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY

    IF V$FUNCTION = 'A' THEN
*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('TELLER.USER':@FM:1,OPERATOR,USE.ID)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
USE.ID=R.ITSS.TELLER.USER<1>
**    IF USE.ID[3,2] # 99 THEN
**       E = '��� ����� ��������' ; CALL ERR ; MESSAGE = 'REPEAT'
**    END
        INP.CO = ''
        AUT.CO = ''
        TRANS.ID= R.NEW(TT.TE.TRANSACTION.CODE)
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('TELLER$NAU':@FM:TT.TE.CO.CODE, ID.NEW ,INP.CO)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
INP.CO=R.ITSS.TELLER$NAU<TT.TE.CO.CODE>
**        TEXT = 'INP.CO=':INP.CO ; CALL REM
**        TEXT = 'COMP=':COMP ; CALL REM
        IF COMP # INP.CO THEN
            E = '��� ������� �� ��� ���'
*** SCB UPG 20160621 - S
*   CALL ERR ;MESSAGE = 'REPEAT'
            CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
        END
    END
    RETURN
END
