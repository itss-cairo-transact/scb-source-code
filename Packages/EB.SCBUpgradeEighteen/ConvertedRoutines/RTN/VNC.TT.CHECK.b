* @ValidationCode : MjotMTU0MzEyNjU4NzpDcDEyNTI6MTY0NDkzMzM0NjkyODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:55:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>50</Rating>
*-----------------------------------------------------------------------------
*---14/07/2002 RANIA KAMAL -------

*THE ROUTINE TO CHECK IF THE OPERATION IN TELLER$NAU THEN
*IF THE DEPARTMENT.CODE OF THE INPUTTER NOT EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*DISPLAY ERROR
*IF THE DEPARTMENT.CODE OF THE INPUTTER  EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
* CHECK IF THE TRANSACTION.CODE NOT EQUAL '9' DISPLAY ERROR


SUBROUTINE VNC.TT.CHECK

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT

    MYCODE = ''
    MYTRANS= ''
    INPUTTER.CODE=''
    DEPT.CODE = ''

    IF V$FUNCTION='I' THEN

        MYCODE = FIELD(R.NEW(TT.TE.INPUTTER),'_', 2)

*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR('TELLER$NAU':@FM:TT.TE.TELLER.ID.1, ID.NEW ,MYTRANS)
        F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
        FN.F.ITSS.TELLER$NAU = ''
        CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
        CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
        MYTRANS=R.ITSS.TELLER$NAU<TT.TE.TELLER.ID.1>
        IF NOT(ETEXT) THEN
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*   CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE, MYCODE, DEPT.CODE)
            F.ITSS.USER = 'F.USER'
            FN.F.ITSS.USER = ''
            CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
            CALL F.READ(F.ITSS.USER,MYCODE,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
            DEPT.CODE=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>

            IF R.USER<EB.USE.DEPARTMENT.CODE> # DEPT.CODE THEN
                E = 'WRONG.DEPTARTMENT'
                CALL ERR ; MESSAGE = 'REPEAT';
            END ELSE

                IF R.NEW(TT.TE.TRANSACTION.CODE) # 9 THEN
                    E='TRANSACTION.CODE.EQ.9'
                    CALL ERR ; MESSAGE = 'REPEAT';
                END
            END
        END
    END


RETURN
END
