* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>44</Rating>
*-----------------------------------------------------------------------------
**---------INGY 10/07/2002---------**
**---------DINA 14/07/2002---------**

    SUBROUTINE VNC.TT.RECORD.STATUS.SEND

*THE VERSION HAS ZERO AUTHORIZED BUT IT CAN PUT IN IHLD STATUS (IHLD)
*THE ROUTINE TO CHECK IF THE OPERATION IN TELLER$NAU (IHLD) THEN
*IF THE DEPARTMENT.CODE OF THE INPUTTER NOT EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)
*DISPLAY ERROR
*IF THE DEPARTMENT.CODE OF THE INPUTTER  EQUAL BRANCH OF USER (R.USER<EB.USE.DEPARTMENT.CODE>)

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER



*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('TELLER.USER':@FM:1,OPERATOR,USE.ID)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
USE.ID=R.ITSS.TELLER.USER<1>
    IF USE.ID[3,2] # 99 THEN
**       E = '��� ����� ��������' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        INPUTT = ''
        BRANCH = ''
        TRANS.ID= R.NEW(TT.TE.TRANSACTION.CODE)
        IF V$FUNCTION ='I' THEN
*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('TELLER$NAU':@FM:TT.TE.INPUTTER, ID.NEW ,INPUTT)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
INPUTT=R.ITSS.TELLER$NAU<TT.TE.INPUTTER>
            IF NOT(ETEXT) THEN
                CALL GET.INPUTTER.BRANCH(BRANCH,INPUTT)
                IF R.USER<EB.USE.DEPARTMENT.CODE> # BRANCH THEN
                    E = '��� ������� �� ��� ���'
                    CALL ERR ;MESSAGE = 'REPEAT'
**15\12\2006
*                END ELSE
*
*                    IF R.NEW(TT.TE.TRANSACTION.CODE) # 11 THEN
*
*                        CALL GET.TELLER.TRN.DESC(TRANS.ID,TRANS.DESC)
**15\12\2006 NESSREN        E = '���� �� ������� ��� ����� �� ����& ' : @FM : TRANS.DESC
*                        CALL ERR ; MESSAGE = 'REPEAT'
*
*                    END
                END

            END
        END
    END
    RETURN
END
