* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.AC.CUR

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP  = ID.COMPANY
    AC.BR = COMP[2]

    IF COMI NE '' THEN

        IF ( COMI[1,2] EQ AC.BR ) OR ( COMI[1,2] EQ '22' AND AC.BR EQ '20' ) THEN

            FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
            CALL OPF(FN.AC,F.AC)
            T.SEL="SELECT FBNK.ACCOUNT WITH @ID EQ ":COMI
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            CALL F.READ(FN.AC,KEY.LIST,R.AC,F.AC,Y.ERR)

            BO.CUR = R.NEW(BO.CURRENCY)
            AC.CUR = R.AC<AC.CURRENCY>
*---------------------------------------------------------------------
            CUS.ID = R.AC<AC.CUSTOMER>
*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

            R.NEW(BO.CUST.NAME) = CUST.NAME
            R.NEW(BO.BF.NAME)   = CUST.NAME
            CALL REBUILD.SCREEN
*---------------------------------------------------------------------
            IF BO.CUR NE AC.CUR THEN
                ETEXT = "THE CURRENCY NOT A SAME"
                CALL STORE.END.ERROR
            END
        END ELSE
            ETEXT = "��� ������ �� ��� �����"
            CALL STORE.END.ERROR
        END
    END
    RETURN
END
