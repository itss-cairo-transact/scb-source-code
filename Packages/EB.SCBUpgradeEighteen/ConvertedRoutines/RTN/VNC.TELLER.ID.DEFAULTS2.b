* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>698</Rating>
*-----------------------------------------------------------------------------
*----- WAEL  21/07/2002----

    SUBROUTINE VNC.TELLER.ID.DEFAULTS2

*1-DEFAULT (NARRATIVE.2) WITH BRANCH NAME
*2-DEFAULT (TELLER.ID.1) WITH TELLER ID OF THE USER (ANY TELLER)
*3-DEFAULT (TELLER.ID.2) WITH TELLER ID OF THE DESTINATION TELLER (HEAD TELLER)
*4-CHECK IF YOU ARE THE CORRECT USER (ANY TELLER BUT NOT VAULT) OR NOT

*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

    IF V$FUNCTION = 'I' THEN
        IF NOT(R.NEW(TT.TE.TELLER.ID.1)) THEN
            IF NOT(R.NEW(TT.TE.TELLER.ID.2)) THEN
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,OPERATOR,DEPT)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,OPERATOR,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                R.NEW(TT.TE.NARRATIVE.2) = DEPT.NAME
*==================================================================================
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.NO=R.ITSS.TELLER.USER<1>

                IF INDEX(TT.NO,'98',1) > 1 THEN E='��� ����� �������'
*E = 'YOU ARE NOT ALLOWED TO ENTER'
                IF INDEX(TT.NO,'99',1) > 1 THEN  E='��� ����� �������'
*E = 'YOU ARE NOT ALLOWED TO ENTER'

                R.NEW(TT.TE.TELLER.ID.1) = TT.NO
                IF LEN (DEPT) < 2 THEN
                    R.NEW(TT.TE.TELLER.ID.2) = 0:DEPT:98
                END

                IF LEN (DEPT) > 1 THEN
                    R.NEW(TT.TE.TELLER.ID.2) = DEPT:98
                END
            END
        END
    END
*=======================================================================================
    IF V$FUNCTION ='A' THEN
        IF INDEX(TT.NO,'98',1) > 1 THEN E='��� ����� �������'
* E = 'YOU ARE NOT ALLOWED TO ENTER'
    END


    IF V$FUNCTION ='R' THEN
        IF INDEX(TT.NO,'98',1) > 1 THEN E='��� ����� �������'
* E = 'YOU ARE NOT ALLOWED TO ENTER'
    END

*=======================================================================================
    IF E THEN CALL ERR ;MESSAGE = 'REPEAT'

    RETURN
END
