* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
********* RANIA 03/08/2003 *******************
*-----------------------------------------------------------------------------
* <Rating>98</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.MM.ROLLOVER

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET

*Line [ 31 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
* *CALL DBR ('MM.MONEY.MARKET':@FM:MM.DATE.TIME,ID.NEW,MYDATE)
F.ITSS.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET'
FN.F.ITSS.MM.MONEY.MARKET = ''
CALL OPF(F.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET)
CALL F.READ(F.ITSS.MM.MONEY.MARKET,ID.NEW,R.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET,ERROR.MM.MONEY.MARKET)
MYDATE=R.ITSS.MM.MONEY.MARKET<MM.DATE.TIME>
 *MYDATE1=MYDATE[1,6]
 *MYDATE2='20':MYDATE1

 *IF MYDATE2 EQ TODAY THEN E = 'ONLY.SEE.FUNCTION.ALLOWED' ;CALL ERR ;MESSAGE = 'REPEAT'

*Line [ 43 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
* CALL DBR ('MM.MONEY.MARKET':@FM:MM.VALUE.DATE,ID.NEW,VDATE)
F.ITSS.MM.MONEY.MARKET = 'FBNK.MM.MONEY.MARKET'
FN.F.ITSS.MM.MONEY.MARKET = ''
CALL OPF(F.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET)
CALL F.READ(F.ITSS.MM.MONEY.MARKET,ID.NEW,R.ITSS.MM.MONEY.MARKET,FN.F.ITSS.MM.MONEY.MARKET,ERROR.MM.MONEY.MARKET)
VDATE=R.ITSS.MM.MONEY.MARKET<MM.VALUE.DATE>
 IF VDATE EQ TODAY THEN E = 'ONLY.SEE.FUNCTION.ALLOWED' ;CALL ERR ;MESSAGE = 'REPEAT'

 RETURN
 END
