* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>248</Rating>
*-----------------------------------------------------------------------------
***** WAEL *** 21/7/2002

SUBROUTINE VNC.TT.RECORD.CHECK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER



GOSUB  INITIAL:
GOSUB  PR1
GOSUB  PR2
GOTO END.PROG


INITIAL:
*==================================================================================================
XX= ''
*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER$NAU':@FM:TT.TE.RECORD.STATUS,ID.NEW,XX)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
XX=R.ITSS.TELLER$NAU<TT.TE.RECORD.STATUS>
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.OP)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.OP=R.ITSS.TELLER.USER<1>

*====================GET THE TELLER.ID OF THE INPUTTER================================
X = ''
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER$NAU':@FM:TT.TE.INPUTTER,ID.NEW,Y)
F.ITSS.TELLER$NAU = 'F.TELLER$NAU'
FN.F.ITSS.TELLER$NAU = ''
CALL OPF(F.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU)
CALL F.READ(F.ITSS.TELLER$NAU,ID.NEW,R.ITSS.TELLER$NAU,FN.F.ITSS.TELLER$NAU,ERROR.TELLER$NAU)
Y=R.ITSS.TELLER$NAU<TT.TE.INPUTTER>
X = FIELD(Y,'_', 2)
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,X,TT.INP)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,X,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.INP=R.ITSS.TELLER.USER<1>
*=====================  LIVE FILE ==============================================================
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER':@FM:TT.TE.INPUTTER,ID.NEW,DD)
F.ITSS.TELLER = 'FBNK.TELLER'
FN.F.ITSS.TELLER = ''
CALL OPF(F.ITSS.TELLER,FN.F.ITSS.TELLER)
CALL F.READ(F.ITSS.TELLER,ID.NEW,R.ITSS.TELLER,FN.F.ITSS.TELLER,ERROR.TELLER)
DD=R.ITSS.TELLER<TT.TE.INPUTTER>
X = FIELD(DD,'_', 2)
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*CALL DBR ('TELLER.USER':@FM:1,X,TT.INP2)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,X,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.INP2=R.ITSS.TELLER.USER<1>


RETURN
*====================================================================================

PR1:
IF XX = 'IHLD'  THEN
 IF TT.INP # TT.OP THEN E = '��� �� ��� ���� ������' ; CALL ERR ; MESSAGE = 'REPEAT'
END

RETURN
*====================================================================================================
PR2:

    IF (XX ='INAU' OR XX='RNAU') AND V$FUNCTION # 'A' AND TT.INP # TT.OP THEN
    E = '��� ����� ����������' ; CALL ERR ; MESSAGE = 'REPEAT'
    END

      IF XX = 'INAU' AND V$FUNCTION = 'A' AND R.NEW(TT.TE.TELLER.ID.2) # TT.OP THEN
       E = '��� ����� �������� ����� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
     END




*===============================================================================================
   IF V$FUNCTION = 'R' AND TT.OP # TT.INP2 THEN
      E = '  ���� �� ��� ������� ��&': X  ; CALL ERR ; MESSAGE = 'REPEAT'
   END
*===============================================
  IF V$FUNCTION ='A' AND XX ='RNAU' THEN
     IF R.NEW(TT.TE.TELLER.ID.2) # TT.OP THEN E = '��� ����� ��������' ; CALL ERR ; MESSAGE = 'REPEAT'
  END

RETURN
*======================================================================================================

END.PROG:

RETURN
END
