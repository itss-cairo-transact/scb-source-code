* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*--- By Haytham & Nessma
    SUBROUTINE VVR.ATM.CHRG

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------------------
    R.NEW(FT.CREDIT.ACCT.NO) = "EGP1131300010099"

    IF MESSAGE EQ 'VAL' THEN
        IF R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.MESG.TYPE> EQ '0420' THEN
            DB.ACT                   = R.NEW(FT.DEBIT.ACCT.NO)
            R.NEW(FT.CREDIT.ACCT.NO) = DB.ACT
            R.NEW(FT.DEBIT.ACCT.NO)  = "EGP1131300010099"
        END
    END

    MMC     = R.NEW(FT.LOCAL.REF)<1,FTLR.MMC>
    PCO     = R.NEW(FT.LOCAL.REF)<1,FTLR.PCODE>[1,2]
    STL.CUR = R.NEW(FT.LOCAL.REF)<1,FTLR.SETTLEMENT.CUR>

    BEGIN CASE
    CASE MMC EQ '6010' AND ( PCO EQ '12' OR PCO EQ '17' )  AND STL.CUR EQ 'EGP'
        R.NEW(FT.CHARGE.TYPE)      = "ATMSCBPOSLO"
        R.NEW(FT.COMMISSION.TYPE)  = "ATMSCBPOSL"
    CASE MMC EQ '6010' AND ( PCO EQ '12' OR PCO EQ '17' ) AND STL.CUR NE 'EGP'
        R.NEW(FT.CHARGE.TYPE)<1,1> = "ATMSCBPOSIN"
*        R.NEW(FT.CHARGE.TYPE)<1,2> = "ATM123POSIN"
        R.NEW(FT.COMMISSION.TYPE)  = "ATMSCBPOSI" 
    CASE OTHERWISE
        R.NEW(FT.CHARGE.TYPE)      = ""
        R.NEW(FT.COMMISSION.TYPE)  = ""
    END CASE
    RETURN
END
