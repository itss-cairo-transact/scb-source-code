* @ValidationCode : MjoxMDg2NTM4MDI3OkNwMTI1MjoxNjQ0OTMyNTk5ODM3OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:43:19
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>162</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.BR.EMP.MAT.EXT.COLL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('CUSTOMER':@FM:EB.CUS.TEXT,COMI,POSS)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,COMI,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    POSS = R.ITSS.CUSTOMER<EB.CUS.TEXT>
    STO1=POSS<1,1>
    STO2=POSS<1,2>
    STO3=POSS<1,3>
    STO4=POSS<1,4>
    IF POSS NE '' THEN
        TEXT = " -������� �������- " : STO1 : STO2 : STO3:STO4  ; CALL REM
    END

    IF MESSAGE = '' THEN

        IF COMI # R.NEW(EB.BILL.REG.DRAWER) THEN

            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''

            CALL REBUILD.SCREEN ; P = 0

        END

        IF COMI THEN


            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT> = ''
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT> = ''

            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''

*    T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": COMI :" AND CURRENCY EQ ":R.NEW(EB.BILL.REG.CURRENCY):" AND CATEGORY EQ '1201'"
            T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": COMI :" AND CURRENCY EQ ":R.NEW(EB.BILL.REG.CURRENCY):" AND (CATEGORY GE 1101 AND  CATEGORY LE 1202  OR CATEGORY GE 1400 AND  CATEGORY LE 1590 )"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

            IF NOT(SELECTED) THEN
                ETEXT='There.Is.No.Debit.Account' ;RETURN
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT> = KEY.LIST<1>
            END

            T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
            IF PGM.VERSION = ",SCB.CHQ.LCY.REG1" OR PGM.VERSION = ",SCB.CHQ.LCY.REG1.CASH" OR PGM.VERSION = ",SCB.CHQ.OUT.OF.CLEARING" THEN
*TEXT = PGM.VERSION ; CALL REM
*CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(EB.BILL.REG.CURRENCY),CURR)
*TEXT = R.NEW(EB.BILL.REG.CURRENCY) ; CALL REM
                T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": COMI :" AND CURRENCY EQ ":R.NEW(EB.BILL.REG.CURRENCY):" AND CATEGORY EQ '9002'"
*TEXT = T.SEL ; CALL REM
            END
            IF PGM.VERSION = ",SCB.BILLS" THEN
*CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(EB.BILL.REG.CURRENCY),CURR)
                T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": COMI :" AND CURRENCY EQ ":R.NEW(EB.BILL.REG.CURRENCY):" AND CATEGORY EQ '9003'"
            END
            IF PGM.VERSION = ",SCB.CHQ.LCY.REG1.COLL1" THEN
*CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,R.NEW(EB.BILL.REG.CURRENC
                T.SEL = "SSELECT FBNK.ACCOUNT WITH CUSTOMER EQ ": COMI :" AND CURRENCY EQ ":R.NEW(EB.BILL.REG.CURRENCY):" AND CATEGORY EQ '9005'"
            END


            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )
            IF NOT(SELECTED) THEN
                ETEXT='There.Is.No.Contingent.Account'
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CONT.ACCT> = KEY.LIST<1>
            END

            CALL REBUILD.SCREEN

        END

    END
RETURN
END
