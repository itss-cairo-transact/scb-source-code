* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>150</Rating>
*-----------------------------------------------------------------------------
*----- WAEL  21/07/2002----

SUBROUTINE VNC.TT.TELLER.ID

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER.ID
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER


IF V$FUNCTION = 'I' THEN

IF NOT(R.NEW(TT.TE.TELLER.ID.1)) THEN
IF NOT(R.NEW(TT.TE.TELLER.ID.2)) THEN



*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*  CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,OPERATOR,DEPT)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,OPERATOR,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*  CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
  R.NEW(TT.TE.NARRATIVE.2) = DEPT.NAME

*==================================================================================
*Line [ 61 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*  CALL DBR ('TELLER.USER':@FM:1,OPERATOR,TT.NO)
F.ITSS.TELLER.USER = 'FBNK.TELLER.USER'
FN.F.ITSS.TELLER.USER = ''
CALL OPF(F.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER)
CALL F.READ(F.ITSS.TELLER.USER,OPERATOR,R.ITSS.TELLER.USER,FN.F.ITSS.TELLER.USER,ERROR.TELLER.USER)
TT.NO=R.ITSS.TELLER.USER<1>

X = ''
X = 0:DEPT:99

IF TT.NO # X  THEN E = 'YOU ARE NOT ALLOWED TO ENTER' ; CALL ERR ; MESSAGE = 'REPEAT'



  R.NEW(TT.TE.TRANSACTION.CODE) = 1
  R.NEW(TT.TE.TELLER.ID.1) = TT.NO
  R.NEW(TT.TE.TELLER.ID.2) = 0:DEPT:98


END
END
END

RETURN
END
