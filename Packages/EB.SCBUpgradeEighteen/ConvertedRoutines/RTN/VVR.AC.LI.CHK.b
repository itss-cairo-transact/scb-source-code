* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMED SABRY 2011/04/12 ***
*******************************************

    SUBROUTINE VVR.AC.LI.CHK
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    WS.COMP = ID.COMPANY

    IF WS.COMP # 'EG0010099' THEN
        IF COMI EQ '' THEN
            WS.GL = R.NEW(AC.CATEGORY)
            WS.CUS = R.NEW(AC.CUSTOMER)
            IF ( WS.GL GE 1101 AND WS.GL LE 1599 ) THEN
                IF ( WS.GL LT 1220 OR WS.GL GT 1227 ) THEN
                    IF WS.CUS # 70300308 AND WS.GL # 1262 THEN
                        ETEXT = "��� ��� ������ ���� �������"
                        CALL STORE.END.ERROR
                    END
                END
            END
        END
    END
*MSABRY 2011/05/11 STOP ROUTINE 'VVR.AC.LEDG.ACCT' AND EMPTY THE ( LINK.TO.LIMIT ) IN THIS ROUTINE After refer to the Mr. Bakry
*    IF R.NEW(AC.LINK.TO.LIMIT)THEN R.NEW(AC.LINK.TO.LIMIT) = ''

    RETURN
