* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* Create by Saizo
* Edit by Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.READER

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

*u5158977ut000100010t9300702106u

    FIRST.LETTER = COMI[1,1]

    IF FIRST.LETTER EQ "U" THEN
        IF COMI THEN

            CHQ.ID = COMI
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO> = FIELD(CHQ.ID,"U",2)

            SAM     = FIELD(CHQ.ID,"T",2)
            BANK    = SAM[1,4]
            BANK.BR = SAM[3,6]
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>    = BANK
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR> = BANK.BR

            VAR1    = FIELD(CHQ.ID,"T",3)
            VAR2    = LEN(VAR1) - 1
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.DR.CITY> = VAR1[1,VAR2]
        END
    END

**** u11000004178ou001700118u2103150o

    IF FIRST.LETTER EQ "u" THEN
        IF COMI THEN

            CHQ.ID = COMI

            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO> = FIELD(CHQ.ID,"u",2)

            SAM     = FIELD(CHQ.ID,"t",2)
            BANK    = SAM[1,4]
            BANK.BR = SAM[3,6]
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>    = BANK
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR> = BANK.BR

            VAR1    = FIELD(CHQ.ID,"t",3)
            VAR2    = LEN(VAR1) - 1
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.DR.CITY> = VAR1[1,VAR2]
        END
    END

    CALL REBUILD.SCREEN
    RETURN
END
