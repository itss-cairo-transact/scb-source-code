* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-5</Rating>
*-----------------------------------------------------------------------------
* ----- 20.06.2002 MOHAMED MAHMOUD (MRA GROUP) -----

    SUBROUTINE VVR.AC.HOLD.DETAILS
*A ROUTINE TO CHECK THAT INPUTTERS FROM DIFFERENT DEPARTMENTS CAN'T UPDATE/DELETE EACH OTHERS WORK

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.HOLD

    IF MESSAGE = 'VAL' THEN

*Line [ 41 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        MYOLD = DCOUNT(R.OLD( AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE>,@SM)
*Line [ 43 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        MYNEW = DCOUNT(R.NEW( AC.LOCAL.REF)<1, ACLR.EXPIRY.DATE>,@SM)
        XX = '0'
        DEPT.CODE = ''
        DEPT.NAME = ''
        DEPT.CODE = R.USER<EB.USE.DEPARTMENT.CODE>
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.CODE,DEPT.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT.CODE,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>


*---------- CHECK THAT EVERY OLD RECORD FOR OTHER DEPT'S STILL EXISTING IN THE NEW RECORDS ------------
*            -----------------------------------------------------------------------------

        NO.OLD.HOLDS = 0 ; NO.NEW.HOLDS = 0

        FOR I= 1 TO MYOLD
            DEPT = ''
            DEPT = R.OLD(AC.LOCAL.REF)<1, ACLR.HOLD.USER,I>
            DEPT = FIELD(DEPT,'-',2,1)
*       IF DEPT NE DEPT.NAME  THEN
            NO.OLD.HOLDS = NO.OLD.HOLDS + 1
        NEXT I

        FOR I= 1 TO MYNEW
            DEPT = ''
            DEPT = R.NEW(AC.LOCAL.REF)<1, ACLR.HOLD.USER,I>
            DEPT = FIELD(DEPT,'-',2,1)
*      IF DEPT NE DEPT.NAME  THEN
            NO.NEW.HOLDS = NO.NEW.HOLDS + 1
        NEXT I

        IF NO.OLD.HOLDS GT NO.NEW.HOLDS THEN

            CALL REBUILD.SCREEN
*   MYERROR = 'While.Not.Authorized'
* E = 'Sorry.You.Deleted.Record. & '
            MYERROR = '��� �����'
            E = ' ���� ��� �� ������ ����& ' : MYERROR
            CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            R.NEW(AC.LOCAL.REF)<1, ACLR.VERSION.NAME> = PGM.VERSION
            CALL REBUILD.SCREEN
        END

*-------

    END

    RETURN
END
