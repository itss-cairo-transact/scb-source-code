* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>808</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.CHQ.CHECK.0017(N.COMI)

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
****UPDATED BY NESSREEN AHMED 6/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.PRESENTED
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
****UPDATED BY NESSREEN AHMED 6/3/2016 for R15****
****$INCLUDE T24.BP I_F.CHEQUES.STOPPED
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.TYPE.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER.SUPPLEMENT
****END OF UPDATE 6/3/2016**************************
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*----------------------------------------------------------
    CHEQ.ID = N.COMI
    ETEXT   = ''
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> NE '' THEN
        FOR I = 1 TO 100
            ACCT.NO    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>
            PAY.CHQ.ID = CHEQ.ID:'.': I
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('SCB.P.CHEQ':@FM:P.CHEQ.ACCOUNT.NO,PAY.CHQ.ID,MYACCN)
F.ITSS.SCB.P.CHEQ = 'F.SCB.P.CHEQ'
FN.F.ITSS.SCB.P.CHEQ = ''
CALL OPF(F.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ)
CALL F.READ(F.ITSS.SCB.P.CHEQ,PAY.CHQ.ID,R.ITSS.SCB.P.CHEQ,FN.F.ITSS.SCB.P.CHEQ,ERROR.SCB.P.CHEQ)
MYACCN=R.ITSS.SCB.P.CHEQ<P.CHEQ.ACCOUNT.NO>
            IF MYACCN THEN
                ETEXT='��� ����� �� ����' ; CALL STORE.END.ERROR
                CALL REBUILD.SCREEN
                I = 100
            END
        NEXT I

        IF NOT(MYACCN) THEN
*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCT.NO,CUST.AC)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.AC=R.ITSS.ACCOUNT<AC.CUSTOMER>
            FN.CUAC = 'F.CUSTOMER.ACCOUNT' ; F.CUAC = '' ; R.CUAC = ''  ; E1.CUAC = ''
            CALL OPF(FN.CUAC,F.CUAC)

            CALL F.READ(FN.CUAC, CUST.AC , R.CUAC, F.CUAC, E1.CUAC)
            LOOP
                REMOVE ACC.ID FROM R.CUAC SETTING SUBVAL
            WHILE ACC.ID:SUBVAL
****UPDATED BY NESSREEN AHMED 6/3/2016 for R15****
**** CHQ.ID = ACC.ID:'*':COMI
****CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID,CHQ.STOP)
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR ('CHEQUE.TYPE.ACCOUNT':@FM:CHQ.TYP.CHEQUE.TYPE,ACC.ID,CHQ.TYPE)
F.ITSS.CHEQUE.TYPE.ACCOUNT = 'FBNK.CHEQUE.TYPE.ACCOUNT'
FN.F.ITSS.CHEQUE.TYPE.ACCOUNT = ''
CALL OPF(F.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT)
CALL F.READ(F.ITSS.CHEQUE.TYPE.ACCOUNT,ACC.ID,R.ITSS.CHEQUE.TYPE.ACCOUNT,FN.F.ITSS.CHEQUE.TYPE.ACCOUNT,ERROR.CHEQUE.TYPE.ACCOUNT)
CHQ.TYPE=R.ITSS.CHEQUE.TYPE.ACCOUNT<CHQ.TYP.CHEQUE.TYPE>
                TY = CHQ.TYPE
*Line [ 86 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT (TY,@VM)
                FOR X = 1 TO DD
                    CHQ.ID = CHQ.TYPE<1,X>:'.':ACC.ID:'.':COMI
*Line [ 108 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR ('CHEQUE.REGISTER.SUPPLEMENT':@FM:CC.CRS.DATE.STOPPED,CHQ.ID,CHQ.STOP)
F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = 'FBNK.CHEQUE.REGISTER.SUPPLEMENT'
FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT = ''
CALL OPF(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT)
CALL F.READ(F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,CHQ.ID,R.ITSS.CHEQUE.REGISTER.SUPPLEMENT,FN.F.ITSS.CHEQUE.REGISTER.SUPPLEMENT,ERROR.CHEQUE.REGISTER.SUPPLEMENT)
CHQ.STOP=R.ITSS.CHEQUE.REGISTER.SUPPLEMENT<CC.CRS.DATE.STOPPED>
                    IF CHQ.STOP THEN
                        ETEXT='��� ����� �����'
                        CALL STORE.END.ERROR
                    END
                NEXT
****END OF UPDATE 6/3/2016*****************************
            REPEAT
        END
*----------------------------------------------------------------  **
        ERR.MSG = ''
        ETEXT = ''
        FN.CHQ.PRES = 'F.SCB.FT.DR.CHQ' ; F.CHQ.PRES = ''
        R.CHQ.PRES  = ''
        CALL OPF(FN.CHQ.PRES,F.CHQ.PRES)
**  --------------------------------------------------------------  **
**             UPDATED BY NESSREEN AHMED ON 26/08/2009              **
**  --------------------------------------------------------------  **
        KEY.LIST.DR = ""  ;  ER.MSG = ""  ; SELECTED.DR = ""

        T.SEL.DR  = "SELECT F.SCB.FT.DR.CHQ WITH (CHEQ.NO EQ ": CHEQ.ID:" OR OLD.CHEQUE.NO EQ ": CHEQ.ID:")"
        T.SEL.DR := " AND CHEQ.TYPE LIKE ...DRFT... "
        CALL EB.READLIST(T.SEL.DR,KEY.LIST.DR,"",SELECTED.DR,ER.MSG)
        IF SELECTED.DR THEN
            ETEXT= "���� ��� ����� ����� ���� �����"  ; CALL STORE.END.ERROR
        END
*    ------------------------------------------------------------    *
        SELECTED.LK = ""
        F.COUNT     = '' ;  FN.COUNT = 'F.AC.LOCKED.EVENTS'
        CALL OPF(FN.COUNT,F.COUNT)
        LOCAL.REF = FN.COUNT<AC.LCK.LOCAL.REF>
        CHQ.NO    = LOCAL.REF<1,AC.LCK.LOC.CHQ.NO>
        T.SEL     = "SELECT FBNK.AC.LOCKED.EVENTS WITH CHQ.NO EQ ":CHEQ.ID:" AND ACCOUNT.NUMBER EQ ":ACCT.NO
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED.LK,ER.MSG)

        IF SELECTED.LK THEN
            ETEXT = "��� ����� ���� ����" ;CALL STORE.END.ERROR
        END

        CALL REBUILD.SCREEN
**------------------------------------------------------
**-------------------  CHECK  ISSUE --------------------
        CUS.ID  = CUST.AC
        CHQ.NO  = CHEQ.ID
        FLAG    = 0

        FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT'     ; R.CUS.AC = ''   ;  F.CUS.AC  = ''
        CALL OPF(FN.CUS.AC,F.CUS.AC)

        FN.CHQ.AC ='FBNK.CHEQUE.ISSUE.ACCOUNT' ; R.CHQ.AC = ''   ;  F.CHQ.AC  = ''
        CALL OPF(FN.CHQ.AC,F.CHQ.AC)

        FN.CHQ.REG ='FBNK.CHEQUE.REGISTER'     ; R.CHQ.REG = ''  ;  F.CHQ.REG =''
        CALL OPF(FN.CHQ.REG,F.CHQ.REG)
*========== a) To Check IF Cheq Is Issued ===================================
        T.SEL ="SELECT FBNK.CHEQUE.REGISTER WITH CUSTOMER EQ ": CUS.ID
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.CHQ.REG,KEY.LIST<I>, R.CHQ.REG, F.CHQ.REG , ERR.CHQ)
                CHQ.NOS = R.CHQ.REG<CHEQUE.REG.CHEQUE.NOS>
                TEXT = 'CHQ.NOS=':CHQ.NOS ; CALL REM
*Line [ 153 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DD = DCOUNT(CHQ.NOS,@VM)
                FOR X = 1 TO DD
                    CHN = CHQ.NOS<1,X>
                    SS  = COUNT(CHN, "-")
                    IF SS > 0 THEN
                        ST.NO   = FIELD(CHN ,"-", 1)
                        ED.NO   = FIELD(CHN ,"-", 2)
                    END ELSE
                        ST.NO   = CHN
                        ED.NO   = CHN
                    END
                    IF (CHQ.NO GE ST.NO) AND (CHQ.NO LE ED.NO) THEN
                        FLAG = "1"
                    END ELSE
                    END
                NEXT X
            NEXT I
        END

        ETEXT   = ''
        IF FLAG = "0" THEN
            ETEXT = '��� ����� �� ���� ������ ' ; CALL STORE.END.ERROR
        END
        ETEXT = ''
    END ELSE
        ETEXT = 'CUST ACCOUNT IS MISSING'
        CALL STORE.END.ERROR
    END
**-------------------------------------------------------------------
    RETURN
END
