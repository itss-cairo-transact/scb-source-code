* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE VVR.ATM.CHR


*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*-----------------------------------------
    FN.CUR ='FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    IF COMI THEN
        AMT.DEB             = R.NEW(FT.DEBIT.AMOUNT)
        CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,ERR1)
        RATE.USD            = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
        AMT.EGP             = COMI * RATE.USD
        AMT                 =  AMT.DEB + AMT.EGP
        AMT.PER             = DROUND((AMT * 0.00114),2)


        IF AMT.PER LE '11.4' THEN
            AMT.FIN = 11.4
        END

        IF AMT.PER GE  '114' THEN
            AMT.FIN = 114
        END
        IF AMT.PER GT '11.4' AND AMT.PER LT '114' THEN
            AMT.FIN = AMT.PER
        END
        R.NEW(FT.COMMISSION.AMT)<1,2>  = 'EGP' : AMT.FIN
        CALL REBUILD.SCREEN

    END
    RETURN
END
