* @ValidationCode : MjoxNTQ2OTA0MjI3OkNwMTI1MjoxNjQ0OTMxODQ2ODI5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:30:46
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>952</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VNC.SCB.WH.ITEMS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.ITEMS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.ITEMS.SERIAL
*------------------------------------------------
    IF V$FUNCTION = "R" THEN
        E = "�� ���� �������"
        CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
    END
*------------------------------------------------
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SCB.WH.ITEMS.SERIAL':@FM:SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16],SER.ID)
    F.ITSS.SCB.WH.ITEMS.SERIAL = 'F.SCB.WH.ITEMS.SERIAL'
    FN.F.ITSS.SCB.WH.ITEMS.SERIAL = ''
    CALL OPF(F.ITSS.SCB.WH.ITEMS.SERIAL,FN.F.ITSS.SCB.WH.ITEMS.SERIAL)
    CALL F.READ(F.ITSS.SCB.WH.ITEMS.SERIAL,ID.NEW[3,16],R.ITSS.SCB.WH.ITEMS.SERIAL,FN.F.ITSS.SCB.WH.ITEMS.SERIAL,ERROR.SCB.WH.ITEMS.SERIAL)
    SER.ID=R.ITSS.SCB.WH.ITEMS.SERIAL<SCB.WH.IT.SER.SERIAL.NO>
    IF V$FUNCTION = "I" THEN
*---- 2011/03/01 BY NESSMA ----
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('SCB.WH.ITEMS':@FM:0,ID.NEW,E.SER)
        F.ITSS.SCB.WH.ITEMS = 'F.SCB.WH.ITEMS'
        FN.F.ITSS.SCB.WH.ITEMS = ''
        CALL OPF(F.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS)
        CALL F.READ(F.ITSS.SCB.WH.ITEMS,ID.NEW,R.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS,ERROR.SCB.WH.ITEMS)
        E.SER=R.ITSS.SCB.WH.ITEMS<0>
        IF E.SER NE '' THEN
            E = "�� ���� ������� �� ��� ������"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
        END
*---
        MY.SER = FIELD(ID.NEW,'.',2)
        MY.SER = TRIM(MY.SER , "0", "L")

        IF MY.SER GT SER.ID THEN

            IF SER.ID EQ '' THEN
                SER.ID = 1
                ID.NEW = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
            END ELSE
                SER.ID  = SER.ID + 1
                ID.NEW  = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
            END
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*            CALL DBR('SCB.WH.ITEMS':@FM:0,ID.NEW,ITEM.EXIST)
            F.ITSS.SCB.WH.ITEMS = 'F.SCB.WH.ITEMS'
            FN.F.ITSS.SCB.WH.ITEMS = ''
            CALL OPF(F.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS)
            CALL F.READ(F.ITSS.SCB.WH.ITEMS,ID.NEW,R.ITSS.SCB.WH.ITEMS,FN.F.ITSS.SCB.WH.ITEMS,ERROR.SCB.WH.ITEMS)
            ITEM.EXIST=R.ITSS.SCB.WH.ITEMS<0>
            IF ITEM.EXIST EQ '' THEN
                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[4,7]
                END ELSE
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[3,8]
                END


*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR("NUMERIC.CURRENCY":@FM:EB.NCN.CURRENCY.CODE,ID.NEW[11,2],CURR)
                F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
                FN.F.ITSS.NUMERIC.CURRENCY = ''
                CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
                CALL F.READ(F.ITSS.NUMERIC.CURRENCY,ID.NEW[11,2],R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
                CURR=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE,ID.NEW[11,2]>
                R.NEW(SCB.WH.IT.CURRENCY)        = CURR
                R.NEW(SCB.WH.IT.CATEGORY)        = ID.NEW[13,4]
                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[4,1]
                END ELSE
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[3,2]
                END


                R.NEW(SCB.WH.IT.WHAREHOUSE.NO)   = ID.NEW[17,2]
                R.NEW(SCB.WH.IT.VERSION.NAME)    = PGM.VERSION

*R.NEW(SCB.WH.IT.DESCRIPTION)     = ""
*R.NEW(SCB.WH.IT.UNITS.BALANCE)   = ""
*R.NEW(SCB.WH.IT.VALUE.BALANCE)   = ""
*R.NEW(SCB.WH.IT.UNIT.OF.STORE)   = ""
*R.NEW(SCB.WH.IT.UNIT.PRICE)      = ""
                R.NEW(SCB.WH.IT.WH.OPENING.DATE) = TODAY
*R.NEW(SCB.WH.IT.EXPIRING.DATE)   = ""
*R.NEW(SCB.WH.IT.TRANSACTION.TYPE)= ""
*R.NEW(SCB.WH.IT.NO.OF.UNITS)     = ""
*R.NEW(SCB.WH.IT.VALUE.DATE)      = ""
            END
        END ELSE
*--------------------- NOT BY HAND
            IF MY.SER EQ '' THEN
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR('SCB.WH.ITEMS.SERIAL':@FM:SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16],SER.ID)
                F.ITSS.SCB.WH.ITEMS.SERIAL = 'F.SCB.WH.ITEMS.SERIAL'
                FN.F.ITSS.SCB.WH.ITEMS.SERIAL = ''
                CALL OPF(F.ITSS.SCB.WH.ITEMS.SERIAL,FN.F.ITSS.SCB.WH.ITEMS.SERIAL)
                CALL F.READ(F.ITSS.SCB.WH.ITEMS.SERIAL,ID.NEW[3,16],R.ITSS.SCB.WH.ITEMS.SERIAL,FN.F.ITSS.SCB.WH.ITEMS.SERIAL,ERROR.SCB.WH.ITEMS.SERIAL)
                SER.ID=R.ITSS.SCB.WH.ITEMS.SERIAL<SCB.WH.IT.SER.SERIAL.NO,ID.NEW[3,16]>

                IF SER.ID EQ '' THEN
                    SER.ID = 1
                    ID.NEW = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
                END ELSE
                    SER.ID = SER.ID + 1
                    ID.NEW = "WH":ID.NEW[3,16] :".":STR('0',5 - LEN(SER.ID)):SER.ID
*---- 2010/03/10 EDITED BY NESSMA
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                    CALL DBR('SCB.WH.ITEMS$NAU':@FM:0,ID.NEW,ERR.ITEM)
                    F.ITSS.SCB.WH.ITEMS$NAU = 'F.SCB.WH.ITEMS$NAU'
                    FN.F.ITSS.SCB.WH.ITEMS$NAU = ''
                    CALL OPF(F.ITSS.SCB.WH.ITEMS$NAU,FN.F.ITSS.SCB.WH.ITEMS$NAU)
                    CALL F.READ(F.ITSS.SCB.WH.ITEMS$NAU,ID.NEW,R.ITSS.SCB.WH.ITEMS$NAU,FN.F.ITSS.SCB.WH.ITEMS$NAU,ERROR.SCB.WH.ITEMS$NAU)
                    ERR.ITEM=R.ITSS.SCB.WH.ITEMS$NAU<0>
                    IF ERR.ITEM NE '' THEN
                        E = '���� ����� ����� ����'
                        CALL ERR;MESSAGE='REPEAT'
                        CALL STORE.END.ERROR
                    END
*----
                END

                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[4,7]
                END ELSE
                    R.NEW(SCB.WH.IT.CUSTOMER)      = ID.NEW[3,8]
                END

*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*                CALL DBR("NUMERIC.CURRENCY":@FM:EB.NCN.CURRENCY.CODE,ID.NEW[11,2],CURR)
                F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
                FN.F.ITSS.NUMERIC.CURRENCY = ''
                CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
                CALL F.READ(F.ITSS.NUMERIC.CURRENCY,ID.NEW[11,2],R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
                CURR=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE,ID.NEW[11,2]>
                R.NEW(SCB.WH.IT.CURRENCY)        = CURR
                R.NEW(SCB.WH.IT.CATEGORY)        = ID.NEW[13,4]
                IF ID.NEW[3,1] = "0" THEN
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[4,1]
                END ELSE
                    R.NEW(SCB.WH.IT.BRANCH)          = ID.NEW[3,2]
                END

                R.NEW(SCB.WH.IT.WHAREHOUSE.NO)   = ID.NEW[17,2]
                R.NEW(SCB.WH.IT.VERSION.NAME)    = PGM.VERSION

                R.NEW(SCB.WH.IT.WH.OPENING.DATE) = TODAY
            END
        END
**********************
        WH.ACCT = FIELD(ID.NEW,'.',1)
        MY.ACCT = WH.ACCT[3,16]

*Line [ 183 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,MY.ACCT,CURR)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,MY.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
        IF CURR EQ '' THEN
            E = "��� ������ ��� �����"
            CALL ERR;MESSAGE='REPEAT'
            CALL STORE.END.ERROR
        END
    END
RETURN
END
