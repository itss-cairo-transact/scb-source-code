* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeEighteen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeEighteen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.SCB.SAFEKEEP.UNIQUE.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SAFEKEEP
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*IF V$FUNCTION = 'I' THEN
*---- NESSMA -----
    BRN  = R.USER<EB.USE.DEPARTMENT.CODE>
    XX   = BRN
    IF LEN(BRN) EQ 1 THEN
        BRN = "0" : BRN
    END
    SF.ID = ID.NEW
*TEXT = "IN ROT ID = " : ID.NEW ; CALL REM

    FN.SF = "F.SCB.SAFEKEEP" ; F.SF = ""
    CALL OPF(FN.SF,F.SF)
    CALL F.READ(FN.SF,SF.ID, R.SF, F.SF, E.SF)

    IF E.SF NE '' THEN
        FN.SFF = "F.SCB.SAFEKEEP$NAU" ; F.SFF = ""
        CALL OPF(FN.SFF,F.SFF)
        CALL F.READ(FN.SFF,SF.ID,RR.SF, F.SFF, EE.SF)

        IF EE.SF NE '' THEN
            R.NEW(SCB.SAF.BRANCH) = XX
        END
    END
*-----------------
*Line [ 58 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-15
*    CALL DBR('SCB.SAFEKEEP':@FM:SCB.SAF.TYPE, SF.ID , TMP.TYPE)
F.ITSS.SCB.SAFEKEEP = 'F.SCB.SAFEKEEP'
FN.F.ITSS.SCB.SAFEKEEP = ''
CALL OPF(F.ITSS.SCB.SAFEKEEP,FN.F.ITSS.SCB.SAFEKEEP)
CALL F.READ(F.ITSS.SCB.SAFEKEEP,SF.ID,R.ITSS.SCB.SAFEKEEP,FN.F.ITSS.SCB.SAFEKEEP,ERROR.SCB.SAFEKEEP)
TMP.TYPE=R.ITSS.SCB.SAFEKEEP<SCB.SAF.TYPE>
    IF TMP.TYPE THEN
*********** E = 'THIS RECORD IS EXIST '  ; CALL ERR ; MESSAGE = 'REPEAT'
    END

    TMP.BR     = SF.ID[1,2]
    TMP.USR.BR = R.USER< EB.USE.DEPARTMENT.CODE>
    IF LEN(TMP.USR.BR) EQ 1 THEN
        TMP.USR.BR = "0": TMP.USR.BR
    END

    IF TMP.USR.BR # TMP.BR THEN
        E = 'THIS NOT THE SAME BRANCH'
*** SCB UPG 20160621 - S
*   CALL ERR;MESSAGE='REPEAT'
        CALL STORE.END.ERROR
*** SCB UPG 20160621 - E
    END
*END

    RETURN
END
***************************************************************************************************
