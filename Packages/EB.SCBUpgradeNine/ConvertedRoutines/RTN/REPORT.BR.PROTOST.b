* @ValidationCode : MjotNzM3ODMxNDU2OkNwMTI1MjoxNjQ0OTM2MTk1MTA0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:43:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NI7OOOOOOOOOOOOOOO**********************
*-----------------------------------------------------------------------------
* <Rating>383</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.BR.PROTOST

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.BR.RETURN'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)
*--------------------------------------
* YTEXT = "Enter the BT No. : "
* CALL TXTINP(YTEXT, 8, 22, "14", "A")
* CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
* BT.ID = COMI

* T.SEL = "SELECT F.SCB.BT.BATCH WITH @ID EQ ": COMI
* CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* IF SELECTED THEN
* CALL F.READ(FN.BATCH,KEY.LIST,R.BATCH,F.BATCH,E1)
*------------------------------------------------------------------------
    BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)
    TEXT = BR.ID ; CALL REM
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
    TEXT ="COUNT= ": DD; CALL REM

    FOR I = 1 TO DD
        BR.ID1 = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
***UPDATE BY NESSMA IF BR.ID1 NE ...
        CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,ERR1)
        BR.COM = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.COMM.CCY.AMT>
* IF BR.COM NE 0 THEN
        TEXT = "IN"  ; CALL REM
        RES = R.NEW(SCB.BT.RETURN.REASON)
        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> EQ 500 THEN
            TEXT = BR.ID1 ; CALL REM
*------------------------------------------------------------------------
            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
            DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>
*Line [ 104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>

            FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                CUST.ADDRESS1 = "���� ��������� ������"
            END ELSE
                CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            END


            SCB.CO     = R.BR<EB.BILL.REG.CO.CODE>
            START.DATE = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.STATUS.DATE>
            ST.DATE    = START.DATE[7,2]:'/':START.DATE[5,2]:"/":START.DATE[1,4]
            BANK.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.INIT.BANK.BENEF>
            BRANCH.NO  = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.INIT.BRANCH.BEN>
            TEXT = BANK.NO ; CALL REM
            AMT.NO     = R.BR<EB.BILL.REG.AMOUNT>
            DR.NAME    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.DR.NAME>
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.NO,BNAME)
            F.ITSS.SCB.BANK = 'F.SCB.BANK'
            FN.F.ITSS.SCB.BANK = ''
            CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
            CALL F.READ(F.ITSS.SCB.BANK,BANK.NO,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
            BNAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*Line [ 137 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BRANCH.NO,BRNAME)
            F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
            FN.F.ITSS.SCB.BANK.BRANCH = ''
            CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
            CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BRANCH.NO,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
            BRNAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>

            AMOUNT    = R.NEW(SCB.BT.CHRG.AMT)
            IN.AMOUNT = AMOUNT
            IF IN.AMOUNT = '' THEN
                IN.AMOUNT = 0
            END
            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 136 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 155 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*  OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT  = TODAY
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
            F.ITSS.COMPANY = 'F.COMPANY'
            FN.F.ITSS.COMPANY = ''
            CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
            CALL F.READ(F.ITSS.COMPANY,SCB.CO,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
            BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*YYBRN  = FIELD(BRANCH,'.',2)
            YYBRN  = BRANCH

            INPUTTER = R.NEW(SCB.BT.INPUTTER)
            AUTH     = R.NEW(SCB.BT.AUTHORISER)
            INP = FIELD(INPUTTER,'_',2)
            AUTHI =FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX11  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)  ; XX10  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

*============
            XX1<1,1>[3,15]    = '������  : ' : BNAME :"/": BRNAME
            XX2<1,1>[3,15]    = '�������: ������� ����� ����� ������� ����� ' :" " : AMT.NO
            XX3<1,1>[3,15]    = '��� : ' : @ID
            XX4<1,1>[3,15]    = '��  : ' : DR.NAME
            XX5<1,1>[3,15]    = '-------------------------------------------------------------------------'
            XX6<1,1>[3,15]    = '���� ���� ���� � '
            XX7<1,1>[3,15]    = '����� ������ ������ ����� ����� ' : " " : '���� ���� ������ �������'
            XX8<1,1>[3,15]    = '�������� �� ��������� ��������'
            XX9<1,1>[30,15]   = '������� ����� ���� �������� �'
            XX10<1,1>[50,15]  = '��� ���� ������'
            XX11<1,1>[50,15]  = '���  : ' : YYBRN

*XX9<1,1>[20,15] = BR.DATA
*-------------------------------------------------------------------
*CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 203 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
            F.ITSS.COMPANY = 'F.COMPANY'
            FN.F.ITSS.COMPANY = ''
            CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
            CALL F.READ(F.ITSS.COMPANY,SCB.CO,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
            BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*YYBRN  = FIELD(BRANCH,'.',2)
            YYBRN  = BRANCH

            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
* PR.HD  ="'L'":SPACE(1):"��� ���� ������"
* PR.HD :="'L'":"������� : ":T.DAY
* PR.HD :="'L'":"����� : ":YYBRN
            PR.HD  ="'L'":"SCB.PROTOSTO"
            PR.HD  ="'L'": YYBRN : "��" : ST.DATE
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT XX5<1,1>
            PRINT XX6<1,1>
            PRINT XX7<1,1>
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX10<1,1>
            PRINT XX11<1,1>
*    END
        END
    NEXT I
*===============================================================
RETURN
END
