* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.COLL.BLOCK.CONV

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    IF R.NEW(COLL.APPLICATION.ID)[1,2] NE "LD" THEN

        GOSUB INITIALISE
        GOSUB BUILD.RECORD
***** SCB R15 UPG 20160628 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
        RETURN
*------------------------------
INITIALISE:

        FN.OFS.SOURCE ="F.OFS.SOURCE"
        F.OFS.SOURCE = ""

        CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
        CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

        FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
        F.OFS.IN         = 0
        OFS.REC          = ""
        OFS.OPERATION    = "AC.LOCKED.EVENTS"
        OFS.OPTIONS      = "COLL"
***OFS.USER.INFO    = "/"

        COMP = C$ID.COMPANY
        COM.CODE = COMP[8,2]
        OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP

        OFS.TRANS.ID     = ""
        OFS.MESSAGE.DATA = ""

        RETURN
*----------------------------------------------------
BUILD.RECORD:

        COMMA = ","

************************************************************

        EX.VALUE = R.NEW(COLL.NOMINAL.VALUE)
        THRD.VALUE = R.NEW(COLL.THIRD.PARTY.VALUE)
        NET.VALUE = EX.VALUE - THRD.VALUE

        OFS.MESSAGE.DATA  =  "ACCOUNT.NUMBER=":R.NEW(COLL.APPLICATION.ID):COMMA
        OFS.MESSAGE.DATA :=  "DESCRIPTION=":ID.NEW:COMMA
        OFS.MESSAGE.DATA :=  "FROM.DATE=":TODAY:COMMA
        OFS.MESSAGE.DATA :=  "LOCKED.AMOUNT=":NET.VALUE

*============================
***        IF V$FUNCTION = 'A'  AND R.NEW(COLL.RECORD.STATUS) EQ 'INAU' THEN

        ADD.LEN = LEN(R.NEW(COLL.ADDRESS))
        IF ADD.LEN NE '14' THEN
            OFS.TRANS.ID  =''
        END ELSE
            OFS.TRANS.ID  = R.NEW(COLL.ADDRESS)
        END
        F.PATH = FN.OFS.IN
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.NEW:"_":R.NEW(AC.PAY.FIRST.CHEQUE.NO) :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160628 - S
        GOSUB OPM.PROCESS
***** SCB R15 UPG 20160628 - E

***     END

        IF V$FUNCTION = 'A' AND  R.NEW(COLL.RECORD.STATUS)='RNAU' THEN
            OFS.OPTIONS   = "COLLR/R"
            OFS.TRANS.ID  = R.NEW(COLL.ADDRESS)

            F.PATH = FN.OFS.IN
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.NEW:"_":R.NEW(AC.PAY.FIRST.CHEQUE.NO) :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

***** SCB R15 UPG 20160628 - S
            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160628 - E

        END

***** SCB R15 UPG 20160628 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E

    END
    RETURN

***** SCB R15 UPG 20160628 - S
*--
OPM.PROCESS:
*--

*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*
    RETURN
***** SCB R15 UPG 20160628 - E

END
