* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>481</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE RPT.BR.CHQ.TENOR.BANKS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 55 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 56 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB


    RETURN
*==============================================================
INITIATE:
****    REPORT.ID='RPT.BR.CHQ.TENOR.BANKS'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*===============================================================
CALLDB:
    YTEXT = "���� ����� ������ YYYYMMDD"
    CALL TXTINP(YTEXT, 8, 22, "8", "ANY")

* COMI = "20060103"
    IF COMI AND LEN(COMI) EQ "8" AND NUM(COMI)  THEN
        COUNT.BR = 0
        COUNT.BNK = 0
        FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
        CALL OPF(FN.BR,F.BR)

***   T.SEL = "SELECT FBNK.BILL.REGISTER WITH BIL.CHQ.TYPE NE 9 AND BIL.CHQ.TYPE NE 10 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7) AND BILL.CHQ.STA EQ 5 AND MATURITY.EXT EQ ":COMI:" BY BANK BY BANK.BR "
***   T.SEL := 'EVAL"BANK.BR':":'|':":'BANK':":'|':":'AMOUNT"'
** T.SEL = "SELECT FBNK.BILL.REGISTER WITH BIL.CHQ.TYPE NE 9 AND BIL.CHQ.TYPE NE 10 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7)  AND ( BANK NE 9902 AND BANK NE 0017 ) AND BILL.CHQ.STA EQ 5 AND MATURITY.EXT EQ ":COMI:" AND CO.CODE EQ ":COMP:" BY BANK BY BANK.BR "
        T.SEL = "SELECT FBNK.BILL.REGISTER WITH BIL.CHQ.TYPE NE 9 AND BIL.CHQ.TYPE NE 10 AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7)  AND IN.OUT.BILL EQ 'YES' AND BILL.CHQ.STA EQ 5 AND MATURITY.EXT EQ ":COMI:" AND CO.CODE EQ ":COMP:" BY BANK BY BANK.BR "
        T.SEL := 'EVAL"BANK.BR':":'|':":'BANK':":'|':":'AMOUNT"'
        KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

*        TEXT = SELECTED ; CALL REM

        BNK.BR.F = FIELD(KEY.LIST<1>,'|',1)
        BNK.F = FIELD(KEY.LIST<1>,'|',2)
        AMT.F = FIELD(KEY.LIST<1>,'|',3)
        TOT.BNK.BR = AMT.F
        TOT.BNK = AMT.F
        COUNT.BR = 1
        COUNT.BNK = 1
        TOOOT =''
        TOTOT =''
*DEBUG
** FOR I = 2 TO SELECTED
        I = 1
        FOR I = 2 TO SELECTED
            BNK.BR = FIELD(KEY.LIST<I>,'|',1)
            BNK = FIELD(KEY.LIST<I>,'|',2)
            AMT= FIELD(KEY.LIST<I>,'|',3)
*            TOOOT = ''
*            TOTOT =''
            IF BNK.F NE BNK THEN
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BNK.BR.F,BRANCH.NAME)
F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
FN.F.ITSS.SCB.BANK.BRANCH = ''
CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BNK.BR.F,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
BRANCH.NAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BNK.F,BANK.NAME)
F.ITSS.SCB.BANK = 'F.SCB.BANK'
FN.F.ITSS.SCB.BANK = ''
CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
CALL F.READ(F.ITSS.SCB.BANK,BNK.F,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
BANK.NAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>

                XX = LEN(BANK.NAME)
                AA = 40 - XX

                SS = LEN(BRANCH.NAME)
                EE = 50 - SS
                WW = LEN(TOT.BNK.BR)
                RR = 20 - WW
                QQ = LEN(TOT.BNK)
                TT = 20 - QQ
*                PRINT SPACE(10) : BANK.NAME

*                TEXT =  "KOKO":TOT.BNK.BR; CALL REM

                PRINT SPACE(5) :" ���  ": BRANCH.NAME :SPACE(EE) : TOT.BNK.BR : SPACE(RR) : COUNT.BR
                PRINT SPACE(10) : "������  " : BANK.NAME :SPACE(AA) : TOT.BNK : SPACE(TT) : COUNT.BNK
                TOOOT = TOOOT + TOT.BNK
                TOTOT = TOTOT + COUNT.BNK
                PRINT SPACE(1) :"-----------------------------------------------------------------------------------------------------------"
                BNK.F = BNK
                BNK.BR.F = BNK.BR
                TOT.BNK.BR = 0
                COUNT.BR = 1
                COUNT.BNK = 1
                TOT.BNK.BR = AMT

                TOT.BNK = 0
                TOT.BNK = AMT

            END ELSE
                IF BNK.BR.F NE BNK.BR THEN
*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BNK.BR.F,BRANCH.NAME)
F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
FN.F.ITSS.SCB.BANK.BRANCH = ''
CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BNK.BR.F,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
BRANCH.NAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>
*Line [ 161 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BNK.F,BANK.NAME)
F.ITSS.SCB.BANK = 'F.SCB.BANK'
FN.F.ITSS.SCB.BANK = ''
CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
CALL F.READ(F.ITSS.SCB.BANK,BNK.F,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
BANK.NAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
                    XX = LEN(BANK.NAME)
                    AA = 40 - XX

                    SS = LEN(BRANCH.NAME)
                    EE = 50 - SS
                    WW = LEN(TOT.BNK.BR)
                    RR = 20 - WW
                    QQ = LEN(TOT.BNK)
                    TT = 20 - QQ

                    PRINT SPACE(5) :" ���  ": BRANCH.NAME :SPACE(EE) : TOT.BNK.BR : SPACE(RR) :COUNT.BR
                    BNK.BR.F = BNK.BR
                    BNK.F = BNK
                    TOT.BNK.BR = 0
                    COUNT.BR= 1
                    TOT.BNK.BR = AMT
                END ELSE
                    TOT.BNK.BR = TOT.BNK.BR + AMT

                    COUNT.BR= COUNT.BR +1
                END
                TOT.BNK = TOT.BNK + AMT
                COUNT.BNK= COUNT.BNK +1
            END

        NEXT I

        IF I EQ SELECTED  THEN

*Line [ 197 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BNK.BR.F,BRANCH.NAME)
F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
FN.F.ITSS.SCB.BANK.BRANCH = ''
CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BNK.BR.F,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
BRANCH.NAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>
*Line [ 204 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BNK.F,BANK.NAME)
F.ITSS.SCB.BANK = 'F.SCB.BANK'
FN.F.ITSS.SCB.BANK = ''
CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
CALL F.READ(F.ITSS.SCB.BANK,BNK.F,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
BANK.NAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*            PRINT SPACE(10) : BANK.NAME
            XX = LEN(BANK.NAME)
            AA = 40 - XX

            SS = LEN(BRANCH.NAME)
            EE = 50 - SS
            WW = LEN(TOT.BNK.BR)
            RR = 20 - WW
            QQ = LEN(TOT.BNK)
            TT = 20 - QQ

*            TEXT =  "HERE":TOT.BNK.BR; CALL REM

            PRINT SPACE(5) :" ���  ": BRANCH.NAME :SPACE(EE) : TOT.BNK.BR : SPACE(RR) : COUNT.BR
            PRINT SPACE(10) : "������ ":BANK.NAME :SPACE(AA) : TOT.BNK : SPACE(TT) : COUNT.BNK
            TOOOT = TOOOT + TOT.BNK
            TOTOT = TOTOT + COUNT.BNK
            PRINT
            PRINT

            PRINT SPACE(25) : "��������" :"      ":TOOOT
            PRINT SPACE(25) : "��� " : "      ": TOTOT

*            TEXT =TOOOT; CALL REM

        END
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "REPORT CREATED " ; CALL REM
    END ELSE
        TEXT = "PLEASE CHECK DATE " ; CALL REM

    END
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 247 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(75):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(70):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50): "����� ����� ������"
    PR.HD :="'L'":SPACE(43):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

*    PR.HD :="'L'":SPACE(1):"��� ������ ������":SPACE(2):"��� ������":SPACE(11):"��� ������ ������":SPACE(09):"����-�����":SPACE(10):"����":SPACE(09):"��� �������":SPACE(06):"���� �������":SPACE(09):"����" :SPACE(10):"����� ���������"
*   PR.HD :="'L'":SPACE(1):STR('_',17):SPACE(2):STR('_',10):SPACE(11):STR('_',17):SPACE(09):STR('_',10):SPACE(10):STR('_',4):SPACE(09):STR('_',11):SPACE(06):STR('_',12):SPACE(09):STR('_',4):SPACE(10):STR('_',15)
    HEADING PR.HD
    RETURN
*==============================================================
END
