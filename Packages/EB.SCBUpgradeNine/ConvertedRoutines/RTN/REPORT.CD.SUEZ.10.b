* @ValidationCode : MjotMTEyNzQzMjE1MTpDcDEyNTI6MTY0NDkzNzA2Mzk0ODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:57:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>775</Rating>
*-----------------------------------------------------------------------------
*-------------------------------------------
* CREATE BY RIHAM YOUSSIF 21/08/2014
*-------------------------------------------
SUBROUTINE REPORT.CD.SUEZ.10

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*-------------------------------------------
    IF V$FUNCTION NE 'R' THEN
        GOSUB INITIATE
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        RETURN
    END
*----------------------------------------------
INITIATE:
*--------
    IF (OPERATOR EQ 'SCB.79061') THEN
        REPORT.ID='REPORT.CD.SUEZ.99'
    END ELSE
        IF (OPERATOR EQ 'SCB.60411') THEN
            REPORT.ID='REPORT.CD.SUEZ.990'
        END ELSE
            IF (OPERATOR EQ 'SCB.14621') THEN
                REPORT.ID='REPORT.CD.SUEZ.996'
            END ELSE
                IF (OPERATOR EQ 'SCB.11452') THEN
                    REPORT.ID='REPORT.CD.SUEZ.13'
                END
            END
        END
        CALL PRINTER.ON(REPORT.ID,'')
        RETURN
*========================================================================
PROCESS:
*-------
        FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS'    ; F.LD = ''      ; R.LD  = ''
        FN.CU     = 'FBNK.CUSTOMER'                 ; F.CU = ''      ; R.CU  = ''
        FN.COM    = 'F.COMPANY'                     ; F.COM= ''      ; R.COM = ''
        CALL OPF(FN.CU,F.CU)
        CALL OPF(FN.LD,F.LD)
        CALL OPF(FN.COM,F.COM)
*------------------------------------------------------------------------
        CD.TYPE   = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
        FINTYP    = FIELD(CD.TYPE,'-',2)

        FF        = R.NEW(LD.LOCAL.REF)<1,LDLR.ORDER>
        TT        = R.NEW(LD.LOCAL.REF)<1,LDLR.ARTICLE.NO>
        CUS.NO    = R.NEW(LD.CUSTOMER.ID)
        NAME11    = R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF,1>

        DATE1     = R.NEW(LD.VALUE.DATE)
        ISSU.DATE  = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]

        DATE2     = R.NEW(LD.FIN.MAT.DATE)
        MAT.DATE  = DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]

        CDNO      = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
        AMT       = R.NEW(LD.AMOUNT)
        TOT.AMT   = AMT
        TOT.AMT1  = FMT(AMT,"L0,")
        TOT.AMT1  = CHANGE(TOT.AMT1,',','.')

        CUR     = R.NEW(LD.CURRENCY)
*Line [ 108 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR,CUR22)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR22=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        NO.TOT.AMT = TOT.AMT1 : ' ' : CUR22
        IN.AMOUNT = TOT.AMT
        CALL WORDS.ARABIC.DEAL.CD(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT   = OUT.AMOUNT : ' ' : CUR22 : ' ' : '��� �����'

        BRN.CODE = R.NEW(LD.CO.CODE)
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.CODE,YYBRN)
        F.ITSS.COMPANY = 'F.COMPANY'
        FN.F.ITSS.COMPANY = ''
        CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
        CALL F.READ(F.ITSS.COMPANY,BRN.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
        YYBRN=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

        ACT.AMT = '180%'
        XX      = SPACE(132)
        XX1     = SPACE(132)  ; XX2   = SPACE(132) ; XX3     = SPACE(132)
        XX4     = SPACE(132)  ; XX5   = SPACE(132) ; XX6     = SPACE(132)
        XX7     = SPACE(132)  ; XX8   = SPACE(132) ; XX9     = SPACE(132)
        XX10    = SPACE(132)  ; XX11  = SPACE(132) ; XX12    = SPACE(132)
        XX13    = SPACE(132)  ; XX14  = SPACE(132) ; XX15    = SPACE(132)
        XX16    = SPACE(132)  ; XX17  = SPACE(132) ; XX18    = SPACE(132)
        XX19    = SPACE(132)  ; XX20  = SPACE(132) ; XX21    = SPACE(132)
        XX22    = SPACE(132)  ; XX23  = SPACE(132) ; XX24    = SPACE(132)
        XX25    = SPACE(132)  ; XX26  = SPACE(132) ; XX27    = SPACE(132)

        TYP      = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
        CDTYP    = FIELD(TYP,'-',2)

        IF CDTYP EQ '10' THEN

            XX<1,1>[3,35]      = 'REPORT.CD.SUEZ.10'
        END ELSE
            XX<1,1>[3,35]      = 'REPORT.CD.SUEZ.100'

        END
        XX1<1,1>[3,35]     =  '��� �������'
        XX2<1,1>[3,35]     =  FINTYP


        XX3<1,1>[3,35]     = '����� ������� ��: '
        XX4<1,1>[3,35]     = FF

        XX5<1,1>[3,35]     = '����� ������� ���:'
        XX6<1,1>[3,35]     = TT



        XX7<1,1>[3,35]     = '��� ������:'
        XX8<1,1>[3,100]     = NAME11

        XX9<1,1>[3,35]     = '����� �������:'
        XX10<1,1>[3,35]    = ISSU.DATE

        XX11<1,1>[3,35]    = '����� ���������:'
        XX12<1,1>[3,35]    = MAT.DATE

        XX13<1,1>[3,35]    = '������ ���� �������:'
        XX14<1,1>[3,35]    =  NO.TOT.AMT
        XX27<1,1>[3,35]   =  OUT.AMT

        XX15<1,1>[3,35]    = '����� ������'
        XX16<1,1>[3,60]    =  YYBRN

        XX17<1,1>[3,35]    = '������ ����������� '
        XX18<1,1>[3,35]    =  ACT.AMT

        XX19<1,1>[3,35]    = '��� ������� ������'
        XX20<1,1>[3,35]    =  ID.NEW

        XX21<1,1>[3,35]      = '��� ������ ��������'
        XX22<1,1>[3,35]      = '����� ������'
        XX23<1,1>[3,35]      = '�� ��� ���� ������'
        XX24<1,1>[3,35]      = '��� ������ ������ '
        XX25<1,1>[3,35]      = '12% '
        XX26<1,1>[3,35]      = '���� ����  '

        PRINT XX<1,1>
        PRINT STR(' ',82)
        PRINT XX1<1,1>
        PRINT XX2<1,1>
        PRINT STR(' ',82)
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT STR(' ',82)
        PRINT XX5<1,1>
        PRINT XX6<1,1>
        PRINT STR(' ',82)
        PRINT XX7<1,1>
        PRINT XX8<1,1>
        PRINT STR(' ',82)
        PRINT XX9<1,1>
        PRINT XX10<1,1>
        PRINT STR(' ',82)
        PRINT XX11<1,1>
        PRINT XX12<1,1>
        PRINT STR(' ',82)
        PRINT XX13<1,1>
        PRINT XX14<1,1>
        PRINT XX27<1,1>
        PRINT STR(' ',82)
        PRINT XX15<1,1>
        PRINT XX16<1,1>
        PRINT STR(' ',82)
        PRINT XX17<1,1>
        PRINT XX18<1,1>
        PRINT STR(' ',82)
        PRINT XX19<1,1>
        PRINT XX20<1,1>
        PRINT STR(' ',82)
        PRINT XX21<1,1>
        PRINT XX22<1,1>
        PRINT XX23<1,1>
        PRINT XX24<1,1>
        PRINT XX25<1,1>
        PRINT XX26<1,1>
        RETURN

    END
