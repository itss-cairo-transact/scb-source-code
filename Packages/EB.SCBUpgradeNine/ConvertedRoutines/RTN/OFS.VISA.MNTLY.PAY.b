* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*****NESSREEN AHMED 19/12/2011******************
    SUBROUTINE OFS.VISA.MNTLY.PAY

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.MNTLY.PAY.AMT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.DATE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.PAY.FLAG
*---------------------------------------
************ OPEN FILES ****************
    E = ''

    FN.ACCT = 'F.ACCOUNT' ; F.ACCT = '' ; R.ACCT = '' ; RETRY = '' ; E1 = ''
    CALL OPF (FN.ACCT,F.ACCT)

    FN.MNT.PAY = 'F.SCB.VISA.MNTLY.PAY.AMT' ; F.MNT.PAY = '' ; R.MNT.PAY = '' ; RETRY1 = '' ; E1 = ''
    CALL OPF(FN.MNT.PAY,F.MNT.PAY)

    FN.PDT = 'F.SCB.VISA.PAY.DATE' ; F.PDT = '' ; R.PDT = '' ; RETRY.PDT = '' ; E.PDT = ''
    CALL OPF(FN.PDT,F.PDT)

    F.VISA.PAY.FL = '' ; FN.VISA.PAY.FL = 'F.SCB.VISA.PAY.FLAG' ; R.VISA.PAY.FL = '' ; E1 = ''
    CALL OPF(FN.VISA.PAY.FL,F.VISA.PAY.FL)

    T.DAT = TODAY
    DD.DAT = T.DAT[1,6]
    KEY.ID = DD.DAT
    CALL F.READ(FN.PDT, KEY.ID, R.PDT, F.PDT, E.PDT)
    DAT.FR  = R.PDT<VPD.DATE.FROM>
    DAT.TO  = R.PDT<VPD.DATE.TO>
    SAL.DAT = R.PDT<VPD.SAL.DATE>
    DB.DAT.FR  = DAT.FR[7,2]
    DB.DAT.TO = DAT.TO[7,2]
    DB.DAT.N = TODAY[7,2]

    CHK.SEL = "SELECT F.SCB.VISA.PAY.FLAG WITH COMPANY.CO EQ 'EG0010099' AND PAYMENT.FLAG EQ 'NO' AND YEAR.MM EQ ":DD.DAT
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
*  E = ''
    END ELSE
****Updated by Nessreen Ahmed 11/4/2012**********************************************
***CHK.SEL.2 = "SELECT F.SCB.VISA.PAY.FLAG WITH (COMPANY.CO EQ 'EG0010001' OR COMPANY.CO EQ 'EG0010013') AND PAYMENT.FLAG EQ 'YES' AND YEAR.MM EQ ":DD.DAT
        CHK.SEL.2 = "SELECT F.SCB.VISA.PAY.FLAG WITH (COMPANY.CO NE 'EG0010088' AND COMPANY.CO NE 'EG0010099') AND PAYMENT.FLAG EQ 'YES' AND YEAR.MM EQ ":DD.DAT : " BY COMPANY.CO "
        KEY.LIST.CHK.2=""
        SELECTED.CHK.2=""
        ER.MSG.CHK.2=""

        E = ''
        CALL EB.READLIST(CHK.SEL.2,KEY.LIST.CHK.2,"",SELECTED.CHK.2,ER.MSG.CHK.2)
        TEXT = '��� ������=':SELECTED.CHK.2 ; CALL REM
        IF NOT(SELECTED.CHK.2) THEN
            E = ''
            E = '��� ����� ������ ����' ; CALL ERR ; MESSAGE = 'REPEAT'
        END ELSE
            SCB.OFS.SOURCE = "TESTOFS"
            SCB.APPL = "FUNDS.TRANSFER"
            SCB.VERSION  = "SCB.VISA"
            SCB.OFS.HEADER  = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,"
            OPENSEQ "OFS.MNGR.IN" , "VISA_MNT_PAY" TO BB THEN
                CLOSESEQ BB
                HUSH ON
                EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"VISA_MNT_PAY"
                HUSH OFF
            END
            OPENSEQ "OFS.MNGR.IN" , "VISA_MNT_PAY" TO BB ELSE
                CREATE BB THEN
                END ELSE
                    STOP 'Cannot create VISA_MNT_PAY File IN OFS.MNGR.IN'
                END
            END
*--------------------------------------------------------------------
****Updated by Nessreen Ahmed 11/4/2012**************************************
            FOR SS = 1 TO SELECTED.CHK.2
                CALL F.READ(FN.VISA.PAY.FL, KEY.LIST.CHK.2<SS> , R.VISA.PAY.FL, F.VISA.PAY.FL, E.PAY.FLAG)
                FIN.COMP = R.VISA.PAY.FL<PAFL.COMPANY.CO>
*TEXT = 'FIN.COMP=':FIN.COMP ; CALL REM
****T.SEL = "SELECT F.SCB.VISA.MNTLY.PAY.AMT WITH (COMPANY.CO EQ EG0010001 OR COMPANY.CO EQ EG0010013) AND PAYMENT.STATE EQ '' AND YEAR.MONTH EQ " : DD.DAT
                T.SEL = "SELECT F.SCB.VISA.MNTLY.PAY.AMT WITH COMPANY.CO EQ " : FIN.COMP : " AND PAYMENT.STATE EQ '' AND YEAR.MONTH EQ " : DD.DAT
                CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*TEXT = 'SEL.BR = ':SELECTED ; CALL REM
                IF SELECTED THEN
                    FOR I = 1 TO SELECTED
                        CR.ACCT = '' ; DB.ACCT = '' ; DB.AMT = '' ; W.B.D = '' ; CHK.BAL = '' ; DB.VAL.DATE = ''
                        WS.ID = KEY.LIST<I>
                        CALL F.READ(FN.MNT.PAY, KEY.LIST<I> , R.MNT.PAY, F.MNT.PAY, E1)
                        DB.ACCT  = R.MNT.PAY<MPA.DB.ACCT.NO>
                        CR.ACCT  = R.MNT.PAY<MPA.CR.ACCT.NO>
                        DB.AMT   = R.MNT.PAY<MPA.DEBIT.AMT>
                        VISA.NO  = R.MNT.PAY<MPA.VISA.NO>
                        CALL F.READ(FN.ACCT, DB.ACCT, R.ACCT, F.ACCT, E3)
                        CUST  = R.ACCT<AC.CUSTOMER>
                        CATEG = R.ACCT<AC.CATEGORY>
                        IF CATEG EQ '1002' THEN
                            DB.VAL.DATE = SAL.DAT
                        END ELSE
                            DB.VAL.DATE = TODAY
                        END
*****UPDATED BY NESSREEN AHMED 26/1/2012************************************************
** COMP  = R.ACCT<AC.CO.CODE>
                        CALL F.READ(FN.ACCT, CR.ACCT, R.ACCT, F.ACCT, E4)
                        COMP  = R.ACCT<AC.CO.CODE>
*****END OF UPDATE 26/1/2012*************************************************************
*------------------------------------------------------------------
                        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"ACVC"
                        OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":"EGP"
                        OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":"EGP"
                        OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
                        OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
                        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":DB.AMT
                        OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":DB.VAL.DATE
                        OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
                        OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
                        OFS.MESSAGE.DATA :=  ",DEBIT.THEIR.REF::=":"VISA.AUTO.PAY"
                        OFS.MESSAGE.DATA :=  ",COMMISSION.CODE=::=":"WAIVE"
                        OFS.MESSAGE.DATA :=  ",CHARGE.CODE::=":"WAIVE"
                        OFS.MESSAGE.DATA :=  ",VISA.NO::=":VISA.NO

                        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "VISAMAST//": COMP :",,": OFS.MESSAGE.DATA
* SCB R15 UPG 20160717 - S
*                        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E
                        BB.DATA  = SCB.OFS.MESSAGE
                        WRITESEQ BB.DATA TO BB ELSE
                        END
                    NEXT I
                END ;* END OF IF SELECTED
            NEXT SS
*-----------WRITE THE FLAG AFTER OFS----------------------------*
            KEY.TO.USE = "EG0010099.":DD.DAT
            CALL F.READ(FN.VISA.PAY.FL,  KEY.TO.USE, R.VISA.PAY.FL, F.VISA.PAY.FL, E1)
            R.VISA.PAY.FL<PAFL.COMPANY.CO> = 'EG0010099'
            R.VISA.PAY.FL<PAFL.TRANS.DATE> = TODAY
            R.VISA.PAY.FL<PAFL.YEAR.MM> = DD.DAT
            R.VISA.PAY.FL<PAFL.PAYMENT.FLAG> = 'NO'
            CALL F.WRITE(FN.VISA.PAY.FL, KEY.TO.USE, R.VISA.PAY.FL)
            CALL JOURNAL.UPDATE(KEY.TO.USE)
*----------------------------------
            TEXT = '�� ����� �����' ; CALL REM
***************
        END         ;**OF SELECTED.CHK
        TEXT = 'End of program' ; CALL REM
        RETURN
************************************************************
    END
