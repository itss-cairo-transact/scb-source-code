* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>25</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPRINT.CHQ.RET

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW
*=============================================================================*
*Line [ 38 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        CALL TXTINP('Enter Transaction Reference', 8, 23, '17', 'ANY')
        IF COMI THEN
            TXN.ID = '' ; TXN.ID = COMI ; ER.MSG = '' ; ETEXT = ''
**            BEGIN CASE
**  CASE TXN.ID[1,2] = 'TT'
            GOSUB SAVE.PARAMETERS
            GOSUB PROCESS.CHQ.RET.RECORD
            IF NOT(ER.MSG) THEN
                GOSUB REPRINT.SLIP
            END
            GOSUB RESTORE.PARAMETERS
*                  CALL JOURNAL.UPDATE(TXN.ID)
**  CASE OTHERWISE
**       ER.MSG = 'Invalid Transaction Reference'
**   END CASE

**   IF ER.MSG THEN
**      TEXT = ER.MSG
**      CALL REM
*CALL TXTINP(ER.MSG, 8, 23, '1.1', '')
**   END
        END
    END

    GOTO PROGRAM.END

*=============================================================================*
SAVE.PARAMETERS:
    SAVE.FUNCTION = V$FUNCTION
    SAVE.APPLICATION = APPLICATION
    SAVE.PGM.VERSION = PGM.VERSION
    SAVE.ID.NEW = ID.NEW

    UPDATE.REC = '' ; GR.YEAR = ''
    UPDATE.TS = '' ; SIZE = ''
    TXN.NO = '' ; PROCESS = ''
    NBG.CODE = '' ; RECEIPT = ''
    RETURN
*=============================================================================*
RESTORE.PARAMETERS:
    MAT R.NEW = MAT R.SAVE
    V$FUNCTION = SAVE.FUNCTION
    APPLICATION = SAVE.APPLICATION
    PGM.VERSION = SAVE.PGM.VERSION
    ID.NEW = SAVE.ID.NEW
    RETURN
*===================== PROCESS SCB.CHQ.RETURN.NEW.RECORD =================================*
PROCESS.CHQ.RET.RECORD:

    VF.SCB.CHQ.RETURN.NEW = '' ; CALL OPF('F.SCB.CHQ.RETURN.NEW', VF.SCB.CHQ.RETURN.NEW)
    ETEXT = ''
    DIM APP.REC(CHQ.RET.AUDIT.DATE.TIME) ; MAT APP.REC = ''
    DIM R.SAVE(CHQ.RET.AUDIT.DATE.TIME)  ; MAT R.SAVE = ''
*         MAT R.NEW = MAT R.SAVE
    MAT R.SAVE = MAT R.NEW
    SIZE = CHQ.RET.AUDIT.DATE.TIME

    CALL F.MATREAD('F.SCB.CHQ.RETURN.NEW',TXN.ID,MAT APP.REC,SIZE,VF.SCB.CHQ.RETURN.NEW,ETEXT)
    MAT R.NEW = '' ; MAT R.NEW = MAT APP.REC
    IF ETEXT THEN
        ER.MSG = ETEXT ; ETEXT = ''
    END
    ELSE
        APPLICATION = 'SCB.CHQ.RETURN.NEW' ; ID.NEW = COMI ; V$FUNCTION = 'I'
        VER.NAME = "SCB.CHQ.RETURN.NEW,SCB.INPUT"
        GOSUB GET.DSF.NAMES.ARRAY
    END
    RETURN
*=============================================================================*
REPRINT.SLIP:
    IF DSF.CNT >= 1 THEN
        FOR DSF.LP = 1 TO DSF.CNT
            CALL PRODUCE.DEAL.SLIP(TRIM(R.DSF<DSF.LP>))
        NEXT DSF.LP
    END
    ELSE
        ER.MSG = 'MISSING SLIP(S) IN VESION RECORD'
    END
    RETURN
*=============================================================================*
GET.DSF.NAMES.ARRAY:
    IF VER.NAME THEN
        R.DSF = '' ; DSF.CNT = ''   ; ETEXT = ''
*Line [ 124 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 125 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR("VERSION":@FM:EB.VER.D.SLIP.FORMAT , VER.NAME , R.DSF)
F.ITSS.VERSION = 'F.VERSION'
FN.F.ITSS.VERSION = ''
CALL OPF(F.ITSS.VERSION,FN.F.ITSS.VERSION)
CALL F.READ(F.ITSS.VERSION,VER.NAME,R.ITSS.VERSION,FN.F.ITSS.VERSION,ERROR.VERSION)
R.DSF=R.ITSS.VERSION<EB.VER.D.SLIP.FORMAT>
        IF NOT(ETEXT) THEN
*         TEXT = "ARRAY = ":R.DSF ; CALL REM
            DSF.CNT = DCOUNT(R.DSF,@VM)
*         TEXT = "DSF COUNT = ":DSF.CNT  ; CALL REM
        END
        ELSE
            ER.MSG = "MISING DEAL SLIP FOR THIS RECORD-VERSION"
        END
    END

*=================== SUBROUTINE END ... YES !!! ==============================*
PROGRAM.END:
    RETURN
END
