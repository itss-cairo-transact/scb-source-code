* @ValidationCode : Mjo4ODkxMTc3MTpDcDEyNTI6MTY0MTc1MDMxMzg2OTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 19:45:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>676</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.ATM.DB.CR.CUST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.ROUTINE.CHK
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.DPST.AC.DRCR
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

* WRITTEN BY NESSREEN AHMED-SCB**8/5/2016****************
* to Debit or Credit the amount of Visa Payment from Customer's Account

    GOSUB INITIALISE
*Line [ 56 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 57 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
    RETURN
*==============================================================
INITIALISE:
    *Line [ 62 ] variable 'DB.ACCT' is  initialised.
    DB.ACCT = ''

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')

    FN.ACC= 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)


    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.BK = ""

    F.OFS.IN = 0
    OFS.REC = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS = "SCB.VISA"
    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    RETURN
*==============================================================
CALLDB:
    F.ATM.CSH.PAY = '' ; FN.ATM.CSH.PAY = 'F.SCB.ATM.DPST.AC.DRCR' ; R.ATM.CSH.PAY = '' ; E1 = ''

    CALL OPF(FN.ATM.CSH.PAY,F.ATM.CSH.PAY)
    F.RT.CHK = '' ; FN.RT.CHK = 'F.SCB.ATM.ROUTINE.CHK' ; R.RT.CHK = '' ; E.CHK = '' ; RETRY.CHK = ''
    CALL OPF(FN.RT.CHK,F.RT.CHK)
******************************************
    YTEXT = "Enter the Required Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    DATEE = COMI
*  BR = R.USER<EB.USE.DEPARTMENT.CODE>
    YYYY = DATEE[1,4]
    MM = DATEE[5,2]
    DD = DATEE[7,2]
********************************************
    CHK.SEL = "SELECT F.SCB.ATM.ROUTINE.CHK WITH YEAR EQ ":YYYY:" AND MONTH EQ ":MM :" AND DAY EQ ":DD :" AND OFS.CR.CUST.AC EQ 'YES' "
    KEY.LIST.CHK=""
    SELECTED.CHK=""
    ER.MSG.CHK=""

    CALL EB.READLIST(CHK.SEL,KEY.LIST.CHK,"",SELECTED.CHK,ER.MSG.CHK)
    IF SELECTED.CHK THEN
        E = '��� �� ����� ��� �������� �� ���' ; CALL ERR ; MESSAGE = 'REPEAT'
    END ELSE
        KEYID = YYYY:MM:DD
        T.SEL = "SELECT F.SCB.ATM.DPST.AC.DRCR WITH POS.DATE EQ ":KEYID:" BY @ID "
        KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
        IF SELECTED THEN
            TEXT = '��� ������=':SELECTED ; CALL REM
            FOR I = 1 TO SELECTED
                VISA.NO = '' ; ACCT.NO = '' ; CUST.NO = '' ; CUST.NAME = '' ; DEP.CODE = '' ; TOT.ALL = ''
                CALL F.READ(FN.ATM.CSH.PAY,KEY.LIST<I>,R.ATM.CSH.PAY,F.ATM.CSH.PA,E2)

                VISA.NO<I> = R.ATM.CSH.PAY<ATDC.VISA.NO>
                DB.ACC<I> =  R.ATM.CSH.PAY<ATDC.DB.ACCT.NO>
*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,DB.ACCT<I>,CUST.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DB.ACCT<I>,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
                IF  CUST.NO = '' THEN
                    DB.CUST = '99499900'
                END ELSE
                    DB.CUST = CUST.NO
                END
                CR.ACCT<I> = R.ATM.CSH.PAY<ATDC.CR.ACCT.NO>
                DB.AMT<I>  = R.ATM.CSH.PAY<ATDC.TOT.AMT.DB>
                CR.AMT<I>  = R.ATM.CSH.PAY<ATDC.TOT.AMT.CR>
                IF DB.AMT<I> = '' THEN
                    AMT = CR.AMT<I>
                END ELSE
                    AMT = DB.AMT<I>
                END
                TERM.ID<I> = R.ATM.CSH.PAY<ATDC.TERMINAL.ID>
                COMMA = ","
                OFS.MESSAGE.DATA =  "TRANSACTION.TYPE=":'ACVA':COMMA
                OFS.MESSAGE.DATA := "DEBIT.CUSTOMER=": DB.CUST:COMMA
                OFS.MESSAGE.DATA := "DEBIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": DB.ACC<I>:COMMA
                OFS.MESSAGE.DATA := "DEBIT.AMOUNT=": AMT:COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "CREDIT.CURRENCY=": 'EGP':COMMA
                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CR.ACCT<I>:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
                OFS.MESSAGE.DATA := "COMMISSION.CODE=":'WAIVE':COMMA
                OFS.MESSAGE.DATA := "CHARGE.CODE=":'WAIVE':COMMA
                OFS.MESSAGE.DATA := "ORDERING.BANK=":'SCB':COMMA
                OFS.MESSAGE.DATA := "LOCAL.REF:32=":TERM.ID<I>
**             CALL F.READ(FN.ACC,ACCT.NO<I>,R.ACC,F.ACC,E11)
**             COMP = R.ACC<AC.CO.CODE>
                COMP = R.ATM.CSH.PAY<ATDC.BRANCH.NUMBER>
                COM.CODE = COMP[8,2]
                OFS.USER.INFO = "VISAMAST//":COMP

**********************************************************************
****************
                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                OFS.ID = "ATMCSH":RND(10000):".":I:".":CR.ACCT<I>:".":TODAY:"-":COMP
                WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

**************************************************************************
            NEXT I
        END
        TEXT = 'KEYID=':KEYID ; CALL REM
        CALL F.READ(FN.RT.CHK, KEYID, R.RT.CHK, F.RT.CHK ,E.CHK)
        R.RT.CHK<ATCK.OFS.CR.CUST.AC> = 'YES'
        CALL F.WRITE(FN.RT.CHK,KEYID, R.RT.CHK)
        CALL JOURNAL.UPDATE(KEYID)
*********************************************************************
        TEXT = '�� ��� ���� ���� ����� '; CALL REM
    END
    RETURN
END
