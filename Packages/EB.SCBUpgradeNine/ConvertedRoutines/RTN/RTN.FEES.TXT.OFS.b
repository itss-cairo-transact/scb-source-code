* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE RTN.FEES.TXT.OFS
**    PROGRAM RTN.FEES.TXT.OFS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RTN.FEES.SMS.OFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.CARD.FEES
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.FEE.EXCP
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CARD.FEES.ALL
*===================================================*
    GOSUB INITIALISE
    GOSUB CHECK.RECORD
    GOSUB END.PROG
    RETURN
*---------------------------------------------------*
END.PROG:
*-- write in temp
    ID.TEMP = FILE.NAME
    CALL F.READ(FN.TEMP,ID.TEMP,R.TEMP,F.TEMP,READ.ER)
    R.TEMP<FEES.SMS.RUN.DATE> = TODAY

    CALL F.WRITE(FN.TEMP,ID.TEMP,R.TEMP)
    CALL JOURNAL.UPDATE(ID.TEMP)

*-- delete file
    DIR.NAME = 'CARD-CENTER'

    HUSH ON
**    EXECUTE 'DELETE ':DIR.NAME:' ':FILE.NAME
    EXECUTE 'rm ':DIR.NAME:'/':FILE.NAME
    HUSH OFF

    RETURN
*----------------------------------------------------------------------*
CHECK.RECORD:
*------------
    NN.SEL = "SELECT CARD-CENTER WITH @ID LIKE Annual..."
    CALL EB.READLIST(NN.SEL,N.LIST,"",SELECTED,ER.MSG.N)

    IF SELECTED GT 1 THEN
        TEXT = "ERROR ** MORE THAN ONE FILE" ; CALL REM
    END

    IF SELECTED EQ 1 THEN
        FILE.NAME = N.LIST<1>

        CALL F.READ(FN.TEMP,FILE.NAME,R.TEMP,F.TEMP,READ.ER)
        IF READ.ER THEN
            Path = "CARD-CENTER/":FILE.NAME
            GOSUB BUILD.RECORD
        END ELSE
            TEXT = "ERROR ** Already.Exist" ; CALL REM
        END
    END
    RETURN
*----------------------------------------------------------------------*
INITIALISE:
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "CARD.FEES.N"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.TEMP = 'F.SCB.RTN.FEES.SMS.OFS'   ; F.TEMP = ''
    CALL OPF(FN.TEMP , F.TEMP)

    FN.CI = "FBNK.CARD.ISSUE" ; F.CI = ""
    CALL OPF(FN.CI, F.CI)

    FN.FEES = "F.SCB.ATM.CARD.FEES"   ; F.FEES = ""
    CALL OPF(FN.FEES, F.TMP)

    FN.EX = "F.SCB.CUS.FEE.EXCP" ; F.EX = ""
    CALL OPF(FN.EX, F.EX)

    FN.TMP = "F.SCB.ATM.APP"   ; F.TMP = ""
    CALL OPF(FN.TMP , F.TMP)
    RETURN
*---------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = ","
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I   = 1

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN

            CARD.NO      = FIELD(Line,",",1)
            XX           = FIELD(Line,",",3)
            YY           = FIELD(Line,",",5)

            IF ( YY EQ 'Open' OR YY EQ 'Restricted' ) THEN
                DAT      = TODAY
                TXN.TYPE = "AC52"
                DB.AMT   = ""
                CURR     = 'EGP'
                PL.ACC   = "PL57040"
                CRD      = "ATMC.":CARD.NO
                R.CI     = ""
                CALL F.READ(FN.CI,CRD,R.CI,F.CI,ERR.CI)
                ACCOUNT  = R.CI<CARD.IS.ACCOUNT>
                CRD.COD  = R.CI<CARD.IS.LOCAL.REF><1,LRCI.CARD.CODE>
                ACC      = ACCOUNT<1,1>
*Line [ 164 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT': @FM:AC.DEPT.CODE,ACC,D.CODE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
D.CODE=R.ITSS.ACCOUNT<AC.DEPT.CODE>

                BRN           = CARD.NO[7,2]
                COMPO         = "EG00100":BRN
                COMP          = COMPO
                COM.CODE      = COMP[8,2]
                OFS.USER.INFO = "AUTO.CHRGE//":COMP

*Line [ 178 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CU)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CU=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC,CU.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CU.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 192 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                CRD.NO  = CARD.NO[1,6] ; CRD.TYPE = ""

                IF CRD.NO EQ '511864' THEN CRD.TYPE = "CLASSIC"
                IF CRD.NO EQ '537077' THEN CRD.TYPE = "TITANIUM"
                IF CRD.NO EQ '534453' THEN CRD.TYPE = "PLATINUM"

                FEES.ID = CRD.TYPE : "." : CRD.COD
                R.FEES  = ""
                CALL F.READ(FN.FEES,FEES.ID,R.FEES,F.FEES,FEES.ER)
                DB.AMT  = R.FEES<CARD.FEES.ANNUAL.FEES>
                IF CRD.COD EQ 901 THEN
                    NN.SEL2  = "SELECT F.SCB.ATM.APP WITH CUSTOMER EQ ":CU.NO
                    NN.SEL2 := " BY DATE.TIME"
                    CALL EB.READLIST(NN.SEL2,NN.LIST,"",SELECTED.2,ER.MSG.2)
                    APP.ID = NN.LIST<SELECTED.2>
                    CALL F.READ(FN.TMP,APP.ID,R.TMP,F.TMP,E.TMP)

                    EX.ID = R.TMP<SCB.VISA.PAYROLL.CUST>
                    CALL F.READ(FN.EX,EX.ID,R.EX,F.EX,EX.ER)
                    IF NOT(EX.ER) THEN
                        DB.AMT = R.EX<FEE.DR.RENEWAL.FEE>
                    END
                END
                CALL VAR.ANNUAL.CARD(ACC,DB.AMT,CARD.NO)
            END

        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath

    RETURN
***------------------------------------------------------------------***
END
