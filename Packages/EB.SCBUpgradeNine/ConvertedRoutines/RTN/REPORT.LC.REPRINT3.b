* @ValidationCode : MjotOTMyNTYxNTMzOkNwMTI1MjoxNjQ0OTM5NTgzMDQwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:39:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**************************NI7OOOOOOOOOOOOOO***********************
SUBROUTINE REPORT.LC.REPRINT3

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.LC.REPRINT3'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.DR  = 'FBNK.LETTER.OF.CREDIT' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '�����  �������':' - ':'ISSUE.LC.NEW '
*------------------------------------------------------------------------

    ID = COMI
    FN.DR = 'FBNK.LETTER.OF.CREDIT' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH @ID EQ ": COMI
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)
********************** MARGIN.ACCOUNT=R.DR<TF.LC.PROVIS.ACC>
        MARGIN.ACCOUNT=R.DR<TF.LC.CREDIT.PROVIS.ACC>
*** TEXT = "AC = " : MARGIN.ACCOUNT    ; CALL REM
        IF MARGIN.ACCOUNT NE '' THEN
            ACC.NO = MARGIN.ACCOUNT
        END
        IF MARGIN.ACCOUNT EQ '' THEN
            MARGIN.ACCOUNT = R.DR<TF.LC.APPLICANT.ACC>
            ACC.NO = R.DR<TF.LC.APPLICANT.ACC>
        END
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.IDD)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUR.IDD=R.ITSS.ACCOUNT<AC.CURRENCY>
        CUR = CUR.IDD
*** TEXT = "CUR = " : CUR    ; CALL REM
        REFERENCE = R.DR<TF.LC.OLD.LC.NUMBER>
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
****        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*Line [ 135 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        ACC.BR  = AC.COMP[8,2]

        CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
*Line [ 110 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CAT.ID,CATEG)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*    CHARGE.CODE= R.NEW(TF.LC.CHARGE.CODE)
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CHARGE.CODE,COMM.AC.NO)
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION<1,2>,CHARGE.CODE,COMM.AC.NO)
* DR.AMOUNT    = R.NEW(TF.LC.CHARGE.AMOUNT)
**************    MARGIN.AMOUNT    = R.DR<TF.LC.DB.PROV.AMOUNT>
        MARGIN.AMOUNT    =  R.DR<TF.LC.PROVIS.AMOUNT>
        ISS.DATE = R.DR<TF.LC.ISSUE.DATE>
        IF MARGIN.AMOUNT EQ '' THEN
            MARGIN.AMOUNT = 0
        END
* AMOUNT    = DR.AMOUNT+CHARGE.AMOUNT
*    CUR.ID2=R.NEW(TF.LC.CHARGE.CURRENCY)
***     TEXT = "CUR = " : CUR    ; CALL REM
*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.ID,CUR1)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR1=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        TEXT = "CUR1 = " : CUR1    ; CALL REM
        IF MARGIN.AMOUNT EQ '' THEN
            MARGIN.AMOUNT = 0
        END
        IN.AMOUNT = MARGIN.AMOUNT
        IF IN.AMOUNT = '' THEN
            IN.AMOUNT = 0
        END
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR1 : ' ' : '�����'

*  DAT  = R.NEW(TF.LC.CHARGE.DATE)
* MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
* CHARGE.CODE= R.NEW(TF.LC.CHARGE.CODE)
        MARGIN.PERC = R.DR<TF.LC.PROVIS.PERCENT>
        INPUTT      = R.DR<TF.DR.INPUTTER>
        AUTH.SON    = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 191 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH.SON,AUTH)
        F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
        FN.F.ITSS.USER.SIGN.ON.NAME = ''
        CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
        CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH.SON,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
        AUTH=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
        INP   = FIELD(INPUTT,'_',2)
        AUTHI =  AUTH
*------------------------------------------------------------------------

        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
        XX8  = SPACE(132)  ; XX9  = SPACE(132)

* XX1<1,1>[3,35]   = COMM.AC.NO
        XX1<1,1>[3,35]   = CUST.NAME
        XX2<1,1>[3,35]   = CUST.ADDRESS

        XX<1,1>[45,15]  = '������     : '
        XX<1,1>[59,15]  = MARGIN.AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = ACC.NO

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR1

* XX4<1,1>[45,15] = '���� ������� : '
* XX4<1,1>[59,15] = MARGIN.PERC
        XX4<1,1>[45,15] = '������ :'
        XX4<1,1>[59,15] = REFERENCE

        XX5<1,1>[45,15] = '����� ���� :'
        XX5<1,1>[59,15] = ISS.DATE

        TEXT = "ISS.DATE : " : ISS.DATE ; CALL REM
        XX6<1,1>[1,15]  = '������'
        XX6<1,1>[30,15] = '��� �������'
        XX6<1,1>[60,15] = '������'

        XX7<1,1>[1,15]  = INP:' �'
        XX7<1,1>[30,15] = COMI:' �'
        XX7<1,1>[60,15] = AUTHI:' �'

        XX8<1,1>[3,35]  = '������ ������� : '
        XX8<1,1>[20,15] = OUT.AMT

        XX9<1,1>[3,15]  = '������         : '
*            XX9<1,1>[20,15] = BR.DATA
    END
*-------------------------------------------------------------------
*Line [ 248 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,ACC.BR,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(45) : "����� �������� "
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX<1,1>
    PRINT XX5<1,1>
* PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
