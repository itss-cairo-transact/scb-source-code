* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**************************NI7OOOOOOOOOOOOOOOOOOOOO********************************************
    SUBROUTINE REPORT.FT.IN.OLD

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LC.ACCOUNT.BALANCES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

**------------------------------------
    COMP      = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM

*-------------------------------------------------------------------------
    RETURN
*==============================================================
INITIATE:
    CUST.NAME        = ''
    CUST.NAME1       = ''
    LC.NU            = ''
    OLD.LC.NU        = ''
    ISSU.BANK.NU     = ''
    ISSU.BANK.NAME   = ''
    BEN.CUS.NU       = ''
    CONF.INST        = ''
    LC.CUR           = ''
    LC.AMT           = ''
    LIAB.AMT         = ''
    DEV.EXP.DAT      = ''
    TD1 = TODAY
***** REPORT.ID='REPORT.FT.IN'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*===============================================================
PROCESS:
*---------------------

    FN.LC  = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    FN.DR  = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
    FN.CU  = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
* FN.FT  = 'FBNK.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = ''
    FN.IN  = 'F.INF.MULTI.TXN' ; F.IN = '' ; R.IN = ''
    FN.BAL = 'FBNK.LC.ACCOUNT.BALANCES' ; F.BAL = '' ;R.BAL = ''
    FN.FT  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT = '' ; R.FT= ''
    FN.CUR = 'FBNK.RE.BASE.CCY.PARAM' ; F.CUR = '' ; R.CUR= ''

    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DR,F.DR)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CUR,F.CUR)

    XXX2  = SPACE(132)  ; XXX5   = SPACE(132) ; XXX10 = SPACE(132)
    XXX4  = SPACE(132)  ; XXX1   = SPACE(132)
    XXX3  = SPACE(132)
    XXX15 = SPACE(132)  ; XXX16  = SPACE(132) ; XXX17 = SPACE(132)
    XXX10 = SPACE(132)  ; XXX11  = SPACE(132) ; XXX12 = SPACE(132)
    XXX21 = SPACE(132)  ; XXX19  = SPACE(132) ; XXX18 = SPACE(132)
    XXX6  = SPACE(132)  ; XXX7   = SPACE(132) ; XXX8  = SPACE(132)


    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*========================================================================

    YTEXT = "Enter the REF. No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    WW = COMI

    YTEXT = "Enter the FROM.DATE : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    QQ = COMI

    YTEXT = "Enter the TO.DATE : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ZZ = COMI

** APP.CUSTNO =   WW
    APP.CUSTNO = '...SCE':WW:'...'
    ISS1       =   QQ
    ISS2       =   ZZ

    CLR.AMT = 0
**T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND ISSUING.BANK.NO EQ ":APP.CUSTNO:" AND (ISSUE.DATE GT ":ISS1:" AND ISSUE.DATE LE ":ISS2:")"
    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH LC.TYPE LIKE LE... AND OLD.LC.NUMBER EQ ":APP.CUSTNO:" AND (ISSUE.DATE GT ":ISS1:" AND ISSUE.DATE LE ":ISS2:")"
*  T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH @ID EQ TF1020113417"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    TEXT = "SEL1 :" : SELECTED ; CALL REM
    IF SELECTED THEN
        ENT.NO = ''
        LOOP
            LC.ID       = ''
            LCAMOUNT    = ''
            LC.CUR      = ''
            LC.ISS      = ''
            LC.EXP      = ''
            LC.NO       = ''
            CUST.MOR    = ''
            CUST.MOR1   = ''
            CUST.MOST   = ''
            CUST.MOST1  = ''
            AMT.EGP.TOT = 0
            AMT.USD.TOT = 0
            AMT.EUR.TOT = 0
            AMT.EGP     = 0
            AMT.USD     = 0
            AMT.EUR     = 0
            AMT1.IN     = 0
            AMT2.IN     = 0
            AMT3.IN     = 0
            XXX2        = 0
            XXX5        = 0
            XXX6        = 0
            XXX7        = 0
            XXX4        = 0
            XXX10       = 0
            XXX11       = 0

            REMOVE LC.ID FROM KEY.LIST SETTING POS
        WHILE LC.ID:POS
            CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ERR)
            LC.CUS      = R.LC<TF.LC.APPLICANT.CUSTNO>
            LC.CUR      = R.LC<TF.LC.LC.CURRENCY>
            LCAMOUNT    = R.LC<TF.LC.LC.AMOUNT>
            LC.ISSE     = R.LC<TF.LC.ISSUE.DATE>
            LC.OLD      = R.LC<TF.LC.OLD.LC.NUMBER>
            LC.MOR      = R.LC<TF.LC.ISSUING.BANK.NO>
** LC.MOST     = R.LC<TF.LC.ADVISING.BK.CUSTNO>
            LC.MOST     = R.LC<TF.LC.BENEFICIARY.CUSTNO>
*Line [ 186 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LC.MOR,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,LC.MOR,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.MOR   = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.MOR1  = LOCAL.REF<1,CULR.ARABIC.NAME.2>

*Line [ 196 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,LC.MOST,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,LC.MOST,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
* CUST.MOST  = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.MOST  = R.LC<TF.LC.BENEFICIARY>
* CUST.MOST1 = LOCAL.REF<1,CULR.ARABIC.NAME.2>

            LC.ISS      = LC.ISSE[7,2]:'/':LC.ISSE[5,2]:'/':LC.ISSE[1,4]
            LC.EXPE     = R.LC<TF.LC.EXPIRY.DATE>
            LC.EXP      = LC.EXPE[7,2]:'/':LC.EXPE[5,2]:'/':LC.EXPE[1,4]
            LC.NO       = LC.ID
            IF LC.CUR EQ 'EGP' THEN
                LC.AMTT      = R.LC<TF.LC.LC.AMOUNT>
            END
            IF LC.CUR NE 'EGP' THEN
                CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 204 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE LC.CUR IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
                RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
                LC.AMTT      = R.LC<TF.LC.LC.AMOUNT> * CUR.RATE
            END
            CALL F.READ(FN.CU,LC.CUS,R.CU,F.CU,ERR)

            CUST.NAME        = R.CU<EB.CUS.SHORT.NAME>

            OLD.LC.NU        = R.LC<TF.LC.OLD.LC.NUMBER>
            ISSU.BANK.NO     = R.LC<TF.LC.ISSUING.BANK>
            ISSU.BANK.NAME   = CUST.NAME
            BEN.CUS.NU       = R.LC<TF.LC.BENEFICIARY.CUSTNO>
*Line [ 230 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,BEN.CUS.NU,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,BEN.CUS.NU,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME>
            CONF.INST        = R.LC<TF.LC.CONFIRM.INST>
            LIAB.AMT         = R.LC<TF.LC.LIABILITY.AMT>
            EXP.DAT          = R.LC<TF.LC.EXPIRY.DATE>
            ADV.DAT          = R.LC<TF.LC.ADVICE.EXPIRY.DATE>

            CALL F.READ(FN.BAL,LC.NO,R.BAL,F.BAL,ERR)

            AMT.OMOLA      = R.BAL<LCAC.CHRG.AMT.DUE>
            AMT.OMOLA.LCY  = R.BAL<LCAC.CHRG.LCCY.AMT>
            DAT.OMOLA      = R.BAL<LCAC.ISSUE.DATE>

*Line [ 231 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.CHARGE = DCOUNT(R.BAL<LCAC.CHRG.CODE>,@VM)
            IF DECOUNT.CHARGE NE 0 THEN
                FOR I = 1 TO DECOUNT.CHARGE

                    CHARGE.CODE      = R.BAL<LCAC.CHRG.CODE><1,I>
                    CHARGE.CURR      = R.BAL<LCAC.CHRG.CCY><1,I>
*TEXT = DECOUNT.CHARGE ; CALL REM
                    IF CHARGE.CURR EQ 'EGP' THEN
*TEXT ="  CHARGE.CURR : ":CHARGE.CURR ; CALL REM
                        AMT.EGP    = R.BAL<LCAC.AMT.REC><1,I>
                        AMT.EGP.TOT += AMT.EGP
                        IF AMT.EGP.TOT = '' THEN
                            AMT.EGP.TOT = 0
                        END
*TEXT = "AMT.EGP : " : AMT.EGP ; CALL REM
                    END
                    IF CHARGE.CURR EQ 'USD' THEN
                        AMT.USD    = R.BAL<LCAC.AMT.REC><1,I>
                        AMT.USD.TOT += AMT.USD
                        IF AMT.USD.TOT = '' THEN
                            AMT.USD.TOT = 0
                        END
                    END
                    IF CHARGE.CURR EQ 'EUR' THEN
                        AMT.EUR    = R.BAL<LCAC.AMT.REC><1,I>
                        AMT.EUR.TOT += AMT.EUR
                        IF AMT.EUR.TOT = '' THEN
                            AMT.EUR.TOT = 0
                        END
                    END
*TEXT = "AMT.EGP.TOT " : AMT.EGP.TOT ; CALL REM
                NEXT I
            END

            IF DECOUNT.CHARGE EQ 0 THEN
                T.SEL6 = "SELECT FBNK.DRAWINGS WITH @ID LIKE LC.ID:'...'"
                CALL EB.READLIST(T.SEL6, KEY.LIST6, "", SELECTED6, ASD6)
                FOR QQ = 1 TO SELECTED6
                    CALL F.READ(FN.DR,KEY.LIST6<QQ>,R.DR,F.DR,ERR.DR)
                    OTHER.CH = R.DR<TF.DR.OTHER.CHARGES>
*Line [ 272 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DCOUNT.DR = DCOUNT(R.DR<TF.DR.OTHER.CHARGES>,@VM)
                    FOR VV = 1 TO DCOUNT.DR
                        OT.CHARGE = R.DR<TF.DR.OTHER.CHARGES><1,VV>
                        CODE.CH  = FIELD(OTHER.CH,'',1)
                        CUR.CH   = FIELD(OTHER.CH,'',2)
                        AMT.CH   = FIELD(OTHER.CH,'',3)
                        IF CUR.CH EQ 'EGP' THEN
                            AMT.EGP      = FIELD(OTHER.CH,'',3)
                            AMT.EGP.TOT += AMT.EGP
                            IF AMT.EGP.TOT = '' THEN
                                AMT.EGP.TOT = 0
                            END
                        END
                        IF CUR.CH EQ 'USD' THEN
                            AMT.USD      = FIELD(OTHER.CH,'',3)
                            AMT.USD.TOT += AMT.USD
                            IF AMT.USD.TOT = '' THEN
                                AMT.USD.TOT = 0
                            END
                        END
                        IF CUR.CH EQ 'EGP' THEN
                            AMT.EUR      = FIELD(OTHER.CH,'',3)
                            AMT.EUR.TOT += AMT.EUR
                            IF AMT.EUR.TOT = '' THEN
                                AMT.EUR.TOT = 0
                            END
                        END
                    NEXT VV
                NEXT QQ
            END
            XXX2<1,1>[1,20]     = LC.OLD
            XXX10<1,1>[1,20]    = CUST.MOR:'':CUST.MOR1
            XXX11<1,1>[1,20]    = CUST.MOST:'':CUST.MOST1
            XXX2<1,1>[20,10]    = LC.CUR
            XXX2<1,1>[30,10]    = LCAMOUNT
            XXX2<1,1>[50,10]    = LC.AMTT
            XXX2<1,1>[70,10]    = LC.ISS
            XXX2<1,1>[90,10]    = LC.EXP
            XXX5<1,1>[1,10]     = '�������� ���������'
            XXX5<1,1>[20,10]    = 'EGP: ':AMT.EGP.TOT
            XXX6<1,1>[20,10]    = 'USD: ':AMT.USD.TOT
            XXX7<1,1>[20,10]    = 'EUR: ':AMT.EUR.TOT
            XXX4<1,1>[1,130]    = '-------------------------------------------------------------'
            XXX1<1,1>[1,130]    = ' '
            PRINT XXX2<1,1>
            PRINT XXX10<1,1>
            PRINT XXX11<1,1>
            PRINT XXX1<1,1>

* PRINT XXX5<1,1>
* PRINT XXX6<1,1>
* PRINT XXX7<1,1>
* PRINT XXX4<1,1>

*REPEAT

***-------------------------------------------------------

            FT.DB.ACC2  = WW:'...'

            IF LEN(WW) EQ 8 THEN
                FT.DB.ACC2  = WW:'...'
            END
            IF LEN(WW) EQ 7 THEN
                FT.DB.ACC2  = '0':WW:'...'
            END

            ISS3        = QQ
            ISS4        = ZZ
            FT.TOT.EGP = ''
            FT.TOT.USD = ''
            FT.TOT.EUR = ''
            FT.AMT.CR.EGP = ''
            FT.AMT.CR.USD = ''
            FT.AMT.CR.EUR = ''
            TOT.COMM.EGP = ''
            TOT.COMM.EUR = ''
            TOT.COMM.USD = ''

** LC.OLD.FT = '...':LC.OLD:'...'
            LC.OLD.FT = '...':LC.OLD[4,20]
*TEXT = "FT.DB.ACC2 : " : FT.DB.ACC2 ; CALL REM
*TEXT = "ISS3 : " : ISS3 ; CALL REM
*TEXT = "ISS4 : " : ISS4 ; CALL REM
*** T.SEL1 = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DEBIT.ACCT.NO LIKE ":FT.DB.ACC2:" AND (DEBIT.THEIR.REF LIKE ":LC.OLD.FT:" OR CREDIT.THEIR.REF LIKE ":LC.OLD.FT:" ) AND (DEBIT.VALUE.DATE GE ":ISS3:" AND DEBIT.VALUE.DATE LE ":ISS4:")"
            T.SEL1 = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE NE 'AC53' AND (DEBIT.THEIR.REF LIKE ":LC.OLD.FT:" OR CREDIT.THEIR.REF LIKE ":LC.OLD.FT:" ) AND (DEBIT.VALUE.DATE GE ":ISS3:" AND DEBIT.VALUE.DATE LE ":ISS4:")"
*** T.SEL1 = "SELECT FBNK.FUNDS.TRANSFER WITH @ID EQ FT1033501389"
            CALL EB.READLIST(T.SEL1, KEY.LIST1, "", SELECTED1, ASD1)
*TEXT = "SELECTED 2 : " : SELECTED1 ; CALL REM
            ENT.NO = ''
            IF SELECTED1 THEN
                FOR MM = 1 TO SELECTED1
                    CALL F.READ(FN.FT,KEY.LIST1<MM>,R.FT,F.FT,ERR)
                    FT.DB    = R.FT<FT.DEBIT.ACCT.NO>
                    FT.CR    = R.FT<FT.CREDIT.ACCT.NO>
                    FT.CUR.D = R.FT<FT.DEBIT.CURRENCY>
                    FT.CUR.C = R.FT<FT.CREDIT.CURRENCY>
                    FT.AMT.CR= R.FT<FT.AMOUNT.CREDITED>
                    COMM.AMT = R.FT<FT.COMMISSION.AMT>
                    CHRG.AMT = R.FT<FT.CHARGE.AMT>

                    IF COMM.AMT EQ '' OR CHRG.AMT EQ '' THEN
                        IF (FT.CR[1,2] EQ 52 OR FT.CR[1,2] EQ 'PL') THEN
                            IF FT.AMT.CR[1,3] EQ 'EGP' THEN
                                FT.AMT.CR.EGP1  = FT.AMT.CR[4,10]
                                FT.AMT.CR.USD   = 0
                                FT.AMT.CR.EUR   = 0
                                FT.AMT.CR.EGP  += FT.AMT.CR.EGP1
                            END
                            IF FT.CUR.C[1,3] EQ 'USD' THEN
                                FT.AMT.CR.USD1  = FT.AMT.CR[4,10]
                                FT.AMT.CR.EGP   = 0
                                FT.AMT.CR.EUR   = 0
                                FT.AMT.CR.USD  += FT.AMT.CR.USD1
                            END
                            IF FT.CUR.C[1,3] EQ 'EUR' THEN
                                FT.AMT.CR.EUR1  = FT.AMT.CR[4,10]
                                FT.AMT.CR.EGP   = 0
                                FT.AMT.CR.USD   = 0
                                FT.AMT.CR.EUR1 += FT.AMT.CR.EUR1
                            END

                            XXX5<1,1>[40,10]    = FT.AMT.CR.EGP
                            XXX6<1,1>[40,10]    = FT.AMT.CR.USD
                            XXX7<1,1>[40,10]    = FT.AMT.CR.EUR
                            XXX4<1,1>[40,20]    = '------------------------------'
                        END
                    END ELSE
*Line [ 401 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        CG= DCOUNT(CHRG.AMT,@VM)
                        FOR NN = 1 TO CG
                            FT.CH    = R.FT<FT.CHARGE.AMT><1,NN>
                            IF FT.CH[1,3] EQ 'EGP' THEN
                                TOT.CHRG = FT.CH[4,13]
                                FT.TOT.EGP += TOT.CHRG
                            END
                            IF FT.CH[1,3] EQ 'USD' THEN

                                TOT.CHRG = FT.CH[4,13]
                                FT.TOT.USD += TOT.CHRG
                            END
                            IF FT.CH[1,3] EQ 'EUR' THEN
                                TOT.CHRG = FT.CH[4,13]
                                FT.TOT.EUR += TOT.CHRG
                            END

                        NEXT NN

                        COMM.AMT = R.FT<FT.COMMISSION.AMT>
*Line [ 422 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        CC = DCOUNT(COMM.AMT,@VM)

                        FOR JJ = 1 TO CC
                            COMM.AMT.1 = R.FT<FT.COMMISSION.AMT><1,JJ>
                            IF COMM.AMT.1[1,3] EQ 'EGP' THEN
                                FT.COM   = COMM.AMT.1[4,13]
                                TOT.COMM.EGP += FT.COM
                            END
                            IF COMM.AMT.1[1,3] EQ 'USD' THEN
                                FT.COM   = COMM.AMT.1[4,13]
                                TOT.COMM.USD += FT.COM
                            END
                            IF COMM.AMT.1[1,3] EQ 'EUR' THEN
                                FT.COM   = COMM.AMT.1[4,13]
                                TOT.COMM.EUR += FT.COM
                            END

                        NEXT JJ
* XXX5<1,1>[35.5]     = "FT"
                        XXX5<1,1>[40,10]    = FT.TOT.EGP + TOT.COMM.EGP
                        XXX6<1,1>[40,10]    = FT.TOT.USD + TOT.COMM.USD
                        XXX7<1,1>[40,10]    = FT.TOT.EUR + TOT.COMM.EUR
                        XXX4<1,1>[40,20]    = '----------------------------------------'
                    END
                NEXT MM
            END

***-------------------------------------------------------
            IF LEN(WW) EQ 8 THEN
                IN.DB.ACC2  = WW:'...'
            END
            IF LEN(WW) EQ 7 THEN
                IN.DB.ACC2  = '0':WW:'...'
            END
**TEXT = "ACC2 : " : IN.DB.ACC2 ; CALL REM
            ISS5 =   QQ
            ISS6 =   ZZ
* LC.OLD.FT = '...':LC.OLD:'...'
            LC.OLD.IN = '...':LC.OLD[4,20]:'...'
***  T.SEL2= "SELECT F.INF.MULTI.TXN WITH THEIR.REFERENCE LIKE ":LC.OLD.FT :" AND ACCOUNT.NUMBER LIKE ":IN.DB.ACC2:" AND PL.CATEGORY NE '' "
            T.SEL2= "SELECT F.INF.MULTI.TXN WITH THEIR.REFERENCE LIKE ":LC.OLD.IN :" AND PL.CATEGORY NE ''  "


            CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
**  TEXT = "SELC2 : " : SELECTED2 ; CALL REM
            IF SELECTED2 THEN
                LOOP
                    REMOVE IN.ID FROM KEY.LIST2 SETTING POS
                WHILE IN.ID:POS
                    IN.NO = IN.ID
                    CALL F.READ(FN.IN,IN.ID,R.IN,F.IN,ERR)
                    IN.AC   = R.IN<INF.MLT.AMOUNT.LCY>
*Line [ 475 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DD = DCOUNT(IN.AC,@VM)
                    FOR NN = 1 TO DD
                        ACCC      = R.IN<INF.MLT.ACCOUNT.NUMBER><1,NN>
                        ACCCC     = R.IN<INF.MLT.ACCOUNT.NUMBER>
                        CUR       = R.IN<INF.MLT.CURRENCY><1,NN>
                        TX.CODE   = R.IN<INF.MLT.TXN.CODE><1,NN>
                        PL.CAT    = R.IN<INF.MLT.PL.CATEGORY><1,NN>
                        IF TX.CODE EQ 79 THEN
                            IF CUR EQ 'EGP' THEN
                                IF PL.CAT NE '' THEN
                                    AMT1      = R.IN<INF.MLT.AMOUNT.LCY><1,NN>
                                    AMT1.IN  += AMT1
                                    IF AMT1.IN = '' THEN
                                        AMT1.IN = 0
                                    END
                                END
                            END
                        END
                        IF TX.CODE EQ 79 THEN
** TEXT = TX.CODE ; CALL REM
                            IF CUR EQ 'USD' THEN
                                IF PL.CAT NE '' THEN
                                    AMT2      = R.IN<INF.MLT.AMOUNT.FCY><1,NN>
**  TEXT = AMT2 ; CALL REM
                                    AMT2.IN  += AMT2
                                    IF AMT2.IN = '' THEN
                                        AMT2.IN = 0
                                    END
                                END
                            END
                        END
                        IF TX.CODE EQ 79 THEN
                            IF CUR EQ 'EUR' THEN
                                IF PL.CAT NE '' THEN
                                    AMT3        = R.IN<INF.MLT.AMOUNT.FCY><1,NN>
                                    AMT3.IN    += AMT3
                                    IF AMT3.IN  = '' THEN
                                        AMT3.IN = 0
                                    END
                                END
                            END
                        END
                        SIGN      = R.IN<INF.MLT.SIGN><1,NN>
                        INPL      = R.IN<INF.MLT.PL.CATEGORY><1,NN>
                        NOTES     = R.IN<INF.MLT.THEIR.REFERENCE><1,NN>
                        INDATE    = R.IN<INF.MLT.VALUE.DATE><1,NN>
                        IF SIGN EQ 'DEBIT' THEN
                            SIGN  = '���'
                        END
                        IF SIGN EQ 'CREDIT' THEN
                            SIGN = '�����'
                        END
                    NEXT NN

                    XXX5<1,1>[60,10]    = AMT1.IN
                    XXX6<1,1>[60,10]    = AMT2.IN
                    XXX7<1,1>[60,10]    = AMT3.IN
                    XXX4<1,1>[60,20]    = '-'
                REPEAT
            END
            IF COMM.AMT NE '' OR CHRG.AMT NE '' THEN
                XXX5<1,1>[80,10]    = AMT1.IN + FT.TOT.EGP + TOT.COMM.EGP + AMT.EGP.TOT
                XXX6<1,1>[80,10]    = AMT2.IN + FT.TOT.USD + TOT.COMM.USD + AMT.USD.TOT
                XXX7<1,1>[80,10]    = AMT3.IN + FT.TOT.EUR + TOT.COMM.EUR + AMT.EUR.TOT
            END ELSE
                XXX5<1,1>[80,10]    = AMT1.IN + FT.AMT.CR.EGP + AMT.EGP.TOT
                XXX6<1,1>[80,10]    = AMT2.IN + FT.AMT.CR.USD + AMT.USD.TOT
                XXX7<1,1>[80,10]    = AMT3.IN + FT.AMT.CR.EUR + AMT.EUR.TOT
            END

            PRINT XXX5<1,1>
            PRINT XXX6<1,1>
            PRINT XXX7<1,1>
            PRINT XXX4<1,1>
        REPEAT
    END
    RETURN
**------------------------------------------------------------------------
PRINT.HEAD:
*---------
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE,":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = HHH:":":MIN
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":SPACE(20):"���� �������� ��������� ����� �����������"
    PR.HD :="'L'"
    PR.HD :="'L'":"������� : ":T.DAY:SPACE(35) :"��� ������ : ":"'P'"
    PR.HD :="'L'"
    PR.HD :="'L'":"����� : " : TIMEFMT
    PR.HD :="'L'"
    PR.HD :="'L'":SPACE(1):"��� �������":SPACE(4):"������":SPACE(7):"������" : SPACE(11):"������ ������":SPACE(9) :"����� �������": SPACE(9):"����� ��������"
    PR.HD :="'L'":SPACE(1):"��� ��������"
    PR.HD :="'L'":SPACE(1):"��� ������� "
    PR.HD :="'L'":"--------------------------------------------------------------------------------------"
    PR.HD :="'L'"
    HEADING PR.HD
    RETURN
**---------------------------------------------------------------------------------------------------------
    RETURN
END
