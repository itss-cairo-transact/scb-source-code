* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>1052</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REBUILD.ECB
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ACCT.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STATIC.CHANGE.TODAY

* This routine will rebuild EB.CONTRACT.BALANCES for account passed
* Only for banks trade dated accounting system
* Account balance must be correct prior to rebuild ECB
    RTN.TIME=TIME()
    EXECUTE "COMO ON REBUILD.ECB_":TODAY:'_':RTN.TIME
    GOSUB OPEN.FILES
    SEL.CMD = "GET.LIST ACC.LIST.CORR"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,ACCT.ERR)
        LOOP
        REMOVE ACC.ID FROM SEL.LIST SETTING POS
    WHILE ACC.ID:POS
            GOSUB MAIN.PROCESS
    REPEAT
    EXECUTE "COMO OFF REBUILD.ECB_":TODAY:'_':RTN.TIME
    CRT 'COMO saved as *** REBUILD.ECB_':TODAY:'_':RTN.TIME:' ***'
    SLEEP 2
    RETURN


***********
OPEN.FILES:
***********

    F.ACCOUNT="F.ACCOUNT"
    FN.ACCOUNT=""
    CALL OPF(F.ACCOUNT,FN.ACCOUNT)

    F.ECB="F.EB.CONTRACT.BALANCES"
    FN.ECB=""
    CALL OPF(F.ECB,FN.ECB)

    F.AET="F.ACCT.ENT.TODAY"
    FN.AET=""
    CALL OPF(F.AET, FN.AET)

    F.STMT.ENTRY="F.STMT.ENTRY"
    FN.STMT.ENTRY=""
    CALL OPF(F.STMT.ENTRY,FN.STMT.ENTRY)

    F.SCT="F.STATIC.CHANGE.TODAY"
    FN.SCT=""
    CALL OPF(F.SCT, FN.SCT)

    R.ACC=""; OPEN.BAL=""; CCY=""; CO.CODE=""; R.AET=""; R.ACC<AC.CO.CODE>=""; C.FLAG=""
    R.ECB=""; I=""; Y.COUNT=""; AET.CNT=""; J=""; MVMT=0; CHK.TYPE="" ;SEL.CMD="";SEL.LIST="";NO.OF.REC="";ACCT.ERR="";ACC.ID=""
    RETURN

*************
MAIN.PROCESS:
*************


    PRINT "Processing account ":ACC.ID
    READU R.ACC FROM FN.ACCOUNT, ACC.ID ELSE R.ACC=""

    READ R.AET FROM FN.AET, ACC.ID ELSE R.AET=""
    OPEN.BAL=R.ACC<AC.OPEN.ACTUAL.BAL>
    CCY=R.ACC<AC.CURRENCY>
    CO.CODE=R.ACC<AC.CO.CODE>
    PRODUCT.CAT=R.ACC<AC.CATEGORY>

    READU R.ECB FROM FN.ECB, ACC.ID ELSE R.ECB=""
    IF NOT(R.ECB) THEN
        PRINT "Missing EB.CONTRACT.BALANCES record"
        RETURN
    END

    Y.COUNT=DCOUNT(R.ECB<ECB.TYPE.SYSDATE>,@VM)
    AET.CNT=DCOUNT(R.AET,@FM)

******** Clear multi-value other than accruals ********

    FOR I =1 TO Y.COUNT
        OP.BAL='' ; OP.BAL.LCL='' ; CREDIT.MVT ='' ; CREDIT.MVT.LCL='' ; DEBIT.MVT='' ; DEBIT.MVT.LCL=''
        OP.BAL=R.ECB<ECB.OPEN.BALANCE,I>
        OP.BAL.LCL=R.ECB<ECB.OPEN.BAL.LCL,I>
        CREDIT.MVT=R.ECB<ECB.CREDIT.MVMT,I>
        CREDIT.MVT.LCL=R.ECB<ECB.CR.MVMT.LCL,I>
        DEBIT.MVT=R.ECB<ECB.DEBIT.MVMT,I>
        DEBIT.MVT.LCL=R.ECB<ECB.DB.MVMT.LCL,I>


        CALL EB.ROUND.AMOUNT(CCY,OP.BAL,"","")
        CALL EB.ROUND.AMOUNT(CCY,OP.BAL.LCL,"","")
        CALL EB.ROUND.AMOUNT(CCY,CREDIT.MVT,"","")
        CALL EB.ROUND.AMOUNT(CCY,CREDIT.MVT.LCL,"","")
        CALL EB.ROUND.AMOUNT(CCY,DEBIT.MVT,"","")
        CALL EB.ROUND.AMOUNT(CCY,DEBIT.MVT.LCL,"","")

        IF (R.ECB<ECB.OPEN.BALANCE,I> NE OP.BAL) OR (R.ECB<ECB.OPEN.BAL.LCL,I> NE OP.BAL.LCL) OR (R.ECB<ECB.CREDIT.MVMT,I> NE CREDIT.MVT) OR (R.ECB<ECB.CR.MVMT.LCL,I> NE CREDIT.MVT.LCL) OR (R.ECB<ECB.DEBIT.MVMT,I> NE DEBIT.MVT) OR(R.ECB<ECB.DB.MVMT.LCL,I> NE DEBIT.MVT.LCL) THEN
                PRINT "Problematic Type SysDate =":R.ECB<ECB.TYPE.SYSDATE,I>:"# Before Formatting":R.ECB<ECB.OPEN.BALANCE,I>:"#":R.ECB<ECB.OPEN.BAL.LCL,I>:"#":R.ECB<ECB.CREDIT.MVMT,I>:"#":R.ECB<ECB.CR.MVMT.LCL,I>:"#":R.ECB<ECB.DEBIT.MVMT,I>:"#":R.ECB<ECB.DB.MVMT.LCL,I>:" After Formatting ":OP.BAL:"#":OP.BAL.LCL:"#":CREDIT.MVT:"#":CREDIT.MVT.LCL:"#":DEBIT.MVT:"#":DEBIT.MVT.LCL
                R.ECB<ECB.OPEN.BALANCE,I>=OP.BAL
                R.ECB<ECB.OPEN.BAL.LCL,I>=OP.BAL.LCL
                R.ECB<ECB.CREDIT.MVMT,I>=CREDIT.MVT
                R.ECB<ECB.CR.MVMT.LCL,I>=CREDIT.MVT.LCL
                R.ECB<ECB.DEBIT.MVMT,I>=DEBIT.MVT
                R.ECB<ECB.DB.MVMT.LCL,I>=DEBIT.MVT.LCL
        END

    NEXT I

    WRITE R.ECB TO FN.ECB, ACC.ID

PROGRAM.END:

    RELEASE FN.ECB, ACC.ID
    RELEASE FN.ACCOUNT, ACC.ID

    RETURN


END
