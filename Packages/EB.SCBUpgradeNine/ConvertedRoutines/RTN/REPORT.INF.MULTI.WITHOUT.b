* @ValidationCode : Mjo4MzgyNTUxNTA6Q3AxMjUyOjE2NDQ5Mzk1Mzc1NTE6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:38:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************AHMED NAHRAWY(FOR EVER NI7OOOOOOOOO)********
*-----------------------------------------------------------------------------
* <Rating>1474</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.INF.MULTI.WITHOUT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    SCB.CO = ID.COMPANY
    OUT.AMOUNT = ''

    REPORT.ID='REPORT.INF.MULTI.WITHOUT'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*-------
    INTER  = ""
    YTEXT = "Enter IN.NO : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ID = COMI

    FN.LD='F.INF.MULTI.TXN' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*   CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
*----
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
    FN.COMP ='F.COMPANY' ; F.COMP = ''
    CALL OPF(FN.COMP,F.COMP)
    CALL F.READ(FN.COMP,SCB.CO,R.COMP,F.COMP,ER.COMP)

    BRANCH = R.COMP<EB.COM.COMPANY.NAME><1,2>
    YYBRN  = BRANCH
    FN.CAT = 'F.CATEGORY'  ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.AC = 'FBNK.ACCOUNT'  ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER'  ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.CUR = 'FBNK.CURRENCY'  ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
*------------------------------------------------------------------------
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,ERR1)
    AC1 = R.LD<INF.MLT.ACCOUNT.NUMBER>
*Line [ 107 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(AC1,@VM)
    I = 0
    INPUTT  = R.LD<INF.MLT.INPUTTER>
    INP     = FIELD(INPUTT,'_',2)
    AUTHI   = R.LD<INF.MLT.AUTHORISER>
    AUTH    = FIELD(AUTHI,'_',2)

    FOR J = 1 TO DD
        ACCC      = R.LD<INF.MLT.ACCOUNT.NUMBER><1,J>
        CUR       = R.LD<INF.MLT.CURRENCY><1,J>
        AMT1      = R.LD<INF.MLT.AMOUNT.LCY><1,J>
        AMT2      = R.LD<INF.MLT.AMOUNT.FCY><1,J>
        SIGN      = R.LD<INF.MLT.SIGN><1,J>
        VALUE.DAT = R.LD<INF.MLT.VALUE.DATE><1,J>
        T.DAY1 = VALUE.DAT[7,2]:'/':VALUE.DAT[5,2]:"/":VALUE.DAT[1,4]
        IF SIGN EQ 'DEBIT' THEN
            SIGN  = '���'
        END ELSE
            IF SIGN EQ 'CREDIT' THEN
                SIGN = '�����'
            END
        END
        IF ACCC EQ '' THEN
            ACCC = R.LD<INF.MLT.PL.CATEGORY><1,J>


            CALL F.READ(FN.CAT,ACCC,R.CAT,F.CAT,ER.CAT)
            MYCAT = R.CAT<EB.CAT.SHORT.NAME>
            CUST.NAME = MYCAT
            CUST.NAME1= MYCAT
            CUST.ADDRESS = MYCAT
            CUR.ID = CUR
*--- EDIT BY NESSMA & RIHAM ON 2012/04/03 ---
            CUST.ADD1 = ''
            CUST.ADD2 = ''
            CUST.REG  = ''
            CUST.GOV  = ''
*--------------------------------------------
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.LD<INF.MLT.ACCOUNT.NUMBER><1,J>
                CALL F.READ(FN.AC,ACCC,R.AC,F.AC,ER.AC)
                CUS.ID = R.AC<AC.CUSTOMER>
                CUR.ID = R.AC<AC.CURRENCY>
                CATEG  = R.AC<AC.CATEGORY>
                YYBRN  = BRANCH
** EDITED BY MR.SAIZO 2012/10/11 ***
*    CALL F.READ(FN.CAT,ACCC,R.CAT,F.CAT,ER.CAT)
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,CATEG,MYCAT1)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                MYCAT1=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
*    MYCAT1 = R.CAT<EB.CAT.SHORT.NAME>
                INTER  = MYCAT1

                IF CUS.ID NE '' THEN
                    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ER.CUS)
                    CUST.NAME  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                    CUST.NAME1 = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>
*********** UPDATED BY NESSMA ************
                    IF CUST.NAME EQ '' THEN
                        CALL F.READ(FN.AC,ACCC,R.AC,F.AC,ER.AC)
                        CUST.NAME  = R.AC<AC.ACCOUNT.TITLE.1>
                    END
******************************************
                    CUST.ADDRESS  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,1>
                    CUST.ADDRESS1 = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,2>

                    CUST.GOV      = R.CUS<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                    CUST.REG      = R.CUS<EB.CUS.LOCAL.REF><1,CULR.REGION>
*Line [ 176 ] change SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION to SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 183 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR (('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION)<1,1>,CUST.GOV,CUST.ADD2)
                    F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
                    FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
                    CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
                    CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.GOV,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
                    CUST.ADD2=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
*Line [ 178 ] change SCB.CUS.REGION':@FM:REG.DESCRIPTION to SCB.CUS.REGION':@FM:REG.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 191 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR (('SCB.CUS.REGION':@FM:REG.DESCRIPTION)<1,1>,CUST.REG,CUST.ADD1)
                    F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
                    FN.F.ITSS.SCB.CUS.REGION = ''
                    CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
                    CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.REG,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
                    CUST.ADD1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
                END
*--- EDIT BY NESSMA & RIHAM ON 2012/04/03 ---
                IF CUS.ID NE '' THEN
                    CUST.ADD1 = ''
                    CUST.ADD2 = ''
                    CUST.REG  = ''
                    CUST.GOV  = ''
                END
*---------------------------------------
                IF CUS.ID EQ '' THEN
                    CUST.GOV = ''
                    CUST.REG  = ''
                    CUST.ADDRESS  = ''
                    CUST.NAME  = ''
                    CUST.NAME1  = ''
                    CUST.ADD2  = ''
                    CUST.ADD1  = ''
                END

                IF CUST.GOV = 98 THEN
                    CUST.ADD1 = ''
                END
                IF CUST.REG = 998 THEN
                    CUST.ADD2 = ''
                END
                IF CUST.GOV = 999 THEN
                    CUST.ADD1 = ''
                END
                IF CUST.REG = 999 THEN
                    CUST.ADD2 = ''
                END
                CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ER.CUR)
                CURR = R.CUR<EB.CUR.CCY.NAME>

                IF CUST.ADDRESS EQ  " " THEN
                    CUST.ADDRESS = "���� ��������� ������"
                END
            END
        END

        CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ER.CUR)
        CUR = R.CUR<EB.CUR.CCY.NAME><1,2>

        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)

        BEGIN CASE
            CASE CUR.ID NE 'EGP'
                AMT22 = FMT(AMT2,"L2")
                IN.AMOUNT = AMT22
                CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ER.CUR)
                CUR = R.CUR<EB.CUR.CCY.NAME><1,2>

                OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                XX6<1,J>[1,15]  = '������ ������� ��������: ':OUT.AMT:IN.AMOUNT
            CASE CUR.ID EQ 'EGP'
                AMT11 = FMT(AMT1,"L2")
                IN.AMOUNT = AMT11
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ER.CUR)
                CUR = R.CUR<EB.CUR.CCY.NAME><1,2>

                OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                XX6<1,J>[1,15]  = '������ ������� �������: ':OUT.AMT:IN.AMOUNT
        END CASE
        BR.DATA = R.LD<INF.MLT.THEIR.REFERENCE><1,J>
        YYBRN  = BRANCH
*Line [ 268 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ER.CUR)
        CUR = R.CUR<EB.CUR.CCY.NAME><1,2>

        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX9  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132) ; XX15 = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132) ; XX16 = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132) ; XX17 = SPACE(132)
        I ++
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

        XX1<1,J>[3,35]  = CUST.NAME :' ':CUST.NAME1
        XX2<1,J>[3,35]  = CUST.ADDRESS

        XX3<1,J>[3,35]  = CUST.ADD2 :' ': CUST.ADD1

        XX<1,J>[45,15]  = '������     : '
        XX<1,J>[59,15]  = IN.AMOUNT

        XX1<1,J>[45,15] = '��� ������:'
        XX1<1,J>[59,15] = ACCC

        XX2<1,J>[45,15] = '��� ������ : '
        XX2<1,J>[59,15] = INTER

        XX3<1,J>[45,15] = '������     : '
        XX3<1,J>[59,15] = CUR

        XX4<1,J>[45,15] = '����� ���� : '
        XX4<1,J>[59,15] = T.DAY1

        XX6<1,J>[1,15]  = '������'
        XX6<1,J>[30,15] = '��� �������'
        XX6<1,J>[60,15] = '������'

        XX7<1,J>[1,15]  = INP:' �'
        XX7<1,J>[30,15] = COMI:' �'
        XX7<1,J>[60,15] = AUTH:' �'

        XX8<1,J>[3,35]  = '������ ������� : '
        XX8<1,J>[20,15] = OUT.AMT
        XX9<1,J>[3,15]  = '������         : '
        XX9<1,J>[20,15] = BR.DATA

** XX10<1,J>[3,15]  = '�����       : '

** XX10<1,J>[20,35] = CUST.NAME :' ':CUST.NAME1

* XX1<1,J>[45,15]  = '��� ������  : '
* XX1<1,J>[80,15]  = ACCC

* XX1<1,J>[3,15]   = '�������      : '
* XX1<1,J>[20,35]  = CUST.ADDRESS :' ':CUST.ADDRESS1

* XX3<1,J>[45,15]  = ' ��� ������ : '
* XX3<1,J>[64,15]  =  INTER

* XX8<1,J>[45,15] = '������ :' :CUR

        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

* XX4<1,J>[45,15]  = ' ������:' :IN.AMOUNT :CUR
* XX5<1,J>[45,15]  = '����� ���� :' : T.DAY

* XX6<1,J>[1,15]  = '������ ������� �������: ':OUT.AMT
* XX7<1,J>[65,15] = '�� ��� ���� ������'

* XX15<1,J>[1,15] ="������"
* XX16<1,J>[1,15]= INP

* XX15<1,J>[30,15] ="�������"
* XX16<1,J>[30,15]= AUTHI

* XX15<1,J>[45,15] ="������"
* XX16<1,J>[45,15] = COMI

*-------------------------------------------------------------------
***  IF ACCC[1,2] EQ 99 THEN
*        YYBRN  = FIELD(BRANCH,'.',2)
        YYBRN  = BRANCH
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
        PR.HD  ="'L'":SPACE(1):"��� ���� ������"
        PR.HD :="'L'":"������� : ":T.DAY
        PR.HD :="'L'":"����� : ":YYBRN
        PR.HD :="'L'":"�����":SIGN
        PR.HD :="'L'":"INMAZKOREEN"
*PR.HD :="'L'"
        PRINT
        HEADING PR.HD
*------------------------------------------------------------------
**PRINT XX<1,I>
***** IF ACCC[1,2] EQ 99 THEN
        PRINT XX1<1,I>
        PRINT XX2<1,I>
        PRINT XX<1,I>
        PRINT STR(' ',82)
        PRINT XX3<1,I>
        PRINT XX4<1,I>
        PRINT XX11<1,I>
        PRINT STR(' ',82)
        PRINT XX8<1,I>
        PRINT XX9<1,I>
        PRINT XX10<1,I>
        PRINT XX5<1,I>
        PRINT STR(' ',82)
        PRINT XX6<1,I>
        PRINT STR('-',82)
        PRINT XX7<1,I>


*PRINT XX10<1,I>
*PRINT XX1<1,I>
*PRINT XX3<1,I>
*PRINT XX4<1,I>
*PRINT XX8<1,I>
*PRINT XX5<1,I>
*PRINT STR(' ',82)
*PRINT XX6<1,I>
*PRINT XX7<1,I>
*PRINT STR('=',82)
*PRINT XX15<1,I>
*PRINT XX16<1,I>
*PRINT STR(' ',82)
        PRINT STR(' ',82)
*******            PRINT STR('=',82)
*** END
    NEXT J
*===============================================================
RETURN
END
