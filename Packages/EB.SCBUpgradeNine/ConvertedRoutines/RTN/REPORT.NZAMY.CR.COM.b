* @ValidationCode : MjoxNTU4NDE2ODQ3OkNwMTI1MjoxNjQ0OTQwMzk0NzU1OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:53:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*********************************NI7OOOOOOOOO************************
SUBROUTINE REPORT.NZAMY.CR.COM
*
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE

*------------------------------------------------------------------------
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> NE ' ' THEN
        GOSUB INITIATE
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ���������" ; CALL REM
    END
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.NZAMY.CR.COM'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR='' ; R.BR=''
    CALL OPF(FN.BR,F.BR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    ID = ID.NEW
*YTEXT = "Enter the BT No. : "
*CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BR,ID,R.BR,F.BR,E1)

    DRAWER.ID   = R.NEW(EB.BILL.REG.DRAWER)

*Line [ 79 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS1= LOCAL.REF<1,CULR.GOVERNORATE>
    CUST.ADDRESS2= LOCAL.REF<1,CULR.REGION>

*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
    F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
    FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
    CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
    CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.ADDRESS1,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
    CUST.ADD2=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
*Line [ 99 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)
    F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
    FN.F.ITSS.SCB.CUS.REGION = ''
    CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
    CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.ADDRESS2,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
    CUST.ADD1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>


    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END


    ACC.NO    = 'PL52877'

    CATEG.ID  = ACC.NO[3,5]
*Line [ 108 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
    
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

    AMOUNT    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>

    IN.AMOUNT = AMOUNT[4,6]
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*BR.DATA = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.OPERATION.TYPE>

    CUR.ID    = R.NEW(EB.BILL.REG.CURRENCY)
*Line [ 120 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
    
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    COM.CODE    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>
*Line [ 155 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('FT.COMMISSION.TYPE':@FM:FT4.DESCRIPTION,COM.CODE,COM.NAME)
    F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
    FN.F.ITSS.FT.COMMISSION.TYPE = ''
    CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
    CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,COM.CODE,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
    COM.NAME=R.ITSS.FT.COMMISSION.TYPE<FT4.DESCRIPTION>

    INPUTTER = R.BR<EB.BILL.REG.INPUTTER>
    AUTH     = R.BR<EB.BILL.REG.AUTHORISER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]     = CATEG
*    XX2<1,1>[3,35]   = CATEG
* XX3<1,1>[3,35]   = CUST.ADDRESS
* XX4<1,1>[3,35]   = CUST.ADD1:' ': CUST.ADD2
    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT[4,10]
    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = 'PL52877'
    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG
    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR
    XX5<1,1>[3,15]  = '�������    : '
    XX5<1,1>[20,15] = COM.NAME
    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15]  = AUTHI
    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = INP
    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = ID
    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT

*-------------------------------------------------------------------
*Line [ 198 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(45) :"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*NEXT I
*END
*===============================================================
RETURN
END
