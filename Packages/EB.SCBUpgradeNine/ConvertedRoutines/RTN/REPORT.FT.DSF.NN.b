* @ValidationCode : MjotMTMyMzY5NDkxNzpDcDEyNTI6MTY0NDkzODM0MjcwNjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:19:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NESSMA**********************
SUBROUTINE REPORT.FT.DSF.NN

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE
*------------------------------------------------------------------------
    IF V$FUNCTION NE 'R' THEN
        GOSUB INITIATE
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ����� �������" ; CALL REM
    END
RETURN
*========================================================================
INITIATE:
    SCB.CO    = ID.COMPANY
    FT.ID     = ID.NEW
    REPORT.ID = 'REPORT.FT.DSF.NN'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT=''
    CALL OPF(FN.FT,F.FT)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT=''
    CALL OPF(FN.ACCT,F.ACCT)

    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
*==========
    ACCT.NAME    = ''
    OUT.AMOUNT   = ''
    CURR.ADD     = R.NEW(FT.CREDIT.CURRENCY)
*Line [ 74 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 75 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CURR.ADD,CUR.2)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CURR.ADD,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR.2=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

    AMT.KHASM    = R.NEW(FT.DEBIT.AMOUNT)
    IN.AMOUNT    = AMT.KHASM
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR.2 : ' ' : '�����'

    ACCT.NO.ADD  = R.NEW(FT.CREDIT.ACCT.NO)

    CALL F.READ(FN.ACCT,ACCT.NO.ADD,R.ACCT,F.ACCT,ER.ACCT)
    CUS.NO       = R.ACCT<AC.CUSTOMER>
    ACCT.NAME    = R.ACCT<AC.ACCOUNT.TITLE.1>
    TEXT = "NAME = " : ACCT.NAME  ; CALL REM

    CATEG.NO     = ACCT.NO.ADD[4,5]
*Line [ 90 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.NO,CATEG)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG.NO,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
    IF CATEG.NO EQ 10000 THEN ACCT.NAME = CATEG

    DATE.FT      = TODAY
    DATE.FT      = DATE.FT[1,4]:"/":DATE.FT[5,2]:"/":DATE.FT[7,2]

    NOTES.DESC   = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED>

    INPUTTER     = R.NEW(FT.INPUTTER)
    AUTH         = R.NEW(FT.AUTHORISER)
    INP          = FIELD(INPUTTER,'_',2)
    AUTHI        = FIELD(AUTH,'_',2)
*============
    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
*============
    XX5<1,1>[1,40] = ACCT.NAME
    XX<1,1>[60,15]  = '������     : '
    XX<1,1>[74,15]  =  IN.AMOUNT
    XX1<1,1>[60,15] = '��� ������ : '
    XX1<1,1>[74,15] = ACCT.NO.ADD
    XX2<1,1>[60,15] = '��� ������ : '
    XX2<1,1>[74,15] = CATEG
    XX3<1,1>[60,15] = '������     : '
    XX3<1,1>[74,15] =  CUR.2
    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = DATE.FT
    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15]  = AUTHI
    XX6<1,1>[30,15] = '��� �������'
    XX7<1,1>[35,15] = FT.ID
    XX6<1,1>[60,15] = '������'
    XX7<1,1>[60,15] = INP
    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = NOTES.DESC
*-------------------------------------------------------------------
*CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,SCB.CO,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*YYBRN  = FIELD(BRANCH,'.',2)
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" SCB.CREDIT1"
    PR.HD :="'L'":"����� �����"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX5<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
