* @ValidationCode : MjotMTMxMjI0ODU4MjpDcDEyNTI6MTY0NDk0MTIzMzI5Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 18:07:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>-88</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.SF.CREDIT.RE.2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.TRANS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SF.TRANS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.SF.CREDIT.RE.2'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.BR = 'F.SCB.SF.TRANS' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)
RETURN
*========================================================================
PROCESS:
*-------
    YTEXT = "Enter the SF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BR,COMI,R.SAFE,F.BR,EE1)
    SAFE.ID   = COMI
    TRNS.TYPE = R.SAFE<SCB.SF.TR.TRANSACTION.TYPE>


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CUS           = R.SAFE<SCB.SF.TR.CUSTOMER.NO>
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>
    CUST.NAME3    = LOCAL<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS1 = LOCAL<1,CULR.ARABIC.ADDRESS>
    CUST.ADDRESS2 = LOCAL<1,CULR.ARABIC.ADDRESS>

*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
    F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
    FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
    CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
    CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.ADDRESS1,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
    CUST.ADD2=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)
    F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
    FN.F.ITSS.SCB.CUS.REGION = ''
    CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
    CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.ADDRESS2,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
    CUST.ADD1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>

    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END

*--------------
    IF TRNS.TYPE EQ 'MRGN' THEN
*        ACC.DB = R.SAFE<SCB.SF.TR.MARGIN.ACCT>
    END

    IF TRNS.TYPE EQ 'EXPD' THEN
*        ACC.DB = R.SAFE<SCB.SF.TR.CUST.DEBIT.ACCT>
    END
*--------------
    ACC.DB = ""
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ("ACCOUNT":@FM:AC.CATEGORY,ACC.DB,CATEG.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.DB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ("ACCOUNT":@FM:AC.CURRENCY,ACC.DB,CURR.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.DB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CURR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>

    CATEG.ID = "54015"
    ACC.DB   = "54015"
*Line [ 116 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG.NAME)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

    AMOUNT    = R.SAFE<SCB.SF.TR.COMMISSION.AMT2>

    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    V.DATE = R.SAFE<SCB.SF.TR.VALUE.DATE>
*Line [ 125 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CURR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CURR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    MAT.DATE  = V.DATE[7,2]:'/':V.DATE[5,2]:"/":V.DATE[1,4]

    INPUTTER = R.SAFE<SCB.SF.TR.INPUTTER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTH     = R.SAFE<SCB.SF.TR.AUTHORISER>
    AUTHI    = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]    = CATEG.NAME
*    XX1<1,1>[3,35]   = CUST.NAME3
*    XX2<1,1>[3,35]   = CUST.ADDRESS
*    XX3<1,1>[3,35]   = CUST.ADD1:' ':CUST.ADD2

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.DB

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG.NAME

    XX3<1,1>[45,15] = '������     : '
*    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

*XX5<1,1>[3,15]  = '��� �����      : '
*XX5<1,1>[20,15] = CHQ.NO

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15]  = INP

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15 ] = COMI

    XX8<1,1>[3,35]   = '������ ������� : '
    XX8<1,1>[20,15]  = OUT.AMT
*-------------------------------------------------------------------
*Line [ 219 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"SAFEKEEP"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
