* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE RTN.POS.CLEARING

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------------

    KEY.LIST = "" ; SELECTED = "" ;  ER.FT = "" ; SW2 = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC37'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
    END ELSE
        GOSUB INITIAL
        GOSUB PROCESS
    END

    TEXT = 'DONE' ; CALL REM

    RETURN

*--------------------------------------------------------------------
INITIAL:
*=======
    FN.AC = "FBNK.ACCOUNT" ;F.AC = ""
    CALL OPF(FN.AC,F.AC)

    FN.AC.LOCK = "FBNK.AC.LOCKED.EVENTS" ;F.AC.LOCK = ""
    CALL OPF(FN.AC.LOCK,F.AC.LOCK)

    FN.AC.LOCK.GU = "F.AC.LOCKED.EVENTS.ATM.123.REF" ;F.AC.LOCK.GU = ""
    CALL OPF(FN.AC.LOCK.GU,F.AC.LOCK.GU)

    FN.AC.LOCK.AU = "F.AC.LOCKED.EVENTS.EBC.AUTH.NO" ;F.AC.LOCK.AU = ""
    CALL OPF(FN.AC.LOCK.AU,F.AC.LOCK.AU)

    FN.CI = "FBNK.CARD.ISSUE" ; F.CI = ""
    CALL OPF(FN.CI, F.CI)


    OPENSEQ "POS.CLEARING" , "POS.CLEARING.":TODAY TO AA THEN
        CLOSESEQ AA
        HUSH ON
        EXECUTE 'DELETE ':"POS.CLEARING":' ':"POS.CLEARING.":TODAY
        HUSH OFF
    END
    OPENSEQ "POS.CLEARING" , "POS.CLEARING.":TODAY TO AA ELSE
        CREATE AA THEN
            PRINT 'FILE POS.CLEARING.':TODAY:' CREATED IN POS.CLEARING'
        END ELSE
            STOP 'Cannot create POS.CLEARING.':TODAY:' File IN POS.CLEARING'
        END
    END


*----------------------

    OPENSEQ "POS.CLEARING" , "POS.CLEARING.ERROR.":TODAY TO EE THEN
        CLOSESEQ EE
        HUSH ON
        EXECUTE 'DELETE ':"POS.CLEARING":' ':"POS.CLEARING.ERROR.":TODAY
        HUSH OFF
    END
    OPENSEQ "POS.CLEARING" , "POS.CLEARING.ERROR.":TODAY TO EE ELSE
        CREATE EE THEN
            PRINT 'FILE POS.CLEARING.ERROR.':TODAY:' CREATED IN POS.CLEARING'
        END ELSE
            STOP 'Cannot create POS.CLEARING.ERROR.':TODAY:' File IN POS.CLEARING'
        END
    END

    RETURN
*---------------------------------------------------------------------
PROCESS:
*=======
    KEY.LIST2 = "" ; SELECTED2 = "" ;  ER.FX2=""
    T.SEL2 = "SELECT POS.CLEARING WITH @ID LIKE ....txt"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.FX2)
    IF SELECTED2 THEN
        FOR K = 1 TO SELECTED2

            OPENSEQ "POS.CLEARING",KEY.LIST2<K> TO BB ELSE
                TEXT = "ERROR OPEN FILE ":KEY.LIST2<K> ; CALL REM
                RETURN
            END

            IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

            EOF = "" ; LINE.NO = 1 ; CHK.FLAG = 0 ; I = 1
            TAG.NO = '' ; LINE.NO.OLD = '' ; SCB.GUID = ''
            WS.CCY = 'EGP'

            LOOP WHILE NOT(EOF)
                READSEQ R.LINE FROM BB THEN
                    IF LINE.NO GE 2 THEN
                        CHK.VAL = LEN(R.LINE)
                        LOOP UNTIL I GT CHK.VAL

                            WS.ACCT.NO       = R.LINE[32,16]
                            WS.PAN           = R.LINE[13,19]
                            WS.PCODE         = R.LINE[60,6]
                            WS.TRN.DATE      = R.LINE[116,6]
                            WS.AUTH.CODE     = R.LINE[147,6]
                            WS.AMT1          = R.LINE[102,12]
                            WS.AMT           = TRIM(WS.AMT1,"0","L")
                            WS.AMT           = WS.AMT / 100
                            SCB.AUTH.CODE    = WS.TRN.DATE:WS.AUTH.CODE
                            WS.TRN.TIME      = R.LINE[122,6]
                            WS.DATE.TIME     = WS.TRN.DATE:WS.TRN.TIME
                            WS.ACQ.CCY       = R.LINE[227,3]
                            WS.SET.CCY       = R.LINE[230,3]
                            WS.TRN.FLAG      = R.LINE[114,2]
                            WS.TERMINAL.ID   = R.LINE[153,16]
                            WS.TERMINAL.LOC  = R.LINE[184,40]
                            WS.TERMINAL.TYPE = R.LINE[224,3]
                            WS.AQUIR.ID      = R.LINE[236,11]
                            WS.FORWD.ID      = R.LINE[247,11]
                            WS.ISSUE.RATE    = R.LINE[296,9]
                            WS.MSG.DESC      = R.LINE[305,25]
                            WS.ATM.MSG.TYPE  = R.LINE[9,4]
                            WS.RET.REF.NO    = R.LINE[135,12]

                            I++
                        REPEAT
                        I = 1

***** GET ACCOUNT NO *****
                        CALL F.READ(FN.AC,WS.ACCT.NO,R.AC,F.AC,ERR.AC)
                        IF NOT(ERR.AC) THEN
                            WS.ACCT = WS.ACCT.NO
                        END ELSE
                            CALL F.READ(FN.AC.LOCK.GU,SCB.AUTH.CODE,R.AC.LOCK.GU,F.AC.LOCK.GU,ERR.LOCK.GU)
                            IF NOT(ERR.LOCK.GU) THEN
                                LOOP
                                    REMOVE ACC.NO FROM R.AC.LOCK.GU SETTING POS
                                WHILE ACC.NO:POS
                                    AC.LOCK.ID = FIELD(ACC.NO,"*",2)
                                    CALL F.READ(FN.AC.LOCK,AC.LOCK.ID,R.AC.LOCK,F.AC.LOCK,ERR.AC.LOCK)
                                    WS.ACCT = R.AC.LOCK<AC.LCK.ACCOUNT.NUMBER>
                                REPEAT
                            END ELSE
                                CRD = "ATMC.":WS.PAN
                                CALL F.READ(FN.CI,CRD,R.CI,F.CI,ERR.CI)
                                LOCAL.REF = R.CI<CARD.IS.LOCAL.REF>
                                WS.ACCT = LOCAL.REF<1,LRCI.FAST.ACCT.NO>
                            END
                        END
**************************

                        IF WS.TRN.FLAG EQ 'DB' THEN
                            WS.DB.ACCT = WS.ACCT
                            WS.CR.ACCT = 'EGP1131300010099'
                        END

                        IF WS.TRN.FLAG EQ 'CR' THEN
                            WS.CR.ACCT = WS.ACCT
                            WS.DB.ACCT = 'EGP1131300010099'
                        END

                        IF WS.TRN.FLAG NE '' THEN

                            IF WS.ACQ.CCY EQ '818' THEN
                                TRN.TYPE = 'AC37'
                                WS.COMM.CODE = 'ATMSCBPOSL'
                            END ELSE
                                TRN.TYPE = 'AC38'
                                WS.COMM.CODE = 'ATMSCBPOSI'
                            END

                            GOSUB BUILD.RECORD.LOCKS
                            GOSUB BUILD.RECORD.FT
                        END


                    END
                    LINE.NO ++

                END ELSE
                    EOF = 1
                END
            REPEAT

            CLOSESEQ BB

            EXECUTE 'COPY FROM POS.CLEARING ':KEY.LIST2<K>:' TO POS.CLEARING.DONE'
            EXECUTE 'DELETE POS.CLEARING ':KEY.LIST2<K>
            EXECUTE 'COPY FROM POS.CLEARING POS.CLEARING.':TODAY:' TO OFS.IN'
            EXECUTE 'DELETE POS.CLEARING POS.CLEARING.':TODAY

        NEXT K
    END

    RETURN
*------------------------------------------------------------------
BUILD.RECORD.LOCKS:
*------------------
    CALL F.READ(FN.AC.LOCK.GU,SCB.AUTH.CODE,R.AC.LOCK.GU,F.AC.LOCK.GU,ERR.ACLKGU)
    IF NOT(ERR.ACLKGU) THEN

        MSG.OFS.LOKS  = "AC.LOCKED.EVENTS,ATM.123/R,ATMPOS//EG0010099,"
        MSG.OFS.LOKS := SCB.AUTH.CODE

        WRITESEQ MSG.OFS.LOKS TO AA ELSE
            PRINT  'CAN NOT WRITE LINE ':MSG.OFS.LOKS
        END

    END ELSE

        CALL F.READ(FN.AC.LOCK.AU,WS.AUTH.CODE,R.AC.LOCK.AU,F.AC.LOCK.AU,ERR.ACLKAU)

        IF NOT(ERR.ACLKAU) THEN
            MSG.OFS.LOKS  = "AC.LOCKED.EVENTS,ATM.123/R,ATMPOS//EG0010099,"
            MSG.OFS.LOKS := WS.AUTH.CODE

            WRITESEQ MSG.OFS.LOKS TO AA ELSE
                PRINT  'CAN NOT WRITE LINE ':MSG.OFS.LOKS
            END

        END ELSE

            WRITESEQ WS.RET.REF.NO TO EE ELSE
                PRINT  'CAN NOT WRITE LINE ':WS.RET.REF.NO
            END

        END
    END

    RETURN
*------------------------------------------------------------------
BUILD.RECORD.FT:
*------------
    MSG.OFS.FT  = "FUNDS.TRANSFER,MECH,ATMPOS//EG0010099,,"

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":TRN.TYPE
    OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY=":WS.CCY
    OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY=":WS.CCY
    OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO=":WS.DB.ACCT
    OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO=":WS.CR.ACCT
    OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT=":WS.AMT
    OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE=":TODAY
    OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE=":TODAY
    OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N=":"NO"
    OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N=":"NO"
    OFS.MESSAGE.DATA :=  ",PAN=":WS.PAN
    OFS.MESSAGE.DATA :=  ",PCODE=":WS.PCODE
    OFS.MESSAGE.DATA :=  ",EBC.SET.DATE=":WS.TRN.DATE
    OFS.MESSAGE.DATA :=  ",TRANSACTION.AMT=":WS.AMT
    OFS.MESSAGE.DATA :=  ",SETTLEMENT.AMT=":WS.AMT
    OFS.MESSAGE.DATA :=  ",LOCAL.DATE.TIME=":WS.DATE.TIME
    OFS.MESSAGE.DATA :=  ",EBC.AUTH.NO=":WS.AUTH.CODE
    OFS.MESSAGE.DATA :=  ",TRANSACTION.CUR=":WS.SET.CCY
    OFS.MESSAGE.DATA :=  ",TERMINAL.ID=":WS.TERMINAL.ID
    OFS.MESSAGE.DATA :=  ",TERMINAL.LOC=":WS.TERMINAL.LOC
    OFS.MESSAGE.DATA :=  ",TERMINAL.TYPE=":WS.TERMINAL.TYPE
    OFS.MESSAGE.DATA :=  ",ACQUIRER.ID=":WS.AQUIR.ID
    OFS.MESSAGE.DATA :=  ",FORWARD.ID=":WS.FORWD.ID
    OFS.MESSAGE.DATA :=  ",ISSUER.RATE=":WS.ISSUE.RATE
    OFS.MESSAGE.DATA :=  ",ATM.MESG.TYPE=":WS.ATM.MSG.TYPE
    OFS.MESSAGE.DATA :=  ",RETRIVAL.REF.NO=":WS.RET.REF.NO
    OFS.MESSAGE.DATA :=  ",ORDERING.BANK=":"ATM"

    IF WS.PCODE[1,2] = '17' OR WS.PCODE[1,2] = '12' THEN
        OFS.MESSAGE.DATA :=  ",COMMISSION.TYPE=":WS.COMM.CODE
    END

    MSG.OFS.FT := OFS.MESSAGE.DATA
    WRITESEQ MSG.OFS.FT TO AA ELSE
        PRINT  'CAN NOT WRITE LINE ':MSG.OFS.FT
    END
    RETURN
*---------------------------------------------------------------------


END
