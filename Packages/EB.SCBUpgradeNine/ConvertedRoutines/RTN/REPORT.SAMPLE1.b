* @ValidationCode : MjotMTIyMDc2ODE4NjpDcDEyNTI6MTY0NDk0MDkwOTE1Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 18:01:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------
*-- CREATE BY AHMED EL-NAHRAWY -----
*-- EDIT BY NESSMA ON 05 / 02 / 2013
*-----------------------------------
SUBROUTINE REPORT.SAMPLE1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE.INPUT
*Line [ 52 ] ADD I_F.USER.SIGN.ON.NAME - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME


*-------------------------------------------------
    COMP = ID.COMPANY
    CUR  = ''
*------------------------------------------------------------------------
    FLG1 = R.NEW(DEPT.SAMP.AMT.HWALA)
    IF FLG1 NE '' THEN

        GOSUB INITIATE
        GOSUB PROCESS

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ���������" ; CALL REM
    END
RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.SAMPLE1'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*-------
    FN.LD    = 'F.SCB.DEPT.SAMPLE1' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.INP   = 'F.SCB.DEPT.SAMPLE.INPUT' ; F.INP = ''
    CALL OPF(FN.INP,F.INP)
*------------------------------------------------------------------------
    DEPT.NO   = R.NEW(DEPT.SAMP.DEPT.NO.HWALA)
    BRANCH.ID = R.NEW(DEPT.SAMP.BRANCH.NO.HWALA)
    SADA1     = R.NEW(DEPT.SAMP.NAMES1.HWALA)
    AMOUNT    = R.NEW(DEPT.SAMP.AMT.HWALA)
    IN.AMOUNT = AMOUNT
    TYPE.HW   = R.NEW(DEPT.SAMP.TYPE.AMT.HWALA)
    CUR.HW    = R.NEW(DEPT.SAMP.CURRENCY.HWALA)
*Line [ 92 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 93 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.HW,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.HW,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

    CUS.HW    = R.NEW(DEPT.SAMP.CUS.HWALA)
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    SUP       = R.NEW(DEPT.SAMP.SUP.HWALA)
    SADA2     = R.NEW(DEPT.SAMP.NAMES2.HWALA)
    SADA2.ACC = R.NEW(DEPT.SAMP.ACC.NAMES2.HWALA)
    PURPOS    = R.NEW(DEPT.SAMP.PURPOSE.HWALA)<1,1>
    PURPOS1   = R.NEW(DEPT.SAMP.PURPOSE.HWALA)<1,2>
    PURPOS2   = R.NEW(DEPT.SAMP.PURPOSE.HWALA)<1,3>

    NOTES     = R.NEW(DEPT.SAMP.NOTES.HWALA)<1,1>
    NOTES1    = R.NEW(DEPT.SAMP.NOTES.HWALA)<1,2>
    NOTES2    = R.NEW(DEPT.SAMP.NOTES.HWALA)<1,3>
    NOTES3    = R.NEW(DEPT.SAMP.NOTES.HWALA)<1,4>
    NOTES4    = R.NEW(DEPT.SAMP.NOTES.HWALA)<1,5>

    REQ.HW    = R.NEW(DEPT.SAMP.REQUEST.DATE.HWALA)
    REP.HW    = R.NEW(DEPT.SAMP.REPLAY.DATE.HWALA)
    REQ.STA   = R.NEW(DEPT.SAMP.REQ.STA.HWALA)
    NOT.MAR   = R.NEW(DEPT.SAMP.NOTES.HWALA.MAR)

*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.NO,DEPT.NAME)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT.NO,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    DEPT.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>


    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
    XX9  = SPACE(132)  ; XX10  = SPACE(132) ; XX12  = SPACE(132)
    XX13 = SPACE(132)  ; XX14  = SPACE(132) ; XX15  = SPACE(132)
    XX16 = SPACE(132)  ; XX17  = SPACE(132) ; XX18  = SPACE(132)
    XX20 = SPACE(132)

    XX<1,1>[3,15]    =  '���  :'
    XX<1,1>[20,15]   =  DEPT.NAME
    XX1<1,1>[15,15]  =  '���  : '
    XX1<1,1>[30,15]  =  BRANCH.ID
    XX2<1,1>[3,15]   =  '������ / '
    XX2<1,1>[30,15]  =  SADA1
    XX3<1,1>[3,30]   =  '�����  ' : AMOUNT
    XX4<1,1>[3,50]   =  '��� ����� : ' :  OUT.AMT
    XX5<1,1>[3,50]   =  '��� ����� :' : TYPE.HW
    XX6<1,1>[3,50]   =  '������ : ' :CUS.HW
    XX7<1,1>[3,50]   =  '������ : ' :CUR.HW
    XX8<1,1>[3,50]   =  '���� : ' :SUP
    XX9<1,1>[3,90]   =  ' ��� ���� ������ / ' : SADA2
    XX10<1,1>[3,40]  =  ' ���� ��� :  '  : SADA2.ACC
    XX11<1,1>[3,80]  =  ' ���� :  ' : PURPOS : '' : PURPOS1 : '' : PURPOS2
    XX12<1,1>[3,40]  =  '����� ������� ������ ����� �����'
    XX13<1,1>[3,40]  =  '������� : ':NOTES: " ":NOTES1
    XX20<1,1>[15,40] =   NOTES2 : " " : NOTES3 : " " : NOTES4
    XX14<1,1>[3,40]  =  '����� ����� : ' : REQ.HW
    XX15<1,1>[3,40]  =  '����� ���� : '  : REP.HW
    XX16<1,1>[3,40]  =  '���� ����� : '  : REQ.STA
    XX17<1,1>[3,40]  =  '������� ������ :  ' : NOT.MAR

    ID    = 'SCB.DEPT.SAMPLE1*':ID.NEW:'...'
    T.SEL = "SELECT F.SCB.DEPT.SAMPLE.INPUT WITH @ID LIKE ": ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR J = 1 TO SELECTED
            CALL F.READ(FN.INP,KEY.LIST<J>,R.INP,F.INP,ERR1)
            INP  = R.INP<SAMP.INPUT>
*Line [ 178 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,INP.DEPT)
            F.ITSS.USER = 'F.USER'
            FN.F.ITSS.USER = ''
            CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
            CALL F.READ(F.ITSS.USER,INP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
            INP.DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
            IF V$FUNCTION EQ 'A' THEN
                AUTH = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 163 ] change EB.USE.USER.ID to EB.USO.USER.ID - ITSS - R21 Upgrade - 28/12/2021
*Line [ 188 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
                F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
                FN.F.ITSS.USER.SIGN.ON.NAME = ''
                CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
                CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
                AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
*Line [ 195 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER':@FM:EB.USE.USER.NAME,AUTHI,AUTH.NAME)
                F.ITSS.USER = 'F.USER'
                FN.F.ITSS.USER = ''
                CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
                CALL F.READ(F.ITSS.USER,AUTHI,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
                AUTH.NAME=R.ITSS.USER<EB.USE.USER.NAME>
                AUTHI = R.INP<SAMP.AUTH>
*Line [ 203 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,AUTHI,DEP2)
                F.ITSS.USER = 'F.USER'
                FN.F.ITSS.USER = ''
                CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
                CALL F.READ(F.ITSS.USER,AUTHI,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
                DEP2=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
                IF DEP2 = 99 THEN
                    AUTH = AUTHI
                END ELSE
                    AUTH = R.INP<SAMP.AUTH>
                END

*Line [ 216 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,AUTH,AUTH.DEPT)
                F.ITSS.USER = 'F.USER'
                FN.F.ITSS.USER = ''
                CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
                CALL F.READ(F.ITSS.USER,AUTH,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
                AUTH.DEPT=R.ITSS.USER<EB.USE.DEPARTMENT.CODE>
            END
            IF ( INP.DEPT EQ 99 AND AUTH.DEPT EQ 99 ) THEN
*Line [ 225 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER':@FM:EB.USE.USER.NAME,INP,INP.NAME)
                F.ITSS.USER = 'F.USER'
                FN.F.ITSS.USER = ''
                CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
                CALL F.READ(F.ITSS.USER,INP,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
                INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
*Line [ 232 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('USER':@FM:EB.USE.USER.NAME,AUTH,AUTH.NAME)
                F.ITSS.USER = 'F.USER'
                FN.F.ITSS.USER = ''
                CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
                CALL F.READ(F.ITSS.USER,AUTH,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
                AUTH.NAME=R.ITSS.USER<EB.USE.USER.NAME>
                XX18<1,1>[3,100]  =  '��� ������� : ' :ID.NEW:"":'���� :' :INP.NAME:"":'����':AUTH.NAME
            END
        NEXT J
    END

*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" KEY1 "
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"��� ������ �������� "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX1<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX13<1,1>
    PRINT STR(' ',82)
    PRINT XX20<1,1>
    PRINT STR(' ',82)
    PRINT XX14<1,1>
    PRINT STR(' ',82)
    PRINT XX15<1,1>
    PRINT STR(' ',82)
    PRINT XX16<1,1>
    PRINT STR(' ',82)
    PRINT XX17<1,1>
    PRINT STR(' ',82)
    PRINT XX18<1,1>
*====================================================*
RETURN
END
