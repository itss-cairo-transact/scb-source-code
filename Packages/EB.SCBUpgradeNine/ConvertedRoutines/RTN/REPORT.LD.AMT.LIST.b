* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*--- CREATED BY NESSMA ON 2017/05/08
    SUBROUTINE REPORT.LD.AMT.LIST

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*----------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB END.PROG
    RETURN
*========================================
INITIATE:
*--------
    REPORT.ID = 'REPORT.LD.AMT.LIST'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    PRINT.ARRAY1 = ""
    PRINT.ARRAY2 = ""
    PRINT.ARRAY3 = ""
    PRINT.ARRAY4 = ""
    PRINT.ARRAY5 = ""
    PRINT.ARRAY6 = ""
    RETURN
*========================================
PRINT.HEAD:
*----------
*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY  = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"REPORT.LD.AMT.LIST"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50) : "������� ����� ������� ������� ������"
    PR.HD :="'L'":SPACE(45):STR('_',50)
    PR.HD :="'L'"
    HEADING PR.HD
    RETURN
*========================================
PROCESS:
*-------
    LD.TOT11 = 0
    LD.TOT12 = 0
    LD.TOT13 = 0
    LD.TOT14 = 0
    LD.TOT15 = 0

    LD.TOT21 = 0
    LD.TOT22 = 0
    LD.TOT23 = 0
    LD.TOT24 = 0
    LD.TOT25 = 0

    LD.TOT31 = 0
    LD.TOT32 = 0
    LD.TOT33 = 0
    LD.TOT34 = 0
    LD.TOT35 = 0

    LD.TOT41 = 0
    LD.TOT42 = 0
    LD.TOT43 = 0
    LD.TOT44 = 0
    LD.TOT45 = 0

    LD.TOT51 = 0
    LD.TOT52 = 0
    LD.TOT53 = 0
    LD.TOT54 = 0
    LD.TOT55 = 0

    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY IN ( 21001 21003 21005 21006 21007 ) "
    T.SEL := " AND CURRENCY EQ EGP"
    T.SEL := " AND AMOUNT GT 0 BY CATEGORY BY AMOUNT"

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM

    IF SELECTED THEN
        FOR HH = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<HH>,R.LD,F.LD,E.LD)
            LD.AMT = R.LD<LD.AMOUNT>
            LD.CAT = R.LD<LD.CATEGORY>

            IF LD.CAT EQ '21001' THEN
                IF LD.AMT GT '0' AND LD.AMT LT '1000000' THEN
                    LD.TOT11 +=  LD.AMT
                END
                IF LD.AMT GE '1000000' AND LD.AMT LE '10000000' THEN
                    LD.TOT12 +=  LD.AMT
                END
                IF LD.AMT GT '10000000' AND LD.AMT LE '20000000' THEN
                    LD.TOT13 +=  LD.AMT
                END
                IF LD.AMT GT '20000000' AND LD.AMT LE '50000000' THEN
                    LD.TOT14 +=  LD.AMT
                END
                IF LD.AMT GT '50000000' THEN
                    LD.TOT15 +=  LD.AMT
                END
            END
*================
            IF LD.CAT EQ '21003' THEN
                IF LD.AMT GT '0' AND LD.AMT LT '1000000' THEN
                    LD.TOT21 +=  LD.AMT
                END
                IF LD.AMT GE '1000000' AND LD.AMT LE '10000000' THEN
                    LD.TOT22 +=  LD.AMT
                END
                IF LD.AMT GT '10000000' AND LD.AMT LE '20000000' THEN
                    LD.TOT23 +=  LD.AMT
                END
                IF LD.AMT GT '20000000' AND LD.AMT LE '50000000' THEN
                    LD.TOT24 +=  LD.AMT
                END
                IF LD.AMT GT '50000000' THEN
                    LD.TOT25 +=  LD.AMT
                END
            END

            IF LD.CAT EQ '21005' THEN
                IF LD.AMT GT '0' AND LD.AMT LT '1000000' THEN
                    LD.TOT31 +=  LD.AMT
                END
                IF LD.AMT GE '1000000' AND LD.AMT LE '10000000' THEN
                    LD.TOT32 +=  LD.AMT
                END
                IF LD.AMT GT '10000000' AND LD.AMT LE '20000000' THEN
                    LD.TOT33 +=  LD.AMT
                END
                IF LD.AMT GT '20000000' AND LD.AMT LE '50000000' THEN
                    LD.TOT34 +=  LD.AMT
                END
                IF LD.AMT GT '50000000' THEN
                    LD.TOT35 +=  LD.AMT
                END
            END

            IF LD.CAT EQ '21006' THEN
                IF LD.AMT GT '0' AND LD.AMT LT '1000000' THEN
                    LD.TOT41 +=  LD.AMT
                END
                IF LD.AMT GE '1000000' AND LD.AMT LE '10000000' THEN
                    LD.TOT42 +=  LD.AMT
                END
                IF LD.AMT GT '10000000' AND LD.AMT LE '20000000' THEN
                    LD.TOT43 +=  LD.AMT
                END
                IF LD.AMT GT '20000000' AND LD.AMT LE '50000000' THEN
                    LD.TOT44 +=  LD.AMT
                END
                IF LD.AMT GT '50000000' THEN
                    LD.TOT45 +=  LD.AMT
                END
            END

            IF LD.CAT EQ '21007' THEN
                IF LD.AMT GT '0' AND LD.AMT LT '1000000' THEN
                    LD.TOT51 +=  LD.AMT
                END
                IF LD.AMT GE '1000000' AND LD.AMT LE '10000000' THEN
                    LD.TOT52 +=  LD.AMT
                END
                IF LD.AMT GT '10000000' AND LD.AMT LE '20000000' THEN
                    LD.TOT53 +=  LD.AMT
                END
                IF LD.AMT GT '20000000' AND LD.AMT LE '50000000' THEN
                    LD.TOT54 +=  LD.AMT
                END
                IF LD.AMT GT '50000000' THEN
                    LD.TOT55 +=  LD.AMT
                END
            END
        NEXT HH
    END
    RETURN
*========================================
END.PROG:
*--------
    TOT="" ; TOT1="" ; TOT2="" ; TOT3="" ; TOT4="" ; TOT5="" ; TOT6=""
    TEXT.ARRAY1 = ""
    TEXT.ARRAY2 = ""
    TEXT.ARRAY3 = ""
    TEXT.ARRAY4 = ""
    TEXT.ARRAY5 = ""
    TEXT.ARRAY6 = ""
    END.LINE    = ""
    CAT.LINE    = ""

    CAT.LINE<1,1>[1,15]  = "�����"
    CAT.LINE<1,1>[15,15] = "���"
    CAT.LINE<1,1>[30,15] = "3 ����"
    CAT.LINE<1,1>[50,15] = "6 ����"
    CAT.LINE<1,1>[65,15] = "���"
    CAT.LINE<1,1>[85,15] = "��������"
    PRINT " "
    PRINT "������ ������� ������" : SPACE(15) : CAT.LINE<1,1>
    PRINT STR('_',100)
    PRINT " "

    TEXT.ARRAY1<1,1>[1,30] = "��� �� �����"
    TEXT.ARRAY2<1,1>[1,30] = "�� ����� � ��� 10 �����"
    TEXT.ARRAY3<1,1>[1,30] = "�� 10 ����� ��� 20 �����"
    TEXT.ARRAY4<1,1>[1,30] = "���� �� 20 ����� ��� 50 �����"
    TEXT.ARRAY5<1,1>[1,30] = "���� �� 50 �����"
    TEXT.ARRAY6<1,1>[1,30] = "��������"

    TOT = LD.TOT11 + LD.TOT21 + LD.TOT31 + LD.TOT41 + LD.TOT51
    TOT6 += TOT
    PRINT.ARRAY1<1,1>[1,15]   = FMT(LD.TOT11,"L0,")
    PRINT.ARRAY1<1,1>[15,15]  = FMT(LD.TOT21,"L0,")
    PRINT.ARRAY1<1,1>[30,15]  = FMT(LD.TOT31,"L0,")
    PRINT.ARRAY1<1,1>[50,15]  = FMT(LD.TOT41,"L0,")
    PRINT.ARRAY1<1,1>[65,15]  = FMT(LD.TOT51,"L0,")
    PRINT.ARRAY1<1,1>[85,15] = FMT(TOT,"L0,")

    TOT = LD.TOT12 + LD.TOT22 + LD.TOT32 + LD.TOT42 + LD.TOT52
    TOT6 += TOT
    PRINT.ARRAY2<1,1>[1,15]   = FMT(LD.TOT12,"L0,")
    PRINT.ARRAY2<1,1>[15,15]  = FMT(LD.TOT22,"L0,")
    PRINT.ARRAY2<1,1>[30,15]  = FMT(LD.TOT32,"L0,")
    PRINT.ARRAY2<1,1>[50,15]  = FMT(LD.TOT42,"L0,")
    PRINT.ARRAY2<1,1>[65,15]  = FMT(LD.TOT52,"L0,")
    PRINT.ARRAY2<1,1>[85,15] = FMT(TOT,"L0,")

    TOT = LD.TOT13 + LD.TOT23 + LD.TOT33 + LD.TOT43 + LD.TOT53
    TOT6 += TOT
    PRINT.ARRAY3<1,1>[1,15]   = FMT(LD.TOT13,"L0,")
    PRINT.ARRAY3<1,1>[15,15]  = FMT(LD.TOT23,"L0,")
    PRINT.ARRAY3<1,1>[30,15]  = FMT(LD.TOT33,"L0,")
    PRINT.ARRAY3<1,1>[50,15]  = FMT(LD.TOT43,"L0,")
    PRINT.ARRAY3<1,1>[65,15]  = FMT(LD.TOT53,"L0,")
    PRINT.ARRAY3<1,1>[85,15] = FMT(TOT,"L0,")

    TOT = LD.TOT14 + LD.TOT24 + LD.TOT34 + LD.TOT44 + LD.TOT54
    TOT6 += TOT
    PRINT.ARRAY4<1,1>[1,15]   = FMT(LD.TOT14,"L0,")
    PRINT.ARRAY4<1,1>[15,15]  = FMT(LD.TOT24,"L0,")
    PRINT.ARRAY4<1,1>[30,15]  = FMT(LD.TOT34,"L0,")
    PRINT.ARRAY4<1,1>[50,15]  = FMT(LD.TOT44,"L0,")
    PRINT.ARRAY4<1,1>[65,15]  = FMT(LD.TOT54,"L0,")
    PRINT.ARRAY4<1,1>[85,15] = FMT(TOT,"L0,")

    TOT = LD.TOT15 + LD.TOT25 + LD.TOT35 + LD.TOT45 + LD.TOT55
    TOT6 += TOT
    PRINT.ARRAY5<1,1>[1,15]   = FMT(LD.TOT15,"L0,")
    PRINT.ARRAY5<1,1>[15,15]  = FMT(LD.TOT25,"L0,")
    PRINT.ARRAY5<1,1>[30,15]  = FMT(LD.TOT35,"L0,")
    PRINT.ARRAY5<1,1>[50,15]  = FMT(LD.TOT45,"L0,")
    PRINT.ARRAY5<1,1>[65,15]  = FMT(LD.TOT55,"L0,")
    PRINT.ARRAY5<1,1>[85,15]  = FMT(TOT,"L0,")

    TOT1 = LD.TOT11 +  LD.TOT12 + LD.TOT13 + LD.TOT14 + LD.TOT15
    TOT2 = LD.TOT21 +  LD.TOT22 + LD.TOT23 + LD.TOT24 + LD.TOT25
    TOT3 = LD.TOT31 +  LD.TOT32 + LD.TOT33 + LD.TOT34 + LD.TOT35
    TOT4 = LD.TOT41 +  LD.TOT42 + LD.TOT43 + LD.TOT44 + LD.TOT45
    TOT5 = LD.TOT51 +  LD.TOT52 + LD.TOT53 + LD.TOT54 + LD.TOT55

    PRINT.ARRAY6<1,1>[1,15]   =  FMT(TOT1,"L0,")
    PRINT.ARRAY6<1,1>[15,15]  =  FMT(TOT2,"L0,")
    PRINT.ARRAY6<1,1>[30,15]  =  FMT(TOT3,"L0,")
    PRINT.ARRAY6<1,1>[50,15]  =  FMT(TOT4,"L0,")
    PRINT.ARRAY6<1,1>[65,15]  =  FMT(TOT5,"L0,")
    PRINT.ARRAY6<1,1>[85,15]  =  FMT(TOT6,"L0,")

    END.LINE<1,1>[1,30]  = TEXT.ARRAY1<1,1>
    END.LINE<1,1>[35,30] = PRINT.ARRAY1<1,1>

    PRINT END.LINE<1,1>
    PRINT " "
    PRINT " "
    END.LINE = ""

    END.LINE<1,1>[1,30]  = TEXT.ARRAY2<1,1>
    END.LINE<1,1>[35,30] = PRINT.ARRAY2<1,1>
    PRINT END.LINE<1,1>
    PRINT " "
    PRINT " "
    END.LINE = ""

    END.LINE<1,1>[1,30]  = TEXT.ARRAY3<1,1>
    END.LINE<1,1>[35,30] = PRINT.ARRAY3<1,1>
    PRINT END.LINE<1,1>
    PRINT " "
    PRINT " "
    END.LINE = ""

    END.LINE<1,1>[1,30]  = TEXT.ARRAY4<1,1>
    END.LINE<1,1>[35,30] = PRINT.ARRAY4<1,1>
    PRINT END.LINE<1,1>
    PRINT " "
    PRINT " "
    END.LINE = ""

    END.LINE<1,1>[1,30]  = TEXT.ARRAY5<1,1>
    END.LINE<1,1>[35,30] = PRINT.ARRAY5<1,1>
    PRINT END.LINE<1,1>
    PRINT " "
    PRINT " "
    END.LINE = ""

    END.LINE<1,1>[1,30]  = TEXT.ARRAY6<1,1>
    END.LINE<1,1>[35,30] = PRINT.ARRAY6<1,1>
    PRINT END.LINE<1,1>
    PRINT " "
    PRINT " "
    END.LINE = ""

    PRINT " "
    PRINT " "
    PRINT SPACE(45):STR('*',15):" ����� ������� ":STR('*',15)

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM
    RETURN
END
