* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
****NESSREEN AHMED 11/7/2010**************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE READ.VISA.DB.ADVICE
**    PROGRAM READ.VISA.DB.ADVICE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE

    TEXT = 'HI' ; CALL REM

**    Path = "/home/visa/NESRO/Suez_All_Card_Details.txt"
    Path = "&SAVEDLISTS&/Suez_All_Card_Details.txt"
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    F.VISA.DB.AD = '' ; FN.VISA.DB.AD = 'F.SCB.READ.DB.ADVICE' ; R.VISA.DB.AD= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.DB.AD,F.VISA.DB.AD)

    EOF = ''
    X = 0
    TEXT = 'X=':X ; CALL REM
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CONTRACT.NO = '' ; CARD.NO = '' ; MBR = '' ; INT.ACCT.NO = '' ; ACCT.TYPE = '' ; EXT.ACCT = ''
            ACCT.CURR = '' ; CARD.F.PROF = '' ; CARD.BR.CODE = '' ; CARD.BR.NAME = '' ; CARD.CURR = '' ; CARD.PRODUCT = ''
            CREATE.DATE = '' ; CANCEL.DATE = '' ; UPDATE.DATE = '' ;  CARD.LIMIT = '' ; MIN.PAY = '' ; PAY.DUE.DATE = ''; LST.ST.DATE = ''

            CONTRACT.NO           = Line[1,25]
            CARD.NO               = Line[27,20]
            MBR                   = Line[48,1]
            INT.ACCT.NO           = Line[50,25]
            ACCT.TYPE             = Line[76,40]
            EXT.ACCT              = Line[117,21]
            ACCT.CURR             = Line[139,3]
            CARD.F.PROF           = Line[143,50]
            CARD.BR.CODE          = Line[194,3]
            CARD.BR.NAME          = Line[198,30]
            CARD.CURR             = Line[229,3]
            CARD.PRODUCT          = Line[233,30]
            CREATE.DATE           = Line[264,8]
            CR.DD = CREATE.DATE[1,2]
            CR.MM = CREATE.DATE[4,2]
            CR.YY = "20":CREATE.DATE[7,2]
            CREATE.DATE.F = CR.YY:CR.MM:CR.DD

            CANCEL.DATE           = Line[273,8]
            CA.DD = CANCEL.DATE[1,2]
            CA.MM = CANCEL.DATE[4,2]
            CA.YY = "20":CANCEL.DATE[7,2]
            CANCEL.DATE.F = CA.YY:CA.MM:CA.DD

            UPDATE.DATE           = Line[282,8]
            UP.DD = UPDATE.DATE[1,2]
            UP.MM = UPDATE.DATE[4,2]
            UP.YY = "20":UPDATE.DATE[7,2]
            UPDATE.DATE.F = UP.YY:UP.MM:UP.DD

            CARD.LIMIT            = Line[291,16]
            MIN.PAY               = Line[308,16]
            PAY.DUE.DATE          = Line[325,8]
            PD.DD = PAY.DUE.DATE[1,2]
            PD.MM = PAY.DUE.DATE[4,2]
            PD.YY = "20":PAY.DUE.DATE[7,2]
            PAY.DUE.DATE.F = PD.YY:PD.MM:PD.DD

            LST.ST.DATE           = Line[334,8]
            LT.DD = LST.ST.DATE[1,2]
            LT.MM = LST.ST.DATE[4,2]
            LT.YY = "20":LST.ST.DATE[7,2]
            LST.ST.DATE.F = LT.YY:LT.MM:LT.DD

            NAME.ON.CARD          = Line[343,30]
            CARD.ONLINE.ST        = Line[374,15]
            CARD.TWCMS.ST         = Line[390,15]
            AMT.ON.HOLD           = Line[406,17]
            CASH.LIMIT            = Line[424,15]
            SALE.LIMIT            = Line[440,15]
            REM.CSH.LIM           = Line[456,15]
            REM.SLE.LIM           = Line[472,15]
            DIRECT.DB.R           = Line[488,3]
            PR.SP.FLAG            = Line[492,1]
            COUNTRY               = Line[494,15]
            REGION                = Line[510,15]
            CITY                  = Line[526,15]
            ZIP.CODE              = Line[542,5]
            CONTRACT.ADD          = Line[548,200]
            EMAIL                 = Line[749,40]
            MOBILE.NO             = Line[790,15]
            PHONE                 = Line[806,15]
            JOB.PHONE             = Line[822,15]
            CONTRACT.ST           = Line[838,10]
            PRIM.CARD             = Line[849,20]
            ACCT.BAL              = Line[870,15]
            CONTRACT.T.NAME       = Line[886,30]

            X = X + 1
**       ID.KEY = CARD.NO:".20100601"
***      ID.KEY = X:".201008"
            ID.KEY = X:".":LST.ST.DATE.F[1,6]
    **       TEXT = 'ID=':ID.KEY ; CALL REM
            CALL F.READ(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD, F.VISA.DB.AD ,E1)

            R.VISA.DB.AD<SCB.DB.CONTRACT.NO>     = TRIM(CONTRACT.NO, " " , "T")
            R.VISA.DB.AD<SCB.DB.CARD.NO>         = TRIM(CARD.NO, " " , "T")
            R.VISA.DB.AD<SCB.DB.MBR>             = MBR
            R.VISA.DB.AD<SCB.DB.INT.ACCT.NO>     = TRIM(INT.ACCT.NO, " " , "T")
            R.VISA.DB.AD<SCB.DB.ACCT.TYPE>       = TRIM(ACCT.TYPE, " " , "T")
            R.VISA.DB.AD<SCB.DB.EXT.ACCT>        = TRIM(EXT.ACCT, " " , "T")
            R.VISA.DB.AD<SCB.DB.ACCT.CURR>       = ACCT.CURR
            R.VISA.DB.AD<SCB.DB.CARD.PROF>       = TRIM(CARD.F.PROF, " " , "T")
            R.VISA.DB.AD<SCB.DB.CARD.BR.CO>      = TRIM(CARD.BR.CODE, " " , "T")
            R.VISA.DB.AD<SCB.DB.CARD.BR.NAME>    = TRIM(CARD.BR.NAME, " " , "T")
            R.VISA.DB.AD<SCB.DB.CARD.CURR>       = CARD.CURR
            R.VISA.DB.AD<SCB.DB.CARD.PRODUCT>    = TRIM(CARD.PRODUCT, " " , "T")
            R.VISA.DB.AD<SCB.DB.CREATE.DATE>     = CREATE.DATE.F
            R.VISA.DB.AD<SCB.DB.CANCEL.DATE>     = CANCEL.DATE.F
            R.VISA.DB.AD<SCB.DB.UPDATE.DATE>     = UPDATE.DATE.F
            R.VISA.DB.AD<SCB.DB.CARD.LIMIT>      = TRIM(CARD.LIMIT, " " , "B")
            R.VISA.DB.AD<SCB.DB.MIN.PAY>         = TRIM(MIN.PAY, " " , "B")
            R.VISA.DB.AD<SCB.DB.PAY.DUE.DATE>    = PAY.DUE.DATE.F
            R.VISA.DB.AD<SCB.DB.LST.STAT.DATE>   = LST.ST.DATE.F
            R.VISA.DB.AD<SCB.DB.NAME.ON.CARD>    = TRIM(NAME.ON.CARD, " " , "T")
            R.VISA.DB.AD<SCB.DB.CARD.ONLINE.ST>  = TRIM(CARD.ONLINE.ST, " " , "T")
            R.VISA.DB.AD<SCB.DB.CARD.TWCMS.ST>   = TRIM(CARD.TWCMS.ST, " " , "T")
            R.VISA.DB.AD<SCB.DB.AMT.ON.HOLD>     = TRIM(AMT.ON.HOLD, " " , "B")
            R.VISA.DB.AD<SCB.DB.CASH.LIMIT>      = TRIM(CASH.LIMIT, " " , "B")
            R.VISA.DB.AD<SCB.DB.SALE.LIMIT>      = TRIM(SALE.LIMIT, " " , "B")
            R.VISA.DB.AD<SCB.DB.REMAIN.CSH.LIM>  = TRIM(REM.CSH.LIM, " " , "B")
            R.VISA.DB.AD<SCB.DB.REMAIN.SLE.LIM>  = TRIM(REM.SLE.LIM, " " , "B")
            R.VISA.DB.AD<SCB.DB.DIRECT.DB.RATE>  = TRIM(DIRECT.DB.R, " " , "T")
            R.VISA.DB.AD<SCB.DB.PR.SP.FLAG>      = PR.SP.FLAG
            R.VISA.DB.AD<SCB.DB.COUNTRY>         = TRIM(COUNTRY, " " , "T")
            R.VISA.DB.AD<SCB.DB.REGION>          = TRIM(REGION, " " , "T")
            R.VISA.DB.AD<SCB.DB.CITY>            = TRIM(CITY, " " , "T")
            R.VISA.DB.AD<SCB.DB.ZIP.CODE>        = TRIM(ZIP.CODE, " " , "T")
            R.VISA.DB.AD<SCB.DB.CON.REG.ADDRESS> = TRIM(CONTRACT.ADD, " " , "T")
            R.VISA.DB.AD<SCB.DB.EMAIL>           = TRIM(EMAIL, " " , "T")
            R.VISA.DB.AD<SCB.DB.MOBILE.NO>       = TRIM(MOBILE.NO, " " , "T")
            R.VISA.DB.AD<SCB.DB.PHONE>           = TRIM(PHONE, " " , "T")
            R.VISA.DB.AD<SCB.DB.JOB.PHONE>       = TRIM(JOB.PHONE, " " , "T")
            R.VISA.DB.AD<SCB.DB.CONTRACT.STAT>   = TRIM(CONTRACT.ST, " " , "T")
            R.VISA.DB.AD<SCB.DB.PRIMARY.CARD>    = TRIM(PRIM.CARD, " " , "T")
            R.VISA.DB.AD<SCB.DB.ACCT.BAL>        = TRIM(ACCT.BAL, " " , "B")
            R.VISA.DB.AD<SCB.DB.CONT.TYPE.NAME>  = TRIM(CONTRACT.T.NAME, " " , "T")

            CALL F.WRITE(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD)
            CALL JOURNAL.UPDATE(ID.KEY)
        END ELSE
            EOF = 1
        END

    REPEAT
    TEXT = '�� ����� ��� ���=' :LST.ST.DATE.F[1,6] ; CALL REM
    CLOSESEQ MyPath
    TEXT = 'END' ; CALL REM
    X = ''
    RETURN
END
