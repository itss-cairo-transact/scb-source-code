* @ValidationCode : MjotODkzOTA1ODg4OkNwMTI1MjoxNjQwNjkzMjUwNzc0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:07:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>4808</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.CUSTOMER1
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
    *Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
    *Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
    *Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COUNTRY
    *Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.LANGUAGE
    *Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER.STATUS
    *Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DEPT.ACCT.OFFICER
    *Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.POSTING.RESTRICT
    *Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.RELATION
    *Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.INDUSTRY
    *Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.SECTOR
    *Line [ 42 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.TARGET
    *Line [ 44 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
    $INSERT           I_CU.LOCAL.REFS
    $INSERT           I_F.SCB.CUS.TITLE
    $INSERT           I_F.SCB.CUS.PROFESSION
    $INSERT           I_F.SCB.CUS.GOVERNORATE
    $INSERT           I_F.SCB.CUS.REGION
    $INSERT           I_F.SCB.CUS.ID.TYPE
    $INSERT           I_F.SCB.CUS.LEGAL.FORM
    $INSERT           I_F.SCB.CUS.TAX.DEPT
*---------------------------------------------------
    GOSUB INITIATE
*Line [ 44 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 57 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    GOSUB BODY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*----------------------------------------------------
INITIATE:
*--------
    REPORT.ID = 'REPORT.CUSTOMER1'
    CALL PRINTER.ON(REPORT.ID,'')
    FN.CU     = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    YTEXT = "Enter the Customer  No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    CALL F.READ(FN.CU,COMI,R.CU,F.CU,E1)
    RETURN
*-----------------------------------------------------
CALLDB:
*-------
    MYMNEMONIC     = R.CU<EB.CUS.MNEMONIC,1>
    MYENSHORTNAME  = R.CU<EB.CUS.SHORT.NAME,1>
    MYARSHORTNAME  = R.CU<EB.CUS.SHORT.NAME,2>
    MYENNAME1      = R.CU<EB.CUS.NAME.1,1>
    MYENNAME2      = R.CU<EB.CUS.NAME.2,1>
    AA             = R.CU<EB.CUS.STREET>
    BB             = R.CU<EB.CUS.TOWN.COUNTRY>
    CC1            = R.CU<EB.CUS.RELATION.CODE>

*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('RELATION':@FM:EB.REL.DESCRIPTION,CC1,CC)
F.ITSS.RELATION = 'F.RELATION'
FN.F.ITSS.RELATION = ''
CALL OPF(F.ITSS.RELATION,FN.F.ITSS.RELATION)
CALL F.READ(F.ITSS.RELATION,CC1,R.ITSS.RELATION,FN.F.ITSS.RELATION,ERROR.RELATION)
CC=R.ITSS.RELATION<EB.REL.DESCRIPTION>
    DD1 = R.CU<EB.CUS.REL.CUSTOMER>
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,DD1,DD)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,DD1,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
DD=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>
    EE  = R.CU<EB.CUS.REVERS.REL.CODE>

    FF1 = R.CU<EB.CUS.SECTOR>
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SECTOR':@FM:EB.SEC.DESCRIPTION,FF1,FF)
F.ITSS.SECTOR = 'F.SECTOR'
FN.F.ITSS.SECTOR = ''
CALL OPF(F.ITSS.SECTOR,FN.F.ITSS.SECTOR)
CALL F.READ(F.ITSS.SECTOR,FF1,R.ITSS.SECTOR,FN.F.ITSS.SECTOR,ERROR.SECTOR)
FF=R.ITSS.SECTOR<EB.SEC.DESCRIPTION>

*MYBRANCH= �����
*   MYBRANCH1   = R.CU<EB.CUS.ACCOUNT.OFFICER>
    BR.N        = R.CU<EB.CUS.COMPANY.BOOK>[2]
    MYBRANCH1   = TRIM(BR.N,"0","L")

*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,MYBRANCH1,MYBRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,MYBRANCH1,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
MYBRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    MYBRANCH  = FIELD(MYBRANCH,'.',2)

*HH= ����� �����
    HH1=R.CU<EB.CUS.OTHER.OFFICER>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,HH1,HH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,HH1,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
HH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

*II= �������
    II1=R.CU<EB.CUS.INDUSTRY>
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('INDUSTRY':@FM:EB.IND.DESCRIPTION,II1,II)
F.ITSS.INDUSTRY = 'F.INDUSTRY'
FN.F.ITSS.INDUSTRY = ''
CALL OPF(F.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY)
CALL F.READ(F.ITSS.INDUSTRY,II1,R.ITSS.INDUSTRY,FN.F.ITSS.INDUSTRY,ERROR.INDUSTRY)
II=R.ITSS.INDUSTRY<EB.IND.DESCRIPTION>

*JJ= ��� �������
    JJ1=R.CU<EB.CUS.TARGET>
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('TARGET':@FM:EB.TAR.DESCRIPTION,JJ1,JJ)
F.ITSS.TARGET = 'F.TARGET'
FN.F.ITSS.TARGET = ''
CALL OPF(F.ITSS.TARGET,FN.F.ITSS.TARGET)
CALL F.READ(F.ITSS.TARGET,JJ1,R.ITSS.TARGET,FN.F.ITSS.TARGET,ERROR.TARGET)
JJ=R.ITSS.TARGET<EB.TAR.DESCRIPTION>

*NESS= ����� ��������
    LOCAL.REF = R.CU<EB.CUS.LOCAL.REF>
    NESS1=LOCAL.REF<1,CULR.LEGAL.FORM>
*Line [ 162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.LEGAL.FORM':@FM:LEG.DESCRIPTION,NESS1,NESS)
F.ITSS.SCB.CUS.LEGAL.FORM = 'F.SCB.CUS.LEGAL.FORM'
FN.F.ITSS.SCB.CUS.LEGAL.FORM = ''
CALL OPF(F.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM)
CALL F.READ(F.ITSS.SCB.CUS.LEGAL.FORM,NESS1,R.ITSS.SCB.CUS.LEGAL.FORM,FN.F.ITSS.SCB.CUS.LEGAL.FORM,ERROR.SCB.CUS.LEGAL.FORM)
NESS=R.ITSS.SCB.CUS.LEGAL.FORM<LEG.DESCRIPTION>

*MYNATIONALITY= �������
    MYNATIONALITY1 = R.CU<EB.CUS.NATIONALITY>
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,MYNATIONALITY1,MYNATIONALITY)
F.ITSS.COUNTRY = 'F.COUNTRY'
FN.F.ITSS.COUNTRY = ''
CALL OPF(F.ITSS.COUNTRY,FN.F.ITSS.COUNTRY)
CALL F.READ(F.ITSS.COUNTRY,MYNATIONALITY1,R.ITSS.COUNTRY,FN.F.ITSS.COUNTRY,ERROR.COUNTRY)
MYNATIONALITY=R.ITSS.COUNTRY<EB.COU.COUNTRY.NAME>

*MM= �������
    MM1 = R.CU<EB.CUS.RESIDENCE>
*Line [ 182 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COUNTRY':@FM:EB.COU.SHORT.NAME,MM1,MM)
F.ITSS.COUNTRY = 'F.COUNTRY'
FN.F.ITSS.COUNTRY = ''
CALL OPF(F.ITSS.COUNTRY,FN.F.ITSS.COUNTRY)
CALL F.READ(F.ITSS.COUNTRY,MM1,R.ITSS.COUNTRY,FN.F.ITSS.COUNTRY,ERROR.COUNTRY)
MM=R.ITSS.COUNTRY<EB.COU.SHORT.NAME>

*����� �������
    NN1 = R.CU<EB.CUS.CONTACT.DATE>
    IF NN1 THEN NN = NN1[7,2]:'/':NN1[5,2]:"/":NN1[0,4]

*���� ������ �����
    OO = R.CU<EB.CUS.INTRODUCER>

* ����
    PP = R.CU<EB.CUS.TEXT>
    QQ = R.CU<EB.CUS.LEGAL.ID>
    RR = R.CU<EB.CUS.REVIEW.FREQUENCY>

* ����� �������
    MYDATE = R.CU<EB.CUS.BIRTH.INCORP.DATE>
    IF MYDATE THEN XX = MYDATE[0,4]:'/':MYDATE[5,2]:"/":MYDATE[7,2]
    PPP    = R.CU<EB.CUS.GLOBAL.CUSTOMER>

    MYLIABLITY1 = R.CU<EB.CUS.CUSTOMER.LIABILITY>
*Line [ 208 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.SHORT.NAME,MYLIABLITY1,MYLIABLITY)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,MYLIABLITY1,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
MYLIABLITY=R.ITSS.CUSTOMER<EB.CUS.SHORT.NAME>

    MYLANGUAGE1 = R.CU<EB.CUS.LANGUAGE>
*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LANGUAGE':@FM:EB.LAN.DESCRIPTION,MYLANGUAGE1,MYLANGUAGE)
F.ITSS.LANGUAGE = 'F.LANGUAGE'
FN.F.ITSS.LANGUAGE = ''
CALL OPF(F.ITSS.LANGUAGE,FN.F.ITSS.LANGUAGE)
CALL F.READ(F.ITSS.LANGUAGE,MYLANGUAGE1,R.ITSS.LANGUAGE,FN.F.ITSS.LANGUAGE,ERROR.LANGUAGE)
MYLANGUAGE=R.ITSS.LANGUAGE<EB.LAN.DESCRIPTION>

    SS1 = R.CU<EB.CUS.POSTING.RESTRICT>
*Line [ 226 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,SS1,SS)
F.ITSS.POSTING.RESTRICT = 'F.POSTING.RESTRICT'
FN.F.ITSS.POSTING.RESTRICT = ''
CALL OPF(F.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT)
CALL F.READ(F.ITSS.POSTING.RESTRICT,SS1,R.ITSS.POSTING.RESTRICT,FN.F.ITSS.POSTING.RESTRICT,ERROR.POSTING.RESTRICT)
SS=R.ITSS.POSTING.RESTRICT<AC.POS.DESCRIPTION>

    TT  = R.CU<EB.CUS.DISPO.OFFICER>
    UU  = R.CU<EB.CUS.POST.CODE>
    VV  = R.CU<EB.CUS.COUNTRY>
    WW  = ""
    XXX = R.CU<EB.CUS.CONFID.TXT>
    YY  = R.CU<EB.CUS.DISPO.EXEMPT>
    ZZ  = R.CU<EB.CUS.ISSUE.CHEQUES>

    LOCAL.REF = R.CU<EB.CUS.LOCAL.REF>
    MYARNAME1 = LOCAL.REF<1,CULR.ARABIC.NAME><1,1>
    MYARNAME2 = LOCAL.REF<1,CULR.ARABIC.NAME.2><1,1>
    ADDRESS1  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    ADDRESS2  = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
    ADDRESS3  = ADDRESS1 : " " : ADDRESS2

    SIGN      = LOCAL.REF<1,CULR.SIGNATURE><1,1>
    SIGN      = FIELD(SIGN,'-',2)
    GENDER    = LOCAL.REF<1,CULR.GENDER><1,1>
    GENDER    = FIELD(GENDER,'-',2)

    MYMARITALSTATUS = LOCAL.REF<1,CULR.MARITAL.STATUS><1,1>
    EDUCATION       = LOCAL.REF<1,CULR.EDUCATION><1,1>
    EDUCATION       = FIELD(EDUCATION,'-',2)
    INCOME          = LOCAL.REF<1,CULR.INCOME><1,1>
    PROFESSION1     = LOCAL.REF<1,CULR.PROFESSION><1,1>

*Line [ 260 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,PROFESSION1,PROFESSION)
F.ITSS.SCB.CUS.PROFESSION = 'F.SCB.CUS.PROFESSION'
FN.F.ITSS.SCB.CUS.PROFESSION = ''
CALL OPF(F.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION)
CALL F.READ(F.ITSS.SCB.CUS.PROFESSION,PROFESSION1,R.ITSS.SCB.CUS.PROFESSION,FN.F.ITSS.SCB.CUS.PROFESSION,ERROR.SCB.CUS.PROFESSION)
PROFESSION=R.ITSS.SCB.CUS.PROFESSION<SCB.PRF.DESCRIPTION>
    ACCOMTYPE         = LOCAL.REF<1,CULR.ACCOM.TYPE><1,1>
    ACCOMTYPE         = FIELD(ACCOMTYPE,'-',2)
    ACCOMLEGAL        = LOCAL.REF<1,CULR.ACCOM.LEGALITY><1,1>
    ACCOMLEGAL        = FIELD(ACCOMLEGAL,'-',2)
*   LOCALPROXY        = LOCAL.REF<1,CULR.PROXY><1,1>
    LOCALPROXY        = II
    LOCALONTHERINCOME = ""
*   LOCALONTHERINCOME = LOCAL.REF<1,CULR.SP.CONDITION><1,1>
    TITLE1            = LOCAL.REF<1,CULR.TITLE><1,1>

*Line [ 277 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,TITLE1,TITLE)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TITLE1,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
    LOCALGOV11   = LOCAL.REF<1,CULR.GOVERNORATE><1,1>

*Line [ 286 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,LOCALGOV11,LOCALGOV)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,LOCALGOV11,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
LOCALGOV=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
    LOCALREGION1 = LOCAL.REF<1,CULR.REGION><1,1>

*Line [ 295 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.REGION':@FM:REG.DESCRIPTION,LOCALREGION1,LOCALREGION)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,LOCALREGION1,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
LOCALREGION=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
    LOCALADDRESS     = LOCAL.REF<1,CULR.ARABIC.ADDRESS><1,1>
    LOCALLEGALREP    = LOCAL.REF<1,CULR.LEGAL.REP><1,1>
    LOCALLEGALREPEXP = LOCAL.REF<1,CULR.LEG.REP.EXP.D><1,1>
    LOCALFAX         = LOCAL.REF<1,CULR.FAX><1,1>
    LOCALTELEX       = LOCAL.REF<1,CULR.TELEX><1,1>
    LOCALMAIL        = LOCAL.REF<1,CULR.EMAIL.ADDRESS><1,1>
    LOCALIDNO        = LOCAL.REF<1,CULR.NSN.NO><1,1>
    LOCALIDTYPE1     = LOCAL.REF<1,CULR.ID.TYPE><1,1>

*Line [ 311 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.ID.TYPE':@FM:ID.TYPE.DESCRIPTION,LOCALIDTYPE1,LOCALIDTYPE)
F.ITSS.SCB.CUS.ID.TYPE = 'F.SCB.CUS.ID.TYPE'
FN.F.ITSS.SCB.CUS.ID.TYPE = ''
CALL OPF(F.ITSS.SCB.CUS.ID.TYPE,FN.F.ITSS.SCB.CUS.ID.TYPE)
CALL F.READ(F.ITSS.SCB.CUS.ID.TYPE,LOCALIDTYPE1,R.ITSS.SCB.CUS.ID.TYPE,FN.F.ITSS.SCB.CUS.ID.TYPE,ERROR.SCB.CUS.ID.TYPE)
LOCALIDTYPE=R.ITSS.SCB.CUS.ID.TYPE<ID.TYPE.DESCRIPTION>
    LOCALIDNUMBER    = LOCAL.REF<1,CULR.ID.NUMBER><1,1>
    LOCALIDDATE1     = LOCAL.REF<1,CULR.ID.ISSUE.DATE><1,1>

    IF LOCALIDDATE1 THEN LOCALIDDATE= LOCALIDDATE1[7,2]:'/':LOCALIDDATE1[5,2]:"/":LOCALIDDATE1[0,4]
    LOCALIDPLACE     = LOCAL.REF<1,CULR.PLACE.ID.ISSUE><1,1>
    LOCALIDEXPIRY1   = LOCAL.REF<1,CULR.ID.EXPIRY.DATE><1,1>

    IF LOCALIDEXPIRY1 THEN LOCALIDEXPIRY =LOCALIDEXPIRY1[7,2]:'/':LOCALIDEXPIRY1[5,2]:"/":LOCALIDEXPIRY1[0,4]
    LOCALREFBANK     = LOCAL.REF<1,CULR.REFERENCE.BANK><1,1>
    LOCALCONTACTNAME = LOCAL.REF<1,CULR.CONTACT.NAMES><1,1>
    LOCALBROKERTYPE  = LOCAL.REF<1,CULR.BROKER.TYPE><1,1>

    QQ = R.CU<EB.CUS.LEGAL.ID>
    OO = R.CU<EB.CUS.INTRODUCER>
    PP = R.CU<EB.CUS.TEXT>
    LOCALOLDID = LOCAL.REF<1,CULR.OLD.CUST.ID><1,1>

    FFF    = R.CU<EB.CUS.OVERRIDE>
    GGG    = R.CU<EB.CUS.RECORD.STATUS>
    HHH    = R.CU<EB.CUS.CURR.NO>
    III    = R.CU<EB.CUS.INPUTTER>
    III    = FIELD(III,"SCB.",2)
    III    = FIELD(III,"_",1)
    INP.NAME  = ""
    USER.ID   = "SCB.":III
*Line [ 343 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER':@FM:EB.USE.USER.NAME, USER.ID ,INP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,USER.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INP.NAME=R.ITSS.USER<EB.USE.USER.NAME>

    JJJ    = R.CU<EB.CUS.DATE.TIME>[1,10]
    JJJ    = JJJ[5,2]:"/":JJJ[3,2]:"/":"20":JJJ[1,2]: " --- " : JJJ[7,2] : ":" : JJJ[9,2]

    KKK    = R.CU<EB.CUS.AUTHORISER>
    LLL    = R.CU<EB.CUS.CO.CODE>
    MMM    = R.CU<EB.CUS.DEPT.CODE>
    NNN    = R.CU<EB.CUS.AUDITOR.CODE>
    OOO    = R.CU<EB.CUS.AUDIT.DATE.TIME>
    MYDATE = R.CU<EB.CUS.BIRTH.INCORP.DATE>

    IF MYDATE THEN XX = MYDATE[0,4]:'/':MYDATE[5,2]:"/":MYDATE[7,2]
    MYSTATUS1 = R.CU<EB.CUS.CUSTOMER.STATUS>

*Line [ 364 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER.STATUS':@FM:EB.CST.DESCRIPTION,MYSTATUS1,MYSTATUS)
F.ITSS.CUSTOMER.STATUS = 'F.CUSTOMER.STATUS'
FN.F.ITSS.CUSTOMER.STATUS = ''
CALL OPF(F.ITSS.CUSTOMER.STATUS,FN.F.ITSS.CUSTOMER.STATUS)
CALL F.READ(F.ITSS.CUSTOMER.STATUS,MYSTATUS1,R.ITSS.CUSTOMER.STATUS,FN.F.ITSS.CUSTOMER.STATUS,ERROR.CUSTOMER.STATUS)
MYSTATUS=R.ITSS.CUSTOMER.STATUS<EB.CST.DESCRIPTION>
    TAXDEPT1  = LOCAL.REF<1,CULR.TAX.DEPARTMENT>

*Line [ 373 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CUS.TAX.DEPT':@FM:TAX..DESCRIPTION,TAXDEPT1,TAXDEPT)
F.ITSS.SCB.CUS.TAX.DEPT = 'F.SCB.CUS.TAX.DEPT'
FN.F.ITSS.SCB.CUS.TAX.DEPT = ''
CALL OPF(F.ITSS.SCB.CUS.TAX.DEPT,FN.F.ITSS.SCB.CUS.TAX.DEPT)
CALL F.READ(F.ITSS.SCB.CUS.TAX.DEPT,TAXDEPT1,R.ITSS.SCB.CUS.TAX.DEPT,FN.F.ITSS.SCB.CUS.TAX.DEPT,ERROR.SCB.CUS.TAX.DEPT)
TAXDEPT=R.ITSS.SCB.CUS.TAX.DEPT<TAX..DESCRIPTION>
    RETURN
*-----------------------------------------------------------
PRINT.HEAD:
*----------
    YYBRN = MYBRANCH
    DATY  = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[0,4]

    PR.HD  = SPACE(1):" ��� ���� ������"  : SPACE(40):"��� :" :YYBRN
    PR.HD := "'L'":SPACE(1):" ������� : ":T.DAY:SPACE(35):"��� ������ : ":"'P'"
    PR.HD := "'L'":SPACE(1):"REPORT.CUSTOMER1"
    SAM = LEN(COMI)
    IF SAM = 7 THEN
        CUS.CLASS    = COMI[2,1]
        IF CUS.CLASS = 1 THEN PR.HD :="'L'":SPACE(25):"������ ���� **������ **"
        IF CUS.CLASS = 2 THEN PR.HD :="'L'":SPACE(25):"������ ���� **����� **"
        IF CUS.CLASS = 3 THEN PR.HD :="'L'":SPACE(25):"������ ���� **����� **"
        IF CUS.CLASS = 4 THEN PR.HD :="'L'":SPACE(25):"������ ���� **���� **"
        IF CUS.CLASS = 5 THEN PR.HD :="'L'":SPACE(25):"������ ���� *������� �"
        IF CUS.CLASS = 6 THEN PR.HD :="'L'":SPACE(25):"������ ���� *������� **"
        PR.HD :="'L'":SPACE(25):STR('_',22)
        HEADING PR.HD
    END

    IF SAM = 8 THEN
        CUS.CLASS1    = COMI[3,1]
        IF CUS.CLASS1 = 1 THEN PR.HD :="'L'":SPACE(25):"������ ���� **������ **"
        IF CUS.CLASS1 = 2 THEN PR.HD :="'L'":SPACE(25):"������ ���� **����� **"
        IF CUS.CLASS1 = 3 THEN PR.HD :="'L'":SPACE(25):"������ ���� **����� **"
        IF CUS.CLASS1 = 4 THEN PR.HD :="'L'":SPACE(25):"������ ���� **���� **"
        IF CUS.CLASS1 = 5 THEN PR.HD :="'L'":SPACE(25):"������ ���� *������� �"
        IF CUS.CLASS1 = 6 THEN PR.HD :="'L'":SPACE(25):"������ ���� *������� **"
        PR.HD :="'L'":SPACE(25):STR('_',22)
        HEADING PR.HD
    END
    RETURN
*----------------------------------------------------------------------
BODY:
*----
    PRINT
    PRINT SPACE(5):"**�������� ��������:"
    PRINT SPACE(5):"____________________"
    PRINT

    IF COMI THEN
        PRINT SPACE(7)             :"��� ����������������      :":COMI
        PRINT
    END

    IF MYENSHORTNAME THEN
        PRINT SPACE(7)             :"��������� ������� �����   :":MYENSHORTNAME
        PRINT
    END

    IF MYARSHORTNAME THEN
        PRINT SPACE(7)             :"��������� ������� ����    :":MYARSHORTNAME
        PRINT
    END

    IF TITLE THEN
        PRINT SPACE(7)         :"��������������������      :":TITLE
        PRINT
    END

    IF MYENNAME1 THEN
        PRINT SPACE(7)         :"��������� ������ �����    :":MYENNAME1
        PRINT
    END

    IF MYARNAME1 THEN
        PRINT SPACE(7)         :"��������� ������ ����     :":MYARNAME1
        PRINT
    END

    IF XX THEN
        PRINT SPACE(7)         :"���������� ����������     :":XX
        PRINT
    END

    IF MYNATIONALITY THEN
        PRINT SPACE(7)         :"���������������������     :":MYNATIONALITY
        PRINT
    END

    IF LOCAL.REF<1,CULR.TAX.EXEMPTION> THEN
        PRINT SPACE(7)         :"��� �������               :":LOCAL.REF<1,CULR.TAX.EXEMPTION>
        PRINT
    END

    IF ADDRESS3 THEN
        PRINT SPACE(7) : "����� ������ : " : ADDRESS3
        PRINT
    END

    IF MYSTATUS THEN
        PRINT SPACE(7)         :"���������� ���������      :":MYSTATUS
        PRINT
    END

    IF MYLIABLITY THEN
        PRINT SPACE(7)         :"������� �� ����������     :":MYLIABLITY
        PRINT
    END

    IF GENDER THEN
        PRINT SPACE(7)         :"����������/ ��������      :":GENDER
        PRINT
    END

    IF MYMARITALSTATUS THEN
        PRINT SPACE(7)         :"������ ���������������    :":MYMARITALSTATUS
        PRINT
    END

    IF EDUCATION THEN
        PRINT SPACE(7)         :"������ ��������������     :":EDUCATION
        PRINT
    END

    IF INCOME THEN
        PRINT SPACE(7)         :"������� ������������      :":INCOME
        PRINT
    END

    IF PROFESSION THEN
        PRINT SPACE(7)         :"���������������������     :":PROFESSION
        PRINT
    END

    IF LOCALPROXY THEN
        PRINT SPACE(7)         :"����� ��������������      :":LOCALPROXY
        PRINT
    END

    IF LOCALONTHERINCOME THEN
*PRINT SPACE(7)         :"����� ��� ���������       :":LOCALONTHERINCOME
*PRINT
    END

    IF ACCOMTYPE THEN
        PRINT SPACE(7)         :"����� ��������������      :":ACCOMTYPE
        PRINT
    END

    IF ACCOMLEGAL THEN
        PRINT SPACE(7)         :"����� �������� �����      :":ACCOMLEGAL
        PRINT
    END

    IF II THEN
        PRINT SPACE(7)         :"����� ������              :":II
        PRINT
    END

    IF FF THEN
        PRINT SPACE(7)         :"������                    :":FF
        PRINT
    END

    IF NESS THEN
        PRINT SPACE(7)         :"����� ��������            :":NESS
    END

    PRINT SPACE(7):STR('_',60)

    PRINT
    PRINT SPACE(5):"**�������-��� �������:"
    PRINT SPACE(7):"______________________"
    PRINT

    IF MM THEN
        PRINT SPACE(7)         :"�������                   :":MM
        PRINT
    END

    IF BB THEN
        PRINT SPACE(7)         :"�����                     :":BB
        PRINT
    END

    IF LOCALGOV THEN
        PRINT SPACE(7)         :"��� ��������              :":LOCALGOV
        PRINT
    END

    IF LOCALREGION THEN
        PRINT SPACE(7)         :"��� � ��� �������         :":LOCALREGION
        PRINT
    END

    ADD1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS><1,1>
    IF ADD1 THEN
        PRINT SPACE(7)     :"��� ��� ������� � ��� ������   ":ADD1
        PRINT
    END

    IF LOCAL.REF<1,CULR.TELEPHONE> NE '' THEN
*Line [ 438 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        YYY = DCOUNT(LOCAL.REF<1,CULR.TELEPHONE>,@SM)
        FOR  Z = 1 TO YYY
            PRINT SPACE(7)     :"��� ������                :":LOCAL.REF<1,CULR.TELEPHONE><1,Z>
            PRINT
        NEXT Z
    END

    IF LOCAL.REF<1,CULR.FAX> NE '' THEN
*Line [ 447 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTFAX  = DCOUNT(LOCAL.REF<1,CULR.FAX>,@SM)
        FOR  NUMBERFAX = 1 TO  DCOUNTFAX
            PRINT SPACE(7)     :"��� ������                :":LOCAL.REF<1,CULR.FAX><1,NUMBERFAX>
            PRINT
        NEXT NUMBERFAX
    END

    IF LOCAL.REF<1,CULR.TELEX> NE '' THEN
*Line [ 456 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTTELEX = DCOUNT(LOCAL.REF<1,CULR.TELEX>,@SM)
        FOR  NUMBERTELEX = 1 TO DCOUNTTELEX
            PRINT SPACE(7)     :"��� ������                :":LOCAL.REF<1,CULR.TELEX><1,NUMBERTELEX>
            PRINT
        NEXT NUMBERTELEX
    END

    IF LOCAL.REF<1,CULR.EMAIL.ADDRESS> NE '' THEN
*Line [ 465 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTMAIL = DCOUNT(LOCAL.REF<1,CULR.EMAIL.ADDRESS>,@SM)
        FOR  NUMBERMAIL = 1 TO DCOUNTMAIL
            PRINT SPACE(7)     :"������ ����������         :":LOCAL.REF<1,CULR.EMAIL.ADDRESS><1,NUMBERMAIL>
        NEXT NUMBERMAIL
    END

    IF LOCALIDNO NE '' OR LOCALIDTYPE NE '' THEN
        PRINT SPACE(7):STR('_',60)
        PRINT
        PRINT SPACE(5):"**����� ����� �������:"
        PRINT SPACE(7):"______________________"
        PRINT

        IF LOCALIDNO THEN
            PRINT SPACE(7)     :"����� ������              :":LOCALIDNO
            PRINT
        END

        IF LOCALIDTYPE THEN
            PRINT SPACE(7)     :"��� �������               :":LOCALIDTYPE
            PRINT
        END

        IF LOCALIDNUMBER THEN
            PRINT SPACE(7)     :"��� �������               :":LOCALIDNUMBER
            PRINT
        END

        IF LOCALIDDATE THEN
            PRINT SPACE(7)     :"����� �������             :":LOCALIDDATE
            PRINT
        END

        IF LOCALIDPLACE THEN
            PRINT SPACE(7)     :"���� �������              :":LOCALIDPLACE
            PRINT
        END

        IF LOCALIDEXPIRY THEN
            PRINT SPACE(7)     :"����� ��������            :":LOCALIDEXPIRY
        END
    END

    IF R.CU<EB.CUS.RELATION.CODE> NE '' THEN
        PRINT SPACE(7):STR('_',60)
        PRINT
        PRINT SPACE(5):"**�����������������:"
        PRINT SPACE(7):"____________________"
        PRINT

*Line [ 516 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTRELCODE = DCOUNT(R.CU<EB.CUS.RELATION.CODE>,@VM)
        FOR NUMBERRELCODE = 1 TO DCOUNTRELCODE
            CC1=R.CU<EB.CUS.RELATION.CODE,NUMBERRELCODE>
*Line [ 659 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('RELATION':@FM:EB.REL.DESCRIPTION,CC1,CC)
F.ITSS.RELATION = 'F.RELATION'
FN.F.ITSS.RELATION = ''
CALL OPF(F.ITSS.RELATION,FN.F.ITSS.RELATION)
CALL F.READ(F.ITSS.RELATION,CC1,R.ITSS.RELATION,FN.F.ITSS.RELATION,ERROR.RELATION)
CC=R.ITSS.RELATION<EB.REL.DESCRIPTION>
            PRINT SPACE(7)     :" ��� �������              :":CC

            IF R.CU<EB.CUS.REL.CUSTOMER> NE '' THEN
                DD1=R.CU<EB.CUS.REL.CUSTOMER,NUMBERRELCODE>
                PRINT SPACE(7) :" ������ ������ ��         :":DD1
            END
        NEXT NUMBERRELCODE
    END

    IF LOCAL.REF<1,CULR.LEGAL.REP> NE '' THEN
*Line [ 531 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        DCOUNTLEGREP = DCOUNT (LOCAL.REF<1,CULR.LEGAL.REP>,@SM)
        FOR  NUMBERLEGREP = 1 TO DCOUNTLEGREP
            PRINT SPACE(7)     :" ����������� �������      :": LOCAL.REF<1,CULR.LEGAL.REP><1,NUMBERLEGREP>

            IF LOCALLEGALREPEXP=LOCAL.REF<1,CULR.LEG.REP.EXP.D> NE '' THEN
                TATO1 = LOCAL.REF<1,CULR.LEG.REP.EXP.D><1,NUMBERLEGREP>
                TATO  = TATO1[7,2]:'/':TATO1[5,2]:"/":TATO1[0,4]
                PRINT SPACE(7) :"����� ������ �������      :":TATO
            END

        NEXT NUMBERLEGREP
    END

    SAMSAM    = LEN(COMI)
    IF SAMSAM = 7 THEN
        CUS.CLASS    = COMI[2,1]
        IF CUS.CLASS = 3 THEN
            IF LOCAL.REF<1,CULR.COM.REG.NO> THEN
                PRINT SPACE(7):STR('_',60)
                PRINT
                PRINT SPACE(5):"**��������:"
                PRINT SPACE(7):"___________"
                PRINT

                IF LOCAL.REF<1,CULR.COM.REG.NO> THEN
                    PRINT SPACE(7) :"��� ����� ������ �������  :":LOCAL.REF<1,CULR.COM.REG.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.COM.REG.EXP.D> THEN
                    PRINT SPACE(7) :"����� �����               :":LOCAL.REF<1,CULR.COM.REG.EXP.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.EXP.DATE> THEN
                    PRINT SPACE(7) :"����� ������ �����        :":LOCAL.REF<1,CULR.LIC.EXP.DATE>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LEGAL.STATUS> THEN
                    PRINT SPACE(7) :"��� �������               :":LOCAL.REF<1,CULR.LEGAL.STATUS>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.EXEMPTION> THEN
                    PRINT SPACE(7) :"��� ���� �������          :":LOCAL.REF<1,CULR.TAX.EXEMPTION>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.ISSUE.DATE> THEN
                    PRINT SPACE(7) :"����� ���� �� ������� �������:":LOCAL.REF<1,CULR.LIC.ISSUE.DATE>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.IMP.LIC.NO> THEN
                    PRINT SPACE(7) :"��� ������� �����������   :":LOCAL.REF<1,CULR.IMP.LIC.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.IMP.LIC.END.D> THEN
                    PRINT SPACE(7) :"����� �������             :":LOCAL.REF<1,CULR.IMP.LIC.END.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.EXP.LIC.NO> THEN
                    PRINT SPACE(7) :"��� ������� ���������     :":LOCAL.REF<1,CULR.EXP.LIC.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.EXP.LIC.END.D> THEN
                    PRINT SPACE(7) :"����� �������             :":LOCAL.REF<1,CULR.EXP.LIC.END.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.IND.NO> THEN
                    PRINT SPACE(7) :"��� ������� �������/������ ������:":LOCAL.REF<1,CULR.LIC.IND.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.IND.EXP.D> THEN
                    PRINT SPACE(7) :"����� ����� �������       :":LOCAL.REF<1,CULR.LIC.IND.EXP.D>
                END
            END

            IF LOCAL.REF<1,CULR.TAX.NO> THEN
                PRINT SPACE(7):STR('_',60)
                PRINT
                PRINT SPACE(5):"**������� - ���������:"
                PRINT SPACE(7):"______________________"
                PRINT

                IF LOCAL.REF<1,CULR.TAX.NO> THEN
                    PRINT SPACE(7) :"��� ������� ��������      :":LOCAL.REF<1,CULR.TAX.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.FILE.NO> THEN
                    PRINT SPACE(7) :"��� ����� �������         :":LOCAL.REF<1,CULR.TAX.FILE.NO>
                    PRINT
                END

                IF TAXDEPT THEN
                    PRINT SPACE(7) :"������� �������           :":TAXDEPT
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.EXEMP.ST.D> THEN
                    PRINT SPACE(7) :"����� ����� �������       :":LOCAL.REF<1,CULR.TAX.EXEMP.ST.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.EXEMP.END.D> THEN
                    PRINT SPACE(7) :"����� ������ �������      :":LOCAL.REF<1,CULR.TAX.EXEMP.END.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.SOCIAL.INSUR.NO> THEN
                    PRINT SPACE(7) :"��� ��� ���������         :":LOCAL.REF<1,CULR.SOCIAL.INSUR.NO>
                    PRINT
                END
                DCOUNTTAX = 0
                FOR  NUMBERTAX = 1 TO DCOUNTTAX

                    IF LOCAL.REF<1,CULR.TAX.STATUS> THEN
                        PRINT SPACE(7):"������ �������            :":LOCAL.REF<1,CULR.TAX.STATUS><1,NUMBERTAX>
                        PRINT
                    END

                NEXT   NUMBERTAX

*                IF LOCAL.REF<1,CULR.LEG.REP.DOC> THEN
*                PRINT SPACE(7):"������ ��������           :":LOCAL.REF<1,CULR.LEG.REP.DOC>
*                PRINT
*                END
            END

            IF LOCAL.REF<1,CULR.CAPITAL.ISSUE> THEN
                PRINT SPACE(7):STR('_',60)
                PRINT
                PRINT SPACE(5):"**������ ��� �����:"
                PRINT SPACE(7):"___________________"
                PRINT
                PRINT SPACE(7)     :" ��� ����� ������               :":LOCAL.REF<1,CULR.CAPITAL.ISSUE>
                PRINT
*                PRINT SPACE(7)     :" ��� ����� ������ ������� ������:":LOCAL.REF<1,CULR.FOUNDER.NAME>
*                PRINT
                PRINT SPACE(7)     :" ���� ��� �������               :":LOCAL.REF<1,CULR.NO.OF.SHARES>
                PRINT
                PRINT SPACE(7)     :" ��� ����� ������ ��            :":LOCAL.REF<1,CULR.CURR.SHARE.VAL>
                PRINT
                PRINT SPACE(7)     :" ���� ����� ������� ������      :":LOCAL.REF<1,CULR.SHARE.VALUE.EGP>
                PRINT
                PRINT SPACE(7)     :"���� ��������                   :":LOCAL.REF<1,CULR.UNDERWRITER>
                PRINT
            END
        END

    END
*------------------------
    SAMSAM    = LEN(COMI)
    IF SAMSAM = 8 THEN
        CUS.CLASS    = COMI[3,1]
        IF CUS.CLASS = 3 THEN
            IF LOCAL.REF<1,CULR.COM.REG.NO> THEN
                PRINT SPACE(7):STR('_',60)
                PRINT
                PRINT SPACE(5):"**��������:"
                PRINT SPACE(7):"___________"
                PRINT

                IF LOCAL.REF<1,CULR.COM.REG.NO> THEN
                    PRINT SPACE(7) :"��� ����� ������ �������  :":LOCAL.REF<1,CULR.COM.REG.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.COM.REG.EXP.D> THEN
                    PRINT SPACE(7) :"����� �����               :":LOCAL.REF<1,CULR.COM.REG.EXP.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.EXP.DATE> THEN
                    PRINT SPACE(7) :"����� ������ �����        :":LOCAL.REF<1,CULR.LIC.EXP.DATE>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LEGAL.STATUS> THEN
                    PRINT SPACE(7) :"��� �������               :":LOCAL.REF<1,CULR.LEGAL.STATUS>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.EXEMPTION> THEN
                    PRINT SPACE(7) :"��� ���� �������          :":LOCAL.REF<1,CULR.TAX.EXEMPTION>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.ISSUE.DATE> THEN
                    PRINT SPACE(7) :"����� ���� �� ������� �������:":LOCAL.REF<1,CULR.LIC.ISSUE.DATE>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.IMP.LIC.NO> THEN
                    PRINT SPACE(7) :"��� ������� �����������   :":LOCAL.REF<1,CULR.IMP.LIC.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.IMP.LIC.END.D> THEN
                    PRINT SPACE(7) :"����� �������             :":LOCAL.REF<1,CULR.IMP.LIC.END.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.EXP.LIC.NO> THEN
                    PRINT SPACE(7) :"��� ������� ���������     :":LOCAL.REF<1,CULR.EXP.LIC.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.EXP.LIC.END.D> THEN
                    PRINT SPACE(7) :"����� �������             :":LOCAL.REF<1,CULR.EXP.LIC.END.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.IND.NO> THEN
                    PRINT SPACE(7) :"��� ������� �������/������ ������:":LOCAL.REF<1,CULR.LIC.IND.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.LIC.IND.EXP.D> THEN
                    PRINT SPACE(7) :"����� ����� �������       :":LOCAL.REF<1,CULR.LIC.IND.EXP.D>
                    PRINT
                END
            END

            IF LOCAL.REF<1,CULR.TAX.NO> THEN
                PRINT SPACE(7):STR('_',60)
                PRINT
                PRINT SPACE(5):"**������� - ���������:"
                PRINT SPACE(7):"______________________"
                PRINT

                IF LOCAL.REF<1,CULR.TAX.NO> THEN
                    PRINT SPACE(7) :"��� ������� ��������      :":LOCAL.REF<1,CULR.TAX.NO>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.FILE.NO> THEN
                    PRINT SPACE(7) :"��� ����� �������         :":LOCAL.REF<1,CULR.TAX.FILE.NO>
                    PRINT
                END

                IF TAXDEPT THEN
                    PRINT SPACE(7) :"������� �������           :":TAXDEPT
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.EXEMP.ST.D> THEN
                    PRINT SPACE(7) :"����� ����� �������       :":LOCAL.REF<1,CULR.TAX.EXEMP.ST.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.TAX.EXEMP.END.D> THEN
                    PRINT SPACE(7) :"����� ������ �������      :":LOCAL.REF<1,CULR.TAX.EXEMP.END.D>
                    PRINT
                END

                IF LOCAL.REF<1,CULR.SOCIAL.INSUR.NO> THEN
                    PRINT SPACE(7) :"��� ��� ���������         :":LOCAL.REF<1,CULR.SOCIAL.INSUR.NO>
                    PRINT
                END

                DCOUNTTAX = 0
                FOR  NUMBERTAX = 1 TO DCOUNTTAX

                    IF LOCAL.REF<1,CULR.TAX.STATUS> THEN
                        PRINT SPACE(7):"������ �������            :":LOCAL.REF<1,CULR.TAX.STATUS><1,NUMBERTAX>
                        PRINT
                    END

                NEXT   NUMBERTAX

*                IF LOCAL.REF<1,CULR.LEG.REP.DOC> THEN
*                    PRINT SPACE(7) :"������ ��������           :":LOCAL.REF<1,CULR.LEG.REP.DOC>
*                    PRINT
*                END
            END

            IF LOCAL.REF<1,CULR.CAPITAL.ISSUE> THEN
                PRINT SPACE(7):STR('_',60)
                PRINT
                PRINT SPACE(5):"**������ ��� �����:"
                PRINT SPACE(7):"___________________"
                PRINT
                PRINT SPACE(7)     :" ��� ����� ������              :":LOCAL.REF<1,CULR.CAPITAL.ISSUE>
                PRINT
*                PRINT SPACE(7)     :"��� ����� ������ ������� ������:":LOCAL.REF<1,CULR.FOUNDER.NAME>
*                PRINT
                PRINT SPACE(7)     :" ���� ��� �������              :":LOCAL.REF<1,CULR.NO.OF.SHARES>
                PRINT
                PRINT SPACE(7)     :" ��� ����� ������ ��           :":LOCAL.REF<1,CULR.CURR.SHARE.VAL>
                PRINT
                PRINT SPACE(7)     :" ���� ����� ������� ������     :":LOCAL.REF<1,CULR.SHARE.VALUE.EGP>
                PRINT
                PRINT SPACE(7)     :"���� ��������                  :":LOCAL.REF<1,CULR.UNDERWRITER>
                PRINT
            END

        END
    END

    IF LOCALOLDID THEN
        PRINT SPACE(7):STR('_',60)
        PRINT
        PRINT SPACE(5):"**������ �������� :"
        PRINT SPACE(7):"___________________"
        PRINT
        IF LOCAL.REF<1,CULR.REFERENCE.BANK> NE '' THEN
*Line [ 846 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            DCOUNTREFBANK = DCOUNT (LOCAL.REF<1,CULR.REFERENCE.BANK>,@SM)
            FOR NUMBERREFBANK = 1 TO DCOUNTREFBANK
                PRINT SPACE(7)            :"����� �������� ���        :":LOCAL.REF<1,CULR.REFERENCE.BANK><1,NUMBERREFBANK>
            NEXT NUMBERREFBANK
        END

        IF LOCAL.REF<1,CULR.CONTACT.NAMES> NE '' THEN
*Line [ 854 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            DCOUNTCONTACTNAME = DCOUNT (LOCAL.REF<1,CULR.CONTACT.NAMES>,@SM)
            FOR  NUMBERCONTACTNAME = 1 TO DCOUNTCONTACTNAME
                PRINT SPACE(7)      :"��� ������� ��������� ��� :":LOCAL.REF<1,CULR.CONTACT.NAMES><1,NUMBERCONTACTNAME>
            NEXT NUMBERCONTACTNAME
        END

        IF LOCALBROKERTYPE THEN PRINT SPACE(7):"���� ������� �� ������    :":LOCALBROKERTYPE
        IF QQ THEN PRINT SPACE(7)             :"������ ������             :":QQ
        IF OO THEN PRINT SPACE(7)             :"��� ���� ������ �����    :":OO
        IF LOCALOLDID THEN PRINT SPACE(7)     :"��� ������ ������         :":LOCALOLDID
    END

    PRINT SPACE(7):STR('_',60)
    PRINT
    PRINT SPACE(5):"**������ ���� ������  :"
    PRINT SPACE(7):"_______________________"
    PRINT
    PRINT SPACE(7):"��� ���� ��������" : III
    PRINT
    PRINT SPACE(7): "��� ���� �������� : " : INP.NAME
    PRINT SPACE(7):"����� � �������          :":JJJ
    PRINT ; PRINT
    PRINT SPACE(60):"����� ������"
    PRINT ;
    RETURN
