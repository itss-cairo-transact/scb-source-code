* @ValidationCode : MjotMTI4NDk5ODI1NjpDcDEyNTI6MTY0MDcxMTE5NzM5MDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 19:06:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*----------------------------------------------------------------------*
* <Rating>326</Rating>
*----------------------------------------------------------------------*
    SUBROUTINE OFS.WH.ACCT.9048

*Line [ 21 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
    *Line [ 23 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
    *Line [ 25 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.OFS.SOURCE
    *Line [ 27 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
    *Line [ 29 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
    *Line [ 31 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER.ACCOUNT
    *Line [ 33 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY
    *Line [ 35 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
    *Line [ 37 ] Removed directory from $$INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.NUMERIC.CURRENCY
    $INSERT           I_OFS.SOURCE.LOCAL.REFS
    $INSERT           I_F.SCB.WH.EXPIRY
    $INSERT           I_AC.LOCAL.REFS
    $INSERT           I_F.CUSTOMER
*---------------------------------------------------------------------*
    GOSUB INITIALISE
    GOSUB PROCESS
    RETURN
*---------------------------------------------------------------------*
INITIALISE:
*-----------
    FN.OFS.SOURCE     = "F.OFS.SOURCE"
    F.OFS.SOURCE      = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN         = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK         = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN          = 0
    F.OFS.BK          = 0
    OFS.REC           = ""
    OFS.OPERATION     = "ACCOUNT"
    OFS.OPTIONS       = "BR"
    COMP              = C$ID.COMPANY
    COM.CODE          = COMP[8,2]
    OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.TRANS.ID      = ""
    OFS.MESSAGE.DATA  = ""
*----------------------------------------------------------------------*
    FN.ACC   = 'FBNK.ACCOUNT'    ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    SYS.DATE = TODAY

    FN.EXP   = "F.SCB.WH.EXPIRY" ; F.EXP = ""
    CALL OPF(FN.EXP, F.EXP)
    RETURN
*----------------------------------------------------------------------*
PROCESS:
*-------
    T.SEL  = "SELECT ":FN.EXP:" WITH EXPIRY.DATE LE ":SYS.DATE
    T.SEL := " AND EXP.FLAG NE 'YES' BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ERR)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.EXP,KEY.LIST<II>,R.EXP,F.EXP,ERR.EXP)
            ACCT.NUM  = R.EXP<EXP.DEBIT.ACCT>
            ACCT.NUM  = ACCT.NUM[1,10]:"9048":ACCT.NUM[15,2]

            CALL F.READ(FN.AC, R.EXP<EXP.DEBIT.ACCT>,R.AC,F.AC,E1)
            COMPO = R.AC<AC.CO.CODE>

            COMP              = COMPO
            COM.CODE          = COMP[8,2]
            OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP
            GOSUB BUILD.RECORD
***** SCB R15 UPG 20160628 - S
*            CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E

            ACCT.NUM = R.EXP<EXP.CREDIT.ACCT>
            ACCT.NUM = ACCT.NUM[1,3]:"19048":ACCT.NUM[9,8]
            CALL F.READ(FN.AC, R.EXP<EXP.CREDIT.ACCT>,R.AC,F.AC,E1)
            COMPO = R.AC<AC.CO.CODE>

            COMP              = COMPO
            COM.CODE          = COMP[8,2]
            OFS.USER.INFO     = "INPUTT":COM.CODE:"/":"/" :COMP

            GOSUB BUILD.RECORD.2
***** SCB R15 UPG 20160628 - S
*            CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
        NEXT II
    END
    RETURN
**---------------------------------------------------------------------*
BUILD.RECORD:
*------------
    COMMA       = ","
    DAT         = TODAY
    AC.SERL     = 1

    CALL F.READ(FN.ACC,ACCT.NUM,R.ACC,F.ACC,ERR)
    IF ERR THEN
        CUSS = ACCT.NUM[1,8]
        IF ACCT.NUM[1,1] EQ 0 THEN
            CUSS = ACCT.NUM[2,8]
        END

        CUR.CODE = ACCT.NUM[9,2]
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.CODE,CURR)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CUR.CODE,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CURR=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>

        OFS.TRANS.ID      = ACCT.NUM
        OFS.MESSAGE.DATA  = "CUSTOMER=":CUSS:COMMA
        OFS.MESSAGE.DATA := "CATEGORY=":"9048":COMMA
        OFS.MESSAGE.DATA := "CURRENCY=":CURR

        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA

        OFS.ID = OFS.TRANS.ID:"-":DAT
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160628 - S
        GOSUB OPM.PROCESS
***** SCB R15 UPG 20160628 - E

    END
    RETURN
**-----------------------------------------------------------------------*
BUILD.RECORD.2:
*--------------
    ACCT.OFS = ACCT.NUM[15,2]
    IF ACCT.OFS[1,1] EQ '0' THEN
        ACCT.OFS = ACCT.NUM[16,1]
    END

    CALL F.READ(FN.ACC,ACCT.NUM,R.ACC,F.ACC,ERR)
    IF ERR THEN
        OFS.TRANS.ID = ACCT.NUM

        OFS.MESSAGE.DATA  = "MNEMONIC=":ACCT.NUM[1,3]:ACCT.OFS:","
        XX = "����� ������ ���� ������� - �����"
        OFS.MESSAGE.DATA := "ACCOUNT.TITLE.1=":XX:","
        OFS.MESSAGE.DATA := "SHORT.TITLE=":XX:","
        OFS.MESSAGE.DATA := "ACCOUNT.OFFICER=":ACCT.OFS

        DAT = TODAY
        OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
        OFS.ID = "NESSMA":TNO:".":OFS.TRANS.ID:"-":DAT
        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160628 - S
        GOSUB OPM.PROCESS
***** SCB R15 UPG 20160628 - E

    END
    RETURN

***** SCB R15 UPG 20160628 - S
*--
OPM.PROCESS:
*--

*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*
    RETURN
***** SCB R15 UPG 20160628 - E
*---------------------------------------------------------------------
END
