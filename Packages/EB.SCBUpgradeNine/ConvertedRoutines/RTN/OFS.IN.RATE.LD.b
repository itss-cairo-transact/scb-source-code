* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>90</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.IN.RATE.LD(LD.FILE)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.RENEW.METHOD
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

    TEMP.AUTH=FIELD(LD.FILE,",",2)
    TEMP.AUTH1=FIELD(TEMP.AUTH,"/",2)
    IF TEMP.AUTH1 NE "A" THEN

        TEMP.CATEG = FIELD(LD.FILE,",",10)
        CATEG1 = FIELD(TEMP.CATEG,"=",2)

        TEMP.CUR = FIELD(LD.FILE,",",6)
        CURR = FIELD(TEMP.CUR,"=",2)
        TEMP.AMT = FIELD(LD.FILE,",",7)
        AMT = FIELD(TEMP.AMT,"=",2)

         TEMP.DAT = FIELD(LD.FILE,",",8)
         DAAT = FIELD(TEMP.DAT,"=",2)

 CATEG = CATEG1:DAAT

****************************************************************************************************
*LD.LOANS.AND.DEPOSITS,SCB.OPEN.OFS,ZEAD01/123456,,CUSTOMER.ID=13200493,CURRENCY=EGP,AMOUNT=20111.09,VALUE.DATE=20060208,FIN.MAT.DATE=20060508,CATEGORY=21005,BUS.DAY.DEFN=EG,INT.RATE.TYPE=5,INT.PAYMT.METHOD=1,AUTO.SCHEDS=NO,INTEREST.BASIS=E,INTEREST.SPREAD=,DEFINE.SCHEDS=NO,DRAWDOWN.ACCOUNT=1320049310100101,CUST.REMARKS=,ALONE.MERGE.IND=,PRIN.LIQ.ACCT=1320049310100101,INT.LIQ.ACCT=1320049310100101,LOCAL.REF:1:1=YES,LOCAL.REF:2:1=2,LOCAL.REF:3:1=2,JOINT.HOLDER=,RELATION.CODE=,LD.RENEWAL.AMT=20111.09,LD.RENEW.DATE=20060208,MATURE.AT.SOD=YES,INTEREST.RATE=6.75
        F.LD.LEVEL = '' ; FN.LD.LEVEL = 'F.SCB.LD.TYPE.LEVEL' ; R.LD.LEVEL = ''
        CALL OPF(FN.LD.LEVEL,F.LD.LEVEL)

        CALL F.READ(FN.LD.LEVEL, CATEG, R.LD.LEVEL, F.LD.LEVEL, ETEXT)
        LOCATE CURR IN R.LD.LEVEL<LDTL.CURRENCY,1> SETTING POS THEN
VV = R.LD.LEVEL<LDTL.CURRENCY,1>
EE = R.LD.LEVEL<LDTL.RATE,POS>
*Line [ 64 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            TEMP.COUNT =  DCOUNT (R.LD.LEVEL<LDTL.AMT.FROM,POS>,@SM)
            FOR I = 1 TO TEMP.COUNT
AMT.FROM = R.LD.LEVEL<LDTL.AMT.FROM,POS,I>
AMT.TO = R.LD.LEVEL<LDTL.AMT.TO,POS,I>
                IF AMT GE AMT.FROM AND AMT LE AMT.TO THEN
                    INT.RATE = R.LD.LEVEL<LDTL.RATE,POS,I>
A = R.NEW(LD.INTEREST.RATE)
B = R.LD.LEVEL<LDTL.RATE,POS,I>
        ZZ = LD.FILE
        LD.FILE = ZZ :",INTEREST.RATE=":INT.RATE

                END

                IF TEMP.COUNT = I AND AMT GE R.LD.LEVEL<LDTL.AMT.FROM,POS,I> THEN
                    INT.RATE = R.LD.LEVEL<LDTL.RATE,POS,I>

        ZZ = LD.FILE
        LD.FILE = ZZ :",INTEREST.RATE=":INT.RATE

                END
            NEXT I
        END
****************************************************************************************************
*        FN.LD.TXN.TYPE.CONDITION = 'F.LD.TXN.TYPE.CONDITION' ; F.LD.TXN.TYPE.CONDITION = ''
 *       POS = ''
  *
   *     CALL OPF( FN.LD.TXN.TYPE.CONDITION,F.LD.TXN.TYPE.CONDITION)
    *    CALL F.READ(FN.LD.TXN.TYPE.CONDITION,CATEG, R.LD.TXN.TYPE.CONDITION,F.LD.TXN.TYPE.CONDITION, ETEXT)
     *   IF ETEXT THEN TEXT = "ERR ":ETEXT ; CALL REM
      *  LOCATE CUR IN R.LD.TXN.TYPE.CONDITION<LTTC.CURRENCY,1> SETTING POS THEN
       *     INT.RATE = R.LD.TXN.TYPE.CONDITION<LTTC.INTEREST.RATE><1,POS>
        *END
*        ZZ = LD.FILE
 *       LD.FILE = ZZ :",INTEREST.RATE=":INT.RATE
* *   LD.FILE = ZZ :",CATEG=":CATEG:",CUR=":CUR
   *
    *END
    RETURN
END
