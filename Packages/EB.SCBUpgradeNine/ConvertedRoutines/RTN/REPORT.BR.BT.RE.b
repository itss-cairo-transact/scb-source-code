* @ValidationCode : MjotOTY2ODg2NzYyOkNwMTI1MjoxNjQ0OTM1NzczOTg2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:36:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>739</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------
** Create By Ni7oooo
** Update By Nessma
*********UPDATED BY REHAM YOUSSIF 11/11/2014**********
*----------------------------------------------------
SUBROUTINE REPORT.BR.BT.RE

*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.BILL.REGISTER
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CURRENCY
*Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CATEGORY
*Line [ 42 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.ACCOUNT
    $INSERT         I_F.SCB.CUS.REGION
    $INSERT         I_F.SCB.CUS.GOVERNORATE
    $INSERT         I_F.SCB.BANK
    $INSERT         I_F.SCB.BANK.BRANCH
    $INSERT         I_F.SCB.BT.BATCH
    $INSERT         I_CU.LOCAL.REFS
    $INSERT         I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �������" ; CALL REM
RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.BR.BT.RE'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)
*--- EDIT BY NESSMA 27/11/2014
    REG    = ""
    GOV    = ""
    AMOUNT = ""
    CUR    = ""
    BRANCH = ""
    MAT.DAT1 = ""
RETURN
*========================================================================
PROCESS:
*-------
    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
    BT.ID = COMI

    CALL F.READ(FN.BATCH,BT.ID,R.BT,F.BATCH,ERR.BT)
    INPUTTER = R.BT<SCB.BT.INPUTTER>
    AUTH     = R.BT<SCB.BT.AUTHORISER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    T.SEL = "SELECT F.SCB.BT.BATCH WITH @ID EQ ": COMI
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.BATCH,KEY.LIST<1>,R.BATCH,F.BATCH,E1)
        BR.ID = R.BATCH<SCB.BT.OUR.REFERENCE>
*Line [ 92 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD    = DCOUNT(BR.ID,@VM)
        TEXT  = DD    ; CALL REM

        FOR I = 1 TO DD
            BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
            RES    = R.BATCH<SCB.BT.RETURN.REASON>
            BT.DAT = R.BATCH<SCB.BT.DATE.TIME>

            IF R.BATCH<SCB.BT.RETURN.REASON><1,I> EQ '' THEN
                CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
                DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>

*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
                F.ITSS.CUSTOMER = 'F.CUSTOMER'
                FN.F.ITSS.CUSTOMER = ''
                CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
                CUST.NAME.2   = LOCAL.REF<1,CULR.ARABIC.NAME.2>
                CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
                CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
                CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
                CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>
                CUST.REGION   = LOCAL.REF<1,CULR.REGION>
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.REGION,REG)
                F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
                FN.F.ITSS.SCB.CUS.REGION = ''
                CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
                CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.REGION,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
                REG=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>

                FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                    CUST.ADDRESS1 = "���� ��������� ������"
                END ELSE
                    CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
                    CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
                    CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>
                    CUST.REGION   = LOCAL.REF<1,CULR.REGION>
*Line [ 143 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.REGION,REG)
                    F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
                    FN.F.ITSS.SCB.CUS.REGION = ''
                    CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
                    CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.REGION,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
                    REG=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
                    CUST.GOVER = LOCAL.REF<1,CULR.GOVERNORATE>
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.GOVER,GOV)
                    F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
                    FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
                    CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
                    CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.GOVER,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
                    GOV=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
                END

                ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
                CHQ.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
                BANK         = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
                BANK.BRAN    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
                COLL.DATE    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.COLL.DATE>

*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK,BNAME)
                F.ITSS.SCB.BANK = 'F.SCB.BANK'
                FN.F.ITSS.SCB.BANK = ''
                CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
                CALL F.READ(F.ITSS.SCB.BANK,BANK,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
                BNAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*Line [ 173 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BANK.BRAN,BRNAME)
                F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
                FN.F.ITSS.SCB.BANK.BRANCH = ''
                CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
                CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BANK.BRAN,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
                BRNAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>
*       CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
                BRN.ID  = AC.COMP[2]
                BRANCH.ID = TRIM(BRN.ID,"0","L")
*Line [ 190 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
                F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
                FN.F.ITSS.DEPT.ACCT.OFFICER = ''
                CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
                CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
                BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

                CATEG.ID  = ACC.NO[11,4]
*Line [ 152 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
                
*Line [ 201 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

                AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
                IN.AMOUNT = AMOUNT
                CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

                CUR.ID  = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 161 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
                
*Line [ 216 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
                F.ITSS.CURRENCY = 'F.CURRENCY'
                FN.F.ITSS.CURRENCY = ''
                CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

                DAT     = TODAY
*            CALL CDT("",DAT,'+1W')
                MAT.DATE = TODAY[1,2] : BT.DAT[1,6]
*                MAT.DATE  = MAT.DAT1[7,2]:'/':MAT.DAT1[5,2]:"/":MAT.DAT1[1,4]
                MAT.DATE = FMT(MAT.DATE,"####/##/##")
                XX        = SPACE(132)
                XX1       = SPACE(132)
                XX2       = SPACE(132)
                XX3       = SPACE(132)
*--------------------------------------------
                PR.HD  = "REPORT.BR.BT.RE"
                HEADING PR.HD
*Line [ 237 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
                F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
                FN.F.ITSS.DEPT.ACCT.OFFICER = ''
                CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
                CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
                BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                YYBRN  = FIELD(BRANCH,'.',2)
                DATY   = TODAY
                T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
                PRINT SPACE(5) : SPACE(1):"��� ���� ������"
                PRINT SPACE(5) : "������� : ":T.DAY
                PRINT SPACE(5) : "����� : ":YYBRN
*---------------------------------------------------------------
                XX<1,1>[3,35]    = "�������"
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,35]    = CUST.NAME:' ':CUST.NAME.2
                XX1<1,1>[3,35]   = CUST.ADDRESS1
                XX2<1,1>[3,35]   = CUST.ADDRESS2
                XX3<1,1>[3,120]  = CUST.ADDRESS3:'-':REG:'-':GOV

                PRINT XX<1,1>
                PRINT XX1<1,1>
                PRINT XX2<1,1>
                PRINT XX3<1,1>
                XX<1,1>  = ""
                XX1<1,1> = ""
                XX2<1,1> = ""
                XX3<1,1> = ""

                XX<1,1>[3,15]   = '������     : '
                XX<1,1>[30,15]  = AMOUNT
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� ������ : '
                XX<1,1>[20,15] = ACC.NO
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� ������ : '
                XX<1,1>[20,15] = CATEG
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '������     : '
                XX<1,1>[20,15] = CUR
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '����� ���� : '
                XX<1,1>[30,15] = MAT.DATE
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� �����      : '
                XX<1,1>[20,15] = CHQ.NO
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� �������    : '
                XX<1,1>[30,15] = BR.ID1
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '����� ������� ����: '
                XX<1,1>[30,15] = BNAME
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� �����: '
                XX<1,1>[30,15] = BRNAME
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]    = '������'
                XX<1,1>[30,15]   = INP
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[1,15]   = "��� �������"
                XX<1,1>[35,15]  = COMI
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]    = '������'
                XX<1,1>[30,15]   = AUTHI
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,35]   = '������ ������� : '
                XX<1,1>[20,15]  = OUT.AMT
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]   = '������         : '
                XX<1,1>[20,15]  = '����� ��� �������'
                PRINT XX<1,1>
                XX<1,1> = ""
            END
        NEXT I
    END
RETURN
*---------------------------------------------------------
END
