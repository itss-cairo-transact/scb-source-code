* @ValidationCode : MjoyNjg1MjM3OTU6Q3AxMjUyOjE2NDA2OTI0MDAwNDE6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 13:53:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*----------------------------
*------ CREATE BY Nahrawy
*------ EDIT BY Nessma
*----------------------------
    SUBROUTINE REPORT.FT.BR2
*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
    *Line [ 23 ] Removed directory from $INSERT- ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
    *Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.USER
    *Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.FUNDS.TRANSFER
    *Line [ 29 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DEPT.ACCT.OFFICER
    *Line [ 31 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CUSTOMER
    *Line [ 33 ] Removed directory from $INSERT- ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.ACCOUNT
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_F.SCB.BT.BATCH
*----------------------------------------------
    TEXT = "START REPORT"  ; CALL REM
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 35 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 42 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "END REPORT" ; CALL REM
    RETURN
*==============================================
INITIATE:
*--------
    REPORT.ID='REPORT.FT.BR2'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*==============================================
CALLDB:
*------
    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H  = ''
    CALL OPF(FN.FT.H,F.FT.H)

    FN.BATCH = 'F.SCB.BT.BATCH'          ; F.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.FT    = 'FBNK.FUNDS.TRANSFER'     ; F.FT    = ''
    CALL OPF(FN.FT,F.FT)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""


    TEXT = "1 / 4" ; CALL REM
    DATE.TO.H = TODAY[3,6]:"..."
    T.SEL  = " SELECT FBNK.FUNDS.TRANSFER$HIS WITH DATE.TIME LIKE ": DATE.TO.H
    T.SEL := " AND ( DEBIT.THEIR.REF LIKE BR... OR ORDERING.BANK LIKE BR... )"
    T.SEL := " AND DEBIT.CURRENCY NE 'EGP'"
    T.SEL := " BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.FT.H,KEY.LIST<Z>,R.FT.H,F.FT.H,E1)
            DEBIT.ID.H     = KEY.LIST<Z>
            DEBIT.AC.NO.H  = R.FT.H<FT.DEBIT.ACCT.NO>
            DEBIT.AMOUNT.H = R.FT.H<FT.DEBIT.AMOUNT>
            DEBIT.REF.H    = R.FT.H<FT.ORDERING.BANK>
            CREDIT.AC.NO.H = R.FT.H<FT.CREDIT.ACCT.NO>
            DEBIT.CUR.H    = R.FT.H<FT.DEBIT.CURRENCY>

            TEXT   = "2 / 4" ; CALL REM
            T.SEL2 = "SELECT F.SCB.BT.BATCH WITH OUR.REFERENCE EQ " : DEBIT.REF.H
            CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
            IF SELECTED2 THEN
                FOR Y = 1 TO SELECTED2
                    CALL F.READ(FN.BATCH,KEY.LIST2<Y>,R.BATCH,F.BATCH,E2)
                    DD   = R.BATCH<SCB.BT.OUR.REFERENCE>
*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,DEBIT.AC.NO.H,CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,DEBIT.AC.NO.H,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                    NAME = LOCAL<1,CULR.ARABIC.NAME>
                    CC   = NAME
                NEXT Y
            END

            XX1 = SPACE(132)
            XX1<1,1>[1,20]    = CREDIT.AC.NO.H
            XX1<1,1>[25,20]   = DEBIT.AC.NO.H
            XX1<1,1>[50,5]    = DEBIT.CUR.H
            XX1<1,1>[60,10]   = DEBIT.AMOUNT.H
            XX1<1,1>[75,20]   = DEBIT.REF.H
            XX1<1,1>[100,20]  = DEBIT.ID.H
            XX1<1,1>[120,20]  = KEY.LIST2<Y>
            XX1<1,2>[1,20]    = CC
            PRINT XX1<1,1>
            PRINT XX1<1,2>
            PR.HD :="'L'":STR('_',120)
        NEXT Z
    END

    TEXT    = "3 / 4" ; CALL REM
    DATE.TO = TODAY[3,6]:"..."
    T.SEL1  = "SELECT ":FN.FT:" WITH DATE.TIME LIKE ":DATE.TO
    T.SEL1 := " AND ( DEBIT.THEIR.REF LIKE BR... OR ORDERING.BANK LIKE BR... )"
    T.SEL1 := " AND DEBIT.CURRENCY NE 'EGP'"
    T.SEL1 := " BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF SELECTED1  THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.FT,KEY.LIST1<I>,R.FT,F.FT,E1)
            DEBIT.ID     = KEY.LIST1<I>
            DEBIT.AC.NO  = R.FT<FT.DEBIT.ACCT.NO>
            DEBIT.AMOUNT = R.FT<FT.DEBIT.AMOUNT>
            DEBIT.REF    = R.FT<FT.ORDERING.BANK>
            CREDIT.AC.NO = R.FT<FT.CREDIT.ACCT.NO>
            DEBIT.CUR    = R.FT<FT.DEBIT.CURRENCY>

            TEXT = "4 / 4" ; CALL REM
            T.SEL3 = "SELECT F.SCB.BT.BATCH WITH OUR.REFERENCE EQ " : DEBIT.REF
            CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)

            IF SELECTED3 THEN
                FOR Q = 1 TO SELECTED3
                    CALL F.READ(FN.BATCH,KEY.LIST3<Q>,R.BATCH,F.BATCH,E3)
                    VV = R.BATCH<SCB.BT.OUR.REFERENCE>
                NEXT Q
            END

            XX  = SPACE(130)
            XX<1,1>[1,20]    = CREDIT.AC.NO
            XX<1,1>[25,20]   = DEBIT.AC.NO
            XX<1,1>[50,5]    = DEBIT.CUR
            XX<1,1>[58,10]   = DEBIT.AMOUNT
            XX<1,1>[70,20]   = DEBIT.REF
            XX<1,1>[95,20]   = DEBIT.ID
            XX<1,1>[120,15]  = KEY.LIST3<Q>
            PRINT XX<1,1>
            PRINT XX<1,2>
            PR.HD :="'L'":STR('_',100)
        NEXT I
    END

    PRINT " "
    PRINT SPACE(50) : "********* ": "����� �������" : " *********"
    RETURN
*===============================================================
PRINT.HEAD:
*----------
*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� ����� ������ ������� ������� "
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������" :SPACE(15):"���� �����":SPACE(10):"������":SPACE(5):"������":SPACE(10):"��� ���������":SPACE(20):"��� ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
