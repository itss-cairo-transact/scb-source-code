* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*---------------------------
* CREATED BY REHAM YOUSSIF
*---------------------------
    PROGRAM POSSESS.RATE.19
*---------------------------
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS.CU
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*----------------------------------------------
OPEN.FILES:
    FN.POSS    = "F.SCB.POSSESS"; F.POSS = ''
    CALL OPF(FN.POSS,F.POSS)

    FN.POSS.CU = 'F.SCB.POSSESS.CU' ; F.POSS.CU = ''
    CALL OPF(FN.POSS.CU,F.POSS.CU)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    CUST.CODE   = ''
    CUST.CODE2  = ''
    PAPER.CODE  = ''
    PAPER.CODE2 = ''
    CUR         = ''
    CUR2        = ''

    CUS.COUNT = 0
*-----------------------------------------------
    T.SEL  = "SELECT F.SCB.POSSESS WITH POSSESS.DATE EQ '20201231' BY CUST.CODE BY @ID BY PAPER.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ASD)
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.POSS,KEY.LIST<I>,R.POSS,F.POSS,ETEXT)
        CALL F.READ(FN.POSS,KEY.LIST<I+1>,R.POSS2,F.POSS,ETEXT)
        POSS.ID = KEY.LIST<I>
        POSS.ID2 = KEY.LIST<I+1>
******************************************
        IF SELECTED THEN
            CUST.CODE   = R.POSS<POSS.CUST.CODE>
            CUST.CODE2  = R.POSS2<POSS.CUST.CODE>
            PAPER.CODE  = R.POSS<POSS.PAPER.CODE>
            PAPER.CODE2 = R.POSS2<POSS.PAPER.CODE>
            CUR         = R.POSS<POSS.CURRENCY>
            CUR2        = R.POSS2<POSS.CURRENCY>
            CUS.COUNT   = CUS.COUNT + 1

*------------VAL3 ������ ��� ������-------------------------------
            IF CUST.CODE2 NE CUST.CODE THEN
                IND        = I - CUS.COUNT + 1
                POSS.ID3   = KEY.LIST<IND>
                IF CUS.COUNT NE 1 THEN
                    CALL F.READ(FN.POSS,POSS.ID3,R.POSS3,F.POSS,ETEXT)
                    CUR3 =  R.POSS3<POSS.CURRENCY>
                    IF CUR3 EQ 'EGP' THEN
                        DEB.AMT3 = '40'
                    END ELSE
                        DEB.AMT3 = '2.55'
                    END
                    R.POSS3<POSS.RATE3> = 0
                    R.POSS3<POSS.MIN3>  = 0
                    R.POSS3<POSS.MAX3>  = 0
                    R.POSS3<POSS.VAL3>  = DEB.AMT3
                    R.POSS3<POSS.RATE4> = 0
                    R.POSS3<POSS.MIN4>  = 0
                    R.POSS3<POSS.MAX4>  = 0
                    R.POSS3<POSS.VAL4>  = 0
                    CALL F.WRITE(FN.POSS,POSS.ID3,R.POSS3)
                    CALL JOURNAL.UPDATE(POSS.ID3)
                    CUS.COUNT  = 0
                END ELSE
                    CALL F.READ(FN.POSS,POSS.ID,R.POSS,F.POSS,ETEXT)
                    CUR33 =  R.POSS<POSS.CURRENCY>
                    IF CUR33 EQ 'EGP' THEN
                        DEB.AMT33 = '40'
                    END ELSE

                        DEB.AMT33 = '2.55'
                    END
                    R.POSS<POSS.RATE3> = 0
                    R.POSS<POSS.MIN3>  = 0
                    R.POSS<POSS.MAX3>  = 0
                    R.POSS<POSS.VAL3>  = DEB.AMT33
                    R.POSS<POSS.RATE4> = 0
                    R.POSS<POSS.MIN4>  = 0
                    R.POSS<POSS.MAX4>  = 0
                    R.POSS<POSS.VAL4>  = 0
                    CALL F.WRITE(FN.POSS,POSS.ID,R.POSS)
                    CALL JOURNAL.UPDATE(POSS.ID)
                    CUS.COUNT  = 0
                END

            END ELSE
                R.POSS2<POSS.RATE3> = 0
                R.POSS2<POSS.MIN3>  = 0
                R.POSS2<POSS.MAX3>  = 0
                R.POSS2<POSS.VAL3>  = 0
                R.POSS2<POSS.RATE4> = 0
                R.POSS2<POSS.MIN4>  = 0
                R.POSS2<POSS.MAX4>  = 0
                R.POSS2<POSS.VAL4>  = 0
                CALL F.WRITE(FN.POSS,POSS.ID2,R.POSS2)
                CALL JOURNAL.UPDATE(POSS.ID2)
            END
************************CALC VAL1 VAL2 AND CURR EQ EGP****************
            IF ((CUST.CODE EQ CUST.CODE2) AND (PAPER.CODE EQ PAPER.CODE2)) AND R.POSS<POSS.CURRENCY> EQ 'EGP'  THEN
*******VAL1 AND CODE EQ 3**************
                R.POSS<POSS.RATE.CODE> = 3
                R.POSS2<POSS.RATE.CODE> = 2
                VALUE1 = R.POSS<POSS.VALUE>
                VALUE2 = R.POSS2<POSS.VALUE>

                TOT.VAL1 = (VALUE1 + VALUE2) * (0.005 / 100)
                IF TOT.VAL1 LE '1' THEN
                    R.POSS<POSS.VAL1> = 1
                    R.POSS2<POSS.VAL1> = 0
                END
                IF TOT.VAL1 GT '1' THEN
                    R.POSS2<POSS.VAL1> = R.POSS2<POSS.AGAINST.POSSESS>
                    R.POSS<POSS.VAL1> = R.POSS<POSS.AGAINST.POSSESS>
                END
                R.POSS<POSS.RATE1>  = 0.005
                R.POSS<POSS.MIN1>   = 1
                R.POSS<POSS.MAX1>   = R.POSS<POSS.VAL1>
                R.POSS2<POSS.RATE1> = 0.005
                R.POSS2<POSS.MIN1>  = 1
                R.POSS2<POSS.MAX1>  = R.POSS<POSS.VAL1>

*******VAL2 AND CODE EQ 3 ***********8

                R.POSS<POSS.RATE2> = 0.04
                R.POSS<POSS.MIN2>  = 10


                VALUE1 = R.POSS<POSS.VALUE>
                VALUE2 = R.POSS2<POSS.VALUE>
                TOT.VAL = (VALUE1 + VALUE2) * (0.04 / 100)
                IF TOT.VAL LE '10' THEN
                    R.POSS<POSS.VAL2> = 10
                    R.POSS2<POSS.VAL2> = 0
                END
                IF TOT.VAL GT '10' THEN
                    R.POSS<POSS.VAL2> = ( VALUE1 * 0.04 ) / 100
                    R.POSS2<POSS.VAL2> = ( VALUE2 * 0.04 ) / 100
                END


                R.POSS<POSS.RATE2> = 0.04
                R.POSS<POSS.MIN2>  = 10
                R.POSS<POSS.MAX2>  = R.POSS<POSS.VAL2>
                R.POSS2<POSS.RATE2> = 0.04
                R.POSS2<POSS.MIN2>  = 10
                R.POSS2<POSS.MAX2>  = R.POSS2<POSS.VAL2>
                CALL F.WRITE(FN.POSS,POSS.ID,R.POSS)
                CALL F.WRITE(FN.POSS,POSS.ID2,R.POSS2)
                CALL JOURNAL.UPDATE(POSS.ID)
                CALL JOURNAL.UPDATE(POSS.ID2)

            END ELSE

                R.POSS<POSS.RATE.CODE> = 2

*******VAL1 AND CODE EQ 2 ***************************
                IF R.POSS<POSS.VAL1> = '' AND R.POSS<POSS.CURRENCY> EQ 'EGP' THEN
                    IF R.POSS<POSS.AGAINST.POSSESS> LE '1' THEN
                        VAL1  = 1
                    END ELSE
                        VAL1 = R.POSS<POSS.AGAINST.POSSESS>
                    END

                    R.POSS<POSS.RATE1> = 0.005
                    R.POSS<POSS.MIN1>  = 1
                    R.POSS<POSS.MAX1> = VAL1
                    R.POSS<POSS.VAL1> = VAL1
                    R.POSS<POSS.RATE2> = 0.04
                    R.POSS<POSS.MIN2>  = 10
*******VAL2 AND CODE EQ 2****************************

                    VAL2  = (R.POSS<POSS.VALUE> *  R.POSS<POSS.RATE2>) / 100

                    IF VAL2 LE '10' THEN
                        R.POSS<POSS.VAL2> = '10'
                    END
                    IF VAL2 GT '10' THEN
                        R.POSS<POSS.VAL2> = VAL2
                    END


                    CALL F.WRITE(FN.POSS,POSS.ID,R.POSS)
                    CALL JOURNAL.UPDATE(POSS.ID)
                    R.POSS<POSS.RATE1> = 0
                    R.POSS<POSS.MIN1>  = 0
                    R.POSS<POSS.MAX1>  = 0
                    R.POSS<POSS.VAL1>  = 0
                    R.POSS<POSS.RATE2> = 0
                    R.POSS<POSS.MIN2>  = 0
                    R.POSS<POSS.MAX2>  = 0
                    R.POSS<POSS.VAL2>  = 0

                    R.POSS<POSS.RATE.CODE> = 0
                END
            END

************************CALC VAL1 VAL2 AND CURR NE EGP****************
            IF ((CUST.CODE EQ CUST.CODE2) AND (PAPER.CODE EQ PAPER.CODE2)) AND R.POSS<POSS.CURRENCY> NE 'EGP'  THEN
*******(VAL1 AND CODE EQ 3)**************
                R.POSS<POSS.RATE.CODE> = 3
                R.POSS2<POSS.RATE.CODE> = 2
                VALUE1 = R.POSS<POSS.VALUE>
                VALUE2 = R.POSS2<POSS.VALUE>

                TOT.VAL1 = (VALUE1 + VALUE2) * (0.005 / 100)

                R.POSS<POSS.VAL1> = TOT.VAL1

                IF TOT.VAL1 LE '0.064' THEN
                    R.POSS<POSS.VAL1> = 0.064
                    R.POSS2<POSS.VAL1> = 0
                END  ELSE
                    R.POSS<POSS.VAL1> = TOT.VAL1
                END

                R.POSS<POSS.RATE1> = 0.005
                R.POSS<POSS.MIN1>  = R.POSS<POSS.VAL1>
                R.POSS<POSS.MAX1>  = R.POSS<POSS.VAL1>
                R.POSS2<POSS.RATE1> = 0.005
                R.POSS2<POSS.MIN1>  = 0
                R.POSS2<POSS.MAX1>  = 0

*******VAL2 AND CODE EQ 3**************

                VALUE3 = R.POSS<POSS.VALUE>
                VALUE4 = R.POSS2<POSS.VALUE>

                TOT.VAL3 = (VALUE3 + VALUE4) * (0.04 / 100)

                IF TOT.VAL3 LE '0.65' THEN
                    R.POSS<POSS.VAL2>  = 0.65
                    R.POSS2<POSS.VAL2> = 0
                END

                IF TOT.VAL3 GT '0.65' THEN

                    R.POSS<POSS.VAL2> = (VALUE3 * 0.04) / 100
                    R.POSS2<POSS.VAL2> = (VALUE4 * 0.04) / 100
                END


                R.POSS<POSS.RATE2> = 0.005
                R.POSS<POSS.MIN2>  = 1
                R.POSS<POSS.MAX2>  = R.POSS<POSS.VAL2>
                R.POSS2<POSS.RATE2> = 0.04
                R.POSS2<POSS.MIN2>  = 10
                R.POSS2<POSS.MAX2>  = R.POSS2<POSS.VAL2>
                CALL F.WRITE(FN.POSS,POSS.ID,R.POSS)
                CALL F.WRITE(FN.POSS,POSS.ID2,R.POSS2)
                CALL JOURNAL.UPDATE(POSS.ID)
                CALL JOURNAL.UPDATE(POSS.ID2)


            END ELSE
                R.POSS<POSS.RATE.CODE> = 2
                IF R.POSS<POSS.VAL1> = '' AND R.POSS<POSS.CURRENCY> NE 'EGP' THEN
*******VAL1 AND CODE EQ 2**************
                    R.POSS<POSS.RATE1> = 0.005
                    R.POSS<POSS.MIN1>  = R.POSS<POSS.AGAINST.POSSESS>
                    R.POSS<POSS.MAX1>  = R.POSS<POSS.AGAINST.POSSESS>
                    IF R.POSS<POSS.AGAINST.POSSESS> LE '0.064' THEN
                        R.POSS<POSS.VAL1> = 0.064
                    END ELSE
                        R.POSS<POSS.VAL1> = R.POSS<POSS.AGAINST.POSSESS>
                    END

*******VAL2 AND CODE EQ 2**************

                    R.POSS<POSS.RATE2> = 0.04
                    R.POSS<POSS.MIN2>  = 0.65

                    VAL2  = (R.POSS<POSS.VALUE> *  R.POSS<POSS.RATE2>) / 100

                    IF VAL2 LE '0.65' THEN
                        R.POSS<POSS.VAL2> = '0.65'
                    END
                    IF VAL2 GT '0.65' THEN
                        R.POSS<POSS.VAL2> = VAL2
                    END
                    CALL F.WRITE(FN.POSS,POSS.ID,R.POSS)
                    CALL JOURNAL.UPDATE(POSS.ID)

                    R.POSS<POSS.RATE1> = 0
                    R.POSS<POSS.MIN1>  = 0
                    R.POSS<POSS.MAX1>  = 0
                    R.POSS<POSS.VAL1>  = 0
                    R.POSS<POSS.RATE2> = 0
                    R.POSS<POSS.MIN2>  = 0
                    R.POSS<POSS.MAX2>  = 0
                    R.POSS<POSS.VAL2>  = 0
                    R.POSS<POSS.RATE.CODE> = 0
                END
            END
        END
    NEXT I
**************CALC VAL4  ���� �������************************
    FVAR.POSS6 = ""
    CALL OPF(FN.POSS,FVAR.POSS6)


    Path.6 = "&SAVEDLISTS&/poss.new.9.txt"
    DIR.NAME = "&SAVEDLISTS&"
    TEXT.FILE = "poss.new.txt"
    VAR.FILE = ''
    EOF = ''


    OPENSEQ DIR.NAME,TEXT.FILE TO VAR.FILE ELSE

        TEXT = "ERROR OPEN FILE ":TEXT.FILE ; CALL REM
        RETURN
    END



    R.LINE6      = '' ; R.POSS6 = '' ; T.SEL6 ='';KEY.LIST6 =''; SELECTED6 = 0
    UPDATE.FLAG6 = '' ; ERR6    = '' ; ER6    ='';L.LIST6  =''; SELECT.NO6=0; Y.SEL6=''
    EOF6 = ''
    MyPath.6 = ''
    EOF6 = ''
    DEB.AMT.6 = 0
    CUS.CONT = 0
*  DEBUG

    LOOP WHILE NOT(EOF)
        READSEQ R.LINE6 FROM VAR.FILE THEN
            CUST.CODE.KK    = TRIM(FIELD(R.LINE6,",",1))
            FOLL.CO         = TRIM(FIELD(R.LINE6,",",7))
            DEB.AMT.6       = TRIM(FIELD(R.LINE6,",",12))
            CUR.NEW.KK      = TRIM(FIELD(R.LINE6,",",5))
            IF CUR.NEW.KK EQ '����� ������' THEN
                CUR  = 'USD'
            END ELSE
                CUR  = 'EGP'
            END

            T.SEL6  = "SELECT F.SCB.POSSESS WITH POSSESS.DATE EQ '20201231' AND CUST.CODE EQ ":CUST.CODE.KK:" AND FOLLOW.CODE EQ ":FOLL.CO:" BY CUST.CODE BY CURRENCY BY FOLLOW.CODE "
            CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ASD6)
            IF SELECTED6 THEN
                CALL F.READ(FN.POSS,KEY.LIST6<SELECTED6>,R.POSS6,F.POSS,ETEXT)
                POSS.ID6 = KEY.LIST6<SELECTED6>

                DEB.AMTFIN   = DEB.AMT.6


                DEB.AMTFIN  = DROUND(DEB.AMTFIN,'3')
                R.POSS6<POSS.VAL4>  = DEB.AMTFIN
                R.POSS6<POSS.RATE4> = 0.011
                R.POSS6<POSS.MIN4>  = DEB.AMTFIN
                R.POSS6<POSS.MAX4>  = DEB.AMTFIN
                CALL F.WRITE(FN.POSS,POSS.ID6,R.POSS6)
                CALL JOURNAL.UPDATE(POSS.ID6)

**********

            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ VAR.FILE
END
