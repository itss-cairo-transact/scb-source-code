* @ValidationCode : MjotMTc2NzcwNjk1NTpDcDEyNTI6MTY0NDkzNzg3MzA3Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:11:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>384</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.DOC.DEBIT.RE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION

    FN.DOC = 'F.SCB.DOCUMENT.PROCURE'; F.DOC = ''; R.DOC = ''
    CALL OPF(FN.DOC,F.DOC)

*****ENTER DOC ID *******
    YTEXT = "Enter Document ID: "
    CALL TXTINP(YTEXT, 8, 22, "13", "A")
    DOC.NO = COMI

*-------------------------------------------------------------------------
    CALL F.READ( FN.DOC,DOC.NO, R.DOC, F.DOC, ETEXT)

    XX = R.DOC<DOC.PRO.DEBIT.ACCT>
    YY = R.DOC<DOC.PRO.COMM.ACCT>

    IF XX NE '' OR YY NE '' THEN
        GOSUB INITIATE
        GOSUB PRINT.HEAD
*Line [ 75 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 76 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
        GOSUB CALLDB

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
RETURN
*==============================================================
INITIATE:
    REPORT.ID='REPORT.DOC.DEBIT.RE'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    OUT.AMOUNT.1 = 0
    OUT.AMOUNT.2 = 0
    OUT.AMOUNT.3 = 0
    OUT.AMT.3    = 0
    ID = DOC.NO
    DATE.TO = TODAY[3,6]:"..."
*-------------------GET DATA FROM SCREEN--------------------------------
    CUST.ID   = R.DOC<DOC.PRO.CUSTOMER.ID>
    CHRG.ACCT = R.DOC<DOC.PRO.DEBIT.ACCT>
    COM.ACCT  = R.DOC<DOC.PRO.COMM.ACCT>
    CHRG.TYPE = R.DOC<DOC.PRO.CHARGE.TYPE>
    CHRG.AMT  = R.DOC<DOC.PRO.CHARGE.AMOUNT>
    COM.TYPE  = R.DOC<DOC.PRO.COMMISSION.TYPE>
    COM.AMT   = R.DOC<DOC.PRO.COMMISSION.AMOUNT>
    ISS.DATE  = R.DOC<DOC.PRO.ISSUE.DATE>
    DOC.ID    = R.DOC<DOC.PRO.DEPT.DOC.ID>
    DOC.NOTES = R.DOC<DOC.PRO.NOTES>

*-------------------GET CURR-------------------------------------------*
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,CHRG.ACCT,CHRG.CURR)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,CHRG.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CHRG.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 109 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CHRG.CURR,CHRG.CUR.N)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CHRG.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CHRG.CUR.N=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COM.ACCT,COM.CURR)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,COM.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    COM.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 112 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 132 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,COM.CURR,COM.CUR.N)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,COM.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    COM.CUR.N=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*-------------------GET CREDIT ACCT AND AMOUNT-------------------------*

    CHARGE.TYPE.1 = CHRG.TYPE<1,1>
    CHARGE.TYPE.2 = CHRG.TYPE<1,2>
    CHARGE.AMT.1  = CHRG.AMT<1,1>
    CHARGE.AMT.2  = CHRG.AMT<1,2>

    COMM.TYPE.1 = COM.TYPE
    COMM.AMT.1  = COM.AMT
*-----------------------------------------------------------------------*
    IF CHARGE.TYPE.1 EQ 'COPYCHRG' THEN
        BYAN1 = '������ �����'
    END
    IF  CHARGE.TYPE.1 EQ 'MANGCHRG' THEN
        BYAN1 = '������ ������'
    END
    IF CHARGE.TYPE.2 EQ 'COPYCHRG' THEN
        BYAN2 = '������ �����'
    END
    IF  CHARGE.TYPE.2 EQ 'MANGCHRG' THEN
        BYAN2 = '������ ������'
    END
    IF  COMM.TYPE.1 EQ 'COMDOC' THEN
        BYAN3 = '����� ����� �����'
    END

*-----------------------------------------------------------------------*
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,CHRG.ACCT,CATEG)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,CHRG.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 173 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COM.ACCT,CATEG2)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,COM.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG2=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 143 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG,CATEG.ID)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG.ID=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*Line [ 145 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 189 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG2,CATEG.ID1)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG2,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG.ID1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*Line [ 196 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT ,CHARGE.TYPE.1,CHRG.N)
    F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
    FN.F.ITSS.FT.CHARGE.TYPE = ''
    CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
    CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHARGE.TYPE.1,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
    CHRG.N=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>
*Line [ 203 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT ,CHARGE.TYPE.2,CHRG.N2)
    F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
    FN.F.ITSS.FT.CHARGE.TYPE = ''
    CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
    CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHARGE.TYPE.2,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
    CHRG.N2=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

    TT1 = CHRG.N2[1,14]
    TT2 = R.USER<EB.USE.DEPARTMENT.CODE>

    IF LEN(TT2) EQ 1 THEN
        TT2 = '0':TT2
    END

    TT3 = TT1:TT2

*Line [ 220 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT ,COMM.TYPE.1,COMM.N)
    F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
    FN.F.ITSS.FT.COMMISSION.TYPE = ''
    CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
    CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,COMM.TYPE.1,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
    COMM.N=R.ITSS.FT.COMMISSION.TYPE<FT4.CATEGORY.ACCOUNT>
*Line [ 227 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME       = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME.2     = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS    = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
    CUST.ADDRESS1   = LOCAL.REF<1,CULR.GOVERNORATE>
    CUST.ADDRESS2   = LOCAL.REF<1,CULR.REGION>

*-----------------------------------------------------------------------*
*Line [ 241 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
    F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
    FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
    CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
    CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.ADDRESS1,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
    CUST.ADD2=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
*Line [ 248 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)
    F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
    FN.F.ITSS.SCB.CUS.REGION = ''
    CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
    CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.ADDRESS2,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
    CUST.ADD1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
*-----------------------------------------------------------------------*
    IN.AMOUNT = CHARGE.AMT.1
    IF IN.AMOUNT = '' THEN
        OUT.AMT.1 = ''
    END
    ELSE
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT.1,78,NO.OF.LINES,ER.MSG)
        OUT.AMT.1 = OUT.AMOUNT.1 : ' ' : CHRG.CUR.N : ' ' : '�����'
    END
    IN.AMOUNT = CHARGE.AMT.2
    IF IN.AMOUNT = '' THEN
        OUT.AMT.2 = ''
    END
    ELSE
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT.2,78,NO.OF.LINES,ER.MSG)
        OUT.AMT.2 = OUT.AMOUNT.2 : ' ' : CHRG.CUR.N : ' ' : '�����'
    END
    IN.AMOUNT = COMM.AMT.1
    IF IN.AMOUNT = '' THEN
        OUT.AMT.3 = ''
    END
    ELSE
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT.3,78,NO.OF.LINES,ER.MSG)
        OUT.AMT.3 = OUT.AMOUNT.3 : ' ' : COM.CUR.N : ' ' : '�����'
    END
*-----------------------------------------------------------------------*
    MAT.DATE  = ISS.DATE[7,2]:'/':ISS.DATE[5,2]:"/":ISS.DATE[1,4]
    INPUTTER = R.DOC<DOC.PRO.INPUTTER>
    INP = FIELD(INPUTTER,'_',2)
    AUTH = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 285 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
    F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
    FN.F.ITSS.USER.SIGN.ON.NAME = ''
    CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
    CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
    AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>



    XX   = SPACE(132)  ; XX3  = SPACE(132)   ; XX10 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)   ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)   ; XX9 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)   ; XX8 = SPACE(132)
    XX16  = SPACE(132)  ; XX17  = SPACE(132) ; XX13 = SPACE(132)
    XX12  = SPACE(132)  ; XX13  = SPACE(132) ; XX20 = SPACE(132)
    XX14  = SPACE(132)  ; XX15  = SPACE(132) ; XX21 = SPACE(132)
    XX18  = SPACE(132)  ; XX19  = SPACE(132) ; XX22 = SPACE(132)

    BYAN1 = '' ; BYAN2 = ''

    XX<1,1>[1,15]    = CUST.NAME
    XX1<1,1>[1,15]   = CUST.NAME.2
    XX3<1,1>[1,15]   = CUST.ADDRESS
    XX4<1,1>[1,15]   = CUST.ADD2 : ' ' : CUST.ADD1

*** DECOUNT.CHARGE = DCOUNT(R.NEW(CHRG.ACCT),VM)
*** FOR I = 1 TO 2
    IF  CHRG.ACCT NE '' THEN
        IF CHARGE.AMT.1 NE 0 OR CHARGE.AMT.1 NE '' THEN
            XX7<1,1>[1,15] = '��� ������ : '
            XX6<1,1>[1,15] =  CHRG.ACCT

            XX8<1,1>[1,15] = CATEG.ID

            XX7<1,1>[20,15] = '������ : '
            XX6<1,1>[20,15] = CHRG.CUR.N

            XX7<1,1>[40,15] = '��� ��������� : '
            XX6<1,1>[40,15] = CHRG.N

            XX7<1,1>[60,15]  = '������ : '
            XX6<1,1>[60,15]  = CHARGE.AMT.1

            XX8<1,1>[50,15] = OUT.AMT.1

            XX9<1,1>[1,15] = '������:'
            XX9<1,1>[20,15] = '������ �����'
        END
        IF CHARGE.AMT.2 NE 0 OR CHARGE.AMT.2 NE '' THEN
            XX19<1,1>[1,15] = '��� ������ : '
            XX20<1,1>[1,15] =  CHRG.ACCT

            XX19<1,1>[20,15] = '������ : '
            XX20<1,1>[20,15] = CHRG.CUR.N

            XX19<1,1>[40,15] = '��� ��������� : '
            XX20<1,1>[40,15] = TT3

            XX19<1,1>[60,15]  = '������ : '
            XX20<1,1>[60,15]  = CHARGE.AMT.2

            XX21<1,1>[1,15] = CATEG.ID

            XX21<1,1>[50,15] = OUT.AMT.2

            XX22<1,1>[1,15] = '������:'
            XX22<1,1>[20,15] = '������ ������'

        END
    END
*--------------------------------------------------------------------------*
    IF COM.ACCT NE '' AND (COMM.AMT.1 NE 0 OR COMM.AMT.1 NE '') THEN
        XX13<1,1>[1,15] = '��� ������ : '
        XX14<1,1>[1,15] =  COM.ACCT

        XX15<1,1>[1,15] = CATEG.ID1

        XX13<1,1>[20,15] = '������ : '
        XX14<1,1>[20,15] = COM.CUR.N

        XX13<1,1>[40,15] = '��� ��������� : '
        XX14<1,1>[40,15] = COMM.N

        XX13<1,1>[60,15]  = '������ : '
        XX14<1,1>[60,15]  = COMM.AMT.1

        XX15<1,1>[50,15] = OUT.AMT.3
        XX16<1,1>[1,15] = '������:'
        XX16<1,1>[20,15] = '����� ����� �����'


    END

*---------------------------------------------------------------------*
    XX<1,1>[40,15] = '����� ���� : '
    XX<1,1>[54,15] = MAT.DATE

    XX17<1,1>[1,15]  = '������'
    XX18<1,1>[1,15] = DOC.NO

    XX17<1,1>[30,15]  = '������'
    XX18<1,1>[30,15]  = INP

**XX5<1,1>[3,15]  = '������ �������'
**XX5<1,1>[20,15] = OUT.AMT.1

    XX17<1,1>[60,15]  = '��� �������'
    XX18<1,1>[60,15] = AUTHI

**************** NEW FIELDS *************
    XX2<1,1>[1,15]  = '��� �������'
    XX2<1,1>[30,15] = DOC.ID
    XX5<1,1>[1,15]  = '�������'
    XX5<1,1>[30,15] = DOC.NOTES

****TEXT = "DDDDD" ; CALL REM
    PRINT XX<1,1>
    PRINT XX1<1,1>
* PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT STR('-',82)
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT XX6<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX19<1,1>
    PRINT XX20<1,1>
    PRINT XX21<1,1>
    PRINT XX22<1,1>
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX2<1,1>
    PRINT XX5<1,1>
    PRINT STR('-',82)
    PRINT XX17<1,1>
    PRINT XX18<1,1>

    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 438 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":"REPORT.DOC.DEBIT.RE"
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"����� : ":YYBRN
    PR.HD :="'L'":SPACE(20):"����� ��� �������� ��������-����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*  END
*END
RETURN
END
