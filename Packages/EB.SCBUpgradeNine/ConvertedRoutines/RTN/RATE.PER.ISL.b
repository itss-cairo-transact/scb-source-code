* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE RATE.PER.ISL(ARG)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST

    ARG = ID.NEW
    AA  = R.NEW(LD.INTEREST.SPREAD)
    BB  = R.NEW(LD.INTEREST.KEY)
    LD.CUR = R.NEW(LD.CURRENCY)
    TT = " ��� ����� ����� ������� ��� ���������"
    FN.BA = "FBNK.BASIC.INTEREST"  ; F.BA = ""
    CALL OPF(FN.BA,F.BA)
    BB.ID=BB:LD.CUR:'...'
    T.SEL = "SELECT FBNK.BASIC.INTEREST WITH @ID LIKE ": BB.ID :" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.BA,KEY.LIST<SELECTED>,R.BA,F.BA,E1)
    RATE.CBE = R.BA<EB.BIN.INTEREST.RATE>

*    TEXT=RATE.CBE:'-':KEY.LIST<SELECTED>;CALL REM
*Line [ 41 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,ARG,RATE)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ARG,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
RATE=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.INTEREST.RATE>
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.NEW.INT.RATE,ARG,RATE2)
F.ITSS.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
FN.F.ITSS.LD.LOANS.AND.DEPOSITS = ''
CALL OPF(F.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS)
CALL F.READ(F.ITSS.LD.LOANS.AND.DEPOSITS,ARG,R.ITSS.LD.LOANS.AND.DEPOSITS,FN.F.ITSS.LD.LOANS.AND.DEPOSITS,ERROR.LD.LOANS.AND.DEPOSITS)
RATE2=R.ITSS.LD.LOANS.AND.DEPOSITS<LD.NEW.INT.RATE>
    IF RATE2 EQ '' THEN
*        RR = RATE + AA
*        RR = RR :"%"
        ARG = "�����" : " (" :RATE.CBE: "%)"
    END
    IF RATE2 NE '' THEN
*        RR = RATE2 + AA
*        RR = RR :"%"
        ARG = "�����" : " (" :RATE.CBE: "%)"
    END
    RETURN
END
