* @ValidationCode : MjoyMDYyOTAxODg3OkNwMTI1MjoxNjQ0OTM4MTc2NjQ5OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:16:16
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
SUBROUTINE REPORT.FT.DEBIT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.FT.DEBIT'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID     = COMI
    FN.LD     = 'FBNK.FUNDS.TRANSFER' ; F.LD = ''
    FN.LD.HIS = 'FBNK.FUNDS.TRANSFER$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)
    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
***    CALL F.READ(FN.LD.HIS,COMI,R.LD.HIS,F.LD.HIS,E2)
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E2)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    F.ITSS.DATES = 'F.DATES'
    FN.F.ITSS.DATES = ''
    CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
    CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
    DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD = TODAY
*------------------------------------------------------------------------
    ID = COMI
    CUS.ID = R.LD<FT.DEBIT.ACCT.NO>
    TEXT = CUS.ID ; CALL REM
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,CUS.ID,CUSNO)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,CUS.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CUSNO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUSNO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>
    TEXT = CUST.NAME ; CALL REM
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,CUS.ID,CATEG)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,CUS.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CATNAME)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATNAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF1)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUSNO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END
    IF CUS.ID[1,2] EQ 'PL' THEN
        XX = CUS.ID[3,8]
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,XX,CATTT1)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,XX,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CATTT1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        CUST.NAME   = CATTT1
        CUST.NAME.1 = CATTT1
    END
    AMOUNT = R.LD<FT.DEBIT.AMOUNT>

    BRANCH.ID = R.LD<FT.DEPT.CODE>

*Line [ 148 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

    DAT = R.LD<FT.DEBIT.VALUE.DATE>

    DAT2 = TODAY
    V.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    V.DATE2  = DAT2[7,2]:'/':DAT2[5,2]:"/":DAT2[1,4]
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.LD<FT.DEBIT.CURRENCY>
*Line [ 117 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    CHQ.NO = R.LD<FT.CHEQUE.NUMBER>

    TOTAL.AMT = R.LD<FT.AMOUNT.DEBITED>[4,15]
    IN.AMOUNT = TOTAL.AMT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.LD<FT.DEBIT.CURRENCY>
*Line [ 128 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 183 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    OUT.AMT1 = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    COM.AMT = R.LD<FT.COMMISSION.AMT>
    CHG.AMT = R.LD<FT.CHARGE.AMT>
    TOT.CHG = R.LD<FT.TOTAL.CHARGE.AMOUNT>
    BENCUS  = R.LD<FT.LOCAL.REF><FTLR.BENEFICIARY.CUS>

    INPUTT    = R.LD<FT.INPUTTER>
    INPUTTER  = FIELD(INPUTT,'_',2)
    AUTH1     = R.LD<FT.AUTHORISER>
    AUTH      = FIELD(AUTH1,'_',2)

    XX    = SPACE(132)  ; XX3   = SPACE(132) ; XX9   = SPACE(132)
    XX1   = SPACE(132)  ; XX4   = SPACE(132) ; XX11  = SPACE(132)
    XX2   = SPACE(132)  ; XX5   = SPACE(132) ; XX10  = SPACE(132)
    XX6   = SPACE(132)  ; XX7   = SPACE(132) ; XX8   = SPACE(132)
    XX12  = SPACE(132)  ; XX13  = SPACE(132) ; XX14  = SPACE(132)
    XX15  = SPACE(132)  ; XX16  = SPACE(132) ; XX17  = SPACE(132)
    XX18  = SPACE(132)  ; XX19  = SPACE(132) ; XX20  = SPACE(132)
    XX21  = SPACE(132)

    XX<1,1>[3,15]    =  '��� ������ :'
    XX1<1,1>[3,15]   =  '-------------'
    XX2<1,1>[3,15]   =  CUS.ID

**  XX10<1,1>[3,15]     = '�����       : '
**  XX10<1,1>[20,35]    = CUST.NAME :' ':CUST.NAME1
* XX1<1,1>[20,35]   = CUST.NAME1

    XX<1,1>[30,15]   = '���� �����: '
    XX1<1,1>[30,15]  = '--------------'
    XX2<1,1>[30,15]  = AMOUNT

    XX<1,1>[55,15]   = '����� ���� : '
    XX1<1,1>[55,15]  ='--------------'
    XX2<1,1>[55,15]  = V.DATE

    XX3<1,1>[3,15]    = '��� ������:'
    XX4<1,1>[3,15]   = '-------------'
    XX5<1,1>[3,15]   = CATNAME

    XX3<1,1>[30,15]   = ' ������: '
    XX4<1,1>[30,15]  = '------------'
    XX5<1,1>[30,15]  =  CUR

    XX3<1,1>[55,15]   = ' ��� ������ : '
    XX4<1,1>[55,15]  = '------------'
    XX5<1,1>[55,15]  = CUST.NAME
    XX6<1,1>[55,15]  = CUST.NAME1
    XX7<1,1>[55,15]  = CUST.ADDRESS

    XX8<1,1>[3,15]   = '������ ������� :'
    XX9<1,1>[3,15]   = '-----------------'
    XX10<1,1>[3,15]   = OUT.AMT


    XX11<1,1>[3,15]   = ' ��� ����� :' : CHQ.NO
    XX11<1,1>[40,16]  = '������ �������� :' : TOTAL.AMT


    XX12<1,1>[3,15]  = '������ �������� �������: '
    XX13<1,1>[3,15]  = '------------------------'
    XX14<1,1>[3,15]  = OUT.AMT1

    XX15<1,1>[3,15]  = ' �������� ��������� :'
    XX16<1,1>[3,15]  = '-----------------------'
    XX17<1,1>[3,15]  = COM.AMT[4,10]
    XX18<1,1>[3,15]  = CHG.AMT[4,10]
    XX19<1,1>[3,15]  = '������ �������� ��������� : ': TOT.CHG[4,10]

    XX20<1,1>[3,15]  = "��� �������"
    XX20<1,1>[30,15] = "��� �������"
    XX20<1,1>[58,15] = "��� �������"

    XX21<1,1>[3,15]  = INPUTTER
    XX21<1,1>[30,15] = COMI
    XX21<1,1>[58,15] = AUTH
*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
**** PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"����� ��� / ����� ��� �����" : SPACE(5) :"������� : ":T.DAY
    PR.HD :="'L'":"------------------------------------------------------------------------------------------- "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
*PRINT STR(' ',82)
    PRINT XX3<1,1>
* PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT XX7<1,1>
    PRINT STR('-',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX17<1,1>
***PRINT STR('-',82)
    PRINT XX18<1,1>
    PRINT XX19<1,1>
    PRINT XX20<1,1>
    PRINT STR('-',82)
    PRINT XX21<1,1>
*===============================================================
RETURN
END
