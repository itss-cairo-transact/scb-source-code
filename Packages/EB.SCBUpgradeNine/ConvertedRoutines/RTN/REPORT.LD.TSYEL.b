* @ValidationCode : MjotMTMwNTkzMzgwMDpDcDEyNTI6MTY0NDkzOTYzMjIyMTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:40:32
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-100</Rating>
*-----------------------------------------------------------------------------
******************************************NI7OOOOOOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.LD.TSYEL
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
*Line [  24] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23& ADD CHANGE I_CU.LOCAL.REF TO I_CU.LOCAL.REFS
    $INSERT  I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_LD.LOCAL.REFS
*-----------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    TEXT = "��� ����� ���������" ; CALL REM

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.LD.TSYEL'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 64 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    F.ITSS.DATES = 'F.DATES'
    FN.F.ITSS.DATES = ''
    CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
    CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
    DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD = TODAY
*------------------------------------------------------------------------
*Line [ 58 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Re-print Contribution Slip ?', 8, 23, '1.1', @FM:'Y_N')
    IF COMI[1,1] = 'Y' THEN
        CALL TXTINP('Enter Transaction Reference', 8, 23, '12', 'ANY')
    END
    YY = COMI:'...'
    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$HIS WITH @ID LIKE ":YY
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.LD.HIS,KEY.LIST<SELECTED>,R.LD.HIS,F.LD.HIS,E2)
    CURRNO = R.LD.HIS<LD.CURR.NO>

    CUS.ID = R.LD.HIS<LD.CUSTOMER.ID>
*Line [ 85 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>
*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END

    ACC   = R.LD.HIS<LD.INT.LIQ.ACCT>

    AMOUNT = R.LD.HIS<LD.REIMBURSE.AMOUNT>
    INTER = R.LD.HIS<LD.TOT.INTEREST.AMT>

    BRANCH.ID = R.LD.HIS<LD.MIS.ACCT.OFFICER>

*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>



*    IN.AMOUNT = AMOUNT
*    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.LD.HIS<LD.CURRENCY>
*Line [ 104 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

*    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
********* EDIT BY NESSMA 2010/11/14
    ACCT.INT.LIQ = R.LD.HIS<LD.INT.LIQ.ACCT>
    ACCT.TXT     = "���� ����� ������"

    TOT.INTERSET.AMT = R.LD.HIS<LD.TOT.INTEREST.AMT>
    INTT = R.LD.HIS<LD.TOT.INTEREST.AMT>
****TOT  = R.LD.HIS<LD.REIMBURSE.AMOUNT> + INTT
    TOT  = R.LD.HIS<LD.REIMBURSE.AMOUNT>

    CALL WORDS.ARABIC.DEAL(TOT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
***********************************
    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX12 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
    XX.1 = SPACE(132)
    XX.2 = SPACE(132)
    XX.3 = SPACE(132)

    XX<1,1>[3,15]       =  '��� �������   '
    XX<1,1>[45,15]      =  COMI

    XX10<1,1>[3,15]     = '�����       : '
    XX10<1,1>[20,35]    = CUST.NAME :' ':CUST.NAME1

    XX11<1,1>[70,15]    = '���� ����� ��� �������   '
    XX12<1,1>[75,15]    = ACC

    XX1<1,1>[3,15]      = '�������      :'
    XX1<1,1>[20,35]     = CUST.ADDRESS
*** EDIT BY NESSMA 2010/11/14 ********
    XX.1<1,1>[3,15]     = ACCT.TXT
    XX.1<1,1>[20,20]    = ACCT.INT.LIQ
***************************************
    XX11<1,1>[50,15]    = '���� �������  '
    XX12<1,1>[50,15]    = AMOUNT

    XX11<1,1>[3,15]     = ' ������ '
    XX12<1,1>[3,15]     = CUR

    XX11<1,1>[20,15]     = ' ���� ������ '
*   XX12<1,1>[20,15]     = TOT.INTERSET.AMT
    XX12<1,1>[20,15]     = 0

    XX5<1,1>[3,15]  = '���� ������� ���� ��� ���� ��� ����� ���� ������� ��� :'  : COMI
    XX6<1,1>[3,15]  = '������ ���� ���� ������'
*   XX6<1,1>[20,15] = TOT

    XX6<1,1>[20,15] = AMOUNT
    XX8<1,1>[1,15]  = '������ ������� :'
    XX8<1,1>[20,15] = OUT.AMT
    XX7<1,1>[65,15] = '�� ��� ���� ������'
****
    XX.2<1,1>[2,20] = TOT.INTERSET.AMT
    XX.3<1,1>[2,20] = TOT
****
*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"����� ����� ������ �������"
***EDIT BY NESSMA 2010/11/14 *********
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"TSYEL"
**************************************
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX10<1,1>
    PRINT XX1<1,1>
*****
    PRINT XX.1<1,1>
*****
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
* PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT STR(' ',82)
***
* PRINT XX.2<1,1>
* PRINT XX.3<1,1>
***
* NEXT I
*===============================================================
RETURN
END
