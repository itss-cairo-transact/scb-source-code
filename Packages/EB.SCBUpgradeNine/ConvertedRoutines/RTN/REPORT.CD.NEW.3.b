* @ValidationCode : MjoxMDk3MjMyNDA6Q3AxMjUyOjE2NDQ5MzY3NDYxOTU6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:52:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-------------------------------
* CREATE BY NAHRAWY
* EDIT BY NESSMA ON 2013/05/09
*-------------------------------
SUBROUTINE REPORT.CD.NEW.3

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CD.TYPES
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*--------------------------------------------------

    IF R.NEW(LD.CURRENCY) EQ 'EGP' THEN
        COMP = ID.COMPANY
        R.VERSION.CONTROL = 'LD.LOANS.AND.DEPOSITS'

        FN.COM = 'F.COMPANY' ;  F.COM  = ''
        CALL OPF ( FN.COM,F.COM)

        CDNO  = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>

        GOSUB INITIATE
        GOSUB PROCESS

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.CD.NEW.3'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*-------
    FN.LD     ='FBNK.LD.LOANS.AND.DEPOSITS'      ; F.LD     = ''
    FN.LD.HIS ='FBNK.LD.LOANS.AND.DEPOSITS$HIS'  ; F.LD.HIS = ''
    FN.CORD = 'FBNK.BASIC.INTEREST' ;F.CORD = '' ; R.CORD   = ''
    CALL OPF(FN.CORD,F.CORD)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    DATE1 = ''
    DATE2 = ''
    XDATE = ''
    VVV   = ''
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*------------------------------------------------------------------------
    AC.DEBIT = R.NEW(LD.DRAWDOWN.ACCOUNT)
    ACLD     = R.NEW(LD.PRIN.LIQ.ACCT)
    ACINT    = R.NEW(LD.INT.LIQ.ACCT)

*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.DEBIT,CUS.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,AC.DEBIT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>

*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END
    CATEG   = R.NEW(LD.CATEGORY)
    CDNO  = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CATEGNAME1)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEGNAME1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
    CATEGNAME = FIELD(CATEGNAME1,'-',1)

    AMT     = R.NEW(LD.AMOUNT)
    CUR     = R.NEW(LD.CURRENCY)
    AMTALL  =  AMT
*Line [ 120 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR,CUR22)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR22=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    DATE1   = R.NEW(LD.VALUE.DATE)
    DATE2   = R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>
    IF DATE2 LT DATE1 THEN
        XDATE =  DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]
    END ELSE
        IF DATE2 GT DATE1 THEN
            XDATE =  DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
        END
    END
    IF DATE2  = '' THEN
        XDATE = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
    END

    FINDATE1  = R.NEW(LD.FIN.MAT.DATE)
    FINDATE   = FINDATE1[1,4]:'/':FINDATE1[5,2]:"/":FINDATE1[7,2]
    NAME11    = R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF>
    CDTYPE    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
    RAT       = R.NEW(LD.INTEREST.RATE)
    RAT2      = R.NEW(LD.INTEREST.KEY)
    RATE3     = RAT2:'...'

    T.SEL     = "SELECT FBNK.BASIC.INTEREST WITH @ID LIKE ":RATE3:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.CORD,KEY.LIST<SELECTED>,R.CORD,F.CORD,READ.ERR)
    RATEF     = RAT
    CD.TYP    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>

    BRANCH.ID = R.NEW(LD.MIS.ACCT.OFFICER)
*Line [ 180 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 187 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('SCB.CD.TYPES':@FM:CD.TYPES.DESCRIPTION,CD.TYP,DESC)
    F.ITSS.SCB.CD.TYPES = 'F.SCB.CD.TYPES'
    FN.F.ITSS.SCB.CD.TYPES = ''
    CALL OPF(F.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES)
    CALL F.READ(F.ITSS.SCB.CD.TYPES,CD.TYP,R.ITSS.SCB.CD.TYPES,FN.F.ITSS.SCB.CD.TYPES,ERROR.SCB.CD.TYPES)
    DESC=R.ITSS.SCB.CD.TYPES<CD.TYPES.DESCRIPTION>
    TYP     = FIELD(DESC,'-',2)
    TYPENEW = FIELD(CD.TYP,'-',3)
    RATENO  = RATEF:" ":"%":"����":" ":TYP

    IF TYPENEW EQ '1M' THEN
        VVV = "X1"
    END
    IF TYPENEW EQ '3M' THEN
        VVV = "X3"
    END
    IF TYPENEW EQ '6M' THEN
        VVV = "X6"
    END
    IF TYPENEW EQ '12M' THEN
        VVV = "X12"
    END

    IN.AMOUNT = AMT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR22 : ' ' : '�����'

    IF TYPENEW EQ '1M' THEN
        RATENO1  = RATENO
        RATENO3  = ''
        RATENO6  = ''
        RATENO12 = ''
    END
    IF TYPENEW EQ '3M' THEN
        RATENO3  = RATENO
        RATENO1  = ''
        RATENO6  = ''
        RATENO12 = ''
    END
    IF TYPENEW EQ '6M' THEN
        RATENO1  = ''
        RATENO3  = ''
        RATENO6  = RATENO
        RATENO12 = ''
    END
    IF TYPENEW EQ '12M' THEN
        RATENO1  = ''
        RATENO3  = ''
        RATENO6  = ''
        RATENO12 = RATENO
    END
*-------------------------------
    IF TYPENEW EQ '1M' THEN
        CDNO1 = CDNO
        CDNO3 = ''
        CDNO6 = ''
        CDNO12 = ''
    END
    IF TYPENEW EQ '3M' THEN
        CDNO1 = ''
        CDNO3 = CDNO
        CDNO6 = ''
        CDNO12 = ''
    END
    IF TYPENEW EQ '6M' THEN
        CDNO1 = ''
        CDNO3 = ''
        CDNO6 = CDNO
        CDNO12 = ''
    END
    IF TYPENEW EQ '12M' THEN
        CDNO1 = ''
        CDNO3 = ''
        CDNO6 = ''
        CDNO12 = CDNO
    END
*-------------------------------
    IF TYPENEW EQ '1M' THEN
        BEGIN CASE
            CASE CDNO EQ ''
                CDNO1 = 1
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = ''
            CASE CDNO EQ 0
                CDNO1 = 1
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = ''
            CASE CDNO NE 0
                CDNO1 = 1
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = ''
            CASE CDNO NE ''
                CDNO1 = 1
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = ''
        END CASE
    END

    IF TYPENEW EQ '3M' THEN
        BEGIN CASE
            CASE CDNO EQ ''
                CDNO1 = ''
                CDNO3 = 1
                CDNO6 = ''
                CDNO12 = ''
            CASE CDNO EQ 0
                CDNO1 = ''
                CDNO3 = 1
                CDNO6 = ''
                CDNO12 = ''
            CASE CDNO NE 0
                CDNO1 = ''
                CDNO3 = 1
                CDNO6 = ''
                CDNO12 = ''
            CASE CDNO NE ''
                CDNO1 = ''
                CDNO3 = 1
                CDNO6 = ''
                CDNO12 = ''
        END CASE
    END

    IF TYPENEW EQ '6M' THEN
        BEGIN CASE
            CASE CDNO EQ ''
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = 1
                CDNO12 = ''
            CASE CDNO EQ 0
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = 1
                CDNO12 = ''
            CASE CDNO NE 0
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = 1
                CDNO12 = ''
            CASE CDNO NE ''
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = 1
                CDNO12 = ''
        END CASE
    END
    IF TYPENEW EQ '12M' THEN
        BEGIN CASE
            CASE CDNO EQ ''
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = 1
            CASE CDNO EQ 0
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = 1
            CASE CDNO NE 0
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = 1
            CASE CDNO NE ''
                CDNO1 = ''
                CDNO3 = ''
                CDNO6 = ''
                CDNO12 = 1
        END CASE
    END

    IF TYPENEW EQ '1M' THEN
        AMTALL1  = AMTALL
        AMTALL3  = ''
        AMTALL6  = ''
        AMTALL12 = ''
    END
    IF TYPENEW EQ '3M' THEN
        AMTALL3  = AMTALL
        AMTALL1  = ''
        AMTALL6  = ''
        AMTALL12 = ''
    END
    IF TYPENEW EQ '6M' THEN
        AMTALL3  = ''
        AMTALL1  = ''
        AMTALL6  = AMTALL
        AMTALL12 = ''
    END
    IF TYPENEW EQ '12M' THEN
        AMTALL3  = ''
        AMTALL1  = ''
        AMTALL6  = ''
        AMTALL12 = AMTALL
    END

    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX13 = SPACE(132) ; XX14 = SPACE(132)  ; XX24 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132) ; XX15 = SPACE(132)  ; XX23 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132) ; XX16 = SPACE(132)  ; XX22 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132) ; XX19 = SPACE(132)  ; XX20 = SPACE(132)
    XX30 = SPACE(132)
    XX90 = SPACE(132)  ; XX91 = SPACE(132) ; XX92 = SPACE(132)
    XX93 = SPACE(132)  ; XX94 = SPACE(132) ; XX95 = SPACE(132)

    XX30<1,1>[3,15]      =  '��� ������� :'
    XX30<1,1>[45,15]     = ID.NEW

    XX<1,1>[3,15]      =  '����� :'
    XX<1,1>[45,15]     = NAME11

    XX2<1,1>[3,15]     = '��� ������ : '
    XX2<1,1>[45,35]    = ACLD

    XX3<1,1>[3,15]     = '���� �������: '
    XX3<1,1>[45,35]    = AMT
    XX11<1,1>[45,35]   = OUT.AMT

    XX10<1,1>[3,15]     = '��� ������� :'
    XX10<1,1>[45,35]    =  CATEGNAME

    XX4<1,1>[3,15]     = '������ ����:'
    XX4<1,1>[45,15]    = RATENO1

    XX15<1,1>[3,15]     = '������3 ����:'
    XX15<1,1>[45,15]    = RATENO3

    XX90<1,1>[3,15]     = '������ 6 ����'
    XX90<1,1>[45,15]    = RATENO6

    XX91<1,1>[3,15]     = '������ ������'
    XX91<1,1>[45,15]    = RATENO12

    XX20<1,1>[3,15]     = '��� �������� ����:'
    XX20<1,1>[45,15]    = CDNO1

    XX19<1,1>[3,15]     = '��� �������� 3 ���� :'
    XX19<1,1>[45,15]    = CDNO3

    XX92<1,1>[3,15]     = '��� �������� 6 ����'
    XX92<1,1>[45,15]    = CDNO6

    XX93<1,1>[3,15]     = '��� �������� ������'
    XX93<1,1>[45,15]    = CDNO12

    XX22<1,1>[3,15]     = '���� �������� ����:'
    XX22<1,1>[45,15]    = AMTALL1

    XX23<1,1>[3,15]     = '���� �������� 3 ����:'
    XX23<1,1>[45,15]    = AMTALL3

    XX94<1,1>[3,15]     = '���� �������� 6 ����'
    XX94<1,1>[45,15]    = AMTALL6

    XX95<1,1>[3,15]     = '���� �������� ����'
    XX95<1,1>[45,15]    = AMTALL12

    XX13<1,1>[3,15]     = '����� ������ ������ ��� ����:'
    XX13<1,1>[45,35]    =  ACINT

    XX14<1,1>[3,15]     = '����� ���� ������� ��� ���� ��� :'
    XX14<1,1>[45,35]    =  ACLD

    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = R.NEW(LD.VALUE.DATE)
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  = "'L'":SPACE(1):VVV
    IF V$FUNCTION = 'A' THEN
        PR.HD  = "'L'":SPACE(1):VVV : " " :"AUTH"
    END
    PR.HD := "'L'":SPACE(1):"����� ������":SPACE(20) : TYP
    PR.HD :="'L'":SPACE(1):"��� ���� ������"

*Line [ 465 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACLD,CUS.LD)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACLD,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CUS.LD=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 472 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.LD,CUS.BOOK)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUS.LD,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    CUS.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>

    IF CUS.BOOK EQ 'EG0010011' THEN
        BRN.CODE = R.NEW(LD.CO.CODE)
*Line [ 482 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.CODE,YYBRN1)
        F.ITSS.COMPANY = 'F.COMPANY'
        FN.F.ITSS.COMPANY = ''
        CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
        CALL F.READ(F.ITSS.COMPANY,BRN.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
        YYBRN1=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        PR.HD :="'L'":"����� : ":YYBRN1
    END
    IF CUS.BOOK NE 'EG0010011' THEN
        PR.HD :="'L'":"����� : " : YYBRN
    END

    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":SPACE(15):"���� ������� ������� ���  "
    PR.HD :="'L'":SPACE(15):" ������ ������ ������� ������"
    PR.HD :="'L'":"REPORT.CD.NEW.1" :SPACE(20):"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PRINT
    HEADING PR.HD

    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX15<1,1>
    PRINT STR(' ',82)
    PRINT XX90<1,1>
    PRINT STR(' ',82)
    PRINT XX91<1,1>
    PRINT STR(' ',82)
    PRINT XX20<1,1>
    PRINT STR(' ',82)
    PRINT XX19<1,1>
    PRINT STR(' ',82)
    PRINT XX92<1,1>
    PRINT STR(' ',82)
    PRINT XX93<1,1>
    PRINT STR(' ',82)
    PRINT XX22<1,1>
    PRINT STR('',82)
    PRINT XX23<1,1>
    PRINT STR('',82)
    PRINT XX94<1,1>
    PRINT STR(' ',82)
    PRINT XX95<1,1>
    PRINT STR(' ',82)
    PRINT XX13<1,1>
    PRINT STR('',82)
    PRINT XX14<1,1>
    PRINT STR('',82)
    PRINT XX16<1,1>
    PRINT STR('',82)
    PRINT XX30<1,1>
    PRINT STR('=',82)
RETURN
END
