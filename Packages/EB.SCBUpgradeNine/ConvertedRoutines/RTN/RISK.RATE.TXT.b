* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-55</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE RISK.RATE.TXT
*    PROGRAM RISK.RATE.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.RAT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*-----------------------------------------------------------------------

    OPENSEQ "/home/signat" , "risk_reating.scb" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"risk_reating.scb"
        HUSH OFF
    END
    OPENSEQ "/home/signat" , "risk_reating.scb" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE risk_reating.scb  CREATED IN /home/signat'
        END ELSE
            STOP 'Cannot create risk_reating.scb File IN /home/signat'
        END
    END
*----------------------------------------------------------------
    FN.RISK = 'F.SCB.RISK.RAT'; F.RISK = '' ; R.RISK = ''
    CALL OPF(FN.RISK,F.RISK)
    T.DATE = TODAY[1,6]:'01'
    CALL CDT("",T.DATE,'-1C')
    NEW.T.DATE = T.DATE[7,2]:T.DATE[5,2]:T.DATE[1,4]

*********SPECIAL RISK RATING ************************************
    DIR.NAME = "&SAVEDLISTS&"
    TEXT.FILE = "SPECIAL.RISK.txt"
    VAR.FILE = ''
    EOF = ''
    OPENSEQ DIR.NAME,TEXT.FILE TO VAR.FILE ELSE

        TEXT = "ERROR OPEN FILE ":TEXT.FILE ; CALL REM
        RETURN
    END

    LOOP WHILE NOT(EOF)

        READSEQ Line FROM VAR.FILE THEN
            CBE.NO     = TRIM(FIELD(Line,"|",1),'',"D")
            RISK.RATE  = TRIM(FIELD(Line,"|",2),'',"D")

            CALL F.READ(FN.RISK,CBE.NO,R.RISK,F.RISK,E2)
            R.RISK<RISK.GADARA.CODE> = RISK.RATE

            CALL F.WRITE(FN.RISK,CBE.NO,R.RISK)
            CALL JOURNAL.UPDATE(CBE.NO)

        END ELSE
            EOF = 'END OF FILE'
        END

    REPEAT
*----------------------------------------------------------------

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    Q = 0
    T.SEL = "SELECT F.SCB.RISK.RAT BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        BB.DATA = '1700':NEW.T.DATE:'00000000000000000000'
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        FOR I = 1 TO SELECTED
            CALL F.READ(FN.RISK,KEY.LIST<I>,R.RISK,F.RISK,E1)
            CBE.NO   = KEY.LIST<I>
            RISK     = R.RISK<RISK.GADARA.CODE>
            NAB.BAL  = R.RISK<RISK.NAB.BAL>/1000
            NAB.DATE = R.RISK<RISK.NAB.DATE>
            NEW.BAL  = FIELD(NAB.BAL,'.',1)
            IF NEW.BAL EQ '' THEN
                NEW.BAL = NAB.BAL
            END

*NEW.DATE = NAB.DATE[7,2]:NAB.DATE[5,2]:NAB.DATE[1,4]
*BB.DATA  = FMT(CBE.NO,"R%12"):FMT(RISK,"R%2"):FMT(NEW.BAL,"R%10"):FMT(NEW.DATE,"R%8")

            IF ((RISK EQ 8 OR RISK EQ 9 OR RISK EQ 10) AND (NEW.BAL EQ '' OR NEW.BAL EQ 0)) THEN
                NAB.DATE = '19010101'
                NEW.BAL  = 1

                R.RISK<RISK.NAB.DATE>    = NAB.DATE
                R.RISK<RISK.NAB.BAL>     = NEW.BAL
                CALL F.WRITE(FN.RISK,CBE.NO,R.RISK)
                CALL JOURNAL.UPDATE(CBE.NO)
            END
            NEW.DATE = NAB.DATE[7,2]:NAB.DATE[5,2]:NAB.DATE[1,4]

            BB.DATA  = FMT(CBE.NO,"R%12"):FMT(RISK,"R%2"):FMT(NEW.BAL,"R%10"):FMT(NEW.DATE,"R%8")
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END
*************************************************************
END
