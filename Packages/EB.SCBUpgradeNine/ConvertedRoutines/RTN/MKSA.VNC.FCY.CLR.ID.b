* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>292</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MKSA.VNC.FCY.CLR.ID

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN

    CLR.DATE = TODAY
    DAY.NOM = OCONV(DATE(),"DW")

    IF (DAY.NOM EQ 7 OR DAY.NOM EQ 2 ) THEN
        CALL CDT('', CLR.DATE,"+2C")
    END
    IF (DAY.NOM EQ 1 OR DAY.NOM EQ 3 OR DAY.NOM EQ 4  ) THEN
        CALL CDT('', CLR.DATE,"+1W")
    END

*    IF CLR.DATE NE 7 OR CLR.DATE NE 2 OR CLR.DATE NE 4 THEN
*
*        DAY.NOM = OCONV(CLR.DATE,"DW")
*
*        IF (DAY.NOM EQ 7 OR DAY.NOM EQ 2 ) THEN
*            CALL CDT('', CLR.DATE,"+2W")
*        END
*        IF (DAY.NOM EQ 1 OR DAY.NOM EQ 3 OR DAY.NOM EQ 4  ) THEN
*            CALL CDT('', CLR.DATE,"+1W")
*        END
*    END


    ID.NEW                   =  CLR.DATE
    R.NEW(SCB.BR.BOOK.DATE)  =  CLR.DATE
*    ID.NEW                   =  TODAY
*    R.NEW(SCB.BR.BOOK.DATE)  =  TODAY
    RETURN
**------------------------------------------------------------------**

END
