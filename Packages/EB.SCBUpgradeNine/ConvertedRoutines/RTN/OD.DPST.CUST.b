* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>193</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OD.DPST.CUST

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY.PARAM

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 46 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 47 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB


    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='OD.DPST.CUST'
    CALL PRINTER.ON(REPORT.ID,'')
    YTEXT = "  ���� ��� ������  " :"   NNNNNNNN   "
    CALL TXTINP(YTEXT, 8, 30, "30", "A")

    RETURN
*===============================================================
CALLDB:

    FN.OD = 'FBNK.ACCOUNT' ; F.OD = ''
    CALL OPF(FN.OD,F.OD)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
*    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 1201 OR CATEGORY EQ 1202 AND WORKING.BALANCE LT 0  AND WORKING.BALANCE NE '' AND CUSTOMER EQ ": COMI
     T.SEL = "SELECT FBNK.ACCOUNT WITH (CATEGORY GE 1101 AND  CATEGORY LE 1202  OR CATEGORY GE 1290 AND  CATEGORY LE 1599 ) AND WORKING.BALANCE LT 0  AND WORKING.BALANCE NE '' AND CUSTOMER EQ ": COMI

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEMP.CUST='' ; TEMP.AC = ''

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.OD,KEY.LIST<I>,R.OD,F.OD,E1)
        CUST.ID = R.OD<AC.CUSTOMER>
        WORK.BAL=R.OD<AC.WORKING.BALANCE>
        CURR.C=R.OD<AC.CURRENCY>
*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CURR.C,CURR)
F.ITSS.CURRENCY.PARAM = 'F.CURRENCY.PARAM'
FN.F.ITSS.CURRENCY.PARAM = ''
CALL OPF(F.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM)
CALL F.READ(F.ITSS.CURRENCY.PARAM,CURR.C,R.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM,ERROR.CURRENCY.PARAM)
CURR=R.ITSS.CURRENCY.PARAM<EB.CUP.CCY.NAME>
        T.SEL.LD = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CUSTOMER.ID  EQ ": CUST.ID : " AND CATEGORY GE 21001 AND CATEGORY LE 21019 AND AMOUNT NE 0 AND STATUS NE LIQ BY @ID BY CURRENCY "
        CALL EB.READLIST(T.SEL.LD,KEY.LIST.LD,"",SELECTED.LD,ER.MSG)
        IF SELECTED.LD THEN
*************************************************
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
            OLD.NO=LOC.REF<1,CULR.OLD.CUST.ID>
*************************************************
**IF  TEMP.CUST NE CUST.ID THEN
            TEMP.CUST=CUST.ID
            IF I = 1 THEN
                XX<1,ZZ>  =STR(' ',40)
**               PRINT XX<1,ZZ>
                ZZ=ZZ+1
                XX<1,ZZ>[1,07]  = OLD.NO
                XX<1,ZZ>[15,40]  = CUST.NAME

                PRINT XX<1,ZZ>
                ZZ=ZZ+1

                FOR J = 1 TO SELECTED.LD

                    CALL F.READ(FN.LD,KEY.LIST.LD<J>,R.LD,F.LD,E1)
                    AMT.LD = R.LD<LD.AMOUNT>
                    CURR.LD.C = R.LD<LD.CURRENCY>
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('CURRENCY.PARAM':@FM:EB.CUP.CCY.NAME,CURR.LD.C,CURR.LD)
F.ITSS.CURRENCY.PARAM = 'F.CURRENCY.PARAM'
FN.F.ITSS.CURRENCY.PARAM = ''
CALL OPF(F.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM)
CALL F.READ(F.ITSS.CURRENCY.PARAM,CURR.LD.C,R.ITSS.CURRENCY.PARAM,FN.F.ITSS.CURRENCY.PARAM,ERROR.CURRENCY.PARAM)
CURR.LD=R.ITSS.CURRENCY.PARAM<EB.CUP.CCY.NAME>
                    FIN.DATE = R.LD<LD.FIN.MAT.DATE>
                    YY=FIN.DATE[1,4]
                    MM=FIN.DATE[5,2]
                    DD=FIN.DATE[7,2]
                    FIN.DATE=YY:"/":MM:"/":DD

                    XX<1,ZZ>[09,16]   = ""
                    XX<1,ZZ>[26,30]   = ""
                    XX<1,ZZ>[66,20]   = ""
                    XX<1,ZZ>[86,15]   = ""
                    XX<1,ZZ>[102,16]  = KEY.LIST.LD<J>
                    XX<1,ZZ>[119,15]  = AMT.LD
                    XX<1,ZZ>[136,10]  = CURR.LD
                    XX<1,ZZ>[155,10]   = FIN.DATE
                    PRINT XX<1,ZZ>
                    ZZ=ZZ+1

                NEXT J
            END
**END
            IF KEY.LIST<I> # TEMP.AC THEN
                TEMP.AC = KEY.LIST<I>
                XX<1,ZZ>[09,16]   = SPACE(16)
                XX<1,ZZ>[42,17]   = KEY.LIST<I>
                XX<1,ZZ>[66,20]   = WORK.BAL
                XX<1,ZZ>[86,15]   = CURR
                PRINT XX<1,ZZ>
                ZZ=ZZ+1
            END
*************
        END
    NEXT I
    RETURN
*===============================================================
PRINT.HEAD:
*Line [ 161 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):"������� �� ���� ����� ����� � ���� ����� "
    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"��� ������ ������":SPACE(2):"��� ������":SPACE(11):"��� ������ ������":SPACE(09):"����-�����":SPACE(10):"����":SPACE(09):"��� �������":SPACE(06):"���� �������":SPACE(09):"����" :SPACE(10):"����� ���������"
    PR.HD :="'L'":SPACE(1):STR('_',17):SPACE(2):STR('_',10):SPACE(11):STR('_',17):SPACE(09):STR('_',10):SPACE(10):STR('_',4):SPACE(09):STR('_',11):SPACE(06):STR('_',12):SPACE(09):STR('_',4):SPACE(10):STR('_',15)
    HEADING PR.HD
    RETURN
*==============================================================
END
