* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE OFS.REQ.V

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL

*---------------------------------------


    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "RE.BASE.CCY.REQ"
    SCB.VERSION = "SCB"


    NK = ''
    NK<1,1>='INPUTT01//EG0010001'  ; NK<1,2>='INPUTT02//EG0010002'   ; NK<1,3>='INPUTT03//EG0010003'
    NK<1,4>='INPUTT04//EG0010004'  ; NK<1,5>='INPUTT05//EG0010005'   ; NK<1,6>='INPUTT06//EG0010006'
    NK<1,7>='INPUTT07//EG0010007'  ; NK<1,8>='INPUTT09//EG0010009'   ; NK<1,9>='INPUTT10//EG0010010'
    NK<1,10>='INPUTT11//EG0010011' ; NK<1,11>='INPUTT12//EG0010012'  ; NK<1,12>='INPUTT13//EG0010013'
    NK<1,13>='INPUTT14//EG0010014' ; NK<1,14>='INPUTT15//EG0010015'  ; NK<1,15>='INPUTT20//EG0010020'
    NK<1,16>='INPUTT21//EG0010021' ; NK<1,17>='INPUTT22//EG0010022'  ; NK<1,18>='INPUTT23//EG0010023'
    NK<1,19>='INPUTT30//EG0010030' ; NK<1,20>='INPUTT31//EG0010031'  ; NK<1,21>='INPUTT32//EG0010032'
    NK<1,22>='INPUTT35//EG0010035' ; NK<1,23>='INPUTT40//EG0010040'  ; NK<1,24>='INPUTT50//EG0010050'
    NK<1,25>='INPUTT51//EG0010051'
    NK<1,26>='INPUTT60//EG0010060' ; NK<1,27>='INPUTT70//EG0010070'  ; NK<1,28>='INPUTT80//EG0010080'
    NK<1,29>='INPUTT81//EG0010081' ; NK<1,30>='INPUTT90//EG0010090'  ; NK<1,31>='INPUTT99//EG0010099'

    OPENSEQ "&SAVEDLISTS&" , "REQ.V.OUT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"REQ.V.OUT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "REQ.V.OUT" TO BB ELSE
        CREATE BB THEN
**        PRINT 'FILE REQ.V.OUT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create REQ.V.OUT File IN &SAVEDLISTS&'
        END
    END


    FOR I = 1 TO 31
        SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/V/PROCESS,":NK<1,I>:",":"EGPR":","
***        PRINT NK<1,I>
        OFS.MESSAGE.DATA  =  ""

        SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
* SCB R15 UPG 20160717 - S
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E

        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END



    NEXT I
    TEXT = 'DONE' ; CALL REM
    RETURN
************************************************************
END
