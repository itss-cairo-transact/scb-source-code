* @ValidationCode : Mjo2MzkzNjMyMTY6Q3AxMjUyOjE2NDQ5MzU2MjAyMjY6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:33:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOO******************
SUBROUTINE REPORT.BR.BT.DEBIT.RE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �����" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.BR.BT.DEBIT.RE'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

*BT.ID = ID.NEW


    BT.ID = COMI
    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)


    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
****TEXT = "COMI = :" :COMI ; CALL REM
*------------------------------------------------------------------------
*IF R.BATCH<SCB.BT.RETURN.REASON> EQ '' THEN
    BR.ID = R.BATCH<SCB.BT.OUR.REFERENCE>
    BR.INPUTT = R.BATCH<SCB.BT.INPUTTER>
    BR.AUTH   = R.BATCH<SCB.BT.AUTHORISER>
    INP = FIELD(BR.INPUTT,'_',2)
    AUTHI =FIELD(BR.AUTH,'_',2)

*Line [ 93 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
*****TEXT = "DD :": DD ; CALL REM
    FOR I = 1 TO DD
        BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
****TEXT = "BR.ID1 = :" : BR.ID1 ; CALL REM
*------------------------------------------------------------------------
        CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
****DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>
        ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>
*Line [ 103 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,DRAWER.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        DRAWER.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 110 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
****        ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
        CHQ.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
        BANK         = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
        BANK.BRAN    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
*Line [ 124 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK,BNAME)
        F.ITSS.SCB.BANK = 'F.SCB.BANK'
        FN.F.ITSS.SCB.BANK = ''
        CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
        CALL F.READ(F.ITSS.SCB.BANK,BANK,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
        BNAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BANK.BRAN,BRNAME)
        F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
        FN.F.ITSS.SCB.BANK.BRANCH = ''
        CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
        CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BANK.BRAN,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
        BRNAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>
******TEXT = "ACC.NO = " : ACC.NO ; CALL REM
*        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        BRN.ID  = AC.COMP[2]
        BRANCH.ID = TRIM(BRN.ID,"0","L")

*Line [ 150 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
        F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
        FN.F.ITSS.DEPT.ACCT.OFFICER = ''
        CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
        CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
        BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

        CATEG.ID  = ACC.NO[11,4]
*Line [ 124 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
        
*Line [ 161 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

        AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
        IN.AMOUNT = AMOUNT

        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

        CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 134 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

        DAT  = TODAY
        MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

        INPUTTER = R.BR<EB.BILL.REG.INPUTTER>
        AUTH = R.BR<EB.BILL.REG.AUTHORISER>
        INP = FIELD(INPUTTER,'_',2)
        AUTHI =FIELD(AUTH,'_',2)

        XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX11  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
        XX8  = SPACE(132)  ; XX9  = SPACE(132)

        XX<1,1>[3,35]   = CUST.NAME:' ':CUST.NAME.2
        XX1<1,1>[3,35]   = CUST.ADDRESS

        XX<1,1>[45,15]  = '������     : '
        XX<1,1>[59,15]  = AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = ACC.NO

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR

        XX4<1,1>[45,15] = '����� ���� : '
        XX4<1,1>[59,15] = MAT.DATE

        XX5<1,1>[3,15]  = '��� �����      : '
        XX5<1,1>[20,15] = CHQ.NO

        XX5<1,1>[45,15]  = '��� �������    : '
        XX5<1,1>[59,15] = BR.ID1

        XX11<1,1>[3,15]  = '����� ������� ����: '
        XX11<1,1>[30,15] = BNAME

        XX11<1,1>[50,15]  = '��� ����� ������� ����: '
        XX11<1,1>[65,15] = BRNAME


        XX6<1,1>[1,15]  = '������'
****   XX7<1,1>[1,15] = AUTHI
        XX7<1,1>[1,15] = BR.AUTH

        XX6<1,1>[30,15]  = '��� �������'
        XX7<1,1>[35,15] = COMI

        XX6<1,1>[60,15]  = '������'
****        XX7<1,1>[60,15] = INP
        XX7<1,1>[60,15] = BR.INPUTT

        XX8<1,1>[3,35]  = '������ ������� : '
        XX8<1,1>[20,15] = OUT.AMT

        XX9<1,1>[3,15]  = '������         : '
        XX9<1,1>[20,15] = '����� ��� ���� �����'

*-------------------------------------------------------------------
        YYBRN  = FIELD(BRANCH,'.',2)
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
        PR.HD  ="'L'":SPACE(1):"��� ���� ������"
        PR.HD :="'L'":"������� : ":T.DAY
        PR.HD :="'L'":"����� : ":YYBRN
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(50) : "����� ��� "
        PR.HD :="'L'":" "
        PRINT
        HEADING PR.HD
*------------------------------------------------------------------
        PRINT XX<1,1>
        PRINT XX1<1,1>
        PRINT XX2<1,1>
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT XX11<1,1>
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT XX8<1,1>
        PRINT XX9<1,1>
        PRINT XX5<1,1>
        PRINT STR(' ',82)
        PRINT XX6<1,1>
        PRINT STR('-',82)
        PRINT XX7<1,1>
    NEXT I
* END
*===============================================================
RETURN
END
