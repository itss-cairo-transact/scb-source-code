* @ValidationCode : MjoxNTE4ODYyMjkzOkNwMTI1MjoxNjQ0OTM4NTQzOTQ4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:22:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************NI7OOOOOOOOOOO**************************
*-----------------------------------------------------------------------------
* <Rating>-136</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.FT.HIS2.UPDATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_DR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.FT.HIS2.UPDATE'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID = COMI
    FN.DR = 'FBNK.FUNDS.TRANSFER$HIS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    TEXT = "COMI = " : COMI ; CALL REM
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATE.TO = TODAY[3,6]:"..."
*------------------------------------------------------------------------
    DB.ACC = R.DR<FT.DEBIT.ACCT.NO>
    CR.ACC = R.DR<FT.CREDIT.ACCT.NO>
    TEXT = "DB.ACC = " : DB.ACC ; CALL REM
    ACC.NO = CR.ACC
    IF CR.ACC[1,2] NE 'PL' OR CR.ACC[1,2] NE 'EG' OR CR.ACC[1,2] NE 'US' THEN
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
        TEXT = "CUS.ID =" : CUS.ID ; CALL REM
*Line [ 98 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CAT.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
*Line [ 112 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
****        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
        ACC.BR  = AC.COMP[8,2]

        CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
*Line [ 102 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CAT.ID,CATEG)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,CAT.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        DR.AMOUNT = R.DR<FT.AMOUNT.DEBITED>[4,20]
        CR.AMOUNT = R.DR<FT.AMOUNT.CREDITED>[4,20]
        AMOUNT    = CR.AMOUNT
        IN.AMOUNT = AMOUNT
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 109 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 146 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        DAT       = R.DR<FT.CREDIT.VALUE.DATE>
        MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
****    BR.DATA   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES>
****    REFER     = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE>
        BR.DATA   = R.DR<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
        REFER     = R.DR<FT.CREDIT.THEIR.REF>
****        BRANCH1   = R.DR<FT.DEPT.CODE>
        BRANCH1   = R.DR<FT.CO.CODE>[2]
        BRANCH1   = TRIM(BRANCH1, "0" , "L")

**COM.AMT = R.DR<FT.COMMISSION.AMT>
**CHG.AMT = R.DR<FT.CHARGE.AMT>
**TOT.CHG = R.DR<FT.TOTAL.CHARGE.AMOUNT>
**BENCUS  = R.DR<FT.LOCAL.REF><1,FTLR.BENEFICIARY.CUS>
    END

    IF CR.ACC[1,2] EQ 'PL' THEN
*Line [ 129 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CR.ACC[3,6],CATEG)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,CR.ACC[3,6],R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        CUST.NAME    = CATEG
        CUST.ADDRESS = CATEG
        CR.AMOUNT = R.DR<FT.AMOUNT.CREDITED>[4,20]
        CUR.ID    = R.DR<FT.CREDIT.CURRENCY>
        AMOUNT    = CR.AMOUNT
        IN.AMOUNT = AMOUNT
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 138 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 187 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        DAT       = R.DR<FT.CREDIT.VALUE.DATE>
        MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
****    BR.DATA   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES>
****    REFER     = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE>
        BR.DATA   = R.DR<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
        REFER     = R.DR<FT.CREDIT.THEIR.REF>
***        BRANCH1   = R.DR<FT.DEPT.CODE>
        BRANCH1   = R.DR<FT.CO.CODE>[2]
        BRANCH1   = TRIM(BRANCH1, "0" , "L")
    END

    INPUTT     = R.DR<FT.INPUTTER>
    INP        = FIELD(INPUTT,'_',2)
    AUTH1      = R.DR<FT.AUTHORISER>
    AUTHI      = FIELD(AUTH1,'_',2)

*------------------------------------------------------------------------

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11  = SPACE(132)

*  IF CAT.ID EQ 1512 THEN
*      LOAN.AMT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.MARG.AMOUNT>
*      DAT1= R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NO.DAY>
*      LOAN.DATE = DAT1[7,2]:'/':DAT1[5,2]:"/":DAT1[1,4]
*      XX11<1,1>[3,15]  = '���� �����     : ':' ':LOAN.AMT
*      XX11<1,1>[45,35] = '������ ����� �� : ':' ':LOAN.DATE
*  END


    XX1<1,1>[3,35]   = CUST.NAME:'':CUST.NAME.2
    XX1<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������:'
    XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX6<1,1>[1,15]  = '������'
    XX6<1,1>[30,15] = '��� �������'
    XX6<1,1>[60,15] = '������'

    XX7<1,1>[1,15]  = INP:' �'
    XX7<1,1>[30,15] = COMI:' �'
    XX7<1,1>[60,15] = AUTHI:' �'

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
** XX8<1,1>[20,15] = "��� ������� ����� ���� ���� �����"

    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = BR.DATA

    XX10<1,1>[3,15]  = '������    : '
    XX10<1,1>[20,15] = REFER

** XX15<1,1>[3,15]  = ' �������� ��������� :'
** XX16<1,1>[3,15]  = '-----------------------'
** XX17<1,1>[3,15]  = COM.AMT[4,10]
** XX18<1,1>[3,15]  = CHG.AMT[4,10]
** XX19<1,1>[3,15]  = '������ �������� ��������� : ': TOT.CHG[4,10]


*-------------------------------------------------------------------
*Line [ 273 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH1,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH1,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    TEXT = "BRANCH = " :BRANCH ; CALL REM
    YYBRN  = FIELD(BRANCH,'.',2)

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":'':"REPORT.FT.HIS2.UPDATE"
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":'����� �����':SPACE(10) :'������� :':T.DAY
    PR.HD :="'L'":'-------------------------------------------------------------------'
    PR.HD :="'L'":CUST.NAME:' ' : CUST.NAME.2

    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
* PRINT XX15<1,1>
* PRINT XX16<1,1>
* PRINT XX17<1,1>
* PRINT STR('-',82)
* PRINT XX18<1,1>

    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
