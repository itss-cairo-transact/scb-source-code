* @ValidationCode : MjotMTk3MTg0OTQ2MzpDcDEyNTI6MTY0NDkzNjM5Nzk4ODp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:46:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>390</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.BR.RETURN
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.USER
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CURRENCY
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CATEGORY
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.ACCOUNT
    $INSERT            I_F.SCB.BANK
    $INSERT            I_F.SCB.BANK.BRANCH
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    BRANCH = ""
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� " ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.BR.RETURN'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*-------
    FN.BATCH = 'F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    AMOUNT = ''
    BT.ID  = ID.NEW
    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX10 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)


*------------------------------------------------------------------------
    BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)

*Line [ 70 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
    FOR I = 1 TO DD

        BR.ID1 = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
        BR.ID2 = R.NEW(SCB.BT.RETURN.REASON)<1,I>
        IF BR.ID2 NE '' THEN
*------------------------------------------------------------------------
            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
            ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
            BILL.NO   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
            BANK.NO   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            BRN.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>

*        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
*Line [ 93 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
            BRN.ID  = AC.COMP[2]
            BRANCH.ID = TRIM(BRN.ID,"0","L")
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
            F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
            FN.F.ITSS.DEPT.ACCT.OFFICER = ''
            CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
            CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
            BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.NO,BANK.NAME)
            F.ITSS.SCB.BANK = 'F.SCB.BANK'
            FN.F.ITSS.SCB.BANK = ''
            CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
            CALL F.READ(F.ITSS.SCB.BANK,BANK.NO,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
            BANK.NAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*Line [ 116 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BRN.NO,BRANCH.NAME)
            F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
            FN.F.ITSS.SCB.BANK.BRANCH = ''
            CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
            CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BRN.NO,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
            BRANCH.NAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>

            CATEG.ID  = ACC.NO[11,4]
*Line [ 102 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
            
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

*Line [ 135 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,DRAWER.ID)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            DRAWER.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 142 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>

            FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                CUST.ADDRESS1 = "���� ��������� ������"
            END ELSE
                CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            END

            AMOUNT      = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT   = AMOUNT

            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID  = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 124 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT      = TODAY
            MAT.DATE = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

            INPUTTER = R.NEW(SCB.BT.INPUTTER)
            AUTH     = R.NEW(SCB.BT.AUTHORISER)

            INP   = FIELD(INPUTTER,'_',2)
            AUTHI = FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX10 = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,35]   = CUST.NAME
            XX1<1,1>[3,35]  = CUST.ADDRESS1

            XX<1,1>[45,15]  = '������     : '
            XX<1,1>[59,15]  = AMOUNT

            XX1<1,1>[45,15] = '��� ������ : '
            XX1<1,1>[59,15] = ACC.NO

            XX2<1,1>[45,15] = '��� ������ : '
            XX2<1,1>[59,15] = CATEG

            XX3<1,1>[45,15] = '������     : '
            XX3<1,1>[59,15] = CUR

            XX4<1,1>[45,15] = '����� ���� : '
            XX4<1,1>[59,15] = MAT.DATE

            XX6<1,1>[1,15]  = '������'
            XX7<1,1>[1,15]  = AUTHI

            XX6<1,1>[30,15] = '��� �������'
            XX7<1,1>[35,15] = BT.ID

            XX6<1,1>[60,15] = '������'
            XX7<1,1>[60,15] = INP

            XX8<1,1>[3,35]  = '������ ������� : '
            XX8<1,1>[20,15] = OUT.AMT

            XX9<1,1>[3,15]  = '������ : '
            XX9<1,1>[10,15] = "��� ���� ��� :" :BILL.NO: "�� " :CUST.NAME: "��" : MAT.DATE
            XX10<1,1>[10,15]= "��� ���" : BANK.NAME : "���" :BRANCH.NAME
*------------------------
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            IF I GT 1 THEN
                PR.HD :="'L'":"PAGE-ONE"
            END

            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN
            PR.HD :="'L'":" "
            PR.HD :="'L'":SPACE(50) : "����� ����"
            PR.HD :="'L'":"REPORT.BR.RETURN"
            PRINT
            HEADING PR.HD

            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT STR(' ',82)
            PRINT STR(' ',82)
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX10<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR('-',82)
            PRINT XX7<1,1>
        END
    NEXT I
*===============================================================
RETURN
END
