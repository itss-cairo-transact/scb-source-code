* @ValidationCode : MjotMTk2ODE5OTM4NzpDcDEyNTI6MTY0NDk0MDgwNzg0Mjp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 18:00:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>-101</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.RET.DEBIT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.TRANS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SF.TRANS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CHQ.RETURN.NEW
*------------------------------------------------------------------------
    IF R.NEW(CHQ.RET.DEBIT.ACCT) NE '' THEN
        GOSUB INITIATE
        GOSUB PROCESS

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ���������" ; CALL REM
        RETURN
    END
*========================================================================
INITIATE:
    REPORT.ID='REPORT.RET.DEBIT'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    CUS           = '52109'

**   CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CUS,CATEG.NAME)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,CUS,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
* CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>
    CUST.NAME     = CATEG.NAME
* CUST.NAME3    = LOCAL<1,CULR.ARABIC.NAME.2>
* CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS  = CATEG.NAME
* CUST.ADDRESS1 = LOCAL<1,CULR.ARABIC.ADDRESS>
* CUST.ADDRESS2 = LOCAL<1,CULR.ARABIC.ADDRESS>
* CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
* CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)
* IF CUST.ADDRESS1 = 98 THEN
*     CUST.ADD1 = ''
* END
* IF CUST.ADDRESS2 = 998 THEN
*     CUST.ADD2 = ''
* END
* IF CUST.ADDRESS1 = 999 THEN
*     CUST.ADD1 = ''
* END
* IF CUST.ADDRESS2 = 999 THEN
*     CUST.ADD2 = ''
* END
    ACC.DB = '54025'
*Line [ 95 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
    
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,ACC.DB,CATEG.NAME)
    F.ITSS.CATEGORY = 'F.CATEGORY'
    FN.F.ITSS.CATEGORY = ''
    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
    CALL F.READ(F.ITSS.CATEGORY,ACC.DB,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
    CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
*Line [ 98 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
    
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CURR.ID,CURR.CC)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CURR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CURR.CC=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ("ACCOUNT":@FM:AC.CATEGORY,ACC.DB,CATEG.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.DB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CATEG.ID=R.ITSS.ACCOUNT<AC.CATEGORY>
    ACCC      = R.NEW(CHQ.RET.DEBIT.ACCT)
    ACC.DB    = ACCC
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ("ACCOUNT":@FM:AC.CURRENCY,ACC.DB,CURR.ID)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.DB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    CURR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 105 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
    
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CURR.ID,CURR.CC)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CURR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CURR.CC=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

***AMOUNT    = R.NEW(SCB.SF.TR.MARGIN.VALUE)
    CURR         = R.NEW(CHQ.RET.CURRENCY)
    BR.DATA      = R.NEW(CHQ.RET.REMARKS)<1,1>
    BR.DATA1     = R.NEW(CHQ.RET.REMARKS)<1,2>
    IF CURR.ID EQ 'EGP' THEN
        LCY.AMT = 20
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = ''
        AMOUNT = LCY.AMT
    END
    IF CURR.ID NE 'EGP' THEN
        FN.CUR  = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

        LCY.AMT = 5 * RATE
        CALL EB.ROUND.AMOUNT ('USD',LCY.AMT,'',"2")
        FCY.AMT = 5
        AMOUNT  = FCY.AMT
    END

    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    V.DATE = R.NEW(CHQ.RET.CHQ.DATE)

    CHQ.NO = R.NEW(CHQ.RET.CHEQUE.NO)
*Line [ 137 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 174 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CURR,CUR)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

*    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    OUT.AMT = OUT.AMOUNT : ' ' : CURR.CC :' ' : '�����'

    MAT.DATE  = V.DATE[7,2]:'/':V.DATE[5,2]:"/":V.DATE[1,4]

    INPUTTER = R.NEW(CHQ.RET.INPUTTER)
    AUTH     = R.NEW(CHQ.RET.AUTHORISER)
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132) ; XX10  = SPACE(132)

    XX<1,1>[3,35]    = CUST.NAME
* XX1<1,1>[3,35]   = CUST.NAME3
    XX2<1,1>[3,35]   = CUST.ADDRESS
* XX3<1,1>[3,35]   = CUST.ADD1:' ':CUST.ADD2
    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = CUS

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG.NAME

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CURR.CC

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX5<1,1>[3,15]  = '��� �����      : '
    XX5<1,1>[20,15] = CHQ.NO

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = INP

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = ID.NEW

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = BR.DATA
    XX10<1,1>[20,15] = BR.DATA1
*-------------------------------------------------------------------
*Line [ 236 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"CHQRET"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
*  PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
RETURN
END
