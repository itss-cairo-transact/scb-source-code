* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-176</Rating>
*-----------------------------------------------------------------------------
************* WAEL *************
*TO CREATE LG LETTER
    SUBROUTINE REPORT.DATA.AHMED

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    FOR I =1 TO 2
*Line [ 49 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 50 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
        GOSUB CALLDB
        MYID = MYCODE:'.':MYTYPE
*** CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.DESCRIPTION,MYID,MYVER)

        IF MYCODE = "1111" THEN
            IF I = 1 THEN
                GOSUB INITIATE
                GOSUB PRINT.HEAD
                GOSUB BODY
                CALL PRINTER.OFF
                CALL PRINTER.CLOSE(REPORT.ID,0,'')
            END ELSE
                GOSUB INITIATE
                GOSUB PRINT.HEAD
                GOSUB BODY2
                CALL PRINTER.OFF
                CALL PRINTER.CLOSE(REPORT.ID,0,'')
            END
        END ELSE
            E = "NOT.VALID.VERSION":" - ":MYVER ; CALL ERR ; MESSAGE = 'REPEAT'
        END
    NEXT I
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='REPORT.DATA'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    IF ID.NEW = '' THEN
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
    K=''
    MYVER=''
    LOCAL.REF = R.LD<LD.LOCAL.REF>

    BENF1 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,1>
    BENF2 =LOCAL.REF<1,LDLR.IN.FAVOR.OF,2>
*    OLDNO =LOCAL.REF<1,LDLR.OLD.NO>
    ADDR1 =LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2 =LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    ADDR3 =LOCAL.REF<1,LDLR.BNF.DETAILS,3>
*********************
* ADDR1 = LOCAL.REF<1,LDLR.BNF.DETAILS>
*ADDR.COUNT = DCOUNT(ADDR1,SM)
*FOR I=1 TO ADDR.COUNT
*   ADDR :=LOCAL.REF<1,LDLR.BNF.DETAILS,I>:'|'
*NEXT I
**********************

    AC.NUM = LOCAL.REF<1,LDLR.DEBIT.ACCT>
*************************************
    FN.AC = 'F.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    CALL F.READ(FN.AC,AC.NUM,R.AC,F.AC,E1)
    CUST.AC=R.AC<AC.CUSTOMER>
*************************************
    THIRD.NO =LOCAL.REF<1,LDLR.THIRD.NUMBER>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    IF THIRD.NO EQ CUST.AC THEN
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
    END ELSE
        K=1
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.AC,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        THIRD.NAME1 = LOC.REF<1,CULR.ARABIC.NAME>
        THIRD.NAME2 = LOC.REF<1,CULR.ARABIC.NAME.2>
        THIRD.ADDR1 = LOC.REF<1,CULR.ARABIC.ADDRESS,1>
        THIRD.ADDR2 = LOC.REF<1,CULR.ARABIC.ADDRESS,2>
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,THIRD.NO,LOC.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,THIRD.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        LG.CUST1 = LOC.REF<1,CULR.ARABIC.NAME>
        LG.CUST2 = LOC.REF<1,CULR.ARABIC.NAME.2>

    END
    ADV.OPER=LOCAL.REF<1,LDLR.ADV.OPERATIVE>
*    LG.NO = LOCAL.REF<1,LDLR.LG.NUMBER,1>
    LG.NO = LOCAL.REF<1,LDLR.OLD.NO>
********************
    SAM   = LG.NO[4,2]
    SAM1  = LG.NO[6,3]
    SAM2  = LG.NO[9,5]
*    SAM3  = LG.NO[10,4]
*    LG.NO='LG/': SAM:"/": SAM1:"/":SAM2
    LG.NO= LOCAL.REF<1,LDLR.OLD.NO>
********************
    LG.AMT =R.LD<LD.AMOUNT>
    CUR=R.LD<LD.CURRENCY>
*Line [ 169 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    LG.TYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
*Line [ 177 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('SCB.LG.PARMS':@FM:SCB.LGP.DESCRIPTION,LG.TYPE,TYPE.NAME)
F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
FN.F.ITSS.SCB.LG.PARMS = ''
CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
CALL F.READ(F.ITSS.SCB.LG.PARMS,LG.TYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
TYPE.NAME=R.ITSS.SCB.LG.PARMS<SCB.LGP.DESCRIPTION>
    LG.NAME = TYPE.NAME
    DATY = TODAY
    XX = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]

    MYCODE =LOCAL.REF<1,LDLR.OPERATION.CODE>
    MYTYPE =LOCAL.REF<1,LDLR.PRODUCT.TYPE>
    MYID = MYCODE:'.':MYTYPE

**CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
*Line [ 193 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.AC,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
AC.OFICER=R.ITSS.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
*Line [ 200 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,AC.OFICER,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN = FIELD(BRANCH,'.',2)
***************************
*************************
    IF ID.NEW NE '' THEN
        AUTH  = OPERATOR
    END ELSE
        AUTHORISER  = R.LD<LD.AUTHORISER>
        AUTH        = FIELD(AUTHORISER,'_',2)
    END
    LG.CO= R.LD<LD.CO.CODE>
*************************
    RETURN
******************
PRINT.HEAD:
    PR.HD ="'L'":SPACE(1):"��� ���� �����������"
    PR.HD :="'L'":SPACE(1):"����������":":":YYBRN
    PR.HD :="'L'":SPACE(1):STR('_',22)
    HEADING PR.HD
    LNTH1 = LEN(BENF1)+12 ; LNTH2 = LEN(BENF2)+12
    LNE1 = 50-LNTH1 ; LNE2 = 50-LNTH2
    LNTHD1 = LEN(ADDR1)+12 ; LNTHD2 = LEN(ADDR2)+12 ;LNTHD3 = LEN(ADDR3)+12
    LNED1 = 50-LNTHD1 ; LNED2 = 50-LNTHD2 ;LNED3 = 50-LNTHD3
    PRINT
    PRINT " ________________________________________________"
    PRINT "| ������� : ":BENF1:SPACE(LNE1):"|"
    PRINT "|         : ":BENF2:SPACE(LNE2):"|"
    PRINT "|":SPACE(49):"|"
    PRINT "| ������� : ":ADDR1:SPACE(LNED1):"|"
    PRINT "|         : ":ADDR2:SPACE(LNED2):"|"
    PRINT "|         : ":ADDR3:SPACE(LNED3):"|"
    PRINT "|_________________________________________________|"
    PRINT
    PRINT
    PRINT SPACE(20): "���� ���� ��� � �: ":LG.NO: "(":TYPE.NAME:")"
    PRINT
    PRINT SPACE(20): "������������ : ":" **":LG.AMT:"**":" ":CRR
    PRINT

    IF MYCODE[2,3]= "111" THEN
        PRINT SPACE(20): "����� ��� ��� : ":THIRD.NAME1
        PRINT SPACE(20): "              : ":THIRD.NAME2
        IF K = 1 THEN
            PRINT SPACE(20): "� ������������: ":LG.CUST1
            PRINT SPACE(20): "              : ":LG.CUST2
        END ELSE
               PRINT SPACE(20): " "
               PRINT SPACE(20): " "            
        END
    END ELSE
        PRINT SPACE(20): "������� � ����� : ":THIRD.NAME1
        PRINT SPACE(20): "                : ":THIRD.NAME2
    END

    PRINT
    PRINT SPACE(20):"________________________________________________"
    RETURN
******************
*===============================================================
BODY:
    PRINT ; PRINT
    PRINT
    PRINT SPACE(5): "����� �������� ���� �� ������ ������ ������ ������ � ������ ����"
    PRINT
    IF MYTYPE EQ 'ADVANCE'  THEN
        PRINT SPACE(3):"���� ��� � ������ �������� ������� ����� �������� ."
    END
    PRINT
    IF MYTYPE EQ "ADVANCE" THEN
        IF ADV.OPER EQ '' THEN
            PRINT SPACE(3):"������ ������� ��������� � ���� ���� ������ ������� ."
        END  ELSE
            IF ADV.OPER EQ 'YES' THEN
                PRINT SPACE(3):"������ ������� ���������."
            END
        END
    END ELSE
        PRINT SPACE(3):"������ ������� ��������� ."
    END
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ;PRINT
    PRINT ; PRINT ; PRINT
    PRINT SPACE(45):"��� ���� ����� �������"
    PRINT;PRINT  "������ �� :":XX
    IF MYTYPE EQ 'ADVANCE' THEN
        IF ADV.OPER EQ 'YES' THEN
            PRINT SPACE(1):'ADVANCE.OPER':'.REPORT.DATA.AHMED.BODY'
        END ELSE
            PRINT SPACE(1):'ADVANCE.UNOPER':'.REPORT.DATA.AHMED.BODY'
        END
    END
    IF MYTYPE EQ 'FINAL' THEN
        PRINT SPACE(1):'FINAL':'.REPORT.DATA.AHMED.BODY'
    END
    IF MYTYPE EQ 'BIDBOND' THEN
        PRINT SPACE(1):'BIDBOND':'.REPORT.DATA.AHMED.BODY'
    END
    CALL LG.ADD(AUTH,LG.CO)
**TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

    RETURN
************************************
BODY2:
    PRINT ; PRINT
    PRINT
    PRINT SPACE(5): "����� �������� ���� �� ������ ������ ������ ������ � ������ ���� "
    PRINT
    IF MYTYPE="ADVANCE" THEN
        IF ADV.OPER EQ '' THEN
            PRINT SPACE(3):"������ ������� ��������� � ���� ���� ������ ������� ."
        END ELSE
            IF ADV.OPER EQ 'YES' THEN
                PRINT SPACE(3):"������ ������� ��������� ."
                TEXT='OPER';CALL REM
            END
        END
    END ELSE
        PRINT SPACE(3):"������ ������� ��������� ."

    END
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ;PRINT
    PRINT ; PRINT ; PRINT
    PRINT SPACE(45):"��� ���� ����� �������"
    PRINT;PRINT  "������ �� :":XX
****************
    IF MYTYPE EQ 'ADVANCE' THEN
        IF ADV.OPER EQ 'YES' THEN
            PRINT SPACE(1):'ADVANCE.OPER':'.REPORT.DATA.AHMED'
        END ELSE
            PRINT SPACE(1):'ADVANCE.UNOPER':'.REPORT.DATA.AHMED'
        END
    END
    IF MYTYPE EQ 'FINAL' THEN
        PRINT SPACE(1):'FINAL':'.REPORT.DATA.AHMED'
    END
    IF MYTYPE EQ 'BIDBOND' THEN
        PRINT SPACE(1):'BIDBOND':'.REPORT.DATA.AHMED'
    END
*****************
    CALL LG.ADD(AUTH,LG.CO)
**TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM
    RETURN
*************************************

END
*==================================================================
