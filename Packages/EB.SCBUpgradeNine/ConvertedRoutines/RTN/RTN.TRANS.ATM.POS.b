* @ValidationCode : MjotNDM1ODk0MzI6Q3AxMjUyOjE2NDA2MTg0ODE3MjA6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 17:21:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>89</Rating>
*-----------------------------------------------------------------------------
*----------------------------------
*-- CREATE BY NESSMA ON 2013/04/11
*----------------------------------
*    PROGRAM RTN.TRANS.ATM.POS
SUBROUTINE RTN.TRANS.ATM.POS
*----------------------------------
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE JBC.h
*---------------------------------------------
    GOSUB PROCESS.VISA
*   GOSUB CREATE.OFS
    GOSUB INITIAL
    TEXT = "�� �������" ; CALL REM
RETURN
*---------------------------------------------
PROCESS.VISA:
*------------
    TOD = TODAY
    MON = TOD[5,2]
    TOD = TOD[3,2]:MON:TOD[7,2]
    FILE.NAME = "cl.txt"
    FILE.ID   = "cl-":TOD:".txt"
RETURN
*---------------------------------------------------
CREATE.OFS:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0

    OFS.REC          = ""
    OFS.OPERATION    = ""
    OFS.OPTIONS      = ""
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "CL-LOCKS-":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
RETURN
*-------------------------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ""
    R.LINE = ""

    ERR='' ; ER=''

    DIR.NAMEE  = "POS"

    OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
        TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
        RETURN
    END

    IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

    LINE.NO  = 1
    CHK.FLAG = 0
    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM BB THEN
            LOCK.ID = FIELD(R.LINE,",EBC.AUTH.NO=,",2)
            IF LOCK.ID THEN
                TEXT = "LOCK EMPTY LINE NO.":LINE.NO ; CALL REM
                CHK.FLAG = 1
            END
            LINE.NO ++
        END ELSE
            EOF = 1
        END
    REPEAT

    IF CHK.FLAG EQ 0 THEN
        BB     = ""
        EOF    = ""
        R.LINE = ""

        OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
            TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
            RETURN
        END
        GOSUB CREATE.OFS

        LOOP WHILE NOT(EOF)
            READSEQ R.LINE FROM BB THEN
                LOCK.ID = FIELD(R.LINE,",EBC.AUTH.NO=",2)
                LOCK.ID = FIELD(LOCK.ID,",",1)
                IF LOCK.ID NE "" THEN
                    GOSUB BUILD.RECORD.LOCKS
                END

                GOSUB BUILD.RECORD
            END ELSE
                EOF = 1
            END
        REPEAT
    END

    CLOSESEQ BB
RETURN
*------------------------------------------------------------------
BUILD.RECORD:
*------------
    MSG.OFS = R.LINE
    MSG.OFS = EREPLACE (MSG.OFS,"_"," ")

    WRITESEQ MSG.OFS TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':MSG.OFS
    END
RETURN
*------------------------------------------------------------------
BUILD.RECORD.LOCKS:
*------------------
    MSG.OFS.LOKS  = "AC.LOCKED.EVENTS,ATM.123/R,ATMPOS//EG0010099,"
    MSG.OFS.LOKS := LOCK.ID

    WRITESEQ MSG.OFS.LOKS TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':MSG.OFS.LOKS
    END

RETURN
*------------------------------------------------------------------
END
