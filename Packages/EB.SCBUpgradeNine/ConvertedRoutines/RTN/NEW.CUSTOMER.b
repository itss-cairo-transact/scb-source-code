* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>81</Rating> UPDATED BY M.ELSAYED <27/1/2009>
*-----------------------------------------------------------------------------
    SUBROUTINE NEW.CUSTOMER(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY

    DDD = TODAY[3,6]
*** DD = "...":DDD:"..."
    DD = DDD:"..."
 
    CUS.USR = R.USER<EB.USE.DEPARTMENT.CODE>

****   TEXT  = CUS.USR ; CALL REM
****T.SEL = "SELECT FBNK.CUSTOMER WITH INPUTTER UNLIKE ...OFS... AND CURR.NO EQ 4 AND ACCOUNT.OFFICER EQ " : CUS.USR
****T.SEL = "SELECT FBNK.CUSTOMER WITH DATE.TIME LIKE " :DD: " AND INPUTTER UNLIKE ...OFS... AND ACCOUNT.OFFICER EQ " : CUS.USR
**MSABRY
* T.SEL = "SELECT FBNK.CUSTOMER WITH DATE.TIME LIKE " :DD: " AND INPUTTER UNLIKE ...OFS... AND ACCOUNT.OFFICER EQ " :CUS.USR :"  AND (CURR.NO EQ 2 OR CURR.NO EQ 1)"
***    T.SEL = "SELECT FBNK.CUSTOMER WITH DATE.TIME LIKE " :DD: " AND POSTING.RESTRICT LE 89 AND INPUTTER UNLIKE ...OFS... AND COMPANY.BOOK EQ " :COMP :"  AND (CURR.NO EQ 2 OR CURR.NO EQ 1)"
    T.SEL = "SELECT FBNK.CUSTOMER WITH DATE.TIME LIKE " :DD: " AND POSTING.RESTRICT LE 89 AND INPUTTER UNLIKE ...OFS... AND COMPANY.BOOK EQ " :COMP :"  AND (CURR.NO EQ 1)"
*** T.SEL = "SELECT FBNK.CUSTOMER WITH @ID IN (32200060 32500048)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

****TEXT = SELECTED ; CALL REM
    ENQ.LP  = '' ; OPER.VAL = ''
*********************
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
