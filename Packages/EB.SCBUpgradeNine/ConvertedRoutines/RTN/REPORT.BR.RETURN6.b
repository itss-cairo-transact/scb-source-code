* @ValidationCode : MjotMTUwNTg3ODgyOkNwMTI1MjoxNjQ0OTM2NjM5NTQ4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:50:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*********************************NI7OOOOOOOOOOOOOO*************************
SUBROUTINE REPORT.BR.RETURN6

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.STATUS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.STATUS
*------------------------------------------------------------------------
* IF R.NEW(SCB.BT.RETURN.REASON) NE '' THEN
*   TEXT = "FIRST BATCH :" R.NEW(SCB.BT.RETURN.REASON) ; CALL REM
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �������" ; CALL REM
*END
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.BR.RETURN6'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
    BT.ID = COMI
****UPDATED BY NESSMA
    INPUTTER  = R.BATCH<SCB.BT.INPUTTER>
    AUTH      = R.BATCH<SCB.BT.AUTHORISER>
    INP       = FIELD(INPUTTER,'_',2)
    AUTHI     = FIELD(AUTH,'_',2)
*------------------------------------------------------------------------
*    IF R.BATCH<SCB.BT.RETURN.REASON> NE '' THEN
    BR.ID     = R.BATCH<SCB.BT.OUR.REFERENCE>
    RETURN.ID = R.BATCH<SCB.BT.RETURN.REASON>
*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
    FOR I = 1 TO DD
        BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
        BR.ID2 = R.BATCH<SCB.BT.RETURN.REASON><1,I>
        IF BR.ID2 NE '' THEN
*------------------------------------------------------------------------
            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
            ACC.MO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
            ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>
            BILL.NO   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
            BANK.NO   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            BRN.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
            DR.NAME   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.DR.NAME>
            DATE1     = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.MAT.DATE>
            STAT      = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.STA>
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BR.STATUS':@FM:SCB.BS.DESCRIPTION,STAT,CHQSTAT)
            F.ITSS.SCB.BR.STATUS = 'F.SCB.BR.STATUS'
            FN.F.ITSS.SCB.BR.STATUS = ''
            CALL OPF(F.ITSS.SCB.BR.STATUS,FN.F.ITSS.SCB.BR.STATUS)
            CALL F.READ(F.ITSS.SCB.BR.STATUS,STAT,R.ITSS.SCB.BR.STATUS,FN.F.ITSS.SCB.BR.STATUS,ERROR.SCB.BR.STATUS)
            CHQSTAT=R.ITSS.SCB.BR.STATUS<SCB.BS.DESCRIPTION>
*           CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.MO,BRANCH.ID)
*Line [ 113 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
            BRN.ID  = AC.COMP[2]
            BRANCH.ID = TRIM(BRN.ID,"0","L")
*Line [ 122 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
            F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
            FN.F.ITSS.DEPT.ACCT.OFFICER = ''
            CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
            CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
            BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 129 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.MO,DRAWER.ID)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.MO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            DRAWER.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.NO,BANK.NAME)
            F.ITSS.SCB.BANK = 'F.SCB.BANK'
            FN.F.ITSS.SCB.BANK = ''
            CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
            CALL F.READ(F.ITSS.SCB.BANK,BANK.NO,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
            BANK.NAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*Line [ 143 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BRN.NO,BRANCH.NAME)
            F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
            FN.F.ITSS.SCB.BANK.BRANCH = ''
            CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
            CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BRN.NO,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
            BRANCH.NAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>

            CATEG.ID  = ACC.MO[11,4]
*Line [ 117 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
            
*Line [ 154 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

*** *DRAWER.ID  = ACC.MO[1,8]
*Line [ 163 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME1.2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>

            FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                CUST.ADDRESS1 = "���� ��������� ������"
            END ELSE
                CUST.ADDRESS1 =LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            END

            AMOUNT     = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT  = AMOUNT

            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 139 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 188 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT        = TODAY
            MAT.DATE   = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            MAT.DATE1  = DATE1[7,2]:'/':DATE1[5,2]:"/":DATE1[1,4]

*            INPUTTER  = R.BR<EB.BILL.REG.INPUTTER>
*            AUTH      = R.BR<EB.BILL.REG.AUTHORISER>
*            INP       = FIELD(INPUTTER,'_',2)
*            AUTHI     = FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX12 = SPACE(2)
            XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX10 = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,35]   = CUST.NAME1
            XX1<1,1>[3,35]  = CUST.NAME1.2
            XX2<1,1>[3,35]  = CUST.ADDRESS1

            XX<1,1>[45,15]  = '������     : '
            XX<1,1>[59,15]  = AMOUNT

            XX1<1,1>[45,15] = '��� ������ : '
            XX1<1,1>[59,15] = ACC.MO

            XX2<1,1>[45,15] = '��� ������ : '
            XX2<1,1>[59,15] = CATEG

            XX3<1,1>[45,15] = '������     : '
            XX3<1,1>[59,15] = CUR
            XX4<1,1>[45,15] = '����� ���� : '
            XX4<1,1>[59,15] = MAT.DATE1

* XX5<1,1>[3,15]  = '����� ���� : '
* XX5<1,1>[20,15] = DD:"  ":"�����"

            XX6<1,1>[1,15]  = '������'
            XX7<1,1>[1,15]  = INP

            XX6<1,1>[30,15] = '��� �������'
            XX7<1,1>[35,15] = COMI

            XX6<1,1>[60,15] = '������'
            XX7<1,1>[60,15] = AUTHI

            XX8<1,1>[3,35]  = '������ ������� : '
            XX8<1,1>[20,15] = OUT.AMT

*******TEXT = "DR.NAME :":DR.NAME ; CALL REM
            IF (STAT EQ 1) OR (STAT EQ 5) OR (STAT EQ 7) OR (STAT EQ 8) OR (STAT EQ 9) OR (STAT EQ 10) OR (STAT EQ 11) OR (STAT EQ 17) THEN
                XX9<1,1>[3,15]  = '������ : '
                XX9<1,1>[10,15] = "��� ���� ��� :":' ' :BILL.NO:' ': "�� ":' ' :DR.NAME:' ': "��":' ': MAT.DATE1
                XX10<1,1>[10,15]= "��� " :' ': BANK.NAME :' ': BRANCH.NAME
            END
            IF (STAT EQ 2) OR (STAT EQ 6) OR (STAT EQ 13) OR (STAT EQ 14) OR (STAT EQ 15) THEN
                XX9<1,1>[3,15]  = '������ : '
                XX9<1,1>[10,15] = "������� ����� ��� :":' ' :BILL.NO:' ': "�� ":' ' :DR.NAME:' ': "��":' ': MAT.DATE1
                XX10<1,1>[10,15]= "��� " :' ': BANK.NAME :' ': BRANCH.NAME
            END
*-------------------------------------------------------------------
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN
            PR.HD :="'L'":" "
            PR.HD :="'L'":SPACE(50) : "����� ����"
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT STR(' ',82)
            PRINT STR(' ',82)
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX10<1,1>
* PRINT XX5<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR('-',82)
            PRINT XX7<1,1>
        END
*------------------------------------------------------------------
    NEXT I
*END
*===============================================================
RETURN
END
