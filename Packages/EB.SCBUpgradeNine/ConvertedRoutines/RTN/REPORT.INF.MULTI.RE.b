* @ValidationCode : MjotNDQwNzcwMDA3OkNwMTI1MjoxNjQ0OTM5NTAwMTY2OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:38:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************AHMED NAHRAWY(FOR EVER NI7OOOOOOOOO)********
SUBROUTINE REPORT.INF.MULTI.RE
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.INF.MULTI.RE'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    YTEXT = "Enter IN.NO : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ID = COMI
    FN.LD='F.INF.MULTI.TXN' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    F.ITSS.DATES = 'F.DATES'
    FN.F.ITSS.DATES = ''
    CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
    CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
    DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
*------------------------------------------------------------------------
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,ERR1)
    AC1 = R.LD<INF.MLT.ACCOUNT.NUMBER>
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(AC1,@VM)
    I = 0
    INP  = R.NEW(INF.MLT.INPUTTER)
    AUTH = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
    F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
    FN.F.ITSS.USER.SIGN.ON.NAME = ''
    CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
    CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
    AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>
    FOR J = 1 TO DD
        ACCC      = R.LD<INF.MLT.ACCOUNT.NUMBER><1,J>
        CUR       = R.LD<INF.MLT.CURRENCY><1,J>
        AMT1      = R.LD<INF.MLT.AMOUNT.LCY><1,J>
        AMT2      = R.LD<INF.MLT.AMOUNT.FCY><1,J>
        SIGN      = R.LD<INF.MLT.SIGN><1,J>
        IF SIGN EQ 'DEBIT' THEN
            SIGN  = '���'
        END ELSE
            IF SIGN EQ 'CREDIT' THEN
                SIGN = '�����'
            END
        END
        IF ACCC EQ '' THEN
            ACCC = R.LD<INF.MLT.PL.CATEGORY><1,J>
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,ACCC,MYCAT)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,ACCC,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            MYCAT=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
            CUST.NAME = MYCAT
            CUST.NAME1= MYCAT
            INTER = MYCAT
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.LD<INF.MLT.ACCOUNT.NUMBER><1,J>
*Line [ 119 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCC,CUS.ID)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CUS.ID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACCC,CUR.ID)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CUR.ID=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 133 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACCC,CATEG)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
****                CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
                BRANCH.ID  = AC.COMP[8,2]

*Line [ 150 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
                F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
                FN.F.ITSS.DEPT.ACCT.OFFICER = ''
                CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
                CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
                BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
*Line [ 157 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CATEGORY':@FM:EB.CAT.SHORT.NAME,CATEG,MYCAT1)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                MYCAT1=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
                INTER = MYCAT1
*Line [ 165 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
                F.ITSS.CUSTOMER = 'F.CUSTOMER'
                FN.F.ITSS.CUSTOMER = ''
                CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                LOCAL.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
                F.ITSS.CURRENCY = 'F.CURRENCY'
                FN.F.ITSS.CURRENCY = ''
                CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                CUST.NAME    = LOCAL.REF1<1,CULR.ARABIC.NAME>
                CUST.NAME1   = LOCAL.REF1<1,CULR.ARABIC.NAME.2>
                CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
                CUST.ADDRESS1= LOCAL.REF1<1,CULR.ARABIC.ADDRESS,2>
                CUST.REGOIN  = LOCAL.REF1<1,CULR.REGION><1,1>
                CUST.GOVERN  = LOCAL.REF1<1,CULR.GOVERNORATE><1,1>

                IF CUST.ADDRESS EQ  " " THEN
                    CUST.ADDRESS = "���� ��������� ������"
                END
            END
        END
*Line [ 125 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 192 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
        BEGIN CASE
            CASE CUR.ID NE 'EGP'
                IN.AMOUNT = AMT2
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 135 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 208 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
                F.ITSS.CURRENCY = 'F.CURRENCY'
                FN.F.ITSS.CURRENCY = ''
                CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                XX6<1,J>[1,15]  = '������ ������� ��������: ':OUT.AMT:IN.AMOUNT
            CASE CUR.ID EQ 'EGP'
                IN.AMOUNT = AMT1
                CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 143 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
            
*Line [ 222 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
                F.ITSS.CURRENCY = 'F.CURRENCY'
                FN.F.ITSS.CURRENCY = ''
                CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
                XX6<1,J>[1,15]  = '������ ������� �������: ':OUT.AMT:IN.AMOUNT
        END CASE

******* CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE,BRANCH.ID)
        YYBRN = FIELD(BRANCH,'.',2)
*Line [ 235 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
***** CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*Line [ 154 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 245 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132) ; XX15 = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132) ; XX16 = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132) ; XX17 = SPACE(132)
        I ++

        XX10<1,J>[3,15]  = '�����       : '
        XX10<1,J>[20,35] = CUST.NAME :' ':CUST.NAME1

        XX1<1,J>[64,15]  = '��� ������  : '
        XX1<1,J>[80,15]  = ACCC

        XX1<1,J>[3,15]   = '�������      : '
        XX1<1,J>[20,35]  = CUST.ADDRESS :' ':CUST.ADDRESS1
*        XX1<1,J+1>[20,35]= CUST.REGOIN  :' ':CUST.GOVERN

        XX3<1,J>[45,15]  = ' ��� ������ : '
        XX3<1,J>[64,15]  =  INTER

        XX8<1,J>[45,15] = '������ :' :CUR

        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

        XX4<1,J>[45,15]  = ' ������:' :IN.AMOUNT :CUR
        XX5<1,J>[45,15]  = '����� ���� :' : T.DAY

        XX6<1,J>[1,15]  = '������ ������� �������: ':OUT.AMT
        XX7<1,J>[65,15] = '�� ��� ���� ������'

        XX15<1,J>[1,15] ="������"
        XX16<1,J>[1,15]= INP

        XX15<1,J>[30,15] ="������"
        XX16<1,J>[30,15]= AUTHI

        XX15<1,J>[45,15] ="�������"
        XX16<1,J>[45,15] = COMI

*-------------------------------------------------------------------
        IF ACCC[1,2] EQ 99 THEN
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"�����":SIGN
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
**PRINT XX<1,I>
***** IF ACCC[1,2] EQ 99 THEN
            PRINT XX10<1,I>
            PRINT XX1<1,I>
            PRINT XX3<1,I>
            PRINT XX4<1,I>
            PRINT XX8<1,I>
            PRINT XX5<1,I>
            PRINT STR(' ',82)
            PRINT XX6<1,I>
            PRINT XX7<1,I>
            PRINT STR('=',82)
            PRINT XX15<1,I>
            PRINT XX16<1,I>
            PRINT STR(' ',82)
            PRINT STR(' ',82)
*******            PRINT STR('=',82)
        END
    NEXT J
*===============================================================
RETURN
END
