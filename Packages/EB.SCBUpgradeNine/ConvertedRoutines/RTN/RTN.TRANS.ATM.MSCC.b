* @ValidationCode : MjoxNDU0NjkwNTI6Q3AxMjUyOjE2NDA3NzIyMjExOTk6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 12:03:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*----------------------------------
*-- CREATE BY NESSMA
*----------------------------------
    SUBROUTINE RTN.TRANS.ATM.MSCC
*----------------------------------
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TRANS.DAILY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------
    DIR.NAME.2 = "MSCC/MSCC.NEW"

    EXECUTE "sh MSCC/fx_file.sh"
    GOSUB OPF.FILES
    GOSUB CHECK.SCB.ATM
    EXECUTE 'DELETE ':'MSCC/MSCC.txt'

    REN.FILE ="cp -R ":DIR.NAME.2:"/":NEW.FILE:" ":FN.OFS.IN:"/":NEW.FILE
    EXECUTE REN.FILE
    RETURN
*---------------------------------------------
OPF.FILES:
*---------
    FN.ATM  = "F.SCB.ATM.TERMINAL.ID" ; F.ATM  = ""
    CALL OPF(FN.ATM, F.ATM)

    FN.SATM = "F.SCB.ATM.TRANS.DAILY" ; F.SATM = ""
    CALL OPF(FN.SATM, F.SATM)
    RETURN
*---------------------------------------------------
CHECK.SCB.ATM:
*-------------
    TOD = TODAY
    MON = TOD[5,2] + 1 - 1
    TOD = TOD[7,2]:"-":MON:"-":TOD[1,4]
    FILE.NAME = "MSCC.txt"
    FILE.ID   = "MSCC":TOD:".txt"

    CALL F.READ(FN.SATM,FILE.ID,R.SATM,F.SATM,ER.SATM)
*** FOR TESTING ONLY ER.SATM =  1
    IF ER.SATM THEN
        R.SATM<SCB.ATM.PROCESSING.DATE>  =  TODAY

        GOSUB CREATE.OFS
        GOSUB INITIAL

        WRITE R.SATM TO F.SATM , FILE.ID  ON ERROR
            STOP 'CAN NOT WRITE RECORD ':FILE.ID:' TO FILE ':F.SATM
        END
        TEXT = "�� �������" ; CALL REM
    END ELSE
        TEXT = "�� ������� ���� ����� �� ���" ; CALL REM
    END
    RETURN
*---------------------------------------------------
CREATE.OFS:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "ATM.TRANS"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "ATM-TRANS-":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME.2,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME.2:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME.2
    END

    OPENSEQ DIR.NAME.2, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME.2
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME.2
        END
    END
    RETURN
*-------------------------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ''
    R.LINE = ""

    ERR='' ; ER=''

    DIR.NAMEE  = "MSCC"
    OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
        TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
        RETURN
    END
    IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

    INDX = 1
    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM BB THEN
            FLG     = 0
            R.LINE  = TRIM (R.LINE)
            ATM.ID  = TRIM(FIELD(R.LINE,"|",12))
            ATM.ID  = CHANGE(ATM.ID,"SUEZ ","")
            AMT     = TRIM(FIELD(R.LINE,"|",9)) / 100
            TYP     = TRIM(FIELD(R.LINE,"|",10))
            PAN     = TRIM(FIELD(R.LINE,"|",1))
            WS.DTR  = TRIM(FIELD(R.LINE,"|",18))
            WS.CTR  = TRIM(FIELD(R.LINE,"|",18))
            NETWORK = TRIM(FIELD(R.LINE,"|",20))

            IF AMT GT 0 THEN
                IF PAN[1,1] EQ 4 AND NETWORK EQ 'VISA' THEN
                    FLG = 1
                END
                IF PAN[1,6] EQ '404238' OR PAN[1,6] EQ '404239' THEN
                    FLG = 1
                END

                IF FLG EQ 1 THEN
                    GOSUB PROCESS
                END
            END
        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ BB
    RETURN
*------------------------------------------------------------------
PROCESS:
*-------
    CALL F.READ(FN.ATM,ATM.ID,R.ATM,F.ATM,ER.ATM)
    IF NOT(ER.ATM) THEN
        ACCOUNT.NUMBER = R.ATM<SCB.ATM.ACCOUNT.NUMBER>
        ACCOUNT.FIX    = "EGP1123400010099"
        BRANCH.CODE    = "EG0010099"
        COMP.CODE      = BRANCH.CODE[8,2]
        OFS.USER.INFO  = "INPUTT" : COMP.CODE : "//" : BRANCH.CODE

        IF TYP[1,2] EQ 'DB' THEN
            DR.ACCOUNT = ACCOUNT.FIX
            CR.ACCOUNT = ACCOUNT.NUMBER
        END

        IF TYP[1,2] EQ 'CR' THEN
            DR.ACCOUNT = ACCOUNT.NUMBER
            CR.ACCOUNT = ACCOUNT.FIX
        END

        TRNS.TYPE = "AC70"
        CR.AMT    = AMT
        DR.CUR    = "EGP"
        CR.CUR    = "EGP"
        DTR       =  WS.DTR
        CTR       =  WS.CTR

        GOSUB BUILD.RECORD
    END
    RETURN
*------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :DTR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :CTR:COMMA

    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :CR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :CR.AMT:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK="       :"SCB":COMMA
    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA

    OFS.MESSAGE.DATA :=  "LOCAL.REF:32:1="      :ATM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:33:1="      :ATM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:43:1="      :PAN:COMMA

    ZZZ   = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    WRITESEQ ZZZ TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZZ
    END

    RETURN
*------------------------------------------------------------------
END
