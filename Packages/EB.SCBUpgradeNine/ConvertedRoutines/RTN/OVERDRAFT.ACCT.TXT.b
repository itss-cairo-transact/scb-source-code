* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE OVERDRAFT.ACCT.TXT
*   PROGRAM  OVERDRAFT.ACCT.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*****
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "OVERDRAFT.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/":' ':"OVERDRAFT.TXT"
        HUSH OFF
    END
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "OVERDRAFT.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE OVERDRAF.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create OVERDRAF.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END

*****
    FN.AC  = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.LI  = 'FBNK.LIMIT' ; F.LI = '' ; R.LI = ''
    CALL OPF( FN.LI,F.LI)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT'  ; F.ADI = '' ; R.ADI = ''
    CALL OPF (FN.ADI, F.ADI)
*---------------------------
    T.SEL = "SELECT FBNK.ACCOUNT WITH (CATEGORY GE 1100 AND CATEGORY LT 1220) OR (CATEGORY GT 1227 AND CATEGORY LE 1599) AND CATEGORY NE 9090 AND POSTING.RESTRICT LE 90 AND OPEN.ACTUAL.BAL NE 0 AND OPEN.ACTUAL.BAL NE '' "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT SELECTED

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)
            ACCT.ID   = KEY.LIST<I>
            ON.ACTUAL.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
            LIMIT.REF = R.AC<AC.LIMIT.REF>
            CUS.ID    = R.AC<AC.CUSTOMER>
*            LIM.ID    = CUS.ID:'.':LIMIT.REF
            LIM.ID    = CUS.ID:'.':FMT(LIMIT.REF,'R%10')
            CALL F.READ(FN.LI,LIM.ID, R.LI, F.LI, ETEXT)
            ADV.AMT   = R.LI<LI.ADVISED.AMOUNT>
            ACCT.NAME = R.AC<AC.ACCOUNT.TITLE.1>
            LMT.RATE = ''
            T.SEL.N = "SELECT FBNK.ACCOUNT.DEBIT.INT WITH @ID LIKE ":ACCT.ID:"... BY @ID"
            CALL EB.READLIST(T.SEL.N, KEY.LIST.N, "", SELECTED.N, ASD.N)
            IF SELECTED.N THEN
                ADI.ID = KEY.LIST.N<SELECTED.N>
                CALL F.READ(FN.ADI,ADI.ID, R.ADI, F.ADI, ETEXT.ADI)
                LMT.RATE = R.ADI<IC.ADI.DR.INT.RATE><1,1>
                IF R.ADI<IC.ADI.DR.MARGIN.OPER> = "ADD" THEN
                    LMT.RATE = R.ADI<IC.ADI.DR.INT.RATE><1,1> + R.ADI<IC.ADI.DR.MARGIN.RATE>
                END


            END

            BB.DATA  = ACCT.ID:'|'
            BB.DATA := ACCT.NAME:'|'
            BB.DATA := ON.ACTUAL.BAL:'|'
            BB.DATA := ADV.AMT:'|'
            BB.DATA := LMT.RATE

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
