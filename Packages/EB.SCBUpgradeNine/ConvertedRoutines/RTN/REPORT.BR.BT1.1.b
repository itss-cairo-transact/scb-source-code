* @ValidationCode : MjoxMDE1MTE2ODUwOkNwMTI1MjoxNjQ0OTM1ODIxMjQzOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:37:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------
** Create By Ni7ooo
** Update By Nessma
*-----------------------
SUBROUTINE REPORT.BR.BT1.1

*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.USER
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CURRENCY
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CATEGORY
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INSERT- ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.BILL.REGISTER
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DEPT.ACCT.OFFICER
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_AC.LOCAL.REFS
    $INSERT            I_BR.LOCAL.REFS
*------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �����" ; CALL REM
RETURN
*================================================
INITIATE:
*--------
    REPORT.ID = 'REPORT.BR.BT1.1'
    CALL PRINTER.ON(REPORT.ID,'')

    XX   = SPACE(132)
RETURN
*================================================
PROCESS:
*-------
    FN.BATCH = 'F.SCB.BT.BATCH'      ; F.BATCH  = ''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER'  ; F.BR     = ''
    CALL OPF(FN.BR,F.BR)

    FN.BATCH = 'F.SCB.BT.BATCH'     ; F.BATCH  = ''
    CALL OPF(FN.BATCH,F.BATCH)

    AMOUNT       = ''
    CUST.ADDRESS = ''
*------------------------------------------------
    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
    BT.ID    = COMI

    INPUTTER = R.BATCH<SCB.BT.INPUTTER>
    AUTH     = R.BATCH<SCB.BT.AUTHORISER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    BR.ID    = R.BATCH<SCB.BT.OUR.REFERENCE>
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD       = DCOUNT(BR.ID,@VM)

    FOR I = 1 TO DD
        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> EQ '' THEN
            BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)

            ACC.NO = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>
*   CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            AC.COMP=R.ITSS.ACCOUNT<AC.CO.CODE>
            BRN.ID  = AC.COMP[2]
            BRANCH.ID = TRIM(BRN.ID,"0","L")
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
            F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
            FN.F.ITSS.DEPT.ACCT.OFFICER = ''
            CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
            CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
            BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>

            IF ACC.NO[1,3] EQ 'EGP' THEN
*Line [ 115 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACC.NO,CUST.NAME)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                CUST.NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
                CATEG.ID     =  ACC.NO[4,8]
*Line [ 106 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
                
*Line [ 125 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
                CUST.ADDRESS = ''
            END ELSE
                IF ACC.NO[1,3] NE 'EGP' THEN
                    CATEG.ID   = ACC.NO[11,4]
*Line [ 113 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
                    
*Line [ 138 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
                    F.ITSS.CATEGORY = 'F.CATEGORY'
                    FN.F.ITSS.CATEGORY = ''
                    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                    CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

                    DRAWER.ID  = ACC.NO[1,8]
*Line [ 147 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
                    F.ITSS.CUSTOMER = 'F.CUSTOMER'
                    FN.F.ITSS.CUSTOMER = ''
                    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
                    CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
                    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
                    CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
                    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
                END
            END

            AMOUNT     = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT  = AMOUNT
            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 129 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
            
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR(('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
            OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
            DAT       = TODAY
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
*--------------------------------------------
            PR.HD  = "REPORT.BR.BT1.1"
            HEADING PR.HD
*Line [ 179 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
            FN.F.ITSS.DEPT.ACCT.OFFICER = ''
            CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
            CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
            BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PRINT SPACE(5) : SPACE(1):"��� ���� ������"
            PRINT SPACE(5) : "������� : ":T.DAY
            PRINT SPACE(5) : "����� : ":YYBRN
*----------------------------------------------
            XX = SPACE(120)
            XX<1,1>[3,30]    = "��� ������"
            XX<1,1>[50,20]   = CUST.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,30]    = "������� "
            XX<1,1>[50,20]   = CUST.ADDRESS
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,20]    = '������     : '
            XX<1,1>[50,15]   = AMOUNT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,20]  = '��� ������ : '
            XX<1,1>[50,15] = ACC.NO
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,20]  = '��� ������ : '
            XX<1,1>[50,15] = CATEG
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,20]   = '������     : '
            XX<1,1>[50,15]  = CUR
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '����� ���� : '
            XX<1,1>[50,15]  = MAT.DATE
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '����� ���� : '
            XX<1,1>[50,15]  = DD:"  ":"�����"
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '������'
            XX<1,1>[50,15]  = AUTHI
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� �������'
            XX<1,1>[50,15]  = COMI
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '������'
            XX<1,1>[50,15]  = INP
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,25]   = '������ ������� : '
            XX<1,1>[50,15]  = OUT.AMT
            PRINT XX<1,1>
            XX<1,1> = ""
        END
    NEXT I
RETURN
*===============================================================
PRINT.HEAD:
*----------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50) : "����� ���"
    PR.HD :="'L'":"REPORT.BR.BT1.1"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*===============================================================
RETURN
END
