* @ValidationCode : MjotMzUwNDIyNTU6Q3AxMjUyOjE2NDQ5Mzc5OTQ1OTM6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 17:13:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>389</Rating>
*-----------------------------------------------------------------------------
*-----------------------
** Create By Ni7ooo
** Update By Nessma
*-----------------------
SUBROUTINE REPORT.FT.BR4

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BL.CHQ.TYPE
*-------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
*Line [ 56 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 57 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������"  ;  CALL REM
RETURN
*==============================================================
INITIATE:
    REPORT.ID = 'REPORT.FT.BR4'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    FN.BATCH = 'F.SCB.BT.BATCH'      ; F.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER'  ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    ID      = ID.NEW
    DATE.TO = TODAY[3,6]:"..."
*---------------------------------------------------------------
    REAS     = R.NEW(SCB.BT.OUR.REFERENCE)
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    RET.REAS = DCOUNT(REAS,@VM)

    FOR I = 1 TO RET.REAS
        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> EQ '' THEN
            BR.ID     = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
            CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E1)
            DRAWER.ID = R.BR<EB.BILL.REG.DRAWER><1,I>
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,DRAWER.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>

            BANK.NO      = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            BRN.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>

*Line [ 104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.NO,BANK.NAME)
            F.ITSS.SCB.BANK = 'F.SCB.BANK'
            FN.F.ITSS.SCB.BANK = ''
            CALL OPF(F.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK)
            CALL F.READ(F.ITSS.SCB.BANK,BANK.NO,R.ITSS.SCB.BANK,FN.F.ITSS.SCB.BANK,ERROR.SCB.BANK)
            BANK.NAME=R.ITSS.SCB.BANK<SCB.BAN.BANK.NAME>
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BRN.NO,BRANCH.NAME)
            F.ITSS.SCB.BANK.BRANCH = 'F.SCB.BANK.BRANCH'
            FN.F.ITSS.SCB.BANK.BRANCH = ''
            CALL OPF(F.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH)
            CALL F.READ(F.ITSS.SCB.BANK.BRANCH,BRN.NO,R.ITSS.SCB.BANK.BRANCH,FN.F.ITSS.SCB.BANK.BRANCH,ERROR.SCB.BANK.BRANCH)
            BRANCH.NAME=R.ITSS.SCB.BANK.BRANCH<SCB.BAB.BRANCH.NAME>

            CHEQ.STAT = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BIL.CHQ.TYPE>
*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.BL.CHQ.TYPE':@FM:BL.CHQ.DESCRIPTION,CHEQ.STAT,CHEQ.NAME)
            F.ITSS.SCB.BL.CHQ.TYPE = 'F.SCB.BL.CHQ.TYPE'
            FN.F.ITSS.SCB.BL.CHQ.TYPE = ''
            CALL OPF(F.ITSS.SCB.BL.CHQ.TYPE,FN.F.ITSS.SCB.BL.CHQ.TYPE)
            CALL F.READ(F.ITSS.SCB.BL.CHQ.TYPE,CHEQ.STAT,R.ITSS.SCB.BL.CHQ.TYPE,FN.F.ITSS.SCB.BL.CHQ.TYPE,ERROR.SCB.BL.CHQ.TYPE)
            CHEQ.NAME=R.ITSS.SCB.BL.CHQ.TYPE<BL.CHQ.DESCRIPTION>

            CATEG.ID  = ACC.NO[11,4]
*Line [ 104 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 130 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
            AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
*Line [ 108 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
            IN.AMOUNT = AMOUNT
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            OUT.AMT = OUT.AMOUNT: ' ' : CUR : ' ' : '�����'
*Line [ 113 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR.ID,CUR)
            F.ITSS.CURRENCY = 'F.CURRENCY'
            FN.F.ITSS.CURRENCY = ''
            CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
            CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
            CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

            DAT       =  R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.RECEIVE.DATE>
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            CHQ.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
            INPUTTER  = R.BR<EB.BILL.REG.INPUTTER>
            AUTH      = R.BR<EB.BILL.REG.AUTHORISER>
            INP       = FIELD(INPUTTER,'_',2)
            AUTHI     = FIELD(AUTH,'_',2)
            DAT       = TODAY
            CALL CDT("",DAT,'+1W')

            XX   = SPACE(132)
*--------------------------------------------
            PR.HD  = "REPORT.FT.BR4"
            HEADING PR.HD

*Line [ 174 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
            FN.F.ITSS.DEPT.ACCT.OFFICER = ''
            CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
            CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
            BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PRINT SPACE(5) : SPACE(1):"��� ���� ������"
            PRINT SPACE(5) : "������� : ":T.DAY
            PRINT SPACE(5) : "����� : ":YYBRN
*----------------------------------------------
            XX<1,1>[3,15]   = CUST.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15]  = '������ : '
            XX<1,1>[60,15]  = AMOUNT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = CUST.ADDRESS
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15] = '��� ������ : '
            XX<1,1>[64,15] = ACC.NO
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '����� ��� ��� :' :BANK.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15] = '��� ������ : '
            XX<1,1>[64,15] = CATEG
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� ':BRANCH.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15] = '������ : '
            XX<1,1>[60,15] = CUR
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15] = '����� ���� : '
            XX<1,1>[20,15] = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '��� ����� : '
            XX<1,1>[15,15] = CHQ.NO:' ':CHEQ.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '������'
            XX<1,1>[15,15]  = INP
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,20] = '��� �������'
            XX<1,1>[30,15] = ID.NEW
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15] = '������'
            XX<1,1>[30,15] = AUTHI
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '������ �������:'
            XX<1,1>[20,15] = OUT.AMT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15]  = '������:'
            XX<1,1>[20,15] = '����� �����'
            PRINT XX<1,1>
            XX<1,1> = ""
        END
    NEXT I
RETURN
*=================================================================
PRINT.HEAD:
*----------
*Line [ 264 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"����� : ":YYBRN
    PR.HD :="'L'":SPACE(1):"����� �����"
    PR.HD :="'L'":"REPORT.FT.BR4"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
RETURN
*==================================================================
END
