* @ValidationCode : MjotMjEwNjEzMjY6Q3AxMjUyOjE2NDQ5NDIwNDEzMzg6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 18:20:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-------------------------------------------
* CREATE BY RIHAM YOUSSIF 21/08/2014
*-------------------------------------------
SUBROUTINE REPRINT.CD.SUEZ.100

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CD.TYPES
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*-------------------------------------------

    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN

*----------------------------------------------
INITIATE:
*--------
    COMP     = ID.COMPANY
    IF (OPERATOR EQ 'SCB.79061') THEN
        REPORT.ID='REPORT.CD.SUEZ.99'
    END ELSE
        IF (OPERATOR EQ 'SCB.60411') THEN
            REPORT.ID='REPORT.CD.SUEZ.990'
        END ELSE
            IF (OPERATOR EQ 'SCB.14621') THEN
                REPORT.ID='REPORT.CD.SUEZ.996'
            END ELSE
                REPORT.ID='REPORT.CD.SUEZ.':ID.COMPANY[2]
            END
        END
    END
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*-------
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS'     ; F.LD     = ''
    FN.LD.HIS = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    FN.CORD   = 'FBNK.BASIC.INTEREST'            ; F.CORD   = '' ; R.CORD = ''
    CALL OPF(FN.CORD,F.CORD)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)
    FN.COM = 'F.COMPANY'; F.COM  = ''
    CALL OPF(FN.COM,F.COM)
    CD.TYPE   = ''
    FINTYP    = ''
    FF        = ''
    TT        = ''
    NAME11    = ''
    DATE1     = ''
    ISSU.DATE = ''
    DATE2     = ''
    MAT.DATE  = ''
    AMT       = ''
    TOT.AMT   = ''
    TOT.AMT1  = ''
    TOT.AMT1  = ''
    CUR       = ''
    NO.TOT.AMT = ''
    IN.AMOUNT = ''
    OUT.AMT   = ''
    BRN.CODE  = ''
    ACT.AMT   = ''
    XX      = ''
    XX1     = ''  ; XX2   = '' ; XX3     = ''
    XX4     = ''  ; XX5   = '' ; XX6     = ''
    XX7     = ''  ; XX8   = '' ; XX9     = ''
    XX10    = ''  ; XX11  = '' ; XX12    = ''
    XX13    = ''  ; XX14  = '' ; XX15    = ''
    XX16    = ''  ; XX17  = '' ; XX18    = ''
    XX19    = ''  ; XX20  = '' ; XX21    = ''
    XX22    = ''  ; XX23  = '' ; XX24    = ''
    XX25    = ''  ; XX26  = '' ; XX27    = ''
    XX144   = ''
*------------------------------------------------------------------------

    YTEXT = "Enter the No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    TEXT = "COMI : " : COMI ; CALL REM

    TYP      = R.LD<LD.LOCAL.REF><1,LDLR.CD.TYPE>
    CDTYP    = FIELD(TYP,'-',2)

    CD.TYPE   = R.LD<LD.LOCAL.REF><1,LDLR.CD.TYPE>
    FINTYP    = FIELD(CD.TYPE,'-',2)

    FF        = R.LD<LD.LOCAL.REF><1,LDLR.ORDER>
    TT        = R.LD<LD.LOCAL.REF><1,LDLR.ARTICLE.NO>

    NAME11    = R.LD<LD.LOCAL.REF><1,LDLR.IN.RESPECT.OF>


    DATE1     = R.LD<LD.VALUE.DATE>
    ISSU.DATE  = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]

    DATE2     = R.LD<LD.FIN.MAT.DATE>
    MAT.DATE  = DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]

    CDNO      = R.LD<LD.LOCAL.REF><1,LDLR.CD.QUANTITY>
    AMT       = R.LD<LD.AMOUNT>
    TOT.AMT   = AMT
    TOT.AMT1  = FMT(AMT,"L0,")
    TOT.AMT1  = CHANGE(TOT.AMT1,',','.')
    CUR       = R.LD<LD.CURRENCY>
    
    
*Line [ 157 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 158 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CUR,CUR22)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR22=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

    NO.TOT.AMT = TOT.AMT1 : ' ' : CUR22
    IN.AMOUNT = TOT.AMT
    CALL WORDS.ARABIC.DEAL.CD(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR22 : ' ' : '��� �����'

    BRN.CODE = R.LD<LD.CO.CODE>
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.CODE,YYBRN)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,BRN.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    YYBRN=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    ACT.AMT = '180%'
*FIRST (TOT.AMT/10) * 18
*ACT.AMT= AMT * (12/100) * 5 * CDNO
    XX      = SPACE(132)
    XX1     = SPACE(132)  ; XX2   = SPACE(132) ; XX3     = SPACE(132)
    XX4     = SPACE(132)  ; XX5   = SPACE(132) ; XX6     = SPACE(132)
    XX7     = SPACE(132)  ; XX8   = SPACE(132) ; XX9     = SPACE(132)
    XX10    = SPACE(132)  ; XX11  = SPACE(132) ; XX12    = SPACE(132)
    XX13    = SPACE(132)  ; XX14  = SPACE(132) ; XX15    = SPACE(132)
    XX16    = SPACE(132)  ; XX17  = SPACE(132) ; XX18    = SPACE(132)
    XX19    = SPACE(132)  ; XX20  = SPACE(132) ; XX21    = SPACE(132)
    XX22    = SPACE(132)  ; XX23  = SPACE(132) ; XX24    = SPACE(132)
    XX25    = SPACE(132)  ; XX26  = SPACE(132) ; XX27    = SPACE(132)
    XX144   = SPACE(132)


    IF CDTYP EQ '10' THEN
        XX<1,1>[3,35]      = 'REPORT.CD.SUEZ.10'
    END
    IF CDTYP EQ '100' THEN
        XX<1,1>[3,35]      = 'REPORT.CD.SUEZ.100'
    END

    XX1<1,1>[3,35]     =  '��� �������'
    XX2<1,1>[3,35]     =  FINTYP


    XX3<1,1>[3,35]     = '����� ������� ��:'
    XX4<1,1>[3,35]     = FF

    XX5<1,1>[3,35]     = '����� ������� ���:'
    XX6<1,1>[3,35]     = TT



    XX7<1,1>[3,35]     = '��� ������:'
    XX8<1,1>[3,70]     = NAME11

    XX9<1,1>[3,35]     = '����� �������:'
    XX10<1,1>[3,35]    = ISSU.DATE

    XX11<1,1>[3,35]    = '����� ���������:'
    XX12<1,1>[3,35]    = MAT.DATE

    XX13<1,1>[3,35]    = '������ ���� �������:'
    XX14<1,1>[3,35]    = NO.TOT.AMT
    XX144<1,1>[3,35]   = OUT.AMT

    XX15<1,1>[3,35]    = '����� ������'
    XX16<1,1>[3,35]    =  YYBRN

    XX17<1,1>[3,35]    = '������ ����������� '
    XX18<1,1>[3,35]    =  ACT.AMT

    XX19<1,1>[3,35]    = '��� ������� ������'
    XX20<1,1>[3,35]    =  COMI


    XX21<1,1>[3,35]    =  '��� ������ ��������'
    XX22<1,1>[3,35]    =  '����� ������'
    XX23<1,1>[3,35]    =  '�� ��� ���� ������'
    XX24<1,1>[3,35]      = '��� ������ ������ '
    XX25<1,1>[3,35]      = '12% '
    XX26<1,1>[3,35]      = '���� '


    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT XX8<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX144<1,1>
    PRINT STR(' ',82)
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT STR(' ',82)
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT STR(' ',82)
    PRINT XX19<1,1>
    PRINT XX20<1,1>
    PRINT STR(' ',82)
    PRINT XX21<1,1>
    PRINT XX22<1,1>
    PRINT XX23<1,1>
    PRINT XX24<1,1>
    PRINT XX25<1,1>
    PRINT XX26<1,1>
RETURN

END
