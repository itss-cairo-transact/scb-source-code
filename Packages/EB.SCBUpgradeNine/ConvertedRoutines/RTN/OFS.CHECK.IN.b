* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.CHECK.IN
***   PROGRAM OFS.CHECK.IN
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE

***************************************************
    FN.OFS.IN = "../bnk.data/OFS/OFS.IN"
    F.OFS.IN  = ''
    CALL OPF( FN.OFS.IN,F.OFS.IN)
**   GOSUB CALLL
**   IF SELECTED.IN THEN
**       GOSUB SLEPP
**   END ELSE
**        GOSUB PROGRAM.END
**   END

****************************************
CALLL:
***TEXT = 'CALL' ; CALL REM
    T.SEL.IN    = "SSELECT ":FN.OFS.IN
    KEY.LIST.IN = ""
    SELECTED.IN = ""
    ER.MSG11=""
    CALL EB.READLIST(T.SEL.IN,KEY.LIST.IN,"",SELECTED.IN,ER.MSG11)
    FN.OFS.IN= FN.OFS.IN ; F.OFS.IN = '' ; R.OFS.IN = ''
***TEXT = 'SELECTED.IN=':SELECTED.IN ; CALL REM
    CALL OPF( FN.OFS.IN,F.OFS.IN)
    IF SELECTED.IN THEN
        GOSUB SLEPP
    END ELSE

***** R15-UPDATE *****
        KEY.LIST2="" ; SELECTED2="" ;  ER.OFS="" ; FLAG = 0
        T.SEL2 = "SELECT F.OFS.MESSAGE.QUEUE"
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.OFS)
        IF SELECTED2 THEN
            GOSUB SLEPP
        END ELSE
            GOTO PROGRAM.END
        END
    END
********************
********************
    RETURN
*************************************************
SLEPP:
***TEXT = 'SLEEP' ; CALL REM
    SLEEP 50
*Line [ 71 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 72 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLL
    RETURN
******************************************************
PROGRAM.END:
***TEXT = 'PROGRAMEND' ; CALL REM
    RETURN
END
