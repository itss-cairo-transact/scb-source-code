* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*----------------------------------
*-- CREATE BY NESSMA ON 2016/03/15
*----------------------------------
    SUBROUTINE RTN.LOAD.MSCC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAD.MSCC
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TRANS.DAILY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------
    EXECUTE "CLEAR-FILE F.SCB.LOAD.MSCC"
    EXECUTE "sh MSCC/fx_file.sh"
    GOSUB OPF.FILES
    GOSUB CHECK.SCB.ATM
    RETURN
*---------------------------------------------
OPF.FILES:
*---------
    FN.ATM  = "F.SCB.ATM.TERMINAL.ID" ; F.ATM  = ""
    CALL OPF(FN.ATM, F.ATM)

    FN.SATM = "F.SCB.ATM.TRANS.DAILY" ; F.SATM = ""
    CALL OPF(FN.SATM, F.SATM)

    FN.TMP = "F.SCB.LOAD.MSCC" ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)
    RETURN
*---------------------------------------------------
CHECK.SCB.ATM:
*-------------
    TOD = TODAY
    MON = TOD[5,2] + 1 - 1
    TOD = TOD[7,2]:"-":MON:"-":TOD[1,4]
    FILE.NAME = "MSCC.txt"
    FILE.ID   = "MSCC":TOD:".txt"

    CALL F.READ(FN.SATM,FILE.ID,R.SATM,F.SATM,ER.SATM)
    ER.SATM =  1
    IF ER.SATM THEN
        R.SATM<SCB.ATM.PROCESSING.DATE>  =  TODAY

        GOSUB CREATE.OFS
        GOSUB INITIAL

*       WRITE R.SATM TO F.SATM , FILE.ID  ON ERROR
*           STOP 'CAN NOT WRITE RECORD ':FILE.ID:' TO FILE ':F.SATM
*       END
        TEXT = "�� ����� �����"
        CALL REM
    END ELSE
        TEXT = "�� ������� ���� ����� �� ���" ; CALL REM
    END
    RETURN
*---------------------------------------------------
CREATE.OFS:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "ATM.TRANS"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "ATM-TRANS-":TODAY:".":RND(10000)

*    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
*        CLOSESEQ V.FILE.IN
*        HUSH ON
*        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
*        HUSH OFF
*        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
*    END

*    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
*        CREATE V.FILE.IN THEN
*            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
*        END ELSE
*            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
*        END
*    END
    RETURN
*-------------------------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ''
    R.LINE = ""

    ERR='' ; ER=''

    DIR.NAMEE  = "MSCC"

    OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
        TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
        RETURN
    END
    IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP
    LINE.NUMBER = 1
    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM BB THEN
            R.LINE = TRIM (R.LINE)
            ATM.ID  = TRIM(FIELD(R.LINE,"|",12))
            ATM.ID  = CHANGE(ATM.ID,"SUEZ ","")
            AMT     = TRIM(FIELD(R.LINE,"|",9)) / 100
            TYP     = TRIM(FIELD(R.LINE,"|",10))
            PAN     = TRIM(FIELD(R.LINE,"|",1))
            WS.DTR  = TRIM(FIELD(R.LINE,"|",18))
            WS.CTR  = TRIM(FIELD(R.LINE,"|",18))
            IF AMT GT 0 THEN
                IF PAN[1,1] EQ 4 THEN
                    GOSUB PROCESS
                END
            END

            TMP.ID = "MSCC*" : TIMEDATE(): "*" : OPERATOR : "*": LINE.NUMBER
            CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
            R.TMP<SCB.MSCC.FIELD.12> = ATM.ID
            R.TMP<SCB.MSCC.FIELD.9>  = AMT
            R.TMP<SCB.MSCC.FIELD.10> = TYP
            R.TMP<SCB.MSCC.FIELD.1>  = PAN
            R.TMP<SCB.MSCC.FIELD.18> = WS.DTR

            R.TMP<SCB.MSCC.FIELD.2> = TRIM(FIELD(R.LINE,"|",2))
            R.TMP<SCB.MSCC.FIELD.3> = TRIM(FIELD(R.LINE,"|",3))
            R.TMP<SCB.MSCC.FIELD.4> = TRIM(FIELD(R.LINE,"|",4))
            R.TMP<SCB.MSCC.FIELD.5> = TRIM(FIELD(R.LINE,"|",5))
            R.TMP<SCB.MSCC.FIELD.6> = TRIM(FIELD(R.LINE,"|",6))
            R.TMP<SCB.MSCC.FIELD.7> = TRIM(FIELD(R.LINE,"|",7))
            R.TMP<SCB.MSCC.FIELD.8> = TRIM(FIELD(R.LINE,"|",8))
            R.TMP<SCB.MSCC.FIELD.11> = TRIM(FIELD(R.LINE,"|",11))
            R.TMP<SCB.MSCC.FIELD.13> = TRIM(FIELD(R.LINE,"|",13))
            R.TMP<SCB.MSCC.FIELD.14> = TRIM(FIELD(R.LINE,"|",14))
            R.TMP<SCB.MSCC.FIELD.15> = TRIM(FIELD(R.LINE,"|",15))
            R.TMP<SCB.MSCC.FIELD.16> = TRIM(FIELD(R.LINE,"|",16))
            R.TMP<SCB.MSCC.FIELD.17> = TRIM(FIELD(R.LINE,"|",17))
            R.TMP<SCB.MSCC.FIELD.19> = TRIM(FIELD(R.LINE,"|",19))
            R.TMP<SCB.MSCC.FIELD.20> = TRIM(FIELD(R.LINE,"|",20))
            R.TMP<SCB.MSCC.FIELD.21> = TRIM(FIELD(R.LINE,"|",21))
            R.TMP<SCB.MSCC.FIELD.22> = TRIM(FIELD(R.LINE,"|",22))

            WRITE R.TMP TO F.TMP , TMP.ID  ON ERROR
                STOP 'CAN NOT WRITE RECORD ':TMP.ID:' TO FILE ':F.TMP
            END

        END ELSE
            EOF = 1
        END
        LINE.NUMBER ++
    REPEAT
    CLOSESEQ BB
    RETURN
*------------------------------------------------------------------
PROCESS:
*-------
    CALL F.READ(FN.ATM,ATM.ID,R.ATM,F.ATM,ER.ATM)
    IF NOT(ER.ATM) THEN
        ACCOUNT.NUMBER = R.ATM<SCB.ATM.ACCOUNT.NUMBER>
        ACCOUNT.FIX    = "EGP1123400010099"
        BRANCH.CODE    = "EG0010099"
        COMP.CODE      = BRANCH.CODE[8,2]
        OFS.USER.INFO  = "INPUTT" : COMP.CODE : "//" : BRANCH.CODE

        IF TYP[1,2] EQ 'DB' THEN
            DR.ACCOUNT = ACCOUNT.FIX
            CR.ACCOUNT = ACCOUNT.NUMBER
        END

        IF TYP[1,2] EQ 'CR' THEN
            DR.ACCOUNT = ACCOUNT.NUMBER
            CR.ACCOUNT = ACCOUNT.FIX
        END

        TRNS.TYPE = "AC70"
        CR.AMT    = AMT
        DR.CUR    = "EGP"
        CR.CUR    = "EGP"
        DTR       =  WS.DTR
        CTR       =  WS.CTR

        GOSUB BUILD.RECORD
    END
    RETURN
*------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :DTR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :CTR:COMMA

    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :CR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :CR.AMT:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK="       :"SCB":COMMA
    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA

    OFS.MESSAGE.DATA :=  "LOCAL.REF:32:1="      :ATM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:33:1="      :ATM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:43:1="      :PAN:COMMA

*    ZZZ   = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

*    WRITESEQ ZZZ TO V.FILE.IN ELSE
*        PRINT  'CAN NOT WRITE LINE ':ZZZ
*    END

    TMP.ID = "MSCC*" : TIMEDATE(): "*" : OPERATOR
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
*    R.TMP<SCB.MSCC.FIELD.12> = ATM.ID
*    R.TMP<SCB.MSCC.FIELD.9>  = AMT
*    R.TMP<SCB.MSCC.FIELD.10> = TYP
*    R.TMP<SCB.MSCC.FIELD.1>  = PAN
*    R.TMP<SCB.MSCC.FIELD.18> = WS.DTR

    R.TMP<SCB.MSCC.FIELD.12> = TRIM(FIELD(R.LINE,"|",12))
    R.TMP<SCB.MSCC.FIELD.9>  = TRIM(FIELD(R.LINE,"|",9))
    R.TMP<SCB.MSCC.FIELD.10> = TRIM(FIELD(R.LINE,"|",10))
    R.TMP<SCB.MSCC.FIELD.1>  = TRIM(FIELD(R.LINE,"|",1))
    R.TMP<SCB.MSCC.FIELD.18> = TRIM(FIELD(R.LINE,"|",18))

    R.TMP<SCB.MSCC.FIELD.2> = TRIM(FIELD(R.LINE,"|",2))
    R.TMP<SCB.MSCC.FIELD.3> = TRIM(FIELD(R.LINE,"|",3))
    R.TMP<SCB.MSCC.FIELD.4> = TRIM(FIELD(R.LINE,"|",4))
    R.TMP<SCB.MSCC.FIELD.5> = TRIM(FIELD(R.LINE,"|",5))
    R.TMP<SCB.MSCC.FIELD.6> = TRIM(FIELD(R.LINE,"|",6))
    R.TMP<SCB.MSCC.FIELD.7> = TRIM(FIELD(R.LINE,"|",7))
    R.TMP<SCB.MSCC.FIELD.8> = TRIM(FIELD(R.LINE,"|",8))
    R.TMP<SCB.MSCC.FIELD.11> = TRIM(FIELD(R.LINE,"|",11))
    R.TMP<SCB.MSCC.FIELD.13> = TRIM(FIELD(R.LINE,"|",13))
    R.TMP<SCB.MSCC.FIELD.14> = TRIM(FIELD(R.LINE,"|",14))
    R.TMP<SCB.MSCC.FIELD.15> = TRIM(FIELD(R.LINE,"|",15))
    R.TMP<SCB.MSCC.FIELD.16> = TRIM(FIELD(R.LINE,"|",16))
    R.TMP<SCB.MSCC.FIELD.17> = TRIM(FIELD(R.LINE,"|",17))
    R.TMP<SCB.MSCC.FIELD.19> = TRIM(FIELD(R.LINE,"|",19))
    R.TMP<SCB.MSCC.FIELD.20> = TRIM(FIELD(R.LINE,"|",20))
    R.TMP<SCB.MSCC.FIELD.21> = TRIM(FIELD(R.LINE,"|",21))
    R.TMP<SCB.MSCC.FIELD.22> = TRIM(FIELD(R.LINE,"|",22))

    WRITE R.TMP TO F.TMP , TMP.ID  ON ERROR
        STOP 'CAN NOT WRITE RECORD ':TMP.ID:' TO FILE ':F.TMP
    END
    RETURN
*------------------------------------------------------------------
END
