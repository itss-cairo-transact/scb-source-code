* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------*
*-------CREATED BY RIHAM YOUSSEF----------*
    PROGRAM POSSESS.ADD.R
*-----------------------------------------*

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.POSSESS


    F.POSS    = "F.SCB.POSSESS" ;  FVAR.POSS = '' ; R.POSS = ''
    CALL OPF(F.POSS,FVAR.POSS)

    T.DATE    = TODAY
    TT.DATE   = T.DATE[1,6]
    SERIAL.NO = 0
*-- EDIT BY NESSMA
    FVAR.POSS = ""
    CALL OPF(F.POSS,FVAR.POSS)
*    OPEN F.POSS TO FVAR.POSS ELSE
*       TEXT = "ERROR OPEN FILE" ; CALL REM
*      RETURN
* END
*-- END EDIT

*-----------------------------------------------------------------*

    Path.1 = "&SAVEDLISTS&/possess.txt"

    OPENSEQ Path.1 TO MyPath.1 ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    R.LINE = '' ; R.POSS = ''; T.SEL='';KEY.LIST=''; SELECTED=0
    UPDATE.FLAG = ''; ERR=''; ER=''; L.LIST=''; SELECT.NO=0; Y.SEL=''
    ZEROS=''; NO.OF.RECORDS=0; NO.OF.REC=0; Z=''



    EOF = ''
    I   = 1
    POSS.ID2 = ''


    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM MyPath.1 THEN
            IF R.LINE NE  '' THEN
                POSS.DATE  = TRIM(FIELD(R.LINE,",",1))
                R.POSS<POSS.POSSESS.DATE> = POSS.DATE[7,4]:POSS.DATE[4,2]:POSS.DATE[1,2]
                IF R.POSS<POSS.POSSESS.DATE> EQ '20201231' THEN
                    IF POSS.ID2 NE R.POSS<POSS.POSSESS.DATE> THEN
                        SERIAL.NO = 0
                        SERIAL.NO =  SERIAL.NO + 1
                        SER.NO    =  FMT(SERIAL.NO,"R%6")
                    END ELSE
                        SERIAL.NO =  SERIAL.NO + 1
                        SER.NO    =  FMT(SERIAL.NO,"R%6")

                    END

                    R.POSS<POSS.CUST.CODE>    = TRIM(FIELD(R.LINE,",",2))
                    CUST.CODE.1               = R.POSS<POSS.CUST.CODE>
                    R.POSS<POSS.CUST.NAME>    = TRIM(FIELD(R.LINE,",",3))[1,35]
                    R.POSS<POSS.PAPER.CODE>   = TRIM(FIELD(R.LINE,",",4))
                    R.POSS<POSS.PAPER>        = TRIM(FIELD(R.LINE,",",5))
                    CUR                       = TRIM(FIELD(R.LINE,",",6))
                    IF CUR EQ '����� ������' THEN

                        R.POSS<POSS.CURRENCY>  = 'USD'
                    END ELSE
                        R.POSS<POSS.CURRENCY>    = 'EGP'
                    END
                    R.POSS<POSS.ISSUE>           = TRIM(FIELD(R.LINE,",",7))
                    R.POSS<POSS.FOLLOW.CODE>     = TRIM(FIELD(R.LINE,",",8))
                    R.POSS<POSS.FOLLOW.NAME>     = TRIM(FIELD(R.LINE,",",9))
                    R.POSS<POSS.QUANTITY>        = TRIM(FIELD(R.LINE,",",10))
                    R.POSS<POSS.FINAL.COST>      = TRIM(FIELD(R.LINE,",",11))
                    R.POSS<POSS.VALUE>           = TRIM(FIELD(R.LINE,",",12))
                    R.POSS<POSS.AGAINST.POSSESS> = TRIM(FIELD(R.LINE,",",13))
                    R.POSS<POSS.ADDRESS>         = TRIM(FIELD(R.LINE,",",15))


                    POSS.ID   = R.POSS<POSS.POSSESS.DATE>:'.':SER.NO:'.':CUST.CODE.1

                    I = I + 1

                    POSS.ID2 = ''
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**       WRITE R.POSS TO FVAR.POSS , POSS.ID  ON ERROR
**         STOP 'CAN NOT WRITE RECORD ':POSS.ID:' TO FILE ':F.POSS
**   END
                    CALL F.WRITE(F.POSS,POSS.ID,R.POSS)

                    CALL JOURNAL.UPDATE(POSS.ID)
*************
                    POSS.ID2  = POSS.ID[1,8]

                END
            END
        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath.1
    RETURN

END
