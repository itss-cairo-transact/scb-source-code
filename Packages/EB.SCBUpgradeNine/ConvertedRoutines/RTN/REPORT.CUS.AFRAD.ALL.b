* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>45</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.CUS.AFRAD.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*---------------------------------------------
    COMPY  = ID.COMPANY
    COMPY1 = ID.COMPANY[2]
*------------------------------------------------------------------------
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIAL:
*-------
    REPORT.ID = "REPORT.CUS.AFRAD.ALL"
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    FN.LD='FBNK.CUSTOMER' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD = TODAY

    YY1 = 0
    YY2 = 0
    YY3 = 0

    CUS.1.18  = 0
    CUS.19.23 = 0
    CUS.24.40 = 0
    CUS.41.60 = 0
    CUS.61    = 0

    TOT.CUS.1.18  = 0
    TOT.CUS.19.23 = 0
    TOT.CUS.24.40 = 0
    TOT.CUS.41.60 = 0
    TOT.CUS.61    = 0
    TOT.SELECTED  = 0

    DAT           = TODAY

    DAT.1 = DAT[1,4] - 1
    DAT.1 = DAT.1 : DAT[5,4]

    DAT.18 = DAT[1,4] - 18
    DAT.18 = DAT.18 : DAT[5,4]

    DAT.19 = DAT[1,4] - 19
    DAT.19 = DAT.19 : DAT[5,4]

    DAT.23 = DAT[1,4] - 23
    DAT.23 = DAT.23 : DAT[5,4]

    DAT.24 = DAT[1,4] - 24
    DAT.24 = DAT.24 : DAT[5,4]

    DAT.40 = DAT[1,4] - 40
    DAT.40 = DAT.40 : DAT[5,4]

    DAT.41 = DAT[1,4] - 41
    DAT.41 = DAT.41 : DAT[5,4]

    DAT.60 = DAT[1,4] - 60
    DAT.60 = DAT.60 : DAT[5,4]
*----------------------------------------------------------------
    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    FOR I = 1 TO SELECTED3
        ZZ = KEY.LIST3<I>
*========================
        T.SEL  = "SELECT ":FN.LD:" WITH  POSTING.RESTRICT LT 90"
        T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
        T.SEL := " AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        T.SEL1  = "SELECT ":FN.LD:" WITH  POSTING.RESTRICT LT 90"
        T.SEL1 := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
        T.SEL1 := " AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL1 := " AND BIRTH.INCORP.DATE GE ": DAT.18
        T.SEL1 := " AND BIRTH.INCORP.DATE LE ": DAT.1
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",CUS.1.18,ER.MSG1)

        T.SEL1  = "SELECT ":FN.LD:" WITH  POSTING.RESTRICT LT 90"
        T.SEL1 := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
        T.SEL1 := " AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL1 := " AND BIRTH.INCORP.DATE GE ": DAT.23
        T.SEL1 := " AND BIRTH.INCORP.DATE LE ": DAT.19
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",CUS.19.23,ER.MSG1)

        T.SEL1  = "SELECT ":FN.LD:" WITH  POSTING.RESTRICT LT 90"
        T.SEL1 := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
        T.SEL1 := " AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL1 := " AND BIRTH.INCORP.DATE GE ": DAT.40
        T.SEL1 := " AND BIRTH.INCORP.DATE LE ": DAT.24
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",CUS.24.40,ER.MSG1)

        T.SEL1  = "SELECT ":FN.LD:" WITH  POSTING.RESTRICT LT 90"
        T.SEL1 := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
        T.SEL1 := " AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL1 := " AND BIRTH.INCORP.DATE GE ": DAT.60
        T.SEL1 := " AND BIRTH.INCORP.DATE LE ": DAT.41
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",CUS.41.60,ER.MSG1)

        T.SEL1  = "SELECT ":FN.LD:" WITH  POSTING.RESTRICT LT 90"
        T.SEL1 := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
        T.SEL1 := " AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL1 := " AND BIRTH.INCORP.DATE GT ": DAT.60
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",CUS.61,ER.MSG1)
*==========================
*Line [ 166 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ZZ,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,ZZ,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        YYBRN  = BRANCH
        XX     = SPACE(132)

        XX<1,1>[3,15]    = YYBRN
        XX<1,1>[37,15]   = SELECTED
        XX<1,1>[55,10]   = CUS.1.18
        XX<1,1>[75,10]   = CUS.19.23
        XX<1,1>[93,10]   = CUS.24.40
        XX<1,1>[107,10]  = CUS.41.60
        XX<1,1>[120,10]  = CUS.61
        PRINT XX<1,1>

        TOT.CUS.1.18  = TOT.CUS.1.18 + CUS.1.18
        TOT.CUS.19.23 = TOT.CUS.19.23 + CUS.19.23
        TOT.CUS.24.40 = TOT.CUS.24.40 + CUS.24.40
        TOT.CUS.41.60 = TOT.CUS.41.60 + CUS.41.60
        TOT.CUS.61    = TOT.CUS.61    + CUS.61
        TOT.SELECTED  = TOT.SELECTED  + SELECTED

        CUS.1.18  = 0
        CUS.19.23 = 0
        CUS.24.40 = 0
        CUS.41.60 = 0
        CUS.61    = 0
    NEXT I

    PRINT " "
    PRINT " "

    XX<1,1> = ""
    XX<1,1>[3,15]    = "�������� = "
    XX<1,1>[37,15]   = TOT.SELECTED
    XX<1,1>[55,10]   = TOT.CUS.1.18
    XX<1,1>[75,10]   = TOT.CUS.19.23
    XX<1,1>[93,10]   = TOT.CUS.24.40
    XX<1,1>[107,10]  = TOT.CUS.41.60
    XX<1,1>[120,10]  = TOT.CUS.61
    PRINT XX<1,1>

    PRINT " "
    PRINT SPACE(50) :"*********  ����� �������  *********"
    RETURN
*-------------------------------------------------------------------
PRINT.HEAD:
*----------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"REPORT.CUS.AFRAD.ALL"
    PR.HD :="'L'":SPACE(50):"���� ������� ����� ����� ���� ����� �����"
    PR.HD :="'L'":SPACE(45):"----------------------------------------------------"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(7):"�����":SPACE(25):"�����":SPACE(5)
    PR.HD :="�� ��� ��� 18 ���"
    PR.HD :=SPACE(9):"19 - 23"
    PR.HD :=SPACE(9):"24 - 40"
    PR.HD :=SPACE(9):"41 - 60"
    PR.HD :=SPACE(9):"���� �� 60"
    PR.HD :="'L'":STR('-',110)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    RETURN
END
