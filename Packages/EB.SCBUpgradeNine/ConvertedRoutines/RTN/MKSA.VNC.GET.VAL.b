* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>278</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MKSA.VNC.GET.VAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.FCY.REF

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN


*    R.NEW(SCB.BR.OUR.REFERENCE) = ''
*    R.NEW(SCB.BR.AMOUNT)        = ''
*    R.NEW(SCB.BR.CURR)          = ''
*    R.NEW(SCB.BR.BR.CODE)       = ''
*    R.NEW(SCB.BR.BANK)          = ''

*    ID.NEW = ID.NEW:'.OUT'

TEXT = "DONT FORGET TO PUT THE ** .OUT **"

    FN.REF  = 'F.SCB.BR.FCY.REF' ; F.REF = '' ; R.REF = ''
    CALL OPF(FN.REF,F.REF)

    I = 1
    H = 1
    T.SEL  = "SELECT F.SCB.BR.FCY.REF WITH VALUE.DATE EQ ": ID.NEW[1,8] :" BY BR.BRANCH "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    TEXT = SELECTED ; CALL REM
    IF SELECTED THEN
        ENT.NO = ''
        LOOP
            REMOVE BR.ID  FROM KEY.LIST SETTING POS
        WHILE BR.ID:POS
            CALL F.READ(FN.REF,BR.ID,R.REF,F.REF,ERR)

**------------------------------------------------------------------**

            OUR.ID            = BR.ID
            AMT               = R.REF<BR.REF.AMT>
            DEPT.CODE         = R.REF<BR.REF.BR.BRANCH>
            CURR              = R.REF<BR.REF.CUR>
            OLD.NO            = R.REF<BR.REF.OLD.NO>
            IF (  DEPT.CODE NE OLD.DEPT AND I GT 1 )  THEN
                H = H +1
                I = 1
            END

            R.NEW(SCB.BR.OUR.REFERENCE)<1,H,I> = OUR.ID
            R.NEW(SCB.BR.AMOUNT)<1,H,I>        = AMT
            R.NEW(SCB.BR.CURR)<1,H,I>          = CURR
            R.NEW(SCB.BR.VALUE.DATE)           = ID.NEW[1,8]
            R.NEW(SCB.BR.OLD.NO)<1,H,I>        = OLD.NO
*    IF DEPT.CODE[3,1] EQ '0' THEN
*        DEP = DEPT.CODE[4,1]
*    END ELSE
*        DEP = DEPT.CODE[3,2]
*    END
*           R.NEW(SCB.BR.BR.CODE)<1,H>       = DEP
            R.NEW(SCB.BR.BR.CODE)<1,H>       = DEPT.CODE

*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,OUR.ID,MY.LOC)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,OUR.ID,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
MY.LOC=R.ITSS.BILL.REGISTER<EB.BILL.REG.LOCAL.REF>

            R.NEW(SCB.BR.BANK)<1,H,I>        = MY.LOC<1,BRLR.BANK>
            I = I + 1

            OLD.DEPT =  DEPT.CODE


        REPEAT

    END ELSE

        RETURN
    END

    RETURN

**------------------------------------------------------------------**

END
