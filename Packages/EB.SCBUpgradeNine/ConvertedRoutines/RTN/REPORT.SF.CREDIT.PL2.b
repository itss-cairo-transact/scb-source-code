* @ValidationCode : MjotMTM1NDQwMjc5NTpDcDEyNTI6MTY0NDk0MTE2MTczMTp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 18:06:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>254</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE REPORT.SF.CREDIT.PL2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.WH.TRANS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.SF.TRANS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.SF.CREDIT.PL2'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*========================================================================
PROCESS:
*---------------------
    CUS           = R.NEW(SCB.SF.TR.COMMISSION.TYPE2)
*Line [ 71 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,CUS,AC.CAT)
    F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
    FN.F.ITSS.FT.COMMISSION.TYPE = ''
    CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
    CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,CUS,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
    AC.CAT=R.ITSS.FT.COMMISSION.TYPE<FT4.CATEGORY.ACCOUNT>
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.DESCRIPTION,CUS,BYAN)
    F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
    FN.F.ITSS.FT.COMMISSION.TYPE = ''
    CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
    CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,CUS,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
    BYAN=R.ITSS.FT.COMMISSION.TYPE<FT4.DESCRIPTION>
    IF LEN(AC.CAT) EQ 5 THEN
*Line [ 75 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021

*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,AC.CAT,ACNAME)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,AC.CAT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        ACNAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        CUST.NAME     = ACNAME
        CUST.ADDRESS  = ACNAME
        ACC.DB        = AC.CAT
*Line [ 81 ] change CATEGORY':@FM:EB.CAT.DESCRIPTION to CATEGORY':@FM:EB.CAT.DESCRIPTION) - ITSS - R21 Upgrade - 28/12/2021
        
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,ACC.DB,CATEG.NAME)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,ACC.DB,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

        AMOUNT    = R.NEW(SCB.SF.TR.COMMISSION.AMT2)
        IN.AMOUNT = AMOUNT
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

        V.DATE = R.NEW(SCB.SF.TR.VALUE.DATE)
*Line [ 89 ] change CURRENCY':@FM:EB.CUR.CCY.NAME to CURRENCY':@FM:EB.CUR.CCY.NAME) - ITSS - R21 Upgrade - 28/12/2021
*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CURR.ID,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CURR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

        MAT.DATE  = V.DATE[7,2]:'/':V.DATE[5,2]:"/":V.DATE[1,4]

        INPUTTER = R.NEW(SCB.SF.TR.INPUTTER)
        INP      = FIELD(INPUTTER,'_',2)

*        AUTH = R.USER<EB.USE.SIGN.ON.NAME>
        AUTH  = ""
        AUTHI = ""
*CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI

        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
        XX8  = SPACE(132)  ; XX9  = SPACE(132)

        XX<1,1>[3,35]   = CUST.NAME
        XX2<1,1>[3,35]  = CUST.ADDRESS
        XX<1,1>[45,15]  = '������     : '
        XX<1,1>[59,15]  = AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = ACC.DB

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG.NAME

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR

        XX4<1,1>[45,15] = '����� ���� : '
        XX4<1,1>[59,15] = MAT.DATE

        XX6<1,1>[1,15]  = '������'
        XX7<1,1>[1,15]  = AUTHI

        XX6<1,1>[30,15] = '��� �������'
        XX7<1,1>[35,15] = INP

        XX6<1,1>[60,15] = '������'
        XX7<1,1>[60,15] = ID.NEW

        XX8<1,1>[3,35]  = '������ ������� : '
        XX8<1,1>[20,15] = OUT.AMT

        XX9<1,1>[3,15]  = '������         : '
        XX9<1,1>[20,15] = BYAN

*-------------------------------------------------------------------
*Line [ 172 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
        F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
        FN.F.ITSS.DEPT.ACCT.OFFICER = ''
        CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
        CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
        BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
        YYBRN  = FIELD(BRANCH,'.',2)
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
        PR.HD  ="'L'":SPACE(1):"��� ���� ������"
        PR.HD :="'L'":"������� : ":T.DAY
        PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� �����"
        PR.HD :="'L'":" "
        PR.HD :="'L'":"SAFEKEEP1"
        PR.HD :="'L'":" "
        PRINT
        HEADING PR.HD
*------------------------------------------------------------------
        PRINT XX<1,1>
        PRINT XX1<1,1>
        PRINT XX2<1,1>
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT XX8<1,1>
        PRINT XX9<1,1>
        PRINT XX5<1,1>
        PRINT STR(' ',82)
        PRINT XX6<1,1>
        PRINT STR('-',82)
        PRINT XX7<1,1>
    END
*===============================================================
RETURN
END
