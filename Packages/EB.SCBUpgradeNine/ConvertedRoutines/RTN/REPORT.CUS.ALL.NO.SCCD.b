* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-- COPIED FROM REPORT.CUS.ALL.3
    SUBROUTINE REPORT.CUS.ALL.NO.SCCD

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*---------
    REPORT.ID='REPORT.CUS.ALL.3'
    CALL PRINTER.ON(REPORT.ID,'')
    COMPY  = ID.COMPANY
    COMPY1 = ID.COMPANY[2]
    RETURN
*========================================================================
PROCESS:
*------
    FN.LD='FBNK.CUSTOMER' ; F.LD = ''
    FN.LD.HIS='FBNK.CUSTOMER$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*Line [ 69 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD = TODAY

    YY1 = 0
    YY2 = 0
    YY3 = 0

    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    FOR I = 1 TO SELECTED3
        ZZ      = KEY.LIST3<I>
        T.SEL   = "SELECT ":FN.LD: " WITH  POSTING.RESTRICT LT 90 AND (SECTOR NE 5010 AND SECTOR NE 5020) AND SECTOR EQ 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL  := " AND SCCD.CUSTOMER NE 'YES'"

        T.SEL1  = "SELECT ":FN.LD: " WITH  POSTING.RESTRICT LT 90 AND (SECTOR NE 5010 AND SECTOR NE 5020) AND SECTOR NE 2000 AND COMPANY.BOOK EQ ":ZZ
        T.SEL1 := " AND SCCD.CUSTOMER NE 'YES'"

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

        CALL F.READ(FN.LD.HIS,KEY.LIST1<I>,R.LD.HIS,F.LD.HIS,E2)
        CALL F.READ(FN.LD.HIS,KEY.LIST1<I+1>,R.LD.HIS,F.LD.HIS,E2)

        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
        XX12 = SPACE(132)  ; XX13 = SPACE(132) ; XXX4 = SPACE(132)

        YY   = SELECTED + SELECTED1
        YY1 += SELECTED
        YY2 += SELECTED1
        YY3 += YY

*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ZZ,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,ZZ,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        YYBRN  = BRANCH
        XX<1,1>[3,15]    =  YYBRN
        XX<1,1>[45,15]   =  SELECTED
        XX<1,1>[60,15]   =  SELECTED1
        XX<1,1>[70,15]   =  YY
        XXX4<1,1>[1,50]   = '--------------------------'
        PRINT XX<1,1>
        PRINT XXX4<1,1>
    NEXT I

    XX1<1,1>[3,15]  = "���������� :"
    XX1<1,1>[45,15] = YY1
    XX1<1,1>[60,15] = YY2
    XX1<1,1>[70,15] = YY3
    PRINT XX1
    RETURN
*-------------------------------------------------------------------
PRINT.HEAD:
*----------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY :SPACE(80): "REPORT.CUS.ALL.NO.SCCD"
    PR.HD :="'L'":SPACE(20):"���� ������� ������� �������� �� ������"
    PR.HD :="'L'":SPACE(20):"- ���� ����� ������ ���� ������"
    PR.HD :="'L'":SPACE(5):"�����":SPACE(32):"�����":SPACE(9):"�����":SPACE(9):"������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*===============================================================
    RETURN
END
