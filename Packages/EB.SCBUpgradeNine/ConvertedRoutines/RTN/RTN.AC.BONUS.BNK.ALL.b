* @ValidationCode : MjotMTkxMDc5MTk3MzpDcDEyNTI6MTY0MDY4ODA3NzQ1Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 12:41:17
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* Create By Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE RTN.AC.BONUS.BNK.ALL
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
    *Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
    *Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.OFS.SOURCE
    *Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COLLATERAL
    *Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_USER.ENV.COMMON
    *Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
    *Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
    *Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.NUMERIC.CURRENCY
    
    $INSERT        I_F.SCB.AC.BONUS.BNK
*--------------------------------------------
    GOSUB INITIALISE
    GOSUB PROCESS
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
    TEXT =  "DONE" ; CALL REM
    RETURN
*--------------------------------------------
INITIALISE:
*----------
    FLG = 0
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS   = "MALEYA2"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "BONUS.OFS.":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END

    TOD.DAT = TODAY
*-----
    RETURN
*----------------------------------------------------
PROCESS:
*-------
    FN.TMP = "F.SCB.AC.BONUS.BNK"    ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    FN.ACCT = "FBNK.ACCOUNT"         ; F.ACCT = ""
    CALL OPF(FN.ACCT,F.ACCT)
*---------------------------------------------------
    N.SEL  = "SELECT ":FN.TMP:" WITH AMOUNT NE ''"
    N.SEL := " BY @ID"
    CALL EB.READLIST(N.SEL,KEY.LIST,'',SELECTED,ERR)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            TMP.ID = KEY.LIST<II>
            CALL F.READ(FN.TMP,TMP.ID, R.TMP, F.TMP, ETEXT)
            COMP          = "EG0010099"
            COMP.CODE     = "99"
            OFS.USER.INFO = "INPUTT" : COMP.CODE : "//" :COMP

            FLD.1  = R.TMP<BONUS.EQUIVALENT.NUMBER>
            FLD.2  = R.TMP<BONUS.PL.CATEGORY>
            AMOUNT = R.TMP<BONUS.AMOUNT>

            IF FLD.1 EQ '' AND FLD.2 EQ '' THEN
            END ELSE
                L.RUN.DAT = R.TMP<BONUS.LAST.RUN.DATE>
*    IF L.RUN.DAT NE '' THEN
                GOSUB CHECK.RUN.DAT
*    END
                IF FLG EQ 0 THEN
                    AMOUNT = R.TMP<BONUS.AMOUNT>
                    IF AMOUNT NE 0 THEN
                        IF AMOUNT GE '0' THEN
***CR.ACC = R.TMP<BONUS.ACCOUNT.NUMBER>
                            CR.ACC        = TMP.ID
                            DR.ACC        = "PL":R.TMP<BONUS.PL.CATEGORY>
                            PROF.CENT.DPT = "99"
                            CURR          = CR.ACC[1,3]
                        END ELSE
***DR.ACC = R.TMP<BONUS.ACCOUNT.NUMBER>
                            DR.ACC        = TMP.ID
                            CR.ACC        = "PL":R.TMP<BONUS.PL.CATEGORY>
                            PROF.CENT.DPT = "99"
                            CURR          = DR.ACC[1,3]
                        END
                        TRNS.TYPE = "ACBB"
                        GOSUB BUILD.RECORD
                    END
                END
            END
        NEXT II
    END
    RETURN
*----------------------------------------------------
CHECK.RUN.DAT:
*-------------
    FLG = 0
    IF TOD.DAT[1,6] EQ L.RUN.DAT[1,6] THEN
        FLG = 1
    END
    RETURN
*----------------------------------------------------
BUILD.RECORD:
*------------
    COMMA     = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :CURR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :KEY.LIST<II>:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK="       :"SCB":COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.CUST="       :"SCB":COMMA

    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :KEY.LIST<II>:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :CURR:COMMA

    AMOUNT = DROUND(AMOUNT,'2')

    IF AMOUNT GE 0 THEN
        OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :AMOUNT:COMMA
    END ELSE
        AMOUNT = AMOUNT * -1
        OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT="        :AMOUNT:COMMA
    END

    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY
*---------------------------------
    IF PROF.CENT.DPT NE '' THEN
        OFS.MESSAGE.DATA := COMMA:"PROFIT.CENTRE.DEPT=": PROF.CENT.DPT
        PROF.CENT.DPT  = ""
    END
*---------------------------------
    ZZZ   = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    WRITESEQ ZZZ TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZZ
    END
***** SCB R15 UPG 20160628 - S
*    OFS.REC = ZZZ
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*    OFS.REC = ''
***** SCB R15 UPG 20160628 - E

    L.RUN.DAT = ""
    RETURN
*-------------------------------------------------------
END
