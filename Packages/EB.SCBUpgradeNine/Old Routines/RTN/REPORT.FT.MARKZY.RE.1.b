* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
****************************************NI7OOOOOOOOOOOOO**********************
    SUBROUTINE REPORT.FT.MARKZY.RE.1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

*-------------------------------------------------------------------------
   * IF R.NEW(FT.DEBIT.CURRENCY) NE R.NEW(FT.CREDIT.CURRENCY) THEN
       * IF R.NEW(FT.DEBIT.CURRENCY) EQ 'EGP' OR R.NEW(FT.CREDIT.CURRENCY) EQ 'EGP' THEN

            GOSUB INITIATE
            GOSUB PRINT.HEAD
*Line [ 48 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
            GOSUB CALLDB

            CALL PRINTER.OFF
            CALL PRINTER.CLOSE(REPORT.ID,0,'')
       * END
   * END

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='REPORT.FT.MARKZY.RE.1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.FT='F.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
   *** CALL F.READ(FN.FT,COMI,R.FT,F.FT,E1)

* FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
* CALL OPF(FN.BR,F.BR)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    *ID = ID.NEW
    DATE.TO = TODAY[3,6]:"..."
*------------------------------------------------------------------------
*    T.SEL="SELECT F.SCB.BT.BATCH WITH DATE.TIME LIKE " :DATE.TO
*    T.SEL="SELECT F.SCB.BT.BATCH "
*    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    IF SELECTED THEN
*        FOR I = 1 TO SELECTED
*            CALL F.READ(FN.BATCH,KEY.LIST<I>,R.BATCH,F.BATCH,E2)
* FT.ID = R.NEW(SCB.BT.OUR.REFERENCE)
*            BT.ID = KEY.LIST<I>
*------------------------------------------------------------------------
*    IF R.NEW(FT.DEBIT.CURRENCY) NE R.NEW(FT.CREDIT.CURRENCY) THEN
*       IF R.NEW(FT.DEBIT.CURRENCY) EQ 'EGP' OR R.NEW(FT.CREDIT.CURRENCY) EQ 'EGP' THEN
****TEXT = COMI ; CALL REM

    CALL F.READ(FN.FT,COMI,R.FT,F.FT,E1)

    AMOUNT   = R.FT<FT.AMOUNT.CREDITED>
*****TEXT = AMOUNT ; CALL REM
    MMM      = LEN(AMOUNT) - 3
    AMOUNT1  = AMOUNT[4,MMM]
    CUR.ID   = R.FT<FT.CREDIT.CURRENCY>
    DAT     = R.FT<FT.CREDIT.VALUE.DATE>


* CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
    CUST.NAME    = "������ �������"

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    IN.AMOUNT = AMOUNT1
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT=OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
* DAT  = R.BR<EB.BILL.REG.MATURITY.DATE>
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    INPUTTER = R.FT<FT.INPUTTER>
    AUTH = R.FT<FT.AUTHORISER>

    BYAN = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
    RATE = R.FT<FT.CUSTOMER.RATE>
    INP = FIELD(INPUTTER,'_',2)
    AUTHI =FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX10 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX13 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)

    XX<1,1>[3,15]   = CUST.NAME
* XX1<1,1>[3,15]  = CUST.ADDRESS

    XX<1,1>[50,15]  = '������ : '
    XX<1,1>[60,15]  = AMOUNT1

* XX1<1,1>[50,15] = '��� ������ : '
* XX1<1,1>[64,15] = ACC.NO

* XX2<1,1>[50,15] = '��� ������ : '
* XX2<1,1>[64,15] = CATEG

* XX2<1,1>[3,15]  = '����� ��� ��� :' :BANK.NAME
* XX3<1,1>[3,15]  = '��� ':BRANCH.NAME

    XX3<1,1>[50,15] = '������ : '
    XX3<1,1>[60,15] = CUR

    XX4<1,1>[50,15] = '����� ���� : '
    XX4<1,1>[64,15] = MAT.DATE

*XX5<1,1>[3,15]  = '��� ����� : '
*XX5<1,1>[15,15] = CHQ.NO:' ':CHEQ.NAME

    XX6<1,1>[3,15]  = '������'
    XX7<1,1>[3,15]  = COMI

    XX6<1,1>[30,15] = '��� �������'
    XX7<1,1>[35,15] = INP

    XX6<1,1>[60,15] = '������'
    XX7<1,1>[60,15] = AUTHI

    XX10<1,1>[3,15] = '������ �������:'
    XX10<1,1>[20,15] = OUT.AMT

    XX11<1,1>[3,15] = '������:'
    XX11<1,1>[20,15] = BYAN
    XX13<1,1>[3,15] = '����� : ' : RATE


    PRINT XX<1,1>
    PRINT XX1<1,1>
* PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT XX13<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>

    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
*  NEXT I
*END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"����� : ":YYBRN  :SPACE(30):"������ ���"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*    END
*  END
    RETURN
END
