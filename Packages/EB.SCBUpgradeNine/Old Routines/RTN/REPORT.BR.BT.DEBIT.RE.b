* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOO******************
    SUBROUTINE REPORT.BR.BT.DEBIT.RE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �����" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.BR.BT.DEBIT.RE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

*BT.ID = ID.NEW


    BT.ID = COMI
    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)


    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
****TEXT = "COMI = :" :COMI ; CALL REM
*------------------------------------------------------------------------
*IF R.BATCH<SCB.BT.RETURN.REASON> EQ '' THEN
    BR.ID = R.BATCH<SCB.BT.OUR.REFERENCE>
    BR.INPUTT = R.BATCH<SCB.BT.INPUTTER>
    BR.AUTH   = R.BATCH<SCB.BT.AUTHORISER>
    INP = FIELD(BR.INPUTT,'_',2)
    AUTHI =FIELD(BR.AUTH,'_',2)

*Line [ 93 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
*****TEXT = "DD :": DD ; CALL REM
    FOR I = 1 TO DD
        BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
****TEXT = "BR.ID1 = :" : BR.ID1 ; CALL REM
*------------------------------------------------------------------------
        CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
****DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>
        ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,DRAWER.ID)
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
        CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
****        ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
        CHQ.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
        BANK         = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
        BANK.BRAN    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
        CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK,BNAME)
        CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BANK.BRAN,BRNAME)
******TEXT = "ACC.NO = " : ACC.NO ; CALL REM
*        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        BRN.ID  = AC.COMP[2]
        BRANCH.ID = TRIM(BRN.ID,"0","L")

        CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

        CATEG.ID  = ACC.NO[11,4]
        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

        AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
        IN.AMOUNT = AMOUNT

        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

        CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>

        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

        DAT  = TODAY
        MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

        INPUTTER = R.BR<EB.BILL.REG.INPUTTER>
        AUTH = R.BR<EB.BILL.REG.AUTHORISER>
        INP = FIELD(INPUTTER,'_',2)
        AUTHI =FIELD(AUTH,'_',2)

        XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX11  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
        XX8  = SPACE(132)  ; XX9  = SPACE(132)

        XX<1,1>[3,35]   = CUST.NAME:' ':CUST.NAME.2
        XX1<1,1>[3,35]   = CUST.ADDRESS

        XX<1,1>[45,15]  = '������     : '
        XX<1,1>[59,15]  = AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = ACC.NO

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR

        XX4<1,1>[45,15] = '����� ���� : '
        XX4<1,1>[59,15] = MAT.DATE

        XX5<1,1>[3,15]  = '��� �����      : '
        XX5<1,1>[20,15] = CHQ.NO

        XX5<1,1>[45,15]  = '��� �������    : '
        XX5<1,1>[59,15] = BR.ID1

        XX11<1,1>[3,15]  = '����� ������� ����: '
        XX11<1,1>[30,15] = BNAME

        XX11<1,1>[50,15]  = '��� ����� ������� ����: '
        XX11<1,1>[65,15] = BRNAME


        XX6<1,1>[1,15]  = '������'
****   XX7<1,1>[1,15] = AUTHI
        XX7<1,1>[1,15] = BR.AUTH

        XX6<1,1>[30,15]  = '��� �������'
        XX7<1,1>[35,15] = COMI

        XX6<1,1>[60,15]  = '������'
****        XX7<1,1>[60,15] = INP
        XX7<1,1>[60,15] = BR.INPUTT

        XX8<1,1>[3,35]  = '������ ������� : '
        XX8<1,1>[20,15] = OUT.AMT

        XX9<1,1>[3,15]  = '������         : '
        XX9<1,1>[20,15] = '����� ��� ���� �����'

*-------------------------------------------------------------------
        YYBRN  = FIELD(BRANCH,'.',2)
        DATY   = TODAY
        T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
        PR.HD  ="'L'":SPACE(1):"��� ���� ������"
        PR.HD :="'L'":"������� : ":T.DAY
        PR.HD :="'L'":"����� : ":YYBRN
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(50) : "����� ��� "
        PR.HD :="'L'":" "
        PRINT
        HEADING PR.HD
*------------------------------------------------------------------
        PRINT XX<1,1>
        PRINT XX1<1,1>
        PRINT XX2<1,1>
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT XX11<1,1>
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT XX8<1,1>
        PRINT XX9<1,1>
        PRINT XX5<1,1>
        PRINT STR(' ',82)
        PRINT XX6<1,1>
        PRINT STR('-',82)
        PRINT XX7<1,1>
    NEXT I
* END
*===============================================================
    RETURN
END
