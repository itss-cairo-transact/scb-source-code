* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************************NI7OOOOOOOOOOO****************
    SUBROUTINE REPORT.FT.LE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*-------------------------------------------------------------------------
    IF R.NEW(FT.COMMISSION.AMT)[1,3] EQ 'EGP' OR R.NEW(FT.CHARGE.AMT)[1,3] EQ 'EGP' THEN

        GOSUB INITIATE
        GOSUB PRINT.HEAD
*Line [ 56 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
        GOSUB CALLDB

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='REPORT.FT.LE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.FT='F.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)

* FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
* CALL OPF(FN.BR,F.BR)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    ID = ID.NEW
    DATE.TO = TODAY[3,6]:"..."
*------------------------------------------------------------------------
*    T.SEL="SELECT F.SCB.BT.BATCH WITH DATE.TIME LIKE " :DATE.TO
*    T.SEL="SELECT F.SCB.BT.BATCH "
*    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    IF SELECTED THEN
*        FOR I = 1 TO SELECTED
*            CALL F.READ(FN.BATCH,KEY.LIST<I>,R.BATCH,F.BATCH,E2)
* FT.ID = R.NEW(SCB.BT.OUR.REFERENCE)
*            BT.ID = KEY.LIST<I>
*------------------------------------------------------------------------
    CALL F.READ(FN.FT,ID,R.FT,F.FT,E1)
    ACC.NO      = R.NEW(FT.CHARGES.ACCT.NO)
    AMOUNT      = R.NEW(FT.TOTAL.CHARGE.AMOUNT)
    MMM         = LEN(AMOUNT) - 3
    AMOUNT1     = AMOUNT[4,MMM]
    CUR.ID      = R.NEW(FT.DEBIT.CURRENCY)
    DAT         = R.NEW(FT.DEBIT.VALUE.DATE)
    COM.CODE    = R.NEW(FT.COMMISSION.CODE)
    CHARGE.CODE = R.NEW(FT.CHARGE.CODE)

**************ADDED BY MAHMOUD 6/12/2009*****************
    CHARGE.CUR = AMOUNT[1,3]
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CUR,CH.CUR)
*********************************************************

****    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
    BRANCH.ID  = AC.COMP[8,2]

    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUSNO)
    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
    CUST.NAME         = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME.2       = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS      = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
*    CUST.ADDRESS.2    = LOCAL.REF<1,CULR.ARABIC.ADDRESS.2>
    CUST.GOV          = LOCAL.REF<1,CULR.GOVERNORATE>
    CUST.REG          = LOCAL.REF<1,CULR.REGION>
    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.GOV,CUST.ADD2)
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.REG,CUST.ADD1)
    CALL DBR ('FT.COMMISSION.TYPE':@FM:FT4.DESCRIPTION<2,2>,COM.CODE,COM.NAME)
    CALL DBR ('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION<2,2>,CHARGE.CODE,CHARGE.NAME)
    IF CUST.GOV = 98 THEN
        CUST.ADD2 = ''
    END
    IF CUST.REG = 998 THEN
        CUST.ADD1 = ''
    END
    IF CUST.GOV = 999 THEN
        CUST.ADD2 = ''
    END
    IF CUST.REG = 999 THEN
        CUST.ADD1 = ''
    END
    CATEG.ID  = ACC.NO[11,4]
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    IN.AMOUNT = AMOUNT1
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

**********UPDATYED BY MAHMOUD 6/12/2009*************************
**    OUT.AMT=OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    OUT.AMT=OUT.AMOUNT : ' ' : CH.CUR : ' ' : '�����'
***************************************************************

* DAT  = R.BR<EB.BILL.REG.MATURITY.DATE>
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    INPUTTER  = R.NEW(FT.INPUTTER)
    AUTH      = R.NEW(FT.AUTHORISER)

    BYAN = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.CREDIT>

    INP   = FIELD(INPUTTER,'_',2)
    AUTHI = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX10 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)

    XX<1,1>[3,15]   = CUST.NAME:' ':CUST.NAME.2
    XX1<1,1>[3,15]  = CUST.ADDRESS
    XX2<1,1>[3,15]  = CUST.ADD1:' ':CUST.ADD2

    XX<1,1>[55,15]  = '������ : '
    XX<1,1>[65,15]  = AMOUNT1

    XX1<1,1>[55,15] = '��� ������ : '
    XX1<1,1>[65,15] = ACC.NO

    XX2<1,1>[55,15] = '��� ������ : '
    XX2<1,1>[65,15] = CATEG

    XX3<1,1>[55,15] = '������ : '
*********UPDATED BY MAHMOUD 6/12/2009*****************
**    XX3<1,1>[65,15] = CUR
    XX3<1,1>[65,15] = CH.CUR
******************************************************

    XX4<1,1>[55,15] = '����� ���� : '
    XX4<1,1>[65,15] = MAT.DATE

*XX5<1,1>[3,15]  = '��� ����� : '
*XX5<1,1>[15,15] = CHQ.NO:' ':CHEQ.NAME

    XX6<1,1>[3,15]  = '������'
    XX7<1,1>[3,15]  = ID.NEW

    XX6<1,1>[30,15] = '��� �������'
    XX7<1,1>[35,15] = INP

    XX6<1,1>[60,15] = '������'
    XX7<1,1>[60,15] = AUTHI

    XX10<1,1>[3,15] = '������ �������:'
    XX10<1,1>[20,15] = OUT.AMT

    XX11<1,1>[3,15] = '������:'
    XX11<1,1>[20,15] = BYAN


    PRINT XX<1,1>
    PRINT XX1<1,1>
* PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>

    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT STR(' ',82)
*  NEXT I
*END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
***    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
***    YYBRN  = FIELD(BRANCH,'.',2)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ID.COMPANY,YYBRN)
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"����� : ":YYBRN  :SPACE(30):"������ ���"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*    END
*  END
    RETURN
END
