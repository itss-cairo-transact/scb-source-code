* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NESSMA**********************
    SUBROUTINE REPORT.TT.DSF.RE
**5SM
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ����� �����" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    CUR.KHASM = ''
    SCB.CO    = ID.COMPANY
    REPORT.ID = 'REPORT.TT.DSF'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.TT = 'FBNK.TELLER' ; F.TT=''
    CALL OPF(FN.TT,F.TT)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT=''
    CALL OPF(FN.ACCT,F.ACCT)

    FN.CUS  = 'FBNK.CUSTOMER' ; F.CUS=''
    CALL OPF(FN.CUS,F.CUS)
*==========
    YTEXT = "Enter the TT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.TT,COMI,R.TT,F.TT,E1)
*==========
    ACC.NAME      = ''
    OUT.AMOUNT    = ''
    ACCT.NO.KHASM = R.TT<TT.TE.ACCOUNT.1>

    CALL F.READ(FN.ACCT,ACCT.NO.KHASM,R.ACCT,F.ACCT,ER.ACCT)
    CUS.NO        = R.ACCT<AC.CUSTOMER>
    ACCT.NAME22   = R.ACCT<AC.ACCOUNT.TITLE.1>
    TEXT = "NAME  = " : ACCT.NAME22  ; CALL REM


    CATEG.NO      = ACCT.NO.KHASM[4,5]
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.NO,CATEG)

    IF CATEG.NO EQ 10000 THEN ACCT.NAME22 = CATEG

    CURR.KHASM    = R.TT<TT.TE.CURRENCY.1>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CURR.KHASM,CUR.1)

    IF CURR.KHASM EQ "EGP" THEN
        AMT.KHASM     = R.TT<TT.TE.AMOUNT.LOCAL.1>
    END ELSE
        AMT.KHASM     = R.TT<TT.TE.AMOUNT.FCY.1>
    END

    IN.AMOUNT     = AMT.KHASM
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT       = OUT.AMOUNT : ' ' : CUR.1 : ' ' : '�����'

    DATE.FT       = R.TT<TT.TE.VALUE.DATE.1>

    YY2 = R.TT<TT.TE.LOCAL.REF><1,TTLR.BRANCH.NO>
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,YY2,BRANCH2)
    YYBRN2  = FIELD(BRANCH2,'.',2)

    NOTES.DESC    = "��� ����� �� ����� ����� ��� ���": " " : YYBRN2

    INPUTTER      = R.TT<TT.TE.INPUTTER>
    AUTH          = R.TT<TT.TE.AUTHORISER>
    INP           = FIELD(INPUTTER,'_',2)
    AUTHI         = FIELD(AUTH,'_',2)
*============
    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
*============
    XX5<1,1>[1,40]  = ACCT.NAME22
    XX<1,1>[60,15]  = '������     : '
    XX<1,1>[74,15]  =  IN.AMOUNT
    XX1<1,1>[60,15] = '��� ������ : '
    XX1<1,1>[74,15] = ACCT.NO.KHASM
    XX2<1,1>[60,15] = '��� ������ : '
    XX2<1,1>[74,15] = CATEG
    XX3<1,1>[60,15] = '������     : '
    XX3<1,1>[74,15] =  CUR.1
    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = DATE.FT
    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15]  = AUTHI
    XX6<1,1>[30,15] = '��� �������'
    XX7<1,1>[35,15] = COMI
    XX6<1,1>[60,15] = '������'
    XX7<1,1>[60,15] = INP
    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = NOTES.DESC
*-------------------------------------------------------------------
*YY = R.TT<TT.TE.LOCAL.REF><1,TTLR.BRANCH.NO>
*CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,YY,BRANCH)
*YYBRN  = FIELD(BRANCH,'.',2)

    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
    YYBRN  = BRANCH
    TEXT   = "BRN ":YYBRN   ; CALL REM

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" SCB.CREDIT1"
    PR.HD :="'L'":"����� ���"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX5<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
