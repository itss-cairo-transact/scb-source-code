* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
********************************************NI7OOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>1122</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.STMT.ENTRY.1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.STMT.ENTRY.1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    YTEXT = "Enter DATE : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    NDAT  = COMI

    YTEXT = "Enter DATE : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    VDAT  = COMI

    FN.LD = "FBNK.STMT.ENTRY"
    F.LD  = ""
    CALL OPF(FN.LD,F.LD)

    YF.CATEG.MONTH = "F.CATEG.MONTH"
    F.CATEG.MONTH  = ""
    CALL OPF(YF.CATEG.MONTH,F.CATEG.MONTH)

    YF.CATEG.ENTRY = "F.CATEG.ENTRY"
    F.CATEG.ENTRY  = ""
    CALL OPF(YF.CATEG.ENTRY,F.CATEG.ENTRY)

    YF.CATEGORY = "F.CATEGORY"
    F.CATEGORY  = ""
    CALL OPF(YF.CATEGORY,F.CATEGORY)

    F.CATEG.ENT.TODAY    = ""
    CATEG.ENT.TODAY.FILE = "F.CATEG.ENT.TODAY"
    CALL OPF(CATEG.ENT.TODAY.FILE,F.CATEG.ENT.TODAY)

    F.CATEG.ENT.FWD = ''
    CATEG.ENT.FWD.FILE = 'F.CATEG.ENT.FWD'
    CALL OPF(CATEG.ENT.FWD.FILE,F.CATEG.ENT.FWD)

    F.LDD  = ''
    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    CALL OPF(FN.LDD,F.LDD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD = TODAY
*------------------------------------------------------------------------
    COMP  = C$ID.COMPANY
    YY    = 1
    PL.NO = '520505205252054520585245252057'

    FOR I = 1 TO 6
        FROM.DATE = NDAT
        TO.DATE   = VDAT
        Y.CAT.NO  = PL.NO[YY,5]

        CALL GET.CATEG.MONTH.ENTRIES(FROM.DATE,TO.DATE,Y.CAT.NO,YR.ENTRY.FILE)

        LOOP
            REMOVE YENTRY.KEY FROM YR.ENTRY.FILE SETTING YTYPE
        WHILE YENTRY.KEY:YTYPE

**READ YR.CATEG.ENTRY FROM F.CATEG.ENTRY, YENTRY.KEY ELSE YR.CATEG.ENTRY = ''
            CALL F.READ(YF.CATEG.ENTRY,YENTRY.KEY,YR.CATEG.ENTRY,F.CATEG.ENTRY,ERR)
            IF ERR THEN YR.CATEG.ENTRY = ''

            IF YR.CATEG.ENTRY<AC.CAT.COMPANY.CODE> = ID.COMPANY THEN
*** UPDATED BY ABEER ON  8/10/2012
                IF YR.CATEG.ENTRY<AC.CAT.TRANSACTION.CODE> = '875' OR YR.CATEG.ENTRY<AC.CAT.TRANSACTION.CODE> = '877' THEN
*** END UPDATED BY ABEER ON 8/10/2012
                    IF YR.CATEG.ENTRY<AC.CAT.PRODUCT.CATEGORY> = '21096' THEN
                        INPP  = YR.CATEG.ENTRY<AC.CAT.INPUTTER>

                        TEXT=YENTRY.KEY:'YENTRY.KEY';CALL REM
                        INPTT = FIELD(INPP,'_',2)
                        IF INPTT = 'INPUTTCOB' THEN

                            CALL F.READ(FN.LD,YENTRY.KEY,R.LD,F.LD,E2)
                            ID     = YENTRY.KEY
                            ACCT   = R.LD<AC.STE.ACCOUNT.NUMBER>
                            CUS.ID = R.LD<AC.STE.CUSTOMER.ID>
                            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)

                            CUST.NAME = LOCAL.REF1<1,CULR.ARABIC.NAME>
                            CUST.NAME1    = LOCAL.REF1<1,CULR.ARABIC.NAME.2>
                            CUST.ADDRESS  = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
                            CUST.ADDRESS1 = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,2>

                            IF CUST.ADDRESS EQ  " " THEN
                                CUST.ADDRESS = "���� ��������� ������"
                            END

                            ACC       = R.LD<AC.STE.ACCOUNT.NUMBER>
                            AMOUNT    = ABS(R.LD<AC.STE.AMOUNT.LCY>)
                            AMOUNTFCY = ABS(R.LD<AC.STE.AMOUNT.FCY>)

                            INTER = R.LD<AC.STE.PRODUCT.CATEGORY>
                            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,INTER,CATEG)
                            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,Y.CAT.NO,CATEG2)
                            BRANCH.ID = R.LD<AC.STE.ACCOUNT.OFFICER>
                            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

                            DAT = R.LD<AC.STE.VALUE.DATE>
                            DAT2= R.LD<LD.VALUE.DATE>

                            V.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
                            V.DATE2 = DAT2[7,2]:'/':DAT2[5,2]:"/":DAT2[1,4]
                            REF = R.LD<AC.STE.OUR.REFERENCE>
                            CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,REF,LOCAL)
                            OLDNO=LOCAL<1,LDLR.OLD.NO>
                            TYPE =LOCAL<1,LDLR.PRODUCT.TYPE>
                            IF TYPE = 'ADVANCE' THEN
                                TYPE1='�����'
                            END
                            IF TYPE = 'BIDBOND' THEN
                                TYPE1='�������'
                            END

                            IF TYPE = 'FINAL' THEN
                                TYPE1='����� '
                            END
                            ENDDATE = LOCAL<1,LDLR.END.COMM.DATE>
                            ENDDATE1 = ENDDATE[7,2]:'/':ENDDATE[5,2]:"/":ENDDATE[1,4]
                            IN.AMOUNT = AMOUNT
                            IN.AMOUNT1= AMOUNTFCY

                            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                            CALL WORDS.ARABIC.DEAL(IN.AMOUNT1,OUT.AMOUNT1,78,NO.OF.LINES,ER.MSG)
                            CUR.ID    = R.LD<AC.STE.CURRENCY>
                            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

******************MODIFIED AS OF 22-1-2017
                            IF CUR.ID EQ 'EGP' THEN
                                OUT.AMOUNT  = OUT.AMOUNT
                            END ELSE
                                OUT.AMOUNT = OUT.AMOUNT1

                            END
********END OF MODIFICATION
                            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'


                            XX   = SPACE(132)  ; XX3  = SPACE(132)
                            XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132) ; XX13 = SPACE(132)
                            XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132) ; XX9 = SPACE(132)
                            XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132) ; XX17= SPACE(132)

                            XX<1,1>[3,15]    =  '��� ������  :'
                            XX<1,1>[45,15]   = REF

                            XX10<1,1>[3,15]  = '������   : '
                            XX10<1,1>[20,35] = CUST.NAME :' ':CUST.NAME1

                            XX1<1,1>[3,15]   = '�������      : '
                            XX1<1,1>[20,35]  = CUST.ADDRESS

                            XX17<1,1>[3,15] = '����� ��� :'
                            XX17<1,1>[20,35] = CATEG2

                            XX3<1,1>[3,15] = '����� �������� ���� ���� ������ ��� ������ ������� �����'
                            XX4<1,1>[3,15] = '��� ���� ������ ��� ' : OLDNO :'(':TYPE1:')':'������ ����� '

                            XX8<1,1>[3,15]   = '������'
                            XX8<1,1>[50,15]  = '������'

**********UPDATED BY MAHMOUD 2/3/2010********************
****            XX5<1,1>[3,15]  = AMOUNT :' ':CUR
                            IF CUR.ID EQ 'EGP' THEN

                                XX5<1,1>[3,15]  = AMOUNT :' ':CUR
                            END ELSE
                                XX5<1,1>[3,15]  = AMOUNTFCY :' ':CUR
                            END
*********************************************************

                            XX5<1,1>[45,15]  = '������� �� ������ �������� �� ':ENDDATE1

                            XX6<1,1>[1,15]   = '������ ������� : ':OUT.AMT

                            XX7<1,1>[65,15]  = '�� ��� ���� ������'
                            XX9<1,1>[3,15]   = '����� ���� ':' ':V.DATE
*-------------------------------------------------------------------
                            YYBRN    = FIELD(BRANCH,'.',2)
                            DATY     = TODAY
                            T.DAY    = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
                            NEWDATE  = NDAT[7,2]:'/':NDAT[5,2]:'/':NDAT[1,4]
                            PR.HD  ="'L'":SPACE(1):"LGCOM-OMOLA"
                            PR.HD : ="'L'":SPACE(1):"��� ���� ������":SPACE(35):ACC
                            PR.HD :="'L'":"������� : ":NEWDATE : SPACE(30) : CATEG
                            PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"����� ��� "
                            PR.HD :="'L'":" "
                            PR.HD :="'L'":" "
                            PRINT
                            HEADING PR.HD
*------------------------------------------------------------------
                            PRINT XX10<1,1>
                            PRINT XX1<1,1>
                            PRINT STR(' ',82)
                            PRINT STR(' ',82)
                            PRINT STR(' ',82)
                            PRINT XX3<1,1>
                            PRINT XX4<1,1>

                            PRINT STR(' ',82)
                            PRINT STR(' ',82)
                            PRINT XX17<1,1>
                            PRINT STR(' ',82)
                            PRINT STR(' ',82)

                            PRINT XX8<1,1>
                            PRINT STR(' ',82)
                            PRINT XX5<1,1>
                            PRINT STR('=',82)
                            PRINT XX6<1,1>
                            PRINT STR(' ',82)
                            PRINT XX9<1,1>
                            PRINT STR('=',82)
                            PRINT XX7<1,1>
                        END
                    END
                END
            END
        REPEAT
        YY = YY + 5
    NEXT I
*===== =========================================================
    RETURN
END
