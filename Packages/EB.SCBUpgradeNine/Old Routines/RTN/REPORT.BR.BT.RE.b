* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>739</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------
** Create By Ni7oooo
** Update By Nessma
*********UPDATED BY REHAM YOUSSIF 11/11/2014**********
*----------------------------------------------------
    SUBROUTINE REPORT.BR.BT.RE

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.USER
    $INSERT T24.BP  I_F.BILL.REGISTER
    $INSERT T24.BP  I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP  I_F.CUSTOMER
    $INSERT T24.BP  I_F.CURRENCY
    $INSERT T24.BP  I_F.CATEGORY
    $INSERT T24.BP  I_F.ACCOUNT
    $INSERT         I_F.SCB.CUS.REGION
    $INSERT         I_F.SCB.CUS.GOVERNORATE
    $INSERT         I_F.SCB.BANK
    $INSERT         I_F.SCB.BANK.BRANCH
    $INSERT         I_F.SCB.BT.BATCH
    $INSERT         I_CU.LOCAL.REFS
    $INSERT         I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.BR.BT.RE'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)
*--- EDIT BY NESSMA 27/11/2014
    REG    = ""
    GOV    = ""
    AMOUNT = ""
    CUR    = ""
    BRANCH = ""
    MAT.DAT1 = ""
    RETURN
*========================================================================
PROCESS:
*-------
    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
    BT.ID = COMI

    CALL F.READ(FN.BATCH,BT.ID,R.BT,F.BATCH,ERR.BT)
    INPUTTER = R.BT<SCB.BT.INPUTTER>
    AUTH     = R.BT<SCB.BT.AUTHORISER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    T.SEL = "SELECT F.SCB.BT.BATCH WITH @ID EQ ": COMI
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        CALL F.READ(FN.BATCH,KEY.LIST<1>,R.BATCH,F.BATCH,E1)
        BR.ID = R.BATCH<SCB.BT.OUR.REFERENCE>
*Line [ 92 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD    = DCOUNT(BR.ID,@VM)
        TEXT  = DD    ; CALL REM

        FOR I = 1 TO DD
            BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
            RES    = R.BATCH<SCB.BT.RETURN.REASON>
            BT.DAT = R.BATCH<SCB.BT.DATE.TIME>

            IF R.BATCH<SCB.BT.RETURN.REASON><1,I> EQ '' THEN
                CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
                DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>

                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
                CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>
                CUST.NAME.2   = LOCAL.REF<1,CULR.ARABIC.NAME.2>
                CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
                CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
                CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
                CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>
                CUST.REGION   = LOCAL.REF<1,CULR.REGION>
                CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.REGION,REG)

                FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                    CUST.ADDRESS1 = "���� ��������� ������"
                END ELSE
                    CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
                    CUST.ADDRESS2 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,2>
                    CUST.ADDRESS3 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,3>
                    CUST.REGION   = LOCAL.REF<1,CULR.REGION>
                    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.REGION,REG)
                    CUST.GOVER = LOCAL.REF<1,CULR.GOVERNORATE>
                    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.GOVER,GOV)
                END

                ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
                CHQ.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
                BANK         = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
                BANK.BRAN    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
                COLL.DATE    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.COLL.DATE>

                CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK,BNAME)
                CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BANK.BRAN,BRNAME)
*       CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
                CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
                BRN.ID  = AC.COMP[2]
                BRANCH.ID = TRIM(BRN.ID,"0","L")
                CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

                CATEG.ID  = ACC.NO[11,4]
                CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

                AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
                IN.AMOUNT = AMOUNT
                CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

                CUR.ID  = R.BR<EB.BILL.REG.CURRENCY>
                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
                OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

                DAT     = TODAY
*            CALL CDT("",DAT,'+1W')
                MAT.DATE = TODAY[1,2] : BT.DAT[1,6]
*                MAT.DATE  = MAT.DAT1[7,2]:'/':MAT.DAT1[5,2]:"/":MAT.DAT1[1,4]
                MAT.DATE = FMT(MAT.DATE,"####/##/##")
                XX        = SPACE(132)
                XX1       = SPACE(132)
                XX2       = SPACE(132)
                XX3       = SPACE(132)
*--------------------------------------------
                PR.HD  = "REPORT.BR.BT.RE"
                HEADING PR.HD
                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
                YYBRN  = FIELD(BRANCH,'.',2)
                DATY   = TODAY
                T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
                PRINT SPACE(5) : SPACE(1):"��� ���� ������"
                PRINT SPACE(5) : "������� : ":T.DAY
                PRINT SPACE(5) : "����� : ":YYBRN
*---------------------------------------------------------------
                XX<1,1>[3,35]    = "�������"
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,35]    = CUST.NAME:' ':CUST.NAME.2
                XX1<1,1>[3,35]   = CUST.ADDRESS1
                XX2<1,1>[3,35]   = CUST.ADDRESS2
                XX3<1,1>[3,120]  = CUST.ADDRESS3:'-':REG:'-':GOV

                PRINT XX<1,1>
                PRINT XX1<1,1>
                PRINT XX2<1,1>
                PRINT XX3<1,1>
                XX<1,1>  = ""
                XX1<1,1> = ""
                XX2<1,1> = ""
                XX3<1,1> = ""

                XX<1,1>[3,15]   = '������     : '
                XX<1,1>[30,15]  = AMOUNT
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� ������ : '
                XX<1,1>[20,15] = ACC.NO
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� ������ : '
                XX<1,1>[20,15] = CATEG
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '������     : '
                XX<1,1>[20,15] = CUR
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '����� ���� : '
                XX<1,1>[30,15] = MAT.DATE
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� �����      : '
                XX<1,1>[20,15] = CHQ.NO
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� �������    : '
                XX<1,1>[30,15] = BR.ID1
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '����� ������� ����: '
                XX<1,1>[30,15] = BNAME
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]  = '��� �����: '
                XX<1,1>[30,15] = BRNAME
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]    = '������'
                XX<1,1>[30,15]   = INP
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[1,15]   = "��� �������"
                XX<1,1>[35,15]  = COMI
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]    = '������'
                XX<1,1>[30,15]   = AUTHI
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,35]   = '������ ������� : '
                XX<1,1>[20,15]  = OUT.AMT
                PRINT XX<1,1>
                XX<1,1>  = ""

                XX<1,1>[3,15]   = '������         : '
                XX<1,1>[20,15]  = '����� ��� �������'
                PRINT XX<1,1>
                XX<1,1> = ""
            END
        NEXT I
    END
    RETURN
*---------------------------------------------------------
END
