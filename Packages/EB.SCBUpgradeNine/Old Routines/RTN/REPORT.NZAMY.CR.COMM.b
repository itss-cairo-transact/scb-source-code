* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NI7OOOOOOOOOOOOOO**********************
*-----------------------------------------------------------------------------
* <Rating>-90</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.NZAMY.CR.COMM
*
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE

*------------------------------------------------------------------------
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT> NE ' ' THEN
        GOSUB INITIATE
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ���������" ; CALL REM
    END
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.NZAMY.CR.COMM'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.B=''
    CALL OPF(FN.BR,F.BR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""


    DRAWER.ID   = R.NEW(EB.BILL.REG.DRAWER)

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
    CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)


    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END

* ACC.NO    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.LIQ.ACCT>

    CATEG.ID  = ACC.NO[3,5]
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

    AMOUNT    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.CCY.AMT>
    AMT1      = AMOUNT[4,9]
    IN.AMOUNT = AMT1
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

*BR.DATA = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE>

    CUR.ID    = R.NEW(EB.BILL.REG.CURRENCY)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

*   DAT =  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE>

*    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    COM.CODE    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COMM.TYPE>
    CALL DBR ('FT.COMMISSION.TYPE':@FM:FT4.DESCRIPTION,COM.CODE,COM.NAME)
    CALL DBR ('FT.COMMISSION.TYPE':@FM:FT4.CATEGORY.ACCOUNT,COM.CODE,NEW.CAT)

*TEXT = 'NEW.CAT': NEW.CAT ; CALL REM
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,NEW.CAT,NEW.DESC)

    ACC.NO    = 'PL':NEW.CAT

*TEXT = ACC.NO ; CALL REM
    INPUTTER = R.NEW(EB.BILL.REG.INPUTTER)
    AUTH     = R.NEW(EB.BILL.REG.AUTHORISER) EB.BILL.REG.AUTHORISER
    INP      = FIELD(INPUTTER,'_',2)

*TEXT = INP ; CALL REM

    AUTH    = R.USER<EB.USE.SIGN.ON.NAME>
*TEXT    = AUTH ; CALL REM
*  CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
* TEXT = AUTHI ; CALL REM

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]    = CATEG
*    XX2<1,1>[3,35]   = CATEG
* XX3<1,1>[3,35]   = CUST.ADDRESS
* XX4<1,1>[3,35]   = CUST.ADD1:' ': CUST.ADD2
    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT
    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.NO
*TEXT = 'ACC.NO': ACC.NO ; CALL REM
    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = NEW.DESC
*TEXT = NEW.DESC ; CALL REM
    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR
* XX4<1,1>[45,15] = '����� ���� : '
* XX4<1,1>[59,15] = MAT.DATE
    XX5<1,1>[3,15]  = '�������    : '
    XX5<1,1>[20,15] = COM.NAME
*TEXT = COM.NAME ; CALL REM
    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTH
    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = INP
    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = ID.NEW
    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
* XX9<1,1>[3,15]  = '������         : '
* XX9<1,1>[20,15] = BR.DATA

*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":"REPORT.NZAMY.CR.COMM"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(45) :"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*NEXT I
*END
*===============================================================
    RETURN
END
