* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
    SUBROUTINE REPORT.SCB.CHQ

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
 TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.SCB.CHQ'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    TD = TODAY
    XX = COMI
    FN.LD = 'FBNK.CHEQUE.ISSUE' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    YTEXT = "Enter the AC No. : "
    CALL TXTINP(YTEXT, 8, 22, "30", "A")
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    YY = FIELD(COMI,'.',1)
    ZZ = FIELD(COMI,'.',2)
    ZZ1= FIELD(COMI,'.',3)
    ACC.NO = ZZ
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
****    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        ACC.BR  = AC.COMP[8,2]

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1   = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END

    AMOUNT = R.LD<CHEQUE.IS.NUMBER.ISSUED> * 1.5
    TEXT ="AMT:" : AMOUNT ; CALL REM
****************ADDED BY MAHMOUD 7/12/2009******************
    AMOUNT1 = R.LD<CHEQUE.IS.NUMBER.ISSUED>
    CHARGES = R.LD<CHEQUE.IS.CHARGES> * 1.5
   *** AMOUNT  = (AMOUNT1 / 2) + CHARGES

************************************************************
    V.DATE = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]

    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.LD<CHEQUE.IS.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)

    XX<1,1>[3,15]    = CUST.NAME

    XX<1,1>[45,15]   = '��� ������  : '
    XX<1,1>[60,15]   = ZZ

    XX1<1,1>[3,35]   = CUST.ADDRESS

    XX2<1,1>[3,15]   = '����� �������� ���� ���� ������ ��� ������ ������� ����� �� ��� :'

    XX3<1,1>[3,15]   = AMOUNT
    XX3<1,1>[20,15]  = "���� ������ ��� ���� ����� ���    (    )"

    XX4<1,1>[3,15]   = OUT.AMT

    XX5<1,1>[60,15]  = "�� ��� ���� ������"
    XX6<1,1>[60,15]  = "--------------------"
*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = TD[7,2]:'/':TD[5,2]:'/':TD[1,4]
    PR.HD ="'L'":"����� : " : YYBRN :SPACE(20):"����� ���":SPACE(20):"������� : ":T.DAY
    PR.HD :="'L'":"-------------------------------------------------------------------------------- "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX1<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>

*===============================================================
    RETURN
END
