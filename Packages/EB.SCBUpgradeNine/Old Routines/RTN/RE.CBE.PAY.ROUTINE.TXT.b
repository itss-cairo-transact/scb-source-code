* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>809</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE RE.CBE.PAY.ROUTINE.TXT
*    PROGRAM CBE.PAY.ROUTINE.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PAY.CBE.CODES

*****
    OPENSEQ "&SAVEDLISTS&" , "CBE.PAY.CODE2.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CBE.PAY.CODE2.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CBE.PAY.CODE2.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CBE.PAY.CODE2.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create CBE.PAY.CODE2.TXT File IN &SAVEDLISTS&'
        END
    END

*****ENTER HEADER MONTH **********
    YTEXT = "Enter Header Month (xx): "
    CALL TXTINP(YTEXT, 8, 22, "2", "A")
    HEADER.MONTH  = COMI
    IF LEN(HEADER.MONTH) EQ 1 THEN
        HEADER.MONTH = '0':HEADER.MONTH
    END
    YTEXT = "Enter Serial no (xx): "
    CALL TXTINP(YTEXT, 8, 22, "2", "A")
    NUM.SEND  = COMI
    IF LEN(NUM.SEND) EQ 1 THEN
        NUM.SEND = '0':NUM.SEND
    END

**********************************

    EOF        = ''
    BB.DATA    = ''
    BNK.CODE   = '1700'
    ZEROS      = STR('0',446)
    T.DATE     = TODAY
    T.YEAR     = T.DATE[1,4]

    IF HEADER.MONTH EQ 12 THEN
        T.YEAR = T.YEAR - 1
    END

    TRANS.DATE.1 = HEADER.MONTH:"-":T.YEAR


    BB.DATA  = TRANS.DATE.1
    BB.DATA := BNK.CODE
    BB.DATA := NUM.SEND
    BB.DATA := '2'
    BB.DATA := ZEROS

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*======== GET DATA FROM USER =====================


    YTEXT = "Enter Posted Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    POST.DATE = COMI
    IF POST.DATE EQ '' THEN
        TEXT ="You must enter Posted Date"; CALL REM
        RETURN
    END

*==================================================================
    FN.CBE  = 'F.SCB.MONTHLY.PAY.CBE' ; F.CBE = '' ; R.CBE = ''
    CALL OPF( FN.CBE,F.CBE)
    FN.CO   = 'F.SCB.PAY.CBE.CODES'   ; F.CO  = '' ; R.CO  = ''
    CALL OPF( FN.CO,F.CO)
    FN.CU   = 'FBNK.CURRENCY'         ; F.CU  = '' ; R.CU  = ''
    CALL OPF( FN.CU,F.CU)

    T.DATE = TODAY
*    T.SEL   = "SELECT F.SCB.MONTHLY.PAY.CBE WITH ((TYPE.FILE EQ 2) OR (TYPE.FILE EQ 1 AND FUNTP.CODE EQ 4 AND DOCTP.CODE EQ 3))"
*   T.SEL  := " AND CUR.CODE NE EGP AND POSTED EQ ''"
*  T.SEL  := " AND TRANSFER.DATE GE ":FROM.DATE:" AND TRANSFER.DATE LE ":TO.DATE:" AND AMT NE 0 AND AMT NE '' AND AMT GE 1"

    T.SEL   = "SELECT F.SCB.MONTHLY.PAY.CBE WITH ((TYPE.FILE EQ 2) OR (TYPE.FILE EQ 1 AND FUNTP.CODE EQ 4 AND DOCTP.CODE EQ 3)) AND POSTED EQ ":POST.DATE

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    RECV.CODE = ''
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>, R.CBE, F.CBE, ETEXT)

*******BY NOHA HAMED 24/7/2016******

            CUS.NO     = R.CBE<CBE.PAY.CUSTOMER.ID>
            IF CUS.NO[1,1] EQ '0' THEN
                CUS.NO = CUS.NO[2,7]
            END

            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO,CUS.LOCAL)
            REG.NO     = CUS.LOCAL<1,CULR.COM.REG.NO>
            TAX.NO     = CUS.LOCAL<1,CULR.TAX.NO>
*************************************

            TR.DATE    =  R.CBE<CBE.PAY.TRANSFER.DATE>
            TRANS.DATE =  TR.DATE[7,2]:"-":TR.DATE[5,2]:"-":TR.DATE[1,4]

*===== GET BRANCH CODE ==============
            BR.NO     = R.CBE<CBE.PAY.BRAN.NO>

            Y.SEL = "SELECT F.SCB.PAY.CBE.CODES WITH GLOBUS.CODE EQ ":BR.NO
            CALL EB.READLIST(Y.SEL, KEY.LIST2, "", SELECTED2, ASD)
            CALL F.READ( FN.CO,KEY.LIST2<1>, R.CO, F.CO, ETEXT)

            BR.CODE.1 = R.CO<CBE.CODE.CODE>
            BR.CODE   = FMT(BR.CODE.1,"R%4")

*===== GET AREA CODE ================
            AREA.NAME = R.CBE<CBE.PAY.ARERAN.CODE>
            IF AREA.NAME = '���� �������' THEN
                AREA.CODE = 1
            END
            ELSE
                AREA.CODE = 2
            END


*======= GET CURRENCY CODE =============

            R.CO = ''
            CURR.CODE = R.CBE<CBE.PAY.CUR.CODE>
            X.SEL = "SELECT F.SCB.PAY.CBE.CODES WITH GLOBUS.CODE EQ ":CURR.CODE
            CALL EB.READLIST(X.SEL, KEY.LIST3, "", SELECTED3, ASD)
            CALL F.READ( FN.CO,KEY.LIST3<1>, R.CO, F.CO, ETEXT)

            CURR.NO.1  = R.CO<CBE.CODE.CODE>
            CURR.NO    = FMT(CURR.NO.1,"R%4")


*===== GET NATIONALITY CODE ================

            NAT.NAME = R.CBE<CBE.PAY.ISSUE.NATION>
            IF NAT.NAME = '����' THEN
                NAT.CODE = 1
            END
            ELSE
                NAT.CODE = 2
            END

*===== GET RECV TERM CODE===================

            RECV.NAME = R.CBE<CBE.PAY.RECV.TERM.CODE>
            IF RECV.NAME  = 'FOB' THEN
                RECV.CODE = 1
            END
            ELSE IF RECV.NAME = 'CIF' THEN
                RECV.CODE = 2
            END
            ELSE IF RECV.NAME = 'C&F' THEN
                RECV.CODE = 3
            END
            ELSE IF RECV.NAME = 'OTHERS' THEN
                RECV.CODE = 4
            END


****CREATE NEW TEXT****

            BB.DATA  = BR.CODE
            SERIAL.NO = FMT(SELECTED5,"R%6")
            BB.DATA  := SERIAL.NO

            BB.DATA := AREA.CODE

            FUNTP.CODE.1 = R.CBE<CBE.PAY.FUNTP.CODE>
            FUNTP.CODE   = FMT(FUNTP.CODE.1,"R%2")
            BB.DATA     := FUNTP.CODE

            DOCTP.CODE.1 = R.CBE<CBE.PAY.DOCTP.CODE>
            DOCTP.CODE   = FMT(DOCTP.CODE.1,"R%2")
            BB.DATA     := DOCTP.CODE

            PAY.CODE.1   = R.CBE<CBE.PAY.PAYMENT.CODE>
            PAY.CODE     = FMT(PAY.CODE.1,"R%3")
            BB.DATA     := PAY.CODE
            IF RECV.CODE EQ '' THEN
                RECV.CODE = 4
            END

            BB.DATA     := CURR.NO
            CNTR.CODE.1  = R.CBE<CBE.PAY.COUNTRY.CODE>
            CNTR.CODE    = FMT(CNTR.CODE.1,"R%4")
            BB.DATA     := CNTR.CODE
            BB.DATA     := TRANS.DATE
            BB.DATA     := RECV.CODE


            LEG.CODE.1   = R.CBE<CBE.PAY.LEG.TYPE>
            LEG.CODE     = FMT(LEG.CODE.1,"R%4")
            BB.DATA     := LEG.CODE

            OP.CODE.1    = R.CBE<CBE.PAY.OP.TYPE.CODE>
            IF OP.CODE.1 EQ '' THEN
                OP.CODE.1 = 11
            END
            OP.CODE      = FMT(OP.CODE.1,"R%2")
            BB.DATA     := OP.CODE

            ISSUE.1      = R.CBE<CBE.PAY.ISSUE.TEXT>
            LEN10        = LEN(ISSUE.1)
            NO10         = 100 - LEN10
            ISSUE.2      = ISSUE.1:STR(' ',NO10)
            BB.DATA     := ISSUE.2



            ACTIVE.CODE.1 = R.CBE<CBE.PAY.ACTIVE.CODE>
            ACTIVE.CODE   = FMT(ACTIVE.CODE.1,"R%4")
            BB.DATA     := ACTIVE.CODE

            AMT.1        = R.CBE<CBE.PAY.AMT>
            AMT.2        = FIELD(AMT.1,'.',1)
            AMT.3        = FMT(AMT.2,"R%15")
            BB.DATA     := AMT.3

            NUM.DOC.1    = R.CBE<CBE.PAY.DOC.NUM>
            NUM.DOC      = FMT(NUM.DOC.1,"R%20")
            BB.DATA     := NUM.DOC

            BB.DATA     := NAT.CODE

            NOTES.1      = R.CBE<CBE.PAY.NOTES1>
            LEN9         = LEN(NOTES.1)
            NO9          = 300 - LEN9
            NOTES.3      = NOTES.1:STR(' ',NO9)
            BB.DATA     := NOTES.3

            CHANGE '/' TO '' IN TAX.NO
            CHANGE '/' TO '' IN REG.NO
            CHANGE '-' TO '' IN TAX.NO
            CHANGE '-' TO '' IN REG.NO
            CHANGE '\' TO '' IN TAX.NO
            CHANGE '\' TO '' IN REG.NO

            BB.DATA :=FMT(REG.NO,"R%15")
            BB.DATA :=FMT(TAX.NO,"R%15")

******UPDATE BY MENNA ASHRAF 5/2/2020
            REQ.GOODS = R.CBE<CBE.PAY.REQ.GOODS.CODE>
            BB.DATA     := FMT(REQ.GOODS,"R%6")
******
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            R.CBE<CBE.PAY.POSTED> = TODAY
            R.CBE<CBE.PAY.TRNDET.SER> = SERIAL.NO
            CALL F.WRITE(FN.CBE,KEY.LIST<I>,R.CBE)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)

*******
            SELECTED5 = SELECTED5 + 1
        NEXT I
    END
    CLOSESEQ BB

    PRINT "FINISHED"
*================

END
