* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE REPORT.FT.SADER

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION

*-------------------------------------------------------------------------
* VV = R.NEW(FT.CHARGE.AMT)
*XX = R.NEW(FT.COMMISSION.AMT)

*IF R.NEW(FT.CHARGE.AMT)[1,3] EQ 'EGP' AND R.NEW(FT.COMMISSION.AMT)[1,3] EQ 'EGP' THEN
* IF R.NEW(FT.DEBIT.CURRENCY) NE R.NEW(FT.CREDIT.CURRENCY) THEN
*    IF R.NEW(FT.DEBIT.CURRENCY) NE 'EGP' AND R.NEW(FT.CREDIT.CURRENCY) NE 'EGP' THEN

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 57 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*   END
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='REPORT.FT.SADER'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.FT='F.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)

* FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
* CALL OPF(FN.BR,F.BR)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    ID = ID.NEW
    DATE.TO = TODAY[3,6]:"..."
*------------------------------------------------------------------------
    COM.CODE = R.NEW(FT.CHARGES.ACCT.NO)
***TEXT = "COM.CODE= " : COM.CODE ; CALL REM
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,COM.CODE,CURRR)
****TEXT = "CURRR= " : CURRR ; CALL REM
    IF CURRR EQ 'EGP' THEN
****TEXT = "COM.CODE[9,2] " : COM.CODE[9,2] ; CALL REM
        AMOUNT   = R.NEW(FT.TOTAL.CHARGE.AMOUNT)
        MMM      = LEN(AMOUNT) - 3
        AMOUNT1  = AMOUNT[4,MMM]
        CUR.ID   = R.NEW(FT.DEBIT.CURRENCY)
        DAT      = R.NEW(FT.DEBIT.VALUE.DATE)
        ACC      = R.NEW(FT.DEBIT.ACCT.NO)

**************ADDED BY MAHMOUD 6/12/2009*****************
        CHARGE.CUR = AMOUNT[1,3]
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CUR,CH.CUR)
*********************************************************

****TEXT = "ACC" : ACC ; CALL REM

        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,COM.CODE,CUS.ID)
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,COM.CODE,CATEG)
        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG,CATEG.ID)
***TEXT = "AAA" ; CALL REM
        CUST.NAME       = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.NAME.2     = LOCAL.REF<1,CULR.ARABIC.NAME.2>
        CUST.ADDRESS    = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
        CUST.ADDRESS1   = LOCAL.REF<1,CULR.GOVERNORATE>
        CUST.ADDRESS2   = LOCAL.REF<1,CULR.REGION>

        CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
        CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)

        IF CUST.ADDRESS1 = 98 THEN
            CUST.ADD2 = ''
        END
        IF CUST.ADDRESS2 = 998 THEN
            CUST.ADD1 = ''
        END
        IF CUST.ADDRESS1 = 999 THEN
            CUST.ADD2 = ''
        END
        IF CUST.ADDRESS2 = 999 THEN
            CUST.ADD1 = ''
        END

        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CURRR,CUR)
        IN.AMOUNT = AMOUNT1
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT=OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
* DAT  = R.BR<EB.BILL.REG.MATURITY.DATE>
        MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
        INPUTTER = R.NEW(FT.INPUTTER)
        AUTH = R.NEW(FT.AUTHORISER)
        INP = FIELD(INPUTTER,'_',2)
        AUTHI =FIELD(AUTH,'_',2)
***TEXT = "INP :" MAT.DATE ; CALL REM
        XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX10 = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)

        XX<1,1>[1,15]    = CUST.NAME
        XX1<1,1>[1,15]   = CUST.NAME.2
        XX3<1,1>[1,15]   = CUST.ADDRESS
        XX4<1,1>[1,15]   = CUST.ADD2 : ' ' : CUST.ADD1

        XX<1,1>[40,15]  = '������ : '
        XX<1,1>[50,15]  = AMOUNT1

        XX1<1,1>[40,15] = '��� ������ : '
        XX1<1,1>[54,15] = COM.CODE

        XX2<1,1>[40,15] = '��� ������ : '
        XX2<1,1>[54,15] = CATEG.ID

        XX3<1,1>[40,15] = '������ : '
        XX3<1,1>[50,15] = CUR

        XX4<1,1>[40,15] = '����� ���� : '
        XX4<1,1>[54,15] = MAT.DATE

        XX6<1,1>[1,15]  = '������'
        XX7<1,1>[1,15] = ID.NEW

        XX6<1,1>[30,15]  = '��� �������'
        XX7<1,1>[30,15]  = INP

        XX5<1,1>[3,15]  = '������ �������'
        XX5<1,1>[20,15] = OUT.AMT

        XX6<1,1>[60,15]  = '������'
        XX7<1,1>[60,15] = AUTHI


        BYAN = R.NEW(FT.LOCAL.REF)<1,FTLR.NOTE.DEBITED>

        XX11<1,1>[3,15] = '������:'
        XX11<1,1>[20,15] = BYAN

****TEXT = "DDDDD" ; CALL REM
        PRINT XX<1,1>
        PRINT XX1<1,1>
* PRINT XX2<1,1>
        PRINT XX3<1,1>
        PRINT XX4<1,1>
        PRINT STR(' ',82)
        PRINT XX5<1,1>
        PRINT STR(' ',82)
* PRINT XX5<1,1>
        PRINT XX11<1,1>
        PRINT STR(' ',82)
        PRINT XX6<1,1>
        PRINT STR('-',82)
        PRINT XX7<1,1>

        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
        PRINT STR(' ',82)
*  NEXT I
    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"����� : ":YYBRN  :SPACE(45):"����� ���"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*  END
*END
    RETURN
END
