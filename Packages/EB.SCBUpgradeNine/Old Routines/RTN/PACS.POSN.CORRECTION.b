* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-63</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE PACS.POSN.CORRECTION(SL.ID)
*------------------------------------------------------------------------------------------*
* Author: Rajesh B
* Routine create position records based on the amount and ccy given in &SAVEDLISTS& record
* POS.LIST
* The format to be used in &SAVEDLISTS& record is,
* COMPANY.CODE^CCY^FAMT^RATE^LAMT
* Here RATE and LAMT is optional, if not provided system will consider the current mid reval rate
*------------------------------------------------------------------------------------------*
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MNEMONIC.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    GOSUB INIT
    GOSUB PROCESS

    RETURN

INIT:

    FN.POSITION = 'F.POSITION'
    F.POSITION = ''; CALL OPF(FN.POSITION, F.POSITION)

    FN.SL = '&SAVEDLISTS&'
    F.SL = ''; CALL OPF(FN.SL, F.SL)

    Y.POS.DATE = R.DATES(EB.DAT.LAST.WORKING.DAY)

    RETURN

PROCESS:

    Y.LCY.AMT = ''; Y.MNE = ''; Y.AMT = ''; Y.CCY = ''; Y.RATE = ''
    CO.CODE = FIELD(SL.ID, '^', 1)
    Y.AMT = FIELD(SL.ID, '^', 3)
    Y.CCY = FIELD(SL.ID, '^', 2)
    Y.RATE  = FIELD(SL.ID, '^', 4)
    Y.LCY.AMT = FIELD(SL.ID, '^', 5)
* lcy equivalent will be arrived based on latest mid rate if RATE is not provided in POS.LIST
    CALL MIDDLE.RATE.CONV.CHECK(Y.AMT, Y.CCY, Y.RATE, "1", Y.LCY.AMT, "", "")
    GOSUB FORM.POS.ID

    RETURN

FORM.POS.ID:

    POS.ID = CO.CODE:'1TR00':Y.CCY:LCCY:Y.POS.DATE
    POS.REC = ''; POS.ERR = ''; POS.LCY.REC = ''

    CALL F.READ(FN.POSITION, POS.ID, POS.REC, F.POSITION, POS.ERR)
    IF POS.ERR THEN
        GOSUB UPDATE.POSITION
        GOSUB UPDATE.LCY.POSITION
    END ELSE
* if Position record exists then check for the record with previous working day...
        CALL CDT('', Y.POS.DATE, '-1W')
        GOSUB FORM.POS.ID
    END

    RETURN

UPDATE.POSITION:

    POS.REC<CCY.POS.AMOUNT.1> = -Y.AMT
    POS.REC<CCY.POS.AMOUNT.2> = Y.LCY.AMT
    POS.REC<CCY.POS.LCY.AMOUNT> = -Y.LCY.AMT
    POS.REC<CCY.POS.SD.AMOUNT.1, 1> = -Y.AMT
    POS.REC<CCY.POS.SD.AMOUNT.2, 1> = Y.LCY.AMT
    POS.REC<CCY.POS.SD.LCY.AMOUNT, 1> = -Y.LCY.AMT
    POS.REC<CCY.POS.SYSTEM.DATE> = TODAY
    CRT 'Position record created with ID: ':POS.ID:' amount ':POS.REC<CCY.POS.AMOUNT.1>:'#':POS.REC<CCY.POS.AMOUNT.2>
    WRITE POS.REC TO F.POSITION, POS.ID

    RETURN

UPDATE.LCY.POSITION:

    POS.LCY.ID = CO.CODE:'1TR00':LCCY:Y.CCY:Y.POS.DATE
    POS.LCY.REC<CCY.POS.AMOUNT.1> = Y.LCY.AMT
    POS.LCY.REC<CCY.POS.AMOUNT.2> = -Y.AMT
    POS.LCY.REC<CCY.POS.LCY.AMOUNT> = Y.LCY.AMT
    POS.LCY.REC<CCY.POS.SD.AMOUNT.1, 1> = Y.LCY.AMT
    POS.LCY.REC<CCY.POS.SD.AMOUNT.2, 1> = -Y.AMT
    POS.LCY.REC<CCY.POS.SD.LCY.AMOUNT, 1> = Y.LCY.AMT
    POS.LCY.REC<CCY.POS.SYSTEM.DATE> = TODAY
    CRT 'Position LCY record created with ID: ':POS.LCY.ID:' amount ':POS.LCY.REC<CCY.POS.AMOUNT.1>:'#':POS.LCY.REC<CCY.POS.AMOUNT.2>
    WRITE POS.LCY.REC TO F.POSITION, POS.LCY.ID

    RETURN
END
