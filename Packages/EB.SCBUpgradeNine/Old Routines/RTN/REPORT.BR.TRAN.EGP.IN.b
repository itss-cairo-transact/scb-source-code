* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE

*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.BR.TRAN.EGP.IN
*** PROGRAM REPORT.BR.TRAN.EGP.IN
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 43 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    GOTO PROGRAM.END
*==============================================================
INITIATE:
    REPORT.ID='REPORT.BR.TRAN.EGP.IN'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

   * DATEE = '20090414'

    T.SEL = "SELECT FBNK.BILL.REGISTER WITH BIL.CHQ.TYPE NE 10 AND CURRENCY NE EGP AND STATUS.DATE EQ ": TODAY :' EVAL"FT.REFERENCE"'
**    T.SEL = "SELECT FBNK.BILL.REGISTER WITH BIL.CHQ.TYPE NE 10 AND CURRENCY EQ EGP AND BANK LIKE 00... AND STATUS.DATE EQ 20080828 "
    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF(FN.BR,F.BR)
    KEY.LIST.BR ="" ; SELECTED.BR="" ;  ER.MSG.BR=""

    IF T.SEL THEN
        CALL EB.READLIST(T.SEL,KEY.LIST.BR,"",SELECTED.BR,ER.MSG.BR)
        TEXT = 'SELECTED.BR=':SELECTED.BR ; CALL REM
        FOR I = 1 TO SELECTED.BR
            CALL F.READ(FN.BR,KEY.LIST.BR<I>, R.BR,F.BR, ETEXT)
****UPDATED BY NESSREEN AHMED ON 10/09/2008********************
*TEXT = 'KEY=':KEY.LIST.BR<I> ; CALL REM
**  FT.REF = R.BR<EB.BILL.REG.LOCAL.REF,BRLR.FT.REFERENCE>
**            TEXT = 'FT.REF=':FT.REF ; CALL REM
**   T.SEL.STMT = "SELECT FBNK.STMT.ENTRY WITH OUR.REFERENCE EQ ":FT.REF
            T.SEL.STMT = "SELECT FBNK.STMT.ENTRY WITH OUR.REFERENCE EQ ":KEY.LIST.BR<I>
****END OF UPDATED*********************************************
            FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT = ''
            CALL OPF(FN.STMT,F.STMT)
            KEY.LIST.STMT ="" ; SELECTED.STMT="" ;  ER.MSG.STMT=""
            CALL EB.READLIST(T.SEL.STMT,KEY.LIST.STMT,"",SELECTED.STMT,ER.MSG.STMT)
* TEXT = 'SELECTED.STMT=' : SELECTED.STMT ; CALL REM
            FOR A =1 TO SELECTED.STMT
                CALL F.READ(FN.STMT,KEY.LIST.STMT<A>, R.STMT,F.STMT, ETEXT)
                DAT = R.STMT<AC.STE.BOOKING.DATE>
                TEXT='DATE= ':DAT; CALL REM
                BOOK.DAT = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
                XX = 30 - LEN(R.STMT<AC.STE.ACCOUNT.NUMBER>)
                YY = 30 - LEN(R.STMT<AC.STE.AMOUNT.LCY>)
* TEXT = 'AC.NO=':R.STMT<AC.STE.ACCOUNT.NUMBER> ; CALL REM
* TEXT = 'AMOUNT=':R.STMT<AC.STE.AMOUNT.LCY> ; CALL REM
                PRINT R.STMT<AC.STE.ACCOUNT.NUMBER> :SPACE(XX) : R.STMT<AC.STE.AMOUNT.LCY> : SPACE(YY) : BOOK.DAT

            NEXT A
**************************************************************************
            CALL DBR('CUSTOMER':@FM:EB.CUS.MNEMONIC,R.BR<EB.BILL.REG.DRAWER>,MN)
            PRINT KEY.LIST.BR<I> : SPACE(15) :R.BR<EB.BILL.REG.DRAWER> : SPACE(15) : MN : SPACE(15) :R.BR<EB.BILL.REG.CURRENCY>
            PRINT "=================================================="
        NEXT I
        TEXT = 'END.OF.REPORT' ; CALL REM
    END

    RETURN
*===============================================================
PRINT.HEAD:
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������ "
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(80):"��� ������ : ":"'P'"
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME, R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PR.HD :="'L'":SPACE(100):"����������":":":YY

    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(58):"������ ������� ������� ���� ������"
    PR.HD :="'L'":SPACE(55):STR('_',36)
    PR.HD :="'L'":" "
* PR.HD :="'L'":SPACE(1):" �����":SPACE(37):"��� ������ ������":SPACE(10):"��� ������ ������":SPACE(10):"�������"
*    PR.HD :="'L'":SPACE(1):"��� ������":SPACE(15):"��� ������":SPACE(25):"������" : SPACE(15) : "��� �������"
*PRINT R.STMT<AC.STE.ACCOUNT.NUMBER> : SPACE(15) : R.STMT<AC.STE.AMOUNT.LCY> : SPACE(15) : BOOK.DAT
    PR.HD :="'L'":SPACE(1):"��� ������":SPACE(17):"������":SPACE(20):"�������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    RETURN
*==============================================================
PROGRAM.END:

    RETURN
END
