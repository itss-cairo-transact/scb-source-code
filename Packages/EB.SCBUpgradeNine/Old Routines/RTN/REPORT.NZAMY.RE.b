* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***********************************NI7OOOOOOOO***************************
    SUBROUTINE REPORT.NZAMY.RE
*
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
*
    COMP = ID.COMPANY
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    COMP = ID.COMPANY
    REPORT.ID='REPORT.NZAMY.RE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.B=''
    CALL OPF(FN.BR,F.BR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*    DATE.TO = TODAY[3,6]:"..."
*   DATE.TO = "081026":"..."
*------------------------------------------------------------------------
*   T.SEL="SELECT F.SCB.BT.BATCH WITH DATE.TIME LIKE " :DATE.TO
*  CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*  IF SELECTED THEN
*      FOR I = 1 TO SELECTED
*          CALL F.READ(FN.BATCH,KEY.LIST<I>,R.BATCH,F.BATCH,E2)

    FN.DR = 'FBNK.BILL.REGISTER' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    YTEXT = "Enter the BR No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    ID = COMI
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    XX = COMP[2]
    DRAWER.ID    = R.DR<EB.BILL.REG.DRAWER>
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    ACC.NO      = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
    ACC.NO1       = '19002000100': XX
    TEXT = "ACC.NO1 : " : ACC.NO1  ; CALL REM
**** ACC.NO2    = ACC.NO1

    CATEG.ID   = ACC.NO[11,4]
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

    AMOUNT    = R.DR<EB.BILL.REG.AMOUNT>
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    BR.DATA   = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.OPERATION.TYPE>

    CUR.ID    = R.DR<EB.BILL.REG.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    DAT =  R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.RECEIVE.DATE>
* DAT  = R.NEW(EB.BILL.REG.MATURITY.DATE)
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    CHQ.NO    = R.DR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>

    INPUTTER = R.DR<EB.BILL.REG.INPUTTER>
    AUTH     = R.DR<EB.BILL.REG.AUTHORISER>
    INP   = FIELD(INPUTTER,'_',2)
    AUTHI = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]   = CUST.NAME
    XX<1,1>[3,35]   = "������ ����� ��� �������"

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = '19002000100' : XX
***XX1<1,1>[59,15] = ACC.NO

    XX2<1,1>[45,15] = '��� ������ : '
**    XX2<1,1>[59,15] = CATEG
    XX2<1,1>[59,15] = "������ ����� ��� �������"

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX5<1,1>[3,15]  = '��� �����      : '
    XX5<1,1>[20,15] = CHQ.NO


    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = COMI

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = INP


    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = BR.DATA
*
*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":"REPORT.NZAMY.RE"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(45) :"����� ���"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*NEXT I
*END
*===============================================================
    RETURN
END
