* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-- CREATE BY NESSMA
    SUBROUTINE RTN.ACCT.STMT.MAIL
*    PROGRAM RTN.ACCT.STMT.MAIL

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ENQUIRY.REPORT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.STMT
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------------
*** SCB R15 UPG  20160728 - S
    FN.OFS.SOURCE = "F.OFS.SOURCE"
    F.OFS.SOURCE  = ''
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN  = ''
    CALL OPF(FN.OFS.IN,F.OFS.IN)

*    SCB.OFS.SOURCE = "TESTOFS"
*** SCB R15 UPG  20160728 - E

    SCB.APPL       = "ENQUIRY.REPORT"
    SCB.VERSION    = "OFS.MAIL"

    OPENSEQ "&SAVEDLISTS&" , "STMT.OUT.NEW" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"STMT.OUT.NEW"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "STMT.OUT.NEW" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE STMT.OUT.NEW CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create STMT.OUT.NEW File IN &SAVEDLISTS&'
        END
    END
*---------------------------------------------------------------
    GOSUB DEL.DATE.TIME
    GOSUB INITIAL
    GOSUB PROCESS
    RETURN
*--------------------------------------------------------------
DEL.DATE.TIME:
*-------------
    FN.ENQ.RPT = 'F.ENQUIRY.REPORT' ; F.ENQ.RPT = ''
    CALL OPF(FN.ENQ.RPT, F.ENQ.RPT)

    TMP.ID = "SBR.AC.STMT.TO.MAIL"
    CALL F.READ(FN.ENQ.RPT,TMP.ID,R.TMP,F.ENQ.RPT,ERR.TMP)
    IF NOT(ERR.TMP) THEN
        WS.DATE.NO = DCOUNT(R.TMP<ENQ.REP.DATE.TIME>,@VM)
        FOR NN = 2 TO WS.DATE.NO
            DEL R.TMP<ENQ.REP.DATE.TIME,NN>
            R.TMP<ENQ.REP.DATE.TIME> = ""
            CALL F.WRITE(FN.ENQ.RPT,TMP.ID,R.TMP)
            CALL JOURNAL.UPDATE(TMP.ID)
        NEXT NN
    END
    RETURN
*--------------------------------------------------------------
INITIAL:
*-------
    FN.CUS  = "FBNK.CUSTOMER"  ; F.CUS = ""
    CALL OPF(FN.CUS,F.CUS)

    FN.COMP = "F.COMPANY"      ; F.COMP = ""
    CALL OPF(FN.COMP,F.COMP)

    FN.TEMP = "F.SCB.CUSTOMER.STMT"  ; F.TEMP = ""
    CALL OPF(FN.TEMP,F.TEMP)

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT" ; F.CUS.ACC = ""
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    T.SEL   = '' ; SELECTED   = '' ; KEY.SEL   = ''
    T.SEL.C = '' ; SELECTED.C = '' ; KEY.SEL.C = ''

    STRR    = "INPUTT"
    SLASH   = "//"
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',LW.DAY)

    DD       = TODAY
    DAY.NUM  = ICONV(DD,'DW')
    TOD.DAY  = OCONV(DAY.NUM,'DW')

    DD2      = TODAY
    CALL CDT('',DD2,'-1W')
    DAY.NUM2 = ICONV(DD2,'DW')
    DAY.NUM3 = OCONV(DAY.NUM2,'DW')

    DAY.MONTH = LW.DAY
    CALL LAST.WDAY(DAY.MONTH)
    RETURN
*===================================================================
PROCESS:
*-------
    TTT1 = TODAY
    TTT2 = TTT1[1,6]:"01"
    CALL CDT('',TTT2,'-1W')

    IF TTT2 EQ DAY.MONTH THEN
        GOSUB SEL.MONTHLY
        GOSUB SEL.DAILY
    END ELSE
        IF DAY.NUM3 GT TOD.DAY OR TOD.DAY EQ 7 THEN
            GOSUB SEL.WEEKLY
        END ELSE
            GOSUB SEL.DAILY
        END
    END
    RETURN
************************************************************
SEL.DAILY:
*---------
    T.SEL = "SELECT ":FN.TEMP:" WITH FREQ EQ DAILY BY @ID"
*     T.SEL = "SELECT ":FN.TEMP:" WITH FREQ EQ DAILY AND SELECTION EQ 'CUSTOMER.NO' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.TEMP,KEY.LIST<I>,R.TEMP,F.TEMP,READ.ER)
*Line [ 169 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NN = DCOUNT(R.TEMP<CUS.STMT.ACCOUNT.NO>,@VM)
            CUST.ID = KEY.LIST<I>

            CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,ERR.CUS)
            CUS.EMAIL = R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
*Line [ 175 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CUS.NN    = DCOUNT(R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>,@SM)
            CUS.E     = ""
            FOR FF = 1 TO CUS.NN
                CUS.E = CUS.E :R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS,FF>
            NEXT FF
            CUS.EMAIL = CUS.E

            IF INDEX(CUS.EMAIL,"@",1) THEN
                FOR II = 1 TO NN
                    CUS.ID  = KEY.LIST<I>
                    SEL.REC = R.TEMP<CUS.STMT.ACCOUNT.NO><1,II>
                    SEL.E.DATE = LW.DAY
                    SEL.S.DATE = LW.DAY

                    IF R.TEMP<CUS.STMT.SELECTION> EQ 'CUSTOMER.NO' THEN
                        SEL.CUS = "CUSTOMER"
                        SEL.REC = KEY.LIST<I>
                        II = NN
                    END ELSE
                        SEL.CUS = "ACCOUNT.NO"
                    END
                    GOSUB GETDATA
                NEXT II
            END
        NEXT I
    END
    RETURN
************************************************************
SEL.WEEKLY:
*----------
    T.SEL = "SELECT ":FN.TEMP:" WITH FREQ EQ DAILY OR FREQ EQ WEEKLY BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.TEMP,KEY.LIST<I>,R.TEMP,F.TEMP,READ.ER)
*Line [ 211 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NN = DCOUNT(R.TEMP<CUS.STMT.ACCOUNT.NO>,@VM)

            CUST.ID   = KEY.LIST<I>
            CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,ERR.CUS)
            CUS.EMAIL = R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
*Line [ 217 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CUS.NN    = DCOUNT(R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>,@SM)
            CUS.E     = ""
            FOR FF = 1 TO CUS.NN
                CUS.E = CUS.E :R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS,FF>
            NEXT FF
            CUS.EMAIL = CUS.E

            IF INDEX(CUS.EMAIL,"@",1) THEN
                LW.DAYW = LW.DAY
                WS.D = '-':DAY.NUM3:'C'
                CALL CDT('',LW.DAYW,WS.D)

                IF R.TEMP<CUS.STMT.FREQ> EQ 'DAILY' THEN
                    SEL.S.DATE = LW.DAY
                    SEL.E.DATE = LW.DAY
                END ELSE
                    SEL.S.DATE = LW.DAYW
                    SEL.E.DATE = LW.DAY
                END

                FOR II = 1 TO NN
                    CUS.ID  = KEY.LIST<I>
                    SEL.REC = R.TEMP<CUS.STMT.ACCOUNT.NO><1,II>

                    IF R.TEMP<CUS.STMT.SELECTION> EQ 'CUSTOMER.NO' THEN
                        SEL.CUS = "CUSTOMER"
                        SEL.REC = KEY.LIST<I>
                        II = NN
                    END ELSE
                        SEL.CUS = "ACCOUNT.NO"
                    END
                    GOSUB GETDATA
                NEXT II
            END
        NEXT I
    END
    RETURN

************************************************************
SEL.MONTHLY:
*-----------
    LW.DAYW = LW.DAY
    WS.D = '-':DAY.NUM3:'C'
    CALL CDT('',LW.DAYW,WS.D)
    T.SEL = "SELECT ":FN.TEMP:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            SWEEK = 0
            CALL F.READ(FN.TEMP,KEY.LIST<I>,R.TEMP,F.TEMP,READ.ER)

            CUST.ID = KEY.LIST<I>
            CALL F.READ(FN.CUS,CUST.ID,R.CUS,F.CUS,ERR.CUS)
            CUS.EMAIL = R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>
*Line [ 272 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
            CUS.NN    = DCOUNT(R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>,@SM)
            CUS.E     = ""
            FOR FF = 1 TO CUS.NN
                CUS.E = CUS.E :R.CUS<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS,FF>
            NEXT FF
            CUS.EMAIL = CUS.E

            IF INDEX(CUS.EMAIL,"@",1) THEN
                LM.DAY.S = LW.DAY[1,6]:'01'

                IF R.TEMP<CUS.STMT.FREQ> EQ 'DAILY' THEN
                    SWEEK      = 1
                    SEL.S.DATE = LW.DAY
                    SEL.E.DATE = LW.DAY
                END ELSE

                    IF R.TEMP<CUS.STMT.FREQ> EQ 'WEEKLY' THEN
                        IF DAY.NUM3 GT TOD.DAY OR TOD.DAY EQ 7 THEN
                            SWEEK = 1
                            SEL.S.DATE = LW.DAYW
                            SEL.E.DATE = LW.DAY
                        END
                    END ELSE
                        SWEEK = 1
                        SEL.S.DATE = LM.DAY.S
                        SEL.E.DATE = DAY.MONTH
                    END
                END
*--------------- MULTI -----------------------
*Line [ 302 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                NN = DCOUNT(R.TEMP<CUS.STMT.ACCOUNT.NO>,@VM)
                FOR II = 1 TO NN
                    CUS.ID  = KEY.LIST<I>
                    SEL.REC = R.TEMP<CUS.STMT.ACCOUNT.NO><1,II>

                    IF R.TEMP<CUS.STMT.SELECTION> EQ 'CUSTOMER.NO' THEN
                        SEL.CUS = "CUSTOMER"
                        SEL.REC = KEY.LIST<I>
                        II = NN
                    END ELSE
                        SEL.CUS = "ACCOUNT.NO"
                    END

                    IF SWEEK = 1 THEN
                        GOSUB GETDATA
                    END
                NEXT II
            END
        NEXT I
    END
    RETURN
******************************************************************
GETDATA:
*-------
    LIST.DATE = SEL.S.DATE :" ": SEL.E.DATE

    IF SEL.CUS EQ "ACCOUNT.NO" THEN
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,SEL.REC,COMP)
        MY.STR = STRR:COMP[2]:SLASH:COMP

        OFS.MESSAGE.DATA  =  ""
        OFS.MESSAGE.DATA  = "LIST:1:1=":LIST.DATE
        OFS.MESSAGE.DATA := ",LIST:1:2=":SEL.REC

        SCB.OFS.HEADER  = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,":MY.STR:",":"RTN.AC.STMT.A4.SCBANK":","
        SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
        OFS.REC         = SCB.OFS.MESSAGE
        SCB.OFS.SOURCE  = "SCBOFFLINE"
        Y.UNIQUE.ID     = UNIQUEKEY()
        CHANGE "-" TO '' IN Y.UNIQUE.ID
        SCB.OFS.ID = "RTN.ACCT.STMT.MAIL-I-":Y.UNIQUE.ID
        SCB.OPT    = ''
        WRITESEQ OFS.REC TO BB ELSE
        END
        CALL OFS.POST.MESSAGE(OFS.REC,SCB.OFS.ID,SCB.OFS.SOURCE,SCB.OPT)
        CALL JOURNAL.UPDATE(SCB.OFS.ID)
        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
        END
        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
        WHILE.INDX = 0
        FN.QUE     = "F.OFS.MESSAGE.QUEUE" ; F.QUE = ""
        CALL OPF(FN.QUE, F.QUE)
        QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
        CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
        LOOP WHILE (QUE.SELECTED)
            WHILE.INDX++
            QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
            CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
            PRINT QUE.SELECTED : " -- * -- " : WHILE.INDX
        REPEAT
*==== V ====
        SCB.OFS.HEADER    = SCB.APPL : ","
        SCB.OFS.HEADER   := SCB.VERSION : "/V/PROCESS,":MY.STR:","
        SCB.OFS.HEADER   := "RTN.AC.STMT.A4.SCBANK":","
        SCB.OFS.MESSAGE   = SCB.OFS.HEADER : OFS.MESSAGE.DATA
        OFS.REC        = SCB.OFS.MESSAGE
        SCB.OFS.SOURCE = "SCBOFFLINE"
        Y.UNIQUE.ID    = UNIQUEKEY()
        CHANGE "-" TO '' IN Y.UNIQUE.ID
        SCB.OFS.ID     = "RTN.ACCT.STMT.MAIL-V-":Y.UNIQUE.ID
        SCB.OPT        = ''
        WRITESEQ OFS.REC TO BB ELSE
        END
        CALL OFS.POST.MESSAGE(OFS.REC,SCB.OFS.ID,SCB.OFS.SOURCE,SCB.OPT)
        CALL JOURNAL.UPDATE(SCB.OFS.ID)
        IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
            SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
        END
        CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
        WHILE.INDX = 0
        FN.QUE     = "F.OFS.MESSAGE.QUEUE" ; F.QUE = ""
        CALL OPF(FN.QUE, F.QUE)
        QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
        CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
        LOOP WHILE (QUE.SELECTED)
            WHILE.INDX++
            QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
            CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
            PRINT QUE.SELECTED : " -- * -- " : WHILE.INDX
        REPEAT
    END   ;* ENDING ACCOUNT CONDITION

    IF SEL.CUS EQ "CUSTOMER" THEN
        CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,SEL.REC,COMP)
        MY.STR = STRR:COMP[2]:SLASH:COMP
        CUSID  = SEL.REC

        CALL F.READ(FN.CUS.ACC,CUSID,R.CUS.ACC,F.CUS.ACC,ERR1)
        LOOP
            REMOVE ACC.NO FROM R.CUS.ACC SETTING POS
        WHILE ACC.NO:POS
            SCB.OFS.HEADER   = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,":MY.STR:",":"RTN.AC.STMT.A4.SCBANK":","

            OFS.MESSAGE.DATA  =  ""
            OFS.MESSAGE.DATA  = "LIST:1:1=":LIST.DATE
            OFS.MESSAGE.DATA := ",LIST:1:2=":ACC.NO

            CC.FLG = 0
            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CC)

            BEGIN CASE
            CASE CC EQ 1002
                CC.FLG = 1
            CASE CC EQ 1710
                CC.FLG = 1
            CASE CC EQ 1711
                CC.FLG = 1
            CASE CC EQ 9090
                CC.FLG = 1
            CASE CC EQ 1220
                CC.FLG = 1
            CASE CC EQ 1221
                CC.FLG = 1
            CASE CC EQ 1222
                CC.FLG = 1
            CASE CC EQ 1223
                CC.FLG = 1
            CASE CC EQ 1224
                CC.FLG = 1
            CASE CC EQ 1225
                CC.FLG = 1
            CASE CC EQ 1226
                CC.FLG = 1
            CASE CC EQ 1227
                CC.FLG = 1
            CASE CC EQ 1021
                CC.FLG = 1
            CASE CC EQ 1022
                CC.FLG = 1
            CASE CC EQ 1407
                CC.FLG = 1
            CASE CC EQ 1408
                CC.FLG = 1
            CASE CC EQ 1413
                CC.FLG = 1
            CASE CC EQ 12005
                CC.FLG = 1
            CASE CC[1,1] EQ '9'
                CC.FLG = 1
            CASE CC[1,2] EQ '30'
                CC.FLG = 1
            CASE OTHERWISE
                CC.FLG = 0
            END CASE

            IF CC.FLG NE 1 THEN
                SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
                OFS.REC = SCB.OFS.MESSAGE
                SCB.OFS.SOURCE = "SCBOFFLINE"
                Y.UNIQUE.ID    = UNIQUEKEY()
                CHANGE "-" TO '' IN Y.UNIQUE.ID
                SCB.OFS.ID     = "RTN.ACCT.STMT.MAIL-I-":Y.UNIQUE.ID
                SCB.OPT        = ''
                WRITESEQ OFS.REC TO BB ELSE
                END
                CALL OFS.POST.MESSAGE(OFS.REC,SCB.OFS.ID,SCB.OFS.SOURCE,SCB.OPT)
                CALL JOURNAL.UPDATE(SCB.OFS.ID)
                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
                END
                CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
                WHILE.INDX = 0
                FN.QUE     = "F.OFS.MESSAGE.QUEUE" ; F.QUE = ""
                CALL OPF(FN.QUE, F.QUE)
                QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
                CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
                LOOP WHILE (QUE.SELECTED)
                    WHILE.INDX++
                    QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
                    CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
                    PRINT QUE.SELECTED : " -- * -- " : WHILE.INDX
                REPEAT

                SCB.OFS.HEADER = SCB.APPL : ","
                SCB.OFS.HEADER := SCB.VERSION : "/V/PROCESS,":MY.STR:","
                SCB.OFS.HEADER := "RTN.AC.STMT.A4.SCBANK":","
                SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
                OFS.REC         = SCB.OFS.MESSAGE
                SCB.OFS.SOURCE  = "SCBOFFLINE"
                Y.UNIQUE.ID     = UNIQUEKEY()
                CHANGE "-" TO '' IN Y.UNIQUE.ID
                SCB.OFS.ID      = "RTN.ACCT.STMT.MAIL-V-":Y.UNIQUE.ID
                SCB.OPT         = ''
                WRITESEQ OFS.REC TO BB ELSE
                END
                CALL OFS.POST.MESSAGE(OFS.REC,SCB.OFS.ID,SCB.OFS.SOURCE,SCB.OPT)
                CALL JOURNAL.UPDATE(SCB.OFS.ID)

                IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
                    SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
                END
                CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')

                WHILE.INDX = 0
                FN.QUE     = "F.OFS.MESSAGE.QUEUE" ; F.QUE = ""
                CALL OPF(FN.QUE, F.QUE)
                QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
                CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
                LOOP WHILE (QUE.SELECTED)
                    WHILE.INDX++
                    QUE.SEL = "SELECT F.OFS.MESSAGE.QUEUE"
                    CALL EB.READLIST(QUE.SEL,QUE.LIST,"",QUE.SELECTED,ER.MSG.QUE)
                    PRINT QUE.SELECTED : " -- * -- " : WHILE.INDX
                REPEAT
            END     ;* END FLAG
        REPEAT
    END   ;* END CUSTOMER CONDIION
*--------------------------------------------------------------
    RETURN
END
