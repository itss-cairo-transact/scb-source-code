* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* CREATED BY RIHAM.YOUSSIF
*-----------------------------------------------------------------------------
    SUBROUTINE REP.EXP2
*    PROGRAM    REP.EXP2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.REP.CUS.EXP
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    WS.COMP = ID.COMPANY

    GOSUB INITIATE
    CALL  PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "TOTAL REPORT PRINTED  ":WS.PRINT.COUNT ; CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID ='SBR.REPORT.CUST.EXP'
    CALL PRINTER.ON(REPORT.ID,'')

    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    FN.REP.CUS  =  'F.REP.CUS.EXP' ; F.REP.CUS = '' ; R.REP.CUS = ''
    CALL OPF (FN.REP.CUS,F.REP.CUS)

    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
    CALL OPF( FN.CUSTOMER,F.CUSTOMER)

    WS.PRINT.COUNT = 0

    T.SEL  = "SELECT FBNK.CUSTOMER WITH COMPANY.BOOK EQ ":WS.COMP
    T.SEL := " AND SCB.POSTING EQ 29 AND NEW.SECTOR NE '' "
    T.SEL := " AND POSTING.RESTRICT NE 70 "
    T.SEL := " AND POSTING.RESTRICT NE 18 AND POSTING.RESTRICT NE 89 "
    T.SEL := " AND CREDIT.CODE LE 100 "
    T.SEL := " AND POSTING.RESTRICT LE 90 AND DRMNT.CODE NE 1 BY @ID "


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I  = 1 TO SELECTED
            ZZ = KEY.LIST<I>

            CALL F.READ(FN.CUSTOMER,KEY.LIST<I>,R.CUSTOMER,F.CUSTOMER,ETEXT)
            WS.UPD.DATE = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.UPDATE.DATE>
            WS.QLTY.RAT = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.QLTY.RATE>

            CALL F.READ(FN.REP.CUS,KEY.LIST<I>,R.REP.CUS,F.REP.CUS,ERR)
            IF ERR THEN
                R.REP.CUS<REP.CUS.RENEW.FLAG>   = '1'
                R.REP.CUS<REP.CUS.RENEW.DATE.P> = TODAY
                CALL F.WRITE(FN.REP.CUS,KEY.LIST<I>,R.REP.CUS)
                CALL JOURNAL.UPDATE(KEY.LIST<I>)
                GOSUB PRINT.HEAD
                WS.PRINT.COUNT ++
            END ELSE
                TT       = TODAY[1,4]
                RENEW.DT = R.REP.CUS<REP.CUS.RENEW.DATE.P>[1,4]

* IF R.REP.CUS<REP.CUS.RENEW.FLAG> # 1 THEN
                IF RENEW.DT LT TT THEN
                    R.REP.CUS<REP.CUS.RENEW.FLAG> = '1'
                    R.REP.CUS<REP.CUS.RENEW.DATE.P> = TODAY
                    CALL F.WRITE(FN.REP.CUS,KEY.LIST<I>,R.REP.CUS)
                    CALL JOURNAL.UPDATE(KEY.LIST<I>)
                    GOSUB PRINT.HEAD
                    WS.PRINT.COUNT ++
                END
            END
        NEXT I
    END

    RETURN
*----------
PRINT.HEAD:
*----------
    NAME      = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
    ADD1      = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS><1,1,1>
    ADD2      = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS><1,1,2>
    ADD3      = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS><1,1,3>

    EXP.DATE2  = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ID.EXPIRY.DATE>
    GOV        = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.GOVERNORATE>
    CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,GOV,GOV.NAME)
    REG        = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.REGION>
    CALL DBR('SCB.CUS.REGION':@FM:REG.DESCRIPTION,REG,REG.NAME)

*----------------------------------------------------------
    XXY = TODAY
    TOD = XXY[7,2]:'/':XXY[5,2]:"/":XXY[1,4]
    LAST = KEY.LIST<I>

    PR.HD  = REPORT.ID
    HEADING PR.HD
    PRINT SPACE(3):"��� ���� �����������"

*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>,BRANCH)
    BR.N       = R.CUSTOMER<EB.CUS.COMPANY.BOOK>[2]
    BR.N       = TRIM(BR.N,"0","L")
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BR.N,BRANCH)
    YY = FIELD(BRANCH,'.',2)
    PRINT SPACE(3) : YY
    PRINT SPACE(3) :"�����������":"  :  ":TOD
    PRINT SPACE(3) : STR('_',22)

    PRINT SPACE(3) : NAME
    PRINT
    PRINT SPACE(3) : LAST
    PRINT
    IF ADD1 NE "���� ��������� ������" AND ADD1 NE '' THEN
        PRINT SPACE(3):  ADD1
    END ELSE
        PRINT SPACE(3): '                               '
    END
    PRINT
    IF ADD2 NE "���� ��������� ������ " AND ADD2 NE '' THEN
        PRINT SPACE(3): ADD2
    END ELSE
        PRINT SPACE(3): '                                 '
    END
    PRINT
    IF REG NE '998'  THEN
        PRINT SPACE(3): REG.NAME
        PRINT SPACE(3): GOV.NAME
    END ELSE

        PRINT SPACE(3): '                                 '
        PRINT SPACE(3): '                                 '
    END

*NEXT I
*END
** WS.PRINT.COUNT ++
    RETURN
END
*==================================================================
