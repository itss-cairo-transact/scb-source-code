* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
    SUBROUTINE REPORT.FT.HISTORY

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.FT.HISTORY'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID     = COMI
    FN.LD     = 'FBNK.FUNDS.TRANSFER' ; F.LD = ''
    FN.LD.HIS = 'FBNK.FUNDS.TRANSFER$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)
    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
******CALL F.READ(FN.LD.HIS,COMI,R.LD.HIS,F.LD.HIS,E2)
    TEXT = COMI ; CALL REM
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E2)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD = TODAY
*------------------------------------------------------------------------
    ID = COMI
    CUS.ID = R.LD<FT.DEBIT.ACCT.NO>
    TEXT = CUS.ID ; CALL REM
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,CUS.ID,CUSNO)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>
    TEXT = CUST.NAME ; CALL REM
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,CUS.ID,CATEG)
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CATNAME)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
    IF CUS.ID[1,2] EQ 'PL' THEN
     TEXT = "2 : ":CUS.ID ; CALL REM
     CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CUS.ID[3,6],CUST.NAME)
     TEXT ="2 : " : CUST.NAME ; CALL REM
     CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CUS.ID[3,6],CUST.NAME1)
     CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CUS.ID[3,6],CATNAME)
    END
    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END

    AMOUNT = R.LD<FT.DEBIT.AMOUNT>

    BRANCH.ID = R.LD<FT.DEPT.CODE>

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

    DAT = R.LD<FT.DEBIT.VALUE.DATE>

    DAT2 = TODAY
    V.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    V.DATE2  = DAT2[7,2]:'/':DAT2[5,2]:"/":DAT2[1,4]
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.LD<FT.DEBIT.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    CHQ.NO = R.LD<FT.CHEQUE.NUMBER>

    TOTAL.AMT = R.LD<FT.AMOUNT.DEBITED>[4,15]
    IN.AMOUNT = TOTAL.AMT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.LD<FT.DEBIT.CURRENCY>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT1 = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    COM.AMT = R.LD<FT.COMMISSION.AMT>
    CHG.AMT = R.LD<FT.CHARGE.AMT>
    TOT.CHG = R.LD<FT.TOTAL.CHARGE.AMOUNT>
    BENCUS  = R.LD<FT.LOCAL.REF><1,FTLR.BENEFICIARY.CUS>

    INPUTT    = R.LD<FT.INPUTTER>
    INPUTTER  = FIELD(INPUTT,'_',2)
    AUTH1     = R.LD<FT.AUTHORISER>
    AUTH      = FIELD(AUTH1,'_',2)

    XX    = SPACE(132)  ; XX3   = SPACE(132) ; XX9   = SPACE(132)
    XX1   = SPACE(132)  ; XX4   = SPACE(132) ; XX11  = SPACE(132)
    XX2   = SPACE(132)  ; XX5   = SPACE(132) ; XX10  = SPACE(132)
    XX6   = SPACE(132)  ; XX7   = SPACE(132) ; XX8   = SPACE(132)
    XX12  = SPACE(132)  ; XX13  = SPACE(132) ; XX14  = SPACE(132)
    XX15  = SPACE(132)  ; XX16  = SPACE(132) ; XX17  = SPACE(132)
    XX18  = SPACE(132)  ; XX19  = SPACE(132) ; XX20  = SPACE(132)
    XX21  = SPACE(132)  ; XX22  = SPACE(132)

    XX<1,1>[3,15]    =  '��� ������ :'
    XX1<1,1>[3,15]   =  '-------------'
    XX2<1,1>[3,15]   =  CUS.ID

**  XX10<1,1>[3,15]     = '�����       : '
**  XX10<1,1>[20,35]    = CUST.NAME :' ':CUST.NAME1
* XX1<1,1>[20,35]   = CUST.NAME1

    XX<1,1>[30,15]   = '���� �����: '
    XX1<1,1>[30,15]  = '--------------'
    XX2<1,1>[30,15]  = AMOUNT

    XX<1,1>[55,15]   = '����� ���� : '
    XX1<1,1>[55,15]  ='--------------'
    XX2<1,1>[55,15]  = V.DATE

    XX3<1,1>[3,15]    = '��� ������:'
    XX4<1,1>[3,15]   = '-------------'
    XX5<1,1>[3,15]   = CATNAME

    XX3<1,1>[30,15]   = ' ������: '
    XX4<1,1>[30,15]  = '------------'
    XX5<1,1>[30,15]  =  CUR

    XX3<1,1>[55,15]   = ' ��� ������ : '
    XX4<1,1>[55,15]  = '------------'
    XX5<1,1>[55,15]  = CUST.NAME
    XX6<1,1>[55,15]  = CUST.NAME1
    XX7<1,1>[55,15]  = CUST.ADDRESS

    XX8<1,1>[3,15]   = '������ ������� :'
    XX9<1,1>[3,15]   = '-----------------'
    XX10<1,1>[3,15]   = OUT.AMT


    XX11<1,1>[3,15]   = ' ��� ����� :' : CHQ.NO
    XX11<1,1>[40,16]  = '������ �������� :' : TOTAL.AMT


    XX12<1,1>[3,15]  = '������ �������� �������: '
    XX13<1,1>[3,15]  = '------------------------'
    XX14<1,1>[3,15]  = OUT.AMT1

    XX22<1,1>[3,15]  = '��� �������� :'
    XX22<1,1>[18,15] = BENCUS

    XX15<1,1>[3,15]  = ' �������� ��������� :'
    XX16<1,1>[3,15]  = '-----------------------'
    XX17<1,1>[3,15]  = COM.AMT[4,10]
    XX18<1,1>[3,15]  = CHG.AMT[4,10]
    XX19<1,1>[3,15]  = '������ �������� ��������� : ': TOT.CHG[4,10]

    XX20<1,1>[3,15]  = "��� �������"
    XX20<1,1>[30,15] = "��� �������"
    XX20<1,1>[58,15] = "��� �������"

    XX21<1,1>[3,15]  = INPUTTER
    XX21<1,1>[30,15] = COMI
    XX21<1,1>[58,15] = AUTH
*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
**** PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"����� ��� / ����� ��� �����" : SPACE(5) :"������� : ":T.DAY
    PR.HD :="'L'":"------------------------------------------------------------------------------------------- "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
* PRINT STR(' ',82)
    PRINT XX3<1,1>
* PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT XX7<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT STR(' ',82)
    PRINT XX22<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX17<1,1>
***PRINT STR('-',82)
    PRINT XX18<1,1>
    PRINT STR('-',30)
    PRINT XX19<1,1>
    PRINT STR(' ',82)
    PRINT XX20<1,1>
    PRINT STR('-',82)
    PRINT XX21<1,1>
*===============================================================
    RETURN
END
