* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
*-----------------------------------------------------------------------------
* <Rating>-104</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.SAMPLE1.CUS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE.INPUT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
    CUR = ''
*------------------------------------------------------------------------
    FLG1 = R.NEW(DEPT.SAMP.AMT.HWALA)
    IF FLG1 NE '' THEN

        GOSUB INITIATE
        GOSUB PROCESS

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ���������" ; CALL REM
    END
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.SAMPLE1.CUS'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.LD    ='F.SCB.DEPT.SAMPLE1' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.INP   ='F.SCB.DEPT.SAMPLE.INPUT' ; F.INP = ''
    CALL OPF(FN.INP,F.INP)
*------------------------------------------------------------------------
    DEPT.NO   = R.NEW(DEPT.SAMP.DEPT.NO.HWALA)
    BRANCH.ID = R.NEW(DEPT.SAMP.BRANCH.NO.HWALA)
    SADA1     = R.NEW(DEPT.SAMP.NAMES1.HWALA)
    AMOUNT    = R.NEW(DEPT.SAMP.AMT.HWALA)
    BEN.CU    = R.NEW(DEPT.SAMP.REPLAY.DATE.TF3)
    IN.AMOUNT = AMOUNT
    TYPE.HW   = R.NEW(DEPT.SAMP.TYPE.AMT.HWALA)
    CUR.HW    = R.NEW(DEPT.SAMP.CURRENCY.HWALA)
 CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.HW,CUR)


    CUS.HW    = R.NEW(DEPT.SAMP.CUS.HWALA)
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    SUP       = R.NEW(DEPT.SAMP.SUP.HWALA)
    SADA2     = R.NEW(DEPT.SAMP.NAMES2.HWALA)
    SADA2.ACC = R.NEW(DEPT.SAMP.ACC.NAMES2.HWALA)
    PURPOS1     = R.NEW(DEPT.SAMP.PURPOSE.HWALA)<1,1>
    PURPOS2    = R.NEW(DEPT.SAMP.PURPOSE.HWALA)<1,2>
    PURPOS3    = R.NEW(DEPT.SAMP.PURPOSE.HWALA)<1,3>

    NOTES     = R.NEW(DEPT.SAMP.NOTES.HWALA)
    REQ.HW    = R.NEW(DEPT.SAMP.REQUEST.DATE.HWALA)
    REP.HW    = R.NEW(DEPT.SAMP.REPLAY.DATE.HWALA)
    REQ.STA   = R.NEW(DEPT.SAMP.REQ.STA.HWALA)
    NOT.MAR   = R.NEW(DEPT.SAMP.NOTES.HWALA.MAR)

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.NO,DEPT.NAME)

*    CUR.ID    = R.LD<LD.CURRENCY>
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

*    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    OUT.AMT = OUT.AMOUNT : ' ' : '�����'

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
    XX9  = SPACE(132)  ; XX10  = SPACE(132) ; XX12  = SPACE(132)
    XX13 = SPACE(132)  ; XX14  = SPACE(132) ; XX15  = SPACE(132)
    XX16 = SPACE(132)  ; XX17  = SPACE(132) ; XX18  = SPACE(132)

    XX<1,1>[3,15]    =  '���  :'
    XX<1,1>[20,15]   =  DEPT.NAME
    XX1<1,1>[15,15]  =  '���  : '
    XX1<1,1>[30,15]  =  BRANCH.ID
    XX2<1,1>[3,15]   =  '������ / '
    XX2<1,1>[30,15]  =  SADA1
    XX3<1,1>[3,30]   =  '�����  ' : AMOUNT
    XX4<1,1>[40,50]  =  '��� ����� : ' :  OUT.AMT
    XX5<1,1>[3,50]  =  '��� ����� :' : TYPE.HW
    XX6<1,1>[3,50]  =  '������ : ' :CUS.HW
    XX7<1,1>[3,50]  =  '������ : ' :CUR.HW
    XX18<1,1>[3,50]  =  '����� :  ' :BEN.CU
    XX8<1,1>[3,50]   =  '���� : ' :SUP
    XX9<1,1>[3,90]   =  ' ��� ���� ������ / ' : SADA2
    XX10<1,1>[3,40]   =  ' ���� ��� :  '  : SADA2.ACC
    XX11<1,1>[3,80]   =  ' ���� :  ' : PURPOS1 : " " : PURPOS2 : " " : PURPOS3
    XX12<1,1>[3,40]   =  '����� ������� ������ ����� �����'
    XX13<1,1>[3,40]  =  '������� : ':NOTES
    XX14<1,1>[3,40]  =  '����� ����� : ' : REQ.HW
    XX15<1,1>[3,40]  =  '����� ���� : '  : REP.HW
    XX16<1,1>[3,40]  =  '���� ����� : '  : REQ.STA
    XX17<1,1>[3,40]  =  '������� ������ :  ' : NOT.MAR

    ID = 'SCB.DEPT.SAMPLE1*':ID.NEW:'...'
    T.SEL = "SELECT F.SCB.DEPT.SAMPLE.INPUT WITH @ID LIKE ": ID :"  AND FUNCTION EQ 99"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "SEL :": SELECTED                        ; CALL REM
    IF SELECTED THEN
        FOR J = 1 TO SELECTED
            CALL F.READ(FN.INP,KEY.LIST<J>,R.INP,F.INP,ERR1)
            INP  = R.INP<SAMP.INPUT>
            AUTH = R.INP<SAMP.AUTH>
            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,INP.DEPT)
            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,AUTH,AUTH.DEPT)
            IF ( INP.DEPT EQ 99 AND AUTH.DEPT EQ 99 ) THEN
                CALL DBR('USER':@FM:EB.USE.USER.NAME,INP,INP.NAME)
                CALL DBR('USER':@FM:EB.USE.USER.NAME,AUTH,AUTH.NAME)
                XX18<1,1>[3,100]  =  '��� ������� : ' :ID.NEW:"":'���� :' :INP.NAME:'���� : ' : AUTH.NAME
            END
        NEXT J
    END


*-------------------------------------------------------------------
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD ="'L'":SPACE(1):" KEY1 "
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"��� ������ �������� "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX1<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT STR(' ',82)
    PRINT XX18<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX13<1,1>
    PRINT STR(' ',82)
    PRINT XX14<1,1>
    PRINT STR(' ',82)
    PRINT XX15<1,1>
    PRINT STR(' ',82)
    PRINT XX16<1,1>
    PRINT STR(' ',82)
    PRINT XX17<1,1>

    PRINT STR(' ',82)
    PRINT XX18<1,1>
**  PRINT STR('=',82)
*   END
*====================================================*
    RETURN
END
