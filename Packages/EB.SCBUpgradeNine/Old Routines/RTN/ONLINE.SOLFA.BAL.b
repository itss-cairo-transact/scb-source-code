* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE ONLINE.SOLFA.BAL
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.ID.TYPE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.PROFESSION
*****
    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "ONLINE.SOLFA.BAL" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"ONLINE.SOLFA.BAL"
        HUSH OFF
    END
    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "ONLINE.SOLFA.BAL" TO BB ELSE
        CREATE BB THEN
        END
        ELSE
            STOP 'Cannot create ONLINE.SOLFA.BAL File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END
*****

    EOF = ''

    FN.AC  = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''

    T.SEL  = "SELECT FBNK.ACCOUNT WITH ( CATEGORY EQ 1407 OR "
    T.SEL := " CATEGORY EQ 1408 OR CATEGORY EQ 1413 OR CATEGORY EQ 1445 OR CATEGORY EQ 1455 ) AND "
    T.SEL := " (ONLINE.ACTUAL.BAL LT 0 AND ONLINE.ACTUAL.BAL NE '' ) BY @ID  "
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL OPF( FN.AC,F.AC)

            CALL F.READ( FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)
            BANK.DAT.YEAR  = R.AC<AC.DATE.LAST.DR.BANK>[1,4]
            BANK.DAT.MONTH = R.AC<AC.DATE.LAST.DR.BANK>[5,2]
            BANK.DAT.DAY   = R.AC<AC.DATE.LAST.DR.BANK>[7,2]
            BANK.DAT       = BANK.DAT.DAY:BANK.DAT.MONTH:BANK.DAT.YEAR

*****CREATE NEW TEXT****
            BB.DATA  = KEY.LIST<I>:"|"
            BB.DATA := R.AC<AC.ONLINE.ACTUAL.BAL>:"|"
            BB.DATA := R.AC<AC.CATEGORY>:"|"
            BB.DATA := BANK.DAT:"|"
            BB.DATA := R.AC<AC.AMNT.LAST.DR.BANK>:"|"

*******
            WRITESEQ BB.DATA TO BB ELSE

                PRINT " ERROR WRITE FILE "
            END
************************
        NEXT I
    END
    CLOSESEQ BB
    TEXT = " TEXT COMPLETE " ; CALL REM
END
