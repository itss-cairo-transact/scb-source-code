* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************************NI7OOOOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
* EDIT BY NESSMA
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.TRADE.CR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.TRADE.CR'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    OUT.AMOUNT = ""
    FN.BR = 'F.SCB.WH.TRANS' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    ACC.NO        = 52218
    CUS           = R.NEW(SCB.WH.TRANS.CUSTOMER)
    CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
    CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>
    CUST.NAME3    = LOCAL<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS1 = LOCAL<1,CULR.ARABIC.ADDRESS>
    CUST.ADDRESS2 = LOCAL<1,CULR.ARABIC.ADDRESS>
    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)

    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END

    CATEG.ID  = R.NEW(SCB.WH.TRANS.CATEGORY)
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,ACC.NO,CATEG)

    AMOUNT    = R.NEW(SCB.WH.TRANS.TOTAL.COMM.CHRG)
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    CUR.ID    = R.NEW(SCB.WH.TRANS.CURRENCY)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    INPUTTER = R.NEW(SCB.WH.TRANS.INPUTTER)
    AUTH     = R.NEW(SCB.WH.TRANS.AUTHORISER)
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[8,35]    = CATEG
    XX1<1,1>[8,35]   = CATEG
    XX<1,1>[45,15]   = '������     : '
    XX<1,1>[59,15]   = AMOUNT

    XX1<1,1>[45,15]  = '��� ������ : '
    XX1<1,1>[59,15]  = 'PL52218'

    XX2<1,1>[45,15]  = '��� ������ : '
    XX2<1,1>[59,15]  = CATEG

    XX3<1,1>[45,15]  = '������     : '
    XX3<1,1>[59,15]  = CUR

    XX6<1,1>[1,15]   = '������'
    XX7<1,1>[1,15]   = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15]  = INP

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15]  = ID.NEW

    XX8<1,1>[3,35]   = '������ ������� : '
    XX8<1,1>[20,15]  = OUT.AMT
*-------------------------------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.WH.TRANS.RECORD.STATUS)='INA2' THEN
        IF R.NEW(SCB.WH.TRANS.WAIVE.COMMISSION) NE 'YES' THEN
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� �����"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX5<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR('-',82)
            PRINT XX7<1,1>
        END
    END
*===============================================================
    RETURN
END
