* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-14</Rating>
*-----------------------------------------------------------------------------
*--- COPY REPORT.CUS.ALL.NEW
*--- BY NESSMA 2014/12/08
    SUBROUTINE REPORT.CUS.EX.100

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "REPORT PRINTED" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.CUS.ALL.NEW'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    XX   = SPACE(132)  ; YY  = SPACE(132) ; ARR.SORT = SPACE(132)
    NUM.1 = 0    ; T.NUM.1 = 0
    NUM.2 = 0    ; T.NUM.2 = 0
    NUM.TOT = 0  ; T.NUM.TOT = 0

    FN.CU ='FBNK.CUSTOMER' ; F.CU = ''
    FN.CU.HIS='FBNK.CUSTOMER$HIS' ; F.CU.HIS = ''
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.CU.HIS,F.CU.HIS)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD = TODAY

    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)

    FOR I = 1 TO SELECTED3
        ZZ = KEY.LIST3<I>
        T.SEL  = "SELECT ":FN.CU: " WITH  POSTING.RESTRICT LT 90 "
        T.SEL := " AND SECTOR NE 5010 AND SECTOR NE 5020"
        T.SEL := " AND CREDIT.CODE EQ ''"
        T.SEL := " AND COMPANY.BOOK EQ " : ZZ
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR NN = 1 TO SELECTED
                CALL F.READ(FN.CU,KEY.LIST<NN>,R.CU,F.CU,E2)
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,ZZ,BRANCH)
                YYBRN   = BRANCH
                NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
                IF NEW.SEC EQ '4650' THEN
                    NUM.1++
                END ELSE
                    NUM.2++
                END
            NEXT NN
        END

        NUM.TOT  = NUM.1 + NUM.2
        XX<1,1>[3,15]    = YYBRN
        XX<1,1>[35,15]   = NUM.1
        XX<1,1>[55,15]   = NUM.2
        XX<1,1>[85,15]   = NUM.TOT
*        PRINT XX<1,1>

        ARR.SORT<1,I>[3,15]  = YYBRN
        ARR.SORT<1,I>[35,15] = NUM.1
        ARR.SORT<1,I>[55,15] = NUM.2
        ARR.SORT<1,I>[85,15] = NUM.TOT

        YY<1,1>[1,15]    = "------------------------"
*        PRINT YY<1,1>
        XX<1,1> = ""
        YY<1,1> = ""

        T.NUM.1   += NUM.1
        T.NUM.2   += NUM.2
        T.NUM.TOT += NUM.TOT

        NUM.1   = 0 ; NUM.2 = 0 ; TOT.NUM = 0
    NEXT I
**--------------------------
    GOSUB SORT.ARRAY.PRINT
**--------------------------
    XX<1,1> = ""
    XX<1,1>[3,15]   = "���������� :"
    XX<1,1>[35,15]  = T.NUM.1
    XX<1,1>[55,15]  = T.NUM.2
    XX<1,1>[85,15]  = T.NUM.TOT
    PRINT ""
    PRINT ""
    PRINT ""
    PRINT "========================"
    PRINT XX<1,1>
    PRINT ""
    PRINT ""
    PRINT ""
    PRINT SPACE(50): " ********** ����� ������� ********** "

    RETURN
*-------------------------------------------------------------------
SORT.ARRAY.PRINT:
*----------------
*-- SORT BY BUBBLE SORT ALGORITHM
    ARR    = SPACE(132)
    ARR    = ARR.SORT
    INDX   = SELECTED3

    FOR II = 1 TO INDX
        INDX2 = INDX - II

        FOR JJ = 1 TO INDX2
            IF ARR<1,JJ>[85,15] GT ARR<1,JJ+1>[85,15] THEN
                TEMP1               = ARR<1,JJ>[3,15]
                ARR<1,JJ>[3,15]     = ARR<1,JJ+1>[3,15]
                ARR<1,JJ+1>[3,15]   = TEMP1

                TEMP2               = ARR<1,JJ>[35,15]
                ARR<1,JJ>[35,15]    = ARR<1,JJ+1>[35,15]
                ARR<1,JJ+1>[35,15]  = TEMP2

                TEMP3               = ARR<1,JJ>[55,15]
                ARR<1,JJ>[55,15]    = ARR<1,JJ+1>[55,15]
                ARR<1,JJ+1>[55,15]  = TEMP3

                TEMP4               = ARR<1,JJ>[85,15]
                ARR<1,JJ>[85,15]    = ARR<1,JJ+1>[85,15]
                ARR<1,JJ+1>[85,15]  = TEMP4
            END
        NEXT JJ
    NEXT II

*----- PRINT SORTED ARRAY
    FOR KK = INDX TO 1 STEP -1
        PRINT ARR<1,KK>

        YY<1,1>[1,15]    = "------------------------"
        PRINT YY<1,1>
        YY<1,1> = ""
    NEXT KK

    RETURN
*-------------------------------------------------------------------
PRINT.HEAD:
*----------
    WW = SPACE(132)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY : SPACE(85) : "REPORT.CUS.EX.100"
    PR.HD :="'L'":SPACE(50):"���� ������� ������� �������� �� ������"
    PR.HD :="'L'":SPACE(50): "�� ���� : ������ - ���� - ����"
    PR.HD :="'L'":SPACE(50):STR('_',50)
    WW<1,1>[3,15]   = "�����"
    WW<1,1>[30,15]   = "�����"
    WW<1,1>[50,15]   = "�����"
    WW<1,1>[80,15]  = "������"
    PR.HD :="'L'":WW<1,1>
    PR.HD :="'L'":STR('_',100)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*===============================================================
    RETURN
END
