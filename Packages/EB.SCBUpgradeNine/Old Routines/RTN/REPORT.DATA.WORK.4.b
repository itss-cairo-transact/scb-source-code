* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-176</Rating>
*-----------------------------------------------------------------------------
************* WAEL *************
*TO CREATE LG LETTER
    SUBROUTINE REPORT.DATA.WORK.4

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.OUR.BANK
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.DATA.WORK.4'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN


*==============================================================
*===============================================================
PROCESS:
    IF ID.NEW = '' THEN
        FN.LD = 'F.SCB.LG.OUR.BANK' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)
        FN.LD.HIS = 'F.SCB.LG.OUR.BANK' ; F.LD.HIS = ''
        CALL OPF(FN.LD.HIS,F.LD.HIS)

        YTEXT = "Enter the L/G. No. : "
        CALL TXTINP(YTEXT, 8, 22, "12", "A")
        CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    END ELSE
        FN.LD = 'F.SCB.LG.OUR.BANK$NAU' ; F.LD = ''
        CALL OPF(FN.LD,F.LD)

        CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)
    END
    K=''
    IDN       = ID.NEW
    IDN2      = IDN:";":1
    CALL F.READ(FN.LD.HIS,IDN2,R.LD.HIS,F.LD.HIS,E1)
    IDAMT     = R.LD<LG.OUR.LG.AMT>
    CUSNAME   = R.LD<LG.OUR.LG.CUS.NAME>
    EXPDATE   = R.LD<LG.OUR.LG.EXPIRY.DATE>
    EXPDATE2  = R.LD.HIS<LG.OUR.LG.EXPIRY.DATE>
    CUR       = R.LD<LG.OUR.LG.CUR>
    BANKBR    = R.LD<LG.OUR.LG.EX.NO>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CRR)
    DATY    = TODAY
    XX      = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]


**CALL DBR('SCB.LG.CHARGE':@FM:SCB.LG.CH.OPERATION.CODE,MYID,OPER.CODE)
*    CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,CUST.AC,AC.OFICER)
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.AC,COMP.BOOK)
    CUS.BR = COMP.BOOK[2]
    AC.OFICER = TRIM(CUS.BR, "0" , "L")


    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,AC.OFICER,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)


    RETURN
*===============================================================
    PRINT SPACE(5):"������� : "   : XX
    PRINT
    PRINT SPACE(5):"������/ ������� ������ ������ ��������"
    PRINT
** PRINT SPACE(5): "��� : " :BANKBR
** PRINT
    PRINT SPACE(20):"������� : � � ��� " : IDN
    PRINT
    PRINT SPACE(5): "����� : " : IDAMT
    PRINT
    PRINT SPACE(5): "���� ���� ���� ,"
    PRINT
    PRINT SPACE(5): "�������� ��� ������ ��� :  " : IDN : "������" :":":EXPDATE
    PRINT
**** PRINT SPACE(5):"����� ��� ���� ����� ��� ��� ���� ������ ������ ������ �������"
    PRINT SPACE(5):"����� ��� ���� ����� ��� ��� ���� ������ ������ ������ �������"
    PRINT
    PRINT SPACE(5):"���� ��� ����� "
    PRINT
    PRINT SPACE(5): "����� ������� ���������"
    PRINT ; PRINT ; PRINT
    PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ;PRINT
    PRINT ; PRINT ; PRINT
    PRINT SPACE(45):"��� ���� ����� �������"
**TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

    RETURN
*************************************
    TEXT = "FIRST "  ; CALL REM
END
*==================================================================
