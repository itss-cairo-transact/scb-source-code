* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>1040</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.CONV.PROCESS
*25-Sep-2016: OFS.CONV.PROCESS is Obsolete in R15 so don't proceed further
    RETURN
*
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LOCKING

*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS

    GOSUB INITIALISE
    GOSUB MAIN.PROCESS

    RETURN
*
**
*
INITIALISE:
*---------
    PRINT "INTO INITIALISE"
    PROCESSED.LIST = ""

    FN.OFS.SOURCE = "F.OFS.SOURCE"
    FV.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,FV.OFS.SOURCE)

    FN.USER = "F.USER"
    FV.USER = ""
    CALL OPF(FN.USER,FV.USER)

    FN.USER.SIGN.ON.NAME = "F.USER.SIGN.ON.NAME"
    FV.USER.SIGN.ON.NAME = ""
    CALL OPF(FN.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME)

    FN.LOCKING = "F.LOCKING"
    FV.LOCKING = ""
    CALL OPF(FN.LOCKING,FV.LOCKING)
    OFS.SOURCE.ID = "OFS.CONV.PROCESS"
    CALL F.READ(FN.OFS.SOURCE,OFS.SOURCE.ID,R.OFS.SOURCE,FV.OFS.SOURCE,"")
    OFS$SOURCE.ID = "OFS.CONV.PROCESS"
    OFS$SOURCE.REC = R.OFS.SOURCE

    FN.OFS.IN = R.OFS.SOURCE<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.OUT = "../bnk.data/OFS/OFS.OUT"
    FN.OFS.LOG = "../bnk.data/OFS/OFS.LOG"
    FN.OFS.BK = R.OFS.SOURCE<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>

*Line [ 74 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPEN FN.OFS.IN TO F.OFS.IN ELSE NULL
*Line [ 76 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPEN FN.OFS.OUT TO F.OFS.OUT ELSE NULL
*Line [ 78 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPEN FN.OFS.LOG TO F.OFS.LOG ELSE NULL
*Line [ 80 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    OPEN FN.OFS.BK TO F.OFS.BK ELSE NULL

    READ R.LOG FROM F.OFS.LOG,OFS.SOURCE.ID ELSE
        WRITE "START OFS" TO F.OFS.LOG,OFS.SOURCE.ID ON ERROR
            PRINT "ERROR IN WRITING TO THE LOG FILE"
        END
    END

    IF TRANSQUERY() THEN
*Line [ 90 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        TRANSEND ELSE NULL
    END

    OPENSEQ FN.OFS.LOG,OFS.SOURCE.ID TO OFS$LOG.FILE.NAME ELSE
        OFS$SOURCE.REC<OFS.SRC.LOG.DETAIL.LEVEL> = 'NONE'
    END

    TSA.ID = APPLICATION
    PORT.NO = TSA.ID[18,99]

    IF PORT.NO THEN
        SEL.CMD = "SELECT ":FN.OFS.IN:" WITH @ID LIKE ":PORT.NO:"..."
    END ELSE
        SEL.CMD = "SELECT ":FN.OFS.IN:" WITH @ID UNLIKE T..."
    END

    RETURN
*
**
*
MAIN.PROCESS:
*-----------

    CALL EB.READLIST(SEL.CMD,OFS.IN.LIST,"","","")
*    PRINT OFS.IN.LIST
    LOOP

        REMOVE OFS.IN.ID FROM OFS.IN.LIST SETTING MORE.IDS
    WHILE OFS.IN.ID:MORE.IDS
        LOCATE OFS.IN.ID IN PROCESSED.LIST<1> SETTING FND.ID THEN
            CONTINUE
        END ELSE
            INS OFS.IN.ID BEFORE PROCESSED.LIST<FND.ID>
        END
        READ R.OFS.REC FROM F.OFS.IN,OFS.IN.ID ELSE R.OFS.REC = ""
*Line [ 126 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        WRITE R.OFS.REC TO F.OFS.BK,OFS.IN.ID ON ERROR NULL
        R.OFS.REC.OUT = ""
*Line [ 129 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        NO.OF.LINES = DCOUNT(R.OFS.REC,@FM)
        FOR LINE.IND = 1 TO NO.OF.LINES
            OFS.MESSAGE = R.OFS.REC<LINE.IND>
            IF OFS.MESSAGE THEN
                PRINT "OFS.MESSAGE IN ":OFS.MESSAGE
****  BAKRY 20131029
                TXN.ID = ""
                NEW.OPERATOR = FIELD(FIELD(OFS.MESSAGE,",",3,1),"/",1,1)
                PRINT "OPERATOR = ":OPERATOR
                PRINT "NEW.OPERATOR = ":NEW.OPERATOR
                IF NEW.OPERATOR THEN
                    SAVE.OPERATOR = OPERATOR
                    OPERATOR = NEW.OPERATOR
                    SAVE.R.USER = R.USER
                    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
                    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")
                END
***-------------
                OFS.STR.TEMP = OFS.MESSAGE
                OFS.STR.TEMP = EREPLACE(OFS.MESSAGE, '/PROCESS', '/VALIDATE', 1, 1)

*                CALL OFS.GLOBUS.MANAGER(OFS.SOURCE.ID, OFS.STR.TEMP)
*                CHKVAL = FIELD (OFS.STR.TEMP,'/', 3)
*                IF CHKVAL [1, 1] = 1 THEN
*                    CALL OFS.POST.MESSAGE (OFS.MESSAGE,'', OFS.SOURCE.ID,'')
*                END ELSE
*                    T24.ERR = 'SET'     ;* Include the failure processes
*                END

                SCB.OFS.SOURCE = "SCBOFSONLINE"
                CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE,OFS.STR.TEMP)

****----- END----
                IF NOT(TRANSQUERY()) THEN
*Line [ 164 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    TRANSTART ELSE NULL
                END
*********************************************************************
***********    IN THIS LINE CHANGE OFS.REQUEST.MANAGER PROGRAM TO OFS.GLOBUS.MANAGER ****************
                CALL LOG.WRITE("","","",1)

                IF TRANSQUERY() THEN
*Line [ 172 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    TRANSEND ELSE NULL
                END
                IF NEW.OPERATOR THEN
                    OPERATOR = SAVE.OPERATOR
                    R.USER = SAVE.R.USER
                END
****  BAKRY 20131029
****---- START-----
                PRINT "BAKRY OFS.MESSAGE OUT ":OFS.STR.TEMP ;*OFS.MESSAGE
                PRINT "TXN ID = ":TXN.ID
                R.OFS.REC.OUT<-1> = OFS.STR.TEMP  ;*OFS.MESSAGE
****--- END-----
            END
*=============================================================================
**        IF T24.ERR = 'SET' THEN
*            R.OFS.REC.OUT = OFS.STR.TEMP
*KKK = "BAKRY  ===  ":OFS.STR.TEMP
*PRINT KKK
* *       END
*=============================================================================
        NEXT LINE.IND

        IF NOT(TRANSQUERY()) THEN
*Line [ 196 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            TRANSTART ELSE NULL
        END
        DELETE F.OFS.IN,OFS.IN.ID ON ERROR
            PRINT "ERROR IN DELETING THE OFS IN "
        END
*=============================================================================
        WRITE R.OFS.REC.OUT ON F.OFS.OUT,OFS.IN.ID ON ERROR
            PRINT "ERROR IN WRITING TO OFS OUT"
        END
        IF TRANSQUERY() THEN
*Line [ 207 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            TRANSEND ELSE NULL
        END

    REPEAT

*        SLEEP SLEEP.TIME

*        GOSUB FIND.TIME

*        IF STOP.PROCESS THEN BREAK

*    REPEAT

    RETURN
*
**
*
FIND.TIME:
*--------

    STOP.PROCESS = 0
    READ R.LOCKING FROM FV.LOCKING,"OFS.CONV.PROCESS" ELSE
        R.LOCKING<EB.LOK.CONTENT> = 1900
        R.LOCKING<EB.LOK.REMARK>  = 10
    END
    STOP.TIME  = R.LOCKING<EB.LOK.CONTENT>
    SLEEP.TIME = R.LOCKING<EB.LOK.REMARK>
    THE.TIME = OCONV(TIME(),"MT")
    THE.TIME = THE.TIME[1,2]:THE.TIME[4,2]
    IF THE.TIME >= STOP.TIME THEN
        STOP.PROCESS = 1
    END
    RETURN
END
