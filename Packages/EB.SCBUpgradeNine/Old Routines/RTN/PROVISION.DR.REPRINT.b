* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***********************************NI7OOOOOOOOOOOOOOO***************
    SUBROUTINE PROVISION.DR.REPRINT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH

    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='PROVISION.DR.REPRINT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    ID = COMI
    ID.LC = COMI[1,12]
    TEXT = "COMI :" = COMI ; CALL REM

    FN.LC='FBNK.LETTER.OF.CREDIT' ;  F.LC = '' ; R.LC = ''
    CALL OPF(FN.LC,F.LC)
    CALL F.READ(FN.LC,ID.LC,R.LC,F.LC,E2)
    LC.AMOUNT    = R.LC<TF.LC.LC.AMOUNT>
    ACC.LC       = R.LC<TF.LC.CREDIT.PROVIS.ACC>
    CUR          = ACC.LC[9,2]

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    ID.LC = COMI[1,12]
    CALL F.READ(FN.LC,ID.LC,R.LC,F.LC,E2)
    LC.AMOUNT     = R.LC<TF.LC.LC.AMOUNT>
    ACC.LC        = R.LC<TF.LC.CREDIT.PROVIS.ACC>
    CUR           = ACC.LC[9,2]

    PERCENT        = R.DR<TF.DR.LOCAL.REF><1,DRLR.DRAEING.PERCENT>
*    CHARGE.AMOUNT = R.NEW(TF.DR.CHARGE.AMOUNT)
    CHARGE.AMOUNT  = R.DR<TF.DR.DOCUMENT.AMOUNT>
    PROV.AMT       = R.DR<TF.DR.PROV.REL.LC.CCY>
    ACC.DRW       = R.DR<TF.DR.PROV.COVER.ACCT>
    CUR1          = ACC.DRW[9,2]
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.LC,CUS.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.LC,CUR.ID)
    CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.LC,CAT.ID)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
*    CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.LC,ACC.BR)

    CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.LC,AC.COMP)
    BRN.ID  = AC.COMP[2]
    ACC.BR = TRIM(BRN.ID,"0","L")

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR22)
    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CAT.ID,CATEG)
    IF PERCENT EQ '' THEN
        AMOUNT    = PROV.AMT
        IN.AMOUNT = AMOUNT
        ACC.LC    = R.LC<TF.LC.CREDIT.PROVIS.ACC>
        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.LC,CUR.ID)
        TEXT = "CUR 2 : " : CUR.ID ; CALL REM
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR22)
        TEXT = "NAME : " : CUR22 ; CALL REM
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT    = OUT.AMOUNT : ' ' : CUR22 : ' ' : '�����'
    END
    IF PERCENT NE '' THEN
        ACC.LC    = R.LC<TF.LC.CREDIT.PROVIS.ACC>
        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.LC,CUR.ID)
        TEXT = "CUR 2 : " : CUR.ID ; CALL REM
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR22)
        TEXT = "NAME : " : CUR22 ; CALL REM
        AMOUNT    = CHARGE.AMOUNT*PERCENT
        IN.AMOUNT = AMOUNT
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT    = OUT.AMOUNT : ' ' : CUR22 : ' ' : '�����'
    END
    INPUTTER = R.DR<TF.DR.INPUTTER>
    INP = FIELD(INPUTTER,'_',2)
    AUTH = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10 = SPACE(132)  ; XX11  = SPACE(132)

    XX1<1,1>[3,35]   = CUST.NAME
    XX2<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.LC

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR22

* XX4<1,1>[45,15] = '����� ����� ������ ����� : '
* XX4<1,1>[80,15] = V.DATE

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = COMI

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = INP

    XX10<1,1>[3,35] = '��������       : '
*-------------------------------------------
    XX8<1,1>[3,35]  = '������ ������� : '
    XX9<1,1>[3,35] = OUT.AMT
* XX9<1,1>[45,35] = "����� ����� ����� �� " : NO.OF.DAYS

*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":"������ ������ ":' - ':"PROVISION DR"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX11<1,1>
    PRINT XX10<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
