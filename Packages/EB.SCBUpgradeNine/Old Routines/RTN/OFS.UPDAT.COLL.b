* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.UPDAT.COLL

*   PROGRAM OFS.UPDAT.COLL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS


    FN.OFS.SOURCE   ="F.OFS.SOURCE"
    F.OFS.SOURCE    = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.OUT       = "../bnk.data/OFS/OFS.OUT"
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    ="COLLATERAL"
    IDDD             ="EG0010001"
    CALL DBR('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,IDDD,N.W.DAY)
    COMP             = "EG0010001"
    COM.CODE         = COMP[8,2]
    OFS.USER.INFO    = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    OFS.OPTION       = "DEP"
    COMMA            = ","


    KEY.LIST  = ""
    SELECTED  = ""
    ER.MSG    = ""
    APP.LD.ID = ""
    T.SEL = "SSELECT ":FN.OFS.OUT:" WITH @ID LIKE ...":TODAY:"-REN-A... AND @ID UNLIKE ...COL... "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FN.OFS.OUT= FN.OFS.OUT ; F.OFS.OUT = '' ; R.OFS.OUT = ''
    CALL OPF( FN.OFS.OUT,F.OFS.OUT)

    FN.LD.LOANS.AND.DEPOSITS = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LD.LOANS.AND.DEPOSITS  = ''
    R.LD.LOANS.AND.DEPOSITS  = ''
    CALL OPF( FN.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS)

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.OFS.OUT,KEY.LIST<I>, R.OFS.OUT,F.OFS.OUT, ETEXT)

            CHECK.VALED  = FIELD(R.OFS.OUT,",",1)
            CHECK.VALED1 = FIELD(CHECK.VALED,"/",2)
            LD.NEW.ID    = FIELD(CHECK.VALED,"/",1)

            IF CHECK.VALED1 = 1 THEN

                CALL F.READ(FN.LD.LOANS.AND.DEPOSITS,LD.NEW.ID,R.LD.LOANS.AND.DEPOSITS,F.LD.LOANS.AND.DEPOSITS, ETEXT)
                APP.LD.ID        = R.LD.LOANS.AND.DEPOSITS<LD.LOCAL.REF,LDLR.COLLATERAL.ID>

                IF APP.LD.ID NE '' THEN

                    OFS.MESSAGE.DATA =""
                    OFS.MESSAGE.DATA :=  "APPLICATION.ID=":LD.NEW.ID
                    F.PATH = FN.OFS.IN
                    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTION:COMMA:OFS.USER.INFO:COMMA:APP.LD.ID:COMMA:OFS.MESSAGE.DATA
                    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.IN, KEY.LIST<I>:"-COL-":TODAY :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
***** SCB R15 UPG 20160628 - S
                    GOSUB OPM.PROCESS
***** SCB R15 UPG 20160628 - E
                END
            END
        NEXT I

***** SCB R15 UPG 20160628 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
    END

    RETURN

***** SCB R15 UPG 20160628 - S
*--
OPM.PROCESS:
*--

*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*
    RETURN
***** SCB R15 UPG 20160628 - E

END
