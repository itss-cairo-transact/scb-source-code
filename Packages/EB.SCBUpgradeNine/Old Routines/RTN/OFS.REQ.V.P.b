* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE OFS.REQ.V.P

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL

*---------------------------------------


    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "REQUEST"
    SCB.VERSION = ""


    NK = ''
    NK<1,1>='TEM001//EG0010001'  ; NK<1,2>='TEM001//EG0010002'   ; NK<1,3>='TEM001//EG0010003'
    NK<1,4>='TEM001//EG0010004'  ; NK<1,5>='TEM001//EG0010005'   ; NK<1,6>='TEM001//EG0010006'
    NK<1,7>='TEM001//EG0010007'  ; NK<1,8>='TEM001//EG0010009'   ; NK<1,9>='TEM001//EG0010010'
    NK<1,10>='TEM001//EG0010011' ; NK<1,11>='TEM001//EG0010012'  ; NK<1,12>='TEM001//EG0010013'
    NK<1,13>='TEM001//EG0010014' ; NK<1,14>='TEM001//EG0010015'  ; NK<1,15>='TEM001//EG0010020'
    NK<1,16>='TEM001//EG0010021' ; NK<1,17>='TEM001//EG0010022'  ; NK<1,18>='TEM001//EG0010023'
    NK<1,19>='TEM001//EG0010030' ; NK<1,20>='TEM001//EG0010031'  ; NK<1,21>='TEM001//EG0010032'
    NK<1,22>='TEM001//EG0010035' ; NK<1,23>='TEM001//EG0010040'  ; NK<1,24>='TEM001//EG0010050'
    NK<1,25>='TEM001//EG0010051'
    NK<1,26>='TEM001//EG0010060' ; NK<1,27>='TEM001//EG0010070'  ; NK<1,28>='TEM001//EG0010080'
    NK<1,29>='TEM001//EG0010081' ; NK<1,30>='TEM001//EG0010090'  ; NK<1,31>='TEM001//EG0010099'
    NK<1,32>='TEM001//EG0010016'

    OPENSEQ "&SAVEDLISTS&" , "REQUEST.OUT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"REQUEST.OUT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "REQUEST.OUT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE REQUEST.OUT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create REQUEST.OUT File IN &SAVEDLISTS&'
        END
    END


    FOR I = 1 TO 32
        SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/V/PROCESS,":NK<1,I>:",":"GENLED":","
** SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/V/PROCESS,":NK<1,I>:",":"SCBPLFIN":","

        PRINT NK<1,I>
        OFS.MESSAGE.DATA  =  ""

        SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA

* SCB R15 UPG 20160717 - S
*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E


        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END
    NEXT I
    RETURN
************************************************************
END
