* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>78</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.CHQ.RETURN



*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.REGISTER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

***** SCB R15 UPG 20160628 - S

*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")

*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')

***** SCB R15 UPG 20160628 - E

    RETURN
*------------------------------
INITIALISE:

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "CHEQUE.REGISTER"
    OFS.OPTIONS      = "SCB"
***OFS.USER.INFO    = "/"

    COMP = C$ID.COMPANY
    COM.CODE = COMP[8,2]
    OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP

    OFS.TRANS.ID     = "SCB":".":ID.NEW
    OFS.MESSAGE.DATA = ""

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    COMMA = ","

************************************************************
*Line [ 98 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    TEMP.COUNT.SCB=DCOUNT(R.NEW(SCB.CHQR.FIRST.CHEQUE.NO),@VM)
    FN.CHQ.REG = 'F.CHEQUE.REGISTER' ; F.CHQ.REG = '' ; R.CHQ.REG = '' ; ETEXT = ''

    CALL OPF(FN.CHQ.REG,F.CHQ.REG)
    CALL F.READ(FN.CHQ.REG,OFS.TRANS.ID,R.CHQ.REG,F.CHQ.REG,ETEXT)
    FOR I = 1  TO TEMP.COUNT.SCB
        IF ETEXT THEN
*TEXT = "NO"; CALL REM
*        OFS.MESSAGE.DATA :=   "RETURNED.CHQS:1:1=":R.NEW(SCB.CHQR.FIRST.CHEQUE.NO)<1,I>:COMMA
            OFS.MESSAGE.DATA :=   "RETURNED.CHQS:":I:":1=":R.NEW(SCB.CHQR.FIRST.CHEQUE.NO)<1,I>:COMMA
        END ELSE
            OFS.MESSAGE.DATA :=   "RETURNED.CHQS:":I:":1=":R.NEW(SCB.CHQR.FIRST.CHEQUE.NO)<1,I>:COMMA
*        TEMP.COUNT=DCOUNT(R.CHQ.REG<CHEQUE.REG.RETURNED.CHQS>,VM)
*       OFS.MESSAGE.DATA =   "RETURNED.CHQS:":TEMP.COUNT+1:":1=":R.NEW(SCB.CHQR.FIRST.CHEQUE.NO)
        END
    NEXT I
*============================
    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, "T":TNO:".":ID.NEW:"_":R.NEW(SCB.CHQR.FIRST.CHEQUE.NO) :ON ERROR  TEXT = " ERROR ";CALL REM ; STOP


    RETURN
END
