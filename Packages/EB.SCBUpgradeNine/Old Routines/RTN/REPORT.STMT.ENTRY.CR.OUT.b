* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NI7OOOOOOOOOOOOOO***************
*-----------------------------------------------------------------------------
* <Rating>444</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.STMT.ENTRY.CR.OUT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.STMT.ENTRY.CR.OUT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    YTEXT = "Enter date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    NDATE = COMI

    FN.LD='FBNK.CATEG.ENTRY' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD = TODAY
    COMP      = C$ID.COMPANY
*------------------------------------------------------------------------
*   T.SEL = "SELECT ":FN.LD: " WITH FIN.MAT.DATE EQ ":TD: " AND (RENEW.IND NE 'YES' OR RENEW.METHOD EQ 1) "
    T.SEL = "SELECT ":FN.LD: " WITH CRF.PROD.CAT EQ 21096 AND TRANSACTION.CODE EQ 875 AND INPUTTER LIKE ...INPUTT... AND BOOKING.DATE EQ ": NDATE : " AND COMPANY.CODE EQ ": COMP
*   T.SEL = "SELECT ":FN.LD.HIS: " WITH @ID=LD0832500145;2 AND FIN.MAT.DATE EQ '20081229' AND RENEW.IND NE 'YES' AND CATEGORY GE 21001 AND CATEGORY LE 21010"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SEL = ':SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E2)
            ID     = KEY.LIST<I>
            CALL DBR ('CATEG.ENTRY':@FM:AC.CAT.PL.CATEGORY,ID,CATID)
****ACCT   = R.LD<AC.CAT.ACCOUNT.NUMBER>
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATID,CATEG1)
            CUS.ID = R.LD<AC.CAT.CUSTOMER.ID>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)

            CUST.NAME    = LOCAL.REF1<1,CULR.ARABIC.NAME>
            CUST.NAME1   = LOCAL.REF1<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
            CUST.ADDRESS1= LOCAL.REF1<1,CULR.ARABIC.ADDRESS,2>
            IF CUST.ADDRESS EQ  " " THEN
                CUST.ADDRESS = "���� ��������� ������"
            END
            ACC       = R.LD<AC.CAT.ACCOUNT.NUMBER>
            AMOUNT    = R.LD<AC.CAT.AMOUNT.LCY>
            AMOUNTFCY = R.LD<AC.CAT.AMOUNT.FCY>
            INTER     = CATEG1
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,INTER,CATEG)
            BRANCH.ID = R.LD<AC.CAT.ACCOUNT.OFFICER>
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
            DAT = R.LD<AC.CAT.VALUE.DATE>
            DAT2= R.LD<LD.VALUE.DATE>
            V.DATE  = NDATE[7,2]:'/':NDATE[5,2]:"/":NDATE[1,4]
            V.DATE2 = DAT2[7,2]:'/':DAT2[5,2]:"/":DAT2[1,4]

            REF = R.LD<AC.CAT.OUR.REFERENCE>
            CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.LOCAL.REF,REF,LOCAL)
            OLDNO=LOCAL<1,LDLR.OLD.NO>
            TYPE =LOCAL<1,LDLR.PRODUCT.TYPE>
            IF TYPE = 'ADVANCE' THEN
                TYPE1='�����'
            END
            IF TYPE = 'BIDBOND' THEN
                TYPE1='�������'
            END

            IF TYPE = 'FINAL' THEN
                TYPE1='����� '
            END
            ENDDATE  = LOCAL<1,LDLR.END.COMM.DATE>
            ENDDATE1 = ENDDATE[7,2]:'/':ENDDATE[5,2]:"/":ENDDATE[1,4]

            IN.AMOUNT = AMOUNT
            IN.AMOUNT1= AMOUNTFCY

            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT1,OUT.AMOUNT1,78,NO.OF.LINES,ER.MSG)
            CUR.ID    = R.LD<AC.STE.CURRENCY>
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            TEXT = CUR.ID ; CALL REM

            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
*****            OUT.AMT1 = OUT.AMOUNT1 : ' ' : CUR : ' ' : '�����'
            XX   = SPACE(132)  ; XX3  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132) ; XX13 = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132) ; XX9 = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
            XX<1,1>[3,15]    =  '��� ������  :'
            XX<1,1>[45,15]   = REF
            XX10<1,1>[3,15]  = '������   : '
            XX10<1,1>[20,35] = CUST.NAME :' ':CUST.NAME1
            XX1<1,1>[3,15]   = '�������      : '
            XX1<1,1>[20,35]  = CUST.ADDRESS
            XX3<1,1>[3,15]   = '����� �������� ���� ���� ������ ��� ������ ������� �����'
            XX4<1,1>[3,15]   = '��� ���� ������ ��� ' : OLDNO :'(':TYPE1:')':'������ �����'
            XX8<1,1>[3,15]   = '������'
            XX8<1,1>[50,15]  = '������'
*******UPDATED BY MAHMOUD 2/3/2010 *******************
****            XX5<1,1>[3,15]   = AMOUNT :' ':CUR
            IF CUR.ID EQ 'EGP' THEN
                XX5<1,1>[3,15]   = AMOUNT :' ':CUR
            END ELSE
                XX5<1,1>[3,15]   = AMOUNTFCY :' ':CUR
            END
******************************************************
            XX5<1,1>[45,15]  = '������� �� ������ �������� �� ':ENDDATE1
            XX6<1,1>[1,15]   = '������ ������� : ':OUT.AMT
            XX7<1,1>[65,15]  = '�� ��� ���� ������'
            XX9<1,1>[3,15]   = '����� ���� ':' ':V.DATE

            YYBRN    = FIELD(BRANCH,'.',2)
            DATY     = TODAY
            T.DAY    = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            NEWDATE  = NDATE[7,2]:'/':NDATE[5,2]:'/':NDATE[1,4]

            PR.HD  ="'L'":SPACE(1):"��� ���� ������":SPACE(40):CATID:SPACE(10):"OMOLA"
            PR.HD :="'L'":"������� : ":NEWDATE:SPACE(35):CATEG1
            PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"����� �����"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX10<1,1>
            PRINT XX1<1,1>
            PRINT STR(' ',82)
            PRINT STR(' ',82)
            PRINT STR(' ',82)
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT STR(' ',82)
            PRINT STR(' ',82)
            PRINT XX8<1,1>
            PRINT STR(' ',82)
            PRINT XX5<1,1>
            PRINT STR('=',82)
            PRINT XX6<1,1>
            PRINT STR(' ',82)
            PRINT XX9<1,1>
            PRINT STR('=',82)
            PRINT XX7<1,1>
        NEXT I
    END
*===============================================================
    RETURN
END
