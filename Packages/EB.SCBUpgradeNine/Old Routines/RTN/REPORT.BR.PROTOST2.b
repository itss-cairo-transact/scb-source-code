* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NI7OOOOOOOOOOOOOOO**********************
*-----------------------------------------------------------------------------
* <Rating>383</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.BR.PROTOST2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.BR.RETURN'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)
*--------------------------------------
* YTEXT = "Enter the BT No. : "
* CALL TXTINP(YTEXT, 8, 22, "14", "A")
* CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
* BT.ID = COMI

* T.SEL = "SELECT F.SCB.BT.BATCH WITH @ID EQ ": COMI
* CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* IF SELECTED THEN
* CALL F.READ(FN.BATCH,KEY.LIST,R.BATCH,F.BATCH,E1)
*------------------------------------------------------------------------
    BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)
    TEXT = BR.ID ; CALL REM
*Line [ 87 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
    TEXT ="COUNT= ": DD; CALL REM

    FOR I = 1 TO DD
        BR.ID1 = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
***UPDATE BY NESSMA IF BR.ID1 NE ...
        CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,ERR1)
        BR.COM = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.COMM.CCY.AMT>
* IF BR.COM NE 0 THEN
        TEXT = "IN"  ; CALL REM
        RES = R.NEW(SCB.BT.RETURN.REASON)
        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> EQ 500 THEN
            TEXT = BR.ID1 ; CALL REM
*------------------------------------------------------------------------
            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
            DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>

            FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                CUST.ADDRESS1 = "���� ��������� ������"
            END ELSE
                CUST.ADDRESS1 = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            END


            SCB.CO     = R.BR<EB.BILL.REG.CO.CODE>
            START.DATE = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.STATUS.DATE>
            ST.DATE    = START.DATE[7,2]:'/':START.DATE[5,2]:"/":START.DATE[1,4]
            BANK.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.INIT.BANK.BENEF>
            BRANCH.NO  = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.INIT.BRANCH.BEN>
            TEXT = BANK.NO ; CALL REM
            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>

            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
* OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            AMT.NO     = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT = AMT.NO
            IF IN.AMOUNT = '' THEN
                IN.AMOUNT = 0
            END
            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            DR.NAME    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.DR.NAME>
            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.NO,BNAME)
            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BRANCH.NO,BRNAME)

            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>

            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT  = TODAY
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
*YYBRN  = FIELD(BRANCH,'.',2)
            YYBRN  = BRANCH

            INPUTTER = R.NEW(SCB.BT.INPUTTER)
            AUTH     = R.NEW(SCB.BT.AUTHORISER)
            INP = FIELD(INPUTTER,'_',2)
            AUTHI =FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX11  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)  ; XX10  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)  ; XX12  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)  ; XX13  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

*============
            XX1<1,1>[3,15]    = '������  : ' : BNAME :"-": BRNAME
            XX2<1,1>[3,15]    = '����� ��� ���� ����� ��� ��� ���������� ������� ���� ��� ����� '
            XX3<1,1>[3,15]    = '��� ��� ������� ��� ����� / ���� ���� ������ '
            XX4<1,1>[3,15]    = '�����  : '  : @ID
            XX5<1,1>[3,15]    = '������� ���� : ���� ' :  DD : '������� + ��� ������� �� ������� ���� ���� '
            XX11<1,1>[3,15]   = '������ :   ' : AMT.NO
            XX12<1,1>[3,15]   =  OUT.AMT
            XX13<1,1>[3,15]   = '������ ������� ��������� '
            XX9<1,1>[30,15]   = '������� ����� ���� �������� �'
            XX10<1,1>[50,15]  = '�� ��� ���� ������'

*XX9<1,1>[20,15] = BR.DATA
*-------------------------------------------------------------------
*CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
*YYBRN  = FIELD(BRANCH,'.',2)
            YYBRN  = BRANCH

            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
* PR.HD  ="'L'":SPACE(1):"��� ���� ������"
* PR.HD :="'L'":"������� : ":T.DAY
* PR.HD :="'L'":"����� : ":YYBRN
            PR.HD  ="'L'":"SCB.PROTOSTO2"
            PR.HD :="'L'":"��� : ": YYBRN
            PR.HD :="'L'": "������ �� : " : ST.DATE
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT XX5<1,1>
            PRINT XX11<1,1>
            PRINT XX12<1,1>
            PRINT XX13<1,1>
            PRINT XX9<1,1>
            PRINT XX10<1,1>
*    END
        END
    NEXT I
*===============================================================
    RETURN
END
