* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>1070</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* Create By Noha Hamed
* Dated 11/5/2015
*-----------------------------------------------------------------------------
    SUBROUTINE PACS008.PENSION.OFS
*   PROGRAM    PACS008.PENSION.OFS

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ALTERNATE.ACCOUNT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS002
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RTN.FEES.SMS.OFS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.EXCP.RATE
*----------------------------------------------

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN
*----------------------------------------------------------------------*
INITIALISE:
*----------
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "PENSION.PACS008"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.PAC2 = 'F.SCB.ACH.PACS002'   ; F.PAC2 = ''
    CALL OPF(FN.PAC2 , F.PAC2)

    FN.PAC8 = 'F.SCB.ACH.PACS008'   ; F.PAC8 = ''
    CALL OPF(FN.PAC8 , F.PAC8)

    FN.PAC8.NAU = 'F.SCB.ACH.PACS008$NAU'   ; F.PAC8.NAU = ''
    CALL OPF(FN.PAC8.NAU , F.PAC8.NAU)

    FN.AC = 'FBNK.ACCOUNT'   ; F.AC = ''
    CALL OPF(FN.AC , F.AC)

    FN.IBAN = 'FBNK.ALTERNATE.ACCOUNT' ; F.IBAN = '' ; R.IBAN = ''
    CALL OPF(FN.IBAN,F.IBAN)

    FN.CU = 'FBNK.CUSTOMER'   ; F.CU = ''
    CALL OPF(FN.CU , F.CU)

    FN.EXCP = 'F.SCB.ACH.EXCP.RATE' ; F.EXCP = ''
    CALL OPF(FN.EXCP , F.EXCP)

    YTEXT = "Enter ACH Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    TT = COMI

    YTEXT = "Enter Value Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    SET.DATE = COMI

    YTEXT = "Enter FILE TYPE : "
    CALL TXTINP(YTEXT, 8, 22, "4", "A")
    FIL.TYP = COMI

    QQ     = TIMEDATE()
    KK     = QQ[1,8]
    T.DATE.TIME = TT:KK[1,2]:KK[4,2]:KK[7,2]
    Y.SEL = "SELECT F.SCB.ACH.PACS002 WITH @ID LIKE ...":TT:"... BY-DSND"
    CALL EB.READLIST(Y.SEL,PAC.LIST, "", PAC.SEL, PAC.ER)

    IF PAC.LIST THEN
        CALL F.READ(FN.PAC2,PAC.LIST<1>, R.PAC2, F.PAC2, PAC2.ER)
        SERIAL.NO = R.PAC2<PACS2.ADD.STATUS.REASON.2>
    END
    ELSE
        SERIAL.NO = 0
    END

    SERIAL.NO.FINAL = SERIAL.NO + 1
    BATCH.SERIAL    = FMT(SERIAL.NO.FINAL,"R%7")

    BATCH.ID        = 'SUCAEGCX-':T.DATE.TIME:'000-':BATCH.SERIAL
    CREATION.DATE   = TT[1,4]:'-':TT[5,2]:'-':TT[7,2]:'T'::QQ[1,8]:'.00000'
    I = 0

    RETURN
*---------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA  = ","
    T.DATE = SET.DATE[1,4]:'-':SET.DATE[5,2]:'-':SET.DATE[7,2]
    T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ":TT:"... AND REQ.SETTLEMENT.DATE EQ ":T.DATE:" AND TRN.DATE EQ '' AND PACS.TYPE EQ 1 AND PURPOSE NE 'COLL' AND CHARGER.BEARER EQ '":FIL.TYP:"'"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

******CHECK WHETHER TO CONTINUE OR NOT*************************
    TEXT = "Selected Pensions =":SELECTED ; CALL REM
*Line [ 146 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    CALL TXTINP('Do you want to contine ?', 8, 23, '1.1', @FM:'Y_N')
    ID = COMI[1,1]

    IF ID EQ 'N' THEN
        RETURN
    END
    R.PAC2 = ''
    IF SELECTED THEN
        FOR J = 1 TO SELECTED

            CALL F.READ( FN.PAC8,KEY.LIST<J>, R.PAC8, F.PAC8, E.PAC8)
            ACCT.NO      = R.PAC8<PACS8.CREDITOR.ACCOUNT.NO>
            REF          = KEY.LIST<J>

            CALL F.READ( FN.PAC8.NAU,REF, R.PAC8.NAU, F.PAC8.NAU, E.PAC8.NAU)

            REF1         = FIL.TYP
            CUST.NO      = ACCT.NO[1,8]

            IF CUST.NO[1,1] EQ '0' THEN
                CUST.NO = CUST.NO[2,7]
            END
*----------------------START CHECK IF IBAN ** BAKRY ** 2020/09/17* ---------------------------------------
            IF ACCT.NO[1,2] = 'EG' AND LEN(ACCT.NO) EQ 29 THEN
                CALL F.READ(FN.IBAN,ACCT.NO,R.IBAN,F.IBAN,ER.IBAN)
                IF NOT(ER.IBAN) THEN
                    ACCT.NO = R.IBAN<AAC.GLOBUS.ACCT.NUMBER>
                END
            END
*----------------------END CHECK IF IBAN ** BAKRY ** 2020/09/17* ---------------------------------------
            CALL F.READ( FN.AC,ACCT.NO, R.AC, F.AC, E.AC)
            CALL F.READ( FN.CU,CUST.NO, R.CU, F.CU, E.CU)
            COMP = R.AC<AC.CO.CODE>
***********UPDATED BY RIHAM YOUSSIF 30/6/2020****
            CATEG = R.AC<AC.CATEGORY>

******** DON'T CREATE OFS FOR THESE CAT
            IF CATEG EQ 9043 OR CATEG EQ 9002 OR CATEG EQ 9005 OR CATEG EQ 9007 OR CATEG EQ 9009 OR CATEG EQ 9003 OR CATEG EQ 9006 OR CATEG EQ 9090 OR CATEG EQ 9091 OR CATEG EQ 9048 OR CATEG EQ 9030 OR CATEG EQ 9031 OR CATEG EQ 9032 OR CATEG EQ 9037 OR CATEG EQ 9015 OR CATEG EQ 9014 OR CATEG EQ 9016 OR CATEG EQ 1205 OR CATEG EQ 1206 OR CATEG EQ 1207 OR CATEG EQ 1208 THEN
                CAT.FLAG = 'Y'
            END
            ELSE
                CAT.FLAG = 'N'
            END
***********************************

            POST.REST = R.CU<EB.CUS.POSTING.RESTRICT>
            CURR = R.AC<AC.CURRENCY>

************ACCOUNT NOT FOUND**********************************
******UPDATE BY RIHAM YOUSSIF 30/6/2020 BY ADDING CONDITION  OF CATEG***********
            IF (E.AC OR POST.REST EQ 70 OR POST.REST GE 90 OR CURR NE 'EGP' OR R.PAC8.NAU NE '' OR CATEG EQ '1002' OR CAT.FLAG EQ 'Y') THEN
*J = J + 1
                IF J EQ 1 THEN
                    TRANSACTION.ID  = 'TXSUCAEGCX-':T.DATE.TIME:'000-':BATCH.SERIAL
                END
                ELSE
                    TRANSACTION.ID  = 'TXSUCAEGCX-':T.DATE.TIME:'000-':TRN.SERIAL
                END

                O.BATCH.ID      = R.PAC8<PACS8.BATCH.ID>
                BATCH.IND       = "pacs.008.001.01"
                GROUP.STATUS    = "PART"
                STATUS.REASON   = ''
                IF E.AC THEN
                    STATUS.REASON.D = "000003"
                END
                IF POST.REST EQ 70 THEN
                    STATUS.REASON.D = "000002"
                END

                O.TRANS.ID      = R.PAC8<PACS8.TRANSACTION.ID>
                SERIAL.NO.FINAL = SERIAL.NO.FINAL + 1
                TRN.SERIAL      = FMT(SERIAL.NO.FINAL,"R%6")

*********INSERT RECORD IN PACS002 FOR REJECTED AND DEAD PEOPLE RECORDS*******************
                R.PAC2<PACS2.BATCH.ID>              =  BATCH.ID
                R.PAC2<PACS2.CREATION.DATE.TIME>    =  CREATION.DATE
                R.PAC2<PACS2.ORIGIN.BATCH.ID>       =  O.BATCH.ID
                R.PAC2<PACS2.ORIGIN.BATCH.INDICATOR> = BATCH.IND
                R.PAC2<PACS2.GROUP.STATUS>          = GROUP.STATUS
                R.PAC2<PACS2.STATUS.REASON>         = STATUS.REASON
                R.PAC2<PACS2.STATUS.REASON.D>       = STATUS.REASON.D
                R.PAC2<PACS2.STATUS.IDENT>          = TRANSACTION.ID
                R.PAC2<PACS2.STATUS.REASON.INFO2>   = TT
                CALL F.WRITE(FN.PAC2,REF,R.PAC2)
                CALL JOURNAL.UPDATE(REF)


            END
            ELSE
**---------------------------------------------------------------------**
*                   ***** ACCOUNT FOUND - OFS RECORD *****                       *
**---------------------------------------------------------------------**

                CREDIT.ACCT  = R.PAC8<PACS8.CREDITOR.ACCOUNT.NO>
                AMT          = R.PAC8<PACS8.AMOUNT>
                CURR         = 'EGP'
*DEBIT.ACCT   = "9943330010508001"
                DEBIT.ACCT   = "9943330010509301"
                COMM.AMT     = 'EGP20.00'

                BEGIN CASE
                CASE FIL.TYP EQ 'SSBE'
                    TXN.TYPE     = "IT01"
                CASE FIL.TYP EQ 'CASH'
                    TXN.TYPE     = "IT01"
                CASE FIL.TYP EQ 'SUPP'
                    TXN.TYPE     = "IT01"
                CASE FIL.TYP EQ 'SALA'
                    TXN.TYPE     = "IT01"
                CASE FIL.TYP EQ 'CBET'
                    TXN.TYPE     = "IT01"
                    COMM.AMT     = 'EGP2.00'
                CASE FIL.TYP EQ 'PENS' OR FIL.TYP EQ 'PENG'
                    TXN.TYPE     = "AC14"
                    DEBIT.ACCT   = "EGP1130100010099"
                    IF AMT GT 1500 THEN
                        COMM.AMT = 'EGP5.00'
                    END
                    ELSE
                        COMM.AMT = 'EGP3.00'
                    END
                CASE OTHERWISE
                    TXN.TYPE     = "IT01"
                END CASE

*                IF AMT GT 1500 THEN
*                    COMM.AMT = 'EGP5.00'
*                END
*                ELSE
*                    COMM.AMT = 'EGP3.00'
*                END
****************UPDATED BY NOHA HAMED 21-3-2018****************
****************GET EXCEPTION COMM*****************************

                CALL F.READ(FN.EXCP,CREDIT.ACCT, R.EXCP, F.EXCP, EXCP.ER)
                IF R.EXCP THEN
                    FLAT.AMT   = R.EXCP<ACH.EXCP.EXCP.FLAT.RATE>
                    EXCP.RATE  = R.EXCP<ACH.EXCP.EXCP.RATE>
                    MAX.AMT    = R.EXCP<ACH.EXCP.EXCP.MAX.AMT>
                    MIN.AMT    = R.EXCP<ACH.EXCP.EXCP.MIN.AMT>
                    START.DATE = R.EXCP<ACH.EXCP.EXCP.START.DATE>
                    EXPIRE.DATE = R.EXCP<ACH.EXCP.EXCP.EXPIRE.DATE>

                    IF (SET.DATE GE START.DATE AND SET.DATE LE EXPIRE.DATE) THEN
                        IF (FLAT.AMT EQ '' OR FLAT.AMT EQ 0) AND (EXCP.RATE EQ '' OR EXCP.RATE EQ 0) THEN
                            COMM.AMT = 'EGP0.00'
                        END
                        ELSE
                            IF FLAT.AMT GT 0 THEN
                                COMM.AMT = 'EGP':DROUND(FLAT.AMT,2)
                            END
                            ELSE
                                IF EXCP.RATE GT 0 THEN
                                    COMM.AMT = DROUND(AMT * EXCP.RATE/100,2)
                                    IF COMM.AMT LT MIN.AMT THEN
                                        COMM.AMT = 'EGP':DROUND(MIN.AMT,2)
                                    END
                                    IF COMM.AMT GT MAX.AMT THEN
                                        COMM.AMT = 'EGP':DROUND(MAX.AMT,2)
                                    END
                                END
                            END
                        END
                    END
                END
*---------------------------------------
                IF AMT LE 20 THEN
                    COMM.AMT = "EGP0.00"
                END
*---------------------------------------
                CUS.SECTOR = ""
                CUS.ID = CREDIT.ACCT[1,8] + 0
                CALL DBR('CUSTOMER':@FM:EB.CUS.SECTOR,CUS.ID,CUS.SECTOR)
                IF CUS.SECTOR EQ 1100 OR CUS.SECTOR EQ 1200 OR CUS.SECTOR EQ 1300 OR CUS.SECTOR EQ 1400 OR CUS.SECTOR EQ 2000 THEN
                    COMM.AMT = "EGP0.00"
                END
*============== UPDATE TILL 15/9/2020 =================
                COMM.AMT = "EGP0.00"
*******************************************************

                COMPP = COMP[8,2]
                IF COMPP[1,1] EQ '0' THEN
                    COMPP = COMPP[2,1]
                END

                OFS.USER.INFO     = "INPUTT01//":COMP
                OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":TXN.TYPE:COMMA
                OFS.MESSAGE.DATA := "DEBIT.AMOUNT=":AMT:COMMA
                OFS.MESSAGE.DATA := "DEBIT.CURRENCY=":CURR:COMMA
                OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":CURR:COMMA
                OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=":COMPP:COMMA
                OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":DEBIT.ACCT:COMMA
                OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":REF:COMMA
                OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CREDIT.ACCT:COMMA
                OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":REF1:COMMA
                OFS.MESSAGE.DATA := "ORDERING.BANK=SCB":COMMA
                OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":SET.DATE:COMMA
                OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":SET.DATE:COMMA
                OFS.MESSAGE.DATA := "CO.CODE=":COMP:COMMA

                IF TXN.TYPE EQ 'AC14' THEN
                    OFS.MESSAGE.DATA := "COMMISSION.CODE=CREDIT LESS CHARGES":COMMA
                    OFS.MESSAGE.DATA := "COMMISSION.TYPE=PENSIONS":COMMA
                    OFS.MESSAGE.DATA := "COMMISSION.AMT=":COMM.AMT:COMMA
                END ELSE
                    OFS.MESSAGE.DATA := "CHARGE.CODE=CREDIT LESS CHARGES":COMMA
                    OFS.MESSAGE.DATA := "CHARGE.TYPE=FTINWCHR":COMMA
                    OFS.MESSAGE.DATA := "CHARGE.AMT=":COMM.AMT:COMMA
                END

                F.PATH  = FN.OFS.IN
                OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                OFS.ID  = "ACH":REF

                OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
                OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP

************UPDATE PACS008 TO BE JUST TAKEN*******************
                R.PAC8<PACS8.TRN.DATE>              = TODAY
                CALL F.WRITE(FN.PAC8,REF,R.PAC8)
                CALL JOURNAL.UPDATE(REF)
            END

        NEXT J
    END

    RETURN
*-----------------------------------------------------------
END
