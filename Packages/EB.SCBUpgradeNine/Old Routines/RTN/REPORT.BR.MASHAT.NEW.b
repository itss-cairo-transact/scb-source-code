* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>491</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.BR.MASHAT.NEW



*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER

*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER

*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER

*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS

*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER

*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY

*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE

*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION



*------------------------------------------------------------------------

    GOSUB INITIATE

    GOSUB PROCESS



    CALL PRINTER.OFF

    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    TEXT  = "��� ����� ������� �������" ; CALL REM

    RETURN

*========================================================================

INITIATE:

    REPORT.ID='REPORT.BR.MASHAT.NEW'

    CALL PRINTER.ON(REPORT.ID,'')

    RETURN

*========================================================================

PROCESS:

*---------------------

    FN.BATCH='F.INF.MULTI.TXN';F.BATCH=''

    CALL OPF(FN.BATCH,F.BATCH)



    FN.BR = 'F.INF.MULTI.TXN' ; F.BR = ''  ; R.BR = ''

    CALL OPF(FN.BR,F.BR)



    BT.ID = ID.NEW





* BT.ID = COMI

* FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''

* CALL OPF(FN.BR,F.BR)

* YTEXT = "Enter the BT No. : "

* CALL TXTINP(YTEXT, 8, 22, "14", "A")

    CALL F.READ(FN.BR,BT.ID,R.BR,F.BR,E1)



*------------------------------------------------------------------------

    BR.ACC = R.NEW(INF.MLT.ACCOUNT.NUMBER)

    IF BR.ACC[1,3] EQ 999 THEN

*Line [ 137 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DD = DCOUNT(BR.ACC,@VM)

        I = 0

        FOR J = 1 TO DD

            BR.ACCOUNT   = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>

***            BR.INSURANCE = R.NEW(SCB.RET.INSURANCE.NO)<1,J>

            BR.AMOUNT    = R.NEW(INF.MLT.AMOUNT.LCY)<1,J>

*------------------------------------------------------------------------

            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,BR.ACCOUNT,CUSNO)

            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)

            CUST.NAME     = LOCAL.REF<1,CULR.ARABIC.NAME>

            CUST.NAME.2   = LOCAL.REF<1,CULR.ARABIC.NAME.2>

            CUST.ADDRESS  = LOCAL.REF<1,CULR.ARABIC.ADDRESS>

            CUST.ADDRESS1 = LOCAL.REF<1,CULR.GOVERNORATE>

            CUST.ADDRESS2 = LOCAL.REF<1,CULR.REGION>

            CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)

            CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)

*     CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,BR.ACCOUNT,BRANCH.ID)
            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
            BRN.ID  = AC.COMP[2]
            BRANCH.ID = TRIM(BRN.ID,"0","L")

            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

            CATEG.ID  = BR.ACCOUNT[11,4]

            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

            IN.AMOUNT = BR.AMOUNT

            CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID    = BR.ACCOUNT[8,2]

            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT       = TODAY

            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

            INPUTTER = R.NEW(EB.BILL.REG.INPUTTER)

            AUTH     = R.NEW(EB.BILL.REG.AUTHORISER)

            INP      = FIELD(INPUTTER,'_',2)

            AUTHI    = FIELD(AUTH,'_',2)



            XX   = SPACE(132)  ; XX3  = SPACE(132)

            XX1  = SPACE(132)  ; XX4  = SPACE(132)

            XX2  = SPACE(132)  ; XX5  = SPACE(132)

            XX6  = SPACE(132)  ; XX7  = SPACE(132)

            XX8  = SPACE(132)  ; XX9  = SPACE(132)



            I ++



            XX<1,I>[3,35]    = CUST.NAME

            XX1<1,I>[3,35]   = CUST.NAME.2

            XX2<1,I>[3,35]   = CUST.ADDRESS

            XX3<1,I>[3,35]   = CUST.ADDRESS1:' ':CUST.ADDRESS2

            XX<1,I>[45,15]   = '������     : '

            XX<1,I>[59,15]   = BR.AMOUNT



            XX1<1,I>[45,15]  = '��� ������ : '

            XX1<1,I>[59,15]  = BR.ACCOUNT



            XX2<1,I>[45,15]  = '��� ������ : '

            XX2<1,I>[59,15]  = CATEG



            XX3<1,I>[45,15]  = '������     : '

            XX3<1,I>[59,15]  = CUR



* XX10<1,I>[45,15]  = '�������   : '

* XX10<1,I>[59,15]  = BR.INSURANCE





            XX4<1,I>[45,15]  = '����� ���� : '

            XX4<1,I>[59,15]  = MAT.DATE



            XX6<1,I>[1,15]   = '������'

            XX7<1,I>[1,15]   = AUTHI



            XX6<1,I>[30,15]  = '��� �������'

            XX7<1,I>[35,15]  = BT.ID



            XX6<1,I>[60,15]  = '������'

            XX7<1,I>[60,15]  = INP



            XX8<1,I>[3,35]   = '������ ������� : '

            XX8<1,I>[20,15]  = OUT.AMT





*-------------------------------------------------------------------

            YYBRN  = FIELD(BRANCH,'.',2)

            DATY   = TODAY

            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]

            PR.HD  ="'L'":SPACE(1):"��� ���� ������"

            PR.HD :="'L'":"������� : ":T.DAY

            PR.HD :="'L'":"����� : ":YYBRN

            PR.HD :="'L'":" "

            PR.HD :="'L'":SPACE(50) : "����� �����"

            PR.HD :="'L'":" "

            PRINT

            HEADING PR.HD

*------------------------------------------------------------------

            PRINT XX<1,I>

            PRINT XX1<1,I>

            PRINT XX2<1,I>

            PRINT XX3<1,I>

* PRINT XX10<1,I>

            PRINT XX4<1,I>

            PRINT STR(' ',82)

            PRINT STR(' ',82)

            PRINT XX8<1,I>

            PRINT XX9<1,I>

            PRINT XX5<1,I>

            PRINT STR(' ',82)

            PRINT XX6<1,I>

            PRINT STR('-',82)

            PRINT XX7<1,I>

        NEXT I

    END

*===============================================================

    RETURN

END
