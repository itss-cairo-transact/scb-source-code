* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
****NESSREEN AHMED 09/5/2010 *************
*-----------------------------------------------------------------------------
* <Rating>65</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE PAY.STOPPED.CHEQUES

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUE.ISSUE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.CHEQ
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CHEQUES.STOPPED
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT

    OPENSEQ "&SAVEDLISTS&" , "CHEQ_ST_PD" TO T.DATA THEN
        CLOSESEQ T.DATA
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CHEQ_ST_PD"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&", "CHEQ_ST_PD" TO T.DATA ELSE
        CREATE T.DATA THEN
****     PRINT 'HHHHHHHHHHHHH'
        END
        ELSE
            STOP 'NNNNNNNNNNNNNN'
        END
    END

    TEXT = 'HI' ; CALL REM

    FN.TELLER = 'F.TELLER$HIS' ; F.TELLER = '' ; R.TELLER = ''  ; E1 = ''
    CALL OPF(FN.TELLER,F.TELLER)

    FN.CUAC = 'F.CUSTOMER.ACCOUNT' ; F.CUAC = '' ; R.CUAC = ''  ; E1.CUAC = ''
    CALL OPF(FN.CUAC,F.CUAC)

**    T.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN (82 92 95 96) AND (AUTH.DATE GE 20091101 AND AUTH.DATE LT 20100509) AND RECORD.STATUS NE 'REVE' BY AUTH.DATE BY CO.CODE "
      T.SEL = "SELECT FBNK.TELLER$HIS WITH (CHEQUE.NO NE '' OR CHEQUE.NUMBER NE '' ) AND (AUTH.DATE GE 20091101 AND AUTH.DATE LT 20100509) AND RECORD.STATUS NE 'REVE' BY AUTH.DATE BY CO.CODE "
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        TEXT = 'SELECTED=':SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            CHQ.NO = '' ; CUST.NO = '' ; ACCT.NO = '' ; CHQ.CURR = '' ; CHQ.AMT = '' ; COMP.CO = '' ; PAY.D = '' ; TT.REF = '' ; REF = ''
            REF = KEY.LIST<I>
            TT.REF  = FIELD(REF, ";" ,1)
            TT.R = TT.REF:";2"
            CALL F.READ(FN.TELLER, KEY.LIST<I> , R.TELLER, F.TELLER, E1)
**   CALL F.READ(FN.TELLER, TT.R , R.TELLER, F.TELLER, E1)
            CHQ.NO  = R.TELLER<TT.TE.CHEQUE.NUMBER>
            CUST.NO = R.TELLER<TT.TE.CUSTOMER.1>
            ACCT.NO = R.TELLER<TT.TE.ACCOUNT.1>
            CHQ.CURR = R.TELLER<TT.TE.CURRENCY.1>
            IF CHQ.CURR = 'EGP' THEN
                CHQ.AMT = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
            END ELSE
                CHQ.AMT = R.TELLER<TT.TE.AMOUNT.FCY.1>
            END
            COMP.CO = R.TELLER<TT.TE.CO.CODE>
            PAY.D   = R.TELLER<TT.TE.AUTH.DATE>

            CALL F.READ(FN.CUAC, CUST.NO , R.CUAC, F.CUAC, E1.CUAC)
            LOOP
                REMOVE ACC.ID FROM R.CUAC SETTING SUBVAL
            WHILE ACC.ID:SUBVAL
                CHQ.ID = ACC.ID:'*':CHQ.NO
                CALL DBR ('CHEQUES.STOPPED':@FM:CHQ.STP.CURRENCY,CHQ.ID,CHQ.STOP)
                IF CHQ.STOP THEN
                    TRNS.DATA = CHQ.NO:"|"
                    TRNS.DATA := ACCT.NO:"|"
                    TRNS.DATA := CHQ.CURR:"|"
                    TRNS.DATA := CHQ.AMT:"|"
                    TRNS.DATA := PAY.D:"|"
                    TRNS.DATA := COMP.CO

                    WRITESEQ TRNS.DATA TO T.DATA ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                END
            REPEAT
        NEXT I
    END
*********
    RETURN
END
