* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE ONLINE.SOLFA.BAL.1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*****

    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "ONLINE.SOLFA.BAL.1" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"ONLINE.SOLFA.BAL.1"
        HUSH OFF
    END
    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "ONLINE.SOLFA.BAL.1" TO BB ELSE
        CREATE BB THEN
        END
        ELSE
            STOP 'Cannot create ONLINE.SOLFA.BAL.1 File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END
*****

    EOF = ''
    OLD.CUS = ''
    FN.AC  = "FBNK.ACCOUNT" ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    BAL.1407 = 0
    BAL.1408 = 0
    BAL.1413 = 0
    BAL.1445 = 0
    BAL.1455 = 0

    T.SEL  = " SELECT FBNK.ACCOUNT WITH ( CATEGORY EQ 1407 OR "
    T.SEL := " CATEGORY EQ 1408 OR CATEGORY EQ 1413 OR CATEGORY EQ 1445 OR CATEGORY EQ 1455) "
    T.SEL := " BY CUSTOMER BY CATEGORY"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,ETEXT)
            CALL F.READ(FN.AC,KEY.LIST<I+1>,R.AC1,F.AC,ETEXT)

            CUST.NO = R.AC<AC.CUSTOMER>
            CAT.NO  = R.AC<AC.CATEGORY>
            OLD.CUS = R.AC1<AC.CUSTOMER>

*****CREATE NEW TEXT****
            IF BAL.1407 EQ '' THEN
                BAL.1407 = 0
            END
            IF BAL.1408 EQ '' THEN
                BAL.1408 = 0
            END
            IF BAL.1413 EQ '' THEN
                BAL.1413 = 0
            END

            IF BAL.1445 EQ '' THEN
                BAL.1445 = 0
            END
            IF BAL.1455 EQ '' THEN
                BAL.1455 = 0
            END

************************
            IF CAT.NO EQ '1407' THEN
                BAL.1407 = R.AC<AC.OPEN.ACTUAL.BAL>
            END
            IF CAT.NO EQ '1408' THEN
                BAL.1408 = R.AC<AC.OPEN.ACTUAL.BAL>
            END
            IF CAT.NO EQ '1413' THEN
                BAL.1413 = R.AC<AC.OPEN.ACTUAL.BAL>
            END

            IF CAT.NO EQ '1445' THEN
                BAL.1445 = R.AC<AC.OPEN.ACTUAL.BAL>
            END
            IF CAT.NO EQ '1455' THEN
                BAL.1455 = R.AC<AC.OPEN.ACTUAL.BAL>
            END

            IF CUST.NO NE OLD.CUS THEN

                BB.DATA  = CUST.NO:"|"
                BB.DATA := BAL.1407:"|"
                BB.DATA := BAL.1408:"|"
                BB.DATA := BAL.1413:"|"
                BB.DATA := BAL.1445:"|"
                BB.DATA := BAL.1455

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

                BAL.1407 = 0
                BAL.1408 = 0
                BAL.1413 = 0
                BAL.1445 = 0
                BAL.1455 = 0

            END

        NEXT I
    END

    CLOSESEQ BB
    TEXT = " TEXT COMPLETE " ; CALL REM
END
