* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE R.DEP.INTER(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = C$ID.COMPANY

    FN.CUS='FBNK.CUSTOMER' ; F.CUS='' ; R.CUS='' ; CALL OPF(FN.CUS,F.CUS)

    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD='' ; R.LD=''
    CALL OPF(FN.LD,F.LD) ; DATEE = ''

    CALL DBR( 'DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD = TODAY
    T.SEL = "SELECT ":FN.LD: " WITH FIN.MAT.DATE GE ":DATEE: " AND FIN.MAT.DATE LE ": TD: " AND RENEW.IND NE 'YES' OR RENEW.METHOD EQ 1 AND (@ID EQ LD0636200301 OR @ID EQ LD0636200038) AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
    IF SELECTED THEN

        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>

        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
