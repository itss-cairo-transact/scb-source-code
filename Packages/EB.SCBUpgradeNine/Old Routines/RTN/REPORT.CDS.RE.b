* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
********************************NI7OOOOOOOOOOOOOOOOOOOOO**********************
    SUBROUTINE REPORT.CDS.RE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
**********MODIFIED BY ABEER AS OF 2017-09-11
*    COMP = ID.COMPANY
****************

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
**********MODIFIED BY ABEER AS OF 2017-09-11
    RETURN
*******END OF MODIFICATION
**************************************
INITIATE:
    REPORT.ID='REPORT.CDS.RE'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------

    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    FN.CORD = 'FBNK.BASIC.INTEREST' ;F.CORD = '' ; R.CORD = ''
    CALL OPF(FN.CORD,F.CORD)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    DATE1 = ''
    DATE2 = ''
    XDATE = ''
    FLG.COD = ''
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*------------------------------------------------------------------------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)
    YTEXT = "Enter the CD No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)

    AMT     = R.LD<LD.AMOUNT>
    CUR     = R.LD<LD.CURRENCY>
    SPREAD  = R.LD<LD.INTEREST.SPREAD>
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR,CUR22)
    DATE1   = R.LD<LD.VALUE.DATE>
    DATE2   = R.LD<LD.LOCAL.REF><1,LDLR.APPROVAL.DATE>
    IF DATE2 LT DATE1 THEN
        XDATE =  DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]
    END ELSE
        IF DATE2 GT DATE1 THEN
            XDATE =  DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
        END
    END
    IF DATE2  = '' THEN
        XDATE = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
    END
    FINDATE1  = R.LD<LD.FIN.MAT.DATE>
    FINDATE   = FINDATE1[1,4]:'/':FINDATE1[5,2]:"/":FINDATE1[7,2]
    NAME11    = R.LD<LD.LOCAL.REF><1,LDLR.IN.RESPECT.OF>
    RAT       = R.LD<LD.INTEREST.RATE>
    RAT2      = R.LD<LD.INTEREST.KEY>
    RATE3     = RAT2:'...'
    T.SEL     = "SELECT FBNK.BASIC.INTEREST WITH @ID LIKE ":RATE3:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.CORD,KEY.LIST<SELECTED>,R.CORD,F.CORD,READ.ERR)
    RATEF     = R.CORD<EB.BIN.INTEREST.RATE> + SPREAD
    CD.TYP    = R.LD<LD.LOCAL.REF><1,LDLR.CD.TYPE>
******************
    BRN.CODE = R.LD<LD.CO.CODE>
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.CODE,YYBRN)
******************
    CALL DBR('SCB.CD.TYPES':@FM:CD.TYPES.DESCRIPTION,CD.TYP,DESC)
    TYP    = FIELD(DESC,'-',2)
    RATENO = RATEF:" ":"%":"����":" ":TYP

    IN.AMOUNT = AMT
    CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR22 : ' ' : '�����'

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)


*    XX<1,1>[3,15]      =  '��� �������   :'
    XX<1,1>[3,15]      = '��� �������'
    XX<1,1>[45,15]     = COMI

    XX2<1,1>[3,15]     = '�����       : '
    XX2<1,1>[45,35]    = NAME11

    XX3<1,1>[3,15]     = '���� �������: '
    XX3<1,1>[45,35]    = AMT
    XX11<1,1>[45,35]   = OUT.AMT

    XX4<1,1>[3,15]     = '������ :'
******************
    CD.TYP    = R.LD<LD.LOCAL.REF><1,LDLR.CD.TYPE>
    INT.MON = FIELD(CD.TYP,'-',3)

    XX4<1,1>[3,15]     = '��� ������ :'
**********MODIFIED BY ABEER AS OF 2017-09-11
    IF CUR EQ 'EGP' THEN
******************1 MONTH
        IF INT.MON EQ '1M' THEN
*XX4<1,1>[45,35]    ='����� ��� ���� ��� ������� ������ �� ����� ������� �� ����� ����� ������ ����� 1/2% �����'
            TEXT = RAT2:'-KEY';CALL REM

            IF RAT2 EQ '87' THEN
                XX4<1,1>[45,35] = '����� ��� ���� ��� ������� ������ �� ����� ������� �����1.25%�����'
                FLG.COD='2'
            END

            IF RAT2 EQ '84' THEN
                XX4<1,1>[45,35] = '����� ��� ���� ��� ������� ������ �� ����� ������� �����0.75%�����'
                FLG.COD='1'
            END
            IF RAT2 EQ '80' THEN
                XX4<1,1>[45,35] = '����� ��� ���� ��� ������� ������ �� ����� ������� �����0.5%�����'
                FLG.COD = '0'
            END
        END
********************3 MONTH
        IF INT.MON EQ '3M' THEN
*XX4<1,1>[45,35]    ='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �� ����� ����� ������ �����1/4% �����'
           IF RAT2 EQ '88' THEN
                XX4<1,1>[45,35]='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �����1.0%�����'
                FLG.COD='2'
            END
            IF RAT2 EQ '85' THEN
                XX4<1,1>[45,35]='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �����0.50%�����'
                FLG.COD='1'
            END
            IF RAT2 EQ '81' THEN
                XX4<1,1>[45,35]='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �����0.25%�����'
                FLG.COD = '0'
            END
        END
*******************6 MONTH
        IF INT.MON EQ '6M' THEN
*XX4<1,1>[45,35]    ='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �� ����� ����� ������ �����1/8% �����'
            IF RAT2 EQ '89' THEN
              XX4<1,1>[45,35] ='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �����0.75%�����'
                FLG.COD='2'
            END

            IF RAT2 EQ '86' THEN
                XX4<1,1>[45,35] ='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �����0.375%�����'
                FLG.COD='1'
            END
            IF RAT2 EQ '82' THEN
                XX4<1,1>[45,35] ='��� ���� ��� ���� ��� ������� ������ �� ����� ������� �����0.125%�����'
                FLG.COD = '0'
            END
        END
    END
*************
    IF CUR EQ 'USD' THEN
        XX4<1,1>[45,35] ='��� ���� ��� ���� ��� ������� ����� ���� 1.75'
    END
*******************
**************END OF MODIFICATION
**    XX4<1,1>[45,35]    = RATENO

    XX5<1,1>[3,15]     = '����� �������  :'
    XX5<1,1>[45,15]    = XDATE

    XX6<1,1>[3,15]     = ' ����� ���������: '
    XX6<1,1>[45,15]    = FINDATE

    XX7<1,1>[3,15]     = ' ������: '
    XX7<1,1>[45,15]    = CUR22

*    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"����� �����"
**********************
    IF CUR EQ 'EGP' THEN
        PR.HD :="'L'":"REPORT.CDS"
    END ELSE
        PR.HD :="'L'":"REPORT.CDS.USD"
    END
******************
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR(' ',82)
    PRINT XX7<1,1>
    PRINT STR('=',82)
*****************
     IF FLG.COD = '1' THEN
        PRINT STR('NEW',1)
     END 
     IF FLG.COD = '0' THEN
        PRINT STR('OLD',1) 
     END
     IF FLG.COD = '' THEN 
        PRINT STR('NOT EXIST',1) 
     END

***********
    RETURN
END
