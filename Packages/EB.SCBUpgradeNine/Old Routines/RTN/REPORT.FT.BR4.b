* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>389</Rating>
*-----------------------------------------------------------------------------
*-----------------------
** Create By Ni7ooo
** Update By Nessma
*-----------------------
    SUBROUTINE REPORT.FT.BR4

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.CHQ.TYPE
*-------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
*Line [ 56 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������"  ;  CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID = 'REPORT.FT.BR4'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.BATCH = 'F.SCB.BT.BATCH'      ; F.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER'  ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    ID      = ID.NEW
    DATE.TO = TODAY[3,6]:"..."
*---------------------------------------------------------------
    REAS     = R.NEW(SCB.BT.OUR.REFERENCE)
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    RET.REAS = DCOUNT(REAS,@VM)

    FOR I = 1 TO RET.REAS
        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> EQ '' THEN
            BR.ID     = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
            CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E1)
            DRAWER.ID = R.BR<EB.BILL.REG.DRAWER><1,I>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)

            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>

            BANK.NO      = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            BRN.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>

            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK.NO,BANK.NAME)
            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BRN.NO,BRANCH.NAME)

            CHEQ.STAT = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BIL.CHQ.TYPE>
            CALL DBR ('SCB.BL.CHQ.TYPE':@FM:BL.CHQ.DESCRIPTION,CHEQ.STAT,CHEQ.NAME)

            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
            AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            IN.AMOUNT = AMOUNT
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            OUT.AMT = OUT.AMOUNT: ' ' : CUR : ' ' : '�����'
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

            DAT       =  R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.RECEIVE.DATE>
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            CHQ.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
            INPUTTER  = R.BR<EB.BILL.REG.INPUTTER>
            AUTH      = R.BR<EB.BILL.REG.AUTHORISER>
            INP       = FIELD(INPUTTER,'_',2)
            AUTHI     = FIELD(AUTH,'_',2)
            DAT       = TODAY
            CALL CDT("",DAT,'+1W')

            XX   = SPACE(132)
*--------------------------------------------
            PR.HD  = "REPORT.FT.BR4"
            HEADING PR.HD

            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PRINT SPACE(5) : SPACE(1):"��� ���� ������"
            PRINT SPACE(5) : "������� : ":T.DAY
            PRINT SPACE(5) : "����� : ":YYBRN
*----------------------------------------------
            XX<1,1>[3,15]   = CUST.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15]  = '������ : '
            XX<1,1>[60,15]  = AMOUNT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = CUST.ADDRESS
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15] = '��� ������ : '
            XX<1,1>[64,15] = ACC.NO
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '����� ��� ��� :' :BANK.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15] = '��� ������ : '
            XX<1,1>[64,15] = CATEG
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� ':BRANCH.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[50,15] = '������ : '
            XX<1,1>[60,15] = CUR
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15] = '����� ���� : '
            XX<1,1>[20,15] = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '��� ����� : '
            XX<1,1>[15,15] = CHQ.NO:' ':CHEQ.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '������'
            XX<1,1>[15,15]  = INP
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,20] = '��� �������'
            XX<1,1>[30,15] = ID.NEW
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15] = '������'
            XX<1,1>[30,15] = AUTHI
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]  = '������ �������:'
            XX<1,1>[20,15] = OUT.AMT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15]  = '������:'
            XX<1,1>[20,15] = '����� �����'
            PRINT XX<1,1>
            XX<1,1> = ""
        END
    NEXT I
    RETURN
*=================================================================
PRINT.HEAD:
*----------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY
    PR.HD :="'L'":SPACE(1):"����� : ":YYBRN
    PR.HD :="'L'":SPACE(1):"����� �����"
    PR.HD :="'L'":"REPORT.FT.BR4"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
*==================================================================
END
