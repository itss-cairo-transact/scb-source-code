* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
    SUBROUTINE  REPORT.LG.MORASEL1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*------------------------------------------------------------------------
    KIND.FG = R.NEW(LD.LOCAL.REF)<1,LDLR.LG.KIND>
    IF KIND.FG NE 'FG' THEN
        GOSUB INITIATE
        GOSUB PROCESS

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT  = "��� ����� ��������� 11" ; CALL REM
        RETURN
    END
*========================================================================
INITIATE:
    REPORT.ID='REPORT.LG.MORASEL1'
****REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    FN.AC    ='FBNK.ACCOUNT'  ; F.AC = ''
    FN.CUS   ='FBNK.CUSTOMER' ; F.CUS = ''
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CUS,F.CUS)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*------------------------------------------------------------------------
    ID  = ID.NEW
    CALL F.READ(FN.LD,ID.NEW,R.LD,F.LD,E1)

    CUS.ID       = R.NEW(LD.CUSTOMER.ID)
    LOCAL.REF    = R.NEW(LD.LOCAL.REF)
* CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF2)
* CUST.NAME    = LOCAL.REF2<1,CULR.ARABIC.NAME>
* CUST.NAME1   = LOCAL.REF2<1,CULR.ARABIC.NAME.2>

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>
    CALL DBR ('CUSTOMER':@FM:EB.CUS.SHORT.NAME,CUS.ID,NAME2)
    CUST.NAME    = NAME2
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END

    ADDR1        = LOCAL.REF<1,LDLR.BNF.DETAILS,1>
    ADDR2        = LOCAL.REF<1,LDLR.BNF.DETAILS,2>
    ADDR3        = LOCAL.REF<1,LDLR.BNF.DETAILS,3>

    OLDLD        = LOCAL.REF<1,LDLR.OLD.NO>

    AMOUNT       = R.NEW(LD.AMOUNT)

    REF          = LOCAL.REF<1,LDLR.SENDING.REF>

    BRANCH.ID    = R.NEW(LD.MIS.ACCT.OFFICER)

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    DAT = R.NEW(LD.FIN.MAT.DATE)
    DAT2= R.NEW(LD.VALUE.DATE)

    V.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    V.DATE2 = DAT2[7,2]:'/':DAT2[5,2]:"/":DAT2[1,4]


    CUR.ID    = R.NEW(LD.CURRENCY)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<1,1>,CUR.ID,CUR)

*** OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    XX    = SPACE(132)  ; XX3   = SPACE(132) ; XX15  = SPACE(132)
    XX1   = SPACE(132)  ; XX4   = SPACE(132) ; XX11  = SPACE(132)
    XX2   = SPACE(132)  ; XX5   = SPACE(132) ; XX10  = SPACE(132)
    XX6   = SPACE(132)  ; XX7   = SPACE(132) ; XX8   = SPACE(132)
    XX12  = SPACE(132)  ; XX13  = SPACE(132) ; XX14  = SPACE(132)
    XX16  = SPACE(132)  ; XX17  = SPACE(132) ; XX18  = SPACE(132)
    XX19  = SPACE(132)  ; XX9   = SPACE(132)

** XX<1,1>[70,15]   =  BRANCH:"ON":V.DATE2

    XX3<1,1>[70,35]  = CUST.NAME

    XX4<1,1>[70,35]  = CUST.ADDRESS

    XX7<1,1>[35,15]  = 'ATTN.GUARANTEES DEPT.'
    XX8<1,1>[35,15]  =  "L/G":"":OLDLD

    XX9<1,1>[35,15]  = 'FOR':"":CUR:"":AMOUNT
    XX10<1,1>[35,15] =  "YOUR REF." : REF


    XX11<1,1>[70,15] = "Dear Sirs,"


    XX12<1,1>[70,15]  = "Further to our todays swift"
    XX19<1,1>[70,15]  = "We enclose herewith two copies of "
    XX13<1,1>[70,15]  = 'The above mentional L/G for you records .'

    XX14<1,1>[65,15]  = 'Kindly acknowledge receipt and we remain'

    XX15<1,1>[30,15]  = 'Yours Faithfully,'

    XX16<1,1>[65,15]  = 'For The Suze Canal Bank/Operations Center'

    XX17<1,1>[70,15]  = 'ENCL.(2)'

*-------------------------------------------------------------------
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"����� : ":SPACE(20):"����� ���� ���� �������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
*PRINT STR(' ',82)
    PRINT XX5<1,1>
* PRINT STR(' ',82)
    PRINT XX6<1,1>
* PRINT XX4<1,1>
*PRINT XX11<1,1>
    PRINT XX8<1,1>
    PRINT STR(' ',82)
    PRINT XX9<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX12<1,1>
    PRINT STR(' ',82)
    PRINT XX19<1,1>
    PRINT STR(' ',82)
    PRINT XX13<1,1>
    PRINT STR(' ',82)
    PRINT XX14<1,1>
    PRINT STR(' ',82)
    PRINT XX15<1,1>
    PRINT STR(' ',82)
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT XX19<1,1>

    PRINT STR('=',82)

*===============================================================
    RETURN
END
