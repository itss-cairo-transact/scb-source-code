* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**************************NI7OOOOOOOOOOOOOO***********************
    SUBROUTINE REPORT.LC.REPRINT1

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.LC.REPRINT1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.DR  = 'FBNK.LETTER.OF.CREDIT' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    FN.DR.HIS  = 'FBNK.LETTER.OF.CREDIT$HIS' ; F.DR.HIS = ''
    CALL OPF(FN.DR.HIS,F.DR.HIS)

    OUT.AMOUNT = ''
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    DATE.TO = TODAY[3,6]:"..."
    NN.TITLE = '����� ���':' - ':'ISSUE PROVISION DEBIT '
*------------------------------------------------------------------------

    IDCOMI = COMI
    YTEXT = "Enter the TF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")

    T.SEL = "SELECT FBNK.LETTER.OF.CREDIT WITH @ID EQ ": COMI
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CALL F.READ(FN.DR,KEY.LIST,R.DR,F.DR,E1)
        MARGIN.ACCOUNT = R.DR<TF.LC.PROVIS.ACC>
        LC.CUR         = R.DR<TF.LC.LC.CURRENCY>
        ACC.NO    = MARGIN.ACCOUNT
        CUR       = MARGIN.ACCOUNT[9,2]
        REFERENCE = R.DR<TF.LC.OLD.LC.NUMBER>

        CURRNO    = R.DR<TF.LC.CURR.NO>
        CURRNO2   = CURRNO - 2

        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
****        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        ACC.BR  = AC.COMP[8,2]

        CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
        MARGIN.AMOUNT    = R.DR<TF.LC.DB.PROV.AMOUNT>
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.ID,CUR1)

        IDNEW = COMI:';':CURRNO2
        CALL F.READ(FN.DR.HIS,IDNEW,R.DR.HIS,F.DR.HIS,ECAA)
        IF MARGIN.AMOUNT EQ '' THEN
            CALL F.READ(FN.DR.HIS,IDNEW,R.DR.HIS,F.DR.HIS,ECAA)
            MARGIN.AMOUNT2 = R.DR.HIS<TF.LC.DB.PROV.AMOUNT>
            MARGIN.RATE    = R.DR.HIS<TF.LC.PROV.EXCH.RATE>
            MARGIN.AMOUNT  = MARGIN.AMOUNT2
            IF MARGIN.AMOUNT LT 0 THEN
                MARGIN.AMOUNT = MARGIN.AMOUNT * (-1)
            END
            IN.AMOUNT = MARGIN.AMOUNT
        END
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR1 : ' ' : '�����'

        MARGIN.PERC = R.DR<TF.LC.PROVIS.PERCENT>

        INPUTT      = R.DR.HIS<TF.DR.INPUTTER>
        INP         = FIELD(INPUTT,'_',2)
        AUTH        = R.DR.HIS<TF.LC.AUTHORISER>
        AUTHI       = FIELD(AUTH,'_',2)
*------------------------------------------------------------------------

        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
        XX8  = SPACE(132)  ; XX9  = SPACE(132)

* XX1<1,1>[3,35]   = COMM.AC.NO
        XX1<1,1>[3,35]   = CUST.NAME
        XX2<1,1>[3,35]   = CUST.ADDRESS

        XX<1,1>[45,15]  = '������     : '
        XX<1,1>[59,15]  = IN.AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = ACC.NO

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR1

* XX4<1,1>[45,15] = '���� ������� : '
* XX4<1,1>[59,15] = MARGIN.PERC
        XX4<1,1>[45,15] = '������ :'
        XX4<1,1>[59,15] = REFERENCE

        XX6<1,1>[1,15]  = '������'
        XX6<1,1>[30,15] = '��� �������'
        XX6<1,1>[60,15] = '������'

        XX7<1,1>[1,15]  = INP:' �'
        XX7<1,1>[30,15] = COMI:' �'
        XX7<1,1>[60,15] = AUTHI:' �'

        XX8<1,1>[3,35]  = '������ ������� : '
        XX8<1,1>[20,15] = OUT.AMT

        XX9<1,1>[3,15]  = '������         : '
*            XX9<1,1>[20,15] = BR.DATA
    END
*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,ACC.BR,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(45) : "����� �������� "
    PR.HD :="'L'":SPACE(35):NN.TITLE
    PR.HD :="'L'":SPACE(30):STR('_',20)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX<1,1>
* PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
