* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE OFS.FT.EXP.MONTH
*    PROGRAM OFS.FT.EXP.MONTH

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EXP.MONTH
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ������  '
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN

        GOSUB CHECKEXIST

        IF SW2 = 0 THEN
            GOSUB INITIALISE
            GOSUB BUILD.RECORD
        END
    END ELSE
        TEXT = '�� ������ �� ��������' ; CALL REM
    END


    RETURN
*-----------------------------------------
CHECKEXIST:
    KEY.LIST="" ; SELECTED="" ;  ER.FT="" ; SW2 = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC75'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "�� ����� ������ ������ �� ���" ; CALL REM
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    RETURN
*-----------------------------------------
INITIALISE:

    OPENSEQ "MECH" , "FT.EXP.MONTH" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"FT.EXP.MONTH"
        HUSH OFF
    END
    OPENSEQ "MECH" , "FT.EXP.MONTH" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FT.EXP.MONTH CREATED IN MECH'
        END ELSE
            STOP 'Cannot create FT.EXP.MONTH File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    FN.EXP = 'F.SCB.EXP.MONTH' ; F.EXP = ''
    CALL OPF(FN.EXP,F.EXP)


    FN.AC= 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    V.DATE = TODAY


    T.SEL = "SELECT F.SCB.EXP.MONTH BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.EXP,KEY.LIST<I>,R.EXP,F.EXP,E1)


*Line [ 103 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.CATEG.COUNT = DCOUNT(R.EXP<EXP.PL.CATEG>,@VM)
            FOR X = 1 TO WS.CATEG.COUNT
                WS.PL.CATEG = R.EXP<EXP.PL.CATEG,X>
                WS.AMT  = R.EXP<EXP.AMOUNT,X>
                WS.ACC  = R.EXP<EXP.ACC.NO,X>
                WS.DESC = R.EXP<EXP.DISCR,X>
                WS.DEPT = R.EXP<EXP.COST.DEP,X>
                WS.NOTES = R.EXP<EXP.DISCR,X>

                CALL F.READ(FN.AC,WS.ACC,R.AC,F.AC,E2)
*                WS.ACC.COMP = R.AC<AC.CO.CODE>
                WS.ACC.COMP = R.EXP<EXP.BRANCH,X>
                WS.ACC.CCY  = R.AC<AC.CURRENCY>

                IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//':WS.ACC.COMP:','

                OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC75":','
                OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":WS.ACC.CCY:','
                OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":WS.ACC.CCY:','
                OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":'PL':WS.PL.CATEG:','
                OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":WS.ACC:','
                OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":WS.AMT:','
                OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:','
                OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:','
                OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
                OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":','
                OFS.MESSAGE.DATA :=  "PROFIT.CENTRE.DEPT=":WS.DEPT:','
                OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":WS.DEPT:','
                OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":WS.DEPT:','
                OFS.MESSAGE.DATA :=  "ORDERING.BANK=":'SCB':','
                OFS.MESSAGE.DATA :=  "NOTS.CREDIT=":WS.NOTES:','
                OFS.MESSAGE.DATA :=  "NOTE.DEBITED=":WS.NOTES

                MSG.DATA = IDD:",":OFS.MESSAGE.DATA
*DEBUG
                WRITESEQ MSG.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            NEXT X

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN FT.EXP.MONTH'
    EXECUTE 'DELETE ':"MECH":' ':"FT.EXP.MONTH"

    RETURN
END
