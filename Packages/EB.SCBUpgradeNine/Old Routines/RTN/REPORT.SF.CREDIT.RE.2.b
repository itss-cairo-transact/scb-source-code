* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>-88</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.SF.CREDIT.RE.2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.SF.CREDIT.RE.2'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.BR = 'F.SCB.SF.TRANS' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)
    RETURN
*========================================================================
PROCESS:
*-------
    YTEXT = "Enter the SF No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BR,COMI,R.SAFE,F.BR,EE1)
    SAFE.ID   = COMI
    TRNS.TYPE = R.SAFE<SCB.SF.TR.TRANSACTION.TYPE>


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CUS           = R.SAFE<SCB.SF.TR.CUSTOMER.NO>
    CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
    CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>
    CUST.NAME3    = LOCAL<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS1 = LOCAL<1,CULR.ARABIC.ADDRESS>
    CUST.ADDRESS2 = LOCAL<1,CULR.ARABIC.ADDRESS>

    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)

    IF CUST.ADDRESS1 = 98 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 998 THEN
        CUST.ADD2 = ''
    END
    IF CUST.ADDRESS1 = 999 THEN
        CUST.ADD1 = ''
    END
    IF CUST.ADDRESS2 = 999 THEN
        CUST.ADD2 = ''
    END

*--------------
    IF TRNS.TYPE EQ 'MRGN' THEN
*        ACC.DB = R.SAFE<SCB.SF.TR.MARGIN.ACCT>
    END

    IF TRNS.TYPE EQ 'EXPD' THEN
*        ACC.DB = R.SAFE<SCB.SF.TR.CUST.DEBIT.ACCT>
    END
*--------------
    ACC.DB = ""
    CALL DBR ("ACCOUNT":@FM:AC.CATEGORY,ACC.DB,CATEG.ID)
    CALL DBR ("ACCOUNT":@FM:AC.CURRENCY,ACC.DB,CURR.ID)

    CATEG.ID = "54015"
    ACC.DB   = "54015"

    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG.NAME)

    AMOUNT    = R.SAFE<SCB.SF.TR.COMMISSION.AMT2>

    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    V.DATE = R.SAFE<SCB.SF.TR.VALUE.DATE>

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CURR.ID,CUR)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    MAT.DATE  = V.DATE[7,2]:'/':V.DATE[5,2]:"/":V.DATE[1,4]

    INPUTTER = R.SAFE<SCB.SF.TR.INPUTTER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTH     = R.SAFE<SCB.SF.TR.AUTHORISER>
    AUTHI    = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]    = CATEG.NAME
*    XX1<1,1>[3,35]   = CUST.NAME3
*    XX2<1,1>[3,35]   = CUST.ADDRESS
*    XX3<1,1>[3,35]   = CUST.ADD1:' ':CUST.ADD2

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = ACC.DB

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG.NAME

    XX3<1,1>[45,15] = '������     : '
*    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

*XX5<1,1>[3,15]  = '��� �����      : '
*XX5<1,1>[20,15] = CHQ.NO

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15]  = INP

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15 ] = COMI

    XX8<1,1>[3,35]   = '������ ������� : '
    XX8<1,1>[20,15]  = OUT.AMT
*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"SAFEKEEP"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
