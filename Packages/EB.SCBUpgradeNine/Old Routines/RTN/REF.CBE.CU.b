* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-36</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REF.CBE.CU

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.LC = 'FBNK.DRAWINGS' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)
    FN.LC.HIS = 'FBNK.DRAWINGS$HIS' ; F.LC.HIS = ''
    CALL OPF(FN.LC.HIS,F.LC.HIS)

    IF O.DATA[1,2] EQ 'FT' THEN
        ZZ = O.DATA:';1'
        CALL F.READ(FN.FT.HIS,ZZ,R.FT.HIS,F.FT.HIS,E2)
        AC.NO.DR     = R.FT.HIS<FT.DEBIT.ACCT.NO>
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO.DR,CUSNO.DR)
**        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO.DR,LOCAL.REF)
**        CBE.NO.DR = LOCAL.REF<1,CULR.CBE.NO>
**       REG.NO.DR =LOCAL.REF<1,CULR.COM.REG.NO>
******************
        AC.NO.CR     = R.FT.HIS<FT.CREDIT.ACCT.NO>
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO.CR,CUSNO.CR)
**        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO.CR,LOCAL.REF)
**     CBE.NO.CR = LOCAL.REF<1,CULR.CBE.NO>
**   REG.NO.CR =LOCAL.REF<1,CULR.COM.REG.NO>
******************
        O.DATA    = CUSNO.DR:'-':CUSNO.CR
        IF (E2) THEN
            ZZZ = O.DATA
            CALL F.READ(FN.FT,ZZZ,R.FT,F.FT,E222)
            AC.NO.DR     = R.FT<FT.DEBIT.ACCT.NO>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO.DR,CUSNO.DR)
**       CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO.DR,LOCAL.REF)
**       CBE.NO.DR = LOCAL.REF<1,CULR.CBE.NO>
**       REG.NO.DR =LOCAL.REF<1,CULR.COM.REG.NO>
***********
            AC.NO.CR     = R.FT<FT.CREDIT.ACCT.NO>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO.CR,CUSNO.CR)
**          CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO.CR,LOCAL.REF)
**        CBE.NO.CR = LOCAL.REF<1,CULR.CBE.NO>
**      REG.NO.CR =LOCAL.REF<1,CULR.COM.REG.NO>
******************
            O.DATA    = CUSNO.DR:'-':CUSNO.CR
        END
    END
    IF O.DATA[1,2] EQ 'TF' THEN
        MM=O.DATA[1,14]
        CALL F.READ(FN.LC,MM,R.LC,F.LC,E22)
        IF NOT(E22) THEN
            LC.CUS.DR    = R.LC<TF.DR.DRAWDOWN.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS.DR,CUSID.DR)

            LC.CUS.CR    = R.LC<TF.DR.PAYMENT.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS.CR,CUSID.CR)

*CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID.DR,LOCAL.REF)
*CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = CUSID.DR:'-':CUSID.CR
        END
        IF E22 THEN
            NN = O.DATA[1,14]:';1'
            CALL F.READ(FN.LC.HIS,NN,R.LC.HIS,F.LC.HIS,E33)

            LC.CUS.DR.H    = R.LC.HIS<TF.DR.DRAWDOWN.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS.DR.H,CUSID.DRH)

            LC.CUS.CR.H=R.LC.HIS<TF.DR.PAYMENT.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS.CR.H,CUSID.CRH)
**            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID.HIS,LOCAL.REF.H)
  **          CUST.NAME.HIS = LOCAL.REF.H<1,CULR.ARABIC.NAME>
            O.DATA    = CUSID.DRH:'-':CUSID.CRH
        END
    END

*************************************************************
    RETURN
END
