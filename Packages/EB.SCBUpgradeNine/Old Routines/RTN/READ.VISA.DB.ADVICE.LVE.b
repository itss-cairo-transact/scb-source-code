* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
****NESSREEN AHMED 28/9/2010**************
****UPDATED BY N.A ON 06/11/2010**********
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
**  SUBROUTINE READ.VISA.DB.ADVICE.LVE
    PROGRAM  READ.VISA.DB.ADVICE.LVE

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.READ.DB.ADVICE

    F.VISA.DB.AD = '' ; FN.VISA.DB.AD = 'F.SCB.READ.DB.ADVICE' ; R.VISA.DB.AD= '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.DB.AD,F.VISA.DB.AD)

    YTEXT = "Enter the End Date Of Month : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    ID.KEY = '' ; MBR = '' ; CARD.NO = '' ; X = 0  ; LST.ST.D = ''
    CONTR.NO = ''
    T.SEL = "SELECT F.SCB.READ.DB.ADVICE WITH LST.STAT.DATE EQ " :COMI : " BY CONTRACT.NO "

    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
    CARD.ST = ''
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF SELECTED THEN
        CALL F.READ(FN.VISA.DB.AD,KEY.LIST<1>, R.VISA.DB.AD, F.VISA.DB.AD ,E1)
        CONTR.NO<1>  = R.VISA.DB.AD<SCB.DB.CONTRACT.NO>
        N.SEL = "SELECT F.SCB.READ.DB.ADVICE WITH LST.STAT.DATE EQ " :COMI:" AND CONTRACT.NO EQ " : CONTR.NO<1> : " AND ((CARD.ONLINE.ST EQ 'Opened') OR ((CARD.TWCMS.ST EQ 'Given') OR (CARD.TWCMS.ST EQ 'Embossed')))"
        KEY.LIST.NN = '' ; SELECTED.NN = '' ; ER.MSG.NN = ''
        CALL EB.READLIST(N.SEL , KEY.LIST.NN,'',SELECTED.NN,ER.MSG.NN)
        IF SELECTED.NN THEN
            FOR NN = 1 TO SELECTED.NN
                CALL F.READ(FN.VISA.DB.AD,KEY.LIST.NN<NN>, R.VISA.DB.AD, F.VISA.DB.AD ,E2)
                ID.KEY = KEY.LIST.NN<NN>
                CARD.ST.D<NN> = 'LVE'
                R.VISA.DB.AD<SCB.DB.CARD.STAT>   = CARD.ST.D<NN>
                CALL F.WRITE(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD)
                CALL JOURNAL.UPDATE(ID.KEY)
                X = X + 1
            NEXT NN
        END
        FOR I = 2 TO SELECTED
            ID.KEY = ''  ; CARD.ST = ''
            CALL F.READ(FN.VISA.DB.AD,KEY.LIST<I>, R.VISA.DB.AD, F.VISA.DB.AD ,E1)
**CARD.NO<I>   = R.VISA.DB.AD<SCB.DB.CARD.NO>
            CONTR.NO<I>  = R.VISA.DB.AD<SCB.DB.CONTRACT.NO>
**MBR<I>       = R.VISA.DB.AD<SCB.DB.MBR>
**LST.ST.D<I>  = R.VISA.DB.AD<SCB.DB.LST.STAT.DATE>

            IF CONTR.NO<I> # CONTR.NO<I-1> THEN
                S.SEL = "SELECT F.SCB.READ.DB.ADVICE WITH LST.STAT.DATE EQ ":COMI :" AND CONTRACT.NO EQ " : CONTR.NO<I> :" AND ((CARD.ONLINE.ST EQ 'Opened') OR ((CARD.TWCMS.ST EQ 'Given') OR (CARD.TWCMS.ST EQ 'Embossed')))"
                KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
                CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)
                IF SELECTED.SS THEN
                    FOR SS = 1 TO SELECTED.SS
                        X = X + 1
                        ID.KEY = KEY.LIST.SS<SS>
                        CARD.ST<I> = 'LVE'
                        CALL F.READ(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD, F.VISA.DB.AD ,E1)
                        R.VISA.DB.AD<SCB.DB.CARD.STAT>   = CARD.ST<I>
                        CALL F.WRITE(FN.VISA.DB.AD,ID.KEY, R.VISA.DB.AD)
                        CALL JOURNAL.UPDATE(ID.KEY)
                    NEXT SS
                END
            END

        NEXT I
        TEXT = 'NO.REC.UPD=':X ; CALL REM
    END
    RETURN
END
