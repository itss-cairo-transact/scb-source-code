* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>560</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
*Create By Nessma
*-----------------------------------------------------------------------------
*    PROGRAM RTN.AC.INVEST.ALL
    SUBROUTINE RTN.AC.INVEST.ALL

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.OFS.SOURCE
    $INSERT T24.BP I_F.COLLATERAL
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.NUMERIC.CURRENCY
    $INSERT           I_F.SCB.AC.INVEST.EGP
*--------------------------------------------
    GOSUB INITIALISE
    GOSUB PROCESS
    TEXT =  "DONE" ; CALL REM
    RETURN
*--------------------------------------------
INITIALISE:
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN     = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN      = 0
    OFS.REC       = ""
    OFS.OPERATION = "FUNDS.TRANSFER"
    OFS.OPTIONS   = "MALEYA"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "MALEYA.OFS.":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
*-----
    M1  = TODAY[1,4]: '0101'
    M11 = TODAY[1,4]: '0331'

    M2  = TODAY[1,4]: '0401'
    M22 = TODAY[1,4]: '0630'

    M3  = TODAY[1,4]: '0701'
    M33 = TODAY[1,4]: '0930'

    M4  = TODAY[1,4]: '1001'
    M44 = TODAY[1,4]: '1231'

    TOD.DAT = TODAY
*-----
    RETURN
*----------------------------------------------------
PROCESS:
    FN.TMP = "F.SCB.AC.INVEST.EGP"    ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    FN.ACCT = "FBNK.ACCOUNT"   ; F.ACCT = ""
    CALL OPF(FN.ACCT,F.ACCT)
*---------------------------------------------------
    N.SEL  = "SELECT ":FN.TMP:" WITH AMOUNT NE ''"
    N.SEL := " BY @ID"
    CALL EB.READLIST(N.SEL,KEY.LIST,'',SELECTED,ERR)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            TMP.ID = KEY.LIST<II>

            CALL F.READ(FN.TMP,TMP.ID, R.TMP, F.TMP, ETEXT)
            COMP          = "EG0010099"
            COMP.CODE     = "99"
            OFS.USER.INFO = "INPUTT" : COMP.CODE : "//" :COMP

            FLD.1 = R.TMP<AC.INV.EQUIVALENT.NUMBER>
            FLD.2 = R.TMP<AC.INV.PL.CATEGORY>

            L.RUN.DAT = R.TMP<AC.INV.LAST.RUN.DATE>
            AMOUNT    = R.TMP<AC.INV.AMOUNT>

            IF FLD.1 EQ '' AND FLD.2 EQ '' THEN
            END ELSE
                L.RUN.DAT = R.TMP<AC.INV.LAST.RUN.DATE>
                IF L.RUN.DAT NE '' THEN
                    GOSUB CHECK.RUN.DAT
                END

                IF FLG EQ 0 THEN
                    AMOUNT = R.TMP<AC.INV.AMOUNT>
                    IF AMOUNT NE 0 THEN

                        IF AMOUNT GE '0' THEN
                            DR.ACC = R.TMP<AC.INV.ACCOUNT.NUMBER>
                            CR.ACC = R.TMP<AC.INV.EQUIVALENT.NUMBER>
                            IF CR.ACC EQ '' THEN
                                CR.ACC = "PL":R.TMP<AC.INV.PL.CATEGORY>
                                PROF.CENT.DPT = "99"
                            END

                        END ELSE
                            AMOUNT = AMOUNT * -1

                            DR.ACC = R.TMP<AC.INV.EQUIVALENT.NUMBER>
                            CR.ACC = R.TMP<AC.INV.ACCOUNT.NUMBER>
                            IF DR.ACC EQ '' THEN
                                DR.ACC = "PL":R.TMP<AC.INV.PL.CATEGORY>
                                PROF.CENT.DPT = "99"
                            END
                        END

                        TRNS.TYPE = "ACMM"

                        IF DR.ACC[1,2] NE 'PL' THEN
                            CURR = DR.ACC[1,3]
                        END ELSE
                            CURR = CR.ACC[1,3]
                        END


                        GOSUB BUILD.RECORD
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E
                    END
                END
            END
        NEXT II
    END
    RETURN
*----------------------------------------------------
CHECK.RUN.DAT:

    IF TOD.DAT GE M1 AND TOD.DAT LE M11 THEN
        IF L.RUN.DAT GE M1 AND L.RUN.DAT LE M11 THEN
            FLG = 1
        END
    END

    IF TOD.DAT GE M2 AND TOD.DAT LE M22 THEN
        IF L.RUN.DAT GE M2 AND L.RUN.DAT LE M22 THEN
            FLG = 2
        END
    END

    IF TOD.DAT GE M3 AND TOD.DAT LE M33 THEN
        IF L.RUN.DAT GE M3 AND L.RUN.DAT LE M33 THEN
            FLG = 3
        END
    END

    IF TOD.DAT GE M4 AND TOD.DAT LE M44 THEN
        IF L.RUN.DAT GE M4 AND L.RUN.DAT LE M44 THEN
            FLG = 4
        END
    END
    FLG = 0
    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA     = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :CURR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :KEY.LIST<II>:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK="       :"SCB":COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.CUST="       :"SCB":COMMA

    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :KEY.LIST<II>:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACC:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :CURR:COMMA
    AMOUNT = DROUND(AMOUNT,'2')

    OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :AMOUNT:COMMA

    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY
*---------------------------------
    IF PROF.CENT.DPT NE '' THEN
        OFS.MESSAGE.DATA := COMMA:"PROFIT.CENTRE.DEPT=": PROF.CENT.DPT
        PROF.CENT.DPT  = ""
    END
*---------------------------------
    ZZZ   = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    WRITESEQ ZZZ TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZZ
    END

***** SCB R15 UPG 20160628 - S
*    OFS.REC = ZZZ
*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*    OFS.REC = ''
***** SCB R15 UPG 20160628 - E

    RETURN
*-------------------------------------------------------
END
