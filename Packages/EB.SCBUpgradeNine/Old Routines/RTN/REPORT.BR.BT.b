* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>470</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.BR.BT

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.USER
    $INSERT T24.BP  I_F.BILL.REGISTER
    $INSERT T24.BP  I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP  I_F.CUSTOMER
    $INSERT T24.BP  I_F.CURRENCY
    $INSERT T24.BP  I_F.CATEGORY
    $INSERT T24.BP  I_F.ACCOUNT
    $INSERT TEMENOS.BP I_F.SCB.BT.BATCH
    $INSERT TEMENOS.BP I_F.SCB.BANK
    $INSERT TEMENOS.BP I_F.SCB.BANK.BRANCH
    $INSERT TEMENOS.BP I_F.SCB.BR.SLIPS
    $INSERT TEMENOS.BP I_CU.LOCAL.REFS
    $INSERT TEMENOS.BP I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.BR.BT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    FN.BATCH = 'F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    BT.ID    = ID.NEW
*------------------------------------------------------------------------
    BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)
*Line [ 62 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD    = DCOUNT(BR.ID,@VM)

    FOR I = 1 TO DD
        RET.RES = R.NEW(SCB.BT.RETURN.REASON)<1,I>
        IF RET.RES EQ '' THEN
            BR.ID1 = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>

            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
            BS.NUM       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.SLIP.REFER>
            CALL DBR('SCB.BR.SLIPS':@FM:SCB.BS.OPERATION.TYPE,BS.NUM,BS.TYPE)

            DRAWER.ID    = R.BR<EB.BILL.REG.DRAWER>
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME.2  = LOCAL.REF<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>

            FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
                CUST.ADDRESS1 = "���� ��������� ������"
            END ELSE
                CUST.ADDRESS1 =LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            END

            ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
            CHQ.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>
            BANK         = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            BANK.BRAN    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>
            DRNAME       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.DR.NAME>
            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,BANK,BNAME)
            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME,BANK.BRAN,BRNAME)
*            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
            CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
            BRN.ID  = AC.COMP[2]
            BRANCH.ID = TRIM(BRN.ID,"0","L")

            CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

            AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT = AMOUNT

            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>

            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT  = TODAY
********************edited by mahmoud magdy 2018/6
*            CALL CDT("",DAT,'+1W')
******************end of edition***************************

            MAT.DATE = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

            INPUTTER = R.NEW(SCB.BT.INPUTTER)
            AUTH     = R.NEW(SCB.BT.AUTHORISER)
            INP      = FIELD(INPUTTER,'_',2)
            AUTHI    = FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX11  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX30  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,35]   = CUST.NAME:' ':CUST.NAME.2
            XX1<1,1>[3,35]  = CUST.ADDRESS1

            XX<1,1>[45,15]  = '������     : '
            XX<1,1>[59,15]  = AMOUNT

            XX1<1,1>[45,15] = '��� ������ : '
            XX1<1,1>[59,15] = ACC.NO

            XX2<1,1>[45,15] = '��� ������ : '
            XX2<1,1>[59,15] = CATEG

            XX3<1,1>[45,15] = '������     : '
            XX3<1,1>[59,15] = CUR

            XX4<1,1>[45,15] = '����� ���� : '
            XX4<1,1>[59,15] = MAT.DATE

            XX5<1,1>[3,15]  = '��� �����      : '
            XX5<1,1>[20,15] = CHQ.NO

            XX5<1,1>[45,15]  = '��� �������    : '
            XX5<1,1>[59,15] = BR.ID1

            XX11<1,1>[3,15]  = '����� ������� ����: '
            XX11<1,1>[30,15] = BNAME

            XX11<1,1>[50,15]  = '��� ����� ������� ����: '
            XX11<1,1>[66,15] = BRNAME

            XX30<1,1>[3,15]  = '��� ������: '
            XX30<1,1>[30,15] = DRNAME

            XX6<1,1>[1,15]  = '������'
            XX7<1,1>[1,15] = AUTHI

            XX6<1,1>[30,15]  = '��� �������'
            XX7<1,1>[60,15] = BT.ID

            XX6<1,1>[60,15]  = '������'
            XX7<1,1>[35,15] = INP

            XX8<1,1>[3,35]  = '������ ������� : '
            XX8<1,1>[20,15] = OUT.AMT

            XX9<1,1>[3,15]  = '������         : '
            XX9<1,1>[20,15] = BS.TYPE

            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            IF I GT 1 THEN
                PR.HD :="'L'":"PAGE-ONE"
            END
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN
            PR.HD :="'L'":" "
            PR.HD :="'L'":SPACE(50) : "����� �����"
            PR.HD :="'L'":"REPORT.BR.BT"

            PRINT
            HEADING PR.HD
*--------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT XX11<1,1>
            PRINT XX30<1,1>
            PRINT STR(' ',82)
            PRINT STR(' ',82)
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX5<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR('-',82)
            PRINT XX7<1,1>
        END
    NEXT I
*===============================================================
    RETURN
END
