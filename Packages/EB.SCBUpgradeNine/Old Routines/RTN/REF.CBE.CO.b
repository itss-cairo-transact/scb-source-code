* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*--------------------------------NI7OOOOOOOOOOOOOOO---------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-20</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REF.CBE.CO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.LC = 'FBNK.DRAWINGS' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)
    FN.LC.HIS = 'FBNK.DRAWINGS$HIS' ; F.LC.HIS = ''
    CALL OPF(FN.LC.HIS,F.LC.HIS)


    IF O.DATA[1,2] EQ 'FT' THEN
        ZZ = O.DATA:';1'
        CALL F.READ(FN.FT.HIS,ZZ,R.FT.HIS,F.FT.HIS,E2)
            AC.NO     = R.FT.HIS<FT.CO.CODE>
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,AC.NO,BRANCH)
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSNO)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = BRANCH
        IF (E2) THEN
            ZZZ = O.DATA
            CALL F.READ(FN.FT,ZZZ,R.FT,F.FT,E222)
            AC.NO     = R.FT<FT.CO.CODE>
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,AC.NO,BRANCHH)
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSNO)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = BRANCHH
        END
    END
    IF O.DATA[1,2] EQ 'TF' THEN
        MM=O.DATA[1,14]
        CALL F.READ(FN.LC,MM,R.LC,F.LC,E22)
        IF NOT(E22) THEN
            LC.CUS    = R.LC<TF.DR.CO.CODE>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,LC.CUS,BRANCH.LC)
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS,CUSID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            O.DATA    = BRANCH.LC
        END
        IF E22 THEN
            NN = O.DATA[1,14]:';1'
            CALL F.READ(FN.LC.HIS,NN,R.LC.HIS,F.LC.HIS,E33)
            LC.CUS.H    = R.LC.HIS<TF.DR.CO.CODE>
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,LC.CUS.H,BRANCH.LC.H)
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS.H,CUSID.HIS)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID.HIS,LOCAL.REF.H)
            CUST.NAME.HIS = LOCAL.REF.H<1,CULR.ARABIC.NAME>
            O.DATA    = BRANCH.LC.H
        END
    END

*************************************************************
    RETURN
END
