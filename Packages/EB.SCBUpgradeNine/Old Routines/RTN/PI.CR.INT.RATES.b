* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>663</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE PI.CR.INT.RATES

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PERIODIC.INTEREST
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.TYPE.LEVEL

    GOSUB INITIATE
*Line [ 37 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='PI.CR.INT.RATES'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    YTEXT = "Enter the Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    YYYY = COMI[1,4]
    MM = COMI[5,2]
    DD = COMI[7,2]

    DATFMT = YYYY:"/":MM:"/":DD
    F.PI= '' ; FN.PI = 'F.PERIODIC.INTEREST$NAU' ; R.PI = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.PI,F.PI)

    T.SEL = "SELECT FBNK.PERIODIC.INTEREST$NAU WITH @ID LIKE ...":COMI
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    INP = '' ; AUTH = '' ; INPP = '' ; AUTHH = '' ; INPP.NAME = '' ; AUTHH.NAME = ''
*================================================================
    IF SELECTED THEN

        PRINT SPACE(10):"|******************************************************|"
        PRINT SPACE(10):"|PRINT OUT OF REUTER CREDIT INTEREST VALUE ":DATFMT
        PRINT SPACE(10):"|******************************************************|"
        PRINT
        PRINT SPACE(1):"======================================================================="
        PRINT SPACE(1):"| FIRST   | 1 WEEK  | 1 MONTH | 2 MONTH | 3 MONTH | 6 MONTH | 1 YEAR   "
        PRINT SPACE(1):"======================================================================="
        RR = '' ; RP = '' ; NN = '' ; I = ''  ; CURRTEXT = '' ;  RATSP = '' ; XX= ''
        VAL.WK = '' ; VAL.1M = '' ; VAL.2M = '' ; VAL.3M = '' ; VAL.6M = '' ; VAL.1Y = ''
        FOR RR = 1 TO SELECTED
            VAL.DESC = ''
            CALL F.READ(FN.PI,KEY.LIST<RR>, R.PI, F.PI,E1)
            CURR.PI = KEY.LIST<RR>[3,3]
            TEXT = 'CURR=':CURR.PI ; CALL REM
            CURRTEXT<RR> = CURR.PI :STR(" ",6):"|":" "
            VAL.DESC = CURRTEXT<RR>
            RP = R.PI<PI.REST.PERIOD>
*Line [ 86 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NN = DCOUNT(RP,@VM)
            FOR  I = 1 TO NN
                REST.PER = '' ; BIDR = '' ; BIDRATE = ''
                REST.PER<I> = R.PI<PI.REST.PERIOD,I>
                BIDRATE<I> = R.PI<PI.BID.RATE,I>
                RATSP = STR("0",7 - LEN(BIDRATE<I>))
                BIDR<I> = BIDRATE<I>:RATSP:" |"
                IF  REST.PER<I> = '07D' THEN
                    VAL.WK = BIDR<I>
                END
                IF  REST.PER<I> = '1M' THEN
                    VAL.1M = BIDR<I>
                END
                IF  REST.PER<I> = '2M' THEN
                    VAL.2M = BIDR<I>
                END
                IF  REST.PER<I> = '3M' THEN
                    VAL.3M = BIDR<I>
                END
                IF  REST.PER<I> = '6M' THEN
                    VAL.6M = BIDR<I>
                END
                IF  REST.PER<I> = 'R' THEN
                    VAL.1Y = BIDR<I>
                END
            NEXT I  ;*NEW CATEGORY*

            XX<1,RR>[3,9] = VAL.DESC
            XX<1,RR>[14,10] = VAL.WK
            XX<1,RR>[24,10] = VAL.1M
            XX<1,RR>[34,10] = VAL.2M
            XX<1,RR>[44,10] = VAL.3M
            XX<1,RR>[54,10] = VAL.6M
            XX<1,RR>[64,10] = VAL.1Y

            PRINT  XX<1,RR>
            PRINT SPACE(1):"-----------------------------------------------------------------------"

        NEXT RR     ;*NEW RANGE FOR CURRENCY*

    END   ;*END OF SELECTED
    INP =  R.PI<PI.INPUTTER>
    INPP = FIELD(INP, "_", 2)
    CALL DBR( 'USER':@FM:EB.USE.USER.NAME, INPP, INPP.NAME)
    AUTH = R.PI<PI.AUTHORISER>
    AUTHH = FIELD(AUTH, "_", 2)
    CALL DBR( 'USER':@FM:EB.USE.USER.NAME, AUTHH, AUTT.NAME)
    PRINT
    PRINT
    PRINT
            PRINT SPACE(1):"======================================================================="
            PRINT
            PRINT SPACE(1):"======================================================================="
            PRINT
            PRINT
            PRINT SPACE(20):"DONE BY:" :
            PRINT
            PRINT
            PRINT SPACE(48):"REVISED:"

    RETURN
*===============================================================
END
