* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
************************* NI7OOOOOOOOOOO ******************
*-----------------------------------------------------------------------------
* <Rating>275</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.BR.BT1.OUT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ������� �����" ; CALL REM
    RETURN
*========================================================================
INITIATE:
*--------
    REPORT.ID='REPORT.BR.BT1.OUT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    FN.BATCH = 'F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    AMOUNT   = ''
    FN.BR    = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    FN.BATCH = 'F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX   = SPACE(132)  ; XX2  = SPACE(132)
*------------------------------------------------------------------------
    BR.ID   = R.BATCH<SCB.BT.OUR.REFERENCE>
    COUNTER = 1
*Line [ 91 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD      = DCOUNT(BR.ID,@VM)

    FOR I = 1 TO DD
        BR.ID1 = R.BATCH<SCB.BT.OUR.REFERENCE><1,I>
        BR.ID2 = R.BATCH<SCB.BT.RETURN.REASON><1,I>
        CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)

        ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>
        COMP      = R.BR<EB.BILL.REG.CO.CODE>
*     CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,BRANCH.ID)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        BRN.ID  = AC.COMP[2]
        BRANCH.ID = TRIM(BRN.ID,"0","L")
        CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

        IF ACC.NO[1,3] EQ 'EGP' THEN
            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACC.NO,CUST.NAME)
            XX<1,1>[3,35]   = CUST.NAME
            CATEG.ID        =  ACC.NO[4,8]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
            XX2<1,1>[45,15] = '��� ������ : '
            XX2<1,1>[59,15] = CATEG
        END ELSE

            IF ACC.NO[1,3] NE 'EGP' THEN
                CATEG.ID  = ACC.NO[11,4]
                CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)
                XX2<1,1>[45,15] = '��� ������ : '
                XX2<1,1>[59,15] = CATEG

                DRAWER.ID  = ACC.NO[1,8]
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                XX<1,1>[3,35]   = CUST.NAME
            END
        END

        AMOUNT    = R.BATCH<SCB.BT.TOT.CREDIT.AMT>
        IN.AMOUNT = AMOUNT

        IF IN.AMOUNT = '' THEN
            IN.AMOUNT = 0
        END
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

        CUR.ID  = R.BR<EB.BILL.REG.CURRENCY>

        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

        DAT      = TODAY
        MAT.DATE = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

        INPUTTER = R.BR<EB.BILL.REG.INPUTTER>
        AUTH     = R.BR<EB.BILL.REG.AUTHORISER>
        INP      = FIELD(INPUTTER,'_',2)
        AUTHI    = FIELD(AUTH,'_',2)

        XX   = SPACE(132)  ; XX3  = SPACE(132)
        XX1  = SPACE(132)  ; XX4  = SPACE(132)
        XX2  = SPACE(132)  ; XX5  = SPACE(132)
        XX6  = SPACE(132)  ; XX7  = SPACE(132)
        XX8  = SPACE(132)  ; XX9  = SPACE(132)

        XX<1,1>[3,35]   = CUST.NAME

        XX<1,1>[45,15]  = '������     : '
        XX<1,1>[59,15]  = AMOUNT

        XX1<1,1>[45,15] = '��� ������ : '
        XX1<1,1>[59,15] = ACC.NO

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

        XX3<1,1>[45,15] = '������     : '
        XX3<1,1>[59,15] = CUR

        XX4<1,1>[45,15] = '����� ���� : '
        XX4<1,1>[59,15] = MAT.DATE

        XX5<1,1>[3,15]  = '����� ���� : '
        XX5<1,1>[20,15] = DD:"  ":"�����"


        XX6<1,1>[1,15]  = '������'
        XX7<1,1>[1,15]  = INP

        XX6<1,1>[30,15]  = '��� �������'
        XX7<1,1>[35,15]  = COMI

        XX6<1,1>[60,15]  = '������'
        XX7<1,1>[60,15]  = AUTHI

        XX8<1,1>[3,35]  = '������ ������� : '
        XX8<1,1>[20,15] = OUT.AMT

    NEXT I
*-------------------------------------------------------------------
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����� ���"
    PR.HD :="'L'":"REPORT.BR.BT1.OUT"
    PR.HD :="'L'"
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
