* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>177</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OFS.FATCA.CHRG
*PROGRAM OFS.FATCA.CHRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    TEXT  = "US CUSTOMERS WILL BE CHARGED WITH FATCA CHARGE"; CALL REM
    YTEXT = "1.CONTINUE  2.EXIT"
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    FA.ORD = COMI

    IF FA.ORD = 1 THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
    END
    RETURN
*------------------------------
INITIALISE:
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "FATCA.CHRG"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    OPENSEQ "&SAVEDLISTS&" , "FATCA.CHRG.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"FATCA.CHRG.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "FATCA.CHRG.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FATCA.CHRG.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create FATCA.CHRG.TXT File IN &SAVEDLISTS&'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    FN.CU    = 'FBNK.CUSTOMER'  ; F.CU =''  ; R.CU = '' ; ERR1=''
    CALL OPF(FN.CU,F.CU)
    FN.CU.AC = 'FBNK.CUSTOMER.ACCOUNT'  ; F.CU.AC =''  ; R.CU.AC = '' ; ERR2=''
    CALL OPF(FN.CU.AC,F.CU.AC)
    FN.AC = 'FBNK.ACCOUNT'  ; F.AC =''  ; R.AC = '' ; ERR3=''
    CALL OPF(FN.AC,F.AC)
    FN.CUR = "FBNK.CURRENCY" ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    CREDIT.ACCT = 'PL52216'
    EGP.FLAG = 'N'
    USD.FLAG = 'N'
    SAVE.EGP.FLAG = 'N'
    SAVE.USD.FLAG = 'N'
    FINAL.CURR = ''
********CALC EQU TO $5**************

    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,E3)
    RATE.USD = R.CUR<EB.CUR.MID.REVAL.RATE,1>
    AMT.USD    = 5 * RATE.USD
************************************

    T.SEL = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT LT 90) AND (ACTIVE.CUST EQ 'YES') AND ((NATIONALITY EQ 'US') OR (OTHER.NATIONALITY EQ 'US'))"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            IF USD.FLAG EQ 'Y' THEN
                DEBIT.ACCT   = USD.ACCT
                OFS.TRANS.ID = USD.ACCT
                FINAL.CURR     = USD.CURR
                GOSUB WRITE.OFS
            END

            IF SAVE.USD.FLAG EQ 'Y' THEN
                DEBIT.ACCT = SAVE.USD.ACCT
                OFS.TRANS.ID = SAVE.USD.ACCT
                FINAL.CURR     = USD.CURR
                GOSUB WRITE.OFS
            END

            IF EGP.FLAG EQ 'Y' THEN
                DEBIT.ACCT   = EGP.ACCT
                OFS.TRANS.ID = EGP.ACCT
                FINAL.CURR     = EGP.CURR
                GOSUB WRITE.OFS
            END

            IF SAVE.EGP.FLAG EQ 'Y' THEN
                DEBIT.ACCT = SAVE.EGP.ACCT
                OFS.TRANS.ID = SAVE.EGP.ACCT
                FINAL.CURR     = EGP.CURR
                GOSUB WRITE.OFS
            END
            IF I NE 1 THEN
                BB.DATA  = OLD.CUS:"|":DEBIT.ACCT

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

            END
            DEBIT.ACCT = ''; OFS.TRANS.ID = ''

            CUS.ID = KEY.LIST<I>
            IF I EQ 1 THEN OLD.CUS = CUS.ID

**************GET ALL ACCOUNTS FOR THE CUSTOMER ******

            CALL F.READ(FN.CU.AC,CUS.ID ,R.CU.AC,F.CU.AC,ERR2)
            LOOP
                REMOVE ACC.ID FROM R.CU.AC SETTING POS.ACCT
            WHILE ACC.ID:POS.ACCT

                CALL F.READ(FN.AC,ACC.ID,R.AC,F.AC,ERR3)
                CATEG    = R.AC<AC.CATEGORY>
                CURR     = R.AC<AC.CURRENCY>
                BAL      = R.AC<AC.OPEN.ACTUAL.BAL>

*COMP     = R.AC<AC.CO.CODE>
*COM.CODE = COMP[8,2]

                IF CURR EQ 'EGP' THEN
                    IF (CATEG EQ '1001' AND BAL GE AMT.USD)  THEN
                        EGP.ACCT = ACC.ID
                        EGP.FLAG = 'Y'
                        COMP     = R.AC<AC.CO.CODE>
                        COM.CODE = COMP[8,2]

                    END

                    IF ((CATEG EQ '6501' OR CATEG EQ '6502' OR CATEG EQ '6503' OR CATEG EQ '6504' OR CATEG EQ '6511' OR CATEG EQ '6512') AND BAL GE AMT.USD) THEN
                        SAVE.EGP.ACCT = ACC.ID
                        SAVE.EGP.FLAG = 'Y'
                        COMP     = R.AC<AC.CO.CODE>
                        COM.CODE = COMP[8,2]

                    END
                    EGP.CURR = 'EGP'
                END

                IF CURR EQ 'USD' THEN
                    IF (CATEG EQ '1001' AND BAL GE 5) THEN

                        USD.ACCT = ACC.ID
                        USD.FLAG = 'Y'
                        COMP     = R.AC<AC.CO.CODE>
                        COM.CODE = COMP[8,2]

                    END

                    IF ((CATEG EQ '6501' OR CATEG EQ '6502' OR CATEG EQ '6503' OR CATEG EQ '6504' OR CATEG EQ '6511' OR CATEG EQ '6512') AND BAL GE 5) THEN
                        SAVE.USD.ACCT = ACC.ID
                        SAVE.USD.FLAG = 'Y'
                        COMP     = R.AC<AC.CO.CODE>
                        COM.CODE = COMP[8,2]

                    END
                    USD.CURR = 'USD'
                END

            REPEAT
            OLD.CUS = CUS.ID
        NEXT I
    END
    TEXT = "US CUSTOMERS HAVE CHARGED SUCCESSFULLY"; CALL REM
    RETURN
********************************
WRITE.OFS:
    INP    = "INPUTT":COM.CODE:"/":"/" :COMP
    OFS.USER.INFO = INP


    OFS.ID = "FA-":CUS.ID
    COMMA = ","

    IF FINAL.CURR EQ 'EGP' THEN
        DEBIT.AMT = DROUND(AMT.USD,1) 
    END
    ELSE
        DEBIT.AMT = 5
    END

    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":'AC98':COMMA
    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=":FINAL.CURR:COMMA
    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":FINAL.CURR:COMMA
    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=": DEBIT.ACCT:COMMA
    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":CREDIT.ACCT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=":DEBIT.AMT:COMMA
    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":TODAY:COMMA
    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":TODAY:COMMA
    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":DEBIT.ACCT:COMMA

    F.PATH = FN.OFS.IN

    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
*PRINT OFS.REC
    DAT = TODAY

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN,OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK,OFS.ID:'-': ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    EGP.FLAG = 'N'
    USD.FLAG = 'N'
    SAVE.EGP.FLAG = 'N'
    SAVE.USD.FLAG = 'N'
    FIN.CURR = ''
    RETURN
************************************************************

END
