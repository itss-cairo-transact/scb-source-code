* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************NI7OOOOOOOOOOO**************************
*-----------------------------------------------------------------------------
* <Rating>-119</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.FT.HIS.UPDATE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.FT.HIS.UPDATE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID = COMI
    FN.DR = 'FBNK.FUNDS.TRANSFER$HIS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)
    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)

    TEXT = "COMI = " : COMI ; CALL REM
    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    DATE.TO = TODAY[3,6]:"..."
*------------------------------------------------------------------------
    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11  = SPACE(132)

    DB.ACC = R.DR<FT.DEBIT.ACCT.NO>
    CR.ACC = R.DR<FT.CREDIT.ACCT.NO>
    TEXT = "DB.ACC = " : DB.ACC ; CALL REM
    ACC.NO = DB.ACC
    IF ACC.NO[1,3] NE 'EGP' OR ACC.NO[1,2] NE 'PL' THEN
        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CUS.ID)
        TEXT = "CUS.ID =" : CUS.ID ; CALL REM
        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,ACC.NO,CUR.ID)
        CALL DBR ('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CAT.ID)
        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)

***************** UPDATED BY MAHMOUD 7/3/2016 ****************************
*##        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACC.NO,ACC.BR)
        CALL DBR ('ACCOUNT':@FM:AC.CO.CODE,ACC.NO,AC.COMP)
        ACC.BR = AC.COMP[8,2]
*********************** END OF UPDATE ************************************

        CUST.NAME   = LOCAL.REF<1,CULR.ARABIC.NAME>
        CUST.NAME.2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
        CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
        XX1<1,1>[3,35]   = CUST.NAME:'':CUST.NAME.2
        XX1<1,1>[3,35]   = CUST.ADDRESS

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

    END
    IF ACC.NO[1,2] EQ 'PL' THEN
        CAT.ID = ACC.NO[3,5]
        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CAT.ID,CATEG)
        XX1<1,1>[3,35]   = CATEG
        XX1<1,1>[3,35]   = CATEG

        XX2<1,1>[45,15] = '��� ������ : '
        XX2<1,1>[59,15] = CATEG

    END
    DR.AMOUNT = R.DR<FT.AMOUNT.DEBITED>[4,20]
    CR.AMOUNT = R.DR<FT.AMOUNT.CREDITED>[4,20]
    AMOUNT    = DR.AMOUNT
    IN.AMOUNT = AMOUNT

    IF IN.AMOUNT = 0 THEN
        OUT.AMOUNT = ''
    END

    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    DAT       = R.DR<FT.DEBIT.VALUE.DATE>

    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

****    BR.DATA   = R.DR<TF.DR.LOCAL.REF><1,DRLR.NOTES>
****    REFER     = R.DR<TF.DR.LOCAL.REF><1,DRLR.REFRENCE>
    BR.DATA   = R.DR<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,1>
    BR.DATA2  = R.DR<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,2>
    REFER     = R.DR<FT.DEBIT.THEIR.REF>
    BRANCH1   = R.DR<FT.DEPT.CODE>

**COM.AMT = R.DR<FT.COMMISSION.AMT>
**CHG.AMT = R.DR<FT.CHARGE.AMT>
**TOT.CHG = R.DR<FT.TOTAL.CHARGE.AMOUNT>
**BENCUS  = R.DR<FT.LOCAL.REF><1,FTLR.BENEFICIARY.CUS>

    INPUTT    = R.DR<FT.INPUTTER>
    INP  = FIELD(INPUTT,'_',2)
    AUTH1     = R.DR<FT.AUTHORISER>
    AUTH      = FIELD(AUTH1,'_',2)


*------------------------------------------------------------------------

    XX   = SPACE(132)  ; XX3  = SPACE(132)  ; XX20 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11  = SPACE(132)

*  IF CAT.ID EQ 1512 THEN
*      LOAN.AMT = R.NEW(TF.DR.LOCAL.REF)<1,DRLR.MARG.AMOUNT>
*      DAT1= R.NEW(TF.DR.LOCAL.REF)<1,DRLR.NO.DAY>
*      LOAN.DATE = DAT1[7,2]:'/':DAT1[5,2]:"/":DAT1[1,4]
*      XX11<1,1>[3,15]  = '���� �����     : ':' ':LOAN.AMT
*      XX11<1,1>[45,35] = '������ ����� �� : ':' ':LOAN.DATE
*  END

**XX1<1,1>[3,35]   = CUST.NAME:'':CUST.NAME.2
**XX1<1,1>[3,35]   = CUST.ADDRESS

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '���� ����� :'
    XX1<1,1>[59,15] = ACC.NO

**XX2<1,1>[45,15] = '��� ������ : '
**XX2<1,1>[59,15] = CATEG

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX6<1,1>[1,15]  = '������'
    XX6<1,1>[30,15] = '��� �������'
    XX6<1,1>[60,15] = '������'

    XX7<1,1>[1,15]  = INP:' �'
    XX7<1,1>[30,15] = COMI:' �'
    XX7<1,1>[60,15] = AUTH:' �'

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
***XX8<1,1>[20,15] = "��� ������� ����� ������ ��� ����� ���� ����� ��� ���� ���� �����"
    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = BR.DATA
    XX20<1,1>[20,15]= BR.DATA2
    XX10<1,1>[3,15]  = '������    : '
    XX10<1,1>[20,15] = REFER

**XX15<1,1>[3,15]  = ' �������� ��������� :'
**XX16<1,1>[3,15]  = '-----------------------'
**XX17<1,1>[3,15]  = COM.AMT[4,10]
**XX18<1,1>[3,15]  = CHG.AMT[4,10]
**XX19<1,1>[3,15]  = '������ �������� ��������� : ': TOT.CHG[4,10]


*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH1,BRANCH)
    TEXT = "BRANCH = " :BRANCH ; CALL REM
    YYBRN  = FIELD(BRANCH,'.',2)

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":'':'REPORT.FT.HIS.UPDATE'
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":'����� ���':SPACE(10) :'������� :':T.DAY
    PR.HD :="'L'":'-----------------------------------------------------------------------------------'
    PR.HD :="'L'":CUST.NAME:' ' : CUST.NAME.2

    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX20<1,1>
    PRINT XX10<1,1>

    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
