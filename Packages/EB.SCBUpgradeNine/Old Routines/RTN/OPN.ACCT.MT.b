* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>81</Rating>UPDATED BY M.ELSAYED <27/1/2009>
*-----------------------------------------------------------------------------
    SUBROUTINE OPN.ACCT.MT(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = C$ID.COMPANY

    DDD = TODAY[3,6]
    DD = "...":DDD:"..."
    TEXT = DD ; CALL REM
    TEXT = COMP ; CALL REM

    CUS.USR = R.USER<EB.USE.DEPARTMENT.CODE>
    TEXT = CUS.USR ; CALL REM
**** T.SEL = "SELECT FBNK.ACCOUNT WITH DATE.TIME LIKE ": DD :" AND CURR.NO EQ 1 AND ACCOUNT.OFFICER EQ ": CUS.USR :" AND CO.CODE EQ ":COMP
    T.SEL = "SELECT FBNK.ACCOUNT WITH ( CURR.NO EQ 1 OR CURR.NO EQ 2 ) AND CO.CODE EQ ":COMP:" AND DATE.TIME LIKE ...DD... "
***T.SEL = "SELECT FBNK.ACCOUNT WITH CURR.NO EQ 1 AND ACCOUNT.OFFICER EQ 32 AND CO.CODE EQ EG0010032 AND DATE.TIME LIKE ...090623... AND @ID UNLIKE GBP..."


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
*********************
    ENQ.LP   = '' ;
    OPER.VAL = ''
*********************
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP


**********************
    RETURN
END
