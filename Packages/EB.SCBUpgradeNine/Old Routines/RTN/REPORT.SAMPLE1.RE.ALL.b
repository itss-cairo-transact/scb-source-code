* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*---------------------------------------------------------
* <Rating>-2</Rating>
*---------------------------------------------------------
** CRETE BY NI7OOOOOOOOOOOOOOO  ***
** EDIT BY NESSMA ON 2012/05/20 ***
** EDITED AND RE-CREATED BY RIHAM @ 2014/02/12**
*---------------------------------------------------------
    SUBROUTINE REPORT.SAMPLE1.RE.ALL
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STANDARD.SELECTION
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE.INPUT
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*-------------------------------------------------------------------
    AMOUNT       = ""
    OUT.AMOUNT   = ""
    IN.AMOUNT    = ""
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*===================================================================
INITIATE:
    REPORT.ID = 'REPORT.SAMPLE1.RE.ALL'
    CALL PRINTER.ON(REPORT.ID,'')
    DAT.TIM   = ""
    COMP      = ID.COMPANY
    CUR       = ''
    XX1       = ''
    SUP       = ''
    DEPT.NAME = ''
    YY.BRN    = ''
    SADA1     = ''
    AMOUNT    = ''
    OUT.AMT   = ''
    TYPE.HW   = ''
    CUS.HW    = ''
    CUR.HW    = ''
    SADA2     = ''
    SADA2.ACC = ''
    PURPOS    = ''
    PURPOS1   = ''
    PURPOS2   = ''
    PURPOS3   = ''
    PURPOS4   = ''
    PURPOS5   = ''
    PURPOS6   = ''
    PURPOS7   = ''
    PURPOS8   = ''
    PURPOS9   = ''

    NOTES     = ''
    NOTES1    = ''
    NOTES2    = ""
    NOTES3    = ""
    NOTES4    = ""
    NOTES5    = ""
    NOTES6    = ""
    NOTES7    = ""
    NOTES8    = ""
    NOTES9    = ""

    REQ.HW    = ""
    REP.HW    = ""
    REQ.STA   = ""

    NOT.MAR   = ""
    NOT.MAR1  = ""
    NOT.MAR2  = ""
    NOT.MAR3  = ""
    NOT.MAR4  = ""
    NOT.MAR5  = ""
    NOT.MAR6  = ""
    NOT.MAR7  = ""
    NOT.MAR8  = ""
    NOT.MAR9  = ""
    NOT.MAR10  = ""
    NOT.MAR11  = ""
    NOT.MAR12  = ""
    NOT.MAR13  = ""
    NOT.MAR14  = ""
    NOT.MAR15  = ""
    NOT.MAR16  = ""
    NOT.MAR17  = ""
    NOT.MAR18  = ""
    NOT.MAR19  = ""
    NOT.MAR20  = ""

    BRANCH.ID       = ""
    NOTES.TWSYA     = ""
    NOTES.TWSYA.MAR = ""

    FN.LD  = 'F.SCB.DEPT.SAMPLE1' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.INP = 'F.SCB.DEPT.SAMPLE.INPUT' ; F.INP = ''
    CALL OPF(FN.INP,F.INP)

    FN.SS = 'F.STANDARD.SELECTION' ; F.SS = ''
    CALL OPF(FN.SS,F.SS)

    XX1   = SPACE(132) ; XX2  = SPACE(132)  ; XX3  = SPACE(132)
    XX4   = SPACE(132) ; XX5  = SPACE(132)  ; XX6  = SPACE(132)
    XX7   = SPACE(132) ; XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11 = SPACE(132)  ; XX12 = SPACE(132)
    XX13  = SPACE(132) ; XX14 = SPACE(132)  ; XX15 = SPACE(132)
    XX16  = SPACE(132) ; XX17 = SPACE(132)  ; XX18 = SPACE(132)
    XX19  = SPACE(132) ; XX20 = SPACE(132)  ; XX21 = SPACE(132)
    XX22  = SPACE(132) ; XX23 = SPACE(132)  ; XX24 = SPACE(132)
    XX25  = SPACE(132) ; XX26 = SPACE(132)  ; XX27 = SPACE(132)
    XX28  = SPACE(132) ; XX29 = SPACE(132)  ; XX30 = SPACE(132)
    XX31  = SPACE(132) ; XX32 = SPACE(132)  ; XX33 = SPACE(132)
    XX34  = SPACE(132) ; XX35 = SPACE(132)  ; XX36 = SPACE(132)
    XX37  = SPACE(132) ; XX38 = SPACE(132)  ; XX39 = SPACE(132)
    XX40  = SPACE(132) ; XX41 = SPACE(132)  ; XX42 = SPACE(132)
    XX43  = SPACE(132) ; XX44 = SPACE(132)  ; XX45 = SPACE(132)
    XX46  = SPACE(132) ; XX47 = SPACE(132)  ; XX48 = SPACE(132)
    XX49  = SPACE(132) ; XX50 = SPACE(132)  ; XX51 = SPACE(132)
    XX52  = SPACE(132) ; XX53 = SPACE(132)  ; XX54 = SPACE(132)
    XX55  = SPACE(132) ; XX56 = SPACE(132)  ; XX57 = SPACE(132)
    XX58  = SPACE(132) ; XX59 = SPACE(132)  ; XX60 = SPACE(132)
    RETURN
*=====================================================================
PROCESS:
*--------
    CALL F.READ(FN.SS,'SCB.DEPT.SAMPLE1',R.SS,F.SS,E1.SS)
    SS.FIELD.NAME = R.SS<SSL.SYS.FIELD.NAME>
    SS.FIELD.NO   = R.SS<SSL.SYS.FIELD.NO>

    YTEXT = "Enter the CL No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.LD,COMI,R.LD,F.LD,E1)
    COMI.ID = COMI
    DAT.TIM = R.LD<DEPT.SAMP.DATE.TIME>
*----------------------------------------------------------------------
    IF R.LD<DEPT.SAMP.DEPT.NO.WH> NE '' THEN
        NAME.DESC = "WH"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END

    IF R.LD<DEPT.SAMP.DEPT.NO.HWALA> NE '' THEN
        NAME.DESC = "HWALA"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.LG11> NE '' THEN
        NAME.DESC = "LG11"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.LG22> NE '' THEN
        NAME.DESC = "LG22"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.TEL> NE '' THEN
        NAME.DESC = "TEL"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.TF1> NE '' THEN
        NAME.DESC = "TF1"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.TF2> NE '' THEN
        NAME.DESC = "TF2"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.TF3> NE '' THEN
        NAME.DESC = "TF3"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.BR> NE '' THEN
        NAME.DESC = "BR"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    IF R.LD<DEPT.SAMP.DEPT.NO.AC> NE '' THEN
        NAME.DESC = "AC"
        GOSUB CALC.OTHER
        GOSUB PRINT.HEAD
        GOSUB PRINT.PAGE
        GOSUB PRINT.PROCESS.2
        GOSUB PRINT.AUTH
    END
    RETURN
*--------------------------------------------------------------------
CALC.OTHER:
*----------
    XX1   = SPACE(132) ; XX2  = SPACE(132)  ; XX3  = SPACE(132)
    XX4   = SPACE(132) ; XX5  = SPACE(132)  ; XX6  = SPACE(132)
    XX7   = SPACE(132) ; XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11 = SPACE(132)  ; XX12 = SPACE(132)
    XX13  = SPACE(132) ; XX14 = SPACE(132)  ; XX15 = SPACE(132)
    XX16  = SPACE(132) ; XX17 = SPACE(132)  ; XX18 = SPACE(132)
    XX19  = SPACE(132) ; XX20 = SPACE(132)  ; XX21 = SPACE(132)
    XX22  = SPACE(132) ; XX23 = SPACE(132)  ; XX24 = SPACE(132)
    XX25  = SPACE(132) ; XX26 = SPACE(132)  ; XX27 = SPACE(132)
    XX28  = SPACE(132) ; XX29 = SPACE(132)  ; XX30 = SPACE(132)
    XX31  = SPACE(132) ; XX32 = SPACE(132)  ; XX33 = SPACE(132)
    XX34  = SPACE(132) ; XX35 = SPACE(132)  ; XX36 = SPACE(132)
    XX37  = SPACE(132) ; XX38 = SPACE(132)  ; XX39 = SPACE(132)
    XX40  = SPACE(132) ; XX41 = SPACE(132)  ; XX42 = SPACE(132)
    XX43  = SPACE(132) ; XX44 = SPACE(132)  ; XX45 = SPACE(132)
    XX46  = SPACE(132) ; XX47 = SPACE(132)  ; XX48 = SPACE(132)
    XX49  = SPACE(132) ; XX50 = SPACE(132)  ; XX51 = SPACE(132)
    XX52  = SPACE(132) ; XX53 = SPACE(132)  ; XX54 = SPACE(132)
    XX55  = SPACE(132) ; XX56 = SPACE(132)  ; XX57 = SPACE(132)
    XX58  = SPACE(132) ; XX59 = SPACE(132)  ; XX60 = SPACE(132)
*---------------------------------------------------------
    XX  = "DEPT.NO.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        DEPT.NO = R.LD<XX1>
        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEPT.NO,DEPT.NAME)
    END

    XX  = "BRANCH.NO.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1       = SS.FIELD.NO<1,FLD.LVL,1>
        BRANCH.ID = R.LD<XX1>
    END

    XX  = "NAMES1.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1   = SS.FIELD.NO<1,FLD.LVL,1>
        SADA1 = R.LD<XX1>
    END

    XX  = "AMT.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1 = SS.FIELD.NO<1,FLD.LVL,1>
        AMOUNT    = R.LD<XX1>
        IN.AMOUNT = AMOUNT
    END

    XX = "TYPE.AMT.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        TYPE.HW = R.LD<XX1>
    END

    XX = "CURRENCY.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1    = SS.FIELD.NO<1,FLD.LVL,1>
        CUR.HW = R.LD<XX1>
        CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.HW,CUR)
    END

    XX = "CUS.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        CUS.HW  = R.LD<XX1>
        CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'
    END

    IF NAME.DESC NE "LG11" AND NAME.DESC NE "LG22" THEN
        XX    = "SUP.":NAME.DESC
        LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
            XX1 = SS.FIELD.NO<1,FLD.LVL,1>
            SUP = R.LD<XX1>
        END
    END

    XX = "NAMES2.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1 = SS.FIELD.NO<1,FLD.LVL,1>
        SADA2 = R.LD<XX1>
    END

    XX = "ACC.NAMES2.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1       = SS.FIELD.NO<1,FLD.LVL,1>
        SADA2.ACC = R.LD<XX1>
    END

    XX = "PURPOSE.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        PURPOS  = R.LD<XX1><1,1>
        PURPOS1 = R.LD<XX1><1,2>
        PURPOS2 = R.LD<XX1><1,3>
        PURPOS3 = R.LD<XX1><1,4>
        PURPOS4 = R.LD<XX1><1,5>
        PURPOS5 = R.LD<XX1><1,6>
        PURPOS6 = R.LD<XX1><1,7>
        PURPOS7 = R.LD<XX1><1,8>
        PURPOS8 = R.LD<XX1><1,9>
        PURPOS9 = R.LD<XX1><1,10>
    END

    XX  = "NOTES.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        NOTES   = R.LD<XX1><1,1>
        NOTES1  = R.LD<XX1><1,2>
        NOTES2  = R.LD<XX1><1,3>
        NOTES3  = R.LD<XX1><1,4>
        NOTES4  = R.LD<XX1><1,5>
        NOTES5  = R.LD<XX1><1,6>
        NOTES6  = R.LD<XX1><1,7>
        NOTES7  = R.LD<XX1><1,8>
        NOTES8  = R.LD<XX1><1,9>
        NOTES9  = R.LD<XX1><1,10>
    END
    XX = "REQUEST.DATE.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1    = SS.FIELD.NO<1,FLD.LVL,1>
        REQ.HW = R.LD<XX1>
    END
    XX = "REPLAY.DATE.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1    = SS.FIELD.NO<1,FLD.LVL,1>
        REP.HW = R.LD<XX1>
    END
    XX = "REQ.STA.":NAME.DESC
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        REQ.STA = R.LD<XX1>
    END
    XX = "NOTES.":NAME.DESC:".MAR"
    LOCATE XX IN SS.FIELD.NAME<1,1> SETTING FLD.LVL THEN
        XX1     = SS.FIELD.NO<1,FLD.LVL,1>
        NOT.MAR   = R.LD<XX1><1,1>
        NOT.MAR1  = R.LD<XX1><1,2>
        NOT.MAR2  = R.LD<XX1><1,3>
        NOT.MAR3  = R.LD<XX1><1,4>
        NOT.MAR4  = R.LD<XX1><1,5>
        NOT.MAR5  = R.LD<XX1><1,6>
        NOT.MAR6  = R.LD<XX1><1,7>
        NOT.MAR7  = R.LD<XX1><1,8>
        NOT.MAR8  = R.LD<XX1><1,9>
        NOT.MAR9  = R.LD<XX1><1,10>
        NOT.MAR10 = R.LD<XX1><1,11>
        NOT.MAR11 = R.LD<XX1><1,12>
        NOT.MAR12 = R.LD<XX1><1,13>
        NOT.MAR13 = R.LD<XX1><1,14>
        NOT.MAR14 = R.LD<XX1><1,15>
        NOT.MAR15 = R.LD<XX1><1,16>
        NOT.MAR16 = R.LD<XX1><1,17>
        NOT.MAR17 = R.LD<XX1><1,18>
        NOT.MAR18 = R.LD<XX1><1,19>
        NOT.MAR19 = R.LD<XX1><1,20>
        NOT.MAR20 = R.LD<XX1><1,21>
    END
    RETURN
*-----------------------------------------------------------------
PRINT.HEAD:
*----------
    YYBRN  = ""
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD = "'L'":"������� : ":T.DAY:"               ":"��� ������� :" : COMI
    PR.HD :="'L'":"��� ������ �������� "
    PR.HD :="'L'":"REPORT.SAMPLE1.RE.ALL"
    PRINT
    HEADING PR.HD
    RETURN
*------------------------------------------------------------------
PRINT.PAGE:
*-----------
    XX1<1,1>[3,15]     =  '���  :'
    XX1<1,1>[20,15]    =  DEPT.NAME

    XX1<1,1>[40,10]    =  '��� : '
    XX1<1,1>[50,80]    =  BRANCH.ID

    XX2<1,1>[3,15]     =  '������ / '
    XX2<1,1>[30,15]    =  SADA1

    AMOUNT = FMT(AMOUNT, "L2,")
    XX3<1,1>[3,30]     =   '�����  ' : AMOUNT
    XX3<1,1>[40,100]   =  '��� ����� : ' :  OUT.AMT
    XX4<1,1>[3,50]     =  '������ : ' :CUS.HW

    XX5<1,1>[3,30]     =  '��� ����� :' : TYPE.HW
    XX5<1,1>[50,100]    =  '������ : ' : CUR
    XX6<1,1>[3,50]     =  '���� : ' :SUP
    XX7<1,1>[3,90]     =  ' ��� ���� ������ / ' : SADA2
    XX8<1,1>[3,45]     =  ' ���� ��� :  '  : SADA2.ACC
    CUS.NAME = ""
    CUS.ID   = ""
    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,SADA2.ACC,CUS.ID)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,CUS.LOCAL)
    CUS.NAME = CUS.LOCAL<1,CULR.ARABIC.NAME>
    XX8<1,1>[50,100]  =  "��� ������" : ":  " : CUS.NAME
    XX10<1,1>[3,80]  = '���� : '
    XX10<1,1>[15,80] = PURPOS
    XX11<1,1>[15,80] = PURPOS1
    XX12<1,1>[15,80] = PURPOS2

    XX13<1,1>[3,40]  =  '����� ������� ������ ����� �����'
    XX14<1,1>[3,40]  =  '������� ����� : '
    XX15<1,1>[20,40] = NOTES
    XX16<1,1>[20,40] = NOTES1
    XX17<1,1>[20,40] = NOTES2
    XX18<1,1>[20,40] = NOTES3
    XX19<1,1>[20,40] = NOTES4
    XX20<1,1>[20,40] = NOTES5
*-----
** ���� �����
*-----
    REQ.HW = FMT(REQ.HW , "####/##/##")
    REP.HW = FMT(REP.HW , "####/##/##")

    XX21<1,1>[3,30]   =  '����� ����� : ' : REQ.HW
    XX21<1,1>[45,60]  =  "����� ����� : " : DAT.TIM<1,4>[7,2]:":":DAT.TIM<1,4>[9,2]
    XX22<1,1>[3,30]  =  '����� ���� : '  : REP.HW
*Line [ 487 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NNN = DCOUNT(DAT.TIM, @VM) - 1
    XX22<1,1>[45,60]  =  "����� ���� : "  : DAT.TIM<1,2>[7,2]:":":DAT.TIM<1,2>[9,2]
    XX23<1,1>[3,40]  =  '���� ����� : '  : REQ.STA
    XX24<1,1>[3,40]  =  '�� ��������'
    XX25<1,1>[15,60]  =  NOT.MAR
    XX26<1,1>[15,60]  =  NOT.MAR1
    XX27<1,1>[15,60]  =  NOT.MAR2
    XX28<1,1>[15,60]  =  NOT.MAR3
    XX29<1,1>[15,60]  =  NOT.MAR4
    XX30<1,1>[15,60]  =  NOT.MAR5
    XX31<1,1>[15,60]  =  NOT.MAR6
    XX32<1,1>[15,60]  =  NOT.MAR7
    XX33<1,1>[15,60]  =  NOT.MAR8
    XX34<1,1>[15,60]  =  NOT.MAR9
    XX35<1,1>[15,60]  =  NOT.MAR10
    XX36<1,1>[15,60]  =  NOT.MAR11
    XX37<1,1>[15,60]  =  NOT.MAR12
    XX38<1,1>[15,60]  =  NOT.MAR13
    XX39<1,1>[15,60]  =  NOT.MAR14
    XX40<1,1>[15,60]  =  NOT.MAR15
    XX41<1,1>[15,60]  =  NOT.MAR16
    XX42<1,1>[15,60]  =  NOT.MAR17
    XX43<1,1>[15,60]  =  NOT.MAR18
    XX44<1,1>[15,60]  =  NOT.MAR19
    XX45<1,1>[15,60]  =  NOT.MAR20
    XX46<1,1>[3,30]  = "������ ����� :"
    XX47<1,1>[3,30]  = "����� ��������: "
    IF R.LD<DEPT.SAMP.DEPT.NO.FIN> NE '' THEN
        NOTES.TWSYA1     = R.LD<DEPT.SAMP.NOTES.TWSYA><1,1>
        NOTES.TWSYA2     = R.LD<DEPT.SAMP.NOTES.TWSYA><1,2>
        NOTES.TWSYA3     = R.LD<DEPT.SAMP.NOTES.TWSYA><1,3>
        NOTES.TWSYA4     = R.LD<DEPT.SAMP.NOTES.TWSYA><1,4>
        NOTES.TWSYA5     = R.LD<DEPT.SAMP.NOTES.TWSYA><1,5>

        NOTES.TWSYA.MAR1 = R.LD<DEPT.SAMP.NOTES.TWSYA.MAR><1,1>
        NOTES.TWSYA.MAR2 = R.LD<DEPT.SAMP.NOTES.TWSYA.MAR><1,2>
        NOTES.TWSYA.MAR3 = R.LD<DEPT.SAMP.NOTES.TWSYA.MAR><1,3>
        NOTES.TWSYA.MAR4 = R.LD<DEPT.SAMP.NOTES.TWSYA.MAR><1,4>
        NOTES.TWSYA.MAR5 = R.LD<DEPT.SAMP.NOTES.TWSYA.MAR><1,5>

        XX46<1,1>[35,100] = NOTES.TWSYA1  : " " : NOTES.TWSYA2 : " " : NOTES.TWSYA3 : " " : NOTES.TWSYA4 : " " : NOTES.TWSYA5

        XX47<1,1>[35,100] = NOTES.TWSYA.MAR1  : " " : NOTES.TWSYA.MAR2 : " " : NOTES.TWSYA.MAR3 : " " : NOTES.TWSYA.MAR4 : " " : NOTES.TWSYA.MAR5
    END
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT XX7<1,1>
    PRINT XX8<1,1>
*    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT XX19<1,1>
    PRINT XX20<1,1>
    PRINT XX21<1,1>
    PRINT XX22<1,1>
    PRINT XX23<1,1>
    PRINT XX24<1,1>
    PRINT XX25<1,1>
    PRINT XX26<1,1>
    PRINT XX27<1,1>
    PRINT XX28<1,1>
    PRINT XX29<1,1>
    PRINT XX30<1,1>
    PRINT XX31<1,1>
    PRINT XX32<1,1>
    PRINT XX33<1,1>
    PRINT XX34<1,1>
    PRINT XX35<1,1>
    PRINT XX36<1,1>
    PRINT XX37<1,1>
    PRINT XX38<1,1>
    PRINT XX39<1,1>
    PRINT XX40<1,1>
    PRINT XX41<1,1>
    PRINT XX42<1,1>
    PRINT XX43<1,1>
    PRINT XX44<1,1>
    PRINT XX45<1,1>
    PRINT XX46<1,1>
    PRINT XX47<1,1>
    RETURN
*--------------------------------------------------------------
PRINT.PROCESS.2:
*---------------
    ID = 'SCB.DEPT.SAMPLE1*':COMI:'*...'
    T.SEL  = "SELECT F.SCB.DEPT.SAMPLE.INPUT WITH"
    T.SEL := " @ID LIKE ": ID
    T.SEL := " BY @ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR J = 1 TO SELECTED-1
            CALL F.READ(FN.INP,KEY.LIST<J>,R.INP,F.INP,ERR1)
            INP  = R.INP<SAMP.INPUT>
            AUTH = R.INP<SAMP.AUTH>

            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,INP,INP.DEPT)
            CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,AUTH,AUTH.DEPT)

            CALL DBR('USER':@FM:EB.USE.USER.NAME,INP,INP.NAME)
            CALL DBR('USER':@FM:EB.USE.USER.NAME,AUTH,AUTH.NAME)

            XX54<1,J>[3,50]  = "���� : ":INP.NAME
            XX54<1,J>[50,100]  = "����  ":AUTH.NAME
            PRINT XX54<1,J>

        NEXT J
        XX54 = ""
        XX55 = ""
        CALL F.READ(FN.INP,KEY.LIST<SELECTED>,R.INP,F.INP,ERR3)
        AUTH = R.INP<SAMP.AUTH>
        CALL DBR('USER':@FM:EB.USE.DEPARTMENT.CODE,AUTH,AUTH.DEPT)
        CALL DBR('USER':@FM:EB.USE.USER.NAME,AUTH,AUTH.NAME)
        XX55<1,SELECTED>[3,30]  = "����  ":AUTH.NAME
        PRINT XX55<1,SELECTED>
    END
    RETURN
*------------------------------------------------------------------
PRINT.AUTH:
*----------
    XX57  = SPACE(30)
    CL.NO = COMI
    CALL F.READ(FN.LD,COMI,R.CL,F.LD,E1)
    OVER.MSG = R.CL<DEPT.SAMP.OVERRIDE>
    USER.ID  = FIELD(OVER.MSG,"*MAR2*",2)
    CALL DBR('USER':@FM:EB.USE.USER.NAME,USER.ID,USER.NAME)
    XX57<1,1>[3,30] = "����  " : USER.NAME
    RETURN
END
