* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE REH1(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.ACC.CL='FBNK.ACCOUNT.CLOSURE'
    F.ACC.CL=''
    R.ACC.C=''
    ACC.CL.ERR=''
    FN.ACC='FBNK.ACCOUNT$HIS'
    F.ACC=''
    R.ACC=''
    ACC.ERR=''
    CALL OPF(FN.ACC.CL,F.ACC.CL)
    CALL OPF(FN.ACC,F.ACC)
    DD = TODAY[3,6]
    DDD = "...":DD:"..."
    TEXT = "DDD":  DDD ; CALL REM
    T.SEL = "SELECT FBNK.ACCOUNT.CLOSURE WITH DATE.TIME LIKE " :DDD
    CALL EB.READLIST(T.SEL,SEL.LIST,'',NO.OF.REC,Y.ERR)
    TEXT = "NO.OF.SEL" : NO.OF.REC ; CALL REM
    FOR I = 1 TO NO.OF.REC
        CALL F.READ(FN.ACC,SEL.LIST<I>,R.ACC,F.ACC,ACC.ERR)
        TEXT = "SEL.LIST<I>" : SEL.LIST<I> ; CALL REM
        CATEG= R.ACC<AC.CATEGORY>
        IF CATEG GE 6501 OR CATEG LE 6504 THEN
            TEXT = "HHHH" ; CALL REM
            ENQ.DATA<2,I> = "@ID"
            ENQ.DATA<3,I> = "EQ"
            ENQ.DATA<4,I> = SEL.LIST<I>
        END
    NEXT I
    RETURN
END
