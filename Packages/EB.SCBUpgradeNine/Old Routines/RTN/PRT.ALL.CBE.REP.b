* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    SUBROUTINE PRT.ALL.CBE.REP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

*-----------------------------------
    CALL "SBM.C.PRT.1600.9.BR"
    CALL "SBM.C.PRT.1600.12.BR"
    CALL "SBM.C.PRT.1600.10.BR"
    CALL "SBM.C.PRT.1600.11.BR"
    CALL "SBM.C.PRT.1600.6.BR"
*-----------------------------------
    CALL "SBM.C.PRT.1600.7.BR"
    CALL "SBM.C.PRT.1600.8.BR"
    CALL "SBM.C.PRT.1900.1.BR"
    CALL "SBM.C.PRT.1900.1A.BR"
    CALL "SBM.C.PRT.1610.1.BR"
*-----------------------------------
    CALL "SBM.C.PRT.1620.1.BR"
    CALL "SBM.C.PRT.1620.2.BR"
    CALL "SBM.C.PRT.1621.1.BR"
    CALL "SBM.C.PRT.2100.1.BR"
    CALL "SBM.C.PRT.2100.2.BR"
*-----------------------------------
    CALL "SBM.C.PRT.2000.1.BR"
    CALL "SBM.C.PRT.2000.2.BR"
    CALL "SBM.C.PRT.2000.3.BR"
    CALL "SBM.C.PRT.2000.4.BR"
*-----------------------------------


    RETURN
END
