* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE OUT.AUTHORIZE.LD(OUT.ERR)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.ERROR
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*    DEBUG
    IDDD="EG0010001"
    CALL DBR('DATES':@FM:EB.DAT.NEXT.WORKING.DAY,IDDD,N.W.DAY)

    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

    FN.OFS.IN = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.OUT = OFS.SOURCE.REC<OFS.SRC.OUT.QUEUE.DIR>
    F.OFS.IN = 0

    OFS.REC = ""
    OFS.OPERATION = "LD.LOANS.AND.DEPOSITS"
    OFS.OPTIONS = "SCB.OPEN.OFS/A"
    OFS.TRANS.ID = ""
    OFS.MESSAGE.DATA = ""

    COMMA = ","

    FIRST.FIELD=''
    VALID.TRANS=''
    TEMP.REN.DATE = FIELD(OUT.ERR,"LD.RENEW.DATE",2)
    FIRST.FIELD = FIELD(OUT.ERR,",",1)
    VALID.TRANS = FIELD(FIRST.FIELD,"/",3)
    TMP.REN.DATE = FIELD(TEMP.REN.DATE,",",1)
    REN.DATEF = FIELD(TMP.REN.DATE,":",3)
    REN.DATE = FIELD(REN.DATEF,"=",2)
    PRINT "BAKRYYYYY ":"-":TMP.REN.DATE:" - ":REN.DATEF:" - ":REN.DATE
    Z = FIELD(OUT.ERR,"/",1)
    IF VALID.TRANS EQ '1' AND REN.DATE = TODAY THEN
        Z = FIELD(OUT.ERR,"/",1)
        CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.CATEGORY,Z,CATEG)
        IF NOT(CATEG) THEN
            CALL DBR('LD.LOANS.AND.DEPOSITS$NAU':@FM:LD.CO.CODE,Z,COMP)
            COM.CODE = COMP[8,2]
*            OFS.USER.INFO = "AUTHOR":COM.CODE:"/":"/" :COMP
*            OFS.USER.INFO = "AUTO.CHRGE":"/":"/" :COMP
            OFS.USER.INFO = "AUTO.LD":"/":"/" :COMP
*
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:Z
            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
*Line [ 83 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            WRITE OFS.REC ON F.OFS.IN, Z:"-":N.W.DAY:"-REN-A-":COMP ON ERROR NULL
***** SCB R15 UPG 20160628 - S
*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
***** SCB R15 UPG 20160628 - E
        END
    END

    RETURN
END
