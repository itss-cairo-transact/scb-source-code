* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
***************************NI7OOOOOOOOOOO**************************
*-----------------------------------------------------------------------------
* <Rating>-100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.SF.COM.RE

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.USER
    $INSERT T24.BP  I_F.USER.SIGN.ON.NAME
    $INSERT T24.BP  I_F.DRAWINGS
    $INSERT T24.BP  I_F.ACCOUNT
    $INSERT T24.BP  I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP  I_F.FUNDS.TRANSFER
    $INSERT T24.BP  I_F.CUSTOMER
    $INSERT T24.BP  I_F.CURRENCY
    $INSERT T24.BP  I_F.CATEGORY
    $INSERT            I_F.SCB.BT.BATCH
    $INSERT            I_CU.LOCAL.REFS
    $INSERT            I_BR.LOCAL.REFS
    $INSERT            I_DR.LOCAL.REFS
    $INSERT            I_FT.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� �������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID = 'REPORT.SF.COM.RE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    FN.DR = 'FBNK.FUNDS.TRANSFER$HIS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    YTEXT = "Enter the FT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.DR,COMI,R.DR,F.DR,E1)
*------------------------------------------------------------------------
    COM.AMT   = R.DR<FT.COMMISSION.AMT>
    CUR.ID    = COM.AMT[1,3]
    AMOUNT    = COM.AMT[4,20]
    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
    OUT.AMT   = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    DAT       = R.DR<FT.CREDIT.VALUE.DATE>
    MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
    BRANCH1   = R.DR<FT.DEPT.CODE>

    INPUTT     = ""
    INP        = ""
    AUTH1      = ""
    AUTHI      = ""
*------------------------------------------------------------------------
    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)
    XX10  = SPACE(132) ; XX11  = SPACE(132)

    XX1<1,1>[3,35]  = ""
    XX1<1,1>[3,35]  = ""

    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������:'
    XX1<1,1>[59,15] = "EGP1620500010099"

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = '������ ����� ������ ��� ������� �������'

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CUR

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX6<1,1>[1,15]  = '������'
    XX6<1,1>[30,15] = '��� �������'
    XX6<1,1>[60,15] = '������'

    XX7<1,1>[1,15]  = INP:' �'
    XX7<1,1>[30,15] = COMI:' �'
    XX7<1,1>[60,15] = AUTHI:' �'

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT
*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH1,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)

    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":'����� �����':SPACE(10) :'������� :':T.DAY
    PR.HD :="'L'":'-------------------------------------------------------------------'
    PR.HD :="'L'":"SF.COM"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT XX4<1,1>
    PRINT XX11<1,1>
    PRINT STR(' ',82)
    PRINT XX8<1,1>

    PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
