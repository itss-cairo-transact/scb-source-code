* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
********************************NI7OOOOOOOOOOOOOOOOOOOOO**********************
    SUBROUTINE REPORT.CD.NEW.2.NEW

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST

    COMP = ID.COMPANY
    R.VERSION.CONTROL = 'LD.LOANS.AND.DEPOSITS'
*------------------------------------------------------------------------
    CDNO  = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
*    IF CDNO NE '' THEN

    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

INITIATE:
    REPORT.ID='REPORT.CD.NEW.2.NEW'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------

    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    FN.CORD = 'FBNK.BASIC.INTEREST' ;F.CORD = '' ; R.CORD = ''
    CALL OPF(FN.CORD,F.CORD)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    DATE1 = ''
    DATE2 = ''
    XDATE = ''
    VVV   = ''
    RATENO1 = '' ; CDNO1 = '' ; AMTALL1 = ''
    RATENO3 = '' ; CDNO3 = '' ; AMTALL3 = ''

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*------------------------------------------------------------------------
    AC.DEBIT = R.NEW(LD.DRAWDOWN.ACCOUNT)
    ACLD     = R.NEW(LD.PRIN.LIQ.ACCT)
    ACINT    = R.NEW(LD.INT.LIQ.ACCT)

    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.DEBIT,CUS.ID)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
    CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

    IF CUST.ADDRESS EQ  " " THEN
        CUST.ADDRESS = "���� ��������� ������"
    END
    CATEG   = R.NEW(LD.CATEGORY)
    CDNO  = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.QUANTITY>
**  CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG,CATEGNAME)
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CATEGNAME1)
    CATEGNAME = FIELD(CATEGNAME1,'-',1)

    AMT     = R.NEW(LD.AMOUNT)
    CUR     = R.NEW(LD.CURRENCY)
    AMTALL  =  AMT
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR,CUR22)
    DATE1   = R.NEW(LD.VALUE.DATE)
    DATE2   = R.NEW(LD.LOCAL.REF)<1,LDLR.APPROVAL.DATE>
    IF DATE2 LT DATE1 THEN
        XDATE =  DATE2[1,4]:'/':DATE2[5,2]:"/":DATE2[7,2]
    END ELSE
        IF DATE2 GT DATE1 THEN
            XDATE =  DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
        END
    END
    IF DATE2  = '' THEN
        XDATE = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]
    END
    FINDATE1  = R.NEW(LD.FIN.MAT.DATE)
    FINDATE   = FINDATE1[1,4]:'/':FINDATE1[5,2]:"/":FINDATE1[7,2]
    NAME11    = R.NEW(LD.LOCAL.REF)<1,LDLR.IN.RESPECT.OF>
    CDTYPE    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>
    RAT       = R.NEW(LD.INTEREST.RATE)
    RAT2      = R.NEW(LD.INTEREST.KEY)
    RATE3     = RAT2:'...'
    T.SEL     = "SELECT FBNK.BASIC.INTEREST WITH @ID LIKE ":RATE3:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.CORD,KEY.LIST<SELECTED>,R.CORD,F.CORD,READ.ERR)
*************RATEF     = R.CORD<EB.BIN.INTEREST.RATE>
    RATEF     = RAT
    CD.TYP    = R.NEW(LD.LOCAL.REF)<1,LDLR.CD.TYPE>

    BRANCH.ID = R.NEW(LD.MIS.ACCT.OFFICER)
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
    CALL DBR('SCB.CD.TYPES':@FM:CD.TYPES.DESCRIPTION,CD.TYP,DESC)
    TYP     = FIELD(DESC,'-',2)
    TYPE1   = FINDATE
    TYPENEW = FIELD(CD.TYP,'-',3)
    TEXT = "TYPENEW: " : TYPENEW ; CALL REM
* RATENO = RATEF:" ":"%":"����":" ":TYP
    RATENO = RATEF:" ":"%"

    IF TYPENEW EQ '1M' THEN
        VVV = "X1"
    END
    IF TYPENEW EQ '3M' THEN
        VVV = "X3"
    END

    IN.AMOUNT = AMT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT = OUT.AMOUNT : ' ' : CUR22 : ' ' : '�����'

*  IF TYPENEW EQ '1M' THEN
    RATENO1 = RATENO
    RATENO3 = ''
*  END
*  IF TYPENEW EQ '3M' THEN
    RATENO3 = RATENO
    RATENO1 = ''
*  END

*  IF TYPENEW EQ '1M' THEN
    CDNO1 = CDNO
    CDNO3 = ''
*  END
*  IF TYPENEW EQ '3M' THEN
    CDNO3 = CDNO
    CDNO1 = ''
*  END

* IF TYPENEW EQ '1M' THEN
    AMTALL1 = AMTALL
    AMTALL3 = ''
* END
* IF TYPENEW EQ '3M' THEN
    AMTALL3 = AMTALL
    AMTALL1 = ''
* END


    XX   = SPACE(132)  ; XX3  = SPACE(132) ; XX13 = SPACE(132) ; XX14 = SPACE(132)  ; XX24 = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132) ; XX15 = SPACE(132)  ; XX23 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132) ; XX16 = SPACE(132)  ; XX22 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132) ; XX19 = SPACE(132)  ; XX20 = SPACE(132)
    XX30 = SPACE(132)  ; XX31 = SPACE(132)

    XX30<1,1>[3,15]      =  '��� ������� :'
    XX30<1,1>[45,15]     = ID.NEW

    XX<1,1>[3,15]      =  '����� :'
    XX<1,1>[45,15]     = NAME11

    XX2<1,1>[3,15]     = '��� ������ : '
    XX2<1,1>[45,35]    = ACLD

    XX3<1,1>[3,15]     = '���� �������: '
    XX3<1,1>[45,35]    = AMT
    XX11<1,1>[45,35]   = OUT.AMT

    XX10<1,1>[3,15]     = '��� ������� :'
    XX10<1,1>[45,35]    =  CATEGNAME

    XX4<1,1>[3,15]     = '������ ����:'
    XX4<1,1>[45,15]    = RATENO1

    XX15<1,1>[3,15]     = '������3 ����:'
    XX15<1,1>[45,15]    = RATENO3

    XX20<1,1>[3,15]     = '��� �������� ����:'
    XX20<1,1>[45,15]    = ''

    XX19<1,1>[3,15]     = '��� �������� 3 ���� :'
    XX19<1,1>[45,15]    = CDNO3

    XX22<1,1>[3,15]     = '���� �������� ����:'
    XX22<1,1>[45,15]    = AMTALL1

    XX23<1,1>[3,15]     = '���� �������� 3 ����:'
    XX23<1,1>[45,15]    = AMTALL3

    XX13<1,1>[3,15]     = '����� ������ ������ ��� ����:'
    XX13<1,1>[45,35]    =  ACINT

    XX14<1,1>[3,15]     = '����� ���� ������� ��� ���� ��� :'
    XX14<1,1>[45,35]    =  ACLD

    XX31<1,1>[3,15]     = '����� ���� :'
    XX31<1,1>[45,35]    =  TYPE1


    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  = "'L'":SPACE(1):VVV
    PR.HD := "'L'":SPACE(1):"����� ������ "
    PR.HD :="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":SPACE(15):"��� ����� ����� ����� ������  "
    PR.HD :="'L'":SPACE(15):"��� ������ �������� ������� ������"
    PR.HD :="'L'":"REPORT.CD.NEW.1M.TRK"
    PR.HD :="'L'": TYP
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD

* PRINT XX30<1,1>
* PRINT STR(' ',82)
    PRINT XX<1,1>
    PRINT STR(' ',82)
    PRINT XX2<1,1>
    PRINT STR(' ',82)
    PRINT XX3<1,1>
    PRINT STR(' ',82)
    PRINT XX11<1,1>
    PRINT XX4<1,1>
    PRINT STR(' ',82)
    PRINT XX10<1,1>
    PRINT STR(' ',82)
    PRINT XX15<1,1>
    PRINT STR(' ',82)
    PRINT XX20<1,1>
    PRINT STR(' ',82)
    PRINT XX19<1,1>
    PRINT STR(' ',82)
    PRINT XX22<1,1>
    PRINT STR('',82)
    PRINT XX23<1,1>
    PRINT STR('',82)
    PRINT XX13<1,1>
    PRINT STR('',82)
    PRINT XX14<1,1>
    PRINT STR('',82)
    PRINT XX16<1,1>
    PRINT STR(' ',82)
    PRINT XX30<1,1>
    PRINT STR(' ',82)
    PRINT XX31<1,1>
    PRINT STR('=',82)
    RETURN
*   END
END
