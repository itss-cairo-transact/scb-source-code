* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
**********************************NI7OOOOOOOOOOO************************
*-----------------------------------------------------------------------------
* <Rating>359</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.MORTAD.CR1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.MORTAD.CR1'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

** YTEXT = "Enter the TF No. : "
** CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)
    BR.ID     = R.NEW(SCB.BT.OUR.REFERENCE)
*Line [ 73 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(BR.ID,@VM)
    FOR I = 1 TO DD
        BR.ID1 = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
        REASON = R.NEW(SCB.BT.RETURN.REASON)<1,I>

        TEXT = "REASON :":REASON ; CALL REM
*------------------------------------------------------------------------
        IF REASON NE '' THEN
            BR.ID1  = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
            CALL F.READ(FN.BR,BR.ID1,R.BR,F.BR,E1)
            TEXT = "COMI: " : COMI ; CALL REM
            TEXT = "BR.ID: " : BR.ID ; CALL REM
            ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CONT.ACCT>
            TEXT = "ACC.NO : " : ACC.NO ; CALL REM
            CALL DBR("ACCOUNT":@FM:AC.CUSTOMER,ACC.NO,CUS)
            CALL DBR("ACCOUNT":@FM:AC.CURRENCY,ACC.NO,CUR.ID)
            CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
            CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>
            CUST.NAME2    = LOCAL<1,CULR.ARABIC.NAME.2>
            CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS>

* ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>

            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

            AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT = AMOUNT
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

* BR.DATA = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.OPERATION.TYPE>

****CUR.ID    = ACC.NO[9,2]
            TEXT = "CUR :": CUR.ID ; CALL REM
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT =  R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.RECEIVE.DATE>
*   DAT  = R.BR<EB.BILL.REG.MATURITY.DATE>
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            CHQ.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>

            INPUTTER = R.NEW(SCB.BT.INPUTTER)
            AUTH = R.NEW(SCB.BT.AUTHORISER)
            INP = FIELD(INPUTTER,'_',2)
            AUTHI =FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,35]   = CUST.NAME:
            XX<1,1>[3,35]   = "����� �����"

            XX<1,1>[45,15]  = '������     : '
            XX<1,1>[59,15]  = AMOUNT

            XX1<1,1>[45,15] = '��� ������ : '
            XX1<1,1>[59,15] = ACC.NO

            XX2<1,1>[45,15] = '��� ������ : '
            XX2<1,1>[59,15] = "������ �����"

            XX3<1,1>[45,15] = '������     : '
            XX3<1,1>[59,15] = CUR

            XX4<1,1>[45,15] = '����� ���� : '
            XX4<1,1>[59,15] = MAT.DATE

            XX5<1,1>[3,15]  = '��� �����      : '
            XX5<1,1>[20,15] = CHQ.NO

            XX6<1,1>[1,15]  = '������'
            XX7<1,1>[1,15] = AUTHI

            XX6<1,1>[30,15]  = '��� �������'
            XX7<1,1>[35,15] = INP

            XX6<1,1>[60,15]  = '������'
            XX7<1,1>[60,15] = COMI

            XX8<1,1>[3,35]  = '������ ������� : '
            XX8<1,1>[20,15] = OUT.AMT

            XX9<1,1>[3,15]  = '������         : '
            XX9<1,1>[20,15] = "����� �����"

*-------------------------------------------------------------------
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� ���"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX5<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR('-',82)
            PRINT XX7<1,1>
*NEXT I
        END
    NEXT I
*===============================================================
    RETURN
END
