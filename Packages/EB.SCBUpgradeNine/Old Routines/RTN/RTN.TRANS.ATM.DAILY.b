* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*----------------------------------
*-- CREATE BY NESSMA
*----------------------------------
    SUBROUTINE RTN.TRANS.ATM.DAILY
*----------------------------------
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TRANS.DAILY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------
    DIR.NAME.2 = "EBC123/EBC123.NEW"
    GOSUB OPF.FILES
    GOSUB CHECK.SCB.ATM

    REN.FILE ="cp -R ":DIR.NAME.2:"/":NEW.FILE:" ":FN.OFS.IN:"/":NEW.FILE
    EXECUTE REN.FILE
    RETURN
*---------------------------------------------
OPF.FILES:
*---------
    FN.ATM  = "F.SCB.ATM.TERMINAL.ID" ; F.ATM  = ""
    CALL OPF(FN.ATM, F.ATM)

    FN.SATM = "F.SCB.ATM.TRANS.DAILY" ; F.SATM = ""
    CALL OPF(FN.SATM, F.SATM)
    RETURN
*---------------------------------------------------
CHECK.SCB.ATM:
*-------------
    TOD = TODAY
    MON = TOD[5,2] + 1 - 1
    TOD = TOD[7,2]:"-":MON:"-":TOD[1,4]
    FILE.NAME = "EBC123.csv"
    FILE.ID   = "EBC123":TOD:".csv"

    CALL F.READ(FN.SATM,FILE.ID,R.SATM,F.SATM,ER.SATM)
*** FOR TESTING ONLY ER.SATM =  1
    IF ER.SATM THEN
        R.SATM<SCB.ATM.PROCESSING.DATE>  =  TODAY
        CALL F.WRITE(FN.SATM,FILE.ID,R.SATM)
        CALL JOURNAL.UPDATE(FILE.ID)

        GOSUB CREATE.OFS
        GOSUB INITIAL
        TEXT = "�� �������" ; CALL REM
    END ELSE
        TEXT = "�� ������� ���� ����� �� ���" ; CALL REM
    END
    RETURN
*---------------------------------------------------
CREATE.OFS:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "ATM.TRANS"

    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "EBC123-":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME.2,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME.2:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME.2
    END

    OPENSEQ DIR.NAME.2, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME.2
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME.2
        END
    END
    RETURN
*-------------------------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ''
    R.LINE = ""

    ERR='' ; ER=''

    DIR.NAMEE  = "EBC123"

    OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
        TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
        RETURN
    END

    IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM BB THEN
            ACQ  = FIELD(R.LINE,",",1)
            AMT1 = FIELD(R.LINE,",",9)
            IF ACQ EQ 'SCB' AND AMT1 GT 0 THEN
                ACQ         = FIELD(R.LINE,",",1)
                ISSUER      = FIELD(R.LINE,",",2)
                PAN         = FIELD(R.LINE,",",3)
                SEQ.NUM     = FIELD(R.LINE,",",4)
                TRANSACTION = FIELD(R.LINE,",",5)
                ACCOUNT.TY  = FIELD(R.LINE,",",6)
                STATS       = FIELD(R.LINE,",",7)
                TYP         = FIELD(R.LINE,",",8)
                AMT1        = FIELD(R.LINE,",",9)
                AMT2        = FIELD(R.LINE,",",10)
                TRANS.TIME  = FIELD(R.LINE,",",11)
                TRANS.DATE  = FIELD(R.LINE,",",12)
                SETTLE_DAT  = FIELD(R.LINE,",",13)
                ATM.ID      = FIELD(R.LINE,",",14)
                RESP.CODE   = FIELD(R.LINE,",",15)
                RESPONSE_C  = FIELD(R.LINE,",",16)

                GOSUB PROCESS
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ BB
    RETURN
*------------------------------------------------------------------
PROCESS:
*-------
    CALL F.READ(FN.ATM,ATM.ID,R.ATM,F.ATM,ER.ATM)
    ACCOUNT.NUMBER = R.ATM<SCB.ATM.ACCOUNT.NUMBER>
    ACCOUNT.FIX    = "EGP1123400010099"
    BRANCH.CODE    = "EG0010099"
    COMP.CODE      = BRANCH.CODE[8,2]
    OFS.USER.INFO  = "INPUTT" : COMP.CODE : "//" : BRANCH.CODE

    IF TYP[1,1] EQ 'T' THEN
        DR.ACCOUNT = ACCOUNT.FIX
        CR.ACCOUNT = ACCOUNT.NUMBER
    END

    IF TYP[1,1] EQ 'R' THEN
        DR.ACCOUNT = ACCOUNT.NUMBER
        CR.ACCOUNT = ACCOUNT.FIX
    END

    TRNS.TYPE = "AC70"
    CR.AMT    = AMT1
    DR.CUR    = "EGP"
    CR.CUR    = "EGP"
    DTR       = SEQ.NUM
    CTR       = SEQ.NUM

    GOSUB BUILD.RECORD
    RETURN
*------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :DTR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :CTR:COMMA

    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :CR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT="       :CR.AMT:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK="       :"SCB":COMMA
    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA

    OFS.MESSAGE.DATA :=  "LOCAL.REF:32:1="      :ATM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:33:1="      :ATM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:35:1="      :SEQ.NUM:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:43:1="      :PAN:COMMA

    ZZZ   = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    WRITESEQ ZZZ TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZZ
    END

    RETURN
*------------------------------------------------------------------
END
