* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MONTHS.BETWEEN(REQ.DATE.1,REQ.DATE.2,MN.COUNT)
************************************************************************
***********by Mahmoud Elhawary ** 8/6/2010 *****************************
************************************************************************
    INCLUDE T24.BP I_COMMON
    INCLUDE T24.BP I_EQUATE
************************************************************************
* Return the number of months between two dates                        *
* Note: if the number of days was less then one month                  *
*       then it doesn't count                                          *
************************************************************************

    IF REQ.DATE.2 GT REQ.DATE.1 THEN
        TDR1   = REQ.DATE.1
        TDR2   = REQ.DATE.2
    END ELSE
        TDR1   = REQ.DATE.2
        TDR2   = REQ.DATE.1
    END

*- check if the dates is the lasd days of month
    LL1 = TDR1
    LL2 = TDR2
    CALL LAST.DAY(LL1)
    CALL LAST.DAY(LL2)
    IF TDR1 EQ LL1 THEN
        ZZZ1 = 1
    END ELSE
        ZZZ1 = 0
    END
    IF TDR2 EQ LL2 THEN
        ZZZ2 = 1
    END ELSE
        ZZZ2 = 0
    END
*---------------

    TDDAY1  = TDR1[7,2]
    TDDAY2  = TDR2[7,2]
    TDRMN1  = TDR1[5,2]
    TDRMN2  = TDR2[5,2]
    TDRYY1  = TDR1[1,4]
    TDRYY2  = TDR2[1,4]

    YEARS.NO = TDRYY2 - TDRYY1
    MONTH.NO = TDRMN2 - TDRMN1
    DAYS.NO  = TDDAY2 - TDDAY1

    YR.MNTH  = YEARS.NO * 12
    MN.COUNT = MONTH.NO + YR.MNTH

    IF ZZZ1 NE 0 AND ZZZ2 NE 0 THEN
        IF DAYS.NO LT 0 THEN
            MN.COUNT = MN.COUNT - 1
        END
    END
    RETURN
END
