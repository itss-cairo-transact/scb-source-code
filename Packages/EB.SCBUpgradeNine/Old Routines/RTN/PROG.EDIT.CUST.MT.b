* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-------------------------------------------------------------------------
* <Rating>1222</Rating>
*-------------------------------------------------------------------------
****    PROGRAM SBD.PROG.EDIT.CUST.MT
    SUBROUTINE PROG.EDIT.CUST.MT
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.PROFESSION
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.ID.TYPE
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RELATION
*-----------------------------------------------------------------------
    COMP = ID.COMPANY
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 53 ] Adding EB.SCBUpgradeNine. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-----------------------------------------------------------------------
*==============================================================
INITIATE:
*--------
    REPORT.ID='PROG.EDIT.CUST.MT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
PRINT.HEAD:
*---------
* CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
* YYBRN  = FIELD(BRANCH,'.',2)
    YYBRN = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID  :SPACE(85):"���  ���������    "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� ������ ������� �������� "
    PR.HD :="'L'":SPACE(50):"������� ��������� ��� ��������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������" :SPACE(10):"��� ������������" :SPACE(25) :"�������������� " :SPACE(20): "������� �����"
    PR.HD :="'L'":"���� ������" :SPACE(10):"��� �����������" :SPACE(25) :"�������������� " :SPACE(20): "������� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��������" :SPACE(10): "������  " :SPACE(10): "��������" :SPACE(10): "������ ����" :SPACE(10): "����� �����  " :SPACE(10): "��� �����"
    PR.HD :="'L'":"��� ����" :SPACE(10): "�.����� " :SPACE(10): "�.������" :SPACE(10): "�.�������� " :SPACE(10): "�.�������� " :SPACE(10): "���� �� ������ "
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
CALLDB:
*-------
    TD = TODAY
    CALL CDT("",TD,'-1W')
    DDD = TD[3,6]

   * DDD = TODAY[3,6]
    DD  = DDD:"..."

    FN.CU  = 'FBNK.CUSTOMER'      ; F.CU  = '' ; R.CU  = ''
    FN.CUH = 'FBNK.CUSTOMER$HIS'  ; F.CUH = '' ; R.CUH = ''

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.CUH,F.CUH)

    COMP = ID.COMPANY
    COMP2 = COMP[8,2]
*****
*         CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ARG,CUS)
*         CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,MYLOCAL)
*         ADR = MYLOCAL<1,CULR.ARABIC.ADDRESS,1>
*****

    T.SEL  = "SELECT ":FN.CU:" WITH DATE.TIME LIKE ": DD :" AND "
    T.SEL := "CURR.NO GT 2 AND ACCOUNT.OFFICER EQ ":COMP2 :" AND "
    T.SEL := "INPUTTER LIKE ...SCB... "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    X = 0
    IF SELECTED  THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU, F.CU,ETEXT)

            LAST.CURR = R.CU<EB.CUS.CURR.NO> - 1
            CUS.ID    = KEY.LIST<I>
            OLD.ID    = CUS.ID:";":LAST.CURR

*            TEXT =    "OLD.CUST  ":OLD.ID ; CALL REM
            CALL F.READ(FN.CUH, OLD.ID ,R.CUH,F.CUH,ETEXTH)
*            TEXT = R.CUH ; CALL REM
** ON LINE DATA
** LINE   1
            NEW.NAME1  = R.CU<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            NEW.ADDR1  = R.CU<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS>
            NEW.NAME2  = R.CU<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME.2>
** LINE   2
            NEW.POST   = R.CU<EB.CUS.POSTING.RESTRICT>
            CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,NEW.POST,NEW.POST1)

            NEW.NAME.E.1 = R.CU<EB.CUS.NAME.1>
            NEW.ADDR.E.1 = R.CU<EB.CUS.STREET>
            NEW.NAME.E.2 = R.CU<EB.CUS.NAME.2>
** LINE   3
            NEW.SECT  = R.CU<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            CALL DBR('SCB.NEW.SECTOR':@FM:C.SCB.NEW.SECTOR.NAME,NEW.SECT,NEW.SECT1)

            NEW.PROF  = R.CU<EB.CUS.LOCAL.REF,CULR.PROFESSION>
            CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,NEW.PROF,NEW.PROF1)

            NEW.GOVER = R.CU<EB.CUS.LOCAL.REF,CULR.GOVERNORATE>
            CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,NEW.GOVER,NEW.GOVER1)

            NEW.TELE1 = R.CU<EB.CUS.LOCAL.REF,CULR.TELEPHONE>

            NEW.ID    = R.CU<EB.CUS.LOCAL.REF,CULR.ID.TYPE>
            CALL DBR('SCB.CUS.ID.TYPE':@FM:ID.TYPE.DESCRIPTION,NEW.ID,NEW.ID1)

            NEW.ID.NO1 = R.CU<EB.CUS.LOCAL.REF,CULR.ID.NUMBER>
** LINE   4
            NEW.NSN.NO1 = R.CU<EB.CUS.LOCAL.REF,CULR.NSN.NO>
            NEW.COM.NO1 = R.CU<EB.CUS.LOCAL.REF,CULR.COM.REG.NO>
            NEW.TAX.NO1 = R.CU<EB.CUS.LOCAL.REF,CULR.TAX.FILE.NO>
            NEW.EXP1    = R.CU<EB.CUS.LOCAL.REF,CULR.ID.EXPIRY.DATE>
            NEW.REL     = R.CU<EB.CUS.RELATION.CODE>
            CALL DBR('RELATION':@FM:EB.REL.DESCRIPTION,NEW.REL,NEW.REL1)
            NEW.TEXT1  = R.CU<EB.CUS.TEXT>
** OLD DATA
** LINE   1
            OLD.NAME1  = R.CUH<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            OLD.ADDR1  = R.CUH<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS>
            OLD.NAME2  = R.CUH<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME.2>
** LINE   2
            OLD.POST   = R.CUH<EB.CUS.POSTING.RESTRICT>
            CALL DBR('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,OLD.POST,OLD.POST1)

            OLD.NAME.E.1 = R.CUH<EB.CUS.NAME.1>
            OLD.ADDR.E.1 = R.CUH<EB.CUS.STREET>
            OLD.NAME.E.2 = R.CUH<EB.CUS.NAME.2>
** LINE   3
            OLD.SECT  = R.CUH<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            CALL DBR('SCB.NEW.SECTOR':@FM:C.SCB.NEW.SECTOR.NAME,OLD.SECT,OLD.SECT1)

            OLD.PROF  = R.CUH<EB.CUS.LOCAL.REF,CULR.PROFESSION>
            CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,OLD.PROF,OLD.PROF1)

            OLD.GOVER = R.CUH<EB.CUS.LOCAL.REF,CULR.GOVERNORATE>
            CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,OLD.GOVER,OLD.GOVER1)

            OLD.TELE1 = R.CUH<EB.CUS.LOCAL.REF,CULR.TELEPHONE>

            OLD.ID    = R.CUH<EB.CUS.LOCAL.REF,CULR.ID.TYPE>
            CALL DBR('SCB.CUS.ID.TYPE':@FM:ID.TYPE.DESCRIPTION,OLD.ID,OLD.ID1)

            OLD.ID.NO1 = R.CUH<EB.CUS.LOCAL.REF,CULR.ID.NUMBER>
** LINE   4
            OLD.NSN.NO1 = R.CUH<EB.CUS.LOCAL.REF,CULR.NSN.NO>
            OLD.COM.NO1 = R.CUH<EB.CUS.LOCAL.REF,CULR.COM.REG.NO>
            OLD.TAX.NO1 = R.CUH<EB.CUS.LOCAL.REF,CULR.TAX.FILE.NO>
            OLD.EXP1    = R.CUH<EB.CUS.LOCAL.REF,CULR.ID.EXPIRY.DATE>
            OLD.REL     = R.CUH<EB.CUS.RELATION.CODE>
            CALL DBR('RELATION':@FM:EB.REL.DESCRIPTION,OLD.REL,OLD.REL1)
            OLD.TEXT1  = R.CUH<EB.CUS.TEXT>

            LINE.1  = SPACE(132)
            LINE.2  = SPACE(132)
            LINE.3  = SPACE(132)
            LINE.4  = SPACE(132)
            LINE.5  = SPACE(132)
            LINE.6  = SPACE(132)
            LINE.7  = SPACE(132)
            LINE.8  = SPACE(132)

            LINE.1<1,1>[1,8]   = CUS.ID
            LINE.1<1,1>[15,35] = NEW.NAME1
            LINE.1<1,1>[55,35] = NEW.ADDR1
            LINE.1<1,1>[95,30] = NEW.NAME2

            LINE.2<1,1>[1,12]  = NEW.POST1
            LINE.2<1,1>[15,35] = NEW.NAME.E.1
            LINE.2<1,1>[55,35] = NEW.ADDR.E.1
            LINE.2<1,1>[95,30] = NEW.NAME.E.2

            LINE.3<1,1>[1,18]   = NEW.SECT1
            LINE.3<1,1>[20,18]  = NEW.PROF1
            LINE.3<1,1>[40,18]  = NEW.GOVER1
            LINE.3<1,1>[60,18]  = NEW.TELE1
            LINE.3<1,1>[80,18]  = NEW.ID1
            LINE.3<1,1>[100,18] = NEW.ID.NO1

            LINE.4<1,1>[1,18]   = NEW.NSN.NO1
            LINE.4<1,1>[20,18]  = NEW.COM.NO1
            LINE.4<1,1>[40,18]  = NEW.TAX.NO1
            LINE.4<1,1>[60,18]  = NEW.EXP1
            LINE.4<1,1>[80,18]  = NEW.REL1
            LINE.4<1,1>[100,18] = NEW.TEXT1


            LINE.5<1,1>[1,8]   = ' '
            LINE.5<1,1>[15,35] = OLD.NAME1
            LINE.5<1,1>[55,35] = OLD.ADDR1
            LINE.5<1,1>[95,30] = OLD.NAME2

            LINE.6<1,1>[1,12]  = OLD.POST1
            LINE.6<1,1>[15,35] = OLD.NAME.E.1
            LINE.6<1,1>[55,35] = OLD.ADDR.E.1
            LINE.6<1,1>[95,30] = OLD.NAME.E.2

            LINE.7<1,1>[1,18]   = OLD.SECT1
            LINE.7<1,1>[20,18]  = OLD.PROF1
            LINE.7<1,1>[40,18]  = OLD.GOVER1
            LINE.7<1,1>[60,18]  = OLD.TELE1
            LINE.7<1,1>[80,18]  = OLD.ID1
            LINE.7<1,1>[100,18] = OLD.ID.NO1

            LINE.8<1,1>[1,18]   = OLD.NSN.NO1
            LINE.8<1,1>[20,18]  = OLD.COM.NO1
            LINE.8<1,1>[40,18]  = OLD.TAX.NO1
            LINE.8<1,1>[60,18]  = OLD.EXP1
            LINE.8<1,1>[80,18]  = OLD.REL1
            LINE.8<1,1>[100,18] = OLD.TEXT1

            PRINT LINE.1<1,1>
            PRINT LINE.2<1,1>
            PRINT LINE.3<1,1>
            PRINT LINE.4<1,1>
            PRINT SPACE(48):STR('=',40)
            PRINT LINE.5<1,1>
            PRINT LINE.6<1,1>
            PRINT LINE.7<1,1>
            PRINT LINE.8<1,1>
            PRINT STR('-',130)

        NEXT I
    END
            PRINT SPACE(30):"=========== END OF REPORT ============="
    RETURN
END
************************************************
