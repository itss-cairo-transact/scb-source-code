* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>-102</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.RET.DEBIT.RE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.WH.TRANS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SF.TRANS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW
*------------------------------------------------------------------------
*    IF R.NEW(CHQ.RET.DEBIT.ACCT) NE '' THEN
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*   END
*========================================================================
INITIATE:
    REPORT.ID='REPORT.RET.DEBIT'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    CUS           = '52109'

    FN.BR = 'F.SCB.CHQ.RETURN.NEW' ; F.BR = ''
    CALL OPF(FN.BR,F.BR)

    YTEXT = "Enter the No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BR,COMI,R.BR,F.BR,E1)

**   CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS,LOCAL)
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,CUS,CATEG.NAME)
* CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>
    CUST.NAME     = CATEG.NAME
* CUST.NAME3    = LOCAL<1,CULR.ARABIC.NAME.2>
* CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS,1>
    CUST.ADDRESS  = CATEG.NAME
* CUST.ADDRESS1 = LOCAL<1,CULR.ARABIC.ADDRESS>
* CUST.ADDRESS2 = LOCAL<1,CULR.ARABIC.ADDRESS>
* CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.ADDRESS1,CUST.ADD2)
* CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.ADDRESS2,CUST.ADD1)
* IF CUST.ADDRESS1 = 98 THEN
*     CUST.ADD1 = ''
* END
* IF CUST.ADDRESS2 = 998 THEN
*     CUST.ADD2 = ''
* END
* IF CUST.ADDRESS1 = 999 THEN
*     CUST.ADD1 = ''
* END
* IF CUST.ADDRESS2 = 999 THEN
*     CUST.ADD2 = ''
* END
    ACC.DB = '54025'
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,ACC.DB,CATEG.NAME)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CURR.ID,CURR.CC)
    CALL DBR ("ACCOUNT":@FM:AC.CATEGORY,ACC.DB,CATEG.ID)
    ACCC      = R.BR<CHQ.RET.DEBIT.ACCT>
    ACC.DB    = ACCC
    CALL DBR ("ACCOUNT":@FM:AC.CURRENCY,ACC.DB,CURR.ID)
    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CURR.ID,CURR.CC)

***AMOUNT    = R.BR<SCB.SF.TR.MARGIN.VALUE>
    CURR         = R.BR<CHQ.RET.CURRENCY>
    BR.DATA      = R.BR<CHQ.RET.REMARKS>
    IF CURR.ID EQ 'EGP' THEN
        LCY.AMT = 20
        CALL EB.ROUND.AMOUNT ('EGP',LCY.AMT,'',"2")
        FCY.AMT = ''
        AMOUNT = LCY.AMT
    END
    IF CURR.ID NE 'EGP' THEN
        FN.CUR  = 'FBNK.CURRENCY' ; F.CUR = ''
        CALL OPF(FN.CUR,F.CUR)
        CALL F.READ(FN.CUR,CURR,R.CUR,F.CUR,ERR1)
        RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

        LCY.AMT = 2 * RATE
        CALL EB.ROUND.AMOUNT ('USD',LCY.AMT,'',"2")
        FCY.AMT = 2
        AMOUNT  = FCY.AMT
    END

    IN.AMOUNT = AMOUNT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

    V.DATE = R.BR<CHQ.RET.CHQ.DATE>

    CHQ.NO = R.BR<CHQ.RET.CHEQUE.NO>

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CURR,CUR)
   * OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

**************UP 20110908
   OUT.AMT = OUT.AMOUNT : ' ' : CURR.CC : ' ' : '�����'

    MAT.DATE  = V.DATE[7,2]:'/':V.DATE[5,2]:"/":V.DATE[1,4]

    INPUTTER = R.BR<CHQ.RET.INPUTTER>
    AUTH     = R.BR<CHQ.RET.AUTHORISER>
    INP      = FIELD(INPUTTER,'_',2)
    AUTHI    = FIELD(AUTH,'_',2)

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132)
    XX8  = SPACE(132)  ; XX9  = SPACE(132)

    XX<1,1>[3,35]    = CUST.NAME
* XX1<1,1>[3,35]   = CUST.NAME3
    XX2<1,1>[3,35]   = CUST.ADDRESS
* XX3<1,1>[3,35]   = CUST.ADD1:' ':CUST.ADD2
    XX<1,1>[45,15]  = '������     : '
    XX<1,1>[59,15]  = AMOUNT

    XX1<1,1>[45,15] = '��� ������ : '
    XX1<1,1>[59,15] = CUS

    XX2<1,1>[45,15] = '��� ������ : '
    XX2<1,1>[59,15] = CATEG.NAME

    XX3<1,1>[45,15] = '������     : '
    XX3<1,1>[59,15] = CURR.CC

    XX4<1,1>[45,15] = '����� ���� : '
    XX4<1,1>[59,15] = MAT.DATE

    XX5<1,1>[3,15]  = '��� �����      : '
    XX5<1,1>[20,15] = CHQ.NO

    XX6<1,1>[1,15]  = '������'
    XX7<1,1>[1,15] = AUTHI

    XX6<1,1>[30,15]  = '��� �������'
    XX7<1,1>[35,15] = INP

    XX6<1,1>[60,15]  = '������'
    XX7<1,1>[60,15] = COMI

    XX8<1,1>[3,35]  = '������ ������� : '
    XX8<1,1>[20,15] = OUT.AMT

    XX9<1,1>[3,15]  = '������         : '
    XX9<1,1>[20,15] = BR.DATA

*-------------------------------------------------------------------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN:SPACE(30):"����� �����"
    PR.HD :="'L'":" "
    PR.HD :="'L'":"CHQRET"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX<1,1>
    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
* PRINT XX5<1,1>
    PRINT STR(' ',82)
    PRINT XX6<1,1>
    PRINT STR('-',82)
    PRINT XX7<1,1>
*===============================================================
    RETURN
END
