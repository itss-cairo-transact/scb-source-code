* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>665</Rating>
*-----------------------------------------------------------------------------
*-- CREATE BY SABRY
*-- EDIT BY NESSMA
    SUBROUTINE REPORT.CUSTOMER.CONFIRM1

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.REP.CUS.EXP
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*---------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "TOTAL REPORT PRINTED  ":WS.PRINT.COUNT ; CALL REM
    RETURN
*==============================================================
INITIATE:
*--------
    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = ''
    CALL OPF( FN.CUSTOMER,F.CUSTOMER)

    WS.COMP = ID.COMPANY

    REPORT.ID='REPORT.CUSTOMER.CONFIRM1'
    CALL PRINTER.ON(REPORT.ID,'')

    WS.2Y.D = TODAY
    CALL ADD.MONTHS(WS.2Y.D,-24)
    WS.4Y.D = TODAY
    CALL ADD.MONTHS(WS.4Y.D,-48)

    FN.REP.CUS  =  'F.REP.CUS.EXP' ; F.REP.CUS = '' ; R.REP.CUS = ''
    CALL OPF (FN.REP.CUS,F.REP.CUS)
    WS.PRINT.COUNT = 0
    RETURN
*=============== ================================================
PROCESS:
*-------
    T.SEL  = "SELECT FBNK.CUSTOMER WITH COMPANY.BOOK EQ ":WS.COMP
    T.SEL := " AND POSTING.RESTRICT LT 90 AND POSTING.RESTRICT NE 70 AND POSTING.RESTRICT NE 18 AND NEW.SECTOR NE '' AND CREDIT.CODE LE 100"
    T.SEL := " AND UPDATE.DATE NE '' WITHOUT SECTOR IN ( 5010 5020 )"
    T.SEL := " AND POSTING.RESTRICT NE 21 BY @ID "

    KEY.LIST="" ;  SELECTED="" ;  ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ZZ=KEY.LIST<I>

            CALL F.READ(FN.CUSTOMER,KEY.LIST<I>, R.CUSTOMER,F.CUSTOMER, ETEXT)
            WS.UPD.DATE = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.UPDATE.DATE>
            WS.QLTY.RAT = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.QLTY.RATE>

            IF WS.QLTY.RAT EQ 1 THEN
                IF WS.UPD.DATE GT WS.4Y.D THEN
                    GOTO NEXT.I
                END
            END
            IF WS.QLTY.RAT GT 1 THEN
                IF WS.UPD.DATE GT WS.2Y.D THEN
                    GOTO NEXT.I
                END
            END

            CALL F.READ(FN.REP.CUS,KEY.LIST<I>,R.REP.CUS,F.REP.CUS,ERR)
            IF ERR THEN
                R.REP.CUS<REP.CUS.RENEW.FLAG> = '1'
                R.REP.CUS<REP.CUS.RENEW.DATE.P> = TODAY
                CALL F.WRITE(FN.REP.CUS,KEY.LIST<I>,R.REP.CUS)
                CALL JOURNAL.UPDATE(KEY.LIST<I>)
            END ELSE
                IF R.REP.CUS<REP.CUS.RENEW.FLAG> # 1 THEN
                    R.REP.CUS<REP.CUS.RENEW.FLAG> = '1'
                    R.REP.CUS<REP.CUS.RENEW.DATE.P> = TODAY
                    CALL F.WRITE(FN.REP.CUS,KEY.LIST<I>,R.REP.CUS)
                    CALL JOURNAL.UPDATE(KEY.LIST<I>)
                END ELSE
                    GOTO NEXT.I
                END
            END
            WS.PRINT.COUNT ++

            NAME=R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>
            ADD1=R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS><1,1,1>
            ADD2=R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS><1,1,2>
            ADD3=R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.ARABIC.ADDRESS><1,1,3>

            WS.GOV=R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.GOVERNORATE>
            CALL DBR('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,WS.GOV,WS.GOV.NAME)
            WS.REG=R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.REGION>
            CALL DBR('SCB.CUS.REGION':@FM:REG.DESCRIPTION,WS.REG,WS.REG.NAME)

            OLD  = R.CUSTOMER<EB.CUS.LOCAL.REF,CULR.OLD.CUST.ID>
            LAST = KEY.LIST<I>
            GOSUB PRINT.HEAD
NEXT.I:
        NEXT I
    END
    RETURN
*--------------------------------------------
PRINT.HEAD:
*-----------
    PR.HD = REPORT.ID
    HEADING PR.HD
    PRINT " ":TODAY
    PRINT SPACE(3):"��� ���� �����������"
    BRN = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN,YY)

    PRINT SPACE(3):"����������":"  :  ":YY
    PRINT SPACE(5): "����� / " : NAME
    PRINT SPACE(5): "������� / ": ADD1
    PRINT SPACE(15): ADD2
    PRINT SPACE(15): ADD3
    PRINT SPACE(15): WS.REG.NAME ;PRINT SPACE(15): WS.GOV.NAME
    PRINT " "
    PRINT " "
    PRINT SPACE(3):"����� ���":" ":LAST:" ."
    PRINT " "
    PRINT " "
    RETURN
END
