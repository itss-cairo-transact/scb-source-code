* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.LD.FINISHED

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.LD.FINISHED'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TEXT = "DATEE : " : DATEE ; CALL REM
    TD = TODAY
    TEXT = "TD:":TD ; CALL REM
*------------------------------------------------------------------------
*   T.SEL = "SELECT ":FN.LD: " WITH FIN.MAT.DATE EQ ":TD: " AND (RENEW.IND NE 'YES' OR RENEW.METHOD EQ 1) "
    T.SEL = "SELECT ":FN.LD: " WITH FIN.MAT.DATE GT ":DATEE: " AND FIN.MAT.DATE LE ":TD:" AND RENEW.IND NE 'YES' AND CATEGORY GE 21001 AND CATEGORY LE 21010 AND CO.CODE EQ " : COMP
*   T.SEL = "SELECT ":FN.LD.HIS: " WITH @ID=LD1010800324;4 AND FIN.MAT.DATE EQ '20100419' AND RENEW.IND NE 'YES' AND CATEGORY GE 21001 AND CATEGORY LE 21010"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = 'SEL = ':SELECTED ; CALL REM
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E2)
* ID = R.LD<LD.LOCAL.REF><1,LDLR.FIRST.LD.ID>
            ID = KEY.LIST<I>
            CUS.ID = R.LD<LD.CUSTOMER.ID>

            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME1= LOCAL.REF<1,CULR.ARABIC.NAME.2>

            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF1)
            CUST.ADDRESS = LOCAL.REF1<1,CULR.ARABIC.ADDRESS,1>

            IF CUST.ADDRESS EQ  " " THEN
                CUST.ADDRESS = "���� ��������� ������"
            END

*            ACC = R.LD<LD.DRAWDOWN.ACCOUNT>
            ACC = R.LD<LD.PRIN.LIQ.ACCT>

*            AMOUNT = R.LD<LD.AMOUNT>
            AMOUNT = R.LD<LD.REIMBURSE.AMOUNT>

            INTER = R.LD<LD.TOT.INTEREST.AMT>

            BRANCH.ID = R.LD<LD.MIS.ACCT.OFFICER>

            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)

            DAT = R.LD<LD.FIN.MAT.DATE>
            DAT2= R.LD<LD.VALUE.DATE>

            V.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            V.DATE2 = DAT2[7,2]:'/':DAT2[5,2]:"/":DAT2[1,4]

            IN.AMOUNT = AMOUNT
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            CUR.ID    = R.LD<LD.CURRENCY>
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            XX   = SPACE(132)  ; XX3  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)

            XX<1,1>[3,15]    =  '��� �������   :'
            XX<1,1>[45,15]   = ID

            XX10<1,1>[3,15]     = '�����       : '
            XX10<1,1>[20,35]    = CUST.NAME :' ':CUST.NAME1
* XX1<1,1>[20,35]   = CUST.NAME1

            XX1<1,1>[64,15]  = '��� ������  : '
            XX1<1,1>[80,15]  = ACC

            XX1<1,1>[3,15]   = '�������      : '
            XX1<1,1>[20,35]  = CUST.ADDRESS

            XX2<1,1>[45,15]  = '������       :'
            XX2<1,1>[64,15]  = AMOUNT : ' ' :CUR

            XX3<1,1>[45,15]  = ' ���� ������: '
            XX3<1,1>[64,15]  =  INTER:' ':CUR
*  XX3<1,1>[30,15] = '���� ������� �������� ��� ����� ���':' ':ID

            XX4<1,1>[45,15] = ' ':AMOUNT:' ':CUR

            XX5<1,1>[45,15]  = '����� ����   : '
            XX5<1,1>[64,15]  = V.DATE2

            XX8<1,1>[45,15]  = ' ����� �������� : ' :V.DATE

            XX6<1,1>[1,15]  = '������ ������� : ':OUT.AMT
            XX7<1,1>[65,15] = '�� ��� ���� ������'

*-------------------------------------------------------------------
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"������� ��������"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX10<1,1>
            PRINT XX1<1,1>
*PRINT STR(' ',82)
            PRINT XX2<1,1>
* PRINT STR(' ',82)
            PRINT XX3<1,1>
* PRINT XX4<1,1>
*PRINT XX11<1,1>
            PRINT XX5<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR(' ',82)
            PRINT XX7<1,1>
            PRINT STR(' ',82)
            PRINT XX8<1,1>
            PRINT STR(' ',82)
            PRINT STR('=',82)
        NEXT I
    END
*====================================================*
    RETURN
END
