* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*************************************NI7OOOOOOOOOOOOOOOOO***************
*-----------------------------------------------------------------------------
* <Rating>-75</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE REPORT.CHQ.RETURN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHQ.RETURN.NEW
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETURN.REASON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    COMP = ID.COMPANY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.CHQ.RETURN'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.CHQ   ='F.SCB.CHQ.RETURN.NEW' ; F.CHQ    = ''
    CALL OPF(FN.CHQ,F.CHQ)

    FN.REAS  ='F.SCB.RETURN.REASON' ; F.REAS = ''
    CALL OPF(FN.REAS,F.REAS)

    FN.COM ='F.COMPANY' ;  F.COM = ''
    CALL OPF(FN.COM,F.COM)

    CUST.NO      = R.NEW(CHQ.RET.CUSTOMER)
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOCAL.REF)
    CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
    CUST.NAME1   = LOCAL.REF<1,CULR.ARABIC.NAME.2>
    CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    CUST.GOV     = LOCAL.REF<1,CULR.GOVERNORATE>
    CUST.REG     = LOCAL.REF<1,CULR.REGION>
    CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,CUST.GOV,GOVNAME)
    CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION,CUST.REG,REGNAME)
    IF CUST.GOV = 98 THEN
        GOVNAME = ''
    END
    IF CUST.REG = 998 THEN
        REGNAME = ''
    END
    IF CUST.GOV = 999 THEN
        GOVNAME = ''
    END
    IF CUST.REG = 999 THEN
        REGNAME = ''
    END


    FINDSTR '���' IN CUST.ADDRESS SETTING FMS,VMS THEN
        CUST.ADDRESS1 = "���� ��������� ������"
    END ELSE
        CUST.ADDRESS1 =LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
    END

    CUR.ID    = R.NEW(CHQ.RET.CURRENCY)

    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)

    ST.DATE    = R.NEW(CHQ.RET.STOP.DATE)
    V.DATE2    = ST.DATE[7,2]:'/':ST.DATE[5,2]:"/":ST.DATE[1,4]

    CUS.ACC    = R.NEW(CHQ.RET.CUST.ACCT)
    CHQ.NO     = R.NEW(CHQ.RET.CHEQUE.NO)

    AMT        = R.NEW(CHQ.RET.AMOUNT)
    IN.AMOUNT  = AMT
    CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
    OUT.AMT    = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

    REAS       = R.NEW(CHQ.RET.REASON)
    CALL DBR ('SCB.RETURN.REASON':@FM:SCB.RR.DESCRIPTION,REAS,REAS.DESC)

    DAT        = TODAY
    N.DATE     = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]

    BRN        = R.NEW(CHQ.RET.CO.CODE)
    TEXT = "BRN : " : BRN ; CALL REM
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN,BRANCH)
    TEXT = "BRANCH : " : BRANCH ; CALL REM
* CALL F.READ(FN.COM,BRN,R.COM,F.COM,E1)
* BRANCH = R.COM<EB.COM.COMPANY.NAME,2>
    TEXT = BRANCH ; CALL REM

    XX   = SPACE(132)  ; XX3  = SPACE(132)
    XX1  = SPACE(132)  ; XX4  = SPACE(132) ; XX11 = SPACE(132)
    XX2  = SPACE(132)  ; XX5  = SPACE(132) ; XX10 = SPACE(132)
    XX6  = SPACE(132)  ; XX7  = SPACE(132) ; XX8  = SPACE(132)
    XX9  = SPACE(132)  ; XX12 = SPACE(132) ; XX13 = SPACE(132)
    XX14  = SPACE(132)  ; XX15 = SPACE(132)

* XX<1,1>[1,15]    = "������� �� : "
* XX<1,1>[30,15]   =  N.DATE

    XX1<1,1>[3,15]   = "����� /  "
    XX1<1,1>[20,35]  = CUST.NAME :' ':CUST.NAME1

    XX2<1,1>[1,15]   = "�������      : "
    XX2<1,1>[20,35]  = CUST.ADDRESS1
    XX3<1,1>[20,35]  = GOVNAME : " " : REGNAME

    XX4<1,1>[2,15]  = "���� ���� ���� �"

    XX5<1,1>[2,15]  = "����� ��� ���� ������� ���� ���� ��� ��� ������ " :V.DATE2

    XX6<1,1>[2,15]  = "����� ������ ����� �� ������ ��� " : CUS.ACC : ":"

    XX7<1,1>[20,15]  = "��� ��� ":CHQ.NO:" ":" ����� ":" ":AMT:" ":CUR

*   XX8<1,1>[2,15]   = "���� �� ������ ������� �� ���� ���� ����� ��� ���� ��� ����� ������ "
    XX8<1,1>[2,15]   = "��� ���� ��� ����� ������"  : " " : REAS.DESC
    XX13<1,1>[25,15] = "(":OUT.AMT:")"

* XX9<1,1>[1,15]  = "��� ���� ������ �������� ������ ��� ���� ����� ������ ����� ��� ���� ���� �������"
*    XX14<1,1>[2,15]  = "��� ���� ������ �������� ������ ��� ���� ����� ������ �����"

*    XX15<1,1>[2,15]  = "��� ���� ���� �������"

    XX10<1,1>[30,15] = "������� ����� ���� �������� ���"

    XX11<1,1>[50,15] = "��� ���� ������"

    XX12<1,1>[50,15] = "����� : " : BRANCH

*-------------------------------------------------------------------
    PR.HD = "'L'":"CHQ.LETTER"
    PR.HD : ="'L'":SPACE(1):BRANCH
    PR.HD : ="'L'":SPACE(1):"��": " " :  N.DATE


* PR.HD :="'L'":"����� : ":YYBRN :SPACE(20):"������� ��������"
* PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    PRINT XX1<1,1>
    PRINT STR(' ',1)
    PRINT XX2<1,1>
    PRINT STR(' ',1)
    PRINT XX3<1,1>
    PRINT STR(' ',1)
    PRINT XX4<1,1>
    PRINT STR(' ',1)
    PRINT XX5<1,1>
    PRINT STR(' ',1)
    PRINT XX6<1,1>
    PRINT STR(' ',1)
    PRINT XX7<1,1>
    PRINT STR(' ',1)
    PRINT XX13<1,1>
    PRINT STR(' ',1)
    PRINT XX8<1,1>
* PRINT STR(' ',1)
* PRINT XX9<1,1>
* PRINT STR(' ',1)
* PRINT XX15<1,1>
    PRINT STR(' ',1)
    PRINT XX10<1,1>
    PRINT STR(' ',1)
    PRINT XX11<1,1>
    PRINT STR(' ',1)
    PRINT XX12<1,1>
    PRINT STR(' ',1)

*====================================================*
    RETURN
END
