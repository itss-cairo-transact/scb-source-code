* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-176</Rating>
*-----------------------------------------------------------------------------
************* WAEL *************
*TO CREATE LG LETTER
    SUBROUTINE REPORT.DATA.WORK

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.PARMS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LG.CHARGE

    GOSUB INITIATE
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*** TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.DATA.WORK'
******  REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN


*==============================================================
*===============================================================
PROCESS:
        DATY    = TODAY
        XX      = DATY[7,2]:"/":DATY[5,2]:"/":DATY[1,4]

*===============================================================
        PRINT SPACE(5):"������� : "   : XX
        PRINT
        PRINT SPACE(5):"������/������� ������ ������ �������� "
        PRINT
        PRINT SPACE(20):"������ �������"
        PRINT
        PRINT SPACE(20):"������� : ������ ������ ������� ����� ������"
        PRINT SPACE(20):"------------------------------------------"
        PRINT
        PRINT SPACE(5): "���� ���� ���� ,"
        PRINT
        PRINT SPACE(5): "����� ��� ���� ��� ��� ���� ������� ������ �������� ������"
        PRINT
        PRINT SPACE(5): "����� ������ �������� ���� ������ ���� �� ������ . "
        PRINT
        PRINT SPACE(5):"����� ����� ������ �� ������ ���� ������ ������� ���� ��������"
        PRINT
        PRINT SPACE(5):"��� ��� �������� �� ������� �� ���� ������ ����� ���� ��� ����� ���"
        PRINT
        PRINT SPACE(5):"������� ����� ������� "
        PRINT ; PRINT ; PRINT
        PRINT SPACE(25):"� ������ ����� ���� ��������  ���" ;PRINT
        PRINT ; PRINT ; PRINT
        PRINT SPACE(45):"��� ���� ����� �������"
**TEXT = "REPORT CREATED SUCCESSFULLY " ; CALL REM

        RETURN
*************************************
        TEXT = "FIRST "  ; CALL REM
    END
*==================================================================
