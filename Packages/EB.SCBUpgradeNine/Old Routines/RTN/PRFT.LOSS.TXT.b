* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1274</Rating>
*-----------------------------------------------------------------------------
**    PROGRAM PRFT.LOSS.TXT
    SUBROUTINE PRFT.LOSS.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CONSL.PRFT.LOSS

*==================== create cf and cs text files ========
    OPENSEQ "&SAVEDLISTS&" , "PRFT.LOSS.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"PRFT.LOSS.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "PRFT.LOSS.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE PRFT.LOSS CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create PRFT.LOSS.CSV File IN &SAVEDLISTS&'
        END
    END
*****
*============================ OPEN TABLES =========================

    FN.CNSL  = 'F.SCB.CONSL.PRFT.LOSS' ; F.CNSL = '' ; R.CNSL = ''
    CALL OPF(FN.CNSL,F.CNSL)

    T.SEL  = "SELECT F.SCB.CONSL.PRFT.LOSS BY @ID"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*======================== BB.DATA ===========================

    IF SELECTED THEN
        BB.DATA = "PL.CATEGORY,PL.DESC,PRODUCT.CATEGORY,PRODUCT.TYPE,CURRENCY,AMOUNT.EQUIVALENT,NATIVE.AMOUNT,LINE,LINE.DESC,SECTOR.CODE,SECTOR.NAME,CUSTOMER.TYPE,BRANCH,BRANCH.NAME,Sigmentation,Sigmentation Name,Department code,Department Name"

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CNSL,KEY.LIST<I>, R.CNSL, F.CNSL, ETEXT)
            SEC.CODE = R.CNSL<CNSL.SECTOR>

            BEGIN CASE
            CASE SEC.CODE = 2000
                CUS.TYPE = "Retail"
            CASE (SEC.CODE = 1100 OR SEC.CODE = 1300)
                CUS.TYPE = "Staff"
            CASE (SEC.CODE = 1200)
                CUS.TYPE = "Staff Relative"
            CASE 1
                CUS.TYPE = "Corporate"
            END CASE

            BB.DATA  = R.CNSL<CNSL.PL.CATEG>:','
            BB.DATA := R.CNSL<CNSL.PL.CATEG.NAME>:','
            BB.DATA := R.CNSL<CNSL.CATEG>:','
            BB.DATA := R.CNSL<CNSL.CATEG.NAME>:','
            BB.DATA := R.CNSL<CNSL.CURR>:','
            BB.DATA := R.CNSL<CNSL.EQU.AMT>:','
            BB.DATA := R.CNSL<CNSL.NATIVE.AMT>:','
            BB.DATA := R.CNSL<CNSL.LINE>:','
            BB.DATA := R.CNSL<CNSL.LINE.DESC>:','
            BB.DATA := R.CNSL<CNSL.SECTOR>:','
            BB.DATA := R.CNSL<CNSL.SECTOR.NAME>:','
            BB.DATA := CUS.TYPE:','
            BB.DATA := R.CNSL<CNSL.BRANCH>:','
            BB.DATA := R.CNSL<CNSL.BRANCH.NAME>:','
            BB.DATA := R.CNSL<CNSL.TARGET>:','
            CALL DBR ('TARGET':@FM:EB.TAR.DESCRIPTION,R.CNSL<CNSL.TARGET>,TARGET.NAM)
            BB.DATA := TARGET.NAM:','
            IF (R.CNSL<CNSL.PL.DEPT.OFFICER> GE 7000 AND R.CNSL<CNSL.PL.DEPT.OFFICER> LE 7999) OR R.CNSL<CNSL.PL.DEPT.OFFICER> EQ 99 THEN
                BB.DATA := R.CNSL<CNSL.PL.DEPT.OFFICER>:','
                CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.CNSL<CNSL.PL.DEPT.OFFICER>,DEPT.NAM)
                BB.DATA := DEPT.NAM
            END



            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END


    CLOSESEQ BB
END
