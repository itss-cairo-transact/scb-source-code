* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>1465</Rating>
*-----------------------------------------------------------------------------
*PROGRAM OFS.ADI
    SUBROUTINE OFS.ADI

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    FN.LIMIT = 'FBNK.LIMIT' ; F.LIMIT = '' ; R.LIMIT = ''
    CALL OPF( FN.LIMIT,F.LIMIT)
*T.SEL.LIMIT = "SELECT FBNK.LIMIT WITH (LIMIT.PRODUCT EQ 101 OR LIMIT.PRODUCT EQ 103 OR LIMIT.PRODUCT EQ 102) AND ACCOUNT NE '' AND NOTES NE 'CREATED BY SYSTEM DEFAULT' AND EXPIRY.DATE GE TODAY AND COLLAT.RIGHT NE '' AND SECURED.AMT NE 0.0 BY @ID"
    T.SEL.LIMIT = "SELECT FBNK.LIMIT WITH (LIMIT.PRODUCT EQ 101 OR LIMIT.PRODUCT EQ 103 OR LIMIT.PRODUCT EQ 102) AND ACCOUNT NE '' AND NOTES NE 'CREATED BY SYSTEM DEFAULT' AND EXPIRY.DATE GE TODAY AND COLLAT.RIGHT NE '' AND SECURED.AMT NE 0.0 AND SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300 AND SECTOR NE 1400 BY @ID"

    CALL EB.READLIST(T.SEL.LIMIT,KEY.LIST.LIMIT,"",SELECTED.LIMIT,ER.MSG)

*PRINT "SELECTED.LIMIT ":SELECTED.LIMIT

    IF SELECTED.LIMIT THEN
        FOR S = 1 TO SELECTED.LIMIT

*************************************************
            CALL F.READ(FN.LIMIT,KEY.LIST.LIMIT<S>,R.LIMIT,F.LIMIT,'')

            IF R.LIMIT<LI.LIMIT.PRODUCT> EQ 102 THEN

                LIMIT.CODE = KEY.LIST.LIMIT<S>

*PRINT "LIMIT.CODE ":LIMIT.CODE

                COL.NO = R.LIMIT<LI.COLLAT.RIGHT>

*PRINT "COL.NO ":COL.NO

*Line [ 70 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                TEMP.COUNT = DCOUNT(COL.NO,@SM)

*PRINT "TEMP.COUNT ":TEMP.COUNT
                MAX.INT.SAV = 0
                FOR I = 1 TO TEMP.COUNT

                    FN.COLL = 'FBNK.COLLATERAL' ; F.COLL = '' ; R.COLL = ''
                    CALL OPF( FN.COLL,F.COLL)
                    T.SEL = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.NO<1,1,I>:"... AND COLLATERAL.CODE EQ ":R.LIMIT<LI.COLLATERAL.CODE>
                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                    IF SELECTED THEN
                        CALL F.READ(FN.COLL,KEY.LIST<1>,R.COLL,F.COLL,'')
                        APP.ID =  R.COLL<COLL.APPLICATION.ID>
                    END

                    CALL DBR ('ACCR.ACCT.CR':@FM:IC.ACRCR.CR.INT.RATE,APP.ID,INT.SAV)

                    IF INT.SAV GT MAX.INT.SAV THEN
                        MAX.INT.SAV = INT.SAV
                    END

                NEXT I
                DEBIT.INT = MAX.INT.SAV

                IF DEBIT.INT = 0.0 THEN CONTINUE
*PRINT "DEBIT.INT ":DEBIT.INT
            END
*************************************************
            IF R.LIMIT<LI.LIMIT.PRODUCT> EQ 101 OR R.LIMIT<LI.LIMIT.PRODUCT> EQ 103 THEN

                LIMIT.CODE = KEY.LIST.LIMIT<S>
                COL.NO = R.LIMIT<LI.COLLAT.RIGHT>
*Line [ 103 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                TEMP.COUNT = DCOUNT(COL.NO,@SM)
                FOR I = 1 TO TEMP.COUNT
                    FN.COLL = 'FBNK.COLLATERAL' ; F.COLL = '' ; R.COLL = ''
                    CALL OPF( FN.COLL,F.COLL)
                    T.SEL = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.NO<1,1,I>:"... AND COLLATERAL.CODE EQ ":R.LIMIT<LI.COLLATERAL.CODE>
                    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
                    IF SELECTED THEN
                        CALL F.READ(FN.COLL,KEY.LIST<1>,R.COLL,F.COLL,'')
                        APP.ID =  R.COLL<COLL.APPLICATION.ID>
                        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,APP.ID,INT.APP.RATE)
                        CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.SPREAD,APP.ID,SPR.APP.RATE)
                        FIRST.INT = INT.APP.RATE + SPR.APP.RATE
                        IF SELECTED GT 1 THEN
                            FOR I = 2 TO SELECTED
                                CALL F.READ(FN.COLL,KEY.LIST<I>,R.COLL,F.COLL,'')
                                APP.ID =  R.COLL<COLL.APPLICATION.ID>
                                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,APP.ID,INT.APP.RATE)
                                CALL DBR ('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.SPREAD,APP.ID,SPR.APP.RATE)
                                APP.INT = INT.APP.RATE + SPR.APP.RATE
                                IF APP.INT GT FIRST.INT THEN
                                    FIRST.INT = APP.INT
                                END
                            NEXT I
                        END
                    END
                NEXT I
                DEBIT.INT = FIRST.INT

                IF DEBIT.INT = 0.0 THEN CONTINUE
*PRINT "DEBIT.INT ":DEBIT.INT
            END
***********CREATE OFS MESSAGE***********

            FN.OFS.SOURCE = "F.OFS.SOURCE"
            F.OFS.SOURCE  = ""
            CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)

***** SCB R15 UPG 20160628 - S
*    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
            CALL F.READ(FN.OFS.SOURCE,"SCBOFFLINE",OFS.SOURCE.REC,F.OFS.SOURCE,'')
***** SCB R15 UPG 20160628 - E

            FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
            FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
            F.OFS.IN         = 0
            F.OFS.BK         = 0
            OFS.REC          = ""
            OFS.OPERATION    = "ACCOUNT.DEBIT.INT"
            OFS.OPTIONS      = "OFS"
***OFS.USER.INFO    = "/"

            COMP = C$ID.COMPANY
            COM.CODE = COMP[8,2]
            OFS.USER.INFO = "INPUTT":COM.CODE:"/":"/" :COMP

            DAT = TODAY
            DAT = DAT[1,6]:"01"
            OFS.TRANS.ID     = R.LIMIT<LI.ACCOUNT>:"-":DAT
            OFS.MESSAGE.DATA = ""
            COMMA = ","

****************************************

            OFS.MESSAGE.DATA =  "DR.INT.RATE:1:1=":DEBIT.INT:COMMA
**********************
            CALL DBR ('ACCOUNT':@FM:AC.WORKING.BALANCE,R.LIMIT<LI.ACCOUNT>,AC.AMOUNT)
*IF AC.AMOUNT GE 0.0 THEN CONTINUE
*IF AC.AMOUNT EQ '' THEN CONTINUE

            LI.CUS = R.LIMIT<LI.LIABILITY.NUMBER>
            LI.CUS.LEN = LEN(LI.CUS)
            IF LI.CUS.LEN EQ 7 THEN
                CUS.CLASS = LI.CUS[2,1]
            END ELSE
                CUS.CLASS = LI.CUS[3,1]
            END

            IF AC.AMOUNT LT 0 THEN
                AC.AMOUNT1 = AC.AMOUNT * -1
            END
*PRINT "AC.AMOUNT " :AC.AMOUNT1
*PRINT "CUS.CALSS " :CUS.CLASS
*PRINT "----------------"

**IF AC.AMOUNT1 LT 40000 AND CUS.CLASS EQ 1 THEN
**    OFS.MESSAGE.DATA := "DR.MARGIN.RATE=0"::COMMA
**END

            IF AC.AMOUNT1 GE 40000 AND CUS.CLASS EQ 1 THEN

                OFS.MESSAGE.DATA :=  "DR.LIMIT.AMT:1:1=40000":COMMA

                OFS.MESSAGE.DATA :=  "DR.INT.RATE:1:2=":DEBIT.INT:COMMA
                OFS.MESSAGE.DATA :=  "DR.MARGIN.OPER:1:2=ADD":COMMA
                OFS.MESSAGE.DATA :=  "DR.MARGIN.RATE:1:2=2":COMMA

            END

            IF CUS.CLASS NE 1 THEN
                OFS.MESSAGE.DATA := "DR.MARGIN.OPER:1:1=ADD"::COMMA
                OFS.MESSAGE.DATA := "DR.MARGIN.RATE:1:1=2"::COMMA
            END
**********************
            IF CUS.CLASS EQ 1 THEN
                OFS.MESSAGE.DATA := "CHARGE.KEY=9"::COMMA
            END ELSE
                OFS.MESSAGE.DATA := "CHARGE.KEY=1"::COMMA
            END
**********************
            OFS.MESSAGE.DATA := "INTEREST.DAY.BASIS=B"::COMMA
            OFS.MESSAGE.DATA := "DR.BALANCE.TYPE=DAILY"::COMMA
            OFS.MESSAGE.DATA := "DR.CALCUL.TYPE=LEVEL"::COMMA

****************************************
            DAT = TODAY
            DAT = DAT[1,6]:"01"
            OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.TRANS.ID:COMMA:OFS.MESSAGE.DATA
            OFS.ID = "T":TNO:".":OFS.TRANS.ID:".":"ADI":"-":DAT

            OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

            OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
            WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM

***** SCB R15 UPG 20160628 - S

*    CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")

*            SCB.OFS.SOURCE = "SCBOFFLINE"
*            SCB.OFS.ID = '' ; SCB.OPT = ''
*            CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*            IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*                SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*            END
*            CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')

***** SCB R15 UPG 20160628 - E

****************************************
        NEXT S
    END
    TEXT = "FINISHED" ; CALL REM
    RETURN
**********************************************************************************************************************
END
