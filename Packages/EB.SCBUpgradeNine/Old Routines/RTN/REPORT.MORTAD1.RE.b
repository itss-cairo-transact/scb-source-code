* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*******************************NI7OOOOOOOOOOO****************
    SUBROUTINE REPORT.MORTAD1.RE
*
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
*
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.MORTAD1.RE'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    BT.ID = COMI
    FN.BATCH='F.SCB.BT.BATCH';F.BATCH=''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR = 'FBNK.BILL.REGISTER' ; F.B=''
    CALL OPF(FN.BR,F.BR)

    YTEXT = "Enter the BT No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CALL F.READ(FN.BATCH,COMI,R.BATCH,F.BATCH,E1)

    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""

    BR.ID  = R.BATCH<SCB.BT.OUR.REFERENCE>
    REASON = R.BATCH<SCB.BT.RETURN.REASON>
*Line [ 72 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.ID = DCOUNT(R.BATCH<SCB.BT.OUR.REFERENCE>,@VM)
    TEXT = "DE : " : DECOUNT.ID ; CALL REM
    FOR J = 1 TO DECOUNT.ID
        IF REASON NE '' THEN
            TEXT = "RES : " : REASON ; CALL REM
            CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E1)
            DRAWER.ID = R.BR<EB.BILL.REG.DRAWER>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)
            CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            ACC.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
            ACC.NO    = '1900200010001'

            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

            AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT = AMOUNT
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            BR.DATA = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.OPERATION.TYPE>

            CUR.ID    = R.BR<EB.BILL.REG.CURRENCY>
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT =  R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.RECEIVE.DATE>
* DAT  = R.BR<EB.BILL.REG.MATURITY.DATE>
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            CHQ.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>

            INPUTTER = R.BR<EB.BILL.REG.INPUTTER>
            AUTH = R.BR<EB.BILL.REG.AUTHORISER>
            INP = FIELD(INPUTTER,'_',2)
            AUTHI =FIELD(AUTH,'_',2)

            XX   = SPACE(132)  ; XX3  = SPACE(132)
            XX1  = SPACE(132)  ; XX4  = SPACE(132)
            XX2  = SPACE(132)  ; XX5  = SPACE(132)
            XX6  = SPACE(132)  ; XX7  = SPACE(132)
            XX8  = SPACE(132)  ; XX9  = SPACE(132)

            XX<1,1>[3,35]   = CUST.NAME
            XX<1,1>[3,35]   = "����� �����"

            XX<1,1>[45,15]  = '������     : '
            XX<1,1>[59,15]  = AMOUNT

            XX1<1,1>[45,15] = '��� ������ : '
            XX1<1,1>[59,15] = 1900200010001

            XX2<1,1>[45,15] = '��� ������ : '
****    XX2<1,1>[59,15] = CATEG
            XX2<1,1>[59,15] = "������ �����"

            XX3<1,1>[45,15] = '������     : '
            XX3<1,1>[59,15] = CUR

            XX4<1,1>[45,15] = '����� ���� : '
            XX4<1,1>[59,15] = MAT.DATE

            XX5<1,1>[3,15]  = '��� �����      : '
            XX5<1,1>[20,15] = CHQ.NO

            XX6<1,1>[1,15]  = '������'
            XX7<1,1>[1,15] = AUTHI

            XX6<1,1>[30,15]  = '��� �������'
            XX7<1,1>[35,15] = INP

            XX6<1,1>[60,15]  = '������'
            XX7<1,1>[60,15] = COMI

            XX8<1,1>[3,35]  = '������ ������� : '
            XX8<1,1>[20,15] = OUT.AMT

            XX9<1,1>[3,15]  = '������         : '
            XX9<1,1>[20,15] = "����� �����"
*
*-------------------------------------------------------------------
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PR.HD  ="'L'":SPACE(1):"��� ���� ������"
            PR.HD :="'L'":"������� : ":T.DAY
            PR.HD :="'L'":"����� : ":YYBRN :SPACE(45) :"����� �����"
            PR.HD :="'L'":" "
            PR.HD :="'L'":" "
            PRINT
            HEADING PR.HD
*------------------------------------------------------------------
            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX4<1,1>
            PRINT XX8<1,1>
            PRINT XX9<1,1>
            PRINT XX5<1,1>
            PRINT STR(' ',82)
            PRINT XX6<1,1>
            PRINT STR('-',82)
            PRINT XX7<1,1>

        END
    NEXT J
*===============================================================
    RETURN
END
