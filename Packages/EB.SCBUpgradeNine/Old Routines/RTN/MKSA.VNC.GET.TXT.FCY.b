* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>676</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE MKSA.VNC.GET.TXT.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.BATCH.FCY

    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN

    R.NEW(SCB.BR.OUR.REFERENCE) = ''
    R.NEW(SCB.BR.AMOUNT)        = ''
    R.NEW(SCB.BR.CURR)          = ''
    R.NEW(SCB.BR.BR.CODE)       = ''
    R.NEW(SCB.BR.BANK)          = ''


    CALL !HUSHIT(0)
*   COMAND = "sh /hq/opce/bclr/user/fcy.clearing/fcytxt.sh"
*   COMAND = "sh /life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING/fcytxt.sh"
    COMAND = "sh FCY.CLEARING/fcytxt.sh"
    EXECUTE COMAND



**------------------------------------------------------------------**
*   Path = "/hq/opce/bclr/user/fcy.clearing/FCY.CLEAR"
*    Path = " /life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING/FCY.CLEAR"
    Path = "FCY.CLEARING/FCY.CLEAR"
**------------------------------------------------------------------**
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
**------------------------------------------------------------------**
    EOF   = ''
    I     = 1
    H     = 1
    E     = ""
    ETEXT = ''
    DEP   = ''

    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
    CALL OPF( FN.BR,F.BR)

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
**-----------------------------------------------
            RECIVE.DATE = TODAY

            DAY.NOM     = OCONV(DATE(),"DW")

            IF ( DAY.NOM EQ 7 OR DAY.NOM EQ 2 ) THEN
                CALL CDT('',RECIVE.DATE,"+2W")
            END
            IF ( DAY.NOM EQ 1 OR DAY.NOM EQ 3 OR DAY.NOM EQ 4 ) THEN
                CALL CDT('',RECIVE.DATE,"+1W")
            END

**-----------------------------------------------
            MAT.DAT               = FIELD(Line,"|",2)
            DD.YEAR               = RECIVE.DATE[1,4]
            DD.MONTH              = RECIVE.DATE[5,2]
            DD.DATE               = RECIVE.DATE[7,2]
            RECIVE.DATE.1         = DD.DATE:"/":DD.MONTH:"/":DD.YEAR
            TOD =  TODAY[7,2]:"/":TODAY[5,2]:"/": TODAY[1,4]
*TEXT = RECIVE.DATE.1 ; CALL REM
            IF MAT.DAT EQ RECIVE.DATE.1 THEN
**------------20120412---------------------------------
                OUR.IDD            = FIELD(Line,"|",24)
                CALL F.READ( FN.BR,OUR.IDD, R.BR, F.BR, ETEXT)

                BR.STA =  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA>

                IF BR.STA NE '7' THEN

**------------20120412---------------------------------
*            TEXT = TOD ; CALL REM
*            IF MAT.DAT EQ TOD THEN
                    OUR.ID            = FIELD(Line,"|",24)
                    AMT               = FIELD(Line,"|",10)
                    DEPT.CODE         = FIELD(Line,"|",21)
                    CURR              = FIELD(Line,"|",6)
                    OLD.NO            = FIELD(Line,"|",8)

                    IF (  DEPT.CODE NE OLD.DEPT AND I GT 1 )  THEN
                        H = H +1
                        I = 1
                    END
*    END

                    R.NEW(SCB.BR.OUR.REFERENCE)<1,H,I> = OUR.ID
                    R.NEW(SCB.BR.AMOUNT)<1,H,I>        = AMT
                    R.NEW(SCB.BR.OLD.NO)<1,H,I>        = OLD.NO
                    CALL DBR('NUMERIC.CURRENCY':@FM:1,CURR,CUR.NAME)
                    R.NEW(SCB.BR.CURR)<1,H,I>          = CUR.NAME

                    IF DEPT.CODE[3,1] EQ '0' THEN
                        DEP = DEPT.CODE[4,1]
                    END ELSE
                        DEP = DEPT.CODE[3,2]
                    END
                    R.NEW(SCB.BR.BR.CODE)<1,H>       = DEP

                    CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,OUR.ID,MY.LOC)

                    R.NEW(SCB.BR.BANK)<1,H,I>        = MY.LOC<1,BRLR.BANK>
                    I = I + 1

                    OLD.DEPT =  DEPT.CODE
                END
            END

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ MyPath

    RETURN
**------------------------------------------------------------------**

END
