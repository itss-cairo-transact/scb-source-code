* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
*-------------------------------------------
* CREATE BY RIHAM YOUSSIF 1/09/2014
*-------------------------------------------
    SUBROUTINE REPORT.CD.SUEZ.PRIV.C

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.PROFESSION
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------
    IF V$FUNCTION NE 'R' THEN
        GOSUB INITIATE
        GOSUB PROCESS
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        RETURN
    END
*----------------------------------------------
INITIATE:
*--------
*   REPORT.ID='REPORT.CD.SUEZ.PRIV'
*    REPORT.ID='REPORT.CD.SUEZ.':ID.COMPANY[2]
* IF (OPERATOR EQ 'SCB.60445' OR OPERATOR EQ 'SCB.24457') THEN
    REPORT.ID='REPORT.CD.SUEZ.99'

    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    FN.CU     = 'FBNK.CUSTOMER'             ; F.CU   = ''      ; R.CU   = ''
    FN.COM    = 'F.COMPANY'                     ; F.COM  = ''      ; R.COM  = ''
    FN.COUT   = 'F.COUNTRY'                     ; F.COUT = ''      ; R.COUT = ''
    FN.PROF   = 'F.SCB.CUS.PROFESSION'          ; F.PROF = ''      ; R.PROF = ''
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.COM,F.COM)
    CALL OPF(FN.COUT,F.COUT)
    CALL OPF(FN.PROF,F.PROF)
*------------------------------------------------------------------------
    YTEXT = "Enter the No. : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    CALL F.READ(FN.CU,COMI,R.CU,F.CU,E1)
    TEXT = "COMI : " : COMI ; CALL REM


    BRN.NO         = R.CU<EB.CUS.COMPANY.BOOK>
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.NO,BRN)
    ID               = COMI
    DATE1            = R.CU<EB.CUS.CONTACT.DATE>
    CONT.DATE        = DATE1[1,4]:'/':DATE1[5,2]:"/":DATE1[7,2]

    ARABIC.NAME      = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
    ARABIC.NAME.2    = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME.2>

    BIRTH1           = R.CU<EB.CUS.BIRTH.INCORP.DATE>
    BIRTH.DATE       =BIRTH1[1,4]:'/':BIRTH1[5,2]:"/":BIRTH1[7,2]
    BIRTH.PLACE      = R.CU<EB.CUS.LOCAL.REF><1,CULR.TAX.EXEMPTION>

    NAT.ID           = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>

    NATION           = R.CU<EB.CUS.NATIONALITY>
    CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,NATION,NATEGP)

    GOV             = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
    ARABIC.ADDR1    = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,1>
    ARABIC.ADDR2    = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,2>

    IF GOV NE '98' THEN
        ARABIC.ADD  = ARABIC.ADDR1 :' ':  ARABIC.ADDR2
        POST.ADD    = ARABIC.ADDR1 :' ':  ARABIC.ADDR2
    END ELSE
        ARABIC.ADD  = ARABIC.ADDR1 : ' ' :ARABIC.ADDR2
        POST.ADD    = ARABIC.ADDR1 : ' ' :ARABIC.ADDR2
    END

    MAIL.ADD  = R.CU<EB.CUS.LOCAL.REF><1,CULR.EMAIL.ADDRESS>

    TELE.NO1 = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>

    TELE.NO2 = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,2>

    TELE.NO3 = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,3>

    JOB.NO  = R.CU<EB.CUS.LOCAL.REF><1,CULR.PROFESSION>
    CALL DBR('SCB.CUS.PROFESSION':@FM:SCB.PRF.DESCRIPTION,JOB.NO,JOB.TITLE)
    JOB.ADD   = R.CU<EB.CUS.LOCAL.REF><1,CULR.JADDR>

    NAME.ENG1    = R.CU<EB.CUS.LOCAL.REF><1,CULR.FNAME>
    NAME.ENG2    = R.CU<EB.CUS.LOCAL.REF><1,CULR.MNAME>
    NAME.ENG3    = R.CU<EB.CUS.LOCAL.REF><1,CULR.LNAME>
    SMS.FLAG     = R.CU<EB.CUS.LOCAL.REF><1,CULR.CU.SMS>

    US.VST.183   = R.CU<EB.CUS.LOCAL.REF><1,CULR.US.VST.183>
    US.CITIZEN   = R.CU<EB.CUS.LOCAL.REF><1,CULR.US.CITIZEN>
    US.GRN.HOLD  = R.CU<EB.CUS.LOCAL.REF><1,CULR.US.GRN.HOLD>
    US.RESIDENT  = R.CU<EB.CUS.LOCAL.REF><1,CULR.US.RESIDENT>
    US.TAXPAYER  = R.CU<EB.CUS.LOCAL.REF><1,CULR.US.TAXPAYER>
    IF SMS.FLAG EQ 'YES' THEN
        SMS.FLAG = '���'
    END ELSE
        SMS.FALG = '��'
    END

    IF US.VST.183 EQ 'YES' THEN
        US.VST.183 = '���'
    END ELSE
        US.VST.183 = '��'
    END
    IF US.CITIZEN EQ 'YES' THEN
        US.CITIZEN = '���'
    END ELSE
        US.CITIZEN = '��'
    END
    IF US.GRN.HOLD EQ 'YES' THEN
        US.GRN.HOLD = '���'
    END ELSE
        US.GRN.HOLD = '��'
    END

    IF  US.RESIDENT EQ 'YES' THEN
        US.RESIDENT = '���'
    END ELSE
        US.RESIDENT = '��'
    END

    IF  US.TAXPAYER EQ 'YES' THEN
        US.TAXPAYER = '���'
    END ELSE
        US.TAXPAYER = '��'
    END

    XX1     = SPACE(132)  ; XX2   = SPACE(132) ; XX3     = SPACE(132)
    XX4     = SPACE(132)  ; XX5   = SPACE(132) ; XX6     = SPACE(132)
    XX7     = SPACE(132)  ; XX8   = SPACE(132) ; XX9     = SPACE(132)
    XX10    = SPACE(132)  ; XX11  = SPACE(132) ; XX12    = SPACE(132)
    XX13    = SPACE(132)  ; XX14  = SPACE(132) ; XX15    = SPACE(132)
    XX16    = SPACE(132)  ; XX17  = SPACE(132) ; XX18    = SPACE(132)
    XX19    = SPACE(132)  ; XX20  = SPACE(132) ; XX21    = SPACE(132)
    XX22    = SPACE(132)  ; XX23  = SPACE(132) ; XX24    = SPACE(132)
    XX25    = SPACE(132)  ; XX26  = SPACE(132) ; XX27    = SPACE(132)
    XX28    = SPACE(132)  ; XX29  = SPACE(132) ; XX30    = SPACE(132)
    XX31    = SPACE(132)  ; XX32  = SPACE(132) ; XX33    = SPACE(132)
    XX34    = SPACE(132)  ; XX35  = SPACE(132) ; XX36    = SPACE(132)
    XX37    = SPACE(132)  ; XX38  = SPACE(132) ; XX39    = SPACE(132)
    XX40    = SPACE(132)  ; XX41  = SPACE(132) ; XX42    = SPACE(132)
    XX43    = SPACE(132)  ; XX44  = SPACE(132) ; XX45    = SPACE(132)
    XX46    = SPACE(132)  ; XX47  = SPACE(132) ; XX48    = SPACE(132)
    XX49    = SPACE(132)  ; XX50  = SPACE(132) ; XX51    = SPACE(132)
    XX277    = SPACE(132)  ; XX288  = SPACE(132)

    XX1<1,1>[3,35]     = 'REPORT.CD.SUEZ.PRIV'
    XX2<1,1>[3,35]     = '(��� ��� ���� ����� ������� ���� ������ (�����'
    XX3<1,1>[3,35]     = '�����'
    XX4<1,1>[3,100]     =  BRN


    XX5<1,1>[3,35]     = '��� ������ '
    XX6<1,1>[3,35]     = COMI

    XX7<1,1>[3,50]     = '����� ��� ������'
    XX8<1,1>[3,35]     = CONT.DATE

    XX9<1,1>[3,35]     = '(����� ������� (������ �������'
    XX10<1,1>[3,120]   = ARABIC.NAME :'  ':  ARABIC.NAME.2

    XX11<1,1>[3,35]    = '����� �������'
    XX12<1,1>[3,35]    =  BIRTH.DATE

    XX13<1,1>[3,35]    = '��� �������'
    XX14<1,1>[3,35]    =  BIRTH.PLACE

    XX15<1,1>[3,35]    = '����� �������'
    XX16<1,1>[3,35]    = NAT.ID


    XX17<1,1>[3,35]    = '�������'
    XX18<1,1>[3,60]    = NATEGP

    XX19<1,1>[3,35]    = '������ ���� '
    XX20<1,1>[3,35]    = ' '

    XX21<1,1>[3,35]    = '����� ������'
    XX22<1,1>[3,100]   = ARABIC.ADD

    XX23<1,1>[3,35]    = '����� ���������'
    XX24<1,1>[3,100]   = POST.ADD

    XX25<1,1>[3,35]    = '����� ������ ����������'
    XX26<1,1>[3,100]   = MAIL.ADD

    XX27<1,1>[3,70]    = '����� ������'
    XX28<1,1>[3,35]    = TELE.NO1 : " " : "/" : " " :TELE.NO2

    XX277<1,1>[3,70]    = '����� �������'
    XX288<1,1>[3,35]    = TELE.NO3


    XX29<1,1>[3,35]    = '����� ������ ����������'
    XX30<1,1>[3,100]   = MAIL.ADD

    XX31<1,1>[3,35]    = '������'
    XX32<1,1>[3,100]   = JOB.TITLE

    XX33<1,1>[3,35]    = '����� �����'
    XX34<1,1>[3,100]   = JOB.ADD

    XX35<1,1>[3,35]    = '����� ���������'
    XX36<1,1>[3,100]   = NAME.ENG1
    XX37<1,1>[3,100]   = NAME.ENG2
    XX38<1,1>[3,100]   = NAME.ENG3


    XX39<1,1>[3,100]   = '�� ��� �� ����� ��� �������� ������� ��������� ���� ���� �� 183 ��� �� ��� ���� �����'
    XX40<1,1>[3,100]   = US.VST.183


    XX41<1,1>[3,100]   = '�� ��� ����� ������'
    XX42<1,1>[3,100]   = US.CITIZEN

    XX43<1,1>[3,100]   = '(�� ���� ������� ��������� (������ ����'
    XX44<1,1>[3,100]   = US.GRN.HOLD


    XX45<1,1>[3,100]   = '�� ��� ���� �� �������� ������� ���������'
    XX46<1,1>[3,100]   = US.RESIDENT

    XX47<1,1>[3,100]   = '�� ��� ���� ����� �������'
    XX48<1,1>[3,100]   = US.TAXPAYER

    XX49<1,1>[3,100]   = '�������� �� ���� ������� �������'
    XX50<1,1>[3,100]   = SMS.FLAG

    PRINT XX1<1,1>
    PRINT XX2<1,1>
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT XX6<1,1>
    PRINT XX7<1,1>
    PRINT XX8<1,1>
    PRINT XX9<1,1>
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT XX19<1,1>
    PRINT XX20<1,1>
    PRINT XX21<1,1>
    PRINT XX22<1,1>
    PRINT XX23<1,1>
    PRINT XX24<1,1>
    PRINT XX25<1,1>
    PRINT XX26<1,1>
    PRINT XX27<1,1>
    PRINT XX28<1,1>
    PRINT XX277<1,1>
    PRINT XX288<1,1>
    PRINT XX29<1,1>
    PRINT XX30<1,1>
    PRINT XX31<1,1>
    PRINT XX32<1,1>
    PRINT XX33<1,1>
    PRINT XX34<1,1>
    PRINT XX35<1,1>
    PRINT XX36<1,1>
    PRINT XX37<1,1>
    PRINT XX38<1,1>
    PRINT XX39<1,1>
    PRINT XX40<1,1>
    PRINT XX41<1,1>
    PRINT XX42<1,1>
    PRINT XX43<1,1>
    PRINT XX44<1,1>
    PRINT XX45<1,1>
    PRINT XX46<1,1>
    PRINT XX47<1,1>
    PRINT XX48<1,1>
    PRINT XX49<1,1>
    PRINT XX50<1,1>
    RETURN

END
