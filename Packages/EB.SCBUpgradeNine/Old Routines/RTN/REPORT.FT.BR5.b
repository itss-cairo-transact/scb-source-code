* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>349</Rating>
*-----------------------------------------------------------------------------
*-----------------------
** Create By Ni7ooo
** Update By Nessma
*-----------------------
    SUBROUTINE REPORT.FT.BR5

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.CHQ.TYPE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT  = "��� ����� ���������" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='REPORT.FT.BR5'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*-------
    FN.BATCH = 'F.SCB.BT.BATCH'     ; F.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

    FN.BR    = 'FBNK.BILL.REGISTER' ; F.BR    = ''
    CALL OPF(FN.BR,F.BR)

    DATE.TO = TODAY[3,6]:"..."
    ID      = ID.NEW
*------------------------------------------------------------------------
    RES     = R.NEW(SCB.BT.OUR.REFERENCE)
*Line [ 82 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    RET.RES = DCOUNT(RES,@VM)

    FOR I = 1 TO RET.RES
        IF R.NEW(SCB.BT.RETURN.REASON)<1,I> EQ '' THEN
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,I>
            CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E1)
            CURR    = R.BR<EB.BILL.REG.CURRENCY>
            ACC.NO  = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,DRAWER.ID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,DRAWER.ID,LOCAL.REF)

            CUST.NAME    = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS,1>
            ACC.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.CUST.ACCT>

            BANK.NO      = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK>
            BRN.NO       = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BANK.BR>

            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME<2,2>,BANK.NO,BANK.NAME)
            CALL DBR ('SCB.BANK.BRANCH':@FM:SCB.BAB.BRANCH.NAME<2,2>,BRN.NO,BRANCH.NAME)

            CHEQ.STAT=R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BIL.CHQ.TYPE>
            CALL DBR ('SCB.BL.CHQ.TYPE':@FM:BL.CHQ.DESCRIPTION,CHEQ.STAT,CHEQ.NAME)

            CATEG.ID  = ACC.NO[11,4]
            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG)

            AMOUNT    = R.BR<EB.BILL.REG.AMOUNT>
            IN.AMOUNT = AMOUNT
            CALL WORDS.ARABIC.DEAL(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)

            BR.DATA = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.OPERATION.TYPE>

            CUR.ID  = R.BR<EB.BILL.REG.CURRENCY>
            CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR.ID,CUR)
            OUT.AMT = OUT.AMOUNT : ' ' : CUR : ' ' : '�����'

            DAT       = TODAY
            MAT.DATE  = DAT[7,2]:'/':DAT[5,2]:"/":DAT[1,4]
            CHQ.NO    = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.BILL.CHQ.NO>

            INPUTTER = R.BR<EB.BILL.REG.INPUTTER>
            AUTH     = R.BR<EB.BILL.REG.AUTHORISER>
            INP      = FIELD(INPUTTER,'_',2)
            AUTHI    = FIELD(AUTH,'_',2)
            XX       = SPACE(132)
*--------------------------------------------
            PR.HD  = "REPORT.FT.BR5"
            HEADING PR.HD
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
            YYBRN  = FIELD(BRANCH,'.',2)
            DATY   = TODAY
            T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
            PRINT SPACE(5) : SPACE(1):"��� ���� ������"
            PRINT SPACE(5) : "������� : ":T.DAY
            PRINT SPACE(5) : "����� : ":YYBRN
*----------------------------------------------
            XX<1,1>[3,35]  = CUST.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,35]  = CUST.ADDRESS
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '������     : '
            XX<1,1>[30,15]  = AMOUNT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� ������ : '
            XX<1,1>[30,15]  = ACC.NO
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� ������ : '
            XX<1,1>[30,15]  = CATEG
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '������     : '
            XX<1,1>[30,15]  = CUR
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '����� ���� : '
            XX<1,1>[30,15]  = MAT.DATE
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� �����      : '
            XX<1,1>[20,15]  = CHQ.NO:'':CHEQ.NAME
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[1,15]   = '������'
            XX<1,1>[20,15]  = AUTHI
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '��� �������'
            XX<1,1>[35,15]  = ID.NEW
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '������'
            XX<1,1>[30,15]  = INP
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,30]   = '������ ������� : '
            XX<1,1>[20,15]  = OUT.AMT
            PRINT XX<1,1>
            XX<1,1> = ""

            XX<1,1>[3,15]   = '������         : '
            XX<1,1>[20,15]  = BR.DATA
            PRINT XX<1,1>
            XX<1,1> = ""
        END
    NEXT I
    RETURN
*===========================================================
PRINT.HEAD:
*----------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":"������� : ":T.DAY
    PR.HD :="'L'":"����� : ":YYBRN
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50) : "����� ��� ������"
    PR.HD :="'L'":"REPORT.FT.BR5"
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
    RETURN
*============================================================
END
