* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*---------------------------------AHMED NAHRAWY (FOR EVER)--------------------------------------------
    SUBROUTINE RATE.PERCENT.AHMED(ARG)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST

    ARG = ID.NEW
    AA  = R.NEW(LD.INTEREST.SPREAD)
    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.INTEREST.RATE,ARG,RATE)
    CALL DBR('LD.LOANS.AND.DEPOSITS':@FM:LD.NEW.INT.RATE,ARG,RATE2)
    IF RATE2 EQ '' THEN
        ARG = RATE + AA
        ARG = ARG :"%"
    END
    IF RATE2 NE '' THEN
        ARG = RATE2 + AA
        ARG = ARG :"%"
    END
    RETURN
END
