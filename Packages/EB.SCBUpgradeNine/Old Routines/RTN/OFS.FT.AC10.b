* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
    PROGRAM OFS.FT.AC10

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*---------------------------------------
    GOSUB CHECKEXIST

    IF SW2 = 0 THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD

***** SCB R15 UPG 20160628 - S
*        CALL START.OFS.TSA("BNK/OFS.CONV.PROCESS")
***** SCB R15 UPG 20160628 - E

    END
    RETURN
*---------------------------------------
CHECKEXIST:
    KEY.LIST="" ; SELECTED="" ;  ER.FT="" ; SW2 = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC45'"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
        SW2 = 1
    END ELSE
        SW2 = 0
    END
    RETURN
*---------------------------------------
INITIALISE:
    OPENSEQ "MECH" , "T":TNO:".FT.AC10.":TODAY TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE "DELETE ":"MECH":' ':"T":TNO:".FT.AC10.":TODAY
        HUSH OFF
    END
    OPENSEQ "MECH" , "T":TNO:".FT.AC10.":TODAY TO BB ELSE
        CREATE BB THEN
            PRINT "FILE T":TNO:".FT.AC10 CREATED IN MECH"
        END ELSE
            STOP "Cannot create T":TNO:".FT.AC10 File IN MECH"
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.FT.H = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = ''
    CALL OPF(FN.FT.H,F.FT.H)

    COMMA = ","
    V.DATE = TODAY

    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC10' AND DEBIT.VALUE.DATE EQ ": V.DATE

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,E1)

            DB.ACCT   = R.FT<FT.DEBIT.ACCT.NO>
            DB.CUR    = R.FT<FT.DEBIT.CURRENCY>
            DB.AMT    = R.FT<FT.DEBIT.AMOUNT>
            CR.AMT    = R.FT<FT.CREDIT.AMOUNT>

            DB.V.DATE = R.FT<FT.DEBIT.VALUE.DATE>
            CR.V.DATE = R.FT<FT.CREDIT.VALUE.DATE>
            CR.ACCT   = R.FT<FT.CREDIT.ACCT.NO>
            CR.CUR    = R.FT<FT.CREDIT.CURRENCY>

            REC.RATE = R.FT<FT.TREASURY.RATE>
            SCB = R.FT<FT.ORDERING.BANK>
            CUR.BASE = R.FT<FT.BASE.CURRENCY>
            IDD   = 'FUNDS.TRANSFER,BR,INPUTT99//EG0010099,'
            IDD.R = 'FUNDS.TRANSFER,BR/R,INPUTT99//EG0010099,'
**************************************************CRAETE FT BY OFS**********************************************
            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC45":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CR.CUR:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":DB.CUR:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":CR.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":DB.ACCT:COMMA

            IF CR.AMT NE '' THEN
                OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":CR.AMT:COMMA
            END

            IF DB.AMT NE '' THEN
                OFS.MESSAGE.DATA :=  "CREDIT.AMOUNT=":DB.AMT:COMMA
            END

            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DB.V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":CR.V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":KEY.LIST<I>:COMMA
            OFS.MESSAGE.DATA :=  "TREASURY.RATE=":REC.RATE:COMMA

            OFS.MESSAGE.DATA :=  "ORDERING.BANK=":SCB:COMMA
            IF CUR.BASE THEN
                OFS.MESSAGE.DATA :=  "BASE.CURRENCY=":CUR.BASE:COMMA
            END
**OFS.MESSAGE.DATA :=  "SEND.PAYMENT.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO"

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
***** SCB R15 UPG 20160628 - S
            GOSUB OPM.PROCESS
***** SCB R15 UPG 20160628 - E

        NEXT I
    END

***** REV FT ******
    DAT.H = TODAY[3,6]:'...'
*DEBUG
    T.SEL2 = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DATE.TIME LIKE ":DAT.H:" AND TRANSACTION.TYPE EQ 'AC10' AND RECORD.STATUS EQ 'REVE'"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
            AC10.ID = KEY.LIST2<NN>[1,12]
            T.SEL3 = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH TRANSACTION.TYPE EQ AC45 AND DEBIT.THEIR.REF EQ ":AC10.ID
            CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
            IF SELECTED3 THEN
                FOR KK = 1 TO SELECTED3

                    MSG.DATA = IDD.R:KEY.LIST3<KK>
                    WRITESEQ MSG.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                NEXT KK
            END

        NEXT NN
    END

**** COPY TO OFS *****

    EXECUTE 'COPY FROM MECH TO OFS.IN':' ':"T":TNO:".FT.AC10.":TODAY
    EXECUTE 'DELETE ':"MECH":' ':"T":TNO:".FT.AC10.":TODAY

**********************

    RETURN

***** SCB R15 UPG 20160628 - S
*--
OPM.PROCESS:
*--

*    SCB.OFS.SOURCE = "SCBOFFLINE"
*    SCB.OFS.ID = '' ; SCB.OPT = ''
*    CALL OFS.POST.MESSAGE(OFS.REC, SCB.OFS.ID, SCB.OFS.SOURCE, SCB.OPT)
*    IF FIELD(SCB.OFS.ID,"-",2) EQ '' THEN
*        SCB.OFS.ID = SCB.OFS.ID:"-":SCB.OFS.SOURCE
*    END
*    CALL LOG.WRITE("F.OFS.MESSAGE.QUEUE",SCB.OFS.ID,OFS.REC,'')
*
    RETURN
***** SCB R15 UPG 20160628 - E
END
