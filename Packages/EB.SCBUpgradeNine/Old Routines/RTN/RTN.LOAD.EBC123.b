* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeNine  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeNine
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
*----------------------------------
*-- CREATE BY NESSMA ON 2016/03/15
*----------------------------------
    SUBROUTINE RTN.LOAD.EBC123

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAD.MSCC
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------
    EXECUTE "CLEAR-FILE F.SCB.LOAD.MSCC"
*EXECUTE "sh MSCC/fx_file.sh"

    GOSUB OPF.FILES
    GOSUB INITIAL

    TEXT = "�� ����� �����"
    CALL REM
    RETURN
*---------------------------------------------
OPF.FILES:
*---------
    FN.TMP  = "F.SCB.LOAD.MSCC" ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)
    RETURN
*---------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ''
    R.LINE = ""

    ERR='' ; ER=''

    FILE.NAME = "EBC123.csv"
    DIR.NAMEE = "EBC123"

    OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
        TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
        RETURN
    END

    IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

    LINE.NUMBER = 1
    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM BB THEN

            R.LINE = TRIM(R.LINE)

            TMP.ID = "EBC123*" : TIMEDATE(): "*" : OPERATOR : "*": LINE.NUMBER
            CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)

            R.TMP<SCB.MSCC.FIELD.1> = TRIM(FIELD(R.LINE,",",1))
            R.TMP<SCB.MSCC.FIELD.2> = TRIM(FIELD(R.LINE,",",2))
            R.TMP<SCB.MSCC.FIELD.3> = TRIM(FIELD(R.LINE,",",3))
            R.TMP<SCB.MSCC.FIELD.4> = TRIM(FIELD(R.LINE,",",4))
            R.TMP<SCB.MSCC.FIELD.5> = TRIM(FIELD(R.LINE,",",5))
            R.TMP<SCB.MSCC.FIELD.6> = TRIM(FIELD(R.LINE,",",6))
            R.TMP<SCB.MSCC.FIELD.7> = TRIM(FIELD(R.LINE,",",7))
            R.TMP<SCB.MSCC.FIELD.8> = TRIM(FIELD(R.LINE,",",8))
            R.TMP<SCB.MSCC.FIELD.9> = TRIM(FIELD(R.LINE,",",9))
            R.TMP<SCB.MSCC.FIELD.10> = TRIM(FIELD(R.LINE,",",10))
            R.TMP<SCB.MSCC.FIELD.11> = TRIM(FIELD(R.LINE,",",11))
            R.TMP<SCB.MSCC.FIELD.12> = TRIM(FIELD(R.LINE,",",12))
            R.TMP<SCB.MSCC.FIELD.13> = TRIM(FIELD(R.LINE,",",13))
            R.TMP<SCB.MSCC.FIELD.14> = TRIM(FIELD(R.LINE,",",14))
            R.TMP<SCB.MSCC.FIELD.15> = TRIM(FIELD(R.LINE,",",15))
            R.TMP<SCB.MSCC.FIELD.16> = TRIM(FIELD(R.LINE,",",16))
            R.TMP<SCB.MSCC.FIELD.17> = TRIM(FIELD(R.LINE,",",17))
            R.TMP<SCB.MSCC.FIELD.18> = TRIM(FIELD(R.LINE,",",18))
            R.TMP<SCB.MSCC.FIELD.19> = TRIM(FIELD(R.LINE,",",19))
            R.TMP<SCB.MSCC.FIELD.20> = TRIM(FIELD(R.LINE,",",20))
            R.TMP<SCB.MSCC.FIELD.21> = TRIM(FIELD(R.LINE,",",21))
            R.TMP<SCB.MSCC.FIELD.22> = TRIM(FIELD(R.LINE,",",22))

            WRITE R.TMP TO F.TMP , TMP.ID  ON ERROR
                STOP 'CAN NOT WRITE RECORD ':TMP.ID:' TO FILE ':F.TMP
            END
*CALL F.WRITE(FN.TMP,TMP.ID, R.TMP)
*CALL JOURNAL.UPDATE(TMP.ID)
        END ELSE
            EOF = 1
        END
        LINE.NUMBER ++
    REPEAT
    CLOSESEQ BB
    RETURN
*------------------------------------------------------------------
END
