* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-------------------------------------------------------------
    SUBROUTINE SCB.B.CHECK.OFS.STATUS
*-------------------------------------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*-------------------------------------------------------------
    FN.STATUS.FILE = "F.SCB.OFS.STATUS.FILE"
    F.STATUS.FILE = ""
    CALL OPF(FN.STATUS.FILE,F.STATUS.FILE)
*
    FN.OFS.QUEUE = "F.OFS.RESPONSE.QUEUE"
    F.OFS.QUEUE = ""
    CALL OPF(FN.OFS.QUEUE,F.OFS.QUEUE)
*
    OFS.ID = "SCBOFFLINE"
    FN.OFS = "F.OFS.SOURCE"
    F.OFS = ""
    CALL OPF(FN.OFS,F.OFS)
*
    CALL F.READ(FN.OFS,"SCBOFFLINE",OFS.REC,F.OFS,"")
    OUT.DIR = OFS.REC<OFS.SRC.OUT.QUEUE.DIR>
*
    CMD = "SELECT ":FN.STATUS.FILE:" WITH @ID UNLIKE ...PROCESSED"
    EXECUTE CMD RTNLIST STATUS.KEY.LIST
*
    LOOP
    WHILE READNEXT STATUS.KEY FROM STATUS.KEY.LIST DO
        CALL F.READ(FN.STATUS.FILE,STATUS.KEY,STATUS.FILE.REC,F.STATUS.FILE,READ.ERR)
        IF STATUS.FILE.REC THEN
            STATUS.KEY.ORG = STATUS.KEY
            TOT.RECS = DCOUNT(STATUS.FILE.REC,@FM)
            IS.PENDING = @FALSE
            OFS.STATUS.REC = ""
            FOR IDX = 1 TO TOT.RECS
                OFS.QUEUE.ID = STATUS.FILE.REC<IDX>
                IF OFS.QUEUE.ID THEN
                    OFS.RESP.ID = OFS.QUEUE.ID["-",1,1]:".1"
                    OFS.QUEUE.REC = ""
*
                    IF NOT(INDEX(OFS.QUEUE.ID,"*PROCESSED*",1)) THEN
                        CALL F.READ(FN.OFS.QUEUE,OFS.RESP.ID,OFS.QUEUE.REC,F.OFS.QUEUE,OFS.READ.ERR)
                        IF OFS.QUEUE.REC THEN
                            OFS.STATUS.REC := OFS.QUEUE.ID["*",1,1]:"*PROCESSED*":OFS.QUEUE.REC<1>:"*":OFS.QUEUE.REC<3>:@FM
                            GOSUB FLUSH.OUTPUT.FILE
                        END ELSE
                            OFS.STATUS.REC := OFS.QUEUE.ID["*",1,1]:"*PENDING":@FM
                            IS.PENDING = @TRUE
                        END
                    END ELSE
                        OFS.STATUS.REC := OFS.QUEUE.ID:@FM
                    END
                END
            NEXT IDX
            IF IS.PENDING THEN
                STATUS.KEY["*",3,1] = "PENDING"
            END ELSE
                STATUS.KEY["*",3,1] = "PROCESSED"
            END
            CALL F.WRITE(FN.STATUS.FILE,STATUS.KEY,OFS.STATUS.REC)
            IF STATUS.KEY.ORG # STATUS.KEY THEN
                CALL F.DELETE(FN.STATUS.FILE,STATUS.KEY.ORG)
            END
        END
    REPEAT
    RETURN
*-------------------------------------------------------------
FLUSH.OUTPUT.FILE:
*-------------------------------------------------------------
    FILE.NAME = OUT.DIR:"/":STATUS.KEY["*",1,1]:"-":TODAY
    OPENSEQ FILE.NAME TO FILE.HANDLER ELSE
        CREATE FILE.HANDLER ELSE
            CALL FATAL.ERROR("Unable to write to output file :":FILE.NAME)
        END
        WEOFSEQ FILE.HANDLER
    END
    OFS.RESP.STR = OFS.QUEUE.REC<3>:"/":OFS.QUEUE.REC<1>:"/":OFS.QUEUE.REC<2>
    WRITESEQ OFS.RESP.STR APPEND TO FILE.HANDLER ELSE
        CALL FATAL.ERROR("Unable to write to output file :":FILE.NAME:" string - ":OFS.QUEUE.REC<2>)
    END
    RETURN
*-------------------------------------------------------------
END
*-------------------------------------------------------------
