* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.LD.CHNG.RATE.21064.A

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*---------------------------------------
    YTEXT  = 'Y/N ���� ����� ������  '

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN

        GOSUB INITIALISE
        GOSUB BUILD.RECORD

        TEXT = "�� �������� �� ��������" ; CALL REM

    END ELSE
        TEXT = '�� ������ �� �������� ' ; CALL REM
    END

    RETURN
*---------------------------------------
INITIALISE:

    OPENSEQ "MECH" , "LD.21064.RATE.CHNG.A" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LD.21064.RATE.CHNG.A"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LD.21064.RATE.CHNG.A" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LD.21064.RATE.CHNG.A CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LD.21064.RATE.CHNG.A File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    COMMA = ","

    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21064 AND VERSION.NAME EQ ',RATE.MORT' "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            COMP.COD     = R.LD<LD.CO.CODE>
            USER.INFO = "AUTO.LD":"//":COMP.COD
            IDD = 'LD.LOANS.AND.DEPOSITS,RATE.MORT/A,':USER.INFO:',':KEY.LIST<I>
**************************************************CRAETE FT BY OFS**********************************************
            OFS.MESSAGE.DATA  =  ""

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LD.21064.RATE.CHNG.A'
    EXECUTE 'DELETE ':"MECH":' ':"LD.21064.RATE.CHNG.A"

    RETURN
END
