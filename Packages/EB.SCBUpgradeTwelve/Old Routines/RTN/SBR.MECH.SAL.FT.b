* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.MECH.SAL.FT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
************ OPEN FILES ****************

    YTEXT = "����� ����� ���� ����� : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    DB.ACCT = COMI


    FN.MCH = "F.SCB.SUEZ.MECH.SAL"  ; F.MCH  = ""
    CALL OPF (FN.MCH,F.MCH)
    TOT.AMT = 0

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

************ CHECK ACCOUNT **************
    CALL F.READ(FN.AC,DB.ACCT,R.AC,F.AC,E9)
    IF NOT(E9) THEN
        WS.ACCT.NAME = R.AC<AC.ACCOUNT.TITLE.1>
        TEXT = '��� ������ ���� : ' : WS.ACCT.NAME ; CALL REM

        YTEXT = "Y/N �� ���� ���������"
        CALL TXTINP(YTEXT, 8, 22, "1", "A")
        FLG1 = COMI

        IF FLG1 = 'Y' OR FLG1 = 'y' THEN
            GO TO PROCESS
        END ELSE
            TEXT = '�� ������ �� ��������' ; CALL REM
            GO TO EXIT.PROG
        END

    END ELSE
        TEXT = '��� ���� ������' ; CALL REM
        TEXT = '�� ������ �� ��������' ; CALL REM
        GO TO EXIT.PROG

    END
*****************************************
PROCESS:

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION  = "MECH"
    SCB.VERSION1 = "MECH1"

    SCB.OFS.HEADER  = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,,,"
    SCB.OFS.HEADER1 = SCB.APPL : "," : SCB.VERSION1 : "/I/PROCESS,,,"
    WS.FILE.COMP = ID.COMPANY[2]
    COMP = ID.COMPANY
    WS.TD = TODAY

    OPENSEQ "MECH" , "MECH.SAL.OUT":WS.FILE.COMP TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"MECH.SAL.OUT":WS.FILE.COMP
        HUSH OFF
    END
    OPENSEQ "MECH" , "MECH.SAL.OUT":WS.FILE.COMP TO BB ELSE
        CREATE BB THEN
        END ELSE
            STOP 'Cannot create MECH.SAL.OUT File IN MECH'
        END
    END
*--------------------------------------------------------------------
**************** DEBIT FROM COMPANY CREDIT TO MIGRATION  ************

    T.SEL1 = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '1' AND TRN.CODE EQ 'AC48' AND CO.CODE EQ ":COMP:" AND RECEIVE.DATE EQ ":WS.TD
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR NN = 1 TO SELECTED1
            CALL F.READ(FN.MCH,KEY.LIST1<NN>,R.MCH,F.MCH,E4)
            TOT.AMT += R.MCH<MCH.AMOUNT>
            R.MCH<MCH.STATUS>  = "2"
            R.MCH<MCH.DB.ACCT> = DB.ACCT

            CALL F.WRITE(FN.MCH,KEY.LIST1<NN>,R.MCH)
            CALL JOURNAL.UPDATE(KEY.LIST1<NN>)

        NEXT NN

        CALL F.READ(FN.AC,DB.ACCT,R.AC,F.AC,E7)
        DB.CUR       = R.AC<AC.CURRENCY>
        COMP.ID      = R.AC<AC.CO.CODE>[2]
        CR.ACCT.MRG  = DB.CUR:"12001000100":COMP.ID

        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC48"
        OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":DB.CUR
        OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":DB.CUR
        OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
        OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT.MRG
        OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":TOT.AMT
        OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
        OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
        OFS.MESSAGE.DATA :=  ",LOCAL.REF:42:1=":DB.ACCT
        OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
        OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

        SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END

** CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

        BB.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.DATA TO BB ELSE
        END

        TEXT = '�� ����� �� ���� ������ �������� ������ ������' ; CALL REM
    END

    IF NOT(SELECTED1) THEN
        TEXT = '����� ����� �� ���� ������ ��� �� ���' ; CALL REM
    END
*--------------------------------------------------------------------
**************** DEBIT FROM MIGRATION CREDIT TO CLIENTS ************

    T.SEL = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '2' AND TRN.CODE EQ 'AC48' AND CO.CODE EQ ":COMP:" AND RECEIVE.DATE EQ ":WS.TD
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
            CALL F.READ(FN.MCH,KEY.LIST<KK>,R.MCH,F.MCH,E4)
            AMT      = R.MCH<MCH.AMOUNT>
            CR.ACCT  = R.MCH<MCH.ACCOUNT.NO>

            CALL F.READ(FN.AC,CR.ACCT,R.AC,F.AC,E8)
            CR.CUR      = R.AC<AC.CURRENCY>
            COMP.ID2    = R.AC<AC.CO.CODE>[2]
**  DB.ACCT.MRG = CR.CUR:"12001000100":COMP.ID2
            DB.ACCT.MRG = CR.ACCT.MRG
*------------------------------------------------------------------
            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC48"
            OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":CR.CUR
            OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":CR.CUR
            OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT.MRG
            OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
            OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":AMT
            OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
            OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
            OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
            OFS.MESSAGE.DATA :=  ",LOCAL.REF:42:1=":DB.ACCT
            OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
            OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

            SCB.OFS.MESSAGE = SCB.OFS.HEADER1 : OFS.MESSAGE.DATA
            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END

** CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END

            ERR.FLAG = FIELD(SCB.OFS.MESSAGE,'/',3)[0,2]
            FT.ID    = FIELD(SCB.OFS.MESSAGE,'/',1)

            IF ERR.FLAG NE "-1" THEN
                R.MCH<MCH.STATUS> = "3"
                R.MCH<MCH.REF>    = FT.ID
                CALL F.WRITE(FN.MCH,KEY.LIST<KK>,R.MCH)
                CALL JOURNAL.UPDATE(KEY.LIST<KK>)
            END

        NEXT KK
        TEXT = '�� ����� �� ������ ������ �������� ������� �������' ; CALL REM
    END
    IF NOT(SELECTED) THEN
        TEXT = '����� ����� �� ������ ������ ��� �� ���' ; CALL REM
    END

*----------------------------------
EXIT.PROG:

    RETURN
************************************************************
END
