* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-80</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBR.NEW.CU.EGSERV.SRL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EGSRV.INDEX

    GOSUB INITIALISE
    GOSUB SEL.COMPANY
    PRINT 'PROGRAM.COMPLETED'

    RETURN
*-----------------------------------------------
INITIALISE:
*----------
    KEY.LIST=""   ; SELECTED=""   ;  ER.MSG=""
    KEY.LIST1=""  ; SELECTED1=""  ;  ER.MSG1=""
    KEY.LIST2=""  ; SELECTED2=""  ;  ER.MSG2=""

    FN.CU = 'FBNK.CUSTOMER'  ; F.CU = '' ; R.CU= ''
    CALL OPF(FN.CU,F.CU)

    FN.EGI = 'F.SCB.EGSRV.INDEX'  ; F.EGI = '' ; R.EGI = ''
    CALL OPF(FN.EGI,F.EGI)

    FN.COMP = 'F.COMPANY'  ; F.COMP= '' ; R.COMP= ''
    CALL OPF(FN.COMP,F.COMP)

    WS.SRL = 0
    WS.EGSRV.SRL = ''

    RETURN
*----------------------------------------------------
SEL.COMPANY:
*-----------
    T.SEL.COMP = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(T.SEL.COMP,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR ICOMP = 1 TO SELECTED
            CO.CODE = KEY.LIST<ICOMP>
            CALL F.READ(FN.EGI,CO.CODE,R.EGI1,F.EGI1,E4)

            BRN.SRL = R.EGI1<EGSRV.BRANCH.END.SRL>[4,8]
            NEW.BRN.SRL = TRIM(BRN.SRL,'0',"L")

            T.SEL.CU = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND COMPANY.BOOK EQ ":KEY.LIST<ICOMP>:" AND PACK.NO EQ '' WITHOUT SECTOR IN (5010 5020)  BY @ID"

            GOSUB READ.CUSTOMER

        NEXT ICOMP
    END
    RETURN
*****************************************************
READ.CUSTOMER:
*-------------
    WS.EGSRV.SRL = ''
    CALL EB.READLIST(T.SEL.CU,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR ICU = 1 TO SELECTED1

            CALL F.READ(FN.CU,KEY.LIST1<ICU>,R.CU,F.CU,E1)
            CUS.NO = KEY.LIST1<ICU>
            GOSUB WRITE.CU.DATA
            GOSUB WRITE.EGSERV.FILE

        NEXT ICU
    END
    GOSUB WRITE.COMPANY.DATA

    RETURN
*************************************************************
WRITE.CU.DATA:
    NEW.BRN.SRL ++
    WS.SRL.MASK  = NEW.BRN.SRL + 100000000
    WS.EGSRV.SRL = 'B':KEY.LIST<ICOMP>[2]:WS.SRL.MASK[8]

    R.CU<EB.CUS.LOCAL.REF,CULR.EGSRV.SRL> = WS.EGSRV.SRL

    CALL F.WRITE(FN.CU,KEY.LIST1<ICU>,R.CU)
    CALL JOURNAL.UPDATE(KEY.LIST1<ICU>)

    RETURN
*----------------------------------------
WRITE.EGSERV.FILE:
    CALL F.READ(FN.EGI,WS.EGSRV.SRL,R.EGI,F.EGI,E2)

    R.EGI<EGSRV.CUSTOMER.ID> = KEY.LIST1<ICU>

    CALL F.WRITE(FN.EGI,WS.EGSRV.SRL,R.EGI)
    CALL JOURNAL.UPDATE(WS.EGSRV.SRL)

    RETURN
*----------------------------------------
WRITE.COMPANY.DATA:
    IF WS.EGSRV.SRL NE '' THEN
        CALL F.READ(FN.EGI,KEY.LIST<ICOMP>,R.EGI,F.EGI,E3)

        R.EGI<EGSRV.BRANCH.END.SRL> = WS.EGSRV.SRL

        CALL F.WRITE(FN.EGI,KEY.LIST<ICOMP>,R.EGI)
        CALL JOURNAL.UPDATE(KEY.LIST<ICOMP>)
    END
    RETURN
*----------------------------------------
