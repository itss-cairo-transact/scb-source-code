* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
************************CREATED BY RIHAM YOUSSEF 20180111***********
*-----------------------------------------------------------------------------
*    PROGRAM SCB.CBE.5002.11
     SUBROUTINE SCB.CBE.5002.11

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.REPORT
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SCB.CBE.5002.1.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.CBE.5002.1.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.CBE.5002.1.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.CBE.5002.1.TXT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.CBE.5002.1.TXT File IN &SAVEDLISTS&'
        END
    END
******************************************************************
    CUST.FILE.NO     = ''
    CBE.NO           = ''
    AMT              = ''
    CUR              = ''
    RATE             = 0
    BEN.CUST         =  ''
    NAT.NAME         = ''
    BENF.1           = ''
    BENF.2           = ''
    FT.NO            = ''


*    DAT.1 = TODAY
*    CALL CDT("",DAT.1,'-1W')
    YTEXT = "Enter report date"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    DAT.1 = COMI
    TEXT =  'DATE : ':DAT.1 ; CALL REM


    HEAD.DESC = "��� ������":"|"
    HEAD.DESC := "��� ����� �������":"|"
    HEAD.DESC := "����� ������ ������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "��� ������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "��� �������� ":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "����� �� �������1":"|"
    HEAD.DESC := "���������" : "|"
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.SCB.CBE.REPORT' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)


    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)


    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.LC.HIS = 'FBNK.LETTER.OF.CREDIT$HIS' ; F.LC.HIS = ''
    CALL OPF(FN.LC.HIS,F.LC.HIS)

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.DR.HIS = 'FBNK.DRAWINGS$HIS' ; F.DR.HIS = ''
    CALL OPF(FN.DR.HIS,F.DR.HIS)



    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.SEC.N = 'F.SCB.NEW.SECTOR' ; F.SEC.N = ''
    CALL OPF(FN.SEC.N,F.SEC.N)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)

    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* CURRENT ACCOUNTS SELECTION *************
    T.SEL  = "SELECT ":FN.CBE:" WITH AMT.1 NE '' AND SYS.DATE EQ ":DAT.1:" BY @ID"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
        *    IF I = SELECTED THEN DEBUG
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
         *   TRANS.ID = KEY.LIST<I>
            CUR.CBE      =  R.CBE<CBE.R.CURR>
            AMT.CBE      = R.CBE<CBE.R.AMT.1>
            CUR = 'USD'
            CALL F.READ(FN.CCY,'USD',R.CCY,F.CCY,E1)
            AMT.RATE     = R.CCY<SBD.CURR.MID.RATE>
            AMT.USD  = AMT.CBE / AMT.RATE
            IF AMT.USD GE '100000' THEN
                GOSUB GET.DETAIL
                   IF NAT.NAME NE 'EGYPT' THEN
                GOSUB PRINT.DET
                  END
            END

        NEXT I
    END
RETURN
*==============================================================
PRINT.DET:
    BB.DATA = CUST.NAME:"|"
    BB.DATA := COM.REG.NO:"|"
    BB.DATA := CBE.NO:"|"
    BB.DATA := AMT:"|"
    BB.DATA := CUR:"|"
    BB.DATA := RATE:"|"
    BB.DATA := BEN.CUST:"|"
    BB.DATA := NAT.NAME:"|"
    BB.DATA := BENF.2:"|"
    BB.DATA := TF.NO:"|"


    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*===============================================================
GET.DETAIL:

    TF.NO = KEY.LIST<I>
    AMT        = R.CBE<CBE.R.AMT.1>
    CUR        = R.CBE<CBE.R.CURR>
    AMT = AMT /1000000
    AMT  =FMT(AMT,"L0,")
    TF.NO      = KEY.LIST<I>
    IF TF.NO[1,2] EQ 'TF' THEN
        MM = TF.NO[1,14]
        CALL F.READ(FN.DR,MM,R.DR,F.DR,E22)
        IF NOT(E22) THEN
            IF CUR EQ 'OTHR' THEN
                CUR     = R.DR<TF.DR.DRAW.CURRENCY>
            END ELSE
                CUR = CUR
            END
            LC.CUS    = R.DR<TF.DR.DRAWDOWN.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS,CUSID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOCAL.REF)
            CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>

            COM.REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
            CBE.NO     = LOCAL.REF<1,CULR.CBE.NO>
        END

        IF E22 THEN
            NN = TF.NO[1,14]:';1'
            CALL F.READ(FN.DR.HIS,NN,R.DR.HIS,F.DR.HIS,E33)
            IF CUR EQ 'OTHR' THEN
                CUR    = R.DR.HIS<TF.DR.DRAW.CURRENCY>
            END ELSE
                CUR = CUR
            END
            LC.CUS.H    = R.DR.HIS<TF.DR.DRAWDOWN.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS.H,CUSID.HIS)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID.HIS,LOCAL.REF.H)
            CUST.NAME  = LOCAL.REF.H<1,CULR.ARABIC.NAME>
            COM.REG.NO = LOCAL.REF.H<1,CULR.COM.REG.NO>
            CBE.NO     = LOCAL.REF.H<1,CULR.CBE.NO>
        END

    END
    IF TF.NO[1,2] EQ 'TF' THEN
        MM = TF.NO[1,12]
        CALL F.READ(FN.LC,MM,R.LC,F.LC,E22)
        IF NOT(E22) THEN
            LC.CUS    = R.LC<TF.DR.DRAWDOWN.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS,CUSID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOCAL.REF)

            IF CUR EQ 'OTHR' THEN
                CUR     = R.DR<TF.DR.DRAW.CURRENCY>
            END ELSE
                CUR = CUR
            END
            CUST.FILE.NO = LOCAL.REF<1,CULR.COM.REG.NO>
            CALL F.READ(FN.CCY,CUR,R.CCY,F.CCY,E1)
            AMT.RATE     = R.CCY<SBD.CURR.MID.RATE>
            AMT.RATE     =  FMT(AMT.RATE,"L2,")
            RATE         = AMT.RATE
            BEN.CUST     = R.LC<TF.LC.BENEFICIARY>[1,37]
            NAT.NAME     = R.LC<TF.LC.EXPIRY.PLACE>
            ORGIN.GOODS  =  R.LC<TF.LC.LOCAL.REF><1,LCLR.ORIGIN.OF.GOODS>
        END ELSE

            NN = TF.NO[1,12]:';1'
            CALL F.READ(FN.LC.HIS,NN,R.LC.HIS,F.LC.HIS,E33)

            IF CUR EQ 'OTHR' THEN
                CUR     = R.DR.HIS<TF.DR.DRAW.CURRENCY>
            END ELSE
                CUR = CUR
            END
            LC.CUS    = R.LC.HIS<TF.DR.DRAWDOWN.ACCOUNT>
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,LC.CUS,CUSID)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOCAL.REF)
*  CBE.NO       = LOCAL.REF<1,CULR.CBE.NO>
            CUST.FILE.NO = LOCAL.REF<1,CULR.COM.REG.NO>

            AMT.RATE      = R.CCY<SBD.CURR.MID.RATE>
            AMT.RATE      =  FMT(AMT.RATE,"L2,")
            RATE          = AMT.RATE
            BEN.CUST      = R.LC.HIS<TF.LC.BENEFICIARY>[1,37]
            NAT.NAME      = R.LC.HIS<TF.LC.EXPIRY.PLACE>

            ORGIN.GOODS  =  R.LC.HIS<TF.LC.LOCAL.REF><1,LCLR.ORIGIN.OF.GOODS>
        END
    END
    RETURN
*===============================================================
