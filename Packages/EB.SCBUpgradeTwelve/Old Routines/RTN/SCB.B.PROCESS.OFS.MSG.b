* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-------------------------------------------------------------
    SUBROUTINE SCB.B.PROCESS.OFS.MSG(FILE.ID)
*-------------------------------------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_SCB.B.PROCESS.OFS.MSG.COMMON
*-------------------------------------------------------------
    FILE.REC = ""
    CALL F.READ(IN.DIR,FILE.ID,FILE.REC,IN.DIR.FP,"")
*    IF FILE.REC THEN
* Take a backup --
    CALL F.WRITE(FN.OFS.BK,FILE.ID,FILE.REC)
* Process each OFS message ---
    STATUS.FILE.REC = ""
    LOOP
    WHILE READNEXT OFS.MESSAGE FROM FILE.REC DO
        USER.ID = OFS.MESSAGE["//EG00",1,1][",",3,1]
        R.SON = ""
        CALL F.READ(FN.USER.SON,USER.ID,R.SON,FP.USER.SON,"")
        USER.OPTION = ""
        IF R.SON THEN
            USER.OPTION = R.SON<1>
        END
        CALL OFS.POST.MESSAGE(OFS.MESSAGE,OPM.ID,OFS$SOURCE.ID,USER.OPTION)
        IF USER.OPTION THEN
            STATUS.FILE.REC := OPM.ID:"-":OFS$SOURCE.ID:"-":USER.OPTION:"*INQUEUE"::@FM
        END ELSE
            STATUS.FILE.REC := OPM.ID:"-":OFS$SOURCE.ID:"-":"*INQUEUE"::@FM
        END
    REPEAT
* Delete the file
    CALL F.DELETE(IN.DIR,FILE.ID)
    IF STATUS.FILE.REC THEN
        STATUS.FILE.KEY = FILE.ID:"*":UNIQUEKEY():"*PROCESSING"
        CALL F.WRITE(FN.STATUS.FILE,STATUS.FILE.KEY,STATUS.FILE.REC)
    END
*    END
    RETURN
*-------------------------------------------------------------
END
*-------------------------------------------------------------
