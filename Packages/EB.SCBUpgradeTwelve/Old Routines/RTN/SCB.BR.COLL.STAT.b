* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>598</Rating>
*-----------------------------------------------------------------------------
* Version 6 22/05/01  GLOBUS Release No. G12.1.01 11/12/01

      SUBROUTINE SCB.BR.COLL.STAT

*********************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*************************************************************************

      GOSUB INITIALISE                   ; * Special Initialising

      GOSUB DEFINE.PARAMETERS

      IF LEN(V$FUNCTION) GT 1 THEN
         GOTO V$EXIT
      END

      CALL MATRIX.UPDATE


*************************************************************************

* Main Program Loop

      LOOP

         CALL RECORDID.INPUT

      UNTIL MESSAGE = 'RET' DO

         V$ERROR = ''

         IF MESSAGE = 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION         ; * Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
               CALL FUNCTION.DISPLAY
               V$FUNCTION = ''
            END

         END ELSE

            GOSUB CHECK.ID               ; * Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE = 'REPEAT' THEN
               GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB PROCESS.DISPLAY        ; * For Display applications

         END

MAIN.REPEAT:
      REPEAT

V$EXIT:
      RETURN                             ; * From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.DISPLAY:

* Display the record fields.

      IF SCREEN.MODE EQ 'MULTI' THEN
         CALL FIELD.MULTI.DISPLAY
      END ELSE
         CALL FIELD.DISPLAY
      END

      RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.


      RETURN


*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

      IF INDEX('V',V$FUNCTION,1) THEN
         E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
         CALL ERR
         V$FUNCTION = ''
      END

      RETURN

*************************************************************************

INITIALISE:
*
* Define often used checkfile variables
*
*Line [ 137 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CHK.ACCOUNT = "ACCOUNT":@FM:AC.SHORT.TITLE:@FM:"L"

*      CHK.CUSTOMER = "CUSTOMER":FM:EB.CUS.SHORT.NAME:FM:'.A'
*      CHK.CUSTOMER.SECURITY = "CUSTOMER.SECURITY":FM:0:FM:'':FM:"CUSTOMER":FM:EB.CUS.SHORT.NAME:FM:'..S'
*      CHK.SAM = "SEC.ACC.MASTER":FM:SC.SAM.ACCOUNT.NAME:FM:'..S'


      RETURN

*************************************************************************

DEFINE.PARAMETERS:* SEE 'I_RULES' FOR DESCRIPTIONS *


      MAT F = "" ; MAT N = "" ; MAT T = ""
      MAT CHECKFILE = "" ; MAT CONCATFILE = ""
      ID.CHECKFILE = "" ; ID.CONCATFILE = ""

      ID.F = "CU.TYPE.YEARMONTH" ; ID.N = "25" ; ID.T = "A"
*
      Z=0
*
      Z+=1 ; F(Z) = "XX<STATUS.ID" ; N(Z) = "3" ; T(Z) = ""
*Line [ 161 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
      CHECKFILE(Z) = "SCB.BR.STATUS":@FM:1:@FM:"L"
      Z+=1 ; F(Z) = "XX-OPEN.BAL" ; N(Z) = "19" ; T(Z) = "AMT"
      Z+=1 ; F(Z) = "XX-DB.MOVE" ; N(Z) = "19" ; T(Z) = "AMT"
      Z+=1 ; F(Z) = "XX-CR.MOVE" ; N(Z) = "19" ; T(Z) = "AMT"
      Z+=1 ; F(Z) = "XX>CLOSE.BAL" ; N(Z) = "19" ; T(Z) = "AMT"

      Z+=1 ; F(Z) = "RESERVED.FIELD1" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.FIELD2" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.FIELD3" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.FIELD4" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'
      Z+=1 ; F(Z) = "RESERVED.FIELD5" ; N(Z) = "35" ; T(Z) = "ANY"
      T(Z)<3> = 'NOINPUT'

*
* Live files DO NOT require V = Z + 9 as there are not audit fields.
* But it does require V to be set to the number of fields
*

      V = Z

      RETURN

*************************************************************************

   END
