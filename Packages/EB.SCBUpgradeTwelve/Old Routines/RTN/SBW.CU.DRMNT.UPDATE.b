* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>362</Rating>
*-----------------------------------------------------------------------------
****** CREATED BY Mohamed Sabry  *******
*****      2013/11/10            *******
****************************************

*    SUBROUTINE SBW.CU.DRMNT.UPDATE
    PROGRAM SBW.CU.DRMNT.UPDATE

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB OPEN.FILES
    GOSUB SEL.CU
    RETURN
****************************************
INITIATE:

    WS.3Y.DATE = TODAY
    CALL ADD.MONTHS(WS.3Y.DATE,-36)

    WS.1Y.DATE = TODAY
    CALL ADD.MONTHS(WS.1Y.DATE,-12)

    CU.SCB.OFS.SOURCE  = "TESTOFS"
    CU.SCB.APPL        = "CUSTOMER"
    CU.SCB.VERSION     = "SCB.DRMNT.OFS"
    CU.SCB.OFS.HEADER  = CU.SCB.APPL : "," : CU.SCB.VERSION


    OPENSEQ "OFS.MNGR.IN" , "DRMNT-":TODAY TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"DRMNT-":TODAY
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , "DRMNT-":TODAY TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create DRMNT-":TODAY:" File IN OFS.MNGR.IN'
        END
    END
    OPENSEQ "OFS.MNGR.OUT" , "DRMNT-":TODAY TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"DRMNT-":TODAY
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "DRMNT-":TODAY TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create DRMNT-":TODAY:" File IN OFS.MNGR.OUT'
        END
    END
    RETURN
****************************************
OPEN.FILES:
*Master files

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ER.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = '' ; R.MM = '' ; ER.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.LC = "FBNK.LETTER.OF.CREDIT"      ; F.LC = "" ; R.LC = '' ; ER.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.OFS.IN = "OFS.IN"
    F.OFS.IN = ''
    CALL OPF(FN.OFS.IN,F.OFS.IN)

    FN.USER = "F.USER"
    FV.USER = ""
    CALL OPF(FN.USER,FV.USER)

    FN.USER.SIGN.ON.NAME = "F.USER.SIGN.ON.NAME"
    FV.USER.SIGN.ON.NAME = ""
    CALL OPF(FN.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME)

* Index files

* Index file for account
    FN.IND.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC = '' ; R.IND.AC = '' ; ER.IND.AC = ''
    CALL OPF(FN.IND.AC,F.IND.AC)

*Index file for LD's , LG's and MM's
    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

*LETTER.OF.CREDIT Index !!! (import)
    FN.LCA = "FBNK.LC.APPLICANT"         ; F.LCA = ""
    CALL OPF(FN.LCA,F.LCA)

**LETTER.OF.CREDIT Index  !!! (exp)
    FN.LCB = "FBNK.LC.BENEFICIARY"   ; F.LCB = ""
    CALL OPF(FN.LCB,F.LCB)

    RETURN
****************************************
SEL.CU:
    T.SEL = "SELECT ":FN.CU:" WITH @ID UNLIKE 994... AND POSTING.RESTRICT LT 89 AND DRMNT.CODE NE 1 AND SCCD.CUSTOMER NE 'YES' AND "
    T.SEL:= "POSTING.RESTRICT NE 70 AND CREDIT.CODE EQ '' AND CREDIT.STAT EQ '' WITHOUT SECTOR IN ( 5010 5020 ) OR @ID IN (444333 5556667 10001000 333333 999888 601010 222222 111111 306306 500500) BY COMPANY.BOOK BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR ICU = 1 TO SELECTED
            WS.IGNR.CUS   = ''
            WS.3Y.CUS     = ''
            WS.LST.TRNS.D = ''

            CALL F.READ(FN.CU,KEY.LIST<ICU>,R.CU,F.CU,ER.CU)
            WS.CU.ID   = KEY.LIST<ICU>
            WS.BR.CODE = R.CU<EB.CUS.COMPANY.BOOK>[2]

            GOSUB CHK.LD
            GOSUB CHK.LC
            GOSUB CHK.AC

            IF WS.IGNR.CUS NE '' THEN
                GOTO NEXT.CU
            END
            IF  WS.LST.TRNS.D NE '' THEN
                IF WS.3Y.CUS NE '' THEN
                    IF WS.LST.TRNS.D LT WS.3Y.DATE THEN
                        BB.IN.DATA  = WS.CU.ID:"*":'3':"*":WS.LST.TRNS.D:"*":"DRMNT":"*":WS.3Y.DATE:"*":WS.LST.TRNS.D
                        WRITESEQ BB.IN.DATA TO BB.IN ELSE
                        END
                        GOSUB RUN.CU.OFS
                    END
                END ELSE
                    IF WS.LST.TRNS.D LT WS.1Y.DATE THEN
                        BB.IN.DATA  = WS.CU.ID:"*":'1':"*":WS.LST.TRNS.D:"*":"DRMNT":"*":WS.1Y.DATE:"*":WS.LST.TRNS.D
                        WRITESEQ BB.IN.DATA TO BB.IN ELSE
                        END
                        GOSUB RUN.CU.OFS
                    END
                END
            END
NEXT.CU:
        NEXT ICU
    END
    RETURN
****************************************
CHK.LD:
*--------
    WS.IGNR.CUS = ''
    WS.3Y.CUS   = ''

    CALL F.READ(FN.IND.LD,WS.CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
    WHILE LD.ID:POS.LD
        IF LD.ID[1,2] EQ 'LD' THEN
            GOSUB LD.REC
        END
    REPEAT
    RETURN
****************************************
LD.REC:
*--------
    CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ER.LD)
    REC.STA        = R.LD<LD.STATUS>
    IF REC.STA NE 'LIQ' THEN
        WS.GL.ID   = R.LD<LD.CATEGORY>
        WS.CY.ID   = R.LD<LD.CURRENCY>
        WS.LD.AMT  = R.LD<LD.AMOUNT>
        IF WS.LD.AMT GT 0 THEN
            IF WS.GL.ID EQ 21096 OR WS.GL.ID EQ 21097 THEN
                WS.IGNR.CUS    = 'Y'
            END
            IF WS.GL.ID GE 21050 AND WS.GL.ID LE 21063 THEN
                WS.IGNR.CUS    = 'Y'
            END
            IF WS.GL.ID GE 21001 AND WS.GL.ID LE 21010 THEN
                WS.3Y.CUS      = 'Y'
            END
            IF WS.GL.ID GE 21017 AND WS.GL.ID LE 21042 THEN
                WS.IGNR.CUS    = 'Y'
            END
            IF WS.GL.ID GE 21101 AND WS.GL.ID LE 21103 THEN
                WS.IGNR.CUS    = 'Y'
            END
        END
    END
    RETURN
****************************************
CHK.LC:
    CALL F.READ(FN.LCA,WS.CU.ID,R.LCA,F.LCA,ER.LCA )
    LOOP
        REMOVE LC.ID FROM R.LC SETTING POS.LC
    WHILE LC.ID:POS.LC
        CALL F.READ(FN.LC,LC.ID,R.LC,F.LC,ETEXT.LC)
        WS.LC.CHK       =  R.LC<TF.LC.OPERATION>
        IF WS.LC.CHK NE "" THEN
            WS.IGNR.CUS = 'Y'
        END
    REPEAT
    RETURN
****************************************
CHK.AC:
*--------
    CALL F.READ(FN.IND.AC,WS.CU.ID,R.IND.AC,F.IND.AC,ER.IND.AC)
    LOOP
        REMOVE AC.ID FROM R.IND.AC SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
        WS.GL.ID         = R.AC<AC.CATEGORY>
        WS.OPN.DATE      = R.AC<AC.OPENING.DATE>
        WS.CU.CR.DATE    = R.AC<AC.DATE.LAST.CR.CUST>
        WS.CU.DR.DATE    = R.AC<AC.DATE.LAST.DR.CUST>

        IF WS.GL.ID[1,1] EQ 9 THEN
            GOTO NEXT.REC
        END
        IF WS.GL.ID GE 1101 AND WS.GL.ID LE 1599 THEN
            WS.IGNR.CUS      = 'Y'
        END
        IF WS.GL.ID GE 6500 AND WS.GL.ID LE 6599 THEN
            WS.3Y.CUS      = 'Y'
        END

        IF WS.LST.TRNS.D EQ '' THEN
            IF ((WS.CU.CR.DATE EQ '') AND (WS.CU.DR.DATE EQ '')) THEN
                WS.LST.TRNS.D = WS.OPN.DATE
            END
        END
        IF WS.CU.CR.DATE GT WS.LST.TRNS.D THEN
            WS.LST.TRNS.D = WS.CU.CR.DATE
        END
        IF WS.CU.DR.DATE GT WS.LST.TRNS.D THEN
            WS.LST.TRNS.D = WS.CU.DR.DATE
        END
NEXT.REC:
    REPEAT
    RETURN
****************************************
GEN.OFS.USER:

    OPERATOR        = "INPUTT":WS.BR.CODE
    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")

*Line [ 290 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.OVR.CONT = DCOUNT(R.USER<EB.USE.OVERRIDE.CLASS>,@VM)
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "DRMT"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "NPOS"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "APOS"
    WS.OVR.CONT ++
    R.USER<EB.USE.OVERRIDE.CLASS,WS.OVR.CONT> = "OVER"

    RETURN
*------------------------------------------------------------------------------------------
RUN.CU.OFS:
    CU.OFS.MESSAGE.DATA =  ",DRMNT.CODE::=":"1"

    CU.SCB.OFS.MESSAGE = CU.SCB.OFS.HEADER : "/I/PROCESS,AUTO.CHRGE//EG00100":WS.BR.CODE:",":WS.CU.ID :CU.OFS.MESSAGE.DATA

    GOSUB GEN.OFS.USER

*    CALL OFS.GLOBUS.MANAGER(CU.SCB.OFS.SOURCE, CU.SCB.OFS.MESSAGE)

    Y.OFS.ID = UNIQUEKEY()
    CHANGE '-' TO '' IN Y.OFS.ID
    Y.OFS.ID = "DORMANT-":TODAY:"-":Y.OFS.ID
    WRITE CU.SCB.OFS.MESSAGE ON F.OFS.IN,Y.OFS.ID ON ERROR
    END

*    BB.OUT.DATA  = CU.SCB.OFS.MESSAGE
*    WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
*    END

    RETURN
*------------------------------------------------------------------------------------------

****************************************
END
