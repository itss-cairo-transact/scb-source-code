* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.LD.SCHD.CBE

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.ACCOUNT
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
    $INSERT T24.BP I_F.CUSTOMER.ACCOUNT
    $INSERT T24.BP I_F.LMM.ACCOUNT.BALANCES
    $INSERT T24.BP I_F.LMM.SCHEDULES.PAST
    $INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT T24.BP I_LD.ENQ.COMMON
    $INSERT T24.BP I_LD.SCH.LIST
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR

    GOSUB INIT
    GOSUB PROCESS1

    TEXT = '�� �������� �� ��������' ; CALL REM

    RETURN

*-----------------------------------------------------------------------------------------------
INIT:
*----
    FN.LM = 'F.LMM.ACCOUNT.BALANCES' ; F.LM = '' ; Y.ID='' ; R.LM=''
    LM.ERR1='' ; Y.TOTAL=0 ; SEL.CMD='' ; SEL.LIST=''
    RET.CODE='' ; NO.OF.REC = 0 ; SEP=","

    FN.LP='F.LMM.SCHEDULES.PAST' ; Y.CUSTOMER='' ; TEMP=0
    YF.ENQ = "F.ENQUIRY" ; F.ENQ = ""
    FN.CUSTOMER.ACCOUNT = 'F.CUSTOMER.ACCOUNT'
    FV.CUSTOMER.ACCOUNT = ''
    FN.CUS = 'FBNK.CUSTOMER'
    FV.CUS = '' ; R.CUS = '' ; ER.CUS = ''

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD = ''
    TOTAL.PD= ''  ; T.SEL.PD = ""

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    R.AC = ''

    FN.SDR = 'FBNK.STMT.ACCT.DR'
    F.SDR = ''
    R.SDR = ''
    ER.SDR = ''

    CALL OPF(FN.LM,F.LM)
    CALL OPF(YF.ENQ,F.ENQ)
    CALL OPF(FN.CUSTOMER.ACCOUNT,FV.CUSTOMER.ACCOUNT)
    CALL OPF(FN.CUS,FV.CUS)
    CALL OPF(FN.PD,F.PD)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.SDR,F.SDR)

*----------------------------------- START OPEN FILE ----------------------------
    OPENSEQ "&SAVEDLISTS&" , "LD.SCHD.CBE.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"LD.SCHD.CBE.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "LD.SCHD.CBE.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LD.SCHD.CBE.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create LD.SCHD.CBE.csv File IN &SAVEDLISTS&'
        END
    END
    NN = 0
*----------------------------------- END OPEN FILE -----------------------------
*----------------------------------- CREATE HEADER -----------------------------
    DAT.1 = TODAY[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')
    WS.VAL.DATE = DAT.1[1,6]:"..."

    SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG IN (21064) AND LD.VALUE.DATE LIKE ":WS.VAL.DATE:" AND LD.LOAN.TYPE IN (301 302 303 304 305 313 314 315 316 317 332 333 334 335 336) BY LD.CATEG"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,RET.CODE)
    LOOP
        REMOVE Y.ID FROM SEL.LIST SETTING POS
    WHILE Y.ID:POS
        LMM.ID = Y.ID
        READ R.RECORD FROM F.LM,LMM.ID ELSE R.RECORD = ""
        CALL LD.ENQ.INT.I

        ID = LMM.ID
        CALL E.LD.SCHED.LIST
        TOTAL.INT = 0
*Line [ 109 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        FOR @VM1 = 1 TO DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM)
            NN++
*Line [ 112 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NEXT @VM1
    REPEAT


    BB.DATA  = "<":NO.OF.REC:","
    BB.DATA := NN:">"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

**    HEAD.DESC = ""
**    HEAD.DESC = "ID.No,"
**    HEAD.DESC := "Date,"
**    HEAD.DESC := "Outstanding,"
**    HEAD.DESC := "Int.Rate"


**    BB.DATA = HEAD.DESC
**    WRITESEQ BB.DATA TO BB ELSE
**        PRINT " ERROR WRITE FILE "
**    END
*------------------------------ END CREATE HEADER -------------------------------
    RETURN
*---------------------------------------------------------------------------------
PROCESS1:
*-------

    READ R.ENQ FROM F.ENQ,"LD.BALANCES.FULL" ELSE R.ENQ = ""
    SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG IN (21064) AND LD.VALUE.DATE LIKE ":WS.VAL.DATE:" AND LD.LOAN.TYPE IN (301 302 303 304 305 313 314 315 316 317 332 333 334 335 336) BY LD.CATEG"

    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,RET.CODE)
    LOOP
        REMOVE Y.ID FROM SEL.LIST SETTING POS
    WHILE Y.ID:POS
        LMM.ID = Y.ID
        READ R.RECORD FROM F.LM,LMM.ID ELSE R.RECORD = ""
        CALL LD.ENQ.INT.I     ;* Open files and store in common

        ID = LMM.ID
        CALL E.LD.SCHED.LIST
        TOTAL.INT = 0


*Line [ 157 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        FOR @VM1 = 1 TO DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM)

            CATEG       = R.RECORD<LD.SL.CATEGORY>
            INT.RATE    = R.RECORD<LD.SL.CURRENT.RATE>
            CURR        = R.RECORD<LD.SL.CURRENCY>
            CUS.ID      = R.RECORD<LD.SL.CUSTOMER.NO>

            CALL F.READ(FN.CUS,CUS.ID,R.CUS,FV.CUS,ER.CUS)
            CUS.NSN     = R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>

*Line [ 168 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            EVENT.DATE  = R.RECORD<LD.SL.EVENT.DATE,@VM1>
*Line [ 170 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AMT.DUE     = R.RECORD<LD.SL.TOTAL.PAYMENT,@VM1>
*Line [ 172 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            PRINC.AMT   = R.RECORD<LD.SL.PRIN.AMOUNT,@VM1>
*Line [ 174 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            INT.AMOUNT  = R.RECORD<LD.SL.INT.AMOUNT,@VM1>
*Line [ 176 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CHRG.AMOUNT = R.RECORD<LD.SL.CHG.AMOUNT,@VM1>
*Line [ 178 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FEE.AMOUNT  = R.RECORD<LD.SL.FEE.AMOUNT,@VM1>
*Line [ 180 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            COMM.AMOUNT = R.RECORD<LD.SL.COMM.AMOUNT,@VM1>
*Line [ 182 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            OUTS.AMOUNT = R.RECORD<LD.SL.RUNNING.BAL,@VM1>

            WS.DATE = FMT(EVENT.DATE,"####-##-##")
*-------------------------------------------------------------------------------------------------------------------------------
            BB.DATA  = CUS.NSN:","
            BB.DATA := WS.DATE:","

            IF OUTS.AMOUNT LT 0 THEN OUTS.AMOUNT = OUTS.AMOUNT * -1

            BB.DATA := OUTS.AMOUNT:","
            BB.DATA := INT.RATE

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            CATEG       = ""
            INT.RATE    = ""
            CURR        = ""
            CUS.ID      = ""
            CUS.NSN     = ""
            EVENT.DATE  = ""
            CUS.NAME    = ""
            AMT.DUE     = ""
            PRINC.AMT   = ""
            INT.AMOUNT  = ""
            CHRG.AMOUNT = ""
            FEE.AMOUNT  = ""
            COMM.AMOUNT = ""
            OUTS.AMOUNT = ""
            TOTAL.PD    = ""
            LON.TYP     = ""
*----------------------------------------------------------------------------

*Line [ 217 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        NEXT @VM1

    REPEAT

    RETURN
*----------------------------------------------------------------------------

END
