* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.MECH.SAL.FILE.DEL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
    FN.MCH = "F.SCB.SUEZ.MECH.SAL"  ; F.MCH  = ""
    CALL OPF (FN.MCH,F.MCH)
    COMP  = ID.COMPANY
    WS.TD = TODAY

*--------------------------------------------------------------------

    WS.SEL.ID = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '1' AND TRN.CODE EQ 'AC48' AND RECEIVE.DATE EQ ":WS.TD:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(WS.SEL.ID,KEY.LIST,"",SELECTED,ER.MSG)
    YTEXT  = '����� ���� ��� ��� ���� = ':SELECTED
    YTEXT := 'Y/N �� ���� �����  '
    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLAG = COMI
    IF FLAG = 'Y' OR FLAG = 'y' THEN

        EXECUTE WS.SEL.ID
        EXECUTE 'DELETE F.SCB.SUEZ.MECH.SAL'
        TEXT = '�� ����� �����' ; CALL REM
    END ELSE
        TEXT = '�� ������ �� ��������' ; CALL REM
    END


    RETURN
************************************************************
END
