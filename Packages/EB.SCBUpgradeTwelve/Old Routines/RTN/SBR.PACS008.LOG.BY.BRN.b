* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-112</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.PACS008.LOG.BY.BRN
*    PROGRAM SBR.PACS008.LOG.BY.BRN

*-------- CREATED BY NOHA HAMED 10/05/2015 ---

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBR.PACS008.LOG'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.PAC  = 'F.SCB.ACH.PACS008'; F.PAC = ''
    R.PAC    = ''
    CALL OPF(FN.PAC,F.PAC)


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    COMP = C$ID.COMPANY
    COM.CODE = COMP[6,4]

    KEY.LIST = ""  ; SELECTED=""  ;  ER.MSG=""
    T.DATE   = ''
    VAL.DATE = ""
    FIL.PERP = ""
    RETURN

*========================================================================
PROCESS:

*------------------TODAY PACS008 SELECTION-------------------------

    YTEXT = "Enter ACH Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    T.DATE  = COMI

    YTEXT = "Enter FILE TYPE : "
    CALL TXTINP(YTEXT, 8, 22, "4", "A")
    FIL.TYP = COMI

    TOT.AMT = 0
    T.COUNT = 0
    T.SEL   = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND TRN.DATE EQ '' AND CHARGER.BEARER LIKE ...":FIL.TYP:" AND CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" BY REQ.SETTLEMENT.DATE"

    BEGIN CASE
    CASE FIL.TYP EQ 'SSBE'
        TIT = "�.������"
    CASE FIL.TYP EQ 'CASH'
        TIT = "���"
    CASE FIL.TYP EQ 'SUBB'
        TIT = "������"
    CASE FIL.TYP EQ 'SALA'
        TIT = "������"
    CASE FIL.TYP EQ 'PENS'
        TIT = "������"
    CASE FIL.TYP EQ 'CBET'
        TIT = "�. �����"
    CASE FIL.TYP EQ 'ALL'
        TIT = '�� �������'
        T.SEL   = "SELECT F.SCB.ACH.PACS008 WITH @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND TRN.DATE EQ '' AND CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" BY REQ.SETTLEMENT.DATE"
    CASE OTHERWISE
        TIT = FIL.TYP
    END CASE
    GOSUB PRINT.HEAD
*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            PAC.ID     = KEY.LIST<I>
            CALL F.READ(FN.PAC,PAC.ID,R.PAC,F.PAC,ER.PAC)
*-- ADDED BY NESSMA 2015/08/09
* IF I EQ 1 THEN
*    VAL.DATE   = R.PAC<PACS8.REQ.SETTLEMENT.DATE>
*    GOSUB PRINT.HEAD
* END
*----------------------------

            VAL.DATE   = R.PAC<PACS8.REQ.SETTLEMENT.DATE>
            ACCT.NO    = R.PAC<PACS8.CREDITOR.ACCOUNT.NO>
            CUS.NAME   = R.PAC<PACS8.CREDITOR.NAME>
            BRN.NAME   = R.PAC<PACS8.CREDITOR.PARTY.BRANCH.NAME>
            AMT        = R.PAC<PACS8.AMOUNT>
            FIL.PERP   = R.PAC<PACS8.CHARGER.BEARER>
            TOT.AMT    = TOT.AMT + AMT
            T.COUNT    = T.COUNT + 1
            IF R.PAC<PACS8.CHARGER.BEARER> EQ 'CASH' AND R.PAC<PACS8.PURPOSE> EQ 'COLL' THEN
                FIL.PERP = 'BILL'
            END

            GOSUB  REPORT.WRITE
        NEXT I
        YY = ''
        PRINT YY
        YY<1,1>[1,10]  = "����� = ":T.COUNT
        YY<1,1>[82,15]  = "�������� = ":TOT.AMT
        PRINT YY
        YY = ''
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END

    RETURN

*===============================================================
REPORT.WRITE:

    XX  = SPACE(132)
    XX<1,I>[1,20]   = PAC.ID
    XX<1,I>[21,15]  = BRN.NAME
    XX<1,I>[36,35]  = CUS.NAME
    XX<1,I>[80,16]  = ACCT.NO
    XX<1,I>[100,15] = AMT
    XX<1,I>[112,10] = VAL.DATE
    XX<1,I>[125,5]  = FIL.PERP


    PRINT XX<1,I>

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    Q.DATE = T.DATE
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
*PR.HD :="'L'":SPACE(30):"��� ����� �������� �������"
    PR.HD :="'L'":SPACE(40):" ����� ��� ":TIT
*  PR.HD := SPACE(3) : "������ ��" : SPACE(3) : VAL.DATE
    PR.HD := SPACE(3) : "������ " : SPACE(3) : T.DATE
    PR.HD :="'L'":SPACE(40):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(10):"�����":SPACE(10):"��� ������":SPACE(35):"��� ������":SPACE(10):"������":SPACE(07):"�.����"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
END
