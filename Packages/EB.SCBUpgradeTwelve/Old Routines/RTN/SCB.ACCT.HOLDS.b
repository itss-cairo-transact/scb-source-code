* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>136</Rating>
*-----------------------------------------------------------------------------
* Version 5 02/06/00  GLOBUS Release No. G11.0.00 29/06/00

      SUBROUTINE SCB.ACCT.HOLDS

*MODIFICATIONS
*************************************************************************
*18/05/00 - GB0001261
*           Jbase changes.
*          All the commented lines containing the keyword ERROR
*          has been changed to V$ERROR

*
*
*MODIFICATIONS
*****************************************************************
*18/05/00 - GB0001261
*           Jbase changes.
*           All commented lines containing the key word ERROR has been
*           changed to V$ERROR.

*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

*************************************************************************

      GOSUB DEFINE.PARAMETERS

      IF LEN(V$FUNCTION) GT 1 THEN
         GOTO V$EXIT
      END

      CALL MATRIX.UPDATE

REM > GOSUB INITIALISE                ;* Special Initialising

*************************************************************************

* Main Program Loop

      LOOP

         CALL RECORDID.INPUT

      UNTIL MESSAGE = 'RET' DO

         V$ERROR = ''

         IF MESSAGE = 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION         ; * Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
               CALL FUNCTION.DISPLAY
               V$FUNCTION = ''
            END

         END ELSE

REM >       GOSUB CHECK.ID                     ;* Special Editing of ID
REM >       IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE = 'REPEAT' THEN
               GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            CALL TABLE.DISPLAY           ; * For Table Files

         END

MAIN.REPEAT:
      REPEAT

V$EXIT:
      RETURN                             ; * From main program

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.


      RETURN


*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

      IF INDEX('V',V$FUNCTION,1) THEN
         E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
         CALL ERR
         V$FUNCTION = ''
      END

      RETURN

*************************************************************************

INITIALISE:


      RETURN

*************************************************************************


DEFINE.PARAMETERS:* SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

   MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
      ID.CHECKFILE = ""
       ID.CONCATFILE = ""

    ID.F  = "SCB.ACCT.HOLDS"; ID.N  = "11.1"; ID.T  = "D"
    Z = 0


    Z+=1 ; F(Z)= "XX.ACCOUNT.NO" ; N(Z) = "17.1" ; T(Z)= "A"
   * T(Z)<3> = 'NOINPUT'


    Z+=1 ; F(Z)= "RESERVED"          ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'




    V = Z + 9
      RETURN
*************************************************************************

   END
