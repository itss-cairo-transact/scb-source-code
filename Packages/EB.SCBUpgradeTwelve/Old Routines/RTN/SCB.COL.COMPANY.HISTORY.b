* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SCB.COL.COMPANY.HISTORY
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.COL = 'FBNK.COLLATERAL$HIS' ; F.COL = ''
    CALL OPF(FN.COL,F.COL)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    TD = TODAY[3,6]:"..."
    T.SEL = "SELECT ":FN.COL:" WITH DATE.TIME LIKE ":TD:" AND COMPANY.BOOK UNLIKE EG..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            COMP.BOOK = ''
            CALL F.READ(FN.COL,KEY.LIST<I>,R.COL,F.COL,E1)
            CUST.NO = FIELD(KEY.LIST<I>,".",1)

            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST.NO,COMP.BOOK)
            R.COL<COLL.LOCAL.REF,COLR.COMPANY.BOOK> = COMP.BOOK
**WRITE  R.COL TO F.COL , KEY.LIST<I> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.COL
**END
            CALL F.WRITE(FN.COL,KEY.LIST<I>,R.COL)
        NEXT I
    END
    RETURN
END
