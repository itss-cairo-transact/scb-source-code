* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*SUBROUTINE SBY.ECL.BANKS.TXT
    PROGRAM SBY.ECL.BANKS.TXT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.FI.RATING
*-----------------------------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "ECL.BANKS.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ECL.BANKS.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ECL.BANKS.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ECL.BANKS.CSV  CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ECL.BANKS.CSV File IN &SAVEDLISTS&'
        END
    END
*----------------------------------------------------------------

    FN.CUS = 'FBNK.CUSTOMER'        ; F.CUS = ''     ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.RATE = 'F.SCB.CUST.FI.RATING'        ; F.RATE = ''     ; R.RATE = ''
    CALL OPF(FN.RATE,F.RATE)

*T.SEL = "SELECT FBNK.CUSTOMER WITH @ID IN (99400098 99400100 99400176 99400445 99400586 99423100 99425601 99450107 99454000 99454100 99474200 99474400 99476100 99480500 99480800 99483000 99490900 99492000 99495000 99499900 99499901) BY @ID"
    T.SEL = "SELECT FBNK.CUSTOMER WITH @ID LIKE 994... BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)


    IF SELECTED THEN
        BB.DATA = "Customer_ID,Customer_Name,Classification,Country,Openning_Date,Rating,PERFORMING/NONPERFORMING,Long_Term_Rate,Short_Term_Rate,Rating_Agency,Rating_Date"
        WRITESEQ BB.DATA TO BB ELSE PRINT " ERROR WRITE FILE "
    END

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,E1)
        CUS.ID     = KEY.LIST<I>
        CUS.NAME   = R.CUS<EB.CUS.SHORT.NAME>
        SEC.NO     = R.CUS<EB.CUS.SECTOR>
        CALL DBR('SECTOR':@FM:EB.SEC.DESCRIPTION,SEC.NO,SEC.NAME)
        RISK.RATE  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.RATEING>
        OPEN.DATE = R.CUS<EB.CUS.CONTACT.DATE>
        COUNTRY.ID = R.CUS<EB.CUS.NATIONALITY>
        CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,COUNTRY.ID,COUNTRY.NAME)
        CALL F.READ(FN.RATE,CUS.ID,R.RATE,F.RATE,E2)
        LONG.RATE     = R.RATE<CU.RAT.LONG.TRM.RAT>
        SHORT.RATE    = R.RATE<CU.RAT.SHORT.TRM.RAT>
        RATING.AGENCY = R.RATE<CU.RAT.RATING.AGENCY>
        RATING.DATE   = R.RATE<CU.RAT.RATING.DATE>
******************************
***GET NEW RATING**************

        BB.DATA  = CUS.ID:','
        BB.DATA := CUS.NAME:','
        BB.DATA := SEC.NAME:','
        BB.DATA := COUNTRY.NAME:','
        BB.DATA := OPEN.DATE:','
        BB.DATA := RISK.RATE:','
        BB.DATA := 'Performing':','
        BB.DATA := LONG.RATE:','
        BB.DATA := SHORT.RATE:','
        BB.DATA := RATING.AGENCY:','
        BB.DATA := RATING.DATE

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    NEXT I

*************************************************************
END
