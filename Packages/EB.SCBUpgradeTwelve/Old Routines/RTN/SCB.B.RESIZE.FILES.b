* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*---------------------------------------------------
    SUBROUTINE SCB.B.RESIZE.FILES(FILE.NAME)
*---------------------------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TSA.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_SCB.B.RESIZE.FILES.COMMON
*---------------------------------------------------
    FILE.STATUS = ""
    SESSION.ID = AGENT.NUMBER:".SCB.B.RESIZE.FILES.LOG"
    BLOCK.SIZE = "4096"
    OPEN "",FILE.NAME TO FILE.HANDLER THEN
        STATUS FILE.STATUS FROM FILE.HANDLER THEN
            FILE.SIZE = FILE.STATUS<6>
            FILE.SEP = FILE.STATUS<23>
            FILE.MODULO = FILE.STATUS<22>
            FILE.TYPE = FILE.STATUS<21>
            IF FILE.TYPE = "J4" THEN

                OVER.FLOW = FILE.SIZE - (FILE.MODULO*FILE.SEP*BLOCK.SIZE)
                OVER.FLOW = OVER.FLOW / (BLOCK.SIZE*FILE.SEP)
                OVER.FLOW = INT(OVER.FLOW/FILE.MODULO)
                IF OVER.FLOW GE "3" THEN
                    EXECUTE "jrf -R ":FILE.NAME CAPTURING Y.OUTPUT
                    Y.OUTPUT = TRIM(Y.OUTPUT)
                    IF INDEX(Y.OUTPUT,"Resize",1) THEN
                        GET.NEW.MODULO = TRIM(TRIM(Y.OUTPUT)[" to ",2,1]['.',1,1])
                        EXECUTE "jrf -S":GET.NEW.MODULO:" ":FILE.NAME
                        READ SESSION.REC FROM HOME.PATH.HANDLER,SESSION.ID THEN
                            SESSION.REC<-1> = FMT(TIMEDATE(),'L#25'):FMT('Resized the file :':FILE.NAME:,"L#50"):FMT('Command used [jrf -S':GET.NEW.MODULO:' ':FILE.NAME:']','L#50')
                        END ELSE
                            SESSION.REC = FMT(TIMEDATE(),'L#25'):FMT('Resized the file :':FILE.NAME:,"L#50"):FMT('Command used [jrf -S':GET.NEW.MODULO:' ':FILE.NAME:']','L#50')
                        END
                        WRITE SESSION.REC TO HOME.PATH.HANDLER,SESSION.ID ON ERROR
                            CALL OCOMO("Resize the file :":FILE.NAME)
                        END
                    END
                END
            END
        END
    END
    CLOSE FILE.HANDLER
    RETURN
*---------------------------------------------------
END
*---------------------------------------------------
