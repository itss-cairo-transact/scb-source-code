* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>383</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.NEGATIVE.LIST.TEXT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CBE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BRSIN.FLAG
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMP.LOCAL.REFS


    OPENSEQ "&SAVEDLISTS&" , "scb.msw" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"scb.msw"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "scb.msw" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE scb.msw CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create scb.msw File IN &SAVEDLISTS&'
        END
    END

**    YTEXT = "Enter the Date : "
**    CALL TXTINP(YTEXT, 8, 22, "12", "A")

    WS.DAT = TODAY[1,6]:'01'
    CALL CDT("",WS.DAT,'-1C')

    DATEH = WS.DAT

    CBE.DATA.H = '1700'
    CBE.DATA.H := DATEH[1,4]
    CBE.DATA.H := DATEH[5,2]
    CBE.DATA.H := DATEH[7,2]
***  CBE.DATA.H := STR("0",493)
    CBE.DATA.H := STR("0",517)

    WRITESEQ CBE.DATA.H TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.CBE = 'F.SCB.VISA.CBE' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.COMP = 'F.COMPANY' ; F.COMP = ''
    CALL OPF(FN.COMP,F.COMP)

    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

******************************************************************
    T.SEL = "SELECT F.SCB.VISA.CBE WITH REP.DATE EQ " : WS.DAT
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)

            CURR       = R.CBE<CBE.CARD.CURRENCY>
            BR.CODE    = R.CBE<CBE.BRANCH>

            CALL F.READ(FN.COMP,BR.CODE,R.COMP,F.COMP,ERR.D)
            LOCAL.REF.CMP = R.COMP<EB.COM.LOCAL.REF>
            BR.CO.CBE     = LOCAL.REF.CMP<1,CMP.CBE.BR.CODE>

            CUST.NO    = R.CBE<CBE.CUSTOMER>
            LATE.CO    = R.CBE<CBE.LATE.CODE>
            BIRTH.D    = R.CBE<CBE.CUS.BIRTH.DATE>
            ID.TYPE    = R.CBE<CBE.CARD.TYPE>
            ID.NO      = R.CBE<CBE.ID.NO>
            ID.ISS.D   = R.CBE<CBE.ID.ISS.DATE>
            ID.EXP.D   = R.CBE<CBE.ID.EXP.DATE>
            HOME.TEL   = R.CBE<CBE.HOME.TEL>
            WORK.TEL   = R.CBE<CBE.WORK.TEL>
            MOB.TEL    = R.CBE<CBE.MOB.TEL>
            CARD.ISS.D = R.CBE<CBE.CARD.ISS.DATE>
            LIMIT      = R.CBE<CBE.LIMIT>
            STOP.DATE  = R.CBE<CBE.STOP.DATE>
            ACCT.BAL1  = R.CBE<CBE.USED.LIMIT>
            ACCT.BAL   = ACCT.BAL1 * -1
            CUST.SEX   = R.CBE<CBE.CU.SEX>
            CUST.NAME  = R.CBE<CBE.CU.NAME>
            BIRTH.PL   = R.CBE<CBE.BIRTH.PLACE>
            ADDRESS    = R.CBE<CBE.CU.ADDRESS>
            CUST.JOB   = R.CBE<CBE.JOB.DESC>
            ID.ISS.PL  = R.CBE<CBE.ID.ISS.PLACE>
            CUST.EMAIL = R.CBE<CBE.EMAIL>
            CUST.TYPE1 = R.CBE<CBE.CUSTOMER.TYPE>
            CUST.LEGAL.M = R.CBE<CBE.CUS.LEGAL.ACTION.M>
            CUST.LEGAL.S = R.CBE<CBE.CUS.LEGAL.ACTION.S>

            IF CUST.TYPE1 = '����' THEN
                CUST.TYPE = '1'
            END

            IF CUST.TYPE1 = '����' THEN
                CUST.TYPE = '2'
            END

            CUST.STAT  = R.CBE<CBE.CUSTOMER.STATE>
            LOAN.TYPE  = R.CBE<CBE.LOAN.KIND>
            CARD.NO    = R.CBE<CBE.CARD.NO>

            CBE.DATA  = BR.CO.CBE
            CBE.DATA := STR("0",12 - LEN(CUST.NO)):CUST.NO
            CBE.DATA := FMT(CUST.TYPE,"R%2")
*** NOT EXIST IN NEW FILE
***            CBE.DATA := FMT(CUST.STAT,"R%2")
***
            CBE.DATA := FMT(CUST.LEGAL.M,"R%2")
            CBE.DATA := FMT(CUST.LEGAL.S,"R%2")

            CBE.DATA := FMT(LATE.CO,"R%2")
            CBE.DATA := FMT(LOAN.TYPE,"R%2")
            CBE.DATA := BIRTH.D
            CBE.DATA := '0':ID.TYPE
*** NEW LENGTH IS 14 REPLACE TO 15
            CBE.DATA := FMT(ID.NO,"l#14")
*****
            CBE.DATA := ID.ISS.D
            CBE.DATA := ID.EXP.D
            CBE.DATA := FMT(HOME.TEL,"R%15")
            CBE.DATA := FMT(WORK.TEL,"R%15")
            CBE.DATA := FMT(MOB.TEL,"R%15")
            CBE.DATA := CARD.ISS.D
            CBE.DATA := FMT(CURR,"R%4")
            LIM.FMT   = FIELD(LIMIT,".",1)
            CBE.DATA := FMT(LIM.FMT,"R%6")
            CBE.DATA := STOP.DATE
            ACCT.FMT  = FIELD(ACCT.BAL,".",1)
            CBE.DATA := FMT(ACCT.FMT,"R%6")
            CBE.DATA := FMT(CARD.NO,"R%12")
            CBE.DATA := CUST.SEX
            CBE.DATA := FMT(CUST.NAME,"l#100")
            CBE.DATA := FMT(BIRTH.PL,"l#40")
            CBE.DATA := FMT(ADDRESS,"l#60")
            CBE.DATA := FMT(CUST.JOB,"l#60")
            CBE.DATA := FMT(ID.ISS.PL,"l#40")
            CBE.DATA := FMT(CUST.EMAIL,"l#50")
*** NEW FIELDS ***
            WS.TEST   = '0'
            CBE.DATA := '2'
            CBE.DATA := FMT(WS.TEST,"R%2")
            CBE.DATA := FMT(WS.TEST,"R%6")
            CBE.DATA := FMT(WS.TEST,"R%6")
            CBE.DATA := FMT(WS.TEST,"R%8")
******

            WRITESEQ CBE.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END
*************************************************************
    TEXT = '�� ����� ����� ������ : ':WS.DAT ; CALL REM
    RETURN

END
