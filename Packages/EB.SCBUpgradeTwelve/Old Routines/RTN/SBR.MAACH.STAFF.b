* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.MAACH.STAFF

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*----------------------------------------------
    GOSUB INITIALISE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*-------------------------------------------
INITIALISE:

    REPORT.ID='SBR.MAACH.STAFF'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.COM = 'F.COMPANY'  ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.CU = 'FBNK.CUSTOMER'  ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT'  ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.POST = 'F.POSTING.RESTRICT'  ; F.POST = ''
    CALL OPF(FN.POST,F.POST)

    TOT.AMT  = 0
    TD       = TODAY
    ERR.FLAG = ''

    OPENSEQ "MECH" , "MAACH.ERROR" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"MAACH.ERROR"
        HUSH OFF
    END
    OPENSEQ "MECH" , "MAACH.ERROR" TO BB ELSE
        CREATE BB THEN
        END ELSE
            STOP 'Cannot create MAACH.ERROR File IN MECH'
        END
    END

    RETURN
*-------------------------------------------
PROCESS:

    SEQ.FILE.NAME = "MECH"
    RECORD.NAME = "maach_cr.all"
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)

        DB.ACCT   = ''
        CR.ACCT   = ''
        CUST.NAME = ''
        VAL.DATE  = ''
        ERR.DESC  = ''
        BRN.NAME  = ''
        DB.AMT.1  = 0
        DB.AMT    = 0

        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            DB.ACCT.1  = FIELD(Y.MSG,"=",3)
            DB.AMT.1   = FIELD(Y.MSG,"=",4)
            VAL.DATE.1 = FIELD(Y.MSG,"=",7)
            CR.ACCT.1  = FIELD(Y.MSG,"=",8)

            DB.ACCT  = FIELD(DB.ACCT.1,",",1)
            DB.AMT   = FIELD(DB.AMT.1,",",1)
            VAL.DATE = FIELD(VAL.DATE.1,",",1)
            CR.ACCT  = FIELD(CR.ACCT.1,",",1)
            TOT.AMT += DB.AMT

            IF VAL.DATE LE TD THEN
                ERR.DESC = '����� ���� ��� �� �����'
            END

            CALL F.READ(FN.AC,CR.ACCT,R.AC,F.AC,E1)
            IF E1 THEN

                ERR.DESC = '������ ��� �����'
                AC.BRN = "EG00100":CR.ACCT[1,2]

            END ELSE
                AC.CUST   = R.AC<AC.CUSTOMER>
                AC.BRN    = R.AC<AC.CO.CODE>
                CUST.NAME = R.AC<AC.ACCOUNT.TITLE.1>

                CALL F.READ(FN.CU,AC.CUST,R.CU,F.CU,E2)
                CUST.POST = R.CU<EB.CUS.POSTING.RESTRICT>
                IF CUST.POST NE '' AND CUST.POST NE '24' THEN
                    CALL F.READ(FN.POST,CUST.POST,R.POST,F.POST,E3)
                    ERR.DESC = R.POST<AC.POS.DESCRIPTION,2>
                END

            END
            CALL F.READ(FN.COM,AC.BRN,R.COM,F.COM,E4)
            BRN.NAME = R.COM<EB.COM.COMPANY.NAME,2>

            IF ERR.DESC NE '' THEN
                ERR.FLAG = 'YES'

                XX = SPACE(132)
                XX<1,1>[1,20]   = CR.ACCT
                XX<1,1>[20,35]  = CUST.NAME
                XX<1,1>[60,35]  = ERR.DESC
                XX<1,1>[105,25] = BRN.NAME

                PRINT XX<1,1>
                PRINT STR('-',130)
            END

            CALL F.READ(FN.AC,DB.ACCT,R.AC,F.AC,E5)
            DB.ACCT.AMT  = R.AC<AC.WORKING.BALANCE>
            DB.CUST.NAME = R.AC<AC.ACCOUNT.TITLE.1>
            DB.AC.BRN    = R.AC<AC.CO.CODE>

            CALL F.READ(FN.COM,DB.AC.BRN,R.COM,F.COM,E4)
            DB.BRN.NAME   = R.COM<EB.COM.COMPANY.NAME,2>
            DB.ACCT.PRINT = DB.ACCT

        END ELSE
            EOF = 1
        END
    REPEAT

    IF DB.ACCT.AMT LT TOT.AMT THEN
        ERR.FLAG = 'YES'
        ERR.DESC = '���� ������ ������'
        XX = SPACE(132)
        XX<1,1>[1,20]   = DB.ACCT.PRINT
        XX<1,1>[20,35]  = DB.CUST.NAME
        XX<1,1>[60,35]  = ERR.DESC
        XX<1,1>[105,25] = DB.BRN.NAME
        PRINT XX<1,1>
        PRINT STR('-',130)
    END

    IF ERR.FLAG EQ 'YES' THEN
        BB.DATA  = 'ERROR'
        WRITESEQ BB.DATA TO BB ELSE
        END
    END ELSE
        BB.DATA  = TD
        WRITESEQ BB.DATA TO BB ELSE
        END

        XX = SPACE(132)
        XX<1,1>[60,35]  = '�� ���� �����'
        PRINT XX<1,1>
    END
    CLOSESEQ SEQ.FILE.POINTER

    RETURN
*-------------------------------------------
PRINT.HEAD:

    YYBRN  = "���� �����"
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"��� ����� �������� ���� : ":DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������" :SPACE(10):"�����":SPACE(35):"��� �����":SPACE(35):"�����"
    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD

    RETURN
*-------------------------------------------

END
