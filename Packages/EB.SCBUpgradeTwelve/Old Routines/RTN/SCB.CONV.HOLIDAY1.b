* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SCB.CONV.HOLIDAY1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
    ST=''
    VVV = O.DATA
    XX1 = LEN(VVV)
    PRE = VVV[1,2]
    POST = VVV[3,XX1]
    XX2 = XX1-2
    FOR X = 1 TO XX2
        V = POST[X,1]
        IF V EQ 'H' THEN
            XLEN = LEN(X)
            IF XLEN EQ 1 THEN
                ST = ST:' ':X:' '
            END ELSE
                ST = ST:X:' '
            END
        END
    NEXT X
    IF PRE EQ 01 THEN
        O.DATA ='*   JAN   *  ':ST
    END
    IF PRE EQ 02 THEN
        O.DATA ='*   FEB   *  ':ST
    END
    IF PRE EQ 03 THEN
        O.DATA ='*   MAR   *  ':ST
    END
    IF PRE EQ 04 THEN
        O.DATA ='*   APR   *  ':ST
    END
    IF PRE EQ 05 THEN
        O.DATA ='*   MAY   *  ':ST
    END
    IF PRE EQ 06 THEN
        O.DATA ='*   JUN   *  ':ST
    END
    IF PRE EQ 07 THEN
        O.DATA ='*   JUL   *  ':ST
    END
    IF PRE EQ 08 THEN
        O.DATA ='*   AUG   *  ':ST
    END
    IF PRE EQ 09 THEN
        O.DATA ='*   SEP   *  ':ST
    END
    IF PRE EQ 10 THEN
        O.DATA ='*   OCT   *  ':ST
    END
    IF PRE EQ 11 THEN
        O.DATA ='*   NOV   *  ':ST
    END
    IF PRE EQ 12 THEN
        O.DATA ='*   DEC   *  ':ST
    END

    RETURN
END
