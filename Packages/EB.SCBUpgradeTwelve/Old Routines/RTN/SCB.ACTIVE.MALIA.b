* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-- CREATE BY AHMED NAHRAWY (FOR EVER)
*-- EDIT BY NESSMA MOHAMMED

    SUBROUTINE SCB.ACTIVE.MALIA

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*----------------------------------------------

    GOSUB INITIATE
*Line [ 50 ] Adding EB.SCBUpgradeTwelve. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PRINT.HEAD

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    FRACTION = ''
    OUT.WORD = ''
    REPORT.ID='SCB.ACTIVE.MALIA'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*----------------------------------------------------
CALLDB:
    F.MULTI = '' ; FN.MULTI = 'F.INF.MULTI.TXN' ; R.MULTI = '' ; E1 = '' ; RETRY1 = ''
    OUT.AMOUNT = ''
    CALL OPF(FN.MULTI,F.MULTI)

    DATT = TODAY

    I = 0
    INPUTTER = R.NEW(INF.MLT.INPUTTER)
    AUTH     = R.USER<EB.USE.SIGN.ON.NAME>
    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)

    AC1 = R.NEW(INF.MLT.ACCOUNT.NUMBER)
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD  = DCOUNT(AC1,@VM)
    FOR J = 1 TO DD
        ACCC      = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
        ACCCC     = R.NEW(INF.MLT.ACCOUNT.NUMBER)
        CUR       = R.NEW(INF.MLT.CURRENCY)<1,J>
        AMT1      = R.NEW(INF.MLT.AMOUNT.LCY)<1,J>
        AMT2      = R.NEW(INF.MLT.AMOUNT.FCY)<1,J>
        SIGN      = R.NEW(INF.MLT.SIGN)<1,J>
        IF SIGN EQ 'DEBIT' THEN
            SIGN  = '���'
        END ELSE
            IF SIGN EQ 'CREDIT' THEN
                SIGN = '�����'
            END
        END
        IF ACCC EQ '' THEN
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
            END
        END

        VALUE.DAT = R.NEW(INF.MLT.VALUE.DATE)<1,J>
        INP       = FIELD(INPUTTER,'_',2)
        NOTES     = R.NEW(INF.MLT.THEIR.REFERENCE)<1,J>
        PLCATEG   = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        ID=ID.NEW

        I ++
        XX = SPACE(90)
        XX<1,I>[1,20]   = ACCC
        XX<1,I>[25,10]  = CUR
        XX<1,I>[40,10]  = AMT1
        XX<1,I>[45,10]  = AMT2
        XX<1,I>[55,6]   = SIGN
        XX<1,I>[65,09]  = VALUE.DAT
        XX<1,I>[80.09]  = NOTES
        XX<1,I>[80,10]  = PLCATEG

PRINT.HEAD:
        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
        YYBRN = FIELD(BRANCH,'.',2)

*-- EDIT BY NESSMA 2016/08/22
* CUSNN = ACCC[2,7]
* CURR  = ACCC[9,2]
        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCC,CUSNN)
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCC,CURR)
*-- END EDIT

        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
        IF ACCC EQ '' THEN
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>

                CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACCC,NAME)
            END
        END
        IF ACCC[1,3] NE 'EGP' OR ACCC[1,3] NE 'USD' OR ACCC[1,3] NE 'GBP' OR ACCC[1,2] NE 'PL' THEN
            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCC,CUSNN)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNN,LOCAL.REF)
            NAME.A  = LOCAL.REF<1,CULR.ARABIC.NAME>
            NAME2.A = LOCAL.REF<1,CULR.ARABIC.NAME.2>

            ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS><1,1>
            CUST.ADDRESS1= LOCAL.REF<1,CULR.GOVERNORATE>
            CUST.ADDRESS2= LOCAL.REF<1,CULR.REGION>

            CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION<1,1>,CUST.ADDRESS1,CUST.ADD2)
            CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION<1,1>,CUST.ADDRESS2,CUST.ADD1)

            IF CUST.ADDRESS1 = 98 THEN
                CUST.ADD1 = ''
            END
            IF CUST.ADDRESS2 = 998 THEN
                CUST.ADD2 = ''
            END
            IF CUST.ADDRESS1 = 999 THEN
                CUST.ADD1 = ''
            END
            IF CUST.ADDRESS2 = 999 THEN
                CUST.ADD2 = ''
            END

        END
        IF ACCC[1,3] EQ 'EGP' OR ACCC[1,3] EQ 'USD' OR ACCC[1,3] EQ 'GBP' THEN
            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACCC,ACTITLE)
            NAME.A  = ACTITLE
            NAME2.A = ACTITLE
            ADDRESS = ACTITLE
        END

        DATY  = TODAY
        T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
        T.DAY1 = VALUE.DAT[7,2]:'/':VALUE.DAT[5,2]:"/":VALUE.DAT[1,4]

        PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(25):"��� :" :YYBRN
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(14):" ����� : ":SIGN
        PR.HD := "'L'":SPACE(1):"-----------------------------------------------------"
        PR.HD :="'L'":" "
        BEGIN CASE
        CASE ACCC NE ''
            PR.HD := "'L'":SPACE(1):"��� ������ :":ACCC :SPACE(20) : NAME.A : SPACE(2) : NAME2.A
            PR.HD :="'L'":" "
        CASE 1
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,ACCC,MYCAT)
            PR.HD := "'L'":SPACE(1):"��� ������ :":PLCATEG :SPACE(20) : MYCAT
        END CASE
        BEGIN CASE
        CASE CUR NE 'EGP'
            PR.HD := "'L'":SPACE(1):"������ :":AMT2:SPACE(8):CURR:SPACE(20):ADDRESS:SPACE(2):CUST.ADD2 :' ': CUST.ADD1
        CASE CUR EQ 'EGP'
            PR.HD := "'L'":SPACE(1):"������ :":AMT1:SPACE(8):CURR:SPACE(20):ADDRESS:SPACE(2):CUST.ADD2 :' ': CUST.ADD1
        END CASE
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"����� ���� :" :T.DAY1
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"SIGN :":SIGN
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"������� :" : NOTES
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "

        BEGIN CASE
        CASE CUR NE 'EGP'

            IN.AMOUNT=AMT2
            CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            VV = OUT.AMOUNT:" ":CURR:" ":"�� ���"
            PR.HD := VV
        END CASE

        IF CUR EQ 'EGP' THEN
            IN.AMOUNT=AMT1
            CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            VV1 = OUT.AMOUNT:" ":CURR:" ":"�� ���"
            PR.HD := VV1
        END

        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"��� �������":SPACE(5):"��� �������"::SPACE(5):"��� �������"
        PR.HD := "'L'":SPACE(1):" ---------          --------         --------"
        PR.HD := "'L'":SPACE(5):INP:SPACE(5):AUTHI:SPACE(5):ID

        HEADING PR.HD
    NEXT J
    RETURN
*==============================================================
    RETURN
END
