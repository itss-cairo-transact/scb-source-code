* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*==================================DOCUMENTATION=======================================
* WAGDY *
*********
* THIS PROGRAM IS USED TO SELECT DATA FROM ACCOUNT TABLE WITH SPECEFIC CRITERIA
* THEN LINKING IT TO THE ACCT.ACTIVITY TABLE
* AND CONVERTING IT TO TXT FILE SAVED IN &SAVEDLISTS& DIRECTORY
* NOTE : FILE IS COMA "|" SEPARATED
* FILE SCHEMA IS:
*-----------------
* ACCT.ACTIVITY.ID|CURRENCY|DAY|DEBIT|CREDIT|BALANCE
*======================================================================================

  SUBROUTINE SCB.ACT.LIST.1.V2
*    PROGRAM SCB.ACT.LIST.1.V2
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)

    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)


    MO.DAYS = ''
    YTEXT = "Enter Date In Format 'YYYYMM' : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    DT = COMI
    BB.DATA= ''
    TEXT = ''
    ETEXT = ''
    SELECTED = ''
*====== Checking the month ======*

    IF DT[5,2] EQ '01' OR DT[5,2] EQ '03' OR DT[5,2] EQ '05' OR DT[5,2] EQ '07' OR DT[5,2] EQ '08' OR DT[5,2] EQ '10' OR DT[5,2] EQ '12' THEN
        MO.DAYS = 31
    END

    IF DT[5,2] EQ '04' OR DT[5,2] EQ '06' OR DT[5,2] EQ '09' OR DT[5,2] EQ '11' THEN
        MO.DAYS = 30
    END
    IF DT[5,2] EQ 02 THEN
        MO.DAYS = 28
    END

*==== Opening File to write =====*
    OPENSEQ "/home/signat" , "SCB.ACT.LIST.":DT TO BB THEN
* OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "BRANCH.TRANS.401" TO T.DATA THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"SCB.ACT.LIST.":DT
        HUSH OFF
    END
    OPENSEQ "/home/signat" , "SCB.ACT.LIST.":DT TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.ACT.LIST CREATED IN home/segnat'
        END
        ELSE
            STOP 'Cannot create SCB.ACT.LIST File IN home/segnat'
        END
    END

    EOF = ''
*====== Selecting Accounts ========*

    FN.AC   = 'FBNK.ACCOUNT'        ; F.AC  = '' ; R.AC  = ''
    FN.ACT  = 'FBNK.ACCT.ACTIVITY'  ; F.ACT = '' ; R.ACT = ''
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.ACT,F.ACT)

    T.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 11500 AND CO.CODE EQ EG0010099 BY ALT.ACCT.ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*====== Getting Account Info ========*
    PRINT "Recorde No. Is : " : SELECTED
    IF SELECTED THEN
        FOR PP = 1 TO SELECTED
* PRINT PP
            DAY.NO = '' ; DTV= '' ; CTV = '' ; BAL = '' ; BAL2 = ''
            CALL F.READ(FN.AC,KEY.LIST<PP>,R.AC,F.AC,ETEXT)
            ACC             = KEY.LIST<PP>
            CUST            = R.AC<AC.CUSTOMER>
            CCY             = R.AC<AC.CURRENCY>
            ACC.TITLE       = R.AC<AC.ACCOUNT.TITLE.1>
            OAB             = R.AC<AC.OPEN.ACTUAL.BAL>

*Line [ 117 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CCY IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>


*========= linking account with date to ACC.ACTIVITY ======*

            ACC.ACT = ACC : "-" : DT
            CALL F.READ(FN.ACT,ACC.ACT,R.ACT,F.ACT,ETEXT)
            IF R.ACT NE '' THEN

                L = 0
*Line [ 129 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                L= DCOUNT(R.ACT<IC.ACT.DAY.NO>,@VM)

                FOR I = 1 TO MO.DAYS
                    II = ''
                    IF I LE 9 THEN
                        II = "0": I
                    END ELSE
                        II = I
                    END
                    BB.DATA = ACC.ACT : "|" : CCY : "|" : DT:II : "|||" : BAL2 * RATE :"|": ACC.TITLE
                    BAL = ''
                    BALX = ''

                    FOR K = 1 TO L
                        DAY.NO          = R.ACT<IC.ACT.DAY.NO><1,K>
                        DTV             = R.ACT<IC.ACT.TURNOVER.DEBIT><1,K>
                        CTV             = R.ACT<IC.ACT.TURNOVER.CREDIT><1,K>
                        BAL             = R.ACT<IC.ACT.BALANCE><1,K>

                        IF (I EQ 1 AND K EQ 1 AND DAY.NO NE '01') THEN
                            BAL2 = BAL - CTV - DTV
                            BB.DATA = ACC.ACT : "|" : CCY : "|" : DT:II : "|||":  BAL2 * RATE :"|": ACC.TITLE
                        END

                        IF DAY.NO EQ II THEN
                            BB.DATA =  ACC.ACT : "|" : CCY : "|" : DT:DAY.NO : "|" : DTV * RATE : "|" :CTV * RATE: "|":  BAL * RATE  :"|": ACC.TITLE
                            BAL2 = BAL
                        END
                    NEXT K
*     PRINT BB.DATA
                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END


                NEXT I
            END
            IF R.ACT EQ '' THEN
                PRINT ACC.ACT
                FOR H = 1 TO MO.DAYS
                    HH = ''
                    IF H LE 9 THEN
                        HH = "0": H
                    END ELSE
                        HH = H
                    END

                    BB.DATA = ACC.ACT : "|" : CCY : "|" : DT:HH : "|||" : OAB * RATE :"|": ACC.TITLE
                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END

                NEXT H
            END
        NEXT PP
    END

    TEXT  =  "FILE WAS CREATED...................." ; CALL REM
END
