* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>200</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.PRINT.DSF.DRMNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DRMNT.FILES
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBR.PRINT.DSF.DRMNT'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CA = 'F.SCB.DRMNT.FILES' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    FN.CU = "FBNK.CUSTOMER" ; F.CU = ""
    CALL OPF(FN.CU,F.CU)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    TOTAL.BOOKS = 0

    COMP = ID.COMPANY
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,COM.NAME)
    YTEXT = "Enter Customer No. : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    CUST.ID = COMI

*    WS.DESC.1 = ' ������ ������� '
*    WS.DESC.2 = '��� ���� ���� �� ��� ���� ������ ��� : ':COM.NAME
*    WS.DESC.3 = '�������� ������� : '
    CALL F.READ(FN.CU,CUST.ID,R.CU,F.CU,ERR.CU)
    WS.ADDRESS = ''
    GOSUB PROCESS

    RETURN
*========================================================================
PROCESS:
    T.SEL = "SELECT ":FN.CA:" WITH CO.CODE EQ ":COMP:" AND CUST.NO EQ ":CUST.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    IF SELECTED GT 0 THEN
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

*            IF I = 1 THEN GOSUB PRINT.HEAD
*            IF I = 16 THEN
*  WS.DESC.1 = ''
*  WS.DESC.2 = ''
*  WS.DESC.3 = ''
            GOSUB PRINT.HEAD
*            END
            CALL F.READ(FN.CA,KEY.LIST<I>,R.CA,F.CA,E1)
            CUS.ID         = R.CA<SDF.CUST.NO>
            CUST.NAME      = R.CA<SDF.CUST.NAME>
            WS.SIGN.COUNT  = R.CA<SDF.SIGN.COUNT.SYS>
            WS.DRMNT.DATE = R.CA<SDF.DRMNT.DATE>
            WS.DRMNT.DATE = FMT(WS.DRMNT.DATE,"####/##/##")
            WS.NOTES       = R.CA<SDF.NOTES>
            ADDRESS1 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,1>
            ADDRESS2 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,2>
            ADDRESS3 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS,3>
            WS.ADDRESS = ADDRESS1:'':ADDRESS2  :'':ADDRESS3

            XX   = SPACE(132)
            XX1  = SPACE(132)
            XX2  = SPACE(132)
            XX3  = SPACE(132)
            XX4  = SPACE(132)
            XX5  = SPACE(132)

            XX<1,1>[1,132]    = CUS.ID
            XX1<1,1>[1,132]   = CUST.NAME
*            XX2<1,1>[1,132]   = WS.DRMNT.DATE
            XX2<1,1>[1,132]   = ''
            XX3<1,1>[1,132]   = COM.NAME
            XX5<1,1>[1,132]   = ''
            XX4<1,1>[1,132]   = WS.ADDRESS

            PRINT XX<1,1>
            PRINT XX1<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT XX5<1,1>
            PRINT XX4<1,1>

        NEXT I


        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        CALL PRINTER.ON(REPORT.ID,'')

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")
    PR.HD  ="'L'"
    PR.HD :="'L'"
    PR.HD :="'L'":'SBR.PRINT.DSF.DRMNT'
    PR.HD :="'L'":T.DAY
    PR.HD :="'L'"
    PR.HD :="'L'"

    HEADING PR.HD

    RETURN
*==============================================================
END
