* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.STAFF.CREDIT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*---------------------------------------
    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)


    YTEXT = "����� ����� ���� ����� : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    DB.ACCT = COMI

    YTEXT = "����� ����� ��� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DB.REF.TXT = COMI

    YTEXT = "����� ����� ��� ������"
    CALL TXTINP(YTEXT, 8, 22, "2", "A")
    DB.TRN.CODE = COMI

    WS.TRN.CODE = 'AC':DB.TRN.CODE

    YTEXT = "����� ����� ����� ����"
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    V.DATE = COMI

************ CHECK ACCOUNT **************

    CALL F.READ(FN.AC,DB.ACCT,R.AC,F.AC,E9)
    IF NOT(E9) THEN
        WS.ACCT.NAME = R.AC<AC.ACCOUNT.TITLE.1>
        TEXT = '��� ������ ���� : ' : WS.ACCT.NAME ; CALL REM

        YTEXT = "Y/N �� ���� ���������"
        CALL TXTINP(YTEXT, 8, 22, "1", "A")
        FLG1 = COMI

        IF FLG1 = 'Y' OR FLG1 = 'y' THEN
            GOSUB INITIALISE
            GOSUB BUILD.RECORD
        END ELSE
            TEXT = '�� ������ �� ��������' ; CALL REM
        END

    END ELSE
        TEXT = '��� ���� ������' ; CALL REM
        TEXT = '�� ������ �� ��������' ; CALL REM
    END
*****************************************
    RETURN
*---------------------------------------
INITIALISE:
    OPENSEQ "MECH" , "STAFF.FT.CREDIT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"STAFF.FT.CREDIT"
        HUSH OFF
    END
    OPENSEQ "MECH" , "STAFF.FT.CREDIT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE STAFF.FT.CREDIT CREATED IN MECH'
        END ELSE
            STOP 'Cannot create STAFF.FT.CREDIT File IN MECH'
        END
    END

    COMMA   = ','
    TOT.AMT = 0

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME = 'emp.csv'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            ACCT.NO1 = FIELD(Y.MSG,",",1)
            AMT      = FIELD(Y.MSG,",",2)
            TOT.AMT += AMT

            ACCT.NO   = FMT(ACCT.NO1,'R%16')

            DR.ACCT = 'EGP1200500010099'
            CR.ACCT = ACCT.NO

            IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//EG0010099,'

            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":WS.TRN.CODE:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":'EGP':COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":'EGP':COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":AMT:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":DB.REF.TXT:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":DB.REF.TXT:COMMA
            OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB"

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

********** TOTAL *********

    CR.ACCT = 'EGP1200500010099'

    IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//EG0010099,'

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":WS.TRN.CODE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":'EGP':COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":'EGP':COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":TOT.AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
    OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
    OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":DB.REF.TXT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":DB.REF.TXT:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB"


    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

    WRITESEQ MSG.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN STAFF.FT.CREDIT'
    EXECUTE 'DELETE ':"MECH":' ':"STAFF.FT.CREDIT"

    TEXT = '�� �������� �� ��������' ; CALL REM

    EXECUTE "sh > MECH/emp.csv"

**************************

    RETURN
END
