* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-------------------------------------NI7OOOOOOOOOOOO----------------------------------------
*-----------------------------------------------------------------------------
* <Rating>84</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SCB.CUR.WRITE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM

    ETEXT        ='' ; E            ='' ; CHQ.NOS ='' ; CHQ.RETURN   ='' ; CHQ.STOP   ='' ; LF ='' ; RH  ='' ;
    COUNTS1      ='' ; COUNTS11     ='' ; CHQ.STAT='' ; CHQ.PAY.DATE ='' ; CHQ.PAY.BRN='' ; ER ='' ; ERS ='' ;
    CHQ.TRNS.PAY ='' ; CHQ.REC.DATE ='' ; CHQ.BEN ='' ; CHQ.REC.DATE ='' ;
    CHQ.AMT      =''
    FLG.MONTH = ""

    FN.CHQ.PRESENT = 'F.SBD.CURRENCY' ; F.CHQ.PRESENT = '' ; R.CHQ.PRESENT = ''
    CALL OPF(FN.CHQ.PRESENT,F.CHQ.PRESENT)

    FN.ACC = 'FBNK.CURRENCY' ;F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
*---------------------------- ADD BY BAKRY TO READ RATES FROM NZD AT EOM ---------------------------------
*----------------------------              2013/04/28                    ---------------------------------
    COMP = "EG0010001"
    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,COMP,LAST.P.END1)
    LAST.CD1 = TODAY
    LAST.CD11 = LAST.CD1[1,4]:LAST.CD1[5,2]:"01"
    CALL CDT("",LAST.CD11,'-1C')
    IF LAST.CD11 = LAST.P.END1 THEN
        FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
        F.BASE = ''
        R.BASE = ''
        CALL OPF(FN.BASE,F.BASE)
        CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
        CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
        FLG.MONTH = "M"
    END

*----------------------------              2013/04/28                    ---------------------------------
*---------------------------- ADD BY BAKRY TO READ RATES FROM NZD AT EOM ---------------------------------

    KEY.LIST = "" ; SELECTED="" ; ER.MSG=""
    T.SEL = "SELECT FBNK.CURRENCY "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR I = 1 TO SELECTED
        SCB.CHQ.ID = KEY.LIST<I>
*---------------------------- ADD BY BAKRY TO READ RATES FROM NZD AT EOM ---------------------------------
        IF FLG.MONTH = "M" THEN
            LOCATE SCB.CHQ.ID IN CURR.BASE<1,1> SETTING POS THEN

                IF SCB.CHQ.ID = 'JPY' THEN
                    MID = R.BASE<RE.BCP.RATE,POS> * 100
                END ELSE
                    MID = R.BASE<RE.BCP.RATE,POS>
                END
                R.CHQ.PRESENT<SBD.CURR.MID.RATE> = MID
                R.CHQ.PRESENT<SBD.CURR.CUR.DATE> = TODAY
            END ELSE
                CALL F.READ(FN.ACC,SCB.CHQ.ID,R.ACC,F.ACC,E1)

                MID = R.ACC<EB.CUR.MID.REVAL.RATE,1>
                R.CHQ.PRESENT<SBD.CURR.MID.RATE> = MID
                R.CHQ.PRESENT<SBD.CURR.CUR.DATE> = TODAY
            END
        END ELSE
*---------------------------- ADD BY BAKRY TO READ RATES FROM NZD AT EOM ---------------------------------
            CALL F.READ(FN.ACC,SCB.CHQ.ID,R.ACC,F.ACC,E1)

            MID = R.ACC<EB.CUR.MID.REVAL.RATE,1>
            R.CHQ.PRESENT<SBD.CURR.MID.RATE> = MID
            R.CHQ.PRESENT<SBD.CURR.CUR.DATE> = TODAY
        END

**WRITE  R.CHQ.PRESENT TO F.CHQ.PRESENT , SCB.CHQ.ID ON ERROR
**    PRINT "CAN NOT WRITE RECORD":SCB.CHQ.ID:"TO" :FN.CHQ.PRESENT
**END
        CALL F.WRITE(FN.CHQ.PRESENT,SCB.CHQ.ID,R.CHQ.PRESENT)
        CALL JOURNAL.UPDATE(SCB.CHQ.ID)
    NEXT I
    RETURN
*--------------------------------------------------------------------*
END
