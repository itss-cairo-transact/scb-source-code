* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*SUBROUTINE SBY.ECL.ALL.TXT
    PROGRAM SBY.ECL.ALL.TXT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ECL
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*-----------------------------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "ECL.ALL.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ECL.ALL.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ECL.ALL.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ECL.ALL.TXT  CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ECL.ALL.TXT File IN &SAVEDLISTS&'
        END
    END
*----------------------------------------------------------------

    FN.ECL  = 'F.SCB.ECL'        ; F.ECL = ''     ; R.ECL = ''
    CALL OPF( FN.ECL,F.ECL)

    FN.CAT = 'F.CATEGORY'     ; F.CAT = ''     ; R.CAT = ''
    CALL OPF( FN.CAT,F.CAT)
*********SPECIAL RISK RATING ************************************
    DIR.NAME = "&SAVEDLISTS&"
    TEXT.FILE = "ECL.SPECIAL.RISK.txt"
    VAR.FILE = ''
    EOF = ''
    OPENSEQ DIR.NAME,TEXT.FILE TO VAR.FILE ELSE

        TEXT = "ERROR OPEN FILE ":TEXT.FILE ; CALL REM
        RETURN
    END

    LOOP WHILE NOT(EOF)

        READSEQ Line FROM VAR.FILE THEN
            CUS.NO     = TRIM(FIELD(Line,"|",1),'',"D")
            RISK.RATE  = TRIM(FIELD(Line,"|",2),'',"D")

            Q.SEL = "SELECT F.SCB.ECL WITH CUS.NO EQ ":CUS.NO
            CALL EB.READLIST(Q.SEL,KEY.LIST1,"",SELECTED1,ER.MSG1)

            IF SELECTED1 THEN
                FOR J = 1 TO SELECTED1
                    CALL F.READ(FN.ECL,KEY.LIST1<J>,R.ECL,F.ECL,E2)
                    R.ECL<ECL.RATING> = RISK.RATE
                    CALL F.WRITE(FN.ECL,KEY.LIST1<J>,R.ECL)
                    CALL JOURNAL.UPDATE(KEY.LIST1<J>)
                NEXT J
            END
        END ELSE
            EOF = 'END OF FILE'
        END

    REPEAT
**********************************

*T.SEL = "SELECT F.SCB.ECL WITH CREDIT.CODE EQ '110' BY @ID"
    T.SEL = "SELECT F.SCB.ECL BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    LAST.W = R.DATES(EB.DAT.LAST.WORKING.DAY)
    LAST.DATE = LAST.W[7,2]:LAST.W[5,2]:LAST.W[1,4]
    IF SELECTED THEN
        BB.DATA = "Facility_ID,Category,Category_Desc,Product_Type,Customer_ID,Portfolio,Product_Type,Rating,Limit_ID,limit_Amt,Collateral_Code,Collateral_Value,Drawn_exposue,Undrawn_exposure,DPD,Valuation_Date,Expiry_Date,Payment_Freq,EIR_12_M,Customer_Type,Currency"
*BB.DATA = "Facility_ID,Category,Category_Desc,Product_Type,Customer_ID,Portfolio,Product_Type,Rating,Limit_ID,Drawn_exposue,DPD,Valuation_Date,Expiry_Date,Payment_Freq,EIR_12_M"
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        FOR I = 1 TO SELECTED
            TOT.BAL = 0
            CALL F.READ(FN.ECL,KEY.LIST<I>,R.ECL,F.ECL,E1)
            ECL.ID    = KEY.LIST<I>
            CRDT.CODE = R.ECL<ECL.CREDIT.CODE>
            IF CRDT.CODE EQ '100' THEN
                CUS.TYPE = "Performing"
            END
            IF CRDT.CODE EQ '110' THEN
                CUS.TYPE = "Non Performing"
            END
            IF CRDT.CODE EQ '' THEN
                CUS.TYPE = "Non Credit Customer"
            END

            FAC.LD    = R.ECL<ECL.FACILITY.ID>[1,2]
            FAC.ID    = R.ECL<ECL.FACILITY.ID>
            EXP.DATE  = R.ECL<ECL.EXPIRE.DATE>

            IF EXP.DATE NE '' THEN
                DAY.NUM   = ICONV(EXP.DATE,'DW')
                EXP.DATE1 = FMT(DAY.NUM,'D4')
            END ELSE
                EXP.DATE1 = ''
            END

            CATEG     = R.ECL<ECL.CATEGORY>
            CALL F.READ(FN.CAT,CATEG, R.CAT, F.CAT, ETEXT.CAT)
*CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CAT.NAME)
            BB.DATA  = R.ECL<ECL.FACILITY.ID>:','
            BB.DATA := CATEG:','
            BB.DATA := R.CAT<EB.CAT.DESCRIPTION,1>:','
*BB.DATA := CAT.NAME:','
            BB.DATA := R.ECL<ECL.LD.TYPE>:','
            BB.DATA := R.ECL<ECL.CUS.NO>:','
            BB.DATA := R.ECL<ECL.PORTFOLIO>:','
            BB.DATA := R.ECL<ECL.PRODUCT.TYPE>:','
            BB.DATA := R.ECL<ECL.RATING>:','
            BB.DATA := R.ECL<ECL.LIMIT.ID>:','
*BB.DATA := R.ECL<ECL.LIMIT.AMT>:','
            BB.DATA := '':','
*BB.DATA := R.ECL<ECL.COLL.CODE>:','
            BB.DATA := '':','
*BB.DATA := R.ECL<ECL.COLL.VALUE>:','
            BB.DATA := '':','
            BB.DATA := DROUND(R.ECL<ECL.DRAWN.EXP>,2):','
*BB.DATA := R.ECL<ECL.UNDRAWN.EXP>:','
            BB.DATA := '':','
*BB.DATA := R.ECL<ECL.CCF>:','
            BB.DATA := R.ECL<ECL.NDPD>:','
*BB.DATA := '29032018':','
            BB.DATA := LAST.DATE:','
**BB.DATA := EXP.DATE[7,2]:EXP.DATE[5,2]:EXP.DATE[3,2]:','
            BB.DATA := EXP.DATE1:','
            BB.DATA := R.ECL<ECL.PAY.FREQ>:','
            BB.DATA := R.ECL<ECL.INTEREST.RATE>:','
            BB.DATA := CUS.TYPE:','
            BB.DATA := R.ECL<ECL.CURRENCY>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END
*************************************************************
END
