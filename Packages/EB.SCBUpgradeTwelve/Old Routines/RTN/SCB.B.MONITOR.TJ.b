* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*--------------------------------------------------------------------------------
    SUBROUTINE SCB.B.MONITOR.TJ
*--------------------------------------------------------------------------------
* @Description: This is a T24 AUTO single thread batch service listening jBASE
*                Transaction Journalling log sets, though TJ provides tools for
*                monitoring they're not exhaustive for easier controls. This program
*                does the automatic switching of logsets when the current logset crosses
*                the threshold limit - set as environment variable named SCB_LS_THRESHOLD
*                in .profile.
*                T24 BATCH.JOB.CONTROL executes this routine every 30 min.
*
* @Date: 27-Aug-2016
*--------------------------------------------------------------------------------
    CALL OCOMO(STR("-",82))
    CALL OCOMO(TIMEDATE():" - Initializing TJ Log Set Listener Service ; Checking Status of current Log Set & Threshold Limit")
    GOSUB DO.INIT
    GOSUB GET.CURRENT.LS
    CALL OCOMO(TIMEDATE():" - Current Log Set # is ":CURR.LS:" with Threshold limit as ":THRESHOLD.LEVEL:"GB")
    GOSUB GET.LS.USAGE
    CALL OCOMO(TIMEDATE():" - TJ logset Consumption for Log Set # ":CURR.LS:" as on {":TIMEDATE():"} is [":FILE.SIZE.B:" Bytes ; ":FILE.SIZE.KB:" KB ; ":FILE.SIZE.MB:" MB ; ":FILE.SIZE.GB:" GB]")
    GOSUB DO.LS.SWITCH
    CALL OCOMO(STR("-",82))
    RETURN
*--------------------------------------------------------------------------------
DO.INIT:
*--------------------------------------------------------------------------------
* All the initial works are done here, get the threshold limit from env variable,
* get the status of current log set by parsing the jlogstatus output
*--------------------------------------------------------------------------------
    Y.OUTPUT = ""
    POS1 = ""
    POS2 = ""
    POS3 = ""
    CURR.LS = ""
    FILE.SIZE.B = ""
    FILE.SIZE.KB = ""
    FILE.SIZE.MB = ""
    FILE.SIZE.GB = ""
    THRESHOLD.LEVEL = ""
    SCB.LS.THRESHOLD = ""
    IF GETENV("SCB_LS_THRESHOLD",SCB.LS.THRESHOLD) THEN
        THRESHOLD.LEVEL = SCB.LS.THRESHOLD
    END ELSE
        THRESHOLD.LEVEL = "29"
    END
    RETURN
*--------------------------------------------------------------------------------
GET.CURRENT.LS:
*--------------------------------------------------------------------------------
* Get the current logset number from jlogstatus output
*--------------------------------------------------------------------------------
    EXECUTE "jlogstatus -c" CAPTURING Y.OUTPUT
    FINDSTR "Current log file set:" IN Y.OUTPUT SETTING POS1,POS2,POS3 THEN
        CURR.LS = TRIM(Y.OUTPUT<POS1,POS2,POS3>["Current log file set:",2,1][",",1,1])
    END
    RETURN
*--------------------------------------------------------------------------------
GET.LS.USAGE:
*--------------------------------------------------------------------------------
* Get the current usage from jlogstatus output ; Get KB, MB, GB equivalent
*--------------------------------------------------------------------------------
    FINDSTR "Total byte count:" IN Y.OUTPUT SETTING POS1,POS2,POS3 THEN
        Y.LINE = TRIM(Y.OUTPUT<POS1,POS2,POS3>["Total byte count:",2,1])
        FILE.SIZE.B = TRIM(Y.LINE,",","A")
        FILE.SIZE.KB = FILE.SIZE.B/1024
        FILE.SIZE.MB = FILE.SIZE.KB/1024
        FILE.SIZE.GB = FILE.SIZE.MB/1024
    END
    RETURN
*--------------------------------------------------------------------------------
DO.LS.SWITCH:
*--------------------------------------------------------------------------------
* If each logset crosses 29GB (out of 32GB TAFC limit) do the switch to next logset.
*--------------------------------------------------------------------------------
    IF INT(FILE.SIZE.GB) > "29" THEN
        CALL OCOMO(TIMEDATE():" - TJ Logsets in Set # ":CURR.LS:" Consumed ":FILE.SIZE.GB:" as on {":TIMEDATE():"}")
        CALL OCOMO(TIMEDATE():" - Switching to NEXT TJ Logset now..")
        EXECUTE "jlogadmin -lnext"
        GOSUB GET.CURRENT.LS
        CALL OCOMO(TIMEDATE(): " - Logset is switched, all data will now be flushed to log set # ":CURR.LS)
        CALL OCOMO(TIMEDATE():" - Current Threshold limit for Log Set # ":CURR.LS:" is ":THRESHOLD.LEVEL:"GB")
    END
    RETURN
*--------------------------------------------------------------------------------
END       ;*Final END Statement
*--------------------------------------------------------------------------------
