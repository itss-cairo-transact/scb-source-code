* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SCB.AHMED.DR(ENQ)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    FN.CUS='FBNK.CUSTOMER'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

    FN.STMT='FBNK.STMT.ACCT.CR'
    F.STMT=''
    R.STMT=''
    CALL OPF(FN.STMT,F.STMT)

    DATEE = '' ; YEAR1 = '' ; MONTH1= ''

    CALL DBR( 'DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    DATEE = '20081130'
*YYYY = DATEE[1,4]
*MM   = DATEE[5,2]
 CC   = "...":DATEE[1,6]:"..."
*TEXT = YYYY ; CALL REM
*TEXT = MM ; CALL REM
    TEXT  = CC ; CALL REM

*T.SEL="SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...CC..."
*   T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ...":CC:"... AND ":VV:" NE 1000 AND @ID UNLIKE ...6501... AND @ID UNLIKE ...6502... AND @ID UNLIKE ...6503..."
*    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ..." : CC :"... AND ":VV:" NE 1000 AND @ID UNLIKE ...6501... AND @ID UNLIKE ...6502... AND @ID UNLIKE ...6503..."
    T.SEL = "SELECT FBNK.STMT.ACCT.DR WITH @ID LIKE ..." : DATEE :" AND TOTAL.INTEREST NE ''"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = SELECTED ; CALL REM
*CALL F.READ(FN.CUS,Y.CUS.ID,ENQ.LP,F.CUS,CUS.ERR1)

    ENQ.LP   = '' ;
    TEXT  = 'KEY.LIST = ':KEY.LIST ; CALL REM
    MYID  = FIELD(KEY.LIST,'-',1)
    VV    = KEY.LIST[11,4]
    TEXT  = "MYID = :": MYID ; CALL REM
    TEXT  = "VV   = :": VV   ; CALL REM
    OPER.VAL = ''
*********************
    IF SELECTED THEN
        TEXT = 'HIIIIIIIIIIIII' ; CALL REM
        FOR ENQ.LP = 1 TO SELECTED
            IF VV NE 1000 AND VV NE 1101 AND VV NE 6501 AND VV NE 6502 AND VV NE 6503 THEN
                ENQ<2,ENQ.LP> = '@ID'
                ENQ<3,ENQ.LP> = 'EQ'
                ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
            END
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
    RETURN
END
