* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>86</Rating>
*-----------------------------------------------------------------------------

    SUBROUTINE SBR.RISK.RATING
*PROGRAM SBR.RISK.RATING

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.LOCAL.REFS
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE.NEW
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.RAT

*------------------------------------------------------
    FN.CREDIT.CBE='F.SCB.CREDIT.CBE.NEW' ; F.CREDIT.CBE=''
    FN.CU='FBNK.CUSTOMER' ; F.CU=''
    R.CU='' ; R.CREDIT=''

    CALL OPF(FN.CREDIT.CBE,F.CREDIT.CBE)
    CALL OPF(FN.CU,F.CU)
    FN.RISK = 'F.SCB.RISK.RAT'; F.RISK = ''
    CALL OPF(FN.RISK,F.RISK) ; R.RISK = ''
    T.DATE = TODAY
    GOSUB GETACC
*----------------------------------------------------------------
    RETURN
**********************************
GETACC:
*-----
*TEXT = 'I AM HERE'; CALL REM
    T.SEL  = "SELECT F.SCB.CREDIT.CBE.NEW WITH ( CO.CODE EQ EG0010001 "
    T.SEL := "AND @ID LIKE 13...) OR (@ID LIKE 323... OR @ID LIKE 133... "
    T.SEL := "OR ( @ID LIKE 63... AND CO.CODE EQ EG0010006 ) OR @ID LIKE 213...  OR @ID LIKE 233... "
    T.SEL := "OR @ID LIKE 703... OR ( @ID LIKE 73... AND CO.CODE EQ EG0010007 ) OR @ID LIKE 403... "
    T.SEL := "OR @ID LIKE 503... OR @ID LIKE 223... OR ( @ID LIKE 53... AND CO.CODE EQ EG0010005 ) "
    T.SEL := "OR @ID LIKE 903... OR @ID LIKE 143... OR @ID LIKE 303... OR (CO.CODE EQ EG0010002 AND @ID LIKE 23...) OR ( @ID LIKE 43... AND CO.CODE EQ EG0010004 ) OR @ID LIKE 230... "
    T.SEL := "OR @ID LIKE 523... OR @ID LIKE 163... OR @ID LIKE 173... OR @ID LIKE 183... OR @ID LIKE 193... OR @ID LIKE 243... OR @ID LIKE 253... OR @ID LIKE 263... OR @ID LIKE 273... OR @ID LIKE 413... OR @ID LIKE 533... "
    T.SEL := "OR @ID LIKE 203... OR @ID LIKE 603... OR @ID LIKE 153... OR @ID LIKE 353... OR @ID LIKE 123... OR ( @ID LIKE 93... AND CO.CODE EQ EG0010009 ) "
    T.SEL := "OR ( @ID LIKE 33... AND CO.CODE EQ EG0010003 ) OR @ID LIKE 103... OR @ID LIKE 113... OR @ID LIKE 803... OR @ID LIKE 813... OR @ID LIKE 513... "
    T.SEL := "OR @ID LIKE 313... OR @ID LIKE 823... OR @ID LIKE 343... OR @ID LIKE 833... OR @ID LIKE 333... OR @ID LIKE 363... OR @ID LIKE 293... ) "
    T.SEL := " AND (TOT.US.99 GT 0 OR TOT.CR.99 GT 0 )"
    T.SEL := " AND GE30.FLAG EQ 'Y'"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED  THEN
        FOR I = 1 TO SELECTED

            CUS.ID = KEY.LIST<I>
            CALL F.READ( FN.CREDIT.CBE,CUS.ID,R.CREDIT, F.CREDIT.CBE, CREDITERR)
            CBE.NO = R.CREDIT<SCB.C.CBE.CBE.NO>
            CALL F.READ( FN.CU,CUS.ID,R.CU, F.CU, CUSERR)
            RISK.RATE  = R.CU<EB.CUS.LOCAL.REF><1,CULR.RISK.RATE>
            NAB.DATE = R.CU<EB.CUS.LOCAL.REF><1,CULR.NAB.DATE>
            IF RISK.RATE EQ '' THEN
                RISK.RATE = 1
            END
            CALL F.READ( FN.RISK,CBE.NO,R.RISK,F.RISK,ERR.RISK)
            IF (ERR.RISK AND CBE.NO NE '') THEN
                GOSUB WRITEDATA
            END
        NEXT I
    END
    RETURN
**************************************************************************
WRITEDATA:
*---------

    R.RISK<RISK.CUST.NO>     = CUS.ID
    R.RISK<RISK.GADARA.CODE> = RISK.RATE
    R.RISK<RISK.NAB.DATE>    = NAB.DATE
*R.RISK<RISK.NAB.BAL>     = FMT(AC.BAL,"R2")

    CALL F.WRITE(FN.RISK,CBE.NO,R.RISK)
    CALL JOURNAL.UPDATE(CBE.NO)
    RETURN
***********************************************************************
