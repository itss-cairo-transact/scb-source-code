* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*SUBROUTINE SBY.ECL.STAGING.TXT
    PROGRAM SBY.ECL.STAGING.TXT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ECL
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*-----------------------------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "ECL.STAGING.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ECL.STAGING.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ECL.STAGING.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ECL.STAGING.CSV  CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ECL.STAGING.CSV File IN &SAVEDLISTS&'
        END
    END
*----------------------------------------------------------------

    FN.ECL  = 'F.SCB.ECL'        ; F.ECL = ''     ; R.ECL = ''
    CALL OPF( FN.ECL,F.ECL)

    FN.CAT = 'F.CATEGORY'        ; F.CAT = ''     ; R.CAT = ''
    CALL OPF( FN.CAT,F.CAT)

    FN.CUS = 'FBNK.CUSTOMER'        ; F.CUS = ''     ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.RSK = 'F.SCB.RISK.MAST'; F.RSK = '' ; R.RSK = ''
    CALL OPF(FN.RSK,F.RSK)

    T.SEL = "SELECT F.SCB.ECL BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    LAST.W = R.DATES(EB.DAT.LAST.WORKING.DAY)
    LAST.DATE = LAST.W[7,2]:LAST.W[5,2]:LAST.W[1,4]

    IF SELECTED THEN
        BB.DATA = "Customer_ID,Customer_Name,Facility_ID,Original_Rating,Current_Rating,PastDueDays,IsDefault,IsWatchlist,Resttrcutured in the past,Localflag1,Localflag2,Localflag3"
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

       FOR I = 1 TO SELECTED
            WATCH.LIST = 'N'
            DEFAULT.LIST = 'N'
            CALL F.READ(FN.ECL,KEY.LIST<I>,R.ECL,F.ECL,E1)
            ECL.ID    = KEY.LIST<I>
            FAC.ID    = R.ECL<ECL.FACILITY.ID>
            EXP.DATE  = R.ECL<ECL.EXPIRE.DATE>
            CUS.NO    = R.ECL<ECL.CUS.NO>
            CALL F.READ(FN.CUS,CUS.NO,R.CUS,F.CUS,E2)
            CUS.NAME  = R.CUS<EB.CUS.SHORT.NAME>
            RISK.RATE = R.CUS<EB.CUS.LOCAL.REF><1,CULR.RISK.RATE>
***STAGING*******************
            IF RISK.RATE EQ 7 THEN
                WATCH.LIST = 'Y'
            END
            IF RISK.RATE EQ 8 OR RISK.RATE EQ 8 OR RISK.RATE EQ 10 THEN
                DEFAULT.LIST = 'Y'
            END

***GET ORIGINAL RATE**********
            I.SEL = "SSELECT ":FN.RSK:" WITH CUSTOMER.ID EQ ":CUS.NO:" BY SYSTEM.DATE"
            CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
            RSK.ID = I.LIST<1>
            CALL F.READ(FN.RSK,RSK.ID,R.RSK,F.RSK,ERR.I1)
            OLD.RISK.RATE = DROUND(R.RSK<RM.TOT.CUS>,0)
******************************

            BB.DATA  = CUS.NO:','
            BB.DATA := CUS.NAME:','
            BB.DATA := FAC.ID:','
            BB.DATA := OLD.RISK.RATE:','
            BB.DATA := RISK.RATE:','
            BB.DATA := R.ECL<ECL.NDPD>:','
            BB.DATA := DEFAULT.LIST:','
            BB.DATA := WATCH.LIST:','

            BB.DATA := '':','
            BB.DATA := '':','
            BB.DATA := '':','
            BB.DATA := ''

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END
*************************************************************
END
