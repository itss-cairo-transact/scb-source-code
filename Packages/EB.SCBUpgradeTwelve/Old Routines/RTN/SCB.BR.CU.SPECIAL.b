* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>1247</Rating>
*-----------------------------------------------------------------------------
* Version 9 15/11/00  GLOBUS Release No. G11.1.01 11/12/00

    SUBROUTINE SCB.BR.CU.SPECIAL

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CU.SPECIAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PARMS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.CUST.LIMIT

*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
        CASE V$FUNCTION EQ 'D'
            GOSUB CHECK.DELETE          ;* Special Deletion checks
        CASE V$FUNCTION EQ 'R'
            GOSUB CHECK.REVERSAL        ;* Special Reversal checks
        CASE OTHERWISE
            GOSUB CROSS.VALIDATION      ;* Special Cross Validation
            IF NOT(V$ERROR) THEN
                GOSUB OVERRIDES
            END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

    IF E THEN V$ERROR = 1

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.
    RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS


    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

    RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
    Z=0


*Line [ 248 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(SCB.BCS.COLLECTION.ACCT),@VM) UNTIL ETEXT
        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,R.NEW(SCB.BCS.COLLECTION.ACCT)<1,I>,COL.ACCT)
        TEMP = FIELD(R.NEW(SCB.BCS.BILL.CHQ.TYPE)<1,I>,'.',2)
* TEXT =  TEMP[1,3] ; CALL REM
        IF COL.ACCT # TEMP[1,3] THEN
            ETEXT = "MAKE SURE THEN CURRENCY IS THE SAME"
            AF = SCB.BCS.COLLECTION.ACCT ; AV = I
            CALL STORE.END.ERROR
        END
    NEXT I

*Line [ 260 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR Z= 1 TO DCOUNT(R.NEW(SCB.BCS.MAX.AMT),@VM) UNTIL ETEXT
        IF R.NEW(SCB.BCS.MAX.AMT)<1,Z> LE R.NEW(SCB.BCS.MIN.AMT)<1,Z> THEN
            ETEXT = "MAKE SURE THE MIN.AMT IS LESS THEN THE MAX.AMT"
            AF = SCB.BCS.MIN.AMT ; AV = Z
            CALL STORE.END.ERROR
        END

    NEXT Z
    I = 0
*Line [ 270 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(SCB.BCS.LIMIT),@VM) UNTIL ETEXT
        LOCATE R.NEW(SCB.BCS.LIMIT)<1,I> IN R.NEW(SCB.BCS.LIMIT)<1,1> SETTING JJ THEN
            IF I # JJ THEN
                ETEXT = "DUPLICATE VALUE"
                AF = SCB.BCS.LIMIT ; AV = I
                CALL STORE.END.ERROR
            END
        END

    NEXT I
    Z=0

*Line [ 283 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR Z= 1 TO DCOUNT(R.NEW(SCB.BCS.MAX.DUR),@VM) UNTIL ETEXT
        IF R.NEW(SCB.BCS.MAX.DUR)<1,Z> LE R.NEW(SCB.BCS.MIN.DUR)<1,Z> THEN
            ETEXT = "MAKE SURE THE MIN.DUR IS LESS THEN THE MAX.DUR"
            AF = SCB.BCS.MIN.DUR ; AV = Z
            CALL STORE.END.ERROR
        END
    NEXT Z

    Z=0
*Line [ 293 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR Z= 1 TO DCOUNT(R.NEW(SCB.BCS.BILL.MAX.DUR),@VM) UNTIL ETEXT
        IF R.NEW(SCB.BCS.BILL.MAX.DUR)<1,Z> LE R.NEW(SCB.BCS.BILL.MIN.DUR)<1,Z> THEN
            ETEXT = "MAKE SURE THE BILL.MIN.DUR IS LESS THEN THE BILL.MAX.DUR"
            AF = SCB.BCS.BILL.MIN.DUR  ; AV = Z
            CALL STORE.END.ERROR
        END
    NEXT Z

    Z=0
*Line [ 303 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR Z= 1 TO DCOUNT(R.NEW(SCB.BCS.MAX.BILL.AMT),@VM) UNTIL ETEXT
        IF R.NEW(SCB.BCS.MAX.BILL.AMT)<1,Z> LE R.NEW(SCB.BCS.MIN.BILL.AMT)<1,Z> THEN
            ETEXT = "MAKE SURE THE MIN.BILL.AMT IS LESS THEN THE MAX.MAX.BILL.AMT"
            AF = SCB.BCS.MIN.BILL.AMT  ; AV = Z
            CALL STORE.END.ERROR
        END
    NEXT Z


    I = 0
*Line [ 314 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(SCB.BCS.BILL.CHQ.TYPE),@VM) UNTIL ETEXT
        LOCATE R.NEW(SCB.BCS.BILL.CHQ.TYPE)<1,I> IN R.NEW(SCB.BCS.BILL.CHQ.TYPE)<1,1> SETTING JJ THEN
            IF I # JJ THEN
                ETEXT = "DUPLICATE VALUE"
                AF = SCB.BCS.BILL.CHQ.TYPE ; AV = I
                CALL STORE.END.ERROR
            END
        END

    NEXT I

    I = 0
*Line [ 327 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(SCB.BCS.BILL.CHQ.TYPE),@VM) UNTIL ETEXT
        CALL DBR('SCB.BR.PARMS':@FM:SCB.BRP.COLL.TYPE,R.NEW(SCB.BCS.BILL.CHQ.TYPE)<1,I>,PAR.COL.TYPE)
        CALL DBR('COLLATERAL':@FM:COLL.COLLATERAL.TYPE,R.NEW(SCB.BCS.COLLATERAL)<1,I>,COL.TYPE)
        IF PAR.COL.TYPE # COL.TYPE THEN
            ETEXT = "THE COLLATERAL TYPE NOT THE SAME "
            AF = SCB.BCS.BILL.CHQ.TYPE ; AV = I
            CALL STORE.END.ERROR
        END ELSE
            LOCATE R.NEW(SCB.BCS.COLLATERAL)<1,I> IN R.NEW(SCB.BCS.COLLATERAL)<1,1> SETTING JJ THEN
                IF I # JJ THEN
                    ETEXT = "DUPLICATE VALUE"
                    AF = SCB.BCS.COLLATERAL ; AV = I
                    CALL STORE.END.ERROR
                END
            END

        END
    NEXT I

    I = 0
*Line [ 348 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR I = 1 TO DCOUNT(R.NEW(SCB.BCS.LIMIT),@VM) UNTIL ETEXT
        TEMP = FIELD(R.NEW(SCB.BCS.LIMIT)<1,I>,'.',1)
        IF TEMP # ID.NEW THEN
            ETEXT = "MUST SAME CUSTOMER ID "
            AF = SCB.BCS.LIMIT ; AV = I
            CALL STORE.END.ERROR
        END  
    NEXT I


REM > CALL XX.CROSSVAL
*
* If END.ERROR has been set then a cross validation error has occurred
*
IF END.ERROR THEN
    A = 1
    LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
    T.SEQU = A
    V$ERROR = 1
    MESSAGE = 'ERROR'
END
RETURN    ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
V$ERROR = ''
ETEXT = ''
TEXT = ''
REM > CALL XX.OVERRIDE
*

*
IF TEXT = "NO" THEN ;* Said NO to override
    V$ERROR = 1
    MESSAGE = "ERROR"         ;* Back to field input

END
RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************

CHECK.DELETE:


RETURN

*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************
DELIVERY.PREVIEW:

RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

IF TEXT = "NO" THEN ;* Said No to override
    CALL TRANSACTION.ABORT    ;* Cancel current transaction
    V$ERROR = 1
    MESSAGE = "ERROR"         ;* Back to field input
    RETURN
END

*
* Additional updates should be performed here
*
REM > CALL XX...



RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:

*CALL DBR('SCB.BR.CU.SPECIAL':@FM:SCB.BCS.LIMIT,ID.NEW,LIMIT)
*        TEXT =  R.OLD(SCB.BCS.LIMIT) ; CALL REM

*IF NOT(R.OLD(SCB.BCS.LIMIT))  THEN
*        TEXT =  R.OLD(SCB.BCS.LIMIT) ; CALL REM
I = 0

*Line [ 460 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
FOR I = 1 TO DCOUNT(R.NEW(SCB.BCS.LIMIT),@VM) UNTIL ETEXT

    BASE.ID = R.NEW(SCB.BCS.LIMIT)<1,I>

    TEXT = BASE.ID ; CALL REM

    FN.SCB.BR.CUST.LIMIT = 'F.SCB.BR.CUST.LIMIT' ; F.SCB.BR.CUST.LIMIT = '' ; R.SCB.BR.CUST.LIMIT = ''

    CALL OPF(FN.SCB.BR.CUST.LIMIT,F.SCB.BR.CUST.LIMIT)

    FN.LIMIT = 'FBNK.LIMIT' ; F.LIMIT = '' ; R.LIMIT = ''

    CALL OPF(FN.LIMIT,F.LIMIT)

    CALL F.READ(FN.LIMIT,BASE.ID, R.LIMIT, F.LIMIT, ETEXT)
XXX = R.LIMIT<LI.MAXIMUM.TOTAL>
ZZZZ = R.LIMIT<LI.LIMIT.CURRENCY>

    R.SCB.BR.CUST.LIMIT<SCB.BCL.TOTAL.SEC.AMT>  = R.LIMIT<LI.MAXIMUM.TOTAL>
    R.SCB.BR.CUST.LIMIT<SCB.BCL.DRAWEE.PER> = R.NEW(SCB.BCS.DRAWEE.PER) 
    R.SCB.BR.CUST.LIMIT<SCB.BCL.DRAWEE.CEILING> = (R.LIMIT<LI.MAXIMUM.TOTAL> * R.NEW(SCB.BCS.DRAWEE.PER)) / 100
*Line [ 482 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    FOR Z = 1 TO DCOUNT(R.NEW(SCB.BCS.DRAWEE),@SM) UNTIL ETEXT
     R.SCB.BR.CUST.LIMIT<SCB.BCL.SPC.DRAWEE> = R.NEW(SCB.BCS.DRAWEE)<1,1,Z>
     R.SCB.BR.CUST.LIMIT<SCB.BCL.SPC.DRAWEE.PER> = R.NEW(SCB.BCS.SPC.DR.PER)<1,1,Z>
     R.SCB.BR.CUST.LIMIT<SCB.BCL.SPC.DRAWEE.CEILING> = (R.LIMIT<LI.MAXIMUM.TOTAL> * R.NEW(SCB.BCS.SPC.DR.PER)<1,1,Z>) / 100
    NEXT Z
    R.SCB.BR.CUST.LIMIT< SCB.BCL.COLLATERAL> = R.NEW(SCB.BCS.COLLATERAL)<1,I>
    R.SCB.BR.CUST.LIMIT< SCB.BCL.COLLATERAL.PER>  = R.LIMIT<LI.COLL.REQD.PRCNT>

    TEXT = R.SCB.BR.CUST.LIMIT<SCB.BCL.DRAWEE.PER> ; CALL REM

    CALL F.WRITE(FN.SCB.BR.CUST.LIMIT,BASE.ID,R.SCB.BR.CUST.LIMIT)

    CALL JOURNAL.UPDATE(BASE.ID)
NEXT I

*END

RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

BEGIN CASE
CASE R.NEW(V-8)[1,3] = "INA"  ;* Record status
REM > CALL XX.AUTHORISATION
CASE R.NEW(V-8)[1,3] = "RNA"  ;* Record status
REM > CALL XX.REVERSAL

END CASE

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

IF INDEX('V',V$FUNCTION,1) THEN
    E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
    CALL ERR
    V$FUNCTION = ''
END

RETURN

*************************************************************************

INITIALISE:

RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

MAT F = "" ; MAT N = "" ; MAT T = ""
MAT CHECKFILE = "" ; MAT CONCATFILE = ""
*Line [ 544 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
ID.CHECKFILE = "CUSTOMER":@FM:2:@FM:"L"

ID.F  = "CUST"; ID.N  = "10"; ID.T  = "CUS"
*
Z=0
*
Z+=1 ; F(Z) = "XX<LIMIT" ;  N(Z) = "32.1" ; T(Z) = "S"
CHECKFILE(Z)="LIMIT"
Z+=1 ; F(Z) = "XX-DRAWEE.PER" ;  N(Z) = "11.1" ; T(Z) = "R"
Z+=1 ; F(Z) = "XX-MIN.DUR" ;  N(Z) = "3.1" ; T(Z) = ""
Z+=1 ; F(Z) = "XX-MAX.DUR" ;  N(Z) = "3.1" ; T(Z) = ""
Z+=1 ; F(Z) = "XX-MIN.AMT" ;  N(Z) = "19.1" ; T(Z) = "AMT"
Z+=1 ; F(Z) = "XX-MAX.AMT" ;  N(Z) = "19.1" ; T(Z) = "AMT"
Z+=1 ; F(Z) = "XX-RESERVED.FIELD10" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-RESERVED.FIELD9" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-RESERVED.FIELD8" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-RESERVED.FIELD7" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-RESERVED.FIELD6" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-XX<BILL.CHQ.TYPE" ;  N(Z) = "7.1" ; T(Z) = "A"
*Line [ 569 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
CHECKFILE(Z)="SCB.BR.PARMS":@FM:1:@FM:"L"
Z+=1 ; F(Z) = "XX-XX-COLLECTION.ACCT" ;  N(Z) = "16.1" ; T(Z) = "ANT"
*Line [ 572 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
CHECKFILE(Z)="ACCOUNT":@FM:3:@FM:"L"
Z+=1 ; F(Z) = "XX-XX>COLLATERAL" ;  N(Z) = "16.1" ; T(Z) = "A"
*Line [ 575 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
CHECKFILE(Z)="COLLATERAL":@FM:2:@FM:"L"
Z+=1 ; F(Z) = "XX-XX<DRAWEE" ;  N(Z) = "10.1" ; T(Z) = ""
*Line [ 578 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
CHECKFILE(Z)="SCB.BR.DRAWEE":@FM:2:@FM:"L"
Z+=1 ; F(Z) = "XX-XX-BILL.MIN.DUR" ;  N(Z) = "3.1" ; T(Z) = ""
Z+=1 ; F(Z) = "XX-XX-BILL.MAX.DUR" ;  N(Z) = "3.1" ; T(Z) = ""
Z+=1 ; F(Z) = "XX-XX-SPC.DR.PER" ;  N(Z) = "11.1" ; T(Z) = "R"
Z+=1 ; F(Z) = "XX-XX-EXCLUDE.MAX" ;  N(Z) = "3.1" ; T(Z) = ""
Z+=1 ; F(Z) = "XX-XX-MIN.BILL.AMT" ;  N(Z) = "19.1" ; T(Z) = "AMT"
Z+=1 ; F(Z) = "XX-XX-MAX.BILL.AMT" ;  N(Z) = "19.1" ; T(Z) = "AMT"

Z+=1 ; F(Z) = "XX-XX-RESERVED.FIELD5" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-XX-RESERVED.FIELD4" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-XX-RESERVED.FIELD3" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX-XX-RESERVED.FIELD2" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'
Z+=1 ; F(Z) = "XX>XX>RESERVED.FIELD1" ; N(Z) = "35" ; T(Z) = "ANY"
T(Z)<3> = 'NOINPUT'

V = Z + 9
*

RETURN

*************************************************************************

END
