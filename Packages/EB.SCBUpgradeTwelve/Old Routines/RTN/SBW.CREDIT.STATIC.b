* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
******* CREATED BY KHALED IBRAHIM *******

    SUBROUTINE SBW.CREDIT.STATIC
**  PROGRAM SBW.CREDIT.STATIC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STATIC.CREDIT.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUSTOMER.GROUP
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*==============================================================
INITIATE:
    DATD.CURR = TODAY
    DATM.CURR = DATD.CURR[1,6]
    DAT.CURR  = DATM.CURR:"01"
    CALL CDT ('',DAT.CURR,'-1W')

*    DATM.LAST = DAT.CURR[1,6]
* MOHAMED SABRY
    DATM.LAST = TODAY[1,6]

    DAT.LAST  = DATM.LAST:"01"
    CALL CDT ('',DAT.LAST,'-1W')

    DAT.CURR.W = TODAY
    DAT.LAST.W = TODAY
    CALL CDT ('',DAT.LAST.W,'-7W')

    DAT.Q   = TODAY
    CALL LAST.DAY(DAT.Q)
    DAT.Q.M = TODAY[5,2]
    DAT.Q.1 = DAT.Q
    DAT.Q.2 = DAT.Q

    IF DAT.Q.M EQ '01' OR DAT.Q.M EQ '04' OR DAT.Q.M EQ '07' OR DAT.Q.M EQ '10' THEN
        CALL ADD.MONTHS(DAT.Q.1,'-1')
        CALL ADD.MONTHS(DAT.Q.2,'-4')
    END

    IF DAT.Q.M EQ '02' OR DAT.Q.M EQ '05' OR DAT.Q.M EQ '08' OR DAT.Q.M EQ '11' THEN
        CALL ADD.MONTHS(DAT.Q.1,'-2')
        CALL ADD.MONTHS(DAT.Q.2,'-5')
    END

    IF DAT.Q.M EQ '03' OR DAT.Q.M EQ '06' OR DAT.Q.M EQ '09' OR DAT.Q.M EQ '12' THEN
        CALL ADD.MONTHS(DAT.Q.1,'-3')
        CALL ADD.MONTHS(DAT.Q.2,'-6')
    END

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,INT.DAT)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,INT.DAT)

    DAT.LAST = DAT.CURR
    DAT.CURR = INT.DAT
    TT = FMT(DAT.CURR,"####/##/##")
    KK = FMT(DAT.LAST,"####/##/##")

    OPENSEQ "CREDIT.STATIC" , "SBW.CREDIT.STATIC.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"CREDIT.STATIC":' ':"SBW.CREDIT.STATIC.CSV"
        HUSH OFF
    END
    OPENSEQ "CREDIT.STATIC" , "SBW.CREDIT.STATIC.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBW.CREDIT.STATIC.CSV CREATED IN CREDIT.STATIC'
        END ELSE
            STOP 'Cannot create SBW.CREDIT.STATIC.CSV File IN CREDIT.STATIC'
        END
    END

    HEAD.DESC = ",":TT:" ":KK:" ":"���� ����� ������ ����� �������� �� ����"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "�":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� �������":","
    HEAD.DESC := "��������":",,,"
    HEAD.DESC := "��������� ":KK:",,,"
    HEAD.DESC := "��������� ":TT:",,,"
    HEAD.DESC := "������":","
    HEAD.DESC := "���� ������":","
    HEAD.DESC := "���� �������� ������":","
    HEAD.DESC := "����� ��������":","
    HEAD.DESC := "���� ������� ������":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    HEAD.DESC := "�����":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.SCC = 'F.SCB.STATIC.CREDIT.CUSTOMER' ; F.SCC = ''
    CALL OPF(FN.SCC,F.SCC)

    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    WS.DIR.INTERNAL.AMT    = 0
    WS.INDIR.INTERNAL.AMT  = 0
    TOTAL.INTERNAL         = 0
    WS.DIR.USED.AMT.LAST   = 0
    WS.INDIR.USED.AMT.LAST = 0
    TOTAL.USED.LAST        = 0
    WS.DIR.USED.AMT        = 0
    WS.INDIR.USED.AMT      = 0
    TOTAL.USED             = 0
    WS.CHANGE              = 0
    WS.CHANGE.PER          = 0
    WS.PER.USED.INTERNAL   = 0
    WS.USED.AVG            = 0
    WS.PER.AVG.INTERNAL    = 0
    GROUP.ID.TMP = 0
    KK = 0
****

    WS.DIR.INTERNAL.AMT.TOT    = 0
    WS.INDIR.INTERNAL.AMT.TOT  = 0
    TOTAL.INTERNAL.TOT         = 0
    WS.DIR.USED.AMT.LAST.TOT   = 0
    WS.INDIR.USED.AMT.LAST.TOT = 0
    TOTAL.USED.LAST.TOT        = 0
    WS.DIR.USED.AMT.TOT        = 0
    WS.INDIR.USED.AMT.TOT      = 0
    TOTAL.USED.TOT             = 0
    WS.CHANGE.TOT              = 0
    WS.CHANGE.PER.TOT          = 0
    WS.PER.USED.INTERNAL.TOT   = 0
    WS.USED.AVG.TOT            = 0
    WS.PER.AVG.INTERNAL.TOT    = 0
****
    RETURN
*========================================================================
PROCESS:

    T.SEL = "SELECT ":FN.SCC:" WITH LPE.DATE EQ ":DAT.LAST:" OR LPE.DATE EQ ":DAT.CURR:" OR LPE.DATE EQ ":INT.DAT:" BY GROUP.ID BY CO.CODE"
**    T.SEL = "SELECT ":FN.SCC:" WITH CUSTOMER.ID IN ( 1100024 1100080 2300094 2300254 13300321 13300371 13300381 13300412 ) BY GROUP.ID BY CO.CODE"
**    T.SEL = "SELECT ":FN.SCC:" WITH CUSTOMER.ID EQ 1224159"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.SCC,KEY.LIST<I>,R.SCC,F.SCC,E1)
            GROUP.ID = R.SCC<SCC.GROUP.ID>

            IF GROUP.ID EQ '' THEN
                GROUP.ID = R.SCC<SCC.CUSTOMER.ID>
            END

            IF I EQ 1 THEN GROUP.ID.TMP = GROUP.ID
            IF GROUP.ID NE GROUP.ID.TMP THEN
                GOSUB DATA.FILE
            END
            CUS.ID   = R.SCC<SCC.CUSTOMER.ID>
            CALL DBR ('SCB.CUSTOMER.GROUP':@FM:CG.GROUP.NAME,GROUP.ID,CUST.NAME)

            IF CUST.NAME EQ '' THEN
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
                CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
            END ELSE
                CUS.ID = GROUP.ID
            END

            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,COMP.ID)
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,BRANCH.NAME)
*---------------------------------------------------------
            DAT = R.SCC<SCC.LPE.DATE>

            WS.CY  = R.SCC<SCC.CURRENCY>
*Line [ 239 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CY.DC  = DCOUNT(WS.CY,@VM)
            FOR X = 1 TO CY.DC
                IF  R.SCC<SCC.CURRENCY,X> NE 'EGP' THEN
                    CALL F.READ(FN.CCY,R.SCC<SCC.CURRENCY,X>,R.CCY,F.CCY,E1)
                    CY.RATE = R.CCY<EB.CUR.SELL.RATE,1>
                    IF R.SCC<SCC.CURRENCY,X> EQ 'JPY' THEN
                        CY.RATE = ( CY.RATE / 100 )
                    END
                END ELSE
                    CY.RATE = 1
                END

*************** TODAY INTERNAL *******************************

                IF DAT EQ INT.DAT THEN
                    WS.DIR.INTERNAL.AMT   += R.SCC<SCC.DIR.INTERNAL.AMT,X>   * CY.RATE
                    WS.INDIR.INTERNAL.AMT += R.SCC<SCC.INDIR.INTERNAL.AMT,X> * CY.RATE
                    TOTAL.INTERNAL        += R.SCC<SCC.TOTAL.INTERNAL.AMT,X> * CY.RATE
                    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ETEXT)
                    LOOP
                        REMOVE AC.ID FROM R.CUS.ACC SETTING POS.ACCT
                    WHILE AC.ID:POS.ACCT
*MSABRY TMP 2011/07/02
*CALL SCB.GET.AVR.DR.BAL(AC.ID,DAT.LAST.W,DAT.CURR.W,AVR.BAL)
                        AVR.BAL =0
                        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,AC.ID,AC.CY)
                        IF AC.CY NE 'EGP' THEN
                            CALL F.READ(FN.CCY,AC.CY,R.CCY,F.CCY,E1)
                            AC.CY.RATE = R.CCY<EB.CUR.SELL.RATE,1>
                            IF AC.CY EQ 'JPY' THEN
                                AC.CY.RATE = ( AC.CY.RATE / 100 )
                            END
                        END ELSE
                            AC.CY.RATE = 1
                        END
                        WS.USED.AVG += AVR.BAL * AC.CY.RATE
                    REPEAT
                END

************** CURRENT MONTH *********************************

                IF DAT EQ DAT.CURR THEN
                    WS.DIR.USED.AMT       += R.SCC<SCC.DIR.USED.AMT,X>   * CY.RATE
                    WS.INDIR.USED.AMT     += R.SCC<SCC.INDIR.USED.AMT,X> * CY.RATE
                    TOTAL.USED            += R.SCC<SCC.TOTAL.USED.AMT,X> * CY.RATE
                END

********************** LAST MONTH *********************************

                IF DAT EQ DAT.LAST THEN
                    WS.DIR.USED.AMT.LAST   += R.SCC<SCC.DIR.USED.AMT,X>   * CY.RATE
                    WS.INDIR.USED.AMT.LAST += R.SCC<SCC.INDIR.USED.AMT,X> * CY.RATE
                    TOTAL.USED.LAST        += R.SCC<SCC.TOTAL.USED.AMT,X> * CY.RATE
                END

****************** CALC CHANGE & AVERAGE **********************
                WS.CHANGE     = ( TOTAL.USED - TOTAL.USED.LAST )

*******************************
            NEXT X
            GROUP.ID.TMP = GROUP.ID
        NEXT I
        IF I EQ SELECTED THEN GOSUB DATA.FILE
    END
**** PRINT TOTALS **************

    BB.DATA  = ","
    BB.DATA := "��������":","
    BB.DATA := ","
    BB.DATA := ","

    BB.DATA := WS.DIR.INTERNAL.AMT.TOT:","
    BB.DATA := WS.INDIR.INTERNAL.AMT.TOT:","
    BB.DATA := TOTAL.INTERNAL.TOT:","
    BB.DATA := WS.DIR.USED.AMT.LAST.TOT:","
    BB.DATA := WS.INDIR.USED.AMT.LAST.TOT:","
    BB.DATA := TOTAL.USED.LAST.TOT:","
    BB.DATA := WS.DIR.USED.AMT.TOT:","
    BB.DATA := WS.INDIR.USED.AMT.TOT:","
    BB.DATA := TOTAL.USED.TOT:","
    BB.DATA := WS.CHANGE.TOT:","
    BB.DATA := WS.CHANGE.PER.TOT:","
    BB.DATA := WS.PER.USED.INTERNAL.TOT:","
    BB.DATA := WS.USED.AVG.TOT:","
    BB.DATA := WS.PER.AVG.INTERNAL.TOT:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
********************************

    RETURN
*------------------------------------------------------------
******************* WRITE DATA IN FILE **********************
DATA.FILE:
***** DIVIDED BY 1,000,000 *****
    WS.DIR.INTERNAL.AMT    = WS.DIR.INTERNAL.AMT    / 1000000
    WS.INDIR.INTERNAL.AMT  = WS.INDIR.INTERNAL.AMT  / 1000000
    TOTAL.INTERNAL         = TOTAL.INTERNAL         / 1000000
    WS.DIR.USED.AMT.LAST   = WS.DIR.USED.AMT.LAST   / 1000000
    WS.INDIR.USED.AMT.LAST = WS.INDIR.USED.AMT.LAST / 1000000
    TOTAL.USED.LAST        = TOTAL.USED.LAST        / 1000000
    WS.DIR.USED.AMT        = WS.DIR.USED.AMT        / 1000000
    WS.INDIR.USED.AMT      = WS.INDIR.USED.AMT      / 1000000
    TOTAL.USED             = TOTAL.USED             / 1000000
    WS.CHANGE              = WS.CHANGE              / 1000000
    WS.USED.AVG            = WS.USED.AVG            / 1000000

    WS.DIR.INTERNAL.AMT    = DROUND(WS.DIR.INTERNAL.AMT,'2')
    WS.INDIR.INTERNAL.AMT  = DROUND(WS.INDIR.INTERNAL.AMT,'2')
    TOTAL.INTERNAL         = DROUND(TOTAL.INTERNAL,'2')
    WS.DIR.USED.AMT.LAST   = DROUND(WS.DIR.USED.AMT.LAST,'2')
    WS.INDIR.USED.AMT.LAST = DROUND(WS.INDIR.USED.AMT.LAST,'2')
    TOTAL.USED.LAST        = DROUND(TOTAL.USED.LAST,'2')
    WS.DIR.USED.AMT        = DROUND(WS.DIR.USED.AMT,'2')
    WS.INDIR.USED.AMT      = DROUND(WS.INDIR.USED.AMT,'2')
    TOTAL.USED             = DROUND(TOTAL.USED,'2')
    WS.CHANGE              = DROUND(WS.CHANGE,'2')
    WS.USED.AVG            = DROUND(WS.USED.AVG,'2')

**************** CALC PERCENTAGE ****************
    IF TOTAL.USED.LAST NE 0 THEN
        WS.CHANGE.PER        = ( WS.CHANGE / TOTAL.USED.LAST ) * 100
    END
    IF TOTAL.INTERNAL NE 0 THEN
        WS.PER.USED.INTERNAL = ( TOTAL.USED / TOTAL.INTERNAL ) * 100
    END
    IF TOTAL.INTERNAL NE 0 THEN
        WS.PER.AVG.INTERNAL  = ( WS.USED.AVG / TOTAL.INTERNAL ) * 100
    END

    WS.CHANGE.PER          = DROUND(WS.CHANGE.PER,'2')
    WS.PER.USED.INTERNAL   = DROUND(WS.PER.USED.INTERNAL,'2')
    WS.PER.AVG.INTERNAL    = DROUND(WS.PER.AVG.INTERNAL,'2')
***************************************************
    IF TOTAL.INTERNAL GE 40 THEN
        KK++
        BB.DATA  = KK:","
        BB.DATA := CUST.NAME:","
        BB.DATA := CUS.ID:","
        BB.DATA := BRANCH.NAME:","

        BB.DATA := WS.DIR.INTERNAL.AMT:","
        BB.DATA := WS.INDIR.INTERNAL.AMT:","
        BB.DATA := TOTAL.INTERNAL:","
        BB.DATA := WS.DIR.USED.AMT.LAST:","
        BB.DATA := WS.INDIR.USED.AMT.LAST:","
        BB.DATA := TOTAL.USED.LAST:","
        BB.DATA := WS.DIR.USED.AMT:","
        BB.DATA := WS.INDIR.USED.AMT:","
        BB.DATA := TOTAL.USED:","
        BB.DATA := WS.CHANGE:","
        BB.DATA := WS.CHANGE.PER:","
        BB.DATA := WS.PER.USED.INTERNAL:","
        BB.DATA := WS.USED.AVG:","
        BB.DATA := WS.PER.AVG.INTERNAL:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.DIR.INTERNAL.AMT.TOT    += WS.DIR.INTERNAL.AMT
        WS.INDIR.INTERNAL.AMT.TOT  += WS.INDIR.INTERNAL.AMT
        TOTAL.INTERNAL.TOT         += TOTAL.INTERNAL
        WS.DIR.USED.AMT.TOT        += WS.DIR.USED.AMT
        WS.INDIR.USED.AMT.TOT      += WS.INDIR.USED.AMT
        TOTAL.USED.TOT             += TOTAL.USED
        WS.DIR.USED.AMT.LAST.TOT   += WS.DIR.USED.AMT.LAST
        WS.INDIR.USED.AMT.LAST.TOT += WS.INDIR.USED.AMT.LAST
        TOTAL.USED.LAST.TOT        += TOTAL.USED.LAST
        WS.CHANGE.TOT     += WS.CHANGE
        WS.CHANGE.PER.TOT += WS.CHANGE.PER
        WS.USED.AVG.TOT   += WS.USED.AVG
        WS.PER.USED.INTERNAL.TOT += WS.PER.USED.INTERNAL
        WS.PER.AVG.INTERNAL.TOT  += WS.PER.AVG.INTERNAL

        WS.DIR.INTERNAL.AMT    = 0
        WS.INDIR.INTERNAL.AMT  = 0
        TOTAL.INTERNAL         = 0
        WS.DIR.USED.AMT.LAST   = 0
        WS.INDIR.USED.AMT.LAST = 0
        TOTAL.USED.LAST        = 0
        WS.DIR.USED.AMT        = 0
        WS.INDIR.USED.AMT      = 0
        TOTAL.USED             = 0
        WS.CHANGE              = 0
        WS.CHANGE.PER          = 0
        WS.PER.USED.INTERNAL   = 0
        WS.USED.AVG            = 0
        WS.PER.AVG.INTERNAL    = 0

    END
*************************************************************
    RETURN
*---------------------------------------------------
END
