* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*SUBROUTINE SBY.ECL.LIMIT.TXT
    PROGRAM SBY.ECL.LIMIT.TXT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ECL
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*-----------------------------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "ECL.LIMIT.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ECL.LIMIT.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ECL.LIMIT.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ECL.LIMIT.TXT  CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ECL.LIMIT.TXT File IN &SAVEDLISTS&'
        END
    END

    ID.EXCP = "....0032060...."
*----------------------------------------------------------------

    FN.ECL  = 'F.SCB.ECL'        ; F.ECL = ''     ; R.ECL = ''
    CALL OPF( FN.ECL,F.ECL)

    TOT.UNDRWN = 0; TOT.DRWN = 0; TOT.CASH = 0
*T.SEL = "SELECT F.SCB.ECL WITH CREDIT.CODE EQ '100' AND LIMIT.ID NE '' BY LIMIT.ID"
    T.SEL = "SELECT F.SCB.ECL WITH LIMIT.ID NE '' AND LIMIT.ID UNLIKE ":ID.EXCP:" BY LIMIT.ID BY CASH.ACCT"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        BB.DATA = "Customer_ID,Limit_ID,limit_Amt,Collateral_Code,Collateral_Value,Cash_Cover,Drawn_exposue,Undrawn_exposure"
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        FOR I = 1 TO SELECTED
            TOT.BAL = 0
            CALL F.READ(FN.ECL,KEY.LIST<I>,R.ECL,F.ECL,E1)
            NEW.LMT.ID = R.ECL<ECL.LIMIT.ID>
            CASH.ACCT  = R.ECL<ECL.CASH.ACCT>
            WS.CCY     = R.ECL<ECL.CURRENCY>

*IF NEW.LMT.ID = '2305162.0035105.03' THEN DEBUG

            IF I = 1 THEN
                OLD.LMT.ID = NEW.LMT.ID
*OLD.CASH.ACCT = CASH.ACCT
            END


            IF NEW.LMT.ID NE OLD.LMT.ID THEN

                UNDRWN.NEW =  LMT.AMT - TOT.DRWN
                BB.DATA  = CUS.NO:','
                BB.DATA := OLD.LMT.ID:','
                BB.DATA := DROUND(LMT.AMT,2):','
                BB.DATA := COLL.CODE:','
                BB.DATA := DROUND(COLL.VAL,2):','
                BB.DATA := DROUND(TOT.CASH,2):','
                BB.DATA := DROUND(TOT.DRWN,2):','
                BB.DATA := DROUND(UNDRWN.NEW,2):','

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOT.DRWN   = 0
                TOT.UNDRWN = 0
                TOT.CASH   = 0
            END

            CUS.NO     = R.ECL<ECL.CUS.NO>
            FAC.ID     = R.ECL<ECL.FACILITY.ID>
            LMT.AMT    = R.ECL<ECL.LIMIT.AMT>
            COLL.CODE  = R.ECL<ECL.COLL.CODE>
            COLL.VAL   = R.ECL<ECL.COLL.VALUE>
            DRWN.AMT   = R.ECL<ECL.DRAWN.EXP>
            UNDRWN.AMT = R.ECL<ECL.UNDRAWN.EXP>
            TOT.DRWN   = TOT.DRWN + DRWN.AMT
            CASH.AMT   = R.ECL<ECL.CASH.COVER>

*IF (FAC.ID[1,2] EQ 'TF' OR FAC.ID[1,2] EQ 'LD') AND (CASH.ACCT NE OLD.CASH.ACCT) THEN
            TOT.CASH   = TOT.CASH + CASH.AMT
*END

            OLD.LMT.ID = NEW.LMT.ID
*OLD.CASH.ACCT = CASH.ACCT
        NEXT I
    END
*************************************************************
END
