* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.MAACH.ERROR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*----------------------------------------------
    TD       = TODAY
    ERR.FLAG = ''

    SEQ.FILE.NAME = "MECH"
    RECORD.NAME = "MAACH.ERROR"
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            ERR.FLAG  = Y.MSG

            IF ERR.FLAG EQ TD THEN
                EXECUTE 'COPY FROM MECH TO OFS.IN maach_cr.all'
                EXECUTE 'DELETE ':"MECH":' ':"MAACH.ERROR"
                EXECUTE 'DELETE ':"MECH":' ':"maach_cr.all"
                TEXT = '�� ������� ' ; CALL REM
            END ELSE
                TEXT = '���� ��� ������ ��� ���� ������' ; CALL REM
            END

        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ SEQ.FILE.POINTER

    RETURN
*-------------------------------------------

END
