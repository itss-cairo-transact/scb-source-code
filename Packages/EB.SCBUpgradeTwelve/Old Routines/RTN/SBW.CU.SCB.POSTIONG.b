* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-276</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBW.CU.SCB.POSTIONG

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR



    GOSUB INITIALISE
    GOSUB SEL.5
    GOSUB SEL.6
    GOSUB SEL.8
    GOSUB SEL.9

*    GOSUB SEL.DEL
*    PRINT 'SCB.POSTING.CLEARED'
*    GOSUB SEL.1
*    GOSUB SEL.2
*    GOSUB SEL.3
*    GOSUB SEL.4
*    GOSUB SEL.7
    PRINT 'PROGRAM.COMPLETED'

    RETURN
*-----------------------------------------------
INITIALISE:
*----------
    KEY.LIST=""   ; SELECTED=""   ;  ER.MSG=""
    KEY.LIST1=""  ; SELECTED1=""  ;  ER.MSG1=""
    KEY.LIST2=""  ; SELECTED2=""  ;  ER.MSG2=""

    FN.CU = 'FBNK.CUSTOMER'  ; F.CU = '' ; R.CU= ''
    CALL OPF(FN.CU,F.CU)

    FN.SEC = 'F.SCB.NEW.SECTOR'  ; F.SEC = '' ; R.SEC= ''
    CALL OPF(FN.SEC,F.SEC)

    RETURN
*----------------------------------------------------
SEL.DEL:
*-----------
***** DELETE SCB.POSTING.RESTRICT FROM CUSTOMERS ****
**----------------------------------------
    T.SEL = "SELECT ":FN.CU:" WITH SCB.POSTING NE ''"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = ''
            GOSUB WRITE.DATA

        NEXT I
    END
    RETURN
*****************************************************
SEL.1:
*-----------
***** TRANSFER ALL POSTING.RESTRICT TO SCB.POSTING.RESTRICT ****
**----------------------------------------
    T.SEL = "SELECT ":FN.CU
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = R.CU<EB.CUS.POSTING.RESTRICT>

            GOSUB WRITE.DATA

        NEXT I
    END
    RETURN
*************************************************************
SEL.2:
*-------
***** WRITE CODE 19 IN SCB.POSTING.RESTRICT WITH CODES 21-78 ****
**------------------------------
    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT IN (21 78) WITHOUT SECTOR IN (5010 5020)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 19
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN
*************************************************************
SEL.3:
*------
***** WRITE CODE 19 IN SCB.POSTING.RESTRICT WITH NATIONALITY EQ EG & NSN.NO EQ '' ****
**------------------------------
    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NATIONALITY EQ EG AND NSN.NO EQ '' AND CUSTOMER.STATUS NE 3 AND ID.TYPE NE 5 WITHOUT SECTOR IN (5010 5020)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 19
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN
*************************************************************
SEL.4:
*------
***** WRITE CODE 19 IN SCB.POSTING.RESTRICT NATIONALITY NE EG & ID.NUMBER EQ '' ****
**------------------------------
    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NATIONALITY NE EG AND ID.NUMBER EQ '' AND CUSTOMER.STATUS NE 3 AND ID.TYPE NE 5 WITHOUT SECTOR IN (5010 5020)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 19
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN

*************************************************************
SEL.5:
*------
***** WRITE CODE 29 IN SCB.POSTING.RESTRICT NATIONALITY EQ EG AND ID.EXPIRY.DATE LE TODAY ****
**------------------------------
    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NATIONALITY EQ EG AND SCB.POSTING NE 19 AND ID.EXPIRY.DATE LT ":TODAY:" AND ID.EXPIRY.DATE NE '' WITHOUT SECTOR IN (5010 5020)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 29
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN

*******EDITED BY MAI SAAD 17 MARCH 2021******************************************************
SEL.6:
*-------
***** WRITE CODE 29 IN SCB.POSTING.RESTRICT NATIONALITY NE EG AND ID.EXPIRY.DATE LE TODAY ****
**------------------------------
*    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NATIONALITY NE EG AND SCB.POSTING NE 19 AND ID.EXPIRY.DATE LT ":TODAY:" WITHOUT SECTOR IN (5010 5020)"
     T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NATIONALITY NE EG AND SCB.POSTING NE 19 AND (ID.EXPIRY.DATE LT ":TODAY:" OR RESID.END.DATE LT ":TODAY:") WITHOUT SECTOR IN (5010 5020)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 29
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN

*************************************************************
SEL.7:
*-----
***** WRITE CODE 19 IN SCB.POSTING.RESTRICT WITH CORPORATE ****
**------------------------------
    T.SEL1 = "SELECT ":FN.SEC:" WITH @ID NE 4250 AND PROVE.CODE EQ 2"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    T.SEL = "SELECT ":FN.CU:" WITH NEW.SECTOR IN ( "
    FOR J = 1 TO SELECTED1
        T.SEL := KEY.LIST1<J>:" "
    NEXT J

    T.SEL := " ) AND POSTING.RESTRICT LT 90 AND (COM.REG.NO EQ '' OR PLACE.ID.ISSUE EQ '') WITHOUT SECTOR IN (5010 5020)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 19
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN
*********************************************************
SEL.8:
*------

************ WRITE CODE 29 IN SCB.POSTING.RESTRICT WITH CORPORATE ******
    T.SEL1 = "SELECT ":FN.SEC:" WITH @ID NE 4250 AND PROVE.CODE EQ 2"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
*********
    T.SEL = "SELECT ":FN.CU:" WITH NEW.SECTOR IN ( "
    FOR J = 1 TO SELECTED1
        T.SEL := KEY.LIST1<J>:" "
    NEXT J

    T.SEL := " ) AND POSTING.RESTRICT LT 90 AND SCB.POSTING NE 19 AND LIC.EXP.DATE LT ":TODAY:" WITHOUT SECTOR IN (5010 5020)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
******************
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 29
            GOSUB WRITE.DATA
        NEXT I
    END

    RETURN
*********************************************************
SEL.9:
*-------
***** WRITE CODE 29 IN SCB.POSTING.RESTRICT WITH CODES 18 ****
**------------------------------
    T.SEL = "SELECT ":FN.CU:" WITH POSTING.RESTRICT EQ 18 AND SCB.POSTING NE 19 WITHOUT SECTOR IN (5010 5020)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            WS.POST.CODE = 29
            GOSUB WRITE.DATA
        NEXT I
    END
    RETURN
*************************************************************
*----------------------------------------
WRITE.DATA:

    R.CU<EB.CUS.LOCAL.REF,CULR.SCB.POSTING> = WS.POST.CODE
    CALL F.WRITE(FN.CU,KEY.LIST<I>,R.CU)
    CALL JOURNAL.UPDATE(KEY.LIST<I>)

    RETURN
*----------------------------------------
