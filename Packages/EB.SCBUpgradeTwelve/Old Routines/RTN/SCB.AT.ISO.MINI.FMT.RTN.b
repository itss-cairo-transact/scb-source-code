* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
*-----------------------------------------------------------------------------------------------------------------------*
* <Rating>-61</Rating>
*-----------------------------------------------------------------------------------------------------------------------*
    SUBROUTINE SCB.AT.ISO.MINI.FMT.RTN(REQ.NO.OF.STMTS,Y.ACCT.NO,R.ACCT,TXN.DETLS)
*-----------------------------------------------------------------------------
* Subroutine Type : PROCEDURE
* Attached to  :
* Attached as  : CALL ROUTINE
* Primary Purpose :
* This routine formats the Mini Statement Details
*-----------------------------------------------------------------------------
* Incoming : REQ.NO.OF.STMTS,Y.ACCT.NO,R.ACCT
*---------------
* Outgoing : TXN.DETLS
*---------------
* Dependencies :
*---------------
* CALLS  : AT.ISO.FMT.BAL.RTN routine
* CALLED BY : -NA-
*-----------------------------------------------------------------------------
* Developed for ATM Framework ISO 8583-87/93 message standards
* Developer: Gpack ATM
*-----------------------------------------------------------------------------
* Modification History :
*
* 24/12/2013 - Ref 866817 / Task 866826
*     Movement to Core
*
*-----------------------------------------------------------------------------
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INTRF.MAPPING
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ALTERNATE.ACCOUNT
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AT.ISO.COMMON
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    GOSUB INIT
    GOSUB PROCESS
    RETURN

*-----------------------------------------------------------------------------
INIT:
*-----------------------------------------------------------------------------
    REQ.NO.OF.STMTS=5
    Y.STMT.ID.LIST=''
    Y.STMT.ID=''
    STMT.TXT=''
    Y.SEL.STMT.PRINTED=''
    Y.SELECTED.PRINTED=''
    Y.SEL.PRINTED.ER=''
    Y.STMT.PRINTED.ID=''
    INTRF.MSG.ID = AT$INTRF.MSG.ID

    FN.STMT.ENTRY = 'F.STMT.ENTRY'
    FV.STMT.ENTRY = ''
    CALL OPF(FN.STMT.ENTRY,FV.STMT.ENTRY)

    FN.ACCT.STMT.PRINT = 'F.ACCT.STMT.PRINT'
    FV.ACCT.STMT.PRINT = ''
    CALL OPF(FN.ACCT.STMT.PRINT,FV.ACCT.STMT.PRINT)

    FN.STMT.PRINTED = 'F.STMT.PRINTED'
    FV.STMT.PRINTED = ''
    CALL OPF(FN.STMT.PRINTED,FV.STMT.PRINTED)

    FN.CURRENCY = 'F.CURRENCY'
    FV.CURRENCY=''
    CALL OPF(FN.CURRENCY,FV.CURRENCY)

    FN.TRANSACTION='F.TRANSACTION'
    FV.TRANSACTION=''
    CALL OPF(FN.TRANSACTION,FV.TRANSACTION)

    RETURN
*-----------------------------------------------------------------------------
PROCESS:
*-----------------------------------------------------------------------------
    CCY.CODE = R.ACCT<AC.CURRENCY>
    CALL F.READ(FN.CURRENCY,CCY.CODE,R.CCY,FV.CURRENCY,ER.CCY)
    NUM.CCY = R.CCY<EB.CUR.NUMERIC.CCY.CODE>
    NUM.CCY = FMT(NUM.CCY,'R%3')
    NO.DEC = R.CCY<EB.CUR.NO.OF.DECIMALS>

    GOSUB GET.STMT.IDS

    RETURN
*-----------------------------------------------------------------------------
GET.STMT.IDS:
*-----------------------------------------------------------------------------

    CALL F.READ(FN.ACCT.STMT.PRINT,Y.ACCT.NO,R.ACCT.STMT.PRINTED,FV.ACCT.STMT.PRINT,PR.ERR)
    IF NOT(R.ACCT.STMT.PRINTED) THEN
        RETURN
    END
    STMT.PRINT.CTR = 1
*Line [ 123 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    AC.STMT.CTR = DCOUNT(R.ACCT.STMT.PRINTED,@FM)

    LOOP
        WHILE(AC.STMT.CTR AND STMT.PRINT.CTR LE REQ.NO.OF.STMTS)
        Y.STMT.PRINTED.ID = R.ACCT.STMT.PRINTED<AC.STMT.CTR>
        Y.STMT.PRINTED.ID = Y.ACCT.NO:"-":FIELD(Y.STMT.PRINTED.ID,"/",1)
        Y.STMT.PRINT.REC = ""
        CALL F.READ(FN.STMT.PRINTED,Y.STMT.PRINTED.ID,Y.STMT.PRINT.REC,FV.STMT.PRINTED,Y.ERR)
*Line [ 132 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
        STMT.CTR = DCOUNT(Y.STMT.PRINT.REC,@FM)

        LOOP
            WHILE(STMT.CTR AND STMT.PRINT.CTR LE REQ.NO.OF.STMTS)
            STMT.ID = Y.STMT.PRINT.REC<STMT.CTR>
            GOSUB READ.STMT.ENTRIES
            STMT.CTR = STMT.CTR - 1
            STMT.PRINT.CTR = STMT.PRINT.CTR + 1
        REPEAT

        AC.STMT.CTR = AC.STMT.CTR - 1

    REPEAT

    IF INTRF.MSG.ID EQ '5004' THEN
        STMT.TXT = FMT(STMT.TXT,"L#400")
        TXN.DETLS = LOWER(STMT.TXT)
    END ELSE
        TXN.DETLS = LOWER(STMT.TXT)
    END

*Line [ 154 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.OF.TXNS = FMT(DCOUNT(TXN.DETLS,@VM),"R%2")

*Line [ 157 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CHANGE @VM TO '' IN TXN.DETLS
    TXN.DETLS=REQ.NO.OF.STMTS:TXN.DETLS
    TXN.DETLS.LEN=LEN(TXN.DETLS)
    TXN.DETLS=FMT(TXN.DETLS,'L#372')
    TXN.DETLS.LEN=LEN(TXN.DETLS)
    IF AT$AT.INTRF.MAPPING THEN
        FldRtn = AT$AT.INTRF.MAPPING<IntrfMapping_PreRtn,1>
        CompFlag= ''
        CALL CHECK.ROUTINE.EXIST(FldRtn,CompFlag,RetInfo)
    END
    IF CompFlag EQ '1' THEN
        CALL @FldRtn(Y.ACCT.NO,R.ACCT,BalAftTxn)
    END
**  CALL AT.ISO.FMT.BAL.RTN(Y.ACCT.NO,R.ACCT,BAL.AFT.TXN)
*  TXN.DETLS.DETS = TXN.DETLS:',':"BAL.AFT.TXN:1:1=":BalAftTxn

    RETURN
*-----------------------------------------------------------------------------
READ.STMT.ENTRIES:
*-----------------------------------------------------------------------------

    CALL F.READ(FN.STMT.ENTRY,STMT.ID,Y.STMT.REC,FV.STMT.ENTRY,Y.ERR.STMT.ENTRY)
    IF R.ACCT<AC.CURRENCY> EQ LCCY THEN
        Y.TXN.AMT = Y.STMT.REC<AC.STE.AMOUNT.LCY>
    END ELSE
        Y.TXN.AMT = Y.STMT.REC<AC.STE.AMOUNT.FCY>
    END
    Y.DT.TXN = Y.STMT.REC<AC.STE.BOOKING.DATE>
    Y.TXN.CODE = Y.STMT.REC<AC.STE.TRANSACTION.CODE>
    Y.TIME.TXN = Y.STMT.REC<AC.STE.DATE.TIME,1>
    TRANSACTION.REC = ''
    TRAN.READ.ERR = ''
    CALL F.READ(FN.TRANSACTION, Y.TXN.CODE, TRANSACTION.REC, FV.TRANSACTION, TRAN.READ.ERR)
    Y.TRAN.DESC = TRANSACTION.REC<AC.TRA.NARRATIVE>

    IF INTRF.MSG.ID EQ '5004' THEN
        GOSUB GET.PHX.ENTRIES
    END ELSE
        GOSUB GET.ATM.ENTRIES
    END

    RETURN
*-----------------------------------------------------------------------------
GET.ATM.ENTRIES:
*-----------------------------------------------------------------------------
    IF INDEX(Y.TXN.AMT,'-',1) THEN
        Y.TXN.AMT = FIELD(Y.TXN.AMT,'-',2)
        Y.SIGN = "D"
    END ELSE        ;*PACS00227453
        Y.SIGN = "C"
    END

    Y.TXN.AMT1 = FIELD(Y.TXN.AMT,'.',1)
    Y.TXN.AMT2 = FIELD(Y.TXN.AMT,'.',2)
    Y.TXN.AMT2 = FMT(Y.TXN.AMT2,'L%':NO.DEC)

    Y.TXN.AMT = Y.TXN.AMT1:Y.TXN.AMT2
    Y.TXN.AMT = FMT(Y.TXN.AMT,'R%12')

    Y.DT.TXN.LEN=LEN(Y.DT.TXN)
    Y.DT.TXN=Y.DT.TXN[3,Y.DT.TXN.LEN]

    Y.TRAN.DESC = Y.TRAN.DESC[1,10]
    Y.TRAN.DESC = FMT(Y.TRAN.DESC,"10' 'L")
    Y.TIME.TXN = Y.TIME.TXN[7,4]

*ONE.LINE = Y.DT.TXN:NUM.CCY:Y.SIGN:Y.TXN.AMT
    ONE.LINE = Y.DT.TXN:Y.SIGN:Y.TRAN.DESC:Y.TXN.AMT
    STMT.TXT<-1> = ONE.LINE

    RETURN          ;*From READ.STMT.ENTRIES
*-----------------------------------------------------------------------------
GET.PHX.ENTRIES:
*-----------------------------------------------------------------------------

    STMT.DT = Y.DT.TXN[7,2]:'-':Y.DT.TXN[5,2]:'-':Y.DT.TXN[3,2]

    Y.TRANS.DESC = FMT(UPCASE(Y.TRAN.DESC),"L#16")

    IF INDEX(Y.TXN.AMT,'-',1) THEN
        Y.TXN.AMT = FIELD(Y.TXN.AMT,'-',2)
        Y.SIGN = " DB"
        IF LEN(Y.TXN.AMT) GT 16 THEN
            Y.TXN.AMT = Y.TXN.AMT[4,16]:Y.SIGN
        END ELSE
            Y.TXN.AMT = Y.TXN.AMT:Y.SIGN
            Y.TXN.AMT = FMT(Y.TXN.AMT,"R#16")
        END

    END ELSE
        Y.TXN.AMT = FMT(Y.TXN.AMT,"R#16")
    END

    ONE.LINE = STMT.DT:Y.TRANS.DESC:Y.TXN.AMT
    STMT.TXT<-1> = ONE.LINE

    RETURN

END




















