* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBR.MECH.SAL.FT.AUTH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*---------------------------------------
************ OPEN FILES ****************

    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT  = ""
    CALL OPF (FN.FT,F.FT)

    FN.FTN = "FBNK.FUNDS.TRANSFER$NAU"  ; F.FTN  = ""
    CALL OPF (FN.FTN,F.FTN)

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION = "MECH1"

    WS.FILE.COMP = ID.COMPANY[2]
    COMP = ID.COMPANY

    OPENSEQ "MECH" , "MECH.SAL.OUT.A":WS.FILE.COMP TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"MECH.SAL.OUT.A":WS.FILE.COMP
        HUSH OFF
    END
    OPENSEQ "MECH" , "MECH.SAL.OUT.A":WS.FILE.COMP TO BB ELSE
        CREATE BB THEN
        END ELSE
            STOP 'Cannot create MECH.SAL.OUT.A File IN MECH'
        END
    END
    WS.AMT = 0

*--------------------------------------------------------------------

******* CHECK COMPANY ACCOUNT *****
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH TRANSACTION.TYPE EQ AC48 AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FTN,KEY.LIST<I>,R.FTN,F.FTN,E4)
            COMP.ACCT = R.FTN<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
            WS.AMT   += R.FTN<FT.DEBIT.AMOUNT>
        NEXT I
    END

    TEXT = '��� ���� ������ : ' : COMP.ACCT ; CALL REM

    CALL F.READ(FN.AC,COMP.ACCT,R.AC,F.AC,E9)
    IF NOT(E9) THEN
        WS.ACCT.NAME = R.AC<AC.ACCOUNT.TITLE.1>
        TEXT = '���� ������ ���� : ' : WS.ACCT.NAME ; CALL REM
        TEXT = '������ ������ ������� : ' : WS.AMT ; CALL REM

        YTEXT = "Y/N �� ���� ���������"
        CALL TXTINP(YTEXT, 8, 22, "1", "A")
        FLG = COMI

        IF FLG = 'Y' OR FLG = 'y' THEN
            GO TO PROCESS
        END ELSE
            TEXT = '�� ������ �� ��������' ; CALL REM
            GO TO EXIT.PROG
        END

    END ELSE
        TEXT = '��� ���� ������' ; CALL REM
        TEXT = '�� ������ �� ��������' ; CALL REM
        GO TO EXIT.PROG

    END

*--------------------------------------------------------------------
PROCESS:

    T.SEL1 = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH TRANSACTION.TYPE EQ AC48 AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR NN = 1 TO SELECTED1
            CALL F.READ(FN.FT,KEY.LIST1<NN>,R.FT,F.FT,E4)

            SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/A/PROCESS,":",":KEY.LIST1<NN>:","

            OFS.MESSAGE.DATA = ""

            SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END

**  CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END
        NEXT NN
        TEXT = '�� �������' ; CALL REM
    END

    IF NOT(SELECTED1) THEN
        TEXT = '������� �� �� ���' ; CALL REM
    END
*--------------------------------------------------------------------
EXIT.PROG:

    RETURN
************************************************************
END
