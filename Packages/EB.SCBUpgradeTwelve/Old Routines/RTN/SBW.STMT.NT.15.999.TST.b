* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE
    SUBROUTINE SBW.STMT.NT.15.999.TST(VAR.DAT,VAR.COMP)
**    PROGRAM SBW.STMT.NT.15.999.TST(VAR.DAT,VAR.COMP)

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.LEGAL.FORM
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-----------------------------------------------------------------------------

    GOSUB INIT

***    WS.COMP = ID.COMPANY
***    WS.COMP = 'EG0010032'
    WS.COMP = VAR.COMP


*-----------------------------------------------------------------------------
    REPORT.ID = 'SBW.STMT.NT.15.999'
    CALL PRINTER.ON(REPORT.ID,'')

    GOSUB PRINT.HEAD

    GOSUB READ.CUSTOMER.FILE

*-----------------------------------------------------------------------------
    PR.LINE = STR('_',132)
    PRINT PR.LINE

    XX = SPACE(132)
    XX<1,1>[10,35]   = '����������� ������� ������� :  '
    XX<1,1>[50,20]   = NO.CUST
    PRINT XX<1,1>
    XX = SPACE(132)
    PRINT XX<1,1>


    XX<1,1>[50,40]   = "*****  �������������  ����������   *****"
    PRINT XX<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')



    RETURN
*----------------------------------------------------------------------------
READ.CUSTOMER.FILE:


    SEL.CUST = "SELECT ":FN.CUST:" WITH"
    SEL.CUST :=" COMPANY.BOOK EQ ":WS.COMP
    SEL.CUST :=" AND NEW.SECTOR EQ 4650"
    SEL.CUST :=" BY @ID"


    CALL EB.READLIST(SEL.CUST,CUST.LIST,'',NO.OF.CUST,ERR.CUST)

    LOOP
        REMOVE Y.CUST.ID FROM CUST.LIST SETTING CUST.POS
    WHILE Y.CUST.ID:CUST.POS
        CALL F.READ(FN.CUST,Y.CUST.ID,R.CUSTOMER,F.CUST,ERR.CUST.ERR)


        Y.SEC.ID  = R.CUSTOMER<EB.CUS.SECTOR>
        Y.IND.ID  = R.CUSTOMER<EB.CUS.INDUSTRY>

        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)

        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
        Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>

        Y.CUS.ACC.ID = Y.CUST.ID

        GOSUB READ.CUSTOMER.ACCOUNT.FILE



    REPEAT

    RETURN
*----------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT.FILE:

    DR.AMT    = 0
    CR.AMT    = 0
    T.DR.AMT  = 0
    T.CR.AMT  = 0
    WS.DR.AMT = 0
    WS.CR.AMT = 0



    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)

    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)

*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>

*---------------

        GOSUB READ.STMT.ENTRY

*---------------
    NEXT Z

****        PRINT "OK"

    GOSUB   SELECT.THE.CUST


    RETURN
*----------------------------------------------------------------------------
READ.STMT.ENTRY:
    SEL.LIST = ''
    CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,STR.DATE,END.DATE,SEL.LIST,OPENING.BAL,ER)

*--------------------------------------

    LOOP
        REMOVE REC.ID FROM SEL.LIST SETTING POS.CUST
    WHILE REC.ID:POS.CUST
        CALL F.READ(FN.STMT,REC.ID,R.STMT,F.STMT,ERR.STMT)

****************
        SS.ID = R.STMT<AC.STE.SYSTEM.ID>
        RR.ST = R.STMT<AC.STE.RECORD.STATUS>
****************
        IF SS.ID EQ 'LD' THEN
            GOTO NEXT.STMT
        END
        IF RR.ST EQ 'REVE' THEN
            GOTO NEXT.STMT
        END
****************

        CUST.ID = R.STMT<AC.STE.CUSTOMER.ID>
        DR.AMT  = 0
        CR.AMT  = 0
        AMT = R.STMT<AC.STE.AMOUNT.LCY>
****************
*Line [ 184 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.STMT<AC.STE.AMOUNT.LCY>,@VM)
        FOR X = 1 TO YY

            AMT = R.STMT<AC.STE.AMOUNT.LCY,1,X>
            IF    AMT LT 0    THEN
                DR.AMT  = DR.AMT + AMT
            END
            IF    AMT GT 0    THEN
                CR.AMT  = CR.AMT + AMT
            END

        NEXT X
****************

        T.DR.AMT   += DR.AMT
        T.CR.AMT   += CR.AMT

NEXT.STMT:

    REPEAT


    RETURN
*-------------------------------------------------------------------------
SELECT.THE.CUST:

    WS.DR.AMT = INT(T.DR.AMT)
    WS.CR.AMT = INT(T.CR.AMT)


    IF WS.DR.AMT GE LOW.AMT  THEN
        IF WS.DR.AMT LT HIGH.AMT THEN
            GOSUB PRINT.ONE.LINE
        END
    END


    IF WS.CR.AMT GE LOW.AMT  THEN
        IF WS.CR.AMT LT HIGH.AMT THEN
            GOSUB PRINT.ONE.LINE
        END
    END

    RETURN
*-------------------------------------------------------------------------
PRINT.ONE.LINE:


    CALL F.READ(FN.SEC,Y.SEC.ID,R.SECTOR,F.SEC,ERR.SEC)
    SEC.NAME = R.SECTOR<EB.SEC.DESCRIPTION,2>


    CALL F.READ(FN.IND,Y.IND.ID,R.INDUSTRY,F.IND,ERR.IND)
    IND.NAME = R.INDUSTRY<EB.IND.DESCRIPTION,2>

    CALL F.READ(FN.LGL,Y.LGL.ID,R.LEGAL,F.LGL,ERR.LGL)
    LGL.NAME = R.LEGAL<LEG.DESCRIPTION,2>
*---------------
    GOSUB WRITE.LINE.01

*---------------
    RETURN

*-----------------------------------------------------------------------
INIT:
    PGM.NAME = "SBW.STMT.NT.15.999"
    SAVE.CUST  = 0
*------------------------
***    CUST.TYPE = "�����������"
    CUST.TYPE = "���������"
    NO.CUST = 0
*------------------------
*    SYS.DATE   = TODAY
*    TODAY =  VAR.DAT

*  ============================================
    CAL.DAYS = 0
    C.STR.DATE = 20100101
**    CALL CDD('C',C.STR.DATE,TODAY,CAL.DAYS)
    CALL CDD('C',C.STR.DATE,VAR.DAT,CAL.DAYS)
    NET.DAYS = INT(CAL.DAYS / 7)
    NET.DAYS = (NET.DAYS * 7) - 1
    CALL CDT('', C.STR.DATE, "+":NET.DAYS:"C")
    SYS.DATE = C.STR.DATE
*  ============================================

*----------------------------------------------------------------------
*------------------------
    STR.DATE = SYS.DATE
    CALL CDT('', STR.DATE, "-6C")
    END.DATE =  SYS.DATE
*------------------------
*    P.DATE   = FMT(TODAY,"####/##/##")
    P.DATE   = FMT(VAR.DAT,"####/##/##")
    P.STR.DATE   = FMT(STR.DATE,"####/##/##")
    P.END.DATE   = FMT(END.DATE,"####/##/##")


*PRINT P.DATE
*PRINT P.STR.DATE
*PRINT P.END.DATE
*------------------------
    LOW.AMT  =  150000
    HIGH.AMT =  99999999999

    P.LOW.AMT = FMT(LOW.AMT,"15L,")
    P.HIGH.AMT = FMT(HIGH.AMT,"15L,")

*------------------------
    FN.STMT = "FBNK.STMT.ENTRY"
    F.STMT = ''
    R.STMT=''
    Y.STMT=''
    Y.STMT.ERR=''
*------------------------
    CALL OPF(FN.STMT,F.STMT)
*------------------------
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUSTOMER = ""
    Y.CUST.ID   = ""
*-------------------------------
    CALL OPF(FN.CUST,F.CUST)
*-------------------------------
    FN.SEC = "FBNK.SECTOR"
    F.SEC  = ""
    R.SECTOR = ""
    Y.SEC.ID   = ""
*-------------------------------
    CALL OPF(FN.SEC,F.SEC)
*-------------------------------
    FN.LGL = "F.SCB.CUS.LEGAL.FORM"
    F.LGL  = ""
    R.LEGAL = ""
    Y.LGL.ID   = ""
*-------------------------------
    CALL OPF(FN.LGL,F.LGL)
*-------------------------------
    FN.IND = "FBNK.INDUSTRY"
    F.IND  = ""
    R.INDUSTRY = ""
    Y.IND.ID   = ""
*-------------------------------
    CALL OPF(FN.IND,F.IND)
*-------------------------------
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUST  = ""
    Y.CUST.ID   = ""
*-------------------------------
    CALL OPF(FN.CUST,F.CUST)
*-------------------------------
    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC  = ""
    R.CUS.ACC = ""
    Y.CUS.ACC.ID   = ""
*-------------------------------
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
*-------------------------------


    T.DR.AMT   = 0
    T.CR.AMT   = 0





    RETURN
*------------------------------------------------------------------------
WRITE.LINE.01:

    XX = SPACE(132)
    XX<1,1>[1,9]     = Y.CUST.ID
    XX<1,1>[10,35]   = CUST.NAME
*    XX<1,1>[50,20]   = SEC.NAME
*    XX<1,1>[50,20]   = LGL.NAME
    XX<1,1>[50,20]   = IND.NAME

    P.DR.AMT  = FMT(T.DR.AMT,"15L2,")
    P.CR.AMT  = FMT(T.CR.AMT,"15L2,")
    T.NT.AMT  = T.CR.AMT + T.DR.AMT
    P.NT.AMT  = FMT(T.NT.AMT,"15L2,")

    XX<1,1>[75,15]   = P.DR.AMT
    XX<1,1>[95,15]  = P.CR.AMT
    XX<1,1>[115,15]  = P.NT.AMT
    PRINT XX<1,1>

    NO.CUST = NO.CUST + 1

*    PR.LINE = STR('_',132)
*    PRINT PR.LINE

    NO.OF.LINE = NO.OF.LINE + 1
    IF NO.OF.LINE GT 39  THEN
        GOSUB PRINT.HEAD
    END

    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
    YYBRN = BRANCH


    T.DAY = P.DATE
    PR.HD ="'L'":SPACE(1):"��� ���� ������ "  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"������� : ":P.DATE:SPACE(86):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(11):SPACE(100):PGM.NAME
    PR.HD :="'L'":SPACE(50):"������ ������ ������� �� ������� ���� ��� �������"
    PR.HD :="'L'":SPACE(55):"������� �� �������":" ":CUST.TYPE
    PR.HD :="'L'":SPACE(50):"�� ":P.LOW.AMT:" �� ":"  ��� ��� �� ":P.HIGH.AMT:" �� "
    PR.HD :="'L'":SPACE(57):"�� �� ������ �� ������� ��������"
    PR.HD :="'L'":SPACE(55):"������ �� ":P.STR.DATE:"   ��� ":P.END.DATE

    PR.HD :="'L'":SPACE(50):STR('_',50)
    PR.HD :="'L'":" "
***********************************************************
    PR.HD :="'L'":SPACE(1)
    PR.HD :="'L'":SPACE(0):" ��� ������" :SPACE(11):" ��� ������ " :SPACE(15):"����� ��������":SPACE(11):"������ ���������":SPACE(4):"������ ����������"
    PR.HD :="'L'":SPACE(74):"�������             �������":SPACE(13):"������"
    PR.HD :="'L'":STR('_',132)
***********************
    HEADING PR.HD
    NO.OF.LINE = 0
    RETURN

END
