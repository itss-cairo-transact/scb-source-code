* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwleve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwleve
*DONE

*-----------------------------------------------------------------------------
* <Rating>1239</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SCB.ACT.ENT.STMT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_SCREEN.VARIABLES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ENQUIRY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ENTRY.STMT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.REPORT.CONTROL

*************************************************************************

* Call $RUN Routine and Exit if running as Phantom *

    IF  PHNO THEN
REM  >  CALL  TEMPLATE.W$RUN
        GOTO  V$EXIT
    END

*************************************************************************

    GOSUB  DEFINE.PARAMETERS

    IF  LEN(V$FUNCTION)  GT  1 THEN
        GOTO  V$EXIT
    END

    CALL  MATRIX.UPDATE

REM  >  GOSUB  INITIALISE                  ;*  Special  Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL  RECORDID.INPUT

    UNTIL  MESSAGE =  'RET'  DO

        V$ERROR  =  ''

        IF  MESSAGE =  'NEW  FUNCTION' THEN

            GOSUB  CHECK.FUNCTION       ;*  Special  Editing of  Function

            IF  V$FUNCTION  EQ 'L'  THEN
                CALL  FUNCTION.DISPLAY
                V$FUNCTION  =  ''
            END
            IF  V$FUNCTION  EQ 'V'  THEN
                FILE.TYPE  =  "I"
            END

        END  ELSE

            GOSUB  CHECK.ID   ;*  Special  Editing of  ID
            IF  V$ERROR  THEN GOTO  MAIN.REPEAT

            CALL  RECORD.READ

            IF  MESSAGE =  'REPEAT'  THEN
                GOTO  MAIN.REPEAT
            END

            CALL  MATRIX.ALTER

REM  >  GOSUB  CHECK.RECORD;*  Special  Editing of  Record
REM  >  IF  ERROR THEN  GOTO MAIN.REPEAT

REM  >  GOSUB  PROCESS.DISPLAY;*  For  Display applications

            LOOP
                GOSUB  PROCESS.FIELDS   ;*  )  For  Input
                GOSUB  PROCESS.MESSAGE  ;*  )  Applications
            WHILE  MESSAGE =  'ERROR'  DO REPEAT

        END

MAIN.REPEAT:
    REPEAT

    V$EXIT:
    RETURN          ;*  From  main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP

        IF  SCREEN.MODE  EQ 'MULTI'  THEN
            IF  FILE.TYPE  EQ 'I'  THEN
                CALL  FIELD.MULTI.INPUT
            END  ELSE
                CALL  FIELD.MULTI.DISPLAY
            END
        END  ELSE
            IF  FILE.TYPE  EQ 'I'  THEN
                CALL  FIELD.INPUT
            END  ELSE
                CALL  FIELD.DISPLAY
            END
        END

    UNTIL  MESSAGE <>  ""  DO

        GOSUB  CHECK.FIELDS   ;*  Special  Field Editing

        IF  T.SEQU  NE ''  THEN T.SEQU<-1>  =  A  +  1

    REPEAT

    RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF  MESSAGE =  'VAL'  OR MESSAGE  EQ 'VER'  THEN
        MESSAGE  =  ''
        BEGIN  CASE
        CASE  V$FUNCTION  EQ 'R'
            END.ERROR  =  ""
REM  >  GOSUB  CHECK.REVERSAL;*  Special  Reversal checks
        CASE  V$FUNCTION  EQ 'V'
            GOSUB  CHECK.VERIFY         ;*  Special  Verify checks
            GOSUB  CHECK.CHANGES        ;*  look  for changed  field
            IF  YWRITE EQ  0 AND  NOT(END.ERROR)  THEN
                GOSUB  CALL.$RUN        ;*  update  using $RUN
                GOTO  PROCESS.MESSAGE.EXIT        ;*  and  return to  Id input
            END
        CASE  OTHERWISE
            GOSUB  AUTH.CROSS.VALIDATION          ;*  Special  Cross Validation
        END  CASE

        IF  END.ERROR  THEN
            GOSUB  POINT.TO.ERROR       ;*  position  on error  field
            GOTO  PROCESS.MESSAGE.EXIT  ;*  return  to field  input
        END

REM  >  IF  NOT(ERROR)  THEN
REM  >  GOSUB  BEFORE.AUTH.WRITE;*  Special  Processing before  write
REM  >  END

        IF  NOT(V$ERROR)  THEN
            CALL  AUTH.RECORD.WRITE

            IF  MESSAGE <>  "ERROR"  THEN
REM  >  GOSUB  AFTER.AUTH.WRITE;*  Special  Processing after  write

                IF  V$FUNCTION  EQ 'V'  THEN
                    GOSUB  CALL.$RUN
                END

            END

        END

    END

PROCESS.MESSAGE.EXIT:

    RETURN

*************************************************************************

PROCESS.DISPLAY:

* Display the record fields.

    IF  SCREEN.MODE  EQ 'MULTI'  THEN
        CALL  FIELD.MULTI.DISPLAY
    END  ELSE
        CALL  FIELD.DISPLAY
    END

    RETURN

*************************************************************************

CHECK.CHANGES:

    YWRITE  =  0
    IF  END.ERROR  EQ ''  THEN
        A  =  1
        LOOP  UNTIL A  GT V  OR YWRITE  DO
            IF  R.NEW(A)  NE  R.OLD(A)  THEN  YWRITE =  1  ELSE A  +=  1
        REPEAT
    END

    RETURN

*************************************************************************

POINT.TO.ERROR:

    IF  END.ERROR  EQ 'Y'  THEN
        P  =  0  ;  A  =  1
        LOOP  UNTIL T.ETEXT<A>  NE  '' DO
            A  +=  1
        REPEAT
        T.SEQU  =  A
        IF  SCREEN.MODE  EQ 'SINGLE'  THEN
            PRINT  S.BELL:
            IF  INPUT.BUFFER[1,LEN(C.F)]  EQ  C.F  THEN
                INPUT.BUFFER  =  INPUT.BUFFER[LEN(C.F)  +  2,99]
*  cancel  C.F  function after  C.W  usage
*  (+2  for space  separator)
            END
        END  ELSE
            E  =  T.ETEXT<A>
            CALL  ERR
        END
    END  ELSE
        T.SEQU  =  'ACTION'  ;  E  =  END.ERROR  ;  L  =  22
        CALL  ERR
    END

    PRINT  @(63,21):  S.CLEAR.EOL:
    VAL.TEXT  =  ""
    MESSAGE  =  'ERROR'

    RETURN

*************************************************************************

    CALL.$RUN:

* Process the 'Work' file using the $Run Routine *

    V$FUNCTION  =  FUNCTION.SAVE

    CALL  ACCT.ENTRY.STMT$RUN

    IF  V$FUNCTION  EQ 'B'  THEN
        V$FUNCTION  =  'V'
    END

    RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

    PRINT  @(0,L1ST-3):S.CLEAR.EOL:     ;*  clear  2nd line  (may  be occupied
*by  output text)

    RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.


    RETURN

*************************************************************************

CHECK.FIELDS:

    BEGIN  CASE

    CASE  AF =  AES.END.DATE
        IF  COMI =  ""  THEN
            COMI  =  TODAY
        END
    END  CASE

    RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:

* The check to ensures the presence of the date and  to make
* sure that END.DATE > START.DATE

    IF  R.NEW(AES.START.DATE)  >TODAY  THEN
        ETEXT  =  'DATE  MUST NOT  BE GREATER  THAN TODAY'
        AF  =  AES.START.DATE
        CALL  STORE.END.ERROR
    END

    IF  R.NEW(AES.END.DATE)  <  R.NEW(AES.START.DATE)  THEN
        ETEXT  =  "END  DATE must  not be  less than  START DATE"
        AF  =  AES.END.DATE
        CALL  STORE.END.ERROR
    END

    DATA.FILE.NAME  =  R.NEW(AES.SELECT.FILE)
*
    ENQ.SELECTION  =  ''
    ENQ.SELECTION<2>  =  R.NEW(AES.SELECTION.FIELD)
    ENQ.SELECTION<3>  =  R.NEW(AES.OPERAND)
    ENQ.SELECTION<4>  =  R.NEW(AES.SELECTION)
    ENQ.SELECTION<9>  =  R.NEW(AES.SORT.FIELD)
*
    CALL  ENQ.VALIDATE.SELECTION
**
    FIELD.MAP  =  ""          ;*  Map  arguments to  fields
    FIELD.MAP<2>  =  AES.SELECTION.FIELD
    FIELD.MAP<3>  =  AES.OPERAND
    FIELD.MAP<4>  =  AES.SELECTION
    FIELD.MAP<9>  =  AES.SORT.FIELD
*
    GOSUB  VALIDATE.ERROR
*

    DATA.FILE.NAME  =''
*
    ENQ.SELECTION  =  R.NEW(AES.FORMAT)
    ENQ.SELECTION<2>  =  R.NEW(AES.ENQ.SEL.FIELD)
    ENQ.SELECTION<3>  =  R.NEW(AES.ENQ.OPERAND)
    ENQ.SELECTION<4>  =  R.NEW(AES.ENQ.SEL)
    ENQ.SELECTION<9>  =  R.NEW(AES.ENQ.SORT)
*
    CALL  ENQ.VALIDATE.SELECTION
*
    FIELD.MAP  =  ""          ;*  Map  arguments to  fields
    FIELD.MAP<1>  =  AES.FORMAT
    FIELD.MAP<2>  =  AES.ENQ.SEL.FIELD
    FIELD.MAP<3>  =  AES.ENQ.OPERAND
    FIELD.MAP<4>  =  AES.ENQ.SEL
    FIELD.MAP<9>  =  AES.ENQ.SORT
*
    GOSUB  VALIDATE.ERROR
*
    RETURN

*************************************************************************

VALIDATE.ERROR:

    IF  ENQ.ERROR  THEN
        I  =  0
        LOOP  I+=1  UNTIL ENQ.ERROR<1,I>  =  ""
            ARG.NUMBER  =  ENQ.ERROR<1,I>[".",1,1]          ;*  Argument  number
            AF  =  FIELD.MAP<ARG.NUMBER>          ;*  Correct  field number
            AV  =  ENQ.ERROR<1,I>[".",2,1]        ;*  Value  number
            ETEXT  =  ENQ.ERROR<2,I>    ;*  Error
            CALL  STORE.END.ERROR
        REPEAT
    END
    RETURN

*************************************************************************

CHECK.REVERSAL:


    RETURN

*************************************************************************

CHECK.VERIFY:

    GOSUB  AUTH.CROSS.VALIDATION
    RETURN

*************************************************************************

AFTER.AUTH.WRITE:


    RETURN

*************************************************************************

BEFORE.AUTH.WRITE:


    RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    FUNCTION.SAVE  =  V$FUNCTION
    IF  V$FUNCTION  EQ "B"  THEN
        V$FUNCTION  =  "V"
        ID.ALL  =  ""
    END
    IF  INDEX('ADEFHQ',V$FUNCTION,1)  THEN
        E  =  'FUNCTION  NOT ALLOWED  FOR THIS  APPLICATION'
        CALL  ERR
        V$FUNCTION  =  ''
    END

    RETURN

*************************************************************************

INITIALISE:


    RETURN

*************************************************************************

DEFINE.PARAMETERS:  *  SEE  'I_RULES'  FOR DESCRIPTIONS  *


    MAT  F =  ""  ;  MAT  N =  ""  ;  MAT  T =  ""
    MAT  CHECKFILE =  ""  ;  MAT  CONCATFILE =  ""
    ID.CHECKFILE  =  ""  ;  ID.CONCATFILE  =  ""

    ID.F  =  "KEY"  ;  ID.N  =  "15"  ;  ID.T  =  "A"
    Z  =  0
    Z  +=  1  ;  F(Z)  =  "XX.LL.DESCRIPTION"  ;  N(Z)  =  "35.3"  ;  T(Z)  =  "A"
*Line [ 460 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    Z  +=  1  ;  F(Z)  =  'SELECT.FILE'  ;  N(Z)  =  '35.1'  ;  T(Z)  =  @FM:'ACCOUNT_ACCOUNT$HIS'
    Z  +=  1  ;  F(Z)  =  'XX<SELECTION.FIELD'  ;  N(Z)  =  '50'  ;  T(Z)  =  'ANY'
    Z  +=  1  ;  F(Z)  =  'XX.OPERAND'  ;  N(Z)  =  '30'  ;  T(Z)  =  'A'
*Line [ 464 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    T(Z)  =  @FM:'EQ_RG_LT_GT_NE_GE_LE_LK_UL_NR'
    Z  +=  1  ;  F(Z)  =  "XX>SELECTION"  ;  N(Z)  =  "45"  ;  T(Z)  =  'ANY'
    Z  +=  1  ;  F(Z)  =  'XX.SORT.FIELD'  ;  N(Z)  =  '23'  ;  T(Z)  =  'ANY'
    Z  +=  1  ;  F(Z)  =  "START.DATE"  ;  N(Z)  =  "10.1"  ;  T(Z)  =  "D"
    Z  +=  1  ;  F(Z)  =  "END.DATE"  ;  N(Z)  =  "10..C"  ;  T(Z)  =  "D"
    Z+=1  ;  F(Z)="FORMAT"  ;  N(Z)="32.1"  ;  T(Z)="A"
    CHECKFILE(Z)="ENQUIRY":@FM:ENQ.FILE.NAME
    Z  +=  1  ;  F(Z)  =  'XX<ENQ.SEL.FIELD'  ;  N(Z)  =  '50'  ;  T(Z)  =  'ANY'
    Z  +=  1  ;  F(Z)  =  'XX.ENQ.OPERAND'  ;  N(Z)  =  '30'
*Line [ 474 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    T(Z)  =  @FM:'EQ_RG_LT_GT_NE_GE_LE_LK_UL_NR'
    Z  +=  1  ;  F(Z)  =  "XX>ENQ.SEL"  ;  N(Z)  =  "45"  ;  T(Z)  =  'ANY'
    Z  +=  1  ;  F(Z)  =  'XX.ENQ.SORT'  ;  N(Z)  =  '23'  ;  T(Z)  =  'ANY'
    Z  +=  1  ;  F(Z)  =  'REPORT.CONTROL.ID'  ;  N(Z)  =  '35.1'  ;  T(Z)  =  'A'
    CHECKFILE(Z)="REPORT.CONTROL":@FM:RCF.SHORT.DESC
*Line [ 480 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    Z  +=  1  ;  F(Z)  =  "STANDARD.HEADING"  ;  N(Z)  =  '3'  ;  T(Z)  =  @FM:'80_132'
    Z  +=  1  ;  F(Z)  =  "XX.REPORT.HEADER"  ;  N(Z)  =  '42'  ;  T(Z)  =  'A'
*Line [ 483 ] Add @FM Instead Of FM - ITSS - R21 Upgrade - 2021-12-26
    Z  +=  1  ;  F(Z)  =  "PAGE.BREAK"  ;  N(Z)  =  '2'  ;  T(Z)  =  @FM:'Y_NO'
    Z+=1  ;  F(Z)="RESERVED.6"  ;  N(Z)="1"  ;  T(Z)<3>="NOINPUT"
    Z+=1  ;  F(Z)="RESERVED.5"  ;  N(Z)="1"  ;  T(Z)<3>="NOINPUT"
    Z+=1  ;  F(Z)="RESERVED.4"  ;  N(Z)="1"  ;  T(Z)<3>="NOINPUT"
    Z+=1  ;  F(Z)="RESERVED.3"  ;  N(Z)="1"  ;  T(Z)<3>="NOINPUT"
    Z+=1  ;  F(Z)="RESERVED.2"  ;  N(Z)="1"  ;  T(Z)<3>="NOINPUT"
    Z+=1  ;  F(Z)="RESERVED.1"  ;  N(Z)="1"  ;  T(Z)<3>="NOINPUT"
    V  =  Z  +9

    RETURN

*************************************************************************

END
