* @ValidationCode : MjotOTk3MzQ2ODpDcDEyNTI6MTY0MjMyNDM4OTI0Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Jan 2022 11:13:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
SUBROUTINE SCB.ATM.CHG(INCOMING,OUTGOING)

    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_AT.ISO.COMMON
    $INSERT I_F.ATM.PARAMETER
    $INSERT I_F.ACCOUNT
    $INSERT I_F.FT.CHARGE.TYPE
    $INSERT I_F.AC.CHARGE.REQUEST
    $INSERT I_F.ALTERNATE.ACCOUNT
*-------------------------------------------------------------------------------------------------------------
    DE.48          = AT$INCOMING.ISO.REQ(48)
    DE.39          = AT$AT.ISO.RESP.CODE
    IF RIGHT(DE.48,4) NE 'OnUs' AND AT$AT.ISO.RESP.CODE EQ "00" THEN
        GOSUB INITIAL
        GOSUB PROCESS
    END ELSE
        OUTGOING       = AT$AT.ISO.RESP.CODE
    END
RETURN
*-------------------------------------------------------------------------------------------------------------
INITIAL:
*--------

    FN.ATM.PARAM = 'F.ATM.PARAMETER' ; F.ATM.PARAM = ''
    CALL OPF(FN.ATM.PARAM,F.ATM.PARAM)

    FN.ACCT = 'F.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

    FN.ALT.ACCT = 'F.ALTERNATE.ACCOUNT' ; F.ALT.ACCT = ''
    CALL OPF(FN.ALT.ACCT,F.ALT.ACCT)

    FN.FT.CHG.TYPE = 'F.FT.CHARGE.TYPE' ; F.FT.CHG.TYPE = ''
    CALL OPF(FN.FT.CHG.TYPE,F.FT.CHG.TYPE)


    ISO.BANK.IMD   = AT$BIN.NO
    ISO.ATM.ID     = AT$INCOMING.ISO.REQ(41)
    ISO.ATM.ID     = TRIM(ISO.ATM.ID, "", "D")
    RESPONSE.CODE  = AT$AT.ISO.RESP.CODE
    DE.48          = AT$INCOMING.ISO.REQ(48)
    CR.CCY         = AT$INCOMING.ISO.REQ(49)
    CUS.ACCT       = AT$INCOMING.ISO.REQ(102)
    CUS.ACCT       = TRIM(CUS.ACCT, " ","D")
    DE.37          = AT$INCOMING.ISO.REQ(37)
    DE.37          = TRIM(DE.37, "","D")
    CR.ACCT        = 'EGP1103800010099'
    R.ATM.ID       = ''
    ATM.ID.ERR     = ''
    ATM.RENT       = ''

    ATM.PARAM.ERR = ''
    R.ATM.PARAM   = ''

    CHKVAL         = ''

    CALL F.READ (FN.ATM.PARAM, 'SYSTEM', R.ATM.PARAM, F.ATM.PARAM, ATM.PARAM.ERR)
    BANK.IMD      = R.ATM.PARAM<ATM.PARA.BANK.IMD>

    CALL F.READ(FN.ACCT, CUS.ACCT, R.ACCT, F.ACCT, ACCT.ERR)
    IF ACCT.ERR THEN
        CALL F.READ(FN.ALT.ACCT, CUS.ACCT, R.ALT.ACCT, F.ALT.ACCT, ACCT.ERR)
        CUS.ACCT = R.ALT.ACCT<1>
        R.ACCT = ""
        ACC.ERR = ""
        CALL F.READ(FN.ACCT, CUS.ACCT, R.ACCT, F.ACCT, ACCT.ERR)
    END

RETURN
*-------------------------------------------------------------------------------------------------------------
PROCESS:
*--------
    CHRG.TYPE.1 = '' ; CHRG.TYPE.2 = ''

    IF RIGHT(DE.48,3) EQ "123" OR RIGHT(DE.48,4) EQ "Free" THEN       ;* Balance enquiry off-us & Free
        IF RIGHT(DE.48,4) EQ "Free" THEN
            CUS.ACCT   = 'EGP1124300010099'
            CHG.AMT        = "1.00"
            CHRG.TYPE.1 = ''
        END ELSE
            CUS.ACCT = AT$INCOMING.ISO.REQ(102)
            CHG.AMT        = "1.00"
            CHRG.TYPE.1    = 'ATMSCBENQ'
        END
    END ELSE
        IF RIGHT(DE.48,5) EQ 'Local' THEN         ;* Balance enquiry Local
            CUS.ACCT = AT$INCOMING.ISO.REQ(102)
            CHG.AMT = "5.00"
            CR.ACCT = "PL52024"
            CHRG.TYPE.1 = ''
        END ELSE
            IF RIGHT(DE.48,3) EQ "Int" THEN       ;* Balance enquiry International
                CUS.ACCT = AT$INCOMING.ISO.REQ(102)
                CHG.AMT = "10.00"
                CR.ACCT = "PL52024"
                CHRG.TYPE.1 = ''
            END
        END
    END
    IF RIGHT(DE.48,4) NE "Free" THEN
        GOSUB GET.LOCK.AMT
    END ELSE
        OUTGOING = DE.39
    END

    IF OUTGOING EQ "00" THEN
        GOSUB CHARGE.REQUEST.OFS
        IF CHKVAL = "-1" THEN
            OUTGOING = "05"
        END
    END
RETURN
*-------------------------------------------------------------------------------------------------------------
CHARGE.REQUEST.OFS:
*------------------


    CHG.VERSION = 'FUNDS.TRANSFER,ATM.CHG'
    USERNAME = AT$ATM.PARAMETER.REC<ATM.PARA.OFS.USER>
    PASSWORD = AT$ATM.PARAMETER.REC<ATM.PARA.OFS.PASSWORD>
    COMP.CODE = ID.COMPANY

    OFS.SOURCE.ID = AT$ATM.PARAMETER.REC<ATM.PARA.CHG.OFS.SOURC>

    FN.OFS.SOURCE = 'F.OFS.SOURCE'
    F.OFS.SOURCE = ''
    CALL OPF(FN.OFS.SOURCE, F.OFS.SOURCE)
    R.OFS.SRC.REC = ''

    CALL F.READ(FN.OFS.SOURCE,OFS.SOURCE.ID,R.OFS.SRC.REC,F.OFS.SOURCE,ERR.REC)
    OFS$SOURCE.REC = R.OFS.SRC.REC
    IF OFS$SOURCE.REC THEN
        OFS$SOURCE.ID = OFS.SOURCE.ID
    END
    CCY.CODE = 'EGP'

    IF RIGHT(DE.48,4) EQ "Free" THEN
        CHG.DESC = AT$INCOMING.ISO.REQ(102)
        CHG.DESC = TRIM(CHG.DESC,"","D")
    END ELSE
        CHG.DESC = 'BALANCE FEES'
        CUS.ACCT       = TRIM(CUS.ACCT, " ","D")
    END

    OFS.MSG = CHG.VERSION:'/I/PROCESS,':USERNAME:'/':PASSWORD:'/':COMP.CODE:',,DEBIT.ACCT.NO::=':CUS.ACCT:','
    OFS.MSG := 'DEBIT.CURRENCY::=':CCY.CODE:',CREDIT.CURRENCY::=':CCY.CODE:',DEBIT.AMOUNT::=':CHG.AMT:',CREDIT.ACCT.NO::=':CR.ACCT
    IF CHRG.TYPE.1 THEN
        OFS.MSG :=',CHARGE.TYPE:1::=':CHRG.TYPE.1
    END
    OFS.MSG := ',DEBIT.THEIR.REF:1:1=':CHG.DESC:',RETRIVAL.REF.NO:1:1=':DE.37
    OFS.MSG := ',ORDERING.CUST:1=BANK,PROFIT.CENTRE.DEPT:1=1,'

    CALL OFS.PROCESS.MANAGER(OFS.MSG,OFS.RESP)
    CHKVAL = FIELD (OFS.RESP,'/', 3)

RETURN
*-------------------------------------------------------------------------------------------------------------
GET.LOCK.AMT:
*------------

    Y.TOT.LAMT = '0' ; Y.LAMT = '0' ; FT.AMT = '0'

    Y.BLCE = R.ACCT<AC.WORKING.BALANCE>
    Y.LOCKED.AMOUNT = R.ACCT<AC.LOCKED.AMOUNT>

    IF Y.LOCKED.AMOUNT NE "" THEN
        Y.CNT = DCOUNT(Y.LOCKED.AMOUNT,@VM)
        Y.LAMT = Y.LOCKED.AMOUNT<1,Y.CNT>
    END

    R.FT.CHG.TYPE = ''
    IF CHRG.TYPE.1 NE '' THEN
        CALL F.READ(FN.FT.CHG.TYPE, CHRG.TYPE.1, R.FT.CHG.TYPE, F.FT.CHG.TYPE, ERR.FT)
        FT.AMT += R.FT.CHG.TYPE<FT5.FLAT.AMT>
    END

    Y.FIN.BALC = Y.BLCE - FT.AMT - Y.LAMT - CHG.AMT
*    PRINT Y.FIN.BALC

    AVL.BALANCE    = R.ACCT<AC.AVAILABLE.BAL>
*Line [ 197 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    AVAL.NO        = DCOUNT(AVL.BALANCE,@VM)
    WS.AVAL.BAL    = AVL.BALANCE<1,AVAL.NO>
    Y.FIN.BALC.AV = WS.AVAL.BAL - FT.AMT - Y.LAMT - CHG.AMT

    IF Y.FIN.BALC LE 0 THEN
        OUTGOING = '51'
    END ELSE
        IF Y.FIN.BALC.AV LE 0 AND WS.AVAL.BAL NE '' THEN
            OUTGOING = '51'
        END ELSE
            OUTGOING = DE.39
        END
    END

RETURN
**---------------------------------------------------------------**
END
*-------------------------------------------------------------------------------------------------------------
