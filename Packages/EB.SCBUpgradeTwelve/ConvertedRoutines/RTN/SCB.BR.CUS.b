* @ValidationCode : MjotMTM1NTI5OTEzNDpDcDEyNTI6MTY0NDkzNDE0MjI3NDp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 16:09:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>987</Rating>
*-----------------------------------------------------------------------------
* Version 9 20/02/03  GLOBUS Release No. G11.1.01 11/12/00

SUBROUTINE SCB.BR.CUS

******************************************************************

*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT.REFERENCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.PARMS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LG.CUS

*************************************************************************

    GOSUB DEFINE.PARAMETERS

    IF LEN(V$FUNCTION) GT 1 THEN
        GOTO V$EXIT
    END

    CALL MATRIX.UPDATE

    GOSUB INITIALISE          ;* Special Initialising

*************************************************************************

* Main Program Loop

    LOOP

        CALL RECORDID.INPUT

    UNTIL (MESSAGE EQ 'RET')

        V$ERROR = ''

        IF MESSAGE EQ 'NEW FUNCTION' THEN

            GOSUB CHECK.FUNCTION        ;* Special Editing of Function

            IF V$FUNCTION EQ 'E' OR V$FUNCTION EQ 'L' THEN
                CALL FUNCTION.DISPLAY
                V$FUNCTION = ''
            END

        END ELSE

            GOSUB CHECK.ID    ;* Special Editing of ID
            IF V$ERROR THEN GOTO MAIN.REPEAT

            CALL RECORD.READ

            IF MESSAGE EQ 'REPEAT' THEN
                GOTO MAIN.REPEAT
            END

            CALL MATRIX.ALTER

            GOSUB CHECK.RECORD          ;* Special Editing of Record
            IF V$ERROR THEN GOTO MAIN.REPEAT

            LOOP
                GOSUB PROCESS.FIELDS    ;* ) For Input
                GOSUB PROCESS.MESSAGE   ;* ) Applications
            WHILE (MESSAGE EQ 'ERROR') REPEAT

        END

MAIN.REPEAT:
    REPEAT

V$EXIT:
RETURN          ;* From main program

*************************************************************************
*                      S u b r o u t i n e s                            *
*************************************************************************

PROCESS.FIELDS:

* Input or display the record fields.

    LOOP
        IF SCREEN.MODE EQ 'MULTI' THEN
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.MULTI.INPUT
            END ELSE
                CALL FIELD.MULTI.DISPLAY
            END
        END ELSE
            IF FILE.TYPE EQ 'I' THEN
                CALL FIELD.INPUT
            END ELSE
                CALL FIELD.DISPLAY
            END
        END

    WHILE NOT(MESSAGE)

        GOSUB CHECK.FIELDS    ;* Special Field Editing

        IF T.SEQU NE '' THEN T.SEQU<-1> = A + 1

    REPEAT

RETURN

*************************************************************************

PROCESS.MESSAGE:

* Processing after exiting from field input (PF5)

    IF MESSAGE = 'DEFAULT' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
        END
    END

    IF MESSAGE = 'PREVIEW' THEN
        MESSAGE = 'ERROR'     ;* Force the processing back
        IF V$FUNCTION <> 'D' AND V$FUNCTION <> 'R' THEN
            GOSUB CROSS.VALIDATION
            IF NOT(V$ERROR) THEN
REM >               GOSUB DELIVERY.PREVIEW   ; * Activate print preview
            END
        END
    END

    IF MESSAGE EQ 'VAL' THEN
        MESSAGE = ''
        BEGIN CASE
            CASE V$FUNCTION EQ 'D'
                GOSUB CHECK.DELETE          ;* Special Deletion checks
            CASE V$FUNCTION EQ 'R'
                GOSUB CHECK.REVERSAL        ;* Special Reversal checks
            CASE OTHERWISE
                GOSUB CROSS.VALIDATION      ;* Special Cross Validation
                IF NOT(V$ERROR) THEN
                    GOSUB OVERRIDES
                END
        END CASE
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.UNAU.WRITE     ;* Special Processing before write
        END
        IF NOT(V$ERROR) THEN
            CALL UNAUTH.RECORD.WRITE
            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.UNAU.WRITE  ;* Special Processing after write
            END
        END

    END

    IF MESSAGE EQ 'AUT' THEN
        GOSUB AUTH.CROSS.VALIDATION     ;* Special Cross Validation
        IF NOT(V$ERROR) THEN
            GOSUB BEFORE.AUTH.WRITE     ;* Special Processing before write
        END

        IF NOT(V$ERROR) THEN

            CALL AUTH.RECORD.WRITE

            IF MESSAGE NE "ERROR" THEN
                GOSUB AFTER.AUTH.WRITE  ;* Special Processing after write
            END
        END

    END

RETURN

*************************************************************************
*                      Special Tailored Subroutines                     *
*************************************************************************

CHECK.ID:

* Validation and changes of the ID entered.  Set ERROR to 1 if in error.

*    MYLIMIT  = ID.NEW :".":"0017010":".01"
*    MYLIMIT  = ID.NEW :".0008000.01"
*    MYLIMIT1 = ID.NEW :".0007010.01"
*    MYLIMIT2 = ID.NEW :".0007020.01"
*    MYLIMIT3 = ID.NEW :".0007030.01"
*    MYLIMIT4 = ID.NEW :".0007040.01"
    MYLIMIT = ID.NEW :".0002500.01"
    MYLIMIT1 = ID.NEW :".0010200.01"

    TEMP.LIMIT=FIELD(ID.NEW,'-',2)
*    IF  COUNT(ID.NEW,'.') NE 3 OR TEMP.LIMIT = '' THEN
*       E='You.Should.Enter.Limit';CALL ERR ; MESSAGE = "REPEAT"

*  END ELSE

*Line [ 225 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('LIMIT':@FM:LIMIT.CURRENCY,TEMP.LIMIT,XXXX)
    F.ITSS.LIMIT = 'F.LIMIT'
    FN.F.ITSS.LIMIT = ''
    CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
    CALL F.READ(F.ITSS.LIMIT,TEMP.LIMIT,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
    XXXX=R.ITSS.LIMIT<LI.LIMIT.CURRENCY>
    IF ETEXT THEN E='Invalid.Limit';CALL ERR ; MESSAGE = "REPEAT"
* END
    T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
    T.SEL = "SELECT FBNK.LIMIT WITH @ID LIKE ":MYLIMIT:" OR @ID LIKE ":MYLIMIT1
*    T.SEL :=" OR @ID LIKE ":MYLIMIT2:" OR @ID LIKE ":MYLIMIT3
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG )

    IF KEY.LIST THEN
        RRR1= KEY.LIST<SELECTED>
        IF R.NEW(SCB.LGCS.LIMIT.NO) = '' THEN
            R.NEW(SCB.LGCS.LIMIT.NO) = KEY.LIST<SELECTED>
        END
* END ELSE
*  T.SEL= '';KEY.LIST = '';SELECTED = '';ERR.MSG = ''
        T.SEL = "SELECT FBNK.LIMIT WITH @ID LIKE ":MYLIMIT1
* CALL EB.READLIST(T.SEL,KEY.LIST1,"",SELECTED1,ERR.MSG )
* IF KEY.LIST1 THEN
*  RRR2 = KEY.LIST1<SELECTED1>
* R.NEW(SCB.LGCS.LIMIT.NO) = KEY.LIST1<SELECTED1>
    END
*ELSE
*        E = 'Customer.Has.No.Limit'; CALL ERR ; MESSAGE = "REPEAT"
*    END
*END

    IF E THEN V$ERROR = 1

RETURN

*************************************************************************

CHECK.RECORD:

* Validation and changes of the Record.  Set ERROR to 1 if in error.
* IF RRR1 THEN R.NEW(SCB.LGCS.LIMIT.NO) = RRR1
* IF RRR2 THEN R.NEW(SCB.LGCS.LIMIT.NO) = RRR2

RETURN

*************************************************************************

CHECK.FIELDS:
REM > CALL XX.CHECK.FIELDS



    IF E THEN
        T.SEQU = "IFLD"
        CALL ERR
    END

RETURN

*************************************************************************

CROSS.VALIDATION:

*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
*
REM > CALL XX.CROSSVAL

*Line [ 290 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    XX = DCOUNT(R.NEW(SCB.LGCS.LG.TYPE),@VM)
    FOR I = 1 TO XX
        MYTYPE = R.NEW(SCB.LGCS.LG.TYPE)<1,I>
*Line [ 300 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('SCB.LG.PARMS':@FM:SCB.LGP.LG.KIND,MYTYPE,LGKIND)
        F.ITSS.SCB.LG.PARMS = 'F.SCB.LG.PARMS'
        FN.F.ITSS.SCB.LG.PARMS = ''
        CALL OPF(F.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS)
        CALL F.READ(F.ITSS.SCB.LG.PARMS,MYTYPE,R.ITSS.SCB.LG.PARMS,FN.F.ITSS.SCB.LG.PARMS,ERROR.SCB.LG.PARMS)
        LGKIND=R.ITSS.SCB.LG.PARMS<SCB.LGP.LG.KIND>
*Line [ 295 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LL = DCOUNT (LGKIND,@VM)
*Line [ 297 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.NEW(SCB.LGCS.LG.KIND)<1,I>,@SM)

        FOR J = 1 TO YY

            LOCATE R.NEW(SCB.LGCS.LG.KIND)<1,I,J> IN LGKIND<1,1> SETTING PPP THEN
            END ELSE
*Line [ 304 ] Adding EB.SCBUpgradeTwelve. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
                ETEXT = 'Wrong.LG.Kind' ; AF = SCB.LGCS.LG.KIND ; AV = I ; AS = J ; CALL EB.SCBUpgradeTwelve.TXT( ETEXT) ; CALL EB.SCBUpgradeTwelve.STORE.END.ERROR
            END
        NEXT J

    NEXT I

*
* If END.ERROR has been set then a cross validation error has occurred
*
    IF END.ERROR THEN
        A = 1
        LOOP UNTIL T.ETEXT<A> <> "" DO A = A+1 ; REPEAT
        T.SEQU = A
        V$ERROR = 1
        MESSAGE = 'ERROR'
    END
RETURN          ;* Back to field input via UNAUTH.RECORD.WRITE

*************************************************************************

OVERRIDES:
*
*  Overrides should reside here.
*
    V$ERROR = ''
    ETEXT = ''
    TEXT = ''
REM > CALL XX.OVERRIDE
*

*
    IF TEXT = "NO" THEN       ;* Said NO to override
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input

    END
RETURN

*************************************************************************

AUTH.CROSS.VALIDATION:


RETURN

*************************************************************************

CHECK.DELETE:


RETURN

*************************************************************************

CHECK.REVERSAL:


RETURN

*************************************************************************
DELIVERY.PREVIEW:

RETURN

*************************************************************************

BEFORE.UNAU.WRITE:
*
*  Contract processing code should reside here.
*
REM > CALL XX.         ;* Accounting, Schedule processing etc etc

    IF TEXT = "NO" THEN       ;* Said No to override
        CALL TRANSACTION.ABORT          ;* Cancel current transaction
        V$ERROR = 1
        MESSAGE = "ERROR"     ;* Back to field input
        RETURN
    END

*
* Additional updates should be performed here
*
REM > CALL XX...



RETURN

*************************************************************************

AFTER.UNAU.WRITE:


RETURN

*************************************************************************

AFTER.AUTH.WRITE:


RETURN

*************************************************************************

BEFORE.AUTH.WRITE:

    BEGIN CASE
        CASE R.NEW(V-8)[1,3] = "INA"        ;* Record status
REM > CALL XX.AUTHORISATION
        CASE R.NEW(V-8)[1,3] = "RNA"        ;* Record status
REM > CALL XX.REVERSAL

    END CASE

RETURN

*************************************************************************

CHECK.FUNCTION:

* Validation of function entered.  Set FUNCTION to null if in error.

    IF INDEX('V',V$FUNCTION,1) THEN
        E = 'FUNCTION NOT ALLOWED FOR THIS APPLICATION'
        CALL ERR
        V$FUNCTION = ''
    END

RETURN

*************************************************************************

INITIALISE:

RETURN

*************************************************************************

DEFINE.PARAMETERS:  * SEE 'I_RULES' FOR DESCRIPTIONS *

REM > CALL XX.FIELD.DEFINITIONS

    MAT F = ""  ; MAT N = "" ;  MAT T = ""
    MAT CHECKFILE = "" ; MAT CONCATFILE = ""
*    ID.CHECKFILE = "CUSTOMER" :FM:2:FM:"L"
*   ID.CHECKFILE = "LIMIT" :FM:2:FM:"L"


    ID.F  = "CUSTNO"; ID.N  = "30"; ID.T  = "ANY"
    Z = 0

    Z+=1 ; F(Z)= "COMM.AMT"     ; N(Z) = "5"      ; T(Z) = "AMT"
    Z+=1 ; F(Z)= "COMM.PREC"    ; N(Z) = "5"      ; T(Z) = "ANY"
    Z+=1 ; F(Z)= "CHRG.AMT"     ; N(Z) = "5"      ; T(Z) = ""
    T(Z)< 2> = "YES_NO"
    Z+=1 ; F(Z)= "CHRGE.AMT"    ;  N(Z) = "35"    ;  T(Z)= "ANY"
    Z+=1 ; F(Z)= "KEEP.FEES"    ;  N(Z) = "5"     ;  T(Z)= "AMT"
    Z+=1 ; F(Z)= "CHRG.AMT.6"   ;  N(Z) = "35"    ;  T(Z)= "ANY"

    Z+=1 ; F(Z)= "RESERVED7"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED6"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED5"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED4"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED3"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED2"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'
    Z+=1 ; F(Z)= "RESERVED1"             ;  N(Z) = "35"    ;  T(Z)= "ANY"
    T(Z)<3> = 'NOINPUT'

    V = Z + 9
RETURN

*************************************************************************
END
