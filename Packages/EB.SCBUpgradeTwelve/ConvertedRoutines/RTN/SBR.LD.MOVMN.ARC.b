* @ValidationCode : MjoxODI3MjY5NDE1OkNwMTI1MjoxNjQ0OTMxNDc1NDczOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:24:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>970</Rating>
*-----------------------------------------------------------------------------
*** ���� ���� ����� ����� ����� ***
*** CREATED BY KHALED ***
*** ARCHIVE BEFORE 20130101 ***
***=================================

SUBROUTINE SBR.LD.MOVMN.ARC

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.RENEW.METHOD
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
    REPORT.ID='SBR.LD.MOVMN'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter LD No. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    ID = COMI

    YTEXT = "Enter Date from : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATF = COMI
    DAT.FROM = DATF[3,6]:'0001'

    YTEXT = "Enter Date to : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATT = COMI
    DAT.TO = DATT[3,6]:'2359'

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$ARC' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    DAT.H  = ''     ; V.DATE.H   = '' ; MAT.DATE.H = '' ; DESC = '' ; DESC.H = ''
    AMT.H  = ''     ; CATEG.H = ''    ; RATE.H = ''     ; RATE.AMT.H = '' ; KEYLIST2 = ''
    CUR.H  = ''     ; DAT1.H = ''     ; CATEG.ID.H = '' ; DESC.ID.H = ''
    COMP = ID.COMPANY
RETURN
*========================================================================
PROCESS:
    T.SEL = "SELECT ":FN.LD: " WITH @ID EQ ":ID:" AND (CATEGORY GE 21001 AND CATEGORY LE 21010) AND STATUS NE 'LIQ' AND CUSTOMER.ID NE ''"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,E1)

            FIRST.ID = R.LD<LD.LOCAL.REF><1,LDLR.FIRST.LD.ID>
            DAT = R.LD<LD.VALUE.DATE>
            V.DATE  = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
            DAT1 = R.LD<LD.FIN.MAT.DATE>
            MAT.DATE  = DAT1[7,2]:'/':DAT1[5,2]:'/':DAT1[1,4]
            AMT = R.LD<LD.AMOUNT>
            AMT1 = FIELD(AMT,'.',1)

            CATEG.ID = R.LD<LD.CATEGORY>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG1)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
            CATEG = CATEG1[6,20]
            RATE = R.LD<LD.INTEREST.RATE>
            RATE.AMT = R.LD<LD.TOT.INTEREST.AMT>
            RATE.AMT1 = FIELD(RATE.AMT,'.',1)

            CUR = R.LD<LD.CURRENCY>

* IF KEY.LIST<Z> EQ FIRST.ID THEN
*     DESC = '�����'
* END ELSE
*     DESC = '�����'
* END


            XX1 = SPACE(132)
            XX1<1,1>[1,15]   = KEY.LIST<Z>
            XX1<1,1>[17,15]  = V.DATE
            XX1<1,1>[35,15]  = MAT.DATE
            XX1<1,1>[52,15]  = AMT
            XX1<1,1>[62,15]  = CATEG
            XX1<1,1>[73,15]  = RATE
            XX1<1,1>[85,10]  = RATE.AMT
            XX1<1,1>[100,15] = CUR
* XX1<1,1>[110,15] = DESC
**********************************************************************************

            T.SEL1 = "SELECT ":FN.LD.H: " WITH (CATEGORY GE 21001 AND CATEGORY LE 21010) AND STATUS NE 'LIQ' AND DATE.TIME GE ":DAT.FROM:" AND DATE.TIME LE ": DAT.TO:" AND CUSTOMER.ID EQ ":CUST.ID:" AND FIRST.LD.ID EQ ":FIRST.ID:" AND CURRENCY EQ ":CUR
            T.SEL1 := " BY @ID"
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 THEN

                CALL F.READ(FN.LD.H,KEY.LIST1<1>,R.LD.H,F.LD.H,E1)

                DAT.H<1> = R.LD.H<LD.VALUE.DATE>
                V.DATE.H<1>  = DAT.H<1>[7,2]:'/':DAT.H<1>[5,2]:'/':DAT.H<1>[1,4]
                DAT1.H<1> = R.LD.H<LD.FIN.MAT.DATE>
                MAT.DATE.H<1>  = DAT1.H<1>[7,2]:'/':DAT1.H<1>[5,2]:'/':DAT1.H<1>[1,4]
                AMT.H<1> = R.LD.H<LD.AMOUNT>
                AMT.H1 = FIELD(AMT.H<1>,'.',1)

                CATEG.ID.H<1> = R.LD.H<LD.CATEGORY>
*Line [ 157 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID.H<1>,CATEG1.H)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,CATEG.ID.H<1>,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                CATEG1.H=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
                CATEG.H<1> = CATEG1.H[6,20]
                RATE.H<1> = R.LD.H<LD.INTEREST.RATE>
                RATE.AMT.H<1> = R.LD.H<LD.TOT.INTEREST.AMT>
                RATE.AMT.H1 = FIELD(RATE.AMT.H<1>,'.',1)

                CUR.H<1> = R.LD.H<LD.CURRENCY>

                KEYLIST1 = FIELD(KEY.LIST<1>,';',1)
                IF KEYLIST1 EQ FIRST.ID THEN
                    DESC.H = '�����'
                END ELSE
                    DESC.H = '�����'
                END

                XX2 = SPACE(132)
                XX2<1,1>[1,15]   = KEY.LIST1<1>
                XX2<1,1>[17,15]  = V.DATE.H<1>
                XX2<1,1>[35,15]  = MAT.DATE.H<1>
                XX2<1,1>[52,15]  = AMT.H<1>
                XX2<1,1>[62,15]  = CATEG.H<1>
                XX2<1,1>[73,15]  = RATE.H<1>
                XX2<1,1>[85,10]  = RATE.AMT.H<1>
                XX2<1,1>[100,15] = CUR.H<1>
                XX2<1,1>[110,15] = DESC.H
                PRINT XX2<1,1>

                FOR I = 2 TO SELECTED1
                    CALL F.READ(FN.LD.H,KEY.LIST1<I>,R.LD.H,F.LD.H,E1)

                    DAT.H<I> = R.LD.H<LD.VALUE.DATE>
                    V.DATE.H<I>  = DAT.H<I>[7,2]:'/':DAT.H<I>[5,2]:'/':DAT.H<I>[1,4]
                    DAT1.H<I> = R.LD.H<LD.FIN.MAT.DATE>
                    MAT.DATE.H<I>  = DAT1.H<I>[7,2]:'/':DAT1.H<I>[5,2]:'/':DAT1.H<I>[1,4]
                    AMT.H<I> = R.LD.H<LD.AMOUNT>
                    AMT.H1<I> = FIELD(AMT.H<I>,'.',1)

                    CATEG.ID.H<I> = R.LD.H<LD.CATEGORY>
*Line [ 201 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID.H<I>,CATEG1.H)
                    F.ITSS.CATEGORY = 'F.CATEGORY'
                    FN.F.ITSS.CATEGORY = ''
                    CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                    CALL F.READ(F.ITSS.CATEGORY,CATEG.ID.H<I>,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                    CATEG1.H=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION,2>
                    CATEG.H<I> = CATEG1.H[6,20]
                    RATE.H<I> = R.LD.H<LD.INTEREST.RATE>
                    RATE.AMT.H<I> = R.LD.H<LD.TOT.INTEREST.AMT>
                    RATE.AMT.H1<I> = FIELD(RATE.AMT.H<I>,'.',1)

                    CUR.H<I> = R.LD.H<LD.CURRENCY>

                    KEYLIST2<I> = FIELD(KEY.LIST1<I>,';',1)

                    IF KEYLIST2<I> NE KEYLIST2<I-1> THEN
                        DESC.H = '�����'
                    END ELSE
                        IF KEYLIST2<I> EQ KEYLIST2<I-1> THEN
                            DESC.H = '�����'
                        END
                    END

                    IF V.DATE.H<I> NE V.DATE.H<I-1> OR MAT.DATE.H<I> NE MAT.DATE.H<I-1> OR AMT.H1<I> NE AMT.H1<I-1> OR CATEG.H<I> NE  CATEG.H<I-1> OR RATE.H<I> NE RATE.H<I-1> OR RATE.AMT.H1<I> NE RATE.AMT.H1<I-1> OR CUR.H<I> NE CUR.H<I-1> THEN

                        XX3 = SPACE(132)
                        XX3<1,1>[1,15]   = KEY.LIST1<I>
                        XX3<1,1>[17,15]  = V.DATE.H<I>
                        XX3<1,1>[35,15]  = MAT.DATE.H<I>
                        XX3<1,1>[52,15]  = AMT.H<I>
                        XX3<1,1>[62,15]  = CATEG.H<I>
                        XX3<1,1>[73,15]  = RATE.H<I>
                        XX3<1,1>[85,10]  = RATE.AMT.H<I>
                        XX3<1,1>[100,15] = CUR.H<I>
                        XX3<1,1>[110,15] = DESC.H
                        PRINT XX3<1,1>
                    END
                NEXT I
            END

            IF KEY.LIST<Z> EQ FIRST.ID THEN
                DESC = '�����'
            END ELSE
                IF KEY.LIST<Z> EQ KEYLIST2<I> THEN
                    DESC = '�����'
                END ELSE
                    DESC = '�����'
                END
            END
            XX1<1,1>[110,15] = DESC

            IF V.DATE.H<I> NE V.DATE OR MAT.DATE.H<I> NE MAT.DATE OR AMT.H1<I> NE AMT1 OR CATEG.H<I> NE CATEG OR RATE.H<I> NE RATE OR RATE.AMT.H1<I> NE RATE.AMT1 OR CUR.H<I> NE CUR THEN
                PRINT XX1<1,1>
            END
        NEXT Z
    END
*===============================================================
    PRINT STR('=',120)
*===============================================================
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 265 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    DATF1  = DATF[7,2]:'/':DATF[5,2]:"/":DATF[1,4]
    DATT1  = DATT[7,2]:'/':DATT[5,2]:"/":DATT[1,4]

    CALL F.READ(FN.LD,ID,R.LD,F.LD,E1)
    CUST.ID = R.LD<LD.CUSTOMER.ID>
*Line [ 280 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"���� ����� �����"
    PR.HD :="'L'":SPACE(55):"��":" ":DATF1:" ":"���":" ":DATT1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������� : ":CUST.ID:SPACE(10):"���� ��������� :":ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������� : ":CUST.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":"��� �������" :SPACE(5):"����� ����":SPACE(5):"����� ���������":SPACE(5):"������" :SPACE(5):"�����":SPACE(5):"�����":SPACE(5):"���� ������":SPACE(5):"������":SPACE(5):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
