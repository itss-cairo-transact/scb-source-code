* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>1312</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* Create By Nessma
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.SMS.TXT.OFS

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RTN.FEES.SMS.OFS
*----------------------------------------------
    GOSUB INITIALISE
    GOSUB CHECK.RECORD
    RETURN
*----------------------------------------------------------------------*
END.PROG:
*--------
    ID.TEMP = FILE.NAME
    CALL F.READ(FN.TEMP,ID.TEMP,R.TEMP,F.TEMP,READ.ER)
    R.TEMP<FEES.SMS.RUN.DATE> = TODAY

    CALL F.WRITE(FN.TEMP,ID.TEMP,R.TEMP)
    CALL JOURNAL.UPDATE(ID.TEMP)

    DIR.NAME = 'CARD-CENTER'

    HUSH ON
    EXECUTE 'DELETE ':DIR.NAME:' ':FILE.NAME
    HUSH OFF
    RETURN
*----------------------------------------------------------------------*
CHECK.RECORD:
*------------
    NN.SEL = "SELECT CARD-CENTER WITH @ID LIKE SMS-..."
    CALL EB.READLIST(NN.SEL,N.LIST,"",SELECTED,ER.MSG.N)

    IF SELECTED GT 1 THEN
        TEXT = "ERROR ** MORE THAN ONE FILE" ; CALL REM
    END

    IF SELECTED EQ 1 THEN
        FILE.NAME = N.LIST<1>
        CALL F.READ(FN.TEMP,FILE.NAME,R.TEMP,F.TEMP,READ.ER)

        IF READ.ER THEN
            Path = "CARD-CENTER/":FILE.NAME
            GOSUB BUILD.RECORD
            GOSUB END.PROG
        END ELSE
            TEXT = "ERROR * Already.Exist" ; CALL REM
            RETURN
        END
    END
    RETURN
*----------------------------------------------------------------------*
INITIALISE:
*----------
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "CARD.SMS"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    FN.TEMP = 'F.SCB.RTN.FEES.SMS.OFS'   ; F.TEMP = ''
    CALL OPF(FN.TEMP , F.TEMP)
    RETURN
*---------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = ","

    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    EOF = ''
    I   = 1
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CUSTOMER.NO  = FIELD(Line,",",1)
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUSTOMER.NO,CUST.COMP)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSTOMER.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUST.COMP=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>

            CARD.NO      = FIELD(Line,",",2)
            XX           = FIELD(Line,",",3)
            YY           = FIELD(Line,",",5)
**---------------------------------------------------------------------**
*                   ***** OFS RECORD *****                       *
**---------------------------------------------------------------------**
            DAT      = TODAY
            TXN.TYPE = "AC53"
            DB.AMT   = '5'
            CURR     = 'EGP'
            PL.ACC   = "PL52648"
            CRD      = "ATMC.":CARD.NO
*Line [ 148 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CARD.ISSUE': @FM:CARD.IS.ACCOUNT,CRD,ACCOUNT)
F.ITSS.CARD.ISSUE = 'FBNK.CARD.ISSUE'
FN.F.ITSS.CARD.ISSUE = ''
CALL OPF(F.ITSS.CARD.ISSUE,FN.F.ITSS.CARD.ISSUE)
CALL F.READ(F.ITSS.CARD.ISSUE,CRD,R.ITSS.CARD.ISSUE,FN.F.ITSS.CARD.ISSUE,ERROR.CARD.ISSUE)
ACCOUNT=R.ITSS.CARD.ISSUE<CARD.IS.ACCOUNT>
            ACC      = ACCOUNT<1,1>
            BRN      = CARD.NO[7,2]
            COMPO    = "EG00100":BRN

            OFS.USER.INFO     = "AUTO.CHRGE//":COMPO
*------------------------------- NEW ------------------------
            ACC.NO = ACC
*Line [ 162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CU)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CU=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 169 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACC.NO,CU.NO)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CU.NO=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 176 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.NO,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>

            FESS.AMT = '5'
            IF (CATEG EQ 1001 OR CATEG EQ 1002 ) OR ( CATEG GE 6501 AND CATEG LE 6513 ) THEN
                GOSUB CHEK.C.BAL
                IF NET.BAL LT FESS.AMT THEN
                    GOSUB GET.NEW.ACC
                END ELSE
                    ACC = ACC.NO
                END

*Line [ 193 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                IF (CATEG EQ 1001 OR CATEG EQ 1002 ) OR ( CATEG GE 6501 AND CATEG LE 6513 ) THEN
                    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":TXN.TYPE:COMMA
                    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=":DB.AMT:COMMA
                    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=":CURR:COMMA
                    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":CURR:COMMA
                    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=99":COMMA
                    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":ACC:COMMA
                    OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":"CARD.FEES-":'201012':COMMA
                    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":PL.ACC:COMMA
                    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":CARD.NO:COMMA
                    OFS.MESSAGE.DATA := "ORDERING.BANK=SCB":COMMA
                    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DAT:COMMA
                    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":DAT:COMMA
                    OFS.MESSAGE.DATA := "COMMISSION.CODE=WAIVE":COMMA
                    OFS.MESSAGE.DATA := "CHARGE.CODE=WAIVE":COMMA
                    OFS.MESSAGE.DATA := "CO.CODE=":COMPO:COMMA

                    F.PATH  = FN.OFS.IN
                    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                    OFS.ID  = "T99.":"AC.TST.":CARD.NO:"-":TODAY

                    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-CARD.FEES' ON ERROR  TEXT = " ERROR ";CALL REM
                    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                END
            END ELSE

                GOSUB GET.NEW.ACC

*Line [ 230 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC,CATEG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATEG=R.ITSS.ACCOUNT<AC.CATEGORY>
                IF (CATEG EQ 1001 OR CATEG EQ 1002 ) OR ( CATEG GE 6501 AND CATEG LE 6513 ) THEN
                    OFS.MESSAGE.DATA  = "TRANSACTION.TYPE=":TXN.TYPE:COMMA
                    OFS.MESSAGE.DATA := "DEBIT.AMOUNT=":DB.AMT:COMMA
                    OFS.MESSAGE.DATA := "DEBIT.CURRENCY=":CURR:COMMA
                    OFS.MESSAGE.DATA := "CREDIT.CURRENCY=":CURR:COMMA
                    OFS.MESSAGE.DATA := "PROFIT.CENTRE.DEPT=99":COMMA
                    OFS.MESSAGE.DATA := "DEBIT.ACCT.NO=":ACC:COMMA
                    OFS.MESSAGE.DATA := "CREDIT.THEIR.REF=":"CARD.FEES-":'201012':COMMA
                    OFS.MESSAGE.DATA := "CREDIT.ACCT.NO=":PL.ACC:COMMA
                    OFS.MESSAGE.DATA := "DEBIT.THEIR.REF=":CARD.NO:COMMA
                    OFS.MESSAGE.DATA := "ORDERING.BANK=SCB":COMMA
                    OFS.MESSAGE.DATA := "DEBIT.VALUE.DATE=":DAT:COMMA
                    OFS.MESSAGE.DATA := "CREDIT.VALUE.DATE=":DAT:COMMA
                    OFS.MESSAGE.DATA := "COMMISSION.CODE=WAIVE":COMMA
                    OFS.MESSAGE.DATA := "CHARGE.CODE=WAIVE":COMMA
                    OFS.MESSAGE.DATA := "CO.CODE=":COMPO:COMMA

                    F.PATH  = FN.OFS.IN
                    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
                    OFS.ID  = "T99.":"AC.TST.":CARD.NO:"-":TODAY

                    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-CARD.FEES' ON ERROR  TEXT = " ERROR ";CALL REM
                    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                END
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath
    RETURN
*-----------------------------------------------------------
CHEK.C.BAL:
*----------
*Line [ 273 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACC.NO,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
    AMT       = '5'
    ZERO      = "0.00"
    BAL       = WORK.BALANCE

*Line [ 284 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACC.NO,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 237 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    LOCK.NO   = DCOUNT(LOCK.AMT,@VM)
    BLOCK     = LOCK.AMT<1,LOCK.NO>
    BAL.BLOCK = BAL - BLOCK
    NET.BAL   = BAL.BLOCK - AMT

    RETURN
**--------------------------------------------------------------------**
GET.NEW.ACC:
*-----------
    FN.CUS.AC = 'FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    FN.ACC    = 'FBNK.ACCOUNT' ;R.ACC='';F.ACC=''
    CALL OPF(FN.ACC,F.ACC)

    CALL F.READ( FN.CUS.AC,CU, R.CUS.AC, F.CUS.AC,ETEXT1)

    LOOP
        REMOVE ACCT FROM R.CUS.AC  SETTING POS1
    WHILE ACCT:POS1
        CALL F.READ(FN.ACC,ACCT,R.ACC,F.ACC,ERR1)
        CURRR     = R.ACC<AC.CURRENCY>
        AC.CATEG  = R.ACC<AC.CATEGORY>

        IF CURRR EQ 'EGP' THEN
            IF ( AC.CATEG EQ 1001 OR AC.CATEG EQ 1002 ) OR ( AC.CATEG GE 6501 AND AC.CATEG LE 6513 ) THEN

*Line [ 319 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACCT,WORK.BALANCE)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
WORK.BALANCE=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                AMT       = 5
                ZERO      = "0.00"
                BAL       = WORK.BALANCE

*Line [ 330 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACCT,LOCK.AMT)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOCK.AMT=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 271 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                LOCK.NO   = DCOUNT(LOCK.AMT,@VM)
                BLOCK     = LOCK.AMT<1,LOCK.NO>
                BAL.BLOCK = BAL - BLOCK
                NET.BAL   = BAL.BLOCK - AMT

                IF BAL.BLOCK LT FESS.AMT THEN
                    CONTINUE
                END ELSE
                    ACC = ACCT
                END
            END
        END
    REPEAT
    RETURN
**--------------------------------------------------------------------**
END
