* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>8870</Rating>
*-----------------------------------------------------------------------------
***NESSREEN AHMED 25/6/2005*********

    SUBROUTINE SCB.CARD.ISSUR.DATA.SUP

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS

* TO CREATE A DAILY BRIDGE FILE BY ALL CUSTOMERS' VISA APPLICATION

    DIR.NAME = '&SAVEDLISTS&'
 *   NEW.FILE = TODAY:".NESSREEN_SUPP"
  NEW.FILE = TODAY:"CARDBRIDGE"
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
**********************FILLING DATA********************************
******************************************************************
   T.SEL =  "SELECT F.SCB.VISA.APP WITH VISA.APP.DATE EQ ": TODAY
*    T.SEL = "SELECT F.SCB.VISA.APP WITH VISA.APP.DATE EQ 20050222"
*    T.SEL = "SELECT F.SCB.VISA.APP WITH @ID LIKE 13... AND @ID NE 13500015 AND @ID NE 13100064 AND CARD.TYPE NE VCLC AND CARD.TYPE NE VCLW"
*    T.SEL = "SELECT F.SCB.VISA.APP WITH @ID EQ 1200001"
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    DIM YY(2)
    YY(1) = '<?xml version="1.0" encoding="Windows-1256" ?>'
    YY(2) = '<BridgeFile>'

    FOR SS=1 TO 2
        WRITESEQ YY(SS) TO V.FILE.IN ELSE
            PRINT  'CAN NOT WRITE LINE ':YY(SS)
        END
    NEXT SS
    TEXT = 'SELECTED=':SELECTED ; CALL REM
    IF SELECTED THEN
*  FOR I = 1 TO 10
        FOR I = 1 TO SELECTED
            ZZ = FMT(I, "R%4")
            M.STATUS = ''
            Z0 = '' ; Z1 = '' ; Z2 = '' ; Z3 = '' ; Z4 = '' ; Z5 = '' ; Z7 = '' ; Z8 = ''
            U1 = '' ; A9 = '' ; A11 = '' ; A12 = '' ; A14 = '' ; PZ1 = '' ;
            P1 = '' ; P2 = '' ; P3 = '' ; P6 = '' ; P7 = '' ; P12 = '' ; P13 = ''
            SZ1 = '' ; S0 = '' ; S1 = '' ; S6 = '' ; S10 = '' ; S11= '' ; S13 = '' ; C1= ''
            D0 = '' ; Z9 = '' ;Z10 = '' ; Z11 = '' ; RZ1 = '' ; R1 = '' ; R2 = '' ; R3 = '' ; R6 = '' ; R7 = '' ; R8 = '' ; R12 = '' ; R13 = ''
            P12 = '' ; P13 = '' ; D1 = '' ; D5 = ''

            FN.VISA.APP = 'F.SCB.VISA.APP' ; F.VISA.APP = '' ; R.VISA.APP = '' ; RETRY3 = '' ; E3 = ''
            KEY.TO.USE = KEY.LIST<I>
            TEXT = 'CUST=':KEY.TO.USE ; CALL REM
            CALL OPF(FN.VISA.APP,F.VISA.APP)
            CALL F.READU(FN.SCB.VISA.APP,  KEY.TO.USE, R.VISA.APP, F.VISA.APP, E3, RETRY3)

            CARD.PROD = R.VISA.APP<SCB.VISA.CARD.TYPE>
            CARD.EXIST = R.VISA.APP<SCB.VISA.CARD.EXIST>
*******************************************************************
            FLAG.EXT = ''
            LOCATE FLAG.EXT IN CARD.EXIST<1,1> SETTING TT THEN
                CARD.PROD = R.VISA.APP<SCB.VISA.CARD.TYPE,TT>
                CARD.STAT = R.VISA.APP<SCB.VISA.CARD.STATUS,TT>
                CARD.ADD = R.VISA.APP<SCB.VISA.ADDITIONAL.CARD,TT>
                CARD.CUS = R.VISA.APP<SCB.VISA.ADDITIONAL.CUSTOMER,TT>
                N.LM = R.VISA.APP<SCB.VISA.LIMIT,TT>
                A14 = N.LM
                C5 = N.LM
                MAIN.CUST = R.VISA.APP<SCB.VISA.MAIN.CUSTOMER,TT>
                CUS = FMT(MAIN.CUST, "R%8")
                CURR = R.VISA.APP<SCB.VISA.CARD.CURRENCY,TT>
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE, CURR, CUR.COD)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.COD=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

*******************************************************************
**************************************************************************
                FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
                FN.ACCOUNT = 'F.ACCOUNT' ; F.ACCOUNT = '' ; R.ACCOUNT = '' ; RETRY2 = '' ; E2 = ''

                CALL OPF(FN.CUSTOMER,F.CUSTOMER)
*TEXT = 'KEY.TO.USE=':KEY.TO.USE ; CALL REM
                CALL F.READU(FN.CUSTOMER, KEY.TO.USE, R.CUSTOMER, F.CUSTOMER, E1, RETRY)

                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
****************FILLING BRIDGE******************************************************************************
                IF LEN(KEY.TO.USE)= 8 THEN
                    CLASS = KEY.TO.USE[3,1]
                END ELSE
                    CLASS = KEY.TO.USE[2,1]
                END
***********************************************************************************************************
                IF CARD.PROD = 'VICL' THEN
                    Z0 = 'SC-V-CR-CLAS'
                    Z3 = 'SCVC'
***********************************************************************************************************
                    IF CLASS = '1' AND CARD.STAT = 'MAIN' THEN
                        A12 = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                        D5 = R.VISA.APP<SCB.VISA.SUPP.LIMIT,TT>
                        Z1 = 'U'
                        Z2 = '#'
                        Z4 = '#'
                        Z5 = 'SCC1'
                        Z8 = 'SCC1'
                        IF CARD.ADD = 'YES' THEN
                            D0 = '1'
                            Z9 = Z0
                            Z10 = '#'
                            Z11 = Z8
                            RZ1 = 'N'

                            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
                            CALL F.READU(FN.CUSTOMER, CARD.CUS, R.CUSTOMER, F.CUSTOMER, E1, RETRY)
                            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                            TL.SUP = LOCAL.REF<1,CULR.TITLE>
*Line [ 176 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR( 'SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,TL.SUP,TITLE.SUP)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TL.SUP,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE.SUP=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
                            EN.TITLE.SUP = TITLE.SUP<1,1>
                            R1 = EN.TITLE.SUP
* CUS.NAME.SUP = R.CUSTOMER<EB.CUS.NAME.1>
                            CUS.NAME.SUP = LOCAL.REF<1,CULR.ARABIC.NAME>
                            R2 = FIELD(CUS.NAME.SUP,' ',2)
                            R3 = FIELD(CUS.NAME.SUP,' ',1)
                            IF LOCAL.REF<1,CULR.ID.NUMBER> # '' THEN
                                ID.NO.SUP = LOCAL.REF<1,CULR.ID.NUMBER>
                            END ELSE
                                ID.NO.SUP = LOCAL.REF<1,CULR.NSN.NO>
                            END
                            R6 = ID.NO.SUP
                            BR.DAT.SUP = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
                            YYYY.SUP = BR.DAT.SUP[1,4]
                            MM.SUP = BR.DAT.SUP[5,2]
                            DD.SUP = BR.DAT.SUP[7,2]
                            BIRTH.DAT.SUP = DD.SUP:'/':MM.SUP:'/':YYYY.SUP
                            R7 = BIRTH.DAT.SUP
                            CU.STAT.SUP = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
                            IF CU.STAT.SUP = '30' THEN
                                R8 = '1'
                            END ELSE
                                R8 = '0'
                            END
                            GENDER.SUP = LOCAL.REF<1,CULR.GENDER>
                            IF GENDER.SUP = 'F-����' THEN
                                SEX.SUP= 'F'
                            END ELSE
                                SEX.SUP= 'M'
                            END
                            R12 = SEX.SUP
                            M.STAT.SUP = LOCAL.REF<1,CULR.MARITAL.STATUS>
                            BEGIN CASE
                            CASE M.STAT.SUP = 'MarriedAndNestler-����� -����'
                                M.STATUS.SUP = 'M'
                            CASE M.STAT.SUP = 'Married-NonNestler�����- ��-����'
                                M.STATUS.SUP = 'M'
                            CASE M.STAT.SUP = 'Single-����'
                                M.STATUS.SUP = 'S'
                            END CASE
                            R13 = M.STATUS.SUP
                            EMB.NAME.SUP = R.CUSTOMER<EB.CUS.SHORT.NAME>
                            D1 = EMB.NAME.SUP
                        END ELSE
                            D0 = '0'
                        END   ;**END OF CARD.ADD = 'YES'
*********************************************************************************************
                    END ELSE
                        IF CLASS = '1' AND CARD.STAT = 'SUPPLEMENTARY FOR EXIST CARD' THEN
                            MAIN.ACCT = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                            NT.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ " : MAIN.ACCT
                            KEY.LIST.4=""
                            SELECTED.4=""
                            ER.MSG.4=""

                            CALL EB.READLIST(NT.SEL,KEY.LIST.4,"",SELECTED.4,ER.MSG.4)
                            IF SELECTED.4 THEN
                                F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; EC = '' ; RETRYC = ''
                                CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                                CALL F.READU(FN.CARD.ISSUE,KEY.LIST.4<1>,R.CARD.ISSUE,F.CARD.ISSUE,EC,RETRYC)
                                CI.LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                                PR.CUST = CI.LOCAL.REF<1,LRCI.VISA.PRIME.CUST>
                                PR.ACCT = CI.LOCAL.REF<1,LRCI.VISA.PRIME.ACCT>
                            END
* KEY.LM = MAIN.CUST:'.0011400.01.':KEY.TO.USE
* CALL DBR ( 'LIMIT':@FM:LI.INTERNAL.AMOUNT,KEY.LM,LMT)
                            Z1 = 'C'
                            Z2 = PR.CUST
                            Z4 = PR.ACCT
                            Z5 = 'SCC2'
                            Z8 = 'SCC2'
                            A12 = MAIN.ACCT
* A14 = LMT
* C5  = LMT
                        END
                    END       ;*END OF IF CLASS = '1'
**********************************************************************************************************
                    IF CLASS = '2' AND CARD.STAT = 'MAIN' THEN

                        A12 = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                        D5 = R.VISA.APP<SCB.VISA.SUPP.LIMIT,TT>
                        Z1 = 'U'
                        Z2 = '#'
                        Z4 = '#'
                        Z5 = 'SCC3'
                        Z8 = 'SCC3'
                        IF CARD.ADD = 'YES' THEN
                            D0 = '1'
                            Z9 = Z0
                            Z10 = '#'
                            Z11 = Z8
                            RZ1 = 'N'

                            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
                            CALL F.READU(FN.CUSTOMER, CARD.CUS, R.CUSTOMER, F.CUSTOMER, E1, RETRY)
                            LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                            TL.SUP = LOCAL.REF<1,CULR.TITLE>
*Line [ 280 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR( 'SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,TL.SUP,TITLE.SUP)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TL.SUP,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE.SUP=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
                            EN.TITLE.SUP = TITLE.SUP<1,1>
                            R1 = EN.TITLE.SUP
*CUS.NAME.SUP = R.CUSTOMER<EB.CUS.NAME.1>
                            CUS.NAME.SUP = LOCAL.REF<1,CULR.ARABIC.NAME>
                            R2 = FIELD(CUS.NAME.SUP,' ',2)
                            R3 = FIELD(CUS.NAME.SUP,' ',1)
                            IF LOCAL.REF<1,CULR.ID.NUMBER> # '' THEN
                                ID.NO.SUP = LOCAL.REF<1,CULR.ID.NUMBER>
                            END ELSE
                                ID.NO.SUP = LOCAL.REF<1,CULR.NSN.NO>
                            END
                            R6 = ID.NO.SUP
                            BR.DAT.SUP = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
                            YYYY.SUP = BR.DAT.SUP[1,4]
                            MM.SUP = BR.DAT.SUP[5,2]
                            DD.SUP = BR.DAT.SUP[7,2]
                            BIRTH.DAT.SUP = DD.SUP:'/':MM.SUP:'/':YYYY.SUP
                            R7 = BIRTH.DAT.SUP
                            CU.STAT.SUP = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
                            IF CU.STAT.SUP = '30' THEN
                                R8 = '1'
                            END ELSE
                                R8 = '0'
                            END
                            GENDER.SUP = LOCAL.REF<1,CULR.GENDER>
                            IF GENDER.SUP = 'F-����' THEN
                                SEX.SUP= 'F'
                            END ELSE
                                SEX.SUP= 'M'
                            END
                            R12 = SEX.SUP
                            M.STAT.SUP = LOCAL.REF<1,CULR.MARITAL.STATUS>
                            BEGIN CASE
                            CASE M.STAT.SUP = 'MarriedAndNestler-����� -����'
                                M.STATUS.SUP = 'M'
                            CASE M.STAT.SUP = 'Married-NonNestler�����- ��-����'
                                M.STATUS.SUP = 'M'
                            CASE M.STAT.SUP = 'Single-����'
                                M.STATUS.SUP = 'S'
                            END CASE
                            R13 = M.STATUS.SUP
                            EMB.NAME.SUP = R.CUSTOMER<EB.CUS.SHORT.NAME>
                            D1 = EMB.NAME.SUP
                        END ELSE
                            D0 = '0'
                        END   ;*END OF CARD.ADD = 'YES'
*******************************************************************************
                    END ELSE
                        IF CLASS = '2' OR CLASS = '5' AND CARD.STAT = 'SUPPLEMENTARY FOR EXIST CARD' THEN
                            MAIN.ACCT = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                            NT.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ " : MAIN.ACCT
                            KEY.LIST.4=""
                            SELECTED.4=""
                            ER.MSG.4=""

                            CALL EB.READLIST(NT.SEL,KEY.LIST.4,"",SELECTED.4,ER.MSG.4)
                            IF SELECTED.4 THEN
                                F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; EC = '' ; RETRYC = ''
                                CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                                CALL F.READU(FN.CARD.ISSUE,KEY.LIST.4<1>,R.CARD.ISSUE,F.CARD.ISSUE,EC,RETRYC)
                                CI.LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                                PR.CUST = CI.LOCAL.REF<1,LRCI.VISA.PRIME.CUST>
                                PR.ACCT = CI.LOCAL.REF<1,LRCI.VISA.PRIME.ACCT>
                            END
                            Z1 = 'C'
                            Z2 = PR.CUST
                            Z4 = PR.ACCT
                            Z5 = 'SCC4'
                            Z8 = 'SCC4'
                            A12 = MAIN.ACCT
*A14 = LMT
*C5  = LMT
                        END
********************************************************************************
                    END       ;*END OF IF CLASS = "2"
********************************************************************************************
                END ELSE      ;*END OF IF CARD.PROD = 'VICL'
*****************************************************************************************
*                    CARD = VIGO
*****************************************************************************************
* IF CARD.PROD = 'VGOC' OR CARD.PROD = 'VGOW' THEN
                    IF CARD.PROD = 'VIGO' THEN
                        Z0 = 'SC-V-CR-GOLD'
                        Z3 = 'SCVG'
*****************************************************************************************
                        IF CLASS = '1' AND CARD.STAT = 'MAIN' THEN

                            A12 = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                            D5 = R.VISA.APP<SCB.VISA.SUPP.LIMIT,TT>
                            Z1 = 'U'
                            Z2 = '#'
                            Z4 = '#'
                            Z5 = 'SCG1'
                            Z8 = 'SCG1'
                            IF CARD.ADD = 'YES' THEN

                                D0 = '1'
                                Z9 = Z0
                                Z10 = '#'
                                Z11 = Z8
                                RZ1 = 'N'

                                CALL OPF(FN.CUSTOMER,F.CUSTOMER)
                                CALL F.READU(FN.CUSTOMER, CARD.CUS, R.CUSTOMER, F.CUSTOMER, E1, RETRY)
                                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                                TL.SUP = LOCAL.REF<1,CULR.TITLE>
*Line [ 393 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                                CALL DBR( 'SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,TL.SUP,TITLE.SUP)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TL.SUP,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE.SUP=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
                                EN.TITLE.SUP = TITLE.SUP<1,1>
                                R1 = EN.TITLE.SUP
* CUS.NAME.SUP = R.CUSTOMER<EB.CUS.NAME.1>
                                CUS.NAME.SUP = LOCAL.REF<1,CULR.ARABIC.NAME>
                                R2 = FIELD(CUS.NAME.SUP,' ',2)
                                R3 = FIELD(CUS.NAME.SUP,' ',1)
                                IF LOCAL.REF<1,CULR.ID.NUMBER> # '' THEN
                                    ID.NO.SUP = LOCAL.REF<1,CULR.ID.NUMBER>
                                END ELSE
                                    ID.NO.SUP = LOCAL.REF<1,CULR.NSN.NO>
                                END
                                R6 = ID.NO.SUP
                                BR.DAT.SUP = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
                                YYYY.SUP = BR.DAT.SUP[1,4]
                                MM.SUP = BR.DAT.SUP[5,2]
                                DD.SUP = BR.DAT.SUP[7,2]
                                BIRTH.DAT.SUP = DD.SUP:'/':MM.SUP:'/':YYYY.SUP
                                R7 = BIRTH.DAT.SUP
                                CU.STAT.SUP = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
                                IF CU.STAT.SUP = '30' THEN
                                    R8 = '1'
                                END ELSE
                                    R8 = '0'
                                END
                                GENDER.SUP = LOCAL.REF<1,CULR.GENDER>
                                IF GENDER.SUP = 'F-����' THEN
                                    SEX.SUP= 'F'
                                END ELSE
                                    SEX.SUP= 'M'
                                END
                                R12 = SEX.SUP
                                M.STAT.SUP = LOCAL.REF<1,CULR.MARITAL.STATUS>
                                BEGIN CASE
                                CASE M.STAT.SUP = 'MarriedAndNestler-����� -����'
                                    M.STATUS.SUP = 'M'
                                CASE M.STAT.SUP = 'Married-NonNestler�����- ��-����'
                                    M.STATUS.SUP = 'M'
                                CASE M.STAT.SUP = 'Single-����'
                                    M.STATUS.SUP = 'S'
                                END CASE
                                R13 = M.STATUS.SUP
                                EMB.NAME.SUP = R.CUSTOMER<EB.CUS.SHORT.NAME>
                                D1 = EMB.NAME.SUP
                            END ELSE
                                D0 = '0'
                            END         ;* END OF CARD.ADD= 'YES'

********************************************************************************
                        END ELSE
                            IF CLASS = '1' AND CARD.STAT = 'SUPPLEMENTARY FOR EXIST CARD' THEN
                                MAIN.ACCT = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                                NT.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ " : MAIN.ACCT
                                KEY.LIST.4=""
                                SELECTED.4=""
                                ER.MSG.4=""

                                CALL EB.READLIST(NT.SEL,KEY.LIST.4,"",SELECTED.4,ER.MSG.4)
                                IF SELECTED.4 THEN
                                    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; EC = '' ; RETRYC = ''
                                    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                                    CALL F.READU(FN.CARD.ISSUE,KEY.LIST.4<1>,R.CARD.ISSUE,F.CARD.ISSUE,EC,RETRYC)
                                    CI.LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                                    PR.CUST = CI.LOCAL.REF<1,LRCI.VISA.PRIME.CUST>
                                    PR.ACCT = CI.LOCAL.REF<1,LRCI.VISA.PRIME.ACCT>
                                    MAIN.ACCT = R.CARD.ISSUE<CARD.IS.ACCOUNT>
                                    LMT = R.CARD.ISSUE<CARD.IS.LIMIT>
                                END
*                             KEY.LM = MAIN.CUST:'.0011400.01.':KEY.TO.USE
*                             CALL DBR ( 'LIMIT':@FM:LI.INTERNAL.AMOUNT,KEY.LM,LMT)

                                Z1 = 'C'
                                Z2 = PR.CUST
                                Z4 = PR.ACCT
                                Z5 = 'SCG2'
                                Z8 = 'SCG2'
                                A12 = MAIN.ACCT
                                A14 = LMT
                                C5  = LMT
********************************************************************************
                            END
                        END   ;*END OF CLASS = "1"
******************************************************************************************************************
                        IF CLASS = '2' AND CARD.STAT = 'MAIN' THEN

                            A12 = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                            D5 = R.VISA.APP<SCB.VISA.SUPP.LIMIT,TT>
                            Z1 = 'U'
                            Z2 = '#'
                            Z4 = '#'
                            Z5 = 'SCG3'
                            Z8 = 'SCG3'
                            IF CARD.ADD = 'YES' THEN
                                D0 = '1'
                                Z9 = Z0
                                Z10 = '#'
                                Z11 = Z8
                                RZ1 = 'N'
                                CALL OPF(FN.CUSTOMER,F.CUSTOMER)
                                CALL F.READU(FN.CUSTOMER, CARD.CUS, R.CUSTOMER, F.CUSTOMER, E1, RETRY)
                                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>
                                TL.SUP = LOCAL.REF<1,CULR.TITLE>
*Line [ 501 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                                CALL DBR( 'SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,TL.SUP,TITLE.SUP)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TL.SUP,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE.SUP=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
                                EN.TITLE.SUP = TITLE.SUP<1,1>
                                R1 = EN.TITLE.SUP
*CUS.NAME.SUP = R.CUSTOMER<EB.CUS.NAME.1>
                                CUS.NAME.SUP = LOCAL.REF<1,CULR.ARABIC.NAME>
                                R2 = FIELD(CUS.NAME.SUP,' ',2)
                                R3 = FIELD(CUS.NAME.SUP,' ',1)
                                IF LOCAL.REF<1,CULR.ID.NUMBER> # '' THEN
                                    ID.NO.SUP = LOCAL.REF<1,CULR.ID.NUMBER>
                                END ELSE
                                    ID.NO.SUP = LOCAL.REF<1,CULR.NSN.NO>
                                END
                                R6 = ID.NO.SUP
                                BR.DAT.SUP = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
                                YYYY.SUP = BR.DAT.SUP[1,4]
                                MM.SUP = BR.DAT.SUP[5,2]
                                DD.SUP = BR.DAT.SUP[7,2]
                                BIRTH.DAT.SUP = DD.SUP:'/':MM.SUP:'/':YYYY.SUP
                                R7 = BIRTH.DAT.SUP
                                CU.STAT.SUP = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
                                IF CU.STAT.SUP = '30' THEN
                                    R8 = '1'
                                END ELSE
                                    R8 = '0'
                                END
                                GENDER.SUP = LOCAL.REF<1,CULR.GENDER>
                                IF GENDER.SUP = 'F-����' THEN
                                    SEX.SUP= 'F'
                                END ELSE
                                    SEX.SUP= 'M'
                                END
                                R12 = SEX.SUP
                                M.STAT.SUP = LOCAL.REF<1,CULR.MARITAL.STATUS>
                                BEGIN CASE
                                CASE M.STAT.SUP = 'MarriedAndNestler-����� -����'
                                    M.STATUS.SUP = 'M'
                                CASE M.STAT.SUP = 'Married-NonNestler�����- ��-����'
                                    M.STATUS.SUP = 'M'
                                CASE M.STAT.SUP = 'Single-����'
                                    M.STATUS.SUP = 'S'
                                END CASE
                                R13 = M.STATUS.SUP
                                EMB.NAME.SUP = R.CUSTOMER<EB.CUS.SHORT.NAME>
                                D1 = EMB.NAME.SUP
                            END ELSE
                                D0 = '0'
                            END         ;*END OF IF CARD.ADD = 'YES'
********************************************************************************
                        END ELSE
                            IF CLASS = '2' OR CLASS = '5' AND CARD.STAT = 'SUPPLEMENTARY FOR EXIST CARD' THEN
                                MAIN.ACCT = R.VISA.APP<SCB.VISA.MAIN.CUST.ACCT,TT>
                                NT.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ " : MAIN.ACCT
                                KEY.LIST.4=""
                                SELECTED.4=""
                                ER.MSG.4=""

                                CALL EB.READLIST(NT.SEL,KEY.LIST.4,"",SELECTED.4,ER.MSG.4)
                                IF SELECTED.4 THEN
                                    F.CARD.ISSUE = '' ; FN.CARD.ISSUE = 'F.CARD.ISSUE' ; R.CARD.ISSUE = '' ; EC = '' ; RETRYC = ''
                                    CALL OPF(FN.CARD.ISSUE,F.CARD.ISSUE)
                                    CALL F.READU(FN.CARD.ISSUE,KEY.LIST.4<1>,R.CARD.ISSUE,F.CARD.ISSUE,EC,RETRYC)
                                    CI.LOCAL.REF = R.CARD.ISSUE<CARD.IS.LOCAL.REF>
                                    PR.CUST = CI.LOCAL.REF<1,LRCI.VISA.PRIME.CUST>
                                    PR.ACCT = CI.LOCAL.REF<1,LRCI.VISA.PRIME.ACCT>
*MAIN.ACCT = R.CARD.ISSUE<CARD.IS.ACCOUNT>
                                    LMT = R.CARD.ISSUE<CARD.IS.LIMIT>
                                END
                                Z1 = 'C'
                                Z2 = PR.CUST
                                Z4 = PR.ACCT
                                Z5 = 'SCG4'
                                Z8 = 'SCG4'
                                A12 = MAIN.ACCT
*A14 = LMT
*C5  = LMT

                            END
****************************************************************************
                        END   ;*END OF IF CLASS = '2'
****************************************************************************************************************
                    END       ;*IF CARD.PROD = 'VIGO'
                END ;*END OF ELSE OF IF CARD.PROD = 'VICL'
*********************************************************************************
*CUS.NAME = '' ; TL = '' ; TITLE = '' ; EN.TITLE = '' ; ID.NO = '' ; BR.DAT = '' ; GENDER = '' ; SEX = ''
**************************************************************************
                FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
                CALL OPF(FN.CUSTOMER,F.CUSTOMER)
*TEXT = 'KEY.TO.USE=':KEY.TO.USE ; CALL REM
                CALL F.READU(FN.CUSTOMER, KEY.TO.USE, R.CUSTOMER, F.CUSTOMER , E1, RETRY)
                LOCAL.REF = R.CUSTOMER<EB.CUS.LOCAL.REF>

* CUS.NAME = R.CUSTOMER<EB.CUS.NAME.1>
                CUS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
                TEXT = CUS.NAME ; CALL REM
                U1 = CUS.NAME
****UPDATED BY NESSREEN AHMED 9/3/2016 for R15****
****            BR.COD = R.CUSTOMER<EB.CUS.ACCOUNT.OFFICER>
                COMP.BOOK =  R.CUSTOMER <EB.CUS.COMPANY.BOOK>
                CUS.BR = COMP.BOOK[8,2]
                BR.COD = TRIM(CUS.BR, "0" , "L")
****END OF UPDATE 9/3/2016*****************************
                BR = FMT(BR.COD, "R%3")
                A9 = "2001-":BR
*Line [ 610 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BR.COD, BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BR.COD,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                BR.NAME = FIELD(BRANCH,'.',1)
                A11 = BR.NAME
                TL = LOCAL.REF<1,CULR.TITLE>
*Line [ 620 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'SCB.CUS.TITLE':@FM:SCB.TIT.DESCRIPTION.TITLE,TL,TITLE)
F.ITSS.SCB.CUS.TITLE = 'F.SCB.CUS.TITLE'
FN.F.ITSS.SCB.CUS.TITLE = ''
CALL OPF(F.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE)
CALL F.READ(F.ITSS.SCB.CUS.TITLE,TL,R.ITSS.SCB.CUS.TITLE,FN.F.ITSS.SCB.CUS.TITLE,ERROR.SCB.CUS.TITLE)
TITLE=R.ITSS.SCB.CUS.TITLE<SCB.TIT.DESCRIPTION.TITLE>
                EN.TITLE = TITLE<1,1>
                P1 = EN.TITLE
                P2 = FIELD(CUS.NAME,' ',2)
                P3 = FIELD(CUS.NAME,' ',1)
                IF LOCAL.REF<1,CULR.ID.NUMBER> # '' THEN
                    ID.NO = LOCAL.REF<1,CULR.ID.NUMBER>
                END ELSE
                    ID.NO = LOCAL.REF<1,CULR.NSN.NO>
                END
                P6 = ID.NO
                POSTAL.COD = R.CUSTOMER<EB.CUS.POST.CODE>
                BR.DAT = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
                YYYY = BR.DAT[1,4]
                MM = BR.DAT[5,2]
                DD = BR.DAT[7,2]
                BIRTH.DAT = DD:'/':MM:'/':YYYY
                P7 = BIRTH.DAT

                CU.STAT = R.CUSTOMER<EB.CUS.CUSTOMER.STATUS>
                IF CU.STAT = '30' THEN
                    P8 = '1'
                END ELSE
                    P8 = '0'
                END
                GENDER = LOCAL.REF<1,CULR.GENDER>
                IF GENDER = 'F-����' THEN
                    SEX= 'F'
                END ELSE
                    SEX= 'M'
                END
                P12 = SEX
                M.STAT = LOCAL.REF<1,CULR.MARITAL.STATUS>
                BEGIN CASE
                CASE M.STAT = 'MarriedAndNestler-����� -����'
                    M.STATUS = 'M'
                CASE M.STAT = 'Married-NonNestler�����- ��-����'
                    M.STATUS = 'M'
                CASE M.STAT = 'Single-����'
                    M.STATUS = 'S'
                END CASE

                P13 = M.STATUS
                SZ1 = 'N'
                LOCATION = LOCAL.REF<1,CULR.VISA.ADRS.TYPE>
                S0 = LOCATION<1,1>
                ADDRESS= LOCAL.REF<1,CULR.VISA.ADDRESS>
                S1 = ADDRESS<1,1>
                REGION = LOCAL.REF<1,CULR.REGION>
*Line [ 675 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'SCB.CUS.REGION':@FM:REG.DESCRIPTION,REGION,RG1)
F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
FN.F.ITSS.SCB.CUS.REGION = ''
CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
CALL F.READ(F.ITSS.SCB.CUS.REGION,REGION,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
RG1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>
*S2 = RG1
                S2 = ADDRESS<1,2>
                GOVERN = LOCAL.REF<1,CULR.GOVERNORATE>
*Line [ 685 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR( 'SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION,GOVERN,DESC)
F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,GOVERN,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
DESC=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
                CITY = DESC
                S6 = CITY

                COUNTRY = R.CUSTOMER<EB.CUS.TOWN.COUNTRY>
                IF COUNTRY = 'EG' THEN
                    S10 = '818'
                END

                TEL.NO = LOCAL.REF<1,CULR.TELEPHONE>
*Line [ 647 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                BB = DCOUNT(TEL.NO,@SM)
                FOR WW = 1 TO BB
                    TEL1 = LOCAL.REF<1,CULR.TELEPHONE,WW>
                    IF LEN(TEL1)> 7 THEN
                        S13 = TEL1
                    END ELSE
                        S11 = TEL1
                    END
                NEXT WW
                FAX.NO = LOCAL.REF<1,CULR.FAX>
                E.MAIL = LOCAL.REF<1,CULR.EMAIL.ADDRESS>

                EMB.NAME = R.CUSTOMER<EB.CUS.SHORT.NAME>
                C1 = EMB.NAME

*************************************************************
                DIM VISA.DATA(50)
                VISA.DATA(1) = '<Record RN="':ZZ:'">'
                VISA.DATA(2) = '<Z0>':Z0:'</Z0>'
                VISA.DATA(3) = '<Z1>':Z1:'</Z1>'
                VISA.DATA(4) = '<Z2>':Z2:'</Z2>'
                VISA.DATA(5) = '<Z3>':Z3:'</Z3>'
                VISA.DATA(6) = '<Z4>':Z4:'</Z4>'
                VISA.DATA(7) = '<Z5>':Z5:'</Z5>'
                VISA.DATA(8) = '<Z7>':'#':'</Z7>'
                VISA.DATA(9) = '<Z8>':Z8:'</Z8>'
                VISA.DATA(10) = '<U1>':U1:'</U1>'
                VISA.DATA(11) = '<A9>':A9:'</A9>'
                VISA.DATA(12) = '<A11>':A11:'</A11>'
                VISA.DATA(13) = '<A12>':A12:'</A12>'
                VISA.DATA(14) = '<A14>':A14:'</A14>'
                VISA.DATA(15) = '<PZ1>':'N':'</PZ1>'
                VISA.DATA(16) = '<P1>':P1:'</P1>'
                VISA.DATA(17) = '<P2>':P2:'</P2>'
                VISA.DATA(18) = '<P3>':P3:'</P3>'
                VISA.DATA(19) = '<P6>':P6:'</P6>'
                VISA.DATA(20) = '<P7>':P7:'</P7>'
                VISA.DATA(21) = '<P8>':P8:'</P8>'
                VISA.DATA(22) = '<P12>':P12:'</P12>'
                VISA.DATA(23) = '<P13>':P13:'</P13>'
                VISA.DATA(24) = '<SZ1>':'N':'</SZ1>'
                VISA.DATA(25) = '<S0>':S0:'</S0>'
                VISA.DATA(26) = '<S1>':S1:'</S1>'
                VISA.DATA(27) = '<S2>':S2:'</S2>'
                VISA.DATA(28) = '<S6>':S6:'</S6>'
                VISA.DATA(29) = '<S10>':'818':'</S10>'
                VISA.DATA(30) = '<S11>':S11:'</S11>'
                VISA.DATA(31) = '<S13>':S13:'</S13>'
                VISA.DATA(32) = '<TZ1>':'E':'</TZ1>'
                VISA.DATA(33) = '<C1>':C1:'</C1>'
                VISA.DATA(34) = '<C5>':C5:'</C5>'
                VISA.DATA(35) = '<D0>':D0:'</D0>'
                VISA.DATA(36) = '<Z9>':Z9:'</Z9>'
                VISA.DATA(37) = '<Z10>':Z10:'</Z10>'
                VISA.DATA(38) = '<Z11>':Z11:'</Z11>'
                VISA.DATA(39) = '<RZ1>':RZ1:'</RZ1>'
                VISA.DATA(40) = '<R1>':R1:'</R1>'
                VISA.DATA(41) = '<R2>':R2:'</R2>'
                VISA.DATA(42) = '<R3>':R3:'</R3>'
                VISA.DATA(43) = '<R6>':R6:'</R6>'
                VISA.DATA(44) = '<R7>':R7:'</R7>'
                VISA.DATA(45) = '<R8>':R8:'</R8>'
                VISA.DATA(46) = '<R12>':R12:'</R12>'
                VISA.DATA(47) = '<R13>':R13:'</R13>'
                VISA.DATA(48) = '<D1>':D1:'</D1>'
                VISA.DATA(49) = '<D5>':D5:'</D5>'
                VISA.DATA(50) = '</Record>'

                FOR A = 1 TO 50
                    WRITESEQ VISA.DATA(A) TO V.FILE.IN ELSE
                        PRINT  'CAN NOT WRITE LINE ':VISA.DATA(A)
                    END
                NEXT A
            END     ;***END OF LOCATE****
        NEXT I
        X = I+1
        TT = FMT(X, "R%4")
        DIM NN(6)
        NN(1) = '<TotalRecord RN="':TT:'">'
        NN(2) = '<RecCount>':X:'</RecCount>'
        NN(3) = '</TotalRecord>'
        NN(4) = '<Q1/>'
        NN(5) = '<Q2/>'
        NN(6) = '</BridgeFile>'
        FOR FF = 1 TO 6
            WRITESEQ NN(FF) TO V.FILE.IN ELSE
                PRINT  'CAN NOT WRITE LINE ':NN(FF)
            END
        NEXT FF
    END   ;***END OF SELECTED
*************************************************************
****************
    RETURN

END
