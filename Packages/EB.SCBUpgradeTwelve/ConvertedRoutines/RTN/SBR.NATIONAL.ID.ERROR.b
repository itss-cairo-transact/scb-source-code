* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    PROGRAM SBR.NATIONAL.ID.ERROR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "NATIONAL.ID.error.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"NATIONAL.ID.error.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "NATIONAL.ID.error.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE NATIONAL.ID.error.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create NATIONAL.ID.error.CSV File IN &SAVEDLISTS&'
        END
    END

    HEAD.DESC  = "CUSTOMER.ID":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "NATIONAL.NUMBER":","
    HEAD.DESC := "BRANCH":","
    HEAD.DESC := "COUNTRY.NAME":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------------------
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST    = '' ; SELECTED    = ''  ;  ER.MSG = ''
    CBE.ID.NEXT = '' ; CBE.ID.PREV = ''

    RETURN

*========================================================================
PROCESS:
*-------------------------------------------
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY NE EG AND @ID UNLIKE 994... AND POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 WITHOUT SECTOR IN (5010 5020) BY NATIONALITY BY ID.NUMBER"
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'EG' AND @ID UNLIKE 994... AND POSTING.RESTRICT LT 90 AND NEW.SECTOR EQ 4650 AND NSN.NO NE '' WITHOUT SECTOR IN (5010 5020) BY NSN.NO BY NATIONALITY"
*    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'EG' AND POSTING.RESTRICT LT 90 AND @ID UNLIKE 994... AND CUSTOMER.STATUS NE 3 AND NSN.NO NE '' AND ID.TYPE NE 5 BY NSN.NO BY NATIONALITY"
    T.SEL = "SELECT ":FN.CU:" WITH NATIONALITY EQ 'EG' AND POSTING.RESTRICT LT 90 AND @ID UNLIKE 994... AND NEW.SECTOR EQ 4650 AND CUSTOMER.STATUS NE 3 AND NSN.NO NE '' AND ID.TYPE NE 5 AND POSTING.RESTRICT NE 12 AND POSTING.RESTRICT NE 18 AND POSTING.RESTRICT NE 21 AND POSTING.RESTRICT NE 70  WITHOUT SECTOR IN ( 5010 5020 ) BY COMPANY.BOOK BY @ID BY NSN.NO "
*    T.SEL = "SELECT ":FN.CU:" WITH @ID IN (13101055 30207459 2203064 1205722 2207174 40217665 11217088 ) BY @ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E2)

            NSN.ID      = R.CU<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>

            CUS.ID      = KEY.LIST<I>
            CUST.NAME   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            WS.POSTING  = R.CU<EB.CUS.POSTING.RESTRICT>
            COMP.ID     = R.CU<EB.CUS.COMPANY.BOOK>
            WS.CU.BIRTH = R.CU<EB.CUS.BIRTH.INCORP.DATE>[3,6]
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COMP.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
            NAT.ID      = R.CU<EB.CUS.NATIONALITY>
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COUNTRY':@FM:EB.COU.COUNTRY.NAME,NAT.ID,COUNTRY.NAME)
F.ITSS.COUNTRY = 'F.COUNTRY'
FN.F.ITSS.COUNTRY = ''
CALL OPF(F.ITSS.COUNTRY,FN.F.ITSS.COUNTRY)
CALL F.READ(F.ITSS.COUNTRY,NAT.ID,R.ITSS.COUNTRY,FN.F.ITSS.COUNTRY,ERROR.COUNTRY)
COUNTRY.NAME=R.ITSS.COUNTRY<EB.COU.COUNTRY.NAME>
            WS.NAT.LEN  = LEN(NSN.ID)
            WS.NAT.1ST = NSN.ID[1,1]
            WS.NAT.BIRTH = NSN.ID[2,6]
            WS.GENDER       = R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER>[1,1]
            WS.NAT.GENDER   = NSN.ID[13,1]

            WS.NAT.GENDER   = WS.NAT.GENDER / 2

            IF WS.NAT.GENDER = INT(WS.NAT.GENDER) THEN
                WS.NAT.GENDER = 'F'
            END ELSE
                WS.NAT.GENDER = 'M'
            END


            IF WS.NAT.1ST NE '2' AND WS.NAT.1ST NE '3' THEN
                PRINT CUS.ID : "  ":NSN.ID :"  NE2OR3"
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := NSN.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","
                BB.DATA := WS.POSTING:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                GOTO NEXT.I
            END

            IF WS.NAT.LEN NE 14 THEN
                PRINT CUS.ID : "  ":NSN.ID :"  NE14"
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := NSN.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","
                BB.DATA := WS.POSTING:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                GOTO NEXT.I

            END
            IF WS.CU.BIRTH NE WS.NAT.BIRTH THEN
                PRINT CUS.ID : "  ":NSN.ID :"  BIRTH"
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := NSN.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","
                BB.DATA := WS.POSTING:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                GOTO NEXT.I
            END
            IF NOT (NUM(NSN.ID)) THEN
                PRINT CUS.ID : "  ":NSN.ID :"  NUM"
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := NSN.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","
                BB.DATA := WS.POSTING:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                GOTO NEXT.I

            END
            IF WS.GENDER NE WS.NAT.GENDER THEN
                PRINT CUS.ID : "  ":NSN.ID :"  ERORR_SEX"
                BB.DATA  = CUS.ID:","
                BB.DATA := CUST.NAME:","
                BB.DATA := NSN.ID:","
                BB.DATA := COMP.NAME:","
                BB.DATA := NAT.ID:","
                BB.DATA := COUNTRY.NAME:","
                BB.DATA := WS.POSTING:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                GOTO NEXT.I

            END
NEXT.I:
        NEXT I
    END
    RETURN
END
