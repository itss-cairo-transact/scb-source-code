* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
************************CREATED BY RIHAM YOUSSEF 20180111***********
*-----------------------------------------------------------------------------
*    PROGRAM SCB.CBE.5002.55
    SUBROUTINE SCB.CBE.5002.55

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.REPORT
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SCB.CBE.5002.5.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.CBE.5002.5.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.CBE.5002.5.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.CBE.5002.5.TXT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.CBE.5002.5.TXT File IN &SAVEDLISTS&'
        END
    END
******************************************************************
    COM.REG.NO       = ''
    CBE.NO           = ''
    AMT              = 0
    CUR              = ''
    RATE             = 0
    BEN.CUST         =  ''
    NAT.NAME         = ''
    BENF.1           = ''
    BENF.2           = ''
    FT.NO            = ''
    CUST.NAME = ''

*    DAT.1 = TODAY
*    CALL CDT("",DAT.1,'-1W')
    YTEXT = "Enter report date"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    DAT.1 = COMI 
    TEXT =  'DATE : ':DAT.1 ; CALL REM


    HEAD.DESC  = "��� ������":"|"
    HEAD.DESC : = "��� ����� �������":"|"
    HEAD.DESC := "����� ������ ������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "��� ������":"|"
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "��� �������� ":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "����� �� �������1":"|"
    HEAD.DESC := "�������":"|"
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.SCB.CBE.REPORT' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)


    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.SEC.N = 'F.SCB.NEW.SECTOR' ; F.SEC.N = ''
    CALL OPF(FN.SEC.N,F.SEC.N)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)

    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* CURRENT ACCOUNTS SELECTION *************

    T.SEL  = "SELECT ":FN.CBE:" WITH (AMT.5 NE '' OR AMT.6 NE '') AND SYS.DATE EQ ":DAT.1:" BY @ID"
*DEBUG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
*        DEBUG
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            CUR.CBE      =  R.CBE<CBE.R.CURR>
            AMT.CBE      = R.CBE<CBE.R.AMT.5>
            IF AMT.CBE EQ '' THEN
                AMT.CBE      = R.CBE<CBE.R.AMT.6>
            END
            CUR = 'USD'
            CALL F.READ(FN.CCY,'USD',R.CCY,F.CCY,E1)
            AMT.RATE     = R.CCY<SBD.CURR.MID.RATE>
            AMT.USD  = AMT.CBE / AMT.RATE
            IF AMT.USD GE '100000' THEN
                GOSUB GET.DETAIL
            END
        NEXT I
    END
    RETURN
*==============================================================
PRINT.DET:
    BB.DATA  = CUST.NAME:"|"
    BB.DATA := COM.REG.NO:"|"
    BB.DATA := CBE.NO:"|"
    BB.DATA := AMT:"|"
    BB.DATA := CUR:"|"
    BB.DATA := RATE:"|"
    BB.DATA := BEN.CUST:"|"
    BB.DATA := NAT.NAME:"|"
    BB.DATA := BENF.2:"|"
    BB.DATA := FT.NO:"|"


    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*===============================================================
GET.DETAIL:


    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
    AMT      = R.CBE<CBE.R.AMT.5>
    IF AMT EQ '' THEN
        AMT = R.CBE<CBE.R.AMT.6>
    END
    AMT        = AMT /1000000
    AMT        = FMT(AMT,"L0,")
    CUR        = R.CBE<CBE.R.CURR>

    FT.NO = KEY.LIST<I>
    IF  FT.NO[1,2] EQ 'FT' THEN
        ZZ = KEY.LIST<I>:';1'
        CALL F.READ(FN.FT.HIS,ZZ,R.FT.HIS,F.FT.HIS,E2)
        IF CUR EQ 'OTHR' THEN
            CUR     = R.FT.HIS<FT.DEBIT.CURRENCY>
        END ELSE
            CUR = CUR
        END

        AC.NO.DR     = R.FT.HIS<FT.DEBIT.ACCT.NO>
*Line [ 233 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO.DR,CUSID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.NO.DR,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 240 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>


        FT.AMT     = R.FT.HIS<FT.DEBIT.AMOUNT>
        FT.AMT.LCY = R.FT.HIS<FT.LOC.AMT.DEBITED>
        CALL F.READ(FN.CCY,CUR,R.CCY,F.CCY,E1)
        AMT.RATE = R.CCY<SBD.CURR.MID.RATE>
        AMT.RATE  =  FMT(AMT.RATE,"L2,")
        RATE      = AMT.RATE
        AC.NO     = R.FT.HIS<FT.CREDIT.ACCT.NO>
*   BENF.2    = R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
*   CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO,CUSNO)
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.NATIONALITY,CUSNO,NAT)

*    CALL DBR ('COUNTRY' :@FM:EB.COU.COUNTRY.NAME,NAT,NAT.NAME)
        NAT.NAME =  R.FT.HIS<FT.LOCAL.REF><1,FTLR.SEND.INSTIT>
        BENF.2   =  R.FT.HIS<FT.LOCAL.REF><1,FTLR.FOR.BK.CHR.COM,1> : R.FT.HIS<FT.LOCAL.REF><1,FTLR.FOR.BK.CHR.COM,2>

        AC.NO.DR     = R.FT.HIS<FT.CREDIT.ACCT.NO>
*Line [ 266 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,AC.NO.DR,CUSID)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,AC.NO.DR,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUSID=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 273 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        BEN.CUST  = LOCAL.REF<1,CULR.ARABIC.NAME>

        COM.REG.NO = LOCAL.REF<1,CULR.COM.REG.NO>
        CBE.NO     = LOCAL.REF<1,CULR.CBE.NO>
        IN.OUT = R.FT.HIS<FT.LOCAL.REF><1,FTLR.INN.OUTT>
        IF IN.OUT EQ 'OUT' THEN
            GOSUB PRINT.DET
        END
    END
    RETURN

*===============================================================
