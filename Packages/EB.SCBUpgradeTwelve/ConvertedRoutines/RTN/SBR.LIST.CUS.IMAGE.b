* @ValidationCode : Mjo2NjU1NjkzMDk6Q3AxMjUyOjE2NDQ5MzIwNjk1NjE6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:34:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-224</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBR.LIST.CUS.IMAGE
**    PROGRAM SBR.LIST.CUS.IMAGE

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*------------------------------------------
*-------*** CUSTOMER SIGNATURES ***--------
*------------------------------------------
    GOSUB INITIAL
RETURN
*------------------------------------
END.PROG:
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    TEXT = "��� �������"  ; CALL REM
RETURN
*==========================================
LINE.END:
*--------
    WW = SPACE(120)
    WW<1,1> = SPACE(20):"********************  ����� �������   ******************"
    PRINT " "
    PRINT WW<1,1>
RETURN
*------------------------------------------
PRINT.ARRAY:
*-----------
    FOR INDX = 1 TO ZZ
        ARRAY.RECORD<1,1>[1,10] = CUST.ARRAY<1,INDX>

*Line [ 66 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUST.ARRAY<1,INDX> ,LOCAL)
        F.ITSS.CUSTOMER = 'F.CUSTOMER'
        FN.F.ITSS.CUSTOMER = ''
        CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
        CALL F.READ(F.ITSS.CUSTOMER,CUST.ARRAY<1,INDX>,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
        LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        CUST.NAME  = LOCAL<1,CULR.ARABIC.NAME>
        ARRAY.RECORD<1,1>[15,30] = CUST.NAME

        ARRAY.RECORD<1,1>[60,30]  = DESC.ARRAY<1,INDX>
        ARRAY.RECORD<1,1>[90,30]  = INP.NAME.ARRAY<1,INDX>
        ARRAY.RECORD<1,1>[120,10] =INP.ARRAY<1,INDX>

        PRINT ARRAY.RECORD<1,1>
        ARRAY.RECORD<1,1> = ""

********
PRINT.ARRAY2:
*********
        ARRAY.RECORD2[90,30]   = AUT.NAME.ARRAY<1,INDX>
        ARRAY.RECORD2[120,10] = AUT.ARRAY<1,INDX>
        PRINT ARRAY.RECORD2<1,1>
        ARRAY.RECORD2<1,1> = ""

********
PRINT.ARRAY3:
*********
        PRINT ARRAY.RECORD3<1,1>
        ARRAY.RECORD3<1,1> = ""


    NEXT INDX

    IF ZZ EQ 1 THEN
        LINESS =SPACE(50) : "********** �� ���� ������  ********"
        PRINT LINESS
        PRINT " "
    END
RETURN
*------------------------------------------
INITIAL:
*-------
    REPORT.ID = 'SBR.LIST.CUS.IMAGE'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.IMG = "F.IM.DOCUMENT.IMAGE"   ;  F.IMG = ""
    CALL OPF(FN.IMG, F.IMG)

    FN.IMG.H = "F.IM.DOCUMENT.IMAGE$HIS"   ;  F.IMG.H = ""
    CALL OPF(FN.IMG.H, F.IMG.H)

    FN.USER = "F.USER"   ;  F.USER = ""
    CALL OPF(FN.USER, F.USER)


    CUST.ARRAY = ""
    DESC.ARRAY = ""
    INP.ARRAY = ""
    AUT.ARRAY = ""
    INP.NAME.ARRAY = ""
    AUT.NAME.ARRAY = ""
    ZZ = 1

    ARRAY.RECORD = ""
    ARRAY.RECORD2 = ""
    ARRAY.RECORD3 = ""
    COMP.ID = ID.COMPANY

*--- ENTER DATA
    YTEXT = "Enter the DATE : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATE.HIS = COMI
*---
    WS.TOD = DATE.HIS
    WS.TOD = WS.TOD[3,6]

    IF COMI EQ TODAY THEN
        TEXT = "��� ����� ������ ��������� �� ��� �����"
        CALL REM
        RETURN
    END ELSE
        GOSUB PRINT.HEAD
        GOSUB PROCESS
        GOSUB PROCESS.HIS
        GOSUB PRINT.ARRAY
        GOSUB PRINT.ARRAY2
        GOSUB PRINT.ARRAY3
        GOSUB LINE.END
        GOSUB END.PROG
    END

RETURN
*------------------------------------------
PROCESS:
*-------
    N.SEL  = "SELECT ":FN.IMG: " WITH DATE.TIME LIKE ":WS.TOD:"..."
    N.SEL := " AND IMAGE.APPLICATION EQ 'CUSTOMER'"
    N.SEL := " AND CO.CODE EQ ":COMP.ID
    CALL EB.READLIST(N.SEL,KEY.LIST,'',SELECTED,ERR)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            IMG.ID = KEY.LIST<II>
            CALL F.READ(FN.IMG, IMG.ID, R.IMG, F.IMG, ETEXT.IMG)
            CUS.ID = R.IMG<IM.DOC.IMAGE.REFERENCE>
            CUR.NO = R.IMG<IM.DOC.CURR.NO>
            INP.NO = R.IMG<IM.DOC.INPUTTER>
            AUT.NO = R.IMG<IM.DOC.AUTHORISER>

            INP =  FIELD(INP.NO,'_',2)
            AUT =  FIELD(AUT.NO,'_',2)
            CALL F.READ(FN.USER, INP, R.USER.INF1, F.USER, ETEXT.USER)
            INP.NAME = R.USER.INF1<EB.USE.USER.NAME>
            CALL F.READ(FN.USER, AUT, R.USER.INF2, F.USER, ETEXT.USER)
            AUT.NAME = R.USER.INF2<EB.USE.USER.NAME>


            IF CUR.NO EQ '1' THEN
                DESC   = " - ����� �����  ����  - "
                CUST.ARRAY<1,ZZ> = CUS.ID
                DESC.ARRAY<1,ZZ> = DESC
                INP.ARRAY<1,ZZ> = INP
                AUT.ARRAY<1,ZZ> = AUT
                INP.NAME.ARRAY<1,ZZ> = INP.NAME
                AUT.NAME.ARRAY<1,ZZ> = AUT.NAME
                ZZ = ZZ + 1
            END

            IF CUR.NO GT '1' THEN
                DESC   = " - ����� �����  -  "
                CUST.ARRAY<1,ZZ> = CUS.ID
                DESC.ARRAY<1,ZZ> = DESC
                INP.ARRAY<1,ZZ> = INP
                AUT.ARRAY<1,ZZ> = AUT
                INP.NAME.ARRAY<1,ZZ> = INP.NAME
                AUT.NAME.ARRAY<1,ZZ> = AUT.NAME


                ZZ = ZZ + 1
            END
        NEXT II
    END

RETURN
*------------------------------------------
PROCESS.HIS:
*-----------
    N.SEL  = "SELECT ":FN.IMG.H: " WITH DATE.TIME LIKE ":WS.TOD:"..."
    N.SEL := " AND IMAGE.APPLICATION EQ 'CUSTOMER'"
    N.SEL := " AND CO.CODE EQ ":COMP.ID
    CALL EB.READLIST(N.SEL,KEY.LIST,'',SELECTED,ERR.H)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            IMG.ID = KEY.LIST<II>
            CALL F.READ(FN.IMG.H, IMG.ID, R.IMG, F.IMG.H, ETEXT.IMG.H)
            CUS.ID = R.IMG<IM.DOC.IMAGE.REFERENCE>
            INP.NO = R.IMG<IM.DOC.INPUTTER>
            AUT.NO = R.IMG<IM.DOC.AUTHORISER>

            INP =  FIELD(INP.NO,'_',2)
            AUT =  FIELD(AUT.NO,'_',2)
            CALL F.READ(FN.USER, INP, R.USER.INF3, F.USER, ETEXT.USER)
            INP.NAME = R.USER.INF3<EB.USE.USER.NAME>
            CALL F.READ(FN.USER, AUT, R.USER.INF4, F.USER, ETEXT.USER)
            AUT.NAME = R.USER.INF4<EB.USE.USER.NAME>



            DESC   = " - ����� �����  - "
            CUST.ARRAY<1,ZZ> = CUS.ID
            DESC.ARRAY<1,ZZ> = DESC
            INP.ARRAY<1,ZZ> = INP
            AUT.ARRAY<1,ZZ> = AUT
            INP.NAME.ARRAY<1,ZZ> = INP.NAME
            AUT.NAME.ARRAY<1,ZZ> = AUT.NAME

            ZZ = ZZ + 1
        NEXT II
    END
RETURN
*------------------------------------------
PRINT.HEAD:
*----------
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*    YYBRN  = FIELD(BRANCH,'.',2)
*---***---
    IF COMP.ID[8,1] EQ 0 THEN
        IDD = COMP.ID[9,2]
    END ELSE
        IDD = COMP.ID[8,2]
    END

*Line [ 260 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,IDD,BRANCH)
    F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
    FN.F.ITSS.DEPT.ACCT.OFFICER = ''
    CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
    CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,IDD,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
    BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)

*---***---
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(110):"SBR.LIST.CUS.IMAGE"
    PR.HD :="'L'":" "
    PR.HD :=SPACE(50): "���� ���������� ���� ��� �����"
    PR.HD :=SPACE(2):"��� ������� �������"
    PR.HD :="'L'":SPACE(53): "�������  �� ��� "
    PR.HD := SPACE(3):YYBRN
    PR.HD := SPACE(3): "�� ��� "

    YYBRN.2 = DATE.HIS[7,2]:'/':DATE.HIS[5,2]:"/":DATE.HIS[1,4]
    PR.HD :=SPACE(3):YYBRN.2

    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10)
    PR.HD :="��� ������":SPACE(30)
    PR.HD :="��� �������":SPACE(20)
    PR.HD :="��� ������" :SPACE(18)
    PR.HD :="��� ������" :SPACE(10)
    PR.HD :="'L'":"":SPACE(91):"��� ������":SPACE(18):"��� ������"

    PR.HD :="'L'":STR('_',132)

    PRINT
    HEADING PR.HD
RETURN
*------------------------------------------
END
