* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.MM.CBE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DE.ADDRESS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    TEXT = '�� �������� �� �������� ' ; CALL REM

    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "F011.FMM" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"F011.FMM"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "F011.FMM" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE F011.FMM CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create F011.FMM File IN &SAVEDLISTS&'
        END
    END

    TDD = TODAY[6]:"..."
    TD  = TODAY
    RETURN

*========================================================================
PROCESS:
    FN.AGN = 'F.DE.ADDRESS' ; F.AGN = ''
    CALL OPF(FN.AGN,F.AGN)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.CCY = 'FBNK.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

*-------------------------------------------
******* Money Market SELECTION *************

    T.SEL = "SELECT ":FN.MM:" WITH CATEGORY IN (21030 21031 21037 21075 21076) AND CURRENCY NE 'EGP' AND CANCEL.IND NE 'Y' AND STATUS NE 'LIQ' AND DATE.TIME LIKE ":TDD
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.MM,KEY.LIST<I>,R.MM,F.MM,E1)
            CUS.ID      = R.MM<MM.CUSTOMER.ID>
            WS.VAL.DATE = R.MM<MM.VALUE.DATE>
            WS.MAT.DATE = R.MM<MM.MATURITY.DATE>
            WS.INT.RATE = R.MM<MM.INTEREST.RATE>
            WS.CCY      = R.MM<MM.CURRENCY>
            WS.AMT      = R.MM<MM.PRINCIPAL>
            WS.CATEG    = R.MM<MM.CATEGORY>
            WS.ROL.DATE = R.MM<MM.ROLLOVER.DATE>
            WS.INT.DATE = R.MM<MM.INT.PERIOD.START>

            IF WS.ROL.DATE NE '' THEN
                WS.LAST.RENEWAL.DATE = WS.ROL.DATE
            END ELSE
                IF WS.INT.DATE NE '' THEN
                    WS.LAST.RENEWAL.DATE = WS.INT.DATE
                END ELSE
                    IF WS.VAL.DATE NE '' THEN
                        WS.LAST.RENEWAL.DATE = WS.VAL.DATE
                    END
                END
            END

            IF WS.CCY EQ 'USD' THEN WS.CCY.CODE = '01'
            IF WS.CCY EQ 'GBP' THEN WS.CCY.CODE = '02'
            IF WS.CCY EQ 'EUR' THEN WS.CCY.CODE = '29'
            IF WS.CCY EQ 'SAR' THEN WS.CCY.CODE = '18'
            IF WS.CCY EQ 'JPY' THEN WS.CCY.CODE = '15'
            IF WS.CCY EQ 'AED' THEN WS.CCY.CODE = '19'

            AGN.ID = 'EG0010001.C-':CUS.ID:'.SWIFT.1'

            CALL F.READ(FN.AGN,AGN.ID,R.AGN,F.AGN,E4)
            WS.SWIFT.CODE1 = R.AGN<DE.ADD.DELIVERY.ADDRESS>

            CHK.LN = LEN(WS.SWIFT.CODE1)
            IF CHK.LN EQ '8' THEN
                WS.SWIFT.CODE = WS.SWIFT.CODE1:'XXX'
            END ELSE
                WS.SWIFT.CODE = WS.SWIFT.CODE1
            END

            IF WS.CATEG EQ '21075' THEN WS.OP.CODE = '0600'
            IF WS.CATEG EQ '21030' THEN WS.OP.CODE = '0100'
            IF WS.CATEG EQ '21076' THEN WS.OP.CODE = '0700'
            IF WS.CATEG EQ '21031' THEN WS.OP.CODE = '0200'
            IF WS.CATEG EQ '21037' THEN WS.OP.CODE = '0100'

            BB.DATA  = TD:","
            BB.DATA := '011':","
            BB.DATA := '0001':","
            BB.DATA := '11':","
            BB.DATA := WS.OP.CODE:","
            BB.DATA := WS.INT.RATE:","
            BB.DATA := WS.CCY.CODE:","
            BB.DATA := WS.AMT:","
            BB.DATA := WS.LAST.RENEWAL.DATE:","
            BB.DATA := WS.MAT.DATE:","
            BB.DATA := WS.SWIFT.CODE:","

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END
    RETURN
*===============================================================
