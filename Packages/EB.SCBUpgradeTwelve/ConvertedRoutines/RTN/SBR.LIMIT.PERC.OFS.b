* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.LIMIT.PERC.OFS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LIMIT.PERC
*---------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN

INITIALISE:
    OPENSEQ "MECH" , "LIMIT.PERC.AMT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LIMIT.PERC.AMT"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LIMIT.PERC.AMT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LIMIT.PERC.AMT CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LIMIT.PERC.AMT File IN MECH'
        END
    END


    OPENSEQ "MECH" , "LIMIT.PERC.AMT.PARENT" TO AA THEN
        CLOSESEQ AA
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LIMIT.PERC.AMT.PARENT"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LIMIT.PERC.AMT.PARENT" TO AA ELSE
        CREATE AA THEN
            PRINT 'FILE LIMIT.PERC.AMT.PARENT CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LIMIT.PERC.AMT.PARENT File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    FN.SL = 'F.SCB.LIMIT.PERC' ; F.SL = ''
    CALL OPF(FN.SL,F.SL)

    FN.LI = 'FBNK.LIMIT' ; F.LI = ''
    CALL OPF(FN.LI,F.LI)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC= ''
    CALL OPF(FN.AC,F.AC)

    COMMA  = ","
    V.DATE = TODAY
    WS.AMT.PERC.TOT1 = 0
    FLAG = 0

    T.SEL = "SELECT ":FN.SL:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.SL,KEY.LIST<I>,R.SL,F.SL,E1)
*Line [ 90 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AC.COUNT = DCOUNT(R.SL<SLP.ACCOUNT.NO>,@VM)
            FOR X = 1 TO AC.COUNT

                ACCT.NO   = R.SL<SLP.ACCOUNT.NO,X>
                LIMIT.ID  = R.SL<SLP.LIMIT,X>
                WS.PERC   = R.SL<SLP.PERC,X>

                ACCT.NO1  = R.SL<SLP.ACCOUNT.NO><1,X-1>
                LIMIT.ID1 = R.SL<SLP.LIMIT><1,X-1>
                WS.PERC1  = R.SL<SLP.PERC><1,X-1>

                LIMIT.ID2 = R.SL<SLP.LIMIT><1,X+1>

                CALL F.READ(FN.AC,ACCT.NO,R.AC,F.AC,E3)

                AC.BAL      = R.AC<AC.ONLINE.ACTUAL.BAL>
                WS.AMT.PERC = AC.BAL * WS.PERC / 100

                CALL F.READ(FN.LI,LIMIT.ID,R.LI,F.LI,E2)
                WS.INTERNAL.AMT = R.LI<LI.INTERNAL.AMOUNT>
                WS.PARENT       = R.LI<LI.RECORD.PARENT>

                CALL F.READ(FN.LI,LIMIT.ID1,R.LI,F.LI,E2)
                WS.PARENT1 = R.LI<LI.RECORD.PARENT>

                WS.LIMIT.ID.LEN = LEN(LIMIT.ID1)

                IF WS.AMT.PERC GT WS.INTERNAL.AMT THEN
                    WS.AMT = WS.INTERNAL.AMT
                END ELSE
                    WS.AMT = WS.AMT.PERC
                END

                IF AC.BAL = 0 THEN WS.AMT = 1

                IF AC.COUNT GT 1 THEN
                    IF LIMIT.ID EQ LIMIT.ID1 THEN
                        FLAG = 1
                        CALL F.READ(FN.AC,ACCT.NO1,R.AC,F.AC,E3)
                        AC.BAL1           = R.AC<AC.ONLINE.ACTUAL.BAL>
                        WS.AMT.PERC1      = AC.BAL1 * WS.PERC1 / 100
                        WS.AMT.PERC.TOT1 += WS.AMT.PERC1
                        WS.AMT.PERC.TOT   = WS.AMT.PERC.TOT1 + WS.AMT.PERC


                        IF WS.AMT.PERC.TOT GT WS.INTERNAL.AMT THEN
                            WS.AMT1 = WS.INTERNAL.AMT
                        END ELSE
                            WS.AMT1 = WS.AMT.PERC.TOT
                        END

                        IF AC.BAL1 = 0 THEN WS.AMT1 = 1

                        GO TO NXT
                    END
                END

                IF WS.LIMIT.ID.LEN EQ 19 OR (LIMIT.ID NE LIMIT.ID2) THEN
                    IF WS.AMT NE 0 THEN
                        IF WS.AMT = 1 THEN WS.AMT = 0
***** OFS FOR PARENT *******
                        WS.AMT = DROUND(WS.AMT,'2')

                        IDD = 'LIMIT,PERC,AUTO.CHRGE//EG0010001,':WS.PARENT

                        OFS.MESSAGE.DATA  =  "ADVISED.AMOUNT=":WS.AMT:COMMA
                        OFS.MESSAGE.DATA :=  "MAXIMUM.SECURED=":WS.AMT:COMMA
                        OFS.MESSAGE.DATA :=  "MAXIMUM.UNSECURED=":WS.AMT:COMMA
                        OFS.MESSAGE.DATA :=  "MAXIMUM.TOTAL=":WS.AMT:COMMA
*Line [ 160 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                        OFS.MESSAGE.DATA :=  "ADM.EXTENSION.DATE=":'NULL':COMMA

                        MSG.DATA = IDD:",":OFS.MESSAGE.DATA

                        WRITESEQ MSG.DATA TO AA ELSE
                            PRINT " ERROR WRITE FILE "
                        END

******************************************

                        IDD = 'LIMIT,PERC,AUTO.CHRGE//EG0010001,':LIMIT.ID

                        OFS.MESSAGE.DATA  =  "ADVISED.AMOUNT=":WS.AMT:COMMA
                        OFS.MESSAGE.DATA :=  "MAXIMUM.SECURED=":WS.AMT:COMMA
                        OFS.MESSAGE.DATA :=  "MAXIMUM.UNSECURED=":WS.AMT:COMMA
                        OFS.MESSAGE.DATA :=  "MAXIMUM.TOTAL=":WS.AMT:COMMA
*Line [ 177 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                        OFS.MESSAGE.DATA :=  "ADM.EXTENSION.DATE=":'NULL':COMMA

                        MSG.DATA = IDD:",":OFS.MESSAGE.DATA

                        WRITESEQ MSG.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END
                    END
                END

NXT:
            NEXT X
            IF FLAG = 1 THEN
                IF WS.AMT1 NE 0 THEN

                    IF WS.AMT1 = 1 THEN WS.AMT1 = 0

************ OFS FOR PARENT ***************
                    WS.AMT1 = DROUND(WS.AMT1,'2')
                    IDD = 'LIMIT,PERC,AUTO.CHRGE//EG0010001,':WS.PARENT1

                    OFS.MESSAGE.DATA  =  "ADVISED.AMOUNT=":WS.AMT1:COMMA
                    OFS.MESSAGE.DATA :=  "MAXIMUM.SECURED=":WS.AMT1:COMMA
                    OFS.MESSAGE.DATA :=  "MAXIMUM.UNSECURED=":WS.AMT1:COMMA
                    OFS.MESSAGE.DATA :=  "MAXIMUM.TOTAL=":WS.AMT1:COMMA
*Line [ 203 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    OFS.MESSAGE.DATA :=  "ADM.EXTENSION.DATE=":'NULL':COMMA

                    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

                    WRITESEQ MSG.DATA TO AA ELSE
                        PRINT " ERROR WRITE FILE "
                    END

*******************************************
                    IDD = 'LIMIT,PERC,AUTO.CHRGE//EG0010001,':LIMIT.ID1

                    OFS.MESSAGE.DATA  =  "ADVISED.AMOUNT=":WS.AMT1:COMMA
                    OFS.MESSAGE.DATA :=  "MAXIMUM.SECURED=":WS.AMT1:COMMA
                    OFS.MESSAGE.DATA :=  "MAXIMUM.UNSECURED=":WS.AMT1:COMMA
                    OFS.MESSAGE.DATA :=  "MAXIMUM.TOTAL=":WS.AMT1:COMMA
*Line [ 219 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    OFS.MESSAGE.DATA :=  "ADM.EXTENSION.DATE=":'NULL':COMMA

                    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

                    WRITESEQ MSG.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            END
        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LIMIT.PERC.AMT.PARENT'
    EXECUTE 'DELETE ':"MECH":' ':"LIMIT.PERC.AMT.PARENT"

    SLEEP 120

    EXECUTE 'COPY FROM MECH TO OFS.IN LIMIT.PERC.AMT'
    EXECUTE 'DELETE ':"MECH":' ':"LIMIT.PERC.AMT"

    RETURN
END
