* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>48</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.PACS008.LOG.BAT
*    PROGRAM    SBR.PACS008.LOG.BAT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*============================================================
    GOSUB INITIATE
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*=============================================================
INITIATE:

    SEQ.FILE.NAME = 'ACH/CreditTransfer'
    REN.FILE =''

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBR.PACS008.LOG.BAT'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.PAC8 = 'F.SCB.ACH.PACS008'   ; F.PAC8 = ''
    CALL OPF(FN.PAC8 , F.PAC8)

    KEY.LIST    = ""  ; SELECTED  =""  ;  ER.MSG  ="" ;R.PAC   = ''
    KEY.LIST.1  = ""  ; SELECTED.1=""  ;  ER.MSG.1="" ;R.PAC.1 = ''
    KEY.LIST.2  = ""  ; SELECTED.2=""  ;  ER.MSG.2="" ;R.PAC.2 = ''
    KEY.LIST.3  = ""  ; SELECTED.3=""  ;  ER.MSG.3="" ;R.PAC.3 = ''
    KEY.LIST.4  = ""  ; SELECTED.4=""  ;  ER.MSG.4="" ;R.PAC.4 = ''
    T.DATE = ''
    VAL.DATE = ""
    T.COUNT  = 0 ;TOT.POSTED.AMT = 0 ; TOT.LOADED.AMT = 0 ;POSTED.AMT =0
    TOT.VAL  = 0 ;POSTED.VAL.AMT = 0 ; BILL.VAL.AMT   = 0
    RETURN

*========================================================================
PROCESS:
*--------
*DEBUG
    YTEXT =  " YYYYMMDD ���� ����� ����� "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    FILE.DATE = COMI
    SET.DATE = FILE.DATE[1,4]:'-':FILE.DATE[5,2]:'-':FILE.DATE[7,2]

*    FILE.DATE = "20180212"
*    FILE.DATE = "20180123"
*---------------------------------------
    GOSUB PRINT.HEAD
*---------------------------------------
    T.SEL = "SELECT ACH/CreditTransfer  WITH @ID LIKE 62_PACS008_":FILE.DATE:"...txt...."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED

        RECORD.NAME = KEY.LIST<I>
        OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
            PRINT 'Unable to Locate ':SEQ.FILE.POINTER
            STOP
            RETURN
        END
        IF IOCTL(SEQ.FILE.POINTER, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP
        EOF = ''
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            FIL.TYP   = Y.MSG[140,4]
            VAL.DAT   = Y.MSG[115,10]
            ACH.DAT   = Y.MSG[47,10]
            NO.OF.REC = Y.MSG[77,5]
            BAT.ID    = Y.MSG[18,23]
            TOT.AMT   = Y.MSG[86,12]
            T.COUNT   = T.COUNT + 1
            IF VAL.DAT EQ SET.DATE THEN
                TOT.VAL = TOT.VAL + TOT.AMT
            END
            T.SEL.1   = "SELECT F.SCB.ACH.PACS008 WITH BATCH.ID EQ ":BAT.ID
            CALL EB.READLIST(T.SEL.1,KEY.LIST.1,"",SELECTED.1,ER.MSG.1)
            SELECTED.2 = 0
            IF SELECTED.1 GT 0 THEN
                T.SEL.2 = "SELECT F.SCB.ACH.PACS008 WITH BATCH.ID EQ ":BAT.ID:" AND TRN.DATE NE ''"
                CALL EB.READLIST(T.SEL.2,KEY.LIST.2,"",SELECTED.2,ER.MSG.2)
                IF SELECTED.2 THEN
                    POSTED.AMT = 0
                    FOR REC.2 = 1 TO SELECTED.2
                        CALL F.READ(FN.PAC8,KEY.LIST.2<REC.2>, R.PAC8, F.PAC8, PAC8.ER)
                        POSTED.AMT   = POSTED.AMT + R.PAC8<PACS8.AMOUNT>
                    NEXT REC.2
                END
            END
            GOSUB  REPORT.WRITE
            TOT.POSTED.AMT = TOT.POSTED.AMT + POSTED.AMT
            TOT.LOADED.AMT = TOT.LOADED.AMT + TOT.AMT
            POSTED.AMT     = 0
        END

        CLOSESEQ SEQ.FILE.POINTER

    NEXT I
*-------------------------- ������ ������ ���� �� ������� ����� ----------------
* T.SEL.3  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ ":TODAY:" AND PACS.TYPE EQ '1' AND REQ.SETTLEMENT.DATE EQ ":SET.DATE
    T.SEL.3  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ ":TODAY:" AND PACS.TYPE EQ '1'"
    CALL EB.READLIST(T.SEL.3,KEY.LIST.3,"",SELECTED.3,ER.MSG.3)
    IF SELECTED.3 THEN
        POSTED.VAL.AMT = 0
        FOR REC.3 = 1 TO SELECTED.3
            CALL F.READ(FN.PAC8,KEY.LIST.3<REC.3>, R.PAC8, F.PAC8, PAC8.ER)
            POSTED.VAL.AMT   = POSTED.VAL.AMT + R.PAC8<PACS8.AMOUNT>
        NEXT REC.3
    END

*------------------------- ������ ���������� ���� ����� -------------------
    T.SEL.4  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ ":TODAY:" AND PACS.TYPE EQ '1' AND CHARGER.BEARER EQ 'CASH' AND PURPOSE EQ 'COLL'"
    CALL EB.READLIST(T.SEL.4,KEY.LIST.4,"",SELECTED.4,ER.MSG.4)
    IF SELECTED.4 THEN
        BILL.VAL.AMT = 0
        FOR REC.4 = 1 TO SELECTED.4
            CALL F.READ(FN.PAC8,KEY.LIST.4<REC.4>, R.PAC8, F.PAC8, PAC8.ER)
            BILL.VAL.AMT   = BILL.VAL.AMT + R.PAC8<PACS8.AMOUNT>
        NEXT REC.4
    END
*-----------------------------------------------------------------------
    XX = ''
    PRINT XX
    DEF.AMT = TOT.LOADED.AMT - TOT.POSTED.AMT
    XX<1,1>[1,10]  = "����� = ":T.COUNT:SPACE(7):"������ ":TOT.LOADED.AMT:SPACE(7):"� �� ":TOT.VAL:SPACE(7):"������  ":TOT.POSTED.AMT:SPACE(7):"����� ":DEF.AMT
    PRINT XX
    XX = ''
    PRINT XX
    XX<1,1>[35,40] = "---------- �������� �� ������� �����  ----------"
    PRINT XX<1,1>
    XX = ''
    XX<1,1>[1,10]  = "   ���� �����":SPACE(10):"����� = ":SELECTED.3:SPACE(10):"������ ���� ": POSTED.VAL.AMT
    PRINT XX
    XX = ''
    XX<1,1>[1,10]  = "�������� �����":SPACE(10):"����� = ":SELECTED.4:SPACE(10):"������ ���� ": BILL.VAL.AMT
    PRINT XX

    XX = ''
    PRINT XX
    XX<1,1>[35,40] = "********************����� �����*****************"
    PRINT XX<1,1>

    RETURN
*==============================================================
REPORT.WRITE:

    XX  = SPACE(132)
    XX<1,I>[1,23]   = BAT.ID
    XX<1,I>[26,4]   = FIL.TYP
    XX<1,I>[33,10]  = ACH.DAT
    XX<1,I>[45,10]  = VAL.DAT
    XX<1,I>[56,5]   = NO.OF.REC
    XX<1,I>[63,12]  = TOT.AMT
    XX<1,I>[81,5]   = SELECTED.1
    XX<1,I>[92,5]   = SELECTED.2
    XX<1,I>[102,12] = POSTED.AMT

    PRINT XX<1,I>
    RETURN
*=============================================================
PRINT.HEAD:
*---------
    TT     = OCONV(TIME(),'MTS')
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]:"  ":TT
    Q.DATE = T.DATE
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"������� ������� ��� ����� ��� ":FILE.DATE[7,2]:'/':FILE.DATE[5,2]:"/":FILE.DATE[1,4]
* PR.HD := SPACE(3) : "������ ��" : SPACE(3) : VAL.DATE
    PR.HD :="'L'":SPACE(40):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(10):"��� �����":SPACE(5):"�����":SPACE(5):"�.�����":SPACE(5):"�.����":SPACE(5):"�����":SPACE(5):"������":SPACE(5):"������":SPACE(5):"������":SPACE(5):"�.����"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
