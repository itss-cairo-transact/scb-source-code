* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SCB.B.RESIZE.FILES.POST
    FULL.FILE = ""
    IS.DATA.PRESENT = @FALSE
    OPENPATH "." TO HOME.PATH ELSE
        CALL FATAL.ERROR("unable to open .")
    END
    CMD = "SELECT . LIKE ...RESIZE...LOG"
    EXECUTE CMD RTNLIST LOG.LIST
    LOOP
    WHILE READNEXT LOG.FILE FROM LOG.LIST DO
        LOG.REC = ""
        READ LOG.REC FROM HOME.PATH,LOG.FILE THEN
            IS.DATA.PRESENT = @TRUE
            FULL.FILE<-1> = LOG.REC
            DELETE HOME.PATH,LOG.FILE
        END
    REPEAT
    IF NOT(IS.DATA.PRESENT) THEN
        FULL.FILE = "No Files to be resized"
    END
    WRITE FULL.FILE TO HOME.PATH,"SCB_Files_Resized_Summary.txt" ON ERROR
        CALL OCOMO("Unable to write summary report")
    END
    RETURN
END
