* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-4</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBW.WEEKLY.BATCH

*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-09
$INSERT I_COMMON
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2022-02-09
$INSERT I_EQUATE
*******************************
    EXECUTE "OFSADMINLOGIN"
*******************************

** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q' ; WS.W = 'W'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

*------------------------------------------------------------------------------

    CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBW.CU.DRMNT.UPDATE")
*   CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBR.DRMNT.AC.CLOSE")
*   CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBR.DRMNT.AC.CLOSE.DEL")

**** CHECK FOR OFS IN ****
OFS.CHECK:

    KEY.LIST2="" ; SELECTED2="" ;  ER.OFS="" ; FLAG = 0
    T.SEL2 = "SELECT F.OFS.MESSAGE.QUEUE"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.OFS)
    IF SELECTED2 THEN
        PRINT ' OFS STILL RUNNING PLEASE WAIT'
        SLEEP 60
        GO TO OFS.CHECK
    END ELSE
        PRINT 'OFS FINISHED PROGRAM CONTINUE'
**************************
        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBD.DRMNT.FILES.FILL")
        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBD.DRMNT.ACTIVE.FILL")

        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBW.CU.SCB.POSTIONG")

        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBM.CREDIT.CBE.K2")
        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBR.CBE.CREDIT.HO.FILL")
        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBR.CBE.HO.003")
        CALL FSCB.RUN.JOB(WS.P,WS.W,WS.A,"SBR.CBE.HO.003.ALL")
    END
    RETURN

END
