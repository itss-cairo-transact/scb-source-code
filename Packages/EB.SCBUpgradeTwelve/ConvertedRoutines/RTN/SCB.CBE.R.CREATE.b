* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>511</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.CBE.R.CREATE

*   PROGRAM  SCB.CBE.R.CREATE

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.REPORT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

**--------------------------------------------------------------------**

    F.CBE = '' ; FN.CBE = 'F.SCB.CBE.REPORT'     ; R.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    F.DR  = '' ; FN.DR = 'FBNK.DRAWINGS'         ; R.DR = ''
    CALL OPF(FN.DR,F.DR)

    F.FT  = '' ; FN.FT = 'FBNK.FUNDS.TRANSFER'   ; R.FT = ''
    CALL OPF(FN.FT,F.FT)

    F.ACC = '' ; FN.ACC = 'FBNK.ACCOUNT'         ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    F.CU  = '' ; FN.CU = 'FBNK.CUSTOMER'         ; R.CU = ''
    CALL OPF(FN.CU,F.CU)

    NO1  = 0 ; NO2  = 0 ; NO3  = 0 ; NO4  = 0 ; NO5  = 0 ; NO6  = 0
    AMT1 = 0 ; AMT2 = 0 ; AMT3 = 0 ; AMT4 = 0 ; AMT5 = 0 ; AMT6 = 0

    SORT1 = ""
    SORT2 = ""
    AMT1  = ""
    DESC  = ""
**=====================================================================**
**              --------------------------------                       **
**--------------------------   DRAWINGS  ------------------------------**
**              --------------------------------                       **

    SELECTED = ""
    ER.MSG   = ""
    DD       = TODAY
    D.T      = TODAY[3,6]:"..."
    T.SEL = "SELECT FBNK.DRAWINGS WITH (( DRAWING.TYPE LIKE M... AND MANUAL.MATURITY EQ ": DD :") OR ( DRAWING.TYPE EQ SP AND DATE.TIME LIKE ": D.T :" )) "

**DEBUG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED

        CALL F.READ(FN.DR,KEY.LIST<I>,R.DR,F.DR,READ.ERR)

        CURR  = ''
        AC1   = ""
        AC2   = ""
        CUR1  = ""
        CUR2  = ""
        R.CBE = ""

        AC1  = R.DR<TF.DR.DRAWDOWN.ACCOUNT>
        AC2  = R.DR<TF.DR.PAYMENT.ACCOUNT>

        CALL F.READ(FN.ACC,AC1,R.ACC1,F.ACC,READ.ERR1)
        CALL F.READ(FN.ACC,AC2,R.ACC2,F.ACC,READ.ERR1)

        CUR1 = R.ACC1<AC.CURRENCY>
        CUR2 = R.ACC2<AC.CURRENCY>
        CATT = R.ACC2<AC.CATEGORY>

**-------------------------                 --------------------------**
**-----                    DRAWINGS  OUT                       -------**
**------------------------                  --------------------------**

        IF AC2[1,3] EQ '994' THEN
            IF CATT  EQ '5001' THEN
***-------------------"�� ���� ������ ���� ������" -----------------**

                IF CUR1 EQ CUR2 THEN
                    CURR        = CUR2
                    NOM1        = '1'
                    DESC        = "��� ������"

                    BEGIN CASE

                    CASE CUR2  EQ 'USD'
                        SORT1 = 1
                        SORT2 = 1

                    CASE CUR2  EQ 'EUR'
                        SORT1 = 2
                        SORT2 = 1

                    CASE CUR2  EQ 'GBP'
                        SORT1 = 3
                        SORT2 = 1
                    CASE OTHERWISE

                        SORT1 = 4
                        SORT2 = 1
                        CURR  = "OTHR"
                    END CASE

                    AMT1 =  R.DR<TF.DR.DOC.AMT.LOCAL>


                    R.CBE<CBE.R.SORT.1>        = SORT1
                    R.CBE<CBE.R.SORT.2>        = SORT2
                    R.CBE<CBE.R.CURR>          = CURR
                    R.CBE<CBE.R.DESC>          = DESC
                    R.CBE<CBE.R.NO.1>          = NOM1
                    R.CBE<CBE.R.AMT.1>         = AMT1
                    R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST<I> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CBE
**END
                    CALL F.WRITE(FN.CBE,KEY.LIST<I>,R.CBE)
                END

***-------------------------"����� ����" ------------------------------**


                IF CUR1 NE CUR2 THEN
                    CURR        = CUR2
                    NOM1        = '1'
                    DESC        = "����� ����"

                    BEGIN CASE

                    CASE CUR2  EQ 'USD'
                        SORT1 = 1
                        SORT2 = 2

                    CASE CUR2  EQ 'EUR'
                        SORT1 = 2
                        SORT2 = 2

                    CASE CUR2  EQ 'GBP'
                        SORT1 = 3
                        SORT2 = 2

                    CASE OTHERWISE

                        SORT1 = 4
                        SORT2 = 2
                        CURR  = "OTHR"
                    END CASE

                    AMT1 =  R.DR<TF.DR.DOC.AMT.LOCAL>


                    R.CBE<CBE.R.SORT.1>        = SORT1
                    R.CBE<CBE.R.SORT.2>        = SORT2
                    R.CBE<CBE.R.CURR>          = CURR
                    R.CBE<CBE.R.DESC>          = DESC
                    R.CBE<CBE.R.NO.1>          = NOM1
                    R.CBE<CBE.R.AMT.1>         = AMT1
                    R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST<I> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CBE
**END
                    CALL F.WRITE(FN.CBE,KEY.LIST<I>,R.CBE)
                END
            END
        END
**----------------------   END   DRAWINGS  OUT   -------------------**


**-------------------------                 --------------------------**
**-----                    DRAWINGS  IN                        -------**
**------------------------                  --------------------------**

        IF AC1[1,3] EQ '994' THEN
*PRINT  KEY.LIST<I>
*PRINT  AC1
*     DEBUG
            CATT = R.ACC1<AC.CATEGORY>
            IF CATT  EQ '5001' THEN

***-------------------"�� ���� ������ ���� ������" -----------------**


                IF CUR1 EQ CUR2 THEN

                    CURR        = CUR1
                    NOM1        = '1'
                    DESC        = "��� ������"
                    BEGIN CASE

                    CASE CUR1  EQ 'USD'
                        SORT1 = 1
                        SORT2 = 1

                    CASE CUR1  EQ 'EUR'
                        SORT1 = 2
                        SORT2 = 1

                    CASE CUR1  EQ 'GBP'
                        SORT1 = 3
                        SORT2 = 1
                    CASE OTHERWISE

                        SORT1 = 4
                        SORT2 = 1
                        CURR  = "OTHR"
                    END CASE

                    AMT1 =  R.DR<TF.DR.DOC.AMT.LOCAL>


                    R.CBE<CBE.R.SORT.1>        = SORT1
                    R.CBE<CBE.R.SORT.2>        = SORT2
                    R.CBE<CBE.R.CURR>          = CURR
                    R.CBE<CBE.R.DESC>          = DESC
                    R.CBE<CBE.R.NO.4>          = NOM1
                    R.CBE<CBE.R.AMT.4>         = AMT1
                    R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST<I> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CBE
**END
                    CALL F.WRITE(FN.CBE,KEY.LIST<I>,R.CBE)
                END
***-------------------------  "����� ����" ---------------------------**

                IF CUR1 NE CUR2 THEN
                    CURR        = CUR1
                    NOM1        = '1'
                    DESC        = "����� ����"

                    BEGIN CASE

                    CASE CUR1  EQ 'USD'
                        SORT1 = 1
                        SORT2 = 2

                    CASE CUR1  EQ 'EUR'
                        SORT1 = 2
                        SORT2 = 2

                    CASE CUR1  EQ 'GBP'
                        SORT1 = 3
                        SORT2 = 2

                    CASE OTHERWISE

                        SORT1 = 4
                        SORT2 = 2
                        CURR  = "OTHR"
                    END CASE

                    AMT1 =  R.DR<TF.DR.DOC.AMT.LOCAL>


                    R.CBE<CBE.R.SORT.1>        = SORT1
                    R.CBE<CBE.R.SORT.2>        = SORT2
                    R.CBE<CBE.R.CURR>          = CURR
                    R.CBE<CBE.R.DESC>          = DESC
                    R.CBE<CBE.R.NO.4>          = NOM1
                    R.CBE<CBE.R.AMT.4>         = AMT1
                    R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST<I> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CBE
**END
                    CALL F.WRITE(FN.CBE,KEY.LIST<I>,R.CBE)
                END
            END
        END

**-----------------   END   DRAWINGS  IN   ----------------------**

    NEXT I

**=====================================================================**
**              --------------------------------                       **
**-----------------------   FUND.TRANSFER  ----------------------------**
**              --------------------------------

    SELECTED.FT = ""
    ER.MSG.FT   = ""

    T.SEL.FT    = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE NE AC43 "

    CALL EB.READLIST(T.SEL.FT,KEY.LIST.FT,"",SELECTED.FT,ER.MSG.FT)
    FOR II = 1 TO SELECTED.FT

        CALL F.READ(FN.FT,KEY.LIST.FT<II>,R.FT,F.FT,READ.ERR)

        CURR  = ''
        AC11  = ""
        AC22  = ""
        CUR11 = ""
        CUR22 = ""
        R.CBE = ""

        AC11  = R.FT<FT.DEBIT.ACCT.NO>
        AC22  = R.FT<FT.CREDIT.ACCT.NO>

        CALL F.READ(FN.ACC,AC11,R.ACC11,F.ACC,READ.ERR11)
        CALL F.READ(FN.ACC,AC22,R.ACC22,F.ACC,READ.ERR11)

        CUR11 = R.ACC11<AC.CURRENCY>
        CUR22 = R.ACC22<AC.CURRENCY>
        CAT11 = R.ACC11<AC.CATEGORY>
        CAT22 = R.ACC22<AC.CATEGORY>
        CUS11 = R.ACC11<AC.CUSTOMER>
        CUS22 = R.ACC22<AC.CUSTOMER>

        CALL F.READ(FN.CU,CUS11,R.CU11,F.CU,READ.ERR11)
        CALL F.READ(FN.CU,CUS22,R.CU22,F.CU,READ.ERR11)

        SEC11 = R.CU11<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
        SEC22 = R.CU22<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

**-------------------------                 --------------------------**
**-----                        FT . OUT                        -------**
**------------------------                  --------------------------**

        IF AC22[1,3] EQ '994' THEN
            IF CAT22 EQ '5001' THEN
*PRINT AC22  :"*": AC11
                IF AC11[1,3] NE '994' THEN
***------------------- "�� ���� ������ ���� ������" -----------------**
                    IF CUR11 EQ CUR22 THEN
                        IF CUR11 NE 'EGP' THEN

                            CURR        = CUR22
                            NOM11       = '1'
                            DESC        = "��� ������"

                            BEGIN CASE

                            CASE CUR22  EQ 'USD'
                                SORT1 = 1
                                SORT2 = 1

                            CASE CUR22  EQ 'EUR'
                                SORT1 = 2
                                SORT2 = 1

                            CASE CUR22  EQ 'GBP'
                                SORT1 = 3
                                SORT2 = 1
                            CASE OTHERWISE

                                SORT1 = 4
                                SORT2 = 1
                                CURR  = "OTHR"
                            END CASE

                            AMT11 =  R.FT<FT.LOC.AMT.CREDITED>

                            R.CBE<CBE.R.SORT.1>        = SORT1
                            R.CBE<CBE.R.SORT.2>        = SORT2
                            R.CBE<CBE.R.CURR>          = CURR
                            R.CBE<CBE.R.DESC>          = DESC

                            IF SEC11 EQ '4650' THEN
                                R.CBE<CBE.R.NO.3>          = NOM11
                                R.CBE<CBE.R.AMT.3>         = AMT11
                            END ELSE
                                R.CBE<CBE.R.NO.2>          = NOM11
                                R.CBE<CBE.R.AMT.2>         = AMT11
                            END

                            R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST.FT<II> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST.FT<II>:"TO" :FN.CBE
**END
                            CALL F.WRITE(FN.CBE,KEY.LIST.FT<II>,R.CBE)
                        END
                    END

***-------------------------"����� ����" ------------------------------**


                    IF CUR11 NE CUR22 THEN
                        CURR        = CUR22
                        NOM11       = '1'
                        DESC        = "����� ����"

                        BEGIN CASE

                        CASE CUR22  EQ 'USD'
                            SORT1 = 1
                            SORT2 = 2

                        CASE CUR22  EQ 'EUR'
                            SORT1 = 2
                            SORT2 = 2

                        CASE CUR22  EQ 'GBP'
                            SORT1 = 3
                            SORT2 = 2

                        CASE OTHERWISE

                            SORT1 = 4
                            SORT2 = 2
                            CURR  = "OTHR"
                        END CASE

                        AMT11 =  R.FT<FT.LOC.AMT.CREDITED>


                        R.CBE<CBE.R.SORT.1>        = SORT1
                        R.CBE<CBE.R.SORT.2>        = SORT2
                        R.CBE<CBE.R.CURR>          = CURR
                        R.CBE<CBE.R.DESC>          = DESC

                        IF SEC11 EQ '4650' THEN
                            R.CBE<CBE.R.NO.3>          = NOM11
                            R.CBE<CBE.R.AMT.3>         = AMT11
                        END ELSE
                            R.CBE<CBE.R.NO.2>          = NOM11
                            R.CBE<CBE.R.AMT.2>         = AMT11
                        END
                        R.CBE<CBE.R.SYS.DATE>      = TODAY
**WRITE  R.CBE TO F.CBE , KEY.LIST.FT<II> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST.FT<II>:"TO" :FN.CBE
**END
                        CALL F.WRITE(FN.CBE,KEY.LIST.FT<II>,R.CBE)
                    END
                END
            END
        END

**----------------------     END FT.OUT            -------------------**

**-------------------------                   ------------------------**
**-----                        FT . IN                         -------**
**------------------------                  --------------------------**

        IF AC22[1,3] NE '994' THEN
            IF CAT11 EQ '5001' THEN
                IF AC11[1,3] EQ '994' THEN

***-------------------"�� ���� ������ ���� ������" -----------------**

                    IF CUR11 EQ CUR22 THEN
                        IF CUR11 NE 'EGP' THEN

                            CURR        = CUR11
                            NOM11       = '1'
                            DESC        = "��� ������"

                            BEGIN CASE

                            CASE CUR11  EQ 'USD'
                                SORT1 = 1
                                SORT2 = 1

                            CASE CUR11  EQ 'EUR'
                                SORT1 = 2
                                SORT2 = 1

                            CASE CUR11  EQ 'GBP'
                                SORT1 = 3
                                SORT2 = 1
                            CASE OTHERWISE

                                SORT1 = 4
                                SORT2 = 1
                                CURR  = "OTHR"
                            END CASE

                            AMT11 =  R.FT<FT.LOC.AMT.CREDITED>

                            R.CBE<CBE.R.SORT.1>        = SORT1
                            R.CBE<CBE.R.SORT.2>        = SORT2
                            R.CBE<CBE.R.CURR>          = CURR
                            R.CBE<CBE.R.DESC>          = DESC

                            IF SEC22 EQ '4650' THEN
                                R.CBE<CBE.R.NO.6>          = NOM11
                                R.CBE<CBE.R.AMT.6>         = AMT11
                            END ELSE
                                R.CBE<CBE.R.NO.5>          = NOM11
                                R.CBE<CBE.R.AMT.5>         = AMT11
                            END
                            R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST.FT<II> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST.FT<II>:"TO" :FN.CBE
**END
                            CALL F.WRITE(FN.CBE,KEY.LIST.FT<II>,R.CBE)
                        END
                    END
***-------------------------"����� ����" ------------------------------**


                    IF CUR11 NE CUR22 THEN
                        CURR        = CUR11
                        NOM11       = '1'
                        DESC        = "����� ����"

                        BEGIN CASE

                        CASE CUR11  EQ 'USD'
                            SORT1 = 1
                            SORT2 = 2

                        CASE CUR11  EQ 'EUR'
                            SORT1 = 2
                            SORT2 = 2

                        CASE CUR11  EQ 'GBP'
                            SORT1 = 3
                            SORT2 = 2

                        CASE OTHERWISE

                            SORT1 = 4
                            SORT2 = 2
                            CURR  = "OTHR"
                        END CASE

                        AMT11 =  R.FT<FT.LOC.AMT.CREDITED>


                        R.CBE<CBE.R.SORT.1>        = SORT1
                        R.CBE<CBE.R.SORT.2>        = SORT2
                        R.CBE<CBE.R.CURR>          = CURR
                        R.CBE<CBE.R.DESC>          = DESC

                        IF SEC22 EQ '4650' THEN
                            R.CBE<CBE.R.NO.6>          = NOM11
                            R.CBE<CBE.R.AMT.6>         = AMT11
                        END ELSE
                            R.CBE<CBE.R.NO.5>          = NOM11
                            R.CBE<CBE.R.AMT.5>         = AMT11
                        END
                        R.CBE<CBE.R.SYS.DATE>      = TODAY

**WRITE  R.CBE TO F.CBE , KEY.LIST.FT<II> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST.FT<II>:"TO" :FN.CBE
**END
                        CALL F.WRITE(FN.CBE,KEY.LIST.FT<II>,R.CBE)
                    END
                END
            END
        END

**-----------------      END   FT  .  IN        ----------------------**

    NEXT II

    RETURN
END
