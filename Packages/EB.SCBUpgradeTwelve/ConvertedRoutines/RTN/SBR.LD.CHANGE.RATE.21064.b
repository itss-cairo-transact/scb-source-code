* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.LD.CHANGE.RATE.21064

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ��������  '

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN

        GOSUB INITIALISE
        GOSUB BUILD.RECORD

        TEXT = "�� �������� �� ��������" ; CALL REM

    END ELSE
        TEXT = '�� ������ �� �������� ' ; CALL REM
    END

    RETURN
*---------------------------------------
INITIALISE:

    YTEXT = "����� ����� ������ ������ ����� �� ������� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "6", "A")
    WS.RATE.PER = COMI

    OPENSEQ "MECH" , "LD.21064.RATE.CHANGE" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LD.21064.RATE.CHANGE"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LD.21064.RATE.CHANGE" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LD.21064.RATE.CHANGE CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LD.21064.RATE.CHANGE File IN MECH'
        END
    END

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    COMMA = ","
    V.DATE = TODAY

    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21064 AND INT.RATE.TYPE EQ 1 AND STATUS NE LIQ AND LOAN.TYPE IN (318 319 320 321 322 323 324)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            WS.NEW.RATE = R.LD<LD.INTEREST.RATE> + WS.RATE.PER
            WS.COMP     = R.LD<LD.CO.CODE>
            WS.COMP1    = WS.COMP[2]
            VER.NAME='RATE.MORT'
            IDD = 'LD.LOANS.AND.DEPOSITS,RATE.MORT,INPUTT':WS.COMP1:'//':WS.COMP:',':KEY.LIST<I>
**************************************************CRAETE FT BY OFS**********************************************
            OFS.MESSAGE.DATA  =  "NEW.INT.RATE=":WS.NEW.RATE:COMMA
            OFS.MESSAGE.DATA  := "LOCAL.REF:6:1=?":VER.NAME:COMMA
            OFS.MESSAGE.DATA :=  "INT.RATE.V.DATE=":V.DATE:COMMA

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LD.21064.RATE.CHANGE'
    EXECUTE 'DELETE ':"MECH":' ':"LD.21064.RATE.CHANGE"

    RETURN
END
