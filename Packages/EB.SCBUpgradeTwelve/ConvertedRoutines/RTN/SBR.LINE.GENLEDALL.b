* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.LINE.GENLEDALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LINE.GENLEDALL
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LINE.GENDETALL
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENDETALL.FILE
*--------------- Modification History-----------------------------
* 29-May-2016  ITSS  R15UPG20160529 Used correct file handler for CLEARFILE statement
********************** CLEAR FILES ********************************

    FN.LED = "F.SCB.LINE.GENLEDALL" ; F.LED = ''
    CALL OPF(FN.LED,F.LED)

*OPEN FN.LED TO FILEVAR ELSE ABORT 201, FN.LED
* R15UPG20160529 - S
*    CLEARFILE FILEVAR
    CLEARFILE F.LED
* R15UPG20160529 - E
    PRINT "GENLEDALL FILE IS CLEARED"


    FN.DET = "F.SCB.LINE.GENDETALL" ; F.LED = ''
    CALL OPF(FN.DET,F.DET)

*OPEN FN.DET TO FILEVAR ELSE ABORT 201, FN.DET
* R15UPG20160529 - S
*    CLEARFILE FILEVAR
    CLEARFILE F.DET
* R15UPG20160529 - E
    PRINT "GENDETALL FILE IS CLEARED"

*********************** OPENING FILES *****************************
    FN.LN   = "F.RE.STAT.REP.LINE"   ; F.LN    = "" ; R.LN = ""
    FN.SLN  = "F.SCB.LINE.GENLEDALL" ; F.SLN   = "" ; R.SLN = ""
    FN.DLN  = "F.SCB.LINE.GENDETALL" ; F.DLN   = "" ; R.DLN = ""
    FN.GLN  = "F.SCB.GENDETALL.FILE" ; F.GLN   = "" ; R.GLN = ""

    CALL OPF (FN.LN,F.LN)
    CALL OPF (FN.SLN,F.SLN)
    CALL OPF (FN.DLN,F.DLN)
    CALL OPF (FN.GLN,F.GLN)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*------------------------------------------------------------------
****** GENLEDALL EGP ************
    T.SEL = "SELECT ":FN.LN:" WITH @ID LIKE GENLEDALL.... AND PROFT.APPLIC.ID EQ PL BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        LN.ID = 0
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            LN.CUR = R.LN<RE.SRL.PROFT.CURRENCY><1,1>
********************** COPY NEW DATA ******************************
            IF LN.CUR EQ 'EGP' THEN
                LN.ID++
                CALL F.READ(FN.SLN,LN.ID,R.SLN,F.SLN,E2)
                R.SLN<LALL.EGP.ID> = KEY.LIST<I>

                CALL F.WRITE(FN.SLN,LN.ID,R.SLN)
                CALL  JOURNAL.UPDATE(LN.ID)
            END
        NEXT I
    END

*------------------------------------------------------------------
****** GENLEDALL FCY ************
    T.SEL = "SELECT ":FN.LN:" WITH @ID LIKE GENLEDALL.... AND PROFT.APPLIC.ID EQ PL BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        LN.ID = 0
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            LN.CUR = R.LN<RE.SRL.PROFT.CURRENCY><1,1>
********************** COPY NEW DATA ******************************
            IF LN.CUR NE 'EGP' THEN
                LN.ID++
                CALL F.READ(FN.SLN,LN.ID,R.SLN,F.SLN,E2)
                R.SLN<LALL.FCY.ID> = KEY.LIST<I>
                CALL F.WRITE(FN.SLN,LN.ID,R.SLN)
                CALL  JOURNAL.UPDATE(LN.ID)
            END
        NEXT I
    END

*------------------------------------------------------------------
****** GENDETALL EGP ************
**  T.SEL = "SELECT ":FN.LN:" WITH @ID LIKE GENDETALL... AND PROFT.APPLIC.ID EQ PL BY @ID"
    T.SEL = "SELECT ":FN.GLN:" WITH APPLIC.ID EQ 'Profit.Loss' BY SORT.NO"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        LN.ID = 0
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            LN.CUR = R.LN<RE.SRL.PROFT.CURRENCY><1,1>
********************** COPY NEW DATA ******************************
            IF LN.CUR EQ 'EGP' THEN
                LN.ID++
                CALL F.READ(FN.DLN,LN.ID,R.DLN,F.DLN,E2)
                R.DLN<DALL.EGP.ID> = KEY.LIST<I>
                CALL F.WRITE(FN.DLN,LN.ID,R.DLN)
                CALL  JOURNAL.UPDATE(LN.ID)
            END
        NEXT I
    END
*------------------------------------------------------------------
****** GENDETALL FCY ************
**  T.SEL = "SELECT ":FN.LN:" WITH @ID LIKE GENDETALL... AND PROFT.APPLIC.ID EQ PL BY @ID"
    T.SEL = "SELECT ":FN.GLN:" WITH APPLIC.ID EQ 'Profit.Loss' BY SORT.NO"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        LN.ID = 0
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            LN.CUR = R.LN<RE.SRL.PROFT.CURRENCY><1,1>
********************** COPY NEW DATA ******************************
            IF LN.CUR NE 'EGP' THEN
                LN.ID++
                CALL F.READ(FN.DLN,LN.ID,R.DLN,F.DLN,E2)
                R.DLN<DALL.FCY.ID> = KEY.LIST<I>
                CALL F.WRITE(FN.DLN,LN.ID,R.DLN)
                CALL  JOURNAL.UPDATE(LN.ID)
            END
        NEXT I
    END
END
