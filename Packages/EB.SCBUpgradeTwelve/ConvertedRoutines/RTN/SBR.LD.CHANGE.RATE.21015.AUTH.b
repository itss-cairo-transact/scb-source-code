* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.LD.CHANGE.RATE.21015.AUTH

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*---------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    TEXT = "�� �������� �� ��������" ; CALL REM

    RETURN
*---------------------------------------
INITIALISE:

    OPENSEQ "MECH" , "LD.21015.RATE.CHANGE.AUTH" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LD.21015.RATE.CHANGE.AUTH"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LD.21015.RATE.CHANGE.AUTH" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LD.21015.RATE.CHANGE.AUTH CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LD.21015.RATE.CHANGE.AUTH File IN MECH'
        END
    END

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21015 AND STATUS NE LIQ"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            WS.COMP      = R.LD<LD.CO.CODE>
            WS.COMP.USER = WS.COMP[2]

            MSG.DATA = 'LD.LOANS.AND.DEPOSITS,RATE/A,AUTHOR':WS.COMP.USER:'//':WS.COMP:',':KEY.LIST<I>

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LD.21015.RATE.CHANGE.AUTH'
    EXECUTE 'DELETE ':"MECH":' ':"LD.21015.RATE.CHANGE.AUTH"

    RETURN
END
