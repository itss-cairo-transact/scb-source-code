* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
***NESSREEN AHMED 14/04/2019*********

    SUBROUTINE SCB.CASH.PAYMENT.B3

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BRSIN.FLAG
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.SETT

* A ROUTINE TO CREATE A TEXT FILE CONTANING ALL THE CASH PAYMENT THAT HAPPENED THROUGH THE DAY

    DIR.NAME = '&SAVEDLISTS&'

*Line [ 55 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR( 'SCB.BRSIN.FLAG':@FM:BRSIN.FLAG.COUNT, "BRSIN-B3" , FLAG)
F.ITSS.SCB.BRSIN.FLAG = 'F.SCB.BRSIN.FLAG'
FN.F.ITSS.SCB.BRSIN.FLAG = ''
CALL OPF(F.ITSS.SCB.BRSIN.FLAG,FN.F.ITSS.SCB.BRSIN.FLAG)
CALL F.READ(F.ITSS.SCB.BRSIN.FLAG,"BRSIN-B3",R.ITSS.SCB.BRSIN.FLAG,FN.F.ITSS.SCB.BRSIN.FLAG,ERROR.SCB.BRSIN.FLAG)
FLAG=R.ITSS.SCB.BRSIN.FLAG<BRSIN.FLAG.COUNT>
    SER = FLAG + 1
**************************************************
    SER.F = FMT(SER, "R%4")

    NEW.FILE = "BRSIN-B3-":SER.F:".CAP"
    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
******************************************************************
**********************FILLING DATA********************************
******************************************************************
    TDATEE = '' ; SDAT = '' ; TR.DATE = '' ; ST.DATE.TIM = ''
    TDATEE = TODAY
    RQTIME = '1500'
    SDAT = TDATEE[3,6]:RQTIME
    TOT.NO.TT = 0 ; TOT.NO.FT1 = 0 ; TOT.NO.FT2 = 0 ; TOT.NO.SET = 0

    T.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE EQ 37 AND DATE.TIME LE ": SDAT
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CARD.NO = '' ; EMB.NAME = '' ;  CURR = '' ; AMT = '' ; PAY.DATE = '' ; REF = ''
            FN.TELLER = 'F.TELLER' ; F.TELELR = '' ; R.TELLER = '' ; RETRY1 = '' ; E1 = ''
            KEY.TO.USE = KEY.LIST<I>
            CALL OPF(FN.TELLER,F.TELLER)
            CALL F.READ(FN.TELLER,  KEY.TO.USE, R.TELLER, F.TELLER, E1)

            LOCAL.REF = R.TELLER<TT.TE.LOCAL.REF>
            VISA.NO = LOCAL.REF<1,TTLR.VISA.NUMBER>
            CARD.NO = VISA.NO:STR(' ',25 - LEN(VISA.NO))
            ACCT = R.TELLER<TT.TE.ACCOUNT.1>
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'ACCOUNT':@FM:AC.CUSTOMER, ACCT , CUST)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUST=R.ITSS.ACCOUNT<AC.CUSTOMER>

            FN.CUSTOMER = 'F.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; E1 = ''
            CALL OPF(FN.CUSTOMER,F.CUSTOMER)
            CALL F.READ(FN.CUSTOMER, CUST, R.CUSTOMER, F.CUSTOMER, E3)

            EMB.NA = R.CUSTOMER<EB.CUS.SHORT.NAME>
            EMB.NAME = STR(' ',30)
            REFEREN = 'CREDIT PAYMENT'
            REF = REFEREN:STR(' ',30 - LEN(REFEREN))
            CURR = R.TELLER<TT.TE.CURRENCY.1>
            AMTT = R.TELLER<TT.TE.NET.AMOUNT>

            N1 = FIELD(AMTT, ".", 1)
            N2 = FIELD(AMTT, ".", 2)
            N3 = N1:N2
            AMT = FMT(N3, "R%12")

***********************************************************
       **   TOTAMT = TOTAMT + AMT
            TR.DATE = R.TELLER<TT.TE.DATE.TIME>
            PAY.DATE = TODAY

***************FILLING BRIDGE******************************************************************************
            AT.DATET = TR.DATE<1,1>
            IF AT.DATET LE SDAT THEN
**************************************************************************

                VISA.DATA = '1'
                VISA.DATA := CARD.NO
                VISA.DATA := EMB.NAME
                VISA.DATA := REF
                VISA.DATA := CURR
                VISA.DATA := AMT
                VISA.DATA := 'N'
                VISA.DATA := PAY.DATE
                VISA.DATA := STR("0",25)
                VISA.DATA := '0001'
                VISA.DATA := '0'
                VISA.DATA := STR("0",28)
                VISA.DATA := STR("0",28)

                DIM ZZ(SELECTED)
                ZZ(SELECTED) = VISA.DATA

                WRITESEQ ZZ(SELECTED) TO V.FILE.IN ELSE
                    PRINT  'CAN NOT WRITE LINE ':ZZ(SELECTED)
                END
***
                TOTAMT = TOTAMT + AMT
                TOT.NO.TT = TOT.NO.TT + 1
            END     ;*End of IF AT.DATET LE SDAT
***
        NEXT I
    END
***********************************************************************************************
***************FT SELECTION********************************************************************
**************************************************************************
    IDDD = "EG0010001"
*Line [ 176 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,IDDD,LAST.W.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LAST.W.DAY=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

**TTT****END OF UPDATE***************
**  N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVC' AND PROCESSING.DATE EQ ": LAST.W.DAY
    N.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVC' AND PROCESSING.DATE EQ ":TDATEE :" AND DATE.TIME LE ": SDAT
    KEY.LIST.N=""
    SELECTED.N=""
    ER.MSG.N=""

    CALL EB.READLIST(N.SEL,KEY.LIST.N,"",SELECTED.N,ER.MSG.N)
    IF SELECTED.N THEN
        FOR X=1 TO SELECTED.N
            FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
            FT.ID = KEY.LIST.N<X>
            CALL OPF(FN.FT,F.FT)
            CALL F.READ(FN.FT,  FT.ID, R.FT, F.FT, E2)

            LOCAL.REF.FT = R.FT<FT.LOCAL.REF>
            VISA.NO.FT = LOCAL.REF.FT<1,FTLR.VISA.NO>
            CARD.NO.FT = VISA.NO.FT:STR(' ',25 - LEN(VISA.NO.FT))
            CUST.FT = R.FT<FT.CREDIT.CUSTOMER>

            FN.CUSTOMER.FT = 'F.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
            CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
            CALL F.READ(FN.CUSTOMER.FT, CUST.FT, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)

            EMB.NA.FT = R.CUSTOMER.FT<EB.CUS.SHORT.NAME>
            EMB.NAME.FT = STR(' ',30)
            REFEREN = 'CREDIT PAYMENT'
            REF.FT = REFEREN:STR(' ',30 - LEN(REFEREN))
            CURR.FT = R.FT<FT.CREDIT.CURRENCY>
********Updated by Nessreen Ahmed 8/11/2021**************************
********   AMTT.FT = R.FT<FT.DEBIT.AMOUNT>
           AMTT.FT = R.FT<FT.LOC.AMT.CREDITED>
********End of Update 8/11/2021**************************************
            N1.FT = FIELD(AMTT.FT, ".", 1)
            N2.FT = FIELD(AMTT.FT, ".", 2)
            N3.FT = N1.FT:N2.FT
            AMT.FT = FMT(N3.FT, "R%12")

    **      TOT.FT = TOT.FT + AMT.FT
            PAY.DATE.FT = R.FT<FT.AUTH.DATE>
***************FILLING BRIDGE FOR FT REC*********************************************************************
            FT.DATETIM = R.FT<FT.DATE.TIME>
            FT.AUTHDT = FT.DATETIM<1,1>
            IF FT.AUTHDT LE SDAT THEN
**************************************************************************
                VISA.DATA.FT = '1'
                VISA.DATA.FT := CARD.NO.FT
                VISA.DATA.FT := EMB.NAME.FT
                VISA.DATA.FT := REF.FT
                VISA.DATA.FT := CURR.FT
                VISA.DATA.FT := AMT.FT
                VISA.DATA.FT := 'N'
                VISA.DATA.FT := PAY.DATE.FT
                VISA.DATA.FT := STR("0",25)
                VISA.DATA.FT := '0001'
                VISA.DATA.FT := '0'
                VISA.DATA.FT := STR("0",28)
                VISA.DATA.FT := STR("0",28)

                AA = SELECTED+SELECTED.N+1
                DIM NN(AA)
                NN(AA) = VISA.DATA.FT

                WRITESEQ NN(AA) TO V.FILE.IN ELSE
                    PRINT  'CAN NOT WRITE LINE ':NN(AA)
                END
                TOT.FT = TOT.FT + AMT.FT
                TOT.NO.FT1 = TOT.NO.FT1 + 1
            END     ;*End of IF SDAT
        NEXT X
    END
***********************************************************************************************
******UPDATED ON 29/10/2008********************************************************************
***************DEBIT PAYMENT FOR SETTLEMENT********************************************************************
*****UPDATED ON 18/11/2008********************************************
**  S.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVS' "
*** S.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVS' AND PROCESSING.DATE EQ ": LAST.W.DAY
    S.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACVS' AND PROCESSING.DATE EQ ": TDATEE : " AND DATE.TIME LE ": SDAT
    KEY.LIST.S=""
    SELECTED.S=""
    ER.MSG.S=""

    CALL EB.READLIST(S.SEL,KEY.LIST.S,"",SELECTED.S,ER.MSG.S)
    IF SELECTED.S THEN
        FOR SS=1 TO SELECTED.S
            FN.FT = 'F.FUNDS.TRANSFER' ; F.FT = '' ; R.FT = '' ; RETRY2= '' ; E2 = ''
            FT.ID.S = KEY.LIST.S<SS>
            CALL OPF(FN.FT,F.FT)
            CALL F.READ(FN.FT,  FT.ID.S, R.FT, F.FT, E2)

            LOCAL.REF.FT.S = R.FT<FT.LOCAL.REF>
            VISA.NO.FT.S = LOCAL.REF.FT.S<1,FTLR.VISA.NO>
            CARD.NO.FT.S = VISA.NO.FT.S:STR(' ',25 - LEN(VISA.NO.FT.S))
            CUST.FT.S = R.FT<FT.CREDIT.CUSTOMER>

            FN.CUSTOMER.FT = 'F.CUSTOMER' ; F.CUSTOMER.FT = '' ; R.CUSTOMER.FT = '' ; RETRY.FT = '' ; E.FT = ''
            CALL OPF(FN.CUSTOMER.FT,F.CUSTOMER.FT)
            CALL F.READ(FN.CUSTOMER.FT, CUST.FT.S, R.CUSTOMER.FT, F.CUSTOMER.FT, E.FT)

            EMB.NA.FT.S = R.CUSTOMER.FT<EB.CUS.SHORT.NAME>
            EMB.NAME.FT.S = STR(' ',30)

            REFEREN.S = 'DEBIT PAYMENT'
            REF.FT.S = REFEREN.S:STR(' ',30 - LEN(REFEREN.S))
            CURR.FT.S = R.FT<FT.CREDIT.CURRENCY>
            AMTT.FT.S = R.FT<FT.DEBIT.AMOUNT>

            N1.FT.S = FIELD(AMTT.FT.S, ".", 1)
            N2.FT.S = FIELD(AMTT.FT.S, ".", 2)
            N3.FT.S = N1.FT.S:N2.FT.S
            AMT.FT.S = FMT(N3.FT.S, "R%12")

        **  TOT.FT.S = TOT.FT.S + AMT.FT.S
            PAY.DATE.FT.S = R.FT<FT.AUTH.DATE>
***************FILLING BRIDGE FOR FT REC*********************************************************************
            FT.DATETIM = R.FT<FT.DATE.TIME>
            FT.AUTHDT = FT.DATETIM<1,1>
            IF FT.AUTHDT LE SDAT THEN

                VISA.DATA.FT.S = '1'
                VISA.DATA.FT.S := CARD.NO.FT.S
                VISA.DATA.FT.S := EMB.NAME.FT.S
                VISA.DATA.FT.S := REF.FT.S
                VISA.DATA.FT.S := CURR.FT.S
                VISA.DATA.FT.S := AMT.FT.S
                VISA.DATA.FT.S := 'Y'
                VISA.DATA.FT.S := PAY.DATE.FT.S
                VISA.DATA.FT.S := STR("0",25)
                VISA.DATA.FT.S := '0001'
                VISA.DATA.FT.S := '0'
                VISA.DATA.FT.S := STR("0",28)
                VISA.DATA.FT.S := STR("0",28)

                SP = SELECTED+SELECTED.N+SELECTED.S+1
                DIM MM(SP)
                MM(SP) = VISA.DATA.FT.S

                WRITESEQ MM(SP) TO V.FILE.IN ELSE
                    PRINT  'CAN NOT WRITE LINE ':MM(SP)
                END
                TOT.FT.S = TOT.FT.S + AMT.FT.S
                TOT.NO.FT2 = TOT.NO.FT2 + 1
            END
        NEXT SS
    END
***********************************************************************************************
***********************************************************************************************
***************CREDIT PAYMENT OR DEBIT PAYMENT FOR SETTLEMENT***************************************************
**SET.SEL = "SELECT F.SCB.VISA.SETT WITH TRANS.DATE EQ ": LAST.W.DAY
    SET.SEL = "SELECT F.SCB.VISA.SETT WITH TRANS.DATE EQ ": TDATEE : " AND DATE.TIME LE ": SDAT

    KEY.LIST.SET=""
    SELECTED.SET=""
    ER.MSG.SET=""

    CALL EB.READLIST(SET.SEL,KEY.LIST.SET,"",SELECTED.SET,ER.MSG.SET)
    IF SELECTED.SET THEN
        FOR ST=1 TO SELECTED.SET
            FN.SET = 'F.SCB.VISA.SETT' ; F.SET = '' ; R.SET = '' ; RETRY3= '' ; E3 = ''
            SET.ID = KEY.LIST.SET<ST>
            CALL OPF(FN.SET,F.SET)
            CALL F.READ(FN.SET,  SET.ID, R.SET, F.SET, E3)
            VISA.NO.SET = R.SET<SETT.CARD.NO>
            CARD.NO.SET = VISA.NO.SET:STR(' ',25 - LEN(VISA.NO.SET))
            CUST.FT.SET = R.SET<SETT.CUST.NO>
            FLAG.1 = R.SET<SETT.TRN.INDICATOR>
            FN.CUSTOMER.SET = 'F.CUSTOMER' ; F.CUSTOMER.SET = '' ; R.CUSTOMER.SET = '' ; RETRY.SET = '' ; E.SET = ''
            CALL OPF(FN.CUSTOMER.SET,F.CUSTOMER.SET)
            CALL F.READ(FN.CUSTOMER.SET, CUST.FT.SET, R.CUSTOMER.SET, F.CUSTOMER.SET, E.SET)

            EMB.NA.SET = R.CUSTOMER.SET<EB.CUS.SHORT.NAME>
            EMB.NAME.SET = STR(' ',30)

            IF FLAG.1 = 'C' THEN
                REFEREN.SET = 'CREDIT PAYMENT'
            END
            IF FLAG.1 = 'D' THEN
                REFEREN.SET = 'DEBIT PAYMENT'
            END

            REF.SET = REFEREN.SET:STR(' ',30 - LEN(REFEREN.SET))
            AMTT.SET = R.SET<SETT.TRN.AMT>

            N3.SET = AMTT.SET*100
            AMT.SET= FMT(N3.SET, "R%12")

       **   TOT.SET = TOT.SET + AMT.SET
            PAY.DATE.SET = R.SET<SETT.TRANS.DATE>

***************FILLING BRIDGE FOR SETT REC*********************************************************************
      *     ST.DATE.TIM = R.SET<SETT.AUDIT.DATE.TIME>
            ST.DATE.TIM = R.SET<SETT.DATE.TIME>
            ST.AUTH.DT = ST.DATE.TIM<1,1>
            IF ST.AUTH.DT LE SDAT THEN
                VISA.DATA.SET = '1'
                VISA.DATA.SET := CARD.NO.SET
                VISA.DATA.SET := EMB.NAME.SET
                VISA.DATA.SET := REF.SET
                VISA.DATA.SET := 'EGP'
                VISA.DATA.SET := AMT.SET
                IF FLAG.1 = 'C' THEN
                    VISA.DATA.SET := 'N'
                END
                IF FLAG.1 = 'D' THEN
                    VISA.DATA.SET := 'Y'
                END
                VISA.DATA.SET := PAY.DATE.SET
                VISA.DATA.SET := STR("0",25)
                VISA.DATA.SET := '0001'
                VISA.DATA.SET := '0'
                VISA.DATA.SET := STR("0",28)
                VISA.DATA.SET := STR("0",28)

                HH = SELECTED+SELECTED.N+SELECTED.S+SELECTED.SET+1
                DIM WW(HH)
                WW(HH) = VISA.DATA.SET

                WRITESEQ WW(HH) TO V.FILE.IN ELSE
                    PRINT  'CAN NOT WRITE LINE ':WW(HH)
                END
                TOT.SET = TOT.SET + AMT.SET
                TOT.NO.SET = TOT.NO.SET + 1
            END
        NEXT ST

    END
****55555555555555555555555555555555555555555555555555555555555555555555555555555************
  **TOTREC.ALL = FMT(SELECTED+SELECTED.N+SELECTED.S+SELECTED.SET+1, "R%6")
    TOTREC.ALL = FMT(TOT.NO.TT+TOT.NO.FT1+TOT.NO.FT2+TOT.NO.SET+1 , "R%6")
    TOTAMT.ALL1 = TOTAMT+TOT.FT+TOT.FT.S+TOT.SET
    TOTAMT.ALL = FMT(TOTAMT.ALL1, "R%17")

    SEQ.NO = FMT(SER, "R%4")
    SEND.DAT = TODAY
 ** CALL CDT('' ,SEND.DAT,+1W)
    XX = HH+1
    DIM NN(XX)
    NN(XX) = '2'
    NN(XX) := TOTAMT.ALL
    NN(XX) := TOTREC.ALL
    NN(XX) := SEQ.NO
    NN(XX) := SEND.DAT
    NN(XX) := STR("0",160)

    WRITESEQ NN(XX) TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':NN(XX)
    END
**************FLAG COUNT*************************************
    F.BRSIN = '' ; FN.BRSIN = 'F.SCB.BRSIN.FLAG' ; R.BRSIN = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.BRSIN,F.BRSIN)
    ID.BRSIN = "BRSIN-B3"
    CALL F.READ(FN.BRSIN, ID.BRSIN, R.BRSIN, F.BRSIN, E1)
    IF NOT(E1) THEN
        R.BRSIN<BRSIN.FLAG.COUNT> = SER
        R.BRSIN<BRSIN.UP.DATE> = TODAY
     ** CALL F.WRITE(FN.BRSIN,"BRSIN", R.BRSIN)
        CALL F.WRITE(FN.BRSIN,ID.BRSIN, R.BRSIN)
        CALL JOURNAL.UPDATE(ID.BRSIN)
    END
*************************************************************
    TEXT = '�� �������� �� ��������' ; CALL REM
    RETURN

END
