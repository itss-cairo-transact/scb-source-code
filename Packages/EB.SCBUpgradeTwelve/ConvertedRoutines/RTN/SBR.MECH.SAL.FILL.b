* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.MECH.SAL.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
    FN.MCH = "F.SCB.SUEZ.MECH.SAL"  ; F.MCH  = ""
    CALL OPF (FN.MCH,F.MCH)

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    TD = TODAY[1,6]

    WS.COMP = ID.COMPANY
    WS.FILE.COMP = ID.COMPANY[2]

    EXECUTE "chmod 775 MECH/mech":WS.FILE.COMP:".csv"

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME = 'mech':WS.FILE.COMP:'.csv'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            EMP.NO   = FIELD(Y.MSG,",",1)
            EMP.NAME = FIELD(Y.MSG,",",2)
            AMT      = FIELD(Y.MSG,",",3)
            CUR.AR   = FIELD(Y.MSG,",",4)
            CUS.NO1  = FIELD(Y.MSG,",",5)
            CATEG    = FIELD(Y.MSG,",",6)
            AC.SER1  = FIELD(Y.MSG,",",7)

            SAL.DATE   = TODAY[1,6]
            PAY.DATE   = TODAY
            PAY.TIME.1 = TIME()
            PAY.TIME.2 = OCONV(PAY.TIME.1,'MTS')
            PAY.TIME   = PAY.TIME.2[1,2]:PAY.TIME.2[4,2]:PAY.TIME.2[7,2]

            CUS.NO   = FMT(CUS.NO1,'R%8')
            AC.SER   = FMT(AC.SER1,'R%2')

            ACCT.NO  = CUS.NO:CUR.AR:CATEG:AC.SER

            CALL F.READ(FN.AC,ACCT.NO,R.AC,F.AC,E5)
            COMP     = R.AC<AC.CO.CODE>

**    IF WS.COMP EQ 'EG0010051' OR WS.COMP EQ 'EG0010070' OR WS.COMP EQ 'EG0010040' THEN
            MCH.ID = CUS.NO:"-":PAY.DATE:"-":PAY.TIME
**    END ELSE
**        MCH.ID = CUS.NO:"-":SAL.DATE
**    END

*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.AR,CUR.CODE)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CUR.AR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR.CODE=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
*------------------------------------------------------------------
            CALL F.READ(FN.MCH,MCH.ID,R.MCH,F.MCH,E4)
            IF NOT(E4) THEN
                GO TO MSG.TXT.ERR
            END
            IF E4 THEN
                R.MCH<MCH.EMP.NO>       = EMP.NO
                R.MCH<MCH.EMP.NAME>     = EMP.NAME
                R.MCH<MCH.CUS.NO>       = CUS.NO
                R.MCH<MCH.CURRENCY>     = CUR.CODE
                R.MCH<MCH.CATEGORY>     = CATEG
                R.MCH<MCH.ACCOUNT.NO>   = ACCT.NO
                R.MCH<MCH.AMOUNT>       = AMT
                R.MCH<MCH.SALARY.DATE>  = SAL.DATE
                R.MCH<MCH.RECEIVE.DATE> = PAY.DATE
                R.MCH<MCH.STATUS>       = "1"
                R.MCH<MCH.CO.CODE>      = WS.COMP
                R.MCH<MCH.TRN.CODE>     = "AC48"
                CALL F.WRITE(FN.MCH,MCH.ID,R.MCH)
                CALL JOURNAL.UPDATE(MCH.ID)
            END
*----------------------------------
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER
    EXECUTE "sh > MECH/mech":WS.FILE.COMP:".csv"

MSG.TXT.DONE:
    TEXT = "�� ����� �����" ; CALL REM
    GO TO EXIT.PROG

MSG.TXT.ERR:
    TEXT = "�� ����� ����� �� ���" ; CALL REM


EXIT.PROG:
    RETURN
************************************************************
END
