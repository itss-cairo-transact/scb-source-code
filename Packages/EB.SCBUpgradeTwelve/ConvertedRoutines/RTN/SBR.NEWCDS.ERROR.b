* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.NEWCDS.ERROR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*---------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*---------------------------------------------

INITIATE:
*========
    REPORT.ID  = 'P.FUNCTION'
    REPORT.ID1 = 'SBR.NEWCDS.ERROR'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.STMT = 'FBNK.STMT.ENTRY' ; F.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.ENT.TD = 'FBNK.ACCT.ENT.TODAY' ; F.ENT.TD  = ''
    CALL OPF(FN.ENT.TD,F.ENT.TD)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    WS.TODAY  = TODAY
    KK        = 0
    WS.LD.REF = ''
    FLAG      = 0
    COMP      = ID.COMPANY

    RETURN
*-------------------------------------------------
PROCESS:
*=======
**** ACCT.ENT.TODAY *****
*************************

    T.SEL2 = "SELECT ":FN.ENT.TD:" BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR X = 1 TO SELECTED2
            CALL F.READ(FN.ENT.TD,KEY.LIST2<X>,R.ENT.TD,F.ENT.TD,EER.R)
            LOOP
                REMOVE WS.STMT.ID FROM R.ENT.TD SETTING POS
            WHILE WS.STMT.ID:POS
                KK++
                CALL F.READ(FN.STMT,WS.STMT.ID,R.STMT,F.STMT,ERR.STMT)
                WS.LD.REF<1,KK> = R.STMT<AC.STE.OUR.REFERENCE>
            REPEAT
        NEXT X
    END
***************************************
**** LD.LOANS.AND.DEPOSITS ****
*******************************

    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY GE '21101' AND CATEGORY LE '21103' AND STATUS EQ 'CUR' AND VALUE.DATE EQ ":WS.TODAY:" AND CO.CODE EQ ":COMP:" BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            LOCATE KEY.LIST<I> IN WS.LD.REF<1,1> SETTING LD.POS THEN
            END ELSE
                FLAG = 1

                CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)

                WS.AMT    = R.LD<LD.AMOUNT>
                WS.BRN.ID = R.LD<LD.CO.CODE>

                CALL F.READ(FN.COM,WS.BRN.ID,R.COM,F.COM,E3)
                WS.BRN.NAME   = R.COM<EB.COM.COMPANY.NAME,2>
                XX1 = SPACE(120)
                XX1<1,1>[1,15]   = KEY.LIST<I>
                XX1<1,1>[20,10]  = WS.AMT
                XX1<1,1>[40,10]  = WS.BRN.NAME

                PRINT XX1<1,1>
            END
        NEXT I
    END

    IF FLAG = 0 THEN
        XX3 = SPACE(120)
        XX3<1,1>[55,35] = '********* �� ���� ������ *********'
        PRINT STR(' ',120)
        PRINT XX3<1,1>
    END
************

    XX2 = SPACE(120)
    XX2<1,1>[55,35] = '********* ����� ������� *********'
    PRINT STR(' ',120)
    PRINT XX2<1,1>

    RETURN
*--------------------------------------------------------
PRINT.HEAD:
*==========
*Line [ 136 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :BRANCH
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID1
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(35):"���� ��������� ������� ��� ���� ��� ���� ��� ������ ������� - ":T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" ":"��� �������":SPACE(13):"������":SPACE(15):"�����"
    PR.HD :="'L'":STR('_',130)
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD


    RETURN
END
