* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.LD.CHANGE.RATE.21015

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*---------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    TEXT = "�� �������� �� ��������" ; CALL REM

    RETURN
*---------------------------------------
INITIALISE:

    OPENSEQ "MECH" , "LD.21015.RATE.CHANGE" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LD.21015.RATE.CHANGE"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LD.21015.RATE.CHANGE" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LD.21015.RATE.CHANGE CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LD.21015.RATE.CHANGE File IN MECH'
        END
    END

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.BID = 'FBNK.BASIC.INTEREST.DATE' ; F.BID = ''
    CALL OPF(FN.BID,F.BID)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21015 AND STATUS NE LIQ"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            WS.FREQ     = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
            WS.CCY      = R.LD<LD.CURRENCY>
            WS.COMP     = R.LD<LD.CO.CODE>
            WS.VAL.DATE = R.LD<LD.VALUE.DATE>

            IF WS.FREQ EQ '3M' THEN
                BI.ID.KEY = '40':WS.CCY
            END

            IF WS.FREQ EQ '6M' THEN
                BI.ID.KEY = '41':WS.CCY
            END

            IF WS.FREQ EQ '9M' THEN
                BI.ID.KEY = '42':WS.CCY
            END

            IF WS.FREQ EQ '12M' THEN
                BI.ID.KEY = '43':WS.CCY
            END

            CALL F.READ(FN.BID,BI.ID.KEY,R.BID,F.BID,E4)
            BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
            BI.ID = BI.ID.KEY:BI.DATE
            CALL F.READ(FN.BI,BI.ID,R.BI,F.BI,E3)
            WS.NEW.RATE = R.BI<EB.BIN.INTEREST.RATE>
            IF BI.DATE LT WS.VAL.DATE THEN
                WS.NEW.VAL.DATE = WS.VAL.DATE
            END ELSE
                WS.NEW.VAL.DATE = BI.DATE
            END


            IDD = 'LD.LOANS.AND.DEPOSITS,RATE,AUTO.LD//':WS.COMP:',':KEY.LIST<I>

**************************************************CRAETE FT BY OFS**********************************************
            OFS.MESSAGE.DATA  =  "NEW.INT.RATE=":WS.NEW.RATE:','
            OFS.MESSAGE.DATA :=  "INT.RATE.V.DATE=":WS.NEW.VAL.DATE:','

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LD.21015.RATE.CHANGE'
    EXECUTE 'DELETE ':"MECH":' ':"LD.21015.RATE.CHANGE"

    RETURN
END
