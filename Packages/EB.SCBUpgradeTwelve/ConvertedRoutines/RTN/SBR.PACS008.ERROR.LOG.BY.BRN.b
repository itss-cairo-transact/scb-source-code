* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>208</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.PACS008.ERROR.LOG.BY.BRN
*    PROGRAM SBR.PACS008.ERROR.LOG.BY.BRN

*-------- CREATED BY NOHA HAMED 10/05/2015 ---

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACH.PACS008
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*-------------------------------------------------------------------------
INITIATE:

    REPORT.ID='P.FUNCTION'
    REPORT.NAME='SBR.PACS008.ERROR.LOG.BY.BRN'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.PAC  = 'F.SCB.ACH.PACS008'; F.PAC = ''
    R.PAC    = ''
    CALL OPF(FN.PAC,F.PAC)


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)
    COMP = C$ID.COMPANY
    COM.CODE = COMP[6,4]

    KEY.LIST  = ""  ; SELECTED=""  ;  ER.MSG=""
    RETURN

*========================================================================
PROCESS:

*------------------TODAY PACS008 SELECTION-------------------------
    T.DATE = ''
    YTEXT = "Enter ACH Date : "
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    T.DATE  = COMI

    YTEXT = "Enter FILE TYPE : "
    CALL TXTINP(YTEXT, 8, 22, "4", "A")
    FIL.TYP = COMI

    TT.MONTH = T.DATE[5,2]

    TOT.AMT = 0
    T.COUNT = 0

    BEGIN CASE
    CASE FIL.TYP EQ 'ALL'
        TIT = "�� �������"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND (CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" OR CREDITOR.PARTY.BRANCH.ID EQ '') BY REQ.SETTLEMENT.DATE"
    CASE FIL.TYP EQ 'SSBE'
        TIT = "�.������"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND CHARGER.BEARER EQ 'SSBE' AND (CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" OR CREDITOR.PARTY.BRANCH.ID EQ '') BY REQ.SETTLEMENT.DATE"
    CASE FIL.TYP EQ 'CASH'
        TIT = "���"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND CHARGER.BEARER EQ 'CASH' AND (CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" OR CREDITOR.PARTY.BRANCH.ID EQ '') BY REQ.SETTLEMENT.DATE"
    CASE FIL.TYP EQ 'SUPP'
        TIT = "������"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND CHARGER.BEARER EQ 'SUPP' AND (CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" OR CREDITOR.PARTY.BRANCH.ID EQ '') BY REQ.SETTLEMENT.DATE"
    CASE FIL.TYP EQ 'SALA'
        TIT = "������"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND CHARGER.BEARER EQ 'SALA' AND (CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" OR CREDITOR.PARTY.BRANCH.ID EQ '') BY REQ.SETTLEMENT.DATE"
    CASE FIL.TYP EQ 'PENS'
        TIT = "������"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND CHARGER.BEARER EQ 'PENS' AND (CREDITOR.PARTY.BRANCH.ID EQ ":COM.CODE:" OR CREDITOR.PARTY.BRANCH.ID EQ '') BY REQ.SETTLEMENT.DATE"
    CASE FIL.TYP EQ 'CBET'
        TIT = "�. �����"
        T.SEL  = "SELECT F.SCB.ACH.PACS008 WITH TRN.DATE EQ '' AND @ID LIKE ":T.DATE:"... AND PACS.TYPE EQ 1 AND CHARGER.BEARER EQ 'CBET' BY CREDITOR.PARTY.BRANCH.ID"
    CASE OTHERWISE
        TIT = "��� �� ����� "
    END CASE
************************************************************
    GOSUB PRINT.HEAD
*************************************************************

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            PAC.ID     = KEY.LIST<I>
            CALL F.READ(FN.PAC,PAC.ID,R.PAC,F.PAC,ER.PAC)
            ACCT.NO    = R.PAC<PACS8.CREDITOR.ACCOUNT.NO>
            CALL F.READ(FN.AC,ACCT.NO,R.AC,F.AC,ER.AC)
            CUS.NO     = R.AC<AC.CUSTOMER>
            CATEG      = R.AC<AC.CATEGORY>
            CALL F.READ(FN.CU,CUS.NO,R.CU,F.CU,ER.CU)

            CUS.NAME   = R.PAC<PACS8.CREDITOR.NAME>
            BRN.NAME   = R.PAC<PACS8.CREDITOR.PARTY.BRANCH.NAME>
            AMT        = R.PAC<PACS8.AMOUNT>
            FIL.TYP    = R.PAC<PACS8.CHARGER.BEARER>
            IF R.PAC<PACS8.CHARGER.BEARER> EQ 'CASH' AND R.PAC<PACS8.PURPOSE> EQ 'COLL' THEN
                FIL.TYP = 'BILL'
            END

            CURR = R.AC<AC.CURRENCY>

            IF ER.AC OR CATEG EQ 9043 OR CATEG EQ 9002 OR CATEG EQ 9005 OR CATEG EQ 9007 OR CATEG EQ 9009 OR CATEG EQ 9003 OR CATEG EQ 9006 OR CATEG EQ 9090 OR CATEG EQ 9091 OR CATEG EQ 9048 OR CATEG EQ 9030 OR CATEG EQ 9031 OR CATEG EQ 9032 OR CATEG EQ 9037 OR CATEG EQ 9015 OR CATEG EQ 9014 OR CATEG EQ 9016 OR CATEG EQ 1205 OR CATEG EQ 1206 OR CATEG EQ 1207 OR CATEG EQ 1208 THEN
                TOT.AMT    = TOT.AMT + AMT
                T.COUNT    = T.COUNT + 1
                DESC = "� ��� �����"
                GOSUB  REPORT.WRITE
            END
            ELSE
                IF CURR NE 'EGP' THEN
                    TOT.AMT    = TOT.AMT + AMT
                    T.COUNT    = T.COUNT + 1
                    DESC = "���� ������"
                    GOSUB  REPORT.WRITE

                END
                IF R.AC<AC.POSTING.RESTRICT> GE 90 THEN
                    TOT.AMT    = TOT.AMT + AMT
                    T.COUNT    = T.COUNT + 1
                    DESC = "� ����"
                    GOSUB  REPORT.WRITE

                END

                IF R.CU<EB.CUS.POSTING.RESTRICT> EQ 70 THEN
                    TOT.AMT    = TOT.AMT + AMT
                    T.COUNT    = T.COUNT + 1
                    DESC       = "� �����"
                    GOSUB  REPORT.WRITE
                END

            END
        NEXT I
        YY = ''
        PRINT YY
        YY<1,1>[1,10]  = "����� = ":T.COUNT
        YY<1,1>[82,15]  = "�������� = ":TOT.AMT
        PRINT YY
        YY = ''
        YY<1,1>[45,40] = "********************����� �����********************"
        PRINT YY<1,1>
    END
    ELSE
        TEXT ="������ �����"; CALL REM
        GOSUB PROGRAM.END
    END
    RETURN

*===============================================================
REPORT.WRITE:

    XX  = SPACE(132)
    XX<1,I>[1,20]   = PAC.ID
    XX<1,I>[20,15]  = BRN.NAME
    XX<1,I>[32,35]   = CUS.NAME
    XX<1,I>[80,16]   = ACCT.NO
    XX<1,I>[90,10]   = AMT
    XX<1,I>[103,15]   = DESC
    XX<1,I>[118,10]   = FIL.TYP

    PRINT XX<1,I>

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):" ��� ����� ��� ������� ":TIT
*   PR.HD :="'L'":SPACE(40):""
    PR.HD :="'L'":SPACE(40):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(10):"�����":SPACE(10):"��� ������":SPACE(25):"��� ������":SPACE(10):"������":SPACE(5):"��� �����":SPACE(6):"�.����� "
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*----------
*----------
PROGRAM.END:
*-----------
END
