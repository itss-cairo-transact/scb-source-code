* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.MECH.SAL.FT.COMM

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL

*---------------------------------------
    GOSUB CHECKEXIST
    IF SW2 = 0 THEN
        GOSUB PROCESS
    END
    RETURN
*---------------------------------------
CHECKEXIST:

    KEY.LIST1="" ; SELECTED1="" ;  ER.FT="" ; SW2 = 0
    COMP = ID.COMPANY ; AMT = 0

    T.SEL1 = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH TRANSACTION.TYPE EQ 'AC48' AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.FT)
    IF SELECTED1 THEN
        TEXT = '�� ��� ��� ������� - ����� ����� ���� ��������' ; CALL REM
        SW2 = 1
    END ELSE
        SW2 = 0
    END

    RETURN

*---------------------------------------
PROCESS:

************ OPEN FILES ****************

    FN.MCH = "F.SCB.SUEZ.MECH.SAL"  ; F.MCH  = ""
    CALL OPF (FN.MCH,F.MCH)

    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT  = ""
    CALL OPF (FN.FT,F.FT)

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION  = "MECH"
    WS.FILE.COMP = ID.COMPANY[2]
    WS.DAT = TODAY

    SCB.OFS.HEADER  = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,,,"

    OPENSEQ "MECH" , "MECH.SAL.OUT.COMM":WS.FILE.COMP TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"MECH.SAL.OUT.COMM":WS.FILE.COMP
        HUSH OFF
    END
    OPENSEQ "MECH" , "MECH.SAL.OUT.COMM":WS.FILE.COMP TO BB ELSE
        CREATE BB THEN
        END ELSE
            STOP 'Cannot create MECH.SAL.OUT.COMM File IN MECH'
        END
    END
*--------------------------------------------------------------------
**************** DEBIT COMM. FROM COMPANY CREDIT TO PL  ************

    YTEXT  = "����� ������ ����� ��� �������  (1)��.����.������   "
    YTEXT := "(2)��.����.������"

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    DB.FLAG = COMI

    IF DB.FLAG EQ '1' THEN
        YTEXT = "����� ����� ���� ����� : "
        CALL TXTINP(YTEXT, 8, 22, "16", "A")
        DB.ACCT = COMI
        COMP.DB.ACCT = COMI

        T.SEL = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '3' AND TRN.CODE EQ 'AC48' AND CO.CODE EQ ":COMP:" AND RECEIVE.DATE EQ ":WS.DAT:" AND DB.ACCT EQ ":COMP.DB.ACCT
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR NN = 1 TO SELECTED
                CALL F.READ(FN.MCH,KEY.LIST<NN>,R.MCH,F.MCH,E4)
                WS.AMT   = R.MCH<MCH.AMOUNT>
                WS.CUR   = R.MCH<MCH.CURRENCY>

                IF WS.CUR EQ 'EGP' THEN

                    IF WS.AMT LE '1500' THEN
                        AMT += '3'
                    END ELSE
                        AMT += '5'
                    END

                END ELSE
                    AMT += '1'
                END

                R.MCH<MCH.STATUS> = "4"
                CALL F.WRITE(FN.MCH,KEY.LIST<NN>,R.MCH)
                CALL JOURNAL.UPDATE(KEY.LIST<NN>)

            NEXT NN

            CR.ACCT  = 'PL52157'

*------------------------------------------------------------------
            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC89"
            OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":WS.CUR
            OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":WS.CUR
            OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
            OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
            OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":AMT
            OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
            OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
            OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
            OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
            OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

            SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END

** CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

            BB.DATA  = SCB.OFS.MESSAGE
            WRITESEQ BB.DATA TO BB ELSE
            END


            TEXT = '�� ��� �������' ; CALL REM

        END

        IF NOT(SELECTED) THEN
            TEXT = '�� ��� ��� �������' ; CALL REM
        END

    END
**************** DEBIT COMM. FROM CUSTOMER CREDIT TO PL  ************
    IF DB.FLAG EQ '2' THEN

        YTEXT = "����� ����� ���� ������ : "
        CALL TXTINP(YTEXT, 8, 22, "16", "A")
        COMP.DB.ACCT = COMI

        T.SEL = "SELECT F.SCB.SUEZ.MECH.SAL WITH STATUS EQ '3' AND TRN.CODE EQ 'AC48' AND CO.CODE EQ ":COMP:" AND RECEIVE.DATE EQ ":WS.DAT:" AND DB.ACCT EQ ":COMP.DB.ACCT
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR NN = 1 TO SELECTED
                CALL F.READ(FN.MCH,KEY.LIST<NN>,R.MCH,F.MCH,E4)
                WS.AMT   = R.MCH<MCH.AMOUNT>
                DB.ACCT  = R.MCH<MCH.ACCOUNT.NO>
                WS.CUR   = R.MCH<MCH.CURRENCY>

                IF WS.CUR EQ 'EGP' THEN

                    IF WS.AMT LE '1500' THEN
                        AMT = '3'
                    END ELSE
                        AMT = '5'
                    END

                END ELSE
                    AMT = '1'
                END

                CR.ACCT  = 'PL52157'

*------------------------------------------------------------------
                OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC89"
                OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":WS.CUR
                OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY::=":WS.CUR
                OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
                OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
                OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":AMT
                OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
                OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
                OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=":"SCB"
                OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N::=":"NO"
                OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N::=":"NO"

                SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
                BB.DATA  = SCB.OFS.MESSAGE
                WRITESEQ BB.DATA TO BB ELSE
                END

** CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

                BB.DATA  = SCB.OFS.MESSAGE
                WRITESEQ BB.DATA TO BB ELSE
                END

                R.MCH<MCH.STATUS> = "4"

                CALL F.WRITE(FN.MCH,KEY.LIST<NN>,R.MCH)
                CALL JOURNAL.UPDATE(KEY.LIST<NN>)

            NEXT NN
            TEXT = '�� ��� �������' ; CALL REM
        END


        IF NOT(SELECTED) THEN
            TEXT = '�� ��� ��� �������' ; CALL REM
        END
    END

    IF DB.FLAG NE '1' AND DB.FLAG NE '2' THEN
        TEXT = '�� ������ �� ��������' ; CALL REM
        GO TO ERR.EXIT
    END
*----------------------------------
ERR.EXIT:


    RETURN
************************************************************
END
