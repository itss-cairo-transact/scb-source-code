* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
**************************MAHMOUD 5/5/2015*******************************
    SUBROUTINE SBR.PROVISIONS.CUST.WRITE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PROVISIONS.CUST

    GOSUB BUILD.RECORD

    RETURN
*---------------------------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    Path  = "&SAVEDLISTS&/PROVISIONS_MIS.CSV"
    TOOT  = 0
    OPENSEQ Path TO MyPath ELSE
        RETURN
    END
    EOF = ''
    I = 1
    TTD1 = TODAY
    FN.ALC = 'F.SCB.PROVISIONS.CUST' ; F.ALC = ''
    CALL OPF(FN.ALC,F.ALC)
**---------------------------------------------------------------------**
    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            CHK.REC        =  FIELD(Line,COMMA,2)
            ALC.ID         =  FIELD(Line,COMMA,3)
            LCY.AMTS       =  FIELD(Line,COMMA,4,5)
            LCY.AMTS = CHANGE(LCY.AMTS,'"','')
            LCY.AMTS = CHANGE(LCY.AMTS,COMMA,'')
            IF CHK.REC EQ '1' THEN
                CRT ALC.ID:" - ":LCY.AMTS
                CALL F.READ(FN.ALC,ALC.ID,R.ALC,F.ALC,EE1)
                R.ALC<ALC.AMOUNT.LCY>  = LCY.AMTS
                R.ALC<ALC.MODIFY.DATE> = TTD1
**--------------------------------------------------------------**
                CALL F.WRITE(FN.ALC,ALC.ID,R.ALC)
                CALL JOURNAL.UPDATE(ALC.ID)
            END
**--------------------------------------------------------------**
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath
    RETURN
***------------------------------------------------------------------***
END
