* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*SUBROUTINE SBY.ECL.NEW.GENERAL.DATA

    PROGRAM SBY.ECL.NEW.GENERAL.DATA

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ECL
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.MAST
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

*-----------------------------------------------------------------------
    GOSUB INITIALS
    GOSUB INIT0
    GOSUB GETCUSAC
    GOSUB INIT0
    GOSUB GETLD
    GOSUB INIT0
    GOSUB GETLC
    GOSUB INIT0
    GOSUB GETDR
    GOSUB INIT0
    GOSUB GETLCEXP

*RUNNING.UNDER.BATCH = 0
    RETURN

*=========== CREATE AND OPEN THE TEXT FILE ============
INITIALS:
*========
*RUNNING.UNDER.BATCH = 1

    FN.RSK = 'F.SCB.RISK.MAST'; F.RSK = '' ; R.RSK = ''
    CALL OPF(FN.RSK,F.RSK)

    FN.CUS  = 'FBNK.CUSTOMER'   ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.CUS.ACC  = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = '' ; R.CUS.ACC = ''
    CALL OPF( FN.CUS.ACC,F.CUS.ACC)

    FN.AC  = 'FBNK.ACCOUNT'              ; F.AC = ''     ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'  ; F.LD = ''      ; R.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.SCH = 'FBNK.LD.SCHEDULE.DEFINE' ; F.SCH = '' ; R.SCH = ''
    CALL OPF(FN.SCH,F.SCH)

    FN.LC = 'FBNK.LETTER.OF.CREDIT'       ; F.LC = ''      ; R.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    FN.REP = 'F.SCB.ECL'; F.REP = ''      ; R.REP = ''; R.CRDT.CBE = ''
    CALL OPF(FN.REP,F.REP)

    FN.LMT = 'F.LIMIT';       F.LMT = ''      ; R.LMT = ''; R.LMT =''
    CALL OPF(FN.LMT,F.LMT)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = ''
    CALL OPF(FN.ADI,F.ADI)

    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    FN.INT = 'FBNK.BASIC.INTEREST' ; F.INT = ''
    CALL OPF(FN.INT,F.INT)

    FN.COLL.RIGHT.CUS = 'FBNK.COLLATERAL.RIGHT.CUST' ; F.COLL.RIGHT.CUS = ''
    CALL OPF(FN.COLL.RIGHT.CUS,F.COLL.RIGHT.CUS)

    FN.COLL.RIGHT = 'FBNK.RIGHT.COLLATERAL' ; F.COLL.RIGHT = ''
    CALL OPF(FN.COLL.RIGHT,F.COLL.RIGHT)

    FN.COLL = 'FBNK.COLLATERAL' ; F.COLL = ''
    CALL OPF(FN.COLL,F.COLL)

    FN.COLL.R = 'FBNK.COLLATERAL.RIGHT' ; F.COLL.R = ''
    CALL OPF(FN.COLL.R,F.COLL.R)

    FN.DRAWINGS = 'FBNK.DRAWINGS' ; F.DRAWINGS = '' ; R.DRAWINGS = ''
    CALL OPF(FN.DRAWINGS,F.DRAWINGS)

    FN.ECL = 'F.SCB.ECL' ; F.ECL = ''; R.ECL1 = ''
    CALL OPF(FN.ECL,F.ECL)

    BNK.DATE1 = TODAY
    CALL CDT("",BNK.DATE1,'-1C')


    IDDD="EG0010001"
*Line [ 167 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,IDDD,LAST.P.INT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
LAST.P.INT=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    T.DATE    = TODAY
    LD.TYPE   = ''
*========================== SOME GLOBAL VARIABLES ===================
    COMP = ID.COMPANY
    TODAY.DATE  = TODAY
    MONTH       = TODAY.DATE[5,2]
    YEAR        = TODAY.DATE[1,4]
    NDPD        = 0
    I = 0
    J = 0
    CREDIT.CODE = ''
    VALID.DATE = '20171231'
    RETURN

*=================================================
INIT0:
*----------------------

    R.REP<ECL.FACILITY.ID>   = 0
    R.REP<ECL.CUS.NO>        = 0
    R.REP<ECL.PORTFOLIO>     = 0
    R.REP<ECL.PRODUCT.TYPE>  = 0
    R.REP<ECL.RATING>        = 0
    R.REP<ECL.COLL.VALUE>    = 0
    R.REP<ECL.DRAWN.EXP>     = 0
    R.REP<ECL.UNDRAWN.EXP>   = 0
    R.REP<ECL.CCF>           = 0
    R.REP<ECL.NDPD>          = 0
    R.REP<ECL.EXPIRE.DATE>   = 0
    R.REP<ECL.PAY.FREQ>      = 0
    R.REP<ECL.INTEREST.RATE> = 0
    R.REP<ECL.LD.TYPE>       = ''

    RETURN
*=====================================
GETCUSAC:
*
    T.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY IN (3201 1102 1404 1206 1417 1419 1208 1421 1484 1401 1405 1406 1415 1480"
    T.SEL := " 1481 1202 1502 1503 1416 1599 1516 1418 1420 1523 1524 1205 1207 1525 1526 1507 1508 1509 1511 1512 1514 1407"
    T.SEL := " 1413 1408 1445 1455 1567 1528 1575 1583 1218 1216 1212 1215 1230 1596 1488 1486 1487 1539 1236 1540 1423"
    T.SEL := " 1422 1485 1402 1499 1483 1493 1203 1301 1302 1303 1377 1390 1399 1532 1438 1416 1201 1477 3005"
    T.SEL := " 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 3011 1591 1001 1002 1003 1059 1585 1586 1587 1262"
    T.SEL := " 1011 1504 1510 1511 1579 1513 1534 1544 1559 1582 1008 6501 1007 6502 6503 1006 6504"
    T.SEL := " 6511 1558 1527 1530 1568 1569 1570 1571 1572 1531 1573 1574 1576 1584 1529 1414 1217"
    T.SEL := " 1211 1212 1518 1519 1214 1589 1598 1595 1597 1486 1487 1479 1478 1220 1221 1222 1223 1224 1225 1226 1227 1533 1561 1009 1535 1538 1515 1424 '1231'"
    T.SEL := " 1601 1602 1603 1604)"
    T.SEL := " AND OPEN.ACTUAL.BAL LT 0 AND OPEN.ACTUAL.BAL NE '' BY CUSTOMER"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN

        FOR I = 1 TO SELECTED
            NDPD = 0
            DAYS    = 'C'
            US.AMT  = 0
            CR.AMT = 0
            INT.RATE  = 0
            PAY.TYPE  = 0
            COLL.AMT  = 0
            YY1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; COLLC = 0
            ADI.CHRG  = 0;ADI.INT.R = 0; ADI.BRATE = 0; ADI.MRG.O = 0; ADI.MRG.R = 0
            GDI.CHRG  = 0;GDI.INT.R = 0; GDI.BRATE = 0; GDI.MRG.O = 0; GDI.MRG.R = 0
            ACC.RATE  = 0; AMT.3060 = 0
            COLL.PERC.ALLOC = 100
            PRODUCT.TYPE    = "funded"
*********************************************

            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            ACC.ID   = KEY.LIST<I>
            FAC.ID   = ACC.ID
            NDPD     = 0
            CUS.ID   = R.AC<AC.CUSTOMER>
            CURR     = R.AC<AC.CURRENCY>
            CATEG    = R.AC<AC.CATEGORY>
            PAY.TYPE = 1
            CASH.ACCT = ''
            CASH.AMT  = 0
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,AC.CATEG,CAT.NAME)
*IF CATEG EQ 1512 OR CATEG EQ 1517 OR CATEG EQ 1230 OR CATEG EQ 1528 OR CATEG EQ 1216 OR CATEG EQ 1215 OR CATEG EQ 1597 OR CATEG EQ 1523 OR CATEG EQ 1514 OR CATEG EQ 1212 OR CATEG EQ 1598 OR CATEG EQ 1595 OR CATEG EQ 1596 OR CATEG EQ 1524 THEN
*    PAY.TYPE =
*END
*ELSE
*    PAY.TYPE = 1
*END
            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            US.AMT    = R.AC<AC.OPEN.ACTUAL.BAL> * RATE * -1
            LMT.REF   = R.AC<AC.LIMIT.REF>
            AC.GDI    = R.AC<AC.CONDITION.GROUP>

            IF CATEG EQ 1507 OR CATEG EQ 1516 OR CATEG EQ 1525 OR CATEG EQ 1526 OR CATEG EQ 1488 OR CATEG EQ 1486 OR  CATEG EQ 1487 THEN
                PAY.TYPE = 0
            END
            IF CATEG EQ 1205 OR CATEG EQ 1206 OR CATEG EQ 1207 OR CATEG EQ 1208 OR CATEG EQ 1407 OR CATEG EQ 1408 OR CATEG EQ 1413 OR CATEG EQ 1445 OR CATEG EQ 1455 THEN
                PAY.TYPE = 12
            END
**********TO GET COLLATERAL(BECAUSE IT IS MULTI)*************
            LMTT      = FMT(LMT.REF,"R%10")
            LMT.ID    = CUS.ID:'.':LMTT
            CALL F.READ(FN.LMT,LMT.ID,R.LMT,F.LMT, ETEXT.LMT)
            IF ETEXT.LMT THEN
                LMT.ID = ''
            END
            LMT.CUR = R.LMT<LI.LIMIT.CURRENCY>
            LOCATE LMT.CUR IN CURR.BASE<1,1> SETTING POS THEN
                LMT.RATE = R.BASE<RE.BCP.RATE,POS>
            END

****GET COLLATERAL 18-4-2018*************
****READ FROM COLLATERAL.RIGHT.CUST WITH CUS.ID**********
            TOT.COLL.AMT = 0
            CALL F.READ(FN.COLL.RIGHT.CUS,CUS.ID,R.COLL.RIGHT.CUS,F.COLL.RIGHT.CUS,ER.COLL.R.CUS)
            LOOP
                REMOVE COLL.RIGHT.ID FROM R.COLL.RIGHT.CUS SETTING POS.COLL.RIGHT
            WHILE COLL.RIGHT.ID:POS.COLL.RIGHT
****READ FROM RIGHT.COLLATERAL WITH COLLATERAL.RIGHT.ID**********
                FLAG = 'N'
                CALL F.READ(FN.COLL.RIGHT,COLL.RIGHT.ID,R.COLL.RIGHT,F.COLL.RIGHT,ER.COLL.R)
                CALL F.READ(FN.COLL.R,COLL.RIGHT.ID,R.COLL.R,F.COLL.R,ER.COLL.RIGHT)
                COLL.LIMIT.ID       = R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE>

****READ FROM RIGHT.COLLATERAL WITH COLLATERAL.RIGHT.ID**********
**   FINDSTR LMT.ID IN COLL.LIMIT.ID SETTING LMT.POS THEN
                LOCATE LMT.ID IN R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE,1> SETTING LMT.POS THEN
                    FLAG = 'Y'
                    COLL.PERC.ALLOC = R.COLL.R<COLL.RIGHT.PERCENT.ALLOC,LMT.POS>
                END

                LOOP
                    REMOVE COLL.ID FROM R.COLL.RIGHT SETTING POS.COLL
                WHILE COLL.ID:POS.COLL

                    IF FLAG EQ 'Y' AND COLL.ID THEN
                        CALL F.READ(FN.COLL,COLL.ID,R.COLL,F.COLL,ER.COLL)
                        COLL.CURR = R.COLL<COLL.CURRENCY>
                        LOCATE COLL.CURR IN CURR.BASE<1,1> SETTING POS THEN
                            COLL.RATE = R.BASE<RE.BCP.RATE,POS>
                        END

                        COLL.ID.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT> * COLL.RATE
                        TOT.COLL.AMT +=COLL.ID.AMT
                    END
                REPEAT
            REPEAT

****************************

            CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,1,1>
            IF CUS.COLL = '' THEN
                CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,2,1>
            END
*Line [ 329 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COLLATERAL.RIGHT':@FM:COLL.RIGHT.COLLATERAL.CODE,CUS.COLL,COLL.CODE)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,CUS.COLL,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
COLL.CODE=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.COLLATERAL.CODE>

**************3060**************
            IF CATEG EQ '1417' OR CATEG EQ '1419' THEN
                NEW.ID = FAC.ID[1,8]:'10306001'
*Line [ 340 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,NEW.ID,AMT.3060)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,NEW.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AMT.3060=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
            END
            ELSE
                AMT.3060 = 0
            END
*******************************************

            INT.AMT      = R.LMT<LI.INTERNAL.AMOUNT>
            ADVISE.AMT   = R.LMT<LI.ADVISED.AMOUNT>
            IF ADVISE.AMT EQ 0 OR ADVISE.AMT EQ '' THEN

                CR.AMT       = INT.AMT * LMT.RATE
            END
            ELSE
                CR.AMT       = ADVISE.AMT * LMT.RATE
            END

            COLL.AMT     = (TOT.COLL.AMT * (COLL.PERC.ALLOC/100)) + AMT.3060
            LMT.EXP.DATE = R.LMT<LI.EXPIRY.DATE>
            EXP.DATE     = LMT.EXP.DATE[1,8]

            IF CATEG EQ '1407' OR CATEG EQ '1408' OR CATEG EQ '1413' OR CATEG EQ 1445 OR CATEG EQ 1455 THEN
                CR.AMT    = US.AMT
            END

            UNDRAWN.AMT  = CR.AMT - US.AMT
***********GET INTEREST RATE****************
*Line [ 355 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            AC.ADI.COUNT = DCOUNT(R.AC<AC.ACCT.DEBIT.INT>,@VM)
            ADI.DAT      = R.AC<AC.ACCT.DEBIT.INT><1,AC.ADI.COUNT>
            ADI.ID       = ACC.ID:"-":ADI.DAT

            CALL F.READ(FN.ADI,ADI.ID,R.ADI,F.ADI,EADD)
            IF NOT(EADD) THEN
                ADI.CHRG  = R.ADI<IC.ADI.CHARGE.KEY>
                ADI.INT.R = R.ADI<IC.ADI.DR.INT.RATE,1>
                ADI.BRATE = R.ADI<IC.ADI.DR.BASIC.RATE,1>
                ADI.MRG.O = R.ADI<IC.ADI.DR.MARGIN.OPER,1>
                ADI.MRG.R = R.ADI<IC.ADI.DR.MARGIN.RATE,1>

                IF ADI.INT.R EQ '' AND ADI.BRATE NE '' THEN
                    INT.ID.LK = ADI.BRATE:CURR:"..."
                    I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
                    CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
                    INT.ID = I.LIST<SELECTED.I>
                    CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
                    ACC.RATE = R.INT<EB.BIN.INTEREST.RATE>
                END

                ELSE
                    ACC.RATE = ADI.INT.R
                END
            END
            ELSE
                GDI.ID.LK = AC.GDI:CURR:"..."
                G.SEL = "SSELECT ":FN.GDI:" WITH @ID LIKE ":GDI.ID.LK
                CALL EB.READLIST(G.SEL,G.LIST,'',SELECTED.G,ER.MSG.G)
                GDI.ID = G.LIST<SELECTED.G>
                CALL F.READ(FN.GDI,GDI.ID,R.GDI,F.GDI,ERR.G1)
                GDI.BRATE = R.GDI<IC.GDI.DR.BASIC.RATE,1>
                GDI.INT.R = R.GDI<IC.GDI.DR.INT.RATE,1>
                GDI.MRG.O = R.GDI<IC.GDI.DR.MARGIN.OPER,1>
                GDI.MRG.R = R.GDI<IC.GDI.DR.MARGIN.RATE,1>

                IF GDI.INT.R EQ '' AND GDI.BRATE NE '' THEN
                    INT.ID.LK = GDI.BRATE:CURR:"..."
                    I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
                    CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
                    INT.ID = I.LIST<SELECTED.I>
                    CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
                    INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
                    ACC.RATE = INT.RATE
                END
                ELSE
                    ACC.RATE = GDI.INT.R
                END

            END
            IF ACC.RATE NE '' AND ACC.RATE NE 0 THEN
                IF ADI.MRG.O EQ 'ADD' THEN
                    ACC.RATE = ACC.RATE + ADI.MRG.R
                END
                IF ADI.MRG.O EQ 'SUBTRACT' THEN
                    ACC.RATE = ACC.RATE - ADI.MRG.R
                END
                IF ADI.MRG.O EQ 'MULTIBLY' THEN
                    ACC.RATE = ACC.RATE * ADI.MRG.R
                END
            END
            INT.RATE = ACC.RATE

********************************************

            GOSUB INSREC
        NEXT I
    END
    RETURN
*************************************************

INSREC:
*======
    CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,E2)
    CREDIT.CODE = R.CUS<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
    NEW.SEC     = R.CUS<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
    RISK.RATE   = R.CUS<EB.CUS.LOCAL.REF><1,CULR.RISK.RATE>
    SME.CATEG   = R.CUS<EB.CUS.LOCAL.REF><1,CULR.SME.CATEGORY>
    IF NEW.SEC NE '4650' THEN
        IF RISK.RATE = '' THEN
            RISK.RATE = 1
        END
        BEGIN CASE
        CASE SME.CATEG = '' OR CREDIT.CODE = 110
            CUS.TYPE  = "Corporate"
        CASE SME.CATEG = 201 AND CREDIT.CODE NE 110
            CUS.TYPE  = "Micro finance"
        CASE SME.CATEG = 205 AND CREDIT.CODE NE 110
            CUS.TYPE  = "Small"
        CASE SME.CATEG = 206 AND CREDIT.CODE NE 110
            CUS.TYPE  = "Medium"
        END CASE
    END
    ELSE
        CUS.TYPE = "Retail"
    END

    IF CREDIT.CODE EQ '100' OR CREDIT.CODE EQ '' OR CREDIT.CODE EQ '110' THEN
        NEW.ID = CUS.ID:'.':FAC.ID:'.':T.DATE
        R.REP<ECL.FACILITY.ID>  = FAC.ID
        R.REP<ECL.CUS.NO>       = CUS.ID
        R.REP<ECL.PORTFOLIO>    = CUS.TYPE
        R.REP<ECL.PRODUCT.TYPE> = PRODUCT.TYPE
        R.REP<ECL.RATING>       = RISK.RATE
        R.REP<ECL.COLL.VALUE>   = COLL.AMT
        R.REP<ECL.DRAWN.EXP>    = US.AMT
        R.REP<ECL.UNDRAWN.EXP>  = UNDRAWN.AMT
        R.REP<ECL.NDPD>         = NDPD
        R.REP<ECL.EXPIRE.DATE>  = EXP.DATE
        R.REP<ECL.PAY.FREQ>     = PAY.TYPE
        R.REP<ECL.INTEREST.RATE> = INT.RATE
        R.REP<ECL.CATEGORY> = CATEG
        R.REP<ECL.CREDIT.CODE>    = CREDIT.CODE   ;*'100'
        R.REP<ECL.LIMIT.ID>    = LMT.ID
        R.REP<ECL.LIMIT.AMT>   = CR.AMT
        R.REP<ECL.COLL.CODE>   = COLL.CODE
        R.REP<ECL.LD.TYPE>     = LD.TYPE
        R.REP<ECL.CASH.ACCT>   = CASH.ACCT
        R.REP<ECL.CASH.COVER>  = CASH.AMT
        R.REP<ECL.CURRENCY>    = CURR
        CALL F.WRITE(FN.REP,NEW.ID,R.REP)
        CALL JOURNAL.UPDATE(NEW.ID)
    END
    RETURN
*************************************
GETLD:
*=====
    Q.SEL  = "SELECT ":FN.LD:" WITH CATEGORY IN (21050 21051 21052 21053 21054 21055 21058 21060 21061 21062"
    Q.SEL := " 21063 21065 21067 21150 21096 21097 21071 21072 21073 21074 21056 21057 21064 21066 21069 21070 21068)"
    Q.SEL := " AND (AMOUNT NE 0 AND FIN.MAT.DATE GT ":BNK.DATE1:") OR (AMOUNT EQ 0 AND OVERDUE.STATUS EQ 'PDO')"
    Q.SEL := " BY CUSTOMER.ID"

    CALL EB.READLIST(Q.SEL,KEY.LIST.LD,"",SELECTED.LD,ER.MSG.LD)
    IF KEY.LIST.LD THEN
        FOR J = 1 TO SELECTED.LD
            DAYS         = 'C'
            NDPD         = 0
            US.AMT       = 0
            CR.AMT       = 0
            PAY.TYPE     = 0
            ACC.RATE     = 0
            CASH.RATE    = 0
            PRODUCT.TYPE = "funded"
            LD.TYPE      = ''
            YY1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; COLLC = 0
            COLLC = 0 ; OUT.AMT = 0; COLL.AMT = 0; SEC.AMT = 0
            COLL.PERC.ALLOC = 100
            PD.RATE         = 1
            CASH.ACCT       = ''
            CASH.AMT        = 0
*****************************************
            CALL F.READ(FN.LD,KEY.LIST.LD<J>,R.LD,F.LD, ETEXT.LD)
            FAC.ID    = KEY.LIST.LD<J>
            CUS.ID    = R.LD<LD.CUSTOMER.ID>
            CURR      = R.LD<LD.CURRENCY>
            CATEG     = R.LD<LD.CATEGORY>
            ACC.RATE  = R.LD<LD.INTEREST.RATE>
            INT.KEY   = R.LD<LD.INTEREST.KEY>
            LOCAL.LD  = R.LD<LD.LOCAL.REF>
            LD.TYPE   = LOCAL.LD<1,LDLR.PRODUCT.TYPE>

*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,AC.CATEG,CAT.NAME)

            IF LD.TYPE EQ 'FINAL' THEN LD.TYPE = 'Performance'
            IF CATEG EQ '21096' OR CATEG EQ '21097' THEN
                PRODUCT.TYPE = "unfunded"
                PAY.TYPE     = 0
                NDPD         = 0
                IF CURR EQ 'EGP' THEN
                    INT.KEY      = '65'
****MID CORIDOR
                END
                ELSE
                    INT.KEY      = '83'
****LIPOR
                END
            END
            ELSE
                CALL F.READ(FN.SCH,FAC.ID,R.SCH,F.SCH,E.SCH)
                FREQ = R.SCH<LD.SD.FREQUENCY>
*CALL DBR('LD.SCHEDULE.DEFINE':@FM:LD.SD.FREQUENCY,FAC.ID,FREQ)
                FREQ = FREQ<1,1>

                FREQ.LEN = LEN(FREQ)
                IF FREQ.LEN EQ 2 THEN
                    PAY.TYPE = FREQ[1,1]

                END
                ELSE
                    PAY.TYPE = FREQ[1,2]
                END
                BEGIN CASE
                CASE PAY.TYPE EQ 1
                    PAY.TYPE = 12
                CASE PAY.TYPE EQ 3
                    PAY.TYPE = 4
                CASE PAY.TYPE EQ 6
                    PAY.TYPE = 2
                CASE PAY.TYPE EQ 12
                    PAY.TYPE = 1

                CASE 1
                    SCH.DATE1 = R.SCH<LD.SD.DATE><1,1>
                    SCH.DATE2 = R.SCH<LD.SD.DATE><1,3>
                    CALL EB.NO.OF.MONTHS(SCH.DATE1,SCH.DATE2,DIFF.MONTH)

                    BEGIN CASE
                    CASE DIFF.MONTH LT 2
                        PAY.TYPE = 12
                    CASE DIFF.MONTH GE 2 AND DIFF.MONTH LT 4
                        PAY.TYPE = 4
                    CASE DIFF.MONTH GE 4 AND DIFF.MONTH LT 7
                        PAY.TYPE = 2
                    CASE DIFF.MONTH GE 7
                        PAY.TYPE = 1
                    END CASE

                END CASE
            END

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            VALUE.DATE = R.LD<LD.VALUE.DATE>
            MAT.DATE   = R.LD<LD.FIN.MAT.DATE>
*EXP.DATE   = R.LD<LD.EXPIRY.DATE>

            DIFF.MONTH = ''
*CALL EB.NO.OF.MONTHS(VALUE.DATE,EXP.DATE,DIFF.MONTH)
*PAY.TYPE   = DROUND((DIFF.MONTH / 30),0)
            LMT.REF    = R.LD<LD.LIMIT.REFERENCE>

**********TO GET COLLATERAL(BECAUSE IT IS MULTI)*************

            LMTT      = FMT(LMT.REF,"R%10")
            LMT.ID    = CUS.ID:'.':LMTT
            CALL F.READ(FN.LMT,LMT.ID,R.LMT,F.LMT, ETEXT.LMT)

            LMT.CUR = R.LMT<LI.LIMIT.CURRENCY>
            LOCATE LMT.CUR IN CURR.BASE<1,1> SETTING POS THEN
                LMT.RATE = R.BASE<RE.BCP.RATE,POS>
            END

***********TO GET COLLATERAL.CODE*************
            CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,1,1>

            IF CUS.COLL = '' THEN
                CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,2,1>
            END
*Line [ 625 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COLLATERAL.RIGHT':@FM:COLL.RIGHT.COLLATERAL.CODE,CUS.COLL,COLL.CODE)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,CUS.COLL,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
COLL.CODE=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.COLLATERAL.CODE>

************TO GET COLLATERAL AMOUNT**************
            TOT.COLL.AMT = 0
            CALL F.READ(FN.COLL.RIGHT.CUS,CUS.ID,R.COLL.RIGHT.CUS,F.COLL.RIGHT.CUS,ER.COLL.R.CUS)
            LOOP
                REMOVE COLL.RIGHT.ID FROM R.COLL.RIGHT.CUS SETTING POS.COLL.RIGHT
            WHILE COLL.RIGHT.ID:POS.COLL.RIGHT
                FLAG = 'N'
                CALL F.READ(FN.COLL.RIGHT,COLL.RIGHT.ID,R.COLL.RIGHT,F.COLL.RIGHT,ER.COLL.R)
                CALL F.READ(FN.COLL.R,COLL.RIGHT.ID,R.COLL.R,F.COLL.R,ER.COLL.RIGHT)
                COLL.LIMIT.ID       = R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE>

**FINDSTR LMT.ID IN COLL.LIMIT.ID SETTING LMT.POS THEN
                LOCATE LMT.ID IN R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE,1> SETTING LMT.POS THEN

                    FLAG = 'Y'
                    COLL.PERC.ALLOC = R.COLL.R<COLL.RIGHT.PERCENT.ALLOC,LMT.POS>

                END

                LOOP
                    REMOVE COLL.ID FROM R.COLL.RIGHT SETTING POS.COLL
                WHILE COLL.ID:POS.COLL
                    IF FLAG EQ 'Y' AND COLL.ID THEN
                        CALL F.READ(FN.COLL,COLL.ID,R.COLL,F.COLL,ER.COLL)
                        COLL.CURR = R.COLL<COLL.CURRENCY>

                        LOCATE COLL.CURR IN CURR.BASE<1,1> SETTING POS THEN
                            COLL.RATE = R.BASE<RE.BCP.RATE,POS>
                        END

                        COLL.ID.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT> * COLL.RATE
                        TOT.COLL.AMT +=COLL.ID.AMT
                    END
                REPEAT
            REPEAT

**************************

            US.AMT       = R.LD<LD.AMOUNT> * RATE
            COLL.AMT     = (TOT.COLL.AMT * (COLL.PERC.ALLOC/100))
            INT.AMT      = R.LMT<LI.INTERNAL.AMOUNT>
            ADVISE.AMT   = R.LMT<LI.ADVISED.AMOUNT>

            IF ADVISE.AMT EQ 0 OR ADVISE.AMT EQ '' THEN

                CR.AMT       = INT.AMT * LMT.RATE
            END
            ELSE
                CR.AMT       = ADVISE.AMT * LMT.RATE
            END

            OUT.AMT   = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT> * RATE
            COLL.AMT  = COLL.AMT + OUT.AMT

*****PAST DUE*************
            LD.PAY.DUE.ID = 'PD':FAC.ID
*Line [ 689 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,LD.PAY.DUE.ID,LD.AMT.OVER)
F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
FN.F.ITSS.PD.PAYMENT.DUE = ''
CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
CALL F.READ(F.ITSS.PD.PAYMENT.DUE,LD.PAY.DUE.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
LD.AMT.OVER=R.ITSS.PD.PAYMENT.DUE<PD.TOTAL.OVERDUE.AMT>
*Line [ 696 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.PAYMENT.DTE.DUE,LD.PAY.DUE.ID,LD.DUE.DATE)
F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
FN.F.ITSS.PD.PAYMENT.DUE = ''
CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
CALL F.READ(F.ITSS.PD.PAYMENT.DUE,LD.PAY.DUE.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
LD.DUE.DATE=R.ITSS.PD.PAYMENT.DUE<PD.PAYMENT.DTE.DUE>
*Line [ 703 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.CURRENCY,LD.PAY.DUE.ID,PD.CURR)
F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
FN.F.ITSS.PD.PAYMENT.DUE = ''
CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
CALL F.READ(F.ITSS.PD.PAYMENT.DUE,LD.PAY.DUE.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
PD.CURR=R.ITSS.PD.PAYMENT.DUE<PD.CURRENCY>
*Line [ 668 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.DUE   = DCOUNT(LD.DUE.DATE,@VM)
            LD.DUE.DATE = LD.DUE.DATE<1,NO.DUE>
*Line [ 713 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('PD.PAYMENT.DUE':@FM:PD.TOTAL.OVERDUE.AMT,LD.PAY.DUE.ID,TOT.OVERDUE.AMT)
F.ITSS.PD.PAYMENT.DUE = 'FBNK.PD.PAYMENT.DUE'
FN.F.ITSS.PD.PAYMENT.DUE = ''
CALL OPF(F.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE)
CALL F.READ(F.ITSS.PD.PAYMENT.DUE,LD.PAY.DUE.ID,R.ITSS.PD.PAYMENT.DUE,FN.F.ITSS.PD.PAYMENT.DUE,ERROR.PD.PAYMENT.DUE)
TOT.OVERDUE.AMT=R.ITSS.PD.PAYMENT.DUE<PD.TOTAL.OVERDUE.AMT>

            LOCATE PD.CURR IN CURR.BASE<1,1> SETTING POS THEN
                PD.RATE = R.BASE<RE.BCP.RATE,POS>
            END

            TOT.OVERDUE.AMT = TOT.OVERDUE.AMT * PD.RATE

            US.AMT = US.AMT + TOT.OVERDUE.AMT
            IF CATEG EQ '21055' THEN
                CR.AMT    = US.AMT
            END

            UNDRAWN.AMT = CR.AMT - US.AMT

            IF LD.DUE.DATE THEN
                DAYS = 'C'
                CALL CDD("",LD.DUE.DATE,TODAY,DAYS)
                NDPD = DAYS
            END
            ELSE
                NDPD = 0
            END

            IF CATEG EQ 21096 OR CATEG EQ 21097 THEN
                EXP.DATE = R.LD<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
            END
            ELSE
                EXP.DATE = MAT.DATE
            END

********GET INTEREST.RATE*****
            IF INT.KEY NE '' THEN
                INT.ID.LK = INT.KEY:CURR:"..."
                I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
                CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
                INT.ID = I.LIST<SELECTED.I>
                CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
                ACC.RATE = R.INT<EB.BIN.INTEREST.RATE>

            END

            IF INT.KEY NE '65' AND INT.KEY NE '83' THEN
                INT.RATE = ACC.RATE + R.LD<LD.INTEREST.SPREAD>
            END ELSE
                INT.RATE = ACC.RATE
            END

**************CASH COVER****************
            CASH.ACCT = LOCAL.LD<1,LDLR.CREDIT.ACCT>
            WS.CASH.CATEG = CASH.ACCT[11,4]
            IF WS.CASH.CATEG EQ '3005' THEN
                CASH.AMT = LOCAL.LD<1,LDLR.MARGIN.AMT>
*CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,CASH.ACCT,CASH.AMT)

*Line [ 774 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.CURRENCY,CASH.ACCT,CASH.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,CASH.ACCT,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CASH.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
                LOCATE CASH.CURR IN CURR.BASE<1,1> SETTING POS THEN

                    CASH.RATE = R.BASE<RE.BCP.RATE,POS>
                END
                CASH.AMT = DROUND(CASH.AMT * CASH.RATE,2)
            END
****************************************
            GOSUB INSREC
        NEXT J
    END

    RETURN
**************************
GETLC:
*=====

    M.FLAG = 'LC'
    V.SEL  = "SELECT FBNK.LETTER.OF.CREDIT WITH APPLICANT.CUSTNO NE '' AND LIABILITY.AMT GT 0"
    V.SEL := " AND CATEGORY.CODE IN (23100 23102 23103 23150 23152 23153 23202 23203 23552 23553 23559 23100 23102 23103 23150 23152 23153)"
*    V.SEL := " AND LIMIT.REFERENCE UNLIKE 25120... AND LIMIT.REFERENCE UNLIKE 35120..."
    V.SEL := " BY APPLICANT.CUSTNO BY LIMIT.REFERENCE"
    CALL EB.READLIST(V.SEL,KEY.LIST.LC,"",SELECTED.LC,ER.MSG.LC)
    IF KEY.LIST.LC THEN
        FOR Q = 1 TO SELECTED.LC
            DAYS    = 'C'
            NDPD    = 0
            US.AMT  = 0
            CR.AMT  = 0
            PAY.TYPE = 0
            INT.RATE = 0
            COLLC = 0; COLL.AMT = 0; OUT.AMT = 0
            YY1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; COLLC = 0
            PRODUCT.TYPE = "unfunded"
            COLL.PERC.ALLOC = 100
            CASH.ACCT = ''
            CASH.AMT  = 0
            CASH.RATE = 0
****************
            CALL F.READ(FN.LC,KEY.LIST.LC<Q>,R.LC,F.LC, ETEXT.LC)
            FAC.ID   = KEY.LIST.LC<Q>
            CUS.ID   = R.LC<TF.LC.APPLICANT.CUSTNO>
            CATEG    = R.LC<TF.LC.CATEGORY.CODE>
            CURR     = R.LC<TF.LC.LC.CURRENCY>
            LC.AMT   = R.LC<TF.LC.LIABILITY.AMT>
            EXP.DATE = R.LC<TF.LC.EXPIRY.DATE>
            LMT.REF  = R.LC<TF.LC.LIMIT.REFERENCE>
*Line [ 827 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CAT.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CAT.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            IF CURR EQ 'EGP' THEN
                INT.KEY      = '65'
            END
            ELSE
                INT.KEY      = '83'
            END

*********TO GET COLLATERAL(BECAUSE IT IS MULTI)*************

            LMTT      = FMT(LMT.REF,"R%10")
            LMT.ID    = CUS.ID:'.':LMTT
            CALL F.READ(FN.LMT,LMT.ID,R.LMT,F.LMT, ETEXT.LMT)

            LMT.CUR = R.LMT<LI.LIMIT.CURRENCY>
            LOCATE LMT.CUR IN CURR.BASE<1,1> SETTING POS THEN
                LMT.RATE = R.BASE<RE.BCP.RATE,POS>
            END

*******TO GET COLLATERAL.CODE*****************************
            CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,1,1>
            IF CUS.COLL = '' THEN
                CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,2,1>
            END
*Line [ 862 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COLLATERAL.RIGHT':@FM:COLL.RIGHT.COLLATERAL.CODE,CUS.COLL,COLL.CODE)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,CUS.COLL,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
COLL.CODE=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.COLLATERAL.CODE>


************TO GET COLLATERAL AMOUNT**********************
            TOT.COLL.AMT = 0
            CALL F.READ(FN.COLL.RIGHT.CUS,CUS.ID,R.COLL.RIGHT.CUS,F.COLL.RIGHT.CUS,ER.COLL.R.CUS)
            LOOP
                REMOVE COLL.RIGHT.ID FROM R.COLL.RIGHT.CUS SETTING POS.COLL.RIGHT
            WHILE COLL.RIGHT.ID:POS.COLL.RIGHT
                FLAG = 'N'
                CALL F.READ(FN.COLL.RIGHT,COLL.RIGHT.ID,R.COLL.RIGHT,F.COLL.RIGHT,ER.COLL.R)
                CALL F.READ(FN.COLL.R,COLL.RIGHT.ID,R.COLL.R,F.COLL.R,ER.COLL.RIGHT)
                COLL.LIMIT.ID       = R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE>

**FINDSTR LMT.ID IN COLL.LIMIT.ID SETTING LMT.POS THEN
                LOCATE LMT.ID IN R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE,1> SETTING LMT.POS THEN
                    FLAG = 'Y'
                    COLL.PERC.ALLOC = R.COLL.R<COLL.RIGHT.PERCENT.ALLOC,LMT.POS>
                END

                LOOP
                    REMOVE COLL.ID FROM R.COLL.RIGHT SETTING POS.COLL
                WHILE COLL.ID:POS.COLL
                    IF FLAG EQ 'Y' AND COLL.ID THEN
                        CALL F.READ(FN.COLL,COLL.ID,R.COLL,F.COLL,ER.COLL)
                        COLL.CURR = R.COLL<COLL.CURRENCY>
                        LOCATE COLL.CURR IN CURR.BASE<1,1> SETTING POS THEN
                            COLL.RATE = R.BASE<RE.BCP.RATE,POS>
                        END

                        COLL.ID.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT> * COLL.RATE
                        TOT.COLL.AMT +=COLL.ID.AMT
                    END
                REPEAT
            REPEAT
****************************

********GET INTEREST.RATE*************************
            IF INT.KEY NE '' THEN
                INT.ID.LK = INT.KEY:CURR:"..."
                I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
                CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
                INT.ID = I.LIST<SELECTED.I>
                CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
                ACC.RATE = R.INT<EB.BIN.INTEREST.RATE>
                INT.RATE = ACC.RATE
            END
*********************************

            US.AMT    = LC.AMT * RATE
            COLL.AMT  = (TOT.COLL.AMT * (COLL.PERC.ALLOC/100))
            INT.AMT    = R.LMT<LI.INTERNAL.AMOUNT>
            ADVISE.AMT   = R.LMT<LI.ADVISED.AMOUNT>

            IF ADVISE.AMT EQ 0 OR ADVISE.AMT EQ '' THEN

                CR.AMT       = INT.AMT * LMT.RATE
            END
            ELSE
                CR.AMT       = ADVISE.AMT * LMT.RATE
            END

*CR.AMT    = R.LMT<LI.ADVISED.AMOUNT> * LMT.RATE
            OUT.AMT   = R.LC<TF.LC.PRO.OUT.AMOUNT> * RATE
            COLL.AMT  = COLL.AMT + OUT.AMT

            UNDRAWN.AMT = CR.AMT - US.AMT

************CASH COVER*************************
            CASH.ACCT = R.LC<TF.LC.CREDIT.PROVIS.ACC>
            CASH.CAT  = CASH.ACCT[11,4]

            IF (CASH.CAT GE 3010 AND CASH.CAT LE 3014) OR (CASH.CAT EQ 3017) OR (CASH.CAT EQ 3065) THEN
**      CALL DBR('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,CASH.ACCT,CASH.AMT)
**      CALL DBR('ACCOUNT':@FM:AC.CURRENCY,CASH.ACCT,CASH.CURR)
**      LOCATE CASH.CURR IN CURR.BASE<1,1> SETTING POS THEN

**          CASH.RATE = R.BASE<RE.BCP.RATE,POS>
**      END
**      CASH.AMT = DROUND(CASH.AMT * CASH.RATE,2)

*CASH.AMT = R.LC<TF.LC.PRO.OUT.AMOUNT>
**END
** ELSE
                CASH.AMT = 0
            END
***********************************************
            GOSUB INSREC
        NEXT Q
    END

    RETURN
****************************
GETDR:
*==============
*DEBUG
    T.SEL = "SELECT ":FN.DRAWINGS:" WITH MATURITY.REVIEW GT ":LAST.P.INT
    T.SEL := " AND CUSTOMER.LINK UNLIKE 99... BY CUSTOMER.LINK"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            DAYS     = 'C'
            NDPD     = 0
            US.AMT   = 0
            CR.AMT   = 0
            PAY.TYPE = 0
            INT.RATE = 0
            R.ECL1   = ''
            COLLC = 0; COLL.AMT = 0; OUT.AMT = 0
            YY1   = 0; H1 = 0 ; YY2 = 0 ; H2 = 0 ; COLLC = 0
            R.LC  = ''
            PRODUCT.TYPE = "unfunded"
            COLL.PERC.ALLOC = 100
            CASH.AMT        = 0
**************************

            FAC.ID = KEY.LIST<I>
            CALL F.READ(FN.DRAWINGS,KEY.LIST<I>, R.DRAWINGS,F.DRAWINGS, ETEXT)
            CUS.ID   = R.DRAWINGS<TF.DR.CUSTOMER.LINK>
            CURR     =   R.DRAWINGS<TF.DR.DRAW.CURRENCY>
            EXP.DATE = R.DRAWINGS<TF.DR.MATURITY.REVIEW>
            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            DR.AMT   = R.DRAWINGS<TF.DR.DOCUMENT.AMOUNT>

            LMT.REF  = R.DRAWINGS<TF.DR.LIMIT.REFERENCE>
            LMTT     = FMT(LMT.REF,"R%10")
            LMT.ID   = CUS.ID:'.':LMTT
            CALL F.READ(FN.LMT,LMT.ID,R.LMT,F.LMT, ETEXT.LMT)
            LMT.CUR = R.LMT<LI.LIMIT.CURRENCY>
            LOCATE LMT.CUR IN CURR.BASE<1,1> SETTING POS THEN
                LMT.RATE = R.BASE<RE.BCP.RATE,POS>
            END

            LC.ID = KEY.LIST<I>[1,12]
            CALL F.READ(FN.LC,LC.ID,R.LC,F.LC, ETEXT.LC)
            CATEG = R.LC<TF.LC.CATEGORY.CODE>
            IF CURR EQ 'EGP' THEN
                INT.KEY      = '65'
            END
            ELSE
                INT.KEY      = '83'
            END

*******TO GET COLLATERAL.CODE*****************************
            CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,1,1>
            IF CUS.COLL = '' THEN
                CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,2,1>
            END
*Line [ 1020 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COLLATERAL.RIGHT':@FM:COLL.RIGHT.COLLATERAL.CODE,CUS.COLL,COLL.CODE)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,CUS.COLL,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
COLL.CODE=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.COLLATERAL.CODE>


************TO GET COLLATERAL AMOUNT**********************
            TOT.COLL.AMT = 0
            CALL F.READ(FN.COLL.RIGHT.CUS,CUS.ID,R.COLL.RIGHT.CUS,F.COLL.RIGHT.CUS,ER.COLL.R.CUS)
            LOOP
                REMOVE COLL.RIGHT.ID FROM R.COLL.RIGHT.CUS SETTING POS.COLL.RIGHT
            WHILE COLL.RIGHT.ID:POS.COLL.RIGHT
                FLAG = 'N'
                CALL F.READ(FN.COLL.RIGHT,COLL.RIGHT.ID,R.COLL.RIGHT,F.COLL.RIGHT,ER.COLL.R)
                CALL F.READ(FN.COLL.R,COLL.RIGHT.ID,R.COLL.R,F.COLL.R,ER.COLL.RIGHT)
                COLL.LIMIT.ID       = R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE>

**FINDSTR LMT.ID IN COLL.LIMIT.ID SETTING LMT.POS THEN
                LOCATE LMT.ID IN R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE,1> SETTING LMT.POS THEN
                    FLAG = 'Y'
                    COLL.PERC.ALLOC = R.COLL.R<COLL.RIGHT.PERCENT.ALLOC,LMT.POS>
                END

                LOOP
                    REMOVE COLL.ID FROM R.COLL.RIGHT SETTING POS.COLL
                WHILE COLL.ID:POS.COLL
                    IF FLAG EQ 'Y' AND COLL.ID THEN
                        CALL F.READ(FN.COLL,COLL.ID,R.COLL,F.COLL,ER.COLL)
                        COLL.CURR = R.COLL<COLL.CURRENCY>
                        LOCATE COLL.CURR IN CURR.BASE<1,1> SETTING POS THEN
                            COLL.RATE = R.BASE<RE.BCP.RATE,POS>
                        END

                        COLL.ID.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT> * COLL.RATE
                        TOT.COLL.AMT +=COLL.ID.AMT
                    END
                REPEAT
            REPEAT

********GET INTEREST.RATE*************************
            IF INT.KEY NE '' THEN
                INT.ID.LK = INT.KEY:CURR:"..."
                I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
                CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
                INT.ID = I.LIST<SELECTED.I>
                CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
                ACC.RATE = R.INT<EB.BIN.INTEREST.RATE>
                INT.RATE = ACC.RATE
            END
*********************************

            US.AMT    = DR.AMT * RATE
            COLL.AMT  = (TOT.COLL.AMT * (COLL.PERC.ALLOC/100))
            INT.AMT   = R.LMT<LI.INTERNAL.AMOUNT>
            ADVISE.AMT  = R.LMT<LI.ADVISED.AMOUNT>

            IF ADVISE.AMT EQ 0 OR ADVISE.AMT EQ '' THEN

                CR.AMT  = INT.AMT * LMT.RATE
            END
            ELSE
                CR.AMT  = ADVISE.AMT * LMT.RATE
            END
            OUT.AMT   = R.LC<TF.LC.PRO.OUT.AMOUNT> * RATE
            COLL.AMT  = COLL.AMT + OUT.AMT

            UNDRAWN.AMT = CR.AMT - US.AMT
            GOSUB INSREC
        NEXT I
    END

    RETURN
**************************
GETLCEXP:
*========

    M.FLAG = 'LC'
    V.SEL  = "SELECT FBNK.LETTER.OF.CREDIT WITH BENEFICIARY.CUSTNO NE '' AND LIABILITY.AMT GT 0"
    V.SEL := " AND CATEGORY.CODE IN (23502 23552 23553 23559)"
*    V.SEL := " AND LIMIT.REFERENCE UNLIKE 25120... AND LIMIT.REFERENCE UNLIKE 35120..."
    V.SEL := " BY BENEFICIARY.CUSTNO BY LIMIT.REFERENCE"
    CALL EB.READLIST(V.SEL,KEY.LIST.LC,"",SELECTED.LC,ER.MSG.LC)
    IF KEY.LIST.LC THEN
        FOR Q = 1 TO SELECTED.LC
            DAYS    = 'C'
            NDPD    = 0
            US.AMT  = 0
            CR.AMT  = 0
            PAY.TYPE = 0
            INT.RATE = 0
            COLLC = 0; COLL.AMT = 0; OUT.AMT = 0
            YY1 = 0 ; H1 = 0 ; YY2 = 0 ; H2 = 0 ; COLLC = 0
            PRODUCT.TYPE = "unfunded"
            COLL.PERC.ALLOC = 100
            CASH.AMT        = 0
****************
            CALL F.READ(FN.LC,KEY.LIST.LC<Q>,R.LC,F.LC, ETEXT.LC)
            FAC.ID   = KEY.LIST.LC<Q>
            CUS.ID   = R.LC<TF.LC.BENEFICIARY.CUSTNO>
            CATEG    = R.LC<TF.LC.CATEGORY.CODE>
            CURR     = R.LC<TF.LC.LC.CURRENCY>
            LC.AMT   = R.LC<TF.LC.LIABILITY.AMT>
            EXP.DATE = R.LC<TF.LC.EXPIRY.DATE>
            LMT.REF  = R.LC<TF.LC.LIMIT.REFERENCE>
*Line [ 1127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,CAT.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CAT.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

            LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN
                RATE = R.BASE<RE.BCP.RATE,POS>
            END

            IF CURR EQ 'EGP' THEN
                INT.KEY      = '65'
            END
            ELSE
                INT.KEY      = '83'
            END

*********TO GET COLLATERAL(BECAUSE IT IS MULTI)*************

            LMTT      = FMT(LMT.REF,"R%10")
            LMT.ID    = CUS.ID:'.':LMTT
            CALL F.READ(FN.LMT,LMT.ID,R.LMT,F.LMT, ETEXT.LMT)

            LMT.CUR = R.LMT<LI.LIMIT.CURRENCY>
            LOCATE LMT.CUR IN CURR.BASE<1,1> SETTING POS THEN
                LMT.RATE = R.BASE<RE.BCP.RATE,POS>
            END

*******TO GET COLLATERAL.CODE*****************************
            CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,1,1>
            IF CUS.COLL = '' THEN
                CUS.COLL  = R.LMT<LI.COLLAT.RIGHT,2,1>
            END
*Line [ 1162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('COLLATERAL.RIGHT':@FM:COLL.RIGHT.COLLATERAL.CODE,CUS.COLL,COLL.CODE)
F.ITSS.COLLATERAL.RIGHT = 'F.COLLATERAL.RIGHT'
FN.F.ITSS.COLLATERAL.RIGHT = ''
CALL OPF(F.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT)
CALL F.READ(F.ITSS.COLLATERAL.RIGHT,CUS.COLL,R.ITSS.COLLATERAL.RIGHT,FN.F.ITSS.COLLATERAL.RIGHT,ERROR.COLLATERAL.RIGHT)
COLL.CODE=R.ITSS.COLLATERAL.RIGHT<COLL.RIGHT.COLLATERAL.CODE>


************TO GET COLLATERAL AMOUNT**********************
            TOT.COLL.AMT = 0
            CALL F.READ(FN.COLL.RIGHT.CUS,CUS.ID,R.COLL.RIGHT.CUS,F.COLL.RIGHT.CUS,ER.COLL.R.CUS)
            LOOP
                REMOVE COLL.RIGHT.ID FROM R.COLL.RIGHT.CUS SETTING POS.COLL.RIGHT
            WHILE COLL.RIGHT.ID:POS.COLL.RIGHT
                FLAG = 'N'
                CALL F.READ(FN.COLL.RIGHT,COLL.RIGHT.ID,R.COLL.RIGHT,F.COLL.RIGHT,ER.COLL.R)
                CALL F.READ(FN.COLL.R,COLL.RIGHT.ID,R.COLL.R,F.COLL.R,ER.COLL.RIGHT)
                COLL.LIMIT.ID       = R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE>

**FINDSTR LMT.ID IN COLL.LIMIT.ID SETTING LMT.POS THEN
                LOCATE LMT.ID IN R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE,1> SETTING LMT.POS THEN
                    FLAG = 'Y'
                    COLL.PERC.ALLOC = R.COLL.R<COLL.RIGHT.PERCENT.ALLOC,LMT.POS>
                END

                LOOP
                    REMOVE COLL.ID FROM R.COLL.RIGHT SETTING POS.COLL
                WHILE COLL.ID:POS.COLL
                    IF FLAG EQ 'Y' AND COLL.ID THEN
                        CALL F.READ(FN.COLL,COLL.ID,R.COLL,F.COLL,ER.COLL)
                        COLL.CURR = R.COLL<COLL.CURRENCY>
                        LOCATE COLL.CURR IN CURR.BASE<1,1> SETTING POS THEN
                            COLL.RATE = R.BASE<RE.BCP.RATE,POS>
                        END

                        COLL.ID.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT> * COLL.RATE
                        TOT.COLL.AMT +=COLL.ID.AMT
                    END
                REPEAT
            REPEAT
****************************

********GET INTEREST.RATE*************************
            IF INT.KEY NE '' THEN
                INT.ID.LK = INT.KEY:CURR:"..."
                I.SEL = "SSELECT ":FN.INT:" WITH @ID LIKE ":INT.ID.LK
                CALL EB.READLIST(I.SEL,I.LIST,'',SELECTED.I,ER.MSG.I)
                INT.ID = I.LIST<SELECTED.I>
                CALL F.READ(FN.INT,INT.ID,R.INT,F.INT,ERR.I1)
                ACC.RATE = R.INT<EB.BIN.INTEREST.RATE>
                INT.RATE = ACC.RATE
            END
*********************************

            US.AMT    = LC.AMT * RATE
            COLL.AMT  = (TOT.COLL.AMT * (COLL.PERC.ALLOC/100))
            INT.AMT    = R.LMT<LI.INTERNAL.AMOUNT>
            ADVISE.AMT   = R.LMT<LI.ADVISED.AMOUNT>

            IF ADVISE.AMT EQ 0 OR ADVISE.AMT EQ '' THEN

                CR.AMT       = INT.AMT * LMT.RATE
            END
            ELSE
                CR.AMT       = ADVISE.AMT * LMT.RATE
            END

*CR.AMT    = R.LMT<LI.ADVISED.AMOUNT> * LMT.RATE
            OUT.AMT   = R.LC<TF.LC.PRO.OUT.AMOUNT> * RATE
            COLL.AMT  = COLL.AMT + OUT.AMT

            UNDRAWN.AMT = CR.AMT - US.AMT

            GOSUB INSREC
        NEXT Q
    END

    RETURN
****************************
END
