* @ValidationCode : MjotMTU1ODc2ODQyMjpDcDEyNTI6MTY0NDkzMTkxMjg3Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:31:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*** ���� �������� ������� ������  ***
*** ���� ��� ������ - ������� - ������� - ������� - ���� ��� ������ ***
*** �������� ������� *** *** ���� ������� ���� ������ ***
*** CREATED BY KHALED ***
***=================================
SUBROUTINE SBR.LG.004

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
    REPORT.ID='SBR.LG.004'
    CALL PRINTER.ON(REPORT.ID,'')

    YTEXT = "Enter CUSTOMER No. : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    ID = COMI

    YTEXT = "Enter CURRENCY : "
    CALL TXTINP(YTEXT, 8, 22, "3", "A")
    CUR = COMI


    YTEXT = "Enter Date from : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATF = COMI

    YTEXT = "Enter Date to : "
    CALL TXTINP(YTEXT, 8, 22, "10", "A")
    DATT = COMI

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.CAT = 'FBNK.CATEG.ENTRY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""
    KEY.LIST4="" ; SELECTED4="" ;  ER.MSG4=""

    XX1 = SPACE(132)  ; XX2 = SPACE(132)  ; XX3 = SPACE(132)  ; XX4 = SPACE(132)
    XX5 = SPACE(132)  ; XX6 = SPACE(132)  ; XX7 = SPACE(132)  ; XX8 = SPACE(132)
    XX9 = SPACE(132)  ; XX10 = SPACE(132) ; XX11 = SPACE(132) ; XX12 = SPACE(132)
    XX13 = SPACE(132) ; XX14 = SPACE(132) ; XX15 = SPACE(132) ; XX16 = SPACE(132)
    XX17 = SPACE(132) ; XX18 = SPACE(132) ; XX19 = SPACE(132) ; XX20 = SPACE(132)
    XX21 = SPACE(132) ; XX22 = SPACE(132) ; XX23 = SPACE(132) ; XX24 = SPACE(132)

    AMT1.TOT1 = 0 ; AMT1.TOT2 = 0 ; AMT1.TOT3 = 0
    AMT11.TOT1 = 0 ; AMT11.TOT2 = 0 ; AMT11.TOT3 = 0
    AMT2.TOT1 = 0 ; AMT2.TOT2 = 0 ; AMT2.TOT3 = 0
    AMT3.TOT1 = 0 ; AMT3.TOT2 = 0 ; AMT3.TOT3 = 0
    AMT4.TOT1 = 0 ; AMT4.TOT2 = 0 ; AMT4.TOT3 = 0
    AMT5.TOT1 = 0 ; AMT5.TOT2 = 0 ; AMT5.TOT3 = 0
    COM.AMT = 0
    COMP = ID.COMPANY
RETURN
*========================================================================
PROCESS:
*** ���� ��� ������ ***
*-----------------------
    T.SEL = "SELECT ":FN.LD: " WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND ISSUE.DATE LT ":DATF:" AND CURRENCY EQ ":CUR
    T.SEL := " BY LIMIT.REFERENCE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,E1)

            REF1 = R.LD<LD.LIMIT.REFERENCE>
            REF = REF1[1,4]

            IF REF EQ '2505' THEN
                AMT11.TOT1 += R.LD<LD.AMOUNT>
            END ELSE
                IF REF EQ '2510' THEN
                    AMT11.TOT2 += R.LD<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2520' THEN
                        AMT11.TOT3 += R.LD<LD.AMOUNT>
                    END
                END
            END
        NEXT Z
**  XX3<1,1>[20,15]  = AMT1.TOT1
**  XX4<1,1>[20,15]  = AMT1.TOT2
**  XX5<1,1>[20,15]  = AMT1.TOT3
    END
*--------------------------------------------------
*** ������� ***
*--------------
    T.SEL1 = "SELECT ":FN.LD: " WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND ISSUE.DATE GE ":DATF:" AND ISSUE.DATE LE ":DATT:" AND CURRENCY EQ ":CUR:" AND OPERATION.CODE IN (1111 1232 1234) BY AMOUNT"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.LD,KEY.LIST1<I>,R.LD,F.LD,E2)
            REF1 = R.LD<LD.LIMIT.REFERENCE>
            REF = REF1[1,4]

            IF REF EQ '2505' THEN
                AMT2.TOT1 += R.LD<LD.AMOUNT>
            END ELSE
                IF REF EQ '2510' THEN
                    AMT2.TOT2 += R.LD<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2520' THEN
                        AMT2.TOT3 += R.LD<LD.AMOUNT>
                    END
                END
            END
        NEXT I
        DATS = R.LD<LD.LOCAL.REF><1,LDLR.ISSUE.DATE>
        ISU.DAY  = FMT(DATS,"####/##/##")

        XX10<1,1>[40,15] = AMT2.TOT1
        XX11<1,1>[40,15] = AMT2.TOT2
        XX12<1,1>[40,15] = AMT2.TOT3
    END

*--------------------------------------------------
*** ������� ***
*---------------
    T.SEL3 =  "SELECT ":FN.LD.H:" WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND FIN.MAT.DATE GE ":DATF:" AND FIN.MAT.DATE LE ":DATT:" AND CURRENCY EQ ":CUR:" AND STATUS EQ 'LIQ' AND OPERATION.CODE IN (1401 1402 1403)"
    T.SEL3 := " BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    IF SELECTED3 THEN
        FOR K = 1 TO SELECTED3
            CALL F.READ(FN.LD.H,KEY.LIST3<K>,R.LD.H,F.LD.H,E4)
            CALL F.READ(FN.LD.H,KEY.LIST3<K+1>,R.LD.H1,F.LD.H,E2)
            IF FIELD(KEY.LIST3<K>,';',1) NE FIELD(KEY.LIST3<K+1>,';',1) THEN
                REF1 = R.LD.H<LD.LIMIT.REFERENCE>
                REF = REF1[1,4]

                IF REF EQ '2505' THEN
                    AMT4.TOT1 += R.LD.H<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2510' THEN
                        AMT4.TOT2 += R.LD.H<LD.AMOUNT>
                    END ELSE
                        IF REF EQ '2520' THEN
                            AMT4.TOT3 += R.LD.H<LD.AMOUNT>
                        END
                    END
                END
            END
        NEXT K
        XX13<1,1>[40,15]  = AMT4.TOT1
        XX14<1,1>[40,15]  = AMT4.TOT2
        XX15<1,1>[40,15]  = AMT4.TOT3
    END
*--------------------------------------------------
*** ���������� ***
*-----------------
    T.SEL4 = "SELECT ":FN.LD.H: " WITH CUSTOMER.ID EQ ":ID:" AND CATEGORY EQ 21096 AND FIN.MAT.DATE GE ":DATF:" AND FIN.MAT.DATE LE ":DATT:" AND CURRENCY EQ ":CUR:" AND STATUS EQ 'LIQ' AND OPERATION.CODE IN (1233 1235 1301 1302 1303 1304 1305 1306 1307 1308)"
    T.SEL4 := "BY @ID"
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
    IF SELECTED4 THEN
        FOR D = 1 TO SELECTED4
            CALL F.READ(FN.LD.H,KEY.LIST4<D>,R.LD.H,F.LD.H,E3)
            CALL F.READ(FN.LD.H,KEY.LIST4<D+1>,R.LD.H1,F.LD.H,E2)
            IF FIELD(KEY.LIST4<D>,';',1) NE FIELD(KEY.LIST4<D+1>,';',1) THEN
                REF1 = R.LD.H<LD.LIMIT.REFERENCE>
                REF = REF1[1,4]

                IF REF EQ '2505' THEN
                    AMT5.TOT1 += R.LD.H<LD.AMOUNT>
                END ELSE
                    IF REF EQ '2510' THEN
                        AMT5.TOT2 += R.LD.H<LD.AMOUNT>
                    END ELSE
                        IF REF EQ '2520' THEN
                            AMT5.TOT3 += R.LD.H<LD.AMOUNT>
                        END
                    END
                END
            END
        NEXT D
        XX16<1,1>[40,15]  = AMT5.TOT1
        XX17<1,1>[40,15]  = AMT5.TOT2
        XX18<1,1>[40,15]  = AMT5.TOT3
    END
*-------------------------------------------------
***  ��� ������ + ������� + �������  ***
*-------------------------------------
    AMT1.TOT1 = AMT11.TOT1 + AMT5.TOT1 + AMT4.TOT1
    AMT1.TOT2 = AMT11.TOT2 + AMT5.TOT2 + AMT4.TOT2
    AMT1.TOT3 = AMT11.TOT3 + AMT5.TOT3 + AMT4.TOT3

    XX3<1,1>[40,15]  = AMT1.TOT1
    XX4<1,1>[40,15]  = AMT1.TOT2
    XX5<1,1>[40,15]  = AMT1.TOT3
*-------------------------------------------------
*** ���� ��� ������ ***
*----------------------
    AMT3.TOT1 = (AMT1.TOT1 + AMT2.TOT1) - (AMT4.TOT1 + AMT5.TOT1)
    AMT3.TOT2 = (AMT1.TOT2 + AMT2.TOT2) - (AMT4.TOT2 + AMT5.TOT2)
    AMT3.TOT3 = (AMT1.TOT3 + AMT2.TOT3) - (AMT4.TOT3 + AMT5.TOT3)

    XX19<1,1>[40,15]  = AMT3.TOT1
    XX20<1,1>[40,15]  = AMT3.TOT2
    XX21<1,1>[40,15]  = AMT3.TOT3

*-------------------------------------------------
*** �������� ������� ***
*----------------------
    T.SEL2 = "SELECT ":FN.CAT: " WITH CUSTOMER.ID EQ ":ID:" AND PRODUCT.CATEGORY EQ 21096 AND BOOKING.DATE GE ":DATF:" AND BOOKING.DATE LE ":DATT:" AND CURRENCY EQ ":CUR
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR C = 1 TO SELECTED2
            CALL F.READ(FN.CAT,KEY.LIST2<C>,R.CAT,F.CAT,E6)
            IF CUR EQ 'EGP' THEN
                COM.AMT += R.CAT<AC.CAT.AMOUNT.LCY>
            END ELSE
                IF CUR NE 'EGP' THEN
                    COM.AMT += R.CAT<AC.CAT.AMOUNT.FCY>
                END
            END
        NEXT C
        XX22<1,1>[40,15]  = COM.AMT
    END
*-------------------------------------------------
*** ���� ������� ***
*---------------
    FIRST.AMT = AMT1.TOT1 + AMT1.TOT2 + AMT1.TOT3
    ISU.AMT   = AMT2.TOT1 + AMT2.TOT2 + AMT2.TOT3
    LAST.AMT  = AMT3.TOT1 + AMT3.TOT2 + AMT3.TOT3
    H.AMT     = FIRST.AMT + ISU.AMT
    IF H.AMT GE LAST.AMT THEN
        XX23<1,1>[40,15] = H.AMT
    END ELSE
        XX23<1,1>[40,15] = LAST.AMT
    END
*-------------------------------------------------
    XX2<1,1>[1,35]  = '���� ��� ������ '
    XX3<1,1>[1,35]  = '���� �������        = '
    XX4<1,1>[1,35]  = '����� �������       = '
    XX5<1,1>[1,35]  = '����� ����� ������� = '
    XX6<1,1>[1,55]  = '������ ������ ������� / ����� ���� ������'
    XX7<1,1>[1,55]  = '������ ������ ���� �� �������� ���� ������'
    XX8<1,1>[1,55]  = '������ ������ ���� �� ������� / ������� ���� ������'
    XX9<1,1>[1,35]  = '���� ��� ������'
    XX10<1,1>[1,35] = '���� �������        = '
    XX11<1,1>[1,35] = '����� �������       = '
    XX12<1,1>[1,35] = '����� ����� ������� = '
    XX13<1,1>[1,35] = '���� �������        = '
    XX14<1,1>[1,35] = '����� �������       = '
    XX15<1,1>[1,35] = '����� ����� ������� = '
    XX16<1,1>[1,35] = '���� �������        = '
    XX17<1,1>[1,35] = '����� �������       = '
    XX18<1,1>[1,35] = '����� ����� ������� = '
    XX19<1,1>[1,35] = '���� �������        = '
    XX20<1,1>[1,35] = '����� �������       = '
    XX21<1,1>[1,35] = '����� ����� ������� = '
    XX22<1,1>[1,35] = '������ �������� �������  = '
    XX23<1,1>[1,35] = '���� ������� ���� ������ = '
**  XX23<1,1>[50,15] = '������ : '
    XX24<1,1>[1,35] = '����� ������ ������'
*--------------------------------------------------
    PRINT XX2<1,1>
    PRINT STR('*',15)
    PRINT XX3<1,1>
    PRINT XX4<1,1>
    PRINT XX5<1,1>
    PRINT STR(' ',120)
    PRINT XX6<1,1>
    PRINT STR('*',30)
    PRINT XX10<1,1>
    PRINT XX11<1,1>
    PRINT XX12<1,1>
    PRINT STR(' ',120)
    PRINT XX7<1,1>
    PRINT STR('*',30)
    PRINT XX13<1,1>
    PRINT XX14<1,1>
    PRINT XX15<1,1>
    PRINT STR(' ',120)
    PRINT XX8<1,1>
    PRINT STR('*',30)
    PRINT XX16<1,1>
    PRINT XX17<1,1>
    PRINT XX18<1,1>
    PRINT STR(' ',120)
    PRINT XX9<1,1>
    PRINT STR('*',15)
    PRINT XX19<1,1>
    PRINT XX20<1,1>
    PRINT XX21<1,1>
    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT XX22<1,1>
    PRINT STR('*',20)
    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT XX23<1,1>
    PRINT STR('*',20)

    PRINT STR('=',120)
    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT XX24<1,1>
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 356 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY = FMT(DATY,"####/##/##")
    DATF1 = FMT(DATF,"####/##/##")
    DATT1 = FMT(DATT,"####/##/##")
    CUST.ID = ID
*Line [ 369 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
    F.ITSS.CUSTOMER = 'F.CUSTOMER'
    FN.F.ITSS.CUSTOMER = ''
    CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
    CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
    LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
*Line [ 377 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CUR,CUR.DESC)
    F.ITSS.CURRENCY = 'F.CURRENCY'
    FN.F.ITSS.CURRENCY = ''
    CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
    CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
    CUR.DESC=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"���� �������� ������� ������"
    PR.HD :="'L'":SPACE(55):"��":" ":DATF1:" ":"���":" ":DATT1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������� : ":CUST.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":"���� �������� : ":CUST.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������������  : ":CUR.DESC
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
