* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.PRINT.VAUT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CA = 'F.SCB.CHEQ.APPL' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    TOTAL.BOOKS = 0

    COMP = ID.COMPANY
*Line [ 51 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,COM.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COM.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    YTEXT = "Enter Customer No. : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    CUST.ID = COMI

    WS.DESC.1 = '������ ��� �� '
    WS.DESC.2 = '����� ������� �� ��� ���� ������ ��� : ':COM.NAME
    WS.DESC.3 = '�������� ������� : '

    GOSUB PROCESS

    RETURN
*========================================================================
PROCESS:

    T.SEL = "SELECT ":FN.CA:" WITH SEND.TO.CUST.DATE EQ ":TODAY:" AND PRINT.VAUT.DATE EQ '' AND CO.CODE EQ ":COMP:" AND CUST.NO EQ ":CUST.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED GT 0 THEN


        FOR I = 1 TO SELECTED

            IF I = 1 THEN GOSUB PRINT.HEAD
            IF I = 16 THEN
                WS.DESC.1 = ''
                WS.DESC.2 = ''
                WS.DESC.3 = ''
                GOSUB PRINT.HEAD
            END
            CALL F.READ(FN.CA,KEY.LIST<I>,R.CA,F.CA,E1)
            CUS.ID          = R.CA<CHQA.CUST.NO>
            CUST.NAME       = R.CA<CHQA.CUST.NAME>
            WS.BOOKS.NO     = R.CA<CHQA.NO.OF.BOOKS>
            WS.APP.DATE     = R.CA<CHQA.APP.DATE>
            WS.APP.DATE     = FMT(WS.APP.DATE,"####/##/##")
            WS.CHQ.TYPE     = R.CA<CHQA.CHEQ.TYPE>
            TOTAL.BOOKS    += R.CA<CHQA.NO.OF.BOOKS>

            XX  = SPACE(132)
            XX<1,1>[1,15]   = CUS.ID
            XX<1,1>[20,35]  = CUST.NAME
            XX<1,1>[60,10]  = WS.BOOKS.NO
            XX<1,1>[80,10]  = WS.CHQ.TYPE
            XX<1,1>[97,20]  = WS.APP.DATE

            PRINT XX<1,1>

            XX = SPACE(132)
*Line [ 101 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            CHQ.NO.COUNT = DCOUNT(R.CA<CHQA.CHQ.NO.START>,@VM)
            FOR X = 1 TO CHQ.NO.COUNT
                WS.CHQ.NO.START = R.CA<CHQA.CHQ.NO.START><1,X>
                XX<1,1>[119,20] = WS.CHQ.NO.START

                PRINT XX<1,1>
            NEXT X
            PRINT STR('-',130)

            R.CA<CHQA.PRINT.VAUT.DATE> = TODAY
            CALL F.WRITE(FN.CA,KEY.LIST<I>,R.CA)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)

        NEXT I

        XX1  = SPACE(132)
        PRINT STR('=',130)
        XX1<1,1>[20,35]  = '��� ������� �������� : ':TOTAL.BOOKS
        PRINT XX1<1,1>
        PRINT STR('=',130)
        PRINT STR(' ',130)

        XX6 = SPACE(132) ;   XX7  = SPACE(132) ;   XX8 = SPACE(132)
        XX9 = SPACE(132) ;   XX10 = SPACE(132)

        XX6<1,1>[1,110]   = '����� ������� �� ���� �� ���� �� ���� ���� ���� �� ���� �� ��� �������'
        XX7<1,1>[1,110]   = '������� ���� ���� ��� ����� �� ������ ���� ���� ��� ������ ����� ������� ���� ����� �� ����� �������'
        PRINT STR(' ',130)
        XX8<1,1>[55,35]  = '����� �������'

        PRINT XX6<1,1>
        PRINT XX7<1,1>
        PRINT XX8<1,1>
        PRINT XX9<1,1>

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        CALL PRINTER.ON(REPORT.ID,'')

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"����� ������ ���� ����� ":SPACE(1):T.DAY
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":WS.DESC.1:WS.DESC.2
    PR.HD :="'L'":WS.DESC.3
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(10):"��� ������":SPACE(25):"��� �������":SPACE(10):"��� ������":SPACE(10):"����� �����":SPACE(10):"����� �������"
    PR.HD :="'L'":STR('_',130)

    HEADING PR.HD

    RETURN
*==============================================================
END
