* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-137</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.AT.ISO.FMT.BAL.RTN(Y.ACCT.NO,R.ACCT,BALANCE.FORMATTED)
*-----------------------------------------------------------------------------
* Subroutine Type : PROCEDURE
* Attached to  : INTRF.MAPPING
* Attached as  : CALL ROUTINE
* Primary Purpose :
* This Routine formats the Ledger & Avaliable Balance for Financial & Non-Financial Transactions
*-----------------------------------------------------------------------------
* Incoming : Y.ACCT.NO,R.ACCT
*---------------
* Outgoing : BALANCE.FORMATTED
*---------------
* Dependencies :
*---------------
* CALLS  : AT.CALC.AVAIL.BALANCE routine
* CALLED BY :
*-----------------------------------------------------------------------------
* Developed for ATM Framework ISO 8583-87/93 message standards
* Developer: Gpack ATM
*-----------------------------------------------------------------------------
* Modification History :
*
* 20/12/2013 - Ref 866817 / Task 866826
*     ATM Pack integration
*     Call Account service api for balance check
*     Consider the format type and length mentioned in
*     parameter table while formatting balances
*     Remove hard coding of message type to identify
*     Phoneix process
* 09/06/2016 - Task 1755374
*     modified routine to display account balance details properly for balance enquiry.
* 16/11/2016 - Task 1926383
*     Modified routine to display the correct formatting in the 54th bit of the message.
* 06/02/2017 - Task 1993486
*     Balance After txn showing wrong format for the Phoenix Messages
* 14/02/2017 - Task 1979734
*     Modified the routine to display the account details properly for balance enquiry.
*-----------------------------------------------------------------------------
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLASS
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 67 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SPF
*Line [ 69 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AT.ISO.COMMON
*Line [ 71 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.CONTRACT.BALANCES
*Line [ 73 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ATM.PARAMETER
*Line [ 75 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INTRF.MESSAGE
*-----------------------------------------------------------------------------

    GOSUB INITIALISE
    GOSUB PROCESS
    RETURN

*-----------------------------------------------------------------------------
INITIALISE:
*-----------------------------------------------------------------------------

    FN.CURRENCY = 'F.CURRENCY'
    FV.CURRENCY = ''
    CALL OPF(FN.CURRENCY,FV.CURRENCY)

    INTRF.MSG.ID = AT$INTRF.MSG.ID
    R.ATM.PARAMETER=AT$ATM.PARAMETER.REC

    PHX.PROCESS = '0'
    LED.BAL = ''
    AVAIL.BAL = ''
    BAL.TYPE = ''

    R.INTRF.MESSAGE = ''
    ER.IMSG = ''
    CALL CACHE.READ("F.INTRF.MESSAGE", INTRF.MSG.ID, R.INTRF.MESSAGE, ER.IMSG)
    IF R.INTRF.MESSAGE<INTRF.MSG.MSG.FORMAT> EQ 'PHOENIX' THEN
        PHX.PROCESS = '1'
    END
    AMT.FMT.TYPE = ''
    AMT.FMT.LENGTH = ''
    AMT.FMT.TYPE = R.ATM.PARAMETER<ATM.PARA.AMT.FMT>
    AMT.FMT.LENGTH = R.ATM.PARAMETER<ATM.PARA.AMT.LENGTH>

    RETURN          ;*From Initialise

*-----------------------------------------------------------------------------
PROCESS:
*-----------------------------------------------------------------------------

    CCY.CODE = R.ACCT<AC.CURRENCY>
    CALL CACHE.READ(FN.CURRENCY,CCY.CODE,R.CCY,ER.CCY)
    NUM.CCY = R.CCY<EB.CUR.NUMERIC.CCY.CODE>
    NUM.CCY = FMT(NUM.CCY,'R%3')
    NO.DEC = R.CCY<EB.CUR.NO.OF.DECIMALS>
    PROC.CODE=AT$PROC
    ACC.TYPE=PROC.CODE[3,2]
    WRK.AMT.TYPE='02'
    LED.AMT.TYPE='01'

    GOSUB GET.WORKING.BALANCE
*LEGER BALANCE IS TAKEN AS ONLINE AVAILABLE BALANCE

    CALL AT.CALC.AVAIL.BALANCE(R.ACCT,WRK.BAL,AVAIL.BAL)

    IF PHX.PROCESS EQ '1' THEN
        GOSUB GET.PHX.BAL
    END ELSE
        GOSUB GET.ATM.BAL
    END

    RETURN
*-----------------------------------------------------------------------------
GET.PHX.BAL:
*-----------------------------------------------------------------------------

    GOSUB CHECK.AMT.SIGN
    LED.BAL = ABS(LED.BAL)
    AVAIL.BAL = ABS(AVAIL.BAL)


    BEGIN CASE

    CASE R.ATM.PARAMETER<ATM.PARA.PHX.BAL.FMT.TYPE> EQ 'ACTUAL'
        BAL.TYPE = '1'
        GOSUB FORMAT.BALANCES
        AVAIL.BAL = ''
        AVAIL.BAL = FMT(AVAIL.BAL,'R%':AMT.FMT.LENGTH)

        IF LED.BAL.SIGN EQ '' THEN
            LED.BAL = FMT(LED.BAL,'R%':AMT.FMT.LENGTH)
        END ELSE
            LED.BAL = LED.BAL.SIGN:FMT(LED.BAL,'R%':AMT.FMT.LENGTH-1)
        END


    CASE R.ATM.PARAMETER<ATM.PARA.PHX.BAL.FMT.TYPE> EQ 'AVAILABLE'
        BAL.TYPE = '2'
        GOSUB FORMAT.BALANCES
        LED.BAL = ''
        LED.BAL = FMT(LED.BAL,'R%':AMT.FMT.LENGTH)

        IF AVAIL.BAL.SIGN EQ '' THEN
*coad modified to display bal after txn with correct format fr PHX
            AVAIL.BAL = FMT(AVAIL.BAL,'R%':AMT.FMT.LENGTH)
        END ELSE
            AVAIL.BAL = AVAIL.BAL.SIGN:FMT(AVAIL.BAL,'R%':AMT.FMT.LENGTH-1)
        END


    CASE R.ATM.PARAMETER<ATM.PARA.PHX.BAL.FMT.TYPE> EQ 'BOTH'
        BAL.TYPE = '3'
        GOSUB FORMAT.BALANCES
        IF LED.BAL.SIGN EQ '' THEN
            LED.BAL = FMT(LED.BAL,'R%':AMT.FMT.LENGTH)
        END ELSE
            LED.BAL = LED.BAL.SIGN:FMT(LED.BAL,'R%':AMT.FMT.LENGTH-1)
        END

        IF AVAIL.BAL.SIGN EQ '' THEN
* coad modified to display bal after txn with correct format fr PHX
            AVAIL.BAL = FMT(AVAIL.BAL,'R%':AMT.FMT.LENGTH)

        END ELSE
            AVAIL.BAL = AVAIL.BAL.SIGN:FMT(AVAIL.BAL,'R%':AMT.FMT.LENGTH-1)
        END

    END CASE

    BALANCE.FORMATTED = BAL.TYPE:LED.BAL:AVAIL.BAL

    RETURN

*-----------------------------------------------------------------------------
GET.ATM.BAL:
*-----------------------------------------------------------------------------


    GOSUB CHECK.AMT.SIGN

    AVAIL.BAL = ABS(AVAIL.BAL)
    LED.BAL = ABS(LED.BAL)

    GOSUB FORMAT.BALANCES

    AVAIL.BAL = FMT(AVAIL.BAL,'R%':AMT.FMT.LENGTH)

    LED.BAL = FMT(LED.BAL,'R%':AMT.FMT.LENGTH)

    BALANCE.FORMATTED ='4':LED.BAL:AVAIL.BAL

    RETURN          ;*From process
*-----------------------------------------------------------------------------
CHECK.AMT.SIGN:
*-----------------------------------------------------------------------------
    DR.SIGN = ''
    CR.SIGN = ''

    BEGIN CASE

*Line [ 226 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CASE AMT.FMT.TYPE MATCHES '+-N':@VM:'N+-'
        CR.SIGN = '+'
        DR.SIGN = '-'
*Line [ 230 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CASE AMT.FMT.TYPE MATCHES '+N':@VM:'N+'
        CR.SIGN = '+'
*Line [ 233 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CASE AMT.FMT.TYPE MATCHES '-N':@VM:'N-'
        DR.SIGN = '-'
*Line [ 236 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CASE AMT.FMT.TYPE MATCHES 'CDN':@VM:'NCD'
        CR.SIGN = 'C'
        DR.SIGN = 'D'
*Line [ 240 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CASE AMT.FMT.TYPE MATCHES 'CN':@VM:'NC'
        CR.SIGN = 'C'
*Line [ 243 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CASE AMT.FMT.TYPE MATCHES 'DN':@VM:'ND'
        DR.SIGN = 'D'
    CASE OTHERWISE
*Line [ 247 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        NULL
    END CASE
* Ledger Balance sign
    IF LED.BAL LE '0' THEN
        IF DR.SIGN NE '' THEN
            LED.BAL.SIGN = DR.SIGN
        END
    END
* Modified as assigning signs based on the balance amount
* Modified code to assign sign properly to the balance amount.
    IF LED.BAL GT 0 THEN
        IF CR.SIGN NE '' THEN
            LED.BAL.SIGN = CR.SIGN
        END
    END

* Available Balance sign
    IF AVAIL.BAL LE '0' THEN
        IF DR.SIGN NE '' THEN
            AVAIL.BAL.SIGN = DR.SIGN
        END
    END
* Modified as assigning signs based on the balance amount
    IF AVAIL.BAL GT '0' THEN
        IF CR.SIGN NE '' THEN
            AVAIL.BAL.SIGN = CR.SIGN
        END
    END

    RETURN
*-----------------------------------------------------------------------------
GET.WORKING.BALANCE:
*-----------------------------------------------------------------------------

    LED.BAL = ''
    WRK.BAL = ''
    RES.LED.BAL = ''
    RES.WRK.BAL = ''

    CALL AccountService.getOnlineActualBalance(Y.ACCT.NO, LED.BAL, RES.LED.BAL)
    CALL AccountService.getWorkingBalance(Y.ACCT.NO, WRK.BAL, RES.WRK.BAL)

    RETURN
*-----------------------------------------------------------------------------
FORMAT.BALANCES:
*-----------------------------------------------------------------------------

    CALL SC.FORMAT.CCY.AMT(CCY.CODE, AVAIL.BAL)
    CALL SC.FORMAT.CCY.AMT(CCY.CODE, LED.BAL)

    AVAIL.BAL = FIELD(AVAIL.BAL,'.',1):FIELD(AVAIL.BAL,'.',2)
    LED.BAL = FIELD(LED.BAL,'.',1):FIELD(LED.BAL,'.',2)

    RETURN
*-----------------------------------------------------------------------------
END
