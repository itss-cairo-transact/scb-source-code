* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
************************CREATED BY RIHAM YOUSSEF 20180111***********
*-----------------------------------------------------------------------------
*    PROGRAM SCB.CBE.5002.54
    SUBROUTINE SCB.CBE.5002.54

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCR.ACCT.CR
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.REPORT
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    GOSUB PRINT.DET
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SCB.CBE.5002.54.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.CBE.5002.54.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.CBE.5002.54.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.CBE.5002.54.TXT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.CBE.5002.54.TXT File IN &SAVEDLISTS&'
        END
    END
******************************************************************
    AMT.CBE.4.TOTAL        = 0
    AMT.CBE.5.TOTAL        = 0
    AMT.CBE.4              = 0
    AMT.CBE.5              = 0
    AMT.CBE.6              = 0

*    DAT.1 = TODAY
*    CALL CDT("",DAT.1,'-1W')
    YTEXT = "Enter report date"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    DAT.1 = COMI
    TEXT =  'DATE : ':DAT.1 ; CALL REM


    HEAD.DESC = "������ ������ ������ �����":"|"
    HEAD.DESC := "������ ���� ������ �����":"|"
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.SCB.CBE.REPORT' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)


    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)


    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.LC.HIS = 'FBNK.LETTER.OF.CREDIT$HIS' ; F.LC.HIS = ''
    CALL OPF(FN.LC.HIS,F.LC.HIS)

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)

    FN.DR.HIS = 'FBNK.DRAWINGS$HIS' ; F.DR.HIS = ''
    CALL OPF(FN.DR.HIS,F.DR.HIS)



    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.SEC.N = 'F.SCB.NEW.SECTOR' ; F.SEC.N = ''
    CALL OPF(FN.SEC.N,F.SEC.N)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)

    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* CURRENT ACCOUNTS SELECTION *************
    T.SEL  = "SELECT ":FN.CBE:" WITH (AMT.4 NE '' OR AMT.5 NE '' OR AMT.6 NE '') AND CURR UNLIKE OT... AND SYS.DATE EQ ":DAT.1:" BY @ID"
*DEBUG

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
*DEBUG
            TRANS.ID = KEY.LIST<I>
            TRANS.ID.2 =TRANS.ID[1,2]
*   IF TRANS.ID.2 NE 'FT' THEN
            CUR.CBE      =  R.CBE<CBE.R.CURR>
            AMT.CBE      = R.CBE<CBE.R.AMT.4>
            IF AMT.CBE EQ '' THEN
                AMT.CBE  = R.CBE<CBE.R.AMT.5>
            END
            IF AMT.CBE EQ '' THEN
                AMT.CBE  = R.CBE<CBE.R.AMT.6>
            END
            CUR = 'USD'
            CALL F.READ(FN.CCY,'USD',R.CCY,F.CCY,E1)
            AMT.RATE     = R.CCY<SBD.CURR.MID.RATE>
            AMT.USD  = AMT.CBE / AMT.RATE
            IF AMT.USD LT '100000' THEN
                GOSUB GET.DETAIL
*   GOSUB PRINT.DET
            END
*  END
        NEXT I
    END
    RETURN
*==============================================================
PRINT.DET:
    AMT.CBE.44.TOTAL = AMT.CBE.4.TOTAL/1000000
    AMT.CBE.444.TOTAL = FMT(AMT.CBE.44.TOTAL,"L0,")
    BB.DATA.1 = AMT.CBE.444.TOTAL
    WRITESEQ BB.DATA.1 TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END




    AMT.CBE.55.TOTAL = AMT.CBE.5.TOTAL/1000000
    AMT.CBE.555.TOTAL = FMT(AMT.CBE.55.TOTAL,"L0,")
    BB.DATA.2 = AMT.CBE.555.TOTAL

    WRITESEQ BB.DATA.2 TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*===============================================================
GET.DETAIL:
*DEBUG
    AMT.4 = R.CBE<CBE.R.AMT.4>
    IF AMT.4 NE '' THEN
        AMT.CBE.4      += AMT.4
    END
    AMT.CBE.4.TOTAL = AMT.CBE.4

    AMT.5 = R.CBE<CBE.R.AMT.5>
    IF AMT.5 NE '' THEN
        AMT.CBE.5      += AMT.5
    END

    AMT.6 = R.CBE<CBE.R.AMT.6>
    IF AMT.6 NE '' THEN
        AMT.CBE.6  += AMT.6
    END


    AMT.CBE.5.TOTAL   = AMT.CBE.5 + AMT.CBE.6

    RETURN

*===============================================================
