* @ValidationCode : Mjo3NTkxNDcyNTU6Q3AxMjUyOjE2NDQ5MzI1MDEzMTM6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:41:41
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*------------------------------ GET LOANS SCHEDUALES AND ACCOUNS DETAILS -----------------------
*------------------------------       CREATED BY BAKRY 2020/02/10        -----------------------
*    SUBROUTINE SBR.LOANS.SCHED
PROGRAM SBR.LOANS.SCHED
    $INSERT I_COMMON
    $INSERT I_EQUATE
    $INSERT I_F.CUSTOMER
    $INSERT I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
    $INSERT I_F.CUSTOMER.ACCOUNT
    $INSERT I_F.LMM.ACCOUNT.BALANCES
    $INSERT I_F.LMM.SCHEDULES.PAST
    $INSERT I_ENQUIRY.COMMON
    $INSERT I_LD.ENQ.COMMON
    $INSERT I_LD.SCH.LIST
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ACCT.DR

    GOSUB INIT
    GOSUB OPENFILES
    GOSUB PROCESS1
    GOSUB PROCESS2
RETURN

*-----------------------------------------------------------------------------------------------
INIT:
*----
    FN.LM = 'F.LMM.ACCOUNT.BALANCES'
    F.LM = ''
    Y.ID=''
    R.LM=''
    LM.ERR1=''
    Y.TOTAL=0
    SEL.CMD=''
    SEL.LIST=''
    RET.CODE=''
    NO.OF.REC = 0
    SEP=","
    FN.LP='F.LMM.SCHEDULES.PAST'
    Y.CUSTOMER=''
    TEMP=0
    YF.ENQ = "F.ENQUIRY"
    F.ENQ = ""
    FN.CUSTOMER.ACCOUNT = 'F.CUSTOMER.ACCOUNT'
    FV.CUSTOMER.ACCOUNT = ''
    FN.CUS = 'FBNK.CUSTOMER'
    FV.CUS = '' ; R.CUS = '' ; ER.CUS = ''

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = '' ; R.PD = ''
    TOTAL.PD= ''  ; T.SEL.PD = ""

    FN.AC = 'FBNK.ACCOUNT'
    F.AC = ''
    R.AC = ''

    FN.SDR = 'FBNK.STMT.ACCT.DR'
    F.SDR = ''
    R.SDR = ''
    ER.SDR = ''
*----------------------------------- START OPEN FILE ----------------------------
    OPENSEQ "&SAVEDLISTS&" , "GET.LD.csv" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"GET.LD.csv"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "GET.LD.csv" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE GET.LD.csv CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create GET.LD.csv File IN &SAVEDLISTS&'
        END
    END
*----------------------------------- END OPEN FILE -----------------------------
*----------------------------------- CREATE HEADER -----------------------------
    HEAD.DAT = TODAY
    CALL CDT("",HEAD.DAT,'-1W')
    HEAD.DESC = "Close Off ":HEAD.DAT[1,4]:"/":HEAD.DAT[5,2]:"/":HEAD.DAT[7,2]
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    HEAD.DESC = ""
    HEAD.DESC = "Contract NO.,"
    HEAD.DESC := "Customer,"
    HEAD.DESC := "Customer Name,"
    HEAD.DESC := "Currency,"
    HEAD.DESC := "Category,"
    HEAD.DESC := "Int. Rate,"
    HEAD.DESC := "Date,"
    HEAD.DESC := "Total Amount,"
    HEAD.DESC := "Principal,"
    HEAD.DESC := "Interest,"
    HEAD.DESC := "Comm.,"
    HEAD.DESC := "Fee.,"
    HEAD.DESC := "Chrg.,"
    HEAD.DESC := "Outstanding,"
    HEAD.DESC := "Total PD.,"
    HEAD.DESC := "Loans Types"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*------------------------------ END CREATE HEADER -------------------------------
RETURN
*---------------------------------------------------------------------------------
OPENFILES:
*---------
    CALL OPF(FN.LM,F.LM)
    CALL OPF(YF.ENQ,F.ENQ)
    CALL OPF(FN.CUSTOMER.ACCOUNT,FV.CUSTOMER.ACCOUNT)
    CALL OPF(FN.CUS,FV.CUS)
    CALL OPF(FN.PD,F.PD)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.SDR,F.SDR)
RETURN

*---------------------------------------------------------------------------------
PROCESS1:
*-------

    READ R.ENQ FROM F.ENQ,"LD.BALANCES.FULL" ELSE R.ENQ = ""
    SEL.CMD="SELECT ":FN.LM:" WITH LD.CATEG IN (21050 21051 21052 21053 21055 21060 21061 21062 21063 21064 21065 21066 21067 21068 21069 21070 21071 21073 21074 21058 21059 ) BY LD.CATEG"

    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NO.OF.REC,RET.CODE)
    LOOP
        REMOVE Y.ID FROM SEL.LIST SETTING POS
    WHILE Y.ID:POS
        LMM.ID = Y.ID
        READ R.RECORD FROM F.LM,LMM.ID ELSE R.RECORD = ""
        CALL LD.ENQ.INT.I     ;* Open files and store in common

        ID = LMM.ID
        CALL E.LD.SCHED.LIST
        TOTAL.INT = 0


*Line [ 158 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
        FOR VM1 = 1 TO DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM)
            CATEG       = R.RECORD<LD.SL.CATEGORY>
            INT.RATE    = R.RECORD<LD.SL.CURRENT.RATE>
            CURR        = R.RECORD<LD.SL.CURRENCY>
            CUS.ID      = R.RECORD<LD.SL.CUSTOMER.NO>
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,FV.CUS,ER.CUS)
*Line [ 166 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            EVENT.DATE  = R.RECORD<LD.SL.EVENT.DATE,VM1>
            CUS.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
*Line [ 170 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            AMT.DUE     = R.RECORD<LD.SL.TOTAL.PAYMENT,VM1>
*Line [ 173 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            PRINC.AMT   = R.RECORD<LD.SL.PRIN.AMOUNT,VM1>
*Line [ 176 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            INT.AMOUNT  = R.RECORD<LD.SL.INT.AMOUNT,VM1>
*Line [ 179 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            CHRG.AMOUNT = R.RECORD<LD.SL.CHG.AMOUNT,VM1>
*Line [ 182 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            FEE.AMOUNT  = R.RECORD<LD.SL.FEE.AMOUNT,VM1>
*Line [ 185 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            COMM.AMOUNT = R.RECORD<LD.SL.COMM.AMOUNT,VM1>
*Line [ 188 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            OUTS.AMOUNT = R.RECORD<LD.SL.RUNNING.BAL,VM1>

*-------------------------------------------------------------------------------------------------------------------------------
            BB.DATA  = ID:","
            BB.DATA := CUS.ID:","
            BB.DATA := CUS.NAME:","
            BB.DATA := CURR:","
            BB.DATA := CATEG:","
            BB.DATA := INT.RATE:"%,"
            BB.DATA := EVENT.DATE:","
            BB.DATA := AMT.DUE:","
            BB.DATA := PRINC.AMT:","
            BB.DATA := INT.AMOUNT:","
            BB.DATA := COMM.AMOUNT:","
            BB.DATA := FEE.AMOUNT:","
            BB.DATA := CHRG.AMOUNT:","
            BB.DATA := OUTS.AMOUNT:","
*Line [ 207 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            IF VM1 = DCOUNT(R.RECORD<LD.SL.EVENT.DATE>,@VM) THEN GOSUB GETPD
            BB.DATA := TOTAL.PD:","
            GOSUB GETTYP
            BB.DATA := LON.TYP
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            CATEG       = ""
            INT.RATE    = ""
            CURR        = ""
            CUS.ID      = ""
            EVENT.DATE  = ""
            CUS.NAME    = ""
            AMT.DUE     = ""
            PRINC.AMT   = ""
            INT.AMOUNT  = ""
            CHRG.AMOUNT = ""
            FEE.AMOUNT  = ""
            COMM.AMOUNT = ""
            OUTS.AMOUNT = ""
            TOTAL.PD    = ""
            LON.TYP     = ""
*----------------------------------------------------------------------------

*Line [ 222 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
*Line [ 233 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
        NEXT VM1

    REPEAT

RETURN
*----------------------------------------------------------------------------
PROCESS2:
*-------
    T.SEL = ""
    T.SEL = "SELECT ":FN.AC
    T.SEL := " WITH CATEGORY IN ('1231' 1487 1488 1215 1216 1218 1230 1236 1595 1596 1598)"
    T.SEL := " BY CUSTOMER BY CATEGORY"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED  THEN

        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
*********************************
            TOT.INT = ""
            TOT.COM = ""
            GRN.TOT = ""
            ACCT.ID = KEY.LIST<I>
            GOSUB GET.DR.INT
            CATEG       = R.AC<AC.CATEGORY>
            CURR        = R.AC<AC.CURRENCY>
            CUS.ID      = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,FV.CUS,ER.CUS)
            EVENT.DATE  = BNK.DATE1
            CUS.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            AMT.DUE     = GRN.TOT
*Line [ 266 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2022-02-09
            PRINC.AMT   = ""  ;*R.RECORD<LD.SL.PRIN.AMOUNT,VM1>
            INT.AMOUNT  = TOT.INT
            CHRG.AMOUNT = TOT.COM
            FEE.AMOUNT  = ''
            COMM.AMOUNT = ''
            OUTS.AMOUNT = R.AC<AC.OPEN.ACTUAL.BAL>

            BB.DATA  = ACCT.ID:","
            BB.DATA := CUS.ID:","
            BB.DATA := CUS.NAME:","
            BB.DATA := CURR:","
            BB.DATA := CATEG:","
            BB.DATA := INT.RATE:"%,"
            BB.DATA := EVENT.DATE:","
            BB.DATA := AMT.DUE:","
            BB.DATA := PRINC.AMT:","
            BB.DATA := INT.AMOUNT:","
            BB.DATA := CHRG.AMOUNT:","
            BB.DATA := FEE.AMOUNT:","
            BB.DATA := COMM.AMOUNT:","
            BB.DATA := OUTS.AMOUNT:","
            BB.DATA := TOTAL.PD:","
            GOSUB GETTYP
            BB.DATA := LON.TYP
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            CATEG       = ""
            INT.RATE    = ""
            CURR        = ""
            CUS.ID      = ""
            EVENT.DATE  = ""
            CUS.NAME    = ""
            AMT.DUE     = ""
            PRINC.AMT   = ""
            INT.AMOUNT  = ""
            CHRG.AMOUNT = ""
            FEE.AMOUNT  = ""
            COMM.AMOUNT = ""
            OUTS.AMOUNT = ""
            TOTAL.PD    = ""
            LON.TYP     = ""
*----------------------------------------------------------------------------

        NEXT I


        RETURN
*----------------------------------------------------------------------------
************************************************************************
GET.DR.INT:
*----------
        BNK.DATE1 = TODAY[1,6]:"01"
        CALL CDT("",BNK.DATE1,'-1C')
        SDR.ID = ACCT.ID:"-":BNK.DATE1
        CALL F.READ(FN.SDR,SDR.ID,R.SDR,F.SDR,Y.SDR.ERR)
        SDR.DATE = FIELD(SDR.ID,'-',2)
        TOT.INT = R.SDR<IC.STMDR.TOTAL.INTEREST>
        TOT.COM = R.SDR<IC.STMDR.TOTAL.CHARGE>
        GRN.TOT = R.SDR<IC.STMDR.GRAND.TOTAL>
        INT.DT  = R.SDR<IC.STMDR.DR.INT.DATE>
        LIQ.CCY = R.SDR<IC.STMDR.LIQUIDITY.CCY>
        M.RATE  = R.SDR<IC.STMDR.USED.MIDDLE.RATE>
*Line [ 318 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        SS = DCOUNT(R.SDR<IC.STMDR.DR.INT.RATE>,@VM)
        INT.RATE = R.SDR<IC.STMDR.DR.INT.RATE><1,SS>
        RETURN
************************************************************************

*----------------------------------------------------------------------------
GETPD:
*=====
        PD.ID = "PD":ID[1,12]
        CALL F.READ(FN.PD,PD.ID,R.PD,F.PD, ETEXT)
        IF NOT(ETEXT) THEN
            IF R.PD<PD.TOTAL.OVERDUE.AMT> NE '' OR R.PD<PD.TOTAL.OVERDUE.AMT> NE 0 THEN
                TOTAL.PD = R.PD<PD.TOTAL.OVERDUE.AMT>
            END
        END
        RETURN
*----------------------------------------------------------------------------
GETTYP:
*=====
        LON.TYP = ''
        IF CATEG = 1487 OR CATEG = 1488 OR CATEG = 1595 OR CATEG = 1596 OR CATEG = 1598 OR CATEG = 21051 OR CATEG = 21052 OR CATEG = 21053 OR CATEG = 21061 THEN
            LON.TYP = "Syndicated Loans"
        END

        IF CATEG = 1215 OR CATEG = 1216 OR CATEG = 1236 OR CATEG = 21060 OR CATEG = 21062 OR CATEG = 21067 OR CATEG = 21069 OR CATEG = 21071 OR CATEG = 21073 OR CATEG = 21074 OR CATEG = 21058 OR CATEG = 21059 THEN
            LON.TYP = "Medium"
        END

        IF CATEG = 21055 OR CATEG = 21063 OR CATEG = 21065 THEN
            LON.TYP = "Retail / Staff"
        END

        IF CATEG = 1218 OR CATEG = 1230 OR CATEG = 21070 OR CATEG = 21064 THEN
            LON.TYP = "Long term"
        END

        RETURN
    END
