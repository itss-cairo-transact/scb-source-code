* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
    SUBROUTINE SBR.LD.CHANGE.RATE.3

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ��������  '

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        TEXT = "�� �������� �� ��������" ; CALL REM

    END ELSE
        TEXT = '�� ������ �� �������� ' ; CALL REM
    END

    RETURN
*---------------------------------------
INITIALISE:

    YTEXT = "����� ����� ��� ������� ������� ������ �� 500000 : "
    CALL TXTINP(YTEXT, 8, 22, "6", "A")
    WS.RATE.GT.500 = COMI

    YTEXT = "����� ����� ��� ������� ������� ����� �� 500000 : "
    CALL TXTINP(YTEXT, 8, 22, "6", "A")
    WS.RATE.LT.500 = COMI

    YTEXT = "����� ����� ����� ������� : "
    CALL TXTINP(YTEXT, 8, 22, "8", "A")
    WS.V.DATE = COMI

    OPENSEQ "MECH" , "LD.21055.RATE.CHANGE" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LD.21055.RATE.CHANGE"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LD.21055.RATE.CHANGE" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LD.21055.RATE.CHANGE CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LD.21055.RATE.CHANGE File IN MECH'
        END
    END

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    TOTAL.AMOUNT = 0 ; K = 0 ; X = 1

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21055 AND INT.RATE.TYPE EQ 1 AND STATUS NE LIQ BY CUSTOMER.ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            CALL F.READ(FN.LD,KEY.LIST<I+1>,R.LD1,F.LD,E2)

            WS.CUST.ID  = R.LD<LD.CUSTOMER.ID>
            WS.CUST.ID1 = R.LD1<LD.CUSTOMER.ID>

            TOTAL.AMOUNT += R.LD<LD.DRAWDOWN.ISSUE.PRC>

            K++

            IF WS.CUST.ID NE WS.CUST.ID1 THEN

                IF TOTAL.AMOUNT GT 500000 THEN
                    WS.NEW.RATE = WS.RATE.GT.500
                END ELSE
                    WS.NEW.RATE = WS.RATE.LT.500
                END

                TOTAL.AMOUNT = 0

                FOR J = 1 TO K
                    WS.COMP  = R.LD<LD.CO.CODE>
                    IDD = 'LD.LOANS.AND.DEPOSITS,RATE,AUTO.LD//':WS.COMP:',':KEY.LIST<X>

                    OFS.MESSAGE.DATA  =  "NEW.INT.RATE=":WS.NEW.RATE:","
                    OFS.MESSAGE.DATA :=  "INT.RATE.V.DATE=":WS.V.DATE:","

                    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

                    WRITESEQ MSG.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                    X++
                NEXT J
                K = 0
                X = I+1
            END
        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LD.21055.RATE.CHANGE'
    EXECUTE 'DELETE ':"MECH":' ':"LD.21055.RATE.CHANGE"

    RETURN
END
