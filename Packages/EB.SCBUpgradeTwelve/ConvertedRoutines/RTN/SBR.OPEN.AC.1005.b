* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-37</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY MOHAMED SABRY 2014/08/22 ****
*********************************************

    SUBROUTINE  SBR.OPEN.AC.1005

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.VERSION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF


**************************************************************

    GOSUB INITIALISE
    GOSUB ENTR.CU.ID


    RETURN
**************************************************************
INITIALISE:
*----------
    FLG.1005 = ''

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "ACCOUNT"
    SCB.VERSION = "OFS.MNGR"
    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = '' ; ER.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = '' ; ER.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.IND.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.IND.AC = '' ; R.IND.AC = '' ; ER.IND.AC = ''
    CALL OPF(FN.IND.AC,F.IND.AC)


    WS.FLD.NO = 0


    RETURN
**************************************************************
ENTR.CU.ID:
    YTEXT = "Enter Customer ID :.... OR E to exit "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")

    WS.CU.ID = COMI

    IF WS.CU.ID EQ 'E' THEN
        RETURN
    END

    CALL F.READ(FN.CU,WS.CU.ID,R.CU,F.CU,ER.CU)
    WS.POST.REST    = R.CU<EB.CUS.POSTING.RESTRICT>
    WS.NEW.SECTOR   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

    IF (ER.CU) THEN
        TEXT = "���� ������ �� ��� ������" ; CALL REM
        GOTO ENTR.CU.ID
    END ELSE
        IF WS.POST.REST GT 90 THEN
            TEXT = "��� ������ ���� ���� ������ �� �����"; CALL REM
            RETURN
        END
        GOSUB CHK.1005
    END

    WS.USR.LOCAL = R.USER<EB.USE.LOCAL.REF>
    WS.SCB.DEPT  = WS.USR.LOCAL<1,USER.SCB.DEPT.CODE>

    IF WS.SCB.DEPT = "5501" THEN
        TEXT = "���� ������� ��� ����� ����� ���� ��� (Y/N)" ; CALL OVE
        IF TEXT = 'Y' THEN
            GOSUB OPEN.ATM
        END ELSE
            RETURN
        END
    END
    RETURN
**************************************************************
CHK.1005:

    CALL F.READ(FN.IND.AC,WS.CU.ID,R.IND.AC,F.IND.AC,ER.IND.AC)
    LOOP
        REMOVE AC.ID FROM R.IND.AC SETTING POS.AC
    WHILE AC.ID:POS.AC
        CALL F.READ(FN.AC,AC.ID,R.AC,F.AC,ER.AC)
        WS.GL.ID = R.AC<AC.CATEGORY>
        WS.CY.ID = R.AC<AC.CURRENCY>
        IF WS.GL.ID = 1005 THEN
            FLG.1005 = 'Y'
            TEXT = "���� ����� ��� 1005 ���� ������ " ; CALL REM
            RETURN
        END
    REPEAT

    IF FLG.1005 EQ '' THEN
        TEXT = "1005 ���� ������� ��� ��� ���� (Y/N)" ; CALL OVE
        IF TEXT = 'Y' THEN
            GOSUB OPEN.AC.1005
        END ELSE
            RETURN
        END
    END
    RETURN

**************************************************************
OPEN.AC.1005:

    WS.CUAC.ID = WS.CU.ID + 100000000
    WS.CUAC.ID = WS.CUAC.ID[8]

    WS.AC.ID   = WS.CUAC.ID:"10100501"

*    OFS.MESSAGE.DATA  :=  ",STATUS:":WS.FLD.NO:":1=":'CLOSE'

    OFS.MESSAGE.DATA  :=  ",CUSTOMER:1:1=":WS.CU.ID
    OFS.MESSAGE.DATA  :=  ",CATEGORY:1:1=":"1005"
*    OFS.MESSAGE.DATA  :=  ",ACCOUNT.TITLE.1:1:1=":R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
    OFS.MESSAGE.DATA  :=  ",ARABIC.TITLE:1:1=":R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>

    IF OFS.MESSAGE.DATA NE '' THEN

        GOSUB PRINT.DET

        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":ID.COMPANY[2]:"//":ID.COMPANY:"," : WS.AC.ID: OFS.MESSAGE.DATA

        BB.IN.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.IN.DATA TO BB.IN ELSE
        END

*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
*   TEXT = SCB.OFS.MESSAGE ; CALL REM
        BB.OUT.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
        END

        WS.FLD.NO = 0
    END
    OFS.MESSAGE.DATA = ''

    RETURN
**************************************************************
PRINT.DET:
    WS.TIME           = TIME()
    WS.TIME           = OCONV(WS.TIME, "MTS")

    WS.FILE.NAME = WS.AC.ID:'*':TODAY:'*':WS.TIME

    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':WS.FILE.NAME
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.IN'
        END
    END

    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':WS.FILE.NAME
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.OUT'
        END
    END
    RETURN
**************************************************************
OPEN.ATM:
*    WS.AC.ID = "9920005910100501"

    IF WS.AC.ID EQ '' THEN
        WS.CUAC.ID = WS.CU.ID + 100000000
        WS.CUAC.ID = WS.CUAC.ID[8]
        WS.AC.ID   = WS.CUAC.ID:"10100501"
    END


    IF WS.NEW.SECTOR NE '4650' THEN
        TEXT = "��� �� ���� ���� ����� ���" ; CALL REM
        RETURN
    END

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL       = "SCB.ATM.APP"
    SCB.VERSION    = "SCB.ATM.OFS"
    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    FN.ATM = 'F.SCB.ATM.APP' ; F.ATM = '' ; R.ATM = '' ; ER.ATM = ''
    CALL OPF(FN.ATM,F.ATM)

    CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,ER.AC)
    IF ER.AC THEN
        TEXT = " �� ���� ����� ��� 1005 ���� ������ " ; CALL REM
        RETURN
    END
    IF NOT(ER.AC) THEN
        GOSUB OPEN.ATM.APP
    END
    RETURN
**************************************************************
OPEN.ATM.APP:


    WS.ATM.APP.ID = WS.CU.ID:'.1'

    CALL F.READ(FN.ATM,WS.ATM.APP.ID,R.ATM,F.ATM,ER.ATM)

    IF NOT(ER.ATM) THEN
        TEXT = " ��� ������ �� ��� ����� ���� ��� ������� �����" ; CALL REM
        RETURN
    END

    OFS.MESSAGE.DATA  :=  ",CUSTOMER:1:1=":WS.CU.ID
    OFS.MESSAGE.DATA  :=  ",CARD.TYPE:1:1=":"801"
    OFS.MESSAGE.DATA  :=  ",ACC.N1:1:1=":WS.AC.ID
    OFS.MESSAGE.DATA  :=  ",FAST.ACC:1:1=":WS.AC.ID
    OFS.MESSAGE.DATA  :=  ",ATM.APP.DATE:1:1=":TODAY

    IF OFS.MESSAGE.DATA NE '' THEN

        GOSUB PRINT.DET.ATM

        SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":ID.COMPANY[2]:"//":ID.COMPANY:"," : WS.ATM.APP.ID: OFS.MESSAGE.DATA

        BB.IN.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.IN.DATA TO BB.IN ELSE
        END

*        CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
        CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
*   TEXT = SCB.OFS.MESSAGE ; CALL REM
        BB.OUT.DATA  = SCB.OFS.MESSAGE
        WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
        END

        WS.FLD.NO = 0
    END
    OFS.MESSAGE.DATA = ''
    RETURN
**************************************************************
PRINT.DET.ATM:
    WS.TIME           = TIME()
    WS.TIME           = OCONV(WS.TIME, "MTS")

* WS.FILE.NAME = 'ATM*':WS.AC.ID:'*':TODAY:'*':WS.TIME

    WS.FILE.NAME = 'ATM*': WS.ATM.APP.ID :'*':TODAY:'*':WS.TIME

    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':WS.FILE.NAME
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.IN" , WS.FILE.NAME TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.IN'
        END
    END

    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':WS.FILE.NAME
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , WS.FILE.NAME TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create ":WS.FILE.NAME:" File IN OFS.MNGR.OUT'
        END
    END
    RETURN
**************************************************************
