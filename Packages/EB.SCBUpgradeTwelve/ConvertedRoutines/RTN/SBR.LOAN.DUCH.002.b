* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-424</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBR.LOAN.DUCH.002

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DEBIT.INT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.AC.LD.DETAIL
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SCB.LOANS.DUCH.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.LOANS.DUCH.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.LOANS.DUCH.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.LOANS.DUCH.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.LOANS.DUCH.CSV File IN &SAVEDLISTS&'
        END
    END

    AMT.LCY = 0 ; AMT.FCY = 0 ; AMT.LCY.TOT = 0 ; AMT.FCY.TOT = 0 ; DESC = ''
    WS.RATE = ''

    DAT = TODAY[1,6]:'01'
    CALL CDT("",DAT,'-1C')
    DAT.HED = DAT[1,4]:'/':DAT[5,2]

    HEAD1 = "���� ����� ����� �� ��� "
    HEAD.DESC  = ","
    HEAD.DESC := HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "������ ��������":","
    HEAD.DESC := "����� ���������":","
    HEAD.DESC := "���� ������":","
    HEAD.DESC := "������ ��������":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

*========================================================================
PROCESS:
    FN.CBE = 'F.CBE.STATIC.AC.LD.DETAIL' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.GDI = 'FBNK.GROUP.DEBIT.INT' ; F.GDI = ''
    CALL OPF(FN.GDI,F.GDI)

    FN.ADI = 'FBNK.ACCOUNT.DEBIT.INT' ; F.ADI = ''
    CALL OPF(FN.ADI,F.ADI)

    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LIM = 'FBNK.LIMIT' ; F.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ;  F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)


    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* ������ ����� ������ ������ *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 3201 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ����� ������ ������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ����� �����*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1102 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ����� �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ����� ����� ����� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1404 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ����� ����� �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ������� ����� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (1206 1208 1401 1402 1405 1406 11240 11238 11232 11234 11239 1480 1481 1499 1483 1493) AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ������� �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ������� ��� ����� ������ *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1202 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ������� ��� ����� ������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ����� �������� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (1301 1302 1303 1377 1390 1399) AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ����� ��������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ����� ������� �� ���� �� ���� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (1502 1503) AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ����� ������� �� ���� �� ����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ����� ������ ������ ����� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1416 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ����� ������ ������ �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ������� ����� ����� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (1201 11242 1477) AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ������� ����� �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� ����� ������ ���� ���� ���� ���� ���� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591) AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� ����� ������ ���� ���� ���� ���� ����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ������ - ���� ���� ���� - ���� ����*************

    T.SEL  = "SELECT ":FN.CBE:" WITH (( CBEM.CATEG IN ( 1001 1002 1003 1059 1205 1207 1504 1507 1508 1509 1510 1511 1512 1513 1514 )"
    T.SEL := " OR CBEM.CATEG IN ( 1534 1544 1559 1582 1407 1413 1408 6501 6502 6503 6504 6511 1558 ))"
    T.SEL := " AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '') BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ������ - ���� ���� ���� - ���� ����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������  *************

    T.SEL  = "SELECT ":FN.CBE:" WITH (( CBEM.CATEG IN ( 21056 21057 21060 21062 21063 )"
    T.SEL := " OR CBEM.CATEG IN ( 1414 1216 1217 1211 1212 1518 1519 1214 1215 ))"
    T.SEL := " AND CBEM.IN.LCY NE '' AND CBEM.IN.LCY NE 0 ) BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN ( 1595 1596 1597 1598 21050 21051 21052 ) AND CBEM.IN.LCY NE '' AND CBEM.IN.LCY NE 0 BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ��� ������ - ����� ����� *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1223 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ��� ������ - ����� �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ��� ������ - ����� ����� �����*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1225 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ��� ������ - ����� ����� �����'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ��� ������ - ������� ��� ����� ������*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1221 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ��� ������ - ������� ��� ����� ������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ��� ������ - ����� ��������*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1224 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ��� ������ - ����� ��������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ��� ������ - ������� ������*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG IN (1222 1227) AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ��� ������ - ������� ������'
    GOSUB PRINT.TOTAL
*-------------------------------------------
******* ������ ���� ������� - ����� ��� ������ - ���� ����*************

    T.SEL  = "SELECT ":FN.CBE:" WITH CBEM.CATEG EQ 1220 AND CBEM.IN.LCY LT 0 AND CBEM.IN.LCY NE '' BY CBEM.BR"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB GET.LIMIT
            GOSUB PRINT.DET
        NEXT I
    END
    DESC = '������ ���� ������� - ����� ��� ������ - ���� ����'
    GOSUB PRINT.TOTAL
*---------------------------------------------
    RETURN
*==============================================================
PRINT.DET:

    BB.DATA  = CUS.ID:","
    BB.DATA := CUST.NAME:","
    BB.DATA := WS.CCY:","
    BB.DATA := AMT.FCY:","
    BB.DATA := AMT.LCY:","
    BB.DATA := MAT.DATE:","
    BB.DATA := WS.RATE:","
    BB.DATA := INT.AMT:","
    BB.DATA := WS.CATEG:","
    BB.DATA := BRANCH:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    AMT.LCY.TOT   += AMT.LCY

    RETURN
*===============================================================
PRINT.TOTAL:

    NN.DATA  = "*****":","
    NN.DATA := DESC:","
    NN.DATA := "***":","
    NN.DATA := "*****":","
    NN.DATA := AMT.LCY.TOT:","
    NN.DATA := "*****":","
    NN.DATA := "***":","
    NN.DATA := "***":","
    NN.DATA := "***":","

    WRITESEQ NN.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    AMT.LCY.TOT = 0  ;  AMT.FCY.TOT = 0 ; DESC = ''
    RETURN
*===============================================================
GET.LIMIT:

    CALL F.READ(FN.LIM,LMT.ID,R.LIM,F.LIM,E1)

    INT.AMT = R.LIM<LI.INTERNAL.AMOUNT>
    LIM.CY  = R.LIM<LI.LIMIT.CURRENCY>

    RETURN
*===============================================================
GET.DETAIL:

    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
    COMP.BR = R.CBE<STD.CBEM.BR>
    COMP = 'EG00100':COMP.BR
*Line [ 465 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    WS.CCY    = R.CBE<STD.CBEM.CY>
    WS.CATEG  = R.CBE<STD.CBEM.CATEG>

    AMT.LCY   = R.CBE<STD.CBEM.IN.LCY>
    AMT.LCY   = DROUND(AMT.LCY,'0')
    IF AMT.LCY GT 0 THEN AMT.LCY = AMT.LCY * -1

    AMT.FCY   = R.CBE<STD.CBEM.IN.FCY>
    AMT.FCY   = DROUND(AMT.FCY,'0')

    IF AMT.FCY GT 0 THEN AMT.FCY = AMT.FCY * -1
*-----------------------------------------------------------
    CUS.ID  = R.CBE<STD.CBEM.CUSTOMER.CODE>
*Line [ 486 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

    IF KEY.LIST<I>[1,2] EQ 'LD' THEN
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E3)
        MAT.DATE  = R.LD<LD.FIN.MAT.DATE>
        WS.RATE.1 = R.LD<LD.INTEREST.RATE>
        WS.RATE.2 = R.LD<LD.INTEREST.SPREAD>
        WS.RATE   = WS.RATE.1 + WS.RATE.2

        LD.LMT.REF = R.LD<LD.LIMIT.REFERENCE>
        LMT.REF.1  = FIELD(LD.LMT.REF,".",1)
        LMT.REF.2  = FIELD(LD.LMT.REF,".",2)
        LMT.REF.3  = FMT(LMT.REF.1,'R%7')
        LMT.REF    = LMT.REF.3:'.':LMT.REF.2
        LMT.ID     = CUS.ID:'.':LMT.REF

    END ELSE
        CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
        GROUP.ID = R.AC<AC.CONDITION.GROUP>

        AC.LMT.REF = R.AC<AC.LIMIT.REF>
        LMT.REF.1 = FIELD(AC.LMT.REF,".",1)
        LMT.REF.2 = FIELD(AC.LMT.REF,".",2)
        LMT.REF.3 = FMT(LMT.REF.1,'R%7')
        LMT.REF = LMT.REF.3:'.':LMT.REF.2
        LMT.ID  = CUS.ID:'.':LMT.REF

        CALL F.READ(FN.GC,GROUP.ID,R.GC,F.GC,E1)
        MAT.DATE = R.GC<IC.GCP.DR.CAP.FREQUENCY>[1,8]
        MAT.DATE = FMT(MAT.DATE,"####/##/##")
        GDI.ID = GROUP.ID:WS.CCY:'...'
        ADI.ID = KEY.LIST<I>:'...'

        T.SEL1 = "SELECT ":FN.ADI:" WITH @ID LIKE ":ADI.ID:" BY @ID"
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
        IF SELECTED1 THEN
            CALL F.READ(FN.ADI,KEY.LIST1<SELECTED1>,R.ADI,F.ADI,E2)
            WS.OPER.1 = R.ADI<IC.ADI.DR.MARGIN.OPER,1>
            WS.MRG.1  = R.ADI<IC.ADI.DR.MARGIN.RATE,1>
            WS.RATE.1 = R.ADI<IC.ADI.DR.INT.RATE,1>

            IF WS.OPER.1 EQ 'ADD' THEN
                WS.RATE = WS.RATE.1 + WS.MRG.1
            END
            IF WS.OPER.1 EQ 'MULTIPLY' THEN
                WS.RATE = WS.RATE.1 * WS.MRG.1
            END
            IF WS.OPER.1 EQ 'SUBTRACT' THEN
                WS.RATE = WS.RATE.1 - WS.MRG.1
            END
            IF WS.OPER.1 EQ '' THEN
                WS.RATE = WS.RATE.1
            END
        END
    END
*    T.SEL1 = "SELECT ":FN.GDI:" WITH @ID LIKE ":GDI.ID:" BY @ID"
*    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
*    IF SELECTED1 THEN
*        CALL F.READ(FN.GDI,KEY.LIST1<SELECTED1>,R.GDI,F.GDI,E2)
*        WS.RATE = R.GDI<IC.GDI.DR.INT.RATE>
*    END

    RETURN
*===============================================================
END
