* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*------------------------------AHMED NAHRAWY (FOR EVER)--------------------
    SUBROUTINE SCB.ACTIVE2
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME

    GOSUB INITIATE
*   GOSUB PRINT.HEAD
*Line [ 43 ] Adding EB.SCBUpgradeTwelve. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 44 ] Adding EB.SCBUpgradeTwelve. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PRINT.HEAD
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    FRACTION = ''
    OUT.WORD = ''
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='TOT.ACTIVE.CUST.AHMED3'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
CALLDB:
    F.MULTI = '' ; FN.MULTI = 'F.INF.MULTI.TXN' ; R.MULTI = '' ; E1 = '' ; RETRY1 = ''
    OUT.AMOUNT = ''
    CALL OPF(FN.MULTI,F.MULTI)

    DATT = TODAY

    I=0
    INPUTTER  = R.NEW(INF.MLT.INPUTTER)
    AUTH = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 67 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
FN.F.ITSS.USER.SIGN.ON.NAME = ''
CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>


    AC1 = R.NEW(INF.MLT.ACCOUNT.NUMBER)
*Line [ 70 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD = DCOUNT(AC1,@VM)
    FOR J = 1 TO DD
*   TEXT ='NO = ':AC1  ; CALL REM
        ACCC      = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
        ACCCC     = R.NEW(INF.MLT.ACCOUNT.NUMBER)
        CUR       = R.NEW(INF.MLT.CURRENCY)<1,J>
        AMT1      = R.NEW(INF.MLT.AMOUNT.LCY)<1,J>
        AMT2      = R.NEW(INF.MLT.AMOUNT.FCY)<1,J>
        SIGN      = R.NEW(INF.MLT.SIGN)<1,J>
        IF SIGN EQ 'DEBIT' THEN
            SIGN  = '���'
        END ELSE
            IF SIGN EQ 'CREDIT' THEN
                SIGN = '�����'
            END
        END
        IF ACCC EQ '' THEN
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
            END
        END

        VALUE.DAT = R.NEW(INF.MLT.VALUE.DATE)<1,J>
* INPUTTER  = R.NEW(INF.MLT.INPUTTER)<1,J>
* AUTH      = R.NEW(INF.MLT.AUTHORISER)<1,J>
        INP   = FIELD(INPUTTER,'_',2)
*AUTHI = FIELD(AUTH,'_',2)
        NOTES     = R.NEW(INF.MLT.THEIR.REFERENCE)<1,J>
        PLCATEG   = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        ID=ID.NEW
* DATE=R.NEW(INF.MLT.DATE.TIME)<1,J>

        I ++
        XX = SPACE(90)
        XX<1,I>[1,20]   = ACCC
        XX<1,I>[25,10]  = CUR
        XX<1,I>[40,10]  = AMT1
        XX<1,I>[45,10]  = AMT2
        XX<1,I>[55,6]   = SIGN
        XX<1,I>[65,09]  = VALUE.DAT
        XX<1,I>[80.09]  = NOTES
        XX<1,I>[80,10]  = PLCATEG
* PRINT XX<1,I>
*    NEXT J

PRINT.HEAD:
*Line [ 126 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
        YYBRN = FIELD(BRANCH,'.',2)
        CUSNO = ACCC[2,7]
        CURR  = ACCC[9,2]

*   TEXT ='CUSNO =' : CUSNO ; CALL REM
*   TEXT ='CURR =' : CURR ; CALL REM

*Line [ 140 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        IF ACCC EQ '' THEN
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
*Line [ 152 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACCC,NAME)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            END
        END
*Line [ 161 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNO,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUSNO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS>
        DATY  = TODAY
        T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
        T.DAY1 = VALUE.DAT[7,2]:'/':VALUE.DAT[5,2]:"/":VALUE.DAT[1,4]

        PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(25):"��� :" :YYBRN
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):" ����� : ":SIGN
        PR.HD := "'L'":SPACE(1):"-----------------------------------------------------"
        PR.HD :="'L'":" "
        BEGIN CASE
        CASE ACCC NE ''
            PR.HD := "'L'":SPACE(1):"��� ������ :":ACCC :SPACE(20) : NAME
            PR.HD :="'L'":" "
        CASE 1
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
*Line [ 184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,ACCC,MYCAT)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,ACCC,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
MYCAT=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
            PR.HD := "'L'":SPACE(1):"��� ������ :":PLCATEG :SPACE(20) : MYCAT
        END CASE
*PR.HD := "'L'":SPACE(1):"������ :":AMT:SPACE(8):NAME1
        BEGIN CASE
        CASE CUR NE 'EGP'
            PR.HD := "'L'":SPACE(1):"������ :":AMT2:SPACE(8):CURR:SPACE(20):ADDRESS
        CASE CUR EQ 'EGP'
            PR.HD := "'L'":SPACE(1):"������ :":AMT1:SPACE(8):CURR:SPACE(20):ADDRESS
        END CASE
* PR.HD := "'L'":SPACE(1):"������ :":AMT:SPACE(8):CURR
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"����� ���� :" :T.DAY1
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"SIGN :":SIGN
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"������� :" : NOTES
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "

* TEXT ='AMOUNT  =' :AMT1  ; CALL REM
* TEXT ='AMOUNT2 =' :AMT2  ; CALL REM

        BEGIN CASE
        CASE CUR NE 'EGP'

* FOR Z = 2 TO DD
* AC5   = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,Z>
* :ACCCC<1,2>
* NEXT Z
* TEXT ='ACC =' : ACCCC<2,1> ; CALL REM
* TEXT ='ACC2 =' : AC5 ; CALL REM
* PR.HD :="��� ����"
* PR.HD :="'L'":" "
* PR.HD := AC5
* PR.HD :="'L'":" "
* PR.HD := "���� ����"

            IN.AMOUNT=AMT2
            CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            VV = OUT.AMOUNT:" ":CURR:" ":"�� ���"
            PR.HD := VV
        END CASE

* NEXT Z
* PR.HD :="��� �������� " : AC5
* PR.HD := "���� ����"
* IN.AMOUNT=AMT
* CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
*        VV = OUT.AMOUNT:" ":CURR:" ":"�� ���
*        PR.HD := VV
*   PR.HD :="'L'":" "

*   CASE SIGN EQ 'EGP'
        IF CUR EQ 'EGP' THEN
            IN.AMOUNT=AMT1
            CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            VV1 = OUT.AMOUNT:" ":CURR:" ":"�� ���"
            PR.HD := VV1
        END

*  END CASE

        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"��� �������":SPACE(5):"��� �������"::SPACE(5):"��� �������"
        PR.HD := "'L'":SPACE(1):" ---------          --------         ----------- "
        PR.HD := "'L'":SPACE(5):INP:SPACE(5):AUTHI:SPACE(5):ID

*        TEXT = 'INPUTTER = ' : INPUTTER ; CALL REM
*        TEXT = 'INP = ' : INP ; CALL REM
*        TEXT = 'AUTHORAIZER = ' : AUTH ; CALL REM
*        TEXT = 'AUTHI = ' : AUTHI ; CALL REM
        HEADING PR.HD
    NEXT J
    RETURN
*==============================================================
    RETURN
END
