* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
**    PROGRAM SBR.SME.TARGET.CONS
    SUBROUTINE SBR.SME.TARGET.CONS

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========

    OPENSEQ "&SAVEDLISTS&" , "SME.CUS.TARGET.DETAIL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SME.CUS.TARGET.DETAIL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SME.CUS.TARGET.DETAIL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SME.CUS.TARGET.DETAIL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SME.CUS.TARGET.DETAIL.CSV File IN &SAVEDLISTS&'
        END
    END
*---------------------------------

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")


    HEAD.DESC  = DAT.HED:","
    HEAD.DESC := "Description":","
    HEAD.DESC := "App.ID":","
    HEAD.DESC := "Customer.No":","
    HEAD.DESC := "Amount":","


    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*--------------------------------------
    FN.CONT = 'F.RE.STAT.LINE.CONT' ; F.CONT = ''
    CALL OPF(FN.CONT,F.CONT)

    FN.CONS = 'FBNK.RE.CONSOL.CONTRACT' ; F.CONS = ''
    CALL OPF(FN.CONS,F.CONS)

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.LC = 'FBNK.LETTER.OF.CREDIT' ; F.LC = ''
    CALL OPF(FN.LC,F.LC)

    FN.DR = 'FBNK.DRAWINGS' ; F.DR = ''
    CALL OPF(FN.DR,F.DR)



    RETURN
*-----------------------------------------------------------------------------
PROCESS:
*=======
    YTEXT = "Enter The Target : "
    CALL TXTINP(YTEXT, 8, 22, "4", "A")
    WS.TARGET = COMI

    WS.LN.ID = 'GENALL...'

    T.SEL1 = "SELECT ":FN.LN:" WITH @ID LIKE ":WS.LN.ID:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.LN,KEY.LIST1<K>,R.LN,F.LN,E6)

            WS.LINE.DESC = R.LN<RE.SRL.DESC,2>

            CONT.ID = KEY.LIST1<K>:'...'
            T.SEL = "SELECT ":FN.CONT:" WITH @ID LIKE ":CONT.ID:" BY @ID"
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
            IF SELECTED THEN
                FOR I = 1 TO SELECTED
                    CALL F.READ(FN.CONT,KEY.LIST<I>,R.CONT,F.CONT,E5)
*Line [ 139 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
                    FOR X = 1 TO DECOUNT.CONT
                        CONS.ID      = R.CONT<RE.SLC.ASST.CONSOL.KEY,X>
                        CONS.APP     = FIELD(CONS.ID,".",1)
                        CONS.TARGET  = FIELD(CONS.ID,".",13)
                        CONT.CUR     = FIELD(CONS.ID,".",4)
                        CONS.APP.LEN = LEN(CONS.APP)

** IF CONS.TARGET EQ 1000 OR CONS.TARGET EQ 1100 OR CONS.TARGET EQ 1200 OR CONS.TARGET EQ 2000 OR CONS.TARGET EQ 3000 OR CONS.TARGET EQ 4000 THEN

                        IF CONS.TARGET EQ WS.TARGET THEN
                            CALL F.READ(FN.CONS,CONS.ID,R.CONS,F.CONS,E2)
                            LOOP
                                REMOVE APP.ID FROM R.CONS SETTING POS
                            WHILE APP.ID:POS
                                IF CONS.APP = 'AC' THEN
                                    CALL F.READ(FN.AC,APP.ID,R.AC,F.AC,EAC)
                                    WS.AMT    = R.AC<AC.OPEN.ACTUAL.BAL>
                                    WS.CUS.NO = R.AC<AC.CUSTOMER>
                                END

                                IF CONS.APP = 'LD' THEN
                                    CALL F.READ(FN.LD,APP.ID,R.LD,F.LD,ELD)
                                    WS.AMT    = R.LD<LD.AMOUNT>
                                    WS.CUS.NO = R.LD<LD.CUSTOMER.ID>
                                END

                                IF CONS.APP = 'MM' THEN
                                    CALL F.READ(FN.MM,APP.ID,R.MM,F.MM,EMM)
                                    WS.AMT    = R.MM<MM.PRINCIPAL>
                                    WS.CUS.NO = R.MM<MM.CUSTOMER.ID>
                                END

                                IF CONS.APP = 'LC' AND CONS.APP.LEN = 12 THEN
                                    CALL F.READ(FN.LC,APP.ID,R.LC,F.LC,ELC)
                                    WS.AMT    = R.LC<TF.LC.LC.AMOUNT>
                                    WS.CUS.NO = R.LC<TF.LC.CON.CUS.LINK>
                                END

                                IF CONS.APP = 'LC' AND CONS.APP.LEN = 14 THEN
                                    CALL F.READ(FN.DR,APP.ID,R.DR,F.DR,EDR)
                                    WS.AMT    = R.DR<TF.DR.DOCUMENT.AMOUNT>
                                    WS.CUS.NO = R.DR<TF.DR.CUSTOMER.LINK>
                                END

                                IF CONS.APP = 'AC' OR CONS.APP = 'LD' OR CONS.APP = 'MM' OR CONS.APP = 'LC' THEN
                                    CALL F.READ(FN.CCY,CONT.CUR,R.CCY,F.CCY,E1)

                                    IF CONT.CUR EQ 'EGP' THEN
                                        RATE = 1
                                    END ELSE
                                        IF CONT.CUR EQ 'JPY' THEN
                                            RATE = R.CCY<SBD.CURR.MID.RATE> / 100
                                        END ELSE
                                            RATE = R.CCY<SBD.CURR.MID.RATE>
                                        END
                                    END
                                    WS.AMT.LCY = WS.AMT * RATE

                                    BB.DATA  = ",":WS.LINE.DESC:","
                                    BB.DATA := "'":APP.ID:"'":","
                                    BB.DATA := WS.CUS.NO:","
                                    BB.DATA := WS.AMT.LCY:","

                                    WRITESEQ BB.DATA TO BB ELSE
                                        PRINT " ERROR WRITE FILE "
                                    END

                                    WS.AMT.LCY = 0
                                END

                            REPEAT
                        END
                    NEXT X
                NEXT I
            END
        NEXT K
    END

    RETURN
*====================================================
