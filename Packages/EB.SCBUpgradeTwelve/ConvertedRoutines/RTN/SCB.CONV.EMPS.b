* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
****************
* WAGDY MOUNIR *
****************

    SUBROUTINE SCB.CONV.EMPS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.GOV

    L.NO =  O.DATA
    COMP =  ID.COMPANY
    EMP.CON = 0
    L.NO = O.DATA[1,3]
    CUS = O.DATA[4,8]


    TAB   = 'F.SCB.LOANS.GOV'
    F.CF  = ''
    ETEXT = ''

    OLD.EMP =""
    NEW.EMP = ""

    CALL OPF(TAB,F.CF)
    T.SEL = "SELECT F.SCB.LOANS.GOV WITH PAY.CODE EQ 13 AND LOAN.NO EQ ":L.NO:" AND CUSTOMER.NO EQ ":CUS: " AND CO.CODE EQ ":COMP: " BY EMP.NAME"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( TAB,KEY.LIST<I>, R.CF, F.CF, ETEXT)
            NEW.EMP = R.CF<GOV.EMP.NAME>

            IF NEW.EMP NE OLD.EMP THEN
                EMP.CON = EMP.CON + 1
            END

            OLD.EMP = NEW.EMP
        NEXT I
    END
    O.DATA = EMP.CON

    RETURN
END
