* @ValidationCode : Mjo1MjYzODIyMzA6Q3AxMjUyOjE2NDQ5MzI4MTczNjk6dXNlcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 15 Feb 2022 15:46:57
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-- CREATE BY AHMED NAHRAWY (FOR EVER)
*-- EDIT BY NESSMA MOHAMMED

SUBROUTINE SCB.ACTIVE.MALIA

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.REGION
*----------------------------------------------

    GOSUB INITIATE
*Line [ 50 ] Adding EB.SCBUpgradeTwelve. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 51 ] Adding EB.SCBUpgradeTwelve. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PRINT.HEAD

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
    FRACTION = ''
    OUT.WORD = ''
    REPORT.ID='SCB.ACTIVE.MALIA'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*----------------------------------------------------
CALLDB:
    F.MULTI = '' ; FN.MULTI = 'F.INF.MULTI.TXN' ; R.MULTI = '' ; E1 = '' ; RETRY1 = ''
    OUT.AMOUNT = ''
    CALL OPF(FN.MULTI,F.MULTI)

    DATT = TODAY

    I = 0
    INPUTTER = R.NEW(INF.MLT.INPUTTER)
    AUTH     = R.USER<EB.USE.SIGN.ON.NAME>
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('USER.SIGN.ON.NAME':@FM:EB.USO.USER.ID,AUTH,AUTHI)
    F.ITSS.USER.SIGN.ON.NAME = 'F.USER.SIGN.ON.NAME'
    FN.F.ITSS.USER.SIGN.ON.NAME = ''
    CALL OPF(F.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME)
    CALL F.READ(F.ITSS.USER.SIGN.ON.NAME,AUTH,R.ITSS.USER.SIGN.ON.NAME,FN.F.ITSS.USER.SIGN.ON.NAME,ERROR.USER.SIGN.ON.NAME)
    AUTHI=R.ITSS.USER.SIGN.ON.NAME<EB.USO.USER.ID>

    AC1 = R.NEW(INF.MLT.ACCOUNT.NUMBER)
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DD  = DCOUNT(AC1,@VM)
    FOR J = 1 TO DD
        ACCC      = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
        ACCCC     = R.NEW(INF.MLT.ACCOUNT.NUMBER)
        CUR       = R.NEW(INF.MLT.CURRENCY)<1,J>
        AMT1      = R.NEW(INF.MLT.AMOUNT.LCY)<1,J>
        AMT2      = R.NEW(INF.MLT.AMOUNT.FCY)<1,J>
        SIGN      = R.NEW(INF.MLT.SIGN)<1,J>
        IF SIGN EQ 'DEBIT' THEN
            SIGN  = '���'
        END ELSE
            IF SIGN EQ 'CREDIT' THEN
                SIGN = '�����'
            END
        END
        IF ACCC EQ '' THEN
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>
            END
        END

        VALUE.DAT = R.NEW(INF.MLT.VALUE.DATE)<1,J>
        INP       = FIELD(INPUTTER,'_',2)
        NOTES     = R.NEW(INF.MLT.THEIR.REFERENCE)<1,J>
        PLCATEG   = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        ID=ID.NEW

        I ++
        XX = SPACE(90)
        XX<1,I>[1,20]   = ACCC
        XX<1,I>[25,10]  = CUR
        XX<1,I>[40,10]  = AMT1
        XX<1,I>[45,10]  = AMT2
        XX<1,I>[55,6]   = SIGN
        XX<1,I>[65,09]  = VALUE.DAT
        XX<1,I>[80.09]  = NOTES
        XX<1,I>[80,10]  = PLCATEG

PRINT.HEAD:
*Line [ 127 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
        F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
        FN.F.ITSS.DEPT.ACCT.OFFICER = ''
        CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
        CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
        BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
        YYBRN = FIELD(BRANCH,'.',2)

*-- EDIT BY NESSMA 2016/08/22
* CUSNN = ACCC[2,7]
* CURR  = ACCC[9,2]
*Line [ 139 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACCC,CUSNN)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CUSNN=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 146 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCC,CURR)
        F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
        FN.F.ITSS.ACCOUNT = ''
        CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
        CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
        CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
*-- END EDIT

*Line [ 155 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
        IF ACCC EQ '' THEN
            ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
        END ELSE
            IF ACCC NE '' THEN
                ACCC = R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,J>

*Line [ 168 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACCC,NAME)
                F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
                FN.F.ITSS.ACCOUNT = ''
                CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
                CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
                NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            END
        END
        IF ACCC[1,3] NE 'EGP' OR ACCC[1,3] NE 'USD' OR ACCC[1,3] NE 'GBP' OR ACCC[1,2] NE 'PL' THEN
*Line [ 178 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACCC,CUSNN)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            CUSNN=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 185 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUSNN,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,CUSNN,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            NAME.A  = LOCAL.REF<1,CULR.ARABIC.NAME>
            NAME2.A = LOCAL.REF<1,CULR.ARABIC.NAME.2>

            ADDRESS = LOCAL.REF<1,CULR.ARABIC.ADDRESS><1,1>
            CUST.ADDRESS1= LOCAL.REF<1,CULR.GOVERNORATE>
            CUST.ADDRESS2= LOCAL.REF<1,CULR.REGION>

*Line [ 199 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.CUS.GOVERNORATE':@FM:GOVE.DESCRIPTION<1,1>,CUST.ADDRESS1,CUST.ADD2)
            F.ITSS.SCB.CUS.GOVERNORATE = 'F.SCB.CUS.GOVERNORATE'
            FN.F.ITSS.SCB.CUS.GOVERNORATE = ''
            CALL OPF(F.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE)
            CALL F.READ(F.ITSS.SCB.CUS.GOVERNORATE,CUST.ADDRESS1,R.ITSS.SCB.CUS.GOVERNORATE,FN.F.ITSS.SCB.CUS.GOVERNORATE,ERROR.SCB.CUS.GOVERNORATE)
            CUST.ADD2=R.ITSS.SCB.CUS.GOVERNORATE<GOVE.DESCRIPTION>
*Line [ 206 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('SCB.CUS.REGION':@FM:REG.DESCRIPTION<1,1>,CUST.ADDRESS2,CUST.ADD1)
            F.ITSS.SCB.CUS.REGION = 'F.SCB.CUS.REGION'
            FN.F.ITSS.SCB.CUS.REGION = ''
            CALL OPF(F.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION)
            CALL F.READ(F.ITSS.SCB.CUS.REGION,CUST.ADDRESS2,R.ITSS.SCB.CUS.REGION,FN.F.ITSS.SCB.CUS.REGION,ERROR.SCB.CUS.REGION)
            CUST.ADD1=R.ITSS.SCB.CUS.REGION<REG.DESCRIPTION>

            IF CUST.ADDRESS1 = 98 THEN
                CUST.ADD1 = ''
            END
            IF CUST.ADDRESS2 = 998 THEN
                CUST.ADD2 = ''
            END
            IF CUST.ADDRESS1 = 999 THEN
                CUST.ADD1 = ''
            END
            IF CUST.ADDRESS2 = 999 THEN
                CUST.ADD2 = ''
            END

        END
        IF ACCC[1,3] EQ 'EGP' OR ACCC[1,3] EQ 'USD' OR ACCC[1,3] EQ 'GBP' THEN
*Line [ 229 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACCC,ACTITLE)
            F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
            FN.F.ITSS.ACCOUNT = ''
            CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
            CALL F.READ(F.ITSS.ACCOUNT,ACCC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
            ACTITLE=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            NAME.A  = ACTITLE
            NAME2.A = ACTITLE
            ADDRESS = ACTITLE
        END

        DATY  = TODAY
        T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
        T.DAY1 = VALUE.DAT[7,2]:'/':VALUE.DAT[5,2]:"/":VALUE.DAT[1,4]

        PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(25):"��� :" :YYBRN
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(14):" ����� : ":SIGN
        PR.HD := "'L'":SPACE(1):"-----------------------------------------------------"
        PR.HD :="'L'":" "
        BEGIN CASE
            CASE ACCC NE ''
                PR.HD := "'L'":SPACE(1):"��� ������ :":ACCC :SPACE(20) : NAME.A : SPACE(2) : NAME2.A
                PR.HD :="'L'":" "
            CASE 1
                ACCC = R.NEW(INF.MLT.PL.CATEGORY)<1,J>
*Line [ 256 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,ACCC,MYCAT)
                F.ITSS.CATEGORY = 'F.CATEGORY'
                FN.F.ITSS.CATEGORY = ''
                CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
                CALL F.READ(F.ITSS.CATEGORY,ACCC,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
                MYCAT=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
                PR.HD := "'L'":SPACE(1):"��� ������ :":PLCATEG :SPACE(20) : MYCAT
        END CASE
        BEGIN CASE
            CASE CUR NE 'EGP'
                PR.HD := "'L'":SPACE(1):"������ :":AMT2:SPACE(8):CURR:SPACE(20):ADDRESS:SPACE(2):CUST.ADD2 :' ': CUST.ADD1
            CASE CUR EQ 'EGP'
                PR.HD := "'L'":SPACE(1):"������ :":AMT1:SPACE(8):CURR:SPACE(20):ADDRESS:SPACE(2):CUST.ADD2 :' ': CUST.ADD1
        END CASE
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"����� ���� :" :T.DAY1
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"SIGN :":SIGN
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"������� :" : NOTES
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "

        BEGIN CASE
            CASE CUR NE 'EGP'

                IN.AMOUNT=AMT2
                CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
                VV = OUT.AMOUNT:" ":CURR:" ":"�� ���"
                PR.HD := VV
        END CASE

        IF CUR EQ 'EGP' THEN
            IN.AMOUNT=AMT1
            CALL WORDS.ARABIC.AHMED(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
            VV1 = OUT.AMOUNT:" ":CURR:" ":"�� ���"
            PR.HD := VV1
        END

        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD := "'L'":SPACE(1):"��� �������":SPACE(5):"��� �������"::SPACE(5):"��� �������"
        PR.HD := "'L'":SPACE(1):" ---------          --------         --------"
        PR.HD := "'L'":SPACE(5):INP:SPACE(5):AUTHI:SPACE(5):ID

        HEADING PR.HD
    NEXT J
RETURN
*==============================================================
RETURN
END
