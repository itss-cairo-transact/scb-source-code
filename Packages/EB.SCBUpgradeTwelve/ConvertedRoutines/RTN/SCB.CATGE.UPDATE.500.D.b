* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.CATGE.UPDATE.500.D
*** PROGRAM SCB.CATGE.UPDATE.500.D

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    FN.CAT = 'FBNK.CATEG.ENTRY.DETAIL' ;F.CAT = '' ; R.CAT = ''

    CALL OPF(FN.CAT,F.CAT)
    KEY.LIST="" ; SELECTED="" ; ER.MSG=""

*   FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''
*   CALL OPF(FN.BASE,F.BASE)
    IDDD="EG0010001-COB"
*Line [ 44 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.PERIOD.END,IDDD,P.END)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
P.END=R.ITSS.DATES<EB.DAT.PERIOD.END>

    START.DAY = P.END[1,6]:"01"
    END.DAY.M = P.END


    T.SEL = "SELECT FBNK.CATEG.ENTRY.DETAIL WITH (BOOKING.DATE GE ":START.DAY:" AND BOOKING.DATE LE ": END.DAY.M :" ) AND CURRENCY NE EGP AND (PL.CATEGORY NE 69999 AND PL.CATEGORY NE 54000 AND PL.CATEGORY NE 54151 AND PL.CATEGORY NE 53000 AND PL.CATEGORY NE 53088) AND ACCOUNT.OFFICER NE '500' "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 1 TO SELECTED
        PRINT "I = ":I
        CALL F.READ(FN.CAT,KEY.LIST<I>,R.CAT,F.CAT,READ.ERR1)

        CURR  = R.CAT<AC.CAT.CURRENCY>
        CCONS = R.CAT<AC.CAT.CONSOL.KEY>
        CONS1 = FIELD(CCONS,".",1,2)
        CONS2 = FIELD(CCONS,".",4,15)

        NEW.CONS = CONS1:'.500.':CONS2

*      CALL F.READ(FN.BASE,'EOM',R.BASE,F.BASE,E3)

*      CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
*      LOCATE CURR IN CURR.BASE<1,1> SETTING POS THEN

*          RATE = R.BASE<RE.BCP.RATE,POS>
        R.CAT<AC.CAT.ACCOUNT.OFFICER> = 500

        IF R.CAT<AC.CAT.CONSOL.KEY> NE '' THEN
            R.CAT<AC.CAT.CONSOL.KEY>      = NEW.CONS
        END

*           NEW.AMT.LCY = R.CAT< AC.CAT.AMOUNT.FCY> * RATE
*           CALL EB.ROUND.AMOUNT ('USD',NEW.AMT.LCY,'',"2")

*           R.CAT< AC.CAT.AMOUNT.LCY>      = NEW.AMT.LCY


**WRITE  R.CAT TO F.CAT , KEY.LIST<I> ON ERROR
**    PRINT "CAN NOT WRITE RECORD":KEY.LIST<I>:"TO" :FN.CAT
**END
        CALL F.WRITE(FN.CAT,KEY.LIST<I>,R.CAT)
*       END
    NEXT I

************************************************************

    RETURN
END
