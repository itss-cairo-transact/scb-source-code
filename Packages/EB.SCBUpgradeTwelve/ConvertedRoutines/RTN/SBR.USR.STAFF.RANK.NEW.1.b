* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwelve  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwelve
*DONE
**** CREATED BY NESSREEN AHMED 20161215 ****
*********************************************

    PROGRAM SBR.USR.STAFF.RANK.NEW.1

    EXECUTE "OFSADMINLOGIN"

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD
**********
     CLOSESEQ BB
     CLOSESEQ BB.ERR

***********
    RETURN

INITIALISE:
*----------
    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "USER"
    SCB.VERSION = "COMPANY"
    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    RETURN

*=====================================================================================
BUILD.RECORD:

    COMMA = ","
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    FN.USR  = 'F.USER' ; F.USR = '' ; R.USR=''
    CALL OPF(FN.USR,F.USR)
*=====================================================================================

    SEQ.FILE.NAME = '&SAVEDLISTS&'
    RECORD.NAME = 'USER.STAFF.RANK'
*********************
    OPENSEQ SEQ.FILE.NAME , "USR.CORRECT.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE USR.CORRECT.txt CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create USR.CORRECT.TXT'
        END
    END
**************
    OPENSEQ SEQ.FILE.NAME , "USR.ERR.txt" TO BB.ERR ELSE
        CREATE BB.ERR THEN
            PRINT 'FILE USR.ERR.txt CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create USR.ERR.TXT'
        END
    END

**************************
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        TEXT = 'Unable to Locate ':SEQ.FILE.POINTER  ; CALL REM
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
*-----------------------------------------------------
            STAFF.NO = FIELD(Y.MSG,"|",1)
            STAFF.RANK.CO = FIELD(Y.MSG,"|",2)
            USER.ID = "SCB.":STAFF.NO
            GOSUB OFS.STAFF.RANK
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER
    RETURN
*=====================================================================================
OFS.STAFF.RANK:
    CALL F.READ(FN.USR,USER.ID,R.USR,F.USR,ER.USR)
    IF NOT(ER.USR) THEN
        GOSUB REPLACE.HMM
        IF OFS.MESSAGE.DATA NE '' THEN
*PRINT  OFS.MESSAGE.DATA
            SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT01//EG0010001," : USER.ID : OFS.MESSAGE.DATA
*PRINT SCB.OFS.MESSAGE
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
*PRINT SCB.OFS.MESSAGE
        END
*****************
        BB.DATA = SCB.OFS.MESSAGE
        CRT BB.DATA
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

******************
        OFS.MESSAGE.DATA = ''
        USER.ID = ''

    END  ELSE
        IF ER.USR THEN
            BB.DATA.ERR = STAFF.NO
            CRT BB.DATA.ERR
            WRITESEQ BB.DATA.ERR TO BB.ERR ELSE
                PRINT " ERROR WRITE FILE "
            END

        END

    END
    RETURN
**************************************************************
REPLACE.HMM:
    OFS.MESSAGE.DATA  =  ",STAFF.RANK:1:1=":STAFF.RANK.CO
    RETURN
**************************************************************
