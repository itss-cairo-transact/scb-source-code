* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*** CREATED BY MOHAMMED SABRY 2014/04/16

    SUBROUTINE SBD.DAILY.BFR.01

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE


** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

*------------------------------------------------------------------------------
*** AUTO RECOVERY PAST DUE ***

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SBD.LD.AUTO.RECOVER")

*------------------------------------------------------------------------------

*** ����� ����� ��������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBR.CD.DIF.DATE")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBR.CD.DIF.DATE.2")

*** ����� ����� ����� ������ �������� ���� ����
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBD.CD.SUEZ.BNK.CUS")
*** ����� ����� �� ����� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"EOD.CHANGE.LIM.DATE")
*** ����� ��� ���� ������� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"APPL.TRN.INS")
*** �������  �� ����� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"TOT.CUST.ACT.BAL")
*** ����� ����� ������� ��������
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SBD.DRMNT.FILES.FILL")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SBD.DRMNT.ACTIVE.FILL")
*** ����� ������� ������ ������� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"APPL.TRN.TOT")
***  ������ ����� ����� ����� ���
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"REPORT.NOTAUTH.TOTAL.UP2")
*** ����� ��� ������� �����������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBR.CUS.ACC.TEXT")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBR.AC.TXN.HIST")
*** ����� ��� ������ ������� ��� ������ �� ����
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"VVR.TRNS.TODAY")
*** ����� ��� ����� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"FILL.TELLER.D.W")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"FILL.TELLER.D.W.NEW")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SCB.CBE.R.CREATE")
***  ������ ��������� �� 2.5 ����� ���� ����������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBD.TLR.CO.3000000")
*** CUSTOMER.POSITION HIS
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"FILL.CUSTOMER.POS")
*** PROTOCOL HISTORY TO SCB.PROTOCOL
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"COB.PROTOCOL")
***����� ��� ������� ������� ��� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"SBD.CUS.P.D.001")
*** ����� �������� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"EOD.HOLD.DEL")
***--------------------------------------------------------------------
*** AUTO RECOVERY LOANS INSTALLMENT ***

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SBD.LD.AUTO.RECOVER.2")

***--------------------------------------------------------------------
*** LOAN STAMP FILE ***
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SBD.LOAN.STAMP.FILL")
*** Goaml customers and accounts' files------------------------------------------------------------
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"GOAML.MAPPING.HIS.2.TXT")
*** Getting versions changes ----------------------------------------------------
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.B,"VERSION.LIV.DIF")
***--------------------------------------------------------------------------
*** MoneyGram FX ***

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.B,"SBD.MG.USD.EGP")

***-------------------------------------------------------

    RETURN
END
