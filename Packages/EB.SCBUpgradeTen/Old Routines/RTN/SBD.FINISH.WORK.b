* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.FINISH.WORK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FINISH.WORK
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF

*********************** OPENING FILES *****************************
    FN.FW  = "F.SCB.FINISH.WORK"  ; F.FW   = "" ; R.FW = ""

    CALL OPF (FN.FW,F.FW)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FLG  = ''
    FLG1 = ''
    USR.COMP = R.USER<EB.USE.DEPARTMENT.CODE>
    USR.COMP1 = FMT(USR.COMP,'R%2')
    COMP = 'EG00100':USR.COMP1
    TD   = TODAY
    TT   = OCONV(TIME(),'MTS')
    FINISH.ID = COMP:'-':TD:'-':R.USER<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
*------------------------------------------------------------------

    YTEXT  = 'Y/N �� �� �������� �� ����� �����  '
    YTEXT := '����� : ':TD:' ':'������ : ':TT

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG1 = COMI
    IF FLG1 = 'Y' OR FLG1 = 'y' THEN
        CALL F.READ(FN.FW,FINISH.ID,R.FW,F.FW,E2)
        IF NOT(E2) THEN
            CUR.NO = R.FW<FW.CURR.NO> + 1
            YTEXT  = "�� ������� �� ��� - �� ���� ��������� (Y / N) : "
            CALL TXTINP(YTEXT, 8, 22, "1", "A")
            FLG = COMI
********************** COPY NEW DATA ******************************
            IF FLG = 'Y' OR FLG = 'y' THEN
                R.FW<FW.BRANCH.CODE> = COMP
                R.FW<FW.FINISH.DATE> = TD
                R.FW<FW.FINISH.TIME> = TT
                R.FW<FW.INPUTTER>    = R.USER<EB.USE.SIGN.ON.NAME>
                R.FW<FW.AUTHORISER>  = R.USER<EB.USE.SIGN.ON.NAME>
                R.FW<FW.DEPT.CODE>   = R.USER<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
                R.FW<FW.CURR.NO>     = CUR.NO
                CALL F.WRITE(FN.FW,FINISH.ID,R.FW)
                CALL  JOURNAL.UPDATE(FINISH.ID)
            END ELSE
                TEXT = '�� ��� �������' ;  CALL REM
            END
        END ELSE
            R.FW<FW.BRANCH.CODE> = COMP
            R.FW<FW.FINISH.DATE> = TD
            R.FW<FW.FINISH.TIME> = TT
            R.FW<FW.INPUTTER>    = R.USER<EB.USE.SIGN.ON.NAME>
            R.FW<FW.AUTHORISER>  = R.USER<EB.USE.SIGN.ON.NAME>
            R.FW<FW.DEPT.CODE>   = R.USER<EB.USE.LOCAL.REF><1,USER.SCB.DEPT.CODE>
            R.FW<FW.CURR.NO>     = 1
            CALL F.WRITE(FN.FW,FINISH.ID,R.FW)
            CALL  JOURNAL.UPDATE(FINISH.ID)
        END
    END ELSE
        TEXT = '�� ������ �� ��������' ; CALL REM
    END
END
