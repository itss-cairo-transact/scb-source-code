* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.CUS.ACC.TRX
*    PROGRAM SBD.CUS.ACC.TRX
***********MAHMOUD*****************
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*    $INCLUDE T24.BP I_F.ACCOUNT.CLOSED
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    $INCLUDE T24.BP I_F.ACCT.ACTIVITY   ;*IC.ACT.
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.DR
    $INCLUDE T24.BP I_F.STMT.ENTRY      ;*AC.STE.
*    $INCLUDE T24.BP I_F.STMT.PRINTED ;*STP.
*    $INCLUDE T24.BP I_F.ACCT.STMT.PRINT        ;*ASP.
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    CC = "A"
    CUST = ''
    FRDT = ''
    TODT = ''
    CUTXT = "INPUT CUSTOMER NUMBER: "
    FRTXT = "INPUT DATE FROM: "
    TOTXT = "INPUT DATE TO: "
    CALL TXTINP(CUTXT, 8, 22, 10,CC)
    CUST = COMI
    CALL TXTINP(FRTXT, 8, 22, 10,CC)
    FRDT = COMI
    CALL TXTINP(TOTXT, 8, 22, 10,CC)
    TODT = COMI

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 70 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    PRINT SPACE(120)
    PRINT STR('-',52):" ����� ������� ":STR('-',53)
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
***   REPORT.ID='SBD.CUS.ACC.TRX'
    REPORT.ID='P.FUNCTION'
    PROGRAM.ID = 'SBD.CUS.ACC.TRX'
    CALL PRINTER.ON(REPORT.ID,'')
    CUST.NAME = ''
    Y.OPEN.BAL = 0
    Y.LAST.BAL = 0
    Y.TOT.DR = 0
    Y.TOT.CR = 0
    Y.MAX.DR.BAL = 0
    Y.MIN.DR.BAL = 0
    Y.COMM   = 0
    K = 0
    MMM = 0
    JJ = 0
    PRVBAL = 0
    PRVBAL1= 0
    BAL1 = 0
    BAL2 = 0
    DB.MOV = 0
    DB.MOV2= 0
    CR.MOV = 0
    CR.MOV2= 0
    BAL.AC = 0
    MAXBAL = 0
    MINBAL = 0
    MINBALX= 0
    MINBAL2 = 0
    MAXBAL2 = 0
    STE.REF = ''
    STE.TTYP = ''
    STE.DR.CUST = ''
    STE.CR.CUST = ''
    TOT.INT = 0
    TOT.COM = 0
    TOTAL.INT = 0
    TOTAL.COM = 0
    JJ = 0
    XX = SPACE(130)
    XX1= SPACE(130)
    TD1 = TODAY
    RETURN
*===============================================================
CALLDB:
    FN.CUS.ACC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.ACC = ''
    FN.ACC     = 'FBNK.ACCOUNT' ; F.ACC = ''
    FN.ACC.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACC.ACT = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.ACC.ACT,F.ACC.ACT)
    FN.AC.CL = 'FBNK.ACCOUNT.CLOSED' ; F.AC.CL = '' ; R.AC.CL = '' ; ER.AC.CL = ''
    CALL OPF(FN.AC.CL,F.AC.CL)
    FN.STE     = 'F.STMT.ENTRY' ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.STP = 'FBNK.STMT.PRINTED' ; F.STP = ''   ; R.STP = ''
    CALL OPF(FN.STP,F.STP)
    FN.ASP = 'FBNK.ACCT.STMT.PRINT' ; F.ASP = ''   ; R.ASP = ''
    CALL OPF(FN.ASP,F.ASP)
    FN.SDR = 'FBNK.STMT.ACCT.DR' ; F.SDR = ''   ; R.SDR = ''
    CALL OPF(FN.SDR,F.SDR)
    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST.STE ="" ; SELECTED.STE ="" ;  ER.MSG.STE =""
    KEY.LIST.STP ="" ; SELECTED.STP ="" ;  ER.MSG.STP =""
    KEY.LIST.ASP ="" ; SELECTED.ASP ="" ;  ER.MSG.ASP =""
    KEY.LIST.SDR ="" ; SELECTED.SDR ="" ;  ER.MSG.SDR =""
*************************************************************************
    CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUST,CU.COM)
***?    IF CU.COM EQ COMP THEN
*========================================================================

    SEL.CMD="SELECT ":FN.CUS.ACC:" WITH @ID EQ ":CUST
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NUMREC,RTNCD)

    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST,CUST.LCL)
    CUST.NAME  = CUST.LCL<1,CULR.ARABIC.NAME>
    CUST.NAME2 = CUST.LCL<1,CULR.ARABIC.NAME.2>
    XX1<1,K>[1,100] =CUST:"  ":CUST.NAME:" ":CUST.NAME2
    PRINT XX1<1,K>
    PRINT SPACE(120)
*------------------------------------
    MMM = LEN(CUST)
    IF MMM LT 8 THEN
        CUS.LK = "0":CUST:"..."
    END ELSE
        CUS.LK = CUST:"..."
    END
    SS.SEL  = "SELECT ":FN.AC.CL:" WITH @ID LIKE ":CUS.LK
    SS.SEL := " AND ACCT.CLOSE.DATE GE ":FRDT
    CALL EB.READLIST(SS.SEL,K.LIST.CL,'',SELECTED.CL,ER.CL)
    LOOP
        REMOVE ACCT.CL FROM K.LIST.CL SETTING POS.ACC.CL
    WHILE ACCT.CL:POS.ACC.CL
        AC.LEN = LEN(ACCT.CL)
        IF AC.LEN EQ 16 THEN
            Y.ACC.ID = ACCT.CL
            GOSUB PROCESS.AC
        END
    REPEAT
*------------------------------------
    CALL F.READ(FN.CUS.ACC,CUST,R.CUS.ACC,F.CUS.ACC,Y.CUS.ACC.ERR)
    LOOP
        REMOVE Y.ACC.ID FROM R.CUS.ACC SETTING POS1
    WHILE Y.ACC.ID:POS1
        GOSUB PROCESS.AC
    REPEAT
*------------------------------------
***?    END ELSE
***?        PRINT "��� ������ �� ��� �����"
***?    END
    RETURN
*...................................
PROCESS.AC:
*-------
    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACC,F.ACC,Y.ACC.ERR)

    K++
    Y.CURR =R.ACC<AC.CURRENCY>
    Y.CAT  =R.ACC<AC.CATEGORY>
    MMM       = 0
    TT        = 0
    TTX       = 0
    PRVBAL    = 0
    PRVBAL1   = 0
    BAL1      = 0
    BAL2      = 0
    DB.MOV    = 0
    DB.MOV2   = 0
    CR.MOV    = 0
    CR.MOV2   = 0
    TOT.INT   = 0
    TOT.COM   = 0
    TOT.TRNS = 0
    TOTAL.INT = 0
    TOTAL.COM = 0
    MAXBAL    = 0
    MINBAL    = 0
    MINBALX   = 0
    MINBAL2   = 0
    MAXBAL2   = 0
    ASP.DT    = ''
    ASP.DATE  = ''
    ASP.BAL   = 0
    ASP.BAL1  = 0
    STE.AMT   = 0
*************************************
    IF Y.CAT NE 1002 THEN
*--------------------------------------------------------------------
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,FRDT,TODT,R.STP,ASP.BAL,ER)
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        PRVBAL = ASP.BAL
        MINBAL = PRVBAL
        MINBALX= PRVBAL
************************************************************************
        LOOP
            REMOVE Y.STE.ID FROM R.STP SETTING POS2
        WHILE Y.STE.ID:POS2
            JJ++
            CALL F.READ(FN.STE,Y.STE.ID,R.STE,F.STE,Y.STE.ERR)
            STE.REF = R.STE<AC.STE.OUR.REFERENCE>
            STE.STA = R.STE<AC.STE.RECORD.STATUS>
            STE.CURR= R.STE<AC.STE.CURRENCY>
            STE.COD = R.STE<AC.STE.TRANSACTION.CODE>
*            STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
*            IF STE.COD EQ '434' THEN
*                TOT.TRNS +=STE.AMT
*            END
            IF STE.REF EQ '' THEN
                STE.REF = R.STE<AC.STE.TRANS.REFERENCE>
*Line [ 250 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                FINDSTR '/B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'/',1) ELSE NULL
            END
            IF STE.REF[1,2] EQ 'FT' THEN
                CALL DBR('FUNDS.TRANSFER':@FM:FT.TRANSACTION.TYPE,STE.REF,STE.TTYP)
                CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.CUSTOMER,STE.REF,STE.DR.CUST)
                CALL DBR('FUNDS.TRANSFER':@FM:FT.CREDIT.CUSTOMER,STE.REF,STE.CR.CUST)
                IF STE.TTYP EQ '' THEN
                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.TRANSACTION.TYPE,STE.REF:';1',STE.TTYP)
                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.CUSTOMER,STE.REF:';1',STE.DR.CUST)
                    CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.CREDIT.CUSTOMER,STE.REF:';1',STE.CR.CUST)
                END
            END ELSE
                STE.DR.CUST = ''
                STE.CR.CUST = ''
                STE.TTYP    = ''
            END
            DR.CR.CUST = STE.DR.CUST:STE.CR.CUST
            STE.BOOK.DATE = R.STE<AC.STE.BOOKING.DATE>
            IF STE.CURR NE 'EGP' THEN
                STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
            END ELSE
                STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
            END
            IF STE.COD EQ '434' THEN
                TOT.TRNS +=STE.AMT
            END

**#            IF STE.BOOK.DATE GE FRDT AND STE.BOOK.DATE LE TODT THEN
****************************************************************
            IF STE.STA NE 'REVE' THEN
                IF STE.AMT LT 0 THEN
                    DB.MOV2 += STE.AMT
                END ELSE
                    CR.MOV2 += STE.AMT
                END
****************************************************************
                MINBAL += STE.AMT
                IF MINBAL LE MINBALX THEN
                    MINBALX = MINBAL
                END
            END
**#            END
            IF MINBALX LT 0 THEN
                MINBAL2 = MINBALX
            END
        REPEAT
        BAL2 = PRVBAL + DB.MOV2 + CR.MOV2
************************************************************************
*        EXECUTE "COMO ON MMMMMM"
*        O.SEL  ="SELECT ":FN.STE:" WITH @ID LIKE ":Y.ACC.ID
*        O.SEL :=" AND TRANSACTION.CODE EQ 434"
*        CALL EB.READLIST(O.SEL,KEY.LIST1,'',SELECTED,ER.MSG)
*        IF SELECTED THEN
*            FOR I = 1 TO SELECTED
*                CRT SELECTED
*                CALL F.READ(FN.STE,KEY.LIST1<X>,R.STE,F.STE,Y.STE.ERR)
*                IF STE.CURR NE 'EGP' THEN
*                    TRNS.AMT = R.STE<AC.STE.AMOUNT.FCY>
*                END ELSE
*                    TRNS.AMT = R.STE<AC.STE.AMOUNT.LCY>
*                END
*                TOT.TRNS += TRNS.AMT
*            NEXT X
*        END
*        EXECUTE "COMO OFF MMMMMM"
************************************************************************
        M.SEL  ="SELECT ":FN.SDR:" WITH @ID GE ":Y.ACC.ID:"-":FRDT
        M.SEL :=" AND @ID LE ":Y.ACC.ID:"-":TODT
        CALL EB.READLIST(M.SEL,KEY.LIST,'',SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.SDR,KEY.LIST<I>,R.SDR,F.SDR,Y.SDR.ERR)
                TOT.INT = R.SDR<IC.STMDR.TOTAL.INTEREST>
                TOT.COM = R.SDR<IC.STMDR.TOTAL.CHARGE>
                TOTAL.INT += TOT.INT
                TOTAL.COM += TOT.COM
            NEXT I
        END ELSE
            ZZ = 0
        END
        GOSUB PRINTLN
    END
    RETURN
*...................................
PRINTLN:
    XX  = SPACE(130)
    XX<1,K>[1,16]   = Y.ACC.ID
    XX<1,K>[18,15]  = FMT(PRVBAL,"L2,")
    XX<1,K>[35,15]  = FMT(DB.MOV2,"L2,")
    XX<1,K>[52,15]  = FMT(CR.MOV2,"L2,")
    XX<1,K>[69,15]  = FMT(BAL2,"L2,")
    XX<1,K>[86,15]  = FMT(MINBAL2,"L2,")
    XX<1,K>[103,15]  = FMT(TOT.TRNS,"L2,")
    XX<1,K>[116,15]  = FMT(TOTAL.INT,"L2,")
    XX<1,K>[130,15]  = FMT(TOTAL.COM,"L2,")

*    XX<1,K>[103,15] = FMT(TOTAL.INT,"L2,")
*    XX<1,K>[120,15] = FMT(TOTAL.COM,"L2,")
    PRINT XX<1,K>
*    PRVBAL  = 0
*    DB.MOV2 = 0
*    CR.MOV2 = 0
*    BAL2    = 0
*    MINBAL2 = 0
*    TOTAL.INT = 0
*    TOTAL.COM = 0
    RETURN
***********************************************************
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TD = TODAY

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    FRDTM  = FRDT[7,2]:'/':FRDT[5,2]:"/":FRDT[1,4]
    TODTM  = TODT[7,2]:'/':TODT[5,2]:"/":TODT[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":PROGRAM.ID
    PR.HD :="'L'":SPACE(50):"��� ������ ���� ���� ���� ����"
    PR.HD :="'L'":SPACE(50):"�� ����� ":FRDTM:"  ��� ����� ":TODTM
    PR.HD :="'L'":SPACE(53):"�������� ������� ������"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":""
    PR.HD :="'L'":"��� ������":SPACE(7):"���� ��� ������":SPACE(3):"������ ����":SPACE(4):"������ ����":SPACE(5):"���� ����� ������":SPACE(3):"���� ���� ����":SPACE(3):" ����� � ":SPACE(3):"�����":SPACE(3):"������ ��������"
    PR.HD :="'L'":STR('_',120)
**    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
