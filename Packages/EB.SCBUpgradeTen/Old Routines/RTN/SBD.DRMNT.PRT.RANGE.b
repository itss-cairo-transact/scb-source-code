* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-151</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.DRMNT.PRT.RANGE
*    PROGRAM SBD.DRMNT.PRT.RANGE

    $INSERT  T24.BP  I_COMMON
    $INSERT  T24.BP  I_EQUATE
    $INSERT  T24.BP  I_F.USER
    $INSERT  T24.BP  I_F.CUSTOMER.ACCOUNT
    $INSERT  T24.BP  I_F.DEPT.ACCT.OFFICER
    $INSERT  T24.BP  I_F.CUSTOMER
    $INSERT  T24.BP  I_F.ACCOUNT
    $INSERT  T24.BP  I_F.CATEGORY
    $INSERT  T24.BP  I_F.CURRENCY
    $INSERT  T24.BP  I_F.POSTING.RESTRICT
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT   TEMENOS.BP I_CU.LOCAL.REFS
    $INSERT   TEMENOS.BP I_AC.LOCAL.REFS
*--------------------------------------
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB PRINT.END
    RETURN
*--------------------------------------
PRINT.END:
*---------
    XX = SPACE(120)
    PRINT XX<1,1>
    XX<1,1>[1,50]   = "**** ������ ��� �������  ****"
    XX<1,1>[70,20]  = FMT(CUS.NO,"R0,")
    PRINT XX<1,1>
    XX = SPACE(120)
    PRINT XX<1,1>
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)
*-------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*--------------------------------------
INITIAL:
*-------
    YTEXT = "Enter Start Date:"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    FROM.DATE = COMI
*-------------
    YTEXT = "Enter End Date:"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    TO.DATE = COMI
    PROGRAM.ID = "SBD.DRMNT.PRT.RANGE"

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT" ;  F.CUS.ACC  = ""
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CUSTOMER = "FBNK.CUSTOMER" ; F.CUSTOMER  = ""
    CALL OPF(FN.CUSTOMER , F.CUSTOMER)

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC  = ""
    CALL OPF(FN.ACC,F.ACC)

    SYS.DATE  = TODAY
    WS.COMP   = ID.COMPANY
    P.DATE    = FMT(SYS.DATE,"####/##/##")

    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    CUS.NO    = 0
    LAST.DATE = ''
    RETURN
*--------------------------------------
PROCESS:
*-------
    CUSTOMER.SEL  = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT EQ '18'"
    CUSTOMER.SEL := " AND DRMNT.CODE EQ 1"
    CUSTOMER.SEL := " AND DRMNT.DATE GE ": FROM.DATE
    CUSTOMER.SEL := " AND DRMNT.DATE LE ": TO.DATE
    CUSTOMER.SEL := " AND COMPANY.BOOK EQ ": WS.COMP
    CALL EB.READLIST(CUSTOMER.SEL,KEY.LIST,'',SELECTED,ERR)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CUS.ID = KEY.LIST<II>

            GOSUB PRINT.CUSTOMER
            GOSUB PRINT.ACCOUNT
            GOSUB PRINT.LINE
        NEXT II
    END ELSE
        GOSUB PRINT.NO.DATA
    END
    RETURN
*--------------------------------------
PRINT.LINE:
*----------
    XX = STR('_',120)
    PRINT XX<1,1>
    RETURN
*--------------------------------------
PRINT.CUSTOMER:
*--------------
    CUS.NO = CUS.NO + 1
    XX     = SPACE(120)
    XX<1,1>[1,10]   = CUS.ID
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
    CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>

    XX<1,1>[12,40]  = CUST.NAME

    PRINT XX<1,1>

    RETURN
*-------------------------------------------------------------------
PRINT.ACCOUNT:
*-------------
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)
    LOOP
        REMOVE ACCOUNT.NUMBER FROM R.CUS.ACC SETTING POS.ACC
    WHILE ACCOUNT.NUMBER:POS.ACC
        CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACCT, F.ACC, E.ACC1)

        W.BAL      = R.ACCT<AC.WORKING.BALANCE>
        Y.CATEG.ID = R.ACCT<AC.CATEGORY>
        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,Y.CATEG.ID,CATEG.NAME)
        WS.CY      = R.ACCT<AC.CURRENCY>
        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,WS.CY,CURR.NAME)
*-----
        CUST.DATE.CR    = R.ACCT<AC.DATE.LAST.CR.CUST>
        CUST.DATE.DR    = R.ACCT<AC.DATE.LAST.DR.CUST>
        OPENING.DATE    = R.ACCT<AC.OPENING.DATE>

        IF CUST.DATE.CR EQ ''   THEN
            IF CUST.DATE.DR EQ ''   THEN

                LAST.DATE = OPENING.DATE
            END
        END
*-----
        IF CUST.DATE.CR GT LAST.DATE THEN
            LAST.DATE = CUST.DATE.CR
        END

        IF CUST.DATE.DR GT LAST.DATE THEN
            LAST.DATE   = CUST.DATE.DR
        END
*-----
        XX = SPACE(120)
        XX<1,1>[1,20]   = ACCOUNT.NUMBER
        XX<1,1>[33,6]   = Y.CATEG.ID
        XX<1,1>[40,12]  = CATEG.NAME
        XX<1,1>[55,3]   = WS.CY
        XX<1,1>[60,15]  = CURR.NAME
        XX<1,1>[80,10]  = FMT(LAST.DATE,"####/##/##")
        XX<1,1>[110,20] = FMT(W.BAL,"R2,")

        PRINT XX<1,1>
        LAST.DATE = ''

    REPEAT
    RETURN
*--------------------------------------
PRINT.NO.DATA:
*-------------
    XX = "********* �� ���� ����� **********"
    PRINT XX<1,1>
    RETURN
*--------------------------------------
PRINT.HEAD:
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
    YYBRN = BRANCH

    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):'SBD.DRMNT.PRT.RANGE'
    PR.HD :="'L'":SPACE(35):" ����� ������� ������� ���� �� ����� ��� ��� ����� "
    PR.HD :="'L'":SPACE(40):" ����� ������ ���� ���� ������ �� "

    F.DATE = FROM.DATE
    T.DATE = TO.DATE
    F.DATE = FMT(F.DATE,"####/##/##")
    T.DATE = FMT(T.DATE,"####/##/##")

    PR.HD :=F.DATE : "���" :T.DATE

    PR.HD :="'L'":SPACE(30):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(05):" ��� ������ "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(25):" �������":SPACE(15):"������":SPACE(14):"��� ����":SPACE(17):"������"
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    LINE.NO = 0
    RETURN
*===============================================================

    RETURN
END
