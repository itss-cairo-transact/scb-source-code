* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
   SUBROUTINE SBD.MLAD.PROG.101.EXCEL
*    PROGRAM SBD.MLAD.PROG.101.EXCEL
*   ------------------------------
    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.USER
    $INSERT T24.BP  I_F.CURRENCY
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_USER.ENV.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.020
*----------------------------------------------
*--- EDIT BY NESSMA ON 2017/12/21
*    FLG   = 0
*    YTEXT = " Enter (1-PRINT) OR (2-CBE)"
*    CALL TXTINP(YTEXT, 8, 22, "17", "A")
*    WS.NUM  = COMI

    OPENSEQ "&SAVEDLISTS&" , "SBD.MLAD.PROG.101.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBD.MLAD.PROG.101.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBD.MLAD.PROG.101.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBD.MLAD.PROG.101 CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBD.MLAD.PROG.101 File IN &SAVEDLISTS&'
        END
    END

    WS.COMP   = ID.COMPANY
    REPORT.ID = 'SBM.C.PRT.001'
    GOSUB INIT
    GOSUB OPENFILES
    GOSUB READ.MLAD.FILE.020
    GOSUB END.PART.TWO

    RETURN
*-------------------------------------------------------------------------
INIT:
    PROGRAM.ID  = "SBD.MLAD.PROG.101"
    FN.MLAD.020 = "F.SCB.MLAD.FILE.020"
    F.MLAD.020  = ""
    R.MLAD.020  = ""
    Y.MLAD.020.ID   = ""
*----------------------------------
    FN.CURRENCY = 'FBNK.CURRENCY'
    F.CURRENCY  = ''
    R.CURRENCY = ''
    Y.CURRENCY.ID = ''
    CALL OPF(FN.CURRENCY,F.CURRENCY)

*---------------------------------------
    SYS.DATE = TODAY
    P.TIME    = TIMEDATE()[1,8]

    SYS.YYMM = SYS.DATE[1,6]

    WRK.DATE = SYS.DATE
    P.DATE   = FMT(SYS.DATE,"####/##/##")

*---------------------------------------
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]
*---------------------------------------
    DIM ARY.X(12)
    ARY.X(1) = "������"
    ARY.X(2) = "������"
    ARY.X(3) = "������"
    ARY.X(4) = "������"
    ARY.X(5) = "������"
    ARY.X(6) = "������"
    ARY.X(7) = "������"
    ARY.X(8) = "������"
    ARY.X(9) = "������"
    ARY.X(10) = "������"
    ARY.X(11) = "������"
    ARY.X(12) = "������"
    MON = ARY.X(WRK.MM)

*-----------------------
    LINE.NO = 0
    MAX.LINE.NO = 30
*-----------------------
    X.CY = "EGP"


    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLAD.020,F.MLAD.020)

    RETURN
*--------------------------------------------------------------------------
READ.MLAD.FILE.020:
    SAVE.CY = ''
    SAVE.AS = ''


    SEL.MLAD.020  = "SELECT ":FN.MLAD.020
    SEL.MLAD.020 := " WITH CURRENCY NE ":X.CY
    SEL.MLAD.020 := " WITH PRINTABLE.CODE EQ 'Y'"
    SEL.MLAD.020 :=" BY @ID"


    CALL EB.READLIST(SEL.MLAD.020,SEL.LIST.MLAD.020,'',NO.OF.MLAD.020,ERR.MLAD.020)
    LOOP
        REMOVE Y.MLAD.020.ID FROM SEL.LIST.MLAD.020 SETTING POS.MLAD.020
    WHILE Y.MLAD.020.ID:POS.MLAD.020
        CALL F.READ(FN.MLAD.020,Y.MLAD.020.ID,R.MLAD.020,F.MLAD.020,ERR.MLAD.020)


        N.CURR   = R.MLAD.020<ML020.CURR.NAME>
        N.DESCR  = R.MLAD.020<ML020.LINE.DESCR>

        N.DAY    = R.MLAD.020<ML020.NEXT.DAY>
        N.WEEK   = R.MLAD.020<ML020.NEXT.WEEK>
        N.MONTH  = R.MLAD.020<ML020.NEXT.MONTH>
        N.3MONTH = R.MLAD.020<ML020.NEXT.3MONTH>
        N.6MONTH = R.MLAD.020<ML020.NEXT.6MONTH>
        N.YEAR   = R.MLAD.020<ML020.NEXT.YEAR>
        N.3YEAR  = R.MLAD.020<ML020.NEXT.3YEAR>
        N.MORE   = R.MLAD.020<ML020.NEXT.MORE>

        M.DAY    = FMT((N.DAY)/1000,"L0")
        M.WEEK   = FMT((N.WEEK)/1000,"L0")
        M.MONTH  = FMT((N.MONTH)/1000,"L0")
        M.3MONTH = FMT((N.3MONTH)/1000,"L0")
        M.6MONTH = FMT((N.6MONTH)/1000,"L0")
        M.YEAR   = FMT((N.YEAR)/1000,"L0")
        M.3YEAR  = FMT((N.3YEAR)/1000,"L0")
        M.MORE   = FMT((N.MORE)/1000,"L0")
        M.TOTAL  = M.DAY + M.WEEK + M.MONTH + M.3MONTH + M.6MONTH + M.YEAR + M.3YEAR + M.MORE

* -------------------------------------
        FILE.CY = FIELD(Y.MLAD.020.ID,'-',1,1)
        FILE.AS = FIELD(Y.MLAD.020.ID,'-',2,1)


        IF SAVE.CY EQ '' THEN

            GOSUB MOVE.ZEROS

            SAVE.CY = FILE.CY
            SAVE.AS = FILE.AS

            GOSUB PRINT.HEAD
        END
* -------------------------------------
        IF SAVE.CY NE FILE.CY THEN

            GOSUB END.PART.TWO

            GOSUB PRINT.HEAD
        END
* -------------------------------------
        IF SAVE.AS NE FILE.AS THEN
            IF FILE.AS EQ 'Y' THEN

                GOSUB END.PART.ONE
            END
        END
* -------------------------------------
        IF SAVE.AS EQ 'X' THEN
            X.DAY    += M.DAY
            X.WEEK   += M.WEEK
            X.MONTH  += M.MONTH
            X.3MONTH += M.3MONTH
            X.6MONTH += M.6MONTH
            X.YEAR   += M.YEAR
            X.3YEAR  += M.3YEAR
            X.MORE   += M.MORE
            X.TOTAL  += M.TOTAL
        END
* -------------------------------------
        IF SAVE.AS EQ 'Y' THEN
            Y.DAY    += M.DAY
            Y.WEEK   += M.WEEK
            Y.MONTH  += M.MONTH
            Y.3MONTH += M.3MONTH
            Y.6MONTH += M.6MONTH
            Y.YEAR   += M.YEAR
            Y.3YEAR  += M.3YEAR
            Y.MORE   += M.MORE
            Y.TOTAL  += M.TOTAL
        END
* -------------------------------------


        P.DAY    = M.DAY
        P.WEEK   = M.WEEK
        P.MONTH  = M.MONTH
        P.3MONTH = M.3MONTH
        P.6MONTH = M.6MONTH
        P.YEAR   = M.YEAR
        P.3YEAR  = M.3YEAR
        P.MORE   = M.MORE
        P.TOTAL  = M.TOTAL

        P.DESCR = N.DESCR
        GOSUB PRINT.ONE.LINE

*---------------
    REPEAT

    RETURN
*--------------------------------------------------------------------------
MOVE.ZEROS:
    X.DAY    = 0
    X.WEEK   = 0
    X.MONTH  = 0
    X.3MONTH = 0
    X.6MONTH = 0
    X.YEAR   = 0
    X.3YEAR  = 0
    X.MORE   = 0
    X.TOTAL  = 0

    Y.DAY    = 0
    Y.WEEK   = 0
    Y.MONTH  = 0
    Y.3MONTH = 0
    Y.6MONTH = 0
    Y.YEAR   = 0
    Y.3YEAR  = 0
    Y.MORE   = 0
    Y.TOTAL  = 0

    RETURN
*--------------------------------------------------------------------------
END.PART.ONE:

    P.DAY    = X.DAY
    P.WEEK   = X.WEEK
    P.MONTH  = X.MONTH
    P.3MONTH = X.3MONTH
    P.6MONTH = X.6MONTH
    P.YEAR   = X.YEAR
    P.3YEAR  = X.3YEAR
    P.MORE   = X.MORE
    P.TOTAL  = X.TOTAL

    P.DESCR = "������ ������)1("


    GOSUB PRINT.ONE.LINE


    SAVE.AS = FILE.AS
    RETURN
*--------------------------------------------------------------------------
END.PART.TWO:
*    XX= SPACE(140)

*    XX<1,1>[1,130]   = STR('_',130)
*    PRINT XX<1,1>

    P.DAY    = Y.DAY
    P.WEEK   = Y.WEEK
    P.MONTH  = Y.MONTH
    P.3MONTH = Y.3MONTH
    P.6MONTH = Y.6MONTH
    P.YEAR   = Y.YEAR
    P.3YEAR  = Y.3YEAR
    P.MORE   = Y.MORE
    P.TOTAL  = Y.TOTAL

    P.DESCR = "������ ����������)2("

    GOSUB PRINT.ONE.LINE

* ------------------------------------------------------------------------

    P.DAY    = X.DAY      - Y.DAY
    P.WEEK   = X.WEEK     - Y.WEEK
    P.MONTH  = X.MONTH    - Y.MONTH
    P.3MONTH = X.3MONTH   - Y.3MONTH
    P.6MONTH = X.6MONTH   - Y.6MONTH
    P.YEAR   = X.YEAR     - Y.YEAR
    P.3YEAR  = X.3YEAR    - Y.3YEAR
    P.MORE   = X.MORE     - Y.MORE
    P.TOTAL  = X.TOTAL    - Y.TOTAL


* -----------------------
    GOSUB SAVE.OLD.VALUE
* -----------------------


    P.DESCR = "������ ������� )3(=)1( - )2("


* -----------------------
    GOSUB PRINT.ONE.LINE

* ------------------------------------------------------------------------
**     ������ ���������

    J.DAY    = J.DAY
    J.WEEK   = J.WEEK     + J.DAY
    J.MONTH  = J.MONTH    + J.WEEK
    J.3MONTH = J.3MONTH   + J.MONTH
    J.6MONTH = J.6MONTH   + J.3MONTH
    J.YEAR   = J.YEAR     + J.6MONTH
    J.3YEAR  = J.3YEAR    + J.YEAR
    J.MORE   = J.MORE     + J.3YEAR
    J.TOTAL  = J.TOTAL    + J.MORE


    P.DAY    = J.DAY
    P.WEEK   = J.WEEK
    P.MONTH  = J.MONTH
    P.3MONTH = J.3MONTH
    P.6MONTH = J.6MONTH
    P.YEAR   = J.YEAR
    P.3YEAR  = J.3YEAR
    P.MORE   = J.MORE
    P.TOTAL  = J.TOTAL


    P.DESCR = "������ ���������"

* ----------------------
    GOSUB PRINT.ONE.LINE
* ----------------------



***    RETURN

* ------------------------------------------------------------------------
**     ���� ������ ������� ��� ������ ����������

    K.DAY    = K.DAY      / Y.TOTAL * 100
    K.WEEK   = K.WEEK     / Y.TOTAL * 100
    K.MONTH  = K.MONTH    / Y.TOTAL * 100
    K.3MONTH = K.3MONTH   / Y.TOTAL * 100
    K.6MONTH = K.6MONTH   / Y.TOTAL * 100
    K.YEAR   = K.YEAR     / Y.TOTAL * 100
    K.3YEAR  = K.3YEAR    / Y.TOTAL * 100
    K.MORE   = K.MORE     / Y.TOTAL * 100
    K.TOTAL  = K.TOTAL    / Y.TOTAL * 100


    P.DAY    = K.DAY
    P.WEEK   = K.WEEK
    P.MONTH  = K.MONTH
    P.3MONTH = K.3MONTH
    P.6MONTH = K.6MONTH
    P.YEAR   = K.YEAR
    P.3YEAR  = K.3YEAR
    P.MORE   = K.MORE
    P.TOTAL  = K.TOTAL


***    P.DESCR = "���� ������ ������� ��� �. ����������"
***    P.DESCR = "�.���� ����� �� �. ��������"


    P.DESCR = "���� ���� ����� ����������"
* ----------------------
    GOSUB PRINT.CENT.LINE

* ------------------------------------------------------------------------
**     ���� ������ ��������� ��� ������ ����������


    J.DAY    = J.DAY      / Y.TOTAL * 100
    J.WEEK   = J.WEEK     / Y.TOTAL * 100
    J.MONTH  = J.MONTH    / Y.TOTAL * 100
    J.3MONTH = J.3MONTH   / Y.TOTAL * 100
    J.6MONTH = J.6MONTH   / Y.TOTAL * 100
    J.YEAR   = J.YEAR     / Y.TOTAL * 100
    J.3YEAR  = J.3YEAR    / Y.TOTAL * 100
    J.MORE   = J.MORE     / Y.TOTAL * 100
    J.TOTAL  = J.TOTAL    / Y.TOTAL * 100


    P.DAY    = J.DAY
    P.WEEK   = J.WEEK
    P.MONTH  = J.MONTH
    P.3MONTH = J.3MONTH
    P.6MONTH = J.6MONTH
    P.YEAR   = J.YEAR
    P.3YEAR  = J.3YEAR
    P.MORE   = J.MORE
    P.TOTAL  = J.TOTAL

***    P.DESCR = "���� ������ ��������� ��� �.����������"
***    P.DESCR = "�.���� ������� �� �.��������"


    P.DESCR = "���� ���� ������� ����������"


* ----------------------
    GOSUB PRINT.CENT.LINE
* ------------------------------------------------------------------------

    SAVE.CY = FILE.CY
    SAVE.AS = FILE.AS

    GOSUB MOVE.ZEROS


    RETURN
*--------------------------------------------------------------------------
SAVE.OLD.VALUE:


**                    �������� ���� ����� �� ���� ���
    J.DAY    = 0
    J.WEEK   = 0
    J.MONTH  = 0
    J.3MONTH = 0
    J.6MONTH = 0
    J.YEAR   = 0
    J.3YEAR  = 0
    J.MORE   = 0
    J.TOTAL  = 0

    K.DAY    = 0
    K.WEEK   = 0
    K.MONTH  = 0
    K.3MONTH = 0
    K.6MONTH = 0
    K.YEAR   = 0
    K.3YEAR  = 0
    K.MORE   = 0
    K.TOTAL  = 0

**                    �������� ���� ����� �� ���� ���
    J.DAY    = P.DAY
    J.WEEK   = P.WEEK
    J.MONTH  = P.MONTH
    J.3MONTH = P.3MONTH
    J.6MONTH = P.6MONTH
    J.YEAR   = P.YEAR
    J.3YEAR  = P.3YEAR
    J.MORE   = P.MORE
    J.TOTAL  = P.TOTAL

    K.DAY    = P.DAY
    K.WEEK   = P.WEEK
    K.MONTH  = P.MONTH
    K.3MONTH = P.3MONTH
    K.6MONTH = P.6MONTH
    K.YEAR   = P.YEAR
    K.3YEAR  = P.3YEAR
    K.MORE   = P.MORE
    K.TOTAL  = P.TOTAL


    RETURN
*--------------------------------------------------------------------------
PRINT.ONE.LINE:

    BB.DATA = ""
    BB.DATA   = P.DESCR:","
    BB.DATA    := FMT(P.DAY,"L"):","
    BB.DATA    := FMT(P.WEEK,"L"):","
    BB.DATA    := FMT(P.MONTH,"L"):","
    BB.DATA    := FMT(P.3MONTH,"L"):","
    BB.DATA    := FMT(P.6MONTH,"L"):","
    BB.DATA    := FMT(P.YEAR,"L"):","
    BB.DATA    := FMT(P.3YEAR,"L"):","
    BB.DATA    := FMT(P.MORE,"L"):","
    BB.DATA    := FMT(P.TOTAL,"L")

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*--------------------------------------------------------------------------
PRINT.CENT.LINE:

    BB.DATA = ""
    BB.DATA     = P.DESCR:","
    BB.DATA    := '%':FMT(P.DAY,"R2,"):","
    BB.DATA    := '%':FMT(P.WEEK,"R2,"):","
    BB.DATA    := '%':FMT(P.MONTH,"R2,"):","
    BB.DATA    := '%':FMT(P.3MONTH,"R2,"):","
    BB.DATA    := '%':FMT(P.6MONTH,"R2,"):","
    BB.DATA    := '%':FMT(P.YEAR,"R2,"):","
    BB.DATA    := '%':FMT(P.3YEAR,"R2,"):","
    BB.DATA    := '%':FMT(P.MORE,"R2,"):","
    BB.DATA    := '%':FMT(P.TOTAL,"R2,")

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*--------------------------------------------------------------------------
PRINT.HEAD:

    BB.DATA      = ""
    HEAD.DESC   = ",,,,":"���� ��������� ������ ����������"
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA      = ""
    HEAD.DESC   = ",,,,":"�������":",":P.DATE
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
    YYBRN = BRANCH

    CURR.NAME = ''
    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,X.CY,CURR.NAME)
    CURR.NAME = N.CURR

    D.X = "����� ������"

    BB.DATA      = ""
    HEAD.DESC   = "������":",":CURR.NAME:" ":D.X
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END



    GOSUB FILL.HEAD.1

    BB.DATA = ""
    BB.DATA    = D.A:","
    BB.DATA   := D.B:","
    BB.DATA   := D.C:","
    BB.DATA   := D.D:","
    BB.DATA   := D.E:","
    BB.DATA   := D.F:","
    BB.DATA   := D.G:","
    BB.DATA   := D.H:","
    BB.DATA   := D.I:","
    BB.DATA   := D.J


    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    RETURN
*===============================================================
FILL.HEAD.1:
    D.A = "�����������������                   "
    D.B = " ����� ������"
    D.C = "����� ���� "
    D.D = "���� �� ����� � ��� ��� "
    D.E = "���� �� ��� � ��� 3 ���� "
    D.F = "���� �� 3 ���� � ��� 6 ���� "
    D.G = "���� �� 6 ���� � ��� ��� "
    D.H = "���� �� ��� � ��� 3 ����� "
    D.I = "���� �� 3 ����� "
    D.J = " �������� "

    RETURN
*===============================================================
END
