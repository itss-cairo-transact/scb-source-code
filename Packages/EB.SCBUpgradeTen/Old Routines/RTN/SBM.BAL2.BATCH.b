* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
********************************CREATED BY REHAM 25/11/2009************
    SUBROUTINE SBM.BAL2.BATCH

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    CLOSE.BAL1 ='' ; CLOSE.BAL11 = '' ; CLOSE.BAL111 =''
    CLOSE.BAL2 ='' ; CLOSE.BAL22 = '' ; CLOSE.BAL222 =''
    CLOSE.BAL3 ='' ; CLOSE.BAL33 = '' ; CLOSE.BAL333 =''
    CLOSE.BAL4 ='' ; CLOSE.BAL44 = '' ; CLOSE.BAL444 =''
    CLOSE.BAL5 ='' ; CLOSE.BAL55 = '' ; CLOSE.BAL555 =''
    CLOSE.BAL6 ='' ; CLOSE.BAL66 = '' ; CLOSE.BAL666 =''
    CLOSE.BAL7 ='' ; CLOSE.BAL77 = '' ; CLOSE.BAL777 =''
    CLOSE.BAL8 ='' ; CLOSE.BAL88 = '' ; CLOSE.BAL888 =''
    CLOSE.BAL9 ='' ; CLOSE.BAL99 = '' ; CLOSE.BAL999 =''
    CLOSE.BAL10 ='' ; CLOSE.BAL110 = '' ; CLOSE.BAL1110 =''
*-------------------------------------------------------------------------
    TOT.CLOSE.BAL1 = ''
    TOT.CLOSE.BAL2 = ''
    TOT.CLOSE.BAL3 = ''
    TOT.CLOSE.BAL4 = ''
    TOT.CLOSE.BAL5 = ''
    TOT.CLOSE.BAL6 = ''
    TOT.CLOSE.BAL7 = ''
    TOT.CLOSE.BAL8 = ''
    TOT.CLOSE.BAL9 = ''
    TOT.CLOSE.BAL10 = ''
*--------------------------------------------------------------------------

*   YTEXT = "ENTER DATE. : "
*  CALL TXTINP(YTEXT, 8, 22, "8", "A")

    DAT = TODAY
    CALL CDT("",DAT,'-1C')


    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBM.BAL2.BATCH'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY

    RETURN
*========================================================================
PROCESS:
    FN.LINE = 'F.RE.STAT.LINE.BAL' ; F.LINE = ''
    CALL OPF(FN.LINE,F.LINE)

    IDD1 = 'BAL-0010':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
    CLOSE.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL11  = CLOSE.BAL1/1000
    CLOSE.BAL111 = FMT(CLOSE.BAL11,"L0,")
*********************************************************************************************************
    IDD2 = 'BAL-0020':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD2,R.LINE,F.LINE,E)
    CLOSE.BAL2  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL22  = CLOSE.BAL2/1000
    CLOSE.BAL222 = FMT(CLOSE.BAL22,"L0,")

*********************************************************************************************************
    IDD3 = 'BAL-0030':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD3,R.LINE,F.LINE,E)
    CLOSE.BAL3  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL33 = CLOSE.BAL3/1000
    CLOSE.BAL333 = FMT(CLOSE.BAL33,"L0,")
*********************************************************************************************************
    IDD4 = 'BAL-0040':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD4,R.LINE,F.LINE,E)
    CLOSE.BAL4  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL44  = CLOSE.BAL4/1000
    CLOSE.BAL444 = FMT(CLOSE.BAL44,"L0,")
*********************************************************************************************************
    IDD5 = 'BAL-0050':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD5,R.LINE,F.LINE,E)
    CLOSE.BAL5  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL55  = CLOSE.BAL5/1000
    CLOSE.BAL555 = FMT(CLOSE.BAL55,"L0,")
*********************************************************************************************************
    IDD6 = 'BAL-0060':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD6,R.LINE,F.LINE,E)
    CLOSE.BAL6  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL66  = CLOSE.BAL6/1000
    CLOSE.BAL666 = FMT(CLOSE.BAL66,"L0,")
*********************************************************************************************************
    IDD7 = 'BAL-0070':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD7,R.LINE,F.LINE,E)
    CLOSE.BAL7  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL77  = CLOSE.BAL7/1000
    CLOSE.BAL777 = FMT(CLOSE.BAL77,"L0,")
*********************************************************************************************************
    IDD8 = 'BAL-0080':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD8,R.LINE,F.LINE,E)
    CLOSE.BAL8  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL88  = CLOSE.BAL8/1000
    CLOSE.BAL888 = FMT(CLOSE.BAL88,"L0,")
*********************************************************************************************************
    IDD9 = 'BAL-0090':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD9,R.LINE,F.LINE,E)
    CLOSE.BAL9  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL99  = CLOSE.BAL9/1000
    CLOSE.BAL999 = FMT(CLOSE.BAL99,"L0,")
*********************************************************************************************************
    IDD10 = 'BAL-0100':'-':'EGP':'-':DAT:'*':COMP
    CALL F.READ(FN.LINE,IDD10,R.LINE,F.LINE,E)
    CLOSE.BAL10  = R.LINE<RE.SLB.CLOSING.BAL>
    CLOSE.BAL110  = CLOSE.BAL10/1000
    CLOSE.BAL1110 = FMT(CLOSE.BAL110,"L0,")

*********************************************************************************************************



    TOT.CLOSE.BAL111 = CLOSE.BAL1 + CLOSE.BAL2
    TOT.CLOSE.BAL11 =  TOT.CLOSE.BAL111/1000
    TOT.CLOSE.BAL1 =  FMT(TOT.CLOSE.BAL11,"L0,")

    TOT.CLOSE.BAL222 = CLOSE.BAL3 + CLOSE.BAL4
    TOT.CLOSE.BAL22 =  TOT.CLOSE.BAL222/1000
    TOT.CLOSE.BAL2 =  FMT(TOT.CLOSE.BAL22,"L0,")

    TOT.CLOSE.BAL333 = CLOSE.BAL5 + CLOSE.BAL6
    TOT.CLOSE.BAL33 =  TOT.CLOSE.BAL333/1000
    TOT.CLOSE.BAL3 =  FMT(TOT.CLOSE.BAL33,"L0,")

    TOT.CLOSE.BAL444 = CLOSE.BAL7 + CLOSE.BAL8
    TOT.CLOSE.BAL44 =  TOT.CLOSE.BAL444/1000
    TOT.CLOSE.BAL4 =  FMT(TOT.CLOSE.BAL44,"L0,")

    TOT.CLOSE.BAL555 = CLOSE.BAL9 + CLOSE.BAL10
    TOT.CLOSE.BAL55 =  TOT.CLOSE.BAL555/1000
    TOT.CLOSE.BAL5 =  FMT(TOT.CLOSE.BAL55,"L0,")

    TOT.CLOSE.BAL666 = CLOSE.BAL1 + CLOSE.BAL3 + CLOSE.BAL5 + CLOSE.BAL7 + CLOSE.BAL9
    TOT.CLOSE.BAL66 =  TOT.CLOSE.BAL666/1000
    TOT.CLOSE.BAL6 =  FMT(TOT.CLOSE.BAL66,"L0,")

    TOT.CLOSE.BAL777 = CLOSE.BAL2 + CLOSE.BAL4 + CLOSE.BAL6 + CLOSE.BAL8 + CLOSE.BAL10
    TOT.CLOSE.BAL77 = TOT.CLOSE.BAL777/1000
    TOT.CLOSE.BAL7 = FMT(TOT.CLOSE.BAL77,"L0,")

    TOT.CLOSE.BAL888 = TOT.CLOSE.BAL777 + TOT.CLOSE.BAL666
    TOT.CLOSE.BAL88 =  TOT.CLOSE.BAL888/1000
    TOT.CLOSE.BAL8 = FMT(TOT.CLOSE.BAL88,"L0,")


    XX= SPACE(120)
    XX<1,1>[1,20]   = "����� ����"
    XX<1,1>[30,20]  = CLOSE.BAL111
    XX<1,1>[60,20]  = CLOSE.BAL222
    XX<1,1>[90,20]  = TOT.CLOSE.BAL1
    PRINT XX<1,1>
    PRINT STR('-',120)

    XX2= SPACE(120)
    XX2<1,1>[1,20]   = "������"
    XX2<1,1>[30,20]  = CLOSE.BAL333
    XX2<1,1>[60,20]  = CLOSE.BAL444
    XX2<1,1>[90,20]  = TOT.CLOSE.BAL2
    PRINT XX2<1,1>
    PRINT STR('-',120)


    XX3= SPACE(120)
    XX3<1,1>[1,20]   = "�����"
    XX3<1,1>[30,20]  = CLOSE.BAL555
    XX3<1,1>[60,20]  = CLOSE.BAL666
    XX3<1,1>[90,20]  = TOT.CLOSE.BAL3
    PRINT XX3<1,1>
    PRINT STR('-',120)

    XX4= SPACE(120)
    XX4<1,1>[1,20]   = "����"
    XX4<1,1>[30,20]  = CLOSE.BAL777
    XX4<1,1>[60,20]  = CLOSE.BAL888
    XX4<1,1>[90,20]  = TOT.CLOSE.BAL4
    PRINT XX4<1,1>
    PRINT STR('-',120)

    XX5= SPACE(120)
    XX5<1,1>[1,20]   = "����� ����"
    XX5<1,1>[30,20]  = CLOSE.BAL999
    XX5<1,1>[60,20]  = CLOSE.BAL1110
    XX5<1,1>[90,20]  = TOT.CLOSE.BAL5
    PRINT XX5<1,1>


    PRINT STR(' ',120)
    PRINT STR(' ',120)
    PRINT STR('=',120)

    XX6= SPACE(120)
    XX6<1,1>[1,20]   = "������"
    XX6<1,1>[30,20]  =  TOT.CLOSE.BAL6
    XX6<1,1>[60,20] =  TOT.CLOSE.BAL7
    XX6<1,1>[90,20] =  TOT.CLOSE.BAL8
    PRINT XX6<1,1>
    PRINT STR('-',120)

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY   = DAT
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    TDAT   = TODAY
    TT     =TDAT[7,2]:'/':TDAT[5,2]:"/":TDAT[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":TT:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"������ ������� ��������� ������� ������"
    PR.HD :="'L'":SPACE(50):"�� ����� : ":TT
    PR.HD :="'L'":SPACE(50):STR('_',40):"���� ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(30):"����� �������� " : SPACE (15):"�����" : SPACE (20) :"������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
