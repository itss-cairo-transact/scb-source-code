* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM  SBD.BASIC.RATES.FILE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.RATE.TEXT


    GOSUB INITIATE
*Line [ 31 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*****
INITIATE:
*--------
    PATHNAME1 = "/home/signat"
    FILENAME1 = "CD.BASIC.INT.TXT"

    OPENSEQ PATHNAME1 , FILENAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATHNAME1:' ':FILENAME1
        HUSH OFF
    END
    OPENSEQ PATHNAME1 , FILENAME1 TO BB ELSE
        CREATE BB THEN
            CRT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END
    CODE.GRP = "87EGP.88EGP.89EGP.83USD."

    RETURN
*****
CALLDB:
*-------
    FN.INT  = 'FBNK.BASIC.INTEREST' ; F.INT = '' ; R.INT = ''
    CALL OPF( FN.INT,F.INT)

    FN.TXT  = 'F.BASIC.RATE.TEXT' ; F.TXT = '' ; R.TXT = ''
    CALL OPF( FN.TXT,F.TXT)
    RETURN
*---------------------------
PROCESS:
*-------
    COD.NUM = COUNT(CODE.GRP,'.')

    FOR CC = 1 TO COD.NUM
        INT.ID.LK = FIELD(CODE.GRP,'.',CC):"..."
        T.SEL = "SSELECT ":FN.INT:" LIKE ":INT.ID.LK
        CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
        IF SELECTED THEN
            INT.ID = KEY.LIST<SELECTED>
            CALL F.READ(FN.INT,INT.ID, R.INT, F.INT, ETEXT1)
            INT.RATE = R.INT<EB.BIN.INTEREST.RATE>
            IF INT.ID[1,5] EQ '83USD' THEN
                INT.RATE = INT.RATE + 1.75
            END
            TXT.ID = INT.ID[1,2]
            CALL F.READ(FN.TXT,TXT.ID,R.TXT,F.TXT,ER.TXT)
            DESC.1 = R.TXT<EB.BRT.DESCRIPTION,1>
            DESC.2 = R.TXT<EB.BRT.DESCRIPTION,2>
            GOSUB INS.REC
        END
    NEXT CC
    CLOSESEQ BB
    RETURN
***************
INS.REC:
*-------
    BB.DATA  = INT.RATE:'|'
    BB.DATA := DESC.1:'|'
    BB.DATA := DESC.2

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
****************************************************
END
