* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*--- CREATE BY NESSMA ON 2011/12/18 ----
    SUBROUTINE SBM.AC.TOTALS.CUS.ALL.RNG.TOT
*---------------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TOPCUS.CR.TOT.LW
*---------------------------------------------------------------------
    YTEXT      = "Enter the Start Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    START.DATE =  COMI

    YTEXT      = "Enter the End Date : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    END.DATE   =  COMI

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS.OPEN
    GOSUB PRINT.ARRAY.OPEN
    GOSUB END.LINE.OPEN
    GOSUB PROCESS.CLOSE
    GOSUB PRINT.HEAD.2
    GOSUB PRINT.ARRAY.CLOSE
    GOSUB END.LINE.CLOSE
    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    TEXT = "REPORT DONE" ;  CALL REM
    RETURN
*----------------------------- INITIALIZATIONS ------------------------
PRINT.ARRAY.OPEN:
*-----------
    PRINT SPACE(30) :  "���� : ������� ���������"
    XX.HD =SPACE(3):"�����"
    XX.HD :=SPACE(22) : "������ �������"

    XX.HD :=SPACE(13): "�����"
    XX.HD :=SPACE(5): "�����"
    XX.HD :=SPACE(5):"��������"
    PRINT XX.HD
    PRINT STR('-',100)
    PRINT " "
    PRINT " "

    FOR ROW.ARR = 1 TO ZZ
        PRINT ARRAY.LINE<1,ROW.ARR>
        PRINT " "
    NEXT ROW.ARR
    RETURN
*--------------------------------------------------------------
PRINT.ARRAY.CLOSE:
*-----------------
    ROW.APR = ""  ; XX.HD = ""

    PRINT SPACE(30) : "����� : ������� �������� "
    XX.HD = SPACE(3):"�����"
    XX.HD :=SPACE(50): "�����"
    XX.HD :=SPACE(5): "�����"
    XX.HD :=SPACE(5):"��������"
    PRINT XX.HD
    PRINT STR('-',100)
    PRINT " "
    PRINT " "

    FOR ROW.ARR = 1 TO HH
        PRINT ARRAY.LINE.2<1,ROW.ARR>
        PRINT " "
    NEXT ROW.ARR
    RETURN
*---------------------------------------------------------------------
END.LINE.OPEN:
*-------------
    PRINT " "
    TOT.M = ""
    TOT.M = TOT.4 + TOT.5
    TOT.M = FMT(TOT.M, "L2,")
    TOT.4 = FMT(TOT.4, "L2,")
    TOT.5 = FMT(TOT.5, "L2,")

    LINE.HD = "�������� :"
    LINE.HD := SPACE(20) :  TOT.M : SPACE(13)
    LINE.HD := TOT.1    : SPACE(9) : TOT.2  : SPACE(10)
    LINE.HD := TOT.3


    PRINT LINE.HD
    RETURN
*====================================
END.LINE.CLOSE:
*--------------
    PRINT " "

    LINE.HD = SPACE(5): "�������� :"
    LINE.HD := SPACE(50) : TOT.6     : SPACE(10)
    LINE.HD := TOT.7     : SPACE(8)  : TOT.8

    PRINT LINE.HD
    RETURN
*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
LINE.END:
*--------
    WW = SPACE(100)
    PRINT " "
    PRINT STR('_',100)
    PRINT " "
    XX.LINE= "**��� ������� ��� �� �� ����� ����� ��� ������ **"
    XX.LINE = "** ��� ������� �� ����� �������� �������� ��� �� �� ����� ����� ��� ������"
    XX.DD = FMT(VAR.TOD, "####/##/##")
    PRINT XX.LINE :SPACE(2): XX.DD

    PRINT " "
    PRINT " "
    WW<1,1> = SPACE(40):"********************  ����� �������   ******************"
    PRINT WW<1,1>
    RETURN
*--------------------------------------------------------------------
INITIATE:
*--------

    TOT.1 = 0
    TOT.2 = 0
    TOT.3 = 0
    TOT.4 = 0
    TOT.5 = 0
    TOT.6 = 0
    TOT.7 = 0
    TOT.8 = 0

    ZZ = 1
    HH = 1
    ALL.BAL.NEW     = 0
    ALL.BAL.NEW.FCY = 0
    ARRAY.LINE      = SPACE(150)
    ARRAY.LINE.2    = SPACE(150)

    REPORT.ID = 'SBM.AC.TOTALS'
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CBE    = "F.SCB.TOPCUS.CR.TOT.LW"    ; F.CBE   = ""
    CALL OPF(FN.CBE,F.CBE)

    FN.CUS    = 'FBNK.CUSTOMER'             ; F.CUS   = ""
    CALL OPF( FN.CUS,F.CUS)

    FN.CUS.H  = 'FBNK.CUSTOMER$HIS'         ; F.CUS.H = ""
    CALL OPF(FN.CUS.H,F.CUS.H)

    EOF        = ''
    ETEXT      = ''
    ETEXT1     = ''
    T.DATE     = TODAY
    KEY.CCY    = 'NZD'
    DDD        = TODAY[1,6]:'01'
    VAR.TOD    = TODAY

    HEAD.D1    = START.DATE
    HEAD.D2    = END.DATE

    COMP.COUNT.2 = 0
    INDV.COUNT.2 = 0
    INDV.COUNT   = 0
    COMP.COUNT   = 0

    RETURN
*------------------------ READ FORM TEXT FILE --------------------
PROCESS.OPEN:
*------------
    T.SEL  = " SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":START.DATE
    T.SEL := " AND CONTACT.DATE LE ":END.DATE
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
*    T.SEL := " AND COMPANY.BOOK NE 'EG0010099'"
    T.SEL := " AND TEXT UNLIKE BR... BY COMPANY.BOOK"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    OLD.CO.CODE       = ''
    INDV.COUNT        = 0
    COMP.COUNT        = 0

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
        CALL F.READ(FN.CUS,KEY.LIST<I+1>,R.CUS.1,F.CUS,CUS.ERR)

        COM.CODE    = R.CUS<EB.CUS.COMPANY.BOOK>
        COM.CODE.1  = R.CUS.1<EB.CUS.COMPANY.BOOK>

        IF COM.CODE EQ COM.CODE.1 THEN
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR    = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>

            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END
        END ELSE
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR    = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END

            ALL.COUNT = COMP.COUNT + INDV.COUNT

            OLDD      = COM.CODE[8,2]
            IF OLDD[1,1] EQ '0' THEN
                OLDD  = OLDD[1]
            END

            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
            BRANCH.NAME  = FIELD(BRAN.NAME,'.',2)
*--------------------------
            CBE.BR = COM.CODE[8,2]
*--------------------------
            GOSUB WRITE.LINE

            COMP.COUNT   = 0
            INDV.COUNT   = 0
        END
    NEXT I
    RETURN
*----------------------------------------------------------------------
PROCESS.CLOSE:
*-------------
    T.SEL    = ""
    SELECTED = 0
    KEY.LIST = ""
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT GE 90 )"
    T.SEL := " AND ( SECTOR NE 5010 AND SECTOR NE 5020 ) AND CURR.NO GT 1"
*   T.SEL := " AND COMPANY.BOOK NE 'EG0010099'"
    T.SEL := " AND WITHOUT TEXT LIKE BR... BY COMPANY.BOOK"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            R.CUS   = ""
            R.CUS.1 = ""
            CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERR)
            CALL F.READ(FN.CUS,KEY.LIST<II+1>,R.CUS.1,F.CUS,CUS.ERR.1)

            COM.CODE    = R.CUS<EB.CUS.COMPANY.BOOK>
            COM.CODE.1  = R.CUS.1<EB.CUS.COMPANY.BOOK>

            IF COM.CODE EQ COM.CODE.1 THEN
                CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERRRR)
                CUS.ID  = KEY.LIST<II>
                CUR.NO  = R.CUS<EB.CUS.CURR.NO>
                RR = 1
                MM = CUR.NO - 1

                FOR NN = CUR.NO TO 1 STEP -1
                    IF RR EQ 1 THEN
                        CUST.ID      = CUS.ID:";"
                        CUST.ID.NXT  = CUS.ID:";":MM
                    END ELSE
                        CUST.ID      = CUS.ID:";":MM
                        MM           = MM -1
                        CUST.ID.NXT  = CUS.ID:";":MM
                    END
                    IF RR EQ 1 THEN
                        RR = 2
                        CALL F.READ(FN.CUS,CUS.ID,R.CUS.N,F.CUS,CUS.ER44)
                        CUS.DAT.2 = R.CUS.N<EB.CUS.DATE.TIME><1,1>[1,6]
                        CUS.POST  = R.CUS.N<EB.CUS.POSTING.RESTRICT>
                    END ELSE
                        CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                        CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,6]
                        CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                    END

                    CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                    CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                    IF CUS.POST NE CUS.POST.NXT THEN
                        IF CUS.POST GE 90 THEN
                            IF CUS.DAT.2 GE START.DATE[3,6] AND CUS.DAT.2 LE END.DATE[3,6] THEN

                                NEW.SECTOR  = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
                                IF NEW.SECTOR NE '' THEN
                                    IF NEW.SECTOR NE '4650' THEN
                                        COMP.COUNT.2 = COMP.COUNT.2 + 1
                                    END ELSE
                                        INDV.COUNT.2 = INDV.COUNT.2 + 1
                                    END
                                END
                                NN       = 1
                            END
                        END
                    END
                NEXT NN

            END ELSE

                CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERRRR)
                CUS.ID  = KEY.LIST<II>
                CUR.NO  = R.CUS<EB.CUS.CURR.NO>
                RR = 1
                MM = CUR.NO - 1

                FOR NN = CUR.NO TO 1 STEP -1
                    IF RR EQ 1 THEN
                        CUST.ID      = CUS.ID:";"
                        CUST.ID.NXT  = CUS.ID:";":MM
                    END ELSE
                        CUST.ID      = CUS.ID:";":MM
                        MM           = MM -1
                        CUST.ID.NXT  = CUS.ID:";":MM
                    END
                    IF RR EQ 1 THEN
                        RR = 2
                        CALL F.READ(FN.CUS,CUS.ID,R.CUS.N,F.CUS,CUS.ER44)
                        CUS.DAT.2 = R.CUS.N<EB.CUS.DATE.TIME><1,1>[1,6]
                        CUS.POST  = R.CUS.N<EB.CUS.POSTING.RESTRICT>
                    END ELSE
                        CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                        CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,6]
                        CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                    END

                    CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                    CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                    IF CUS.POST NE CUS.POST.NXT THEN
                        IF CUS.POST GE 90 THEN
                            IF CUS.DAT.2 GE START.DATE[3,6] AND CUS.DAT.2 LE END.DATE[3,6] THEN
                                NEW.SECTOR  = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
                                IF NEW.SECTOR NE '' THEN
                                    IF NEW.SECTOR NE '4650' THEN
                                        COMP.COUNT.2 = COMP.COUNT.2 + 1
                                    END ELSE
                                        INDV.COUNT.2 = INDV.COUNT.2 + 1
                                    END
                                END
                                NN       = 1
                            END
                        END
                    END
                NEXT NN

                ALL.COUNT.2 = COMP.COUNT.2 + INDV.COUNT.2
                OLDD        = COM.CODE[8,2]
                OLDD        = OLDD + 0

                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
                BRANCH.NAME.2  = FIELD(BRAN.NAME,'.',2)

                GOSUB WRITE.LINE.2

                COMP.COUNT.2 = 0
                INDV.COUNT.2 = 0
            END
        NEXT II
    END
    RETURN
*-------------------------------------------------------------------*
WRITE.LINE:
*----------
    ARRAY.LINE<1,ZZ>[3,15]   = BRANCH.NAME

    TOTT.MM = ALL.BAL.NEW + ALL.BAL.NEW.FCY
    TOTT.MM = FMT(TOTT.MM, "L2,")
    ARRAY.LINE<1,ZZ>[30,30]  = TOTT.MM

    ARRAY.LINE<1,ZZ>[60,10]  = INDV.COUNT
    ARRAY.LINE<1,ZZ>[72,10]  = COMP.COUNT
    ARRAY.LINE<1,ZZ>[82,10]  = ALL.COUNT

    ZZ = ZZ + 1
*%%%%%%% TOTALS %%%%%%%%%%%%%%%
    TOT.1 = TOT.1 + INDV.COUNT
    TOT.2 = TOT.2 + COMP.COUNT
    TOT.3 = TOT.3 + ALL.COUNT
    TOT.4 = TOT.4 + ALL.BAL.NEW
    TOT.5 = TOT.5 + ALL.BAL.NEW.FCY
*%%%%%%% TOTALS %%%%%%%%%%%%%%%
    ALL.BAL.NEW     = 0
    ALL.BAL.NEW.FCY = 0
    RETURN
*------------------------------------------------------------------*
CALC.BALANCE:
    CUST.NUM = CUSTO
    CUSTO    = CUST.NUM : ".LCY"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000
    ALL.BAL.NEW = ALL.BAL.NEW +  CBE.IN.LCY

    CUSTO  = CUST.NUM : ".FCY"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY.FCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000

    ALL.BAL.NEW.FCY = ALL.BAL.NEW.FCY +  CBE.IN.LCY.FCY
    RETURN
*----------------------------------------------------
WRITE.LINE.2:
*------------
    ARRAY.LINE.2<1,HH>[3,15]   = BRANCH.NAME.2
    ARRAY.LINE.2<1,HH>[60,10]  = INDV.COUNT.2
    ARRAY.LINE.2<1,HH>[72,10]  = COMP.COUNT.2
    ARRAY.LINE.2<1,HH>[82,10]  = ALL.COUNT.2

*    TEXT = BRANCH.NAME.2:" ** ":ALL.COUNT.2  ; CALL REM

*    ARRAY.LINE.2<1,HH>[105,10]  = INDV.COUNT.2
*    ARRAY.LINE.2<1,HH>[115,10]  = COMP.COUNT.2
*    ARRAY.LINE.2<1,HH>[125,10]  = ALL.COUNT.2

    HH = HH + 1
*%%%%%%%%%%% TOTALS %%%%%%%%%%%%
    TOT.6 = TOT.6 + INDV.COUNT.2
    TOT.7 = TOT.7 + COMP.COUNT.2
    TOT.8 = TOT.8 + ALL.COUNT.2
*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    COMP.COUNT.2   = 0
    INDV.COUNT.2   = 0
    ALL.COUNT.2    = 0
    BRANCH.NAME.2  = ""

    RETURN
************************ PRINT HEAD *******************************
PRINT.HEAD:
*----------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = VAR.TOD
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"����� �������"
    PR.HD :=T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(109):"SBM.AC.TOTALS.CUS.ALL.RNG.TOT"
    PR.HD :="'L'":" "

    FROM.DATE.2  = FMT(HEAD.D1, "####/##/##")
    END.DATE.2   = FMT(HEAD.D2 , "####/##/##")

    PR.HD :="'L'":SPACE(40): "������� �������� � ������� ���� ����"
    PR.HD :="'L'":SPACE(40):" ��":SPACE(5):FROM.DATE.2 : "���" : SPACE(3):END.DATE.2

    PR.HD :="'L'" :SPACE(30):STR('_',60)
    PR.HD :="'L'" :" "
    PR.HD :="'L'":SPACE(60)
*    PR.HD :="������� ��������"
*    PR.HD :=SPACE(30)
*    PR.HD :="������� �������"

*    PR.HD :="'L'":SPACE(3):"�����"
*    PR.HD :=SPACE(22) : "������ �������"

*    PR.HD :=SPACE(13): "�����"
*    PR.HD :=SPACE(5): "�����"
*    PR.HD :=SPACE(5):"��������"

*    PR.HD :=SPACE(15): "�����"
*    PR.HD :=SPACE(5): "�����"
*    PR.HD :=SPACE(5):"��������"

*    PR.HD :="'L'":STR('_',100)
    PRINT
    HEADING PR.HD
    RETURN
PRINT.HEAD.2:
*----------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = VAR.TOD
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"����� �������"
    PR.HD :=T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(109):"SBM.AC.TOTALS.CUS.ALL.RNG.TOT"
    PR.HD :="'L'":" "

    FROM.DATE.2  = FMT(HEAD.D1, "####/##/##")
    END.DATE.2   = FMT(HEAD.D2 , "####/##/##")

    PR.HD :="'L'":SPACE(40): "������� �������� � ������� ���� ����"
    PR.HD :="'L'":SPACE(40):" ��":SPACE(5):FROM.DATE.2 : "���" : SPACE(3):END.DATE.2

    PR.HD :="'L'" :SPACE(30):STR('_',60)
    PR.HD :="'L'" :" "
    PR.HD :="'L'":SPACE(60)
*    PR.HD :="������� ��������"
*    PR.HD :=SPACE(30)
*    PR.HD :="������� �������"

*    PR.HD :="'L'":SPACE(3):"�����"
*    PR.HD :=SPACE(22) : "������ �������"

*    PR.HD :=SPACE(13): "�����"
*    PR.HD :=SPACE(5): "�����"
*    PR.HD :=SPACE(5):"��������"

*    PR.HD :=SPACE(15): "�����"
*    PR.HD :=SPACE(5): "�����"
*    PR.HD :=SPACE(5):"��������"

*    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
    RETURN
END
