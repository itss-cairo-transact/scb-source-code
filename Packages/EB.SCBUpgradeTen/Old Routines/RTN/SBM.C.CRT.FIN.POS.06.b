* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
****MOHSEN
    PROGRAM SBM.C.CRT.FIN.POS.06
    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.RE.STAT.LINE.BAL
    $INSERT TEMENOS.BP I_F.CBE.BANK.FIN.POS
    $INSERT TEMENOS.BP I_F.SCB.CBE.CY.RATE
    $INSERT T24.BP  I_F.DATES
*---------------------------------------------------
    FN.FIN = "F.CBE.BANK.FIN.POS"
    F.FIN  = ""

    FN.LINE = "FBNK.RE.STAT.LINE.BAL"
    F.LINE  = ""

    FN.CCY = "F.SCB.CBE.CY.RATE"
    F.CCY  = ""

    FN.CY = "FBNK.CURRENCY"
    F.CY  = ""

    FN.REP.LINE = "F.RE.STAT.REP.LINE"
    F.REP.LINE  = ""

    FN.COMPANY = "F.COMPANY"
    F.COMPANY  = ""

    CALL OPF (FN.REP.LINE,F.REP.LINE)
    CALL OPF (FN.COMPANY,F.COMPANY)
    CALL OPF (FN.CY,F.CY)

    FN.DATE = "F.DATES"
    F.DATE  = ""

*-----------------------------------------------------
    WS.FILE.AMT = 0
    WS.AMT.LCY = 0

    WS.AMT.FCY = 0
    WS.FIN.AMT.LCY =  0
    WS.FIN.AMT.FCY =  0
    WS.BR = 0
    WS.AMT = 0
    WS.CY = 0
*-----------------------------------------------------------

    DIM ARRAY1(17,4)
*                                        ���� ���� ��� ������ �������
    ARRAY1(1,1) = "2060500"
    ARRAY1(1,2) = "0616"
    ARRAY1(1,3) = "GENLED-"
    ARRAY1(1,4) = "LCY"

    ARRAY1(2,1) = "2060500"
    ARRAY1(2,2) = "0816"
    ARRAY1(2,3) = "GENLED-"
    ARRAY1(2,4) = "FCY"
*                                         ����� �� ������ �������
    ARRAY1(3,1) = "1150100"
    ARRAY1(3,2) = "0111"
    ARRAY1(3,3) = "GENLED-"
    ARRAY1(3,4) = "LCY"

    ARRAY1(4,1) = "1150100"
    ARRAY1(4,2) = "0311"
    ARRAY1(4,3) = "GENLED-"
    ARRAY1(4,4) = "FCY"

    ARRAY1(5,1) = "2060100"
    ARRAY1(5,2) = "0620"
    ARRAY1(5,3) = "GENLED-"
    ARRAY1(5,4) = "LCY"

    ARRAY1(6,1) = "2060100"
    ARRAY1(6,2) = "0820"
    ARRAY1(6,3) = "GENLED-"
    ARRAY1(6,4) = "FCY"
*                                                     ���������
    ARRAY1(7,1) = "1150200"
    ARRAY1(7,2) = "0120"
    ARRAY1(7,3) = "GENLED-"
    ARRAY1(7,4) = "LOCAL"

*?    ARRAY1(8,1) = "1150200"
*???    ARRAY1(8,2) = "0320"
*?    ARRAY1(8,2) = "0120"
*?    ARRAY1(8,3) = "GENLED-"
*?    ARRAY1(8,4) = "FCY"
    ARRAY1(8,1) = "0"
    ARRAY1(8,2) = "0"
    ARRAY1(8,3) = ""
    ARRAY1(8,4) = ""
*                                                      ���������
    ARRAY1(9,1) = "2060200"
    ARRAY1(9,2) = "0630"
    ARRAY1(9,3) = "GENLED-"
    ARRAY1(9,4) = "LOCAL"

*?    ARRAY1(10,1) = "2060200"
*???    ARRAY1(10,2) = "0840"
*?    ARRAY1(10,2) = "0630"
*?    ARRAY1(10,3) = "GENLED-"
*?    ARRAY1(10,4) = "FCY"
    ARRAY1(10,1) = "0"
    ARRAY1(10,2) = "0"
    ARRAY1(10,3) = ""
    ARRAY1(10,4) = ""
*                                              ������� ����� ������
    ARRAY1(11,1) = "2060600"
    ARRAY1(11,2) = "0610"
    ARRAY1(11,3) = "GENLED-"
    ARRAY1(11,4) = "LCY"

    ARRAY1(12,1) = "2060600"
    ARRAY1(12,2) = "0810"
    ARRAY1(12,3) = "GENLED-"
    ARRAY1(12,4) = "FCY"

*                                                ����� ����� ����
    ARRAY1(13,1) = "2061000"
    ARRAY1(13,2) = "0600"
    ARRAY1(13,3) = "GENLED-"
    ARRAY1(13,4) = "LCY"

    ARRAY1(14,1) = "2061000"
    ARRAY1(14,2) = "0800"
    ARRAY1(14,3) = "GENLED-"
    ARRAY1(14,4) = "FCY"

    ARRAY1(15,1) = "1150500"
    ARRAY1(15,2) = "0090"
    ARRAY1(15,3) = "GENLED-"
    ARRAY1(15,4) = "LCY"

    ARRAY1(16,1) = "1150500"
    ARRAY1(16,2) = "0290"
    ARRAY1(16,3) = "GENLED-"
    ARRAY1(16,4) = "FCY"

    ARRAY1(17,1) = "2061000"
    ARRAY1(17,2) = "0635"
    ARRAY1(17,3) = "GENLED-"
    ARRAY1(17,4) = "LCY"
***********************************************************

*                                             LAST
*------------------------------------------------
    R.FIN = ""
    R.DATE = ""
    MSG.DATE = ""
*----------------------------PROCEDURE---------------------
    CALL OPF (FN.FIN,F.FIN)
    CALL OPF (FN.LINE,F.LINE)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.DATE,F.DATE)
*------------------------PROCEDURE------------------------
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
*WS.TODAY = R.DATE<EB.DAT.LAST.WORKING.DAY>
    WS.TODAY = R.DATE<EB.DAT.LAST.PERIOD.END>
*    WS.TODAY  = TODAY
*    WS.TODAY  = 20100131
    CRT WS.TODAY
    GOSUB A.100.ARRAY1

    RETURN
*------------------------------------------------
A.100.ARRAY1:

*** UPDATED BY MOHAMED SABRY 2012/10/16

    SEL.COMPANY = "SELECT ":FN.COMPANY:" BY @ID"
    CALL EB.READLIST(SEL.COMPANY,KEY.COMP,"",SEL.COMP,ER.COMP)

    SEL.CURRENCY = "SELECT ":FN.CY:" BY @ID"
    CALL EB.READLIST(SEL.CURRENCY,KEY.CY,"",SEL.CY,ER.CY)
    KEY.CY<SEL.CY+1> = "LOCAL"
    SEL.CY           = SEL.CY+1

*** END OF UPDATE BY MOHAMED SABRY 2012/10/16

    FOR AR1 = 1 TO 17
        WS.FIN.ID  =  ARRAY1(AR1,1)
        WS.LINE.NO =  ARRAY1(AR1,2)
        WS.REPORT  =  ARRAY1(AR1,3)
        WS.LCY.FCY =  ARRAY1(AR1,4)
        WS.AMT.LCY =  0
        WS.AMT.FCY =  0
        GOSUB A.110.GET.LINE.KEY
    NEXT AR1
    RETURN
*------------------------------------------------
*** UPDATED BY MOHAMED SABRY 2012/10/16
*------------------------------------------------
A.110.GET.LINE.KEY:
    IF SEL.COMP THEN
        FOR ICOMP = 1 TO SEL.COMP
            IF SEL.CY THEN
                FOR ICY = 1 TO SEL.CY
                    WS.LINE.ID   = WS.REPORT:WS.LINE.NO:'-':KEY.CY<ICY>:'-':WS.TODAY:'*':KEY.COMP<ICOMP>
                    CALL F.READ(FN.LINE,WS.LINE.ID,R.LINE,F.LINE,MSG.LINE)
                    IF NOT (MSG.LINE) THEN
                        GOSUB A.150.READ.LINE.BAL
                    END
                NEXT ICY
            END
        NEXT ICOMP
    END
    RETURN
*------------------------------------------------
*------------------------------------------------
A.150.READ.LINE.BAL:
*    SEL.CMD = "SELECT ":FN.LINE:" WITH @ID LIKE ":WS.REPORT:WS.LINE.NO:"... AND @ID LIKE ...":WS.TODAY:"..."
*    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
*    LOOP
*        REMOVE WS.LINE.ID FROM SEL.LIST SETTING POS
*    WHILE WS.LINE.ID:POS

    CALL F.READ(FN.LINE,WS.LINE.ID,R.LINE,F.LINE,MSG.LINE)

    PRINT WS.LINE.ID
    IF MSG.LINE THEN PRINT MSG.LINE

    WS.COMN.ID = WS.LINE.ID
    WS.BR = WS.COMN.ID[2]
    IF WS.BR LT 10 THEN
        WS.BR = WS.COMN.ID[1]
    END
    WS.COMN.LOCAL = FIELD(WS.COMN.ID,"-", 3)
    IF WS.FIN.ID EQ 0 THEN
        GOTO A.150.1
    END
    IF WS.LCY.FCY EQ "LOCAL" AND WS.COMN.LOCAL NE "LOCAL" THEN
        GOTO A.150.1
    END

    IF WS.LCY.FCY EQ "LOCAL" AND  WS.COMN.LOCAL EQ "LOCAL" THEN
        WS.AMT.LCY = R.LINE<RE.SLB.CLOSING.BAL.LCL>
*CRT "A.................":WS.AMT.LCY:"  ":WS.COMN.LOCAL:" ":NO.OF.REC
        WS.AMT.FCY = 0
        GOSUB A.500.UPDAT
        GOTO A.150.1
    END

    IF WS.COMN.LOCAL EQ "LOCAL"  THEN
        GOTO A.150.1
    END

    WS.COMN.CY =    FIELD(WS.COMN.ID,"-", 3)

    IF WS.LCY.FCY EQ "LOCAL" AND WS.COMN.CY NE "LOCAL" THEN
        GOTO A.150.1
    END

    IF WS.LCY.FCY EQ "LCY" AND WS.COMN.CY NE "EGP" THEN
        GOTO A.150.1
    END
    IF WS.LCY.FCY EQ "FCY" AND WS.COMN.CY EQ "EGP" THEN
        GOTO A.150.1
    END
    IF WS.COMN.CY EQ "EGP" THEN
        WS.AMT.LCY = R.LINE<RE.SLB.CLOSING.BAL>
        WS.AMT.FCY = 0
        GOSUB A.500.UPDAT
        GOTO A.150.1
    END
    IF WS.COMN.CY NE "EGP" THEN
        WS.AMT.LCY = 0
        WS.AMT.FCY = R.LINE<RE.SLB.CLOSING.BAL>
        GOSUB A.200.CALC.FCY
        GOSUB A.500.UPDAT
    END

A.150.1:
*REPEAT
    RETURN
*------------------------------------------------
A.200.CALC.FCY:
*    WS.KEYCY = "NZD"
    WS.KEYCY = WS.COMN.CY
*    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    IF MSG.CY EQ "" THEN
        GOSUB A.210.CALC
    END

    RETURN

A.210.CALC:
*    A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*    LOCATE WS.COMN.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
*     WS.RATE.ARY = R.CCY<RE.BCP.RATE>
*    WS.RATE = WS.RATE.ARY<1,CYPOS>
    WS.RATE = R.CCY<CCR.CY.RATE>
*    WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>

    WS.MLT.DIVD =  R.CCY<CCR.CY.CALC>
*    WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>


    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT.FCY * WS.RATE
    END

    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT.FCY / WS.RATE
    END

    WS.AMT.FCY = WS.AMT.LOCAL
    RETURN
*------------------------------------------------
A.500.UPDAT:
    WS.NEW.KEY = WS.FIN.ID:"*":WS.BR
    CALL F.READ(FN.FIN,WS.NEW.KEY,R.FIN,F.FIN,MSG.FIN)
    IF MSG.FIN NE "" THEN
        RETURN
    END

    WS.FIN.AMT.LCY =  R.FIN<CBE.FIN.POS.AMT.LCY>
    WS.FIN.AMT.FCY =  R.FIN<CBE.FIN.POS.AMT.FCY>

    WS.FIN.AMT.LCY =  WS.FIN.AMT.LCY + WS.AMT.LCY
    WS.FIN.AMT.FCY =  WS.FIN.AMT.FCY + WS.AMT.FCY

    R.FIN<CBE.FIN.POS.AMT.LCY>  =  WS.FIN.AMT.LCY
    R.FIN<CBE.FIN.POS.AMT.FCY>  =  WS.FIN.AMT.FCY
    CALL F.WRITE(FN.FIN,WS.NEW.KEY,R.FIN)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)

    RETURN
END
