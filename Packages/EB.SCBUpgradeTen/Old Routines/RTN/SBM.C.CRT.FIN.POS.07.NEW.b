* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****MOHSEN
    PROGRAM SBM.C.CRT.FIN.POS.07.NEW

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.BANK.FIN.POS
*-----------------------------------------------------------------------
    FN.ACLD = "F.CBE.MAST.AC.LD"
    F.ACLD = ""

    FN.FINPOS = "F.CBE.BANK.FIN.POS"
    F.FINPOS = ""
*--------------------------------------------------------
    CALL OPF (FN.ACLD,F.ACLD)
    CALL OPF (FN.FINPOS,F.FINPOS)
*---------------------------------------------------------
    WS.AMT = 0
*----------------------------------------------------------------------
    GOSUB A.100.READ.ACCOUNT
    RETURN
*----------------------------------------------------------------------
A.100.READ.ACCOUNT:
    SEL.CMDAC = "SELECT ":FN.ACLD:" WITH CBEM.CATEG GE 21001 AND CBEM.CATEG LE 21010"
    CALL EB.READLIST(SEL.CMDAC,SEL.LISTAC,"",NO.OF.RECAC,RET.CODEAC)
    LOOP
        REMOVE WS.ACLD.ID FROM SEL.LISTAC SETTING POSAC
    WHILE WS.ACLD.ID:POSAC
        CALL F.READ(FN.ACLD,WS.ACLD.ID,R.ACLD,F.ACLD,MSG.ACLD)
    WS.ACLD.CY = R.ACLD<C.CBEM.CY>
    WS.AC.BR   = R.ACLD<C.CBEM.BR>
*    WS.ACLD.AMT = R.ACLD<C.CBEM.IN.LCY>
    WS.ACLD.AMT = R.ACLD<C.RESERVED4>
*    WS.AMT.FCY = R.ACLD<C.RESERVED3>
        GOSUB A.500.CHG.FIN.POS

A.100.REPEAT:
    REPEAT
    RETURN
*-------------------------------------------------------------------
A.500.CHG.FIN.POS:
    WS.FINPOS.ID = 2040301
    WS.FINPOS.KEY = WS.FINPOS.ID:"*":WS.AC.BR

    CALL F.READ(FN.FINPOS,WS.FINPOS.KEY,R.FINPOS,F.FINPOS,MSG.FINPOS)
    IF MSG.FINPOS NE "" THEN
        RETURN
    END

    IF WS.ACLD.CY EQ "EGP" THEN
        WS.COMN = R.FINPOS<CBE.FIN.POS.AMT.LCY>
        WS.COMN = WS.COMN - WS.ACLD.AMT
        R.FINPOS<CBE.FIN.POS.AMT.LCY>   = WS.COMN
        GOSUB A.600.UP.FINPOS
    END
    IF WS.ACLD.CY NE "EGP" THEN
        WS.COMN = R.FINPOS<CBE.FIN.POS.AMT.FCY>
        WS.COMN = WS.COMN - WS.ACLD.AMT
        R.FINPOS<CBE.FIN.POS.AMT.FCY>   = WS.COMN
        GOSUB A.600.UP.FINPOS
    END
    WS.FINPOS.ID = 2040400
    WS.FINPOS.KEY = WS.FINPOS.ID:"*":WS.AC.BR

    CALL F.READ(FN.FINPOS,WS.FINPOS.KEY,R.FINPOS,F.FINPOS,MSG.FINPOS)
    IF MSG.FINPOS NE "" THEN
        RETURN
    END

    IF WS.ACLD.CY EQ "EGP" THEN
        WS.COMN = R.FINPOS<CBE.FIN.POS.AMT.LCY>
        WS.COMN = WS.COMN + WS.ACLD.AMT
        R.FINPOS<CBE.FIN.POS.AMT.LCY>   = WS.COMN
        GOSUB A.600.UP.FINPOS
    END
    IF WS.ACLD.CY NE "EGP" THEN
        WS.COMN = R.FINPOS<CBE.FIN.POS.AMT.FCY>
        WS.COMN = WS.COMN + WS.ACLD.AMT
        R.FINPOS<CBE.FIN.POS.AMT.FCY>   = WS.COMN
        GOSUB A.600.UP.FINPOS
    END


    RETURN
*------------------------------------------------------------
A.600.UP.FINPOS:

    CALL F.WRITE(FN.FINPOS,WS.FINPOS.KEY,R.FINPOS)
    CALL  JOURNAL.UPDATE(WS.FINPOS.KEY)

    RETURN
*------------------------------------------------------------

END
