* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>67</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM SBD.DPST.NATIONALTY
     SUBROUTINE SBD.DPST.NATIONALTY

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*-------------------------------------------------------------------------

    GOSUB INITIATE
     *DEBUG
***** CURRENCY SELECTION *************
*    T.SEL2 = "SELECT F.COUNTRY WITH @ID EQ 'AE' BY @ID"
     T.SEL2 = "SELECT F.COUNTRY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
        CALL F.READ(FN.CO,KEY.LIST2<NN>,R.CO,F.CO,E1)
            CUR.ID = "'":KEY.LIST2<NN>:"'"
****          CUR.ID = KEY.LIST2<NN>
            GOSUB PROCESS
        NEXT NN
    END
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SBD.ACC.DPST.N.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBD.ACC.DPST.N.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBD.ACC.DPST.N.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBD.ACC.DPST.N.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBD.ACC.DPST.N.CSV File IN &SAVEDLISTS&'
        END
    END

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CO = 'F.COUNTRY' ; F.CO = '' ; R.CO = ''
    CALL OPF(FN.CO,F.CO)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD)
*    TD = TODAY
*    CALL CDT ('',TD,'-1W')
    DAT.HED = FMT(TD,"####/##/##")

*    HEAD1 = "COUNTRY.DEPOSITS"
    HEAD1 = "DATE  : "
    HEAD.DESC  = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC  = "COUNTRY.NAME":","
    HEAD.DESC := "CURRENT.ACCOUNT":","
    HEAD.DESC := "DEPOSITS":","
    HEAD.DESC := "TOTAL":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

*========================================================================
PROCESS:
*IF CUR.ID EQ 'US' THEN
    CUR.EQV  = 0 ; DEP.EQV = 0
*-------------------------------------------
******* MM SELECTION *************
* IF CUR.ID EQ 'SA' THEN DEBUG
*DEBUG
    T.SEL = "SELECT ":FN.MM:" WITH CATEGORY EQ 21076 AND MATURITY.DATE GT ":TD:" AND VALUE.DATE LE ":TD:" AND COUNTRY.RISK EQ ":CUR.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.MM,KEY.LIST<I>,R.MM,F.MM,E1)
            MM.CURR = R.MM<MM.CURRENCY>
            AMT = R.MM<MM.PRINCIPAL>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 139 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE MM.CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>

            DEP.EQV += AMT * RATE

        NEXT I
    END
******* SIGHT SELECTION *************
*IF CUR.ID EQ "'NO'" THEN DEBUG
    T.SEL  = "SELECT ":FN.AC:" WITH (CATEGORY EQ 5001 OR CATEGORY EQ 5020) AND OPEN.ACTUAL.BAL LT 0 AND RESIDENCE EQ ":CUR.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            AMT = R.AC<AC.OPEN.ACTUAL.BAL>
            AC.CURR = R.AC<AC.CURRENCY>
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 158 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE AC.CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>

            CUR.EQV += AMT * RATE

        NEXT I
    END

*    CALL F.READ(FN.CO,CUR.ID,R.CO,F.CO,E1)
    COUNTRY.NAME = R.CO<EB.COU.COUNTRY.NAME,1>

    IF CUR.EQV LT 0 THEN CUR.EQV = CUR.EQV * -1
    IF DEP.EQV LT 0 THEN DEP.EQV = DEP.EQV * -1

    ALL.AMT = DEP.EQV + CUR.EQV
    CUR.EQV = CUR.EQV / 1000
    DEP.EQV = DEP.EQV / 1000
    ALL.AMT = ALL.AMT / 1000

    CUR.EQV = DROUND(CUR.EQV,'0')
    DEP.EQV = DROUND(DEP.EQV,'0')
    ALL.AMT = DROUND(ALL.AMT,'0')

    BB.DATA  = COUNTRY.NAME:","
    BB.DATA := CUR.EQV:","
    BB.DATA := DEP.EQV:","
    BB.DATA := ALL.AMT:","

    IF ALL.AMT NE 0 THEN
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    END
*********************************
    RETURN
END
