* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.CATEG.ENTRY.R1(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

*-----------------------------------
    SYS.DATE   = TODAY
    LAST.WORK.DATE   = SYS.DATE
    CALL CDT('', LAST.WORK.DATE, "-1W")
*------------------------
    ENQ.NAME   = ENQ.DATA<1.1>
    OPER.CODE  = "LE"
*------------------------
        ENQ.DATA<2,1> = "@ID"
        ENQ.DATA<3,1> = "LK"
        ENQ.DATA<4,1> = "...E..."
*------------------------
        ENQ.DATA<2,2> = "BOOKING.DATE"
        ENQ.DATA<3,2> = "EQ"
        ENQ.DATA<4,2> = LAST.WORK.DATE
*------------------------
*        ENQ.DATA<2,3> = "RECORD.STAUTS"
*        ENQ.DATA<3,3> = "NE"
*        ENQ.DATA<4,3> = "REVE"
*------------------------
        ENQ.DATA<2,3> = "AMOUNT.LCY"
        ENQ.DATA<3,3> = "LT"
        ENQ.DATA<4,3> =  0
*------------------------
        ENQ.DATA<2,4> = "PL.CATEGORY"
        ENQ.DATA<3,4> = "GE"
        ENQ.DATA<4,4> =  51000
*------------------------
        ENQ.DATA<2,5> = "PL.CATEGORY"
        ENQ.DATA<3,5> = "LE"
        ENQ.DATA<4,5> = 59999
*------------------------
*        ENQ.DATA<2,6> = "CATEGORY"
*        ENQ.DATA<3,6> = "LE"
*        ENQ.DATA<4,6> = 59999
*------------------------
*-----------------------------------
    RETURN
END
