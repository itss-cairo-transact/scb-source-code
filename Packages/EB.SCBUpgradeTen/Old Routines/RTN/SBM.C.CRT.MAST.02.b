* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBM.C.CRT.MAST.02

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.CONTROL
*---------------------------------------------------------
    FN.AC = "F.CBE.MAST.AC.LD"
    F.AC  = ""

    FN.CBE = "F.CBE.STATIC.MAST"
    F.CBE  = ""

    FN.CONT = "F.CBE.STATIC.CONTROL"
    F.CONT  = ""

    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ""
*-------------------------------------------------
    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CONT,F.CONT)
    CALL OPF (FN.CUS,F.CUS)
*-----------------------------------------
    WS.AC.ID         = ""
    WS.CBE.ID        = ""
    MSG.AC           = ""
    MSG.CBE          = ""
    MSG.CY           = ""
    WS.INDSTRY       = ""
    WS.SECTR         = ""
    WS.N.KEY         = ""
    WS.CBE.CODE      = ""
    WS.OLD.CODE      = ""
    WS.CUST.NO       = ""
    WS.COUNT         = ""
    WS.STATIC.KEY    = ""
    WS.CBEM.ID       = ""
    WS.BR            = ""
    R.STAT           = ""
    R.CY             = ""
    WS.CY            = ""
    WS.FIL.AC.LD.AMT = "0"
    WS.CUS.CODE      = ""
    WS.RATE          = "0"
    WS.MLT.DIVD      = "0"
    WS.COMN.AMT      = "0"
    WS.COMN.COUNT    = "0"
    WS.CATEG         = ""
    A.ARY            = ""
    CYPOS            = ""
    WS.RATE.ARY      = ""
    WS.MLT.DIVD.ARY  = ""

    WS.KEY      = "NZD"
    DATY        = TODAY
    WS.CONT.KEY = "SBM.C.CRT.MAST.02"
    MSG.CONT    = ""
    CALL F.READ(FN.CONT,WS.CONT.KEY,R.CONT,F.CONT,MSG.CONT)

    IF MSG.CONT NE "" THEN
        GOSUB A.600.CRT.CONTROL
        WS.CC = 0
        GOSUB A.050.PROC
    END
    RETURN
***********************    PROCEDURE AREA     *****************************************************
A.050.PROC:
    SEL.CMD = "SELECT ":FN.AC
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

    LOOP
        REMOVE WS.AC.ID FROM SEL.LIST SETTING POS
    WHILE WS.AC.ID:POS

        CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,MSG.AC)
        IF R.AC<C.CBEM.CUSTOMER.CODE> = ""  THEN
            GOTO AAAA
        END

        WS.FIL.AC.LD.AMT           = 0
        WS.CATEG                   = R.AC<C.CBEM.CATEG>
        WS.FIL.AC.LD.AMT           = R.AC<C.CBEM.IN.LCY>
        WS.BR                      = R.AC<C.CBEM.BR>
        WS.CUS.CODE                = R.AC<C.CBEM.CUSTOMER.CODE>
        WS.CY                      = R.AC<C.CBEM.CY>
        WS.CBEM.ID                 = WS.CUS.CODE:"*":WS.BR
        MSG.CBE                    = ""
        CALL F.READ(FN.CBE,WS.CBEM.ID,R.CBE,F.CBE,MSG.CBE)

        IF MSG.CBE THEN
            GOSUB INIT.REC.055
        END
        CALL F.READ(FN.CBE,WS.CBEM.ID,R.CBE,F.CBE,MSG.CBE)

        IF MSG.CBE EQ "" THEN
            MOHSEN =  R.CBE<C.CBE.NEW.SECTOR>
            GOSUB A.100.CHK
        END
AAAA:
    REPEAT
    RETURN
*--------------------------------------------------------

A.100.CHK:
    IF MOHSEN EQ 4650 AND WS.CATEG EQ 1012 THEN
    END

    GOSUB A.200.UPDAT.REC.LE

    RETURN
A.200.UPDAT.REC.LE:
    IF MOHSEN EQ 4650 AND WS.CATEG EQ 1012 THEN
    END

    IF (WS.CATEG GE 1001  AND WS.CATEG LE 1007) OR WS.CATEG EQ 2006 THEN
        GOSUB A.205.LE
    END

    IF WS.CATEG EQ 1019 THEN
        GOSUB A.220.LE
    END
*------------------------------------------------------
    IF WS.CATEG GE 6501  AND WS.CATEG LE 6508 THEN
        GOSUB A.215.LE
    END
    IF WS.CATEG GE 6513  AND WS.CATEG LE 6517 THEN
        GOSUB A.215.LE
    END
    IF WS.CATEG EQ 6511 THEN
        GOSUB A.215.LE
    END
*--------------------------------------------------
    IF WS.CATEG EQ 1012 THEN
        GOSUB A.220.LE
    END
    IF WS.CATEG EQ 1013 THEN
        GOSUB A.220.LE
    END
    IF WS.CATEG EQ 1014 THEN
        GOSUB A.220.LE
    END

    IF WS.CATEG EQ 1015 THEN
        GOSUB A.220.LE
    END

    IF WS.CATEG EQ 1016 THEN
        GOSUB A.220.LE
    END
    IF WS.CATEG EQ 3001 THEN
        GOSUB A.220.LE
    END
    IF WS.CATEG EQ 3050 THEN
        GOSUB A.220.LE
    END
*----------------------------------------------------
    IF WS.CATEG EQ 3005 THEN
        GOSUB A.225.LE
    END
    IF WS.CATEG EQ 3014 THEN
        GOSUB A.225.LE
    END
*----------------------------------------------------
    IF WS.CATEG GE 3010  AND WS.CATEG LE 3013 THEN
        GOSUB A.230.LE
    END
    IF WS.CATEG GE 3015  AND WS.CATEG LE 3017 THEN
        GOSUB A.230.LE
    END
    IF WS.CATEG GE 3060  AND WS.CATEG LE 3065 THEN
        GOSUB A.230.LE
    END
    IF WS.CATEG EQ 16170 THEN
        GOSUB A.230.LE
    END
*----------------------------------------------------
*-- EDIT 2017/11/09
*   IF WS.CATEG GE 1100 AND WS.CATEG LE 1219 THEN
    IF WS.CATEG GE 1100 AND WS.CATEG LE 1210 THEN
        GOSUB A.236.LE
    END
*-------------------------------------------------
    IF WS.CATEG GE 1300  AND WS.CATEG LE 1499 THEN
        GOSUB A.236.LE
    END
    IF WS.CATEG GE 1500  AND WS.CATEG LE 1519 THEN
        GOSUB A.236.LE
    END
    IF WS.CATEG GE 1520  AND WS.CATEG LE 1594 THEN
        GOSUB A.236.LE
    END
*-------------------------------------------------
    IF WS.CATEG GE 1595  AND WS.CATEG LE 1599 THEN
        GOSUB A.237.LE
    END
    IF WS.CATEG GE 1211  AND WS.CATEG LE 1219 THEN
        GOSUB A.237.LE
    END
    IF WS.CATEG EQ 1230 THEN
        GOSUB A.237.LE
    END
*------------------------ ������� ���������
    IF WS.CATEG GE 1220  AND WS.CATEG LE 1299 THEN
        GOSUB A.237.LE
    END
*--------------------------------------------------
    IF WS.CATEG EQ 6512 THEN
        GOSUB A.240.LE
    END
    RETURN

A.205.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.305.EQ
        RETURN
    END
    IF WS.FIL.AC.LD.AMT GT 0   THEN

        WS.COMN.AMT = R.CBE<C.CBE.CUR.AC.LE>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CUR.AC.LE> = WS.COMN.AMT
    END

    IF WS.FIL.AC.LD.AMT LT 0   THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CUR.AC.LE.DR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CUR.AC.LE.DR>  = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.CUR.AC.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.CUR.AC.LE.NO.OF.AC>  = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
A.215.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.315.EQ
        RETURN
    END

    IF WS.FIL.AC.LD.AMT GT 0   THEN
        WS.COMN.AMT = R.CBE<C.CBE.SAV.AC.LE>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.SAV.AC.LE> = WS.COMN.AMT
    END

    IF WS.FIL.AC.LD.AMT LT 0   THEN
        WS.COMN.AMT =  R.CBE<C.CBE.SAV.AC.LE.DR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.SAV.AC.LE.DR>  = WS.COMN.AMT
    END
    WS.COMN.COUNT = R.CBE<C.CBE.SAV.AC.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.SAV.AC.LE.NO.OF.AC> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN

A.220.LE:
    IF MOHSEN EQ 4650 AND WS.CATEG EQ 1012 THEN
    END
    IF WS.CY  NE "EGP" THEN
        GOSUB A.320.EQ
        RETURN
    END
    WS.COMN.AMT = R.CBE<C.CBE.BLOCK.AC.LE>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.BLOCK.AC.LE> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.BLOCK.AC.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.BLOCK.AC.LE.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN

A.225.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.325.EQ
        RETURN
    END
    WS.COMN.AMT = R.CBE<C.CBE.MARG.LG.LE>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.MARG.LG.LE> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.MARG.LG.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.MARG.LG.LE.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN

A.230.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.330.EQ
        RETURN
    END
    WS.COMN.AMT = R.CBE<C.CBE.MARG.LC.LE>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.MARG.LC.LE> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.MARG.LC.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.MARG.LC.LE.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN

A.235.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.335.EQ
        RETURN
    END
    IF WS.FIL.AC.LD.AMT LT 0 THEN
        WS.COMN.AMT = R.CBE<C.CBE.FACLTY.LE>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.FACLTY.LE> = WS.COMN.AMT
    END
    IF WS.FIL.AC.LD.AMT GT 0 THEN
        WS.COMN.AMT = R.CBE<C.CBE.FACLTY.LE.CR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.FACLTY.LE.CR>  = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.FACLTY.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.FACLTY.LE.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN
*----------------------------------------
A.336.EQ:
    IF WS.FIL.AC.LD.AMT LT 0 THEN
        WS.COMN.AMT = R.CBE<C.CBE.LOANS.EQ.L>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.LOANS.EQ.L> = WS.COMN.AMT
    END
    IF WS.FIL.AC.LD.AMT GT 0 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.LOANS.EQ.L.CR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.LOANS.EQ.L.CR> = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.LOANS.EQ.L.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.LOANS.EQ.L.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN
*----------------------------------------
A.236.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.336.EQ
        RETURN
    END
    IF WS.FIL.AC.LD.AMT LT 0 THEN
        WS.COMN.AMT = R.CBE<C.CBE.LOANS.LE.L>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.LOANS.LE.L> = WS.COMN.AMT
    END
    IF WS.FIL.AC.LD.AMT GT 0 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.LOANS.LE.L.CR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.FACLTY.LE.CR> = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.LOANS.LE.L.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.LOANS.LE.L.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN
*--- END EDIT ----------------------------------------
*-----------------------------------------------------
A.237.LE:
    IF WS.CY  NE "EGP" THEN
        GOSUB A.337.EQ
        RETURN
    END
    WS.COMN.AMT = R.CBE<C.CBE.FACLTY.LE>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.FACLTY.LE> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.FACLTY.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.FACLTY.LE.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN
*-------------------------------------------------
A.240.LE:
    IF WS.CY NE "EGP" THEN
        GOSUB A.340.EQ
        RETURN
    END
    WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.LE.1Y>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.DEPOST.AC.LE.1Y> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.DEPOST.AC.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.DEPOST.AC.LE.NO.OF.AC> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
*-------------------------------------------------
A.305.EQ:
    IF WS.FIL.AC.LD.AMT GT 0   THEN

        WS.COMN.AMT = R.CBE<C.CBE.CUR.AC.EQ>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CUR.AC.EQ> = WS.COMN.AMT
    END

    IF WS.FIL.AC.LD.AMT LT 0   THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CUR.AC.EQ.DR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CUR.AC.EQ.DR>  = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.CUR.AC.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.CUR.AC.EQ.NO.OF.AC>  = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
A.315.EQ:
*    WS.COMN.AMT = R.CBE<C.CBE.SAV.AC.EQ>
*    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
*    R.CBE<C.CBE.SAV.AC.EQ> = WS.COMN.AMT

    IF WS.FIL.AC.LD.AMT GT 0   THEN
        WS.COMN.AMT = R.CBE<C.CBE.SAV.AC.EQ>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.SAV.AC.EQ> = WS.COMN.AMT
    END

    IF WS.FIL.AC.LD.AMT LT 0   THEN
        WS.COMN.AMT =  R.CBE<C.CBE.SAV.AC.EQ.DR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.SAV.AC.EQ.DR>  = WS.COMN.AMT
    END
    WS.COMN.COUNT = R.CBE<C.CBE.SAV.AC.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.SAV.AC.EQ.NO.OF.AC> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN

A.320.EQ:
    IF MOHSEN EQ 4650 AND WS.CATEG EQ 1012 THEN
    END
    WS.COMN.AMT = R.CBE<C.CBE.BLOCK.AC.EQ>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.BLOCK.AC.EQ> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.BLOCK.AC.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.BLOCK.AC.EQ.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN

A.325.EQ:
    WS.COMN.AMT = R.CBE<C.CBE.MARG.LG.EQ>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.MARG.LG.EQ> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.MARG.LG.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.MARG.LG.EQ.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN

A.330.EQ:
    WS.COMN.AMT = R.CBE<C.CBE.MARG.LC.EQ>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.MARG.LC.EQ> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.MARG.LC.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.MARG.LC.EQ.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN

A.335.EQ:
    IF WS.FIL.AC.LD.AMT LT 0 THEN
        WS.COMN.AMT = R.CBE<C.CBE.FACLTY.EQ>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.FACLTY.EQ> = WS.COMN.AMT
    END
    IF WS.FIL.AC.LD.AMT GT 0 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.FACLTY.EQ.CR>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.FACLTY.EQ.CR> = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.FACLTY.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.FACLTY.EQ.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN
*------------------------------------------------
A.337.EQ:
    WS.COMN.AMT = R.CBE<C.CBE.FACLTY.EQ>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.FACLTY.EQ> = WS.COMN.AMT

    WS.COMN.COUNT = R.CBE<C.CBE.FACLTY.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.FACLTY.EQ.NO.OF.AC> = WS.COMN.COUNT
    GOSUB A.500.UPDAT
    RETURN
*------------------------------------------------
A.340.EQ:
    WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.EQ.1Y>
    WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
    R.CBE<C.CBE.DEPOST.AC.EQ.1Y> = WS.COMN.AMT
    WS.COMN.COUNT = R.CBE<C.CBE.DEPOST.AC.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.DEPOST.AC.EQ.NO.OF.AC> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
*------------------------------------------------
A.500.UPDAT:
*************************************

    CALL F.WRITE(FN.CBE,WS.CBEM.ID,R.CBE)
    CALL  JOURNAL.UPDATE(WS.CBEM.ID)

    RETURN
*------------------------------------------------
*-----------------����� ����� ����� ���� ����� �������
A.600.CRT.CONTROL:
    DATY = TODAY
    WS.CONT.KEY = "SBM.C.CRT.MAST.02"
    R.CONT<C.CONTROL.DATE> = DATY
    CALL F.WRITE(FN.CONT,WS.CONT.KEY,R.CONT)
    CALL  JOURNAL.UPDATE(WS.CONT.KEY)
    RETURN
*---------------------------------------------------------
INIT.REC.055:

    R.STAT                              = ''

    CALL F.READ(FN.CUS,WS.CUS.CODE,R.CUS,F.CUS,MSG.CUS)

    WS.OLD.CODE                         = R.CUS<EB.CUS.INDUSTRY>
    WS.CBME.ID                          = WS.CUS.CODE:"*":WS.BR
    R.STAT<C.CBE.CUSTOMER.CODE>         = WS.CUS.CODE
    R.STAT<C.CBE.BR>                    = WS.BR
    R.STAT<C.CBE.INDUSTRY>              = R.CUS<EB.CUS.INDUSTRY>
    R.STAT<C.CBE.SECTOR>                = R.CUS<EB.CUS.SECTOR>
    R.STAT<C.CBE.LEGAL.FORM>            = R.CUS<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
    R.STAT<C.CBE.NEW.SECTOR>            = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
    R.STAT<C.CBE.NEW.INDUSTRY>          = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.INDUSTRY>
    R.STAT<C.CBE.CUR.AC.LE>             = "0"
    R.STAT<C.CBE.CUR.AC.LE.DR>          = "0"
    R.STAT<C.CBE.CUR.AC.LE.NO.OF.AC>    = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.1Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.2Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.3Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.MOR3>     = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.NO.OF.AC> = "0"
    R.STAT<C.CBE.CER.AC.LE.3Y>          = "0"
    R.STAT<C.CBE.CER.AC.LE.5Y>          = "0"
    R.STAT<C.CBE.CER.AC.LE.GOLD>        = "0"
    R.STAT<C.CBE.CER.AC.LE.NO.OF.CER>   = "0"
    R.STAT<C.CBE.SAV.AC.LE>             = "0"
    R.STAT<C.CBE.SAV.AC.LE.DR>          =  0
    R.STAT<C.CBE.SAV.AC.LE.NO.OF.AC>    = "0"
    R.STAT<C.CBE.BLOCK.AC.LE>           = "0"
    R.STAT<C.CBE.BLOCK.AC.LE.NO.OF.AC>  = "0"
    R.STAT<C.CBE.MARG.LC.LE>            = "0"
    R.STAT<C.CBE.MARG.LC.LE.NO.OF.AC>   = "0"
    R.STAT<C.CBE.MARG.LG.LE>            = "0"
    R.STAT<C.CBE.MARG.LG.LE.NO.OF.AC>   = "0"
    R.STAT<C.CBE.FACLTY.LE>             = "0"
    R.STAT<C.CBE.FACLTY.LE.CR>          = "0"
    R.STAT<C.CBE.FACLTY.LE.NO.OF.AC>    = "0"
    R.STAT<C.CBE.LOANS.LE.S>            = "0"
    R.STAT<C.CBE.LOANS.LE.S.CR>         = "0"
    R.STAT<C.CBE.LOANS.LE.S.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.LE.M>            = "0"
    R.STAT<C.CBE.LOANS.LE.M.CR>         = "0"
    R.STAT<C.CBE.LOANS.LE.M.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.LE.L>            = "0"
    R.STAT<C.CBE.LOANS.LE.L.CR>         = "0"
    R.STAT<C.CBE.LOANS.LE.L.NO.OF.AC>   = "0"
    R.STAT<C.CBE.COMRCL.PAPER.LE>       = "0"
    R.STAT<C.CBE.COMRCL.PAPER.LE.NO>    = "0"
    R.STAT<C.CBE.CUR.AC.EQ>             = "0"
    R.STAT<C.CBE.CUR.AC.EQ.DR>          = "0"
    R.STAT<C.CBE.CUR.AC.EQ.NO.OF.AC>    = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.1Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.2Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.3Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.MOR3>     = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.NO.OF.AC> = "0"
    R.STAT<C.CBE.CER.AC.EQ.3Y>          = "0"
    R.STAT<C.CBE.CER.AC.EQ.5Y>          = "0"
    R.STAT<C.CBE.CER.AC.EQ.GOLD>        = "0"
    R.STAT<C.CBE.CER.AC.EQ.NO.OF.CER>   = "0"
    R.STAT<C.CBE.SAV.AC.EQ>             = "0"
    R.STAT<C.CBE.SAV.AC.EQ.DR>          =  0
    R.STAT<C.CBE.SAV.AC.EQ.NO.OF.AC>    = "0"
    R.STAT<C.CBE.BLOCK.AC.EQ>           = "0"
    R.STAT<C.CBE.BLOCK.AC.EQ.NO.OF.AC>  = "0"
    R.STAT<C.CBE.MARG.LC.EQ>            = "0"
    R.STAT<C.CBE.MARG.LC.EQ.NO.OF.AC>   = "0"
    R.STAT<C.CBE.MARG.LG.EQ>            = "0"
    R.STAT<C.CBE.MARG.LG.EQ.NO.OF.AC>   = "0"
    R.STAT<C.CBE.FACLTY.EQ>             = "0"
    R.STAT<C.CBE.FACLTY.EQ.CR>          = "0"
    R.STAT<C.CBE.FACLTY.EQ.NO.OF.AC>    = "0"
    R.STAT<C.CBE.LOANS.EQ.S>            = "0"
    R.STAT<C.CBE.LOANS.EQ.S.CR>         = "0"
    R.STAT<C.CBE.LOANS.EQ.S.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.EQ.M>            = "0"
    R.STAT<C.CBE.LOANS.EQ.M.CR>         = "0"
    R.STAT<C.CBE.LOANS.EQ.M.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.EQ.L>            = "0"
    R.STAT<C.CBE.LOANS.EQ.L.CR>         = "0"
    R.STAT<C.CBE.LOANS.EQ.L.NO.OF.AC>   = "0"
    R.STAT<C.CBE.COMRCL.PAPER.EQ>       = "0"
    R.STAT<C.CBE.COMRCL.PAPER.EQ.NO>    = "0"

    CALL F.WRITE(FN.CBE,WS.CBEM.ID,R.STAT)
    CALL  JOURNAL.UPDATE(WS.CBEM.ID)

    RETURN
*---------------------------------------------------------
END
