* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-251</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.FUND.DEP.003

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FUND.DEP
*-------------------------------------------------------------------------
    GOSUB INITIATE

***** CURRENCY SELECTION *************
    T.SEL2 = "SELECT FBNK.CURRENCY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
            CUR.ID = KEY.LIST2<NN>
            GOSUB PROCESS
        NEXT NN
    END
    RETURN

*-------------------------------------------------------------------------
INITIATE:
    FN.FD = 'F.SCB.FUND.DEP' ; F.FD = ''
    CALL OPF(FN.FD,F.FD)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    OPENSEQ "&SAVEDLISTS&" , "TOP.25.LARGEST.DEPOSITORS.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"TOP.25.LARGEST.DEPOSITORS.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "TOP.25.LARGEST.DEPOSITORS.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE TOP.25.LARGEST.DEPOSITORS.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create TOP.25.LARGEST.DEPOSITORS.CSV File IN &SAVEDLISTS&'
        END
    END

    DAT = TODAY
    CALL CDT("",DAT,'-1C')

    TD = TODAY
    CALL CDT ('',TD,'-1W')
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "TOP.25.LARGEST.DEPOSITORS"
    HEAD.DESC  = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "RANK":","
    HEAD.DESC := "NAME":","
    HEAD.DESC := "C/A":","
    HEAD.DESC := "TIME DEPOSITS":","
    HEAD.DESC := "CDs":","
    HEAD.DESC := "SAVING A/C":","
    HEAD.DESC := "INTERNAL ACCOUNT":","
    HEAD.DESC := "CONTINGENT LG":","
    HEAD.DESC := "CONTINGENT LC":","
    HEAD.DESC := "TOTAL":","
    HEAD.DESC := "CURRENCY":","
    HEAD.DESC := "BRANCH":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

*========================================================================
PROCESS:

    T.SEL  = "SELECT ":FN.FD:" WITH CURRENCY EQ ":CUR.ID:" BY-DSND TOTAL.AMT"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        IF SELECTED GE 25 THEN
            SEL.COUNT = 25
        END ELSE
            SEL.COUNT = SELECTED
        END
        FOR I = 1 TO SEL.COUNT

            CALL F.READ(FN.FD,KEY.LIST<I>,R.FD,F.FD,E1)

            CUS.ID = R.FD<FD.CUSTOMER.NO>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.ID,CUS.COMP)
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CUS.COMP,CUS.BRN)

            CUST.NAME = R.FD<FD.CUSTOMER.NAME>
            CUR.AMT   = R.FD<FD.CURRENT.ACCT>
            SAV.AMT   = R.FD<FD.SAVING.ACCT>
            DEP.AMT   = R.FD<FD.TIME.DEP>
            CDS.AMT   = R.FD<FD.CDS>
            INT.AMT   = R.FD<FD.INT.ACCT>
            CLG.AMT   = R.FD<FD.CONT.LG>
            CLC.AMT   = R.FD<FD.CONT.LC>
            OTH.AMT   = R.FD<FD.OTHERS>
            AMT.TOTAL = R.FD<FD.TOTAL.AMT>
            IF AMT.TOTAL GT 0 THEN
                BB.DATA  = I:","
                BB.DATA := CUST.NAME:","
                BB.DATA := CUR.AMT:","
                BB.DATA := DEP.AMT:","
                BB.DATA := CDS.AMT:","
                BB.DATA := SAV.AMT:","
                BB.DATA := INT.AMT:","
                BB.DATA := CLG.AMT:","
                BB.DATA := CLC.AMT:","
                BB.DATA := AMT.TOTAL:","
                BB.DATA := CUR.ID:","
                BB.DATA := CUS.BRN:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I

        BB.DATA = '******':","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    END
    RETURN
*===============================================================
END
