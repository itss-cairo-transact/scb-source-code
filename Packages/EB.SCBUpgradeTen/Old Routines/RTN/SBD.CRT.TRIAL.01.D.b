* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>330</Rating>
*-----------------------------------------------------------------------------
    PROGRAM  SBD.CRT.TRIAL.01.D
* SUBROUTINE SBD.CRT.TRIAL.01.D
    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.RE.STAT.LINE.CONT
    $INSERT T24.BP  I_F.RE.BASE.CCY.PARAM
    $INSERT T24.BP  I_F.CONSOLIDATE.ASST.LIAB
    $INSERT T24.BP  I_F.CONSOLIDATE.PRFT.LOSS
    $INSERT TEMENOS.BP I_F.CATEG.MAS.D
***---------------------------------------------------
    FN.CONT = "FBNK.RE.STAT.LINE.CONT"
    F.CONT  = ""

    FN.CONSASLB = "FBNK.CONSOLIDATE.ASST.LIAB"
    F.CONSASLB = ""

    FN.CONSPL = "FBNK.CONSOLIDATE.PRFT.LOSS"
    F.CONSPL = ""

    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

    FN.CAT = "F.CATEG.MAS.D"
    F.CAT  = ""


***----------------------------------------------------
***----------------------------------------------------
    CALL OPF (FN.CONT,F.CONT)
    CALL OPF (FN.CONSASLB,F.CONSASLB)
    CALL OPF (FN.CONSPL,F.CONSPL)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.CAT,F.CAT)
***----------------------------------------------------
    FN.MATCH = "F.CATEG.MAS.D"
*-- EDIT BY NESSMA
    FILEVAR = ""
    CALL OPF(FN.MATCH,FILEVAR)
*    OPEN FN.MATCH TO FILEVAR ELSE ABORT 201, FN.MATCH
*-- END EDIT
    CLEARFILE FILEVAR
    WS.TODAY  = TODAY
    GOSUB A.050.PROC
*    GOSUB  FIX.NZD
    RETURN
***----------------------------------------------------
A.050.PROC:
    SEL.CMD = "SELECT ":FN.CONT:" WITH @ID LIKE GENLED...."
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

    LOOP
        REMOVE WS.CONT.ID FROM SEL.LIST SETTING POS
    WHILE WS.CONT.ID:POS

        CALL F.READ(FN.CONT,WS.CONT.ID,R.CONT,F.CONT,MSG.CONT)

        WS.LINE.NO = FIELD(WS.CONT.ID,'.',2)
        WS.BR.NO   = FIELD(WS.CONT.ID,'.',3)
        IF  WS.BR.NO   EQ "EG0010088"  THEN
            GOTO A.050.REP
        END
        WS.APPL.COUNT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
        GOSUB A.060.CHK.ASST

        WS.APPL.PL.COUNT = DCOUNT(R.CONT<RE.SLC.PRFT.CONSOL.KEY>,@VM)

        GOSUB A.070.CHK.PL
*
A.050.REP:
    REPEAT
A.050.EXIT:
    RETURN
***-------------------------------------------------------
A.060.CHK.ASST:
    FOR  A.SUB = 1 TO WS.APPL.COUNT
        WS.ASST.CONSOL.KEY = R.CONT<RE.SLC.ASST.CONSOL.KEY,A.SUB>
        WS.ASST.TYPE       = R.CONT<RE.SLC.ASSET.TYPE,A.SUB>
        WS.ASST.COUNT = DCOUNT(R.CONT<RE.SLC.ASSET.TYPE,A.SUB>,@SM)
*        WS.BR.NO = WS.ASST.CONSOL.KEY[9]
        WS.CY.CODE = FIELD(WS.ASST.CONSOL.KEY,'.',4)
        WS.GL.CODE = FIELD(WS.ASST.CONSOL.KEY,'.',5)
        GOSUB A.061.ASTTYP
    NEXT A.SUB
    RETURN
***-------------------------------------------------------
A.061.ASTTYP:
    FOR  WS.AST.SUB = 1 TO WS.ASST.COUNT
        WS.ASST.TYPEX  = R.CONT<RE.SLC.ASSET.TYPE,A.SUB,WS.AST.SUB>
        WS.BAL.EQV = 0
        GOSUB A.065.CONSLASLB
    NEXT WS.AST.SUB
    RETURN
*---------------------------------------------------
A.065.CONSLASLB:
*    WS.BR.NO.X = WS.BR.NO[1,6]
*    IF WS.BR.NO.X NE "EG0010"  THEN
*        RETURN
*    END
    CALL F.READ(FN.CONSASLB,WS.ASST.CONSOL.KEY,R.CONSASLB,F.CONSASLB,MSG.CONSASLB)
    A.ARY.CONS =  R.CONSASLB<RE.ASL.TYPE>
*Line [ 119 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.ASST.TYPEX IN A.ARY.CONS<1,1> SETTING CONSPOS ELSE NULL

    A.BAL.ARY.CUR = 0 ; A.DR.ARY.CUR = 0 ; A.CR.ARY.CUR = 0
    IF WS.CY.CODE EQ "EGP"  THEN
        A.BAL.ARY = R.CONSASLB<RE.ASL.BALANCE>
        A.DR.ARY  = R.CONSASLB<RE.ASL.DEBIT.MOVEMENT>
        A.CR.ARY  = R.CONSASLB<RE.ASL.CREDIT.MOVEMENT>
    END ELSE
        A.BAL.ARY = R.CONSASLB<RE.ASL.LOCAL.BALANCE>
        A.DR.ARY  = R.CONSASLB<RE.ASL.LOCAL.DEBIT.MVE>
        A.CR.ARY  = R.CONSASLB<RE.ASL.LOCAL.CREDT.MVE>

        A.BAL.ARY.CUR = 0 ; A.DR.ARY.CUR = 0 ; A.CR.ARY.CUR = 0
        A.BAL.ARY.CUR = R.CONSASLB<RE.ASL.BALANCE>
        A.DR.ARY.CUR  = R.CONSASLB<RE.ASL.DEBIT.MOVEMENT>
        A.CR.ARY.CUR  = R.CONSASLB<RE.ASL.CREDIT.MOVEMENT>
    END

    WS.BAL = A.BAL.ARY<1,CONSPOS>
    WS.DR  = A.DR.ARY<1,CONSPOS>
    WS.CR  = A.CR.ARY<1,CONSPOS>

    WS.BAL.CUR = A.BAL.ARY.CUR<1,CONSPOS>
    WS.DR.CUR  = A.DR.ARY.CUR<1,CONSPOS>
    WS.CR.CUR  = A.CR.ARY.CUR<1,CONSPOS>

    WS.BAL = WS.BAL + WS.DR + WS.CR
    WS.BAL.CUR = WS.BAL.CUR + WS.DR.CUR + WS.CR.CUR

    IF     WS.CY.CODE EQ "EGP"  THEN
        WS.BAL.EQV = WS.BAL
    END
    IF     WS.CY.CODE NE "EGP"  THEN
        GOSUB A.110.CY
    END
    GOSUB A.100.CHK.CATEG.D
    RETURN
***------------PROFIT AND LOSS -------------------------------------------
A.070.CHK.PL:
    FOR  A.SUB = 1 TO WS.APPL.PL.COUNT
        WS.PL.CONSOL.KEY = R.CONT<RE.SLC.PRFT.CONSOL.KEY,A.SUB>
        WS.PL.CY         = R.CONT<RE.SLC.PROFIT.CCY,A.SUB>
        WS.PL.COUNT = DCOUNT(R.CONT<RE.SLC.PROFIT.CCY,A.SUB>,@SM)
*        WS.BR.NO = WS.PL.CONSOL.KEY[9]
*???        WS.CY.CODE = FIELD(WS.PL.CONSOL.KEY,'.',4)
*???        WS.CY.CODE = WS.PL.CY
        WS.GL.CODE = FIELD(WS.PL.CONSOL.KEY,'.',2)

        GOSUB A.072.PL
    NEXT A.SUB
    RETURN

***-------------------------------------------------------
A.072.PL:
    FOR  WS.PL.SUB = 1 TO WS.PL.COUNT
        WS.PL.TYPEX  = R.CONT<RE.SLC.PROFIT.CCY,A.SUB,WS.PL.SUB>
        WS.CY.CODE = WS.PL.TYPEX
        GOSUB A.074.PL
    NEXT WS.PL.SUB
    RETURN
***-------------------------------------------------------
***-------------------------------------------------------
A.074.PL:
    CALL F.READ(FN.CONSPL,WS.PL.CONSOL.KEY,R.CONSPL,F.CONSPL,MSG.CONSPL)
    A.ARY.CONS =  R.CONSPL<RE.PTL.CURRENCY>
*Line [ 185 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.PL.TYPEX IN A.ARY.CONS<1,1> SETTING CONSPOS ELSE NULL
    WS.CY.CODE = WS.PL.TYPEX
    IF  WS.CY.CODE EQ "EGP"  THEN
        A.BAL.ARY = R.CONSPL<RE.PTL.BALANCE>
        A.DR.ARY  = R.CONSPL<RE.PTL.DEBIT.MOVEMENT>
        A.CR.ARY  = R.CONSPL<RE.PTL.CREDIT.MOVEMENT>
        A.BAL.YTD.ARY = R.CONSPL<RE.PTL.BALANCE.YTD>

        WS.BAL = A.BAL.ARY<1,CONSPOS>
        WS.DR  = A.DR.ARY<1,CONSPOS>
        WS.CR  = A.CR.ARY<1,CONSPOS>
        WS.BAL.YTD = A.BAL.YTD.ARY<1,CONSPOS>
        WS.BAL = WS.BAL + WS.BAL.YTD + WS.DR + WS.CR
        WS.BAL.EQV = WS.BAL
    END
**********��������� � ��������� ������ ��� ������� ���
**********������ ������� ��� �������

**    IF WS.CY.CODE NE "EGP" AND WS.LINE.NO EQ 0120 THEN
    A.BAL.ARY = R.CONSPL<RE.PTL.CCY.BALANCE>
    A.DR.ARY  = R.CONSPL<RE.PTL.CCY.DEBIT.MVE>
    A.CR.ARY  = R.CONSPL<RE.PTL.CCY.CREDT.MVE>
    A.BAL.YTD.ARY = R.CONSPL<RE.PTL.CCY.BALANCE.YTD>
    WS.BAL = A.BAL.ARY<1,CONSPOS>
    WS.DR  = A.DR.ARY<1,CONSPOS>
    WS.CR  =    A.CR.ARY<1,CONSPOS>
    WS.BAL.YTD = A.BAL.YTD.ARY<1,CONSPOS>
*
    WS.BAL = WS.BAL + WS.BAL.YTD + WS.DR + WS.CR

    A.BAL.ARYEQ = R.CONSPL<RE.PTL.BALANCE>
    A.DR.ARYEQ  = R.CONSPL<RE.PTL.DEBIT.MOVEMENT>
    A.CR.ARYEQ  = R.CONSPL<RE.PTL.CREDIT.MOVEMENT>
    A.BAL.YTD.ARYEQ = R.CONSPL<RE.PTL.BALANCE.YTD>
    WS.BALEQ = A.BAL.ARYEQ<1,CONSPOS>
    WS.DREQ  = A.DR.ARYEQ<1,CONSPOS>
    WS.CREQ  = A.CR.ARYEQ<1,CONSPOS>
    WS.BAL.YTDEQ = A.BAL.YTD.ARYEQ<1,CONSPOS>
    WS.BALEQ = WS.BALEQ + WS.BAL.YTDEQ + WS.DREQ + WS.CREQ
    WS.BAL.EQV = WS.BALEQ
    GOSUB A.100.CHK.CATEG.D
    RETURN
**    END

**    IF WS.CY.CODE NE "EGP" AND WS.LINE.NO EQ 0630 THEN
**        A.BAL.ARY = R.CONSPL<RE.PTL.CCY.BALANCE>
**        A.DR.ARY  = R.CONSPL<RE.PTL.CCY.DEBIT.MVE>
**        A.CR.ARY  = R.CONSPL<RE.PTL.CCY.CREDT.MVE>
**        A.BAL.YTD.ARY = R.CONSPL<RE.PTL.CCY.BALANCE.YTD>
**        WS.BAL = A.BAL.ARY<1,CONSPOS>
**        WS.DR  = A.DR.ARY<1,CONSPOS>
**        WS.CR  =    A.CR.ARY<1,CONSPOS>
**        WS.BAL.YTD = A.BAL.YTD.ARY<1,CONSPOS>
***
**        WS.BAL = WS.BAL + WS.BAL.YTD + WS.DR + WS.CR
**
**        A.BAL.ARYEQ = R.CONSPL<RE.PTL.BALANCE>
**        A.DR.ARYEQ  = R.CONSPL<RE.PTL.DEBIT.MOVEMENT>
**        A.CR.ARYEQ  = R.CONSPL<RE.PTL.CREDIT.MOVEMENT>
**        A.BAL.YTD.ARYEQ = R.CONSPL<RE.PTL.BALANCE.YTD>
**        WS.BALEQ = A.BAL.ARYEQ<1,CONSPOS>
**        WS.DREQ  = A.DR.ARYEQ<1,CONSPOS>
**        WS.CREQ  = A.CR.ARYEQ<1,CONSPOS>
**        WS.BAL.YTDEQ = A.BAL.YTD.ARYEQ<1,CONSPOS>
**        WS.BALEQ = WS.BALEQ + WS.BAL.YTDEQ + WS.DREQ + WS.CREQ
**        WS.BAL.EQV = WS.BALEQ
**        GOSUB A.100.CHK.CATEG.D
**        RETURN
**    END
**************************************************

**    IF  WS.CY.CODE NE "EGP"  THEN
**        A.BAL.ARY = R.CONSPL<RE.PTL.CCY.BALANCE>
**        A.DR.ARY  = R.CONSPL<RE.PTL.CCY.DEBIT.MVE>
**        A.CR.ARY  = R.CONSPL<RE.PTL.CCY.CREDT.MVE>
**        A.BAL.YTD.ARY = R.CONSPL<RE.PTL.CCY.BALANCE.YTD>
**        WS.BAL = A.BAL.ARY<1,CONSPOS>
**        WS.DR  = A.DR.ARY<1,CONSPOS>
**        WS.CR  =    A.CR.ARY<1,CONSPOS>
**        WS.BAL.YTD = A.BAL.YTD.ARY<1,CONSPOS>
**        WS.BAL = WS.BAL + WS.BAL.YTD + WS.DR + WS.CR
**        GOSUB A.110.CY
**    END

    IF WS.CY.CODE NE "EGP" AND WS.LINE.NO EQ 9993 THEN
        A.BAL.ARY9 = R.CONSPL<RE.PTL.BALANCE>
        A.DR.ARY9  = R.CONSPL<RE.PTL.DEBIT.MOVEMENT>
        A.CR.ARY9  = R.CONSPL<RE.PTL.CREDIT.MOVEMENT>
        A.BAL.YTD.ARY9 = R.CONSPL<RE.PTL.BALANCE.YTD>

        WS.BAL9 = A.BAL.ARY9<1,CONSPOS>
        WS.DR9  = A.DR.ARY9<1,CONSPOS>
        WS.CR9  = A.CR.ARY9<1,CONSPOS>
        WS.BAL.YTD9 = A.BAL.YTD.ARY9<1,CONSPOS>
        WS.BAL9 = WS.BAL9 + WS.BAL.YTD9 + WS.DR9 + WS.CR9
        WS.BAL.EQV = WS.BAL9
    END

    GOSUB A.100.CHK.CATEG.D

    RETURN
***-------------------------------------------------------

A.100.CHK.CATEG.D:
    WS.CAT.ID = WS.BR.NO:"*":WS.LINE.NO:"*":WS.GL.CODE:"*":WS.CY.CODE
    MSG.CAT = ""
    CALL F.READ(FN.CAT,WS.CAT.ID,R.CAT,F.CAT,MSG.CAT)
    IF MSG.CAT NE ""  THEN
        GOSUB A.400.NEW.REC
    END
    IF MSG.CAT EQ ""  THEN
        GOSUB A.300.OLD.REC
    END

    RETURN
***-------------------------------------------------------
A.110.CY:
*    WS.KEY = "NZD"
*    CALL F.READ(FN.CCY,WS.KEY,R.CCY,F.CCY,MSG.CCY)
*    IF MSG.CCY EQ "" THEN
*        A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*        LOCATE WS.CY.CODE IN A.ARY<1,1> SETTING CYPOS ELSE NULL
*        WS.RATE.ARY = R.CCY<RE.BCP.RATE>
*        WS.RATE = WS.RATE.ARY<1,CYPOS>
*        WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
*        WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>
    GOSUB A.210.CALQ.EQV
*    END
    RETURN
*----------------------------------------------------------
A.210.CALQ.EQV:
*DEBUG
**    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
    WS.BAL.EQV = WS.BAL       ;* WS.RATE
    WS.BAL = WS.BAL.CUR
**    END
**    IF WS.MLT.DIVD NE "MULTIPLY" THEN
**        WS.BAL.EQV = WS.BAL / WS.RATE
**    END
    RETURN
*-----------------------------------------------------------
A.300.OLD.REC:
    WS.TEMP = R.CAT<CAT.CATD.BAL.IN.ACT.CY>
    WS.TEMP = WS.TEMP + WS.BAL
    R.CAT<CAT.CATD.BAL.IN.ACT.CY> = WS.TEMP

    WS.TEMP = R.CAT<CAT.CATD.BAL.IN.LOCAL.CY>
    WS.TEMP = WS.TEMP +  WS.BAL.EQV
    R.CAT<CAT.CATD.BAL.IN.LOCAL.CY> = WS.TEMP
    CALL F.WRITE(FN.CAT,WS.CAT.ID,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CAT.ID)
    RETURN
*--------------------------------------------------------
A.400.NEW.REC:
    R.CAT<CAT.CATD.BAL.IN.ACT.CY> = WS.BAL
    R.CAT<CAT.CATD.BAL.IN.LOCAL.CY> =  WS.BAL.EQV
    R.CAT<CAT.CATD.CATEG.CODE>      =  WS.GL.CODE
    R.CAT<CAT.CATD.ASST.LIAB>       =  WS.LINE.NO
    R.CAT<CAT.CATD.CY.ALPH.CODE>    =  WS.CY.CODE
    R.CAT<CAT.CATD.BR>       =  WS.BR.NO
    R.CAT<CAT.RESERVED2> = WS.TODAY
    CALL F.WRITE(FN.CAT,WS.CAT.ID,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CAT.ID)
    RETURN
***-----------------
FIX.NZD:
    LIN.ID = 'AC.1.TR.NZD.14900..............EG0010099'
    FN.NZ = "FB99.CONS.ASST.LIAB.NZD"
    F.NZ  = ""
    CALL OPF (FN.NZ,F.NZ)
    CALL F.READ(FN.NZ,LIN.ID,R.NZ,F.NZ,EZ)

    IF R.NZ<20> EQ '' THEN
        AMMT = R.NZ<21>
    END ELSE
        AMMT = R.NZ<20>
    END

    R.CAT<CAT.CATD.BAL.IN.ACT.CY>   = ""
    R.CAT<CAT.CATD.BAL.IN.LOCAL.CY> =  AMMT
    R.CAT<CAT.CATD.CATEG.CODE>      = '20010'
    R.CAT<CAT.CATD.ASST.LIAB>       = '1045'
    R.CAT<CAT.CATD.CY.ALPH.CODE>    =  'NZD'
    R.CAT<CAT.CATD.BR>              = "EG0010099"
    R.CAT<CAT.RESERVED2>            = WS.TODAY
    WS.CAT.ID = 'EG0010099*1045*14900*NZD'
    CALL F.WRITE(FN.CAT,WS.CAT.ID,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CAT.ID)

    RETURN
***------------------
END
