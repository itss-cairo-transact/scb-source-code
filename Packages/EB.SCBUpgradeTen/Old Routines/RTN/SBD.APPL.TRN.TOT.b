* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-193</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.APPL.TRN.TOT
*    PROGRAM SBD.APPL.TRN.TOT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PROTOCOL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.APPLICATION.NAME
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS


    PRV_COMP_ID = ''
    GOSUB INITIATE

    GOSUB PRINT.HEAD
*Line [ 48 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
* TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.APPL.TRN.TOT'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*=============================================================
CALLDB:
    FN.PROT = 'F.PROTOCOL'             ; F.PROT = ''    ;R.PROT = ''
    FN.PROT.1 = 'F.PROTOCOL'             ; F.PROT.1 = '' ;R.PROT.1 = ''
    FN.APPL = 'F.SCB.APPLICATION.NAME' ; F.APPL = ''
    PRV_APPL =''
    NO.TRN.1 = 0
    EMP.TOT = 0
    TOT.REV = 0
    ENQ.NO.1 = 0
    OPCE.NO.1 = 0
    TOT.TRN.ALL = 0
    TOT.TRN.BFR.4 = 0
    PRT.OPCE = 0
    OPCE.ENQ.1 = 0
    OPCE.DEL.1 = 0

    CALL OPF(FN.PROT,F.PROT)
    CALL OPF(FN.APPL,F.APPL)

    T.SEL = "SELECT F.PROTOCOL WITH (LEVEL.FUNCTION LIKE ...A... AND ID NE '' AND ID UNLIKE TT... ) OR (LEVEL.FUNCTION LIKE ...I... AND ID NE '' AND ID LIKE TT... ) OR (APPLICATION LIKE ...ENQUIRY...) BY COMPANY.ID"
*    T.SEL = "SELECT F.PROTOCOL WITH  COMPANY.ID EQ EG0010013 AND LEVEL.FUNCTION LIKE ...A... AND ID NE '' BY APPLICATION"
*   T.SEL = "SELECT F.PROTOCOL WITH  PROCESS.DATE EQ 20091207 AND COMPANY.ID EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND LEVEL.FUNCTION LIKE ...A... AND ID NE '' BY APPLICATION"
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*TEXT =SELECTED ;CALL REM
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.PROT,KEY.LIST<I>,R.PROT,F.PROT,E1)

            IF ((PRV_COMP_ID NE R.PROT<EB.PTL.COMPANY.ID>) AND I GT 1) OR I EQ SELECTED  THEN
*=================================================
                BRN.NO = PRV_COMP_ID[2]
                BRN.NO = TRIM(BRN.NO ,'0',"L")
*------------------- TO GET NAME OF BR  -------------------
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,PRV_COMP_ID,BRANCH)
*------------------- TO GET NO OF EMP   --------------------
                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.TELEX.NO,BRN.NO,EMP.NO)
*------------------- TO GET LAST 2 TRNS --------------------
                T.SEL.1 = "SELECT F.PROTOCOL WITH  LEVEL.FUNCTION LIKE ...A... AND ID NE '' AND COMPANY.ID EQ ":PRV_COMP_ID:" BY TIME"
                CALL EB.READLIST(T.SEL.1,KEY.LIST1,"",SELECTED1,ER.MSG1)
                CALL F.READ(FN.PROT.1,KEY.LIST1<SELECTED1>,R.PROT.1,F.PROT.1,E1.1)
                IF EMP.NO = '' THEN EMP.NO = 0
                LST.TRN = R.PROT.1<EB.PTL.TIME>[1,2]:":":R.PROT.1<EB.PTL.TIME>[3,2]:":":R.PROT.1<EB.PTL.TIME>[5,2]
                SELECTED2 = SELECTED1 - 1
                CALL F.READ(FN.PROT.1,KEY.LIST1<SELECTED2>,R.PROT.1,F.PROT.1,E1.1)
                B.LST.TRN = R.PROT.1<EB.PTL.TIME>[1,2]:":":R.PROT.1<EB.PTL.TIME>[3,2]:":":R.PROT.1<EB.PTL.TIME>[5,2]
*-------------------- TO GET NO OF TRN AFTER 4 --------------
                FOR CONT.1 = 1 TO SELECTED1
                    CALL F.READ(FN.PROT.1,KEY.LIST1<CONT.1>,R.PROT.1,F.PROT.1,E1.1)
                    IF R.PROT.1<EB.PTL.TIME> LT "160000" THEN
                        TRN.BFR.4 = SELECTED1 - CONT.1
                    END
                NEXT CONT.1
*-----------------------------------------------------------
                EMP.TOT = EMP.TOT + EMP.NO
                TOT.TRN.BFR.4 = TOT.TRN.BFR.4 + TRN.BFR.4
*-------------------- TO GET REV. TRN  ---------------------
                T.SEL3 = "SELECT F.PROTOCOL WITH  LEVEL.FUNCTION LIKE ...R... AND ID NE '' AND APPLICATION UNLIKE ENQ... AND REMARK UNLIKE '...AUTOMATIC LOGOUT...' AND COMPANY.ID EQ ":PRV_COMP_ID
                CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
                TOT.REV = TOT.REV + SELECTED3
                NO.TRN.1 = NO.TRN.1 + 1
                TOT.TRN.ALL = TOT.TRN.ALL + NO.TRN.1
*-----------------------------------------------------------
                PRINT
                NO.TRN = 0
                IF PRV_COMP_ID EQ 'EG0010088' THEN
                    OPCE.ENQ.1 = ENQ.NO.1
                    OPCE.DEL.1 = SELECTED3
                    OPCE.TRN.1 = NO.TRN.1
*       NO.TRN.1 = NO.TRN.1 + OPCE.NO.1
*  NO.TRN.1 = OPCE.NO.1
*      PRT.OPCE = 1
                END
                IF PRV_COMP_ID NE 'EG0010088' THEN
                    XX = SPACE(120)
                    XX<1,1>[1,5]   = PRV_COMP_ID
                    XX<1,1>[15,25]   = BRANCH
                    XX<1,1>[40,6]   = NO.TRN.1
                    XX<1,1>[52,3]   = ENQ.NO.1
                    XX<1,1>[65,3]   = SELECTED3
                    XX<1,1>[80,4]   = EMP.NO
                    XX<1,1>[95,8]   = B.LST.TRN
                    XX<1,1>[110,8]  = LST.TRN
                    XX<1,1>[125,3]  = TRN.BFR.4
                    PRINT XX<1,1>
                END
                NO.TRN.1 = 0
                ENQ.NO.1 = 0

*  PRV_COMP_ID = R.PROT<EB.PTL.COMPANY.ID>
            END
            ELSE
                APPL = FIELD(R.PROT<EB.PTL.APPLICATION>,",",1)
                BEGIN CASE
                CASE APPL EQ 'ENQUIRY.SELECT'
                    ENQ.NO.1 = ENQ.NO.1 + 1
                CASE APPL EQ 'LETTER.OF.CREDIT'
                    OPCE.NO.1 = OPCE.NO.1 + 1
                CASE APPL EQ 'DRAWINGS'
                    OPCE.NO.1 = OPCE.NO.1 + 1
                CASE OTHERWISE
                    NO.TRN.1 = NO.TRN.1 + 1
                END CASE
            END
            PRV_COMP_ID = R.PROT<EB.PTL.COMPANY.ID>
        NEXT I
*   IF PRT.OPCE EQ 0 THEN
        OPCE.NO.1 = OPCE.NO.1 + OPCE.TRN.1
        PRINT ; PRINT "EG0010088    ���� ��������  ":SPACE(11):OPCE.NO.1:SPACE(10):OPCE.ENQ.1:SPACE(11):OPCE.DEL.1
*   END
        TOT.TRN.ALL = TOT.TRN.ALL + OPCE.NO.1 - OPCE.TRN.1
        PRINT STR('=',125)
        PRINT ; PRINT SPACE(5):" ������ ������ ����� = ":TOT.TRN.ALL:" �����":SPACE(10):"������� ":TOT.REV:SPACE(10):"������ ��� ��������  ":EMP.TOT:SPACE(10):"����� ��� 4  ":TOT.TRN.BFR.4
        PRINT STR('=',125)
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*     CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,PRV_COMP_ID,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
*    YYBRN = BRANCH
    DATY = TODAY
*   T.DAY = DATY[3,2]:'/':DATY[5,2]:"/":DATY[0,4]
    T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):"SBD.APPL.TRN.TOT"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):"���� ���� �������� ������� ������  ":T.DAY

*   PR.HD :="'L'":SPACE(50): " �� ������ " :ST.DATE :" ��� " : EN.DATE
    PR.HD :="'L'":SPACE(40):STR('_',55)


    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(20):" �����" :SPACE(11):" �����":SPACE(7):"�������":SPACE(5):"�������":SPACE(5):"�����������":SPACE(11):"��� ���� ��������":SPACE(10):"������ ��� 4"
*    PR.HD := "'L'" : " ��� ������  " : SPACE (3) : " ��� ������  "  : SPACE (8) : " ����� ������   " : SPACE(3) : "  ����� ����� ������� " :  SPACE(3) : " ������  " : SPACE(2) :"�.������� - �.�������"
    PR.HD :="'L'":STR('_',128)
    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
