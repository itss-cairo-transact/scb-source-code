* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.TRANS.TODAY.A.R2(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-----------------------------------
    SYS.DATE   = TODAY
    WORK.DATE   = SYS.DATE
    WORK.DATE  = 20081127
*------------------------
    FN.TRANS = "F.FUNDS.TRANSFER$HIS"
    F.TRANS = ''
    R.TRANS=''
    Y.TRANS=''
    Y.TRANS.ERR=''

    CALL OPF(FN.TRANS,F.TRANS)
*------------------------





    SEL.CMD   = "SELECT ":FN.TRANS:" WITH CO.CODE EQ ":COMP
    SEL.CMD  := "AND PROCESSING.DATE GE ":WORK.DATE
    SEL.CMD  := " AND DEBIT.AMOUNT GT 0"
    SEL.CMD  := " AND ((DEBIT.ACCT.NO GE  PL51000 AND DEBIT.ACCT.NO LE PL54031)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO GE PL54033 AND DEBIT.ACCT.NO LE PL59999)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO GE PL16511 AND DEBIT.ACCT.NO LE PL16512)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL11057)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL10200)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL11052)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL11050)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16201)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16205)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16208)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16250)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16500)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16504)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16506)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16508)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16521)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL11054)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL11058)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16110)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16186)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16520)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16426)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL3050)"
    SEL.CMD  := " OR (DEBIT.ACCT.NO EQ PL16513))"

    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)

    IF NOREC >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
    RETURN

END
