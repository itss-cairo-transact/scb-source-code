* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*---------------SAMY EMBABY--------------------------------------------------------------
* <Rating>208</Rating>
*----M BAKRY---------MOHAMED MOSTAFA--------2012/10/22------------------------
*    PROGRAM SBD.DPST.NATIONALTY.TOTAL.ALL
    SUBROUTINE SBD.DPST.NATIONALTY.TOTAL.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
***** CURRENCY SELECTION *************
*    T.SEL2 = "SELECT F.COUNTRY WITH @ID EQ 'LB' BY @ID"
    T.SEL2 = "SELECT F.COUNTRY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CUR.ID = ''
    IF SELECTED2 THEN
        FOR NN1 = 1 TO SELECTED2
            CUR.ID<1,NN1> = ''
        NEXT NN1
        FOR NN = 1 TO SELECTED2
            CALL F.READ(FN.CO,KEY.LIST2<NN>,R.CO,F.CO,E1)
*            CUR.ID<1,NN> = "'":KEY.LIST2<NN>:"'"
            CUR.ID<1,NN> = KEY.LIST2<NN>
        NEXT NN
    END
    GOSUB PROCESS.MM
    GOSUB PROCESS.AC
    GOSUB PRINT.REC

    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SBD.ACC.DPST.N.TOTAL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBD.ACC.DPST.N.TOTAL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBD.ACC.DPST.N.TOTAL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBD.ACC.DPST.N.TOTAL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBD.ACC.DPST.N.TOTAL.CSV File IN &SAVEDLISTS&'
        END
    END

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CO = 'F.COUNTRY' ; F.CO = '' ; R.CO = ''
    CALL OPF(FN.CO,F.CO)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD)
*    TD = TODAY
*    CALL CDT ('',TD,'-1W')
    DAT.HED = FMT(TD,"####/##/##")

*    HEAD1 = "COUNTRY.DEPOSITS"
    HEAD1 = "DATE  : "
    HEAD.DESC  = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC  = "COUNTRY.NAME":","
    HEAD.DESC := "CURRENT.ACCOUNT":","
    HEAD.DESC := "DEPOSITS":","
    HEAD.DESC := "TOTAL":","
    HEAD.DESC := "10%":","
    HEAD.DESC := "AVAILABLE USE":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

*========================================================================
PROCESS.MM:
*IF CUR.ID EQ 'US' THEN
    CUR.EQV  = 0 ; DEP.EQV = 0
*-------------------------------------------
******* MM SELECTION *************
    T.SEL = "SELECT ":FN.MM:" WITH CATEGORY EQ 21076 AND MATURITY.DATE GT ":TD:" AND VALUE.DATE LE ":TD
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.MM,KEY.LIST<I>,R.MM,F.MM,E1)
            MM.CURR = R.MM<MM.CURRENCY>
            AMT = R.MM<MM.PRINCIPAL>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 146 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE MM.CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            DEP.EQV1 = 0

            DEP.EQV1 = AMT * RATE

            CHK.CONT = R.MM<MM.COUNTRY.RISK>
            LOCATE  CHK.CONT IN CUR.ID<1,1> SETTING POS1 THEN
                DEP.EQV<1,POS1> += DEP.EQV1
            END
        NEXT I

    END
*    FOR KK1 = 1 TO SELECTED2
*        IF DEP.EQV<1,KK1> NE '' THEN
*            PRINT CUR.ID<1,KK1>:"  ===   ":DEP.EQV<1,KK1>
*        END
*    NEXT KK1
    PRINT "FINISH MONEY.MARKET"
    RETURN
*=============================================================================================================================
PROCESS.AC:
*----------
******* SIGHT SELECTION *************
    T.SEL  = "SELECT ":FN.AC:" WITH (CATEGORY EQ 5001 OR CATEGORY EQ 5020) AND OPEN.ACTUAL.BAL LT 0 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            AMT = R.AC<AC.OPEN.ACTUAL.BAL>
            AC.CURR = R.AC<AC.CURRENCY>
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 180 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE AC.CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>

*            CUR.EQV += AMT * RATE
            CUR.EQV1 = 0
            CUR.EQV1 = AMT * RATE

            CALL DBR ('CUSTOMER':@FM:EB.CUS.RESIDENCE,R.AC<AC.CUSTOMER>,CHK.CONT)
*          CHK.CONT = R.AC<AC.RESIDENCE>
            LOCATE  CHK.CONT IN CUR.ID<1,1> SETTING POS1 THEN
                CUR.EQV<1,POS1> += CUR.EQV1
            END
        NEXT I
    END

*    FOR KK2 = 1 TO SELECTED2
*        IF DEP.EQV<1,KK2> NE '' OR CUR.EQV<1,KK2> NE '' THEN
*            PRINT CUR.ID<1,KK2>:"  ===   ":CUR.EQV<1,KK2> :"  ===  ":DEP.EQV<1,KK2>
*        END
*    NEXT KK2
    RETURN
*==========================================================================================================
PRINT.REC:
*=========
    CHK.ALL.AMT.1 = 0
    FOR KK3 = 1 TO SELECTED2
        IF DEP.EQV<1,KK3> NE '' OR CUR.EQV<1,KK3> NE '' OR DEP.EQV<1,KK3> NE 0 OR CUR.EQV<1,KK3> NE 0 THEN
            CHK.ALL.AMT.1 += (DEP.EQV<1,KK3> + ( CUR.EQV<1,KK3> * -1 ))
            PRINT CHK.ALL.AMT.1
*DEBUG
        END
    NEXT KK3
    CHK.ALL.AMT.1 = CHK.ALL.AMT.1 * 10 / 100
    CHK.ALL.AMT.1 = CHK.ALL.AMT.1 / 1000
    CHK.ALL.AMT.1 = DROUND(CHK.ALL.AMT.1,0)

*PRINT "BAKKKKKKKKKKKK   " :CHK.ALL.AMT.1
    FOR KK2 = 1 TO SELECTED2
        IF DEP.EQV<1,KK2> NE 0 OR CUR.EQV<1,KK2> NE 0 OR DEP.EQV<1,KK2> NE 0 OR CUR.EQV<1,KK2> NE 0 THEN
            CALL F.READ(FN.CO,KEY.LIST2<KK2>,R.CO,F.CO,E1)
            COUNTRY.NAME = R.CO<EB.COU.COUNTRY.NAME,1>

            IF CUR.EQV<1,KK2> LT 0 THEN CUR.EQV<1,KK2> = CUR.EQV<1,KK2> * -1
            IF DEP.EQV<1,KK2> LT 0 THEN DEP.EQV<1,KK2> = DEP.EQV<1,KK2> * -1

            ALL.AMT<1,KK2> = DEP.EQV<1,KK2> + CUR.EQV<1,KK2>
            CUR.EQV<1,KK2> = CUR.EQV<1,KK2> / 1000
            DEP.EQV<1,KK2> = DEP.EQV<1,KK2> / 1000
            ALL.AMT<1,KK2> = ALL.AMT<1,KK2> / 1000

            CUR.EQV<1,KK2> = DROUND(CUR.EQV<1,KK2>,'0')
            DEP.EQV<1,KK2> = DROUND(DEP.EQV<1,KK2>,'0')
            ALL.AMT<1,KK2> = DROUND(ALL.AMT<1,KK2>,'0')
*****************************************************************************
            PERCENT<1,KK2> = ALL.AMT<1,KK2> * 10 / 100
            PERCENT<1,KK2> = DROUND(PERCENT<1,KK2>,'0')
            TOTAL.PERCENT += PERCENT<1,KK2>
            TOTAL.ALL.AMT += ALL.AMT<1,KK2>
            AVAILABLE.USE<1,KK2> = (CHK.ALL.AMT.1 - ALL.AMT<1,KK2>)
*---------------------------------------------------------------------------
            TOTAL.CUR.EQV += CUR.EQV<1,KK2>
            TOTAL.DEP.EQV += DEP.EQV<1,KK2>
            CRT TOTAL.CUR.EQV<1,KK2>
*CRT TOTAL.AVAILABLE.USE
*CRT ALL.AMT<1,KK2>

*****************************************************************************
            BB.DATA  = COUNTRY.NAME:","
            BB.DATA := CUR.EQV<1,KK2>:","
            BB.DATA := DEP.EQV<1,KK2>:","
            BB.DATA := ALL.AMT<1,KK2>:","
            BB.DATA := ","
            BB.DATA := AVAILABLE.USE<1,KK2>:","
****            BB.DATA := PERCENT<1,KK2>:","
*******************************************************************************
            IF ALL.AMT<1,KK2> NE 0 THEN
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

            END
        END
    NEXT KK2
    IF NN EQ SELECTED2 THEN
        BB.DATA = ' '
        BB.DATA = ","
        BB.DATA := TOTAL.CUR.EQV:","
*        BB.DATA := ","
        BB.DATA := TOTAL.DEP.EQV:","
        BB.DATA := ","
*        BB.DATA := TOTAL.ALL.AMT:","
        BB.DATA := TOTAL.PERCENT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

    END

*********************************
    RETURN
END
