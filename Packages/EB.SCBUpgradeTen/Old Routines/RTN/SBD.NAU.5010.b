* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-- COPIED FROM SBD.REPORT.NOTAUTH1.NEW
    SUBROUTINE SBD.NAU.5010
*    PROGRAM SBD.NAU.5010

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FOREX
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PAYMENT.STOP
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.CHARGE.REQUEST
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*Line [ 80 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DRMNT.FILES
*Line [ 82 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 84 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USR.LOCAL.REF
*Line [ 86 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.APP
*Line [ 88 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*-----------------------------------------------------
    GOSUB INITIATE
    GOSUB SUB.OPEN
    GOSUB PRINT.HEAD
    GOSUB PROCESS
*   GOSUB PRINT.END.LINE

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "REPORT PRINTED" ; CALL REM
    RETURN
*========================================================================
PRINT.END.LINE:
*--------------
    TOT.XX  = ""
    TOT.XX  = XX1+XX2+XX3+XX4+XX5+XX6+XX7+XX8+XX9+XX10
    TOT.XX += XX11+XX12+XX13+XX14+XX15+XX16+XX17+XX18+XX19+XX20
    TOT.XX += XX21+XX22+XX23+XX24+XX25+XX26+XX27

    IF TOT.XX EQ 0 THEN
        PRINT SPACE(50):"�� ���� ������ ��� �����"
    END

    PRINT " "
    PRINT SPACE(40) :"*********** ": "����� �������"  : " ***********"
    RETURN
*----------------------------
INITIATE:
*--------
* COMP      = ID.COMPANY
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*========================================================================
SUB.OPEN:
*--------
    FN.USER   = 'F.USER'     ; F.USER = ''

    FN.CU     = 'FBNK.CUSTOMER$NAU'           ; F.CU = ''
    FN.AC     = 'FBNK.ACCOUNT$NAU'            ; F.AC = ''
    FN.DRAW   = 'FBNK.DRAWINGS$NAU'           ; F.DRAW = ''
    FN.LETTER = 'FBNK.LETTER.OF.CREDIT$NAU'   ; F.LETTER = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE$NAU'     ; F.IM = ''
    FN.FT     = 'FBNK.FUNDS.TRANSFER$NAU'     ; F.FT = ''
    FN.TT     = 'FBNK.TELLER$NAU'             ; F.TT = ''
    FN.IN     = 'F.INF.MULTI.TXN$NAU'         ; F.IN = ''
    FN.CLOSE  = 'FBNK.ACCOUNT.CLOSURE$NAU'    ; F.CLOSE = ''
    FN.MM     = 'FBNK.MM.MONEY.MARKET$NAU'    ; F.MM = ''
    FN.FOR    = 'FBNK.FOREX$NAU'              ; F.FOR = ''
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
    FN.BT     = 'F.SCB.BT.BATCH$NAU'             ; F.BT = ''
    FN.BR     = 'FBNK.BILL.REGISTER$NAU'             ; F.BR = ''
    FN.CBE    = 'F.SCB.MONTHLY.PAY.CBE$NAU'        ; F.CBE = ''
    FN.DOC    = 'F.SCB.DOCUMENT.PROCURE$NAU'      ; F.DOC = ''
    FN.CL     = 'F.SCB.DEPT.SAMPLE1$NAU'          ; F.CL = ''
    FN.CARD   = 'FBNK.CARD.ISSUE$NAU'             ; F.CARD = ''
    FN.LIM    = 'FBNK.LIMIT$NAU'                  ; F.LIM = ''
    FN.COLL   = 'FBNK.COLLATERAL$NAU'             ; F.COLL = ''
    FN.COMP   = 'F.COMPANY'                    ; F.COMP = ''
    FN.COR    = 'FBNK.COLLATERAL.RIGHT$NAU'       ; F.COR = ''
    FN.PAY.STOP='FBNK.PAYMENT.STOP$NAU'               ; F.PAY.STOP = ''
    FN.FATCA  = 'FBNK.FATCA.CUSTOMER.SUPPLEMENTARY.INFO$NAU'    ; F.FATCA = ''
    FN.US     = 'F.SCB.MODIFY.USER.AUTHORITY$NAU'                   ; F.US =''
    FN.DRMNT  = "F.SCB.DRMNT.FILES$NAU"                             ; F.DRMNT = ""
    FN.AC.CH  = "F.AC.CHARGE.REQUEST$NAU"                               ; F.AC.CH =""
    FN.ATM.APP    = "F.SCB.ATM.APP$NAU" ; F.ATM.APP = ""
    CALL OPF(FN.USER,F.USER)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.DRAW,F.DRAW)
    CALL OPF(FN.LETTER,F.LETTER)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.CLOSE,F.CLOSE)
    CALL OPF(FN.MM,F.MM)
    CALL OPF(FN.FOR,F.FOR)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.BT,F.BT)
    CALL OPF(FN.BR,F.BR)
    CALL OPF(FN.CBE,F.CBE)
    CALL OPF (FN.CL,F.CL)
    CALL OPF(FN.CARD,F.CARD)
    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.DOC,F.DOC)
    CALL OPF(FN.COLL,F.COLL)
    CALL OPF(FN.COR,F.COR)
    CALL OPF(FN.PAY.STOP,F.PAY.STOP)
    CALL OPF(FN.FATCA,F.FATCA)
    CALL OPF(FN.US,F.US)
    CALL OPF(FN.DRMNT,F.DRMNT)
    CALL OPF(FN.AC.CH,F.AC.CH)
    CALL OPF(FN.ATM.APP,F.ATM.APP)
    CALL OPF(FN.COMP,F.COMP)
    RETURN
*-----------------------------------------------
PROCESS:
*--------
    T.SEL1 = "SELECT FBNK.CUSTOMER$NAU"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.CU,KEY.LIST1<I>,R.CU,F.CU,E.CU)
            ID = KEY.LIST1<I>
            CO.CODE = R.CU<EB.CUS.COMPANY.BOOK>
            INP1    = R.CU<EB.CUS.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>


            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "CUSTOMER"
                XX.ARRAY<1,1>[50,20] = "�����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.CU<EB.CUS.ACCOUNT.OFFICER>,BRANCH)
            YYBRN = FIELD(BRANCH,'.',2)

            IF WS.SCB.DEPT EQ '5010' THEN
                XX1 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX1
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = YYBRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END

    IF SELECTED1 EQ 0 THEN

        XX1 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "CUSTOMER"
        XX.ARRAY<1,1>[50,20] = "�����"
        XX.ARRAY<1,1>[80,40]  = XX1
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
***********************************************************
    T.SEL2 = "SELECT FBNK.ACCOUNT$NAU"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
    IF SELECTED2 THEN
        FOR I = 1 TO SELECTED2
            CALL F.READ(FN.AC,KEY.LIST2<I>,R.AC,F.AC,E.AC)
            ID = KEY.LIST2<I>
            CO.CODE = R.AC<AC.CO.CODE>
            INP1    = R.AC<AC.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "ACCOUNT"
                XX.ARRAY<1,1>[50,20] = "������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END

            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX2 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX2
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED2 EQ 0 THEN

        XX2 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "ACCOUNT"
        XX.ARRAY<1,1>[50,20] = "������"
        XX.ARRAY<1,1>[80,40]  = XX2
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
******************************************************
    T.SEL3 = "SELECT FBNK.DRAWINGS$NAU"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG)

    IF SELECTED3 THEN
        FOR I = 1 TO SELECTED3
            CALL F.READ(FN.DRAW,KEY.LIST3<I>,R.DRAW,F.DRAW,E.DRAW)
            ID = KEY.LIST3<I>
            CO.CODE = R.DRAW<TF.DR.CO.CODE>
            INP1    = R.DRAW<TF.DR.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "DRAWING"
                XX.ARRAY<1,1>[50,20] = "������� ����� - ��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX3 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX3
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED3 EQ 0 THEN

        XX3 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "DRAWING"
        XX.ARRAY<1,1>[50,20] = "������� ����� -��������"
        XX.ARRAY<1,1>[80,40]  = XX3
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
********************************
    T.SEL4 = "SELECT FBNK.LETTER.OF.CREDIT$NAU"
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG)

    IF SELECTED4 THEN
        FOR I = 1 TO SELECTED4
            CALL F.READ(FN.LETTER,KEY.LIST4<I>,R.LETTER,F.LETTER,E.LETTER)
            ID = KEY.LIST4<I>
            CO.CODE = R.LETTER<TF.LC.CO.CODE>
            INP1    = R.LETTER<TF.LC.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "LETTER OF CREDIT"
                XX.ARRAY<1,1>[50,20] = "��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX4 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX4
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED4 EQ 0 THEN

        XX4 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "LETTER OF CREDIT"
        XX.ARRAY<1,1>[50,20] = "��������"
        XX.ARRAY<1,1>[80,40]  = XX4
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
********************************
    T.SEL5 = "SELECT F.IM.DOCUMENT.IMAGE$NAU"
    CALL EB.READLIST(T.SEL5,KEY.LIST5,"",SELECTED5,ER.MSG)

    IF SELECTED5 THEN
        FOR I = 1 TO SELECTED5
            CALL F.READ(FN.IM,KEY.LIST5<I>,R.IM,F.IM,E.IM)
            ID = KEY.LIST5<I>
            CO.CODE = R.IM<IM.DOC.CO.CODE>
            INP1    = R.IM<IM.DOC.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "IM.DOCUMENT.IMAGE"
                XX.ARRAY<1,1>[50,20] = "������� �������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END

            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX5 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX5
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED5 EQ 0 THEN

        XX5 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "IM.DOCUMENT.IMAGE"
        XX.ARRAY<1,1>[50,20] = "������"
        XX.ARRAY<1,1>[80,40]  = XX5
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END

**************************************************************

    T.SEL6 = "SELECT FBNK.FUNDS.TRANSFER$NAU "
    CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ER.MSG)
    IF SELECTED6 THEN
        FOR I = 1 TO SELECTED6
            CALL F.READ(FN.FT,KEY.LIST6<I>,R.FT,F.FT,E.FT)
            ID = KEY.LIST6<I>
            CO.CODE = R.FT<FT.CO.CODE>
            INP1    = R.FT<FT.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "FUNDS.TRANSFER"
                XX.ARRAY<1,1>[50,20] = "���� ����� ��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX6 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX6
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I

    END
    IF SELECTED6 EQ 0 THEN

        XX6 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "FUNDS.TRANSFER"
        XX.ARRAY<1,1>[50,20] = "���� ����� ��������"
        XX.ARRAY<1,1>[80,40]  = XX6
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
********************************************************
    T.SEL7 = "SELECT FBNK.TELLER$NAU"
    CALL EB.READLIST(T.SEL7,KEY.LIST7,"",SELECTED7,ER.MSG)
    IF SELECTED7 THEN
        FOR I = 1 TO SELECTED7
            CALL F.READ(FN.TT,KEY.LIST7<I>,R.TT,F.TT,E.TT)
            ID = KEY.LIST7<I>
            CO.CODE = R.TT<TT.TE.CO.CODE>
            INP1    = R.TT<TT.TE.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "TELLER"
                XX.ARRAY<1,1>[50,20] = "������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX7 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX7
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED7 EQ 0 THEN

        XX7 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "TELLER"
        XX.ARRAY<1,1>[50,20] = "������"
        XX.ARRAY<1,1>[80,40]  = XX7
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
**********************************************************
    T.SEL8 = "SELECT F.INF.MULTI.TXN$NAU"
    CALL EB.READLIST(T.SEL8,KEY.LIST8,"",SELECTED8,ER.MSG)
    IF SELECTED8 THEN
        FOR I = 1 TO SELECTED8
            CALL F.READ(FN.IN,KEY.LIST8<I>,R.IN,F.IN,E.IN)
            ID = KEY.LIST8<I>
            CO.CODE = R.IN<INF.MLT.CO.CODE>
            INP1    = R.IN<INF.MLT.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "INF.MULTI.TXN"
                XX.ARRAY<1,1>[50,20] = "������ �������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END

            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX8 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX8
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I

    END
    IF SELECTED8 EQ 0 THEN

        XX8 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "INF.MULTI.TXN"
        XX.ARRAY<1,1>[50,20] = "������ �������"
        XX.ARRAY<1,1>[80,40]  = XX8
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
*********************************************************************************
    T.SEL9 = "SELECT FBNK.ACCOUNT.CLOSURE$NAU"
    CALL EB.READLIST(T.SEL9,KEY.LIST9,"",SELECTED9,ER.MSG)
    IF SELECTED9 THEN
        FOR I = 1 TO SELECTED9
            CALL F.READ(FN.CLOSE,KEY.LIST9<I>,R.CLOSE,F.CLOSE,E.CLOSE)
            ID = KEY.LIST9<I>
            CO.CODE = R.CLOSE<AC.ACL.CO.CODE>
            INP1    = R.CLOSE<AC.ACL.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "ACCOUNT.CLOSURE"
                XX.ARRAY<1,1>[50,20] = "��� ������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END

            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX9 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX9
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I

    END
    IF SELECTED9 EQ 0 THEN

        XX9 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "ACCOUNT.CLOSURE"
        XX.ARRAY<1,1>[50,20] = "��� ������"
        XX.ARRAY<1,1>[80,40]  = XX9
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END

************************************************
    T.SEL10 = "SELECT FBNK.MM.MONEY.MARKET$NAU "
    CALL EB.READLIST(T.SEL10,KEY.LIST10,"",SELECTED10,ER.MSG)
    IF SELECTED10 THEN
        FOR I = 1 TO SELECTED10
            CALL F.READ(FN.MM,KEY.LIST10<I>,R.MM,F.MM,E.MM)
            ID = KEY.LIST10<I>
            CO.CODE = R.MM<MM.CO.CODE>
            INP1    = R.MM<MM.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "MM.MONEY.MARKET"
                XX.ARRAY<1,1>[50,20] = "��� �����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX10 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX10
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I

    END
    IF SELECTED10 EQ 0 THEN

        XX10 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "MM.MONEY.MARKET"
        XX.ARRAY<1,1>[50,20] = "��� �����"
        XX.ARRAY<1,1>[80,40]  = XX10
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
**********************************************************8
    T.SEL11 = "SELECT FBNK.FOREX$NAU "
    CALL EB.READLIST(T.SEL11,KEY.LIST11,"",SELECTED11,ER.MSG)

    IF SELECTED11 THEN
        FOR I = 1 TO SELECTED11
            CALL F.READ(FN.FOR,KEY.LIST11<I>,R.FOR,F.FOR,E.FOR)
            ID = KEY.LIST11<I>
            CO.CODE = R.FOR<FX.CO.CODE>
            INP1    = R.FOR<FX.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "FOREX"
                XX.ARRAY<1,1>[50,20] = "������ ����� �������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX11 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX11
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END


        NEXT I
    END
    IF SELECTED11 EQ 0 THEN
        XX11 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "FOREX"
        XX.ARRAY<1,1>[50,20] = "������ ����� �������"
        XX.ARRAY<1,1>[80,40]  = XX11
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
***************************************************************

    T.SEL12 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21016 AND CATEGORY LE 21032 "
    CALL EB.READLIST(T.SEL12,KEY.LIST12,"",SELECTED12,ER.MSG)
    IF SELECTED12 THEN
        FOR I = 1 TO SELECTED12
            CALL F.READ(FN.LD,KEY.LIST12<I>,R.LD,F.LD,E.LD)
            ID = KEY.LIST12<I>
            CO.CODE = R.LD<LD.CO.CODE>
            INP1    = R.LD<LD.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "LD.LOANS.AND.DEPOSITS"
                XX.ARRAY<1,1>[50,20] = "������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX12 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX12
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED12 EQ 0 THEN
        XX12 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "LD.LOANS.AND.DEPOSITS"
        XX.ARRAY<1,1>[50,20] = "������"
        XX.ARRAY<1,1>[80,40]  = XX12
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
****************************************************************************
    T.SEL13 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21001 AND CATEGORY LE 21015"
    CALL EB.READLIST(T.SEL13,KEY.LIST13,"",SELECTED13,ER.MSG)

    IF SELECTED13 THEN
        FOR I = 1 TO SELECTED13
            CALL F.READ(FN.LD,KEY.LIST13<I>,R.LD,F.LD,E.LD)
            ID = KEY.LIST13<I>
            CO.CODE = R.LD<LD.CO.CODE>
            INP1    = R.LD<LD.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "LD.LOANS.AND.DEPOSITS"
                XX.ARRAY<1,1>[50,20] = "�����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX13 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX13
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED13 EQ 0 THEN
        XX13 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "LD.LOANS.AND.DEPOSITS"
        XX.ARRAY<1,1>[50,20] = "�����"
        XX.ARRAY<1,1>[80,40]  = XX13
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
***********************************************************************
    T.SEL14 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21096 AND CATEGORY LE 21097"
    CALL EB.READLIST(T.SEL14,KEY.LIST14,"",SELECTED14,ER.MSG)
    IF SELECTED14 THEN
        FOR I = 1 TO SELECTED14
            CALL F.READ(FN.LD,KEY.LIST14<I>,R.LD,F.LD,E.LD)
            ID = KEY.LIST14<I>
            CO.CODE = R.LD<LD.CO.CODE>
            INP1    = R.LD<LD.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "LD.LOANS.AND.DEPOSITS"
                XX.ARRAY<1,1>[50,20] = "������ ����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX14 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX14
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED14 EQ 0 THEN
        XX14 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "LD.LOANS.AND.DEPOSITS"
        XX.ARRAY<1,1>[50,20] = "������ ����"
        XX.ARRAY<1,1>[80,40]  = XX14
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END


******************************************************************
    T.SEL15 = "SELECT F.SCB.BT.BATCH$NAU"
    CALL EB.READLIST(T.SEL15,KEY.LIST15,"",SELECTED15,ER.MSG)
    IF SELECTED15 THEN
        FOR I = 1 TO SELECTED15
            CALL F.READ(FN.BT,KEY.LIST15<I>,R.BT,F.BT,E.BT)
            ID = KEY.LIST15<I>
            CO.CODE = R.BT<SCB.BT.CO.CODE>
            INP1    = R.BT<SCB.BT.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "SCB.BT.BATCH"
                XX.ARRAY<1,1>[50,20] = "����� ����� ���������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX15 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX15
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED15 EQ 0 THEN
        XX15 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "SCB.BT.BATCH"
        XX.ARRAY<1,1>[50,20] = "����� ����� ���������"
        XX.ARRAY<1,1>[80,40]  = XX15
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
*******************************************************************
    T.SEL16 = "SELECT FBNK.BILL.REGISTER$NAU"
    CALL EB.READLIST(T.SEL16,KEY.LIST16,"",SELECTED16,ER.MSG)
    IF SELECTED16 THEN
        FOR I = 1 TO SELECTED16
            CALL F.READ(FN.BR,KEY.LIST16<I>,R.BR,F.BR,E.BR)
            ID = KEY.LIST16<I>
            CO.CODE = R.BR<EB.BILL.REG.CO.CODE>
            INP1    = R.BR<EB.BILL.REG.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "BILL.REGISTER"
                XX.ARRAY<1,1>[50,20] = "����� � �������� �������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX16 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX16
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED16 EQ 0 THEN
        XX16 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "BILL.REGISTER"
        XX.ARRAY<1,1>[50,20] = "����� � �������� �������"
        XX.ARRAY<1,1>[80,40]  = XX16
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END

************************************************************
    T.SEL17 = "SELECT F.SCB.MONTHLY.PAY.CBE$NAU"
    CALL EB.READLIST(T.SEL17,KEY.LIST17,"",SELECTED17,ER.MSG)
    IF SELECTED17 THEN
        FOR I = 1 TO SELECTED17
            CALL F.READ(FN.CBE,KEY.LIST17<I>,R.CBE,F.CBE,E.CBE)
            ID = KEY.LIST17<I>
            CO.CODE = R.CBE<CBE.PAY.CO.CODE>
            INP1    = R.CBE<CBE.PAY.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "SCB.MONTHLY.PAY.CBE"
                XX.ARRAY<1,1>[50,20] = "������� � ������� �������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX17 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX17
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED17 EQ 0 THEN
        XX17 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "SCB.MONTHLY.PAY.CBE"
        XX.ARRAY<1,1>[50,20] = "������� � ������� �������"
        XX.ARRAY<1,1>[80,40]  = XX17
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
****************************************************************************************************************************

    T.SEL18 = "SELECT F.SCB.DOCUMENT.PROCURE$NAU"
    CALL EB.READLIST(T.SEL18,KEY.LIST18,"",SELECTED18,ER.MSG)
    IF SELECTED18 THEN
        FOR I = 1 TO SELECTED18
            CALL F.READ(FN.DOC,KEY.LIST18<I>,R.DOC,F.DOC,E.DOC)
            ID = KEY.LIST18<I>
            CO.CODE = R.DOC<DOC.PRO.CO.CODE>
            INP1    = R.DOC<DOC.PRO.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "SCB.DOCUMENT.PROCURE"
                XX.ARRAY<1,1>[50,20] = "������� �����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX18 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX18
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED18 EQ 0 THEN
        XX18 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "SCB.DOCUMENT.PROCURE"
        XX.ARRAY<1,1>[50,20] = "������� �����"
        XX.ARRAY<1,1>[80,40]  = XX18
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
************************************************************

    T.SEL19 = "SELECT F.SCB.DEPT.SAMPLE1$NAU"
    CALL EB.READLIST(T.SEL19,KEY.LIST19,"",SELECTED19,ER.MSG)
    IF SELECTED19 THEN
        FOR I = 1 TO SELECTED19
            CALL F.READ(FN.CL,KEY.LIST19<I>,R.CL,F.CL,E.CL)
            ID = KEY.LIST19<I>
            CO.CODE = R.CL<DEPT.SAMP.CO.CODE>
            INP1    = R.CL<DEPT.SAMP.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "SCB.DEPT.SAMPLE1"
                XX.ARRAY<1,1>[50,20] = "��� ������ ��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX19 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX19
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED19 EQ 0 THEN
        XX19 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "SCB.DEPT.SAMPLE1"
        XX.ARRAY<1,1>[50,20] = "��� ������ ��������"
        XX.ARRAY<1,1>[80,40]  = XX19
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
***************************************************************
    T.SEL20 = "SELECT FBNK.CARD.ISSUE$NAU"
    CALL EB.READLIST(T.SEL20,KEY.LIST20,"",SELECTED20,ER.MSG)
    IF SELECTED20 THEN
        FOR I = 1 TO SELECTED20
            CALL F.READ(FN.CARD,KEY.LIST20<I>,R.CARD,F.CARD,E.CARD)
            ID = KEY.LIST20<I>
            CO.CODE = R.CARD<CARD.IS.CO.CODE>
            INP1    = R.CARD<CARD.IS.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "CARD.ISSUE"
                XX.ARRAY<1,1>[50,20] = "��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX20 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX20
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED20 EQ 0 THEN
        XX20 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "CARD.ISSUE"
        XX.ARRAY<1,1>[50,20] = "��������"
        XX.ARRAY<1,1>[80,40]  = XX20
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
*******************************************************

    T.SEL21 = "SELECT FBNK.LIMIT$NAU"
    CALL EB.READLIST(T.SEL21,KEY.LIST21,"",SELECTED21,ER.MSG)
    IF SELECTED21 THEN
        FOR I = 1 TO SELECTED21
            CALL F.READ(FN.LIM,KEY.LIST21<I>,R.LIM,F.LIM,E.LIM)
            ID = KEY.LIST21<I>
            CO.CODE = R.LIM<LI.CO.CODE>
            INP1    = R.LIM<LI.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "LIMIT"
                XX.ARRAY<1,1>[50,20] = "���� �����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX21 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX21
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED21 EQ 0 THEN
        XX21 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "LIMIT"
        XX.ARRAY<1,1>[50,20] = "���� �����"
        XX.ARRAY<1,1>[80,40]  = XX21
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
*********************************************************

    T.SEL22 = "SELECT FBNK.COLLATERAL$NAU"
    CALL EB.READLIST(T.SEL22,KEY.LIST22,"",SELECTED22,ER.MSG)

    IF SELECTED22 THEN
        FOR I = 1 TO SELECTED22
            CALL F.READ(FN.COLL,KEY.LIST22<I>,R.COLL,F.COLL,E.COLL)
            ID = KEY.LIST22<I>
            CO.CODE = R.COLL<COLL.CO.CODE>
            INP1    = R.COLL<COLL.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "COLLATERAL"
                XX.ARRAY<1,1>[50,20] = "���� �������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX22 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX22
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED22 EQ 0 THEN
        XX22 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "COLLATERAL"
        XX.ARRAY<1,1>[50,20] = "���� �������"
        XX.ARRAY<1,1>[80,40]  = XX22
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
*************************************************************

    T.SEL23 = "SELECT FBNK.COLLATERAL.RIGHT$NAU"
    CALL EB.READLIST(T.SEL23,KEY.LIST23,"",SELECTED23,ER.MSG)

    IF SELECTED23 THEN
        FOR I = 1 TO SELECTED23
            CALL F.READ(FN.COR,KEY.LIST23<I>,R.COR,F.COR,E.COR)
            ID = KEY.LIST23<I>
            CO.CODE = R.COR<COLL.RIGHT.CO.CODE>
            INP1    = R.COR<COLL.RIGHT.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "COLLATERAL.RIGHT"
                XX.ARRAY<1,1>[50,20] = "��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX23 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX23
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED23 EQ 0 THEN
        XX23 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "COLLATERAL"
        XX.ARRAY<1,1>[50,20] = "��������"
        XX.ARRAY<1,1>[80,40]  = XX23
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
*****************************************************************
    T.SEL24 = "SELECT FBNK.PAYMENT.STOP$NAU"
    CALL EB.READLIST(T.SEL24,KEY.LIST24,"",SELECTED24,ER.MSG)

    IF SELECTED24 THEN
        FOR I = 1 TO SELECTED24
            CALL F.READ(FN.PAY.STOP,KEY.LIST24<I>,R.PAY.STOP,F.PAY.STOP,E.PAY.STOP)
            ID = KEY.LIST24<I>
            CO.CODE = R.PAY.STOP<AC.PAY.CO.CODE>
            INP1    = R.PAY.STOP<AC.PAY.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "PAYMENT.STOP"
                XX.ARRAY<1,1>[50,20] = "����� ��� �����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX24 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX24
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED24 EQ 0 THEN
        XX24 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "PAYMENT.STOP"
        XX.ARRAY<1,1>[50,20] = "����� ��� �����"
        XX.ARRAY<1,1>[80,40]  = XX24
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
******************************************************************

    T.SEL25 = "SELECT F.FATCA.CUSTOMER.SUPPLEMENTARY.INFO$NAU"
    CALL EB.READLIST(T.SEL25,KEY.LIST25,"",SELECTED25,ER.MSG)

    IF SELECTED25 THEN
        FOR I = 1 TO SELECTED25
            CALL F.READ(FN.FATCA,KEY.LIST25<I>,R.FATCA,F.FATCA,E.FATCA)
            ID = KEY.LIST25<I>
            CO.CODE = R.FATCA<FA.FI.CO.CODE>
            INP1    = R.FATCA<FA.FI.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "CUSTOMER FATCA"
                XX.ARRAY<1,1>[50,20] = "�����"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END

            CALL DBR('CUSTOMER':@FM:EB.CUS.ACCOUNT.OFFICER,ID,BRN)
            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRN,BRANCH)
            YYBRN = FIELD(BRANCH,'.',2)
            CALL DBR('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,ID,CO.CODE)

            IF WS.SCB.DEPT EQ '5010' THEN
                XX25 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX25
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = YYBRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED25 EQ 0 THEN
        XX25 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "CUSTOMER"
        XX.ARRAY<1,1>[50,20] = "�����"
        XX.ARRAY<1,1>[80,40]  = XX25
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
************************************
    T.SEL26= "SELECT F.SCB.MODIFY.USER.AUTHORITY$NAU"
    CALL EB.READLIST(T.SEL26,KEY.LIST26,"",SELECTED26,ER.MSG)

    IF SELECTED26 THEN
        FOR I = 1 TO SELECTED26
            CALL F.READ(FN.US,KEY.LIST26<I>,R.US,F.US,E.US)
            ID = KEY.LIST26<I>
            O.CODE = R.US<MUA.CO.CODE>
            INP1    = R.US<MUA.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "USER"
                XX.ARRAY<1,1>[50,20] = "������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX26 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX26
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED26 EQ 0 THEN
        XX26 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "USER"
        XX.ARRAY<1,1>[50,20] = "������"
        XX.ARRAY<1,1>[80,40]  = XX26
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END

************************************
    T.SEL27= "SELECT F.SCB.DRMNT.FILES$NAU"
    CALL EB.READLIST(T.SEL27,KEY.LIST27,"",SELECTED27,ER.MSG)

    IF SELECTED27 THEN
        FOR I = 1 TO SELECTED27
            CALL F.READ(FN.DRMNT,KEY.LIST27<I>,R.DRMNT,F.DRMNT,E.DRMNT)
            ID = KEY.LIST27<I>

            CO.CODE = R.DRMNT<SDF.CO.CODE>
            INP1    = R.DRMNT<SDF.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "DRMNT.FILES"
                XX.ARRAY<1,1>[50,20] = "������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX27 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX27
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED27 EQ 0 THEN
        XX27 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "DRMNT FILES"
        XX.ARRAY<1,1>[50,20] = "������"
        XX.ARRAY<1,1>[80,40]  = XX27
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
************************************
    T.SEL28= "SELECT F.AC.CHARGE.REQUEST$NAU"
    CALL EB.READLIST(T.SEL28,KEY.LIST28,"",SELECTED28,ER.MSG)

    IF SELECTED28 THEN
        FOR I = 1 TO SELECTED28
            CALL F.READ(FN.AC.CH,KEY.LIST28<I>,R.AC.CH,F.AC.CH,E.AC.CH)
            ID = KEY.LIST28<I>

            CO.CODE = R.AC.CH<CHG.CO.CODE>
            INP1    = R.AC.CH<CHG.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "AC.CHARGE"
                XX.ARRAY<1,1>[50,20] = "������ ��������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX28 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX28
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED28 EQ 0 THEN
        XX28 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "AC.CHARGE"
        XX.ARRAY<1,1>[50,20] = "������ ������ "
        XX.ARRAY<1,1>[80,40]  = XX28
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END

************************************
    T.SEL29= "SELECT F.SCB.ATM.APP$NAU"
    CALL EB.READLIST(T.SEL29,KEY.LIST29,"",SELECTED29,ER.MSG)

    IF SELECTED29 THEN
        FOR I = 1 TO SELECTED29
            CALL F.READ(FN.ATM.APP,KEY.LIST29<I>,R.ATM.APP,F.ATM.APP,E.ATM.APP)
            ID = KEY.LIST29<I>

            CO.CODE = R.ATM.APP<SCB.VISA.CO.CODE>
            INP1    = R.ATM.APP<SCB.VISA.INPUTTER>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR('USER':@FM:EB.USE.LOCAL.REF,INPUTT1,WS.LOCAL)
            WS.SCB.DEPT  = WS.LOCAL<1,USER.SCB.DEPT.CODE>
            IF I = 1 THEN
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]  = "ATM.APP"
                XX.ARRAY<1,1>[50,20] = "������� ������"
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH)
            BRN  = BRANCH
            IF WS.SCB.DEPT EQ '5010' THEN
                XX29 = ID
                XX.ARRAY = SPACE(132)
                XX.ARRAY<1,1>[1,40]   = XX29
                XX.ARRAY<1,1>[50,20]  = CO.CODE
                XX.ARRAY<1,1>[80,40]  = BRN
                XX.ARRAY<1,1>[100,20] = INPUTT1
                PRINT XX.ARRAY
                PRINT " "
                XX.ARRAY = ""
            END
        NEXT I
    END
    IF SELECTED29 EQ 0 THEN
        XX29 = '0'
        XX.ARRAY = SPACE(132)
        XX.ARRAY<1,1>[1,40]  = "ATM.APP"
        XX.ARRAY<1,1>[50,20] = "������� ������"
        XX.ARRAY<1,1>[80,40]  = XX29
        PRINT XX.ARRAY
        PRINT " "
        XX.ARRAY = ""
    END
    RETURN
*-------------------------------------------------------------------
PRINT.HEAD:
*---------
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE,":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = HHH:":":MIN
    BRN.ID  = ID.COMPANY
    BRN.ID  = BRN.ID[2] - 1 + 1

*  CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRN.ID,BRANCH)
*  YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":SPACE(40):"���� �������� ����� ����� ����� " : SPACE(2) :"GLOBUS"
    PR.HD :="'L'":"������� : ":T.DAY :SPACE(5):"����� : " : TIMEFMT:SPACE(40) :"��� ������ : ":"'P'"
    PR.HD :="'L'":"����� : " : TIMEFMT
    PR.HD :="'L'":"SBD.PRINT.NAU.5010" : SPACE(40):"�����" : SPACE(5) : "������ ��������":SPACE(60) : "������"
    PR.HD :="'L'":SPACE(50):STR('_',80)

    HEADING PR.HD
*===============================================================
    RETURN
END
