* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.CUST.TEL.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
*Line [ 35 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.CUST.TEL'
    XX = ''
    KK = 0

    RETURN
*===============================================================
CALLDB:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    YTEXT = "����� ����� ��� ����� : "
    CALL TXTINP(YTEXT, 8, 22, "2", "A")
    COMP  = "EG00100":COMI

    CALL F.READ(FN.COM,COMP,R.COM,F.COM,E2)

    IF E2 THEN
        TEXT = '��� ����� ��� ����' ; CALL REM
        TEXT = '�� ������ �� ��������' ; CALL REM
        GOTO EXIT.PROG
    END ELSE
        WS.COMP.NAME = R.COM<EB.COM.COMPANY.NAME,2>
        TEXT = WS.COMP.NAME ; CALL REM
    END

*************************************************************************
    T.SEL = "SELECT ":FN.CU: " WITH SECTOR EQ '2000' AND COMPANY.BOOK EQ ":COMP:" AND SCCD.CUSTOMER NE 'YES' AND POSTING.RESTRICT LT '90'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            FOR J = 1 TO 3
                WS.TEL.NO = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,J>

                WS.TEL.NO.ID = WS.TEL.NO[1,2]
                IF WS.TEL.NO.ID EQ '01' THEN
                    WS.TEL.LEN = LEN(WS.TEL.NO)
                    IF WS.TEL.LEN EQ 11 THEN
                        XX = ''
                        XX<1,1>[1,11]   = WS.TEL.NO
                        CALL PRINTER.ON(REPORT.ID,'')
                        GOSUB PRINT.HEAD
                        PRINT XX<1,1>
                        CALL PRINTER.OFF
                        CALL PRINTER.CLOSE(REPORT.ID,0,'')
                    END

                END

            NEXT J

        NEXT I
        TEXT = 'DONE' ; CALL REM
    END
EXIT.PROG:

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    PR.HD  ="'L'":" "
    HEADING PR.HD
    RETURN
*==============================================================
END
