* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.GENLEDALL.SEGM.AL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY

*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========

    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.SEGMENT.AL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"GENLEDALL.SEGMENT.AL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.SEGMENT.AL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE GENLEDALL.SEGMENT.AL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create GENLEDALL.SEGMENT.AL.CSV File IN &SAVEDLISTS&'
        END
    END
*---------------------------------

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD.DESC   = "CATEGORY":","
    HEAD.DESC  := "CATEGORY.DESC":","
    HEAD.DESC  := "CURRENCY":","
    HEAD.DESC  := "AMOUNT.EQUIVALENT":","
    HEAD.DESC  := "NATIVE.AMOUNT":","
    HEAD.DESC  := "LINE":","
    HEAD.DESC  := "LINE.DESC":","
    HEAD.DESC  := "BRANCH Code":","
    HEAD.DESC  := "BRANCH.NAME":","
    HEAD.DESC  := "Product type":","
    HEAD.DESC  := "Segmentation Code":","
    HEAD.DESC  := "Segmentation Desc":","
    HEAD.DESC  := DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*--------------------------------------

    FN.CONT = 'F.RE.STAT.LINE.CONT' ; F.CONT = ''
    CALL OPF(FN.CONT,F.CONT)

    FN.CONS = 'F.CONSOLIDATE.ASST.LIAB' ; F.CONS = ''
    CALL OPF(FN.CONS,F.CONS)

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    WS.CATEG    = '' ; WS.CATEG.DESC = '' ; WS.CCY       = '' ; WS.AMT.LCY = 0
    WS.AMT.FCY  = 0  ; WS.LINE.NO    = 0  ; WS.LINE.DESC = '' ; WS.COMP = ''
    WS.BRN.NAME = '' ; WS.PRODUCT    = 0  ; WS.TARGET    = 0  ; WS.TARGET.NAME = 0

    RETURN
*-----------------------------------------------------------------------------
PROCESS:
*=======
    CONT.ID = 'GENLEDALL...'
    BRN.88  = '...EG0010088'

    T.SEL1 = "SELECT ":FN.CONT:" WITH @ID LIKE ":CONT.ID:" AND @ID UNLIKE ":BRN.88:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF SELECTED1 THEN
        FOR KM = 1 TO SELECTED1
            CALL F.READ(FN.CONT,KEY.LIST1<KM>,R.CONT,F.CONT,E5)
*Line [ 125 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
            FOR KM1 = 1 TO DECOUNT.CONT
                ASST.CONS.ID    = R.CONT<RE.SLC.ASST.CONSOL.KEY,KM1>
                ASST.TYPE       = R.CONT<RE.SLC.ASSET.TYPE,KM1>
*Line [ 130 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                ASST.TYPE.COUNT = DCOUNT(ASST.TYPE,@SM)

                CALL F.READ(FN.CONS,ASST.CONS.ID,R.CONS,F.CONS,E2)

                WS.TARGET = R.CONS<RE.ASL.VARIABLE.9>
                WS.CATEG  = R.CONS<RE.ASL.VARIABLE.1>
                WS.CCY    = R.CONS<RE.ASL.CURRENCY>

                CALL F.READ(FN.CAT,WS.CATEG,R.CAT,F.CAT,E4)
                WS.CATEG.DESC = R.CAT<EB.CAT.DESCRIPTION,1>

                CALL F.READ(FN.TAR,WS.TARGET,R.TAR,F.TAR,E3)
                WS.TARGET.NAME = R.TAR<EB.TAR.DESCRIPTION,1>

                WS.LINE.NO   = FIELD(KEY.LIST1<KM>,".",2)
                WS.COMP      = FIELD(KEY.LIST1<KM>,".",3)

                LN.ID = 'GENLEDALL.':WS.LINE.NO
                CALL F.READ(FN.LN,LN.ID,R.LN,F.FN,E5)
                WS.LINE.DESC = R.LN<RE.SRL.DESC><1,1>

                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,WS.BRN.NAME)

                CALL F.READ(FN.CCY,WS.CCY,R.CCY,F.CCY,E1)
                IF WS.CCY EQ 'EGP' THEN
                    RATE = 1
                END ELSE
                    IF WS.CCY EQ 'JPY' THEN
                        RATE = R.CCY<SBD.CURR.MID.RATE> / 100
                    END ELSE
                        RATE = R.CCY<SBD.CURR.MID.RATE>
                    END
                END

                FOR II = 1 TO ASST.TYPE.COUNT
                    ASST.TYPE1 =  R.CONT<RE.SLC.ASSET.TYPE,KM1,II>
*Line [ 167 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE ASST.TYPE1 IN R.CONS<RE.ASL.TYPE,1> SETTING TYP ELSE NULL

                    IF WS.CCY EQ 'EGP' THEN
                        WS.AMT.FCY += R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
                        WS.AMT.LCY  = WS.AMT.FCY
                    END ELSE
                        WS.AMT.FCY += R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
                        WS.AMT.LCY += R.CONS<RE.ASL.LOCAL.BALANCE,TYP> + R.CONS<RE.ASL.LOCAL.DEBIT.MVE,TYP> + R.CONS<RE.ASL.LOCAL.CREDT.MVE,TYP>
                    END
                NEXT II

                IF WS.CATEG NE '' AND WS.CATEG.DESC NE '' THEN
                    BB.DATA  = WS.CATEG:","
                    BB.DATA := WS.CATEG.DESC:","
                    BB.DATA := WS.CCY:","
                    BB.DATA := WS.AMT.LCY:","
                    BB.DATA := WS.AMT.FCY:","
                    BB.DATA := WS.LINE.NO:","
                    BB.DATA := WS.LINE.DESC:","
                    BB.DATA := WS.COMP:","
                    BB.DATA := WS.BRN.NAME:","
                    BB.DATA := WS.PRODUCT:","
                    BB.DATA := WS.TARGET:","
                    BB.DATA := WS.TARGET.NAME:","

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
                WS.AMT.FCY = 0
                WS.AMT.LCY = 0

            NEXT KM1

            WS.CATEG    = '' ; WS.CATEG.DESC = '' ; WS.CCY       = '' ; WS.AMT.LCY = 0
            WS.AMT.FCY  = 0  ; WS.LINE.NO    = 0  ; WS.LINE.DESC = '' ; WS.COMP = ''
            WS.BRN.NAME = '' ; WS.PRODUCT    = 0  ; WS.TARGET    = 0  ; WS.TARGET.NAME = 0


        NEXT KM
    END

    RETURN
*====================================================
