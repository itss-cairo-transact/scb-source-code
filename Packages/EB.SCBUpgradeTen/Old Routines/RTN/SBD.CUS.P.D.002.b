* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>3503</Rating>
*-----------------------------------------------------------------------------
*����� ��������� ���� ��� ��� ������ �������
*    PROGRAM    SBD.CUS.P.D.002
    SUBROUTINE SBD.CUS.P.D.002
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.P.D.TEMP
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.INDUSTRY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RELATION
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS

*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.ID.TYPE
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CU.ID.ISS.PLACE
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.PROFESSION
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.STAFF.RANK
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TAX.DEPT
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.LEGAL.FORM
*---------------------------------------------------
    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ""
*
    FN.TEMP = "F.SCB.CUS.P.D.TEMP"
    F.TEMP  = ""
*
    FN.POST = "F.POSTING.RESTRICT"
    F.POST = ""
*
    FN.SECT = "FBNK.SECTOR"
    F.SECT = ""
*
    FN.INDST = "FBNK.INDUSTRY"
    F.INDST = ""
*
    FN.NEW.SECT = "F.SCB.NEW.SECTOR"
    F.NEW.SECT = ""
*
    FN.NEW.INDST = "F.SCB.NEW.INDUSTRY"
    F.NEW.INDST = ""
*
    FN.COMP = "F.COMPANY"
    F.COMP = ""
*
    FN.USER = "F.USER"
    F.USER = ""
*
    FN.DATE = "F.DATES"
    F.DATE  = ""

*
    FN.RELAT = "FBNK.RELATION"
    F.RELAT = ""
*
    FN.TARG  = "FBNK.TARGET"
    F.TARG  = ""
*
    FN.CONTRY = "F.COUNTRY"
    F.CONTRY = ""
*
    FN.CUSST = "FBNK.CUSTOMER.STATUS"
    F.CUSST = ""
*
    FN.IDTYPE = "F.SCB.CUS.ID.TYPE"
    F.IDTYPE = ""
*
    FN.IDISPL = "F.SCB.CU.ID.ISS.PLACE"
    F.IDISPL = ""
*
    FN.CUSTITL = "F.SCB.CUS.TITLE"
    F.CUSTITL = ""
*
    FN.CUSPROF = "F.SCB.CUS.PROFESSION"
    F.CUSPROF = ""
*
    FN.CUSGOV = "F.SCB.CUS.GOVERNORATE"
    F.CUSGOV = ""
*
    FN.CUSREG = "F.SCB.CUS.REGION"
    F.CUSREG = ""
*
    FN.CUSSTAF = "F.SCB.CUS.STAFF.RANK"
    F.CUSSTAF = ""
*
    FN.CUSTAXD = "F.SCB.CUS.TAX.DEPT"
    F.CUSTAXD = ""
*
    FN.CUSLEGL = "F.SCB.CUS.LEGAL.FORM"
    F.CUSLEGL = ""
*----------------------------------------------------
    CALL OPF (FN.CUS,F.CUS)
*
    CALL OPF (FN.TEMP,F.TEMP)
*
    CALL OPF (FN.POST,F.POST)
*
    CALL OPF (FN.SECT,F.SECT)
*
    CALL OPF (FN.INDST,F.INDST)
*
    CALL OPF (FN.NEW.SECT,F.NEW.SECT)
*
    CALL OPF (FN.NEW.INDST,F.NEW.INDST)
*
    CALL OPF (FN.COMP,F.COMP)
*
    CALL OPF (FN.DATE,F.DATE)
*
    CALL OPF (FN.RELAT,F.RELAT)
*
    CALL OPF (FN.TARG,F.TARG)
*
    CALL OPF (FN.CONTRY,F.CONTRY)
*
    CALL OPF (FN.CUSST,F.CUSST)
*
    CALL OPF (FN.IDTYPE,F.IDTYPE)
*
    CALL OPF (FN.IDISPL,F.IDISPL)
*
    CALL OPF (FN.CUSTITL,F.CUSTITL)
*
    CALL OPF (FN.CUSPROF,F.CUSPROF)
*
    CALL OPF (FN.CUSGOV,F.CUSGOV)
*
    CALL OPF (FN.CUSREG,F.CUSREG)
*
    CALL OPF (FN.CUSSTAF,F.CUSSTAF)
*
    CALL OPF (FN.CUSTAXD,F.CUSTAXD)
*
    CALL OPF (FN.CUSLEGL,F.CUSLEGL)

* ---------------------------------------- WORKING AREA
*---------------------------  PRINTER HEADER
    P.TITL = "��������� ���� ��� ��� ������ �������"
    P.AC.NO = "��� ������"
    P.CL.NAM = "��� ������"
    P.AMND.TYPE = "������ ������"
    P.DATA.BEF = "���� ��� �������"
    P.DATA.AFTER = "���� ��� �������"
    WS.LINE.COUNT = 50
    WS.PAGE.COUNT = 0
    WS.COMPAR.NO =    0
    WS.CUS.NO = 0
    WS.FRST.REC = 0
    WS.FRST.TIME =  0
    WS.CL.NAME = ""
    WS.CL.NO = 0
    WS.FRST.BR = 0
    WS.COMP = ID.COMPANY
    WS.COMP.BR = ID.COMPANY
*-------------------------------------------------------------
    DIM ARRAY(129,2)
    ARRAY(1,1) = "O*001"
    ARRAY(1,2) =  "*MNEMONIC*"

    ARRAY(2,1) = "O*002"
    ARRAY(2,2) =  "*SECTOR*"

    ARRAY(3,1) = "O*003"
    ARRAY(3,2) =  "*ACCOUNT.OFFICER*"

    ARRAY(4,1) = "O*004"
    ARRAY(4,2) =  "*INDUSTRY*"

    ARRAY(5,1) = "O*005"
    ARRAY(5,2) =  "*TARGET*"

    ARRAY(6,1) = "O*006"
    ARRAY(6,2) =  "*NATIONALITY*"

    ARRAY(7,1) = "O*007"
    ARRAY(7,2) =  "*CUSTOMER.STATUS*"

    ARRAY(8,1) = "O*008"
    ARRAY(8,2) =  "*RESIDENCE*"

    ARRAY(9,1) = "O*009"
    ARRAY(9,2) =  "*CONTACT.DATE*"

    ARRAY(10,1) = "O*010"
    ARRAY(10,2) =  "*INTRODUCER*"

    ARRAY(11,1) = "O*011"
    ARRAY(11,2) =  "*LEGAL.ID*"

    ARRAY(12,1) = "O*012"
    ARRAY(12,2) =  "*REVIEW.FREQUENCY*"

    ARRAY(13,1) = "O*013"
    ARRAY(13,2) =  "*BIRTH.INCORP.DATE*"

    ARRAY(14,1) = "O*014"
    ARRAY(14,2) =  "*GLOBAL.CUSTOMER*"

    ARRAY(15,1) = "O*015"
    ARRAY(15,2) =  "*LANGUAGE*"

    ARRAY(16,1) = "O*016"
    ARRAY(16,2) =  "*POSTING.RESTRICT*"

    ARRAY(17,1) = "O*017"
    ARRAY(17,2) =  "*DISPO.OFFICER*"

    ARRAY(18,1) = "O*018"
    ARRAY(18,2) =  "*CONFID.TXT*"

    ARRAY(19,1) = "O*019"
    ARRAY(19,2) =  "*DISPO.EXEMPT*"

    ARRAY(20,1) = "O*020"
    ARRAY(20,2) =  "*ISSUE.CHEQUES*"

    ARRAY(21,1) = "O*021"
    ARRAY(21,2) =  "*CLS.CPARTY*"

    ARRAY(22,1) = "O*022"
    ARRAY(22,2) =  "*FX.COMM.GROUP.ID*"

    ARRAY(23,1) = "O*023"
    ARRAY(23,2) =  "*RESIDENCE.REGION*"


* ---------------------------------------LOCAL FIELDS NOT MULTY

    ARRAY(24,1) = "L*001"
    ARRAY(24,2) =  "*ARABIC.NAME*"

    ARRAY(25,1) = "L*002"
    ARRAY(25,2) =  "*ID.TYPE*"

    ARRAY(26,1) = "L*003"
    ARRAY(26,2) =  "*ID.NUMBER*"

    ARRAY(27,1) = "L*004"
    ARRAY(27,2) =  "*ID.ISSUE.DATE*"

    ARRAY(28,1) = "L*005"
    ARRAY(28,2) =  "*ID.EXPIRY.DATE*"

    ARRAY(29,1) = "L*006"
    ARRAY(29,2) =  "*PLACE.ID.ISSUE*"

    ARRAY(30,1) = "L*007"
    ARRAY(30,2) =  "*VERSION.NAME*"

    ARRAY(31,1) = "L*008"
    ARRAY(31,2) =  "*ACTIVITY*"

    ARRAY(32,1) = "L*009"
    ARRAY(32,2) =  "*SHAREHOLDER*"

    ARRAY(33,1) = "L*010"
    ARRAY(33,2) =  "*SIGNATURE*"

    ARRAY(34,1) = "L*011"
    ARRAY(34,2) =  "*ATM.TITLE*"

    ARRAY(35,1) = "L*012"
    ARRAY(35,2) =  "*ATM.MARITAL.ST*"

    ARRAY(36,1) = "L*013"
    ARRAY(36,2) =  "*CBE.NO*"

    ARRAY(37,1) = "L*014"
    ARRAY(37,2) =  "*OPENING.DATE*"

    ARRAY(38,1) = "L*015"
    ARRAY(38,2) =  "*TITLE*"

    ARRAY(39,1) = "L*016"
    ARRAY(39,2) =  "*TELEX*"

    ARRAY(40,1) = "L*017"
    ARRAY(40,2) =  "*EMPLOEE.NO*"

    ARRAY(41,1) = "L*018"
    ARRAY(41,2) =  "*GENDER*"

    ARRAY(42,1) = "L*019"
    ARRAY(42,2) =  "*MARITAL.STATUS*"

    ARRAY(43,1) = "L*020"
    ARRAY(43,2) =  "*EDUCATION*"

    ARRAY(44,1) = "L*021"
    ARRAY(44,2) =  "*INCOME*"

    ARRAY(45,1) = "L*022"
    ARRAY(45,2) =  "*PROFESSION*"

    ARRAY(46,1) = "L*023"
    ARRAY(46,2) =  "*JOB.DESCRIPTION*"

    ARRAY(47,1) = "L*024"
    ARRAY(47,2) =  "*ACCOM.TYPE*"

    ARRAY(48,1) = "L*025"
    ARRAY(48,2) =  "*ACCOM.LEGALITY*"

    ARRAY(49,1) = "L*026"
    ARRAY(49,2) =  "*GOVERNORATE*"

    ARRAY(50,1) = "L*027"
    ARRAY(50,2) =  "*REGION*"

    ARRAY(51,1) = "L*028"
    ARRAY(51,2) =  "*OTHER.INCOME*"

    ARRAY(52,1) = "L*029"
    ARRAY(52,2) =  "*DEPOSIT.VAL.TDY*"

    ARRAY(53,1) = "L*030"
    ARRAY(53,2) =  "*NSN.NO*"

    ARRAY(54,1) = "L*031"
    ARRAY(54,2) =  "*OLD.CUST.ID*"

    ARRAY(55,1) = "L*032"
    ARRAY(55,2) =  "*STAFF.RANK*"

    ARRAY(56,1) = "L*033"
    ARRAY(56,2) =  "*TAXCARD.IS.DATE*"

    ARRAY(57,1) = "L*034"
    ARRAY(57,2) =  "TAX.EXEMPTION*"

    ARRAY(58,1) = "L*035"
    ARRAY(58,2) =  "*TAX.EXEMP.ST.D*"

    ARRAY(59,1) = "L*036"
    ARRAY(59,2) =  "*TAX.EXEMP.END.D*"

    ARRAY(60,1) = "L*037"
    ARRAY(60,2) =  "*TAX.FILE.NO*"

    ARRAY(61,1) = "L*038"
    ARRAY(61,2) =  "*LIC.IND.NO*"

    ARRAY(62,1) = "L*039"
    ARRAY(62,2) =  "*TAX.DEPARTMENT*"

    ARRAY(63,1) = "L*040"
    ARRAY(63,2) =  "*IMP.LIC.NO*"

    ARRAY(64,1) = "L*041"
    ARRAY(64,2) =  "*IMP.LIC.END.D*"

    ARRAY(65,1) = "L*042"
    ARRAY(65,2) =  "*EXP.LIC.NO*"

    ARRAY(66,1) = "L*043"
    ARRAY(66,2) =  "*EXP.LIC.END.D*"

    ARRAY(67,1) = "L*044"
    ARRAY(67,2) =  "*COM.REG.NO*"

    ARRAY(68,1) = "L*045"
    ARRAY(68,2) =  "COM.REG.EXP.D*"

    ARRAY(69,1) = "L*046"
    ARRAY(69,2) =  "*SOCIAL.INSUR.NO*"

    ARRAY(70,1) = "L*047"
    ARRAY(70,2) =  "*ISSUED.CAPITAL*"

    ARRAY(71,1) = "L*048"
    ARRAY(71,2) =  "*LIC.ISSUE.DATE*"

    ARRAY(72,1) = "L*049"
    ARRAY(72,2) =  "*BROKER.TYPE*"

    ARRAY(73,1) = "L*050"
    ARRAY(73,2) =  "*UNDERWRITER*"

    ARRAY(74,1) = "L*051"
    ARRAY(74,2) =  "*PAID.CAPITAL*"

    ARRAY(75,1) = "L*052"
    ARRAY(75,2) =  "*NO.OF.SHARES*"

    ARRAY(76,1) = "L*053"
    ARRAY(76,2) =  "*SHARE.VALUE.EGP*"

    ARRAY(77,1) = "L*054"
    ARRAY(77,2) =  "*CURR.SHARE.VAL*"

    ARRAY(78,1) = "L*055"
    ARRAY(78,2) =  "*LEGAL.FORM*"

    ARRAY(79,1) = "L*056"
    ARRAY(79,2) =  "*BIC.CODE*"

    ARRAY(80,1) = "L*057"
    ARRAY(80,2) =  "*LIC.IND.EXP.D*"

    ARRAY(81,1) = "L*058"
    ARRAY(81,2) =  "*ARABIC.NAME.2*"

    ARRAY(82,1) = "L*059"
    ARRAY(82,2) =  "*TAX.NO*"

    ARRAY(83,1) = "L*060"
    ARRAY(83,2) =  "*LEGAL.STATUS*"

    ARRAY(84,1) = "L*061"
    ARRAY(84,2) =  "*LIC.EXP.DATE*"

    ARRAY(85,1) = "L*062"
    ARRAY(85,2) =  "*EMPLOYEE.BRANCH*"

    ARRAY(86,1) = "L*063"
    ARRAY(86,2) =  "*CREDIT.NO*"

    ARRAY(87,1) = "L*064"
    ARRAY(87,2) =  "*VISA.ADRS.TYPE*"

    ARRAY(88,1) = "L*065"
    ARRAY(88,2) =  "*FNAME*"

    ARRAY(89,1) = "L*066"
    ARRAY(89,2) =  "*MNAME*"

    ARRAY(90,1) = "L*067"
    ARRAY(90,2) =  "*LNAME*"

    ARRAY(91,1) = "L*068"
    ARRAY(91,2) =  "*JADDR*"

    ARRAY(92,1) = "L*069"
    ARRAY(92,2) =  "*NEW.SECTOR*"

    ARRAY(93,1) = "L*070"
    ARRAY(93,2) =  "*NEW.INDUSTRY*"

    ARRAY(94,1) = "L*071"
    ARRAY(94,2) =  "*CREDIT.STAT*"
*
    ARRAY(95,1) = "OM*001"
    ARRAY(95,2) = "*SHORT.NAME*"

    ARRAY(96,1) = "OM*002"
    ARRAY(96,2) = "*NAME.1*"

    ARRAY(97,1) = "OM*003"
    ARRAY(97,2) = "*NAME.2*"

    ARRAY(98,1) = "OM*004"
    ARRAY(98,2) = "*STREET*"

    ARRAY(99,1) = "OM*005"
    ARRAY(99,2) = "*TOWN.COUNTRY*"

    ARRAY(100,1) = "OM*006"
    ARRAY(100,2) = "*RELATION.CODE*"

    ARRAY(101,1) = "OM*007"
    ARRAY(101,2) = "*REL.CUSTOMER*"

    ARRAY(102,1) = "OM*008"
    ARRAY(102,2) = "*OTHER.OFFICER*"

    ARRAY(103,1) = "OM*009"
    ARRAY(103,2) = "*TEXT*"

    ARRAY(104,1) = "OM*010"
    ARRAY(104,2) = "*POST.CODE*"

    ARRAY(105,1) = "OM*011"
    ARRAY(105,2) = "*COUNTRY*"

    ARRAY(106,1) = "OM*012"
    ARRAY(106,2) = "*RATING*"

    ARRAY(107,1) = "LM*001"
    ARRAY(107,2) = "*ARABIC.ADDRESS*"
*..............................................
    ARRAY(108,1) = "LM*002"
    ARRAY(108,2) = "*TELEPHONE*"
*................................................................
    ARRAY(109,1) = "LM*003"
    ARRAY(109,2) = "*FAX*"
*................................................................
    ARRAY(110,1) = "LM*004"
    ARRAY(110,2) = "*EMAIL.ADDRESS*"
*................................................................
    ARRAY(111,1) = "LM*005"
    ARRAY(111,2) = "*REFERENCE.BANK*"
*................................................................
    ARRAY(112,1) = "LM*006"
    ARRAY(112,2) = "*CONTACT.NAMES*"
*...............................................................
    ARRAY(113,1) = "LM*007"
    ARRAY(113,2) = "*LEGAL.REP*"
*................................................................
    ARRAY(114,1) = "LM*008"
    ARRAY(114,2) = "*EXP.D*"
*................................................................
    ARRAY(115,1) = "LM*009"
    ARRAY(115,2) = "*REPR.NAME*"
*................................................................
    ARRAY(116,1) = "LM*010"
    ARRAY(116,2) = "*REPR.LEVEL*"
*................................................................
    ARRAY(117,1) = "LM*011"
    ARRAY(117,2) = "*ID.TYPE*"
*................................................................
    ARRAY(118,1) = "LM*012"
    ARRAY(118,2) = "*ID.NUMBER*"
*................................................................
    ARRAY(119,1) = "LM*013"
    ARRAY(119,2) = "*REPR.EXP.DATE*"
*................................................................
    ARRAY(120,1) = "LM*014"
    ARRAY(120,2) = "*REQ.DOC*"
*................................................................
    ARRAY(121,1) = "LM*015"
    ARRAY(121,2) = "*MEMBER.OF.DIREC*"
*.................................................
    ARRAY(122,1) = "LM*016"
    ARRAY(122,2) = "*OCCUPATION*"
*................................................................
    ARRAY(123,1) = "LM*017"
    ARRAY(123,2) = "*CAPITAL.ISSUE*"
*................................................................
    ARRAY(124,1) = "LM*018"
    ARRAY(124,2) = "*TAX.STATUS*"
*................................................................
    ARRAY(125,1) = "LM*019"
    ARRAY(125,2) = "*VISA.ADDRESS*"
*................................................................
    ARRAY(126,1) = "L*072"
    ARRAY(126,2) = "*ACTIV.CUST*"
*................................................................
    ARRAY(127,1) = "L*073"
    ARRAY(127,2) = "*ACTIV.CUST.D*"

    ARRAY(128,1) = "L*074"
    ARRAY(128,2) =  "*CREDIT.CODE*"

    ARRAY(129,1) = "L*075"
    ARRAY(129,2) =  "*CULR.CORR.STATUS*"
*END AMEND
*-------------------------------------- END OF WORKING AREA
*------------PROCEDURE --------------------------------------
    REPORT.ID='SBD.CUS.P.D.002'

**    REPORT.ID='P.FUNCTIONS'

*     REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    DATY = R.DATE<EB.DAT.LAST.WORKING.DAY>
*    WS.COMP.DAT = WS.LAST.DAT[3,6]

*------------------------------
* HARES.MAHMOUD
*    IF WS.LINE.COUNT GT 40 THEN
*        GOSUB A.2000.PRT.HEAD
*    END
*------------------------------

    WS.PRNT = 0
    GOSUB A.100.PROC
    IF WS.PRNT EQ 1   THEN
        WS.INPUT.A = FIELD(WS.INPUT,"_", 2)
        WS.AUTH.A  = FIELD(WS.AUTH,"_", 2)
        GOSUB A.400.USER
        XX = SPACE(140)
        XX<1,1>[50,20]  = "*INPUTTER*"
        XX<1,1>[72,50]  = WS.INPUT.NAME
        PRINT XX<1,1>

        XX = SPACE(140)
        XX<1,1>[50,20]  = "*AUTHORISER*"
        XX<1,1>[72,50]  = WS.AUTH.NAME
        PRINT XX<1,1>

        XX<1,1>[1,132]   =  STR('-',132)
        PRINT XX<1,1>
    END


*------------------------------
* HARES.MAHMOUD
    IF WS.PRNT = 0   THEN
        IF WS.LINE.COUNT GT 40 THEN
            GOSUB A.2000.PRT.HEAD
        END
    END
*------------------------------

    WS.PRNT = 0



    XX = SPACE(140)
    XX<1,1>[1,132]  = SPACE(58):"REPORT OF END"
    PRINT XX<1,1>
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*------------------------------------------------
A.100.PROC:
    SEL.CMD = "SELECT ":FN.TEMP:" WITH CO.CODE EQ ":WS.COMP:" BY @ID"
*    SEL.CMD = "SELECT ":FN.TEMP:"  WITH DEPT.CODE EQ 1 BY @ID"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.TEMP.ID FROM SEL.LIST SETTING POSTMP
    WHILE WS.TEMP.ID:POSTMP
        CALL F.READ(FN.TEMP,WS.TEMP.ID,R.TEMP,F.TEMP,MSG.TEMP)

        IF WS.FRST.BR   = 0 THEN
            WS.FRST.BR = 1
            WS.COMP.ID = R.TEMP<SCB.P.CO.CODE>
        END
* --------------------------------------------------   CHANGE   BRANCH CODE
        IF WS.COMP.ID NE R.TEMP<SCB.P.CO.CODE> THEN
            WS.COMP.ID = R.TEMP<SCB.P.CO.CODE>
            WS.PAGE.COUNT = 0
            GOSUB A.2000.PRT.HEAD
        END
        WS.CUS.NO = FIELD(WS.TEMP.ID,';',1)

        CALL F.READ(FN.CUS,WS.CUS.NO,R.CUS,F.CUS,MSG.CUS)

        WS.CL.NAME = R.CUS<EB.CUS.LOCAL.REF,CULR.ARABIC.NAME>

        WS.CL.NO = WS.CUS.NO

****----------------- TO DEFINE L* AND O*   OR LM* AND OM*
        WS.COMP.T = WS.TEMP.ID[8]
        WS.COMP   = WS.COMP.T[1,5]

        WS.COMP.TX = WS.COMP.T[1,1]

        IF WS.COMP.TX EQ "M" THEN
            WS.COMP.T = WS.TEMP.ID[9]
            WS.COMP   = WS.COMP.T[1,6]
        END
****-------------END DEFINATION  L* AND O*   OR LM* AND OM*

        IF WS.FRST.TIME EQ 0 THEN
            WS.FRST.TIME = 1
            WS.COMPAR.NO = WS.CUS.NO
            WS.INPUT = R.TEMP<SCB.P.INPUTTER>
            WS.AUTH  = R.TEMP<SCB.P.AUTHORISER>

            WS.FRST.REC = 0
        END

        IF WS.LINE.COUNT GT 40 THEN
            GOSUB A.2000.PRT.HEAD
        END
*-----------------------------------------------------CHANGE CUSTOMER NO.
        IF    WS.CUS.NO NE WS.COMPAR.NO THEN
            WS.INPUT.A = FIELD(WS.INPUT,"_", 2)
            WS.AUTH.A  = FIELD(WS.AUTH,"_", 2)
            GOSUB A.400.USER
            XX = SPACE(140)
            XX<1,1>[50,20]  = "*INPUTTER*"
            XX<1,1>[72,50]  = WS.INPUT.NAME
            PRINT XX<1,1>

            XX = SPACE(140)
            XX<1,1>[50,20]  = "*AUTHORISER*"
            XX<1,1>[72,50]  = WS.AUTH.NAME
            PRINT XX<1,1>

            XX<1,1>[1,132]   =  STR('-',132)
            PRINT XX<1,1>

            WS.LINE.COUNT = WS.LINE.COUNT + 3
            XX = SPACE(140)
            WS.COMPAR.NO = WS.CUS.NO
            WS.INPUT = R.TEMP<SCB.P.INPUTTER>
            WS.AUTH  = R.TEMP<SCB.P.AUTHORISER>
            WS.INPUT.A = FIELD(WS.INPUT,"_", 2)
            WS.AUTH.A  = FIELD(WS.AUTH,"_", 2)

            WS.FRST.REC = 0
        END

        GOSUB A.200.PRT

A.100.A:
    REPEAT
A.100.EXIT:
    RETURN
*-----------------------------------------------------PRINTING DATA
A.200.PRT:
    WS.PRNT = 1
    IF WS.LINE.COUNT GT 40 THEN
        GOSUB A.2000.PRT.HEAD
    END
    GOSUB A.250.GET.AMND.TYPE
    XX = SPACE(140)
    IF WS.FRST.REC EQ 0 THEN
        XX<1,1>[1,8]   = WS.CL.NO
        XX<1,1>[11,35]  = WS.CL.NAME
        WS.FRST.REC = 1
    END
    XX<1,1>[50,20]  = ARRAY(I,2)
*--------------------------------------TO READ SUM DEFINATIONS FROM FILES
    IF ARRAY(I,1) EQ "O*016" OR  ARRAY(I,1) EQ "O*002" OR ARRAY(I,1) EQ "O*004" OR ARRAY(I,1) EQ "L*069" OR ARRAY(I,1) EQ "L*070" THEN
        GOSUB A.300.GET.DSCRPTION
        GOTO A.200.A
    END
*
    IF ARRAY(I,1) EQ "OM*006" OR ARRAY(I,1) EQ "O*005" OR ARRAY(I,1) EQ "OM*011"  THEN
        GOSUB A.300.GET.DSCRPTION
        GOTO A.200.A
    END
    IF  ARRAY(I,1) EQ "O*007" OR ARRAY(I,1) EQ "L*002" OR ARRAY(I,1) EQ "L*006" THEN
        GOSUB A.300.GET.DSCRPTION
        GOTO A.200.A
    END
    IF ARRAY(I,1) EQ "L*015" OR ARRAY(I,1) EQ "L*022" OR ARRAY(I,1) EQ "L*026" THEN
        GOSUB A.300.GET.DSCRPTION
        GOTO A.200.A
    END
    IF ARRAY(I,1) EQ "O*023" OR  ARRAY(I,1) EQ "L*032" OR ARRAY(I,1) EQ "L*039" THEN
        GOSUB A.300.GET.DSCRPTION
        GOTO A.200.A
    END
    IF ARRAY(I,1) EQ "L*055" OR ARRAY(I,1) EQ "L*027" THEN
        GOSUB A.300.GET.DSCRPTION
        GOTO A.200.A
    END
*
*-------------- ----------------------------END READ DEFINATION FROM FILES
    XX<1,1>[72,65]  = R.TEMP<SCB.P.FIELD.1>
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    IF WS.LINE.COUNT GT 40 THEN
        GOSUB A.2000.PRT.HEAD
    END
    XX = SPACE(140)
    XX<1,1>[72,65]  = R.TEMP<SCB.P.FIELD.3>
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
A.200.A:
    IF WS.LINE.COUNT GT 40 THEN
        GOSUB A.2000.PRT.HEAD
    END
    XX = SPACE(140)
    XX<1,1>[72,65]   =  STR('-',65)
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    RETURN
*--------------------------------------------------
A.250.GET.AMND.TYPE:
    FOR I = 1 TO 129
        IF  WS.COMP EQ ARRAY(I,1)  THEN
            RETURN
        END
    NEXT I

    RETURN
*-------------------------------------------
A.300.GET.DSCRPTION:
    WS.BEF = ""
    WS.AFTER = ""

    WS.LEN.B = LEN(R.TEMP<SCB.P.FIELD.1>)
    WS.LEN.A = LEN(R.TEMP<SCB.P.FIELD.3>)
    WS.B.ID =  R.TEMP<SCB.P.FIELD.1>[1,WS.LEN.B]
    WS.A.ID =  R.TEMP<SCB.P.FIELD.3>[1,WS.LEN.A]

    IF ARRAY(I,1) EQ "O*016" THEN
        GOSUB A.310.GET.POST
    END

    IF ARRAY(I,1) EQ "O*002" THEN
        GOSUB A.320.GET.SECTOR
    END

    IF ARRAY(I,1) EQ "O*004" THEN
        GOSUB A.330.GET.INDSTRY
    END

    IF ARRAY(I,1) EQ "L*069" THEN
        GOSUB A.340.GET.NEW.SECTOR
    END

    IF ARRAY(I,1) EQ "L*070" THEN
        GOSUB A.350.GET.NEW.INDST
    END
*
    IF ARRAY(I,1) EQ "OM*006" THEN
        GOSUB A.450.REL
    END

    IF ARRAY(I,1) EQ "O*005" THEN
        GOSUB A.500.TAGT
    END

    IF ARRAY(I,1) EQ "OM*011" THEN
        GOSUB A.550.CONTRY
    END

    IF ARRAY(I,1) EQ "O*007" THEN
        GOSUB A.600.CUS.STAT
    END

    IF ARRAY(I,1) EQ "L*002" THEN
        GOSUB A.650.ID.TYPE
    END

    IF ARRAY(I,1) EQ "L*006" THEN
        GOSUB A.700.ID.ISS.PL
    END

    IF ARRAY(I,1) EQ "L*015" THEN
        GOSUB A.750.TCUS.TITL
    END

    IF ARRAY(I,1) EQ "L*022" THEN
        GOSUB A.800.PROF
    END

    IF ARRAY(I,1) EQ "L*026" THEN
        GOSUB A.850.GOV
    END

    IF ARRAY(I,1) EQ "O*023" THEN
        GOSUB A.900.REGION
    END

    IF ARRAY(I,1) EQ "L*032" THEN
        GOSUB A.950.ST.RANK
    END

    IF ARRAY(I,1) EQ "L*039" THEN
        GOSUB A.1000.TAX.DEPT
    END

    IF ARRAY(I,1) EQ "L*055" THEN
        GOSUB A.1050.LEGAL
    END

    IF ARRAY(I,1) EQ "L*027" THEN
        GOSUB A.900.REGION
    END
*
    XX<1,1>[72,4]  = WS.B.ID
    XX<1,1>[76,65]  = WS.BEF
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    IF WS.LINE.COUNT GT 40 THEN
        GOSUB A.2000.PRT.HEAD
    END
    XX = SPACE(140)
    XX<1,1>[72,4]  = WS.A.ID
    XX<1,1>[76,65]  = WS.AFTER
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    RETURN
*------------------------------------------------
A.310.GET.POST:
    MSG.COMN = ""
    CALL F.READ(FN.POST,WS.B.ID,R.POST,F.POST,MSG.COMN)
    WS.BEF = R.POST<AC.POS.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.POST,WS.A.ID,R.POST,F.POST,MSG.COMN)
    WS.AFTER = R.POST<AC.POS.DESCRIPTION,2>
    RETURN
*------------------------------------------------
A.320.GET.SECTOR:
    MSG.COMN = ""
    CALL F.READ(FN.SECT,WS.B.ID,R.SECT,F.SECT,MSG.COMN)
    WS.BEF = R.SECT<EB.SEC.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.SECT,WS.A.ID,R.SECT,F.SECT,MSG.COMN)
    WS.AFTER = R.SECT<EB.SEC.DESCRIPTION,2>
    RETURN
*------------------------------------------------
A.330.GET.INDSTRY:
    MSG.COMN = ""
    CALL F.READ(FN.INDST,WS.B.ID,R.INDST,F.INDST,MSG.COMN)
    WS.BEF = R.INDST<EB.IND.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.INDST,WS.A.ID,R.INDST,F.INDST,MSG.COMN)
    WS.AFTER = R.INDST<EB.IND.DESCRIPTION,2>
    RETURN
*------------------------------------------------
A.340.GET.NEW.SECTOR:
    MSG.COMN = ""
    CALL F.READ(FN.NEW.SECT,WS.B.ID,R.N.SECT,F.NEW.SECT,MSG.COMN)
    WS.BEF = R.N.SECT<C.SCB.NEW.SECTOR.NAME>
    MSG.COMN = ""
    CALL F.READ(FN.NEW.SECT,WS.A.ID,R.N.SECT,F.NEW.SECT,MSG.COMN)
    WS.AFTER = R.N.SECT<C.SCB.NEW.SECTOR.NAME>
    RETURN
*------------------------------------------------
A.350.GET.NEW.INDST:
    MSG.COMN = ""
    CALL F.READ(FN.NEW.INDST,WS.B.ID,R.N.INDST,F.NEW.INDST,MSG.COMN)
    WS.BEF = R.N.INDST<C.SCB.NEW.INDST.NAME>
    MSG.COMN = ""
    CALL F.READ(FN.NEW.INDST,WS.A.ID,R.N.INDST,F.NEW.INDST,MSG.COMN)
    WS.AFTER = R.N.INDST<C.SCB.NEW.INDST.NAME>
    RETURN

*------------------------------------------------------------------
A.400.USER:
    CALL F.READ(FN.USER,WS.INPUT.A,R.USER.INF,F.USER,MSG.USER)
    WS.INPUT.NAME = R.USER.INF<EB.USE.USER.NAME>
    CALL F.READ(FN.USER,WS.AUTH.A,R.USER.INFO,F.USER,MSG.USER)
    WS.AUTH.NAME = R.USER.INFO<EB.USE.USER.NAME>
    RETURN
*------------------------------------------------------------------
*
A.450.REL:
    MSG.COMN = ""
    CALL F.READ(FN.RELAT,WS.B.ID,R.RELAT,F.RELAT,MSG.COMN)
    WS.BEF = R.RELAT<EB.REL.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.RELAT,WS.A.ID,R.RELAT,F.RELAT,MSG.COMN)
    WS.AFTER = R.RELAT<EB.REL.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.500.TAGT:
    MSG.COMN = ""
    CALL F.READ(FN.TARG,WS.B.ID,R.TARG,F.TARG,MSG.COMN)
    WS.BEF = R.TARG<EB.TAR.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.TARG,WS.A.ID,R.TARG,F.TARG,MSG.COMN)
    WS.AFTER = R.TARG<EB.TAR.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.550.CONTRY:
    MSG.COMN = ""
    CALL F.READ(FN.CONTRY,WS.B.ID,R.CONTRY,F.CONTRY,MSG.COMN)
    WS.BEF = R.CONTRY<EB.COU.COUNTRY.NAME,2>
    MSG.COMN = ""
    CALL F.READ(FN.CONTRY,WS.A.ID,R.CONTRY,F.CONTRY,MSG.COMN)
    WS.AFTER = R.CONTRY<EB.COU.COUNTRY.NAME,2>
    RETURN
*---------------------------------------------------
A.600.CUS.STAT:
    MSG.COMN = ""
    CALL F.READ(FN.CUSST,WS.B.ID,R.CUSST,F.CUSST,MSG.COMN)
    WS.BEF = R.CUSST<EB.CST.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSST,WS.A.ID,R.CUSST,F.CUSST,MSG.COMN)
    WS.AFTER = R.CUSST<EB.CST.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.650.ID.TYPE:
    MSG.COMN = ""
    CALL F.READ(FN.IDTYPE,WS.B.ID,R.IDTYPE,F.IDTYPE,MSG.COMN)
    WS.BEF = R.IDTYPE<ID.TYPE.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.IDTYPE,WS.A.ID,R.IDTYPE,F.IDTYPE,MSG.COMN)
    WS.AFTER = R.IDTYPE<ID.TYPE.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.700.ID.ISS.PL:
    MSG.COMN = ""
    CALL F.READ(FN.IDISPL,WS.B.ID,R.IDISPL,F.IDISPL,MSG.COMN)
    WS.BEF = R.IDISPL<IDPL.DESCRIPTION>
    MSG.COMN = ""
    CALL F.READ(FN.IDISPL,WS.A.ID,R.IDISPL,F.IDISPL,MSG.COMN)
    WS.AFTER = R.IDISPL<IDPL.DESCRIPTION>
    RETURN
*---------------------------------------------------
A.750.TCUS.TITL:
    MSG.COMN = ""
    CALL F.READ(FN.CUSTITL,WS.B.ID,R.CUSTITL,F.CUSTITL,MSG.COMN)
    WS.BEF = R.CUSTITL<SCB.TIT.DESCRIPTION.TITLE,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSTITL,WS.A.ID,R.CUSTITL,F.CUSTITL,MSG.COMN)
    WS.AFTER = R.CUSTITL<SCB.TIT.DESCRIPTION.TITLE,2>
    RETURN
*---------------------------------------------------
A.800.PROF:
    MSG.COMN = ""
    CALL F.READ(FN.CUSPROF,WS.B.ID,R.CUSPROF,F.CUSPROF,MSG.COMN)
    WS.BEF = R.CUSPROF<SCB.PRF.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSPROF,WS.A.ID,R.CUSPROF,F.CUSPROF,MSG.COMN)
    WS.AFTER = R.CUSPROF<SCB.PRF.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.850.GOV:
    MSG.COMN = ""
    CALL F.READ(FN.CUSGOV,WS.B.ID,R.CUSGOV,F.CUSGOV,MSG.COMN)
    WS.BEF = R.CUSGOV<GOVE.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSGOV,WS.A.ID,R.CUSGOV,F.CUSGOV,MSG.COMN)
    WS.AFTER = R.CUSGOV<GOVE.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.900.REGION:
    MSG.COMN = ""
    CALL F.READ(FN.CUSREG,WS.B.ID,R.CUSREG,F.CUSREG,MSG.COMN)
    WS.BEF = R.CUSREG<REG.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSREG,WS.A.ID,R.CUSREG,F.CUSREG,MSG.COMN)
    WS.AFTER = R.CUSREG<REG.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.950.ST.RANK:
    MSG.COMN = ""
    CALL F.READ(FN.CUSSTAF,WS.B.ID,R.CUSSTAF,F.CUSSTAF,MSG.COMN)
    WS.BEF = R.CUSSTAF<STAFF.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSSTAF,WS.A.ID,R.CUSSTAF,F.CUSSTAF,MSG.COMN)
    WS.AFTER = R.CUSSTAF<STAFF.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.1000.TAX.DEPT:
    MSG.COMN = ""
    CALL F.READ(FN.CUSTAXD,WS.B.ID,R.CUSTAXD,F.CUSTAXD,MSG.COMN)
    WS.BEF = R.CUSTAXD<TAX..DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSTAXD,WS.A.ID,R.CUSTAXD,F.CUSTAXD,MSG.COMN)
    WS.AFTER = R.CUSTAXD<TAX..DESCRIPTION,2>
    RETURN
*---------------------------------------------------
A.1050.LEGAL:
    MSG.COMN = ""
    CALL F.READ(FN.CUSLEGL,WS.B.ID,R.CUSLEGL,F.CUSLEGL,MSG.COMN)
    WS.BEF = R.CUSLEGL<LEG.DESCRIPTION,2>
    MSG.COMN = ""
    CALL F.READ(FN.CUSLEGL,WS.A.ID,R.CUSLEGL,F.CUSLEGL,MSG.COMN)
    WS.AFTER = R.CUSLEGL<LEG.DESCRIPTION,2>
    RETURN
*---------------------------------------------------
*

A.2000.PRT.HEAD:
    WS.PAGE.COUNT = WS.PAGE.COUNT + 1
    CALL F.READ(FN.COMP,WS.COMP.BR,R.COMP,F.COMP,MSG.COMP)
    WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
*    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������":SPACE(70):WS.BR.NAME
*    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(95):"PAGE : ":WS.PAGE.COUNT:SPACE(5):"CUS.P.D.002"
    PR.HD :="'L'":SPACE(45):P.TITL
    PR.HD :="'L'":SPACE(5):P.AC.NO:SPACE(5):P.CL.NAM:SPACE(25):P.AMND.TYPE:SPACE(10):P.DATA.BEF
    PR.HD :="'L'":SPACE(78):P.DATA.AFTER
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
    WS.LINE.COUNT = 0
    RETURN
END
