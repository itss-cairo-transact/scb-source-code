* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*---------------------------------------NI7OOOOOOOOOOOOOOOOOO--------------------------------------
*-----------------------------------------------------------------------------
* <Rating>16520</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.REPORT.MALIA.FUNDING.CUR.CO
**    SUBROUTINE SBD.REPORT.MALIA.FUNDING.ALL
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FOREX
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

    COMP = ID.COMPANY
    DD = TODAY

    IDDD="EG0010001"
    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,IDDD,N.W.DAY)
    HHH = N.W.DAY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN

*========================================================================
INITIATE:

    OPENSEQ "&SAVEDLISTS&" , "SCB.MALIA.FUNDING.ALLL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.MALIA.FUNDING.ALLL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.MALIA.FUNDING.ALLL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.MALIA.FUNDING.ALLL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.MALIA.FUNDING.ALLL.CSV File IN &SAVEDLISTS&'
        END
    END
    DAT.HED = TODAY
    HEAD1   = "SAMPLE FUNDING REPORT "
    HEAD.DESC = HEAD1:" ":DAT.HED:",":COMP

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

** HEAD.DESC  = "CASH INFLOW":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    HEAD.DESC = "":","
    HEAD.DESC := "INTERBANK":","
    HEAD.DESC := "CURRENT ACCOUNT":","
    HEAD.DESC := "SAVING ACCOUNT":","
    HEAD.DESC := "CORPRATE DEPOSITS":","
    HEAD.DESC := "CDS":","
    HEAD.DESC := "OTHER":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DEPT   = 'F.DEPT.ACCT.OFFICER' ; F.DEPT = ''
    FN.AC     = 'FBNK.ACCOUNT' ; F.AC = ''
    FN.CU     = 'FBNK.CUSTOMER' ; F.CU = ''
    FN.CURR   = 'FBNK.CURRENCY' ; F.CURR = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE' ; F.IM = ''
    FN.FT     = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    FN.TT     = 'FBNK.TELLER$NAU' ; F.TT = ''
    FN.IN     = 'F.INF.MULTI.TXN' ; F.IN = ''
    FN.CLOSE  = 'FBNK.ACCOUNT.CLOSURE' ; F.CLOSE = ''
    FN.MM     = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    FN.FOR    = 'FBNK.FOREX' ; F.FOR = ''
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LETTER = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER = ''
    FN.DRAW   = 'FBNK.DRAWINGS' ; F.DRAW = ''
    FN.BT     = 'F.SCB.BT.BATCH' ; F.BT = ''
    FN.BR     = 'FBNK.BILL.REGISTER' ; F.BR = ''
    FN.CM     = 'F.COMPANY'  ; F.CM = ''
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    FN.DAT = 'F.DATES' ; F.DAT = '' ; R.DAT = ''

    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.DAT,F.DAT)
    CALL OPF(FN.DEPT,F.DEPT)
    CALL OPF(FN.LETTER,F.LETTER)
    CALL OPF(FN.DRAW,F.DRAW)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.CURR,F.CURR)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.CLOSE,F.CLOSE)
    CALL OPF(FN.MM,F.MM)
    CALL OPF(FN.FOR,F.FOR)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.BT,F.BT)
    CALL OPF(FN.CM,F.CM)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG=""  ; R.DEP = ''
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG=""
    KEY.LIST4="" ; SELECTED4="" ;  ER.MSG=""

    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD  = TODAY
***TEXT = COMP ; CALL REM
*------------------------------------------------------------------------


    XXX1  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX11 = SPACE(132) ; XXX12 = SPACE(132)
    XXX2  = SPACE(132)  ; XXX5  = SPACE(132)  ; XXX10 = SPACE(132) ; XXX13 = SPACE(132)
    XXX3  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX6  = SPACE(132) ; XXX7  = SPACE(132)
    XXX15 = SPACE(132)  ; XXX16 = SPACE(132)  ; XXX17 = SPACE(132) ; XXX14 = SPACE(132)
    XXX21 = SPACE(132)  ; XXX19 = SPACE(132)  ; XXX18 = SPACE(132) ; XXX20 = SPACE(132)
    XXX6  = SPACE(132)  ; XXX7  = SPACE(132)  ; XXX8  = SPACE(132) ; XXX9 = SPACE(132)

   T.SEL.CUR = "SELECT FBNK.CURRENCY BY @ID "
  ***  T.SEL.CUR = "SELECT FBNK.CURRENCY WITH @ID EQ EGP "
    CALL EB.READLIST(T.SEL.CUR,KEY.LIST.CUR,"",SELECTED.CUR,ER.MSG)
    FOR KKKK = 1 TO SELECTED.CUR
        CURR.ID = KEY.LIST.CUR<KKKK>
        XX1 = '' ; XX2 = '' ; XX3 = '' ; XX4 = '' ; XX5 = '' ; XX6= '' ; XX7 = '' ; XX8 = ''
        AC.BAL1      = '' ; XX1 = '' ; XX1.OVER = '' ; XX1.LOAN1 = '' ; XX1.LOAN2 = ''
        AC.BAL1.MM   = '' ; XX2 = '' ; XX2.OVER = '' ; XX2.LOAN1 = '' ; XX2.LOAN2 = ''
        AC.BAL1.OVER = '' ; XX3 = '' ; XX3.OVER = '' ; XX3.LOAN1 = '' ; XX3.LOAN2 = ''
        AC.BAL1.LOAN1= '' ; XX4 = '' ; XX4.OVER = '' ; XX4.LOAN1 = '' ; XX4.LOAN2 = ''
        AC.BAL1.LOAN2= '' ;AC.BAL1.LOAN2.CDS ='' ; XX5 = '' ; XX5.OVER = '' ; XX5.LOAN1 = '' ; XX5.LOAN2 = ''
        AC.BAL2      = '' ; XX6 = '' ; XX6.OVER = '' ; XX6.LOAN1 = '' ; XX6.LOAN2 = ''
        AC.BAL2.MM   = '' ; XX7 = '' ; XX7.OVER = '' ; XX7.LOAN1 = '' ; XX7.LOAN2 = ''
        AC.BAL2.OVER = '' ; XX8 = '' ; XX8.OVER = '' ; XX8.LOAN1 = '' ; XX8.LOAN2 = ''
        AC.BAL2.LOAN1= ''                            ; XX8.LOAN1.CHQ = '' ; XX8.LOAN2.CDS = ''
        AC.BAL2.LOAN2= '' ; AC.BAL2.LOAN2.CDS = ''   ; XX7.LOAN1.CHQ = '' ; XX7.LOAN2.CDS = ''
        AC.BAL3      = ''                            ; XX6.LOAN1.CHQ = '' ; XX6.LOAN2.CDS = ''
        AC.BAL3.MM   = ''                            ; XX5.LOAN1.CHQ = '' ; XX5.LOAN2.CDS = ''
        AC.BAL3.OVER = ''                            ; XX4.LOAN1.CHQ = '' ; XX4.LOAN2.CDS = ''
        AC.BAL3.LOAN1= '' ;AC.BAL3.LOAN1.CHQ = ''   ; XX3.LOAN1.CHQ = '' ; XX3.LOAN2.CDS = ''
        AC.BAL3.LOAN2= '' ;AC.BAL3.LOAN2.CDS = ''; XX2.LOAN1.CHQ = '' ; XX2.LOAN2.CDS = ''
        AC.BAL4      = ''                            ; XX1.LOAN1.CHQ = '' ; XX1.LOAN2.CDS = ''
        AC.BAL4.MM   = ''
        AC.BAL4.OVER = ''
        AC.BAL4.LOAN1= ''
        AC.BAL4.LOAN2= ''  ; AC.BAL4.LOAN2.CDS = ''
        AC.BAL5      = ''
        AC.BAL5.MM   = ''
        AC.BAL5.OVER = ''
        AC.BAL5.LOAN1= ''
        AC.BAL5.LOAN2= ''  ; AC.BAL5.LOAN2.CDS = ''
        AC.BAL6      = ''
        AC.BAL6.MM   = ''
        AC.BAL6.OVER = ''
        AC.BAL6.LOAN1= ''
        AC.BAL6.LOAN2= '' ; AC.BAL6.LOAN2.CDS = ''
        AC.BAL7      = ''
        AC.BAL7.MM   = ''
        AC.BAL7.OVER = ''
        AC.BAL7.LOAN1= ''
        AC.BAL7.LOAN2= ''  ; AC.BAL7.LOAN2.CDS = ''
        AC.BAL8      = ''
        AC.BAL8.MM   = ''
        AC.BAL8.OVER = ''
        AC.BAL8.LOAN1= ''
        AC.BAL8.LOAN2= '' ; AC.BAL8.LOAN2.CDS = ''
        AC.BAL2      = ''
        XX1.20 = ''
        XX2.OVER     = ''
        XX3.OVER     = ''
        XX4.OVER     = ''
        XX5.OVER     = ''
        XX6.OVER     = ''
        XX7.OVER     = ''
        XX8.OVER     = ''
        XX1.LOAN2    = ''
        XX2.LOAN2    = ''
        XX3.LOAN2    = ''
        XX4.LOAN2    = ''
        XX5.LOAN2    = ''
        XX6.LOAN2    = ''
        XX7.LOAN2    = ''
        XX8.LOAN2    = ''
        AC.BAL.20 = ''   ; AC.BAL.20.CDS = ''
        AC.BAL1.20 = ''  ; AC.BAL1.20.CDS = ''
        AC.BAL2.20 = ''  ; AC.BAL2.20.CDS = ''
        AC.BAL3.20 = ''  ; AC.BAL3.20.CDS = ''
        AC.BAL4.20 = ''  ; AC.BAL4.20.CDS = ''
        AC.BAL5.20 = ''  ; AC.BAL5.20.CDS = ''
        AC.BAL6.20 = ''  ; AC.BAL6.20.CDS = ''
        AC.BAL7.20 = ''  ; AC.BAL7.20.CDS = ''
        AC.BAL8.20 = ''  ; AC.BAL8.20.CDS = ''
        XX1.LOAN1    = ''
        XX2.LOAN1    = ''
        XX3.LOAN1    = ''
        XX4.LOAN1    = ''
        XX5.LOAN1    = ''
        XX6.LOAN1    = ''
        XX7.LOAN1    = ''
        XX8.LOAN1    = ''
        AC.BAL2      = ''
        YDAYS        = ''
        DAYS         = ''
*=================BEGINING INTER BANK===================================================
        T.SEL= "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 2002 2003 2000 5021 5000 2001 5001 5020 ) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
**   T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 9940000510200003 9940080020200001 0121756710140501 0110023510140501 0110122810120501 0121214710120601 0120913210120601)"
** T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 0110122810120501 0120913210120601)"

        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            CUS.LIM = R.AC<AC.LIMIT.REF>
            CUS.ID  = R.AC<AC.CUSTOMER>
            AC.BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                LMT.REF1    = FIELD(CUS.LIM,'.',1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    XX = CUS.ID:'.':'0000':CUS.LIM
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    XX = CUS.ID:'.':'000':CUS.LIM
                END
*TEXT = "XX: ":XX ; CALL REM
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD  = TODAY
                YDAYS = ''
                DAYS = 'C'
                IF AC.EXP GE AC.TOD THEN
                    CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                END
                AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                IF DAYS GE 1 AND DAYS LE 30 THEN
                    AC.BAL1 += R.AC<AC.OPEN.ACTUAL.BAL>
                END
****************6 MONTHS********************
                IF DAYS GT 30 AND DAYS LE 180 THEN
                    AC.BAL2 +=   R.AC<AC.OPEN.ACTUAL.BAL>
                END
**************UP 1 YEAR*********************
                IF DAYS GT 180 AND DAYS LE 365 THEN
                    AC.BAL4 += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************UP 2 YEAR**************
                IF  DAYS GT 365 AND DAYS LE 730 THEN
                    AC.BAL5 += R.AC<AC.OPEN.ACTUAL.BAL>
                END
*TEXT = "AC.BAL5 ": AC.BAL5
***************FROM 2 TO 5 YEARS***********
                IF DAYS GT 730 AND DAYS LE 1825 THEN
                    AC.BAL6 += R.AC<AC.OPEN.ACTUAL.BAL>
                END
*TEXT = "AC.BAL6 ": AC.BAL6
****************FROM 5 TO 10 YEARS**************
                IF DAYS GT 1825 AND DAYS LE 3650 THEN
                    AC.BAL7 += R.AC<AC.OPEN.ACTUAL.BAL>
                END
*TEXT = "AC.BAL7 ": AC.BAL7
***************OVER 10 YEARS********************
                IF DAYS GT 3650 THEN
                    AC.BAL8 += R.AC<AC.OPEN.ACTUAL.BAL>
                END
*TEXT = "AC.BAL8 " : AC.BAL8 ; CALL REM
            END
***************************ON*************************
            IF  CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO' THEN
                AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
                AC.BAL3 += AC.BAL
                XX1 = FMT(AC.BAL3,"R2")
            END
        NEXT I
***************************************************
        T.SEL2 = "SELECT FBNK.MM.MONEY.MARKET WITH CATEGORY IN (21030 21031) AND CURRENCY EQ  ":CURR.ID
* T.SEL2 = "SELECT FBNK.MM.MONEY.MARKET WITH @ID IN ( MM1108100009 MM1019600004 )"
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
*TEXT = "SELECTED " : SELECTED2 ; CALL REM
        FOR J = 1 TO SELECTED2
            CALL F.READ(FN.MM,KEY.LIST2<J>,R.MM,F.MM,E2)
            CUS.ID.MM  = R.MM<MM.CUSTOMER.ID>
            MM.EXP     = R.MM<MM.MATURITY.DATE>
            MM.AMT     = R.MM<MM.PRINCIPAL>
*TEXT = "AC.BAL2 : " : AC.BAL2 ; CALL REM
*TEXT = " MM.AMT : " : MM.AMT ; CALL REM
            CALL F.READ(FN.CU,CUS.ID.MM,R.CU,F.CU,E2)
            SEC     = R.CU<EB.CUS.SECTOR>
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            AC.TOD = TODAY
            DAYS.MM =  'C'
            IF MM.EXP GE AC.TOD THEN
                CALL CDD ("",AC.TOD,MM.EXP,DAYS.MM)
            END
******************1 MONTHS*******************
            IF DAYS.MM GE 1 AND DAYS.MM LE 30 THEN
                AC.BAL1.MM += R.MM<MM.PRINCIPAL>
            END
            XX2 = AC.BAL1 + AC.BAL1.MM
            IF XX2 = '' THEN
                XX2 = 0
            END
****************6 MONTHS********************
            IF DAYS.MM GT 30 AND DAYS.MM LE 180 THEN
                AC.BAL2.MM += R.MM<MM.PRINCIPAL>
            END
            XX3 = AC.BAL2 + AC.BAL2.MM
            IF XX3 = '' THEN
                XX3 = 0
            END
**************UP 1 YEAR*********************
            IF DAYS.MM GT 180 AND DAYS.MM LE 365 THEN
                AC.BAL4.MM += R.MM<MM.PRINCIPAL>
            END
            XX4 = AC.BAL4 + AC.BAL4.MM
            IF XX4 = '' THEN
                XX4 = 0
            END
************UP 2 YEAR**************
            IF  DAYS.MM GT 365 AND DAYS.MM LE 730 THEN
                AC.BAL5.MM += R.MM<MM.PRINCIPAL>
            END
            XX5 = AC.BAL5 + AC.BAL5.MM
            IF XX5 = '' THEN
                XX5 = 0
            END

***************FROM 2 TO 5 YEARS***********
            IF DAYS.MM GT 730 AND DAYS.MM LE 1825 THEN
                AC.BAL6.MM += R.MM<MM.PRINCIPAL>
            END
            XX6 = AC.BAL6 + AC.BAL6.MM
            IF XX6 = '' THEN
                XX6 = 0
            END

****************FROM 5 TO 10 YEARS**************
            IF DAYS.MM GT 1825 AND DAYS.MM LE 3650 THEN
                AC.BAL7.MM += R.MM<MM.PRINCIPAL>
            END
            XX7 = AC.BAL7   +  AC.BAL7.MM
            IF XX7 = '' THEN
                XX7 = 0
            END

***************OVER 10 YEARS********************
            IF DAYS.MM GT 3650 THEN
                AC.BAL8.MM += R.MM<MM.PRINCIPAL>
            END
            XX8 = AC.BAL8 + AC.BAL8.MM

            IF XX8 = '' THEN
                XX8 = 0
            END
***************************ON*************************
            IF  MM.EXP EQ ''  THEN
                AC.BAL3.MM += R.MM<MM.PRINCIPAL>
            END
            XX1 = FMT(AC.BAL3,"R2")
            IF XX1 = '' THEN
                XX1 = 0
            END
        NEXT J


**********************************************************
*==============================END INTER BANK=======================
*==============================BEGINING CURRENCT ACCOUNT=====================
        T.SEL3 = "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 11242 11238 11240 11232 11234 11239 1208 1211 1212 1216 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 1407 1413 1480 1481 1499 1408 1582 1483 1595 1493 1558 ) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
**  T.SEL3 = "SELECT FBNK.ACCOUNT WITH @ID EQ 0130508210100101"
        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG)
*TEXT = "SEL : ": SELECTED3 ; CALL REM
        FOR K = 1 TO SELECTED3
            CALL F.READ(FN.AC,KEY.LIST3<K>,R.AC,F.AC,ERR)
**TEXT = "KEY.LIST3 : " : KEY.LIST3<K> ; CALL REM
            CUS.ID3 = R.AC<AC.CUSTOMER>
            CUS.LIM3= R.AC<AC.LIMIT.REF>
**TEXT = "CUS.LIM3 : " : CUS.LIM3 ; CALL REM
            IF( CUS.LIM3 NE '' AND CUS.LIM3 NE '0' AND CUS.LIM3 NE 'NOSTRO' ) THEN
                LMT.REF3    = FIELD(CUS.LIM3,'.',1)
                IF LEN(LMT.REF3) EQ 3 THEN
                    XY = CUS.ID3:'.':'0000':CUS.LIM3
                END
                IF LEN(LMT.REF3) EQ 4 THEN
                    XY = CUS.ID3:'.':'000':CUS.LIM3
                END
**TEXT = "XY: ":XY ; CALL REM
                CALL F.READ(FN.LIM,XY,R.LIM,F.LIM,E2)
                AC.EXP3  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD3  = TODAY
                YDAYS    = ''
                DAYS.33 = 'C'
                IF AC.EXP3 GE AC.TOD3 THEN
                    CALL CDD ("",AC.TOD3,AC.EXP3,DAYS.33)
                END
                AC.BAL.OVER = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                IF DAYS.33 GE 1 AND DAYS.33 LE 30 THEN
                    AC.BAL1.OVER += R.AC<AC.OPEN.ACTUAL.BAL>
                END

                XX2.OVER = AC.BAL1.OVER
                IF XX2.OVER = '' THEN
                    XX2.OVER = 0
                END
****************6 MONTHS********************
                IF DAYS.33 GT 30 AND DAYS.33 LE 180 THEN
                    AC.BAL2.OVER +=   R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX3.OVER = AC.BAL2.OVER
                IF XX3.OVER = '' THEN
                    XX3.OVER = 0
                END
**************UP 1 YEAR*********************
                IF DAYS.33 GT 180 AND DAYS.33 LE 365 THEN
                    AC.BAL4.OVER += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX4.OVER = AC.BAL4.OVER
                IF XX4.OVER = '' THEN
                    XX4.OVER = 0
                END

************UP 2 YEAR**************
                IF  DAYS.33 GT 365 AND DAYS.33 LE 730 THEN
                    AC.BAL5.OVER += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX5.OVER = AC.BAL5.OVER
                IF XX5.OVER = '' THEN
                    XX5.OVER = 0
                END

***************FROM 2 TO 5 YEARS***********
                IF DAYS.33 GT 730 AND DAYS.33 LE 1825 THEN
                    AC.BAL6.OVER += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX6.OVER = AC.BAL6.OVER
                IF XX6.OVER = '' THEN
                    XX6.OVER = 0
                END

****************FROM 5 TO 10 YEARS**************
                IF DAYS.33 GT 1825 AND DAYS.33 LE 3650 THEN
                    AC.BAL7.OVER += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX7.OVER = AC.BAL7.OVER
                IF XX7.OVER = '' THEN
                    XX7.OVER = 0
                END

***************OVER 10 YEARS********************
                IF DAYS.33 GT 3650 THEN
                    AC.BAL8.OVER += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX8.OVER = AC.BAL8.OVER
                IF XX8.OVER = '' THEN
                    XX8.OVER = 0
                END
            END
***************************ON*************************
            IF  (CUS.LIM3 EQ '' OR CUS.LIM3 EQ 'NOSTRO') THEN
                AC.BAL.OVER = R.AC<AC.OPEN.ACTUAL.BAL>
                AC.BAL3.OVER += AC.BAL.OVER
            END
            XX1.OVER = FMT(AC.BAL3.OVER,"R2")
            IF XX1.OVER = '' THEN
                XX1.OVER = 0
            END
        NEXT K
*==============================END CURRENCT.ACCOUNT==========================
*=============================BEGINING SAVING.ACCOUNT=================
        T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 6501 6502 6503 6504 6511 16151 16153 16188 16152 11380 16175) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
** T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN (1404)"
        CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ERR.MSG)
*TEXT = "SEL4 : " : SELECTED4 ; CALL REM
        FOR M = 1 TO SELECTED4
            CALL F.READ(FN.AC,KEY.LIST4<M>,R.AC,F.AC,ERR)
            CUS.ID4 = R.AC<AC.CUSTOMER>
            CUS.LIM4= R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID4,R.CU,F.CU,ERR)
            SEC.NEW=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF SEC.NEW EQ 4650 THEN
                IF( CUS.LIM4 NE '' AND CUS.LIM4 NE '0' AND CUS.LIM4 NE 'NOSTRO' ) THEN
                    LMT.REF4    = FIELD(CUS.LIM4,'.',1)
                    IF LEN(LMT.REF4) EQ 3 THEN
                        XY4 = CUS.ID4:'.':'0000':CUS.LIM4
                    END
                    IF LEN(LMT.REF4) EQ 4 THEN
                        XY4 = CUS.ID4:'.':'000':CUS.LIM4
                    END
*TEXT = "XY: ":XY ; CALL REM
                    CALL F.READ(FN.LIM,XY4,R.LIM,F.LIM,E2)
                    AC.EXP4  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD4  = TODAY
                    DAYS.44 = 'C'
                    IF AC.EXP4 GE AC.TOD4 THEN
                        CALL CDD ("",AC.TOD4,AC.EXP4,DAYS.44)
                    END
                    AC.BAL.LOAN1 = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                    IF DAYS.44 GE 1 AND DAYS.44 LE 30 THEN
                        AC.BAL1.LOAN1 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END

                    XX2.LOAN1 = AC.BAL1.LOAN1
                    IF XX2.LOAN1 = '' THEN
                        XX2.LOAN1 = 0
                    END

****************6 MONTHS********************
                    IF DAYS.44 GT 30 AND DAYS.44 LE 180 THEN
                        AC.BAL2.LOAN1 +=   R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                    XX3.LOAN1 = AC.BAL2.LOAN1
                    IF XX3.LOAN1 = '' THEN
                        XX3.LOAN1 = 0
                    END
**************UP 1 YEAR*********************
                    IF DAYS.44 GT 180 AND DAYS.44 LE 365 THEN
                        AC.BAL4.LOAN1 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                    XX4.LOAN1 = AC.BAL4.LOAN1
                    IF XX4.LOAN1 = '' THEN
                        XX4.LOAN1 = 0
                    END
************UP 2 YEAR**************
                    IF  DAYS.44 GT 365 AND DAYS.44 LE 730 THEN
                        AC.BAL5.LOAN1 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                    XX5.LOAN1 = AC.BAL5.LOAN1
                    IF XX5.LOAN1 = '' THEN
                        XX5.LOAN1 = 0
                    END
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.44 GT 730 AND DAYS.44 LE 1825 THEN
                        AC.BAL6.LOAN1 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                    XX6.LOAN1 = AC.BAL6.LOAN1
                    IF XX6.LOAN1 = '' THEN
                        XX6.LOAN1 = 0
                    END
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.44 GT 1825 AND DAYS.44 LE 3650 THEN
                        AC.BAL7.LOAN1 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                    XX7.LOAN1 = AC.BAL7.LOAN1
                    IF XX7.LOAN1 = '' THEN
                        XX7.LOAN1 = 0
                    END
***************OVER 10 YEARS********************
                    IF DAYS.44 GT 3650 THEN
                        AC.BAL8.LOAN1 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                    XX8.LOAN1 = AC.BAL8.LOAN1
                    IF XX8.LOAN1 = '' THEN
                        XX8.LOAN1 = 0
                    END
                END
***************************ON*************************
                IF  (CUS.LIM4 EQ '' OR CUS.LIM4 EQ 'NOSTRO') THEN
                    AC.BAL.LOAN1 = R.AC<AC.OPEN.ACTUAL.BAL>
                    AC.BAL3.LOAN1 += AC.BAL.LOAN1
                END
**TEXT = "XX1.LOAN1 : " : AC.BAL3.LOAN1 ; CALL REM
                XX1.LOAN1 = AC.BAL3.LOAN1
                IF AC.BAL3.LOAN1 EQ '' THEN
                    AC.BAL3.LOAN1 = 0
                END
            END
        NEXT M
*=======================END SAVING.ACCOUNT==============================
*=======================BEGINING CORPRATE LOANS=======================
** T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1102 1404 1414 1206 1208 1401 1402 1405 1406 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591)"
***** T.SEL6="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY IN ( 21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032 ) AND CURRENCY EQ USD "
* T.SEL6="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ((VALUE.DATE LE ":HHH:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":HHH:" AND AMOUNT EQ 0 )) AND CATEGORY IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032) AND CURRENCY EQ  " :CURR.ID
        T.SEL6="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ((VALUE.DATE LE ":HHH:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":HHH:" AND AMOUNT EQ 0 )) AND CATEGORY IN (21001 21002 21003 21004 21005 21006 21007 21008 21009 21010) AND CURRENCY EQ  " :CURR.ID
** T.SEL6="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ((VALUE.DATE LE 20110531 AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT 20110531 AND AMOUNT EQ 0 )) AND CATEGORY IN (21032) AND CURRENCY EQ USD "
        CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ERR.MSG)
        **TEXT = "SEL6 : " : SELECTED6 ; CALL REM
        FOR NN = 1 TO SELECTED6
            CALL F.READ(FN.LD,KEY.LIST6<NN>,R.LD,F.LD,ERR)
            CUS.ID6 = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CU,CUS.ID6,R.CU,F.CU,ERR)
            SEC.NEW=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            AC.EXP5  = R.LD<LD.FIN.MAT.DATE>
            AC.TOD5  = TODAY
            DAYS.55  = 'C'
            IF AC.EXP5 GE AC.TOD5 THEN
                CALL CDD ("",AC.TOD5,AC.EXP5,DAYS.55)
            END
            IF R.LD<LD.AMOUNT> NE 0 THEN
                AMT = R.LD<LD.AMOUNT>
            END ELSE
                AMT = R.LD<LD.REIMBURSE.AMOUNT>
            END
           * IF SEC.NEW NE 4650 AND SEC.NEW NE '' THEN
                IF AC.EXP5 NE '' OR AC.EXP5 NE 0 THEN

******************1 MONTHS*******************
                    IF DAYS.55 GE 0 AND DAYS.55 LE 30 THEN

                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL1.LOAN2 += AMT
                    END
****************6 MONTHS********************
                    IF DAYS.55 GT 30 AND DAYS.55 LE 180 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL2.LOAN2 +=  AMT
                    END

**************UP 1  YEAR*********************
                    IF DAYS.55 GT 180 AND DAYS.55 LE 365 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END

                        AC.BAL4.LOAN2 += AMT
                    END
************UP 2 YEAR**************
                    IF  DAYS.55 GT 365 AND DAYS.55 LE 730 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL5.LOAN2 += AMT
                    END
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.55 GT 730 AND DAYS.55 LE 1825 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL6.LOAN2 += AMT
                    END
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.55 GT 1825 AND DAYS.55 LE 3650 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL7.LOAN2 += AMT
                    END
***************OVER 10 YEARS********************
                    IF DAYS.55 GT 3650 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL8.LOAN2 += AMT
                    END
                END
***************************ON*************************
**    IF  (CUS.LIM4 EQ '' OR CUS.LIM4 EQ 'NOSTRO') THEN
                IF  AC.EXP5 EQ '' THEN
                    IF R.LD<LD.AMOUNT> NE 0 THEN
                        AMT = R.LD<LD.AMOUNT>
                    END ELSE
                        AMT = R.LD<LD.REIMBURSE.AMOUNT>
                    END
                    AC.BAL.LOAN2 = AMT
                    AC.BAL3.LOAN2 += AC.BAL.LOAN2
                END
            *END
        NEXT NN
        ***TEXT = "AC.BAL1.LOAN2 : " : AC.BAL1.LOAN2 ; CALL REM
*****SECOND PART *************
**  T.SEL20= "SELECT FBNK.ACCOUNT WITH CATEGORY IN (6512 1012 1013 1014 1015 1016 1019 16170 3011 3012 3013 3014 3017 3010 3005) AND CURRENCY EQ " : CURR.ID :"  AND  OPEN.ACTUAL.BAL GT 0 "
        T.SEL20= "SELECT FBNK.ACCOUNT WITH CATEGORY IN (6512) AND CURRENCY EQ " : CURR.ID :"  AND  OPEN.ACTUAL.BAL GT 0 "
**   T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 9940000510200003 9940080020200001 0121756710140501 0110023510140501 0110122810120501 0121214710120601 0120913210120601)"
** T.SEL20= "SELECT FBNK.ACCOUNT WITH @ID IN ( 0130469410101601 3030089010300501 )"

        CALL EB.READLIST(T.SEL20,KEY.LIST20,"",SELECTED20,ER.MSG)
        FOR QQ = 1 TO SELECTED20
            CALL F.READ(FN.AC,KEY.LIST20<QQ>,R.AC,F.AC,E2)
            CUS.LIM20 = R.AC<AC.LIMIT.REF>
* TEXT = "CUS.LIM20 : " : CUS.LIM20 ; CALL REM
            CUS.ID20  = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID20,R.CU,F.CU,ERR)
            SEC.NEW.20=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
* TEXT = "SEC.NEW.20 :" : SEC.NEW.20 ; CALL REM
            AC.BAL20  = R.AC<AC.OPEN.ACTUAL.BAL>
           * IF ( SEC.NEW.20 NE 4650 AND SEC.NEW.20 NE '') THEN
                IF( CUS.LIM20 NE '' AND CUS.LIM20 NE '0' AND CUS.LIM20 NE 'NOSTRO' ) THEN
                    LMT.REF1.20    = FIELD(CUS.LIM20,'.',1)
                    IF LEN(LMT.REF1.20) EQ 3 THEN
                        XXZZ = CUS.ID20:'.':'0000':CUS.LIM20
                    END
                    IF LEN(LMT.REF1.20) EQ 4 THEN
                        XXZZ = CUS.ID20:'.':'000':CUS.LIM20
                    END
                    CALL F.READ(FN.LIM,XXZZ,R.LIM,F.LIM,E2)
                    AC.EXP20  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD20  = TODAY
                    YDAYS20 = ''
                    DAYS.20 = 'C'
                    IF AC.EXP20 GE AC.TOD20 THEN
                        CALL CDD ("",AC.TOD20,AC.EXP20,DAYS.20)
                    END
                    AC.BAL.20 = R.AC<AC.OPEN.ACTUAL.BAL>
* IF ( SEC.NEW.20 NE 4650 AND SEC.NEW.20 NE '') THEN
******************1 MONTHS*******************
                    IF DAYS.20 GE 0 AND DAYS.20 LE 30 THEN
                        AC.BAL1.20 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END

****************6 MONTHS********************
                    IF DAYS.20 GT 30 AND DAYS.20 LE 180 THEN
                        AC.BAL2.20 +=   R.AC<AC.OPEN.ACTUAL.BAL>
                    END
**************UP 1 YEAR*********************
                    IF DAYS.20 GT 180 AND DAYS.20 LE 365 THEN
                        AC.BAL4.20 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
************UP 2 YEAR**************
                    IF  DAYS.20 GT 365 AND DAYS.20 LE 730 THEN
                        AC.BAL5.20 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.20 GT 730 AND DAYS.20 LE 1825 THEN
                        AC.BAL6.20 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.20 GT 1825 AND DAYS.20 LE 3650 THEN
                        AC.BAL7.20 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
***************OVER 10 YEARS********************
                    IF DAYS.20 GT 3650 THEN
                        AC.BAL8.20 += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
                END
***************************ON*************************
**    TEXT = "INTP UF11111 " : CUS.LIM20 ; CALL REM
                IF  (CUS.LIM20 EQ '' OR CUS.LIM20 EQ 'NOSTRO') THEN
** TEXT = "INTP UF2222 " : CUS.LIM20 ; CALL REM
                    AC.BAL.20 = R.AC<AC.OPEN.ACTUAL.BAL>
                    AC.BAL3.20 += AC.BAL.20
**  TEXT = "AC.BAL3.20 :": AC.BAL3.20 ; CALL REM
                    XX1.20 = FMT(AC.BAL3.20,"R2")
                END
                XX1.LOAN2 = AC.BAL3.LOAN2 + AC.BAL3.20
            *END
        NEXT QQ

        XX2.LOAN2 = AC.BAL1.LOAN2 + AC.BAL1.20
        XX3.LOAN2 = AC.BAL2.LOAN2 + AC.BAL2.20
        XX4.LOAN2 = AC.BAL4.LOAN2 + AC.BAL4.20
        XX5.LOAN2 = AC.BAL5.LOAN2 + AC.BAL5.20
        XX6.LOAN2 = AC.BAL6.LOAN2 + AC.BAL6.20
        XX7.LOAN2 = AC.BAL7.LOAN2 + AC.BAL7.20
        XX8.LOAN2 = AC.BAL8.LOAN2 + AC.BAL8.20

*=============================END CORPRATE LOANS====================
*=============================BEGIN CD'S=================
** T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1102 1404 1414 1206 1208 1401 1402 1405 1406 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591)"
***** T.SEL6="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY IN ( 21001 21002 21003 21004 21005 21006 21007 21008 21009 21010 21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032 ) AND CURRENCY EQ USD "
        T.SEL6.CDS="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ((VALUE.DATE LE ":HHH:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":HHH:" AND AMOUNT EQ 0 )) AND CATEGORY IN (21020 21021 21022 21023 21024 21025 21026 21027 21028 21029 21032) AND CURRENCY EQ  " :CURR.ID
** T.SEL6="SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ((VALUE.DATE LE 20110531 AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT 20110531 AND AMOUNT EQ 0 )) AND CATEGORY IN (21032) AND CURRENCY EQ USD "
        CALL EB.READLIST(T.SEL6.CDS,KEY.LIST6.CDS,"",SELECTED6.CDS,ERR.MSG.CDS)
*TEXT = "SEL6 : " : SELECTED6 ; CALL REM
        FOR NNN = 1 TO SELECTED6.CDS
            CALL F.READ(FN.LD,KEY.LIST6.CDS<NNN>,R.LD,F.LD,ERR)
            CUS.ID6.CDS = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CU,CUS.ID6.CDS,R.CU,F.CU,ERR)
            SEC.NEW.CDS = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            AC.EXP5.CDS  = R.LD<LD.FIN.MAT.DATE>
            AC.TOD5.CDS  = TODAY
            DAYS.55.CDS  = 'C'
            IF AC.EXP5.CDS GE AC.TOD5.CDS THEN
                CALL CDD ("",AC.TOD5.CDS,AC.EXP5.CDS,DAYS.55.CDS)
            END
            IF R.LD<LD.AMOUNT> NE 0 THEN
                AMT.CDS = R.LD<LD.AMOUNT>
            END ELSE
                AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
            END

           * IF SEC.NEW.CDS EQ 4650 THEN
                IF AC.EXP5.CDS NE '' OR AC.EXP5.CDS NE 0 THEN
******************1 MONTHS*******************
                    IF DAYS.55.CDS GE 0 AND DAYS.55.CDS LE 30 THEN

                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL1.LOAN2.CDS += AMT.CDS
                    END
****************6 MONTHS********************
                    IF DAYS.55.CDS GT 30 AND DAYS.55.CDS LE 180 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL2.LOAN2.CDS +=  AMT.CDS
                    END

**************UP 1  YEAR*********************
                    IF DAYS.55.CDS GT 180 AND DAYS.55.CDS LE 365 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END

                        AC.BAL4.LOAN2.CDS += AMT.CDS
                    END
************UP 2 YEAR**************
                    IF  DAYS.55.CDS GT 365 AND DAYS.55.CDS LE 730 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL5.LOAN2.CDS += AMT.CDS
                    END
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.55.CDS GT 730 AND DAYS.55.CDS LE 1825 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL6.LOAN2.CDS += AMT.CDS
                    END
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.55.CDS GT 1825 AND DAYS.55.CDS LE 3650 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL7.LOAN2.CDS += AMT.CDS
                    END
***************OVER 10 YEARS********************
                    IF DAYS.55.CDS GT 3650 THEN
                        IF R.LD<LD.AMOUNT> NE 0 THEN
                            AMT.CDS = R.LD<LD.AMOUNT>
                        END ELSE
                            AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                        END
                        AC.BAL8.LOAN2.CDS += AMT.CDS
                    END
                END
***************************ON*************************
**    IF  (CUS.LIM4.CDS EQ '' OR CUS.LIM4.CDS EQ 'NOSTRO') THEN
                IF  AC.EXP5.CDS EQ '' THEN
                    IF R.LD<LD.AMOUNT> NE 0 THEN
                        AMT.CDS = R.LD<LD.AMOUNT>
                    END ELSE
                        AMT.CDS = R.LD<LD.REIMBURSE.AMOUNT>
                    END
                    AC.BAL.LOAN2.CDS = AMT.CDS
                    AC.BAL3.LOAN2.CDS += AC.BAL.LOAN2.CDS
                END
            *END
        NEXT NNN

*****SECOND PART *************
   *     T.SEL20.CDS= "SELECT FBNK.ACCOUNT WITH CATEGORY IN (6512 1012 1013 1014 1015 1016 1019 16170 3011 3012 3013 3014 3017 3010 3005) AND CURRENCY EQ " : CURR.ID :" AND OPEN.ACTUAL.BAL GT 0 "
**   T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 9940000510200003 9940080020200001 0121756710140501 0110023510140501 0110122810120501 0121214710120601 0120913210120601)"
** T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 0110122810120501 0120913210120601)"

        CALL EB.READLIST(T.SEL20.CDS,KEY.LIST20.CDS,"",SELECTED20.CDS,ER.MSG.CDS)
        FOR QQQ = 1 TO SELECTED20.CDS
            CALL F.READ(FN.AC,KEY.LIST20.CDS<QQQ>,R.AC,F.AC,E2)
            CUS.LIM20.CDS = R.AC<AC.LIMIT.REF>
            CUS.ID20.CDS  = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID20.CDS,R.CU,F.CU,ERR)
            SEC.NEW.CDS =R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            AC.BAL20.CDS  = R.AC<AC.OPEN.ACTUAL.BAL>
            IF SEC.NEW.CDS EQ 4650 THEN
                IF( CUS.LIM20.CDS NE '' AND CUS.LIM20.CDS NE '0' AND CUS.LIM20.CDS NE 'NOSTRO' ) THEN
                    LMT.REF1.20.CDS    = FIELD(CUS.LIM20.CDS,'.',1)
                    IF LEN(LMT.REF1.20.CDS) EQ 3 THEN
                        XXZZ.CDS = CUS.ID20.CDS:'.':'0000':CUS.LIM20.CDS
                    END
                    IF LEN(LMT.REF1.20.CDS) EQ 4 THEN
                        XXZZ.CDS = CUS.ID20.CDS:'.':'000':CUS.LIM20.CDS
                    END
*TEXT = "XX: ":XX ; CALL REM
                    CALL F.READ(FN.LIM,XXZZ.CDS,R.LIM,F.LIM,E2)
                    AC.EXP20.CDS  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD20.CDS  = TODAY
                    YDAYS20.CDS   = ''
                    DAYS.20.CDS   = 'C'
                    IF AC.EXP20.CDS GE AC.TOD20.CDS THEN
                        CALL CDD ("",AC.TOD20,AC.EXP20,DAYS.20)
                    END
                    AC.BAL.20.CDS = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                    IF DAYS.20.CDS GE 0 AND DAYS.20.CDS LE 30 THEN
                        AC.BAL1.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
                    END

****************6 MONTHS********************
                    IF DAYS.20.CDS GT 30 AND DAYS.20.CDS LE 180 THEN
                        AC.BAL2.20.CDS +=   R.AC<AC.OPEN.ACTUAL.BAL>
                    END
**************UP 1 YEAR*********************
                    IF DAYS.20.CDS GT 180 AND DAYS.20.CDS LE 365 THEN
                        AC.BAL4.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
************UP 2 YEAR**************
                    IF  DAYS.20.CDS GT 365 AND DAYS.20.CDS LE 730 THEN
                        AC.BAL5.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
*TEXT = "AC.BAL5 ": AC.BAL5
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.20.CDS GT 730 AND DAYS.20.CDS LE 1825 THEN
                        AC.BAL6.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
*TEXT = "AC.BAL6 ": AC.BAL6
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.20.CDS GT 1825 AND DAYS.20.CDS LE 3650 THEN
                        AC.BAL7.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
*TEXT = "AC.BAL7 ": AC.BAL7
***************OVER 10 YEARS********************
                    IF DAYS.20.CDS GT 3650 THEN
                        AC.BAL8.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
                    END
*TEXT = "AC.BAL8 " : AC.BAL8 ; CALL REM
                END
*  XX2.LOAN2 = AC.BAL1.LOAN2 + AC.BAL1.20
*  XX3.LOAN2 = AC.BAL2.LOAN2 + AC.BAL2.20
*  XX4.LOAN2 = AC.BAL4.LOAN2 + AC.BAL4.20
*  XX5.LOAN2 = AC.BAL5.LOAN2 + AC.BAL5.20
*  XX6.LOAN2 = AC.BAL6.LOAN2 + AC.BAL6.20
*  XX7.LOAN2 = AC.BAL7.LOAN2 + AC.BAL7.20
*  XX8.LOAN2 = AC.BAL8.LOAN2 + AC.BAL8.20
***************************ON*************************
                IF  (CUS.LIM20.CDS EQ '' OR CUS.LIM20.CDS EQ 'NOSTRO') THEN
* TEXT = "IN IF " ;  CALL REM
                    AC.BAL.20.CDS = R.AC<AC.OPEN.ACTUAL.BAL>
                    AC.BAL3.20.CDS += R.AC<AC.OPEN.ACTUAL.BAL>
* TEXT =  "AC.BAL3.20.CDS : " : AC.BAL3.20.CDS ; CALL REM

                    XX1.20.CDS = FMT(AC.BAL3.20,"R2")
                END
            END
        NEXT QQQ

        XX1.LOAN2.CDS = AC.BAL3.LOAN2.CDS + AC.BAL3.20.CDS
        XX2.LOAN2.CDS = AC.BAL1.LOAN2.CDS + AC.BAL1.20.CDS
        XX3.LOAN2.CDS = AC.BAL2.LOAN2.CDS + AC.BAL2.20.CDS
        XX4.LOAN2.CDS = AC.BAL4.LOAN2.CDS + AC.BAL4.20.CDS
        XX5.LOAN2.CDS = AC.BAL5.LOAN2.CDS + AC.BAL5.20.CDS
        XX6.LOAN2.CDS = AC.BAL6.LOAN2.CDS + AC.BAL6.20.CDS
        XX7.LOAN2.CDS = AC.BAL7.LOAN2.CDS + AC.BAL7.20.CDS
        XX8.LOAN2.CDS = AC.BAL8.LOAN2.CDS + AC.BAL8.20.CDS

*=============================END CD'S ====================
*============================BEGIN OTHER=====================
        T.SEL4.CHQ="SELECT FBNK.ACCOUNT WITH CATEGORY IN (16151 16153 16188 16152 11380 16170 16175 3011 3012 3013 3014 3017 3010 3005 1012 1013 1014 1015 1016 1019 16113) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
** T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN (1404)"
        CALL EB.READLIST(T.SEL4.CHQ,KEY.LIST4.CHQ,"",SELECTED4.CHQ,ERR.MSG)
*TEXT = "SEL4 : " : SELECTED4.CHQ ; CALL REM
        FOR MMM = 1 TO SELECTED4.CHQ
            CALL F.READ(FN.AC,KEY.LIST4.CHQ<MMM>,R.AC,F.AC,ERR)
            CUS.ID4.CHQ = R.AC<AC.CUSTOMER>
            CUS.LIM4.CHQ= R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID4.CHQ,R.CU,F.CU,ERR)
            SEC.NEW.CHQ=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
*TEXT = "SEC.NEW.CHQ :":SEC.NEW.CHQ ; CALL REM
* IF SEC.NEW.CHQ EQ 4650 THEN
            IF( CUS.LIM4.CHQ NE '' AND CUS.LIM4.CHQ NE '0' AND CUS.LIM4.CHQ NE 'NOSTRO' ) THEN
                LMT.REF4.CHQ   = FIELD(CUS.LIM4.CHQ,'.',1)
                IF LEN(LMT.REF4.CHQ) EQ 3 THEN
                    XY4.CHQ = CUS.ID4.CHQ:'.':'0000':CUS.LIM4.CHQ
                END
                IF LEN(LMT.REF4.CHQ) EQ 4 THEN
                    XY4.CHQ = CUS.ID4.CHQ:'.':'000':CUS.LIM4.CHQ
                END
*TEXT = "XY: ":XY ; CALL REM
                CALL F.READ(FN.LIM,XY4.CHQ,R.LIM,F.LIM,E2)
                AC.EXP4.CHQ  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD4.CHQ  = TODAY
                DAYS.44.CHQ = 'C'
                IF AC.EXP4.CHQ GE AC.TOD4.CHQ THEN
                    CALL CDD ("",AC.TOD4.CHQ,AC.EXP4.CHQ,DAYS.44.CHQ)
                END
                AC.BAL.LOAN1.CHQ = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                IF DAYS.44.CHQ GE 1 AND DAYS.44.CHQ LE 30 THEN
                    AC.BAL1.LOAN1.CHQ += R.AC<AC.OPEN.ACTUAL.BAL>
                END

                XX2.LOAN1.CHQ = AC.BAL1.LOAN1.CHQ
                IF XX2.LOAN1.CHQ = '' THEN
                    XX2.LOAN1.CHQ = 0
                END

****************6 MONTHS********************
                IF DAYS.44.CHQ GT 30 AND DAYS.44.CHQ LE 180 THEN
                    AC.BAL2.LOAN1.CHQ +=   R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX3.LOAN1.CHQ = AC.BAL2.LOAN1.CHQ
                IF XX3.LOAN1.CHQ = '' THEN
                    XX3.LOAN1.CHQ = 0
                END
**************UP 1 YEAR*********************
                IF DAYS.44.CHQ GT 180 AND DAYS.44.CHQ LE 365 THEN
                    AC.BAL4.LOAN1.CHQ += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX4.LOAN1.CHQ = AC.BAL4.LOAN1.CHQ
                IF XX4.LOAN1.CHQ = '' THEN
                    XX4.LOAN1.CHQ = 0
                END
************UP 2 YEAR**************
                IF  DAYS.44.CHQ GT 365 AND DAYS.44.CHQ LE 730 THEN
                    AC.BAL5.LOAN1.CHQ += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX5.LOAN1.CHQ = AC.BAL5.LOAN1.CHQ
                IF XX5.LOAN1.CHQ = '' THEN
                    XX5.LOAN1.CHQ = 0
                END
***************FROM 2 TO 5 YEARS***********
                IF DAYS.44.CHQ GT 730 AND DAYS.44.CHQ LE 1825 THEN
                    AC.BAL6.LOAN1.CHQ += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX6.LOAN1.CHQ = AC.BAL6.LOAN1.CHQ
                IF XX6.LOAN1.CHQ = '' THEN
                    XX6.LOAN1.CHQ = 0
                END
****************FROM 5 TO 10 YEARS**************
                IF DAYS.44.CHQ GT 1825 AND DAYS.44.CHQ LE 3650 THEN
                    AC.BAL7.LOAN1.CHQ += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX7.LOAN1.CHQ = AC.BAL7.LOAN1.CHQ
                IF XX7.LOAN1.CHQ = '' THEN
                    XX7.LOAN1.CHQ = 0
                END
***************OVER 10 YEARS********************
                IF DAYS.44.CHQ GT 3650 THEN
                    AC.BAL8.LOAN1.CHQ += R.AC<AC.OPEN.ACTUAL.BAL>
                END
                XX8.LOAN1.CHQ = AC.BAL8.LOAN1.CHQ
                IF XX8.LOAN1.CHQ = '' THEN
                    XX8.LOAN1.CHQ = 0
                END
            END
***************************ON*************************
            IF  (CUS.LIM4.CHQ EQ '' OR CUS.LIM4.CHQ EQ 'NOSTRO') THEN

                AC.BAL.LOAN1.CHQ = R.AC<AC.OPEN.ACTUAL.BAL>
                AC.BAL3.LOAN1.CHQ += AC.BAL.LOAN1.CHQ
*TEXT = "AC.BAL3 : " : AC.BAL3.LOAN1.CHQ ; CALL REM
            END
**TEXT = "XX1.LOAN1 : " : AC.BAL3.LOAN1 ; CALL REM
            XX1.LOAN1.CHQ = AC.BAL3.LOAN1.CHQ
            IF AC.BAL3.LOAN1.CHQ EQ '' THEN
                AC.BAL3.LOAN1.CHQ = 0
            END
*END
        NEXT MMM
*===========================END OTHER=========================

        BB.DATA  = CURR.ID :"," :COMP
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*****************************ON***********
        BB.DATA  = "ON":","
        BB.DATA := XX1:","
        BB.DATA := XX1.OVER :","
        BB.DATA := XX1.LOAN1:","
        BB.DATA := XX1.LOAN2:","
        BB.DATA := XX1.LOAN2.CDS:","
        BB.DATA := XX1.LOAN1.CHQ:","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************ON*******************
**************************1M ************
        BB.DATA  = "1M":","
        BB.DATA := XX2:","
        BB.DATA := XX2.OVER:","
        BB.DATA := XX2.LOAN1:","
        BB.DATA := XX2.LOAN2:","
        BB.DATA := XX2.LOAN2.CDS:","
        BB.DATA := XX2.LOAN1.CHQ:","
* TEXT = "XX2.LOAN2 " : XX2.LOAN2 ; CALL REM

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 M*****************
************************6M*******************
        BB.DATA  = "6M":","
        BB.DATA := XX3:","
        BB.DATA := XX3.OVER:","
        BB.DATA := XX3.LOAN1:","
        BB.DATA := XX3.LOAN2:","
        BB.DATA := XX3.LOAN2.CDS:","
        BB.DATA := XX3.LOAN1.CHQ:","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**********************6M************************
*********************1 YEAR********************
        BB.DATA  = "1 YEAR":","
        BB.DATA := XX4:","
        BB.DATA := XX4.OVER:","
        BB.DATA := XX4.LOAN1:","
        BB.DATA := XX4.LOAN2:","
        BB.DATA := XX4.LOAN2.CDS:","
        BB.DATA := XX4.LOAN1.CHQ:","
*TEXT = "BR.DATA : " : BB.DATA ; CALL REM
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************1 YEAR*******************
***************************2 YEAR*************
        BB.DATA  = "2 YEAR":","
        BB.DATA :=  XX5:","
        BB.DATA :=  XX5.OVER:","
        BB.DATA :=  XX5.LOAN1:","
        BB.DATA :=  XX5.LOAN2:","
        BB.DATA :=  XX5.LOAN2.CDS:","
        BB.DATA :=  XX5.LOAN1.CHQ:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************2 YEAR********************
*************************2 - 5 YEAR*************
        BB.DATA  = "2 - 5 YEAR":","
        BB.DATA := XX6:","
        BB.DATA :=  XX6.OVER:","
        BB.DATA :=  XX6.LOAN1:","
        BB.DATA :=  XX6.LOAN2:","
        BB.DATA :=  XX6.LOAN2.CDS:","
        BB.DATA :=  XX6.LOAN1.CHQ:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************2 - 5 YEAR******************
************************10 -5 YEAR*******************8
        BB.DATA  = "5 -10 YEAR":","
        BB.DATA := XX7:","
        BB.DATA :=  XX7.OVER:","
        BB.DATA :=  XX7.LOAN1:","
        BB.DATA :=  XX7.LOAN2:","
        BB.DATA :=  XX7.LOAN2.CDS:","
        BB.DATA :=  XX7.LOAN1.CHQ:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************5 TO 10 YEAR***************
*************************OVER 10********************
        BB.DATA  = "OVER 10":","
        BB.DATA := XX8:","
        BB.DATA :=  XX8.OVER:","
        BB.DATA :=  XX8.LOAN1:","
        BB.DATA :=  XX8.LOAN2:","
        BB.DATA :=  XX8.LOAN2.CDS:","
        BB.DATA :=  XX8.LOAN1.CHQ:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    NEXT KKKK
*******************10 OVER*******************
*===============================================================
    RETURN
END
