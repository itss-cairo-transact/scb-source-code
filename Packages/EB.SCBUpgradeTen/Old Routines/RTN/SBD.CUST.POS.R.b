* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>4754</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.CUST.POS.R

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.POS.TODAY
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*-------------------------------------------------------------------------
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*    TEXT = " BEGINING OF REPORT  "; CALL REM
*-------------------------------------------------------------------------
*Line [ 72 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.CUST.POS.R'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    FN.CUS = 'FBNK.CUSTOMER'         ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACC = 'FBNK.ACCOUNT'          ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.LDD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LDD = '' ; R.LDD = ''
    CALL OPF(FN.LDD,F.LDD)
    FN.LGG = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LGG = '' ; R.LGG = ''
    CALL OPF(FN.LGG,F.LGG)
    FN.CDD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.CDD = '' ; R.CDD = ''
    CALL OPF(FN.CDD,F.CDD)
    FN.LCIMP = 'FBNK.LETTER.OF.CREDIT'
    F.LCIMP = '' ; R.LCIMP = ''
    CALL OPF(FN.LCIMP,F.LCIMP)
    FN.LCEXP = 'FBNK.LETTER.OF.CREDIT'
    F.LCEXP = '' ; R.LCEXP = ''
    CALL OPF(FN.LCEXP,F.LCEXP)
    FN.LCCOL = 'FBNK.LETTER.OF.CREDIT'
    F.LCCOL = '' ; R.LCCOL = ''
    CALL OPF(FN.LCCOL,F.LCCOL)
    FN.POS = 'F.SCB.CUST.POS.TODAY'  ; F.POS = '' ; R.POS = ''
    CALL OPF(FN.POS,F.POS)
    FN.CAT = 'F.CATEGORY'            ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'FBNK.CURRENCY'         ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.LIM = 'FBNK.LIMIT'            ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)
    FN.COL = 'FBNK.COLLATERAL'       ; F.COL = '' ; R.COL = ''
    CALL OPF(FN.COL,F.COL)
    FN.EVT = 'FBNK.AC.LOCKED.EVENTS' ; F.EVT = '' ; R.EVT = ''
    CALL OPF(FN.EVT,F.EVT)

    KEY.LIST ="" ; SELECTED ="" ; ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ; ER.MSG1=""
    XNUMBER  = 0
    YYNUMBER = 0
    ZZNUMBER = 0
*    TDD = TODAY
*    CALL CDT("",TDD,'-1W')
*=================================================================

    T.SEL = "SELECT ":FN.POS:" WITH @ID UNLIKE 99... AND CO.CODE EQ ":COMP:" BY OLD.CUST.ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED  THEN

        XX = SPACE(132) ; XX1 = SPACE(132) ; XX2 = SPACE(132) ; XX3 = SPACE(132) ; XX4 = SPACE(132) ; XX5 = SPACE(132) ; XX6 = SPACE(132) ; XX7 = SPACE(132)

        K=0 ; K1=0 ; K2=0 ; K3=0 ; K4=0 ; K5=0 ; K6=0 ; K7=0

        POS.ID = '' ; CUS.ID = '' ; CUS.ZZ = '' ; CUS.NAME = ''

        AC.COD   = '' ; AC.ID         = ''
        AC.BAL   = '' ; AC.LIM.REF    = ''
        AC.CUR   = '' ; AC.CUR.NAME   = '' ; AC.LIM.REFS = ''
        AC.CAT   = '' ; AC.CAT.NAME   = ''
        AC.LIMIT = '' ; AC.LOCK       = ''
        AC.LIST  = '' ; SELECTED.AC   = '' ; ER.AC       = ''

        LD.COD   = '' ; LD.ID         = ''
        LD.BAL   = '' ; LD.LIM.REF    = ''
        LD.CUR   = '' ; LD.CUR.NAME   = '' ; LD.LIM.REFS= ''
        LD.CAT   = '' ; LD.CAT.NAME   = ''
        LD.LIMIT = '' ; LD.COLL       = ''
        LD.LIST  = '' ; SELECTED.LD   = '' ; ER.LD      = ''

        LG.COD   = '' ; LG.ID         = ''
        LG.BAL   = '' ; LG.LIM.REF    = ''
        LG.CUR   = '' ; LG.CUR.NAME   = '' ; LG.LIM.REFS= ''
        LG.CAT   = '' ; LG.CAT.NAME   = ''
        LG.LIMIT = '' ; LG.COLL       = ''
        LG.LIST  = '' ; SELECTED.LG   = '' ; ER.LG      = ''

        CD.COD   = '' ; CD.ID         = ''
        CD.BAL   = '' ; CD.LIM.REF    = ''
        CD.CUR   = '' ; CD.CUR.NAME   = '' ; CD.LIM.REFS= ''
        CD.CAT   = '' ; CD.CAT.NAME   = ''
        CD.LIMIT = '' ; CD.COLL       = ''
        CD.LIST  = '' ; SELECTED.CD   = '' ; ER.CD      = ''

        LCIMP.COD   = '' ; LCIMP.ID         = ''
        LCIMP.BAL   = '' ; LCIMP.LIM.REF    = ''
        LCIMP.CUR   = '' ; LCIMP.CUR.NAME   = '' ; LCIMP.LIM.REFS= ''
        LCIMP.CAT   = '' ; LCIMP.CAT.NAME   = ''
        LCIMP.LIMIT = '' ; LCIMP.COLL       = ''
        LCIMP.LIST  = '' ; SELECTED.LCIMP   = '' ; ER.LCIMP      = ''

        LCEXP.COD   = '' ; LCEXP.ID         = ''
        LCEXP.BAL   = '' ; LCEXP.LIM.REF    = ''
        LCEXP.CUR   = '' ; LCEXP.CUR.NAME   = '' ; LCEXP.LIM.REFS= ''
        LCEXP.CAT   = '' ; LCEXP.CAT.NAME   = ''
        LCEXP.LIMIT = '' ; LCEXP.COLL       = ''
        LCEXP.LIST  = '' ; SELECTED.LCEXP   = '' ; ER.LCEXP      = ''

        LCCOL.COD   = '' ; LCCOL.ID         = ''
        LCCOL.BAL   = '' ; LCCOL.LIM.REF    = ''
        LCCOL.CUR   = '' ; LCCOL.CUR.NAME   = '' ; LCCOL.LIM.REFS= ''
        LCCOL.CAT   = '' ; LCCOL.CAT.NAME   = ''
        LCCOL.LIMIT = '' ; LCCOL.COLL       = ''
        LCCOL.LIST  = '' ; SELECTED.LCCOL   = '' ; ER.LCCOL      = ''

*******************************************************************
        FOR I = 1 TO SELECTED
            K++
            CALL F.READ(FN.POS,KEY.LIST<I>,R.POS,F.POS,E1)
            POS.ID = KEY.LIST<I>
            CUS.ID = FIELD (POS.ID,'-',1)
            CUS.ZZ = CUS.ID:".0000"
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LCL.REF)
            CUS.NAME = LCL.REF<1,CULR.ARABIC.NAME>
            OLD.CUST = LCL.REF<1,CULR.OLD.CUST.ID>

******ADDED IN 18/7/2010**********************
            CUS.CR.STAT   = LCL.REF<1,CULR.CREDIT.STAT>
            CUS.CR.CODE   = LCL.REF<1,CULR.CREDIT.CODE>
            IF CUS.CR.STAT EQ '' AND ( CUS.CR.CODE NE 110 AND  CUS.CR.CODE NE 120 ) THEN
            XNUMBER++
**********************************************
                XX<1,K>[1,10]   = CUS.ID
                XX<1,K>[12,35]  = CUS.NAME
                XX<1,K>[50,15]  = '(����� ������ ':OLD.CUST:')'
*            XX<1,K>[90,10]  = OLD.CUST
                PRINT STR("-",120)
                PRINT XX<1,K>
*            PRINT STR('-',60)
*******************************
                CUST.NO = CUS.ID[6]
                CUST.T  = CUST.NO[1,1]
                IF CUST.T EQ 1 OR CUST.T EQ 2 THEN
                    YYNUMBER++
                END
                IF CUST.T EQ 3 THEN
                    ZZNUMBER++
                END
*******************************
*---------------------------*AC*--------------------
                ACC.NO1 = ''
                AC.COD = R.POS<CUST.AC>
*Line [ 226 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DAC = DCOUNT(AC.COD,@VM)
                IF DAC GT 0 THEN
                    FOR ACC = 1 TO DAC
                        K1++
                        AC.ID  = R.POS<CUST.AC,ACC>
                        AC.SER = AC.ID[15,2]
                        AC.CAT = R.POS<CUST.AC.CATEG,ACC>
                        AC.CUR = R.POS<CUST.AC.CURR,ACC>
                        IF AC.CAT NE '1002' THEN
                            CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,ER.ACC)
                            AC.LIM.REF  = R.ACC<AC.LIMIT.REF>
                            AC.LIM.REFS = CUS.ZZ:AC.LIM.REF
                            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,AC.CUR,AC.CUR.NAME)
                            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,AC.CAT,AC.CAT.NAME)
                            AC.BAL += R.ACC<AC.OPEN.ACTUAL.BAL>
                            CALL DBR('LIMIT':@FM:LI.MAXIMUM.TOTAL,AC.LIM.REFS,AC.LIMIT)
                            AC.LOCK += R.ACC<AC.LOCKED.AMOUNT,1>
                            GOSUB PRINTAC
                        END
                    NEXT ACC
                END
*---------------------------*LD*------------------------
                LDC.NO1 = ''
                LD.COD = R.POS<CUST.LD>
*Line [ 251 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DLD = DCOUNT(LD.COD,@VM)
                IF DLD GT 0 THEN
                    FOR LDD = 1 TO DLD
                        K2++
                        LD.ID  = R.POS<CUST.LD,LDD>
                        CALL F.READ(FN.LDD,LD.ID,R.LDD,F.LDD,ER.LDD)
*-------------------
                        LDC.NO1 = R.LDD<LD.CATEGORY>:"/":R.LDD<LD.CURRENCY>
                        IF LDD = 1 THEN
                            LDC.NO = LDC.NO1
                        END
                        IF LDC.NO # LDC.NO1 THEN
                            GOSUB PRINTLD
                        END
*-------------------
                        LD.LIM.REF  = R.LDD<LD.LIMIT.REFERENCE>
                        LD.LIM.REFS = CUS.ZZ:LD.LIM.REF
                        LD.CUR = R.POS<CUST.LD.CURR,LDD>
                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LD.CUR,LD.CUR.NAME)
                        LD.CAT = R.POS<CUST.LD.CATEG,LDD>
                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,LD.CAT,LD.CAT.NAME)
                        LD.BAL += R.LDD<LD.AMOUNT>
                        CALL F.READ(FN.LIM,LD.LIM.REFS,R.LIM,F.LIM,ER)
                        LD.LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
                        LD.COL.ID = R.LDD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
                        CALL F.READ(FN.COL,LD.COL.ID,R.COL,F.COL,EER)
                        LD.COLL += R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
                        LDC.NO = LDC.NO1
                    NEXT LDD
                    IF LDD = DLD THEN K2++ ; GOSUB PRINTLD
                END
*---------------------------*LG*------------------------
                LGC.NO1 = ''
                LG.COD = R.POS<CUST.LG>
*Line [ 286 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DLG = DCOUNT(LG.COD,@VM)
                IF DLG GT 0 THEN
                    FOR LGG = 1 TO DLG
                        K3++
                        LG.ID  = R.POS<CUST.LG,LGG>
                        CALL F.READ(FN.LGG,LG.ID,R.LGG,F.LGG,ER.LGG)
*-------------------
                        LGC.NO1 = R.LGG<LD.CATEGORY>:"/":R.LGG<LD.CURRENCY>
                        IF LGG = 1 THEN
                            LGC.NO = LGC.NO1
                        END
                        IF LGC.NO # LGC.NO1 AND LG.ID NE '' THEN
                            GOSUB PRINTLG
                        END
*--------------------
                        LG.CUR = R.POS<CUST.LG.CURR,LGG>
                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LG.CUR,LG.CUR.NAME)
                        LG.CAT = R.POS<CUST.LG.CATEG,LGG>
                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,LG.CAT,LG.CAT.NAME)
                        LG.BAL += R.LGG<LD.AMOUNT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LG.LIM.REF  = R.LGG<LD.LIMIT.REFERENCE>
                        LG.LIM.REFS = CUS.ZZ:LG.LIM.REF
                        CALL F.READ(FN.LIM,LG.LIM.REFS,R.LIM,F.LIM,ER)
                        LG.LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
                        LG.COL.ID = R.LGG<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
                        CALL F.READ(FN.COL,LG.COL.ID,R.COL,F.COL,EER)
                        LG.COLL += R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LGC.NO = LGC.NO1
                    NEXT LGG
                    IF LGG = DLG THEN K3++ ; GOSUB PRINTLG
                END
*---------------------------*CD*------------------------
                CDC.NO1 = ''
                CD.COD = R.POS<CUST.CD>
*Line [ 323 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DCD = DCOUNT(CD.COD,@VM)
                IF DCD GT 0 THEN
                    FOR CDD = 1 TO DCD
                        K4++
                        CD.ID  = R.POS<CUST.CD,CDD>
                        CALL F.READ(FN.CDD,CD.ID,R.CDD,F.CDD,ER.CDD)
*-------------------
                        CDC.NO1 = R.CDD<LD.CATEGORY>:"/":R.CDD<LD.CURRENCY>
                        IF CDD = 1 THEN
                            CDC.NO = CDC.NO1
                        END
                        IF CDC.NO # CDC.NO1 AND CD.ID NE '' THEN
                            GOSUB PRINTCD
                        END
*--------------------
                        CD.CUR = R.POS<CUST.CD.CURR,CDD>
                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CD.CUR,CD.CUR.NAME)
                        CD.CAT = R.POS<CUST.CD.CATEG,CDD>
                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CD.CAT,CD.CAT.NAME)
                        CD.BAL += R.CDD<LD.AMOUNT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        CD.LIM.REF  = R.CDD<LD.LIMIT.REFERENCE>
                        CD.LIM.REFS = CUS.ZZ:CD.LIM.REF
                        CALL F.READ(FN.LIM,CD.LIM.REFS,R.LIM,F.LIM,ER)
                        CD.LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
                        CD.COL.ID = R.CDD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
                        CALL F.READ(FN.COL,CD.COL.ID,R.COL,F.COL,EER)
                        CD.COLL += R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        CDC.NO = CDC.NO1
                    NEXT CDD
                    IF CDD = DCD THEN K4++ ; GOSUB PRINTCD
                END
*---------------------------*LCIMP*------------------------
                LCIMPC.NO1 = ''
                LCIMP.COD = R.POS<CUST.LC.IMP>
*Line [ 360 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DLCIMP = DCOUNT(LCIMP.COD,@VM)
                IF DLCIMP GT 0 THEN
                    FOR LCI = 1 TO DLCIMP
                        K5++
                        LCIMP.ID  = R.POS<CUST.LC.IMP,LCI>
                        CALL F.READ(FN.LCIMP,LCIMP.ID,R.LCIMP,F.LCIMP,ER.LCIMP)
*-------------------
                        LCIMPC.NO1 = R.LCIMP<TF.LC.CATEGORY.CODE>:"/":R.LCIMP<TF.LC.LC.CURRENCY>
                        IF LCI = 1 THEN
                            LCIMPC.NO = LCIMPC.NO1
                        END
                        IF LCIMPC.NO # LCIMPC.NO1 AND LCIMP.ID NE '' THEN
                            GOSUB PRINTLCIMP
                        END
*--------------------
                        LCIMP.CUR = R.POS<CUST.LC.CURR.IMP,LCI>
                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LCIMP.CUR,LCIMP.CUR.NAME)
                        LCIMP.CAT = R.LCIMP<TF.LC.CATEGORY.CODE>
                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,LCIMP.CAT,LCIMP.CAT.NAME)
                        LCIMP.BAL += R.LCIMP<TF.LC.LIABILITY.AMT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LCIMP.LIM.REF  = R.LCIMP<TF.LC.LIMIT.REFERENCE>
                        LCIMP.LIM.REFS = CUS.ZZ:LCIMP.LIM.REF
                        CALL F.READ(FN.LIM,LCIMP.LIM.REFS,R.LIM,F.LIM,ER)
                        LCIMP.LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
                        LCIMP.COLL  = ''
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LCIMPC.NO = LCIMPC.NO1
                    NEXT LCI
                    IF LCI = DLCIMP THEN K5++ ; GOSUB PRINTLCIMP
                END
*---------------------------*LCEXP*------------------------
                LCEXPC.NO1 = ''
                LCEXP.COD = R.POS<CUST.LC.EXP>
*Line [ 395 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DLCEXP = DCOUNT(LCEXP.COD,@VM)
                IF DLCEXP GT 0 THEN
                    FOR LCE = 1 TO DLCEXP
                        K6++
                        LCEXP.ID  = R.POS<CUST.LC.EXP,LCE>
                        CALL F.READ(FN.LCEXP,LCEXP.ID,R.LCEXP,F.LCEXP,ER.LCEXP)
*-------------------
                        LCEXPC.NO1 = R.LCEXP<TF.LC.CATEGORY.CODE>:"/":R.LCEXP<TF.LC.LC.CURRENCY>
                        IF LCE = 1 THEN
                            LCEXPC.NO = LCEXPC.NO1
                        END
                        IF LCEXPC.NO # LCEXPC.NO1 AND LCEXP.ID NE '' THEN
                            GOSUB PRINTLCEXP
                        END
*--------------------
                        LCEXP.CUR = R.POS<CUST.LC.CURR.EXP,LCE>
                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LCEXP.CUR,LCEXP.CUR.NAME)
                        LCEXP.CAT = R.LCEXP<TF.LC.CATEGORY.CODE>
                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,LCEXP.CAT,LCEXP.CAT.NAME)
                        LCEXP.BAL += R.LCEXP<TF.LC.LIABILITY.AMT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LCEXP.LIM.REF  = R.LCEXP<TF.LC.LIMIT.REFERENCE>
                        LCEXP.LIM.REFS = CUS.ZZ:LCEXP.LIM.REF
                        CALL F.READ(FN.LIM,LCEXP.LIM.REFS,R.LIM,F.LIM,ER)
                        LCEXP.LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
                        LCEXP.COLL  = ''
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LCEXPC.NO = LCEXPC.NO1
                    NEXT LCE
                    IF LCE = DLCEXP THEN K6++ ; GOSUB PRINTLCEXP
                END
*---------------------------*LCCOL*------------------------
                LCCOLC.NO1 = ''
                LCCOL.COD = R.POS<CUST.LC.COLL>
*Line [ 430 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                DLCCOL = DCOUNT(LCCOL.COD,@VM)
                IF DLCCOL GT 0 THEN
                    FOR LCC = 1 TO DLCCOL
                        K7++
                        LCCOL.ID  = R.POS<CUST.LC.COLL,LCC>
                        CALL F.READ(FN.LCCOL,LCCOL.ID,R.LCCOL,F.LCCOL,ER.LCCOL)
*-------------------
                        LCCOLC.NO1 = R.LCCOL<TF.LC.CATEGORY.CODE>:"/":R.LCCOL<TF.LC.LC.CURRENCY>
                        IF LCC = 1 THEN
                            LCCOLC.NO = LCCOLC.NO1
                        END
                        IF LCCOLC.NO # LCCOLC.NO1 AND LCCOL.ID NE '' THEN
                            GOSUB PRINTLCCOL
                        END
*--------------------
                        LCCOL.CUR = R.POS<CUST.LC.CURR.COLL,LCC>
                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LCCOL.CUR,LCCOL.CUR.NAME)
                        LCCOL.CAT = R.LCCOL<TF.LC.CATEGORY.CODE>
                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,LCCOL.CAT,LCCOL.CAT.NAME)
                        LCCOL.BAL += R.LCCOL<TF.LC.LIABILITY.AMT>
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LCCOL.LIM.REF  = R.LCCOL<TF.LC.LIMIT.REFERENCE>
                        LCCOL.LIM.REFS = CUS.ZZ:LCCOL.LIM.REF
                        CALL F.READ(FN.LIM,LCCOL.LIM.REFS,R.LIM,F.LIM,ER)
                        LCCOL.LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
                        LCCOL.COLL  = ''
*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        LCCOLC.NO = LCCOLC.NO1
                    NEXT LCC
                    IF LCC = DLCCOL THEN K7++ ; GOSUB PRINTLCCOL
                END
*===============================================================
*            PRINT STR('-',120)
            END
        NEXT I
        CUSNUMBER = YYNUMBER + ZZNUMBER
        PRINT STR('=',120)
        PRINT '��� ������� : ':CUSNUMBER
        PRINT '����� : ':YYNUMBER
        PRINT '����� : ':ZZNUMBER
*===============================================================
    END ELSE
*        TEXT = "NO RECORD EXIST" ; CALL REM
        XNUMBER = 0
    END
    RETURN
*===============================================================
**************************
PRINTAC:
    XX1<1,K1>[5,5]    = AC.CAT
    XX1<1,K1>[12,35]  = AC.CAT.NAME
    XX1<1,K1>[45,2]   = AC.SER
    XX1<1,K1>[50,20]  = AC.CUR.NAME
    XX1<1,K1>[72,15]  = AC.BAL
    XX1<1,K1>[92,15]  = AC.LIMIT
    XX1<1,K1>[108,15] = AC.LOCK
    PRINT XX1<1,K1>
    AC.CAT      = ''
    AC.CAT.NAME = ''
    AC.CUR.NAME = ''
    AC.BAL   = ''
    AC.LIMIT = ''
    AC.LOCK  = ''
    RETURN
**************************
PRINTLD:
    XX2<1,K2>[5,5]    = LD.CAT
    XX2<1,K2>[12,35]   = LD.CAT.NAME
    XX2<1,K2>[50,20]  = LD.CUR.NAME
    XX2<1,K2>[72,15]  = LD.BAL
    XX2<1,K2>[92,15]  = LD.LIMIT
    XX2<1,K2>[108,15] = LD.COLL
    PRINT XX2<1,K2>
    LD.CAT      = ''
    LD.CAT.NAME = ''
    LD.CUR.NAME = ''
    LD.BAL   = ''
    LD.LIMIT = ''
    LD.COLL  = ''
    RETURN
***********************************
PRINTLG:
    XX3<1,K3>[5,5]    = LG.CAT
    XX3<1,K3>[12,35]   = LG.CAT.NAME
    XX3<1,K3>[50,20]  = LG.CUR.NAME
    XX3<1,K3>[72,15]  = LG.BAL
    XX3<1,K3>[92,15]  = LG.LIMIT
    XX3<1,K3>[108,15] = LG.COLL
    PRINT XX3<1,K3>
    LG.CAT      = ''
    LG.CAT.NAME = ''
    LG.CUR.NAME = ''
    LG.BAL   = ''
    LG.LIMIT = ''
    LG.COLL  = ''
    RETURN
**************************************
PRINTCD:
    XX4<1,K4>[5,5]    = CD.CAT
    XX4<1,K4>[12,35]   = CD.CAT.NAME
    XX4<1,K4>[50,20]  = CD.CUR.NAME
    XX4<1,K4>[72,15]  = CD.BAL
    XX4<1,K4>[92,15]  = CD.LIMIT
    XX4<1,K4>[108,15] = CD.COLL
    PRINT XX4<1,K4>
    CD.CAT      = ''
    CD.CAT.NAME = ''
    CD.CUR.NAME = ''
    CD.BAL   = ''
    CD.LIMIT = ''
    CD.COLL  = ''
    RETURN
**************************************
PRINTLCIMP:
    XX5<1,K5>[5,5]    = LCIMP.CAT
    XX5<1,K5>[12,35]   = LCIMP.CAT.NAME
    XX5<1,K5>[50,20]  = LCIMP.CUR.NAME
    XX5<1,K5>[72,15]  = LCIMP.BAL
    XX5<1,K5>[92,15]  = LCIMP.LIMIT
    XX5<1,K5>[108,15] = LCIMP.COLL
    PRINT XX5<1,K5>
    LCIM.CAT       = ''
    LCIMP.CAT.NAME = ''
    LCIMP.CUR.NAME = ''
    LCIMP.BAL   = ''
    LCIMP.LIMIT = ''
    LCIMP.COLL  = ''
    RETURN
**************************************
PRINTLCEXP:
    XX6<1,K6>[5,5]    = LCEXP.CAT
    XX6<1,K6>[12,35]   = LCEXP.CAT.NAME
    XX6<1,K6>[50,20]  = LCEXP.CUR.NAME
    XX6<1,K6>[72,15]  = LCEXP.BAL
    XX6<1,K6>[92,15]  = LCEXP.LIMIT
    XX6<1,K6>[108,15] = LCEXP.COLL
    PRINT XX6<1,K6>
    LCEXP.CAT      = ''
    LCEXP.CAT.NAME = ''
    LCEXP.CUR.NAME = ''
    LCEXP.BAL   = ''
    LCEXP.LIMIT = ''
    LCEXP.COLL  = ''
    RETURN
**************************************
PRINTLCCOL:
    XX7<1,K7>[5,5]    = LCCOL.CAT
    XX7<1,K7>[12,35]   = LCCOL.CAT.NAME
    XX7<1,K7>[50,20]  = LCCOL.CUR.NAME
    XX7<1,K7>[72,15]  = LCCOL.BAL
    XX7<1,K7>[92,15]  = LCCOL.LIMIT
    XX7<1,K7>[108,15] = LCCOL.COLL
    PRINT XX7<1,K7>
    LCCOL.CAT      = ''
    LCCOL.CAT.NAME = ''
    LCCOL.CUR.NAME = ''
    LCCOL.BAL      = ''
    LCCOL.LIMIT    = ''
    LCCOL.COLL     = ''
    RETURN
**************************************
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

    YYBRN   = BRANCH
    DATY    = TODAY
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    T.DAY1 = TDD[7,2]:'/':TDD[5,2]:"/":TDD[1,4]
*    PR.HD  ="'L'":" "
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(50):"����� ������� �������"
    PR.HD :="'L'":SPACE(50):"�� ����� :":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',35)
*    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(4):"�������" :SPACE(10):"��� �������" :SPACE(10):"�����":SPACE(5):"������" :SPACE(15):"������" :SPACE(15):"�� �����" :SPACE(10):"������"
    PR.HD :="'L'":STR('_',120)
    HEADING PR.HD
    PRINT
    RETURN
*==============================================================
END
