* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>127</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.FUND.DEP.001

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*-------------------------------------------------------------------------

    GOSUB INITIATE

***** CURRENCY SELECTION *************
    T.SEL2 = "SELECT FBNK.CURRENCY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
            CUR.ID = KEY.LIST2<NN>
            GOSUB PROCESS
        NEXT NN
    END
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "DEPOSIT.COMPOSITION.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DEPOSIT.COMPOSITION.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DEPOSIT.COMPOSITION.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DEPOSIT.COMPOSITION.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create DEPOSIT.COMPOSITION.CSV File IN &SAVEDLISTS&'
        END
    END

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    DAT = TODAY
    CALL CDT ('',DAT,'-1C')

    TD = TODAY
    CALL CDT ('',TD,'-1W')
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "DEPOSIT.COMPOSITION"
    HEAD.DESC  = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC := "SIGHT":","
    HEAD.DESC := "TERM < 3M":","
    HEAD.DESC := "TERM 3M < 6M":","
    HEAD.DESC := "TERM 6M < 12M":","
    HEAD.DESC := "TERM > 12M":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

*========================================================================
PROCESS:

    RETAIL.AMT = 0 ; CORP.AMT = 0
    RET.TERM3  = 0 ; RET.TERM6  = 0 ; RET.TERM12  = 0 ; RET.SIGHT  = 0 ; RET.TERM13  = 0
    CORP.TERM3 = 0 ; CORP.TERM6 = 0 ; CORP.TERM12 = 0 ; CORP.SIGHT = 0 ; CORP.TERM13 = 0

*-------------------------------------------
******* TERMS SELECTION *************

    T.SEL = "SELECT ":FN.LD:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21010) OR (CATEGORY GE 21019 AND CATEGORY LE 21029) OR (CATEGORY EQ 21032 OR CATEGORY EQ 21041 OR CATEGORY EQ 21042)) AND (( VALUE.DATE LE ":DAT:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":DAT:" AND AMOUNT EQ 0 )) AND CURRENCY EQ ":CUR.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            CATEG = R.LD<LD.CATEGORY>
            AMT = R.LD<LD.AMOUNT>
            IF AMT EQ 0 THEN
                AMT = R.LD<LD.REIMBURSE.AMOUNT>
            END
            CUS.ID = R.LD<LD.CUSTOMER.ID>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF CATEG GE 21001 AND CATEG LE 21004 THEN
                IF CUST.SECTOR EQ 4650 THEN
                    RET.TERM3  += AMT
                END ELSE
                    CORP.TERM3 += AMT
                END
            END

            IF CATEG EQ 21005 THEN
                IF CUST.SECTOR EQ 4650 THEN
                    RET.TERM6  += AMT
                END ELSE
                    CORP.TERM6 += AMT
                END
            END

            IF CATEG EQ 21006 THEN
                IF CUST.SECTOR EQ 4650 THEN
                    RET.TERM12  += AMT
                END ELSE
                    CORP.TERM12 += AMT
                END
            END

            IF (CATEG GE 21007 AND CATEG LE 21010) OR (CATEG GE 21019 AND CATEG LE 21029) OR (CATEG EQ 21032 OR CATEG EQ 21041 OR CATEG EQ 21042) THEN
                IF CUST.SECTOR EQ 4650 THEN
                    RET.TERM13  += AMT
                END ELSE
                    CORP.TERM13 += AMT
                END
            END
        NEXT I
    END
****************************************
******* SIGHT SELECTION *************

    T.SEL  = "SELECT ":FN.AC:" WITH (( CATEGORY IN (1481 1499 1408 1582 1483 1595 1493 1558)"
    T.SEL := " OR CATEGORY IN (1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 11242 1208 1211 1212 1216 1301 1302 1303 1377 1390)"
    T.SEL := " OR CATEGORY IN (1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513)"
    T.SEL := " OR CATEGORY IN (6501 6502 6503 6504 6511 6512 16151 16153 16188 16152 11380 16175 1012 1013 1014 1015 1016 1019 16170)"
    T.SEL := " OR CATEGORY IN (3011 3012 3013 3014 3017 3010 3005)"
    T.SEL := " OR CATEGORY IN (1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 11238 1407 1413 11240 11232 11234 11239 1480))"
    T.SEL := " AND CURRENCY EQ ":CUR.ID:" AND OPEN.ACTUAL.BAL GT 0)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CATEG = R.AC<AC.CATEGORY>
            AMT = R.AC<AC.OPEN.ACTUAL.BAL>
            CUS.ID = R.AC<AC.CUSTOMER>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF CUST.SECTOR EQ 4650 THEN
                RET.SIGHT  += AMT
            END ELSE
                CORP.SIGHT += AMT
            END
        NEXT I
    END

    BB.DATA  = 'RETAIL':","
    BB.DATA := RET.SIGHT:","
    BB.DATA := RET.TERM3:","
    BB.DATA := RET.TERM6:","
    BB.DATA := RET.TERM12:","
    BB.DATA := RET.TERM13:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = 'CORPORATE':","
    BB.DATA := CORP.SIGHT:","
    BB.DATA := CORP.TERM3:","
    BB.DATA := CORP.TERM6:","
    BB.DATA := CORP.TERM12:","
    BB.DATA := CORP.TERM13:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA = '******':","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*********************************
    RETURN
END
