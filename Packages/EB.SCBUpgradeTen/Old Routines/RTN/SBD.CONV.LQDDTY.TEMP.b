* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*--- CREATE BY KHALED -----------------------------------
*--- EDIT BY NESSMA -------------------------------------
    PROGRAM SBD.CONV.LQDDTY.TEMP

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*---------------------------------------------------------
    GOSUB INITIAL
    GOSUB PROCESS
    RETURN
*--------------------------------------------------------
INITIAL:
*-------
    COMP   = ID.COMPANY
    FN.LN  = 'F.RE.STAT.REP.LINE'      ; F.LN  = '' ; R.LN  = ''
    FN.BAL = 'F.RE.STAT.LINE.BAL'      ; F.BAL = '' ; R.BAL = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CCY = '' ; R.CCY = ''
    FN.LQD = 'F.SCB.LQDDTY.DAILY.FILE' ; F.LQD = ''

    CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.LQD,F.LQD)

    ROW.A     = 0
    ROW.B     = 0
    DIV.A.B   = 0
    TDD       = TODAY
    DAT.ID    = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,LWD)
    CALL CDT("",TDD,'-1W')
    CLOSE.BAL = 0
    NN        = 'LQDDTY-00'
    NNFCY     = 'LQDDTYFCY-00'

    FN.CO = "F.COMPANY" ; F.CO = ""
    CALL OPF(FN.CO, F.CO)
    RETURN
*==================================================================
PROCESS:
*-------
    N.SEL = "SELECT F.COMPANY WITH @ID UNLIKE ...88 BY @ID"
    CALL EB.READLIST(N.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    FOR I = 10 TO 90
        FOR XX = 1 TO SELECTED
            DESC   = NN:I
            BAL.ID = DESC:"-":"LOCAL":"-":TD1:"*":KEY.LIST<XX>
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E2)

            IF NOT(E2) THEN
                IF I GT 40 THEN
                    N.CODE = 'B'
                END ELSE
                    N.CODE = 'A'
                END

                CLOSE.BAL = R.BAL<RE.SLB.CLOSING.BAL.LCL>
                LQD.ID    = "LCY.":N.CODE:".":LWD:".":KEY.LIST<XX>
                CALL F.READ(FN.LQD,LQD.ID,R.LQD,F.LQD,E3)
                R.LQD<LQDF.CY.CODE>     = "LCY"
                R.LQD<LQDF.COLUMN.CODE> = N.CODE
                R.LQD<LQDF.LQDDTY.DATE> = LWD
                IF I EQ 10 THEN R.LQD<LQDF.COLUMN.01>   = CLOSE.BAL
                IF I EQ 20 THEN R.LQD<LQDF.COLUMN.03>   = CLOSE.BAL
                IF I EQ 25 THEN R.LQD<LQDF.COLUMN.04>   = CLOSE.BAL
                IF I EQ 28 THEN R.LQD<LQDF.COLUMN.06>   = CLOSE.BAL
                IF I EQ 30 THEN R.LQD<LQDF.COLUMN.08>   = CLOSE.BAL
                IF I EQ 40 THEN R.LQD<LQDF.COLUMN.09>   = CLOSE.BAL
                IF I EQ 50 THEN R.LQD<LQDF.COLUMN.11>   = CLOSE.BAL
                IF I EQ 60 THEN R.LQD<LQDF.COLUMN.12>   = CLOSE.BAL
                IF I EQ 70 THEN R.LQD<LQDF.COLUMN.13>   = CLOSE.BAL
                IF I EQ 80 THEN R.LQD<LQDF.COLUMN.14>   = CLOSE.BAL
                IF I EQ 90 THEN R.LQD<LQDF.COLUMN.14>  += CLOSE.BAL

                R.LQD<LQDF.TOTAL.ROW.A> =  R.LQD<LQDF.COLUMN.01> + R.LQD<LQDF.COLUMN.03> + R.LQD<LQDF.COLUMN.04> + R.LQD<LQDF.COLUMN.06> + R.LQD<LQDF.COLUMN.08> + R.LQD<LQDF.COLUMN.09>
                R.LQD<LQDF.TOTAL.ROW.B> =  R.LQD<LQDF.COLUMN.11> + R.LQD<LQDF.COLUMN.12> + R.LQD<LQDF.COLUMN.13> + R.LQD<LQDF.COLUMN.14>
                R.LQD<LQDF.LQDDTY.BR>   = KEY.LIST<XX>
                CALL F.WRITE(FN.LQD,LQD.ID,R.LQD)
                CALL  JOURNAL.UPDATE(LQD.ID)
            END

            DESC    = NNFCY:I
            BAL.ID1 = DESC:"-":"LOCAL":"-":TD1:"*":KEY.LIST<XX>
            CALL F.READ(FN.BAL,BAL.ID1,R.BAL,F.BAL,E4)

            IF NOT(E4) THEN
                IF I GT 48 THEN
                    N.CODE = 'B'
                END ELSE
                    N.CODE = 'A'
                END

                FCY.CLOSE.BAL = R.BAL<RE.SLB.CLOSING.BAL.LCL>
                LQD.ID1       = "FCY.":N.CODE:".":LWD:".":KEY.LIST<XX>
                CALL F.READ(FN.LQD,LQD.ID1,R.LQD,F.LQD,E6)
                R.LQD<LQDF.CY.CODE>     = "FCY"
                R.LQD<LQDF.COLUMN.CODE> = N.CODE
                R.LQD<LQDF.LQDDTY.DATE> = LWD
                IF I EQ 10 THEN R.LQD<LQDF.COLUMN.01>   += FCY.CLOSE.BAL
                IF I EQ 15 THEN R.LQD<LQDF.COLUMN.02>   += FCY.CLOSE.BAL
                IF I EQ 20 THEN R.LQD<LQDF.COLUMN.03>   += FCY.CLOSE.BAL
                IF I EQ 30 THEN R.LQD<LQDF.COLUMN.05>   += FCY.CLOSE.BAL
                IF I EQ 40 THEN R.LQD<LQDF.COLUMN.06>   += FCY.CLOSE.BAL
                IF I EQ 45 THEN R.LQD<LQDF.COLUMN.07>   += FCY.CLOSE.BAL
                IF I EQ 48 THEN R.LQD<LQDF.COLUMN.08>   += FCY.CLOSE.BAL
                IF I EQ 50 THEN R.LQD<LQDF.COLUMN.11>   += FCY.CLOSE.BAL
                IF I EQ 60 THEN R.LQD<LQDF.COLUMN.12>   += FCY.CLOSE.BAL
                IF I EQ 70 THEN R.LQD<LQDF.COLUMN.13>   += FCY.CLOSE.BAL
                IF I EQ 80 THEN R.LQD<LQDF.COLUMN.15>   += FCY.CLOSE.BAL
                IF I EQ 90 THEN R.LQD<LQDF.COLUMN.15>  += FCY.CLOSE.BAL
                R.LQD<LQDF.TOTAL.ROW.A> =  R.LQD<LQDF.COLUMN.01> + R.LQD<LQDF.COLUMN.02> + R.LQD<LQDF.COLUMN.03> + R.LQD<LQDF.COLUMN.05> + R.LQD<LQDF.COLUMN.06> + R.LQD<LQDF.COLUMN.07> + R.LQD<LQDF.COLUMN.08>
                R.LQD<LQDF.TOTAL.ROW.B> =  R.LQD<LQDF.COLUMN.11> + R.LQD<LQDF.COLUMN.12> + R.LQD<LQDF.COLUMN.13> + R.LQD<LQDF.COLUMN.15>
                R.LQD<LQDF.LQDDTY.BR>   = KEY.LIST<XX>

                CALL F.WRITE(FN.LQD,LQD.ID1,R.LQD)
                CALL  JOURNAL.UPDATE(LQD.ID1)
            END
        NEXT XX
    NEXT I
    RETURN
END
