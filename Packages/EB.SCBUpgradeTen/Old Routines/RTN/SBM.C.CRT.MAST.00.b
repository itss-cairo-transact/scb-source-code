* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.C.CRT.MAST.00
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD

*---------------------------------------------------
    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ""

    FN.AC = "FBNK.ACCOUNT"
    F.AC  = ""

    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

    FN.CBE = "F.CBE.MAST.AC.LD"
    F.CBE  = ""
*----------------------------------------------------
    WS.CUSTOMER = 0
    WS.CO.CODE =  0
    WS.BR =  0
    WS.CATEG = 0
    WS.AMT = 0
    WS.CY = ""
    WS.LCY = 0
    WS.FCY = 0
    WS.INDS = 0
    WS.OLD.SECTOR = 0
    WS.NEW.SECTOR = 0
    WS.NEW.INDST = ""
    WS.LEGAL.FORM = 0
    WS.RATE = 0
    WS.MLT.DIVD = 0
    WS.KEY = ""
    R.CBE = ""
    WS.AMT.LOCAL = ""
*-------------------------------------------------
    CALL OPF (FN.CUS,F.CUS)
    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.CBE,F.CBE)
*------------------------------------------------
    GOSUB A.100.CRT.AC
    RETURN
*------------------------------------------------
A.100.CRT.AC:
    SEL.CMD = "SELECT ":FN.AC
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.AC.ID FROM SEL.LIST SETTING POS
    WHILE WS.AC.ID:POS
        CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,MSG.AC)
        IF WS.AC.ID EQ "" THEN
            GOTO A.100.A
        END
        IF R.AC<AC.CUSTOMER> EQ "" THEN
            GOTO A.100.A
        END
        WS.CUSTOMER = R.AC<AC.CUSTOMER>

        WS.CO.CODE =  R.AC<AC.CO.CODE>
        WS.BR =  WS.CO.CODE[2]
        WS.CATEG = R.AC<AC.CATEGORY>

        WS.AMT = R.AC<AC.OPEN.ACTUAL.BAL>
        WS.CY = R.AC<AC.CURRENCY>
        WS.KEY = WS.AC.ID
        GOSUB A.300.GET.CUS

        IF WS.CY EQ "EGP" THEN
            WS.LCY = WS.AMT
            WS.FCY = WS.AMT
        END

        IF WS.CY NE "EGP" THEN
            GOSUB A.400.GET.CY
            WS.LCY = WS.AMT.LOCAL
            WS.FCY = WS.AMT

        END
        GOSUB  A.500.CRT.REC
A.100.A:
    REPEAT
    RETURN
*------------------------------------------------
A.300.GET.CUS:
    CALL F.READ(FN.CUS,WS.CUSTOMER,R.CUS,F.CUS,MSG.CUS)
    IF MSG.CUS EQ "" THEN
        WS.INDS = R.CUS<EB.CUS.INDUSTRY>
        WS.OLD.SECTOR = R.CUS<EB.CUS.SECTOR>
        WS.NEW.SECTOR = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
        WS.NEW.INDST  = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.INDUSTRY>
        WS.LEGAL.FORM = R.CUS<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
        RETURN
    END
    WS.INDS = 0
    WS.OLD.SECTOR = 0
    WS.NEW.SECTOR = 0
    WS.NEW.INDST  = ""
    WS.LEGAL.FORM = 0
    RETURN

*------------------------------------------------
A.400.GET.CY:
*    CALL F.READ(FN.CY,WS.CY,R.CY,F.CY,MSG.CY)
*    IF MSG.CY EQ "" THEN
*        WS.RATE = R.CY<EB.CUR.MID.REVAL.RATE,1>
*        WS.MLT.DIVD = R.CY<EB.CUR.QUOTATION.CODE>
*    END
*    IF WS.MLT.DIVD EQ "0" THEN
*        WS.AMT.LOCAL = WS.AMT * WS.RATE
*    END
*    IF WS.MLT.DIVD EQ "2" THEN
*        WS.AMT.LOCAL = WS.AMT / WS.RATE
*    END
    WS.KEYCY = "NZD"
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    IF MSG.CY EQ "" THEN
        GOSUB A.400.CALC
    END

    RETURN

A.400.CALC:
    A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 155 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
    WS.RATE.ARY = R.CCY<RE.BCP.RATE>
    WS.RATE = WS.RATE.ARY<1,CYPOS>
    WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
    WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>
    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT * WS.RATE
    END
    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT / WS.RATE
    END
    RETURN
*--------------------------------------------------------

A.500.CRT.REC:
*============================================= FOR INTERNAL =============
    IF WS.CATEG EQ 16170 THEN
        R.CBE<C.CBEM.CUSTOMER.CODE> = WS.CUSTOMER
        R.CBE<C.CBEM.BR> = WS.BR
        R.CBE<C.CBEM.NEW.SECTOR> = 4650
        R.CBE<C.CBEM.NEW.INDUSTRY> = WS.NEW.INDST
        R.CBE<C.CBEM.INDUSTRY> =  1100
        R.CBE<C.CBEM.OLD.SECTOR> = 2000
        R.CBE<C.CBEM.LEGAL.FORM> =  ''
    END
*========================================================================
    R.CBE<C.CBEM.CUSTOMER.CODE> = WS.CUSTOMER
    R.CBE<C.CBEM.BR> = WS.BR
    R.CBE<C.CBEM.NEW.SECTOR> = WS.NEW.SECTOR
    R.CBE<C.CBEM.NEW.INDUSTRY> = WS.NEW.INDST
    R.CBE<C.CBEM.INDUSTRY> =  WS.INDS
    R.CBE<C.CBEM.OLD.SECTOR> = WS.OLD.SECTOR
    R.CBE<C.CBEM.LEGAL.FORM> =  WS.LEGAL.FORM
    R.CBE<C.CBEM.CATEG> =  WS.CATEG
    R.CBE<C.CBEM.CY> =  WS.CY
    R.CBE<C.CBEM.IN.LCY> =  WS.LCY
    R.CBE<C.CBEM.IN.FCY> =  WS.FCY
    R.CBE<C.CBEM.MRGN.LCY> = 0
    R.CBE<C.CBEM.MRGN.FCY> = 0
    R.CBE<C.CBEM.MRGN.PRCNT> = 0
    R.CBE<C.CBEM.PRODCT.TYPE> = ""
    R.CBE<C.CBEM.BENF.STAT>   = ""

    CALL F.WRITE(FN.CBE,WS.KEY,R.CBE)
    CALL  JOURNAL.UPDATE(WS.KEY)
    RETURN
END
