* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>306</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.MLAD.PROG.300
***    PROGRAM SBD.MLAD.PROG.300
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.HIST
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    XCURR = 'EGP'
    FR.DATE = TODAY

    GOSUB GET.LAST.REC.MLADHIST

    CAL.DAYS = 0
    CALL CDD('C',FR.DATE,TO.DATE,CAL.DAYS)

*  ---------
    IF CAL.DAYS GT 0  THEN
        GOSUB PROCESS
    END
*  ---------


*-------------------------------------
    RETURN
*==============================================================
INITIATE:
    COMP = ID.COMPANY
    PROGRAM.NAME = 'SBD.MLAD.PROG.300'

    T.AGL.BAL = 0
    T.CER.BAL = 0
    T.SAV.BAL = 0
    T.CUR.BAL = 0
    T.OTR.BAL = 0


    T.AGL.BALX = 0
    T.CER.BALX = 0
    T.SAV.BALX = 0
    T.CUR.BALX = 0
    T.OTR.BALX = 0


    T.AGL.BALY = 0
    T.CER.BALY = 0
    T.SAV.BALY = 0
    T.CUR.BALY = 0
    T.OTR.BALY = 0

*----------------------------------
    FN.COMP = "FBNK.COMPANY"
    F.COMP  = ""
    R.COMP = ""
    Y.COMP.ID = ""

    FN.MLADHIS = "F.SCB.MLAD.HIST"
    F.MLADHIS  = ""
    R.MLADHIS  = ""
    Y.MLADHIS.ID  = ""
    KEY.LISTHIST  = ""

    CALL OPF(FN.MLADHIS,F.MLADHIS)
*----------------------------------

    FN.LINE = 'F.RE.STAT.LINE.BAL'
    F.LINE = ''

    CALL OPF(FN.LINE,F.LINE)

*----------------------------------
    DIM AR.CURR(50)
    DIM AR.RATE(50)
    DIM AR.TYPE(50)

    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)


    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)

    CUR.COD = R.CUR<RE.BCP.ORIGINAL.CCY>
*Line [ 114 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.CURR = DCOUNT(CUR.COD,@VM)

    FOR POS = 1 TO NO.CURR

        AR.CURR(POS) = R.CUR<RE.BCP.ORIGINAL.CCY,POS>
        AR.RATE(POS) = R.CUR<RE.BCP.RATE,POS>
        AR.TYPE(POS) = R.CUR<RE.BCP.RATE.TYPE,POS>

    NEXT POS

*----------------------------------
    FN.DATE = "F.DATES"
    F.DATE  = ""
    R.DATE = ""
    Y.DATE.ID = ""

    CALL OPF (FN.DATE,F.DATE)
*----------------------------------

    Y.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,Y.DATE.ID,R.DATE,F.DATE,ERR.DATE)
    TO.DATE = R.DATE<EB.DAT.LAST.PERIOD.END>




    RETURN
*========================================================================
PROCESS:
*-----------------
    FOR X = 1 TO NO.CURR

*  ---------
        IF AR.CURR(X) EQ "INR" THEN
            GO TO NEXT.RECORD
        END
*  ---------
        XCURR = AR.CURR(X)
        XRATE = AR.RATE(X)
        XTYPE = AR.TYPE(X)


        WRK.DATE = FR.DATE

        FOR J = 1 TO CAL.DAYS


            CALL CDT('',WRK.DATE,'+1C')
*  ---------
            IF WRK.DATE GT TO.DATE  THEN
                GO TO NEXT.RECORD
            END
*  ---------
            IDD1 = 'GENLED-0510':'-':'EGP':'-':WRK.DATE:'*':'EG0010001'
            CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
            IF E1    THEN
                GO TO END.LOOP.J
            END
*  ----------------------------------------------------------------------



            DAT = WRK.DATE


            WS.CUR.BAL     = 0
            WS.EQV.CUR.BAL = 0
            WS.SAV.BAL     = 0
            WS.EQV.SAV.BAL = 0


            T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010077' AND @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
            CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SEL.COMP,ER.MSG3)


            FOR I = 1 TO SEL.COMP


                COMP = KEY.LIST3<I>

*********************************************************************************************************        ������ �����
*                                                                                                    ����
                IDD1 = 'GENLED-0510':'-':XCURR:'-':DAT:'*':COMP
                CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)


                WS.CUR.BAL      += R.LINE<RE.SLB.CLOSING.BAL>
                WS.EQV.CUR.BAL  += R.LINE<RE.SLB.CLOSING.BAL.LCL>



*                                                                                                   �����
                IDD1 = 'GENLED-0710':'-':XCURR:'-':DAT:'*':COMP
                CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
                WS.CUR.BAL      += R.LINE<RE.SLB.CLOSING.BAL>
                WS.EQV.CUR.BAL  += R.LINE<RE.SLB.CLOSING.BAL.LCL>

*********************************************************************************************************       ������ �������
*--------------------------------------------------------------------------
**                                                                                 ��� ����� ���� �� �����
                IF COMP EQ 'EG0010011' THEN

                    IDD2 = 'GENLED-0525':'-':XCURR:'-':DAT:'*':COMP
                    GOSUB SEARCH.SAVE.LINE

                    IDD2 = 'GENLED-0527':'-':XCURR:'-':DAT:'*':COMP
                    GOSUB SEARCH.SAVE.LINE

                    IDD2 = 'GENLED-0725':'-':XCURR:'-':DAT:'*':COMP
                    GOSUB SEARCH.SAVE.LINE

                    IDD2 = 'GENLED-0727':'-':XCURR:'-':DAT:'*':COMP
                    GOSUB SEARCH.SAVE.LINE
                END

*--------------------------------------------------------------------------
*                                                                                 ������ ������ ���� �� �����
                IF COMP NE 'EG0010011' THEN
                    IDD2 = 'GENLED-0520':'-':XCURR:'-':DAT:'*':COMP
                    GOSUB SEARCH.SAVE.LINE

                    IDD2 = 'GENLED-0720':'-':XCURR:'-':DAT:'*':COMP
                    GOSUB SEARCH.SAVE.LINE
                END


END.LOOP.I:
*---------------

            NEXT I

*********************************************************************************************************


            Y.MLADH.ID = XCURR:"-":DAT

            WRK.YY   = DAT[1,4]
            WRK.MM   = DAT[5,2]
            WRK.DD   = DAT[7,2]

            WS.CY   = XCURR

            WS.YEAR   = FMT(WRK.YY,"R%4")
            WS.MONTH  = FMT(WRK.MM,"R%2")
            WS.DAY    = FMT(WRK.DD,"R%2")

            WS.TOT.CCY.BAL = WS.CUR.BAL + WS.SAV.BAL


            IF    WS.TOT.CCY.BAL GT 0 THEN
                GOSUB WRITE.MLAD.HIS.FILE
            END


END.LOOP.J:
*---------------
        NEXT J


NEXT.RECORD:

    NEXT X
*********************************************************************************************************
*-------------------------------------------------
    RETURN
*********************************************************************************************************
SEARCH.SAVE.LINE:

    CALL F.READ(FN.LINE,IDD2,R.LINE,F.LINE,E)
    WS.SAV.BAL       += R.LINE<RE.SLB.CLOSING.BAL>
    WS.EQV.SAV.BAL   += R.LINE<RE.SLB.CLOSING.BAL.LCL>

    RETURN


*--------------------------------------------------------------------------
WRITE.MLAD.HIS.FILE:
*-------------------
    R.MLADHIS = ''
    R.MLADH = ''

*   -------------------

    R.MLADH<MLAD.HIS.CURRENT.AMT>     = WS.CUR.BAL
    R.MLADH<MLAD.HIS.EQV.CURR.AMT>    = WS.EQV.CUR.BAL
    R.MLADH<MLAD.HIS.SAVING.AMT>      = WS.SAV.BAL
    R.MLADH<MLAD.HIS.EQV.SAVING.AMT>  = WS.EQV.SAV.BAL
    R.MLADH<MLAD.HIS.TOT.CCY.BALANCE> = WS.TOT.CCY.BAL
*   -------------------

    R.MLADH<MLAD.HIS.CURRENCY>        = WS.CY
    R.MLADH<MLAD.HIS.ID.YEAR>         = WS.YEAR
    R.MLADH<MLAD.HIS.ID.MONTH>        = WS.MONTH
    R.MLADH<MLAD.HIS.ID.DAY>          = WS.DAY

*------------------------
    CALL F.WRITE(FN.MLADHIS,Y.MLADH.ID,R.MLADH)
    CALL JOURNAL.UPDATE(Y.MLADH.ID)

    RETURN

*===============================================================
GET.FIRST.DAY:


GET.FIRST.DAY.LP:


    Y.MLADH.ID = XCURR:"-":FR.DATE


    ERR.MLADHIS = ''

    CALL F.READ(FN.MLADHIS,Y.MLADH.ID,R.MLADH,F.MLADHIS,ERR.MLADHIS)


    IF ERR.MLADHIS    THEN
        CALL CDT('',FR.DATE,'-1C')
        GO TO GET.FIRST.DAY.LP
    END



    RETURN
*===============================================================
GET.LAST.REC.MLADHIST:

    ERR.MLADHIS = 'RECORD NOT FOUND'

    LOOP
    WHILE (ERR.MLADHIS NE '')
    WHILE (FR.DATE GT 20100401)

        Y.MLADH.ID = XCURR:"-":FR.DATE

        CALL F.READ(FN.MLADHIS,Y.MLADH.ID,R.MLADH,F.MLADHIS,ERR.MLADHIS)

        CALL CDT('',FR.DATE,'-1C')

    REPEAT


    RETURN
*===============================================================
END
