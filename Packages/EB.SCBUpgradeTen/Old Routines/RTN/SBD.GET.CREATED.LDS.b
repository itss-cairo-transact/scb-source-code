* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*** CREATED BY MOHAMMED SABRY 2011/01/02
    SUBROUTINE SBD.GET.CREATED.LDS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.DATE.LPE)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,WS.DATE.LWD)
    WS.1ST.DAY  =   WS.DATE.LPE[1,6]:'01'

    F.SLD = '' ; FN.SLD = 'F.SCB.LD'; R.SLD = ''
    CALL OPF(FN.SLD,F.SLD)
    KEY.LIST1=""
    SELECTED1=""
    ER.MSG1=""
**********************************
*-------------------------------------------------- LD.LOANS.AND.DEPOSITS ------------------------------------------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''

    CALL OPF( FN.LD,F.LD)
**   T.SEL1 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH  STATUS NE 'FWD' AND (( VALUE.DATE LE ":WS.DATE.LPE:" AND FIN.MAT.DATE GT ":WS.DATE.LWD:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":WS.DATE.LPE:" AND AMOUNT EQ 0 )) AND CATEGORY NE '' "
    T.SEL1 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH  STATUS NE 'FWD' AND ( CATEGORY GE 21001 AND CATEGORY LE 21010 ) AND AMOUNT NE 0 AND ( VALUE.DATE GE '":WS.1ST.DAY :"' AND VALUE.DATE LE '":WS.DATE.LPE:"' ) AND CATEGORY NE '' "
**********************************
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF KEY.LIST1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.LD,KEY.LIST1<I>, R.LD,F.LD, ETEXT1)
            CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,R.LD<LD.CUSTOMER.ID>,WS.CUS.BR)

            WS.SLD.ID                    =  KEY.LIST1<I>
            R.SLD<SLD.CUSTOMER.ID>       =  R.LD<LD.CUSTOMER.ID>
            R.SLD<SLD.AMOUNT>            =  R.LD<LD.AMOUNT>
            R.SLD<SLD.CURRENCY>          =  R.LD<LD.CURRENCY>
            R.SLD<SLD.VALUE.DATE>        =  R.LD<LD.VALUE.DATE>
            R.SLD<SLD.FIN.MAT.DATE>      =  R.LD<LD.FIN.MAT.DATE>
            R.SLD<SLD.CATEGORY>          =  R.LD<LD.CATEGORY>
            R.SLD<SLD.INTEREST.RATE>     =  R.LD<LD.INTEREST.RATE>
            R.SLD<SLD.INTEREST.SPREAD>   =  R.LD<LD.INTEREST.SPREAD>
            R.SLD<SLD.TOT.INTEREST.AMT>  =  R.LD<LD.TOT.INTEREST.AMT>
            R.SLD<SLD.REIMBURSE.AMOUNT>  =  R.LD<LD.REIMBURSE.AMOUNT>
            R.SLD<SLD.TOT.INT>           =  R.LD<LD.INTEREST.RATE> + R.LD<LD.INTEREST.SPREAD>
            R.SLD<SLD.SYS.DATE>          =  TODAY
            R.SLD<SLD.CUS.BR>            =  WS.CUS.BR
            R.SLD<SLD.CO.CODE>           = R.LD<LD.CO.CODE>
            CALL F.WRITE(FN.SLD,WS.SLD.ID,R.SLD)
            CALL JOURNAL.UPDATE(WS.SLD.ID)

            WS.SLD.ID = ''
        NEXT I
    END

*************************************************************
    RETURN
END
