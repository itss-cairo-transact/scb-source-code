* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*** ���� ����� ��� ��� �������� ������ ***
*** CREATED BY KHALED ***
***======================================

    SUBROUTINE SBD.EXCESS.LIMIT.DET1
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EXCEPTION.LOG.FILE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 53 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.EXCESS.LIMIT.DET1'
    CALL PRINTER.ON(REPORT.ID,'')

    LD.AC.NO = 0       ; FT.AC.NO = 0
    LD.CUS.NAME = ''   ; FT.CUS.NAME = ''
    LD.AMT  = 0        ; FT.AMT  = 0
    LD.TIME = ''       ; FT.TIME = ''
    LD.DATE = ''       ; FT.DATE = '' ; FT.OVER2 = ''
    LD.AUTH = ''       ; FT.AUTH = '' ; LD.OVER2 = ''

    TT.AC.NO = 0       ; LC.AC.NO = 0
    TT.CUS.NAME = ''   ; LC.CUS.NAME = ''
    TT.AMT  = 0        ; LC.AMT  = 0
    TT.TIME = ''       ; LC.TIME = ''
    TT.DATE = ''       ; LC.DATE = '' ; TT.OVER2 = ''
    TT.AUTH = ''       ; LC.AUTH = '' ; LC.OVER2 = ''
    COMP = ID.COMPANY
    RETURN
*========================================================================
CALLDB:
    FN.EC = 'F.EXCEPTION.LOG.FILE'       ; F.EC = ''
    FN.FT = 'FBNK.FUNDS.TRANSFER'        ; F.FT = ''
    FN.TT = 'FBNK.TELLER'                ; F.TT = ''
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LC = 'FBNK.LETTER.OF.CREDIT'      ; F.LC = ''

    CALL OPF(FN.EC,F.EC)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""
    KEY.LIST4="" ; SELECTED4="" ;  ER.MSG4=""
*************************************************************************
    T.SEL = "SELECT ":FN.EC:" WITH COMP.CODE EQ ":COMP:" BY REC.KEY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.EC,KEY.LIST<I>,R.EC,F.EC,E1)
            EXC.KEY = R.EC<EB.EXC.REC.KEY>
            EXC.KEY.ID = EXC.KEY[1,2]
            IF EXC.KEY.ID EQ 'LD' THEN
*************************************************************************
                T.SEL1 = "SELECT ":FN.LD: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...OVER... OR OVERRIDE LIKE ...EXCESS...)"
                CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
                IF SELECTED1 THEN
                    FOR J = 1 TO SELECTED1
                        CALL F.READ(FN.LD,KEY.LIST1<J>,R.LD,F.LD,E2)
                        LD.AMT = R.LD<LD.AMOUNT>
                        LD.AC.NO = R.LD<LD.DRAWDOWN.ACCOUNT>
                        LD.OVER = R.LD<LD.OVERRIDE>
                        FINDSTR 'OVER' IN LD.OVER SETTING FMS,VMS THEN
                            LD.OVER2 = 'OVERDRAFT'
                            FINDSTR 'ON AVAILABLE' IN LD.OVER SETTING FMS,VMS THEN
                                LD.OVER2 = 'OVERDRAFT-AVAL'
                            END
                        END
                        FINDSTR 'Limit' IN LD.OVER SETTING FMS,VMS THEN
                            LD.OVER2 = 'EXCEED LIMIT'
                            FINDSTR 'ON AVAILABLE' IN LD.OVER SETTING FMS,VMS THEN
                                LD.OVER2 = 'EXCEED-LIMIT-AVAL'
                            END
                        END
*===============================================================
                        IF LD.AC.NO EQ '' THEN
                            LD.LOCAL = R.LD<LD.LOCAL.REF>
                            LD.AC.NO = LD.LOCAL<1,LDLR.DEBIT.ACCT>
                        END
*===============================================================
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,LD.AC.NO,LOCAL.REF)
                        LD.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,LD.AC.NO,OLD.REF)
                        LD.OLD.NO = OLD.REF
                        LD.AUTH1 = R.LD<LD.OVERRIDE>
                        LD.AUTH2 = FIELD(LD.AUTH1,'SCB.',2)
                        IF LD.AUTH2 NE '' THEN
                            LD.AUTH3 = 'SCB.':LD.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,LD.AUTH3,AUTH.NAME)
                            LD.AUTH = AUTH.NAME
                        END ELSE
                            LD.AUTH1 = R.LD<LD.AUTHORISER>
                            LD.AUTH3 = FIELD(LD.AUTH1,"_",2)
                            LD.AUTH4 = LD.AUTH3[1,3]
                            IF LD.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,LD.AUTH3,AUTH.NAME)
                                LD.AUTH = AUTH.NAME
                            END ELSE
                                LD.AUTH = LD.AUTH1
                            END
                        END
                        LD.DD.TT = R.LD<LD.DATE.TIME>
*------------------------------------------------------------------------
                        LD.DATE1 = LD.DD.TT[1,2]
                        LD.DATE2 = LD.DD.TT[3,2]
                        LD.DATE3 = LD.DD.TT[5,2]
                        LD.DATE = LD.DATE3:"/":LD.DATE2:"/":LD.DATE1
*------------------------------------------------------------------------
                        LD.TIME1 = LD.DD.TT[7,2]
                        LD.TIME2 = LD.DD.TT[9,2]
                        LD.TIME = LD.TIME2:":":LD.TIME1
*------------------------------------------------------------------------
                        XX = SPACE(132)
                        XX4 = SPACE(132)
                        XX8 = SPACE(132)
                        XX<1,1>[1,20]    = LD.AC.NO
                        XX4<1,1>[1,20]   = LD.OLD.NO
                        XX<1,1>[20,35]   = LD.CUS.NAME
                        XX4<1,1>[22,35]  = EXC.KEY
                        XX<1,1>[57,10]   = LD.AMT
                        XX<1,1>[72,10]   = LD.TIME
                        XX<1,1>[87,10]   = LD.DATE
                        XX<1,1>[103,35]  = LD.AUTH
                        XX4<1,1>[103,35] = LD.OVER2

*  FINDSTR 'ON AVAILABLE' IN LD.OVER SETTING FMS,VMS THEN
*  END ELSE
                        PRINT XX<1,1>
                        PRINT XX4<1,1>
                        PRINT XX8<1,1>
                        PRINT STR('-',130)
*  END

                    NEXT J
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'FT' THEN
                T.SEL2 = "SELECT ":FN.FT: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...OVER... OR OVERRIDE LIKE ...EXCESS...)"
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
                IF SELECTED2 THEN
                    FOR K = 1 TO SELECTED2
                        CALL F.READ(FN.FT,KEY.LIST2<K>,R.FT,F.FT,E3)
                        FT.AMT = R.FT<FT.AMOUNT.DEBITED>[4,20]
                        FT.AC.NO = R.FT<FT.DEBIT.ACCT.NO>
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,FT.AC.NO,LOCAL.REF)
                        FT.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,FT.AC.NO,OLD.REF)
                        FT.OLD.NO = OLD.REF
                        FT.AUTH1 = R.FT<FT.OVERRIDE>
                        FT.OVER = R.FT<FT.OVERRIDE>
                        FINDSTR 'OVER' IN FT.OVER SETTING FMS,VMS THEN
                            FT.OVER2 = 'OVERDRAFT'
                            FINDSTR 'ON AVAILABLE' IN FT.OVER SETTING FMS,VMS THEN
                                FT.OVER2 = 'OVERDRAFT-AVAL'
                            END
                        END

                        FINDSTR 'Limit' IN FT.OVER SETTING FMS,VMS THEN
                            FT.OVER2 = 'EXCEED LIMIT'
                            FINDSTR 'ON AVAILABLE' IN FT.OVER SETTING FMS,VMS THEN
                                FT.OVER2 = 'EXCEED-LIMIT-AVAL'
                            END
                        END

                        FT.AUTH2 = FIELD(FT.AUTH1,'SCB.',2)
                        IF FT.AUTH2 NE '' THEN
                            FT.AUTH3 = "SCB.":FT.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,FT.AUTH3,AUTH.NAME)
                            FT.AUTH = AUTH.NAME
                        END ELSE
                            FT.AUTH1 = R.FT<FT.AUTHORISER>
                            FT.AUTH3 = FIELD(FT.AUTH1,"_",2)
                            FT.AUTH4 = FT.AUTH3[1,3]
                            IF FT.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,FT.AUTH3,AUTH.NAME)
                                FT.AUTH = AUTH.NAME
                            END ELSE
                                FT.AUTH = FT.AUTH1
                            END
                        END
                        FT.DD.TT = R.FT<FT.DATE.TIME>
*------------------------------------------------------------------------
                        FT.DATE1 = FT.DD.TT[1,2]
                        FT.DATE2 = FT.DD.TT[3,2]
                        FT.DATE3 = FT.DD.TT[5,2]
                        FT.DATE = FT.DATE3:"/":FT.DATE2:"/":FT.DATE1
*------------------------------------------------------------------------
                        FT.TIME1 = FT.DD.TT[7,2]
                        FT.TIME2 = FT.DD.TT[9,2]
                        FT.TIME = FT.TIME2:":":FT.TIME1
*------------------------------------------------------------------------
                        XX1 = SPACE(132)
                        XX5 = SPACE(132)
                        XX9 = SPACE(132)
                        XX1<1,1>[1,20]    = FT.AC.NO
                        XX5<1,1>[1,20]    = FT.OLD.NO
                        XX1<1,1>[20,35]   = FT.CUS.NAME
                        XX5<1,1>[22,35]  = EXC.KEY
                        XX1<1,1>[57,10]   = FT.AMT
                        XX1<1,1>[72,10]   = FT.TIME
                        XX1<1,1>[87,10]   = FT.DATE
                        XX1<1,1>[103,35]  = FT.AUTH
                        XX5<1,1>[103,35] = FT.OVER2
* FINDSTR 'ON AVAILABLE' IN FT.OVER SETTING FMS,VMS THEN
* END ELSE

                        PRINT XX1<1,1>
                        PRINT XX5<1,1>
                        PRINT XX9<1,1>
                        PRINT STR('-',130)
* END
                    NEXT K
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'TT' THEN
                T.SEL3 = "SELECT ":FN.TT: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...OVER... OR OVERRIDE LIKE ...EXCESS...)"
                CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
                IF SELECTED3 THEN
                    FOR L = 1 TO SELECTED3
                        CALL F.READ(FN.TT,KEY.LIST3<L>,R.TT,F.TT,E4)
                        TT.AMT = R.TT<TT.TE.AMOUNT.LOCAL.1>
                        TT.AC.NO = R.TT<TT.TE.ACCOUNT.1>
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,TT.AC.NO,LOCAL.REF)
                        TT.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,TT.AC.NO,OLD.REF)
                        TT.OLD.NO = OLD.REF
                        TT.AUTH1 = R.TT<TT.TE.OVERRIDE>
                        TT.OVER = R.TT<TT.TE.OVERRIDE>
                        FINDSTR 'OVER' IN TT.OVER SETTING FMS,VMS THEN
                            TT.OVER2 = 'OVERDRAFT'

                            FINDSTR 'ON AVAILABLE' IN TT.OVER SETTING FMS,VMS THEN
                                TT.OVER2 = 'OVERDRAFT-AVAL'
                            END
                        END

                        FINDSTR 'Limit' IN TT.OVER SETTING FMS,VMS THEN
                            TT.OVER2 = 'EXCEED LIMIT'

                            FINDSTR 'ON AVAILABLE' IN TT.OVER SETTING FMS,VMS THEN
                                TT.OVER2 = 'EXCEED-LIMIT-AVAL'
                            END
                        END

                        TT.AUTH2 = FIELD(TT.AUTH1,'SCB.',2)
                        IF TT.AUTH2 NE '' THEN
                            TT.AUTH3 = 'SCB.':TT.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,TT.AUTH3,AUTH.NAME)
                            TT.AUTH = AUTH.NAME
                        END ELSE
                            TT.AUTH1 = R.TT<TT.TE.AUTHORISER>
                            TT.AUTH3 = FIELD(TT.AUTH1,"_",2)
                            TT.AUTH4 = TT.AUTH3[1,3]
                            IF TT.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,TT.AUTH3,AUTH.NAME)
                                TT.AUTH = AUTH.NAME
                            END ELSE
                                TT.AUTH = TT.AUTH1
                            END
                        END
                        TT.DD.TT = R.TT<TT.TE.DATE.TIME>
*------------------------------------------------------------------------
                        TT.DATE1 = TT.DD.TT[1,2]
                        TT.DATE2 = TT.DD.TT[3,2]
                        TT.DATE3 = TT.DD.TT[5,2]
                        TT.DATE = TT.DATE3:"/":TT.DATE2:"/":TT.DATE1
*------------------------------------------------------------------------
                        TT.TIME1 = TT.DD.TT[7,2]
                        TT.TIME2 = TT.DD.TT[9,2]
                        TT.TIME = TT.TIME2:":":TT.TIME1
*------------------------------------------------------------------------
                        XX2  = SPACE(132)
                        XX6  = SPACE(132)
                        XX10 = SPACE(132)

                        XX2<1,1>[1,20]    = TT.AC.NO
                        XX6<1,1>[1,20]    = TT.OLD.NO
                        XX2<1,1>[20,35]   = TT.CUS.NAME
                        XX6<1,1>[22,35]  = EXC.KEY
                        XX2<1,1>[57,10]   = TT.AMT
                        XX2<1,1>[72,10]   = TT.TIME
                        XX2<1,1>[87,10]   = TT.DATE
                        XX2<1,1>[103,35]  = TT.AUTH
                        XX6<1,1>[103,35]   = TT.OVER2
*  FINDSTR 'ON AVAILABLE' IN TT.OVER SETTING FMS,VMS THEN
*  END ELSE
                        PRINT XX2<1,1>
                        PRINT XX6<1,1>
                        PRINT XX10<1,1>
                        PRINT STR('-',130)
*  END
                    NEXT L
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'TF' THEN
                T.SEL4 = "SELECT ":FN.LC: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...OVER... OR OVERRIDE LIKE ...EXCESS...)"
                CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
                IF SELECTED4 THEN
                    FOR M = 1 TO SELECTED4
                        CALL F.READ(FN.LC,KEY.LIST4<M>,R.LC,F.LC,E5)
                        LC.AMT = R.LC<TF.LC.PROVIS.AMOUNT>
                        LC.AC.NO = R.LC<TF.LC.APPLICANT.ACC>
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,LC.AC.NO,LOCAL.REF)
                        LC.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,LC.AC.NO,OLD.REF)
                        LC.OLD.NO = OLD.REF
                        LC.AUTH1 = R.LC<TF.LC.OVERRIDE>
                        LC.OVER = R.LC<TF.LC.OVERRIDE>
                        FINDSTR 'OVER' IN LC.OVER SETTING FMS,VMS THEN
                            LC.OVER2 = 'OVERDRAFT'
                            FINDSTR 'ON AVAILABLE' IN LC.OVER SETTING FMS,VMS THEN
                                LC.OVER2 = 'OVERDRAFT-AVAL'
                            END
                        END

                        FINDSTR 'Limit' IN LC.OVER SETTING FMS,VMS THEN
                            LC.OVER2 = 'EXCEED LIMIT'
                            FINDSTR 'ON AVAILABLE' IN LC.OVER SETTING FMS,VMS THEN
                                LC.OVER2 = 'EXCEED-LIMIT-AVAL'
                            END
                        END

                        LC.AUTH2 = FIELD(LC.AUTH1,'SCB.',2)
                        IF LC.AUTH2 NE '' THEN
                            LC.AUTH3 = 'SCB.':LC.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,LC.AUTH3,AUTH.NAME)
                            LC.AUTH = AUTH.NAME
                        END ELSE
                            LC.AUTH1 = R.LC<TF.LC.AUTHORISER>
                            LC.AUTH3 = FIELD(LC.AUTH1,"_",2)
                            LC.AUTH4 = LC.AUTH3[1,3]
                            IF LC.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,LC.AUTH3,AUTH.NAME)
                                LC.AUTH = AUTH.NAME
                            END ELSE
                                LC.AUTH = LC.AUTH1
                            END
                        END
                        LC.DD.TT = R.LC<TF.LC.DATE.TIME>
*------------------------------------------------------------------------
                        LC.DATE1 = LC.DD.TT[1,2]
                        LC.DATE2 = LC.DD.TT[3,2]
                        LC.DATE3 = LC.DD.TT[5,2]
                        LC.DATE = LC.DATE3:"/":LC.DATE2:"/":LC.DATE1
*------------------------------------------------------------------------
                        LC.TIME1 = LC.DD.TT[7,2]
                        LC.TIME2 = LC.DD.TT[9,2]
                        LC.TIME = LC.TIME2:":":LC.TIME1
*------------------------------------------------------------------------
                        XX3 = SPACE(132)
                        XX7 = SPACE(132)
                        XX11 = SPACE(132)
                        XX3<1,1>[1,20]    = LC.AC.NO
                        XX7<1,1>[1,20]    = LC.OLD.NO
                        XX3<1,1>[20,35]   = LC.CUS.NAME
                        XX7<1,1>[22,35]   = EXC.KEY
                        XX3<1,1>[57,10]   = LC.AMT
                        XX3<1,1>[72,10]   = LC.TIME
                        XX3<1,1>[87,10]   = LC.DATE
                        XX3<1,1>[103,35]  = LC.AUTH
                        XX7<1,1>[103,35]  = LC.OVER2
*  FINDSTR 'ON AVAILABLE' IN LC.OVER SETTING FMS,VMS THEN
*  END ELSE

                        PRINT XX3<1,1>
                        PRINT XX7<1,1>
                        PRINT XX11<1,1>
                        PRINT STR('-',130)
* END
                    NEXT M
                END
            END
*************************************************************************
        NEXT I
    END

    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  ����� �������  ***'
    PRINT XX25<1,1>

*************************************************************************
    RETURN
*===============================================================
PRINT.HEAD:
*---------
*   CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TD1 = TODAY[1,4]
    TD2 = TODAY[5,2]
    TD3 = TODAY[7,2]
    TD = TD1:"/":TD2:"/":TD3

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(1):'���� ��� ����� ��������'
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"���� �������� ��� ��� �������� �� : ":TD
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������" :SPACE(10):"�����":SPACE(30):"������":SPACE(10):"�����":SPACE(10):"�������":SPACE(10):"���� �������"
    PR.HD :="'L'":"����� ������":SPACE(10):"������":SPACE(75):"��� �������"
    PR.HD :="'L'":STR('_',130)
**    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
