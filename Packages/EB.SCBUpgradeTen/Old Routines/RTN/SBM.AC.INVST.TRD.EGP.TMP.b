* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>260</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------------------------------------------------
* COPY FROM SBM.AC.INVEST.TRADE.EGP By Nessma Mohammad
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.AC.INVST.TRD.EGP.TMP

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT T24.BP I_F.FUNDS.TRANSFER
    $INSERT T24.BP I_F.DATES
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.CATEGORY
    $INSERT T24.BP I_F.STMT.ENTRY
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT           I_F.SCB.AC.INVEST.EGP
    $INSERT           I_AC.LOCAL.REFS
    $INSERT           I_BR.LOCAL.REFS
*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT.CREATED "   ; CALL REM
    RETURN
*-----------------------------INITIALIZATIONS------------------------
INITIATE:
    REPORT.ID='SBM.AC.INVEST.TRADE.EGP'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.TMP = "F.SCB.AC.INVEST.EGP"  ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    I         = 1
********** TO GET FROM AND TO DATE ******************************
    T.DATE  = TODAY
    T.MONTH = T.DATE[5,2]
    T.YEAR  = TODAY[1,4]

    IF T.MONTH EQ '01' OR T.MONTH EQ '02' OR T.MONTH EQ '03' THEN
        T.YEAR = TODAY[1,4] - 1
    END

    IF T.MONTH EQ '01' OR T.MONTH EQ '04' OR T.MONTH EQ '07' OR T.MONTH EQ '10' THEN
        CALL ADD.MONTHS(T.DATE,'-1')
    END
    IF T.MONTH EQ '02' OR T.MONTH EQ '05' OR T.MONTH EQ '08' OR T.MONTH EQ '11' THEN
        CALL ADD.MONTHS(T.DATE,'-2')
    END
    IF T.MONTH EQ '03' OR T.MONTH EQ '06' OR T.MONTH EQ '09' OR T.MONTH EQ '12' THEN
        CALL ADD.MONTHS(T.DATE,'-3')
    END

    CORRECT.L.DATE = T.YEAR:T.DATE[5,4]
    CALL LAST.DAY(CORRECT.L.DATE)

    FIRST.DATE = CORRECT.L.DATE[1,6]:'01'
    CORRECT.F.DATE = FIRST.DATE
    CALL ADD.MONTHS(CORRECT.F.DATE,'-2')
    RETURN
*------------------------RED FORM TEXT FILE----------------------
PROCESS:
    T.SEL  = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ''"
    T.SEL := " AND CATEGORY IN (12014 12028) AND POSTING.RESTRICT LT 90"
    T.SEL := " AND NO.OF.BONDS NE '' AND CURRENCY EQ 'EGP' BY CATEGORY"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    TOT.1 = 0; TOT.2 = 0; TOT.3 = 0; TOT.4 = 0; TOT.5 = 0; TOT.6 = 0

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)

            ACCT.ID     = KEY.LIST<I>
            ACCT.TITLE  = R.AC<AC.ACCOUNT.TITLE.1>
            NO.OF.BONDS = R.AC<AC.LOCAL.REF,ACLR.NO.OF.BONDS>
            MATUR.DATE  = R.AC<AC.LOCAL.REF,ACLR.MATUR.DATE>
            COST.BONDS  = R.AC<AC.LOCAL.REF,ACLR.COST.BONDS>
            CALL GET.ENQ.BALANCE(ACCT.ID,CORRECT.L.DATE,PREV.BAL)
            PREV.BAL    = R.AC<AC.LOCAL.REF,ACLR.PREV.BAL>
            PRICE.EVAL  = R.AC<AC.LOCAL.REF,ACLR.PRICE.EVAL>
            EVAL.BAL    = R.AC<AC.LOCAL.REF,ACLR.EVAL.BAL>
            REVALUATION = EVAL.BAL + PREV.BAL
*-------------------------
            GOSUB WRT.TMP
*-------------------------
***************** TO GET PREV OPEN BAL ********************
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,CORRECT.F.DATE,CORRECT.L.DATE,ID.LIST,OPENING.BAL,ER)
            ADD.AMT = 0; SUB.AMT = 0

            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS
            WHILE STE.ID:POS

                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.TRANS = R.STE<AC.STE.TRANSACTION.CODE>

                IF STE.TRANS NE '' AND STE.TRANS NE '849' THEN
                    STE.AMT   = R.STE<AC.STE.AMOUNT.LCY>
                    IF STE.AMT LT 0 THEN
                        ADD.AMT = ADD.AMT + STE.AMT
                    END ELSE
                        SUB.AMT = SUB.AMT + STE.AMT
                    END
                END
            REPEAT
**-----------------------
            GOSUB WRITE
**-----------------------
            TOT.1 = TOT.1 + COST.BONDS
            TOT.2 = TOT.2 + PREV.BAL
            TOT.3 = TOT.3 + EVAL.BAL
            TOT.4 = TOT.4 + REVALUATION
            TOT.5 = TOT.5 + ADD.AMT
            TOT.6 = TOT.6 + SUB.AMT
        NEXT I

        XX1 = SPACE(132)
        XX3 = SPACE(132)
        XX5 = SPACE(132)

        XX3<1,1>[1,132]  = ""
        XX1<1,1>[1,15]   = "�������"
        XX1<1,1>[41,15]  = DROUND(TOT.1,2)
        XX1<1,1>[55,15]  = DROUND(TOT.2,2)
        XX1<1,1>[90,15]  = DROUND(TOT.3,2)
        XX1<1,1>[110,15] = DROUND(TOT.4,2)
        XX1<1,1>[125,15] = DROUND(TOT.5,2)
        XX5<1,1>[125,15] = DROUND(TOT.6,2)

        PRINT XX3<1,1>
        PRINT XX3<1,1>
        PRINT XX1<1,1>
        PRINT XX5<1,1>
    END ELSE
        XX15 = SPACE(132)
        XX15<1,1>[60,132]  = "�� ���� ������"
        PRINT XX15<1,1>
    END
    RETURN
*******************************************************
WRT.TMP:
    NAZEER.1      = ""
    NAZEER.1.NAME = ""

*-- WRITE IN TEMP WITH AMOUNT WITH @ID
    CALL F.READ(FN.TMP,KEY.LIST<I>, R.TMP, F.TMP, ETEXT.TMP)
    R.TMP<AC.INV.AMOUNT> = REVALUATION
    VAR.1      = R.TMP<AC.INV.EQUIVALENT.NUMBER>
    VAR.1.NAME = R.TMP<AC.INV.EQUIVALENT.NAME>

    VAR.2      = R.TMP<AC.INV.PL.CATEGORY>
    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,VAR.2,VAR.2.NAME)

    IF VAR.1 NE '' THEN
        NAZEER.1      = VAR.1
        NAZEER.1.NAME = VAR.1.NAME
    END

    IF VAR.2 NE '' THEN
        NAZEER.1      = VAR.2
        NAZEER.1.NAME = VAR.2.NAME
    END

    WRITE  R.TMP TO F.TMP , KEY.LIST<I> ON ERROR
        PRINT "CAN NOT WRITE RECORD": KEY.LIST<I> : "TO" :FN.TMP
    END
    RETURN
*******************************************************
WRITE:
    XX2    = SPACE(132)
    XX4    = SPACE(132)
    XX9    = SPACE(132)
    XX.TMP = SPACE(132)
    YY.TMP = ""

    XX9<1,1>[1,135]  = STR('-',135)

    XX2<1,1>[1,15]   = ACCT.ID
    XX4<1,1>[1,30]   = ACCT.TITLE
    XX2<1,1>[26,15]  = NO.OF.BONDS
    XX4<1,1>[26,15]  = MATUR.DATE[7,2]:'/':MATUR.DATE[5,2]:'/':MATUR.DATE[1,4]

    XX.1 = DROUND(COST.BONDS,2)
    XX.1 = FMT(XX.1, "L2,")
    XX2<1,1>[41,15]  = XX.1

    XX.1 = DROUND(PREV.BAL,2)
    XX.1 = FMT(XX.1, "L2,")
    XX2<1,1>[55,15]  = XX.1

    XX2<1,1>[76,15]  = DROUND(PRICE.EVAL,2)

    XX.1 = DROUND(EVAL.BAL,2)
    XX.1 = FMT(XX.1, "L2,")
    XX2<1,1>[90,15]  = XX.1

    XX.1 = DROUND(REVALUATION,2)
    XX.1 = FMT(XX.1, "L2,")
    XX2<1,1>[110,15] = XX.1

    XX.1 = DROUND(ADD.AMT,2)
    XX.1 = FMT(XX.1, "L2,")
    XX2<1,1>[125,15] = XX.1

    XX.1 = DROUND(SUB.AMT,2)
    XX.1 = FMT(XX.1, "L2,")
    XX4<1,1>[125,15] = XX.1

    XX.TMP<1,1>[1,30] = NAZEER.1
    YY.TMP<1,1>[1,30] = NAZEER.1.NAME

    PRINT XX2<1,1>
    PRINT XX4<1,1>
    PRINT XX.TMP<1,1>
    PRINT YY.TMP<1,1>
    PRINT XX9<1,1>
    RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    L.DATE = CORRECT.L.DATE
    L.DATE = L.DATE[7,2]:'/':L.DATE[5,2]:"/":L.DATE[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" ":SPACE(110):"SBM.AC.INVST.TRD.EGP.TMP"

    PR.HD :="'L'":SPACE(50):"����������� ������� ���� ��������"
    PR.HD :="'L'":SPACE(62):"��":SPACE(2):L.DATE
    PR.HD :="'L'":SPACE(50):STR('_',33)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������":SPACE(15):"�����":SPACE(7):"�������":SPACE(8):"������ ������":SPACE(5):"�. �������":SPACE(4):"�.���� �.�������":SPACE(4):"���� �������":SPACE(8):"��������"
    PR.HD :="'L'":"��� ������":SPACE(45):"�.���������":SPACE(58):"�����������"
    PR.HD :="'L'":"��� � ���� �������"
    PR.HD :="'L'":"��� � ���� �������"

    PR.HD :="'L'":STR('_',135)
    PRINT
    HEADING PR.HD
    RETURN
*----------------------------------------------------------
END
