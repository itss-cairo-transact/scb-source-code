* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBD.DEP.DUCH.001

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.LEGAL.FORM
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "DAILY.DEPOSITS.DUCH.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DAILY.DEPOSITS.DUCH.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DAILY.DEPOSITS.DUCH.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DAILY.DEPOSITS.DUCH.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create DAILY.DEPOSITS.DUCH.CSV File IN &SAVEDLISTS&'
        END
    END

    CUS.ID      = ''  ; CUST.NAME  = '' ; WS.CCY    = '' ; WS.AMT = 0        ; WS.BUCKET.LABEL   = ''
    WS.MAT.DATE = ''  ; WS.RATE    = 0  ; WS.SPREAD = 0  ; CUST.SECTOR  = '' ; WS.RATE.TYPE.CODE = ''
    DAYS        = ''  ; INT.EXPENS = 0  ; WS.CATEG  = 0  ; WS.RATE.TYPE = ''
    WS.AMT.1002     = 0  ; WA.AMT.LCY = 0 ; WS.LD.INV.AMT = '' ; WS.TARGET = ''
    WS.AMT.1002.FCY = 0  ; WA.AMT.FCY = 0 ; WS.INDUSTRY   = '' ; WS.FREQ = ''
    WS.TOT.RATE = 0  ; WS.NXT.INT.AMT = 0 ; WS.INT.AMT = 0

    DAT.1 = TODAY[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,DAT.2)

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "DAILY.DEPOSITS"
    HEAD.DESC = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "CURRENCY":","
    HEAD.DESC := "AMOUNT.EQUIVALENT":","
    HEAD.DESC := "NATIVE.AMOUNT":","

    HEAD.DESC := "Weighted.Average.AMOUNT.EQUIVALENT":","
    HEAD.DESC := "Weighted.Average.NATIVE.AMOUNT":","

    HEAD.DESC := "MATURITY.DATE":","
    HEAD.DESC := "ISSUE.DATE":","
    HEAD.DESC := "RATE":","
    HEAD.DESC := "SPREAD.RATE":","
    HEAD.DESC := "TOTAL.RATE":","
    HEAD.DESC := "TYPE.OF.RATE":","
    HEAD.DESC := "CUSTOMER.TYPE":","
    HEAD.DESC := "SECTOR.NAME":","
    HEAD.DESC := "DAYS.TO.MATURITY":","
    HEAD.DESC := "INTEREST.EXPENSES":","
    HEAD.DESC := "BUCKET.LABEL":","
    HEAD.DESC := "LEDGER":","
    HEAD.DESC := "PRODUCT.TYPE":","
    HEAD.DESC := "BRANCH.NAME":","
    HEAD.DESC := "BANK.CODE":","
    HEAD.DESC := "INVESTED.AMOUNT":","
    HEAD.DESC := "INDUSTRY":","
    HEAD.DESC := "Customer.Gender":","
    HEAD.DESC := "Customer.Birthday":","
    HEAD.DESC := "Legal.Form":","
    HEAD.DESC := "NEXT.INTEREST":","
    HEAD.DESC := "INTEREST.AMOUNT":","
    HEAD.DESC := "Segmentation":","
    HEAD.DESC := "Segmentation.Desc":","
    HEAD.DESC := "Interest.Frequency":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN

*========================================================================
PROCESS:
    FN.CBE = 'F.SCB.CUS.POS.LW' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = ''
    CALL OPF(FN.GCI,F.GCI)

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.SEC.N = 'F.SCB.NEW.SECTOR' ; F.SEC.N = ''
    CALL OPF(FN.SEC.N,F.SEC.N)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.BID = 'FBNK.BASIC.INTEREST.DATE' ; F.BID = ''
    CALL OPF(FN.BID,F.BID)

    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)

    FN.AD = 'FBNK.ACCOUNT.DATE' ; F.AD = ''
    CALL OPF(FN.AD,F.AD)

    FN.LEG = 'F.SCB.CUS.LEGAL.FORM' ; F.LEG = ''
    CALL OPF(FN.LEG,F.LEG)

    FN.SCH = 'FBNK.LD.SCHEDULE.DEFINE' ; F.SCH = ''
    CALL OPF(FN.SCH,F.SCH)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* CURRENT ACCOUNTS SELECTION *************

    T.SEL  = "SELECT ":FN.CBE:" WITH (( CATEGORY IN (1422 1445 1415 1417 1418 1419 1420 1421 1481 1484 1485 1499 1408 1582 1483 1595 1493 1558 1525)"
    T.SEL := " OR CATEGORY IN (1001 1003 1005 1006 1007 1008 1009 1059 1102 1201 1202 1205 1206 1207 11242 1208 1211 1212 1215 1216 1301 1302 1303 1377 1390)"
    T.SEL := " OR CATEGORY IN (1399 1401 1402 1404 1405 1406 1414 1455 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513 1535 1526 1011 1214 1524 1579 1596 1597)"
    T.SEL := " OR CATEGORY IN (1514 1516 1518 1519 1523 1534 1544 1559 1560 1566 1577 1588 1599 1407 1413 11240 11232 11239 1480 2006 1217 16706 16700 1585 1586 1587))"
    T.SEL := " AND LCY.AMOUNT GT 0) BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'Current.Accounts'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
*---------------------------------------------

******* PAYROLL ACCOUNTS SELECTION *************

    T.SEL  = "SELECT ":FN.CBE:" WITH CATEGORY EQ 1002 AND LCY.AMOUNT GT 0 BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'Payroll.Accounts'
            GOSUB GET.DETAIL
        NEXT I
    END
*---------------------------------------------
******* SAVING ACCOUNT SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN (6501 6502 6503 6504 6505 6506 6507 6508 6511 6513 6514 6515 6516 6517) AND LCY.AMOUNT GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'Saving.Accounts'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
*-------------------------------------------------------
******* TIME DEPOSITS SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21015) OR (CATEGORY EQ 6512)) AND LCY.AMOUNT GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'Time.Deposits'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
************************************************************
******* CDs SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 21017 AND CATEGORY LE 21029) OR (CATEGORY EQ 21032 OR CATEGORY EQ 21036 OR CATEGORY EQ 21041 OR CATEGORY EQ 21042)) AND LCY.AMOUNT GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'CDs'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
************************************************************
******* OTHER SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN (16113 16151 16153 16188 16152 11380 16175 1012 1013 1014 1015 1016 1019 16170 3011 3012 3013 3014 3017 3010 3005 3050 3060 3065 16198 16149 1017 3206 3208) AND LCY.AMOUNT GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'Others'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
    RETURN
*==============================================================
PRINT.DET:

    BB.DATA  = CUS.ID:","
    BB.DATA := CUST.NAME:","
    BB.DATA := WS.CCY:","

    IF WS.CATEG EQ '1002' THEN
        BB.DATA := WS.AMT.1002:","
        BB.DATA := WS.AMT.1002.FCY:","

    END ELSE
        BB.DATA := WS.AMT:","
        BB.DATA := WS.AMT.FCY:","
    END

    BB.DATA := WA.AMT.LCY:","
    BB.DATA := WA.AMT.FCY:","
    BB.DATA := WS.MAT.DATE:","
    BB.DATA := WS.VAL.DATE:","
    BB.DATA := WS.RATE:","
    BB.DATA := WS.SPREAD:","

    WS.TOT.RATE = WS.RATE + WS.SPREAD
    BB.DATA := WS.TOT.RATE:","

    INT.EXPENS  = ( WS.AMT * WS.TOT.RATE * DAYS ) / 36500
    INT.EXPENS  = DROUND(INT.EXPENS,'0')

    BB.DATA := WS.RATE.TYPE:","
    BB.DATA := CUST.SECTOR:","
    BB.DATA := SECTOR.NAME:","
    BB.DATA := DAYS:","
    BB.DATA := INT.EXPENS:","
    BB.DATA := WS.BUCKET.LABEL:","
    BB.DATA := WS.CATEG:","
    BB.DATA := DESC:","
    BB.DATA := BRN.NAME:","
    BB.DATA := CUST.BANK:","
    BB.DATA := WS.LD.INV.AMT:","
    BB.DATA := WS.INDUSTRY:","
    BB.DATA := WS.GENDER:","
    BB.DATA := WS.BDATE:","
    BB.DATA := WS.LEGAL:","
    BB.DATA := WS.NXT.INT.AMT:","
    BB.DATA := WS.INT.AMT:","
    BB.DATA := WS.TARGET:","
    BB.DATA := WS.TARGET.NAME:","
    BB.DATA := WS.FREQ:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*===============================================================
GET.DETAIL:

    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
    CALL F.READ(FN.CBE,KEY.LIST<I+1>,R.CBE1,F.CBE,E2)

    WS.CATEG    = R.CBE<CUPOS.CATEGORY>

    IF WS.CATEG NE '1002' THEN
        CUS.ID      = R.CBE<CUPOS.CUSTOMER>

        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

        WS.TARGET      = R.CU<EB.CUS.TARGET>
        CALL F.READ(FN.TAR,WS.TARGET,R.TAR,F.TAR,E3)
        WS.TARGET.NAME = R.TAR<EB.TAR.DESCRIPTION>

        CUST.NAME        = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
        CUST.BANK        = R.CU<EB.CUS.LOCAL.REF><1,CULR.CU.BANK>
        CUST.SECTOR.CODE = R.CU<EB.CUS.SECTOR>
        CUST.SECTOR.CODE.NEW = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
        WS.INDUSTRY          = R.CU<EB.CUS.INDUSTRY>
        WS.GENDER            = R.CU<EB.CUS.LOCAL.REF><1,CULR.GENDER>
        WS.BDATE             = R.CU<EB.CUS.BIRTH.INCORP.DATE>
        WS.LEGAL.ID          = R.CU<EB.CUS.LOCAL.REF><1,CULR.LEGAL.FORM>

        CALL F.READ(FN.SEC.N,CUST.SECTOR.CODE.NEW,R.SEC.N,F.SEC.N,E1)
        SECTOR.NAME = R.SEC.N<C.SCB.NEW.SECTOR.NAME>

        CALL F.READ(FN.LEG,WS.LEGAL.ID,R.LEG,F.LEG,E6)
        WS.LEGAL = R.LEG<LEG.DESCRIPTION,1>

        IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1300 OR CUST.SECTOR.CODE EQ 1400 OR CUST.SECTOR.CODE EQ 2000 THEN
            IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1300 THEN
                CUST.SECTOR = 'Staff'
            END

            IF CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1400 THEN
                CUST.SECTOR = 'Staff.Relatives'
            END
            IF CUST.SECTOR.CODE EQ 2000 THEN
                CUST.SECTOR = 'Retail'
            END
        END ELSE
            CUST.SECTOR = 'Corporate'
        END

        WS.CCY      = R.CBE<CUPOS.DEAL.CCY>
        WS.AMT.FCY  = R.CBE<CUPOS.DEAL.AMOUNT>

        IF DAT.1 EQ DAT.2 THEN
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 398 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE WS.CCY IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            AMT.RATE = R.CCY<RE.BCP.RATE,POS>
            WS.AMT   = WS.AMT.FCY * AMT.RATE
        END ELSE
            WS.AMT   = R.CBE<CUPOS.LCY.AMOUNT>
        END

        APP.REF.ID  = FIELD(KEY.LIST<I>,"*",2)
        TRNS.ID     = FIELD(KEY.LIST<I>,"*",5)
        WS.COMP     = R.CBE<CUPOS.CO.CODE>

        IF ( WS.CATEG GE 3010 AND WS.CATEG LE 3017 ) THEN DESC = 'LCs'
        IF ( WS.CATEG EQ 3005 ) THEN DESC = 'LGs'
        IF ( WS.CATEG EQ 3060 OR WS.CATEG EQ 3065 ) THEN DESC = 'LCs'

        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRN.NAME)

        IF APP.REF.ID EQ 'LD' OR APP.REF.ID EQ 'CD' THEN
            WS.MAT.DATE = R.CBE<CUPOS.MATURITY.DATE>
            WS.VAL.DATE = R.CBE<CUPOS.VALUE.DATE>

            CALL F.READ(FN.LD,TRNS.ID,R.LD,F.LD,E1)
            WS.RATE           = R.LD<LD.INTEREST.RATE>
            WS.RATE.TYPE.CODE = R.LD<LD.INT.RATE.TYPE>
            WS.SPREAD         = R.LD<LD.INTEREST.SPREAD>
            WS.LD.RATE        = R.LD<LD.INTEREST.RATE>
            WS.LD.INV.AMT     = R.LD<LD.LOCAL.REF><1,LDLR.INVESTED.AMOUNT>
            WS.NXT.INT.AMT    = R.LD<LD.TOT.INTEREST.AMT>


            IF WS.LD.RATE EQ '' THEN
                WS.LD.KEY = R.LD<LD.INTEREST.KEY>
                BI.ID.KEY = WS.LD.KEY:WS.CCY

                CALL F.READ(FN.BID,BI.ID.KEY,R.BID,F.BID,E4)
                BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
                BI.ID = BI.ID.KEY:BI.DATE

                CALL F.READ(FN.BI,BI.ID,R.BI,F.BI,E3)
                WS.RATE = R.BI<EB.BIN.INTEREST.RATE>

            END
            IF WS.RATE.TYPE.CODE EQ 3 THEN
                WS.RATE.TYPE = 'FLOATING'
            END ELSE
                WS.RATE.TYPE = 'FIXED'
            END

            WS.RATE.T = WS.RATE + WS.SPREAD

********************************
            DAYS1 = "C"
            IF WS.MAT.DATE NE '' THEN
                CALL CDD("",WS.VAL.DATE,WS.MAT.DATE,DAYS1)
            END ELSE
                DAYS1 = ''
            END
            IF WS.CCY EQ 'EGP' THEN
                WS.INT.AMT = ( WS.AMT.FCY * WS.RATE.T * DAYS1 ) / 36500
            END ELSE
                WS.INT.AMT = ( WS.AMT.FCY * WS.RATE.T * DAYS1 ) / 36000
            END

***** Interest Frequency *****

            CALL F.READ(FN.SCH,TRNS.ID,R.SCH,F.SCH,E3)
            WS.SCH.TYPE = R.SCH<LD.SD.SCH.TYPE,1>
            IF WS.SCH.TYPE EQ 'I' THEN
                WS.FREQ = R.SCH<LD.SD.FREQUENCY,1>
            END ELSE
                WS.FREQ = R.SCH<LD.SD.FREQUENCY,2>
            END

******************************
        END

********************************
        IF APP.REF.ID EQ 'AC' THEN
            WS.RATE.TYPE = 'FIXED'
            CALL F.READ(FN.AC,TRNS.ID,R.AC,F.AC,E1)
            CUST.NAME = R.AC<AC.ACCOUNT.TITLE.1>
            GROUP.ID  = R.AC<AC.CONDITION.GROUP>
            WS.VAL.DATE = R.AC<AC.OPENING.DATE>
            CALL F.READ(FN.GC,GROUP.ID,R.GC,F.GC,E1)
            WS.MAT.DATE  = R.GC<IC.GCP.CR.CAP.FREQUENCY>[1,8]

******************************
            IF CUS.ID EQ '' THEN
                WS.RATE         = ''
                WS.SPREAD       = ''
                WS.RATE.TYPE    = ''
                CUST.SECTOR     = ''
                WS.BUCKET.LABEL = ''
            END ELSE

*********** SELECT FROM ACI *****

*Line [ 496 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.AC.COUNT = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
                AD.DATE     = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
                ACI.ID      = TRNS.ID:'-':AD.DATE

                CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 502 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
                WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>
                IF WS.RATE EQ '' THEN

**** SELECT FROM BASIC.INTEREST ****
                    ACI.BI.KEY   = R.ACI<IC.ACI.CR.BASIC.RATE>
                    ACI.BI.ID.KEY    = ACI.BI.KEY:WS.CCY
                    ACI.OPER     = R.ACI<IC.ACI.CR.MARGIN.OPER>
                    ACI.MRG.RATE = R.ACI<IC.ACI.CR.MARGIN.RATE>

                    CALL F.READ(FN.BID,ACI.BI.ID.KEY,R.BID,F.BID,E4)
                    ACI.BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
                    ACI.BI.ID   = ACI.BI.ID.KEY:ACI.BI.DATE

                    CALL F.READ(FN.BI,ACI.BI.ID,R.BI,F.BI,E3)

                    WS.RATE = R.BI<EB.BIN.INTEREST.RATE>
                    IF ACI.OPER EQ 'SUBTRACT' THEN
                        WS.RATE = WS.RATE - ACI.MRG.RATE
                    END
                    IF ACI.OPER EQ 'ADD' THEN
                        WS.RATE = WS.RATE + ACI.MRG.RATE
                    END


                    IF WS.RATE EQ '' THEN

**** SELECT RATE FROM GCI ****

                        GD.ID   = GROUP.ID:WS.CCY
                        CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
                        GD.DATE = R.GD<AC.GRD.CREDIT.GROUP.DATE>
                        IF GD.DATE EQ '' THEN
                            GD.DATE = R.GD<AC.GRD.CREDIT.DATES,1>
                        END
                        GCI.ID  = GD.ID:GD.DATE

                        CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
*Line [ 541 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        WS.RATE.COUNT   = DCOUNT(R.GCI<IC.GCI.CR.INT.RATE>,@VM)

                        FOR KK = 1 TO WS.RATE.COUNT
                            WS.AMT.POS   = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK>
                            IF KK = 1 THEN
                                WS.AMT.POS.1 = 0
                            END ELSE
                                WS.AMT.POS.1 = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK-1>
                            END
                            IF WS.AMT GT WS.AMT.POS.1 AND WS.AMT LE WS.AMT.POS THEN
                                WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,KK>
                            END
                            WS.LAST.AMT = R.GCI<IC.GCI.CR.LIMIT.AMT><1,WS.RATE.COUNT-1>
                            IF WS.AMT GT WS.LAST.AMT THEN
                                WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,WS.RATE.COUNT>
                            END
                        NEXT KK

                        WS.OPER   = R.GCI<IC.GCI.CR.MARGIN.OPER>
                        WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE,1>
                        IF WS.SPREAD = '' THEN
                            WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE,2>
                        END

                        IF WS.OPER EQ 'SUBTRACT' THEN
                            WS.SPREAD = '-':WS.SPREAD
                        END

                    END
                END
            END

        END
********************************
        DAYS = "C"
        IF WS.MAT.DATE NE '' THEN
            CALL CDD("",DAT.2,WS.MAT.DATE,DAYS)
        END ELSE
            DAYS = ''
        END

**        INT.EXPENS  = ( WS.AMT * WS.RATE * DAYS ) / 36500
**        INT.EXPENS  = DROUND(INT.EXPENS,'0')

        MONTHS.NO = 1
        CALL EB.NO.OF.MONTHS(DAT.2,WS.MAT.DATE,MONTHS.NO)

        IF DAYS EQ 1 THEN
            WS.BUCKET.LABEL = '1.DAY'
        END ELSE
            IF DAYS GT 1 AND DAYS LE 7 THEN
                WS.BUCKET.LABEL = '1.WEEK'
            END ELSE
                IF MONTHS.NO EQ 1 THEN
                    WS.BUCKET.LABEL = '1.MONTH'
                END
            END
        END

**IF DAYS GT 7 AND DAYS LE 31 THEN WS.BUCKET.LABEL = '1.MONTH'
**IF DAYS GT 31 AND DAYS LE 93 THEN WS.BUCKET.LABEL = '3.MONTHS'
**IF DAYS GT 93 AND DAYS LE 186 THEN WS.BUCKET.LABEL = '6.MONTHS'
**IF DAYS GT 186 AND DAYS LE 370 THEN WS.BUCKET.LABEL = '1.YEAR'
**IF DAYS GT 370 AND DAYS LE 1090 THEN WS.BUCKET.LABEL = '3.YEAR'
**IF DAYS GT 1090 THEN WS.BUCKET.LABEL = 'MORE.THAN.3.YEAR'

        IF MONTHS.NO GT 1 AND MONTHS.NO LE 3 THEN WS.BUCKET.LABEL = '3.MONTHS'
        IF MONTHS.NO GT 3 AND MONTHS.NO LE 6 THEN WS.BUCKET.LABEL = '6.MONTHS'
        IF MONTHS.NO GT 6 AND MONTHS.NO LE 9 THEN WS.BUCKET.LABEL = '9.MONTHS'
        IF MONTHS.NO GT 9 AND MONTHS.NO LE 12 THEN WS.BUCKET.LABEL = '1.YEAR'
        IF MONTHS.NO GT 12 AND MONTHS.NO LE 18 THEN WS.BUCKET.LABEL = '1.5.YEAR'
        IF MONTHS.NO GT 18 AND MONTHS.NO LE 24 THEN WS.BUCKET.LABEL = '2.YEAR'
        IF MONTHS.NO GT 24 AND MONTHS.NO LE 36 THEN WS.BUCKET.LABEL = '3.YEAR'
        IF MONTHS.NO GT 36 AND MONTHS.NO LE 48 THEN WS.BUCKET.LABEL = '4.YEAR'
        IF MONTHS.NO GT 48 AND MONTHS.NO LE 60 THEN WS.BUCKET.LABEL = '5.YEAR'
        IF MONTHS.NO GT 60 AND MONTHS.NO LE 72 THEN WS.BUCKET.LABEL = '6.YEAR'
        IF MONTHS.NO GT 72 AND MONTHS.NO LE 84 THEN WS.BUCKET.LABEL = '7.YEAR'
        IF MONTHS.NO GT 84 AND MONTHS.NO LE 96 THEN WS.BUCKET.LABEL = '8.YEAR'
        IF MONTHS.NO GT 96 AND MONTHS.NO LE 108 THEN WS.BUCKET.LABEL = '9.YEAR'
        IF MONTHS.NO GT 108 AND MONTHS.NO LE 120 THEN WS.BUCKET.LABEL = '10.YEAR'
        IF MONTHS.NO GT 120 AND MONTHS.NO LE 132 THEN WS.BUCKET.LABEL = '11.YEAR'
        IF MONTHS.NO GT 132 AND MONTHS.NO LE 144 THEN WS.BUCKET.LABEL = '12.YEAR'
        IF MONTHS.NO GT 144 AND MONTHS.NO LE 156 THEN WS.BUCKET.LABEL = '13.YEAR'
        IF MONTHS.NO GT 156 AND MONTHS.NO LE 168 THEN WS.BUCKET.LABEL = '14.YEAR'
        IF MONTHS.NO GT 168 AND MONTHS.NO LE 180 THEN WS.BUCKET.LABEL = '15.YEAR'
        IF MONTHS.NO GT 180 AND MONTHS.NO LE 192 THEN WS.BUCKET.LABEL = '16.YEAR'
        IF MONTHS.NO GT 192 AND MONTHS.NO LE 204 THEN WS.BUCKET.LABEL = '17.YEAR'
        IF MONTHS.NO GT 204 AND MONTHS.NO LE 216 THEN WS.BUCKET.LABEL = '18.YEAR'
        IF MONTHS.NO GT 216 AND MONTHS.NO LE 228 THEN WS.BUCKET.LABEL = '19.YEAR'
        IF MONTHS.NO GT 228 AND MONTHS.NO LE 240 THEN WS.BUCKET.LABEL = '20.YEAR'
        IF MONTHS.NO GT 240 THEN WS.BUCKET.LABEL = 'MORE.THAN.20.YEAR'

        WA.AMT.LCY = WS.AMT * DAYS
        WA.AMT.FCY = WS.AMT.FCY * DAYS

    END
    IF WS.CATEG EQ '1002' THEN

        GCI.ID.1002 = '1EGP...'
        T.SEL3 = "SELECT ":FN.GCI:" WITH @ID LIKE ":GCI.ID.1002:" BY @ID"
        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
        IF SELECTED3 THEN
            CALL F.READ(FN.GCI,KEY.LIST3<SELECTED3>,R.GCI,F.GCI,E3)
            WS.RATE.1002 = R.GCI<IC.GCI.CR.INT.RATE,1>
        END

        BRN  = R.CBE<CUPOS.CO.CODE>
        BRN1 = R.CBE1<CUPOS.CO.CODE>

        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN,BRN.NAME)

        WS.AMT.1002.FCY += R.CBE<CUPOS.DEAL.AMOUNT>
        WS.AMT.1002     += R.CBE<CUPOS.DEAL.AMOUNT>
        CUS.ID    = ''
        CUST.NAME = 'Salaries'
        WS.CCY    = R.CBE<CUPOS.DEAL.CCY>
        WS.MAT.DATE     = ''
        WS.VAL.DATE     = ''
        WS.RATE         = WS.RATE.1002
        CUST.SECTOR     = 'Staff'
        SECTOR.NAME     = '������ ������'
        DAYS            = ''
        INT.EXPENS      = ''
        WS.BUCKET.LABEL = ''

        IF BRN NE BRN1 THEN
            GOSUB PRINT.DET
            WS.AMT.1002.FCY = 0
            WS.AMT.1002 = 0
        END
    END
    RETURN

*===============================================================
