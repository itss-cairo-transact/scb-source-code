* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
***** CREATED BY NESSMA 2010/10/19 **************
    SUBROUTINE SBD.LD.MAT.DATE.MS

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLIDAY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*-----------------------------------------*
    GOSUB INITIAL
    GOSUB CALC.RANG
    GOSUB BFOR.PROCESS

    IF FLG NE "E" THEN
        GOSUB PRINT.HEAD
        GOSUB PROCESS
        GOSUB PRINT.END
    END
    RETURN
*-----------------------------------------*
INITIAL:
*-------
    GOSUB OPEN.FILES

    PROGRAM.ID = "SBD.LD.MAT.DATE.MS"
    REPORT.ID  = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    SYS.DATE = TODAY
    P.DATE   = FMT(SYS.DATE,"####/##/##")
*WS.COMP  = ID.COMPANY
    RANG.1   = ""
    RANG.2   = ""
    FLG      = ""

    ARR.LINE    = SPACE(120)
    ARR.AMT.BRN = SPACE(120)

    AMT.TEST = 0
    TOT.TEMP = 0

    WS.LD.CUS.FLAG  = 0
    WS.LD.BR.FLAG   = ''
    WS.TOTAL.BRANCH = 0
    WS.TOTAL.CUS    = 0
    WS.TOTAL.BANK   = 0
    WS.COMP         = ''
    RETURN

OPEN.FILES:
*----------
    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"   ; F.LD  = ""
    CALL OPF(FN.LD,F.LD)

    FN.CUS = "FBNK.CUSTOMER"               ; F.CUS = ""
    CALL OPF(FN.CUS,F.CUS)

    FN.HOL = 'F.HOLIDAY'   ; F.HOL = '' ; R.HOL = ''
    CALL OPF(FN.HOL,F.HOL)

    FN.COMP = 'F.COMPANY'  ; F.COMP = '' ; R.COMP = ''
    CALL OPF(FN.COMP,F.COMP)

    FN.PARM  = "FBNK.RE.BASE.CCY.PARAM"  ; F.PARM    = ""
    CALL OPF(FN.PARM,F.PARM)

    RETURN

BFOR.PROCESS:
*------------
    YTEXT = "Enter the Company No. : "
    CALL TXTINP(YTEXT, 8, 22, "14", "A")
    CH.NO = COMI
    IF LEN(CH.NO) EQ 1 THEN
        WS.COMP = "EG001000":CH.NO
    END
    IF LEN(CH.NO) EQ 2 THEN
        WS.COMP = "EG00100":CH.NO
    END
    IF CH.NO # "ALL" THEN
        CALL F.READ(FN.COMP,WS.COMP, R.COMP, F.COMP, E.CO)

        IF E.CO THEN
            TEXT = "ERROR IN COMPANY NO."  ;CALL REM
            FLG = "E"
        END
    END
    WS.COMP = "AND CO.CODE EQ ":WS.COMP:" "
    IF CH.NO EQ "ALL" THEN
        WS.COMP = ''
    END
    SEL.N  = "SELECT ": FN.LD :" WITH CATEGORY GE 21020 AND CATEGORY LE 21032 ":WS.COMP
    SEL.N := "AND FIN.MAT.DATE GE ":RANG.1:" AND FIN.MAT.DATE LE ":RANG.2
    SEL.N := " BY CO.CODE BY CUSTOMER.ID"
    RETURN
*-----------------------------------------*
PROCESS:
*-------
    CALL EB.READLIST(SEL.N,KEY.LIST,'',SELECTED,ERR.LD)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<II>, R.LD, F.LD, E.LD)
*** -------------- ADD BY MOHAMED SABRY --2010/10/27--------------
            WS.LD.AMT       = R.LD<LD.AMOUNT>
            WS.LD.CY        = R.LD<LD.CURRENCY>
            WS.LD.CUS.ID    = R.LD<LD.CUSTOMER.ID>
            WS.LD.CO.CODE   = R.LD<LD.CO.CODE>

            IF  WS.LD.CUS.FLAG EQ 0 THEN
                WS.LD.CUS.FLAG = WS.LD.CUS.ID
            END

            IF WS.LD.BR.FLAG  EQ '' THEN
                WS.LD.BR.FLAG = WS.LD.CO.CODE
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.LD.CO.CODE,WS.BR.NAME)
                PRINT "���":" " : WS.BR.NAME
            END

            IF WS.LD.CUS.FLAG NE WS.LD.CUS.ID THEN
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.LD.CUS.FLAG,WS.NAME)
                WS.NAME1 = WS.NAME<1,CULR.ARABIC.NAME>
                ARR.LINE<1,1>[1,10]   = WS.LD.CUS.FLAG
                ARR.LINE<1,1>[22,30]  = WS.NAME1
                ARR.LINE<1,1>[75,26]  = FMT(WS.TOTAL.CUS,"R2,")
                PRINT ARR.LINE<1,1>
                ARR.LINE<1,1> = ''
                WS.TOTAL.CUS = 0
            END

            IF WS.LD.BR.FLAG # WS.LD.CO.CODE   THEN
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.LD.BR.FLAG,WS.BR.NAME)
                PRINT SPACE(80) :"������ ��� ":" ":WS.BR.NAME:" = ": FMT(WS.TOTAL.BRANCH,"R2,")
                WS.TOTAL.BRANCH = 0
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.LD.CO.CODE,WS.BR.NAME)
                PRINT "���":" " : WS.BR.NAME
            END

            CALL F.READ(FN.PARM,'NZD',R.PARM.S,F.PARM,E.S)
*Line [ 167 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE WS.LD.CY IN R.PARM.S<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            WS.RATE   = R.PARM.S<RE.BCP.RATE,POS>
            WS.LD.AMT = WS.LD.AMT *WS.RATE

            WS.TOTAL.CUS    += WS.LD.AMT
            WS.TOTAL.BRANCH += WS.LD.AMT
            WS.TOTAL.BANK   += WS.LD.AMT
            WS.LD.CUS.FLAG   = WS.LD.CUS.ID
            WS.LD.BR.FLAG    = WS.LD.CO.CODE

            IF II EQ SELECTED THEN
                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,WS.LD.CUS.FLAG,WS.NAME)
                WS.NAME1 = WS.NAME<1,CULR.ARABIC.NAME>
                ARR.LINE<1,1>[1,10]   = WS.LD.CUS.FLAG
                ARR.LINE<1,1>[22,30]  = WS.NAME1
                ARR.LINE<1,1>[75,26]  = FMT(WS.TOTAL.CUS,"R2,")
                PRINT ARR.LINE<1,1>
                GOSUB PRINT.END
            END
** -----------------------------------------------------
        NEXT II
    END
    RETURN
*------------------------------------------*
CALC.RANG:
*---------
    MOD.DATE = TODAY
    CALL CDT ('' ,MOD.DATE , "+7C")
*------ CALC START.DATE -------------------*
    CALL AWD("EG00",MOD.DATE,RET)
    IF RET EQ 'W' THEN
        START.DATE = MOD.DATE
    END
    IF RET EQ 'H' THEN
        CALL SCB.WORKING.DAY(MOD.DATE,"EGP")
        START.DATE = MOD.DATE
    END
    RANG.1   = START.DATE
*------ CALC END.DATE -------------------------
    END.DATE = START.DATE
    CALL CDT ('' , END.DATE , "+1C")
    CALL AWD("EG00",END.DATE,RET3)
    IF RET3 EQ 'W' THEN
        END.DATE = START.DATE
    END
    IF RET3 EQ 'H' THEN
        CALL SCB.WORKING.DAY(END.DATE,"EGP")
        CALL CDT ('' , END.DATE , "-1C")
    END
    RANG.2 = END.DATE
    RETURN
*------------------------------------------*
PRINT.HEAD:
*----------
    IF CH.NO EQ "ALL" THEN
        BRANCH = "ALL"
    END ELSE
        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP[16,9],BRANCH)
    END
    YYBRN = BRANCH

    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110): PROGRAM.ID
    PR.HD :="'L'":SPACE(35) : "�������� ��������� ����  ����� ������� ������"
    PR.HD :="'L'":SPACE(37):"�� ������ �� ":SPACE(2):FMT(RANG.1,"####/##/##")
    PR.HD :=SPACE(10):"���":SPACE(2):FMT(RANG.2,"####/##/##")
    PR.HD :="'L'":" ":SPACE(33):STR('_',48)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"��� ������":SPACE(10):"��� ������"
    PR.HD :=SPACE(37):"������ ���� �������� ��������"
    PR.HD :="'L'":SPACE(68):"������� ������ �������� �����"
    PR.HD :=SPACE(1):STR('_',120)
    PR.HD :="'L'":" "
    HEADING PR.HD
    LINE.NO = 0
    RETURN
*-----------------------------------------*
PRINT.END:
*---------
    PRINT SPACE(80) :"������ ��� ":" ":WS.LD.BR.FLAG:" = ": FMT(WS.TOTAL.BRANCH,"R2,")
    EMPTY.LINE = " "
    PRINT SPACE(70):STR('_',20)
    PRINT EMPTY.LINE
    PRINT SPACE(70):"������ ":YYBRN:" = ": FMT(WS.TOTAL.BANK,"R2,")
    PRINT EMPTY.LINE
    PRINT EMPTY.LINE
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)
*TEXT = "��� �������"  ; CALL REM
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*-----------------------------------------*
END
