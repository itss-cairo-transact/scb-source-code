* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.ALL.GENLED.02.NEW

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D.AL.NEW
    $INSERT T24.BP  I_F.COMPANY
*---------------------------------------------------
    FN.CAT = "F.CATEG.MAS.D.AL.NEW"
    F.CAT  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""

*---------------------------------------------------
    CALL OPF (FN.CAT,F.CAT)
    CALL OPF (FN.COMP,F.COMP)
*-----------------------------------------------------
    WS.TOT.LCY = 0
    WS.TOT.FCY = 0
    WS.TOT.CONT = 0
    WS.TOT.UNCLAS =  0
*----------------------------------------------------

    DIM ARRAY1(100,5)
    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010077' AND @ID UNLIKE 'EG0010088' BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SEL.COMP,ER.MSG3)

    FOR I = 1 TO SEL.COMP

        WS.COMP = KEY.LIST3<I>

        ARRAY1(I,1) = WS.COMP
        ARRAY1(I,2) = "0"
        ARRAY1(I,3) = "0"
        ARRAY1(I,4) = "0"
        ARRAY1(I,5) = "0"

    NEXT I
*-----------------------------------------------------

    WS.HEAD.01.B = ""

    WS.PAGE.COUNT = 0

    WS.TABL.AMT = 0
*----------------------------PROCEDURE---------------------
*---------------------------------------------------------
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')

*------------------------PROCEDURE------------------------
    GOSUB A.100.PROC

    GOSUB A.500.PRT.HEAD
    WS.SUB = 1
    GOSUB A.400.PRT
    XX = SPACE(132)
    PRINT STR('=',120)
    XX<1,1>[1,30]   = "������ ��������"
    XX<1,1>[31,3]   = ""
    XX<1,1>[36,20]  = FMT(WS.TOT.LCY, "R2,")
    XX<1,1>[58,3]   = ""


    XX<1,1>[69,20]   = FMT(WS.TOT.CONT, "R2,")
    XX<1,1>[109,3]   = ""
    XX<1,1>[92,20]  = FMT(WS.TOT.UNCLAS, "R2,")

    PRINT XX<1,1>

    XX = SPACE(132)

    PRINT STR('=',120)
    XX<1,1>[55,30]  = "�����   �����"

    PRINT XX<1,1>
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*-----------------------------------------------
*------------------------------------------------
A.100.PROC:
    SEL.CMD = "SELECT ":FN.CAT:" BY CATD.BR BY CATD.ASST.LIAB"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CAT.ID FROM SEL.LIST SETTING POS
    WHILE WS.CAT.ID:POS
        CALL F.READ(FN.CAT,WS.CAT.ID,R.CAT,F.CAT,MSG.CAT)
        WS.LINE.NO = R.CAT<CATAL.CATD.ASST.LIAB>
        WS.AMT = R.CAT<CATAL.CATD.BAL.IN.LOCAL.CY>
        WS.BR  = R.CAT<CATAL.CATD.BR>

        WS.SUB = 1
        GOSUB  A.200.TABL
A.100.REP:
    REPEAT
A.100.EXIT:
    RETURN
*------------------------------------------------
A.200.TABL:
    IF WS.SUB GT SEL.COMP  THEN
        RETURN
    END
    IF ARRAY1(WS.SUB,1) EQ WS.BR THEN
        GOSUB A.300.ACUMLAT
        RETURN
    END
    WS.SUB = WS.SUB + 1
    GOTO A.200.TABL
    RETURN
*------------------------------------------------
A.300.ACUMLAT:
    IF  WS.LINE.NO LT 8998  THEN
        WS.TABL.AMT = ARRAY1(WS.SUB,2)
        WS.TABL.AMT =    WS.TABL.AMT + WS.AMT
        ARRAY1(WS.SUB,2) = WS.TABL.AMT
        RETURN
    END

    IF  WS.LINE.NO GE 8998  AND WS.LINE.NO LT 9990 THEN
        WS.TABL.AMT = ARRAY1(WS.SUB,4)
        WS.TABL.AMT =    WS.TABL.AMT + WS.AMT
        ARRAY1(WS.SUB,4) = WS.TABL.AMT
        RETURN
    END

    IF  WS.LINE.NO EQ 9990 THEN
        WS.TABL.AMT = ARRAY1(WS.SUB,5)
        WS.TABL.AMT =    WS.TABL.AMT + WS.AMT
        ARRAY1(WS.SUB,5) = WS.TABL.AMT
        RETURN
    END
    RETURN
*------------------------------------------------
A.400.PRT:
    IF WS.SUB GT SEL.COMP THEN
        RETURN
    END

    WS.COMP.ID  = ARRAY1(WS.SUB,1)
    CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
    WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
    WS.DEF.LCY = ARRAY1(WS.SUB,2)
    WS.TOT.LCY += WS.DEF.LCY


    WS.DEF.CONT = ARRAY1(WS.SUB,4)
    WS.TOT.CONT += WS.DEF.CONT
    WS.UNCLAS = ARRAY1(WS.SUB,5)
    WS.TOT.UNCLAS +=  WS.UNCLAS
    XX = SPACE(132)

    XX<1,1>[1,30]   = WS.BR.NAME
    XX<1,1>[31,3]   = ""
    XX<1,1>[36,20]  = FMT(WS.DEF.LCY, "R2,")
    XX<1,1>[58,3]   = ""


    XX<1,1>[69,20]  = FMT(WS.DEF.CONT, "R2,")
    XX<1,1>[109,3]  = ""
    XX<1,1>[92,20]  = FMT(WS.UNCLAS, "R2,")
    PRINT XX<1,1>
    XX = SPACE(132)
    PRINT XX<1,1>
    WS.SUB = WS.SUB + 1
    GOTO A.400.PRT
    RETURN
*------------------------------------------------


A.500.PRT.HEAD:
    WS.PAGE.COUNT = WS.PAGE.COUNT + 1

    DATY = TODAY
    CALL CDT('',DATY,'-1W')
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������":SPACE(20):WS.HEAD.01.B:"  ":T.DAY:SPACE(20):WS.PAGE.COUNT:"   SBD.ALL.GENLED.02.NEW"
    PR.HD :="'L'":SPACE(5):"����� ":SPACE(20):"��� ���� � ���� ":SPACE(20):"��� �����":SPACE(13):"��� ����"
    PR.HD :="'L'":STR('_',132)

    HEADING PR.HD
    PRINT
    RETURN
END
