* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*------------------------
*** Create By Nessma ***
*------------------------
    SUBROUTINE SBD.CALC.SAV.BAL.ZERO.ALL

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.RE.BASE.CCY.PARAM
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.CATEGORY
    $INSERT T24.BP I_F.CURRENCY
    $INSERT T24.BP I_F.COMPANY
    $INSERT T24.BP I_F.NUMERIC.CURRENCY
    $INSERT TEMENOS.BP I_CU.LOCAL.REFS
*---------------------------------------------------
    CMPNY.ID = ID.COMPANY
    GOSUB INTIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    GOSUB PRINT.TOT
    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "DONE"  ; CALL REM
    RETURN
*----------------------------------------------------------
LINE.END:
*--------
    PRINT " "
    PRINT SPACE(50) : "********* ����� �������  *********"
    RETURN
*----------------------------------------------------------
INTIAL:
*------
    REPORT.ID = 'SBM.AC.TOTALS'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC  = 'FBNK.ACCOUNT'        ;  F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER'       ;  F.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.COMP = 'F.COMPANY'          ;  F.COMP  = ''
    CALL OPF(FN.COMP,F.COMP)

    FN.CCC  = 'FBNK.CURRENCY'      ;  F.CCC  = ''
    CALL OPF(FN.CCC,F.CCC)

    FN.CNUM  = 'F.NUMERIC.CURRENCY'   ;  F.CNUM  = ''
    CALL OPF(FN.CNUM,F.CNUM)

    FN.CUR  = 'FBNK.CURRENCY'         ; F.CUR  = ''
    CALL OPF(FN.CUR,F.CUR)

    ENQ.LP   = 0
    CUR.RATE = ""
    COMP.OLD = ""
    COMP.NEW = ""
    CATEG    = ""
    CUR      = ""
    TOT.NO   = 0
    TOT.BAL  = 0
    BAL      = 0
    BREAK.2  = ""
    BREAK.1  = ""
    TOTAL.BANK = 0

    XX = ""
    YY = ""
    ZZ = ""
    NN = 1

    TOT.NO.CUR    = 0
    TOT.BL.CUR    = 0
    TOT.NO.CATEG  = 0
    TOT.BL.CATEG  = 0

    TOT.NO.BRN = 0
    CATEGORY.LIST = ""

    RETURN
*---------------------------------------------------
PROCESS:
*--------
    T.SEL   = "SELECT ":FN.AC:" WITH CATEGORY"
    T.SEL  := " IN ( 6501 6502 6503 6504 6511 )"
    T.SEL  := " AND OPEN.ACTUAL.BAL EQ 0"
    T.SEL  := " AND CO.CODE EQ " : CMPNY.ID
    T.SEL  := " BY CO.CODE BY CATEGORY BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E.AC)
            BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            CUR  = R.AC<AC.CURRENCY>
            CUS  = R.AC<AC.CUSTOMER>
            CATG = R.AC<AC.CATEGORY>
            CUR.CODE = KEY.LIST<I>[9,2]

            COMP.NEW  = R.AC<AC.CO.CODE>
            BREAK.2   = COMP.NEW

            IF I EQ 1 THEN
                BREAK.1 = BREAK.2
*                PRINT "-------------------------"
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.NEW,COMP.NAME)
*                PRINT "���  " : COMP.NAME
*                PRINT "-------------------------"
            END

            GOSUB PRINT.LINE.ALL

            IF BREAK.1 EQ BREAK.2 THEN
                IF CUR EQ 'EGP' THEN
                    TOTAL.BANK ++
                    TOT.NO++
                END ELSE
                    TOTAL.BANK ++
                    TOT.NO++
                END
            END ELSE
*-------------------------------------
                GOSUB PRINT.LINE
*-------------------------------------
*               PRINT "-------------------------"
*               CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BREAK.2,COMP.NAME)
*               PRINT "���  " : COMP.NAME
*               PRINT "-------------------------"

                IF CUR EQ 'EGP' THEN
                    TOT.NO++
                    TOTAL.BANK ++
                END ELSE
                    TOTAL.BANK ++
                    TOT.NO++
                END
            END

            BREAK.1 = BREAK.2
        NEXT I
    END ELSE
        PRINT "NO DATA"
    END
    RETURN
*-------------------------------------------------------
PRINT.LINE.ALL:
*--------------
    RR = ""
    RR<1,1>[1,20] = KEY.LIST<I>
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,LOCAL.REF)
    RR<1,1>[20,30] = LOCAL.REF<1,CULR.ARABIC.NAME>

    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATG,CATEG.NN)
    RR<1,1>[60,20] = CATEG.NN

    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,R.AC<AC.CURRENCY>,CUR.NAME)
    RR<1,1>[100,20] = CUR.NAME

    PRINT RR<1,1>
    PRINT " "
    RR = ""
    RETURN
*-------------------------------------------------------
PRINT.LINE:
*----------
    LINE = ""

    COMP.ID = FIELD(BREAK.1 ,"*", 1)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)

    LINE<1,1>[1,20]  = "������ ���  " :SPACE(5) : COMP.NAME
    LINE<1,1>[35,20] = TOT.NO

    IF TOT.NO GT 0 THEN
        PRINT STR('_',40)
        PRINT LINE<1,1>
        PRINT STR('_',40)
    END

    TOT.NO  = 0
    TOT.BAL = 0
    RETURN
*-------------------------------------------------------
PRINT.HEAD:
*-----------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  = "'L'":SPACE(1):" ��� ���� ������"
    PR.HD := "'L'":SPACE(1):"����� �������"
    PR.HD := T.DAY:SPACE(85):"��� ������ : ":"'P'"
*Line [ 216 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    PR.HD := "'L'":SPACE(109):"SBD.CALC.SAV.BAL.NULL.ALL"
    PR.HD := "'L'"
    PR.HD := "'L'":SPACE(50):"������� ������ ������ ������ �������"
    PR.HD := "'L'":SPACE(60):"������ ����� ���"
    PR.HD := "'L'":SPACE(45):STR('_',50)
    PR.HD := "'L'": " "
    PR.HD := "'L'":SPACE(1):"���  ������"
    PR.HD := SPACE(15) : "��� ������"
    PR.HD := SPACE(20) : "��� ������"
    PR.HD := SPACE(25) : "������"
    PR.HD := "'L'":STR('_',120)
    PR.HD := "'L'"
    PRINT
    HEADING PR.HD
    RETURN
*-------------------------------------------------------
PRINT.TOT:
*---------
    PRINT " "
    PRINT STR('_',120)
    PRINT SPACE(50): "������ ����� " : SPACE(10) : TOTAL.BANK
    PRINT STR('_',120)
    PRINT " "

    RETURN
*-------------------------------------------------------
END
