* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.HQ.ACCT3

****Mahmoud Elhawary******5/4/2009****************************
*  New report for interbranch transactions                   *
**************************************************************

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY
    $INCLUDE T24.BP I_F.TELLER       ;*TT.TE.
    $INCLUDE T24.BP I_F.FUNDS.TRANSFER         ;*FT.
    $INCLUDE           I_FT.LOCAL.REFS  ;*FTLR.
    $INCLUDE T24.BP I_F.BILL.REGISTER          ;*EB.BILL.REG.
    $INCLUDE           I_BR.LOCAL.REFS  ;*BRLR.
    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM      ;*RE.BCP.
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*    TDY = ''
*    TXT1 = "���� ����� �����  "
*    CALL TXTINP(TXT1, 8, 22, 14, TDY)
*    TD1 = COMI

    TD1 = TODAY
    TTDY = TODAY
    LWDY = TODAY
    CALL CDT("",LWDY,'-1W')

    PATHNAME1 = "&SAVEDLISTS&"
    FILENAME1 = "ACCT_HQ3.":COMP
*-----------------------------------------------------------------------
    OPENSEQ PATHNAME1 , FILENAME1 TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':PATHNAME1:' ':FILENAME1
        HUSH OFF
    END
    OPENSEQ PATHNAME1 , FILENAME1 TO BB ELSE
        CREATE BB THEN
            PRINT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END
*----------------------------------------------------------------

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 77 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
INITIATE:
*--------
    REPORT.ID  ='SBD.HQ.ACCT3'
    REPORT.NAME='SBD.HQ.ACCT3'
    CALL PRINTER.ON(REPORT.ID,'')
    K = 0
    K.LIST.COM ="" ; SELECTED.COM ="" ; ER.MSG.COM =""
    K.LIST.ACC ="" ; SELECTED.ACC ="" ; ER.MSG.ACC =""
    K.LIST.STE ="" ; SELECTED.STE ="" ; ER.MSG.STE =""
    K.LIST.ENT ="" ; SELECTED.ENT ="" ; ER.MSG.ENT =""
    K.LIST.AC =""  ; SELECTED.AC =""  ; ER.MSG.AC =""
    K.LIST    =""  ; SELECTED    =""  ; ER.MSG    =""
    AMT.LCY = ''
    AMT.IN.DR  = 0
    AMT.IN.CR  = 0
    AMT.OUT.DR = 0
    AMT.OUT.CR = 0
    TOT.IN.DR  = 0
    TOT.IN.CR  = 0
    TOT.OUT.DR = 0
    TOT.OUT.CR = 0
    TOTAL.IN.DR = 0
    TOTAL.IN.CR = 0
    TOTAL.OUT.DR = 0
    TOTAL.OUT.CR = 0
    STE.CNF.DR = ''
    STE.CNF.CR = ''
                    STE.CR.COMP = ''
                    STE.DR.COMP = ''
    STE.VER = ''
    STE.TCOD= ''
    CN.FLAG = ''
    RETURN
*===============================================
CALLDB:
*--------
    FN.COM = 'F.COMPANY'
    F.COM = ''
    R.COM = ''
    CALL OPF(FN.COM,F.COM)
    FN.STE = 'FBNK.STMT.ENTRY'
    F.STE = ''
    R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.ENT = 'FBNK.ACCT.ENT.TODAY'
    F.ENT = ''
    R.ENT = ''
    CALL OPF(FN.ENT,F.ENT)
    FN.LWD = 'FBNK.ACCT.ENT.LWORK.DAY'
    F.LWD = ''
    R.LWD = ''
    CALL OPF(FN.LWD,F.LWD)
    FN.STP = 'FBNK.STMT.PRINTED'
    F.STP = ''
    R.STP = ''
    CALL OPF(FN.STP,F.STP)
    FN.ACC = 'FBNK.ACCOUNT'
    F.ACC = ''
    R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.CUR = 'FBNK.RE.BASE.CCY.PARAM'
    F.CUR = ''
    R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    RETURN
*===============================================
PROCESS:
*--------
    C.SEL  ="SELECT ":FN.COM
    C.SEL :=" WITH @ID EQ EG0010099"
    C.SEL :=" BY @ID"
    CALL EB.READLIST(C.SEL,K.LIST.COM,'',SELECTED.COM,ER.MSG.COM)
    IF SELECTED.COM THEN
        TOTAL.IN.DR = 0
        TOTAL.IN.CR = 0
        TOTAL.OUT.DR = 0
        TOTAL.OUT.CR = 0
        FOR CMM = 1 TO SELECTED.COM
            CALL F.READ(FN.COM,K.LIST.COM<CMM>,R.COM,F.COM,ER.COM)
            TOT.IN.DR  = 0
            TOT.IN.CR  = 0
            TOT.OUT.DR = 0
            TOT.OUT.CR = 0
            LLC = 0
            KK1 = 0
            COM.ID = K.LIST.COM<CMM>
            A.SEL  ="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 11500 11505 )"
            A.SEL :=" AND CO.CODE EQ ":COMP
            A.SEL :=" AND CURRENCY EQ 'EGP'"
            A.SEL :=" BY @ID"
            CALL EB.READLIST(A.SEL,K.LIST.AC,'',SELECTED.AC,ER.MSG.AC)
            LOOP
                REMOVE AC.ID FROM K.LIST.AC SETTING POS.AC
            WHILE AC.ID:POS.AC
                STP.ID = AC.ID :"-":TD1
                CALL F.READ(FN.ENT,AC.ID,R.ENT,F.ENT,ER.ENT)
                LOOP
                    REMOVE STE.ID FROM R.ENT SETTING POS.STE
                WHILE STE.ID:POS.STE
                    AMT.IN.DR  = 0
                    AMT.IN.CR  = 0
                    AMT.OUT.DR = 0
                    AMT.OUT.CR = 0
                    STE.TTYP   = ''
                    STE.CNF.DR = ''
                    STE.CNF.CR = ''
                    STE.CR.COMP = ''
                    STE.DR.COMP = ''
                    STE.VER = ''
                    STE.TCOD= ''
                    CN.FLAG = ''
                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                    STE.NRV2 = R.STE<AC.STE.NARRATIVE><1,2>
                    STE.ACC  = R.STE<AC.STE.ACCOUNT.NUMBER>
                    STE.CUR  = R.STE<AC.STE.CURRENCY>
                    STE.COM  = R.STE<AC.STE.COMPANY.CODE>
                    STE.REF  = R.STE<AC.STE.TRANS.REFERENCE>
                    STE.LCY  = R.STE<AC.STE.AMOUNT.LCY>
                    STE.FCY  = R.STE<AC.STE.AMOUNT.FCY>
                    STE.VLD  = R.STE<AC.STE.VALUE.DATE>
                    STE.STA  = R.STE<AC.STE.RECORD.STATUS>
                    STE.SID  = R.STE<AC.STE.SYSTEM.ID>
                    STE.DPT  = R.STE<AC.STE.DEPARTMENT.CODE>
                    STE.CRF  = R.STE<AC.STE.CRF.TYPE>
                    STE.STN  = R.STE<AC.STE.STMT.NO>
                    IF STE.NRV2[1,6] NE '994999' THEN
                        STE.NRV2 = ''
                    END
                    IF STE.REF[4] EQ '\BNK' THEN
                        STE.REF = FIELD(STE.REF,'\',1):'\B01'
                    END
                    FINDSTR '\B' IN STE.REF SETTING Y.XX THEN STE.REF = FIELD(STE.REF,'\',1) ; CN.FLAG = 'CONF' ELSE CN.FLAG = ''
*Line [ 215 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DXX = DCOUNT(STE.STN,@VM)
                    FOR DX1 = 1 TO DXX
                        SSTE = R.STE<AC.STE.STMT.NO,DX1>
                        IF SSTE[1,2] EQ 'EG' THEN
                            IF STE.NRV2 EQ '' THEN
                                DX1 = DXX
                            END ELSE
                                IF STE.NRV2[2] EQ SSTE[2] OR STE.NRV2[2] EQ COMP[2] THEN
                                    DX1 = DXX
                                END
                                IF STE.NRV2 EQ '99499900' AND SSTE[2] EQ '99' THEN
                                    DX1 = DXX
                                END
                            END
                        END
                    NEXT DX1
*******************************************************
                    IF STE.REF[1,2] EQ 'FT' THEN
                        CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.THEIR.REF,STE.REF,STE.CNF.DR)
                        CALL DBR('FUNDS.TRANSFER':@FM:FT.CREDIT.THEIR.REF,STE.REF,STE.CNF.CR)
                        CALL DBR('FUNDS.TRANSFER':@FM:FT.TRANSACTION.TYPE,STE.REF,STE.TTYP)
                        CALL DBR('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,STE.REF,STE.FTLR)
                        CALL DBR('FUNDS.TRANSFER':@FM:FT.CREDIT.COMP.CODE,STE.REF,STE.CR.COMP)
                        CALL DBR('FUNDS.TRANSFER':@FM:FT.DEBIT.COMP.CODE,STE.REF,STE.DR.COMP)
                        STE.VER = STE.FTLR<1,FTLR.VERSION.NAME>
                        IF STE.TTYP EQ '' THEN
                            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.THEIR.REF,STE.REF:';1',STE.CNF.DR)
                            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.CREDIT.THEIR.REF,STE.REF:';1',STE.CNF.CR)
                            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.TRANSACTION.TYPE,STE.REF:';1',STE.TTYP)
                            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.LOCAL.REF,STE.REF:';1',STE.FTLR)
                            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.CREDIT.COMP.CODE,STE.REF:';1',STE.CR.COMP)
                            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.DEBIT.COMP.CODE,STE.REF:';1',STE.DR.COMP)
                            STE.VER = STE.FTLR<1,FTLR.VERSION.NAME>
                        END
*                        IF STE.NRV2 EQ '' THEN
*                            IF STE.LCY LT 0 THEN
*                                SSTE = STE.DR.COMP
*                            END ELSE
*                                SSTE = STE.CR.COMP
*                            END
*                        END
                    END
                    IF STE.REF[1,2] EQ 'TT' THEN
                        CALL DBR('TELLER':@FM:TT.TE.NARRATIVE.1,STE.REF,STE.CNF.DR)
                        CALL DBR('TELLER':@FM:TT.TE.NARRATIVE.1,STE.REF,STE.CNF.CR)
                        CALL DBR('TELLER':@FM:TT.TE.TRANSACTION.CODE,STE.REF,STE.TCOD)
                        IF STE.TCOD EQ '' THEN
                            CALL DBR('TELLER$HIS':@FM:TT.TE.NARRATIVE.1,STE.REF:';1',STE.CNF.DR)
                            CALL DBR('TELLER$HIS':@FM:TT.TE.NARRATIVE.1,STE.REF:';1',STE.CNF.CR)
                            CALL DBR('TELLER$HIS':@FM:TT.TE.TRANSACTION.CODE,STE.REF:';1',STE.TCOD)
                        END
                    END
                    IF STE.REF[1,2] EQ 'BR' THEN
                        CALL DBR('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,STE.REF,STE.BR.LCL)
                        STE.BR.BANK = STE.BR.LCL<1,BRLR.BANK>
                    END ELSE
                        STE.BR.BANK = ''
                    END
                    IF STE.TTYP EQ 'AC08' OR STE.TTYP EQ 'AC09' OR STE.TTYP EQ 'AC11' OR STE.TTYP EQ 'IC02' THEN
                        IF STE.NRV2[2] EQ '00' OR STE.NRV2[2] EQ COMP[2] OR STE.NRV2[2] EQ '' THEN
                            SSTE = 'EG0010088'
                        END
                    END
                    IF STE.REF[1,2] EQ 'BR' AND STE.LCY LT 0 AND STE.BR.BANK NE '9902' AND STE.BR.BANK NE '9901' THEN
                        IF STE.NRV2[2] EQ '00' OR STE.NRV2[2] EQ COMP[2] OR STE.NRV2[2] EQ '' THEN
                            SSTE = 'EG0010088'
                        END
                    END
                    IF STE.REF[1,3] EQ 'SCB' AND STE.SID EQ 'CQ' THEN
                        SSTE = 'EG0010088'
                    END
                    IF CN.FLAG EQ 'CONF' AND SSTE[2] NE '99' THEN
                        STE.CNF.DR = 'CONFIRMATION'
                        STE.CNF.CR = 'CONFIRMATION'
                    END
                    IF STE.VER NE ',SCB.CONFIRM' AND STE.VER NE ',SCB.CHQ.1' AND STE.VER NE ',SCB.CRT1' AND COM.ID NE 'EG0010099' THEN
*                        IF STE.SID EQ 'LC' OR STE.SID EQ 'LCD' OR STE.SID EQ 'LCM' THEN
*                            STE.CNF.DR = 'CONFIRMATION'
*                            STE.CNF.CR = 'CONFIRMATION'
*                        END
                        IF STE.REF[1,3] EQ 'SCB' AND STE.SID EQ 'CQ' THEN
                            STE.CNF.DR = 'CONFIRMATION'
                            STE.CNF.CR = 'CONFIRMATION'
                        END
                        IF STE.REF[1,2] EQ 'BR' AND STE.SID EQ 'CQ' AND STE.CRF EQ 'CREDIT' THEN
                            IF SSTE[2] NE '99' THEN
                                STE.CNF.DR = 'CONFIRMATION'
                                STE.CNF.CR = 'CONFIRMATION'
                            END
                        END
                        IF SSTE[2] EQ '88' AND STE.LCY LE 0 THEN
                            STE.CNF.DR = 'CONFIRMATION'
                            STE.CNF.CR = 'CONFIRMATION'
                        END
                    END
*****************************************************
                    IF SSTE EQ COM.ID THEN
                        IF STE.STA NE 'REVE' THEN
                            IF STE.COM NE SSTE THEN
*****************************************************
                                K++
                                KK1++
                                LLC = STE.LCY
*>>>>>>>>>>>>>>>>>>>>>>>>
                                IF KK1 = 1 AND LLC NE 0 THEN
                                    GOSUB PRINTBR
                                END
*>>>>>>>>>>>>>>>>>>>>>>>>
                                IF STE.LCY LE 0 THEN
*                                    IF STE.CNF.DR EQ 'CONFIRMATION' THEN
*                                        AMT.IN.DR   = STE.LCY
*                                        TOT.IN.DR  += AMT.IN.DR
*                                        CCF = 'CONFIRMATION'
*                                    END ELSE
                                    AMT.OUT.DR  = STE.LCY
                                    TOT.OUT.DR += AMT.OUT.DR
                                    CCF = ''
*                                    END
                                END ELSE
*                                    IF STE.CNF.CR EQ 'CONFIRMATION' THEN
*                                        AMT.IN.CR   = STE.LCY
*                                        TOT.IN.CR  += AMT.IN.CR
*                                        CCF = 'CONFIRMATION'
*                                    END ELSE
                                    AMT.OUT.CR  = STE.LCY
                                    TOT.OUT.CR += AMT.OUT.CR
                                    CCF = ''
*                                    END
                                END
*****************************************************
*>>>>>>>>>>>>>>>>>>>>>>>>
                                GOSUB PRINTLN
*>>>>>>>>>>>>>>>>>>>>>>>>
                            END
                        END
                    END
                REPEAT
            REPEAT
*>>>>>>>>>>>>>>>>>>>>>>>>
            IF LLC NE 0 THEN
                GOSUB PRINTTOT
            END
*>>>>>>>>>>>>>>>>>>>>>>>>
        NEXT CMM
    END
*>>>>>>>>>>>>>>>>>>>>>>>>
*    GOSUB PRINTTOTAL
*>>>>>>>>>>>>>>>>>>>>>>>>
    RETURN
*===============================================
*...................................
PRINTBR:
    XX1 = SPACE(120)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COM.ID,COM.NAME)
    XX1<1,K> = COM.NAME
    PRINT XX1<1,K>[1,50]
    RETURN
*...................................
PRINTLN:
    XX  = SPACE(120)
    XX<1,K>[1,15]   = FMT(STE.VLD,"####/##/##")
    XX<1,K>[20,15]  = STE.REF
    XX<1,K>[40,5]   = STE.CUR
    XX<1,K>[45,15]  = FMT(AMT.IN.DR,"L2,")
    XX<1,K>[60,15]  = FMT(AMT.IN.CR,"L2,")
    XX<1,K>[75,15]  = FMT(AMT.OUT.DR,"L2,")
    XX<1,K>[90,15]  = FMT(AMT.OUT.CR,"L2,")
    XX<1,K>[105,15] = FMT(STE.FCY,"L2,")
    PRINT XX<1,K>
    STE.REF = ''
    STE.CUR = ''
    STE.TTYP = ''
    STE.CNF.DR = ''
    STE.CNF.CR = ''
    STE.VER = ''
    AMT.IN.DR  = 0
    AMT.IN.CR  = 0
    AMT.OUT.DR = 0
    AMT.OUT.CR = 0
    STE.FCY = 0
    RETURN
*...................................
PRINTTOT:
    PRINT STR('-',120)
    XX2 = SPACE(120)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COM.ID,COM.NAME)
    XX2<1,K>[1,20]  = '������ ':COM.NAME
    XX2<1,K>[45,15] = FMT(TOT.IN.DR,"L2,")
    XX2<1,K>[60,15] = FMT(TOT.IN.CR,"L2,")
    XX2<1,K>[75,15] = FMT(TOT.OUT.DR,"L2,")
    XX2<1,K>[90,15] = FMT(TOT.OUT.CR,"L2,")
    PRINT XX2<1,K>
    TOTAL.IN.DR += TOT.IN.DR
    TOTAL.IN.CR += TOT.IN.CR
    TOTAL.OUT.DR += TOT.OUT.DR
    TOTAL.OUT.CR += TOT.OUT.CR
    PRINT STR('-',120)
    KK1 = 0
*--------------------------------------------------------
    XX.DATA  = "31":"|"
    XX.DATA := TD1:"|"
    XX.DATA := TOT.IN.DR:"|"
    XX.DATA := TOT.IN.CR:"|"
*--------------------------------------------------------
    XX1.DATA  = "31":"|"
    XX1.DATA := TD1:"|"
    XX1.DATA := TOT.OUT.DR:"|"
    XX1.DATA := TOT.OUT.CR:"|"
*--------------------------------------------------------
    WRITESEQ XX.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*--------------------------------------------------------
    WRITESEQ XX1.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*...........................................
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    TD = TD1
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH
    T.DAY1  = FMT(TD,"####/##/##")
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":SPACE(50):"���� ������ ������ ������� ����� (�) �� ����� ":T.DAY1:SPACE(10):"(���� �����)"
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�����":SPACE(45):"������� �����":SPACE(15):"������� �����"
    PR.HD :="'L'":"�������":SPACE(10):"��� �������":SPACE(5):'������':SPACE(5):"����":SPACE(10):"����":SPACE(10):"����":SPACE(10):"����":SPACE(10):"������ �������"
    PR.HD :="'L'":STR('_',120)
**    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
