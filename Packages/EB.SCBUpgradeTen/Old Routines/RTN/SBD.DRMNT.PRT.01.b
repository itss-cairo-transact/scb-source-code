* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-11</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.DRMNT.PRT.01
***    PROGRAM    SBD.DRMNT.PRT.01
*   ------------------------------
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.POS.TODAY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DRMNT.FILE
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*================================================================
*                                                  ����� �� ��������
*               ����� ������� ���� �� ����� �������� �� ��� �����
*                             ����� ������� ���� �� ����� �������
*                                 ���� ��� ������ ����� �������
*=================================================================
    GOSUB INIT
    WS.COMP = ID.COMPANY
***    WS.COMP = "EG0010001"
*-------------------------
    GOSUB OPENFILES
    GOSUB CLEAR.FILE.DRMNT.FILE
*-------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01"  THEN

        REPORT.ID = 'P.FUNCTION'
        CALL PRINTER.ON(REPORT.ID,'')
        GOSUB PRINT.HEAD
        GOSUB READ.CUST.POS.FILE
    END
*-------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01"  THEN
        XX = SPACE(120)
        PRINT XX<1,1>
        XX = STR('_',120)
        PRINT XX<1,1>
        XX = SPACE(120)
        PRINT XX<1,1>
        XX<1,1>[1,50]   = "**** ������ ��� �������  ****"
        XX<1,1>[70,20]  = FMT(NO.DRMNT.CUST,"R0,")
        PRINT XX<1,1>
        XX = SPACE(120)
        PRINT XX<1,1>
        PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)

        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
*-----------------------------------------------------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10"  THEN
*-------------------------------
        GOSUB OPENFILES
*-------------------------------
        GOSUB READ.CUST.POS.FILE
*-------------------------------
    END
    RETURN
*-----------------------------------------------------------------------------
CLEAR.FILE.DRMNT.FILE:
    WS.START = "P.":WS.COMP:"..."
****    NN.SEL = "SELECT F.SCB.DRMNT.FILE WITH COMPANY.CODE EQ ":WS.COMP
    NN.SEL = "SELECT F.SCB.DRMNT.FILE WITH @ID LIKE ":WS.START
    CALL EB.READLIST(NN.SEL,KEY.LIST,"",SELECTED.NN,ER.MSG.NN)
    IF SELECTED.NN THEN
        FOR NN = 1 TO SELECTED.NN
            CALL F.READ(FN.DR,KEY.LIST<NN>, R.DR, F.DR, E.DR)
**DELETE F.DR , KEY.LIST<NN>
            CALL F.DELETE (FN.DR,KEY.LIST<NN>)
        NEXT NN
    END
    RETURN
*-------------------------------------------------------------------------
INIT:
    FN.DR = "F.SCB.DRMNT.FILE"
    F.DR = ""

    CUST.ARRAY = ""
    DATE.ARRAY = ""
    NN         = 0

    PROGRAM.ID    = "SBD.DRMNT.PRT.01"
    NO.DRMNT.CUST = 0

    FN.CUS.POS    = "F.SCB.CUST.POS.TODAY"
    F.CUS.POS     = ""
    R.CUS.POS     = ""
    Y.CUS.POS.ID  = ""

    FN.CUS.ACC    = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC     = ""
    R.CUS.ACC     = ""
    Y.CUS.ACC.ID  = ""

    FN.CUSTOMER   = "FBNK.CUSTOMER"
    F.CUSTOMER    = ""
    R.CUSTOMER    = ""
    Y.CUST.ID     = ""

    FN.LMM.CUST   = "FBNK.LMM.CUSTOMER"
    F.LMM.CUST    = ""
    R.LMM.CUST    = ""
    Y.LMM.CUST.ID = ""


    FN.ACC    = "FBNK.ACCOUNT"
    F.ACC     = ""
    R.ACCOUNT = ""
    Y.ACC.ID  = ""

    FN.POST   = "FBNK.POSTING.RESTRICT"
    F.POST    = ""
    R.POST    = ""
    Y.POST.ID = ""

    FN.LD   = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD    = ""
    R.LD    = ""
    Y.LD.ID = ""

*    FN.LC   = "FBNK.LETTER.OF.CREDIT"
*    F.LC    = ""
*    R.LC    = ""
*    Y.LC.ID = ""

*MSABRY TMP
    FN.LC  = "FBNK.LC.APPLICANT"   ; F.LC = ""
    FN.LCC = "FBNK.LETTER.OF.CREDIT"   ; F.LCC = ""

    FN.LCB  = "FBNK.LC.BENEFICIARY"   ; F.LCB = ""


    OLD.CUST  = 0
    CUST.NAME = ''
    NO.OF.LD  = ''
    NO.OF.LC  = ''
*---------------------------------------
    SYS.DATE = TODAY
    SYS.YYMM = SYS.DATE[1,6]
    WRK.DATE = SYS.DATE
*---------------------------------------
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]
*---------------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
        WRK.MM   = WRK.MM + 2

        IF WRK.MM GT 12 THEN
            WRK.YY = WRK.YY + 1
            WRK.MM = WRK.MM - 12
        END

        WRK.DD   = 01
        WRK.MM = FMT(WRK.MM,"R%2")
        WRK.DD = FMT(WRK.DD,"R%2")
        WRK.DATE = WRK.YY:WRK.MM:WRK.DD

        CALL CDT('',WRK.DATE,'-1C')
        WRK.YY   = WRK.DATE[1,4]
        WRK.MM   = WRK.DATE[5,2]
        WRK.DD   = WRK.DATE[7,2]
    END
*---------------------------------------
    OLD1.YY   = WRK.DATE[1,4] - 1
    OLD1.MM   = WRK.DATE[5,2]
    OLD1.DD   = WRK.DATE[7,2]
    OLD1.DATE = OLD1.YY:OLD1.MM:OLD1.DD
    OLD1.YYMM = OLD1.DATE[1,6]
*---------------------------------------
    OLD3.YY   = WRK.DATE[1,4] - 3
    OLD3.MM   = WRK.DATE[5,2]
    OLD3.DD   = WRK.DATE[7,2]
    OLD3.DATE = OLD3.YY:OLD3.MM:OLD3.DD
    OLD3.YYMM = OLD3.DATE[1,6]
*---------------------------------------
    P.DATE   = FMT(SYS.DATE,"####/##/##")
*---------------------------------------
    DIM ARY.X(12)
    ARY.X(1)  = "������"
    ARY.X(2)  = "������"
    ARY.X(3)  = "������"
    ARY.X(4)  = "������"
    ARY.X(5)  = "������"
    ARY.X(6)  = "������"
    ARY.X(7)  = "������"
    ARY.X(8)  = "������"
    ARY.X(9)  = "������"
    ARY.X(10) = "������"
    ARY.X(11) = "������"
    ARY.X(12) = "������"
    MON       = ARY.X(WRK.MM)
*-----------------------
    LINE.NO             = 0
    MAX.LINE.NO         = 30
    WS.POSTING.RESTRICT = 90
    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL OPF(FN.LMM.CUST,F.LMM.CUST)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.LCB,F.LCB)

    CALL OPF(FN.LCC,F.LCC)

    CALL OPF(FN.DR,F.DR)

    RETURN
*--------------------------------------------------------------------------
READ.CUST.POS.FILE:
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
        SEL.CUS.POS = "SELECT ":FN.CUS.POS:" WITH"
        SEL.CUS.POS :=" @ID LIKE ...":SYS.DATE
        SEL.CUS.POS :=" AND CO.CODE EQ ":WS.COMP
        SEL.CUS.POS :=" BY @ID"
    END
    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10" THEN
        SEL.CUS.POS = "SELECT ":FN.CUS.POS:" WITH"
        SEL.CUS.POS :=" @ID LIKE ...":SYS.DATE
        SEL.CUS.POS :=" AND CO.CODE EQ ":WS.COMP
        SEL.CUS.POS :=" BY @ID"
    END

    CALL EB.READLIST(SEL.CUS.POS,SEL.LIST.CUS.POS,'',NO.OF.CUS.POS,ERR.CUS.POS)
    LOOP
        REMOVE Y.CUS.POS.ID FROM SEL.LIST.CUS.POS SETTING POS.CUS.POS
    WHILE Y.CUS.POS.ID:POS.CUS.POS
        CALL F.READ(FN.CUS.POS,Y.CUS.POS.ID,R.CUS.POS,F.CUS.POS,ERR.CUS.POS)
        Y.CUST.ID = FIELD(Y.CUS.POS.ID,"-",1)
        PRINT.CUST.NAME = 0

        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
        OLD.DRMNT.CODE = LOCAL.REF<1,CULR.DRMNT.CODE>
        OLD.DRMNT.DATE = LOCAL.REF<1,CULR.DRMNT.DATE>

        CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,Y.CUST.ID,PO.CD)
        OLD.POST.RES = PO.CD
*------------------------------
        IF OLD.DRMNT.CODE GT 0 THEN
            GO TO NEXT.CUSTOMER
        END
*------------------------------
        IF OLD.POST.RES EQ 18 THEN
            GO TO NEXT.CUSTOMER
        END
*------------------------------
        CU.POST.NAME = ''

        CALL DBR ('POSTING.RESTRICT':@FM: AC.POS.DESCRIPTION,OLD.POST.RES,PO.DESCR)

        CU.POST.NAME = PO.DESCR

        IF OLD.POST.RES GT 0 THEN
            GO TO NEXT.CUSTOMER
        END
*------------------------------
        GOSUB READ.LD.FILE

        IF NO.OF.LD GT 0 THEN
            GO TO NEXT.CUSTOMER
        END
*------------------------------
        GOSUB READ.LC.FILE

        IF NO.OF.LC GT 0 THEN
            GO TO NEXT.CUSTOMER
        END
*------------------------------
        DRMNT.FLAG   = 'Y'
        Y.CUS.ACC.ID = Y.CUST.ID
        TOT.BAL      = 0

        GOSUB READ.CUSTOMER.ACCOUNT

        IF DRMNT.CURR EQ '' THEN
            IF DRMNT.SAVE EQ '' THEN
                GO TO NEXT.CUSTOMER
            END
        END

        IF DRMNT.FLAG = 'N' THEN
            GO TO NEXT.CUSTOMER
        END

        Y.CUS.ACC.ID   = Y.CUST.ID
        NO.DRMNT.CUST += 1
* -----------------------
        IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10" THEN
            CALL SCB.DRMNT.OFS(Y.CUST.ID)
        END
* -----------------------
        IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
*---- WRITE TO DRMNT FILE

            DRMNT.ID = ""
            DRMNT.ID = "P.":WS.COMP:".":Y.CUST.ID

            R.DR<DRMNT.CUSTOMER.NO>     =  Y.CUST.ID
            R.DR<DRMNT.LAST.TRANS.DATE> =  HIGH.DATE
            R.DR<DRMNT.COMPANY.CODE>    =  WS.COMP
            R.DR<DRMNT.RUN.DATE>        =  TODAY
            R.DR<DRMNT.PRINT.UPDATE>    = "P"
            R.DR<DRMNT.FLAG.RUN>        =  "NO"
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****            WRITE R.DR TO F.DR , DRMNT.ID ON ERROR
****            END
            CALL F.WRITE(FN.DR,DRMNT.ID,R.DR)
            CALL JOURNAL.UPDATE(DRMNT.ID)
****END OF UPDATE 24/3/2016*****************************
            GOSUB READ.CUSTOMER.ACCOUNT.2
        END
* -----------------------
NEXT.CUSTOMER:
    REPEAT
    RETURN
*--------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT:
*-------------------          ������ ������
    NO.OF.ACC = ''
    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)
    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)
    IF NO.OF.ACC EQ 0 THEN
        DRMNT.FLAG = 'N'
        RETURN
    END
    DRMNT.CURR = ''
    DRMNT.SAVE = ''
    DRMNT.OTHR = ''
    HIGH.DATE  = ''
*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>
        Y.CUST.ID = Y.CUS.ACC.ID
*---------------
        GOSUB READ.ACCOUNT.FILE
*---------------
    NEXT Z
LAST.CUSTOMER:
    RETURN
*-------------------------------------------------------------------------
READ.ACCOUNT.FILE:

    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACCOUNT,F.ACC,ERR.ACC)

    Y.CATEG.ID      = R.ACCOUNT<AC.CATEGORY>
*-------------------
    OPENING.DATE    = R.ACCOUNT<AC.OPENING.DATE>
    CUST.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.CUST>
    CUST.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.CUST>
    W.BAL           = R.ACCOUNT<AC.WORKING.BALANCE>
    WS.CY           = R.ACCOUNT<AC.CURRENCY>
    AC.POST.RES     = R.ACCOUNT<AC.POSTING.RESTRICT>
*-------------------
    TOT.BAL = TOT.BAL + W.BAL
*-------------------
    AC.POST.NAME = ''

    CALL DBR ('POSTING.RESTRICT':@FM: AC.POS.DESCRIPTION,AC.POST.RES,PO.DESCR)

    AC.POST.NAME = PO.DESCR
*-------------------
    MORE.SURE = 'N'

    BEGIN CASE

    CASE Y.CATEG.ID EQ 1001
        DRMNT.CURR = 'Y'

    CASE Y.CATEG.ID EQ 1019
        DRMNT.CURR = 'Y'

    CASE Y.CATEG.ID GE 6500  AND  Y.CATEG.ID LE 6599
        DRMNT.SAVE = 'Y'

    CASE Y.CATEG.ID NE 0
        DRMNT.OTHR = 'Y'

    END CASE
*------------------------------------
    IF DRMNT.OTHR EQ 'Y'  THEN
        DRMNT.FLAG = 'N'
    END
*------------------------------------
    LAST.DATE = ''

    IF CUST.DATE.CR EQ ''   THEN
        IF CUST.DATE.DR EQ ''   THEN

            LAST.DATE = OPENING.DATE
        END
    END
*------------------------------------
    IF CUST.DATE.CR GT LAST.DATE THEN
        LAST.DATE = CUST.DATE.CR
    END

    IF CUST.DATE.DR GT LAST.DATE THEN
        LAST.DATE   = CUST.DATE.DR
    END
*-------------------------------
    IF LAST.DATE GT HIGH.DATE THEN
        HIGH.DATE   = LAST.DATE
    END
*-------------------------------
    LAST.YYMM = LAST.DATE[1,6]



*-------------------------------
    WS.OLD1.YY = OLD1.DATE[1,4]
    WS.OLD1.MM = OLD1.DATE[5,2]
    WS.OLD1.DD = OLD1.DATE[7,2]
    WS.OLD1.YYMM = WS.OLD1.YY:WS.OLD1.MM
    WS.OLD1.DATE = WS.OLD1.YY:WS.OLD1.MM:WS.OLD1.DD
*---------------------------------------------
*                                                            ������ �������� �������
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
        IF DRMNT.CURR EQ 'Y' THEN
***        IF LAST.DATE GT WS.OLD1.DATE THEN
            IF LAST.YYMM GT WS.OLD1.YYMM THEN
                DRMNT.FLAG = 'N'
            END
        END
    END
*** ---------------------------------

    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.01" THEN
        IF DRMNT.CURR EQ 'Y' THEN
            IF LAST.DATE GT WS.OLD1.DATE THEN
***            IF LAST.YYMM GT WS.OLD1.YYMM THEN
                DRMNT.FLAG = 'N'
            END
        END
    END
*---------------------------------------------
*-----------                         �� ���� ������ ������� �� ��������� ����� �� 3 �����

    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
        IF DRMNT.SAVE EQ 'Y' THEN
***        IF LAST.DATE GT OLD3.DATE THEN
            IF LAST.YYMM GT OLD3.YYMM THEN
                DRMNT.FLAG = 'N'
            END
        END
    END

***--------------------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10" THEN
        IF DRMNT.SAVE EQ 'Y' THEN
            IF LAST.DATE GT OLD3.DATE THEN
***            IF LAST.YYMM GT OLD3.YYMM THEN
                DRMNT.FLAG = 'N'
            END
        END
    END

***-------------------------- ---------------------------------------------------
***                  �� ���� �������� �� ������� ����� ������ �� �������� ������

    IF NO.OF.DEPOSIT GT 0  THEN
        IF      DRMNT.FLAG = 'Y'   THEN
            DRMNT.FLAG = 'N'
        END
    END
*-----------   �� ���� �� ������ ���� ���� ��� ������ ���� ���� ���� ���� ������ �������


    IF MORE.SURE EQ 'Y' THEN
        DRMNT.FLAG = 'Y'
    END
*-----------
    RETURN
*-------------------------------------------------------------------------
READ.LD.FILE:
***********************

*-------------------
    NO.OF.DEPOSIT = ''

*-------------------               + �������� +   �������

    CALL F.READ(FN.LMM.CUST, Y.CUST.ID, R.LMM.CUST, F.LMM.CUST, E1)


******    LG.CATEG = ''
******    LG.FIELD = R.CUS.POS<CUST.LD>
******    DLG = DCOUNT(LG.FIELD,VM)

    IF E1 EQ '' THEN

        NO.OF.DEPOSIT += 1

    END



***********************
*-------------------
    NO.OF.LD = ''

*-------------------         ������ ������
    LG.CATEG = ''
    LG.FIELD = R.CUS.POS<CUST.LG>
*Line [ 556 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DLG = DCOUNT(LG.FIELD,@VM)

    IF DLG GT 0 THEN
        FOR X = 1 TO DLG

******            LG.ID  = R.CUS.POS<CUST.LG,X>
******            CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,ER.LD)
******            LG.CATEG = R.LD<LD.CATEGORY>
******            LG.DATE.D  = R.LD<LD.FIN.MAT.DATE>
******            LG.AMT   = R.LD<LD.AMOUNT>

            NO.OF.LD += 1

        NEXT X
    END

*-------------------         �������� �������
    LG.CATEG = ''
    LG.FIELD = R.CUS.POS<CUST.LC.IMP>
*Line [ 576 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DLG = DCOUNT(LG.FIELD,@VM)

    IF DLG GT 0 THEN
        FOR X = 1 TO DLG

*****            LG.ID  = R.CUS.POS<CUST.LC.IMP,X>
*****            CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,ER.LD)
*****            LG.CATEG = R.LD<LD.CATEGORY>
*****            LG.DATE.D  = R.LD<LD.FIN.MAT.DATE>
*****            LG.AMT   = R.LD<LD.AMOUNT>

            NO.OF.LD += 1

        NEXT X
    END

*-------------------         �������� �����
    LG.CATEG = ''
    LG.FIELD = R.CUS.POS<CUST.LC.EXP>
*Line [ 596 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DLG = DCOUNT(LG.FIELD,@VM)

    IF DLG GT 0 THEN
        FOR X = 1 TO DLG

*****            LG.ID  = R.CUS.POS<CUST.LC.EXP,X>
*****            CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,ER.LD)
*****            LG.CATEG = R.LD<LD.CATEGORY>
*****            LG.DATE.D  = R.LD<LD.FIN.MAT.DATE>
*****            LG.AMT   = R.LD<LD.AMOUNT>

            NO.OF.LD += 1

        NEXT X
    END


    RETURN
*---------------------------*LC *------------------------
READ.LC.FILE:
*    NO.OF.LC = ''
*    SEL.LC  = "SELECT ":FN.LC:" WITH CON.CUS.LINK EQ ":Y.CUST.ID
*    SEL.LC := " AND LIABILITY.AMT GT 0"
*    SEL.LC := " AND EXPIRY.DATE GT ":SYS.DATE
*    CALL EB.READLIST(SEL.LC,SEL.LIST.LC,'',NO.OF.LC,ERR.LC)

    NO.OF.LC = ''

*MSABRY TMP
    CALL F.READ(FN.LC,Y.CUST.ID,R.LC,F.LC,ER.LC )
    LOOP
        REMOVE LC.ID FROM R.LC SETTING POS.LC
    WHILE LC.ID:POS.LC
        CALL F.READ(FN.LCC,LC.ID,R.LCC,F.LCC,ETEXT.LD)
        WS.LCC.CHK =    R.LCC<TF.LC.OPERATION>
        WS.LC.AMT  =    R.LCC<TF.LC.LIABILITY.AMT>
        WS.LC.EX.AMT =  R.LCC<TF.LC.EXPIRY.DATE>
        IF WS.LCC.CHK NE "" THEN
            IF WS.LC.AMT GT 0 THEN
                IF WS.LC.EX.AMT GT SYS.DATE THEN
                    NO.OF.LC += 1
                END
            END
        END

    REPEAT


*MSABRY TMP
    CALL F.READ(FN.LCB,Y.CUST.ID,R.LCB,F.LCB,ER.LCB )
    LOOP
        REMOVE LCB.ID FROM R.LCB SETTING POS.LCB
    WHILE LCB.ID:POS.LCB
        CALL F.READ(FN.LCC,LCB.ID,R.LCC,F.LCC,ETEXT.LD)
        WS.LCC.CHK =    R.LCC<TF.LC.OPERATION>
        WS.LC.AMT  =    R.LCC<TF.LC.LIABILITY.AMT>
        WS.LC.EX.AMT =  R.LCC<TF.LC.EXPIRY.DATE>
        IF WS.LCC.CHK NE "" THEN
            IF WS.LC.AMT GT 0 THEN
                IF WS.LC.EX.AMT GT SYS.DATE THEN
                    NO.OF.LC += 1
                END
            END
        END

    REPEAT


    RETURN
*--------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT.2:

    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)

    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)

*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>
        Y.CUST.ID = Y.CUS.ACC.ID


*---------------
        GOSUB READ.ACCOUNT.FILE.2
*---------------
    NEXT Z


    GOSUB READ.LD.FILE.2





****************
    RETURN
*-------------------------------------------------------------------------
READ.ACCOUNT.FILE.2:
    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACCOUNT,F.ACC,ERR.ACC)

    Y.CATEG.ID   = R.ACCOUNT<AC.CATEGORY>
*-----------
    OPENING.DATE    = R.ACCOUNT<AC.OPENING.DATE>

    CUST.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.CUST>
    CUST.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.CUST>
    BANK.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.BANK>
    BANK.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.BANK>
    AUTO.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.AUTO>
    AUTO.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.AUTO>

    W.BAL           = R.ACCOUNT<AC.WORKING.BALANCE>
    WS.CY           = R.ACCOUNT<AC.CURRENCY>
    AC.POST.RES     = R.ACCOUNT<AC.POSTING.RESTRICT>

*------------------------------------
    LAST.DATE = ''

    IF CUST.DATE.CR EQ ''   THEN
        IF CUST.DATE.DR EQ ''   THEN

            LAST.DATE = OPENING.DATE
        END
    END
*------------------------------------
    IF CUST.DATE.CR GT LAST.DATE THEN
        LAST.DATE = CUST.DATE.CR
    END

    IF CUST.DATE.DR GT LAST.DATE THEN
        LAST.DATE   = CUST.DATE.DR
    END
*-----------
    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,Y.CATEG.ID,S.NAME)
    CATEG.NAME = S.NAME

    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,WS.CY,CY.NAME)
    CURR.NAME = CY.NAME


    AC.POST.NAME = ''
    CALL DBR ('POSTING.RESTRICT':@FM: AC.POS.DESCRIPTION,AC.POST.RES,PO.DESCR)
    AC.POST.NAME = PO.DESCR
*-----------
    GOSUB PRINT.REPORT
*-------------------------------
    RETURN
*-------------------------------------------------------------------------
PRINT.REPORT:
    IF PRINT.CUST.NAME EQ 0 THEN
        GOSUB WRITE.ARY.1
    END

    GOSUB WRITE.ARY.2

    RETURN
*------------------------------------------------------------------------
WRITE.ARY.1:

    IF LINE.NO GT MAX.LINE.NO THEN
        GOSUB PRINT.HEAD
    END

    XX = STR('_',120)
    PRINT XX<1,1>

    XX = SPACE(120)
*------------
    IF DRMNT.FLAG EQ 'Y' THEN
        DRMNT.FLAG = 'YES'
    END
    IF DRMNT.FLAG EQ 'N' THEN
        DRMNT.FLAG = 'NO'
    END
*------------
    XX<1,1>[1,10]   = Y.CUST.ID
    XX<1,1>[12,40]  = CUST.NAME
    XX<1,1>[55,17]  = CU.POST.NAME

    PRINT XX<1,1>

    LINE.NO = +1

    PRINT.CUST.NAME = 1

    RETURN
*------------------------------------------------------------------------
WRITE.ARY.2:
    IF LINE.NO GT MAX.LINE.NO THEN
        GOSUB PRINT.HEAD
    END
    XX = SPACE(120)
    XX<1,1>[1,20]   = Y.ACC.ID
    XX<1,1>[33,6]   = Y.CATEG.ID
    XX<1,1>[40,12]  = CATEG.NAME
    XX<1,1>[55,17]  = AC.POST.NAME
    XX<1,1>[75,3]   = WS.CY
    XX<1,1>[80,12]  = CURR.NAME
    XX<1,1>[95,10]  = FMT(LAST.DATE,"####/##/##")
    XX<1,1>[110,20] = FMT(W.BAL,"R2,")

    PRINT XX<1,1>

    LINE.NO = +1

    RETURN
*------------------------------------------------------------------------
READ.LD.FILE.2:
    NO.OF.LD = ''
    CALL F.READ(FN.LMM.CUST, Y.CUST.ID, R.LMM.CUST, F.LMM.CUST, E1)

    LOOP

        REMOVE LDD FROM R.LMM.CUST  SETTING POS111
    WHILE LDD:POS111
        CALL F.READ(FN.LD,LDD, R.LD, F.LD,E)

        LD.CONT.NO = LDD
        LD.CATEG   = R.LD<LD.CATEGORY>
        LD.CURR    = R.LD<LD.CURRENCY>
        LD.AMT     = R.LD<LD.AMOUNT>
        LD.LAST.D  = R.LD<LD.FIN.MAT.DATE>

        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,LD.CATEG,S.NAME)
        CATEG.NAME = S.NAME

        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LD.CURR,CY.NAME)
        CURR.NAME = CY.NAME

        XX = SPACE(120)
        XX<1,1>[1,20]   = LD.CONT.NO
        XX<1,1>[33,6]   = LD.CATEG
        XX<1,1>[40,12]  = CATEG.NAME
        XX<1,1>[75,3]   = LD.CURR
        XX<1,1>[80,12]  = CURR.NAME
        XX<1,1>[95,10]  = FMT(LD.LAST.D,"####/##/##")
        XX<1,1>[110,20] = FMT(LD.AMT,"R2,")
        PRINT XX<1,1>

        LINE.NO = +1
    REPEAT
    RETURN
*--------------------------------------------------------------------------
PRINT.HEAD:
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
    YYBRN = BRANCH

    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):PROGRAM.ID
    PR.HD :="'L'":SPACE(35):" ����� ������� ������� ���� �� ����� ��� ��� ����� "
    PR.HD :="'L'":SPACE(40):"���� ����� ������ ���� ���� ���":" ":MON:" ":WRK.YY
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(05):" ��� ������ "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(25):" �������":SPACE(15):"������":SPACE(09):"������":SPACE(14):"��� ����":SPACE(06):"������"

    HEADING PR.HD
    LINE.NO = 0
    RETURN
*===============================================================
END
