* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>362</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.CREDIT.CARD.DELETED
*    PROGRAM SBD.CREDIT.CARD.DELETED

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS


    GOSUB INITIATE
    CURR.COMP = ID.COMPANY
    YTEXT  = ' FROM DATE YYMMDD '
    CALL TXTINP(YTEXT, 8, 22, "6", "")
    FR.DATE = COMI
*  FR.DATE = "111001"
    FD.1=FR.DATE:"0000"
    YTEXT  = ' TO DATE YYMMDD '
    CALL TXTINP(YTEXT, 8, 22, "6", "")
    FI.DATE = COMI
*  FI.DATE = "111010"
    TD.1 =FI.DATE:"0000"

    ST.DATE = "20":FR.DATE[1,2]:"/":FR.DATE[3,2]:"/":FR.DATE[5,2]
    EN.DATE = "20":FI.DATE[1,2]:"/":FI.DATE[3,2]:"/":FI.DATE[5,2]

    GOSUB PRINT.HEAD

*Line [ 60 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.CREDIT.CARD.DELETED'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*=============================================================
CALLDB:

    FN.CI = 'F.CARD.ISSUE$HIS'           ; F.CI = ''    ;R.CI = ''
    FN.CA = 'F.CARD.ISSUE'               ; F.CA = ''    ;R.CA = ''
    FN.USR = 'F.USER'                    ; F.USR = ''   ;R.USR = ''

    BRANCH = ''
    PRV.BRAN = ''
    RENEW.CARD = ''   ; V.NO = 0 ; M.NO = 0 ; A.NO = 0
    CALL OPF(FN.CI,F.CI)
    CALL OPF(FN.CA,F.CA)
    CALL OPF(FN.USR,F.USR)
    TOT.TRIAL = 0

    T.SEL = "SELECT FBNK.CARD.ISSUE$HIS WITH RECORD.STATUS EQ REVE "
    T.SEL :="AND DATE.TIME GT ":FD.1:" AND DATE.TIME LT ":TD.1
    T.SEL :=" BY CO.CODE BY @ID BY DATE.TIME"
* TEXT = CURR.COMP ; CALL REM
    KEY.LIST =""  ; SELECTED =""  ;  E1="" ; E2=''
    KEY.LIST1=""  ; SELECTED1= ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CI,KEY.LIST<I>,R.CI,F.CI,E1)

            IF NOT(E1) THEN
*=================================================
                BRANCH = ''
                E.DATE.1= ''
*PRINT
* TOT.TRIAL = TOT.TRIAL + 1

                BRANCH.NO = R.CI<CARD.IS.CO.CODE>
                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,R.CI<CARD.IS.CO.CODE>,BRANCH)
                YYBRN = FIELD(BRANCH,'.',1)
                R.CA = ''
                T.SEL = "SELECT FBNK.CARD.ISSUE WITH ACCOUNT EQ ":R.CI<CARD.IS.ACCOUNT>:" BY DATE.TIME"
                CALL EB.READLIST(T.SEL,KEY.LIST1,"",SELECTED1,ER.MSG1)
                CALL F.READ(FN.CA,KEY.LIST1<SELECTED1>,R.CA,F.CA,E1)

*  IF KEY.LIST1<SELECTED1>[6,16] GT '4000000000000000' THEN
                IF R.CA NE '' THEN
                    RENEW.CARD = RENEW.CARD + 1
                END
                TOT.TRIAL = TOT.TRIAL + 1

                IF R.CI<CARD.IS.CO.CODE> NE PRV.BRAN THEN
                    PRINT STR('-',125)
                    PRV.BRAN = R.CI<CARD.IS.CO.CODE>
                END
                C.TYP = ""
                IF KEY.LIST<I>[1,2] EQ "VI" THEN
                    C.TYP = "����"
                    V.NO  = V.NO + 1
                END
                IF KEY.LIST<I>[1,2] EQ "MA" THEN
                    C.TYP = "�����"
                    M.NO  = M.NO + 1
                END
                IF KEY.LIST<I>[1,2] EQ "AT" THEN
                    C.TYP = "����"
                    A.NO = A.NO + 1
                END
                XX = SPACE(120)
                XX<1,1>[1,10]    = BRANCH
                XX<1,1>[15,16]    = KEY.LIST<I>[6,16]
                XX<1,1>[35,30]   = R.CI<CARD.IS.NAME>
                XX<1,1>[74,5]   = C.TYP
                XX<1,1>[90,15]   = "20":R.CI<CARD.IS.DATE.TIME>[1,2]:"/":R.CI<CARD.IS.DATE.TIME>[3,2]:"/":R.CI<CARD.IS.DATE.TIME>[5,2]
                XX<1,1>[110,20]  = KEY.LIST1<SELECTED1>[6,16]

                PRINT XX<1,1>

            END

        NEXT I

        PRINT STR('=',125)

        PRINT ; PRINT SPACE(5):" ���� :  ":V.NO:SPACE(30):" :������� ":M.NO:SPACE(30):" ���� ��� : ":A.NO
        PRINT ; PRINT SPACE(5):" ������ ��� �������� �������  ":TOT.TRIAL
        PRINT ; PRINT SPACE(5):" ��� �������� ������� )��� ����-����( ���� �������  " :RENEW.CARD
        PRINT STR('=',125)
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

    RETURN
*===============================================================
PRINT.HEAD:
    R.USR = ''
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USR<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CURR.COMP,BRANCH)
    YYBRN = FIELD(BRANCH,'.',1)
    YYBRN = BRANCH
    DATY = TODAY
*X   T.DAY = DATY[3,2]:'/':DATY[5,2]:"/":DATY[0,4]
    T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
*    PR.HD ="'L'":SPACE(1):"Suez Canal Bank "  : SPACE(90):"Branch:" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
*    PR.HD :="'L'":SPACE(1):"Date    : ":T.DAY:SPACE(85):"Page No.   : ":"'P'"
    PR.HD :="'L'":SPACE(1):"SBD.CREDIT.CARD.DELETED"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ���� ��������� ���������� �������  "
    PR.HD :="'L'":SPACE(50): " �� ������ " :ST.DATE :" ��� " : EN.DATE
    PR.HD :="'L'":SPACE(40):STR('_',55)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):" �����" :SPACE(10):" �.�������":SPACE(15):"�����":SPACE(25):"�����":SPACE(13):"�.�������":SPACE(9):"������� �������"
    PR.HD :="'L'":STR('_',128)
**X    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
