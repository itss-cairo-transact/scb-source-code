* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>60</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.MLAD.PROG.305
***    PROGRAM SBD.MLAD.PROG.305
**                            ����� / ���� �����
***                            �������� �

*   ------------------------------
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.HIST

*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------

    GOSUB INIT

    GOSUB OPENFILES

    GOSUB DEL.ROUTINE


*-------------------------------------------------------------------------

    SEL.MLADH  = "SELECT ":FN.MLADHIS:" WITH "
    SEL.MLADH := "ID.DAY NE 99 "
    SEL.MLADH := "AND ID.MONTH NE 99 "
    SEL.MLADH := "AND ID.YEAR NE 9999 "
    SEL.MLADH := "BY @ID"

    PROCESS.TYPE = "A"
    GOSUB MAIN.ROUTINE


*-------------------------------------------
    SEL.MLADH  = "SELECT ":FN.MLADHIS:" WITH "
    SEL.MLADH := "ID.DAY EQ 99 "
    SEL.MLADH := "AND ID.MONTH NE 99 "
    SEL.MLADH := "AND ID.YEAR NE 9999 "
    SEL.MLADH := "BY @ID"

    PROCESS.TYPE = "B"
    GOSUB MAIN.ROUTINE


*-------------------------------------------
    SEL.MLADH  = "SELECT ":FN.MLADHIS:" WITH "
    SEL.MLADH := "ID.DAY EQ 99 "
    SEL.MLADH := "AND ID.MONTH EQ 99 "
    SEL.MLADH := "AND ID.YEAR NE 9999 "
    SEL.MLADH := "BY @ID"

    PROCESS.TYPE = "C"
    GOSUB MAIN.ROUTINE

*-------------------------------
*    PRINT
*    FOR IX = 1 TO 100
*        IF A.CUR(IX) NE 0 THEN
*            PRINT IX:" ":A.CUR(IX):" ":A.MED(IX):" ":A.MAX(IX):"  ":(A.MED(IX)/A.MAX(IX))*100
*        END
*    NEXT IX

*-------------------------------

    RETURN
*-------------------------------------------------------------------------
INIT:

    PROGRAM.ID = "SBD.MLAD.PROG.305"

    FN.MLADHIS = "F.SCB.MLAD.HIST"
    F.MLADHIS  = ""
    R.MLADHIS  = ""
    Y.MLADHIS.ID  = ""
    KEY.LISTHIST  = ""

    WS.SAVE.CY       = 0
    WS.SAVE.DATE     = 0
    WS.SAVE.KEY      = 0
    WS.SAVE.CATG     = 0
    NO.REC           = 0
    TOT.CUR.BAL      = 0
    TOT.EQV.CUR.BAL  = 0
    TOT.SAV.BAL      = 0
    TOT.EQV.SAV.BAL  = 0
    WS.TOT.CCY.BALANCE = 0

    WS.SAVE.CY       =   0
    WS.SAVE.YEAR     =   0
    WS.SAVE.MONTH    =   0

    WS.SAVE.KEY = 0
    WS.FILE.KEY = 0

    S.DAY            = 0
    S.MON            = 0
    S.YEAR           = 0

    WS.SAVE.REC      = 0

*---------------------------------------
    SYS.DATE = TODAY

    WRK.YEAR = SYS.DATE[1,4] - 5
    WRK.MON  = SYS.DATE[5,2]
    WRK.DAY  = SYS.DATE[7,2]
    WRK.DATE = WRK.YEAR:WRK.MON:WRK.DAY

*---------------------------------------
    DIM A.CUR(100)
    DIM A.MED(100)
    DIM A.MAX(100)

    FOR IX = 1 TO 100
        A.CUR(IX) = 0
        A.MED(IX) = 0
        A.MAX(IX) = 0
    NEXT IX

    IX = 0


    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLADHIS,F.MLADHIS)

    RETURN
*--------------------------------------------------------------------------
MAIN.ROUTINE:

    CALL EB.READLIST(SEL.MLADH,SEL.LIST.MLADH,'',NO.OF.MLADH,ERR.MLADH)


    LOOP
        REMOVE Y.MLADH.ID FROM SEL.LIST.MLADH SETTING POS.MLADH
    WHILE Y.MLADH.ID:POS.MLADH

        CALL F.READ(FN.MLADHIS,Y.MLADH.ID,R.MLADH,F.MLADH,ERR.MLADH)


        WS.FILE.CY       =   Y.MLADH.ID[1,3]
        WS.FILE.YEAR     =   Y.MLADH.ID[1,8]
        WS.FILE.MON      =   Y.MLADH.ID[1,10]

        WS.FILE.DATE     =   Y.MLADH.ID[5,8]

***                                           ����� ������� ��� �� ��� �����
        IF PROCESS.TYPE = 'A' THEN
            IF WS.FILE.DATE LE WRK.DATE THEN
*******                PRINT WS.FILE.DATE:"    ":WRK.DATE
                GO TO NEXT.TRIAL.REC
            END
        END

        WS.CY            =   R.MLADH<MLAD.HIS.CURRENCY>

        WS.YEAR          =   R.MLADH<MLAD.HIS.ID.YEAR>
        WS.MONTH         =   R.MLADH<MLAD.HIS.ID.MONTH>
        WS.DAY           =   R.MLADH<MLAD.HIS.ID.DAY>

        WS.CUR.BAL       =   R.MLADH<MLAD.HIS.CURRENT.AMT>
        WS.EQV.CUR.BAL   =   R.MLADH<MLAD.HIS.EQV.CURR.AMT>
        WS.SAV.BAL       =   R.MLADH<MLAD.HIS.SAVING.AMT>
        WS.EQV.SAV.BAL   =   R.MLADH<MLAD.HIS.EQV.SAVING.AMT>
        WS.TOT.CCY.BAL   =   R.MLADH<MLAD.HIS.TOT.CCY.BALANCE>
*----------------------------------------------
        MAX.FLAG  = ''

        IF PROCESS.TYPE EQ  "A"   THEN
            MAX.FLAG  =  'Y'
            GOSUB SEARCH.IN.ARY
        END
*----------------------------------------------
        IF PROCESS.TYPE EQ "A" THEN
            WS.FILE.KEY  = WS.FILE.MON
        END
*----------------------------------------------
        IF PROCESS.TYPE EQ "B" THEN
            WS.FILE.KEY  = WS.FILE.YEAR
        END
*----------------------------------------------
        IF PROCESS.TYPE EQ "C" THEN
            WS.FILE.KEY  = WS.FILE.CY
        END
*----------------------------------------------

        BEGIN CASE
*       ------------------------
        CASE  WS.SAVE.KEY EQ 0
            GOSUB SAVE.NEW.REC

*       ------------------------
        CASE WS.SAVE.KEY EQ WS.FILE.KEY
            IF PROCESS.TYPE EQ "A" THEN
                GOSUB GET.MIN.VALUE
            END
            IF PROCESS.TYPE EQ "B" THEN
                GOSUB GET.MIN.VALUE
            END
            IF PROCESS.TYPE EQ "C" THEN
                GOSUB GET.MED.VALUE
            END

*       ------------------------
        CASE WS.SAVE.KEY NE WS.FILE.KEY

            GOSUB CHANGE.ONE.REC

*       ------------------------
        END CASE
*---------------

NEXT.TRIAL.REC:
    REPEAT

    GOSUB CHANGE.ONE.REC

    RETURN
*--------------------------------------------------------------------------
SAVE.NEW.REC:

*----------------------------------------------
    WS.SAVE.KEY = WS.FILE.KEY
*----------------------------------------------

    WS.SAVE.CUR.BAL       =   WS.CUR.BAL
    WS.SAVE.EQV.CUR.BAL   =   WS.EQV.CUR.BAL
    WS.SAVE.SAV.BAL       =   WS.SAV.BAL
    WS.SAVE.EQV.SAV.BAL   =   WS.EQV.SAV.BAL
    WS.SAVE.TOT.CCY.BAL   =   WS.TOT.CCY.BAL
    WS.SAVE.CY            =   WS.CY
    WS.SAVE.YEAR          =   WS.YEAR
    WS.SAVE.MONTH         =   WS.MONTH
    WS.SAVE.DAY           =   WS.DAY
    WS.SAVE.REC           =   1


    RETURN
*--------------------------------------------------------------------------
GET.MIN.VALUE:

    IF WS.SAVE.TOT.CCY.BAL  GT  WS.TOT.CCY.BAL  THEN
        WS.SAVE.CUR.BAL       =   WS.CUR.BAL
        WS.SAVE.EQV.CUR.BAL   =   WS.EQV.CUR.BAL
        WS.SAVE.SAV.BAL       =   WS.SAV.BAL
        WS.SAVE.EQV.SAV.BAL   =   WS.EQV.SAV.BAL
        WS.SAVE.TOT.CCY.BAL   =   WS.TOT.CCY.BAL
    END

    RETURN
*--------------------------------------------------------------------------
GET.MED.VALUE:

    WS.SAVE.CUR.BAL       +=   WS.CUR.BAL
    WS.SAVE.EQV.CUR.BAL   +=   WS.EQV.CUR.BAL
    WS.SAVE.SAV.BAL       +=   WS.SAV.BAL
    WS.SAVE.EQV.SAV.BAL   +=   WS.EQV.SAV.BAL
    WS.SAVE.TOT.CCY.BAL   +=   WS.TOT.CCY.BAL
    WS.SAVE.REC           +=   1

    RETURN
*--------------------------------------------------------------------------
CHANGE.ONE.REC:

*   -------------------------------
    IF PROCESS.TYPE EQ "A" THEN
        WS.SAVE.DAY = 99
    END
*   -------------------------------
    IF PROCESS.TYPE EQ "B" THEN
        WS.SAVE.DAY = 99
        WS.SAVE.MONTH = 99
    END
*   -------------------------------
    IF PROCESS.TYPE EQ "C" THEN
        IF WS.SAVE.REC EQ 0 THEN
            WS.SAVE.REC = 1
        END

        WS.SAVE.DAY  = 99
        WS.SAVE.MONTH  = 99
        WS.SAVE.YEAR = 9999

        WS.SAVE.CUR.BAL       =    (WS.SAVE.CUR.BAL / WS.SAVE.REC)
        WS.SAVE.EQV.CUR.BAL   =    (WS.SAVE.EQV.CUR.BAL / WS.SAVE.REC)
        WS.SAVE.SAV.BAL       =    (WS.SAVE.SAV.BAL     / WS.SAVE.REC)
        WS.SAVE.EQV.SAV.BAL   =    (WS.SAVE.EQV.SAV.BAL / WS.SAVE.REC)
        WS.SAVE.TOT.CCY.BAL   =    (WS.SAVE.TOT.CCY.BAL / WS.SAVE.REC)
    END
*   -------------------------------

    Y.MLADH.ID = WS.SAVE.CY:"-":WS.SAVE.YEAR:WS.SAVE.MONTH:WS.SAVE.DAY

    GOSUB WRITE.MLAD.HIS.FILE
    GOSUB SAVE.NEW.REC



    RETURN
*--------------------------------------------------------------------------
WRITE.MLAD.HIS.FILE:
*-------------------
    R.MLADHIS = ''
*------------------------
    IF PROCESS.TYPE EQ "C" THEN
        MAX.FLAG  =  'N'
        GOSUB SEARCH.IN.ARY
    END
*   -------------------


    R.MLADH<MLAD.HIS.CURRENT.AMT>     = WS.SAVE.CUR.BAL
    R.MLADH<MLAD.HIS.EQV.CURR.AMT>    = WS.SAVE.EQV.CUR.BAL
    R.MLADH<MLAD.HIS.SAVING.AMT>      = WS.SAVE.SAV.BAL
    R.MLADH<MLAD.HIS.EQV.SAVING.AMT>  = WS.SAVE.EQV.SAV.BAL
    R.MLADH<MLAD.HIS.TOT.CCY.BALANCE> = WS.SAVE.TOT.CCY.BAL

    IF PROCESS.TYPE EQ "C" THEN
        R.MLADH<MLAD.HIS.CURRENT.AMT>     = F.MED / F.MAX * 100
        R.MLADH<MLAD.HIS.SAVING.AMT>      = F.MAX
    END
*   -------------------

    R.MLADH<MLAD.HIS.CURRENCY>        = WS.SAVE.CY
    R.MLADH<MLAD.HIS.ID.YEAR>         = WS.SAVE.YEAR
    R.MLADH<MLAD.HIS.ID.MONTH>        = WS.SAVE.MONTH
    R.MLADH<MLAD.HIS.ID.DAY>          = WS.SAVE.DAY

*------------------------
    CALL F.WRITE(FN.MLADHIS,Y.MLADH.ID,R.MLADH)
    CALL JOURNAL.UPDATE(Y.MLADH.ID)
*------------------------
    RETURN

*===============================================================
DEL.ROUTINE:

    SEL.MLADH  = "SELECT ":FN.MLADHIS:" WITH "
    SEL.MLADH := "ID.DAY EQ 99 "


    CALL EB.READLIST(SEL.MLADH,SEL.LIST.MLADH,'',NO.OF.MLADH,ERR.MLADH)


    FOR I = 1 TO NO.OF.MLADH

        CALL F.READ(FN.MLADHIS,SEL.LIST.MLADH<I>,R.MLADHIS,F.MLADHIS,READ.ERRMLAD)
**UPDATED BY ABEER ---UPGRADING R15---2016-03-22---
**  DELETE F.MLADHIS , SEL.LIST.MLADH<I>
        CALL F.DELETE(FN.MLADHIS,SEL.LIST.MLADH<I>)
***

    NEXT I
    RETURN
*===============================================================
SEARCH.IN.ARY:

    FOR IX = 1 TO 100

        IF   MAX.FLAG EQ 'Y'    THEN
            IF  A.CUR(IX) EQ WS.CY  THEN
                A.MAX(IX) = WS.TOT.CCY.BAL
                IX = 100
                GO TO END.SEARCH.IN.ARY
            END
        END

        IF   MAX.FLAG EQ 'Y'    THEN
            IF  A.CUR(IX) EQ 0  THEN
                A.CUR(IX) = WS.CY
                A.MAX(IX) = WS.TOT.CCY.BAL
                IX = 100
                GO TO END.SEARCH.IN.ARY
            END
        END
* --------
        IF   MAX.FLAG EQ 'N'    THEN
            IF  A.CUR(IX) EQ WS.SAVE.CY  THEN
                A.MED(IX) = WS.SAVE.TOT.CCY.BAL
                F.MED     =  A.MED(IX)
                F.MAX     =  A.MAX(IX)
                IX = 100
                GO TO END.SEARCH.IN.ARY
            END
        END

        IF   MAX.FLAG EQ 'N'    THEN
            IF  A.CUR(IX) EQ  0 THEN
                A.CUR(IX) = WS.SAVE.CY
                A.MED(IX) = WS.SAVE.TOT.CCY.BAL
                F.MED     =  A.MED(IX)
                F.MAX     =  A.MAX(IX)
                IX = 100
                GO TO END.SEARCH.IN.ARY
            END
        END


END.SEARCH.IN.ARY:

    NEXT IX


    RETURN
*===============================================================
END
