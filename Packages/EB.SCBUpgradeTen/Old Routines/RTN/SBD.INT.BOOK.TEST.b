* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM CUST.COLL.RISK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS



*==================== create cf and cs text files ========

    OPENSEQ "&SAVEDLISTS&" , "CUST.COLL.RISK.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CUST.COLL.RISK.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CUST.COLL.RISK.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CUST.COLL.RISK.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create CUST.COLL.RISK.TXT File IN &SAVEDLISTS&'
        END
    END


*============================ OPEN TABLES =========================

    FN.COLL.R  = 'FBNK.COLLATERAL.RIGHT' ; F.COLL.R = '' ; R.COLL.R = ''
    CALL OPF(FN.COLL.R,F.COLL.R)
    FN.COLL    = 'FBNK.COLLATERAL'       ; F.COLL   = '' ; R.COLL   = ''
    CALL OPF(FN.COLL,F.COLL)
    FN.LI    = 'FBNK.LIMIT'              ; F.LI     = '' ; R.LI     = ''
    CALL OPF(FN.LI,F.LI)
    FN.CU    = 'FBNK.CUSTOMER'           ; F.CU     = '' ; R.CU     = ''
    CALL OPF(FN.CU,F.CU)

    COMP      = ''
    COMP      = C$ID.COMPANY
    CO        = COMP[8,2]
    IF CO[1,1] EQ 0 THEN
        CO = CO[2,1]
    END
    T.SEL  = "SELECT FBNK.COLLATERAL.RIGHT WITH LIMIT.REFERENCE NE '' AND DEPT.CODE EQ ":CO
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*======================== BB.DATA ===========================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            TOT.LT.2000 = 0
            TOT.GT.2000 = 0

            CALL F.READ( FN.COLL.R,KEY.LIST<I>, R.COLL.R, F.COLL.R, ETEXT)
            LIMIT.REF = R.COLL.R<COLL.RIGHT.LIMIT.REFERENCE>
*Line [ 89 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            NO.LIMIT  = DCOUNT(LIMIT.REF,@VM)
            CRT @(30,3) LIMIT.REF
            FOR J = 1 TO NO.LIMIT
                LI    = LIMIT.REF<1,J>
                LIMIT = FIELD(LI,'.',1)
                CALL F.READ(FN.LI,LIMIT,R.LI,F.LI,ERR1)
                LIMIT.PRO = R.LI< LI.LIMIT.PRODUCT>
                IF LIMIT.PRO LE 2000 THEN
                    R.COLL = ''
                    Y.SEL  = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":LIMIT:"..."
                    CALL EB.READLIST(Y.SEL, KEY.LIST1, "", SELECTED1, ERR2)
                    FOR K = 1 TO SELECTED1
                        CALL F.READ( FN.COLL,KEY.LIST1<K>, R.COLL, F.COLL, ERR3)
                        AMT = R.COLL<COLL.LOCAL.REF><1,COLR.COLL.AMOUNT>
                        TOT.LT.2000 = TOT.LT.2000 + AMT

                    NEXT K
                END
                IF LIMIT.PRO GT 2000 THEN
                    R.COLL = ''
                    Z.SEL  = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":LIMIT:"..."
                    CALL EB.READLIST(Z.SEL, KEY.LIST2, "", SELECTED2, ERR2)
                    FOR O = 1 TO SELECTED2
                        CALL F.READ( FN.COLL,KEY.LIST2<O>, R.COLL, F.COLL,ERR4)
                        AMT = R.COLL<COLL.LOCAL.REF><1,COLR.COLL.AMOUNT>
                        TOT.GT.2000 = TOT.GT.2000 + AMT
                    NEXT O
                END
            NEXT J
            CUS.ID = LIMIT
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,ERR4)
            CUS.NAME    = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            OLD.CUS     = R.CU<EB.CUS.LOCAL.REF><1,CULR.OLD.CUST.ID>
****            BRANCH      = R.CU<EB.CUS.ACCOUNT.OFFICER>
            BRANCH      = R.CU<EB.CUS.COMPANY.BOOK>[2]
            BRANCH      = TRIM(BRANCH, "0" , "L")

            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH,BR.NAME)
            BRANCH.NAME  = FIELD(BR.NAME,'.',2)


            BB.DATA  = BRANCH:'|'
            BB.DATA := BRANCH.NAME:'|'
            BB.DATA := CUS.ID:'|'
            BB.DATA := CUS.NAME:'|'
            BB.DATA := TOT.LT.2000:'|'
            BB.DATA := TOT.GT.2000:'|'
            BB.DATA := OLD.CUS


            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END


        NEXT I
    END

    CLOSESEQ BB

END
