* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-273</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.DRMNT.PRT.02.DRMNT.DATE
*------------------------------------------
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.POS.TODAY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*========================================================
*                                                  ����� �� ��������
*               ����� ������� ���� �� ����� �������� �� ��� �����
*                             ����� ������� ���� �� ����� �������
*                                 ���� ��� ������ ����� �������
*=========================================================
    GOSUB INIT
*-------------------------------
    WS.COMP = ID.COMPANY
*-------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.02.DRMNT.DATE"  THEN
        REPORT.ID = 'P.FUNCTION'
        CALL PRINTER.ON(REPORT.ID,'')
        GOSUB PRINT.HEAD
        GOSUB OPENFILES
*-------------------------------
        GOSUB READ.CUST.POS.FILE
*-------------------------------
    END

    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.02.DRMNT.DATE"  THEN
        XX = SPACE(120)
        PRINT XX<1,1>
        XX = STR('_',120)
        PRINT XX<1,1>
        XX = SPACE(120)
        PRINT XX<1,1>
        XX<1,1>[1,50]   = "**** ������ ��� �������  ****"
        XX<1,1>[70,20]  = FMT(NO.DRMNT.CUST,"R0,")
        PRINT XX<1,1>
        XX = SPACE(120)
        PRINT XX<1,1>
        PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)
*-------------------------------
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
    END
*-----------------------------------------------------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10"  THEN
*-------------------------------
        GOSUB OPENFILES
*-------------------------------
        GOSUB READ.CUST.POS.FILE
*-------------------------------
    END
    RETURN
*-------------------------------------------------------------------------
INIT:
    PROGRAM.ID = "SBD.DRMNT.PRT.02.DRMNT.DATE"

    NO.DRMNT.CUST = 0

    FN.CUS.POS = "F.SCB.CUST.POS.TODAY"
    F.CUS.POS  = ""
    R.CUS.POS  = ""
    Y.CUS.POS.ID   = ""

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC  = ""
    R.CUS.ACC = ""
    Y.CUS.ACC.ID   = ""

    FN.CUSTOMER = "FBNK.CUSTOMER"
    F.CUSTOMER  = ""
    R.CUSTOMER  = ""
    Y.CUST.ID   = ""

    FN.LMM.CUST = "FBNK.LMM.CUSTOMER"
    F.LMM.CUST  = ""
    R.LMM.CUST  = ""
    Y.LMM.CUST.ID   = ""


    FN.ACC = "FBNK.ACCOUNT"
    F.ACC  = ""
    R.ACCOUNT = ""
    Y.ACC.ID = ""

    FN.POST = "FBNK.POSTING.RESTRICT"
    F.POST  = ""
    R.POST  = ""
    Y.POST.ID = ""

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD  = ""
    R.LD = ""
    Y.LD.ID = ""

    FN.LC = "FBNK.LETTER.OF.CREDIT"
    F.LC  = ""
    R.LC = ""
    Y.LC.ID = ""

    OLD.CUST = 0
    CUST.NAME = ''
    NO.OF.LD   = ''
    NO.OF.LC   = ''
*---------------------------------------
    SYS.DATE = TODAY
    SYS.YYMM = SYS.DATE[1,6]

    WRK.DATE = SYS.DATE

*---------------------------------------
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]
*---------------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
        WRK.MM   = WRK.MM + 2

        IF WRK.MM GT 12 THEN
            WRK.YY = WRK.YY + 1
            WRK.MM = WRK.MM - 12
        END

        WRK.DD   = 01
        WRK.MM = FMT(WRK.MM,"R%2")
        WRK.DD = FMT(WRK.DD,"R%2")
        WRK.DATE = WRK.YY:WRK.MM:WRK.DD

        CALL CDT('',WRK.DATE,'-1C')
        WRK.YY   = WRK.DATE[1,4]
        WRK.MM   = WRK.DATE[5,2]
        WRK.DD   = WRK.DATE[7,2]
    END
*---------------------------------------

    OLD1.YY   = WRK.DATE[1,4] - 1
    OLD1.MM   = WRK.DATE[5,2]
    OLD1.DD   = WRK.DATE[7,2]
    OLD1.DATE = OLD1.YY:OLD1.MM:OLD1.DD
    OLD1.YYMM = OLD1.DATE[1,6]
*---------------------------------------
    OLD3.YY   = WRK.DATE[1,4] - 3
    OLD3.MM   = WRK.DATE[5,2]
    OLD3.DD   = WRK.DATE[7,2]
    OLD3.DATE = OLD3.YY:OLD3.MM:OLD3.DD
    OLD3.YYMM = OLD3.DATE[1,6]
*---------------------------------------
    P.DATE   = FMT(SYS.DATE,"####/##/##")

*   PRINT OLD1.DATE:" ":OLD3.DATE
*---------------------------------------
    DIM ARY.X(12)
    ARY.X(1) = "������"
    ARY.X(2) = "������"
    ARY.X(3) = "������"
    ARY.X(4) = "������"
    ARY.X(5) = "������"
    ARY.X(6) = "������"
    ARY.X(7) = "������"
    ARY.X(8) = "������"
    ARY.X(9) = "������"
    ARY.X(10) = "������"
    ARY.X(11) = "������"
    ARY.X(12) = "������"
    MON = ARY.X(WRK.MM)

*-----------------------
    LINE.NO = 0
    MAX.LINE.NO = 30
*-----------------------
    WS.POSTING.RESTRICT = 90
*-----------------------
    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.CUS.POS,F.CUS.POS)
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL OPF(FN.LMM.CUST,F.LMM.CUST)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)

    RETURN
*--------------------------------------------------------------------------
READ.CUST.POS.FILE:

    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.02.DRMNT.DATE" THEN
        SEL.CUS.POS = "SELECT ":FN.CUS.POS:" WITH"
        SEL.CUS.POS :=" @ID LIKE ...":SYS.DATE
        SEL.CUS.POS :=" AND CO.CODE EQ ":WS.COMP
        SEL.CUS.POS :=" BY @ID"
    END

    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.02.DRMNT.DATE" THEN
        SEL.CUS.POS = "SELECT ":FN.CUS.POS:" WITH"
        SEL.CUS.POS :=" @ID LIKE ...":SYS.DATE
        SEL.CUS.POS :=" AND CO.CODE EQ ":WS.COMP
        SEL.CUS.POS :=" BY @ID"
    END

    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10" THEN
        SEL.CUS.POS = "SELECT ":FN.CUS.POS:" WITH"
        SEL.CUS.POS :=" @ID LIKE ...":SYS.DATE
***      SEL.CUS.POS :=" AND CO.CODE EQ ":WS.COMP
***      SEL.CUS.POS :=" BY @ID"
    END

    CALL EB.READLIST(SEL.CUS.POS,SEL.LIST.CUS.POS,'',NO.OF.CUS.POS,ERR.CUS.POS)
    LOOP
        REMOVE Y.CUS.POS.ID FROM SEL.LIST.CUS.POS SETTING POS.CUS.POS
    WHILE Y.CUS.POS.ID:POS.CUS.POS
        CALL F.READ(FN.CUS.POS,Y.CUS.POS.ID,R.CUS.POS,F.CUS.POS,ERR.CUS.POS)



        Y.CUST.ID = FIELD(Y.CUS.POS.ID,"-",1)


        PRINT.CUST.NAME = 0

****        IF Y.CUST.ID GT 1200400 THEN
****            RETURN
****        END

        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
        OLD.DRMNT.CODE = LOCAL.REF<1,CULR.DRMNT.CODE>
        OLD.DRMNT.DATE = LOCAL.REF<1,CULR.DRMNT.DATE>

        CALL DBR ('CUSTOMER':@FM:EB.CUS.POSTING.RESTRICT,Y.CUST.ID,PO.CD)
        OLD.POST.RES = PO.CD



        XX.DRMNT.YYMM  = OLD.DRMNT.DATE[1,6]
        XX.TODAY.YYMM  = TODAY[1,6]
*------------------------------



        IF OLD.DRMNT.CODE EQ 1 THEN

            IF OLD.POST.RES EQ 18 THEN

                IF XX.DRMNT.YYMM EQ XX.TODAY.YYMM THEN


                    DRMNT.FLAG = 'Y'

                    Y.CUS.ACC.ID = Y.CUST.ID

                    NO.DRMNT.CUST +=1

                    GO TO CONT.PRINT

                END
            END
        END

        GO TO NEXT.CUSTOMER

CONT.PRINT:
*------------------------------


        CU.POST.NAME = ''

        CALL DBR ('POSTING.RESTRICT':@FM: AC.POS.DESCRIPTION,OLD.POST.RES,PO.DESCR)

        CU.POST.NAME = PO.DESCR

*        IF OLD.POST.RES GT 0 THEN
*            GO TO NEXT.CUSTOMER
*        END
*------------------------------
** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**         &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**                 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

*       **************************************

        IF PROGRAM.ID EQ "SBD.DRMNT.UPD.10" THEN
            CALL SCB.DRMNT.OFS(Y.CUST.ID)
        END
* -----------------------

        IF PROGRAM.ID EQ "SBD.DRMNT.PRT.01" THEN
            GOSUB READ.CUSTOMER.ACCOUNT.2
        END
* -----------------------

        IF PROGRAM.ID EQ "SBD.DRMNT.PRT.02.DRMNT.DATE" THEN
            GOSUB READ.CUSTOMER.ACCOUNT.2
        END
* -----------------------

** &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**         &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
**                 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

*       **************************************
*-----------

NEXT.CUSTOMER:


*---------------
    REPEAT
    RETURN
*--------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT:

*-------------------          ������ ������



    NO.OF.ACC = ''


    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)
    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)
    IF NO.OF.ACC EQ 0 THEN
        DRMNT.FLAG = 'N'
        RETURN
    END
    DRMNT.CURR = ''
    DRMNT.SAVE = ''
    DRMNT.OTHR = ''
*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>
        Y.CUST.ID = Y.CUS.ACC.ID
*****     PRINT  Y.ACC.ID:" ... ":Y.CUST.ID:" ... ":NO.OF.ACC
*---------------
        GOSUB READ.ACCOUNT.FILE
*---------------
    NEXT Z
****************
*kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk
LAST.CUSTOMER:
    RETURN
*-------------------------------------------------------------------------
READ.ACCOUNT.FILE:

    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACCOUNT,F.ACC,ERR.ACC)


    Y.CATEG.ID   = R.ACCOUNT<AC.CATEGORY>
*-----------
    OPENING.DATE    = R.ACCOUNT<AC.OPENING.DATE>

    CUST.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.CUST>
    CUST.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.CUST>
*    BANK.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.BANK>
*    BANK.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.BANK>
*    AUTO.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.AUTO>
*    AUTO.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.AUTO>

    W.BAL           = R.ACCOUNT<AC.WORKING.BALANCE>
    WS.CY           = R.ACCOUNT<AC.CURRENCY>
    AC.POST.RES     = R.ACCOUNT<AC.POSTING.RESTRICT>

*-------------------

    TOT.BAL = TOT.BAL + W.BAL

*-------------------
*    IF W.BAL EQ ''  THEN
*        W.BAL = 0
*    END
*    IF W.BAL EQ 0 THEN
*        RETURN
*    END
*-------------------
    AC.POST.NAME = ''

    CALL DBR ('POSTING.RESTRICT':@FM: AC.POS.DESCRIPTION,AC.POST.RES,PO.DESCR)

    AC.POST.NAME = PO.DESCR


*    IF R.ACCOUNT<AC.POSTING.RESTRICT> GE WS.POSTING.RESTRICT  THEN
*        DRMNT.FLAG = 'N'
*        RETURN
*    END
*-------------------

    MORE.SURE = 'N'

    BEGIN CASE

    CASE Y.CATEG.ID EQ 1001
        DRMNT.CURR = 'Y'

    CASE Y.CATEG.ID EQ 1019
        DRMNT.CURR = 'Y'
****        MORE.SURE  = 'Y'

    CASE Y.CATEG.ID GE 6500  AND  Y.CATEG.ID LE 6599
        DRMNT.SAVE = 'Y'

    CASE Y.CATEG.ID NE 0
        DRMNT.OTHR = 'Y'

    END CASE
*-------------------
    IF DRMNT.OTHR EQ 'Y'  THEN
        DRMNT.FLAG = 'N'
    END
*------------------------------------
    LAST.DATE = ''

    IF CUST.DATE.CR EQ ''   THEN
        IF CUST.DATE.DR EQ ''   THEN

            LAST.DATE = OPENING.DATE
        END
    END
*------------------------------------


    IF CUST.DATE.CR GT LAST.DATE THEN
        LAST.DATE = CUST.DATE.CR
    END

    IF CUST.DATE.DR GT LAST.DATE THEN
        LAST.DATE   = CUST.DATE.DR
    END
*-----------

*-------------------------------
    LAST.YYMM = LAST.DATE[1,6]
****                �� ���� �������� �� ������� ����� ������ ������ �� 3 ����� ���� �� ���

    WS.OLD1.YY = OLD1.DATE[1,4]
    WS.OLD1.MM = OLD1.DATE[5,2]
    WS.OLD1.DD = OLD1.DATE[7,2]

    IF NO.OF.DEPOSIT GT 0  THEN
        WS.OLD1.YY  = WS.OLD1.YY - 2
    END

    WS.OLD1.YYMM = WS.OLD1.YY:WS.OLD1.MM
    WS.OLD1.DATE = WS.OLD1.YY:WS.OLD1.MM:WS.OLD1.DD
*---------------------------------------------
    IF PROGRAM.ID EQ "SBD.DRMNT.PRT.02.DRMNT.DATE" THEN
        IF DRMNT.CURR EQ 'Y' THEN
***        IF LAST.DATE GT WS.OLD1.DATE THEN
            IF LAST.YYMM GT WS.OLD1.YYMM THEN
                DRMNT.FLAG = 'N'
            END
        END
    END
*** ---------------------------------

    IF PROGRAM.ID EQ "SBD.DRMNT.UPD.01" THEN
        IF DRMNT.CURR EQ 'Y' THEN
            IF LAST.DATE GT WS.OLD1.DATE THEN
***            IF LAST.YYMM GT WS.OLD1.YYMM THEN
                DRMNT.FLAG = 'N'
            END
        END
    END
*---------------------------------------------
*-----------                         �� ���� ������ ������� �� ��������� ����� �� 3 �����

    IF DRMNT.SAVE EQ 'Y' THEN
***        IF LAST.DATE GT OLD3.DATE THEN
        IF LAST.YYMM GT OLD3.YYMM THEN
            DRMNT.FLAG = 'N'
        END
    END
*-----------   �� ���� �� ������ ���� ���� ��� ������ ���� ���� ���� ���� ������ �������


    IF MORE.SURE EQ 'Y' THEN
        DRMNT.FLAG = 'Y'
    END
*-----------
    RETURN
*-------------------------------------------------------------------------
READ.LD.FILE:
***********************

*-------------------
    NO.OF.DEPOSIT = ''

*-------------------               + �������� +   �������

    CALL F.READ(FN.LMM.CUST, Y.CUST.ID, R.LMM.CUST, F.LMM.CUST, E1)


******    LG.CATEG = ''
******    LG.FIELD = R.CUS.POS<CUST.LD>
******    DLG = DCOUNT(LG.FIELD,VM)

    IF E1 EQ '' THEN

        NO.OF.DEPOSIT += 1

    END



***********************
*-------------------
    NO.OF.LD = ''

*-------------------         ������ ������
    LG.CATEG = ''
    LG.FIELD = R.CUS.POS<CUST.LG>
*Line [ 560 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DLG = DCOUNT(LG.FIELD,@VM)

    IF DLG GT 0 THEN
        FOR X = 1 TO DLG

******            LG.ID  = R.CUS.POS<CUST.LG,X>
******            CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,ER.LD)
******            LG.CATEG = R.LD<LD.CATEGORY>
******            LG.DATE.D  = R.LD<LD.FIN.MAT.DATE>
******            LG.AMT   = R.LD<LD.AMOUNT>

            NO.OF.LD += 1

        NEXT X
    END

*-------------------         �������� �������
    LG.CATEG = ''
    LG.FIELD = R.CUS.POS<CUST.LC.IMP>
*Line [ 580 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DLG = DCOUNT(LG.FIELD,@VM)

    IF DLG GT 0 THEN
        FOR X = 1 TO DLG

*****            LG.ID  = R.CUS.POS<CUST.LC.IMP,X>
*****            CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,ER.LD)
*****            LG.CATEG = R.LD<LD.CATEGORY>
*****            LG.DATE.D  = R.LD<LD.FIN.MAT.DATE>
*****            LG.AMT   = R.LD<LD.AMOUNT>

            NO.OF.LD += 1

        NEXT X
    END

*-------------------         �������� �����
    LG.CATEG = ''
    LG.FIELD = R.CUS.POS<CUST.LC.EXP>
*Line [ 600 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DLG = DCOUNT(LG.FIELD,@VM)

    IF DLG GT 0 THEN
        FOR X = 1 TO DLG

*****            LG.ID  = R.CUS.POS<CUST.LC.EXP,X>
*****            CALL F.READ(FN.LD,LG.ID,R.LD,F.LD,ER.LD)
*****            LG.CATEG = R.LD<LD.CATEGORY>
*****            LG.DATE.D  = R.LD<LD.FIN.MAT.DATE>
*****            LG.AMT   = R.LD<LD.AMOUNT>

            NO.OF.LD += 1

        NEXT X
    END


    RETURN
*---------------------------*LC *------------------------
READ.LC.FILE:


    NO.OF.LC = ''

    SEL.LC  = "SELECT ":FN.LC:" WITH CON.CUS.LINK EQ ":Y.CUST.ID
    SEL.LC := " AND LIABILITY.AMT GT 0"
    SEL.LC := " AND EXPIRY.DATE GT ":SYS.DATE

*    PRINT SEL.LC


    CALL EB.READLIST(SEL.LC,SEL.LIST.LC,'',NO.OF.LC,ERR.LC)


    RETURN
*--------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT.2:

    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)

    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)

*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>
        Y.CUST.ID = Y.CUS.ACC.ID


*---------------
        GOSUB READ.ACCOUNT.FILE.2
*---------------
    NEXT Z


    GOSUB READ.LD.FILE.2





****************
    RETURN
*-------------------------------------------------------------------------
READ.ACCOUNT.FILE.2:
    CALL F.READ(FN.ACC,Y.ACC.ID,R.ACCOUNT,F.ACC,ERR.ACC)

    Y.CATEG.ID   = R.ACCOUNT<AC.CATEGORY>
*-----------
    OPENING.DATE    = R.ACCOUNT<AC.OPENING.DATE>

    CUST.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.CUST>
    CUST.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.CUST>
    BANK.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.BANK>
    BANK.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.BANK>
    AUTO.DATE.CR    = R.ACCOUNT<AC.DATE.LAST.CR.AUTO>
    AUTO.DATE.DR    = R.ACCOUNT<AC.DATE.LAST.DR.AUTO>

    W.BAL           = R.ACCOUNT<AC.WORKING.BALANCE>
    WS.CY           = R.ACCOUNT<AC.CURRENCY>
    AC.POST.RES     = R.ACCOUNT<AC.POSTING.RESTRICT>

*------------------------------------
    LAST.DATE = ''

    IF CUST.DATE.CR EQ ''   THEN
        IF CUST.DATE.DR EQ ''   THEN

            LAST.DATE = OPENING.DATE
        END
    END
*------------------------------------


    IF CUST.DATE.CR GT LAST.DATE THEN
        LAST.DATE = CUST.DATE.CR
    END

    IF CUST.DATE.DR GT LAST.DATE THEN
        LAST.DATE   = CUST.DATE.DR
    END


*-----------

    CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,Y.CATEG.ID,S.NAME)
    CATEG.NAME = S.NAME

    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,WS.CY,CY.NAME)
    CURR.NAME = CY.NAME


    AC.POST.NAME = ''
    CALL DBR ('POSTING.RESTRICT':@FM: AC.POS.DESCRIPTION,AC.POST.RES,PO.DESCR)
    AC.POST.NAME = PO.DESCR

*-----------

    GOSUB PRINT.REPORT
*-------------------------------
    RETURN
*-------------------------------------------------------------------------
PRINT.REPORT:
    IF PRINT.CUST.NAME EQ 0 THEN
        GOSUB WRITE.ARY.1
    END


    GOSUB WRITE.ARY.2

    RETURN
*------------------------------------------------------------------------
WRITE.ARY.1:

    IF LINE.NO GT MAX.LINE.NO THEN
        GOSUB PRINT.HEAD
    END

    XX = STR('_',120)
    PRINT XX<1,1>

    XX = SPACE(120)
*------------
    IF DRMNT.FLAG EQ 'Y' THEN
        DRMNT.FLAG = 'YES'
    END
    IF DRMNT.FLAG EQ 'N' THEN
        DRMNT.FLAG = 'NO'
    END
*------------

    XX<1,1>[1,10]   = Y.CUST.ID
    XX<1,1>[12,40]  = CUST.NAME
    XX<1,1>[55,17]  = CU.POST.NAME

    PRINT XX<1,1>

    LINE.NO = +1

    PRINT.CUST.NAME = 1

    RETURN
*------------------------------------------------------------------------
WRITE.ARY.2:

    IF LINE.NO GT MAX.LINE.NO THEN
        GOSUB PRINT.HEAD
    END


    XX = SPACE(120)
    XX<1,1>[1,20]   = Y.ACC.ID
    XX<1,1>[33,6]   = Y.CATEG.ID
    XX<1,1>[40,12]  = CATEG.NAME
    XX<1,1>[57,15]  = AC.POST.NAME

    XX<1,1>[60,10]  = FMT(OLD.DRMNT.DATE,"####/##/##")

    XX<1,1>[78,3]   = WS.CY
    XX<1,1>[85,12]  = CURR.NAME
    XX<1,1>[97,10] = FMT(LAST.DATE,"####/##/##")
    XX<1,1>[110,20] = FMT(W.BAL,"R2,")

    PRINT XX<1,1>
    LINE.NO = +1
    RETURN
*------------------------------------------------------------------------
READ.LD.FILE.2:

    NO.OF.LD = ''

    CALL F.READ(FN.LMM.CUST, Y.CUST.ID, R.LMM.CUST, F.LMM.CUST, E1)


    LOOP

        REMOVE LDD FROM R.LMM.CUST  SETTING POS111
    WHILE LDD:POS111
        CALL F.READ(FN.LD,LDD, R.LD, F.LD,E)

        LD.CONT.NO = LDD
        LD.CATEG   = R.LD<LD.CATEGORY>
        LD.CURR    = R.LD<LD.CURRENCY>
        LD.AMT     = R.LD<LD.AMOUNT>
        LD.LAST.D  = R.LD<LD.FIN.MAT.DATE>

****************
        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,LD.CATEG,S.NAME)
        CATEG.NAME = S.NAME
****************
        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,LD.CURR,CY.NAME)
        CURR.NAME = CY.NAME
****************
        XX = SPACE(120)
        XX<1,1>[1,20]   = LD.CONT.NO
        XX<1,1>[33,6]   = LD.CATEG
        XX<1,1>[40,12]  = CATEG.NAME



        XX<1,1>[75,3]   = LD.CURR
        XX<1,1>[80,12]  = CURR.NAME


        XX<1,1>[95,10]  = FMT(LD.LAST.D,"####/##/##")


        XX<1,1>[110,20] = FMT(LD.AMT,"R2,")

        PRINT XX<1,1>

        LINE.NO = +1


    REPEAT


*******************************************************************************

    RETURN
*--------------------------------------------------------------------------
PRINT.HEAD:
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
    YYBRN = BRANCH


    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):PROGRAM.ID
    PR.HD :="'L'":SPACE(35):" ����� ������� ������� ���� �� ����� ��� ��� ����� "
    PR.HD :="'L'":SPACE(40):" ����� ������ ���� ���� ���":" ":MON:" ":WRK.YY
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(05):" ��� ������ "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(25):" �������":SPACE(15):"������":SPACE(09):"������":SPACE(14):"��� ����":SPACE(06):"������"
    PR.HD :="'L'":SPACE(58):"����� ������"
    HEADING PR.HD
    LINE.NO = 0
    RETURN
*===============================================================
END
