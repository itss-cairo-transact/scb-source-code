* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.FAILED.LOG.2
*    PROGRAM SBD.FAILED.LOG.2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PROTOCOL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.STATUS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*-----------------------------------------
    GOSUB INITIATE
    CURR.COMP = ID.COMPANY
    GOSUB PRINT.HEAD

*Line [ 42 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.FAILED.LOG.2'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*=============================================================
CALLDB:

    FN.PROT = 'F.PROTOCOL'   ; F.PROT  = ''  ; R.PROT = ''
    FN.USR  = 'F.USER'       ; F.USR   = ''  ; R.USR  = ''
    BRANCH  = ''             ; PRV.USR = ''

    CALL OPF(FN.PROT,F.PROT)
    CALL OPF(FN.USR,F.USR)
    TOT.TRIAL = 0
    T.SEL = "SELECT F.PROTOCOL WITH APPLICATION EQ 'SIGN.ON' AND REMARK NE '' AND REMARK NE 'AUTOMATIC LOGOUT' BY USER BY TIME"
    KEY.LIST=""  ; SELECTED=""  ;  E1="" ; E2=''

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.PROT,KEY.LIST<I>,R.PROT,F.PROT,E1)

            IF NOT(E1) THEN
                BRANCH    = ''     ;    E.DATE.1 = ''
                SYS.DATE  = KEY.LIST<I>[1,8]
                IF NUM(SYS.DATE) THEN
                END ELSE
                    SYS.DATE  = ""
                END

                CALL F.READ(FN.USR,R.PROT<EB.PTL.USER>,R.USR,F.USR,E2)
                BRANCH.NO       = R.USR<EB.USE.DEPARTMENT.CODE>
                E.DATE.1        = R.USR<EB.USE.END.DATE.PROFILE>
                BRANCH.NO.2     = FMT(BRANCH.NO,'R%2')
                CURR.BRANCH.LOG = 'EG00100':BRANCH.NO.2

                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.NO,BRANCH)
                YYBRN = FIELD(BRANCH,'.',1)

                IF CURR.BRANCH.LOG EQ CURR.COMP THEN
                    TOT.TRIAL = TOT.TRIAL + 1
                    IF R.PROT<EB.PTL.USER> NE PRV.USR THEN
                        PRINT STR('-',125)
                        PRV.USR = R.PROT<EB.PTL.USER>
                    END
                    XX  = SPACE(120)
                    XX2 = SPACE(120)
                    XX<1,1>[1,16]    = YYBRN
                    XX<1,1>[19,25]   = R.PROT<EB.PTL.REMARK>
                    XX2<1,1>[55,10]  = SYS.DATE
                    XX<1,1>[70,20]   = R.PROT<EB.PTL.USER>
                    XX<1,1>[88,50]   = R.PROT<EB.PTL.TIME>[1,2]:':':R.PROT<EB.PTL.TIME>[3,2]:':':R.PROT<EB.PTL.TIME>[5,2]
                    XX<1,1>[101,10]  = E.DATE.1
                    XX<1,1>[113,15]  = R.USR<EB.USE.USER.NAME>
                    PRINT XX<1,1>
                    IF SYS.DATE THEN
                        PRINT XX2<1,1>
                    END
                END
            END
        NEXT I

        PRINT STR('=',125)
        PRINT ; PRINT SPACE(5):" No. Of Trails ":TOT.TRIAL

        PRINT STR('=',125)
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END
    RETURN
*=======================================================================
PRINT.HEAD:
*----------
    R.USR  = ''
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USR<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CURR.COMP,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',1)
    YYBRN  = BRANCH
    DATY   = TODAY
    DATY   = FMT(DATY,"####/##/##")
    PR.HD  ="'L'":SPACE(1):"Suez Canal Bank "  : SPACE(90):"Branch:" :YYBRN
    PR.HD :="'L'":SPACE(1):"Date    : ":DATY:SPACE(85):"Page No.   : ":"'P'"
    PR.HD :="'L'":SPACE(1):"SBD.FAILED.LOG.2"
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(50):" Failed Attempts to LOG ON T24 TODAY ":DATY
    PR.HD :="'L'":SPACE(40):STR('_',55)


    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(2):"Branch" :SPACE(13):" Attemp. Desc. ":SPACE(13):"System.Date":SPACE(10):"User ID":SPACE(12):"Time ":SPACE(8):"End Profile"
    PR.HD :="'L'":STR('_',128)
    HEADING PR.HD
    RETURN
*==============================================================
