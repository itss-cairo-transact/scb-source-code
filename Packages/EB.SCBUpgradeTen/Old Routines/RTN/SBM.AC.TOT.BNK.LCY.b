* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>897</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM SBM.AC.TOT.BNK.LCY
    SUBROUTINE SBM.AC.TOT.BNK.LCY

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.ACCOUNT
    $INSERT T24.BP I_F.DATES
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_USER.ENV.COMMON
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT           I_AC.LOCAL.REFS
    $INSERT           I_CU.LOCAL.REFS
    $INSERT           I_F.SCB.CUS.POS.LW
*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS.OPEN
    GOSUB PRINT.ARRAY
    GOSUB PRINT.LINE.TOT
    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*---------------------------------------------------------------------
PRINT.LINE.TOT:
    PRINT SPACE(1) : STR('_',120)
    PRINT " "

    TOTAL.BALANCE        = FMT(TOTAL.BALANCE, "L2,")
    BALANCE.TOTAL.BANK   = FMT(BALANCE.TOTAL.BANK, "L2,")
    BALANCE.TOTAL.BANK.1 = FMT(BALANCE.TOTAL.BANK.1, "L2,")

    PRINT SPACE(15) : TOTAL.INDV : SPACE(10) : BALANCE.TOTAL.BANK : SPACE(13) : TOTAL.COMP : SPACE(10) : BALANCE.TOTAL.BANK.1 : SPACE(10) : TOTAL.COUNT : SPACE(10) : TOTAL.BALANCE
    PRINT " "
    RETURN
*---------------------------------------------------------------------
PRINT.ARRAY:
    FOR ROW.ARR = 1 TO ZZ
        PRINT ARRAY.LINE<1,ROW.ARR>
        PRINT " "
    NEXT ROW.ARR
    RETURN
*---------------------------------------------------------------------
LINE.END:
    WW      = SPACE(120)
    WW<1,1> = SPACE(20):"********************  ����� �������   ******************"

    PRINT " "
    PRINT WW<1,1>
    RETURN
*--------------------------------------------------------------------
INITIATE:
    ZZ = 1
    HH = 1

    TOTAL.COMP           = 0
    TOTAL.INDV           = 0
    ALL.BAL.NEW          = 0
    ALL.BAL.NEW.1        = 0
    BALANCE.TOTAL.BANK   = 0
    BALANCE.TOTAL.BANK.1 = 0

    TOT.BAL       = 0
    TOTAL.BALANCE = 0
    TOTAL.COUNT   = 0
    ARRAY.LINE    = SPACE(150)

    REPORT.ID='SBM.AC.TOTALS'
    CALL PRINTER.ON(REPORT.ID,'')
    FN.TEMP = "F.SCB.CUS.POS.LW"   ; F.TEMP = ""
    CALL OPF(FN.TEMP, F.TEMP)
    FN.CUS = 'FBNK.CUSTOMER'              ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    T.DATE    = TODAY
    KEY.CCY   = 'NZD'
*-----
    COMP.COUNT.2 = 0
    INDV.COUNT.2 = 0
    RETURN
*------------------------READ FORM TEXT FILE----------------------
PROCESS.OPEN:
    T.SEL  = "SELECT F.SCB.CUS.POS.LW WITH DEAL.CCY EQ 'EGP'"
    T.SEL := "  AND ( CO.CODE NE 'EG0010099' AND CO.CODE NE 'EG0010088' )"
    T.SEL := " AND DEAL.AMOUNT GT 0 AND CUSTOMER NE ''"
    T.SEL := " AND ( ( CATEGORY GE 1001 AND CATEGORY LE 1003 )"
    T.SEL := " OR ( CATEGORY GE 21001 AND CATEGORY LE 21010 )"
    T.SEL := " OR ( CATEGORY GE 21020 AND CATEGORY LE 21042 )"
    T.SEL := " OR ( CATEGORY GE 1300  AND CATEGORY LE 1559 )"
    T.SEL := " OR ( CATEGORY GE 1100  AND CATEGORY LE 1212 )"
    T.SEL := " OR ( CATEGORY GE 6501  AND CATEGORY LE 6504 )"
    T.SEL := " OR ( CATEGORY EQ 1216  OR CATEGORY EQ 21019 )"
    T.SEL := " OR ( CATEGORY EQ 6512  OR CATEGORY EQ 6511 )"
    T.SEL := " OR ( CATEGORY EQ 1211  OR CATEGORY EQ 1212 )"
    T.SEL := " OR ( CATEGORY EQ 1290  OR CATEGORY EQ 1599 ) )"
    T.SEL := " BY CO.CODE BY CUSTOMER"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    OLD.CO.CODE       = ''
    INDV.COUNT        = 0
    COMP.COUNT        = 0

    FOR I = 1 TO SELECTED
        CALL F.READ(FN.TEMP,KEY.LIST<I>,R.TEMP,F.TEMP,TEMP.ERR)
        CALL F.READ(FN.TEMP,KEY.LIST<I+1>,R.TEMP.1,F.TEMP,TEMP.ERR.1)

        CUS.ID      = R.TEMP<CUPOS.CUSTOMER>
        CUS.ID.1    = R.TEMP.1<CUPOS.CUSTOMER>
        COM.CODE    = R.TEMP<CUPOS.CO.CODE>
        COM.CODE.1  = R.TEMP.1<CUPOS.CO.CODE>

        IF COM.CODE EQ COM.CODE.1 THEN
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR  = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>

            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN

                    IF CUS.ID NE CUS.ID.1 THEN
                        COMP.COUNT = COMP.COUNT + 1
                    END
                    CUSTO      = CUS.ID
                    TEMP.ID    = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE

                    IF CUS.ID NE CUS.ID.1 THEN
                        INDV.COUNT = INDV.COUNT + 1
                    END
                    CUSTO.1    = CUS.ID
                    TEMP.ID    = KEY.LIST<I>
                    GOSUB CALC.BALANCE.1
                END
            END

        END ELSE
            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>

            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    IF CUS.ID NE CUS.ID.1 THEN
                        COMP.COUNT = COMP.COUNT + 1
                    END
                    CUSTO      = CUS.ID
                    TEMP.ID    = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    IF CUS.ID NE CUS.ID.1 THEN
                        INDV.COUNT = INDV.COUNT + 1
                    END
                    CUSTO.1    = CUS.ID
                    TEMP.ID    = KEY.LIST<I>
                    GOSUB CALC.BALANCE.1
                END
            END
*---------------
            ALL.COUNT = COMP.COUNT + INDV.COUNT

            OLDD      = COM.CODE[8,2]
            IF OLDD[1,1] EQ '0' THEN
                OLDD = OLDD[1]
            END

            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
            BRANCH.NAME = FIELD(BRAN.NAME,'.',2)
            CBE.BR      = COM.CODE[8,2]
            GOSUB WRITE.LINE

            COMP.COUNT   = 0
            INDV.COUNT   = 0
        END
    NEXT I
    RETURN
*-------------------------------------------------------------------*
WRITE.LINE:

    ARRAY.LINE<1,ZZ>[1,15]   = BRANCH.NAME

    ARRAY.LINE<1,ZZ>[16,10]  = INDV.COUNT
    XX.1 = ALL.BAL.NEW
    XX.1 = FMT(XX.1, "L2,")
    ARRAY.LINE<1,ZZ>[30,20]  = XX.1

    ARRAY.LINE<1,ZZ>[55,10]  = COMP.COUNT
    XX.2 = ALL.BAL.NEW.1
    XX.2 = FMT(XX.2, "L2,")
    ARRAY.LINE<1,ZZ>[70,20]  = XX.2

    ARRAY.LINE<1,ZZ>[95,10]  = ALL.COUNT
    TOT.BAL = ALL.BAL.NEW + ALL.BAL.NEW.1
    XX.3 = TOT.BAL
    XX.3 = FMT(XX.3, "L2,")
    ARRAY.LINE<1,ZZ>[110,20] = XX.3
    ZZ = ZZ + 1

    TOTAL.INDV           = TOTAL.INDV + INDV.COUNT
    TOTAL.COMP           = TOTAL.COMP + COMP.COUNT
    BALANCE.TOTAL.BANK   = BALANCE.TOTAL.BANK   + ALL.BAL.NEW
    BALANCE.TOTAL.BANK.1 = BALANCE.TOTAL.BANK.1 + ALL.BAL.NEW.1

    TOTAL.BALANCE        = TOTAL.BALANCE + TOT.BAL
    TOTAL.COUNT          = TOTAL.COUNT   + ALL.COUNT

    ALL.BAL.NEW   = 0
    ALL.BAL.NEW.1 = 0
    INDV.COUNT    = 0
    COMP.COUNT    = 0

    TOT.BAL       = 0
    ALL.COUNT     = 0
    RETURN
*------------------------------------------------------------------*
CALC.BALANCE:
    CALL F.READ(FN.TEMP,TEMP.ID,R.TEMP,F.TEMP,TEMP.ERR)
    CBE.IN.LCY  = R.TEMP<CUPOS.DEAL.AMOUNT>

    ALL.BAL.NEW = ALL.BAL.NEW +  CBE.IN.LCY
    RETURN
*----------------------------------------------------
CALC.BALANCE.1:
    CALL F.READ(FN.TEMP,TEMP.ID,R.TEMP,F.TEMP,TEMP.ERR)
    CBE.IN.LCY.1  = R.TEMP<CUPOS.DEAL.AMOUNT>

    ALL.BAL.NEW.1 = ALL.BAL.NEW.1 +  CBE.IN.LCY.1
    RETURN
*----------------------------------------------------
************************ PRINT HEAD *******************************
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(109):"SBM.AC.TOTALS.BANK.LCY"
    PR.HD :="'L'":" "

    END.DATE  = TODAY
    END.DATE  = FMT(END.DATE , "####/##/##")

    PR.HD :="'L'":SPACE(40): "����� ����� ������� ������� �� "
    PR.HD :=SPACE(3): END.DATE
    PR.HD :="'L'":SPACE(40): "(���� - ����� - ����� ���� - ������)"

    PR.HD :="'L'":SPACE(30):STR('_',60)
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(15):"���"
    PR.HD :=SPACE(15):"�����"
    PR.HD :=SPACE(15):"���"
    PR.HD :=SPACE(15):"�����"
    PR.HD :=SPACE(10):"������ ���"
    PR.HD :=SPACE(15):"������"

    PR.HD :="'L'":"�����":SPACE(10):"�������"
    PR.HD :=SPACE(10):"�����"
    PR.HD :=SPACE(13):"�������"
    PR.HD :=SPACE(15):"�������"
    PR.HD :=SPACE(7):"������� � �������"
    PR.HD :=SPACE(10):"������� ������� ������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
END
