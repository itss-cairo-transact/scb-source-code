* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.1500.2
*    PROGRAM  SBM.C.PRT.1500.2
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.BANK.FIN.POS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*-----------------------------------------------
    FN.CBE = "F.CBE.BANK.FIN.POS"
    F.CBE  = ""
    FN.BR = "F.DEPT.ACCT.OFFICER"
    F.BR = ""


*--------------------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.BR,F.BR)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""

    WS.FLAG.PRT = 0
    WS.BR = 0
    WS.H.D.T = ""
    WS = ""
* WS.T = ""
    WS.PAG = 0
    WS.ST.LCY = 0
    WS.ST.FCY = 0
    WS.T.LCY = 0
    WS.T.FCY = 0
    WS.FIN.TLCY = 0
    WS.FIN.TFCY = 0
    WS.FRST.TIME = 0
*                                                          PRINTER HEADER
    HEAD.01.TITL = "������ ������ ������"
    HEAD.01.PROG = "PROGRAM SBM.C.PRT.1500.2"
    HEAD.01 = SPACE(50):HEAD.01.TITL:SPACE(40):HEAD.01.PROG
*.....................................................
    HEAD.02.PAG = "��� ����":" : "
    HEAD.02.TITL = "���������� � ���� ���������"
    HEAD.02 = SPACE(50):HEAD.02.TITL:SPACE(43):HEAD.02.PAG
*.....................................................
    HEAD.03.A = "���� �����"
    HEAD.03.B = "���� ������"
    HEAD.03.C = "��������"
    HEAD.03 = SPACE(50):HEAD.03.A:SPACE(5):HEAD.03.B:SPACE(5):HEAD.03.C
********************** ********************************
*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE

*------------------------------------------START PROCESSING
*   GOSUB A.5000.PRT.HEAD

    WS.BR.PRNT = 1
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
**** GOTO A.050
    WS.BR.PRNT = 6
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

 *******GOTO A.050
    WS.BR.PRNT = 13
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 32
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 21
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 23
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 70
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 7
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 40
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 50
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 31
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 22
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 5
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.BR.PRNT = 90
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 14
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 30
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 2
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 4
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 20
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 80
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 60
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 15
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 35
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 9
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 12
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 81
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 3
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 10
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE

    WS.BR.PRNT = 11
    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
A.050:
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*------------------------------------------------

*------------------------------------------------
A.100.PROCESS:
*    SEL.CMD = "SELECT ":FN.CBE:" BY @ID"
*    SEL.CMD = "SELECT ":FN.CBE:" BY FIN.POS.BR BY FIN.POS.LIN.NO":" WITH FIN.POS.BR EQ 1"
    SEL.CMD = "SELECT ":FN.CBE:" BY FIN.POS.LIN.NO":" WITH FIN.POS.BR EQ ":WS.BR.PRNT
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS


        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        WS.AST.LIAB = WS.CBE.ID[1,1]
        IF WS.AST.LIAB = 1  THEN
            GOTO A.100.A
        END
        IF WS.FRST.TIME EQ 0 THEN
            WS.FRST.TIME = 1
            WS.BR = R.CBE<CBE.FIN.POS.BR>
            GOSUB A.5000.PRT.HEAD
            WS.FIN.TLCY = 0
            WS.FIN.TFCY = 0
            WS.COMN = 0
            WS.AMT.LCY = 0
            WS.AMT.FCY = 0
            WS.ST.LCY = 0
            WS.ST.FCY = 0
            WS.T.LCY = 0
            WS.T.FCY = 0
            WS.FIN.TLCY = 0
            WS.FIN.TFCY = 0
        END

        IF   R.CBE<CBE.FIN.POS.BR>  NE WS.BR   THEN
            WS.BR = R.CBE<CBE.FIN.POS.BR>
            GOSUB A.5000.PRT.HEAD
            WS.FIN.TLCY = 0
            WS.FIN.TFCY = 0
            WS.COMN = 0
            WS.AMT.LCY = 0
            WS.AMT.FCY = 0
            WS.ST.LCY = 0
            WS.ST.FCY = 0
            WS.T.LCY = 0
            WS.T.FCY = 0
            WS.FIN.TLCY = 0
            WS.FIN.TFCY = 0
        END
        WS.LINE.NO = R.CBE<CBE.FIN.POS.LIN.NO>
        WS.DESCR = R.CBE<CBE.FIN.POS.DSCRPTION>
        WS.REC.TYPE = R.CBE<CBE.FIN.POS.REC.TYPE>

        WS.AMT.LCY = R.CBE<CBE.FIN.POS.AMT.LCY>
        WS.AMT.FCY = R.CBE<CBE.FIN.POS.AMT.FCY>

        IF WS.REC.TYPE EQ "H" THEN
            GOSUB A.200.PRT.H
        END

        IF WS.REC.TYPE EQ "SH" THEN
            GOSUB A.210.PRT.SH
        END

        IF WS.REC.TYPE EQ "HD" THEN
            GOSUB A.220.PRT.HD
        END

        IF WS.REC.TYPE EQ "ST" THEN
            GOSUB A.230.PRT.ST
        END

        IF WS.REC.TYPE EQ "T" THEN
            GOSUB A.240.PRT.T
        END

        IF WS.REC.TYPE EQ "D" THEN
            WS.REF  = WS.CBE.ID
            GOSUB A.250.PRT.D
        END
        IF WS.LINE.NO EQ 2040400  THEN
            GOSUB A.5000.PRT.HEAD
        END
        IF WS.LINE.NO  EQ 2089999  THEN
            GOSUB A.5000.PRT.HEAD
        END

A.100.A:
    REPEAT
    RETURN
*----------------------------------------------------------
A.200.PRT.H:
    XX = SPACE(132)
    XX<1,1>[16,50]   = WS.DESCR
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[16,50]   = STR('_',50)
    PRINT XX<1,1>

    RETURN
*----------------------------------------------------------
A.210.PRT.SH:
    XX = SPACE(132)
    XX<1,1>[19,46]   = WS.DESCR
    PRINT XX<1,1>
    RETURN
*---------------------------------------------------------
A.220.PRT.HD:
    WS.FIN.TLCY = WS.FIN.TLCY + WS.AMT.LCY
*.........................................
    WS.FIN.TFCY = WS.FIN.TFCY + WS.AMT.FCY
*.........................................
    WS.COMN = WS.AMT.LCY + WS.AMT.FCY
    XX = SPACE(132)
    XX<1,1>[16,50]   = WS.DESCR
    XX<1,1>[70,15]   = FMT(WS.AMT.LCY, "R0,")
    XX<1,1>[89,15]   = FMT(WS.AMT.FCY, "R0,")
    XX<1,1>[110,15]   = FMT(WS.COMN, "R0,")

    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[70,15]   = "=========="
    XX<1,1>[89,15]   = "=========="
    XX<1,1>[110,15]   = "=========="
    PRINT XX<1,1>
    RETURN
*--------------------------------------------------------
A.230.PRT.ST:
    XX = SPACE(132)
    XX<1,1>[70,15]   = "----------"
    XX<1,1>[89,15]   = "----------"
    XX<1,1>[110,15]   = "----------"
    PRINT XX<1,1>

    WS.COMN = WS.ST.LCY + WS.ST.FCY
    XX = SPACE(132)
    XX<1,1>[16,50]    = WS.DESCR
    XX<1,1>[70,15]   = FMT(WS.ST.LCY, "R0,")
    XX<1,1>[89,15]   = FMT(WS.ST.FCY, "R0,")
    XX<1,1>[110,15]   = FMT(WS.COMN, "R0,")
    PRINT XX<1,1>

    XX = SPACE(132)
    XX<1,1>[70,15]   = "----------"
    XX<1,1>[89,15]   = "----------"
    XX<1,1>[110,15]   = "----------"
    PRINT XX<1,1>
    WS.ST.LCY = 0
*.........................................
    WS.ST.FCY = 0
    RETURN
*--------------------------------------------------------
A.240.PRT.T:
    XX = SPACE(132)
    XX<1,1>[70,15]   = "=========="
    XX<1,1>[89,15]   = "=========="
    XX<1,1>[110,15]   = "=========="
    PRINT XX<1,1>

*    WS.COMN = WS.AMT.LCY + WS.AMT.FCY
    WS.COMN = WS.T.LCY + WS.T.FCY
*..................................������ ������
    IF WS.LINE.NO EQ 2129999  THEN
        WS.COMN = WS.FIN.TLCY + WS.FIN.TFCY
        XX = SPACE(132)
        XX<1,1>[16,50]    = WS.DESCR
        XX<1,1>[70,15]   = FMT(WS.FIN.TLCY, "R0,")
        XX<1,1>[89,15]   = FMT(WS.FIN.TFCY, "R0,")
        XX<1,1>[110,15]   = FMT(WS.COMN, "R0,")
        PRINT XX<1,1>
    END
*.......................................����� ������ ������ �������

    IF WS.LINE.NO NE 2129999  THEN
        XX = SPACE(132)
        XX<1,1>[16,50]    = WS.DESCR
        XX<1,1>[70,15]   = FMT(WS.T.LCY, "R0,")
        XX<1,1>[89,15]   = FMT(WS.T.FCY, "R0,")
        XX<1,1>[110,15]   = FMT(WS.COMN, "R0,")
        PRINT XX<1,1>
    END
    XX = SPACE(132)
    XX<1,1>[70,15]   = "=========="
    XX<1,1>[89,15]   = "=========="
    XX<1,1>[110,15]   = "=========="
    PRINT XX<1,1>
    WS.ST.LCY = 0
    WS.ST.FCY = 0
    WS.T.LCY = 0
    WS.T.FCY = 0
    RETURN
*--------------------------------------------
A.250.PRT.D:
    WS.ST.LCY = WS.ST.LCY + WS.AMT.LCY
    WS.T.LCY = WS.T.LCY + WS.AMT.LCY

    WS.FIN.TLCY = WS.FIN.TLCY + WS.AMT.LCY
*.........................................
    WS.ST.FCY = WS.ST.FCY + WS.AMT.FCY
    WS.T.FCY = WS.T.FCY + WS.AMT.FCY
    WS.FIN.TFCY = WS.FIN.TFCY + WS.AMT.FCY
*.........................................
    WS.COMN = WS.AMT.LCY + WS.AMT.FCY
    XX = SPACE(132)
    XX<1,1>[1,10]    = WS.REF
    XX<1,1>[22,43]    = WS.DESCR
    XX<1,1>[70,15]   = FMT(WS.AMT.LCY, "R0,")
    XX<1,1>[89,15]   = FMT(WS.AMT.FCY, "R0,")
    XX<1,1>[110,15]   = FMT(WS.COMN, "R0,")
    PRINT XX<1,1>
    RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    CALL F.READ(FN.BR,WS.BR,R.BR,F.BR,MSG.BR)
    WS.BR.NAME = R.BR<EB.DAO.NAME>

    WS.PAG = WS.PAG + 1
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":HEAD.01
    PR.HD :="'L'":HEAD.02:WS.PAG
    PR.HD :="'L'":HEAD.03
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
    PRINT
    HEADING PR.HD
    RETURN
*------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
