* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* ---------------CREATED BY RIHAM YOUSSEF 20180111---------------------------
*-----------------------------------------------------------------------------
    PROGRAM SBD.CD.AND.LD
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.RENEW.METHOD
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "SBM.CD.AND.LD.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBM.CD.AND.LD.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBM.CD.AND.LD.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBM.CD.AND.LD.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBM.CD.AND.LD.CSV File IN &SAVEDLISTS&'
        END
    END
****************************************************************************

    WS.COMP    = ''
    CUS.ID     = ''
    CUST.NAME  = ''
    STAFF.Y    = ''
    WS.AC      = ''
    WS.CATEG   = ''
    DESC       = ''
    TRNS.ID    = ''
    CUST.SECTOR= ''
    WS.CCY     = ''
    WS.LD.RATE = 0
    WS.RATE    = 0
    WS.SPREAD  = 0
    WS.AMT.FCY = 0
    WS.AMT     = 0
    WS.VAL.D = ''
    WS.MAT.D = ''
    DAY.FIN     = ''
    WS.RENEW.IND   = ''
    WS.RENEW.NAME  = ''
    DAT.1 = ''
    DAT.2 = ''
    BI.ID.KEY = ''

    TD = TODAY
    DAT.1 = TD[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,DAT.2)



    HEAD.DESC  = "BRANCH":","
    HEAD.DESC := "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "STAFF(Y/N)":","
    HEAD.DESC := "FULL ACCOUNT NO.":","
    HEAD.DESC := "CATEGORY CODE ":","
    HEAD.DESC := "PRODUCT NAME ":","
    HEAD.DESC := "LD NUMBER":","
    HEAD.DESC := "CUSTOMER SEGMENT":","
    HEAD.DESC := "CURRENCY":","
    HEAD.DESC := "DEFAULT RATE":","
    HEAD.DESC := "EXECPTIONAL RATE ":","
    HEAD.DESC := "SPREAD.RATE":","
    HEAD.DESC := "AMOUNT IN ORIGINAL":","
    HEAD.DESC := "AMOUNT EQUIVALENT IN EGP":","
    HEAD.DESC := "DEAL START DATE":","
    HEAD.DESC := "DEAL MATURITY DATE":","
    HEAD.DESC := "DEAL TENOR ":","
    HEAD.DESC := "DEAL INSTRUCTIONS":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*========================================================================
PROCESS:
    FN.CBE = 'F.SCB.CUS.POS.LW' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = ''
    CALL OPF(FN.GCI,F.GCI)

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)


    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.SEC.N = 'F.SCB.NEW.SECTOR' ; F.SEC.N = ''
    CALL OPF(FN.SEC.N,F.SEC.N)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.BID = 'FBNK.BASIC.INTEREST.DATE' ; F.BID = ''
    CALL OPF(FN.BID,F.BID)

    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)

    FN.AD = 'FBNK.ACCOUNT.DATE' ; F.AD = ''
    CALL OPF(FN.AD,F.AD)

    FN.RE = 'F.SCB.LD.RENEW.METHOD' ; F.RE = ''
    CALL OPF(FN.RE,F.RE)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

******* TIME DEPOSITS SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21011) OR (CATEGORY EQ 21013) OR (CATEGORY EQ 6512)) AND LCY.AMOUNT GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            DESC = 'Time.Deposits'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
************************************************************
******* CDs SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 21017 AND CATEGORY LE 21029) OR (CATEGORY EQ 21032 OR CATEGORY EQ 21041 OR CATEGORY EQ 21042)) AND LCY.AMOUNT GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            DESC = 'CDs'
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
    RETURN
*==============================================================
PRINT.DET:
    BB.DATA  = WS.COMP:","
    BB.DATA := CUS.ID:","
    BB.DATA := CUST.NAME:","
    BB.DATA := STAFF.Y:","
    BB.DATA := WS.AC:","
    BB.DATA := WS.CATEG:","
    BB.DATA := CATEG:","
    BB.DATA := TRNS.ID:","
    BB.DATA := CUST.SECTOR:","
    BB.DATA := WS.CCY:","
    BB.DATA := WS.LD.RATE:","
    BB.DATA := WS.RATE:","
    BB.DATA := WS.SPREAD:","
    BB.DATA := WS.AMT.FCY:","
    BB.DATA := WS.AMT:","
    BB.DATA := WS.VAL.DATE:","
    BB.DATA := WS.MAT.DATE:","
    BB.DATA := DAY.FIN:","
    BB.DATA := RENEW.NAME:","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*===============================================================
GET.DETAIL:
    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)

    WS.CATEG    = R.CBE<CUPOS.CATEGORY>
    CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<1,2>,WS.CATEG,CATEG)
    CUS.ID      = R.CBE<CUPOS.CUSTOMER>

    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
    CUST.NAME            = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
    CUST.BANK            = R.CU<EB.CUS.LOCAL.REF><1,CULR.CU.BANK>
    CUST.SECTOR.CODE     = R.CU<EB.CUS.SECTOR>
    CUST.SECTOR.CODE.NEW = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
    WS.INDUSTRY          = R.CU<EB.CUS.INDUSTRY>

    CALL F.READ(FN.SEC.N,CUST.SECTOR.CODE.NEW,R.SEC.N,F.SEC.N,E1)
    SECTOR.NAME = R.SEC.N<C.SCB.NEW.SECTOR.NAME>

    IF CUST.SECTOR.CODE EQ 1100 THEN
        STAFF.Y = 'Y'
    END ELSE
        STAFF.Y = 'N'
    END
*****************************************************************************
    IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1300 OR CUST.SECTOR.CODE EQ 1400 OR CUST.SECTOR.CODE EQ 2000 THEN
        IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1300 THEN
            CUST.SECTOR = 'Staff'
        END

        IF CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1400 THEN
            CUST.SECTOR = 'Staff.Relatives'
        END
        IF CUST.SECTOR.CODE EQ 2000 THEN
            CUST.SECTOR = 'Retail'
        END
    END ELSE
        CUST.SECTOR = 'Corporate'
    END
********************************************************************
    WS.CCY      = R.CBE<CUPOS.DEAL.CCY>
    WS.AMT.FCY  = R.CBE<CUPOS.DEAL.AMOUNT>

    IF DAT.1 EQ DAT.2 THEN
        CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 298 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE WS.CCY IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
        AMT.RATE = R.CCY<RE.BCP.RATE,POS>
        WS.AMT   = WS.AMT.FCY * AMT.RATE
    END ELSE
        WS.AMT   = R.CBE<CUPOS.LCY.AMOUNT>
    END

    APP.REF.ID  = FIELD(KEY.LIST<I>,"*",2)
    TRNS.ID     = FIELD(KEY.LIST<I>,"*",5)
    WS.COMP     = R.CBE<CUPOS.CO.CODE>

    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRN.NAME)

    WS.MAT.DATE = R.CBE<CUPOS.MATURITY.DATE>
    WS.VAL.DATE = R.CBE<CUPOS.VALUE.DATE>
    WS.RATE     = R.CBE<CUPOS.INT.RATE>

    CALL F.READ(FN.LD,TRNS.ID,R.LD,F.LD,E1)
    WS.RATE           = R.LD<LD.INTEREST.RATE>
    WS.RATE.TYPE.CODE = R.LD<LD.INT.RATE.TYPE>
    WS.SPREAD         = R.LD<LD.INTEREST.SPREAD>
    WS.LD.RATE        = R.LD<LD.INTEREST.RATE>

    IF  WS.MAT.DATE NE '' AND WS.VAL.DATE NE '' THEN
        DAY.FIN = "C"
        CALL CDD("",WS.VAL.DATE,WS.MAT.DATE,DAY.FIN)
    END

    WS.LD.INV.AMT     = R.LD<LD.LOCAL.REF><1,LDLR.INVESTED.AMOUNT>
    WS.RENEW.IND      = R.LD<LD.LOCAL.REF><1,LDLR.RENEW.IND>
    WS.RENEW.METHOD   = R.LD<LD.LOCAL.REF><1,LDLR.RENEW.METHOD>
    CALL DBR('SCB.LD.RENEW.METHOD':@FM:LD.REN.DESCRIPTION,WS.RENEW.METHOD,RENEW.NAME)


    WS.AC             = R.LD<LD.DRAWDOWN.ACCOUNT>

    IF WS.LD.RATE NE '' THEN
        WS.RATE = WS.SPREAD + WS.LD.RATE
    END

    IF WS.LD.RATE EQ '' THEN
        WS.LD.KEY = R.LD<LD.INTEREST.KEY>
        BI.ID.KEY = WS.LD.KEY:WS.CCY

        CALL F.READ(FN.BID,BI.ID.KEY,R.BID,F.BID,E4)
        BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
        BI.ID = BI.ID.KEY:BI.DATE
        CALL F.READ(FN.BI,BI.ID,R.BI,F.BI,E3)
        WS.RATE = WS.SPREAD + R.BI<EB.BIN.INTEREST.RATE>
    END

***********************************************************
    IF  WS.CATEG EQ '6512' THEN
        WS.AC = TRNS.ID
*********** SELECT FROM ACI *****
        CALL F.READ(FN.AC,TRNS.ID,R.AC,F.AC,E1)
        CUST.NAME = R.AC<AC.ACCOUNT.TITLE.1>
        GROUP.ID  = R.AC<AC.CONDITION.GROUP>
        WS.VAL.DATE = R.AC<AC.OPENING.DATE>
        CALL F.READ(FN.GC,GROUP.ID,R.GC,F.GC,E1)
        WS.MAT.DATE  = R.GC<IC.GCP.CR.CAP.FREQUENCY>[1,8]

*Line [ 361 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.AC.COUNT = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
        AD.DATE     = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
        ACI.ID      = TRNS.ID:'-':AD.DATE

        CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 367 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        WS.RATE.COUNT     = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
        WS.LD.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>
        IF WS.LD.RATE EQ '' THEN

**** SELECT FROM BASIC.INTEREST ****
            ACI.BI.KEY       = R.ACI<IC.ACI.CR.BASIC.RATE>
            ACI.BI.ID.KEY    = ACI.BI.KEY:WS.CCY
            ACI.OPER         = R.ACI<IC.ACI.CR.MARGIN.OPER>
            WS.SPREAD        = R.ACI<IC.ACI.CR.MARGIN.RATE>


            CALL F.READ(FN.BID,ACI.BI.ID.KEY,R.BID,F.BID,E4)
            ACI.BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
            ACI.BI.ID   = ACI.BI.ID.KEY:ACI.BI.DATE

            CALL F.READ(FN.BI,ACI.BI.ID,R.BI,F.BI,E3)

            WS.LD.RATE = R.BI<EB.BIN.INTEREST.RATE>
            IF ACI.OPER EQ 'SUBTRACT' THEN
                WS.RATE = WS.LD.RATE - WS.SPREAD
            END

            IF ACI.OPER EQ 'ADD' THEN
                WS.RATE = WS.LD.RATE + WS.SPREAD
            END


            IF WS.RATE EQ '' THEN
                WS.RATE = WS.LD.RATE
            END


            IF WS.LD.RATE EQ '' THEN

**** SELECT RATE FROM GCI ****

                GD.ID   = GROUP.ID:WS.CCY
                CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
                GD.DATE = R.GD<AC.GRD.CREDIT.GROUP.DATE>
                IF GD.DATE EQ '' THEN
                    GD.DATE = R.GD<AC.GRD.CREDIT.DATES,1>
                END
                GCI.ID  = GD.ID:GD.DATE

                CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
*Line [ 413 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                WS.RATE.COUNT   = DCOUNT(R.GCI<IC.GCI.CR.INT.RATE>,@VM)

                FOR KK = 1 TO WS.RATE.COUNT
                    WS.AMT.POS   = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK>
                    IF KK = 1 THEN
                        WS.AMT.POS.1 = 0
                    END ELSE
                        WS.AMT.POS.1 = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK-1>
                    END
                    IF WS.AMT GT WS.AMT.POS.1 AND WS.AMT LE WS.AMT.POS THEN
                        WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,KK>
                    END
                    WS.LAST.AMT = R.GCI<IC.GCI.CR.LIMIT.AMT><1,WS.RATE.COUNT-1>
                    IF WS.AMT GT WS.LAST.AMT THEN
                        WS.LD.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,WS.RATE.COUNT>
                    END
                NEXT KK

                WS.OPER   = R.GCI<IC.GCI.CR.MARGIN.OPER>
                WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE,1>
                IF WS.SPREAD = '' THEN
                    WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE,2>
                END

                IF WS.OPER EQ 'SUBTRACT' THEN
                    WS.RATE = WS.LD.RATE - WS.SPREAD
                END
                IF WS.OPER EQ 'ADD' THEN
                    WS.RATE = WS.LD.RATE + WS.SPREAD
                END
                IF WS.RATE EQ '' THEN

                    WS.RATE = WS.LD.RATE
                END

            END
        END

    END
    IF WS.CATEG EQ '6512' THEN
        IF WS.RATE EQ '0' THEN
            WS.RATE = WS.LD.RATE
        END
    END
    RETURN

*===============================================================
