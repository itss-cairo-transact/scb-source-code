* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1266</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.MATCH.DEPOSIT.FR.ALL

***    PROGRAM SBD.MATCH.DEPOSIT.FR.ALL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
    $INSERT T24.BP  I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*-------------------------------------------------------------------------
*    REPORT.ID='P.FUNCTION'
    REPORT.ID='SBD.MATCH.DEPOSIT.FR.ALL'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    PROGRAM.NAME = 'SBD.MATCH.DEPOSIT.FR.ALL'
*-------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    FOR X = 1 TO NO.CURR

        GOSUB MOVE.ZEROS

        PRINT.FLAG = 'NO'

        GOSUB PROCESS
        GOSUB TOTAL.REPORT

        IF PRINT.FLAG EQ 'YES' THEN

            GOSUB MOVE.ZEROS

            CALL F.READ(FN.CURRENCY,AR.CURR(X),R.CURRENCY,F.CURRENCY,ECCY)
            CURR.NAME = R.CURRENCY<EB.CUR.CCY.NAME,2>
            HEAD.B1 = CURR.NAME
            GOSUB PRINT.HEAD
            GOSUB PROCESS
            GOSUB TOTAL.REPORT

        END

    NEXT X
*-------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:

    TXT1 = "�����"
    TXT2 = "�����"
    TXT3 = "�����"
    TXT4 = "������"

    H.BRANCH  = "�� ���� �����"


    HEAD.A1 = "���� �������� ������� �� ����� ������� ���������"
*    HEAD.B1 = "�������� �������� ����� ����"

    HEAD.B2 = "���� ������"
*    HEAD.B2 = "���� ����� ���"
    HEAD.B2 = " ��� ���� ����� ������"
    S.HEAD1 = "�������� �������"
    S.HEAD2 = "������ �������"
    S.HEAD3 = "������ ��������"
    S.HEAD4 = "������� �������"
    S.HEAD5 = "������ ��������"

*----------------------------------
    FN.COMP = "FBNK.COMPANY"
    F.COMP  = ""
    R.COMP = ""
    Y.COMP.ID = ""

*----------------------------------

    FN.LINE = 'F.RE.STAT.LINE.BAL'
    F.LINE = ''

    CALL OPF(FN.LINE,F.LINE)

*----------------------------------
    DIM AR.CURR(50)
    DIM AR.RATE(50)
    DIM AR.TYPE(50)

    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)

    CUR.COD = R.CUR<RE.BCP.ORIGINAL.CCY>
*Line [ 121 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    NO.CURR = DCOUNT(CUR.COD,@VM)

    FOR POS = 1 TO NO.CURR

        AR.CURR(POS) = R.CUR<RE.BCP.ORIGINAL.CCY,POS>

        AR.RATE(POS) = R.CUR<RE.BCP.RATE,POS>
        AR.RATE(POS) = 1

        AR.TYPE(POS) = R.CUR<RE.BCP.RATE.TYPE,POS>

    NEXT POS

*----------------------------------
    FN.CURRENCY = 'FBNK.CURRENCY'
    F.CURRENCY  = ''
    R.CURRENCY = ''
    Y.CURRENCY.ID = ''
    CALL OPF(FN.CURRENCY,F.CURRENCY)
*----------------------------------
    FN.DATE = "F.DATES"
    F.DATE  = ""
    R.DATE = ""
    Y.DATE.ID = ""

    CALL OPF (FN.DATE,F.DATE)
*----------------------------------

    Y.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,Y.DATE.ID,R.DATE,F.DATE,ERR.DATE)
    DAT = R.DATE<EB.DAT.LAST.PERIOD.END>
    L.WS.DATE = R.DATE<EB.DAT.LAST.WORKING.DAY>
    SYS.DATE = TODAY
    P.DATE   = FMT(SYS.DATE,"####/##/##")
    P.WS.DATE = FMT(L.WS.DATE,"####/##/##")


    RETURN
*========================================================================
PROCESS:


    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010077' AND @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"


    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SEL.COMP,ER.MSG3)



    FOR I = 1 TO SEL.COMP


        AGL.BAL = 0
        CER.BAL = 0
        SAV.BAL = 0
        CUR.BAL = 0
        OTR.BAL = 0
* ---------------------------
        AGL.BALX = 0
        CER.BALX = 0
        SAV.BALX = 0
        CUR.BALX = 0
        OTR.BALX = 0


        COMP = KEY.LIST3<I>

*********************************************************************************************************



        XCURR = AR.CURR(X)
        XRATE = AR.RATE(X)
        XTYPE = AR.TYPE(X)



        IDD1 = 'GENLED-0710':'-':XCURR:'-':DAT:'*':COMP
        CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
        CUR.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        CUR.BAL1X = R.LINE<RE.SLB.OPEN.BAL>

*----------------------
        IF COMP EQ 'EG0010011' THEN
            IDD2 = 'GENLED-0725':'-':XCURR:'-':DAT:'*':COMP
        END

        IF COMP NE 'EG0010011' THEN
            IDD2 = 'GENLED-0720':'-':XCURR:'-':DAT:'*':COMP
        END

        CALL F.READ(FN.LINE,IDD2,R.LINE,F.LINE,E)
        SAV.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        SAV.BAL1X = R.LINE<RE.SLB.OPEN.BAL>
*----------------------
        IF COMP EQ 'EG0010011' THEN
            IDD3 = 'GENLED-0727':'-':XCURR:'-':DAT:'*':COMP
        END

        IF COMP NE 'EG0010011' THEN
            IDD3 = 'GENLED-0730':'-':XCURR:'-':DAT:'*':COMP
            IDD3X = 'GENLED-0735':'-':XCURR:'-':DAT:'*':COMP
        END

        CALL F.READ(FN.LINE,IDD3,R.LINE,F.LINE,E)
        AGL.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        AGL.BAL1X = R.LINE<RE.SLB.OPEN.BAL>

        CALL F.READ(FN.LINE,IDD3X,R.LINE,F.LINE,E)
        AGL.BAL3X  = R.LINE<RE.SLB.CLOSING.BAL.LCL>
        AGL.BAL3Y  = R.LINE<RE.SLB.OPEN.BAL.LCL>

        AGL.BAL1   =  AGL.BAL1 + AGL.BAL3X
        AGL.BAL1X  =  AGL.BAL1X + AGL.BAL3Y

        AGL.BAL3X  = 0
        AGL.BAL3Y  = 0

*----------------------
        IDD4 = 'GENLED-0723':'-':XCURR:'-':DAT:'*':COMP
        CALL F.READ(FN.LINE,IDD4,R.LINE,F.LINE,E)
        CER.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        CER.BAL1X = R.LINE<RE.SLB.OPEN.BAL>
*----------------------

*        CER.BAL1  = 0
*        CER.BAL1X = 0
*        CER.BAL1Y = 0

        OTR.BAL1  = 0
        OTR.BAL1X = 0
        OTR.BAL1Y = 0
*********************************************************************************************************
*********************************************************************************************************
*********************************************************************************************************
        AGL.BAL += (AGL.BAL1 * XRATE)
        CER.BAL += (CER.BAL1 * XRATE)
        SAV.BAL += (SAV.BAL1 * XRATE)
        CUR.BAL += (CUR.BAL1 * XRATE)
        OTR.BAL += (OTR.BAL1 * XRATE)
*************************************************
        AGL.BALX += (AGL.BAL1X * XRATE)
        CER.BALX += (CER.BAL1X * XRATE)
        SAV.BALX += (SAV.BAL1X * XRATE)
        CUR.BALX += (CUR.BAL1X * XRATE)
        OTR.BALX += (OTR.BAL1X * XRATE)

*********************************************************************************************************
        AGL.BAL = FMT((AGL.BAL)/1000,"L0")
        CER.BAL = FMT((CER.BAL)/1000,"L0")
        SAV.BAL = FMT((SAV.BAL)/1000,"L0")
        CUR.BAL = FMT((CUR.BAL)/1000,"L0")
        OTR.BAL = FMT((OTR.BAL)/1000,"L0")
*************************************************
        AGL.BALX = FMT((AGL.BALX)/1000,"L0")
        CER.BALX = FMT((CER.BALX)/1000,"L0")
        SAV.BALX = FMT((SAV.BALX)/1000,"L0")
        CUR.BALX = FMT((CUR.BALX)/1000,"L0")
        OTR.BALX = FMT((OTR.BALX)/1000,"L0")
*********************************************************************************************************


        AGL.BALY = AGL.BAL - AGL.BALX
        CER.BALY = CER.BAL - CER.BALX
        SAV.BALY = SAV.BAL - SAV.BALX
        CUR.BALY = CUR.BAL - CUR.BALX
        OTR.BALY = OTR.BAL - OTR.BALX

        TOT.BALY = AGL.BALY + CER.BALY + SAV.BALY + CUR.BALY + OTR.BALY
*********************************************************************************************************
*        PRINT AGL.BAL:"    ":AGL.BALX:"    ":AGL.BALY
*        PRINT CUR.BAL:"    ":CUR.BALX:"    ":CUR.BALY
*        PRINT SAV.BAL:"    ":SAV.BALX:"    ":SAV.BALY

        T.AGL.BAL += AGL.BAL
        T.CER.BAL += CER.BAL
        T.SAV.BAL += SAV.BAL
        T.CUR.BAL += CUR.BAL
        T.OTR.BAL += OTR.BAL


        T.AGL.BALX += AGL.BALX
        T.CER.BALX += CER.BALX
        T.SAV.BALX += SAV.BALX
        T.CUR.BALX += CUR.BALX
        T.OTR.BALX += OTR.BALX


        T.AGL.BALY += AGL.BALY
        T.CER.BALY += CER.BALY
        T.SAV.BALY += SAV.BALY
        T.CUR.BALY += CUR.BALY
        T.OTR.BALY += OTR.BALY
        T.TOT.BALY += TOT.BALY

*********************************************************************************************************

        IF PRINT.FLAG EQ 'YES' THEN

            CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
            YYBRN  = BRANCH

            XX= SPACE(140)

            XX<1,1>[1,13]   = YYBRN

            XX<1,1>[14,1]   = '|'
            XX<1,1>[15,10]  = CUR.BALX
            XX<1,1>[25,10]  = CUR.BAL
            XX<1,1>[35,8]   = CUR.BALY

            XX<1,1>[43,1]   = '|'
            XX<1,1>[44,10]  = SAV.BALX
            XX<1,1>[54,10]  = SAV.BAL
            XX<1,1>[64,8]   = SAV.BALY

            XX<1,1>[72,1]   = '|'
            XX<1,1>[73,10]  = CER.BALX
            XX<1,1>[83,10]  = CER.BAL
            XX<1,1>[93,8]   = CER.BALY

            XX<1,1>[101,1]   = '|'
            XX<1,1>[102,10]  = AGL.BALX
            XX<1,1>[112,10]  = AGL.BAL
            XX<1,1>[122,8]   = AGL.BALY

            XX<1,1>[130,1]   = '|'
            XX<1,1>[131,8]   = TOT.BALY

            PRINT XX<1,1>
*-------------------------------------------------
            XX= SPACE(140)

            XX<1,1>[1,13]    = '_____________'
            XX<1,1>[14,1]    = '|'
            XX<1,1>[15,10]   = '__________'
            XX<1,1>[25,10]   = '__________'
            XX<1,1>[35,8]    = '________'
            XX<1,1>[43,1]    = '|'
            XX<1,1>[44,10]   = '__________'
            XX<1,1>[54,10]   = '__________'
            XX<1,1>[64,8]    = '________'
            XX<1,1>[72,1]    = '|'
            XX<1,1>[73,10]   = '__________'
            XX<1,1>[83,10]   = '__________'
            XX<1,1>[93,8]    = '________'
            XX<1,1>[101,1]   = '|'
            XX<1,1>[102,10]  = '__________'
            XX<1,1>[112,10]  = '__________'
            XX<1,1>[122,8]   = '________'
            XX<1,1>[130,1]   = '|'
            XX<1,1>[131,8]   = '________'

            PRINT XX<1,1>

        END


    NEXT I

*********************************************************************************************************

    IF PRINT.FLAG EQ 'YES' THEN

*    PRINT STR('=',140)

        XX= SPACE(140)

        XX<1,1>[1,13]  = "������ �����"

        XX<1,1>[14,1]   = '|'
        XX<1,1>[15,10]  = T.CUR.BALX
        XX<1,1>[25,10]  = T.CUR.BAL
        XX<1,1>[35,8]   = T.CUR.BALY

        XX<1,1>[43,1]   = '|'
        XX<1,1>[44,10]  = T.SAV.BALX
        XX<1,1>[54,10]  = T.SAV.BAL
        XX<1,1>[65,8]   = T.SAV.BALY

        XX<1,1>[72,1]   = '|'
        XX<1,1>[73,10]  = T.CER.BALX
        XX<1,1>[83,10]  = T.CER.BAL
        XX<1,1>[93,8]   = T.CER.BALY

        XX<1,1>[101,1]   = '|'
        XX<1,1>[102,10]  = T.AGL.BALX
        XX<1,1>[112,10]  = T.AGL.BAL
        XX<1,1>[122,8]   = T.AGL.BALY

        XX<1,1>[130,1]   = '|'
        XX<1,1>[131,8]   = T.TOT.BALY

        PRINT XX<1,1>
        PRINT STR('_',138)

    END
*-------------------------------------------------

    RETURN
*********************************************************************************************************
TOTAL.REPORT:

    TT.BALX = T.CUR.BALX + T.SAV.BALX + T.CER.BALX + T.AGL.BALX
    TT.BAL  = T.CUR.BAL  + T.SAV.BAL  + T.CER.BAL  + T.AGL.BAL
    TT.BALY = T.CUR.BALY + T.SAV.BALY + T.CER.BALY + T.AGL.BALY

*-----------------------------------------------------
    IF PRINT.FLAG EQ 'YES' THEN
        XX= SPACE(140)
        PRINT XX<1,1>
        PRINT XX<1,1>
        PRINT XX<1,1>
        PRINT XX<1,1>
        PRINT XX<1,1>
*-----------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[30,20]  = '____________________'
        XX<1,1>[50,20]  = '____________________'
        XX<1,1>[70,20]  = '____________________'
        XX<1,1>[90,15]  = '_______________'
        PRINT XX<1,1>
*-----------------------------------------------------
        XX= SPACE(140)

        XX<1,1>[29,1]   = '|'
        XX<1,1>[30,19]  = "������ ��������"
        XX<1,1>[49,1]   = '|'
        XX<1,1>[50,19]  = "����������"
        XX<1,1>[69,1]   = '|'
        XX<1,1>[70,19]  = "�����������"
        XX<1,1>[89,1]   = '|'
        XX<1,1>[90,14]  = "�����������"
        XX<1,1>[104,1]  = '|'
        PRINT XX<1,1>
*---------------------------
        GOSUB R.MOVE.LINE
*-----------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[29,1]   = '|'
        XX<1,1>[30,19]  = S.HEAD1
        XX<1,1>[49,1]   = '|'
        XX<1,1>[50,19]  = FMT(T.CUR.BALX,"L0,")
        XX<1,1>[69,1]   = '|'
        XX<1,1>[70,19]  = FMT(T.CUR.BAL,"L0,")
        XX<1,1>[89,1]   = '|'
        XX<1,1>[90,14]  = FMT(T.CUR.BALY,"L0,")
        XX<1,1>[104,1]  = '|'
        PRINT XX<1,1>
*---------------------------
        GOSUB R.MOVE.LINE
*-----------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[29,1]   = '|'
        XX<1,1>[30,19]  = S.HEAD2
        XX<1,1>[49,1]   = '|'
        XX<1,1>[50,19]  = FMT(T.SAV.BALX,"L0,")
        XX<1,1>[69,1]   = '|'
        XX<1,1>[70,19]  = FMT(T.SAV.BAL,"L0,")
        XX<1,1>[89,1]   = '|'
        XX<1,1>[90,14]  = FMT(T.SAV.BALY,"L0,")
        XX<1,1>[104,1]  = '|'
        PRINT XX<1,1>
*---------------------------
        GOSUB R.MOVE.LINE
*-----------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[29,1]   = '|'
        XX<1,1>[30,19]  = S.HEAD3
        XX<1,1>[49,1]   = '|'
        XX<1,1>[50,19]  = FMT(T.CER.BALX,"L0,")
        XX<1,1>[69,1]   = '|'
        XX<1,1>[70,19]  = FMT(T.CER.BAL,"L0,")
        XX<1,1>[89,1]   = '|'
        XX<1,1>[90,14]  = FMT(T.CER.BALY,"L0,")
        XX<1,1>[104,1]  = '|'
        PRINT XX<1,1>
*---------------------------
        GOSUB R.MOVE.LINE
*-----------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[29,1]   = '|'
        XX<1,1>[30,19]  = S.HEAD4
        XX<1,1>[49,1]   = '|'
        XX<1,1>[50,19]  = FMT(T.AGL.BALX,"L0,")
        XX<1,1>[69,1]   = '|'
        XX<1,1>[70,19]  = FMT(T.AGL.BAL,"L0,")
        XX<1,1>[89,1]   = '|'
        XX<1,1>[90,14]  = FMT(T.AGL.BALY,"L0,")
        XX<1,1>[104,1]  = '|'
        PRINT XX<1,1>
*---------------------------
        GOSUB R.MOVE.LINE
*-----------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[29,1]   = '|'
        XX<1,1>[30,19]  = S.HEAD5
        XX<1,1>[49,1]   = '|'
        XX<1,1>[50,19]  = FMT(TT.BALX,"L0,")
        XX<1,1>[69,1]   = '|'
        XX<1,1>[70,19]  = FMT(TT.BAL,"L0,")
        XX<1,1>[89,1]   = '|'
        XX<1,1>[90,14]  = FMT(TT.BALY,"L0,")
        XX<1,1>[104,1]  = '|'
        PRINT XX<1,1>
*---------------------------
        GOSUB R.MOVE.LINE


    END

*-----------------------------------------------------
    IF TT.BAL  GT 0 THEN
        PRINT.FLAG = 'YES'
    END

    IF TT.BALX GT 0 THEN
        PRINT.FLAG = 'YES'
    END
*---------------------------

    RETURN

*===============================================================
R.MOVE.LINE:

    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = '___________________'
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = '___________________'
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = '___________________'
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = '______________'
    XX<1,1>[104,1]  = '|'

    PRINT XX<1,1>

    RETURN
*===============================================================
MOVE.ZEROS:
    T.AGL.BAL = 0
    T.CER.BAL = 0
    T.SAV.BAL = 0
    T.CUR.BAL = 0
    T.OTR.BAL = 0


    T.AGL.BALX = 0
    T.CER.BALX = 0
    T.SAV.BALX = 0
    T.CUR.BALX = 0
    T.OTR.BALX = 0


    T.AGL.BALY = 0
    T.CER.BALY = 0
    T.SAV.BALY = 0
    T.CUR.BALY = 0
    T.OTR.BALY = 0

    T.TOT.BALY = 0

    RETURN
*===============================================================
*===============================================================
*===============================================================


*===============================================================
*===============================================================
*===============================================================
PRINT.HEAD:
*---------


    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :H.BRANCH
    PR.HD :="'L'":SPACE(1):" ������� : ":P.DATE:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):PROGRAM.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):HEAD.A1
    PR.HD :="'L'":SPACE(55):HEAD.B1:SPACE(1):HEAD.B2
    PR.HD :="'L'":SPACE(60):"�� ����� : ":P.WS.DATE
    PR.HD :="'L'":SPACE(50):STR('_',45)
    PR.HD :="'L'":" "


    PR.HD :="'L'":STR('_',138)
    PR.HD :="'L'":SPACE(13):"|":SPACE(5):"�������� �������":SPACE(7):"|":SPACE(6):"������ �������":SPACE (8):"|":SPACE(5):"������ ��������":SPACE(8):"|":SPACE(5):"������� ����":SPACE(11):"|":"������"
    PR.HD :="'L'":"��� �����":SPACE(4):"|":STR('_',28):"|":STR('_',28):"|":STR('_',28):"|":STR('_',28):"|"
    PR.HD :="'L'":SPACE(13):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT4
    PR.HD :="'L'":STR('_',138)





    HEADING PR.HD
    RETURN
*==============================================================
END
