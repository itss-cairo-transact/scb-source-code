* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****MOHSEN
    PROGRAM SBM.C.CRT.FIN.POS.02
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.BANK.FIN.POS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*---------------------------------------------------
    FN.FIN = "F.CBE.BANK.FIN.POS"
    F.FIN  = ""
    FN.MAST = "F.CBE.MAST.AC.LD"
    F.MAST  = ""
*-----------------------------------------------------
    WS.FILE.AMT = 0
    WS.AMT.LCY = 0

    WS.AMT.FCY = 0
    WS.FIN.AMT.LCY =  0
    WS.FIN.AMT.FCY =  0

*-----------------------------------------------------------
* ARRAY SUB 1 = KEY OF FILE CBE.BANK.FIN.POS
* ARRAY SUB 2 = CATEGORY TO BE USED FOR READING CBE.MAST.AC.LD FILE

    DIM ARRAY1(110,4)
    ARRAY1(1,1) = "1090105"
    ARRAY1(1,2) = "1480"
    ARRAY1(1,3) = "1480"
    ARRAY1(1,4) = "DR"

    ARRAY1(2,1) = "1010300"
    ARRAY1(2,2) = "3200"
    ARRAY1(2,3) = "3200"
    ARRAY1(2,4) = "ALL"

    ARRAY1(3,1) = "1080100"
    ARRAY1(3,2) = "11150"
    ARRAY1(3,3) = "11150"
    ARRAY1(3,4) = "ALL"

    ARRAY1(4,1) = "1090105"
    ARRAY1(4,2) = "1490"
    ARRAY1(4,3) = "1490"
    ARRAY1(4,4) = "DR"


    ARRAY1(5,1) = "1090105"
    ARRAY1(5,2) = "1481"
    ARRAY1(5,3) = "1481"
    ARRAY1(5,4) = "DR"

    ARRAY1(6,1) = "2040100"
    ARRAY1(6,2) = "1001"
    ARRAY1(6,3) = "1002"
    ARRAY1(6,4) = "CR"

    ARRAY1(7,1) = "2040200"
    ARRAY1(7,2) = "6501"
    ARRAY1(7,3) = "6504"
    ARRAY1(7,4) = "CR"

    ARRAY1(8,1) = "2040301"
    ARRAY1(8,2) = "21001"
    ARRAY1(8,3) = "21007"
    ARRAY1(8,4) = "ALL"

    ARRAY1(9,1) = "2040302"
    ARRAY1(9,2) = "21008"
    ARRAY1(9,3) = "21010"
    ARRAY1(9,4) = "ALL"

    ARRAY1(10,1) = "2040502"
    ARRAY1(10,2) = "21020"
    ARRAY1(10,3) = "21028"
    ARRAY1(10,4) = "ALL"

    ARRAY1(11,1) = "2040601"
    ARRAY1(11,2) = "3005"
    ARRAY1(11,3) = "3005"
    ARRAY1(11,4) = "CR"

    ARRAY1(12,1) = "2040601"
    ARRAY1(12,2) = "3010"
    ARRAY1(12,3) = "3013"
    ARRAY1(12,4) = "CR"

    ARRAY1(13,1) = "2040602"
    ARRAY1(13,2) = "1012"
    ARRAY1(13,3) = "1014"
    ARRAY1(13,4) = "ALL"

    ARRAY1(14,1) = "2040602"
    ARRAY1(14,2) = "1015"
    ARRAY1(14,3) = "1016"
    ARRAY1(14,4) = "ALL"

    ARRAY1(15,1) = "2040100"
    ARRAY1(15,2) = "1100"
    ARRAY1(15,3) = "1219"
    ARRAY1(15,4) = "CR"

    ARRAY1(16,1) = "2040100"
    ARRAY1(16,2) = "1300"
    ARRAY1(16,3) = "1599"
    ARRAY1(16,4) = "CR"

    ARRAY1(17,1) = "2010201"
    ARRAY1(17,2) = "2000"
    ARRAY1(17,3) = "2000"
    ARRAY1(17,4) = "CR"

    ARRAY1(18,1) = "2010303"
    ARRAY1(18,2) = "2001"
    ARRAY1(18,3) = "2001"
    ARRAY1(18,4) = "CR"

    ARRAY1(19,1) = "2040602"
    ARRAY1(19,2) = "1019"
    ARRAY1(19,3) = "1019"
    ARRAY1(19,4) = "ALL"

    ARRAY1(20,1) = "2040602"
    ARRAY1(20,2) = "3001"
    ARRAY1(20,3) = "3001"
    ARRAY1(20,4) = "ALL"

    ARRAY1(21,1) = "2040601"
    ARRAY1(21,2) = "3014"
    ARRAY1(21,3) = "3014"
    ARRAY1(21,4) = "CR"

    ARRAY1(22,1) = "2040601"
    ARRAY1(22,2) = "3015"
    ARRAY1(22,3) = "3017"
    ARRAY1(22,4) = "CR"


    ARRAY1(23,1) = "2040200"
    ARRAY1(23,2) = "6511"
    ARRAY1(23,3) = "6511"
    ARRAY1(23,4) = "CR"

    ARRAY1(24,1) = "2040301"
    ARRAY1(24,2) = "6512"
    ARRAY1(24,3) = "6512"
    ARRAY1(24,4) = "ALL"

    ARRAY1(25,1) = "1090105"
    ARRAY1(25,2) = "1491"
    ARRAY1(25,3) = "1491"
    ARRAY1(25,4) = "DR"

    ARRAY1(26,1) = "1090111"
    ARRAY1(26,2) = "1482"
    ARRAY1(26,3) = "1482"
    ARRAY1(26,4) = "DR"

    ARRAY1(27,1) = "1090109"
    ARRAY1(27,2) = "1483"
    ARRAY1(27,3) = "1483"
    ARRAY1(27,4) = "DR"

    ARRAY1(28,1) = "1090109"
    ARRAY1(28,2) = "1493"
    ARRAY1(28,3) = "1493"
    ARRAY1(28,4) = "DR"

    ARRAY1(29,1) = "1090112"
    ARRAY1(29,2) = "1558"
    ARRAY1(29,3) = "1558"
    ARRAY1(29,4) = "DR"

    ARRAY1(30,1) = "2040502"
    ARRAY1(30,2) = "21032"
    ARRAY1(30,3) = "21032"
    ARRAY1(30,4) = "ALL"

    ARRAY1(31,1) = "0"
    ARRAY1(31,2) = "0"
    ARRAY1(31,3) = "0"
    ARRAY1(31,4) = ""

    ARRAY1(32,1) = "0"
    ARRAY1(32,2) = "0"
    ARRAY1(32,3) = "0"
    ARRAY1(32,4) = ""

    ARRAY1(33,1) = "0"
    ARRAY1(33,2) = "0"
    ARRAY1(33,3) = "0"
    ARRAY1(33,4) = ""

    ARRAY1(34,1) = "0"
    ARRAY1(34,2) = "0"
    ARRAY1(34,3) = "0"
    ARRAY1(34,4) = ""

    ARRAY1(35,1) = "0"
    ARRAY1(35,2) = "0"
    ARRAY1(35,3) = "0"
    ARRAY1(35,4) = ""

    ARRAY1(36,1) = "0"
    ARRAY1(36,2) = "0"
    ARRAY1(36,3) = "0"
    ARRAY1(36,4) = ""

    ARRAY1(37,1) = "0"
    ARRAY1(37,2) = "0"
    ARRAY1(37,3) = "0"
    ARRAY1(37,4) = ""

    ARRAY1(38,1) = "0"
    ARRAY1(38,2) = "0"
    ARRAY1(38,3) = "0"
    ARRAY1(38,4) = ""

    ARRAY1(39,1) = "0"
    ARRAY1(39,2) = "0"
    ARRAY1(39,3) = "0"
    ARRAY1(39,4) = ""

    ARRAY1(40,1) = "0"
    ARRAY1(40,2) = "0"
    ARRAY1(40,3) = "0"
    ARRAY1(40,4) = ""

    ARRAY1(41,1) = "0"
    ARRAY1(41,2) = "0"
    ARRAY1(41,3) = "0"
    ARRAY1(41,4) = ""

    ARRAY1(42,1) = "0"
    ARRAY1(42,2) = "0"
    ARRAY1(42,3) = "0"
    ARRAY1(42,4) = ""

    ARRAY1(43,1) = "0"
    ARRAY1(43,2) = "0"
    ARRAY1(43,3) = "0"
    ARRAY1(43,4) = ""

    ARRAY1(44,1) = "0"
    ARRAY1(44,2) = "0"
    ARRAY1(44,3) = "0"
    ARRAY1(44,4) = ""

    ARRAY1(45,1) = "0"
    ARRAY1(45,2) = "0"
    ARRAY1(45,3) = "0"
    ARRAY1(45,4) = ""

    ARRAY1(46,1) = "0"
    ARRAY1(46,2) = "0"
    ARRAY1(46,3) = "0"
    ARRAY1(46,4) = ""

    ARRAY1(47,1) = "0"
    ARRAY1(47,2) = "0"
    ARRAY1(47,3) = "0"
    ARRAY1(47,4) = ""

    ARRAY1(48,1) = "0"
    ARRAY1(48,2) = "0"
    ARRAY1(48,3) = "0"
    ARRAY1(48,4) = ""

    ARRAY1(49,1) = "0"
    ARRAY1(49,2) = "0"
    ARRAY1(49,3) = "0"
    ARRAY1(49,4) = ""

    ARRAY1(50,1) = "1020102"
    ARRAY1(50,2) = "21079"
    ARRAY1(50,3) = "21079"
    ARRAY1(50,4) = "ALL"

    ARRAY1(51,1) = "1020104"
    ARRAY1(51,2) = "5081"
    ARRAY1(51,3) = "5081"
    ARRAY1(51,4) = "ALL"

    ARRAY1(52,1) = "1020201"
    ARRAY1(52,2) = "5000"
    ARRAY1(52,3) = "5000"
    ARRAY1(52,4) = "ALL"

    ARRAY1(53,1) = "1020306"
    ARRAY1(53,2) = "21076"
    ARRAY1(53,3) = "21076"
    ARRAY1(53,4) = "ALL"

    ARRAY1(54,1) = "1090102"
    ARRAY1(54,2) = "1101"
    ARRAY1(54,3) = "1102"
    ARRAY1(54,4) = "DR"
    ARRAY1(55,1) = "1090104"
    ARRAY1(55,2) = "1404"
    ARRAY1(55,3) = "1404"
    ARRAY1(55,4) = "DR"

    ARRAY1(56,1) = "1090104"
    ARRAY1(56,2) = "1414"
    ARRAY1(56,3) = "1414"
    ARRAY1(56,4) = "DR"

    ARRAY1(57,1) = "1090105"
    ARRAY1(57,2) = "1401"
    ARRAY1(57,3) = "1401"
    ARRAY1(57,4) = "DR"

    ARRAY1(58,1) = "1090105"
    ARRAY1(58,2) = "1405"
    ARRAY1(58,3) = "1406"
    ARRAY1(58,4) = "DR"

    ARRAY1(59,1) = "1090105"
    ARRAY1(59,2) = "1206"
    ARRAY1(59,3) = "1206"
    ARRAY1(59,4) = "DR"

    ARRAY1(60,1) = "1090105"
    ARRAY1(60,2) = "1208"
    ARRAY1(60,3) = "1208"
    ARRAY1(60,4) = "DR"

    ARRAY1(61,1) = "1090106"
    ARRAY1(61,2) = "1300"
    ARRAY1(61,3) = "1302"
    ARRAY1(61,4) = "DR"

    ARRAY1(62,1) = "1090106"
    ARRAY1(62,2) = "1390"
    ARRAY1(62,3) = "1390"
    ARRAY1(62,4) = "DR"

    ARRAY1(63,1) = "1090107"
    ARRAY1(63,2) = "1502"
    ARRAY1(63,3) = "1503"
    ARRAY1(63,4) = "DR"

    ARRAY1(64,1) = "1090108"
    ARRAY1(64,2) = "1202"
    ARRAY1(64,3) = "1202"
    ARRAY1(64,4) = "DR"

    ARRAY1(65,1) = "1090108"
    ARRAY1(65,2) = "1212"
    ARRAY1(65,3) = "1212"
    ARRAY1(65,4) = "DR"

    ARRAY1(66,1) = "1090109"
    ARRAY1(66,2) = "1200"
    ARRAY1(66,3) = "1201"
    ARRAY1(66,4) = "DR"

    ARRAY1(67,1) = "1090109"
    ARRAY1(67,2) = "1211"
    ARRAY1(67,3) = "1211"
    ARRAY1(67,4) = "DR"

    ARRAY1(68,1) = "1090109"
    ARRAY1(68,2) = "1290"
    ARRAY1(68,3) = "1290"
    ARRAY1(68,4) = "DR"

    ARRAY1(69,1) = "1090109"
    ARRAY1(69,2) = "1400"
    ARRAY1(69,3) = "1400"
    ARRAY1(69,4) = "DR"

    ARRAY1(70,1) = "1090109"
    ARRAY1(70,2) = "1402"
    ARRAY1(70,3) = "1403"
    ARRAY1(70,4) = "DR"

    ARRAY1(71,1) = "1090109"
    ARRAY1(71,2) = "1408"
    ARRAY1(71,3) = "1408"
    ARRAY1(71,4) = "DR"

    ARRAY1(72,1) = "1090109"
    ARRAY1(72,2) = "1437"
    ARRAY1(72,3) = "1437"
    ARRAY1(72,4) = "DR"

    ARRAY1(73,1) = "1090109"
    ARRAY1(73,2) = "1505"
    ARRAY1(73,3) = "1506"
    ARRAY1(73,4) = "DR"

    ARRAY1(74,1) = "1090109"
    ARRAY1(74,2) = "1510"
    ARRAY1(74,3) = "1510"
    ARRAY1(74,4) = "DR"

    ARRAY1(75,1) = "1090111"
    ARRAY1(75,2) = "1501"
    ARRAY1(75,3) = "1501"
    ARRAY1(75,4) = "DR"

    ARRAY1(76,1) = "1090112"
    ARRAY1(76,2) = "1100"
    ARRAY1(76,3) = "1100"
    ARRAY1(76,4) = "DR"

    ARRAY1(77,1) = "1090112"
    ARRAY1(77,2) = "1205"
    ARRAY1(77,3) = "1205"
    ARRAY1(77,4) = "DR"

    ARRAY1(78,1) = "1090112"
    ARRAY1(78,2) = "1207"
    ARRAY1(78,3) = "1207"
    ARRAY1(78,4) = "DR"

    ARRAY1(79,1) = "1090112"
    ARRAY1(79,2) = "1511"
    ARRAY1(79,3) = "1514"
    ARRAY1(79,4) = "DR"

    ARRAY1(80,1) = "1090112"
    ARRAY1(80,2) = "1517"
    ARRAY1(80,3) = "1519"
    ARRAY1(80,4) = "DR"

    ARRAY1(81,1) = "1090112"
    ARRAY1(81,2) = "1507"
    ARRAY1(81,3) = "1509"
    ARRAY1(81,4) = "DR"

    ARRAY1(82,1) = "1090112"
    ARRAY1(82,2) = "1504"
    ARRAY1(82,3) = "1504"
    ARRAY1(82,4) = "DR"

    ARRAY1(83,1) = "1090112"
    ARRAY1(83,2) = "1559"
    ARRAY1(83,3) = "1559"
    ARRAY1(83,4) = "DR"

    ARRAY1(84,1) = "1090112"
    ARRAY1(84,2) = "11064"
    ARRAY1(84,3) = "11064"
    ARRAY1(84,4) = "DR"

    ARRAY1(85,1) = "1090112"
    ARRAY1(85,2) = "1001"
    ARRAY1(85,3) = "1003"
    ARRAY1(85,4) = "DR"

    ARRAY1(86,1) = "1090112"
    ARRAY1(86,2) = "1551"
    ARRAY1(86,3) = "1551"
    ARRAY1(86,4) = "DR"

    ARRAY1(87,1) = "1090112"
    ARRAY1(87,2) = "21053"
    ARRAY1(87,3) = "21053"
    ARRAY1(87,4) = "DR"

    ARRAY1(88,1) = "1090112"
    ARRAY1(88,2) = "21074"
    ARRAY1(88,3) = "21074"
    ARRAY1(88,4) = "DR"

    ARRAY1(89,1) = "1020303"
    ARRAY1(89,2) = "5001"
    ARRAY1(89,3) = "5001"
    ARRAY1(89,4) = "DR"
    ARRAY1(90,1) = "1090112"
    ARRAY1(90,2) = "1534"
    ARRAY1(90,3) = "1534"
    ARRAY1(90,4) = "DR"

    ARRAY1(91,1) = "1090112"
    ARRAY1(91,2) = "1220"
    ARRAY1(91,3) = "1220"
    ARRAY1(91,4) = "ALL"

    ARRAY1(92,1) = "1090108"
    ARRAY1(92,2) = "1221"
    ARRAY1(92,3) = "1221"
    ARRAY1(92,4) = "ALL"

    ARRAY1(93,1) = "1090108"
    ARRAY1(93,2) = "1222"
    ARRAY1(93,3) = "1222"
    ARRAY1(93,4) = "ALL"

    ARRAY1(94,1) = "1090102"
    ARRAY1(94,2) = "1223"
    ARRAY1(94,3) = "1223"
    ARRAY1(94,4) = "ALL"

    ARRAY1(95,1) = "1090106"
    ARRAY1(95,2) = "1224"
    ARRAY1(95,3) = "1224"
    ARRAY1(95,4) = "ALL"

    ARRAY1(96,1) = "1090104"
    ARRAY1(96,2) = "1225"
    ARRAY1(96,3) = "1225"
    ARRAY1(96,4) = "ALL"

    ARRAY1(97,1) = "1090109"
    ARRAY1(97,2) = "1227"
    ARRAY1(97,3) = "1227"
    ARRAY1(97,4) = "ALL"

    ARRAY1(98,1) = "1090112"
    ARRAY1(98,2) = "1544"
    ARRAY1(98,3) = "1544"
    ARRAY1(98,4) = "DR"


    ARRAY1(99,1) = "1090105"
    ARRAY1(99,2) = "1415"
    ARRAY1(99,3) = "1415"
    ARRAY1(99,4) = "DR"

    ARRAY1(100,1) = "1090108"
    ARRAY1(100,2) = "1216"
    ARRAY1(100,3) = "1216"
    ARRAY1(100,4) = "DR"

    ARRAY1(101,1) = "1090106"
    ARRAY1(101,2) = "1377"
    ARRAY1(101,3) = "1377"
    ARRAY1(101,4) = "DR"

    ARRAY1(102,1) = "1090106"
    ARRAY1(102,2) = "1399"
    ARRAY1(102,3) = "1399"
    ARRAY1(102,4) = "DR"

    ARRAY1(103,1) = "1090105"
    ARRAY1(103,2) = "1477"
    ARRAY1(103,3) = "1477"
    ARRAY1(103,4) = "DR"

    ARRAY1(104,1) = "1090111"
    ARRAY1(104,2) = "1577"
    ARRAY1(104,3) = "1577"
    ARRAY1(104,4) = "DR"

    ARRAY1(105,1) = "1090111"
    ARRAY1(105,2) = "1588"
    ARRAY1(105,3) = "1588"
    ARRAY1(105,4) = "DR"

    ARRAY1(106,1) = "1090105"
    ARRAY1(106,2) = "1499"
    ARRAY1(106,3) = "1499"
    ARRAY1(106,4) = "DR"

    ARRAY1(107,1) = "1090111"
    ARRAY1(107,2) = "1566"
    ARRAY1(107,3) = "1566"
    ARRAY1(107,4) = "DR"

    ARRAY1(108,1) = "1090111"
    ARRAY1(108,2) = "1599"
    ARRAY1(108,3) = "1599"
    ARRAY1(108,4) = "DR"

    ARRAY1(109,1) = "1090111"
    ARRAY1(109,2) = "1560"
    ARRAY1(109,3) = "1560"
    ARRAY1(109,4) = "DR"

    ARRAY1(110,1) = "1090111"
    ARRAY1(110,2) = "1591"
    ARRAY1(110,3) = "1591"
    ARRAY1(110,4) = "DR"
*

*                                             MOHSEN
*------------------------------------------------
    R.FIN = ""
*----------------------------PROCEDURE---------------------
    CALL OPF (FN.FIN,F.FIN)
    CALL OPF (FN.MAST,F.MAST)
*------------------------PROCEDURE------------------------
    GOSUB A.100.ARRAY1
    RETURN
*------------------------------------------------
A.100.ARRAY1:
    FOR AR1 = 1 TO 110
        WS.FIN.ID = ARRAY1(AR1,1)
        WS.CATEG.FRM =  ARRAY1(AR1,2)
        WS.CATEG.TO  =  ARRAY1(AR1,3)
        WS.SIGN  =  ARRAY1(AR1,4)
        WS.AMT.LCY = 0
        WS.AMT.FCY = 0
        GOSUB A.200.CHK
    NEXT AR1
    RETURN
*------------------------------------------------
A.200.CHK:
    IF WS.CATEG.FRM  EQ 0 THEN
        RETURN
    END
    GOSUB A.400.GET.CATEG
*   GOSUB A.500.UPDAT
    RETURN
*------------------------------------------------
A.400.GET.CATEG:
*   SEL.CMD = "SELECT ":FN.MAST:" WITH CBEM.CATEG EQ ":WS.CATEG
    SEL.CMD = "SELECT ":FN.MAST:" WITH CBEM.CATEG GE ":WS.CATEG.FRM:" AND CBEM.CATEG LE ":WS.CATEG.TO
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.MAST.ID FROM SEL.LIST SETTING POS
    WHILE WS.MAST.ID:POS
        CALL F.READ(FN.MAST,WS.MAST.ID,R.MAST,F.MAST,MSG.CAT)
        WS.FILE.AMT = R.MAST<C.CBEM.IN.LCY>
        IF WS.FILE.AMT  EQ 0 THEN
            GOTO  A.400.3
        END

        IF  WS.FILE.AMT LT 0 THEN
            WS.SS = "DR"
        END
        IF  WS.FILE.AMT GT 0 THEN
            WS.SS = "CR"
        END

        IF  WS.SIGN  EQ "ALL" THEN
            GOTO   A.400.1
        END

        IF  WS.SIGN EQ WS.SS THEN
            GOTO A.400.1
        END

        GOTO  A.400.3
A.400.1:
        WS.BR.F = R.MAST<C.CBEM.BR>
        WS.BR = WS.BR.F
        IF WS.BR.F LT 10 THEN
            WS.BR = WS.BR.F[1]
        END
        WS.AMT.LCY = 0
        WS.AMT.FCY = 0
        IF R.MAST<C.CBEM.CY> EQ "EGP" THEN

*           WS.AMT.LCY = WS.AMT.LCY + WS.FILE.AMT
            WS.AMT.LCY =              WS.FILE.AMT
        END

        IF R.MAST<C.CBEM.CY> NE "EGP" THEN

*           WS.AMT.FCY = WS.AMT.FCY + WS.FILE.AMT
            WS.AMT.FCY =              WS.FILE.AMT
        END
        GOSUB A.500.UPDAT

A.400.3:
    REPEAT
    RETURN
A.500.UPDAT:
    WS.NEW.KEY = WS.FIN.ID:"*":WS.BR
*   CALL F.READ(FN.FIN,WS.FIN.ID,R.FIN,F.FIN,MSG.FIN)
    CALL F.READ(FN.FIN,WS.NEW.KEY,R.FIN,F.FIN,MSG.FIN)
    IF MSG.FIN NE "" THEN
        RETURN
    END
    WS.FIN.AMT.LCY =  R.FIN<CBE.FIN.POS.AMT.LCY>
    WS.FIN.AMT.FCY =  R.FIN<CBE.FIN.POS.AMT.FCY>

    WS.FIN.AMT.LCY =  WS.FIN.AMT.LCY + WS.AMT.LCY
    WS.FIN.AMT.FCY =  WS.FIN.AMT.FCY + WS.AMT.FCY

    R.FIN<CBE.FIN.POS.AMT.LCY>  =  WS.FIN.AMT.LCY
    R.FIN<CBE.FIN.POS.AMT.FCY>  =  WS.FIN.AMT.FCY
*    CALL F.WRITE(FN.FIN,WS.FIN.ID,R.FIN)
*    CALL  JOURNAL.UPDATE(WS.FIN.ID)
    CALL F.WRITE(FN.FIN,WS.NEW.KEY,R.FIN)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)
    RETURN
END
