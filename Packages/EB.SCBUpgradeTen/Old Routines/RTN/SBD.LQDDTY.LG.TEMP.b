* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBD.CONV.LQDDTY.TEMP
**    SUBROUTINE SBD.CONV.LQDDTY.TEMP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
    COMP = ID.COMPANY
*-----------------------------------------------------------------------
    FN.LG =  'FBNK.LD.LOANS.AND.DEPOSITS'; F.LG = ''; R.LG = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''

    FN.LQD = 'F.SCB.LQDDTY.DAILY.FILE' ; F.LQD = ''

    CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.LG,F.LG)
    CALL OPF(FN.LQD,F.LQD)

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,LWD)
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

    WS.BR.LG.AMT    = 0
    WS.BR.MRG.AMT   = 0
    WS.BR.LG.NET    = 0
    WS.LG.COMPAY    = ''
    LG.COMPANY      = ''
    WS.LG.COMPANY   = ''
********* LOCAL LINES *************
* LQD.ID = "LCY.B.":LWD:".":WS.LG.COMPANY
*-UPDATE BY MOHAMED SABRY 2010/04/07-- LG NOT COVER----------------------
    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND CURRENCY EQ 'EGP' AND STATUS NE 'LIQ' AND LG.KIND NE 'BB' AND MARGIN.AMT NE '' AND LIMIT.REFERENCE LIKE 2510.... BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.LG,KEY.LIST<II>,R.LG,F.LG,EER)
            LG.COMPANY      = R.LG<LD.CO.CODE>
* WS.BR.LG.AMT   += R.LG<LD.AMOUNT>
* WS.BR.MRG.AMT  += R.LG<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>

            IF WS.LG.COMPANY EQ '' THEN
                WS.LG.COMPANY = LG.COMPANY
            END

            IF WS.LG.COMPANY NE LG.COMPANY THEN
                WS.BR.LG.NET = (( WS.BR.LG.AMT -  WS.BR.MRG.AMT ) / 2 )
                LQD.ID = "LCY.B.":LWD:".":WS.LG.COMPANY
                CALL F.READ(FN.LQD,LQD.ID,R.LQD,F.LQD,E)
                IF  NOT (E)  THEN
                    R.LQD<LQDF.COLUMN.15> = WS.BR.LG.NET
                    R.LQD<LQDF.TOTAL.ROW.B> += WS.BR.LG.NET
                    CALL F.WRITE(FN.LQD,LQD.ID,R.LQD)
                    CALL  JOURNAL.UPDATE(LQD.ID)
                    WS.BR.LG.AMT    = 0
                    WS.BR.MRG.AMT   = 0
                    WS.BR.LG.NET    = 0
                END
            END
            WS.BR.LG.AMT   += R.LG<LD.AMOUNT>
            WS.BR.MRG.AMT  += R.LG<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>

            WS.LG.COMPANY = LG.COMPANY

        NEXT II
        WS.BR.LG.NET = (( WS.BR.LG.AMT -  WS.BR.MRG.AMT ) / 2 )
        LQD.ID = "LCY.B.":LWD:".":WS.LG.COMPANY
        CALL F.READ(FN.LQD,LQD.ID,R.LQD,F.LQD,E)
        IF  NOT (E)  THEN
            R.LQD<LQDF.COLUMN.15> = WS.BR.LG.NET
            R.LQD<LQDF.TOTAL.ROW.B> += WS.BR.LG.NET
            CALL F.WRITE(FN.LQD,LQD.ID,R.LQD)
            CALL  JOURNAL.UPDATE(LQD.ID)
            WS.BR.LG.AMT    = 0
            WS.BR.MRG.AMT   = 0
            WS.BR.LG.NET    = 0
        END

    END
* WS.BR.LG.NET = (( WS.BR.LG.AMT -  WS.BR.MRG.AMT ) / 2 )
* WS.BANK.LG.NET += WS.BR.LG.NET
*------------------------------------------------------------------------
END
**************************************************
