* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>3384</Rating>
*-----------------------------------------------------------------------------
*-----------------------------------
*--- Create By Ni7oooooooooooooooooo
*-----------------------------------
    SUBROUTINE SBD.REPORT.NAU.TEXT

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FOREX
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.TOTAL
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BOAT.DATA
*Line [ 80 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 82 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 84 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 86 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*------------------------------------------------
    COMP = ID.COMPANY

    YTEXT = "Enter the User. No. : "
    CALL TXTINP(YTEXT, 6,6 , "12", "A")

    USER.ID = COMI
    INP   = TRIM(COMI, "0", "L")
    ID.NO = 'SCB.':INP
    INP.ID = '...':INP:'...'

    FN.USER   = 'F.USER'     ; F.USER = ''
    CALL OPF(FN.USER,F.USER)

    CALL DBR ('USER':@FM:EB.USE.USER.NAME,ID.NO,NAME11)
    IF NAME11 THEN
        NAME.INP  = NAME11

        GOSUB INITIATE
        GOSUB PRINT.HEAD
        GOSUB SEL.P.1
        GOSUB SEL.P.2
        GOSUB SEL.P.3
        GOSUB SEL.P.4
        GOSUB SEL.P.5
        GOSUB SEL.P.6
        GOSUB SEL.P.7
        GOSUB SEL.P.8
        GOSUB SEL.P.9
        GOSUB SEL.P.10
        GOSUB SEL.P.11
        GOSUB SEL.P.12
        GOSUB SEL.P.13
        GOSUB SEL.P.14
        GOSUB SEL.P.15
        GOSUB SEL.P.16
        GOSUB SEL.P.17
        GOSUB SEL.P.18
        GOSUB SEL.P.19
        GOSUB SEL.P.20
        GOSUB SEL.P.21
        GOSUB SEL.P.22
        GOSUB SEL.P.23
        GOSUB SEL.P.END
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    END ELSE
        NAME.INP = '��� ������ ��� ����'
        GOSUB INITIATE
        GOSUB PRINT.HEAD
        CALL PRINTER.OFF
        CALL PRINTER.CLOSE(REPORT.ID,0,'')
        TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM

    END
    RETURN
*========================================================================
INITIATE:
  **  REPORT.ID = 'SBD.REPORT.NAU.TEXT'
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.DEPT   = 'F.DEPT.ACCT.OFFICER'   ; F.DEPT     = ''
    FN.BOT.TOT= 'F.SCB.BOAT.TOTAL$NAU'      ; F.BOT.TOT  = ''
    FN.BOT    = 'F.SCB.BOAT.DATA$NAU'       ; F.BOT      = ''
    FN.CARD   = 'FBNK.CARD.ISSUE$NAU' ; F.CARD = ''
    FN.AC     = 'FBNK.ACCOUNT$NAU' ; F.AC = ''
    FN.CU     = 'FBNK.CUSTOMER$NAU' ; F.CU = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE$NAU' ; F.IM = ''
    FN.FT     = 'FBNK.FUNDS.TRANSFER$NAU' ; F.FT = ''
    FN.TT     = 'FBNK.TELLER$NAU' ; F.TT = ''
    FN.IN     = 'F.INF.MULTI.TXN$NAU' ; F.IN = ''
    FN.CLOSE  = 'FBNK.ACCOUNT.CLOSURE$NAU' ; F.CLOSE = ''
    FN.MM     = 'FBNK.MM.MONEY.MARKET$NAU' ; F.MM = ''
    FN.FOR    = 'FBNK.FOREX$NAU' ; F.FOR = ''
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
    FN.LETTER = 'FBNK.LETTER.OF.CREDIT$NAU' ; F.LETTER = ''
    FN.DRAW   = 'FBNK.DRAWINGS$NAU' ; F.DRAW = ''
    FN.BT     = 'F.SCB.BT.BATCH$NAU' ; F.BT = ''
    FN.BR     = 'FBNK.BILL.REGISTER$NAU' ; F.BR = ''
    FN.CM     = 'F.COMPANY'  ; F.CM = ''
    FN.USER   = 'F.USER'     ; F.USER = ''
    FN.CBE    = 'F.SCB.MONTHLY.PAY.CBE$NAU'     ; F.CBE = ''
    FN.LIM    = 'FBNK.LIMIT$NAU'     ; F.LIM = ''
    FN.COLL   = 'FBNK.COLLATERAL$NAU'     ; F.COLL = ''
    FN.COR    = 'FBNK.COLLATERAL.RIGHT$NAU'     ; F.COR = ''

    CALL OPF(FN.DEPT,F.DEPT)
    CALL OPF(FN.BOT.TOT,F.BOT.TOT)
    CALL OPF(FN.BOT,F.BOT)
    CALL OPF(FN.LETTER,F.LETTER)
    CALL OPF(FN.DRAW,F.DRAW)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.CARD,F.CARD)
    CALL OPF(FN.CLOSE,F.CLOSE)
    CALL OPF(FN.MM,F.MM)
    CALL OPF(FN.FOR,F.FOR)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.BT,F.BT)
    CALL OPF(FN.BR,F.BR)
    CALL OPF(FN.CM,F.CM)
    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.COLL,F.COLL)
    CALL OPF(FN.COR,F.COR)

    XXX1   = SPACE(132)  ; XXX4  = SPACE(132) ; XXX11  = SPACE(132) ;XXX12  = SPACE(132)
    XX18   = SPACE(132)  ; XX19  = SPACE(132) ; XX20  = SPACE(132)
    XXX2   = SPACE(132)  ; XXX5  = SPACE(132) ; XXX10  = SPACE(132) ;XXX13  = SPACE(132) ; XX5 = SPACE(132)
    XXX15  = SPACE(132)  ; XXX16 = SPACE(132) ; XXX17  = SPACE(132) ;XXX14  = SPACE(132)
    XXX21  = SPACE(132)  ; XXX19 = SPACE(132) ; XXX18  = SPACE(132) ;XXX20  = SPACE(132)
    XXX22  = SPACE(132)
    XXX6   = SPACE(132)  ; XXX7  = SPACE(132) ; XXX8   = SPACE(132) ;XXX9   = SPACE(132) ; XXX24 = SPACE(132)
    XXX23  = SPACE(132)  ; XXX25 = SPACE(132) ; XXX27  = SPACE(132) ;XXX35  = SPACE(132)
    XXX29  = SPACE(132)  ; XXX31 = SPACE(132) ; XXX33  = SPACE(132) ;XXX3   = SPACE(132)
    XXX36  = SPACE(132)  ; XXX37 = SPACE(132) ; XXX38  = SPACE(132) ;XXX39  = SPACE(132)
    XXX43  = SPACE(132)  ; XXX42 = SPACE(132) ; XXX41  = SPACE(132) ;XXX40  = SPACE(132)
    XXX44  = SPACE(132)  ; XXX45 = SPACE(132) ; XXX46  = SPACE(132) ;XXX47  = SPACE(132)
    XXX48  = SPACE(132)  ; XXX49 = SPACE(132) ; XXX50  = SPACE(132) ;XXX51  = SPACE(132)
    XXX52  = SPACE(132)  ; XXX53 = SPACE(132) ; XXX54  = SPACE(132) ;XXX55  = SPACE(132)
    XXX28  = SPACE(132)  ; XXX30 = SPACE(132) ; XXX32  = SPACE(132) ;XXX34  = SPACE(132)
    XXX41  = SPACE(132)  ; XXX40 = SPACE(132) ; XXX39  = SPACE(132) ;XXX38  = SPACE(132)
    XXX37  = SPACE(132)  ; XXX36 = SPACE(132) ; XXX35  = SPACE(132) ;XXX34  = SPACE(132)
    XXX33  = SPACE(132)  ; XXX32 = SPACE(132) ; XXX31  = SPACE(132) ;XXX30  = SPACE(132)
    XXXX16  = SPACE(132)  ; XXXX8 = SPACE(132) ; XXXX6  = SPACE(132) ; XXXX7  = SPACE(132)
    XXXX17  = SPACE(132) ;  XXXX18  = SPACE(132)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""  ; R.DEP = ''
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG="" ; R.IM = ''

    RETURN
*========================================================================
SEL.P.1:
*-------
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD  = TODAY
*------------------------------------------------------------------------
    T.SEL1 = "SELECT FBNK.CUSTOMER$NAU WITH INPUTTER LIKE ": INP.ID
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG)
    IF SELECTED1 THEN
        XX1 = SELECTED1
        XXX2<1,1>[50,10] = KEY.LIST1[1,2]
        XXX37<1,1>[1,4] = "----"
        PRINT XXX2<1,1>
        PRINT XXX37<1,1>
        FOR II = 1 TO SELECTED1
            CALL F.READ(FN.CU,KEY.LIST1<II>,R.CU,F.CU,E1)
            INP1    = R.CU<EB.CUS.INPUTTER>
            CO.CU   = R.CU<EB.CUS.COMPANY.BOOK>
            INPUTT1 = FIELD(INP1,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT1,NAME1)
            XXX3<1,II>[30,20] = KEY.LIST1<II>
            XXX3<1,II>[50,20] = CO.CU
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.CU,NA.CO.CU)
            XXX3<1,II>[70,20] = NA.CO.CU
            XXX36<1,II>[30,4] = "----"
            PRINT XXX3<1,II>
            PRINT XXX36<1,II>
        NEXT II

    END ELSE
        XX1 = ''
        XXX2<1,1>[21,20]    = "������ ������ ��� �����"
        XXX2<1,1>[50,2]     = "CU"
        XXX37<1,1>[30,4]    = "----"
        PRINT XXX2<1,1>
        PRINT XXX37<1,1>
    END
    RETURN
*-------------------------------------------------------------------
SEL.P.2:
    T.SEL2 = "SELECT FBNK.ACCOUNT$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
    IF SELECTED2 THEN
        XX2 = SELECTED2
        XXX5<1,1>[21,3]    = XX2
        XXX5<1,1>[50,6]    = KEY.LIST2[1,2]
        XXX38<1,1>[1,4]   = "----"
        PRINT XXX5<1,1>
        PRINT XXX38<1,1>
        FOR J = 1 TO SELECTED2
            CALL F.READ(FN.AC,KEY.LIST2<J>,R.AC,F.AC,E2)
            INP2 = R.AC<AC.INPUTTER>
            CO.AC   = R.AC<AC.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.AC,NA.CO.AC)
            INPUTT2 = FIELD(INP2,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT2,NAME2)
            XXX6<1,J>[30,20] = KEY.LIST2<J>
            XXX6<1,J>[50,20] = CO.AC
            XXX6<1,J>[70,20] = NA.CO.AC
            XXX39<1,J>[30,4] = "----"
            PRINT XXX6<1,J>
            PRINT XXX39<1,J>
        NEXT J
    END ELSE
        XX2 = ''
        XXX5<1,1>[21,20]   = "�� ���� ������ ��� �����"
        XXX5<1,1>[60,2]   = "AC"
        XXX38<1,1>[30,4] = "-----"
        PRINT XXX5<1,1>
        PRINT XXX38<1,1>
    END
    RETURN
*--------------------------------------------------------------
SEL.P.3:
    T.SEL3 = "SELECT FBNK.DRAWINGS$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG)
    IF SELECTED3 THEN
        XX3 = SELECTED3
        XXX7<1,1>[21,20]  = XX3
        XXX7<1,1>[50,10]  = KEY.LIST3[1,2]
        XXX40<1,1>[30,4]  = "----"
        PRINT XXX7<1,1>
        PRINT XXX40<1,1>
        FOR K = 1 TO SELECTED3
            CALL F.READ(FN.DRAW,KEY.LIST3<K>,R.DRAW,F.DRAW,E3)
            INP3    = R.DRAW<TF.DR.INPUTTER>
            CO.DR   = R.DRAW<TF.DR.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.DR,NA.CO.DR)
            INPUTT3 = FIELD(INP3,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT3,NAME3)
            XXX8<1,K>[30,20] = KEY.LIST3<K>
            XXX8<1,K>[50,20] = CO.DR
            XXX8<1,K>[70,20] = NA.CO.DR
            XXX41<1,K>[30,4]  = "----"
            PRINT XXX8<1,K>
            PRINT XXX41<1,K>
        NEXT K
    END ELSE
        XX3 = ''
        XXX7<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX7<1,1>[60,2]    = "DR"
        XXX40<1,1>[30,4]  = "----"
        PRINT XXX7<1,1>
        PRINT XXX40<1,1>
    END
    RETURN
*-----------------------------------------------------------------------
SEL.P.4:
    T.SEL4 = "SELECT FBNK.LETTER.OF.CREDIT$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG)
    IF SELECTED4 THEN
        XX4 = SELECTED4
        XXX9<1,1>[21,20]    = XX4
        XXX9<1,1>[50,2]    = KEY.LIST4[1,2]
        XXX42<1,1>[30,4]  = "-----"
        PRINT XXX9<1,1>
        PRINT XXX42<1,1>
        FOR KKK = 1 TO SELECTED4
            CALL F.READ(FN.LETTER,KEY.LIST4<KKK>,R.LETTER,F.LETTER,E4)
            INP4    = R.LETTER<TF.LC.INPUTTER>
            CO.LC   = R.LETTER<TF.LC.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.LC,NA.CO.LC)
            INPUTT4 = FIELD(INP4,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT4,NAME4)
            XXX10<1,KKK>[30,20] = KEY.LIST4<KKK>
            XXX10<1,KKK>[50,20] = CO.LC
            XXX10<1,KKK>[70,20] = NA.CO.LC
            XXX43<1,KKK>[30,4] = "----"
            PRINT XXX10<1,KKK>
            PRINT XXX43<1,KKK>
        NEXT L
    END ELSE
        XX4 = ''
        XXX9<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX9<1,1>[60,2]    = "LC"
        XXX42<1,1>[30,4]   = "-----"
        PRINT XXX9<1,1>
        PRINT XXX42<1,1>
    END
    RETURN
*-------------------------------------------------------------------
SEL.P.5:
    T.SEL5 = "SELECT F.IM.DOCUMENT.IMAGE$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL5,KEY.LIST5,"",SELECTED5,ER.MSG)
    IF SELECTED5 THEN
        FOR Q =1 TO SELECTED5
            CALL F.READ(FN.IM,KEY.LIST5<I>,R.IM,F.IM,E2)
            INPUTT    = R.IM<IM.DOC.INPUTTER>
            INPUTTER  = FIELD(INPUTT,'_',2)
            CALL DBR ('USER':@FM:EB.USE.DEPARTMENT.CODE,INPUTTER,DEPT)

            IF SELECTED5 THEN
                XX5 = SELECTED5
                XXX11<1,1>[21,20]    = XX5
                XXX11<1,1>[50,2]    = KEY.LIST5[1,2]
                XXX44<1,1>[30,4]    = "----"
                PRINT XXX11<1,1>
                PRINT XXX44<1,1>
                FOR M = 1 TO SELECTED5
                    CALL F.READ(FN.IM,KEY.LIST5<M>,R.IM,F.IM,E5)
                    INP5    = R.IM<IM.DOC.INPUTTER>
                    INPUTT5 = FIELD(INP5,'_',2)
                    CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT5,NAME5)
                    XXX12<1,M>[30,20] = KEY.LIST5<M>
                    XXX12<1,M>[50,20] = INPUTT5
                    XXX12<1,M>[70,20] = NAME5
                    XXX45<1,M>[30,4] = "----"
                    PRINT XXX12<1,M>
                    PRINT XXX45<1,M>
                NEXT M
            END
        NEXT Q
    END ELSE
        XX5 = ''
        XXX11<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX11<1,1>[60,2]    = "IM"
        XXX44<1,1>[30,5]    = "----"
        PRINT XXX11<1,1>
        PRINT XXX44<1,1>
    END
    RETURN
*-----------------------------------------------------
SEL.P.6:
    T.SEL6 = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ER.MSG)
    IF SELECTED6 NE 0 THEN
        XX6 = SELECTED6
        XXX13<1,1>[21,20]    = XX6
        XXX13<1,1>[50,2]    = KEY.LIST6[1,2]
        XXX46<1,1>[30,4]   = "----"
        PRINT XXX13<1,1>
        PRINT XXX46<1,1>
        NN = ''
        FOR NN = 1 TO SELECTED6
            CALL F.READ(FN.FT,KEY.LIST6<NN>,R.FT,F.FT,E6)
            INP6    = R.FT<FT.INPUTTER>
            CO.FT   = R.FT<FT.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.FT,NA.CO.FT)
            INPUTT6 = FIELD(INP6,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT6,NAME6)
            XXX14<1,NN>[30,20] = KEY.LIST6<NN>
            XXX14<1,NN>[50,20] = CO.FT
            XXX14<1,NN>[70,20] = NA.CO.FT
            XXX47<1,NN>[30,4] = "----"
            PRINT XXX14<1,NN>
            PRINT XXX47<1,NN>
        NEXT NN
    END ELSE
        XX6 = ''
        XXX13<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX13<1,1>[60,2]    = "FT"
        XXX46<1,1>[30,4]   = "----"
        PRINT XXX13<1,1>
        PRINT XXX46<1,1>
    END
    RETURN
*-------------------------------------------------------------
SEL.P.7:
    T.SEL7 = "SELECT FBNK.TELLER$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL7,KEY.LIST7,"",SELECTED7,ER.MSG)
    IF SELECTED7 THEN
        XX7 = SELECTED7
        XXX15<1,1>[21,20]    = XX7
        XXX15<1,1>[50,2]    = KEY.LIST7[1,2]
        XXX48<1,1>[30,4]    = "----"
        PRINT XXX15<1,1>
        PRINT XXX48<1,1>
        FOR MM = 1 TO SELECTED7
            CALL F.READ(FN.TT,KEY.LIST7<MM>,R.TT,F.TT,E7)
            INP7    = R.TT<TT.TE.INPUTTER>
            CO.TEL   = R.TT<TT.TE.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.TEL,NA.CO.TEL)
            INPUTT7 = FIELD(INP7,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT7,NAME7)
            XXX16<1,MM>[30,20] = KEY.LIST7<MM>
            XXX16<1,MM>[50,20] = CO.TEL
            XXX16<1,MM>[70,20] = NA.CO.TEL
            XXX49<1,MM>[30,4] = "----"
            PRINT XXX16<1,MM>
            PRINT XXX49<1,MM>
        NEXT MM
    END ELSE
        XX7 = ''
        XXX15<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX15<1,1>[60,2]    = "TT"
        XXX48<1,1>[30,4]    = "----"
        PRINT XXX15<1,1>
        PRINT XXX48<1,1>
    END
    RETURN
*----------------------------------------------------------------------
SEL.P.8:
    T.SEL8 = "SELECT F.INF.MULTI.TXN$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL8,KEY.LIST8,"",SELECTED8,ER.MSG)
    IF SELECTED8 THEN
        XX8 = SELECTED8
        XXX17<1,1>[21,20]    = XX8
        XXX17<1,1>[50,2]    = KEY.LIST8[1,2]
        XXX50<1,1>[30,4]    = "----"
        PRINT XXX17<1,1>
        PRINT XXX50<1,1>
        FOR SS = 1 TO SELECTED8
            CALL F.READ(FN.IN,KEY.LIST8<SS>,R.IN,F.IN,E8)
            INP8    = R.IN<INF.MLT.INPUTTER>
            CO.IN   = R.IN<INF.MLT.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.IN,NA.CO.IN)
            INPUTT8 = FIELD(INP8,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT8,NAME8)
            XXX18<1,SS>[30,20] = KEY.LIST8<SS>
            XXX18<1,SS>[50,20] = CO.IN
            XXX18<1,SS>[70,20] = NA.CO.IN
            XXX51<1,SS>[30,4]    = "----"
            PRINT XXX18<1,SS>
            PRINT XXX51<1,SS>
        NEXT SS
    END ELSE
        XX8 = ''
        XXX17<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX17<1,1>[60,3]    = "INF"
        XXX50<1,1>[30,4]    = "----"
        PRINT XXX17<1,1>
        PRINT XXX50<1,1>
    END
    RETURN
*-------------------------------------------------------------
SEL.P.9:
    T.SEL9 = "SELECT FBNK.ACCOUNT.CLOSURE$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL9,KEY.LIST9,"",SELECTED9,ER.MSG)
    IF SELECTED9 THEN
        XX9 = SELECTED9
        XXX19<1,1>[21,20]   = XX9
        XXX19<1,1>[50,20]   = KEY.LIST9[1,2]
        XXX52<1,1>[30,4]    = "----"
        PRINT XXX19<1,1>
        PRINT XXX52<1,1>
        FOR QQ = 1 TO SELECTED9
            CALL F.READ(FN.CLOSE,KEY.LIST9<QQ>,R.CLOSE,F.CLOSE,E9)
            INP9    = R.CLOSE<AC.ACL.INPUTTER>
            CO.CLS  = R.CLOSE<AC.ACL.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.CLS,NA.CO.CLS)
            INPUTT9 = FIELD(INP9,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT9,NAME9)
            XXX20<1,QQ>[30,20] = KEY.LIST9<QQ>
            XXX20<1,QQ>[50,20] = CO.CLS
            XXX20<1,QQ>[70,20] = NA.CO.CLS
            XXX53<1,QQ>[30,4]    = "----"
            PRINT XXX20<1,QQ>
            PRINT XXX53<1,QQ>
        NEXT QQ
    END ELSE
        XX9 = ''
        XXX19<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX19<1,1>[60,4]    = "ACLS"
        XXX52<1,1>[30,4]    = "----"
        PRINT XXX19<1,1>
        PRINT XXX52<1,1>
    END
    RETURN
*--------------------------------------------------------------------
SEL.P.10:
    T.SEL10 = "SELECT FBNK.MM.MONEY.MARKET$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL10,KEY.LIST10,"",SELECTED10,ER.MSG)
    IF SELECTED10 THEN
        XX10 = SELECTED10
        XXX21<1,1>[21,20]    = XX10
        XXX21<1,1>[50,20]    = KEY.LIST10[1,2]
        XXX54<1,1>[30,4]    = "----"
        PRINT  XXX21<1,1>
        PRINT  XXX54<1,1>

        FOR BB = 1 TO SELECTED10
            CALL F.READ(FN.MM,KEY.LIST10<BB>,R.MM,F.MM,E10)
            INP10    = R.MM<MM.INPUTTER>
            CO.MM    = R.MM<MM.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.MM,NA.CO.MM)
            INPUTT10 = FIELD(INP10,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT10,NAME10)
            XXX22<1,BB>[30,20] = KEY.LIST10<BB>
            XXX22<1,BB>[50,20] = CO.MM
            XXX22<1,BB>[70,20] = NA.CO.MM
            XXX55<1,BB>[30,4] = "----"
            PRINT XXX22<1,BB>
            PRINT XXX55<1,BB>
        NEXT BB
    END

    IF SELECTED10 EQ 0 THEN
        XX10 = ''
        XXX21<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX21<1,1>[60,2]    = "MM"
        XXX54<1,1>[30,4]    = "----"
        PRINT XXX21<1,1>
        PRINT XXX54<1,1>
    END
    RETURN
*-----------------------------------------------------------
SEL.P.11:
    T.SEL11 = "SELECT FBNK.FOREX$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL11,KEY.LIST11,"",SELECTED11,ER.MSG)
    IF SELECTED11 NE 0 THEN
        XX11 = SELECTED11
        XXX23<1,1>[21,20]    = XX11
        XXX23<1,1>[50,20]    = KEY.LIST11[1,2]
        PRINT  XXX23<1,1>
        FOR AA = 1 TO SELECTED11
            CALL F.READ(FN.FOR,KEY.LIST11<AA>,R.FOR,F.FOR,E11)
            INP11    = R.FOR<FX.INPUTTER>
            CO.FX    = R.FOR<FX.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.FX,NA.CO.FX)
            INPUTT11 = FIELD(INP11,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT11,NAME11)
            XXX24<1,AA>[30,20] = KEY.LIST11<AA>
            XXX24<1,AA>[50,20] = CO.FX
            XXX24<1,AA>[70,20] = NA.CO.FX
            PRINT XXX24<1,AA>
        NEXT AA
    END

    IF SELECTED11 EQ 0 THEN
        XX11 = ''
        XXX23<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX23<1,1>[60,2]    = "FX"
        PRINT XXX23<1,1>
    END
    RETURN
*-----------------------------------------------------------------
SEL.P.12:
    T.SEL12 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21020 AND CATEGORY LE 21025 AND INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL12,KEY.LIST12,"",SELECTED12,ER.MSG)
    IF SELECTED12 NE 0 THEN
        XX12 = SELECTED12
        XXX25<1,1>[21,20]    = XX12
        XXX25<1,1>[50,20]    = KEY.LIST12[1,2]
        FOR CC = 1 TO SELECTED11
            CALL F.READ(FN.LD,KEY.LIST12<CC>,R.LD,F.LD,E12)
            INP12    = R.LD<LD.INPUTTER>
            CO.LD1   = R.LD<LD.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.LD1,NA.CO.LD1)
            INPUTT12 = FIELD(INP12,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT12,NAME12)
            XXX26<1,CC>[30,20] = KEY.LIST12<CC>
            XXX26<1,CC>[50,20] = CO.LD1
            XXX26<1,CC>[70,20] = NA.CO.LD1
        NEXT CC
    END

    IF SELECTED12 EQ 0 THEN
        XX12 = ''
        XXX25<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX25<1,1>[60,4]    = "LCD"
    END
    RETURN
*---------------------------------------------------
SEL.P.13:
    T.SEL13 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21001 AND CATEGORY LE 21010 AND INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL13,KEY.LIST13,"",SELECTED13,ER.MSG)
    IF SELECTED13 NE 0 THEN
        XX13 = SELECTED13
        XXX27<1,1>[21,20]    = XX13
        XXX27<1,1>[50,20]    = KEY.LIST13[1,2]
        FOR DD = 1 TO SELECTED13
            CALL F.READ(FN.LD,KEY.LIST13<DD>,R.LD,F.LD,E13)
            INP13= R.LD<LD.INPUTTER>
            CO.LD2   = R.LD<LD.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.LD2,NA.CO.LD2)
            INPUTT13 = FIELD(INP13,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT13,NAME13)
            XXX28<1,DD>[30,20] = KEY.LIST13<DD>
            XXX28<1,DD>[50,20] = CO.LD2
            XXX28<1,DD>[70,20] = NA.CO.LD2
        NEXT DD
    END

    IF SELECTED13 EQ 0 THEN
        XX13 = ''
        XXX27<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX27<1,1>[60,3]    = "LD"
    END
    RETURN
*-------------------------------------------------------------
SEL.P.14:
    T.SEL14 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY EQ 21096 AND INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL14,KEY.LIST14,"",SELECTED14,ER.MSG)
    IF SELECTED14 NE 0 THEN
        XX14 = SELECTED14
        XXX29<1,1>[21,20]    = XX14
        XXX29<1,1>[50,20]    = KEY.LIST14[1,2]
        FOR GG = 1 TO SELECTED14
            CALL F.READ(FN.LD,KEY.LIST14<GG>,R.LD,F.LD,E14)
            INP14    = R.LD<LD.INPUTTER>
            CO.LD3   = R.LD<LD.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.LD3,NA.CO.LD3)
            INPUTT14 = FIELD(INP14,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT14,NAME14)
            XXX30<1,GG>[30,20] = KEY.LIST14<GG>
            XXX30<1,GG>[50,20] = CO.LD3
            XXX30<1,GG>[70,20] = NA.CO.LD3
        NEXT GG
    END

    IF SELECTED14 EQ 0 THEN
        XX14 = ''
        XXX29<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX29<1,1>[60,3]    = "LG"
    END
    RETURN
*----------------------------------------------
SEL.P.15:
    T.SEL15 = "SELECT F.SCB.BT.BATCH$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL15,KEY.LIST15,"",SELECTED15,ER.MSG)
    IF SELECTED15 NE 0 THEN
        XX15 = SELECTED15
        XXX31<1,1>[21,20]    = XX15
        XXX31<1,1>[50,20]    = KEY.LIST15[1,2]
        FOR HH = 1 TO SELECTED15
            CALL F.READ(FN.BT,KEY.LIST15<HH>,R.BT,F.BT,E15)
            INP15    = R.BT<SCB.BT.INPUTTER>
            CO.BT  = R.BT<SCB.BT.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.BT,NA.CO.BT)
            INPUTT15 = FIELD(INP15,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT15,NAME15)
            XXX30<1,HH>[30,20] = KEY.LIST15<HH>
            XXX30<1,HH>[50,20] = CO.BT
            XXX30<1,HH>[70,20] = NA.CO.BT
        NEXT HH
    END

    IF SELECTED15 EQ 0 THEN
        XX15 = ''
        XXX31<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX31<1,1>[60,3]    = "BT"
    END
    RETURN
*--------------------------------------------------------
SEL.P.16:
    T.SEL16 = "SELECT FBNK.BILL.REGISTER$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL16,KEY.LIST16,"",SELECTED16,ER.MSG)
    IF SELECTED16 NE 0 THEN
        XX16 = SELECTED16
        XXX33<1,1>[21,20]  = SELECTED16
        FOR YY = 1 TO SELECTED16
            CALL F.READ(FN.BR,KEY.LIST16<YY>,R.BR,F.BR,E16)
            INP16    = R.BR<EB.BILL.REG.INPUTTER>
            CO.BR   = R.BR<EB.BILL.REG.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.BR,NA.CO.BR)
            INPUTT16 = FIELD(INP16,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT16,NAME16)
            XXX32<1,YY>[30,20] = KEY.LIST16<YY>
            XXX32<1,YY>[50,20] = CO.BR
            XXX32<1,YY>[70,20] = NA.CO.BR
            XXX33<1,YY>[50,5]  = KEY.LIST16<YY>[1,2]
        NEXT YY
    END

    IF SELECTED16 EQ 0 THEN
        XX16 = ''
        XXX33<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX33<1,1>[60,3]    = "BR"
    END
    RETURN
*----------------------------------
SEL.P.17:
    T.SEL17 = "SELECT F.SCB.MONTHLY.PAY.CBE$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL17,KEY.LIST17,"",SELECTED17,ER.MSG)
    IF SELECTED17 NE 0 THEN
        XX17 = SELECTED17
        XXX35<1,1>[21,20]    = XX17
        XXX35<1,1>[50,20]    = KEY.LIST17[1,2]
        PRINT  XXX35<1,1>
        FOR ZZ = 1 TO SELECTED17
            CALL F.READ(FN.CBE,KEY.LIST17<ZZ>,R.CBE,F.CBE,E17)

            INP17    = R.CBE<EB.BILL.REG.INPUTTER>
            CO.CBE   = R.CBE<CBE.PAY.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.CBE,NA.CO.CBE)
            INPUTT17 = FIELD(INP17,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT17,NAME17)
            XXX34<1,ZZ>[30,20] = KEY.LIST17<ZZ>
            XXX34<1,ZZ>[50,20] = CO.CBE
            XXX34<1,ZZ>[70,20] = NA.CO.CBE
            PRINT XXX34<1,ZZ>
        NEXT ZZ
    END

    IF SELECTED17 EQ 0 THEN
        XX17 = ''
        XXX35<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX35<1,1>[60,4]    = "CBE"
        PRINT XXX35<1,1>
    END
    RETURN
*--------------------------------------------------
SEL.P.18:
    T.SEL18 = "SELECT FBNK.LIMIT$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL18,KEY.LIST18,"",SELECTED18,ER.MSG)
    IF SELECTED18 NE 0 THEN
        XX18 = SELECTED18
        XXX37<1,1>[21,20]    = XX18
        XXX37<1,1>[50,20]    = "LIM"
        PRINT  XXX37<1,1>
        FOR WW = 1 TO SELECTED18
            CALL F.READ(FN.LIM,KEY.LIST18<WW>,R.LIM,F.LIM,E18)
            INP18    = R.LIM<LI.INPUTTER>
            CO.LIM   = R.LIM<LI.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.LIM,NA.CO.LIM)
            INPUTT18 = FIELD(INP18,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT18,NAME18)
            XXX36<1,WW>[30,20] = KEY.LIST18<WW>
            XXX36<1,WW>[50,20] = CO.LIM
            XXX36<1,WW>[70,20] = NA.CO.LIM
            PRINT XXX36<1,WW>
        NEXT WW
    END

    IF SELECTED18 EQ 0 THEN
        XX18 = ''
        XXX37<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX37<1,1>[60,4]    = "LIM"
        PRINT XXX37<1,1>
    END
    RETURN
*-----------------------------------------------------------
SEL.P.19:
    T.SEL19 = "SELECT FBNK.COLLATERAL$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL19,KEY.LIST19,"",SELECTED19,ER.MSG)
    IF SELECTED19 NE 0 THEN
        XX19 = SELECTED19
        XXX39<1,1>[21,20]    = XX19
        XXX39<1,1>[50,20]    = "COL"
        PRINT  XXX39<1,1>
        FOR PP = 1 TO SELECTED19
            CALL F.READ(FN.COLL,KEY.LIST19<PP>,R.COLL,F.COLL,E19)
            INP19    = R.COLL<COLL.INPUTTER>
            CO.COLL   = R.COLL<COLL.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.COLL,NA.CO.COLL)
            INPUTT19 = FIELD(INP19,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT19,NAME19)
            XXX38<1,PP>[30,20] = KEY.LIST19<PP>
            XXX38<1,PP>[50,20] = CO.COLL
            XXX38<1,PP>[70,20] = NA.CO.COLL
            PRINT XXX38<1,PP>
        NEXT PP
    END

    IF SELECTED19 EQ 0 THEN
        XX19 = ''
        XXX39<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX39<1,1>[60,4]    = "COLL"
        PRINT XXX39<1,1>
    END
    RETURN
*--------------------------------------------------------
SEL.P.20:
    PP = ''

    T.SEL20 = "SELECT FBNK.COLLATERAL.RIGHT$NAU WITH INPUTTER LIKE  " : INP.ID
    CALL EB.READLIST(T.SEL20,KEY.LIST20,"",SELECTED20,ER.MSG)
    IF SELECTED20 NE 0 THEN
        XX20 = SELECTED20
        XXX41<1,1>[21,20]    = XX20
        XXX41<1,1>[50,3]    = "COR"
        PRINT  XXX41<1,1>
        FOR MN = 1 TO SELECTED20
            CALL F.READ(FN.COR,KEY.LIST20<MN>,R.COR,F.COR,E20)
            INP20    = R.COR<COLL.RIGHT.INPUTTER>
            CO.COR   = R.COR<COLL.RIGHT.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.COR,NA.CO.COR)
            INPUTT20 = FIELD(INP20,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT20,NAME20)
            XXX40<1,MN>[30,20] = KEY.LIST20<MN>
            XXX40<1,MN>[50,20] = CO.COR
            XXX40<1,MN>[70,20] = NA.CO.COR
            PRINT XXX40<1,MN>
        NEXT MN
    END

    IF SELECTED20 EQ 0 THEN
        XX20 = ''
        XXX41<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX41<1,1>[60,4]    = "Cor"
        PRINT XXX41<1,1>
    END
    RETURN
*--------------------------------------------------------------
SEL.P.21:
    T.SEL21 = "SELECT F.SCB.BOAT.DATA$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL21,KEY.LIST21,"",SELECTED21,ER.MSG)
    IF SELECTED21 NE 0 THEN
        XX21 = SELECTED21
        XXX42<1,1>[21,20]    = XX21
        XXX42<1,1>[50,8]    = "BOTDATA"
        PRINT  XXX42<1,1>
        FOR MN = 1 TO SELECTED21
            CALL F.READ(FN.BOT,KEY.LIST21<MN>,R.BOT,F.BOT,E21)
            INP21    = R.BOT<BOD.INPUTTER>
            CO.BOT   = R.BOT<BOD.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.BOT,NA.CO.BOT)
            INPUTT21 = FIELD(INP21,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT21,NAME21)
            XXX43<1,MN>[30,20] = KEY.LIST21<MN>
            XXX43<1,MN>[50,20] = CO.BOT
            XXX43<1,MN>[70,20] = NA.CO.BOT
            PRINT XXX43<1,MN>
        NEXT MN
    END

    IF SELECTED21 EQ 0 THEN
        XX21 = ''
        XXX42<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX42<1,1>[60,8]    = "BOTDATA"
        PRINT XXX42<1,1>
    END
    RETURN
*--------------------------------------------------------
SEL.P.22:
    T.SEL22 = "SELECT F.SCB.BOAT.TOTAL$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL22,KEY.LIST22,"",SELECTED22,ER.MSG)
    IF SELECTED22 NE 0 THEN
        XX22 = SELECTED22
        XXX44<1,1>[21,20]    = XX22
        XXX44<1,1>[50,7]    = "BOTTOT"
        PRINT  XXX44<1,1>
        FOR MN = 1 TO SELECTED22
            CALL F.READ(FN.BOT.TOT,KEY.LIST22<MN>,R.BOT.TOT,F.BOT.TOT,E21)
            INP22    = R.BOT.TOT<BOT.INPUTTER>
            CO.BOT.TOT   = R.BOT.TOT<BOT.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.BOT.TOT,NA.CO.BOT.TOT)
            INPUTT22 = FIELD(INP22,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT22,NAME22)
            XXX45<1,MN>[30,20] = KEY.LIST22<MN>
            XXX45<1,MN>[50,20] = CO.BOT.TOT
            XXX45<1,MN>[70,20] = NA.CO.BOT.TOT
            PRINT XXX45<1,MN>
        NEXT MN
    END

    IF SELECTED22 EQ 0 THEN
        XX22 = ''
        XXX44<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX44<1,1>[60,8]    = "BOTTOT"
        PRINT XXX44<1,1>
    END
    RETURN
*--------------------------------------------------------
SEL.P.23:
    T.SEL23 = "SELECT FBNK.CARD.ISSUE$NAU WITH INPUTTER LIKE " : INP.ID
    CALL EB.READLIST(T.SEL23,KEY.LIST23,"",SELECTED23,ER.MSG)
    IF SELECTED23 NE 0 THEN
        XX23 = SELECTED23
        XXX46<1,1>[21,20]    = XX23
        XXX46<1,1>[50,5]    = "CAD"
        PRINT  XXX46<1,1>
        FOR MMM = 1 TO SELECTED23
            CALL F.READ(FN.CARD,KEY.LIST23<MMM>,R.CARD,F.CARD,E21)
            INP23    = R.CARD<CARD.IS.INPUTTER>
            CO.CARD   = R.CARD<CARD.IS.CO.CODE>
            CALL DBR ('COMPANY'::@FM:EB.COM.COMPANY.NAME,CO.CARD,NA.CO.CARD)
            INPUTT23 = FIELD(INP23,'_',2)
            CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPUTT23,NAME23)
            XXX47<1,MMM>[30,20] = KEY.LIST23<MMM>
            XXX47<1,MMM>[50,20] = CO.CARD
            XXX47<1,MMM>[70,20] = NA.CO.CARD
            PRINT XXX47<1,MMM>
        NEXT MMM
    END

    IF SELECTED23 EQ 0 THEN
        XX23 = ''
        XXX46<1,1>[21,20]    = "�� ���� ������ ��� �����"
        XXX46<1,1>[60,4]    = "CAD"
        PRINT XXX46<1,1>
    END
    RETURN
*-----------------------------------------------------------------
SEL.P.END:
    XX2 = SELECTED23 + SELECTED22+SELECTED21+SELECTED20+SELECTED19+SELECTED18+SELECTED17+SELECTED16+SELECTED15+SELECTED14+SELECTED13+SELECTED12+SELECTED11+SELECTED10+SELECTED9+SELECTED8+SELECTED7+SELECTED6+SELECTED5+SELECTED4+SELECTED3+SELECTED2+SELECTED1
    XXX4<1,1>[1,120]   = '-------------------------------------------------------------'
    COMP = COMP : "-" : COMP
    COMP1 = COMP
    XXX4<1,1>[1,120]   = '------------------------------------------------------------------------------------'

    XXXX6<1,1>[1,5]  = '�������  : '
    XXXX7<1,1>[1,20] = '----------------'
    XXXX8<1,1>[1,132] = 'AC=������':';':'CU= �����':';':'TT= ������':';':'LC=����������' :';':'DR=�������� ���� - ��������':';':'SGIN= ���������' : ';' :'BR=������� ����������� '
    XXXX16<1,1>[1,132]= 'FT: ���� ����� ��������':';':'AC-CLS=����� ���� �������� ':';':'CDS=��������' :';':'LD =�������' :';':'LG  : ������ ������' :';':'MULT= �� ���� ��� ������ '
    XXXX17<1,1>[1,132]= 'LIM:���� �����':';':'COLL : ��������':';':'COR:��� �������� ':';':'CBE=��������� �������':';':'BT = ����� ������� �����������'
    XXXX18<1,1>[1,132]= 'BOT.DATA : ������ �������' :';':'BOT.TOTAL : ������ ����� �������' :';':'CAD : ��������'

    PRINT XXXX6
    PRINT XXXX7
    PRINT XXXX8
    PRINT XXXX16
    PRINT XXXX17
    PRINT XXXX18
    RETURN
*-------------------------------------------------------------
PRINT.HEAD:
*---------
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE,":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = HHH:":":MIN

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":SPACE(20):"���� �������� ����� ����� ����� "
    PR.HD :="'L'":"������� : ":T.DAY :SPACE(35) :"��� ������ : ":"'P'"
    PR.HD :="'L'":"����� : " : TIMEFMT
    PR.HD :="'L'":NAME.INP:SPACE(3) : ID.NO
    PR.HD :="'L'":STR('_',60)
    PR.HD :="'L'":"�����" :SPACE(13):"��� ��������":SPACE(25):"��� �������"
    PR.HD :="'L'":STR('_',60)

    PRINT
    HEADING PR.HD
*------------------------------------------------------------------
    RETURN
END
