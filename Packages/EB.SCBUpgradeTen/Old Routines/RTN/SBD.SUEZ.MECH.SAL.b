* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*--- CREATED BY NESSMA ON 2019/03/20
**    PROGRAM SBD.SUEZ.MECH.SAL
    SUBROUTINE SBD.SUEZ.MECH.SAL

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    TEXT = "FINISHED"  ; CALL REM
    RETURN
*---------------------------------------
INITIAL:
*--------
    SAL.DATE = ""
    YTEXT = "Enter Salary Date (YYYYMM): "
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    SAL.DATE = COMI

    OPENSEQ "/home/signat" , "SBD.SUEZ.MECH.SAL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"SBD.SUEZ.MECH.SAL.CSV"
        HUSH OFF
    END
    OPENSEQ "/home/signat" , "SBD.SUEZ.MECH.SAL.CSV" TO BB THEN
        CREATE BB THEN
            PRINT 'FILE SBD.SUEZ.MECH.SAL.CSV CREATED IN /home/signat'
        END ELSE
            STOP 'Cannot create SBD.SUEZ.MECH.SAL.CSV FILE File IN /home/signat'
        END
    END

    FN.CUS = "FBNK.CUSTOMER"  ; F.CUS = ""
    CALL OPF(FN.CUS, F.CUS)

    FN.CR = "F.SCB.SUEZ.MECH.SAL" ; F.CR = ""
    CALL OPF(FN.CR, F.CR)

    FN.AC = "FBNK.ACCOUNT" ; F.AC = ""
    CALL OPF(FN.AC, F.AC)

    T.SEL    = "" ; SEL.LIST = "" ; NO.OF.REC = "" ; ERR.SEL = ""
    CUS.ID   = "" ; R.CUS    = "" ; ERR.CUS   = ""

    BB.DATA    = ""
    RETURN
*---------------------------------------
PRINT.HEAD:
*-----------
    BB.DATA   = "SBD.SUEZ.MECH.SAL,,,,,"
    BB.DATA  := ",,,,,":" OUTPUT DATE : ":TODAY:",,,,,,,,,,,"
    BB.DATA  := ",,,,,":" SALARY DATE : ":COMI :",,,,,,,,,,,"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = "CUSTOMER.NO,"
    BB.DATA := "CUSTOMER.NAME,"
    BB.DATA := "CUSTOMER.BIRTHDATE,"
    BB.DATA := "Customer ID Number,"
    BB.DATA := "Open Date,"
    BB.DATA := "Employer,"
    BB.DATA := "Sector,"
    BB.DATA := "Salary Amount,"
    BB.DATA := "Salary Date,"
    BB.DATA := "Salary Currency,"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*---------------------------------------
PROCESS:
*-------
    T.SEL  = "SELECT F.SCB.SUEZ.MECH.SAL WITH SALARY.DATE EQ ":SAL.DATE
    CALL EB.READLIST(T.SEL,SEL.LIST,"",NO.OF.REC,ERR.SEL)
    IF NO.OF.REC THEN
        FOR NN = 1 TO NO.OF.REC
            CALL F.READ(FN.CR,SEL.LIST<NN>,R.CR,F.CR,ERR.CR)
            CALL F.READ(FN.CUS,R.CR<MCH.CUS.NO>,R.CUS,F.CUS,ERR.CUS)

            BB.DATA   = R.CR<MCH.CUS.NO>:","
            BB.DATA  := R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:","
            BB.DATA  := R.CUS<EB.CUS.BIRTH.INCORP.DATE>:","
            BB.DATA  := "'":R.CUS<EB.CUS.LOCAL.REF><1,CULR.NSN.NO>:"',"
            BB.DATA  := FMT(R.CUS<EB.CUS.CONTACT.DATE>,"####/##/##"):","
***            BB.DATA  := R.CUS<EB.CUS.GENDER>:","
***            BB.DATA  := R.CUS<EB.CUS.MARITAL.STATUS>:","
            CALL F.READ(FN.AC,R.CR<MCH.DB.ACCT>,R.AC,F.AC,ERR.AC)
            CALL F.READ(FN.CUS,R.AC<AC.CUSTOMER>,R.CUS2,F.CUS,ERR.CUS2)
            BB.DATA  := R.CUS2<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:","
            BB.DATA  := R.CUS<EB.CUS.SECTOR>:","
            BB.DATA  := R.CR<MCH.AMOUNT>:","
            BB.DATA  := FMT(R.CR<MCH.SALARY.DATE>,"####/##"):","
            BB.DATA  := R.CR<MCH.CURRENCY>:","

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT NN
    END

    BB.DATA  = ",,SELECTED":",":NO.OF.REC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = ",,END.OF.REPORT"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*-------------------------------------------------------
END
