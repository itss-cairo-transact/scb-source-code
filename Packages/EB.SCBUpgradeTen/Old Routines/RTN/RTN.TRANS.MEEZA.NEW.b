* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE RTN.TRANS.MEEZA.NEW

*----------------------------------
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LOCKED.EVENTS
*   $INCLUDE  I_F.AC.LOCKED.EVENTS.GUID
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.AC.LCK.LOCAL.REF
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------
    KEY.LIST = "" ; SELECTED = "" ;  ER.FT = "" ; SW2 = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'ACMZ'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
        SW2 = 1
    END ELSE
        SW2 = 0
    END
*--------------------------------------------------------------------
    IF SW2 = 0 THEN

        FN.AC.LOCK = "FBNK.AC.LOCKED.EVENTS" ;F.AC.LOCK = ""
        CALL OPF(FN.AC.LOCK,F.AC.LOCK)

        FN.AC.LOCK.GU = "F.AC.LOCKED.EVENTS.GUID" ;F.AC.LOCK.GU = ""
        CALL OPF(FN.AC.LOCK.GU,F.AC.LOCK.GU)

        FN.CI = "FBNK.CARD.ISSUE" ; F.CI = ""
        CALL OPF(FN.CI, F.CI)

        GOSUB PROCESS.MEEZA
        GOSUB INITIAL

**********************************************************
        TEXT = "�� �������" ; CALL REM
    END
    RETURN
*---------------------------------------------
PROCESS.MEEZA:
*------------
    TOD = TODAY
    MON = TOD[5,2]
    TOD = TOD[3,2]:MON:TOD[7,2]
    RETURN
*-------------------------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ""
    R.LINE = ""

    ERR =  '' ; ER = ''
    DIR.NAMEE  = "MEEZA"


    KEY.LIST2 = "" ; SELECTED2 = "" ;  ER.FX2=""
    T.SEL2 = "SELECT MEEZA WITH @ID LIKE ....txt"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.FX2)
    IF SELECTED2 THEN
        FOR II = 1 TO SELECTED2

            OPENSEQ DIR.NAMEE,KEY.LIST2<II> TO BB ELSE
                TEXT = "ERROR OPEN FILE ":KEY.LIST2<II> ; CALL REM
                RETURN
            END

            IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

            EOF    = ""
            LINE.NO  = 1
            CHK.FLAG = 0
            I = 1
            TAG.IND = 3
            LEN.IND = 3
            TAG.NO = ''
            LINE.NO.OLD = ''
            SCB.GUID = ''

            GOSUB CREATE.OFS
            LOOP WHILE NOT(EOF)
                READSEQ R.LINE FROM BB THEN
                    IF LINE.NO GE 2 THEN
                        CHK.VAL = LEN(R.LINE)
                        LOOP UNTIL I GT CHK.VAL
*======================================================
                            TAG.NO = R.LINE[I,TAG.IND]
                            I += TAG.IND
                            TAG.LEN = R.LINE[I,LEN.IND]
                            I += LEN.IND
                            VAL.D = R.LINE[I,TAG.LEN]
                            I += TAG.LEN
*======================================================
                            CUR = "EGP"

                            IF TAG.NO = "008" THEN
                                TRANS.ID =  VAL.D
                            END

                            IF TAG.NO = "062" THEN
                                SCB.GUID =  UPCASE(VAL.D)
                            END
                            IF TAG.NO = "010" THEN
                                PAN = VAL.D
                            END

                            IF TAG.NO = "012" THEN
                                AMT = VAL.D/100
                            END

                            IF TAG.NO = "009" THEN
                                VAL = VAL.D
                            END
*======================================================
                        REPEAT

                        I = 1
*-------------CLEAR VARIBALES----------
                        ACCT             = ''
                        LOCAL.REF.LOCK   = ''
                        ATM.123.LOCK     = ''
                        RETR.LOCK        = ''
                        CRD = ''
                        LOCAL.REF = ''

*-------------TO GET ACCOUNT NUMBER FROM AC.LOCKED.EVENTS----------

                        CALL F.READ(FN.AC.LOCK.GU,SCB.GUID,R.AC.LOCK.GU,F.AC.LOCK.GU,ERR.LOCK.GU)
                        IF NOT(ERR.LOCK.GU) THEN
                            LOOP
                                REMOVE ACC.NO FROM R.AC.LOCK.GU SETTING POS
                            WHILE ACC.NO:POS
                                AC.LOCK.ID = FIELD(ACC.NO,"*",2)
                                CALL F.READ(FN.AC.LOCK,AC.LOCK.ID,R.AC.LOCK,F.AC.LOCK,ERR.AC.LOCK)
                                ACCT             = R.AC.LOCK<AC.LCK.ACCOUNT.NUMBER>
                                LOCAL.REF.LOCK   = R.AC.LOCK<AC.LCK.LOCAL.REF>
                                ATM.123.LOCK     = LOCAL.REF.LOCK<1,AC.LCK.LOC.ATM.123.REF.NO>
                                RETR.LOCK        = LOCAL.REF.LOCK<1,AC.LCK.LOC.RETRIVAL.REF.NO>
                            REPEAT
                        END

*------------TO GET ACCOUNT FROM  PAN IS CARD NUMBER---------------

                        IF ERR.AC.LOCK OR ERR.LOCK.GU THEN
                            CRD           = "ATMC.":PAN

                            CALL F.READ(FN.CI,CRD,R.CI,F.CI,ERR.CI)
                            LOCAL.REF = R.CI<CARD.IS.LOCAL.REF>

                            ACCT = LOCAL.REF<1,LRCI.FAST.ACCT.NO>
                        END
***------------------------------------------------------------------
                        IF TRANS.ID EQ 'CT' THEN

                            IF VAL EQ '0100' THEN
                                IF NOT(ERR.LOCK.GU) THEN
                                    GOSUB BUILD.RECORD.LOCKS
                                END
                                GOSUB BUILD.RECORD.FT
                            END ELSE
                                GOSUB BUILD.RECORD.FT.REV
                            END

                            TRANS.ID  =  ''
                            VAL = ''
                        END


                    END
                    LINE.NO ++

                END ELSE
                    EOF = 1
                END
            REPEAT

            CLOSESEQ BB

            EXECUTE 'COPY FROM MEEZA ':KEY.LIST2<II>:' TO MEEZA/MEEZA.DONE'
            EXECUTE 'DELETE MEEZA ':KEY.LIST2<II>

            EXECUTE 'COPY FROM MEEZA TO OFS.IN ':NEW.FILE
            EXECUTE 'DELETE MEEZA ':NEW.FILE

        NEXT II
    END ELSE
        TEXT = "ERROR NO FILES FOUND" ; CALL REM
    END
    RETURN
*---------------------------------------------------
CREATE.OFS:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0

    OFS.REC          = ""
    OFS.OPERATION    = ""
    OFS.OPTIONS      = ""
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = "MEEZA"        ;*OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "MEEZA-LOCKS-":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME
    END

    OPENSEQ DIR.NAME, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME
        END
    END
    RETURN
*------------------------------------------------------------------
BUILD.RECORD.LOCKS:
*------------------
    MSG.OFS.LOKS  = "AC.LOCKED.EVENTS,ATM.123/R,ATMPOS//EG0010099,"
    MSG.OFS.LOKS := SCB.GUID

    WRITESEQ MSG.OFS.LOKS TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':MSG.OFS.LOKS
    END

    RETURN
*------------------------------------------------------------------
BUILD.RECORD.FT:
*------------
    MSG.OFS.FT  = "FUNDS.TRANSFER,SCB.MEEZA,ATMPOS//EG0010099,,"

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"ACMZ"
    OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY=":CUR
    OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY=":CUR
    OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO=":ACCT
    OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO=":"EGP1632600010099"
    OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT=":AMT
    OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE=":TODAY
    OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE=":TODAY
    OFS.MESSAGE.DATA :=  ",COMMISSION.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",CHARGE.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N=":"NO"
    OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N=":"NO"
    OFS.MESSAGE.DATA :=  ",THIRD.REINB.INS=":SCB.GUID
    OFS.MESSAGE.DATA :=  ",ATM.123.REF=":ATM.123.LOCK
    OFS.MESSAGE.DATA :=  ",RETRIVAL.REF.NO=":RETR.LOCK

    MSG.OFS.FT := OFS.MESSAGE.DATA
    WRITESEQ MSG.OFS.FT TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':MSG.OFS.FT
    END
    RETURN
*------------------------------------------------------------------------------
BUILD.RECORD.FT.REV:
*-------------------
    MSG.OFS.FT  = "FUNDS.TRANSFER,SCB.MEEZA,ATMPOS//EG0010099,,"

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"ACMZ"
    OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY=":CUR
    OFS.MESSAGE.DATA :=  ",CREDIT.CURRENCY=":CUR
    OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO=":"EGP1632600010099"
    OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO=":ACCT
    OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT=":AMT
    OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE=":TODAY
    OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE=":TODAY
    OFS.MESSAGE.DATA :=  ",COMMISSION.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",CHARGE.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",ORDERING.BANK=":"SCB"
    OFS.MESSAGE.DATA :=  ",DR.ADVICE.REQD.Y.N=":"NO"
    OFS.MESSAGE.DATA :=  ",CR.ADVICE.REQD.Y.N=":"NO"
    OFS.MESSAGE.DATA :=  ",THIRD.REINB.INS=":SCB.GUID

    MSG.OFS.FT := OFS.MESSAGE.DATA
    WRITESEQ MSG.OFS.FT TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':MSG.OFS.FT
    END
    RETURN
END
