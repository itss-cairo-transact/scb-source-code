* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.AC.LD.BAL

*    PROGRAM  SBD.AC.LD.BAL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.AC.LD.BAL
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D

*-------------------------------------------------------------------

    FN.SCB = 'F.SCB.AC.LD.BAL' ; F.SCB = '' ; R.SCB = ''
    CALL OPF (FN.SCB,F.SCB)
    SCB.IDD = 'EG0010001-CURR-':TODAY:'-MID'
    CALL F.READ(FN.SCB,SCB.IDD,R.SCB,F.SCB,SCB.ERR11)

    IF SCB.ERR11 THEN
        TIME.TYPE = 'MID'
    END ELSE
        TIME.TYPE = 'FINAL'
    END

    GOSUB INITIATE

    GOSUB PROCESS.AC.OPEN
    GOSUB PROCESS.AC.ONLINE

    GOSUB PROCESS.LD.OPEN
    GOSUB PROCESS.LD.ONLINE

    GOSUB PROCESS.DIFF

    RETURN

*-----------------------------INITIALIZATIONS------------------------

INITIATE:

    FN.LD   ='FBNK.LD.LOANS.AND.DEPOSITS'  ; F.LD  =''  ; R.LD  =''
    CALL OPF(FN.LD,F.LD)

    FN.ACC  = 'FBNK.ACCOUNT'               ; F.ACC = '' ; R.ACC = ''
    CALL OPF( FN.ACC,F.ACC)

    FN.CUR  = 'FBNK.CURRENCY'              ; F.CUR = '' ; R.CUR = ''
    CALL OPF (FN.CUR,F.CUR)

    FN.SCB  = 'F.SCB.AC.LD.BAL'            ; F.SCB = '' ; R.SCB = ''
    CALL OPF (FN.SCB,F.SCB)

    FN.PRT  = 'F.CATEG.MAS.D'              ; F.PRT = '' ; R.PRT = ''
    CALL OPF (FN.PRT,F.PRT)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM' ; F.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)

    ETEXT            = ''
    LCY.AC.BAL.OPN   = 0
    LCY.AC.BAL.ONLN  = 0
    LCY.AC.BAL.DIF   = 0
    FCY.AC.BAL.OPN   = 0
    FCY.AC.BAL.ONLN  = 0
    FCY.AC.BAL.DIF   = 0
    LCY.LD.BAL.NEW   = 0
    FCY.LD.BAL.NEW   = 0
    FLAG             = ''
    TYPE             = ''
    R.SCB            = ''
    CATEG            = ''
    CURR             = ''
    COM.CODE         = ''
    RETURN

*------------------------                  ------------------------
*------------------------ Account  . Opne  ------------------------
*------------------------                  ------------------------

PROCESS.AC.OPEN:

*   T.SEL  = "SELECT F.CATEG.MAS.D WITH CATD.CATEG.CODE IN ( 1001 6501 6502 6503 6504 6511 6512) BY CATD.BR BY CATD.CATEG.CODE "
    T.SEL  = "SELECT F.CATEG.MAS.D WITH CATD.CATEG.CODE IN ( 1001 6501 6502 6503 6504 6511 6512) AND (@ID UNLIKE '...0616...' AND @ID UNLIKE '...0610...' AND @ID UNLIKE '...0810...' AND @ID UNLIKE 'EG0010099...' )  BY CATD.BR BY CATD.CATEG.CODE "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    FOR I = 1 TO SELECTED

        CALL F.READ(FN.PRT,KEY.LIST<I>,R.PRT,F.PRT,PRT.ERR)
        CALL F.READ(FN.PRT,KEY.LIST<I+1>,R.PRT.1,F.PRT,PRT.ERR)

        AC.BAL.OPN  = R.PRT<CAT.CATD.BAL.IN.ACT.CY>

        COM.CODE    = R.PRT<CAT.CATD.BR>
        COM.CODE.1  = R.PRT.1<CAT.CATD.BR>

        CATEG       = R.PRT<CAT.CATD.CATEG.CODE>
        CATEG.1     = R.PRT.1<CAT.CATD.CATEG.CODE>

        CURR        = R.PRT<CAT.CATD.CY.ALPH.CODE>
        CUR.ID      = R.PRT<CAT.CATD.CY.ALPH.CODE>

        IF CURR EQ 'EGP' THEN
            CURR = 'CCY'
        END ELSE
            CURR =  'FCY'
        END

        IF CATEG EQ 1001  THEN
            TYPE = 'CURR'
        END
        IF ( CATEG GE 6501 AND CATEG LE 6511 ) THEN
            TYPE = 'SAVE'
        END

        IF CATEG EQ '6512' THEN
            FALG = '3'
            TYPE = 'LD05'
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
            DESC = "����� �����-���� 3 ����"
        END
        IF  CATEG.1 EQ 1001  THEN
            TYPE.1 = 'CURR'
        END
        IF ( CATEG.1 GE 6501 AND CATEG.1 LE 6511 ) THEN
            TYPE.1 = 'SAVE'
        END
        IF CATEG.1 EQ '6512' THEN
            FALG = '3'
            TYPE.1 = 'LD05'
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
            DESC = "����� �����-���� 3 ����"
        END

        REPT.BK   = COM.CODE :TYPE
        REPT.BK.1 = COM.CODE.1 :TYPE.1


        IF REPT.BK EQ REPT.BK.1 THEN


            IF CURR EQ 'FCY' THEN
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)

*     IF CUR.ID EQ 'JPY' THEN
*         RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1> / 100
*     END ELSE
*         RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
*     END
                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.OPN      = AC.BAL.OPN * RATE
                FCY.AC.BAL.OPN  += FCY.BAL.OPN
            END ELSE
                LCY.AC.BAL.OPN  += AC.BAL.OPN
            END
        END ELSE
            IF CURR EQ 'FCY' THEN
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)

*              IF CUR.ID EQ 'JPY' THEN
*                   RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1> / 100
*              END ELSE
*                 RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
*              END

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.OPN        = AC.BAL.OPN * RATE
                FCY.AC.BAL.OPN    += FCY.BAL.OPN

            END ELSE
                LCY.AC.BAL.OPN   += AC.BAL.OPN
            END

            D.T      = TIMEDATE()
            DAT.TIM  = '20':D.T

            SCB.ID   = COM.CODE :'-':TYPE:'-':TODAY :'-' :TIME.TYPE

            CALL F.READ(FN.SCB,SCB.ID,R.SCB,F.SCB,SCB.ERR)

            IF CATEG EQ 1001 THEN
                FALG = '1'
                TYPE = 'CURR'
                DESC = "������ �����"
            END

            IF ( CATEG GE 6501 AND CATEG LE 6511 ) THEN
                FALG = '2'
                TYPE = 'SAVE'
                DESC = "������ �����"
            END

            IF CATEG EQ '6512' THEN
                FALG = '3'
                TYPE = 'LD05'
                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
                DESC = "����� �����-���� 3 ����"
            END


            R.SCB<SCB.FLAG>             = FLAG
            R.SCB<SCB.COMPANY>          = COM.CODE
            R.SCB<SCB.DATE>             = TODAY
            R.SCB<SCB.TIME>             = D.T
            R.SCB<SCB.TYPE>             = TYPE
            R.SCB<SCB.CATEG>            = CATEG
            R.SCB<SCB.DESC>             = DESC


            R.SCB<SCB.LCY.AMT.OPEN>     = LCY.AC.BAL.OPN
            R.SCB<SCB.FCY.AMT.OPEN>     = FCY.AC.BAL.OPN
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****            WRITE  R.SCB TO F.SCB , SCB.ID ON ERROR
****                PRINT "CAN NOT WRITE RECORD":SCB.ID:"TO" :FN.SCB
****            END
            CALL F.WRITE(FN.SCB,SCB.ID,R.SCB)
****END OF UPDATE 24/3/2016*****************************
            CALL JOURNAL.UPDATE(SCB.ID)

            LCY.AC.BAL.OPN   = 0
            FCY.AC.BAL.OPN   = 0

        END

    NEXT I

    RETURN
*------------------------                  ------------------------
*------------------------ Account . Online ------------------------
*------------------------                  ------------------------

PROCESS.AC.ONLINE:

    ETEXT            = ''
    LCY.AC.BAL.OPN   = 0
    LCY.AC.BAL.ONLN  = 0
    LCY.AC.BAL.DIF   = 0
    FCY.AC.BAL.OPN   = 0
    FCY.AC.BAL.ONLN  = 0
    FCY.AC.BAL.DIF   = 0
    LCY.LD.BAL.NEW   = 0
    FCY.LD.BAL.NEW   = 0
    FLAG             = ''
    TYPE             = ''
    R.SCB            = ''
    CATEG            = ''
    CURR             = ''
    COM.CODE         = ''

    T.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1001 6501 6502 6503 6504 6511 6512) BY CO.CODE BY CATEGORY "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    FOR II = 1 TO SELECTED

        CALL F.READ(FN.ACC,KEY.LIST<II>,R.AC,F.ACC,ACC.ERR)
        CALL F.READ(FN.ACC,KEY.LIST<II+1>,R.AC.1,F.ACC,ACC.ERR)

        AC.BAL.ONLN = R.AC<AC.ONLINE.ACTUAL.BAL>

        COM.CODE    = R.AC<AC.CO.CODE>
        COM.CODE.1  = R.AC.1<AC.CO.CODE>

        CATEG       = R.AC<AC.CATEGORY>
        CATEG.1     = R.AC.1<AC.CATEGORY>

        CURR        = R.AC<AC.CURRENCY>
        CUR.ID      = R.AC<AC.CURRENCY>

        IF CURR EQ 'EGP' THEN
            CURR = 'CCY'
        END ELSE
            CURR =  'FCY'
        END

        IF CATEG EQ 1001  THEN
            TYPE = 'CURR'
        END
        IF ( CATEG GE 6501 AND CATEG LE 6511 ) THEN
            TYPE = 'SAVE'
        END

        IF CATEG EQ '6512' THEN
            FALG = '3'
            TYPE = 'LD05'
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
            DESC = "����� �����-���� 3 ����"
        END

        IF  CATEG.1 EQ 1001  THEN
            TYPE.1 = 'CURR'

        END
        IF ( CATEG.1 GE 6501 AND CATEG.1 LE 6511 ) THEN
            TYPE.1 = 'SAVE'
        END


        IF CATEG.1 EQ '6512' THEN
            FALG = '3'

            TYPE.1 = 'LD05'
            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
            DESC = "����� �����-���� 3 ����"
        END

        REPT.BK   = COM.CODE :TYPE
        REPT.BK.1 = COM.CODE.1 :TYPE.1


        IF REPT.BK EQ REPT.BK.1 THEN


            IF CURR EQ 'FCY' THEN
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)

*             IF CUR.ID EQ 'JPY' THEN
*                   RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1> / 100
*             END ELSE
*                 RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
*             END

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.ONLN     = AC.BAL.ONLN * RATE
                FCY.AC.BAL.ONLN += FCY.BAL.ONLN

            END ELSE
                LCY.AC.BAL.ONLN += AC.BAL.ONLN

            END
        END ELSE

            IF CURR EQ 'FCY' THEN
                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)

*                IF CUR.ID EQ 'JPY' THEN
*                   RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1> / 100
*              END ELSE
*                 RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
*            END

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.ONLN      = AC.BAL.ONLN * RATE
                FCY.AC.BAL.ONLN  += FCY.BAL.ONLN

            END ELSE
                LCY.AC.BAL.ONLN  += AC.BAL.ONLN

            END

            D.T      = TIMEDATE()
            DAT.TIM  = '20':D.T

            SCB.ID   = COM.CODE :'-':TYPE:'-':TODAY :'-' :TIME.TYPE

            CALL F.READ(FN.SCB,SCB.ID,R.SCB,F.SCB,SCB.ERR)

            IF CATEG EQ 1001 THEN
                FALG = '1'
                TYPE = 'CURR'
                DESC = "������ �����"
            END
            IF ( CATEG GE 6501 AND CATEG LE 6511 ) THEN
                FALG = '2'
                TYPE = 'SAVE'
                DESC = "������ �����"
            END
            IF CATEG EQ '6512' THEN
                FALG = '3'
                TYPE = 'LD05'
                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
                DESC = "����� �����-���� 3 ����"
            END


            R.SCB<SCB.FLAG>             = FLAG
            R.SCB<SCB.COMPANY>          = COM.CODE
            R.SCB<SCB.DATE>             = TODAY
            R.SCB<SCB.TIME>             = D.T
            R.SCB<SCB.TYPE>             = TYPE
            R.SCB<SCB.CATEG>            = CATEG
            R.SCB<SCB.DESC>             = DESC

            R.SCB<SCB.LCY.AMT.ONLINE>   = LCY.AC.BAL.ONLN
            R.SCB<SCB.FCY.AMT.ONLINE>   = FCY.AC.BAL.ONLN
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****            WRITE  R.SCB TO F.SCB , SCB.ID ON ERROR
****                PRINT "CAN NOT WRITE RECORD":SCB.ID:"TO" :FN.SCB
****            END
            CALL F.WRITE(FN.SCB,SCB.ID,R.SCB)
****END OF UPDATE 24/3/2016*****************************
            CALL JOURNAL.UPDATE(SCB.ID)
            LCY.AC.BAL.ONLN  = 0
            FCY.AC.BAL.ONLN  = 0

        END

    NEXT II

    RETURN

*------------------------  LD . OPEN  ------------------------

PROCESS.LD.OPEN:

    LCY.LD.BAL.OPN   = 0
    LCY.LD.BAL.ONLN  = 0
    LCY.LD.BAL.DIF   = 0
    FCY.LD.BAL.OPN   = 0
    FCY.LD.BAL.ONLN  = 0
    FCY.LD.BAL.DIF   = 0
    FCY.BAL          = 0
    FLAG             = ''
    TYPE             = ''
    R.SCB            = ''
    CATEG            = ''
    CURR             = ''
    COM.CODE         = ''

*    T.SEL  = "SELECT F.CATEG.MAS.D WITH ( CATD.CATEG.CODE GE 21001 AND CATD.CATEG.CODE LE 21032 ) BY CATD.BR BY CATD.CATEG.CODE "
    T.SEL  = "SELECT F.CATEG.MAS.D WITH ( CATD.CATEG.CODE GE 21001 AND CATD.CATEG.CODE LE 21032 ) AND ( @ID UNLIKE '...9990...' AND @ID UNLIKE '...0610...' AND @ID UNLIKE 'EG0010099...' AND @ID UNLIKE '...0810...' ) BY CATD.BR BY CATD.CATEG.CODE "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED.LD, ASD)


    FOR III = 1 TO SELECTED.LD

        CALL F.READ(FN.PRT,KEY.LIST<III>,R.PRT,F.PRT,PRT.ERR)
        CALL F.READ(FN.PRT,KEY.LIST<III+1>,R.PRT.1,F.PRT,PRT.ERR)

        LD.BAL      = R.PRT<CAT.CATD.BAL.IN.ACT.CY>
        COM.CODE    = R.PRT<CAT.CATD.BR>
        COM.CODE.1  = R.PRT.1<CAT.CATD.BR>

        CATEG       = R.PRT<CAT.CATD.CATEG.CODE>
        CATEG.1     = R.PRT.1<CAT.CATD.CATEG.CODE>

        CURR        = R.PRT<CAT.CATD.CY.ALPH.CODE>
        CUR.ID      = R.PRT<CAT.CATD.CY.ALPH.CODE>

        IF CURR EQ 'EGP' THEN
            CURR = 'CCY'
        END ELSE
            CURR =  'FCY'
        END

        IF ( CATEG GE 21001 AND CATEG LE 21010 ) THEN
            TYPE = 'LD':CATEG[4,2]
        END ELSE
            IF ( CATEG GE 21019 AND CATEG LE 21032 ) THEN
                TYPE = 'CD'
            END
        END

        IF ( CATEG.1 GE 21001 AND CATEG.1 LE 21010 ) THEN
            TYPE.1 = 'LD':CATEG.1[4,2]
        END ELSE
            IF ( CATEG.1 GE 21019 AND CATEG.1 LE 21032 ) THEN
                TYPE.1 = 'CD'
            END
        END

        REPT.BK.LD   = COM.CODE :TYPE
        REPT.BK.LD.1 = COM.CODE.1 :TYPE.1


        IF REPT.BK.LD EQ REPT.BK.LD.1 THEN

            IF CURR EQ 'FCY' THEN
*    CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)
*    RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.OPN       = LD.BAL * RATE
                FCY.LD.BAL.OPN   += FCY.BAL.OPN

            END ELSE
                LCY.LD.BAL.OPN   += LD.BAL

            END

        END ELSE

            IF CURR EQ 'FCY' THEN

*                CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)
*                RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.OPN      = LD.BAL * RATE
                FCY.LD.BAL.OPN  += FCY.BAL.OPN
            END ELSE
                LCY.LD.BAL.OPN  += LD.BAL
            END

            D.T      = TIMEDATE()
            DAT.TIM  = '20':D.T
            SCB.ID   = COM.CODE :'-':TYPE:'-':TODAY :'-' :TIME.TYPE

            CALL F.READ(FN.SCB,SCB.ID,R.SCB,F.SCB,SCB.ERR)

            IF ( CATEG GE 21001 AND CATEG LE 21010 ) THEN
                FALG = '3'
                TYPE = 'LD':CATEG[4,2]
                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
                DESC = LD.DESC
            END
            IF ( CATEG GE 21019 AND CATEG LE 21032 ) THEN
                FALG = '4'
                TYPE = 'CD'
                DESC = '������ �������'
            END

            R.SCB<SCB.FLAG>             = FLAG
            R.SCB<SCB.COMPANY>          = COM.CODE
            R.SCB<SCB.DATE>             = TODAY
            R.SCB<SCB.TIME>             = D.T
            R.SCB<SCB.TYPE>             = TYPE
            R.SCB<SCB.CATEG>            = CATEG
            R.SCB<SCB.DESC>             = DESC

            R.SCB<SCB.LCY.AMT.OPEN>     = LCY.LD.BAL.OPN
            R.SCB<SCB.FCY.AMT.OPEN>     = FCY.LD.BAL.OPN
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****            WRITE  R.SCB TO F.SCB , SCB.ID ON ERROR
****                PRINT "CAN NOT WRITE RECORD":SCB.ID:"TO" :FN.SCB
****            END
            CALL F.WRITE(FN.SCB,SCB.ID,R.SCB)
****END OF UPDATE 24/3/2016*****************************
            CALL JOURNAL.UPDATE(SCB.ID)

            LCY.LD.BAL.OPN   = 0
            FCY.LD.BAL.OPN   = 0

        END

    NEXT III

    RETURN

**------------------------------------------------------------------
*------------------------  LD . Online      ------------------------
**------------------------------------------------------------------

PROCESS.LD.ONLINE:

    LCY.LD.BAL.OPN   = 0
    LCY.LD.BAL.ONLN  = 0
    LCY.LD.BAL.DIF   = 0
    FCY.LD.BAL.OPN   = 0
    FCY.LD.BAL.ONLN  = 0
    FCY.LD.BAL.DIF   = 0
    FCY.BAL          = 0
    FLAG             = ''
    TYPE             = ''
    R.SCB            = ''
    CATEG            = ''
    CURR             = ''
    COM.CODE         = ''

    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ( CATEGORY GE 21001 AND CATEGORY LE 21032 ) AND FIN.MAT.DATE GT " : TODAY : " BY CO.CODE BY CATEGORY "
*    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH ( CATEGORY GE 21001 AND CATEGORY LE 21032 ) AND FIN.MAT.DATE GT " : TODAY : " AND CO.CODE EQ EG0010001 "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED.LDD, ASD)


    FOR DDD = 1 TO SELECTED.LDD

        CALL F.READ(FN.LD,KEY.LIST<DDD>,R.LD,F.LD,LD.ERR)
        CALL F.READ(FN.LD,KEY.LIST<DDD+1>,R.LD.1,F.LD,LD.ERR)

        LD.BAL      = R.LD<LD.AMOUNT>

        COM.CODE    = R.LD<LD.CO.CODE>
        COM.CODE.1  = R.LD.1<LD.CO.CODE>

        CATEG       = R.LD<LD.CATEGORY>
        CATEG.1     = R.LD.1<LD.CATEGORY>

        CURR        = R.LD<LD.CURRENCY>
        CUR.ID      = R.LD<LD.CURRENCY>
        MAT.DATE    = R.LD<LD.FIN.MAT.DATE>
        VAL.DATE    = R.LD<LD.VALUE.DATE>

        IF CURR EQ 'EGP' THEN
            CURR = 'CCY'
        END ELSE
            CURR =  'FCY'
        END

        IF ( CATEG GE 21001 AND CATEG LE 21010 ) THEN
            TYPE = 'LD':CATEG[4,2]
        END ELSE
            IF ( CATEG GE 21019 AND CATEG LE 21032 ) THEN
                TYPE = 'CD'
            END
        END

        IF ( CATEG.1 GE 21001 AND CATEG.1 LE 21010 ) THEN
            TYPE.1 = 'LD':CATEG.1[4,2]
        END ELSE
            IF ( CATEG.1 GE 21019 AND CATEG.1 LE 21032 ) THEN
                TYPE.1 = 'CD'
            END
        END

        REPT.BK.LD   = COM.CODE :TYPE
        REPT.BK.LD.1 = COM.CODE.1 :TYPE.1

        IF REPT.BK.LD EQ REPT.BK.LD.1 THEN

            IF CURR EQ 'FCY' THEN
*               CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)
*               RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.ONLN       = LD.BAL * RATE
                FCY.LD.BAL.ONLN   += FCY.BAL.ONLN
            END ELSE
                LCY.LD.BAL.ONLN   += LD.BAL
            END

        END ELSE

            IF CURR EQ 'FCY' THEN

*CALL F.READ(FN.CUR,CUR.ID,R.CUR,F.CUR,ERR)
*RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

                CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
                LOCATE CUR.ID IN CURR.BASE<1,1> SETTING POS THEN
                    RATE = R.BASE<RE.BCP.RATE,POS>
                END

                FCY.BAL.ONLN      = LD.BAL * RATE
                FCY.LD.BAL.ONLN  += FCY.BAL.ONLN
            END ELSE
                LCY.LD.BAL.ONLN  += LD.BAL
            END

            D.T      = TIMEDATE()
            DAT.TIM  = '20':D.T
            SCB.ID   = COM.CODE :'-':TYPE:'-':TODAY :'-' :TIME.TYPE

            IF ( CATEG GE 21001 AND CATEG LE 21010 ) THEN
                FALG = '3'
                TYPE = 'LD':CATEG[4,2]
                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG,LD.DESC)
                DESC = LD.DESC
            END
            IF ( CATEG GE 21019 AND CATEG LE 21032 ) THEN
                FALG = '4'
                TYPE = 'CD'
                DESC = '������ �������'
            END

            CALL F.READ(FN.SCB,SCB.ID,R.SCB,F.SCB,SCB.ERR)

            R.SCB<SCB.FLAG>             = FLAG
            R.SCB<SCB.COMPANY>          = COM.CODE
            R.SCB<SCB.DATE>             = TODAY
            R.SCB<SCB.TIME>             = D.T
            R.SCB<SCB.TYPE>             = TYPE
            R.SCB<SCB.CATEG>            = CATEG
            R.SCB<SCB.DESC>             = DESC

            IF COM.CODE EQ 'EG0010070' THEN

*DEBUG
            END

            R.SCB<SCB.LCY.AMT.ONLINE>   = LCY.LD.BAL.ONLN
            R.SCB<SCB.FCY.AMT.ONLINE>   = FCY.LD.BAL.ONLN
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****            WRITE  R.SCB TO F.SCB , SCB.ID ON ERROR
****                PRINT "CAN NOT WRITE RECORD":SCB.ID:"TO" :FN.SCB
****            END
            CALL F.WRITE(FN.SCB,SCB.ID,R.SCB)
****END OF UPDATE 24/3/2016*****************************
            CALL JOURNAL.UPDATE(SCB.ID)

            LCY.LD.BAL.ONLN  = 0
            FCY.LD.BAL.ONLN  = 0

        END

    NEXT DDD

    RETURN

*------------------------                  ------------------------
*------------------------     Diff         ------------------------
*------------------------                  ------------------------

PROCESS.DIFF:

    ETEXT            = ''
    LCY.AC.BAL.OPN   = 0
    LCY.AC.BAL.ONLN  = 0
    LCY.AC.BAL.DIF   = 0
    FCY.AC.BAL.OPN   = 0
    FCY.AC.BAL.ONLN  = 0
    FCY.AC.BAL.DIF   = 0
    LCY.LD.BAL.NEW   = 0
    FCY.LD.BAL.NEW   = 0
    FLAG             = ''
    TYPE             = ''
    R.SCB            = ''
    CATEG            = ''
    CURR             = ''
    COM.CODE         = ''

    FN.SCB1 = 'F.SCB.AC.LD.BAL' ; F.SCB1 = '' ; R.SCB1 = ''
    CALL OPF (FN.SCB1,F.SCB1)

    T.SEL.DIF  = "SELECT F.SCB.AC.LD.BAL WITH DATE EQ " : TODAY : " BY @ID "

    CALL EB.READLIST(T.SEL.DIF, KEY.LIST.D, "", SELECTED.DIF, ASD)

    FOR D = 1 TO SELECTED.DIF
        ETEXT = ''
        CALL F.READ(FN.SCB1,KEY.LIST.D<D>,R.SCB1,F.SCB1,SCB.ERR11)


        LCY.BAL.ONLN1         = R.SCB1<SCB.LCY.AMT.ONLINE>
        FCY.BAL.ONLN1         = R.SCB1<SCB.FCY.AMT.ONLINE>
        LCY.BAL.OPN1          = R.SCB1<SCB.LCY.AMT.OPEN>
        FCY.BAL.OPN1          = R.SCB1<SCB.FCY.AMT.OPEN>

*        LCY.BAL.DIF          = LCY.BAL.ONLN1 - LCY.BAL.OPN1
*       FCY.BAL.DIF          = FCY.BAL.ONLN1 - FCY.BAL.OPN1

        LCY.BAL.DIF          = SSUB(LCY.BAL.ONLN1,LCY.BAL.OPN1)
        FCY.BAL.DIF          = SSUB(FCY.BAL.ONLN1,FCY.BAL.OPN1)

*PRINT "ONLINE= " :LCY.BAL.ONLN1 : "<>" : LCY.BAL.OPN1

*  R.SCB1<SCB.LCY.AMT.DIF> = LCY.AC.BAL.ONLN1 - LCY.AC.BAL.OPN1
*  R.SCB1<SCB.FCY.AMT.DIF> = FCY.AC.BAL.ONLN1 - FCY.AC.BAL.OPN1

        IF R.SCB1<SCB.COMPANY>  EQ 'EG0010060' THEN
*DEBUG
        END
*PRINT "BFR= " :LCY.BAL.ONLN1 : "-" : LCY.BAL.OPN1 :'-' : R.SCB1<SCB.LCY.AMT.DIF>

        R.SCB1<SCB.LCY.AMT.DIF>  = LCY.BAL.DIF
        R.SCB1<SCB.FCY.AMT.DIF>  = FCY.BAL.DIF

*       R.SCB1<SCB.LCY.AMT.DIF>  = R.SCB1<SCB.LCY.AMT.ONLINE> - R.SCB1<SCB.LCY.AMT.OPEN>
*       R.SCB1<SCB.FCY.AMT.DIF>  = R.SCB1<SCB.FCY.AMT.ONLINE> - R.SCB1<SCB.FCY.AMT.OPEN>

        SCB.ID.DIF = KEY.LIST.D<D>
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****        WRITE  R.SCB1 TO F.SCB1 , SCB.ID.DIF ON ERROR
****            PRINT "CAN NOT WRITE RECORD":SCB.ID.DIF:"TO" :FN.SCB1
****        END
        CALL F.WRITE(FN.SCB1,SCB.ID.DIF,R.SCB1)
        CALL JOURNAL.UPDATE(SCB.ID.DIF)
****END OF UPDATE 24/3/2016*****************************
*PRINT "AFT= " :LCY.BAL.ONLN1 : "-" : LCY.BAL.OPN1 :'-' : R.SCB1<SCB.LCY.AMT.DIF>
    NEXT D

    RETURN

**------------------------------------------------------------------

END
