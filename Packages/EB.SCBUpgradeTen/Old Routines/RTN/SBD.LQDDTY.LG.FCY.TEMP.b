* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>288</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.LQDDTY.LG.FCY.TEMP
**    SUBROUTINE SBD.CONV.LQDDTY.FCY.TEMP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LQDDTY.DAILY.FILE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
    COMP = ID.COMPANY
*-----------------------------------------------------------------------
    FN.LG =  'FBNK.LD.LOANS.AND.DEPOSITS'; F.LG = ''; R.LG = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM' ; F.CCY = '' ; R.CCY = ''
    FN.DCY = 'F.SBD.CURRENCY' ; F.DCY = '' ; R.DCY = ''

    FN.LQD = 'F.SCB.LQDDTY.DAILY.FILE' ; F.LQD = ''

    CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.DCY,F.DCY)
    CALL OPF(FN.LG,F.LG)
    CALL OPF(FN.LQD,F.LQD)

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,DAT.ID,LWD)
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

    WS.BR.LG.AMT    = 0
    WS.BR.LG.FCY.AMT    = 0
    WS.BR.MRG.AMT   = 0
    WS.BR.MRG.FCY.AMT   = 0
    WS.BR.LG.NET    = 0
    WS.LG.COMPAY    = ''
    LG.COMPANY      = ''
    WS.LG.COMPANY   = ''
********* LOCAL LINES *************
*-UPDATE BY MOHAMED SABRY 2010/04/07-- LG NOT COVER----------------------
    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND CURRENCY NE 'EGP' AND STATUS NE 'LIQ' AND LG.KIND NE 'BB' AND MARGIN.AMT NE '' AND LIMIT.REFERENCE LIKE 2510.... BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.LG,KEY.LIST<II>,R.LG,F.LG,EER)
            LG.COMPANY      = R.LG<LD.CO.CODE>

            IF WS.LG.COMPANY EQ '' THEN
                WS.LG.COMPANY = LG.COMPANY
            END

            IF WS.LG.COMPANY NE LG.COMPANY THEN
                WS.BR.LG.NET = (( WS.BR.LG.AMT -  WS.BR.MRG.AMT ) / 2 )
                LQD.ID = "FCY.B.":LWD:".":WS.LG.COMPANY
                CALL F.READ(FN.LQD,LQD.ID,R.LQD,F.LQD,E)
                IF  NOT (E)  THEN
                    R.LQD<LQDF.COLUMN.16> = WS.BR.LG.NET
                    R.LQD<LQDF.TOTAL.ROW.B> += WS.BR.LG.NET
                    CALL F.WRITE(FN.LQD,LQD.ID,R.LQD)
                    CALL  JOURNAL.UPDATE(LQD.ID)
                    WS.BR.LG.AMT        = 0
                    WS.BR.LG.FCY.AMT    = 0
                    WS.BR.MRG.AMT       = 0
                    WS.BR.MRG.FCY.AMT   = 0
                    WS.BR.LG.NET        = 0
                END
            END
            WS.CY.LG        = R.LG<LD.CURRENCY>

***** UPDATED BY MOHAMED SABRY 2013/10/28 *****************************
*            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*            LOCATE WS.CY.LG IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
*            RATE = R.CCY<RE.BCP.RATE,POS>

            CALL F.READ(FN.DCY,WS.CY.LG,R.DCY,F.DCY,E1)
            RATE = R.DCY<SBD.CURR.MID.RATE>
            IF WS.CY.LG EQ 'JPY' THEN
                RATE = RATE / 100
            END
***** END UPDATE BY MOHAMED SABRY 2013/10/28 *****************************

            WS.BR.LG.FCY.AMT   = R.LG<LD.AMOUNT> * RATE
            WS.BR.LG.AMT       += WS.BR.LG.FCY.AMT
            WS.BR.MRG.FCY.AMT   = R.LG<LD.LOCAL.REF><1,LDLR.MARGIN.AMT> * RATE
            WS.BR.MRG.AMT      += WS.BR.MRG.FCY.AMT
            WS.LG.COMPANY       = LG.COMPANY

        NEXT II
        WS.BR.LG.NET = (( WS.BR.LG.AMT -  WS.BR.MRG.AMT ) / 2 )
        LQD.ID = "FCY.B.":LWD:".":WS.LG.COMPANY
        CALL F.READ(FN.LQD,LQD.ID,R.LQD,F.LQD,E)
        IF  NOT (E)  THEN
            R.LQD<LQDF.COLUMN.16> = WS.BR.LG.NET
            R.LQD<LQDF.TOTAL.ROW.B> += WS.BR.LG.NET
            CALL F.WRITE(FN.LQD,LQD.ID,R.LQD)
            CALL  JOURNAL.UPDATE(LQD.ID)
            WS.BR.LG.AMT        = 0
            WS.BR.LG.FCY.AMT    = 0
            WS.BR.MRG.AMT       = 0
            WS.BR.MRG.FCY.AMT   = 0
            WS.BR.LG.NET        = 0
        END
    END
*------------------------------------------------------------------------
END
**************************************************
