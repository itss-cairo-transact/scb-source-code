* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>127</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.C.CRT.MAST.03

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.CONTROL

*-----------------------------------------------
    FN.LD = "F.CBE.MAST.AC.LD"
    F.LD  = ""

    FN.CBE = "F.CBE.STATIC.MAST"
    F.CBE  = ""

    FN.CONT = "F.CBE.STATIC.CONTROL"
    F.CONT  = ""

    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ""
*--------------------------------
    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.CONT,F.CONT)
    CALL OPF (FN.CUS,F.CUS)
*----------------------------------
    CLEARFILE F.CBE
    CLEARFILE F.CONT
*---------------------------------
    WS.LD.ID = ""
    WS.CBE.ID = ""
    WS.CY.ID = ""
    MSG.LD = ""
    MSG.CBE = ""
    MSG.CY = ""
    WS.INDSTRY = ""
    WS.SECTR = ""
    WS.N.KEY = ""
    WS.CBE.CODE = ""
    WS.OLD.CODE = ""
    WS.CUST.NO = ""
    WS.COUNT = ""
    WS.STATIC.KEY = ""
    WS.BR = ""
    R.CY = ""
    WS.CY = ""
    WS.FIL.AC.LD.AMT = "0"
    WS.CUS.CODE = ""
    WS.RATE = "0"
    WS.MLT.DIVD = "0"
    WS.COMN.AMT = "0"
    WS.COMN.COUNT = "0"
    WS.CATEG = ""
    WS.MOHSEN  = 0
    A.ARY = ""
    CYPOS = ""
    WS.RATE.ARY = ""
    WS.MLT.DIVD.ARY = ""


    DATY = TODAY
    WS.CONT.KEY = "SBM.C.CRT.MAST.03"
    MSG.CONT = ""
    CALL F.READ(FN.CONT,WS.CONT.KEY,R.CONT,F.CONT,MSG.CONT)

    IF MSG.CONT NE "" THEN
        GOSUB A.600.CRT.CONTROL
        WS.CC = 0
        GOSUB A.050.PROC
    END
    RETURN
***************    PROCEDURE AREA     **************************
A.050.PROC:
    SEL.CMD = "SELECT ":FN.LD
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

    LOOP
        REMOVE WS.LD.ID FROM SEL.LIST SETTING POS
    WHILE WS.LD.ID:POS

        CALL F.READ(FN.LD,WS.LD.ID,R.AC,F.LD,MSG.LD)
        IF R.AC<C.CBEM.CUSTOMER.CODE> = "" AND R.AC<C.CBEM.CATEG> NE 16170 THEN
            GOTO AAAA
        END

        WS.FIL.AC.LD.AMT             = 0
        WS.CATEG                     = R.AC<C.CBEM.CATEG>
        WS.FIL.AC.LD.AMT             = R.AC<C.CBEM.IN.LCY>
        WS.BR                        = R.AC<C.CBEM.BR>
        WS.CUS.CODE                  = R.AC<C.CBEM.CUSTOMER.CODE>
        WS.CY                        = R.AC<C.CBEM.CY>
        WS.STATIC.KEY                = WS.CUS.CODE:"*":WS.BR
        WS.CBEM.ID                   = WS.CUS.CODE:"*":WS.BR
        MSG.CBE                      = ""

        CALL F.READ(FN.CBE,WS.STATIC.KEY,R.CBE,F.CBE,MSG.CBE)

        IF MSG.CBE THEN
            GOSUB INIT.REC.055
        END
        CALL F.READ(FN.CBE,WS.STATIC.KEY,R.CBE,F.CBE,MSG.CBE)

        IF MSG.CBE EQ "" THEN
            GOSUB A.100.CHK
        END
AAAA:
    REPEAT
    RETURN
*--------------------------------------------------------

A.100.CHK:


    GOSUB A.200.UPDAT.REC.LE

    RETURN
*----------------------------------------------------

*----------------------------------------------------
A.200.UPDAT.REC.LE:
*** UPDATED BY MOHAMED SABRY 2014/09/14
    IF WS.CATEG GE 21001  AND WS.CATEG LE 21013 THEN
        GOSUB A.205.LE
    END

    IF WS.CATEG GE 21017 AND WS.CATEG LE 21029 THEN
        GOSUB A.215.LE
    END
    IF WS.CATEG EQ 21032 THEN
        GOSUB A.215.LE
    END

    IF WS.CATEG GE 21050 AND WS.CATEG LE 21066 THEN
        GOSUB A.220.LE
    END

    RETURN
*--------------------------------------------------
A.205.LE:
    IF WS.CY NE "EGP" THEN
        GOSUB A.305.EQ
        RETURN
    END
    IF WS.CATEG GE 21001 AND WS.CATEG LE 21007 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.LE.1Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.LE.1Y> = WS.COMN.AMT
    END

    IF WS.CATEG EQ 21008 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.LE.2Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.LE.2Y> = WS.COMN.AMT
    END

    IF WS.CATEG EQ 21009 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.LE.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.LE.3Y> = WS.COMN.AMT
    END
*** UPDATED BY MOHAMED SABRY 2014/09/14
    IF WS.CATEG EQ 21013 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.LE.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.LE.3Y> = WS.COMN.AMT
    END
*** END OF UPDATE
    IF WS.CATEG EQ 21010 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.LE.MOR3>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.LE.MOR3> = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.DEPOST.AC.LE.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.DEPOST.AC.LE.NO.OF.AC> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
*---------------------------------------------------------------------------
A.215.LE:
    IF WS.CY NE "EGP" THEN
        GOSUB A.315.EQ
        RETURN
    END
    IF WS.CATEG GE 21017 AND WS.CATEG LE 21022 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.LE.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.LE.3Y> = WS.COMN.AMT
    END
    IF WS.CATEG GE 21023 AND WS.CATEG LE 21024 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.LE.5Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.LE.5Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21025 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.LE.GOLD>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.LE.GOLD> = WS.COMN.AMT
    END
    IF WS.CATEG GE 21026 AND WS.CATEG LE 21028 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.LE.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.LE.3Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21029 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.LE.5Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.LE.5Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21032 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.LE.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.LE.3Y> = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.CER.AC.LE.NO.OF.CER>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.CER.AC.LE.NO.OF.CER> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
*---------------------------------------------------------------------------

A.220.LE:
    IF WS.CY NE "EGP" THEN
        GOSUB A.320.EQ
        RETURN
    END
*MSABRY 2014/1/30
    IF WS.CATEG GE 21050 AND WS.CATEG LE 21066 THEN
        WS.COMN.AMT = R.CBE<C.CBE.LOANS.LE.L>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.LOANS.LE.L> = WS.COMN.AMT
        WS.COMN.COUNT = R.CBE<C.CBE.LOANS.LE.L.NO.OF.AC>
        WS.COMN.COUNT = WS.COMN.COUNT +  1
        R.CBE<C.CBE.LOANS.LE.L.NO.OF.AC> = WS.COMN.COUNT
    END

    GOSUB A.500.UPDAT
    RETURN
*---------------------------------------------------------------------------

********************************EQV
A.305.EQ:
    IF WS.CATEG GE 21001 AND WS.CATEG LE 21007 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.EQ.1Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.EQ.1Y> = WS.COMN.AMT
    END

    IF WS.CATEG EQ 21008 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.EQ.2Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.EQ.2Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21009 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.EQ.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.EQ.3Y> = WS.COMN.AMT
    END
*** UPDATED BY MOHAMED SABRY 2014/09/14
    IF WS.CATEG EQ 21013 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.EQ.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.EQ.3Y> = WS.COMN.AMT
    END
*** END OF UPDATED
    IF WS.CATEG EQ 21010 THEN
        WS.COMN.AMT = R.CBE<C.CBE.DEPOST.AC.EQ.MOR3>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.DEPOST.AC.EQ.MOR3> = WS.COMN.AMT
    END
    WS.COMN.COUNT = R.CBE<C.CBE.DEPOST.AC.EQ.NO.OF.AC>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.DEPOST.AC.EQ.NO.OF.AC> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
*---------------------------------------------------------------------------
A.315.EQ:
    IF WS.CATEG GE 21017 AND WS.CATEG LE 21022 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.EQ.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.EQ.3Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21032 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.EQ.3Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.EQ.3Y> = WS.COMN.AMT
    END
    IF WS.CATEG GE 21023 AND WS.CATEG LE 21024 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.EQ.5Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.EQ.5Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21029 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.EQ.5Y>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.EQ.5Y> = WS.COMN.AMT
    END
    IF WS.CATEG EQ 21025 THEN
        WS.COMN.AMT =  R.CBE<C.CBE.CER.AC.EQ.GOLD>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.CER.AC.EQ.GOLD> = WS.COMN.AMT
    END

    WS.COMN.COUNT = R.CBE<C.CBE.CER.AC.EQ.NO.OF.CER>
    WS.COMN.COUNT = WS.COMN.COUNT + 1
    R.CBE<C.CBE.CER.AC.EQ.NO.OF.CER> = WS.COMN.COUNT

    GOSUB A.500.UPDAT
    RETURN
*---------------------------------------------------------------------------

A.320.EQ:
*MSABRY 2014/1/30
    IF WS.CATEG GE 21050 AND WS.CATEG LE 21066 THEN
        WS.COMN.AMT = R.CBE<C.CBE.LOANS.EQ.L>
        WS.COMN.AMT = WS.COMN.AMT + WS.FIL.AC.LD.AMT
        R.CBE<C.CBE.LOANS.EQ.L> = WS.COMN.AMT
        WS.COMN.COUNT = R.CBE<C.CBE.LOANS.EQ.L.NO.OF.AC>
        WS.COMN.COUNT = WS.COMN.COUNT +  1
        R.CBE<C.CBE.LOANS.EQ.L.NO.OF.AC> = WS.COMN.COUNT
    END

    GOSUB A.500.UPDAT
    RETURN
*---------------------------------------------------------------------------
A.500.UPDAT:
************
    CALL F.WRITE(FN.CBE,WS.STATIC.KEY,R.CBE)
    CALL  JOURNAL.UPDATE(WS.STATIC.KEY)
    RETURN
*--------------------------------------------------------
*-----------------����� ����� ����� ���� ����� �������
A.600.CRT.CONTROL:
    DATY = TODAY
    WS.CONT.KEY = "SBM.C.CRT.MAST.03"
    R.CONT<C.CONTROL.DATE> = DATY
    CALL F.WRITE(FN.CONT,WS.CONT.KEY,R.CONT)
    CALL  JOURNAL.UPDATE(WS.CONT.KEY)
    RETURN
*---------------------------------------------------------
INIT.REC.055:

    R.STAT                              = ''

    CALL F.READ(FN.CUS,WS.CUS.CODE,R.CUS,F.CUS,MSG.CUS)

    WS.OLD.CODE                         = R.CUS<EB.CUS.INDUSTRY>
    WS.CBME.ID                          = WS.CUS.CODE:"*":WS.BR
    R.STAT<C.CBE.CUSTOMER.CODE>         = WS.CUS.CODE
    R.STAT<C.CBE.BR>                    = WS.BR
    R.STAT<C.CBE.INDUSTRY>              = R.CUS<EB.CUS.INDUSTRY>
    R.STAT<C.CBE.SECTOR>                = R.CUS<EB.CUS.SECTOR>
    R.STAT<C.CBE.LEGAL.FORM>            = R.CUS<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
    R.STAT<C.CBE.NEW.SECTOR>            = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
    R.STAT<C.CBE.NEW.INDUSTRY>          = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.INDUSTRY>
    R.STAT<C.CBE.CUR.AC.LE>             = "0"
    R.STAT<C.CBE.CUR.AC.LE.DR>          = "0"
    R.STAT<C.CBE.CUR.AC.LE.NO.OF.AC>    = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.1Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.2Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.3Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.MOR3>     = "0"
    R.STAT<C.CBE.DEPOST.AC.LE.NO.OF.AC> = "0"
    R.STAT<C.CBE.CER.AC.LE.3Y>          = "0"
    R.STAT<C.CBE.CER.AC.LE.5Y>          = "0"
    R.STAT<C.CBE.CER.AC.LE.GOLD>        = "0"
    R.STAT<C.CBE.CER.AC.LE.NO.OF.CER>   = "0"
    R.STAT<C.CBE.SAV.AC.LE>             = "0"
    R.STAT<C.CBE.SAV.AC.LE.DR>          =  0
    R.STAT<C.CBE.SAV.AC.LE.NO.OF.AC>    = "0"
    R.STAT<C.CBE.BLOCK.AC.LE>           = "0"
    R.STAT<C.CBE.BLOCK.AC.LE.NO.OF.AC>  = "0"
    R.STAT<C.CBE.MARG.LC.LE>            = "0"
    R.STAT<C.CBE.MARG.LC.LE.NO.OF.AC>   = "0"
    R.STAT<C.CBE.MARG.LG.LE>            = "0"
    R.STAT<C.CBE.MARG.LG.LE.NO.OF.AC>   = "0"
    R.STAT<C.CBE.FACLTY.LE>             = "0"
    R.STAT<C.CBE.FACLTY.LE.CR>          = "0"
    R.STAT<C.CBE.FACLTY.LE.NO.OF.AC>    = "0"
    R.STAT<C.CBE.LOANS.LE.S>            = "0"
    R.STAT<C.CBE.LOANS.LE.S.CR>         = "0"
    R.STAT<C.CBE.LOANS.LE.S.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.LE.M>            = "0"
    R.STAT<C.CBE.LOANS.LE.M.CR>         = "0"
    R.STAT<C.CBE.LOANS.LE.M.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.LE.L>            = "0"
    R.STAT<C.CBE.LOANS.LE.L.CR>         = "0"
    R.STAT<C.CBE.LOANS.LE.L.NO.OF.AC>   = "0"
    R.STAT<C.CBE.COMRCL.PAPER.LE>       = "0"
    R.STAT<C.CBE.COMRCL.PAPER.LE.NO>    = "0"
    R.STAT<C.CBE.CUR.AC.EQ>             = "0"
    R.STAT<C.CBE.CUR.AC.EQ.DR>          = "0"
    R.STAT<C.CBE.CUR.AC.EQ.NO.OF.AC>    = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.1Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.2Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.3Y>       = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.MOR3>     = "0"
    R.STAT<C.CBE.DEPOST.AC.EQ.NO.OF.AC> = "0"
    R.STAT<C.CBE.CER.AC.EQ.3Y>          = "0"
    R.STAT<C.CBE.CER.AC.EQ.5Y>          = "0"
    R.STAT<C.CBE.CER.AC.EQ.GOLD>        = "0"
    R.STAT<C.CBE.CER.AC.EQ.NO.OF.CER>   = "0"
    R.STAT<C.CBE.SAV.AC.EQ>             = "0"
    R.STAT<C.CBE.SAV.AC.EQ.DR>          =  0
    R.STAT<C.CBE.SAV.AC.EQ.NO.OF.AC>    = "0"
    R.STAT<C.CBE.BLOCK.AC.EQ>           = "0"
    R.STAT<C.CBE.BLOCK.AC.EQ.NO.OF.AC>  = "0"
    R.STAT<C.CBE.MARG.LC.EQ>            = "0"
    R.STAT<C.CBE.MARG.LC.EQ.NO.OF.AC>   = "0"
    R.STAT<C.CBE.MARG.LG.EQ>            = "0"
    R.STAT<C.CBE.MARG.LG.EQ.NO.OF.AC>   = "0"
    R.STAT<C.CBE.FACLTY.EQ>             = "0"
    R.STAT<C.CBE.FACLTY.EQ.CR>          = "0"
    R.STAT<C.CBE.FACLTY.EQ.NO.OF.AC>    = "0"
    R.STAT<C.CBE.LOANS.EQ.S>            = "0"
    R.STAT<C.CBE.LOANS.EQ.S.CR>         = "0"
    R.STAT<C.CBE.LOANS.EQ.S.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.EQ.M>            = "0"
    R.STAT<C.CBE.LOANS.EQ.M.CR>         = "0"
    R.STAT<C.CBE.LOANS.EQ.M.NO.OF.AC>   = "0"
    R.STAT<C.CBE.LOANS.EQ.L>            = "0"
    R.STAT<C.CBE.LOANS.EQ.L.CR>         = "0"
    R.STAT<C.CBE.LOANS.EQ.L.NO.OF.AC>   = "0"
    R.STAT<C.CBE.COMRCL.PAPER.EQ>       = "0"
    R.STAT<C.CBE.COMRCL.PAPER.EQ.NO>    = "0"
*=======================================================
    IF R.AC<C.CBEM.CATEG> EQ 16170  THEN
        WS.OLD.CODE                         = 1100
        WS.CBME.ID                          = WS.CUS.CODE:"*":WS.BR
        R.STAT<C.CBE.CUSTOMER.CODE>         = WS.CUS.CODE
        R.STAT<C.CBE.BR>                    = WS.BR
        R.STAT<C.CBE.INDUSTRY>              = R.CUS<EB.CUS.INDUSTRY>
        R.STAT<C.CBE.SECTOR>                = R.CUS<EB.CUS.SECTOR>
        R.STAT<C.CBE.LEGAL.FORM>            = R.CUS<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
        R.STAT<C.CBE.NEW.SECTOR>            = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
        R.STAT<C.CBE.NEW.INDUSTRY>          = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.INDUSTRY>
    END
*=======================================================
    CALL F.WRITE(FN.CBE,WS.CBEM.ID,R.STAT)
    CALL  JOURNAL.UPDATE(WS.CBEM.ID)
    RETURN
*---------------------------------------------------------
END
