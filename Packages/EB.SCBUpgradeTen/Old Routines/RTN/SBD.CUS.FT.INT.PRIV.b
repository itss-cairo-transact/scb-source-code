* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* CREATED BY REHAM.YOUSSEF 2020/08/
*-----------------------------------------------------------------------------
*    PROGRAM SBD.CUS.FT.INT.PRIV
    SUBROUTINE SBD.CUS.FT.INT.PRIV
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.INT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB HIS
    RETURN
**************************************
INITIATE:
    EXECUTE "CLEAR-FILE F.SCB.FT.INT"

    FN.INT   = 'F.SCB.FT.INT'    ; F.INT   = '' ; R.INT  = ''
    CALL OPF(FN.INT,F.INT)

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.FT   = 'FBNK.FUNDS.TRANSFER$HIS'    ; F.FT  = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    KEY.LIST= ""   ; SELECTED  = "" ; ER.MSG = ""
    KEY.LIST2= ""  ; SELECTED2 = "" ; ER.MSG2 = ""
    DEB.AMT = ""    ; SERIAL = 0 ; SERIAL2 = 0
    RETURN
**************************HIS ONLY *******************************
HIS:


    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    PRINT  "ST.DATE=":ST.DATE

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    PRINT "EN.DATE=":EN.DATE

    T.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH INPUTTER LIKE ...EIB... AND (DEBIT.VALUE.DATE GE ":ST.DATE :" AND DEBIT.VALUE.DATE LE " :EN.DATE :" ) AND DEBIT.CUSTOMER NE '' AND RECORD.STATUS NE 'REVE' BY DEBIT.CUSTOMER"
*    T.SEL = "GET-LIST FT.INT.ALL"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT "SEL=":SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT1,F.FT,ETEXT)
            DR.CUS = R.FT1<FT.DEBIT.CUSTOMER>
            DR.AMT = R.FT1<FT.LOC.AMT.DEBITED>
            CALL F.READ(FN.CUS,DR.CUS,R.CUS,F.CUS,ETEXT)
            LOCAL.REF = R.CUS<EB.CUS.LOCAL.REF>
            NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
            IF NEW.SEC EQ '4650' THEN
                R.INT<INT.TOTAL.PRIVATE> += DR.AMT
                CALL F.READ(FN.INT,DR.CUS,R.INT,F.INT,ETEXT)
                IF ETEXT THEN
                    SERIAL1 = 1
                END ELSE
                    SERIAL1 = R.INT<INT.FT.NUMBER> + 1
                END
                R.INT<INT.FT.NUMBER>  = SERIAL1
                R.INT<INT.TOTAL.PRIVATE> += DR.AMT
                CALL F.WRITE(FN.INT,DR.CUS,R.INT)
                CALL JOURNAL.UPDATE(DR.CUS)
                R.CUS = ''
                R.INT<INT.TOTAL.PRIVATE> = ''
                R.INT<INT.FT.NUMBER> = ''
            END ELSE

                R.INT<INT.TOTAL.COR> += DR.AMT

                CALL F.READ(FN.INT,DR.CUS,R.INT,F.INT,ETEXT)
                IF ETEXT THEN
                    SERIAL2 = 1
                END ELSE
                    SERIAL2 = R.INT<INT.FT.NUMBER> + 1
                END
                R.INT<INT.FT.NUMBER>  = SERIAL2
                R.INT<INT.TOTAL.COR> += DR.AMT
                CALL F.WRITE(FN.INT,DR.CUS,R.INT)
                CALL JOURNAL.UPDATE(DR.CUS)
                R.CUS = ''
                R.INT<INT.TOTAL.COR> = ''
                R.INT<INT.FT.NUMBER> = ''
            END
        NEXT I
    END
    RETURN
**********************************************************************
END
