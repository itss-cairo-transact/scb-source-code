* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*---------------------------------------NI7OOOOOOOOOOOOOOOOOO--------------------------------------
*-----------------------------------------------------------------------------
* <Rating>3227</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.REPORT.NAU.BRANCHS

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FOREX
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 78 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 80 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL.RIGHT
*Line [ 82 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE
*Line [ 84 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DOCUMENT.PROCURE


    COMP = C$ID.COMPANY
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
***TEXT = "REPORT SUCCESFULLY COMPLETED" ; CALL REM
    RETURN
*========================================================================
INITIATE:
    REPORT.ID='SBD.REPORT.NAU.BRANCHS'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DEPT   = 'F.DEPT.ACCT.OFFICER' ; F.DEPT = ''
    FN.AC     = 'FBNK.ACCOUNT$NAU' ; F.AC = ''
    FN.CU     = 'FBNK.CUSTOMER$NAU' ; F.CU = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE$NAU' ; F.IM = ''
    FN.FT     = 'FBNK.FUNDS.TRANSFER$NAU' ; F.FT = ''
    FN.TT     = 'FBNK.TELLER$NAU' ; F.TT = ''
    FN.IN     = 'F.INF.MULTI.TXN$NAU' ; F.IN = ''
    FN.CLOSE  = 'FBNK.ACCOUNT.CLOSURE$NAU' ; F.CLOSE = ''
    FN.MM     = 'FBNK.MM.MONEY.MARKET$NAU' ; F.MM = ''
    FN.FOR    = 'FBNK.FOREX$NAU' ; F.FOR = ''
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD = ''
    FN.LETTER = 'FBNK.LETTER.OF.CREDIT$NAU' ; F.LETTER = ''
    FN.DRAW   = 'FBNK.DRAWINGS$NAU'         ; F.DRAW = ''
    FN.BT     = 'F.SCB.BT.BATCH$NAU'        ; F.BT = ''
    FN.BR     = 'FBNK.BILL.REGISTER'        ; F.BR = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE$NAU'   ; F.IM = ''
    FN.CM     = 'F.COMPANY'                 ; F.CM = ''
    FN.USER   = 'F.USER'                    ; F.USER = ''
    FN.CBE    = 'F.SCB.MONTHLY.PAY.CBE'     ; F.CBE = ''
    FN.CARD   = 'FBNK.CARD.ISSUE$NAU'           ; F.CARD = ''
    FN.CBE    = 'F.SCB.MONTHLY.PAY.CBE$NAU'      ; F.CBE = ''
    FN.DOC    = 'F.SCB.DOCUMENT.PROCURE$NAU'     ; F.DOC = ''
    FN.LIM    = 'FBNK.LIMIT'     ; F.LIM = ''
    FN.COL    = 'FBNK.COLLATERAL'     ; F.COL = ''
    FN.COR    = 'FBNK.COLLATERAL.RIGHT'     ; F.COR = ''

    CALL OPF(FN.DEPT,F.DEPT)
    CALL OPF(FN.LETTER,F.LETTER)
    CALL OPF(FN.DRAW,F.DRAW)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.CLOSE,F.CLOSE)
    CALL OPF(FN.MM,F.MM)
    CALL OPF(FN.FOR,F.FOR)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.BT,F.BT)
    CALL OPF(FN.CM,F.CM)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.USER,F.USER)
    CALL OPF(FN.CBE,F.CBE)
    CALL OPF(FN.CARD,F.CARD)
    CALL OPF(FN.CM,F.CM)
    CALL OPF(FN.DOC,F.DOC)
    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.COL,F.COL)
    CALL OPF(FN.COR,F.COR)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""  ; R.DEP = ''
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG=""
    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
    TD = TODAY
*------------------------------------------------------------------------
****    T.SEL = "SELECT F.DEPT.ACCT.OFFICER WITH @ID GE 1 AND @ID LE 100  WITHOUT @ID IN (8 16 17 18 19 24 25 26 27 28 29 33 34 51 52 53 54 55 56 57 58 59 77 91 100) BY @ID"
    T.SEL = "SELECT F.COMPANY BY @ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
***TEXT = "SEL : " : SELECTED ; CALL REM
    FOR I = 1 TO SELECTED
** XX = KEY.LIST<I>
        ZZ = KEY.LIST<I>
        ZZ1  = ZZ[2]
        ZZ2  = TRIM(ZZ1, "0" , "L")
        XX   = ZZ2

        CALL F.READ(FN.CM,KEY.LIST<I>,R.CM,F.CM,E2)
        NAMEV  = R.CM<EB.COM.COMPANY.NAME>
***TEXT = NAMEV ; CALL REM
        NAMEV1 = R.CM<EB.COM.COMPANY.NAME><1,2>
        YY = '...':XX
        T.SEL1 = "SELECT FBNK.CUSTOMER$NAU WITH COMPANY.BOOK EQ " : ZZ
        CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG)
        IF SELECTED1 NE 0 THEN
            XX1 = SELECTED1
        END

        IF SELECTED1 EQ 0 THEN
            XX1 = ''
        END

        T.SEL2 = "SELECT FBNK.ACCOUNT$NAU WITH CO.CODE EQ " : ZZ :" AND DEPT.CODE EQ ": XX
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
        IF SELECTED2 NE 0 THEN
            XX2 = SELECTED2
        END

        IF SELECTED2 EQ 0 THEN
            XX2 = ''
        END

        T.SEL3 = "SELECT FBNK.DRAWINGS$NAU WITH CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG)
        IF SELECTED3 NE 0 THEN
            XX3 = SELECTED3
        END

        IF SELECTED3 EQ 0 THEN
            XX3 = ''
        END

        T.SEL4 = "SELECT FBNK.LETTER.OF.CREDIT$NAU WITH DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG)
        IF SELECTED4 NE 0 THEN
            XX4 = SELECTED4
        END

        IF SELECTED4 EQ 0 THEN
            XX4 = ''
        END

        T.SEL5 = "SELECT F.IM.DOCUMENT.IMAGE$NAU WITH CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL5,KEY.LIST5,"",SELECTED5,ER.MSG)
        IF SELECTED5 THEN
            FOR Q =1 TO SELECTED5
                CALL F.READ(FN.IM,KEY.LIST5<I>,R.IM,F.IM,E2)
                INPUTT    = R.IM<IM.DOC.INPUTTER>
                INPUTTER  = FIELD(INPUTT,'_',2)
                CALL DBR ('USER':@FM:EB.USE.DEPARTMENT.CODE,INPUTTER,DEPT)

                IF DEPT NE XX THEN
                    XX5 = ''
                END
                IF DEPT = XX THEN
                    IF SELECTED5 NE 0 THEN
                        XX5 = SELECTED5
                    END

                    IF SELECTED5 EQ 0 THEN
                        XX5 = ''
                    END
                END
            NEXT Q
        END

        T.SEL6 = "SELECT FBNK.FUNDS.TRANSFER$NAU  WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ER.MSG)
        IF SELECTED6 NE 0 THEN
            XX6 = SELECTED6
        END

        IF SELECTED6 EQ 0 THEN
            XX6 = ''
        END

        T.SEL7 = "SELECT FBNK.TELLER$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL7,KEY.LIST7,"",SELECTED7,ER.MSG)
        IF SELECTED7 NE 0 THEN
            XX7 = SELECTED7
        END

        IF SELECTED7 EQ 0 THEN
            XX7 = ''
        END

        T.SEL8 = "SELECT F.INF.MULTI.TXN$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL8,KEY.LIST8,"",SELECTED8,ER.MSG)
        IF SELECTED8 NE 0 THEN
            XX8 = SELECTED8
        END

        IF SELECTED8 EQ 0 THEN
            XX8 = ''
        END


        T.SEL9 = "SELECT FBNK.ACCOUNT.CLOSURE$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL9,KEY.LIST9,"",SELECTED9,ER.MSG)
        IF SELECTED9 NE 0 THEN
            XX9 = SELECTED9
        END

        IF SELECTED9 EQ 0 THEN
            XX9 = ''
        END

        T.SEL10 = "SELECT FBNK.MM.MONEY.MARKET$NAU WITH DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL10,KEY.LIST10,"",SELECTED10,ER.MSG)
        IF SELECTED10 NE 0 THEN
            XX10 = SELECTED10
        END

        IF SELECTED10 EQ 0 THEN
            XX10 = ''
        END

        T.SEL11 = "SELECT FBNK.FOREX$NAU WITH DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL11,KEY.LIST11,"",SELECTED11,ER.MSG)
        IF SELECTED11 NE 0 THEN
            XX11 = SELECTED11
        END

        IF SELECTED11 EQ 0 THEN
            XX11 = ''
        END

        T.SEL12 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21020 AND CATEGORY LE 21025 AND AND CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL12,KEY.LIST12,"",SELECTED12,ER.MSG)
        IF SELECTED12 NE 0 THEN
            XX12 = SELECTED12
        END

        IF SELECTED12 EQ 0 THEN
            XX12 = ''
        END

        T.SEL13 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY GE 21001 AND CATEGORY LE 21010 AND CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL13,KEY.LIST13,"",SELECTED13,ER.MSG)
        IF SELECTED13 NE 0 THEN
            XX13 = SELECTED13
        END

        IF SELECTED13 EQ 0 THEN
            XX13 = ''
        END

        T.SEL14 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH CATEGORY EQ 21096 AND CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL14,KEY.LIST14,"",SELECTED14,ER.MSG)
        IF SELECTED14 NE 0 THEN
            XX14 = SELECTED14
        END

        IF SELECTED14 EQ 0 THEN
            XX14 = ''
        END

        T.SEL15 = "SELECT F.SCB.BT.BATCH$NAU WITH CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL15,KEY.LIST15,"",SELECTED15,ER.MSG)
        IF SELECTED15 NE 0 THEN
            XX15 = SELECTED15
        END

        IF SELECTED15 EQ 0 THEN
            XX15 = ''
        END

        T.SEL16 = "SELECT FBNK.BILL.REGISTER$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL16,KEY.LIST16,"",SELECTED16,ER.MSG)
        IF SELECTED16 NE 0 THEN
            XX16 = SELECTED16
        END

        IF SELECTED16 EQ 0 THEN
            XX16 = ''
        END

        T.SEL17 = "SELECT F.SCB.MONTHLY.PAY.CBE$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL17,KEY.LIST17,"",SELECTED17,ER.MSG)
** IF SELECTED17 NE 0 THEN
**   XX17 = SELECTED17
** END

** IF SELECTED17 EQ 0 THEN
**   XX17 = ''
** END

        T.SEL18 = "SELECT FBNK.CARD.ISSUE$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL18,KEY.LIST18,"",SELECTED18,ER.MSG)
        IF SELECTED18 NE 0 THEN
            XX18 = SELECTED18

        END

        IF SELECTED18 EQ 0 THEN
            XX18 = ''
        END


        T.SEL19 = "SELECT F.SCB.MONTHLY.PAY.CBE$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL19,KEY.LIST19,"",SELECTED19,ER.MSG)
        IF SELECTED19 NE 0 THEN
            XX19 = SELECTED19
        END

        IF SELECTED19 EQ 0 THEN
            XX19 = ''
        END

        T.SEL20 = "SELECT F.SCB.DOCUMENT.PROCURE$NAU WITH CO.CODE EQ " : ZZ
        CALL EB.READLIST(T.SEL20,KEY.LIST20,"",SELECTED20,ER.MSG)
        IF SELECTED20 NE 0 THEN

            XX20 = SELECTED20
        END

        IF SELECTED20 EQ 0 THEN
            XX20 = ''
        END

        T.SEL21 = "SELECT FBNK.LIMIT$NAU WITH CO.CODE EQ " : ZZ  : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL21,KEY.LIST21,"",SELECTED21,ER.MSG)
        IF SELECTED21 NE 0 THEN

            XX21 = SELECTED21
        END

        IF SELECTED21 EQ 0 THEN
            XX21 = ''
        END

        T.SEL22 = "SELECT FBNK.COLLATERAL$NAU WITH CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL22,KEY.LIST22,"",SELECTED22,ER.MSG)
        IF SELECTED22 NE 0 THEN

            XX22 = SELECTED22
        END

        IF SELECTED22 EQ 0 THEN
            XX22 = ''
        END

        T.SEL23 = "SELECT FBNK.COLLATERAL.RIGHT$NAU WITH CO.CODE EQ " : ZZ : " AND DEPT.CODE EQ " : XX
        CALL EB.READLIST(T.SEL23,KEY.LIST23,"",SELECTED23,ER.MSG)
        IF SELECTED23 NE 0 THEN

            XX23 = SELECTED23
        END

        IF SELECTED23 EQ 0 THEN
            XX23 = ''
        END



        XXX1   = SPACE(132)  ; XXX4   = SPACE(132) ; XXX11 = SPACE(132) ;XXX12 = SPACE(132)
        XXX2   = SPACE(132)  ; XXX5   = SPACE(132) ; XXX10 = SPACE(132) ;XXX13 = SPACE(132)
        XXX15  = SPACE(132)  ; XXX16  = SPACE(132) ; XXX17 = SPACE(132) ;XXX14 = SPACE(132)
        XXX21  = SPACE(132)  ; XXX19  = SPACE(132) ; XXX18 = SPACE(132) ;XXX20 = SPACE(132)  ; XXX21 = SPACE(132)  ; XXX24 = SPACE(132)
        XXX23  = SPACE(132)  ; XXX24  = SPACE(132) ; XXX25 = SPACE(132)
        XXX6   = SPACE(132)  ; XXX7   = SPACE(132) ; XXX8  = SPACE(132) ; XXX9 = SPACE(132)  ; XXX22 = SPACE(132)  ; XX22 = SPACE(132)
        XXX21   = SPACE(132)  ; XXX22   = SPACE(132) ; XXX23  = SPACE(132)
        XXX30   = SPACE(132)  ; XXX31   = SPACE(132) ; XXX32  = SPACE(132) ; XXX33 = SPACE(132)  ; XXX34 = SPACE(132)  ; XXX35 = SPACE(132)
        XX23   = SPACE(132)  ; XX24   = SPACE(132)
        XX17   = SPACE(132)  ; XXX18   = SPACE(132); XXX22 = SPACE(132) ; XXX30 = SPACE(132) ; XXX23 = SPACE(132)
        IF (SELECTED23+SELECTED22+SELECTED21+SELECTED20+SELECTED19+SELECTED18 + SELECTED17+SELECTED16+SELECTED15+SELECTED14+SELECTED13+SELECTED12+SELECTED11+SELECTED10+SELECTED9+SELECTED8+SELECTED7+SELECTED6+SELECTED5+SELECTED4+SELECTED3+SELECTED2+SELECTED1) NE 0 THEN
            XXX2<1,1>[1,20]    = NAMEV1
            XXX2<1,1>[20,5]    = XX1
            XXX2<1,1>[25,5]    = XX2
            XXX2<1,1>[30,5]    = XX3
            XXX2<1,1>[35,5]    = XX4
            XXX2<1,1>[40,5]    = XX5
            XXX2<1,1>[45,5]    = XX6
            XXX2<1,1>[50,5]    = XX7
            XXX2<1,1>[55,5]    = XX8
            XXX2<1,1>[60,5]    = XX9
            XXX2<1,1>[65,5]    = XX10
            XXX2<1,1>[70,5]    = XX11
            XXX2<1,1>[75,5]    = XX12
            XXX2<1,1>[80,5]    = XX13
            XXX2<1,1>[85,5]    = XX14
            XXX2<1,1>[89,5]    = XX15
            XXX2<1,1>[91,5]    = XX16

            IF SELECTED17 NE 0 THEN
                XXX2<1,1>[94,5]    = SELECTED17
            END

            IF SELECTED17 EQ 0 THEN
                XXX2<1,1>[94,5]    = ''
            END

            IF SELECTED18 NE 0 THEN
                XXX2<1,1>[99,5]    = SELECTED18
            END

            IF SELECTED18 EQ 0 THEN
                XXX2<1,1>[99,5]    = ''
            END

            IF SELECTED19 NE 0 THEN
                XXX2<1,1>[104,5]    = SELECTED19
            END

            IF SELECTED19 EQ 0 THEN
                XXX2<1,1>[104,5]    = ''
            END

            IF SELECTED20 NE 0 THEN
                XXX2<1,1>[110,5]    = SELECTED20
            END

            IF SELECTED20 EQ 0 THEN
                XXX2<1,1>[110,5]    = ''
            END

            IF SELECTED21 NE 0 THEN
                XXX15<1,1>[19,5]    = SELECTED21
            END

            IF SELECTED21 EQ 0 THEN
                XXX15<1,1>[19,5]    = ''
            END
            IF SELECTED22 NE 0 THEN
                XXX15<1,1>[25,5]    = SELECTED22
            END

            IF SELECTED22 EQ 0 THEN
                XXX15<1,1>[25,5]    = ''
            END

            IF SELECTED23 NE 0 THEN
                XXX15<1,1>[29,5]    = SELECTED23
            END

            IF SELECTED23 EQ 0 THEN
                XXX15<1,1>[29,5]    = ''
            END


            XXX4<1,1>[1,120]   = '-------------------------------------------------------------------------------------------------------------------------'
            XXX5<1,1>[1,120]   = ''
****PRINT XX1<1,1>
            PRINT XXX2<1,1>
            PRINT XXX5<1,1>
            PRINT XXX15<1,1>
            PRINT XXX4<1,1>
        END
        IF (SELECTED23+SELECTED22+SELECTED21+SELECTED20+SELECTED19+SELECTED18+SELECTED17+SELECTED16+SELECTED15+SELECTED14+SELECTED13+SELECTED12+SELECTED11+SELECTED10+SELECTED9+SELECTED8+SELECTED7+SELECTED6+SELECTED5+SELECTED4+SELECTED3+SELECTED2+SELECTED1) = 0 THEN

            XXX4<1,1>[1,120]   = '------------------------------------------------------------------------------------'
            XXX2<1,1>[1,20]    =   NAMEV1
            XXX2<1,1>[41,35]   = "�� ���� ������ ��� �����"
            PRINT XXX2<1,1>
            PRINT XXX5<1,1>
            PRINT XXX15<1,1>
            PRINT XXX4<1,1>

        END


    NEXT I

*   XXX5<1,1>[1,5]  = '�������  : '
*   XXX6<1,1>[1,20] = '----------------'
*   XXX7<1,1>[1,20] = 'AC    : ACCOUNT'
*   XXX8<1,1>[1,20] = 'CU    : CUSTOMER'
*   XXX9<1,1>[1,20] = 'TT    : TELLER'
*   XXX10<1,1>[1,20]= 'LC    : LETTER.OF.CREDIT'
*   XXX11<1,1>[1,20]= 'DR    : DRAWINGS'
*   XXX12<1,1>[1,20]= 'SGIN  : IM.DOCUMENT.IMAGE'
*   XXX13<1,1>[1,20]= 'BR    : BILL.REGISTER'
*   XXX14<1,1>[1,20]= 'BT    : SCB.BT.BATCH'
*   XXX15<1,1>[1,20]= 'FT    : FUNDS.TRANSFER'
*   XXX16<1,1>[1,20]= 'AC-CLS: ACCOUNT.CLOSURE'
*   XXX17<1,1>[1,20]= 'CDS   : LD.LOANS.DEPOSITS'
*   XXX18<1,1>[1,20]= 'LD    : LD.LOANS.DEPOSITS'
*   XXX19<1,1>[1,20]= 'LG    : LD.LOANS.DEPOSITS'
*   XXX20<1,1>[1,20]= 'MULT  : INF.MULTI.TXN'
*   XXX21<1,1>[1,20]= 'CBE   : SCB.MONTHLY.PAY.CBE'
*   XXX22<1,1>[1,20]= 'CARD  : CARD.ISSUE'
*   XXX23<1,1>[1,20]= 'DOC   : SCB.DOCUMENT.PROCURE'
*   XXX24<1,1>[1,60]= 'LIM   : LIMIT '
*   XXX25<1,1>[1,60]= 'COLL  : COLLATERAL '
*   XXX26<1,1>[1,60]= 'COR   : COLLATERAL.RIGHT'

    XXX6<1,1>[1,5]  = '�������  : '
    XXX7<1,1>[1,20] = '----------------'
    XX8<1,1>[1,132] = 'AC=������':';':'CU= �����':';':'TT= ������':';':'LC=����������' :';':'DR=�������� ���� - ��������':';':'SGIN= ���������' : ';' :'BR=������� �����������': ';': 'BT = ����� ������� �����������'
    XXX16<1,1>[1,132]= 'FT: ���� ����� ��������':';':'AC-CLS=����� ���� �������� ':';':'CDS=��������' :';':'LD =�������' :';':'LG  : ������ ������' :';':'MULT= �� ���� ��� ������ ' : ';':'CBE=��������� �������'
    XXX31<1,1>[1,20]='DOC = ������� �������'
    XXX32<1,1>[1,20]='CAD = �������� '
    XXX33<1,1>[1,20]='LIM = ���� �����'
    XXX34<1,1>[1,20]='COL = �������� '

***XXX30<1,1>[1,20]= 'HO    : ����� �������� ���� �����'
    PRINT XXX5
    PRINT XXX6
    PRINT XXX7
    PRINT XX8
    PRINT XXX16
    PRINT XXX31
    PRINT XXX32
    PRINT XXX33
    PRINT XXX34
* PRINT XXX30
** PRINT XXX33
* PRINT XXX34
* PRINT XXX35

*-------------------------------------------------------------------
PRINT.HEAD:
*---------
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE,":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = HHH:":":MIN

    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:'/':DATY[1,4]
    PR.HD  ="'L'":SPACE(1):"��� ���� ������":SPACE(20):"���� �������� ����� �����"
    PR.HD  ="'L'":SPACE(1):"SBD.REPORT.NAU.BRANCHS"
    PR.HD :="'L'":"�������":T.DAY:SPACE(35) :"��� ������ : ":"'P'"
    PR.HD :="'L'":"����� : " : TIMEFMT
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":"�����" :SPACE(13):"CU":SPACE(4):"AC":SPACE(2):"LCD":SPACE(2):"LC":SPACE(2):"SIG":SPACE(3):"FT":SPACE(4):"TT":SPACE(2):"MULT":SPACE(1):"AC-CLS":SPACE(2):"MM":SPACE(2):"FX":SPACE(2):"CD'S":SPACE(2):"LD":SPACE(2):"LG":SPACE(2):"BT":SPACE(2):"BR":SPACE(3):"CARD":SPACE(2):"CBE":SPACE(2):"DOC"
    PR.HD :="'L'":"     " :SPACE(12):"LIM":SPACE(3):"COL":SPACE(2):"COR"
    PR.HD :="'L'":STR('_',120)
**    PRINT
    HEADING PR.HD

*------------------------------------------------------------------
*** NEXT I
***END
*===============================================================
    RETURN
END
