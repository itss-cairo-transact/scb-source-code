* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*--- CREATED BY NESSMA ON 2018/02/03
    PROGRAM SBD.CUSTOMER.LIST.ALL
*    SUBROUTINE SBD.CUSTOMER.LIST.ALL

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.QUALITY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.RATING
*-----------------------------------------------
    BRN.CODE = R.USER<EB.USE.DEPARTMENT.CODE>
*    BRN.CODE = ID.COMPANY[2]
    COMP.ID = ""
    NN.SEL = ""  ; KEY.LIST = "" ; SELECTED = "" ; ERR.SEL = ""
    IF BRN.CODE EQ 99 OR OPERATOR='SCB.OFSADMIN' THEN
        FN.CO = "F.COMPANY" ; F.CO = ""
        CALL OPF(FN.CO, F.CO)
        NN.SEL = "SELECT F.COMPANY WITH @ID UNLIKE ...88 BY @ID"
        CALL EB.READLIST(NN.SEL,KEY.LIST,"",SELECTED,ERR.SEL)
        FOR HH = 1 TO SELECTED
            COMP.ID = KEY.LIST<HH>
            GOSUB INITIAL
            GOSUB PRINT.HEAD
            GOSUB PROCESS
        NEXT HH
    END ELSE
        COMP.ID = "EG00100":FMT( BRN.CODE, 'R%2')
        GOSUB INITIAL
        GOSUB PRINT.HEAD
        GOSUB PROCESS
    END
    TEXT = "FINISHED BRANCH ":COMP.ID  ; CALL REM
    RETURN
*---------------------------------------
INITIAL:
*--------
*    OPENSEQ "&SAVEDLISTS&" , "CUSTOMER.LIST.ALL.":COMP.ID:".CSV" TO BB THEN
    OPENSEQ "/home/signat" , "CUSTOMER.LIST.ALL.":COMP.ID:".CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"CUSTOMER.LIST.ALL.":COMP.ID:".CSV"
        HUSH OFF
    END
*    OPENSEQ "&SAVEDLISTS&" , "CUSTOMER.LIST.ALL.":COMP.ID:".CSV" TO BB ELSE
    OPENSEQ "/home/signat" , "CUSTOMER.LIST.ALL.":COMP.ID:".CSV" TO BB THEN
        CREATE BB THEN
            PRINT 'FILE CUSTOMER.LIST.ALL CREATED IN /home/signat'
        END ELSE
            STOP 'Cannot create CUSTOMER.LIST FILE File IN /home/signat'
        END
    END

    FN.CUS = "FBNK.CUSTOMER"  ; F.CUS = ""
    CALL OPF(FN.CUS, F.CUS)
    T.SEL    = "" ; SEL.LIST = "" ; NO.OF.REC = "" ; ERR.SEL = ""
    CUS.ID   = "" ; R.CUS    = "" ; ERR.CUS   = ""
    CUS.ID.2 = "" ; R.CUS.2  = "" ; ERR.CUS.2 = ""

    COUNT.BRN = 1 ; COUNT.BNK = 0
    RETURN
*---------------------------------------
PRINT.HEAD:
*-----------
    BB.DATA    = ""
    BB.DATA    = ",,,,,":" OUTPUT DATE : ":TODAY:",,,,,,,,,,,"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA    = ""
    BB.DATA    = "CUSTOMER.ID":","
    BB.DATA   := "CUSTOMER.NAME":","
    BB.DATA   := "COMPANY.CODE":","
    BB.DATA   := "CUSTOMER.COMPANY":","
    BB.DATA   := "CONTACT.DATE":","
    BB.DATA   := "POSTING.RESTRICT":","
    BB.DATA   := "NEW.SECTOR":","
    BB.DATA   := "RISK.RATE":","
    BB.DATA   := "QLTY.RATE":","
    BB.DATA   := "SCB.POSTING.RESTRICT":","
    BB.DATA   := "UPDATE.DATE":","
    BB.DATA   := "NEXT.UPDATE.DATE":","
    BB.DATA   := "DORMANT.DATE":","
    BB.DATA   := "DORMANT.CODE":","
    BB.DATA   := "BANK":","
    BB.DATA   := "BANK.NAME":","
    BB.DATA   := "SCB-CDs"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    WS.BANK.NAME = "" ; WS.BANK = "" ; WS.FLAG = ""
    RETURN
*---------------------------------------
PROCESS:
*-------
    T.SEL  = "SELECT FBNK.CUSTOMER"
    T.SEL := " WITH @ID NE '175840175751763.01'"
    T.SEL := " AND POSTING.RESTRICT LT 89"
*    T.SEL := " AND COMPANY.BOOK NE EG0010088"
*    T.SEL := " AND COMPANY.BOOK NE EG0010099"
    T.SEL := " AND SECTOR NE 5010 AND SECTOR NE 5020"
    T.SEL := " AND COMPANY.BOOK EQ ":COMP.ID
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,SEL.LIST,"",NO.OF.REC,ERR.SEL)
    IF NO.OF.REC THEN
        FOR NN = 1 TO NO.OF.REC
            CUS.ID   = SEL.LIST<NN>
            CUS.ID.2 = SEL.LIST<NN+1>

            CALL F.READ(FN.CUS,CUS.ID,R.CUS,F.CUS,ERR.CUS)
            CALL F.READ(FN.CUS,CUS.ID.2,R.CUS.2,F.CUS,ERR.CUS.2)
            CUS.COMP.2  = R.CUS.2<EB.CUS.COMPANY.BOOK>
            WS.BANK = R.CUS<EB.CUS.LOCAL.REF><1,CULR.CU.BANK>
            CALL DBR ('SCB.BANK':@FM:SCB.BAN.BANK.NAME,WS.BANK,WS.BANK.NAME)
            WS.FLAG = R.CUS<EB.CUS.LOCAL.REF><1,CULR.SCCD.CUSTOMER>

            CUS.NAME  = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,1>
            CUS.NAME := " ":R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,2>
            CUS.NAME := " ":R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME,3>
            IF CUS.NAME EQ '' THEN
                CUS.NAME = R.CUS<EB.CUS.SHORT.NAME>
            END
            CUS.COMP  = R.CUS<EB.CUS.COMPANY.BOOK>
            COMP.NAME = ""
            CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,CUS.COMP,COMP.NAME)

            CONT.DATE = R.CUS<EB.CUS.CONTACT.DATE>
            IF CONT.DATE THEN
                CONT.DATE = FMT(CONT.DATE,"####/##/##")
            END
            POST.REST1 = R.CUS<EB.CUS.POSTING.RESTRICT>
            CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,POST.REST1,POST.REST)

            NEW.SEC   = R.CUS<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            CALL DBR ('SCB.NEW.SECTOR':@FM:C.SCB.NEW.SECTOR.NAME,NEW.SEC,NEW.SECTOR)

            CR.STAT   = R.CUS<EB.CUS.LOCAL.REF><1,CULR.CREDIT.STAT>

            SCB.POST1 = R.CUS<EB.CUS.LOCAL.REF><1,CULR.SCB.POSTING>
            CALL DBR ('POSTING.RESTRICT':@FM:AC.POS.DESCRIPTION,SCB.POST1,SCB.POST)

            RISK.RAT1 = R.CUS<EB.CUS.LOCAL.REF><1,CULR.RISK.RATE>
            CALL DBR ('SCB.RISK.RATING':@FM:RIRAT.DESCRIPTION,RISK.RAT1,RISK.RAT)

            QLTY.RAT1 = R.CUS<EB.CUS.LOCAL.REF><1,CULR.QLTY.RATE>
            CALL DBR ('SCB.CUS.QUALITY':@FM:SCB.QUAT.DESCRIPTION,QLTY.RAT1,QLTY.RAT)

            UPD.DATE  =  R.CUS<EB.CUS.LOCAL.REF><1,CULR.UPDATE.DATE>
            IF UPD.DATE THEN
                UPD.DATE = FMT(UPD.DATE,"####/##/##")
            END

            NXT.UPD.DATE = R.CUS<EB.CUS.REVIEW.FREQUENCY>
            FREQ.ENRCH = "" ; FREQ.1 = "" ; DATE.1 = ""
            FREQ.1     = NXT.UPD.DATE[9,5]
            DATE.1     = NXT.UPD.DATE[1,8]
            CALL EB.BUILD.RECURRENCE.MASK(FREQ.1 ,DATE.1, FREQ.ENRCH)
            DRMNT.DATE = R.CUS<EB.CUS.LOCAL.REF><1,CULR.DRMNT.DATE>
            IF DRMNT.DATE THEN
                DRMNT.DATE = FMT(DRMNT.DATE,"####/##/##")
            END
            DRMNT.CODE = R.CUS<EB.CUS.LOCAL.REF><1,CULR.DRMNT.CODE>
            IF DRMNT.CODE EQ 1 THEN DRMNT.CODE = "������ ��������"

            BB.DATA    = ""
            BB.DATA    = CUS.ID:","
            BB.DATA   := CUS.NAME:","
            BB.DATA   := CUS.COMP:","
            BB.DATA   := COMP.NAME:","
            BB.DATA   := CONT.DATE:","
            BB.DATA   := POST.REST:","
            BB.DATA   := NEW.SECTOR:","
            BB.DATA   := RISK.RAT:","
            BB.DATA   := QLTY.RAT:","
            BB.DATA   := SCB.POST:","
            BB.DATA   := UPD.DATE:","
            BB.DATA   := FREQ.ENRCH:","
            BB.DATA   := DRMNT.DATE:","
            BB.DATA   := DRMNT.CODE:","
            BB.DATA   := WS.BANK:","
            BB.DATA   := WS.BANK.NAME:","
            BB.DATA   := WS.FLAG

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            IF CUS.COMP EQ CUS.COMP.2 THEN
                COUNT.BRN++
            END
            IF CUS.COMP NE CUS.COMP.2 THEN
                BB.DATA = ""
                BB.DATA = "=====,TOTAL,":COMP.NAME:",":COUNT.BRN
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                COUNT.BRN = 1
            END

            COUNT.BNK++
            RISK.RAT1  = ""
            QLTY.RAT1  = ""
            CUS.ID     = ""
            CUS.NAME   = ""
            CUS.COMP   = ""
            COMP.NAME  = ""
            CONT.DATE  = ""
            POST.REST  = ""
            POST.REST1 = ""
            NEW.SECTOR = ""
            RISK.RAT   = ""
            QLTY.RAT   = ""
            SCB.POST   = ""
            SCB.POST1  = ""
            UPD.DATE   = ""
            FREQ.ENRCH = ""
            DRMNT.DATE = ""
            DRMNT.CODE = ""
        NEXT NN
    END

*    BB.DATA = ""
*    BB.DATA = "=====,BANK-TOTAL,":COUNT.BNK
*    WRITESEQ BB.DATA TO BB ELSE
*        PRINT " ERROR WRITE FILE "
*    END
    RETURN
*---------------------------------------
END
