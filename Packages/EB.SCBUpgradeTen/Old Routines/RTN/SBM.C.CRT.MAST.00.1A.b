* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.C.CRT.MAST.00.1A
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CBE.CY.RATE
*---------------------------------------------------
*
    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

    FN.NRATE = "F.SCB.CBE.CY.RATE"
    F.NRATE  = ""

*----------------------------------------------------
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.NRATE,F.NRATE)

*-------------------------------------------------
    FN.CLEAR = "F.SCB.CBE.CY.RATE"
**OPEN FN.CLEAR TO FILEVAR ELSE ABORT 201, FN.CLEAR
    FILEVAR = ''
    CALL OPF(FN.CLEAR,FILEVAR)

    CLEARFILE FILEVAR
*------------------------------------------------
    R.NRATE = ""
    GOSUB A.100.GET.NZD
    RETURN
*------------------------------------------
A.100.GET.NZD:
    WS.KEYCY = "NZD"
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    IF MSG.CY EQ "" THEN
        GOSUB A.200.READ
    END

    RETURN
*---------------------------------------------------
A.200.READ:
    WS.COUNT = DCOUNT(R.CCY<RE.BCP.ORIGINAL.CCY>,@VM)
    GOSUB A.250.CON
    RETURN
*------------------------------------------------
A.250.CON:
    FOR APP = 1  TO WS.COUNT
        WS.CY.CODE  = R.CCY<RE.BCP.ORIGINAL.CCY,APP>
        WS.RATE.ARY = R.CCY<RE.BCP.RATE,APP>
        WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE,APP>
        GOSUB A.500.CRT.REC
    NEXT APP

    RETURN
*--------------------------------------------------------

A.500.CRT.REC:

    R.NRATE<CCR.CY.RATE>  = WS.RATE.ARY
    R.NRATE<CCR.CY.CALC>  = WS.MLT.DIVD.ARY
    R.NRATE<CCR.RESERVED01> = TODAY
    CALL F.WRITE(FN.NRATE,WS.CY.CODE,R.NRATE)
    CALL  JOURNAL.UPDATE(WS.CY.CODE)
    RETURN
END
