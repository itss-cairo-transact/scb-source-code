* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****MOHSEN
    PROGRAM SBM.C.CRT.FIN.POS.01
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.BANK.FIN.POS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*---------------------------------------------------
    FN.FIN = "F.CBE.BANK.FIN.POS"
    F.FIN  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""

*-------------------------------------------------
*REC KEY  =  X               ������ � ������
*         =  XX              ����� ���� ������ � ������
*         =  XXXX             ����� ������� �� ����� ������

*REC NATURE -----   H  = HEAD
*                   SH = SUB HEAD
*                   D  = DETAIL
*                   ST = SUB TOTAL
*                   T  = TOTAL
*                   HD = HEAD AND DATAIL IN SAME TIME
*-------------------------------------------------
    WS.KEY = ""
    R.FIN = ""
*-------------------------------------------------
    CALL OPF (FN.FIN,F.FIN)
    CALL OPF (FN.COMP,F.COMP)
*-------------------------------------------------------
    FN.CLEAR = "F.CBE.BANK.FIN.POS"
** OPEN FN.CLEAR TO FILEVAR ELSE ABORT 201, FN.CLEAR

    FILEVAR = ''
    CALL OPF(FN.CLEAR,FILEVAR)
    CLEARFILE FILEVAR
*------------------------PROCEDURE------------------------
    GOSUB A.050.GET.ALL.BR
* GOSUB A.100.CRT
    RETURN
*------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC
*
        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR LT 10 THEN
            WS.BR = WS.COMP.ID[1]
        END
*        IF  WS.BR EQ 1 THEN
*            GOSUB A.100.CRT
*        END

*        IF  WS.BR EQ 32 THEN
*            GOSUB A.100.CRT
*        END

*        IF  WS.BR EQ 99 THEN
*            GOSUB A.100.CRT
*        END
        GOSUB A.100.CRT
A.050.A:
    REPEAT
    RETURN

*-----------------------------------------------
A.100.CRT:
*...........................................................

    WS.KEY = "1010000"
    WS.DSCR = "����� � ����� �������� ��� ����� �������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1010100"
    WS.DSCR = "������"
    WS.TYPE = "D"
    GOSUB A.500.CRT


    WS.KEY = "1010200"
    WS.DSCR = "����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT


    WS.KEY = "1010300"
    WS.DSCR = "����� � ������ � ������� ����� ����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1010400"
    WS.DSCR = "����� �������� ��� ����� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1019999"
    WS.DSCR = "������ ����� � ����� �������� ��� ����� �������"
    WS.TYPE = "T"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "1020000"
    WS.DSCR = "����� ��� ������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1020100"
    WS.DSCR = "����� �������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020101"
    WS.DSCR = "�����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020102"
    WS.DSCR = "�� ���� ���� �� 10 %"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020103"
    WS.DSCR = "����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020104"
    WS.DSCR = "����� ����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020105"
    WS.DSCR = "����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020199"
    WS.DSCR = "����� ������� ��� ����� �������"
    WS.TYPE = "ST"
    GOSUB A.500.CRT

    WS.KEY = "1020200"
    WS.DSCR = "���� �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020201"
    WS.DSCR = "������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020202"
    WS.DSCR = "�����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020203"
    WS.DSCR = "����� ���� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020204"
    WS.DSCR = "����� ��� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020299"
    WS.DSCR = "����� ������� ��� ������ �������"
    WS.TYPE = "ST"
    GOSUB A.500.CRT

    WS.KEY = "1020300"
    WS.DSCR = "���� ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020301"
    WS.DSCR = "������ �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020302"
    WS.DSCR = "��� ������ "
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020303"
    WS.DSCR = "��� ������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020304"
    WS.DSCR = "�����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020305"
    WS.DSCR = "��� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020306"
    WS.DSCR = "��� ������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020307"
    WS.DSCR = "������ �������� �������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1020308"
    WS.DSCR = "��� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020309"
    WS.DSCR = "��� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1020399"
    WS.DSCR = "����� ������� ��� ������ ��������"
    WS.TYPE = "ST"
    GOSUB A.500.CRT

    WS.KEY = "1029999"
    WS.DSCR = "������ ������� ��� ������"
    WS.TYPE = "T"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "1030000"
    WS.DSCR = "����� ��� ����� ������� ���� ���� �����"
    WS.TYPE = "HD"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "1040000"
    WS.DSCR = "���� ����� � ����� ������ ���� "
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1040100"
    WS.DSCR = "���� �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1040101"
    WS.DSCR = "���� ������� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1040102"
    WS.DSCR = "���� ���� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1040200"
    WS.DSCR = "����� ������ ���� ����� ����� ��� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1049999"
    WS.DSCR = "������ ���� ������� � ������� �������� ������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1050000"
    WS.DSCR = "���� ����� �������"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1060000"
    WS.DSCR = "��������� ����� ���� ��������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1060100"
    WS.DSCR = "��������� �� ���"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1060101"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060102"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060103"
    WS.DSCR = "������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060104"
    WS.DSCR = "����� ������ ���������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060105"
    WS.DSCR = "����� ����� ���� ������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060106"
    WS.DSCR = "����� ���� �� ��� ����� ����� ����� ��������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060200"
    WS.DSCR = "��������� �� ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1060201"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060202"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060203"
    WS.DSCR = "������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060204"
    WS.DSCR = "����� ������ ���������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1060205"
    WS.DSCR = "����� ����� ���� ������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1069999"
    WS.DSCR = "������ ����������� ������� ���� ��������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1070000"
    WS.DSCR = "��������� ����� ����� �����"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1070100"
    WS.DSCR = "��������� �� ���"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1070101"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1070102"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1070103"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1070200"
    WS.DSCR = "��������� �� ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1070201"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1070202"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1070203"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1079999"
    WS.DSCR = "������ ����������� ������� ������� �����"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................

    WS.KEY = "1080000"
    WS.DSCR = "����� ������ ������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1080100"
    WS.DSCR = "����� ����� �� ���"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1080101"
    WS.DSCR = "���� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1080102"
    WS.DSCR = "��� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1080200"
    WS.DSCR = "����� ����� �� ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1080201"
    WS.DSCR = "���� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1080202"
    WS.DSCR = "��� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1089999"
    WS.DSCR = "������ ������� �������� ��������"
    WS.TYPE = "T"
    GOSUB A.500.CRT


*..........................................................
    WS.KEY = "1090000"
    WS.DSCR = "������� �������� ����� �������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1090100"
    WS.DSCR = "����� ���� ���"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1090101"
    WS.DSCR = "����� ���"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090102"
    WS.DSCR = "����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090103"
    WS.DSCR = "����� ������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090104"
    WS.DSCR = "����� ����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090105"
    WS.DSCR = "����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090106"
    WS.DSCR = "����� ����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090107"
    WS.DSCR = "����� ������� �� ���� �� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090108"
    WS.DSCR = "����� ��� ������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090109"
    WS.DSCR = "������� ����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090110"
    WS.DSCR = "���� ������ �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1090111"
    WS.DSCR = "����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090112"
    WS.DSCR = "���� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090200"
    WS.DSCR = "����� ��� ���"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1090201"
    WS.DSCR = "����� ��� ����� � �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090202"
    WS.DSCR = "������� ����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090203"
    WS.DSCR = "���� ������ �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "1090204"
    WS.DSCR = "����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1090205"
    WS.DSCR = "���� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1099999"
    WS.DSCR = "������ ��������� ������� �������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................

    WS.KEY = "1100000"
    WS.DSCR = "������� �������� ����� ������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1100100"
    WS.DSCR = "���� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1100200"
    WS.DSCR = "���� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1109999"
    WS.DSCR = "������ ��������� ���������� ������� ������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1110000"
    WS.DSCR = "��������� ����� ����� ��� ��� ����� ���������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1110100"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1110200"
    WS.DSCR = "����� ������ ��������� ����� ��� ����� ������ �������� ���"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1119999"
    WS.DSCR = "������ ����������� ������� ������� ��� ��� ����� ���������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1120000"
    WS.DSCR = "��������� ����� �� ����� ����� ���� ����� ������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1120100"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1120200"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1129999"
    WS.DSCR = "������ ����������� ������� �� ������� ������� ���� �������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1130000"
    WS.DSCR = "������� ��� ����� ������� ����� ��������� �� ����� �������"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1140000"
    WS.DSCR = "��������� �� ���� ����� ����� ����� 15"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1150000"
    WS.DSCR = "����� ����� � ���� ����"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "1150100"
    WS.DSCR = "������ �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150200"
    WS.DSCR = "�������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150300"
    WS.DSCR = "����� ���� ������� ��������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150400"
    WS.DSCR = "����� ������ ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150500"
    WS.DSCR = "������� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150600"
    WS.DSCR = "������� ������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150700"
    WS.DSCR = "����� ����� ���  ���� ���� ���� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150800"
    WS.DSCR = "����� ������� ����� ��� ��� ���������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1150900"
    WS.DSCR = "������� � ���"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1151000"
    WS.DSCR = "����� ������ ��� ���� �������� �� ����� ��� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1151100"
    WS.DSCR = "���� ��� ������� ����� ���� ����� ��� ��� ���� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1151200"
    WS.DSCR = "����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1151300"
    WS.DSCR = "����� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1151400"
    WS.DSCR = "����� ����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "1159999"
    WS.DSCR = "������ ������� ������� � ������ ������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1160000"
    WS.DSCR = "����� ����� � ������ ������� ������ ��� ��� �������"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1170000"
    WS.DSCR = "���� ����� ����� �� ��� ����"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1189999"
    WS.DSCR = "������ ������ "
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "1190000"
    WS.DSCR = "������ ���� ������"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

    RETURN
*------------------------------------------------
A.500.CRT:
    R.FIN<CBE.FIN.POS.LIN.NO>  = WS.KEY
    R.FIN<CBE.FIN.POS.BR>  = WS.BR
    WS.NEW.KEY = WS.KEY:"*":WS.BR
    R.FIN<CBE.FIN.POS.DSCRPTION> = WS.DSCR
    R.FIN<CBE.FIN.POS.REC.TYPE> =  WS.TYPE
    R.FIN<CBE.FIN.POS.AMT.LCY>  =  0
    R.FIN<CBE.FIN.POS.AMT.FCY>  =  0
    CALL F.WRITE(FN.FIN,WS.NEW.KEY,R.FIN)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)
    RETURN
END
