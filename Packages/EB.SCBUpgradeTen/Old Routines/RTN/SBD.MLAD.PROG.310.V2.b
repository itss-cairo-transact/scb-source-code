* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>226</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.MLAD.PROG.310.V2
***    PROGRAM SBD.MLAD.PROG.310.V2
**                            ����� / ���� �����
***                            �������� �

*   ------------------------------
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.HIST
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.020
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------

    GOSUB INIT

    GOSUB OPENFILES



*-------------------------------------------------------------------------

*-------------------------------------------
    SEL.MLADH  = "SELECT ":FN.MLADHIS:" WITH @ID LIKE ...99999999"

    GOSUB RESET.MY.ARRY

    GOSUB MAIN.ROUTINE

*-------------------------------
*-------------------------------

    RETURN
*-------------------------------------------------------------------------
INIT:

    PROGRAM.ID = "SBD.MLAD.PROG.310.V2"

    FN.MLADHIS = "F.SCB.MLAD.HIST"
    F.MLADHIS  = ""
    R.MLADHIS  = ""
    Y.MLADHIS.ID  = ""
    KEY.LISTHIST  = ""

    FN.MLAD020 = "F.SCB.MLAD.FILE.020"
    F.MLAD020  = ""
    R.MLAD020  = ""
    Y.MLAD020.ID  = ""

*---------------------------------------
    DIM A.L.CAT(6)
    DIM A.H.CAT(6)
    DIM A.3YEAR(6)
    DIM A.YEAR(6)
    DIM A.6MONTH(6)
    DIM A.3MONTH(6)
    DIM A.MONTH(6)
    DIM A.WEEK(6)
    DIM A.DAY(6)
    DIM A.TOTAL(6)


    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLADHIS,F.MLADHIS)
    CALL OPF(FN.MLAD020,F.MLAD020)
    RETURN
*--------------------------------------------------------------------------
MAIN.ROUTINE:

    CALL EB.READLIST(SEL.MLADH,SEL.LIST.MLADH,'',NO.OF.MLADH,ERR.MLADH)


    LOOP
        REMOVE Y.MLADH.ID FROM SEL.LIST.MLADH SETTING POS.MLADH
    WHILE Y.MLADH.ID:POS.MLADH

        CALL F.READ(FN.MLADHIS,Y.MLADH.ID,R.MLADH,F.MLADH,ERR.MLADH)


        WS.FILE.CY       =   Y.MLADH.ID[1,3]

        WS.CY            =   R.MLADH<MLAD.HIS.CURRENCY>

        WS.PERCENT         =   R.MLADH<MLAD.HIS.CURRENT.AMT>
        WS.BALANCE         =   R.MLADH<MLAD.HIS.SAVING.AMT>
        WS.MED.MIN.BAL     =   R.MLADH<MLAD.HIS.TOT.CCY.BALANCE>

* ---------
        IF WS.PERCENT LT 100 THEN
            WS.BALANCE = WS.BALANCE - WS.MED.MIN.BAL
        END ELSE
            WS.MED.MIN.BAL = 0
        END
* ---------

        FOR IX = 1 TO 1

            WS.PART.VALUE = WS.BALANCE / A.TOTAL(IX)

            WS.VAL.3YEAR  = A.3YEAR(IX)  * WS.PART.VALUE
            WS.VAL.YEAR   = A.YEAR(IX)   * WS.PART.VALUE
            WS.VAL.6MONTH = A.6MONTH(IX) * WS.PART.VALUE
            WS.VAL.3MONTH = A.3MONTH(IX) * WS.PART.VALUE
            WS.VAL.MONTH  = A.MONTH(IX)  * WS.PART.VALUE
            WS.VAL.WEEK   = A.WEEK(IX)   * WS.PART.VALUE
            WS.VAL.DAY    = A.DAY(IX)    * WS.PART.VALUE


***             PRINT WS.CY:" ":WS.MED.MIN.BAL:" ":WS.VAL.6MONTH:" ":WS.VAL.3MONTH:" ":WS.VAL.MONTH:" ":WS.VAL.WEEK:" ":WS.VAL.DAY

            GOSUB WRITE.MLAD.FILE.020


        NEXT IX
*----------------------------------------------


    REPEAT


    RETURN
*--------------------------------------------------------------------------
WRITE.MLAD.FILE.020:

****    GOSUB CLEAR.MLAD.FILE.020


    Y.MLAD020.ID = WS.CY:"-Y-050"

    CALL F.READ(FN.MLAD020,Y.MLAD020.ID,R.MLAD020,F.MLAD020,ERR.MLAD020)

    R.MLAD020<ML020.NEXT.DAY>      += WS.VAL.DAY
    R.MLAD020<ML020.NEXT.WEEK>     += WS.VAL.WEEK
    R.MLAD020<ML020.NEXT.MONTH>    += WS.VAL.MONTH
    R.MLAD020<ML020.NEXT.3MONTH>   += WS.VAL.3MONTH
    R.MLAD020<ML020.NEXT.6MONTH>   += WS.VAL.6MONTH
    R.MLAD020<ML020.NEXT.YEAR>     += WS.VAL.YEAR
    R.MLAD020<ML020.NEXT.3YEAR>    += WS.VAL.3YEAR

*                                                                 ����� ����� ������ �� ������ ���� �� ���
    R.MLAD020<ML020.NEXT.3YEAR>    += WS.MED.MIN.BAL

*------------------------
    R.MLAD020<ML020.RECORD.STATUS>  = 'Y'
    R.MLAD020<ML020.RESERVED1> = Y.MLAD020.ID[5,5]
    CALL F.WRITE(FN.MLAD020,Y.MLAD020.ID,R.MLAD020)
    CALL JOURNAL.UPDATE(Y.MLAD020.ID)

    RETURN
*===============================================================
CLEAR.MLAD.FILE.020:

    Y.MLAD020.ID = WS.CY:"-Y-050"

    CALL F.READ(FN.MLAD020,Y.MLAD020.ID,R.MLAD020,F.MLAD020,ERR.MLAD020)

    R.MLAD020<ML020.NEXT.DAY>      = 0
    R.MLAD020<ML020.NEXT.WEEK>     = 0
    R.MLAD020<ML020.NEXT.MONTH>    = 0
    R.MLAD020<ML020.NEXT.3MONTH>   = 0
    R.MLAD020<ML020.NEXT.6MONTH>   = 0
    R.MLAD020<ML020.NEXT.YEAR>     = 0
    R.MLAD020<ML020.NEXT.3YEAR>    = 0
    R.MLAD020<ML020.NEXT.MORE>     = 0

*------------------------
    CALL F.WRITE(FN.MLAD020,Y.MLAD020.ID,R.MLAD020)
    CALL JOURNAL.UPDATE(Y.MLAD020.ID)

    RETURN

*===============================================================
RESET.MY.ARRY:

    FOR IX = 1 TO 6
        BEGIN CASE
* -----
        CASE IX EQ 1
            A.L.CAT(IX)  = 0
            A.H.CAT(IX)  = 100
            A.3YEAR(IX)  = 0
            A.YEAR(IX)   = 180
            A.6MONTH(IX) = 90
            A.3MONTH(IX) = 60
            A.MONTH(IX)  = 23
            A.WEEK(IX)   = 6
            A.DAY(IX)    = 1
* -----
        CASE IX EQ 2
            A.L.CAT(IX)  = 0
            A.H.CAT(IX)  = 100
            A.3YEAR(IX)  = 0
            A.YEAR(IX)   = 180
            A.6MONTH(IX) = 90
            A.3MONTH(IX) = 60
            A.MONTH(IX)  = 23
            A.WEEK(IX)   = 6
            A.DAY(IX)    = 1
* -----
        CASE IX EQ 3
            A.L.CAT(IX)  = 0
            A.H.CAT(IX)  = 100
            A.3YEAR(IX)  = 0
            A.YEAR(IX)   = 0
            A.6MONTH(IX) = 120
            A.3MONTH(IX) = 60
            A.MONTH(IX)  = 20
            A.WEEK(IX)   = 4
            A.DAY(IX)    = 1
* -----
        CASE IX EQ 4
            A.L.CAT(IX)  = 0
            A.H.CAT(IX)  = 100
            A.3YEAR(IX)  = 0
            A.YEAR(IX)   = 0
            A.6MONTH(IX) = 120
            A.3MONTH(IX) = 60
            A.MONTH(IX)  = 20
            A.WEEK(IX)   = 4
            A.DAY(IX)    = 1
* -----
        CASE IX EQ 5
            A.L.CAT(IX)  = 0
            A.H.CAT(IX)  = 100
            A.3YEAR(IX)  = 0
            A.YEAR(IX)   = 0
            A.6MONTH(IX) = 120
            A.3MONTH(IX) = 60
            A.MONTH(IX)  = 20
            A.WEEK(IX)   = 4
            A.DAY(IX)    = 1
* -----
        CASE IX EQ 6
            A.L.CAT(IX)  = 0
            A.H.CAT(IX)  = 100
            A.3YEAR(IX)  = 0
            A.YEAR(IX)   = 0
            A.6MONTH(IX) = 120
            A.3MONTH(IX) = 60
            A.MONTH(IX)  = 20
            A.WEEK(IX)   = 4
            A.DAY(IX)    = 1

        END CASE

        A.TOTAL(IX)  = A.YEAR(IX) +  A.6MONTH(IX) + A.3MONTH(IX) + A.MONTH(IX) + A.WEEK(IX) + A.DAY(IX)

    NEXT IX
    RETURN
*===============================================================
*===============================================================
END
