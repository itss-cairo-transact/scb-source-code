* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>127</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.FUND.DEP.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.MM.MONEY.MARKET
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*-------------------------------------------------------------------------

    GOSUB INITIATE
**DEBUG
***** CURRENCY *************
******    T.SEL2 = "SELECT FBNK.CURRENCY BY @ID"
    T.SEL2 = "SELECT FBNK.CURRENCY WITH @ID EQ EGP"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
            CUR.ID = KEY.LIST2<NN>
            GOSUB PROCESS
        NEXT NN
    END
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "DEPOSIT.ALL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DEPOSIT.ALL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DEPOSIT.ALL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DEPOSIT.ALL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create DEPOSIT.ALL.CSV File IN &SAVEDLISTS&'
        END
    END

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    DAT = TODAY
    CALL CDT("",DAT,'-1C')
    TD  = TODAY
    CALL CDT("",TD,'-1W')
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "DEPOSITS"
    HEAD.DESC  = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
*  HEAD.DESC := "ON":","
    HEAD.DESC := "1DAYS":","
    HEAD.DESC := "2DAYS":","
    HEAD.DESC := "3DAYS":","
    HEAD.DESC := "4DAYS":","
    HEAD.DESC := "5DAYS":","
    HEAD.DESC := "2WEEKS":","
    HEAD.DESC := "3WEEKS":","
    HEAD.DESC := "4WEEKS":","
    HEAD.DESC := "5WEEKS":","
    HEAD.DESC := "6WEEKS":","
    HEAD.DESC := "7WEEKS":","
    HEAD.DESC := "8WEEKS":","
    HEAD.DESC := "OVER 8 WEEKS":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN

*========================================================================
PROCESS:

    BAL1.1.P = 0 ; BAL14.1.P  = ''  ; BAL1.1.C  = 0 ; BAL14.1.C  = ''
    BAL1.P  = 0 ; BAL1.C  = 0  ; BAL2.P  = 0 ; BAL2.C  = 0 ;BAL3.P  = 0 ; BAL3.C  = 0  ; BAL4.P  = 0 ; BAL4.C  = 0
    BAL5.P  = 0 ; BAL5.C  = 0  ; BAL6.P  = 0 ; BAL6.C  = 0 ;BAL7.P  = 0 ; BAL7.C  = 0  ; BAL8.P  = 0 ; BAL8.C  = 0
    BAL9.P  = 0 ; BAL9.C  = 0  ; BAL10.P = 0 ; BAL10.C = 0 ;BAL11.P = 0 ; BAL11.C = 0  ; BAL12.P = 0 ; BAL12.C = 0
    BAL13.P = 0 ; BAL13.C = 0  ; BAL14.P = 0 ; BAL14.C = 0 ;BAL15.P = 0 ; BAL15.C = 0  ; BAL16.P = 0 ; BAL16.C = 0
    BAL17.P = 0 ; BAL17.C = 0  ; BAL18.P = 0 ; BAL18.C = 0 ;BAL19.P = 0 ; BAL19.C = 0  ; BAL20.P = 0 ; BAL20.C = 0
    BAL21.P = 0 ; BAL21.C = 0  ; BAL22.P = 0 ; BAL22.C = 0 ;BAL23.P = 0 ; BAL23.C = 0  ; BAL24.P = 0 ; BAL24.C = 0
    BAL25.P = 0 ; BAL25.C = 0  ; BAL26.P = 0 ; BAL26.C = 0 ;BAL27.P = 0 ; BAL27.C = 0  ; BAL28.P = 0 ; BAL28.C = 0
    BAL29.P = 0 ; BAL29.C = 0  ; BAL30.P = 0 ; BAL30.C = 0 ;BAL31.P = 0 ; BAL31.C = 0  ; BAL32.P = 0 ; BAL32.C = 0
    BAL33.P = 0 ; BAL33.C = 0  ; BAL34.P = 0 ; BAL34.C = 0 ;BAL35.P = 0 ; BAL35.C = 0  ; BAL36.P = 0 ; BAL36.C = 0
    BAL37.P = 0 ; BAL37.C = 0  ; BAL38.P = 0 ; BAL38.C = 0 ;BAL39.P = 0 ; BAL39.C = 0  ; BAL40.P = 0 ; BAL40.C = 0
    BAL41.P = 0 ; BAL41.C = 0  ; BAL42.P = 0 ; BAL42.C = 0 ;BAL43.P = 0 ; BAL43.C = 0  ; BAL44.P = 0 ; BAL44.C = 0
    BAL45.P = 0 ; BAL45.C = 0  ; BAL46.P = 0 ; BAL46.C = 0 ;BAL47.P = 0 ; BAL47.C = 0  ; BAL48.P = 0 ; BAL48.C = 0
    BAL49.P = 0 ; BAL49.C = 0  ; BAL50.P = 0 ; BAL50.C = 0 ; BAL51.P = 0; BAL51.C = 0  ; BAL52.P = 0 ; BAL52.C = 0
    BAL53.P = 0 ; BAL53.C = 0  ; BAL54.P = 0 ; BAL54.C = 0 ; BAL55.P = 0; BAL55.C = 0
*-------------------------------------------
******* LD  *************
    T.SEL = "SELECT ":FN.LD:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21010) OR (CATEGORY GE 21020 AND CATEGORY LE 21029) OR (CATEGORY EQ 21032)) AND (( VALUE.DATE LE ":DAT:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":DAT:" AND AMOUNT EQ 0 )) AND CURRENCY EQ ":CUR.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR G = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<G>,R.LD,F.LD,E1)
            CATEG    = R.LD<LD.CATEGORY>
            AMT      = R.LD<LD.AMOUNT>
            IF AMT EQ 0 THEN
                AMT  = R.LD<LD.REIMBURSE.AMOUNT>
            END
            CUS.ID   = R.LD<LD.CUSTOMER.ID>
            EXP.DATE = R.LD<LD.FIN.MAT.DATE>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

***********GET NO OF DAYS*************
            TD.DATE = TODAY
            DAYS.55  = 'C'
            IF EXP.DATE GE TD.DATE THEN
                CALL CDD ("",TD.DATE,EXP.DATE,DAYS.55)
            END
************END NO OF DAYS**************
****** OVER NIGHT **************
            IF EXP.DATE EQ '' THEN
                IF CUST.SECTOR EQ 4650 THEN
                    BAL28.P  += AMT
                END ELSE
                    BAL28.C  += AMT
                END
            END
********END OVER NIGHT *********
            IF  EXP.DATE NE '' THEN
                IF DAYS.55 GE 0 AND DAYS.55 LE 1 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL1.P += AMT
                    END ELSE
                        BAL1.C  += AMT
                    END
                END

                IF DAYS.55 = 2 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL2.P  += AMT
                    END ELSE
                        BAL2.C += AMT
                    END
                END

                IF DAYS.55 = 3 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL3.P  += AMT
                    END ELSE
                        BAL3.C += AMT
                    END
                END

                IF DAYS.55 = 4 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL4.P  += AMT
                    END ELSE
                        BAL4.C  += AMT
                    END
                END
                IF DAYS.55 = 5 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL5.P  += AMT
                    END ELSE
                        BAL5.C  += AMT
                    END
                END
                IF DAYS.55 GT 5 AND DAYS.55 LE 14 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL6.P  += AMT
                    END ELSE
                        BAL6.C  += AMT
                    END
                END
                IF DAYS.55 GT 14 AND DAYS.55 LE 21 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL7.P  += AMT
                    END ELSE
                        BAL7.C  += AMT
                    END
                END

                IF DAYS.55 GT 21 AND DAYS.55 LE 28 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL8.P  += AMT
                    END ELSE
                        BAL8.C  += AMT
                    END
                END
                IF DAYS.55 GT 28 AND DAYS.55 LE 35 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL9.P  += AMT
                    END ELSE
                        BAL9.C  += AMT
                    END
                END
                IF DAYS.55 GT 35 AND DAYS.55 LE 42 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL10.P  += AMT
                    END ELSE
                        BAL10.C  += AMT
                    END
                END
                IF DAYS.55 GT 42 AND DAYS.55 LE 49 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL11.P  += AMT
                    END ELSE
                        BAL11.C  += AMT
                    END
                END
                IF DAYS.55 GT 49 AND DAYS.55 LE 56 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL12.P  += AMT
                    END ELSE
                        BAL12.C  += AMT
                    END
                END
                IF DAYS.55 GT 56 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL13.P  += AMT
                    END ELSE
                        BAL13.C  += AMT
                    END
                END
            END
        NEXT G
    END
****************************************
******* AC  *************

    T.SEL  = "SELECT ":FN.AC:" WITH  CATEGORY IN (6512 6501 6502 6503 6504 6511 1012 1013 1014 1015 1016 1019 16170) AND CURRENCY EQ ":CUR.ID:" AND OPEN.ACTUAL.BAL GT 0"
  *   T.SEL  = "SELECT FBNK.ACCOUNT WITH CATEGORY  IN ( 6512 6501 6502 6503 6504 6511 16151 16153 16188 16152 11380 16175 1012 1013 1014 1015 1016 1019 16170  3011 3012 3013 3014 3017 3010 3005)  AND CURRENCY EQ EGP AND OPEN.ACTUAL.BAL GT 0 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR GG = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<GG>,R.AC,F.AC,E1)
            CATEG       = R.AC<AC.CATEGORY>
            AMT         = R.AC<AC.OPEN.ACTUAL.BAL>
            CUS.ID      = R.AC<AC.CUSTOMER>
            CUS.LIM     = R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
****** OVER NIGHT **************
            IF (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
                IF CUST.SECTOR EQ 4650 THEN
                    BAL27.P  += AMT
                END ELSE
                    BAL27.C  += AMT
                END
            END
*************************************
********BEGIN DAYS *****************
            IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                LMT.REF1    = FIELD(CUS.LIM,'.',1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    XX = CUS.ID:'.':'0000':CUS.LIM
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    XX = CUS.ID:'.':'000':CUS.LIM
                END
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD  = TODAY
                YDAYS   = ''
                DAYS    = 'C'
                IF AC.EXP GE AC.TOD THEN
                    CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                END

                IF DAYS GE 0 AND DAYS LE 1 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL14.P  += AMT
                    END ELSE
                        BAL14.C  += AMT
                    END
                END
                IF DAYS = 2  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL15.P  += AMT
                    END ELSE
                        BAL15.C  += AMT
                    END
                END
                IF DAYS = 3  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL16.P  += AMT
                    END ELSE
                        BAL16.C  += AMT
                    END
                END
                IF DAYS = 4  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL17.P  += AMT
                    END ELSE
                        BAL17.C  += AMT
                    END
                END
                IF DAYS = 5  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL18.P  += AMT
                    END ELSE
                        BAL18.C  += AMT
                    END
                END
                IF DAYS GT 5 AND DAYS LE 14  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL19.P  += AMT
                    END ELSE
                        BAL19.C  += AMT
                    END
                END
                IF DAYS GT 14 AND DAYS LE 21  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL20.P  += AMT
                    END ELSE
                        BAL20.C  += AMT
                    END
                END
                IF DAYS GT 21 AND DAYS LE 28  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL21.P  += AMT
                    END ELSE
                        BAL21.C  += AMT
                    END
                END
                IF DAYS GT 28 AND DAYS LE 35  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL22.P  += AMT
                    END ELSE
                        BAL22.C  += AMT
                    END
                END
                IF DAYS GT 35 AND DAYS LE 42  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL23.P  += AMT
                    END ELSE
                        BAL23.C  += AMT
                    END
                END
                IF DAYS GT 42 AND DAYS LE 49  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL24.P  += AMT
                    END ELSE
                        BAL24.C  += AMT
                    END
                END
                IF DAYS GT 49 AND DAYS LE 56  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL25.P  += AMT
                    END ELSE
                        BAL25.C  += AMT
                    END
                END
                IF DAYS GE 56 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL26.P  += AMT
                    END ELSE
                        BAL26.C  += AMT
                    END
                END
            END
        NEXT GG
    END

**TEXT = BAL27.P + BAL27.C ; CALL REM
**************CURRECNT AC CORPRATE AND PERSONAL
    T.SEL  = "SELECT ":FN.AC:" WITH  CATEGORY IN (1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 11242 1208 1211 1212 1216 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 11238 1407 1413 11240 11232 11239 1480 1481 1499 1408 1582 1483 1595 1493 1558 ) AND CURRENCY EQ ":CUR.ID:"  AND OPEN.ACTUAL.BAL GT 0 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR GGG = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<GGG>,R.AC,F.AC,E1)
            CATEG       = R.AC<AC.CATEGORY>
            AMT         = R.AC<AC.OPEN.ACTUAL.BAL>
            CUS.ID      = R.AC<AC.CUSTOMER>
            CUS.LIM     = R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
****** OVER NIGHT **************
            IF (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
                IF CUST.SECTOR EQ 4650 THEN
                    BAL29.P  += AMT
                END ELSE
                    BAL29.C  += AMT
                END
            END
*********BEGIN DAYS
            IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                LMT.REF1    = FIELD(CUS.LIM,'.',1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    XX = CUS.ID:'.':'0000':CUS.LIM
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    XX = CUS.ID:'.':'000':CUS.LIM
                END
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD  = TODAY
                YDAYS   = ''
                DAYS    = 'C'
                IF AC.EXP GE AC.TOD THEN
                    CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                END
                IF DAYS GE 0 AND DAYS LE 1 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL30.P  += AMT
                    END ELSE
                        BAL30.C  += AMT
                    END
                END
                IF DAYS = 2  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL31.P  += AMT
                    END ELSE
                        BAL31.C  += AMT
                    END
                END
                IF DAYS = 3  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL32.P  += AMT
                    END ELSE
                        BAL32.C  += AMT
                    END
                END
                IF DAYS = 4  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL33.P  += AMT
                    END ELSE
                        BAL33.C  += AMT
                    END
                END
                IF DAYS = 5  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL34.P  += AMT
                    END ELSE
                        BAL34.C  += AMT
                    END
                END
                IF DAYS GT 5 AND DAYS LE 14  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL35.P  += AMT
                    END ELSE
                        BAL35.C  += AMT
                    END
                END
                IF DAYS GT 14 AND DAYS LE 21  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL36.P  += AMT
                    END ELSE
                        BAL36.C  += AMT
                    END
                END
                IF DAYS GT 21 AND DAYS LE 28  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL37.P  += AMT
                    END ELSE
                        BAL37.C  += AMT
                    END
                END
                IF DAYS GT 28 AND DAYS LE 35  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL38.P  += AMT
                    END ELSE
                        BAL38.C  += AMT
                    END
                END
                IF DAYS GT 35 AND DAYS LE 42  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL39.P  += AMT
                    END ELSE
                        BAL39.C  += AMT
                    END
                END
                IF DAYS GT 42 AND DAYS LE 49  THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL39.P  += AMT
                    END ELSE
                        BAL39.C  += AMT
                    END
                END
                IF DAYS GT 49 AND DAYS LE 56 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL40.P  += AMT
                    END ELSE
                        BAL40.C  += AMT
                    END
                END
                IF DAYS GT 56 THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL41.P  += AMT
                    END ELSE
                        BAL41.C  += AMT
                    END
                END
            END
        NEXT GGG
    END

***********BEGIN OTHER
    T.SEL  = "SELECT ":FN.AC:" WITH  CATEGORY IN (16151 16153 16188 16152 11380 16175 3011 3012 3013 3014 3017 3010 3005) AND CURRENCY EQ ":CUR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR GGGG = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<GGGG>,R.AC,F.AC,E1)
            CATEG       = R.AC<AC.CATEGORY>
            AMT         = R.AC<AC.OPEN.ACTUAL.BAL>
            CUS.ID      = R.AC<AC.CUSTOMER>
            CUS.LIM     = R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
            CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
****** OVER NIGHT **************
            IF (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
* IF CUST.SECTOR EQ 4650 THEN
                BAL42.P  += AMT
*END ELSE
*    BAL42.C  += AMT
* END
            END
*********BEGIN DAYS
            IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                LMT.REF1    = FIELD(CUS.LIM,'.',1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    XX = CUS.ID:'.':'0000':CUS.LIM
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    XX = CUS.ID:'.':'000':CUS.LIM
                END
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD  = TODAY
                YDAYS   = ''
                DAYS    = 'C'
                IF AC.EXP GE AC.TOD THEN
                    CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                END
                IF DAYS GE 0 AND DAYS LE 1 THEN
*  IF CUST.SECTOR EQ 4650 THEN
                    BAL43.P  += AMT
*  END ELSE
*      BAL43.C  += AMT
*  END
                END
                IF DAYS = 2  THEN
* IF CUST.SECTOR EQ 4650 THEN
                    BAL44.P  += AMT
* END ELSE
*     BAL44.C  += AMT
* END
                END
                IF DAYS = 3  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL45.P  += AMT
*END ELSE
*   BAL45.C  += AMT
* END
                END
                IF DAYS = 4  THEN
* IF CUST.SECTOR EQ 4650 THEN
                    BAL46.P  += AMT
* END ELSE
*     BAL46.C  += AMT
* END
                END
                IF DAYS = 5  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL47.P  += AMT
*END ELSE
*    BAL47.C  += AMT
*END
                END
                IF DAYS GT 5 AND DAYS LE 14  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL48.P  += AMT
*END ELSE
*    BAL48.C  += AMT
*END
                END
                IF DAYS GT 14 AND DAYS LE 21  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL49.P  += AMT
*END ELSE
*    BAL49.C  += AMT
*END
                END
                IF DAYS GT 21 AND DAYS LE 28  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL50.P  += AMT
*END ELSE
*    BAL50.C  += AMT
*END
                END
                IF DAYS GT 28 AND DAYS LE 35  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL51.P  += AMT
*END ELSE
*    BAL51.C  += AMT
*END
                END
                IF DAYS GT 35 AND DAYS LE 42  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL52.P  += AMT
*END ELSE
*    BAL52.C  += AMT
*END
                END
                IF DAYS GT 42 AND DAYS LE 49  THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL53.P  += AMT
*END ELSE
*    BAL53.C  += AMT
*END
                END
                IF DAYS GT 49 AND DAYS LE 56 THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL54.P  += AMT
*END ELSE
*    BAL54.C  += AMT
*END
                END
                IF DAYS GT 56 THEN
*IF CUST.SECTOR EQ 4650 THEN
                    BAL55.P  += AMT
*END ELSE
*    BAL55.C  += AMT
*END
                END
            END
        NEXT GGGG
    END

*************END DAYS*******************
**TEXT = BAL27.P ; CALL REM
**TEXT = BAL27.C ; CALL REM
**TEXT = BAL27.P + BAL27.C ; CALL REM
    BB.DATA  = 'PERSONAL DEPOSITS':","
    BB.DATA := BAL1.P  + BAL14.P + BAL27.P + BAL28.P :","
    BB.DATA := BAL2.P  + BAL15.P:","
    BB.DATA := BAL3.P  + BAL16.P:","
    BB.DATA := BAL4.P  + BAL17.P:","
    BB.DATA := BAL5.P  + BAL18.P:","
    BB.DATA := BAL6.P  + BAL19.P:","
    BB.DATA := BAL7.P  + BAL20.P:","
    BB.DATA := BAL8.P  + BAL21.P:","
    BB.DATA := BAL9.P  + BAL22.P:","
    BB.DATA := BAL10.P + BAL23.P:","
    BB.DATA := BAL11.P + BAL24.P:","
    BB.DATA := BAL12.P + BAL25.P:","
    BB.DATA := BAL13.P + BAL26.P:","
    BB.DATA := CUR.ID:","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = 'CORPORATE DEPOSITS':","
    BB.DATA := BAL1.C  + BAL14.C + BAL27.C + BAL28.C :","
    BB.DATA := BAL2.C  + BAL15.C:","
    BB.DATA := BAL3.C  + BAL16.C:","
    BB.DATA := BAL4.C  + BAL17.C:","
    BB.DATA := BAL5.C  + BAL18.C:","
    BB.DATA := BAL6.C  + BAL19.C:","
    BB.DATA := BAL7.C  + BAL20.C:","
    BB.DATA := BAL8.C  + BAL21.C:","
    BB.DATA := BAL9.C  + BAL22.C:","
    BB.DATA := BAL10.C + BAL23.C:","
    BB.DATA := BAL11.C + BAL24.C:","
    BB.DATA := BAL12.C + BAL25.C:","
    BB.DATA := BAL13.C + BAL26.C:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = 'CORPORATE CURRECNT.ACCOUNT':","
    BB.DATA := BAL29.C :","
    BB.DATA := BAL30.C:","
    BB.DATA := BAL31.C:","
    BB.DATA := BAL32.C:","
    BB.DATA := BAL33.C:","
    BB.DATA := BAL34.C:","
    BB.DATA := BAL35.C:","
    BB.DATA := BAL36.C:","
    BB.DATA := BAL37.C:","
    BB.DATA := BAL38.C:","
    BB.DATA := BAL39.C:","
    BB.DATA := BAL40.C:","
    BB.DATA := BAL41.C:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = 'PERSONAL CURRECNT.ACCOUNT':","
    BB.DATA := BAL29.P:","
    BB.DATA := BAL30.P:","
    BB.DATA := BAL31.P:","
    BB.DATA := BAL32.P:","
    BB.DATA := BAL33.P:","
    BB.DATA := BAL34.P:","
    BB.DATA := BAL35.P:","
    BB.DATA := BAL36.P:","
    BB.DATA := BAL37.P:","
    BB.DATA := BAL38.P:","
    BB.DATA := BAL39.P:","
    BB.DATA := BAL40.P:","
    BB.DATA := BAL41.P:","
    BB.DATA := CUR.ID:","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA  = 'OTHER':","
    BB.DATA := BAL42.P + BAL43.P:","
    BB.DATA := BAL44.P:","
    BB.DATA := BAL45.P:","
    BB.DATA := BAL46.P:","
    BB.DATA := BAL47.P:","
    BB.DATA := BAL48.P:","
    BB.DATA := BAL49.P:","
    BB.DATA := BAL50.P:","
    BB.DATA := BAL51.P:","
    BB.DATA := BAL52.P:","
    BB.DATA := BAL53.P:","
    BB.DATA := BAL54.P:","
    BB.DATA := BAL55.P:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA = '******':","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*********************************
    RETURN
END
