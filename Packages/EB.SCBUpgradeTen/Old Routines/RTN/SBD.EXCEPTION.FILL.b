* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.EXCEPTION.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EXCEPTION.LOG.FILE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.EXCEPTION.FILE

*********************** OPENING FILES *****************************
    FN.ORG  = "F.EXCEPTION.LOG.FILE"   ; F.ORG   = ""
    FN.CPY  = "F.SCB.EXCEPTION.FILE"  ; F.CPY   = ""

    CALL OPF (FN.ORG,F.ORG)
    CALL OPF (FN.CPY,F.CPY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
*-------------------------------------------------------------
    T.SEL = "SELECT ":FN.ORG
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ORG,KEY.LIST<I>,R.ORG,F.ORG,E1)
            CALL F.READ(FN.CPY,KEY.LIST<I>,R.CPY,F.CPY,E2)
            EXC.ID = KEY.LIST<I>
********************** COPY NEW DATA ******************************

            R.CPY<EXC.REC.KEY>   = R.ORG<EB.EXC.REC.KEY>
            R.CPY<EXC.COMP.CODE> = R.ORG<EB.EXC.COMP.CODE>
            R.CPY<EXC.RUN.DATE>  = R.ORG<EB.EXC.RUN.DATE>

            CALL F.WRITE(FN.CPY,EXC.ID,R.CPY)
           ** CALL  JOURNAL.UPDATE(EXC.ID)

        NEXT I
    END
END
