* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>332</Rating>
*-----------------------------------------------------------------------------
  SUBROUTINE SBD.VISA.PAY.NOT.SENT
*    PROGRAM SBD.VISA.PAY.NOT.SENT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS


    GOSUB INITIATE
    CURR.COMP = ID.COMPANY

    GOSUB PRINT.HEAD

*Line [ 49 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.VISA.PAY.NOT.SENT'
    CALL PRINTER.ON(REPORT.ID,'')

    RETURN
*=============================================================
CALLDB:

    FN.TE = 'F.TELLER'                     ; F.TE = ''    ;R.TE = ''
    FN.FT = 'F.FUNDS.TRANSFER'                 ; F.FT = ''    ;R.FT = ''
    BRANCH = ''
    PRV.BRAN = ''

    CALL OPF(FN.TE,F.TE)
    CALL OPF(FN.FT,F.FT)
    TOT.TR = 0
*   CURR.COMP = ID.COMPANY

    T.SEL = "SELECT FBNK.FUNDS.TRANSFER "
    KEY.LIST=""  ; SELECTED=""  ;  E1="" ; E2=''
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF KEY.LIST THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,E1)
            IF NOT(E1) THEN
*=================================================
                BRANCH = ''
                V.CAT = R.FT<FT.CREDIT.ACCT.NO>[11,4]
                TR.TYP= R.FT<FT.TRANSACTION.TYPE>
                IF ((V.CAT EQ 1205 OR V.CAT EQ 1206) AND TR.TYP NE 'ACVC') OR ((V.CAT EQ 1207 OR V.CAT EQ 1208) AND TR.TYP NE 'ACVM' ) THEN
                    TOT.TR = TOT.TR + 1
                    CUS.NAME = ''
                    BRANCH.NO = R.FT<FT.CREDIT.COMP.CODE>
                    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,R.FT<FT.CREDIT.COMP.CODE>,BRANCH)
                    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,R.FT<FT.CREDIT.ACCT.NO>,CUS.NAME)

                    YYBRN = FIELD(BRANCH,'.',1)

                    XX = SPACE(120)
                    XX<1,1>[1,10]    = BRANCH
                    XX<1,1>[15,16]    = KEY.LIST<I>
                    XX<1,1>[35,15]   = R.FT<FT.CREDIT.ACCT.NO>
                    XX<1,1>[60,10]   =  R.FT<FT.DEBIT.AMOUNT>
                    XX<1,1>[75,5]   =  R.FT<FT.TRANSACTION.TYPE>
                    XX<1,1>[85,20]   =  CUS.NAME
                    PRINT XX<1,1>
                END
            END
        NEXT I
*======================= TELLER ========================
        T.SEL = "SELECT FBNK.TELLER "
        KEY.LIST=""  ; SELECTED=""  ;  E1="" ; E2=''
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF KEY.LIST THEN
            FOR I = 1 TO SELECTED
                CALL F.READ(FN.TE,KEY.LIST<I>,R.TE,F.TE,E1)
                IF NOT(E1) THEN
                    BRANCH = ''
                    V.CAT = R.TE<TT.TE.ACCOUNT.1>[11,4]
                    TR.TYP= R.TE<TT.TE.TRANSACTION.CODE>
                    IF ((V.CAT EQ 1205 OR V.CAT EQ 1206) AND TR.TYP NE '37') OR ((V.CAT EQ 1207 OR V.CAT EQ 1208) AND TR.TYP NE '38' ) THEN
                        TOT.TR = TOT.TR + 1
                        CUS.NAME = ''
*     BRANCH.NO = R.FT<FT.CREDIT.COMP.CODE>
*     CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,R.FT<FT.CREDIT.COMP.CODE>,BRANCH)
                        CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,R.TE<TT.TE.ACCOUNT.1>,CUS.NAME)

*     YYBRN = FIELD(BRANCH,'.',1)

                        XX = SPACE(120)
                        XX<1,1>[1,10]    = BRANCH
                        XX<1,1>[15,16]    = KEY.LIST<I>
                        XX<1,1>[35,15]   = R.TE<TT.TE.ACCOUNT.1>
                        XX<1,1>[60,10]   =  R.TE<TT.TE.AMOUNT.LOCAL.1>
                        XX<1,1>[75,5]   =  R.TE<TT.TE.TRANSACTION.CODE>
                        XX<1,1>[85,20]   =  CUS.NAME
                        PRINT XX<1,1>
                    END
                END
            NEXT I
        END
*==============================================================
            PRINT STR('=',125)
            PRINT ; PRINT SPACE(5):" ��� �������  ":TOT.TR
            IF TOT.TR = 0 THEN
                PRINT SPACE(5):" �� ��������� ���������������� "
            END
            PRINT STR('=',125)
        END ELSE
            ENQ.ERROR = "NO RECORDS FOUND"
        END

        RETURN
*===============================================================
PRINT.HEAD:
        R.USR = ''
        CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USR<EB.USE.DEPARTMENT.CODE>,BRANCH)
        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CURR.COMP,BRANCH)
        YYBRN = FIELD(BRANCH,'.',1)
        YYBRN = BRANCH
        DATY = TODAY
        T.DAY = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]
        PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
*    PR.HD ="'L'":SPACE(1):"Suez Canal Bank "  : SPACE(90):"Branch:" :YYBRN
        PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
*    PR.HD :="'L'":SPACE(1):"Date    : ":T.DAY:SPACE(85):"Page No.   : ":"'P'"
        PR.HD :="'L'":SPACE(1):"SBD.VISA.PAY.NOT.SENT"
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(50):" ���� �������� ���� ��� ��� ������ ��������  "
        PR.HD :="'L'":SPACE(50):" ��� ���� ��� �������� ���   ":T.DAY
*    PR.HD :="'L'":SPACE(50): " �� ������ " :ST.DATE :" ��� " : EN.DATE
        PR.HD :="'L'":SPACE(40):STR('_',55)
        PR.HD :="'L'":" "
        PR.HD :="'L'":" "
        PR.HD :="'L'":SPACE(2):" �����" :SPACE(8):"���� ������ ":SPACE(13):"������":SPACE(12):"������":SPACE(8):"�.������"
        PR.HD :="'L'":STR('_',128)
**X    PRINT
        HEADING PR.HD
        RETURN
*==============================================================
