* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*----------------------------------
*------ CREATE BY NESSMA
*----------------------------------
    SUBROUTINE RTN.TRANS.PREPAID
*    PROGRAM RTN.TRANS.PREPAID
*----------------------------------
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TERMINAL.ID
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ATM.TRANS.DAILY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE JBC.h
*---------------------------------------------
    DIR.NAME.2 = "PREPAID/PREPAID.NEW"

    GOSUB OPF.FILES
    GOSUB CHECK.SCB.ATM

    REN.FILE ="cp -R ":DIR.NAME.2:"/":NEW.FILE:" ":FN.OFS.IN:"/":NEW.FILE
    EXECUTE REN.FILE
    RETURN
*---------------------------------------------
OPF.FILES:
*---------
    FN.ATM  = "F.SCB.ATM.TERMINAL.ID" ; F.ATM  = ""
    CALL OPF(FN.ATM, F.ATM)

    FN.SATM = "F.SCB.ATM.TRANS.DAILY" ; F.SATM = ""
    CALL OPF(FN.SATM, F.SATM)
    RETURN
*---------------------------------------------------
CHECK.SCB.ATM:
*-------------
    TOD = TODAY
    MON = TOD[5,2] + 1 - 1
    TOD = TOD[7,2]:"-":MON:"-":TOD[1,4]
    FILE.ID   = "PREPAID.":TOD:".csv"
    DR.ACCOUNT = "" ; CR.ACCOUNT = ""
    CALL F.READ(FN.SATM,FILE.ID,R.SATM,F.SATM,ER.SATM)
*** FOR TESTING ONLY ER.SATM =  1
    IF ER.SATM THEN
        R.SATM<SCB.ATM.PROCESSING.DATE>  =  TODAY

        GOSUB CREATE.OFS
        GOSUB INITIAL

        WRITE R.SATM TO F.SATM , FILE.ID  ON ERROR
            STOP 'CAN NOT WRITE RECORD ':FILE.ID:' TO FILE ':F.SATM
        END
        TEXT = "�� �������" ; CALL REM
    END ELSE
        TEXT = "�� ������� ���� ����� �� ���" ; CALL REM
    END
    RETURN
*---------------------------------------------------
CREATE.OFS:
*----------
    FN.OFS.SOURCE = "F.OFS.SOURCE"  ;  F.OFS.SOURCE = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    F.OFS.IN         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "PREPAID"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    DIR.NAME = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    NEW.FILE = "PREPAID-TRANS-":TODAY:".":RND(10000)

    OPENSEQ DIR.NAME.2,NEW.FILE TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ': DIR.NAME.2:' ':NEW.FILE
        HUSH OFF
        PRINT 'FILE ':NEW.FILE:' DELETE FROM ':DIR.NAME.2
    END

    OPENSEQ DIR.NAME.2, NEW.FILE TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE:' CREATED IN ':DIR.NAME.2
        END ELSE
            STOP 'Cannot create ':NEW.FILE:' to ':DIR.NAME.2
        END
    END
    RETURN
*-------------------------------------------------------------------
INITIAL:
*-------
    BB     = ""
    EOF    = ''
    R.LINE = ""
    ERR='' ; ER=''

    PROF.CENT.DPT = "99"

    DIR.NAMEE  = "PREPAID"
    NN.SEL = "SELECT PREPAID WITH @ID LIKE ...NodeReport-ALL.csv"
    CALL EB.READLIST(NN.SEL,N.LIST,"",SELECTED,ER.MSG.N)
    IF SELECTED THEN
        FOR XX = 1 TO SELECTED
            FILE.NAME = N.LIST<XX> ; EOF = ""
            OPENSEQ DIR.NAMEE,FILE.NAME TO BB ELSE
                TEXT = "ERROR OPEN FILE ":FILE.NAME ; CALL REM
                RETURN
            END
            IF IOCTL(BB, JIOCTL_COMMAND_SEQ_CHANGE_RECORDSIZE,"3000") ELSE STOP

            POOL.ACC = "EGP1670000010099"
            HH = 1
            LOOP WHILE NOT(EOF)
                READSEQ R.LINE FROM BB THEN
                    IF HH GE 2 THEN
                        SET.DATE    = FIELD(R.LINE, "," , 1)
                        TRANS.CLASS = FIELD(R.LINE, "," , 2)
                        TRANS.TYPE  = FIELD(R.LINE, "," , 3)
                        TERM.ID     = FIELD(R.LINE, "," , 4)
                        ACTION      = FIELD(R.LINE, "," , 5)
                        COUNT.F     = FIELD(R.LINE, "," , 6)
                        CRD.HLD.AMT = FIELD(R.LINE, "," , 7)
                        CRD.HLD.CUR = FIELD(R.LINE, "," , 8)
                        EQUAV.AMT   = FIELD(R.LINE, "," , 9)
                        EQUAV.CUR   = FIELD(R.LINE, "," , 10)
                        LEDG.AC.NUM = FIELD(R.LINE, "," , 11)
                        LEDG.AC.CUR = FIELD(R.LINE, "," , 12)

                        GOSUB PROCESS
                    END
                    HH++
                END ELSE
                    EOF = 1
                END

            REPEAT
            CLOSESEQ BB

            EXECUTE 'rm PREPAID/':FILE.NAME
        NEXT XX
    END
    RETURN
*------------------------------------------------------------------
PROCESS:
*-------
    ACCOUNT.NUMBER = ""
    GOSUB GET.ACC.NUM

    IF ACTION EQ 'debit' THEN
        DR.ACCOUNT = POOL.ACC
        CR.ACCOUNT = ACCOUNT.NUMBER
    END
    IF ACTION EQ 'credit' THEN
        DR.ACCOUNT = ACCOUNT.NUMBER
        CR.ACCOUNT = POOL.ACC
***EQUAV.AMT  = EQUAV.AMT * -1
        CRD.HLD.AMT = CRD.HLD.AMT * -1
    END

    BRANCH.CODE    = "EG0010099"
    COMP.CODE      = BRANCH.CODE[8,2]
    OFS.USER.INFO  = "INPUTT" : COMP.CODE : "//" : BRANCH.CODE

    TRNS.TYPE      = "ACNN"
    DR.CUR         = "EGP"
    CR.CUR         = "EGP"
    DTR            = TERM.ID
    CTR            = TERM.ID
***    IF EQUAV.AMT EQ 0 OR EQUAV.AMT EQ '' THEN
    IF CRD.HLD.AMT EQ 0 OR CRD.HLD.AMT EQ '' THEN
    END ELSE
        GOSUB BUILD.RECORD
    END
    RETURN
*------------------------------------------------------------------
GET.ACC.NUM:
*-----------
*----- ATM Onus --------------------------------------------------
    IF TRANS.CLASS EQ 'ATM Onus' THEN
        CALL F.READ(FN.ATM,TERM.ID,R.ATM,F.ATM,ER.ATM)
        IF NOT(ER.ATM) THEN
            ACCOUNT.NUMBER = R.ATM<SCB.ATM.ACCOUNT.NUMBER>
        END
    END
*----- ATM 123 -----------------------------------------------------
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Balance' THEN
        ACCOUNT.NUMBER = ""
    END
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Balance Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "EGP1670100010099"
    END
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Withdrawl Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Withdrawal Full Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670100010099"
    END
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Withdrawal Fees Reversal' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM 123' AND TRANS.TYPE EQ 'Withdrawal Partial Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670100010099"
    END
*------ ATM MDS ISIS
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Balance' THEN
        ACCOUNT.NUMBER = ""
    END
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Balance Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "EGP1670200010099"
    END
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Withdrawl Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Withdrawal Full Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670200010099"
    END
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Withdrawal Fees Reversal' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM MDS ISIS' AND TRANS.TYPE EQ 'Withdrawal Partial Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670200010099"
    END
*----- ATM MDS Int ------------------------------------------------
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Balance' THEN
        ACCOUNT.NUMBER = ""
    END
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Balance Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Withdrawl Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Withdrawal Full Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Withdrawal Fees Reversal' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM MDS Int' AND TRANS.TYPE EQ 'Withdrawal Partial Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
*------ ATM 19123 --------------------------------------------------
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Balance' THEN
        ACCOUNT.NUMBER = ""
    END
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Balance Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "EGP1670100010099"
    END
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Withdrawl Fees' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Withdrawal Full Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670100010099"
    END
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Withdrawal Fees Reversal' THEN
        ACCOUNT.NUMBER = "PL57059"
    END
    IF TRANS.CLASS EQ 'ATM 19123' AND TRANS.TYPE EQ 'Withdrawal Partial Reversal' THEN
        ACCOUNT.NUMBER = "EGP1670100010099"
    END
*------ POS Local ----------------------------------------------------
    IF TRANS.CLASS EQ 'POS Local' AND TRANS.TYPE EQ 'Purchase' THEN
        ACCOUNT.NUMBER = "EGP1670300010099"
    END
    IF TRANS.CLASS EQ 'POS Local' AND TRANS.TYPE EQ 'Internet Purchase' THEN
        ACCOUNT.NUMBER = "EGP1670300010099"
    END
    IF TRANS.CLASS EQ 'POS Local' AND TRANS.TYPE EQ 'Purchase Return' THEN
        ACCOUNT.NUMBER = "EGP1670300010099"
    END
    IF TRANS.CLASS EQ 'POS Local' AND TRANS.TYPE EQ 'Cash Disbursement' THEN
        ACCOUNT.NUMBER = "EGP1670300010099"
    END
    IF TRANS.CLASS EQ 'POS Local' AND TRANS.TYPE EQ 'Purchase With Cash Back' THEN
        ACCOUNT.NUMBER = "EGP1670300010099"
    END
*------ POS Int ------------------------------------------------------
    IF TRANS.CLASS EQ 'POS Int' AND TRANS.TYPE EQ 'Purchase' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
    IF TRANS.CLASS EQ 'POS Int' AND TRANS.TYPE EQ 'Internet Purchase' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
    IF TRANS.CLASS EQ 'POS Int' AND TRANS.TYPE EQ 'Purchase Return' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
    IF TRANS.CLASS EQ 'POS Int' AND TRANS.TYPE EQ 'Cash Disbursement' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
    IF TRANS.CLASS EQ 'POS Int' AND TRANS.TYPE EQ 'Purchase With Cash Back' THEN
        ACCOUNT.NUMBER = "EGP1670400010099"
    END
*------ CI Joining ----------------------------------------------
    IF TRANS.CLASS EQ 'CI Joining Fees' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "PL57058"
    END
    IF TRANS.CLASS EQ 'CI Joining Fees Reversal' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "PL57058"
    END
*------ CI Reissue Card Fees -----------------------------------------
    IF TRANS.CLASS EQ 'CI Reissue Card Fees' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "PL57058"
    END
    IF TRANS.CLASS EQ 'CI Reissue Card Fees Reversal' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "PL57058"
    END
*------ CI Reissue PIN Fees ------------------------------------------
    IF TRANS.CLASS EQ 'CI Reissue PIN Fees' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "PL57058"
    END
    IF TRANS.CLASS EQ 'CI Reissue PIN Fees Reversal' AND TRANS.TYPE EQ 'Withdrawl' THEN
        ACCOUNT.NUMBER = "PL57058"
    END
    RETURN
*------------------------------------------------------------------
BUILD.RECORD:
*------------
    COMMA = ","

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE="    :TRNS.TYPE:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO="       :DR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY="      :DR.CUR:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF="     :DTR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF="    :CTR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO="      :CR.ACCOUNT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY="     :CR.CUR:COMMA
***    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT="        :EQUAV.AMT:COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT="        :CRD.HLD.AMT:COMMA
    OFS.MESSAGE.DATA :=  "ORDERING.BANK="       :"SCB":COMMA
    OFS.MESSAGE.DATA :=  "PROCESSING.DATE="     :TODAY:COMMA
    OFS.MESSAGE.DATA :=  "PROFIT.CENTRE.DEPT="  :PROF.CENT.DPT:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:32:1="      :TERM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:33:1="      :TERM.ID:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:47:1="      :CRD.HLD.AMT:COMMA
    OFS.MESSAGE.DATA :=  "LOCAL.REF:52:1="      :CRD.HLD.CUR:COMMA

    ZZZ   = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA

    WRITESEQ ZZZ TO V.FILE.IN ELSE
        PRINT  'CAN NOT WRITE LINE ':ZZZ
    END

    RETURN
*------------------------------------------------------------------
END
