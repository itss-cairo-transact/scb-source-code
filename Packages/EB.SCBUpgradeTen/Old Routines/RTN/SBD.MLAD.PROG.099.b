* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-72</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.MLAD.PROG.099
***    PROGRAM    SBD.MLAD.PROG.099

*   ------------------------------
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.020
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON



    GOSUB INIT

    IF PROGRAM.ID EQ "SBD.MLAD.PROG.099"  THEN
        GOSUB OPENFILES
*-------------------------------
        GOSUB READ.MLAD.FILE.020
*-------------------------------
    END
*-----------------------------------------------------------------------------


    RETURN
*-------------------------------------------------------------------------
INIT:

    DIM I.KEY(50)
    DIM I.DAY(50)
    DIM I.WEEK(50)
    DIM I.MONTH(50)
    DIM I.3MONTH(50)
    DIM I.6MONTH(50)
    DIM I.YEAR(50)
    DIM I.3YEAR(50)
    DIM I.MORE(50)

    FOR I = 1 TO 50
        I.KEY(I)    = ''
        I.DAY(I)    = 0
        I.WEEK(I)   = 0
        I.MONTH(I)  = 0
        I.3MONTH(I) = 0
        I.6MONTH(I) = 0
        I.YEAR(I)   = 0
        I.3YEAR(I)  = 0
        I.MORE(I)   = 0
    NEXT I

    PROGRAM.ID = "SBD.MLAD.PROG.099"


    FN.MLAD.020 = "F.SCB.MLAD.FILE.020"
    F.MLAD.020  = ""
    R.MLAD.020  = ""
    Y.MLAD.020.ID   = ""

*----------------------------------
    FN.CURRENCY = 'FBNK.CURRENCY'
    F.CURRENCY  = ''
    R.CURRENCY = ''
    Y.CURRENCY.ID = ''
    CALL OPF(FN.CURRENCY,F.CURRENCY)



*---------------------------------------
    SYS.DATE = TODAY
    SYS.YYMM = SYS.DATE[1,6]

    WRK.DATE = SYS.DATE
    P.DATE   = FMT(SYS.DATE,"####/##/##")

*---------------------------------------
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]
*---------------------------------------
    LINE.NO = 0
    MAX.LINE.NO = 30
*-----------------------


    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLAD.020,F.MLAD.020)

    RETURN
*--------------------------------------------------------------------------
READ.MLAD.FILE.020:
    GOSUB MOVE.ZEROS

    SEL.MLAD.020  = "SELECT ":FN.MLAD.020
    SEL.MLAD.020 := " WITH PRINTABLE.CODE EQ 'N'"
****    SEL.MLAD.020 := " AND CURRENCY EQ 'SEK'"
    SEL.MLAD.020 :=" BY @ID"



    CALL EB.READLIST(SEL.MLAD.020,SEL.LIST.MLAD.020,'',NO.OF.MLAD.020,ERR.MLAD.020)
    LOOP
        REMOVE Y.MLAD.020.ID FROM SEL.LIST.MLAD.020 SETTING POS.MLAD.020
    WHILE Y.MLAD.020.ID:POS.MLAD.020
        CALL F.READ(FN.MLAD.020,Y.MLAD.020.ID,R.MLAD.020,F.MLAD.020,ERR.MLAD.020)


        FILE.CY = FIELD(Y.MLAD.020.ID,'-',1,1)
        FILE.AS = FIELD(Y.MLAD.020.ID,'-',2,1)
        FILE.SR = FIELD(Y.MLAD.020.ID,'-',3,1)
        FILE.KY = FILE.AS:FILE.SR

        N.RATE   = R.MLAD.020<ML020.RATE>

        N.DAY    = R.MLAD.020<ML020.NEXT.DAY>
        N.WEEK   = R.MLAD.020<ML020.NEXT.WEEK>
        N.MONTH  = R.MLAD.020<ML020.NEXT.MONTH>
        N.3MONTH = R.MLAD.020<ML020.NEXT.3MONTH>
        N.6MONTH = R.MLAD.020<ML020.NEXT.6MONTH>
        N.YEAR   = R.MLAD.020<ML020.NEXT.YEAR>
        N.3YEAR  = R.MLAD.020<ML020.NEXT.3YEAR>
        N.MORE   = R.MLAD.020<ML020.NEXT.MORE>
* -------------------------------------
        X.DAY    = (N.DAY      * N.RATE)
        X.WEEK   = (N.WEEK     * N.RATE)
        X.MONTH  = (N.MONTH    * N.RATE)
        X.3MONTH = (N.3MONTH   * N.RATE)
        X.6MONTH = (N.6MONTH   * N.RATE)
        X.YEAR   = (N.YEAR     * N.RATE)
        X.3YEAR  = (N.3YEAR    * N.RATE)
        X.MORE   = (N.MORE     * N.RATE)
*---------------
        GOSUB SEARCH.IN.ARRAY
*---------------
    REPEAT


*---------------

    FOR I = 1 TO 50
        BEGIN CASE

        CASE I.KEY(I) NE ''
            Y.MLAD.020.ID = "ZZZ-":I.KEY(I)[1,1]:"-":I.KEY(I)[2,3]

            GOSUB WRITE.ONE.RECORD:

        END CASE

    NEXT I
*---------------
    RETURN
*--------------------------------------------------------------------------
MOVE.ZEROS:
    X.DAY    = 0
    X.WEEK   = 0
    X.MONTH  = 0
    X.3MONTH = 0
    X.6MONTH = 0
    X.YEAR   = 0
    X.3YEAR  = 0
    X.MORE   = 0

    RETURN
*--------------------------------------------------------------------------
SEARCH.IN.ARRAY:

    FOR I = 1 TO 50
        IF I.KEY(I) EQ '' THEN
            I.KEY(I)    = FILE.KY
            I.DAY(I)    = X.DAY
            I.WEEK(I)   = X.WEEK
            I.MONTH(I)  = X.MONTH
            I.3MONTH(I) = X.3MONTH
            I.6MONTH(I) = X.6MONTH
            I.YEAR(I)   = X.YEAR
            I.3YEAR(I)  = X.3YEAR
            I.MORE(I)   = X.MORE
            I = 50
        END

        IF I.KEY(I) EQ FILE.KY  THEN
            I.DAY(I)    += X.DAY
            I.WEEK(I)   += X.WEEK
            I.MONTH(I)  += X.MONTH
            I.3MONTH(I) += X.3MONTH
            I.6MONTH(I) += X.6MONTH
            I.YEAR(I)   += X.YEAR
            I.3YEAR(I)  += X.3YEAR
            I.MORE(I)   += X.MORE
            I = 50
        END


    NEXT I

    RETURN
*--------------------------------------------------------------------------
WRITE.ONE.RECORD:


    CALL F.READ(FN.MLAD.020,Y.MLAD.020.ID,R.MLAD.020,F.MLAD.020,ERR.MLAD.020)
*   -------------------
    IF ERR.MLAD.020 THEN
        RETURN
    END ELSE
*   -------------------

        X.RATE   = R.MLAD.020<ML020.RATE>


        R.MLAD.020<ML020.NEXT.DAY>     += (X.RATE     * I.DAY(I))
        R.MLAD.020<ML020.NEXT.WEEK>    += (X.RATE     * I.WEEK(I))
        R.MLAD.020<ML020.NEXT.MONTH>   += (X.RATE     * I.MONTH(I))
        R.MLAD.020<ML020.NEXT.3MONTH>  += (X.RATE     * I.3MONTH(I))
        R.MLAD.020<ML020.NEXT.6MONTH>  += (X.RATE     * I.6MONTH(I))
        R.MLAD.020<ML020.NEXT.YEAR>    += (X.RATE     * I.YEAR(I))
        R.MLAD.020<ML020.NEXT.3YEAR>   += (X.RATE     * I.3YEAR(I))
        R.MLAD.020<ML020.NEXT.MORE>    += (X.RATE     * I.MORE(I))


*------------------------

        R.MLAD.020<ML020.RECORD.STATUS>  = 'Y'

        R.MLAD.020<ML020.RESERVED1> = Y.MLAD.020.ID[5,5]
        CALL F.WRITE(FN.MLAD.020,Y.MLAD.020.ID,R.MLAD.020)
        CALL JOURNAL.UPDATE(Y.MLAD.020.ID)

    END

    RETURN
*--------------------------------------------------------------------------

END
