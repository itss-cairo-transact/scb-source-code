* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-149</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.MLAD.PROG.030
***    PROGRAM SBD.MLAD.PROG.030

***                            ����� / ���� �����

*   ------------------------------
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.010
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.020
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.HIST
*-------------------------------------------------------------------------

    GOSUB INIT


    GOSUB OPENFILES

*-------------------------------

    GOSUB READ.MLAD.FILE.010


*-------------------------------

    RETURN
*-------------------------------------------------------------------------
INIT:


    PROGRAM.ID = "SBD.MLAD.PROG.030"


    FN.MLAD010 = "F.SCB.MLAD.FILE.010"
    F.MLAD010  = ""
    R.MLAD010  = ""
    Y.MLAD010.ID  = ""

    FN.MLAD020 = "F.SCB.MLAD.FILE.020"
    F.MLAD020  = ""
    R.MLAD020  = ""
    Y.MLAD020.ID  = ""

    FN.MLADHIS = "F.SCB.MLAD.HIST"
    F.MLADHIS  = ""
    R.MLADHIS  = ""
    Y.MLADHIS.ID  = ""
    KEY.LISTHIST  = ""


*---------------------------------------

    WS.VALUE = ''


    RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLAD010,F.MLAD010)
    CALL OPF(FN.MLAD020,F.MLAD020)
    CALL OPF(FN.MLADHIS,F.MLADHIS)


    RETURN
*--------------------------------------------------------------------------
READ.MLAD.FILE.010:


    SEL.ML010  = "SELECT ":FN.MLAD010:" BY @ID"


    CALL EB.READLIST(SEL.ML010,SEL.LIST,'',NO.ML010,ERR.ML010)


    LOOP
        REMOVE Y.MLAD010.ID FROM SEL.LIST SETTING POS.ML010
    WHILE Y.MLAD010.ID:POS.ML010
        CALL F.READ(FN.MLAD010,Y.MLAD010.ID,R.MLAD010,F.MLAD010,ERR.ML010)



        WS.CY        = R.MLAD010<ML010.CURRENCY>
        WS.PERIOD    = R.MLAD010<ML010.PERIOD>

*===================================================================================================        ���� - ���� ���������
* ------------------------------------------------------------------------------                   ����� ������ ��� ����� �������
        WS.FLD0010   = R.MLAD010<ML010.FLD0010.AMT>
        WS.FLD0020   = R.MLAD010<ML010.FLD0020.AMT>
        WS.FLD0080   = R.MLAD010<ML010.FLD0080.AMT>
        WS.FLD0090   = R.MLAD010<ML010.FLD0090.AMT>
        WS.FLD0100   = R.MLAD010<ML010.FLD0100.AMT>

        WS.VALUE  = WS.FLD0010 + WS.FLD0020 + WS.FLD0080 + WS.FLD0090 + WS.FLD0100

*        WS.VALUE   = R.MLAD010<ML010.FLD0080.AMT>

        Y.MLAD020.ID = WS.CY:"-X-030"
        GOSUB WRITE.MLAD.FILE.020
* ------------------------------------------------------------------------------                         ����� ��� ������ �������
        WS.FLD0040   = R.MLAD010<ML010.FLD0040.AMT>
        WS.FLD0060   = R.MLAD010<ML010.FLD0060.AMT>
        WS.VALUE  = WS.FLD0040 + WS.FLD0060
        Y.MLAD020.ID = WS.CY:"-X-040"
        GOSUB WRITE.MLAD.FILE.020

* ------------------------------------------------------------------------------                        ����� ��� ������ ��������
*
        WS.FLD0030   = R.MLAD010<ML010.FLD0030.AMT>
        WS.FLD0070   = R.MLAD010<ML010.FLD0070.AMT>
        WS.VALUE  = WS.FLD0030 + WS.FLD0070
        Y.MLAD020.ID = WS.CY:"-X-050"
        GOSUB WRITE.MLAD.FILE.020

* ------------------------------------------------------------------------------                                  �������
*                                             �� ��� ��� ������ ����� ��� ������� ������� ���� ���
*                                                               ��� �������� 2011/11/15
*        WS.FLD0310   = R.MLAD010<ML010.FLD0310.AMT>
*        WS.VALUE  = WS.FLD0310
*        Y.MLAD020.ID = WS.CY:"-X-070"
*        GOSUB WRITE.MLAD.FILE.020
* ------------------------------------------------------------------------------                                  ���� �������
        WS.FLD0300   = R.MLAD010<ML010.FLD0300.AMT>

        WS.VALUE  = WS.FLD0300
        Y.MLAD020.ID = WS.CY:"-X-060"

        GOSUB WRITE.MLAD.FILE.020


* ------------------------------------------------------------------------------                                  ������
        WS.FLD0320   = R.MLAD010<ML010.FLD0320.AMT>
        WS.FLD0330   = R.MLAD010<ML010.FLD0330.AMT>
        WS.FLD0340   = R.MLAD010<ML010.FLD0340.AMT>
        WS.FLD0360   = R.MLAD010<ML010.FLD0360.AMT>
        WS.FLD0370   = R.MLAD010<ML010.FLD0370.AMT>

        WS.VALUE  = WS.FLD0320 + WS.FLD0330 + WS.FLD0340 + WS.FLD0360 + WS.FLD0370
        Y.MLAD020.ID = WS.CY:"-X-090"

        GOSUB WRITE.MLAD.FILE.020




*=================================================================================================             ����� - ���� ����������

* ------------------------------------------------------------------------------                         ����� ������ ������ �������
        WS.FLD0250   = R.MLAD010<ML010.FLD0250.AMT>
        WS.FLD0260   = R.MLAD010<ML010.FLD0260.AMT>
        WS.VALUE  =  WS.FLD0250 + WS.FLD0260
        Y.MLAD020.ID = WS.CY:"-Y-030"
        GOSUB WRITE.MLAD.FILE.020

* ------------------------------------------------------------------------------                         ����� ������ ������ ��������
        WS.FLD0270   = R.MLAD010<ML010.FLD0270.AMT>
        WS.VALUE  =  WS.FLD0270
        Y.MLAD020.ID = WS.CY:"-Y-040"
        GOSUB WRITE.MLAD.FILE.020

* ------------------------------------------------------------------------------                        ����� ����� ������� ���������
        WS.VALUE = R.MLAD010<ML010.CERT.AMT> + R.MLAD010<ML010.DEPOSIT.AMT>
        Y.MLAD020.ID = WS.CY:"-Y-050"

        GOSUB WRITE.MLAD.FILE.020


*------------------------------------








*   --------------------------------------
    REPEAT

    RETURN
*--------------------------------------------------------------------------
WRITE.MLAD.FILE.020:

*-----------------------
    IF WS.VALUE EQ 0 THEN
        RETURN
    END
*-----------------------



    CALL F.READ(FN.MLAD020,Y.MLAD020.ID,R.MLAD020,F.MLAD020,ERR.MLAD020)


***     ----------------------------------------------------

    BEGIN CASE

    CASE WS.PERIOD EQ "DAY"
        R.MLAD020<ML020.NEXT.DAY>      += WS.VALUE

    CASE WS.PERIOD EQ "WEEK"
        R.MLAD020<ML020.NEXT.WEEK>     += WS.VALUE

    CASE WS.PERIOD EQ "MONTH"
        R.MLAD020<ML020.NEXT.MONTH>    += WS.VALUE

    CASE WS.PERIOD EQ "3MONTH"
        R.MLAD020<ML020.NEXT.3MONTH>   += WS.VALUE

    CASE WS.PERIOD EQ "6MONTH"
        R.MLAD020<ML020.NEXT.6MONTH>   += WS.VALUE

    CASE WS.PERIOD EQ "YEAR"
        R.MLAD020<ML020.NEXT.YEAR>     += WS.VALUE

    CASE WS.PERIOD EQ "3YEAR"
        R.MLAD020<ML020.NEXT.3YEAR>    += WS.VALUE

    CASE WS.PERIOD EQ "MORE"
        R.MLAD020<ML020.NEXT.MORE>     += WS.VALUE



    END CASE



*------------------------

    R.MLAD020<ML020.RECORD.STATUS>  = 'Y'


    R.MLAD020<ML020.RESERVED1> = Y.MLAD020.ID[5,5]

    CALL F.WRITE(FN.MLAD020,Y.MLAD020.ID,R.MLAD020)
    CALL JOURNAL.UPDATE(Y.MLAD020.ID)



    RETURN
*===============================================================
END
