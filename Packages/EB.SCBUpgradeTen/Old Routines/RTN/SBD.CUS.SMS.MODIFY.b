* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----MAI SAAD 17 OCT 2018-----------------
* <Rating>1623</Rating>
*----------------------
    SUBROUTINE  SBD.CUS.SMS.MODIFY

*  PROGRAM  SBD.CUS.SMS.MODIFY

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIALISE
    RETURN

INITIALISE:
*----------------------------------------------------------------------*
    FN.CUSTOMER  = 'FBNK.CUSTOMER' ; F.CUSTOMER = ''
    CALL OPF(FN.CUSTOMER ,  F.CUSTOMER)
****************************************************
    DIR.NAME2 = '&SAVEDLISTS&'
    NEW.FILE2 = "CUSTOMER.ERR.":TODAY

    OPENSEQ DIR.NAME2,NEW.FILE2 TO V.FILE.IN THEN
        CLOSESEQ V.FILE.IN
        HUSH ON
        EXECUTE 'DELETE ':DIR.NAME2:' ':NEW.FILE2
        HUSH OFF
        PRINT 'FILE ':NEW.FILE2:' DELETE FROM ':DIR.NAME2
    END
    OPENSEQ DIR.NAME2, NEW.FILE2 TO V.FILE.IN ELSE
        CREATE V.FILE.IN THEN
            PRINT 'FILE ' :NEW.FILE2:' CREATED IN ':DIR.NAME2
        END
        ELSE
            STOP 'Cannot create ':NEW.FILE2:' to ':DIR.NAME2
        END
    END
**-------------------------------------------------------
    FN.OFS.SOURCE    = "F.OFS.SOURCE"
    F.OFS.SOURCE     = ""
    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "CUSTOMER"
    OFS.OPTIONS      = "CUSTOMER.SMS"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    COMMA = ","
    DIRCY = '&SAVEDLISTS&'
    FILE.NAME = "CUSTOMER-LIST.txt"

    OPENSEQ DIRCY,FILE.NAME TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''
    I = 1
    CARD.NO = ''

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
* CHK.LIN      = FIELD(Line,",",1)[1,6]
            CUS.SMS.NUMBER = FIELD(Line,",",2)
            CUS.ID = FIELD(Line,",",1)
            IF CUS.SMS.NUMBER # '' THEN
                IF CUS.ID # '' THEN
                   * IF LEN(CUS.ID) GT 7 THEN
                        CUS.ID = TRIM(CUS.ID,"0","L")
                   * END
* TEXT = "NUM= ": CUS.SMS.NUMBER ; CALL REM
* TEXT = "NUM= ": CUS.ID ; CALL REM
**WANT TO CHECK IF NUMBER IS LESS THAN 11
                    CALL F.READ(FN.CUSTOMER,  CUS.ID, R.CUSTOMER ,F.CUSTOMER, READ.ER.CUSTOMER)
** ----------------------- OFS ----------------
                    OFS.MESSAGE.DATA :="SMS.1=":CUS.SMS.NUMBER:COMMA
*TEXT = "OFS DATA= ":  OFS.MESSAGE.DATA ; CALL REM
                    COMP =  R.CUSTOMER<EB.CUS.CO.CODE>
                    BRN    = COMP[8,2]

                    COMPO  = COMP
                    OFS.USER.INFO     = "INPUTT":BRN:"//":COMPO

                    OFS.IDD = CUS.ID
                    F.PATH  = FN.OFS.IN
                    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:OFS.IDD:COMMA:OFS.MESSAGE.DATA
                    OFS.ID  = "CUS.":CUS.ID:"-":TODAY

                    IF BRN NE '' THEN
                        XX = XX + 1
                        OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
                        WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-CUS.SMS' ON ERROR  TEXT = " ERROR ";CALL REM
                        OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
                        WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
                    END ELSE  ;**Else of not write**
                        CUS.DATA = CUS.ID
                        CUS.DATA := "|CUSTOMER SMS="
                        CUS.DATA :=  CUS.SMS.NUMBER
                        CUS.DATA := "|Branch="
                        CUS.DATA :=  BRN
                        NM = NM + 1
                        WRITESEQ CUS.DATA TO V.FILE.IN ELSE
                            PRINT  'CAN NOT WRITE LINE ':CUS.DATA
                        END
                    END
                END
            END
            OFS.MESSAGE.DATA = ''
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath

    TEXT = 'PROGRAM IS FINISHED' ; CALL REM
    TEXT = 'CUSTOMERS HAVE BEEN UPDATED = ':XX ; CALL REM
    TEXT = 'CUSTOMERS HAVE BEEN FAILED = ':NM ; CALL REM
    RETURN
***------------------------------------------------------------------***
END
