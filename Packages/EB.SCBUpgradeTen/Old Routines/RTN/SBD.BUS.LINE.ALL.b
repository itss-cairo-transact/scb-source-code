* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.BUS.LINE.ALL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

*---------------------------------------

    EXECUTE "SBR.BUS.LINE.001"
    EXECUTE "SBR.BUS.LINE.002"
    EXECUTE "SBR.BUS.LINE.AL.EGP"
    EXECUTE "SBR.BUS.LINE.AL.FCY"
    EXECUTE "SBR.BUS.LINE.PL.EGP"
    EXECUTE "SBR.BUS.LINE.PL.FCY"
    EXECUTE "SBD.BUS.LINE.AVRG.FILL.01"
    EXECUTE "SBD.BUS.LINE.AVRG.FILL.02"
    EXECUTE "SBD.BUS.LINE.AVRG.FILL.03"

    DAT.1 = TODAY[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')

    DAT.ID = 'EG0010001'
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,DAT.2)

    IF DAT.1 EQ DAT.2 THEN
        EXECUTE "SBM.BUS.LINE.AVRG.01"
        EXECUTE "SBM.BUS.LINE.AVRG.02"
    END

    RETURN

END
