* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-228</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.TLR.CO.3000000
***    PROGRAM SBD.TLR.CO.3000000

** WRITTEN BY " HARES M. MAHMOUD "

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.LEGAL.FORM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INDUSTRY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
**********************************************************************

    WS.MAX.AMOUNT = 2500000
*    WS.MAX.AMOUNT = 1000000
    GOSUB INIT

*------------------------
*                                ***  ����� �� �� ����� ��������
    STR.DATE = TODAY
*    CALL CDT('', STR.DATE, "-1W")
    END.DATE = STR.DATE
*-------------------------------
    FN.TLLR = "FBNK.TELLER"
*    FN.TLLR = "FBNK.TELLER$HIS"
**********************************************************************

    F.TLLR  = ""
    R.TELLER = ""
    Y.TLLR.ID   = ""
* ------------------
    F.TLLR  = ""
    R.TELLER = ""
    Y.TLLR.ID   = ""


*-------------------------------
    CALL OPF(FN.TLLR,F.TLLR)
*-------------------------------
    WS.COMP = ID.COMPANY
**    WS.COMP = 'EG0010002'

*-----------------------------------------------------------------------------
*    REPORT.ID = 'SBD.TLR.CO.3000000'
    REPORT.ID = 'SBQ.STMT.CO.999'
    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD

* --------------------
    GOSUB READ.TELLER.FILE
* --------------------

    PR.LINE = STR('_',132)
    PRINT PR.LINE

    XX = SPACE(132)
    XX<1,1>[10,35]   = '����������� ������� ������� :  '
    XX<1,1>[47,20]   = NO.CUST
    PRINT XX<1,1>
    XX = SPACE(132)
    PRINT XX<1,1>


    XX<1,1>[50,40]   = "*****  �������������  ����������   *****"
    PRINT XX<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*----------------------------------------------------------------------------
READ.TELLER.FILE:

    S.CUST.ID = 0
    WS.DR.AMT = 0
    WS.CR.AMT = 0
*--------------------------------------------
*    FN.TLLR = "FBNK.TELLER"

    SEL.TLLR = "SELECT ":FN.TLLR:" WITH"
    SEL.TLLR :=" DR.CR.MARKER EQ 'DEBIT'"
    SEL.TLLR :=" AND CUSTOMER.1 NE ''"
    SEL.TLLR :=" BY CUSTOMER.1"
*--------------------------------------------
*    FN.TLLR = "FBNK.TELLER$HIS"

    SEL.TLLRH = "SELECT ":FN.TLLR:" WITH"
    SEL.TLLRH :=" DR.CR.MARKER EQ 'DEBIT'"
    SEL.TLLRH :=" AND RECORD.STATUS EQ 'MAT'"
    SEL.TLLRH :=" AND CUSTOMER.1 NE ''"
    SEL.TLLRH :=" AND AUTH.DATE EQ ":STR.DATE
    SEL.TLLRH :=" BY CUSTOMER.1"
*--------------------------------------------
    IF  FN.TLLR EQ "FBNK.TELLER$HIS" THEN
        SEL.TLLR  =  SEL.TLLRH
    END
*--------------------------------------------


    CALL EB.READLIST(SEL.TLLR,TLLR.LIST,'',NO.OF.TLLR,ERR.TLLR)


    LOOP
        REMOVE Y.TLLR.ID FROM TLLR.LIST SETTING TLLR.POS

    WHILE Y.TLLR.ID:TLLR.POS
        CALL F.READ(FN.TLLR,Y.TLLR.ID,R.TELLER,F.TLLR,ERR.TLLR.ERR)

        T.CUST.ID = R.TELLER<TT.TE.CUSTOMER.1>

        LCL.AMT = R.TELLER<TT.TE.AMOUNT.LOCAL.1>
*--------
        IF S.CUST.ID EQ 0 THEN
            S.CUST.ID = T.CUST.ID
        END
*--------
        IF S.CUST.ID NE T.CUST.ID THEN
            GOSUB PREPAIR.PRINT.LINE

            WS.DR.AMT = 0
            WS.CR.AMT = 0
            S.CUST.ID = T.CUST.ID
        END
*--------
        WS.DR.AMT += LCL.AMT



    REPEAT

    GOSUB PREPAIR.PRINT.LINE

    RETURN


*----------------------------------------------------------------------------
PREPAIR.PRINT.LINE:

    Y.CUST.ID = S.CUST.ID

    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
    N.SEC  = LOCAL.REF<1,CULR.NEW.SECTOR>


*--------
    IF N.SEC EQ '4650' THEN
        RETURN
    END
*------------
    IF INT(WS.DR.AMT) GE WS.MAX.AMOUNT THEN
        GOSUB READ.CUSTOMER.FILE
        GOSUB PRINT.ONE.LINE
        PR.LINE = STR('_',132)
        PRINT PR.LINE
    END
*------------

    RETURN
*----------------------------------------------------------------------------

READ.CUSTOMER.FILE:

    CALL F.READ(FN.CUST,Y.CUST.ID,R.CUSTOMER,F.CUST,ERR.CUST.ERR)

    Y.SEC.ID  = R.CUSTOMER<EB.CUS.SECTOR>
    Y.IND.ID  = R.CUSTOMER<EB.CUS.INDUSTRY>
    Y.CMP.ID  = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
    CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
    Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>
    NN.SEC   = LOCAL.REF<1,CULR.NEW.SECTOR>
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,Y.CMP.ID,CUSTBR)
    CUST.BR = CUSTBR


    RETURN

*----------------------------------------------------------------------------
PRINT.ONE.LINE:

    Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>

    CALL F.READ(FN.SEC,Y.SEC.ID,R.SECTOR,F.SEC,ERR.SEC)
    SEC.NAME = R.SECTOR<EB.SEC.DESCRIPTION,2>

    CALL F.READ(FN.LGL,Y.LGL.ID,R.LEGAL,F.LGL,ERR.LGL)
    LGL.NAME = R.LEGAL<LEG.DESCRIPTION,2>
*---------------

    XX = SPACE(132)
    PRINT XX<1,1>
    GOSUB WRITE.LINE.01
    GOSUB WRITE.LINE.ALL
*---------------
    RETURN

*------------------------------------------------------------------------
WRITE.LINE.ALL:
* -------
    SSS.MARK = 'CREDIT'
 *   SSS.MARK = '�������'
    GOSUB R.ONE.MARK

* -------
    SSS.MARK = 'DEBIT'
 *   SSS.MARK = '�������'
    GOSUB R.ONE.MARK


    RETURN
*------------------------------------------------------------------------
R.ONE.MARK:

    P.TOT.AMT = 0
    P.CCY.AMT = 0
    SAVE.CCY = 0


*    FN.TLLR = "FBNK.TELLER"
    PRT.TLLR  = "SELECT ":FN.TLLR:" WITH"
    PRT.TLLR := " CUSTOMER.1 EQ ":Y.CUST.ID
    PRT.TLLR :=" AND DR.CR.MARKER EQ ":SSS.MARK
    PRT.TLLR := " BY CURRENCY.1"

    CALL EB.READLIST(PRT.TLLR,PRT.LIST,'',NO.OF.PRT,ERR.PRT)
* ---------
    IF  NO.OF.PRT EQ 0 THEN
        GO TO NEXT.REC
    END
* ---------
    LOOP
        REMOVE Y.PRT.ID FROM PRT.LIST SETTING PRT.POS
    WHILE Y.PRT.ID:PRT.POS
        CALL F.READ(FN.TLLR,Y.PRT.ID,R.PRT,F.TLLR,ERR.PRT.ERR)

        P.ID      = Y.PRT.ID
        P.CCY     = R.PRT<TT.TE.CURRENCY.1>
        P.MARK    = R.PRT<TT.TE.DR.CR.MARKER>
        P.FCY.AMT = R.PRT<TT.TE.AMOUNT.FCY.1>
        P.RATE    = R.PRT<TT.TE.RATE.1>
        P.LCL.AMT = R.PRT<TT.TE.AMOUNT.LOCAL.1>

        P.TOT.AMT += P.LCL.AMT
* --------
        IF SAVE.CCY EQ 0 THEN
            SAVE.CCY = P.CCY
        END
        IF SAVE.CCY NE P.CCY  THEN
            GOSUB PRINT.ONE.CCY
            SAVE.CCY = P.CCY
            P.CCY.AMT = 0
        END
* --------
        P.CCY.AMT += P.LCL.AMT

        XX = SPACE(132)

        XX<1,1>[40,15] = P.ID
        XX<1,1>[55,10] = P.MARK
        XX<1,1>[65,3]  = P.CCY

        XX<1,1>[70,17]    = FMT(P.FCY.AMT,"17L,2")
        XX<1,1>[90,17]    = FMT(P.RATE,"10L,7")
        XX<1,1>[110,17]   = FMT(P.LCL.AMT,"17L,2")

        PRINT XX<1,1>
*--------
    REPEAT
*--------

    GOSUB PRINT.ONE.CCY

    XX = SPACE(132)

    XX<1,1>[60,10] = '*****'
    XX<1,1>[70,15] = '�����������'
    XX<1,1>[85,10] =  P.MARK
    XX<1,1>[110,17]   = FMT(P.TOT.AMT,"17L,2")

    PRINT XX<1,1>

    XX = SPACE(132)
    PRINT XX<1,1>

NEXT.REC:

    RETURN
*------------------------------------------------------------------------
PRINT.ONE.CCY:

    XX = SPACE(132)

    XX<1,1>[60,10] = '*****'
    XX<1,1>[70,15] = '�����������'
    XX<1,1>[85,10] =  SAVE.CCY
    XX<1,1>[110,17]   = FMT(P.CCY.AMT,"17L,2")

    PRINT XX<1,1>



    RETURN
*------------------------------------------------------------------------
WRITE.LINE.01:

    P.DR.AMT = 0
    P.CR.AMT = 0

    XX = SPACE(132)
    XX<1,1>[1,9]     = Y.CUST.ID
    XX<1,1>[10,35]   = CUST.NAME
*    XX<1,1>[50,20]   = SEC.NAME
*    XX<1,1>[50,20]   = LGL.NAME
    XX<1,1>[47,25] = LEFT(LGL.NAME, 25)
    XX<1,1>[75,17]  = CUST.BR

    P.DR.AMT  = FMT(WS.DR.AMT,"17L,2")
    P.CR.AMT  = FMT(WS.CR.AMT,"17L,2")

*    XX<1,1>[75,17]   = P.DR.AMT
*    XX<1,1>[95,17]   = P.CR.AMT
*    XX<1,1>[95,17]   = NO.OF.PRT
*    XX<1,1>[115,17]  = NN.SEC

    PRINT XX<1,1>

    NO.CUST = NO.CUST + 1

*    PR.LINE = STR('_',132)
*    PRINT PR.LINE

    NO.OF.LINE = NO.OF.LINE + 1
    IF NO.OF.LINE GT 39  THEN
        GOSUB PRINT.HEAD
    END

    RETURN
*-----------------------------------------------------------------------
INIT:
    PGM.NAME = "SBQ.STMT.CO.999"
    SAVE.CUST  = 0
*------------------------
    CUST.TYPE = "�����������"
***    CUST.TYPE = "���������"

    NO.CUST = 0
*------------------------
    WRK.DATE   = TODAY

******   WRK.DATE = 20100815


    WRK.YY     = WRK.DATE[1,4]
    WRK.MM     = WRK.DATE[5,2]
    WRK.DD     = WRK.DATE[7,2]

*------------------------
    BEGIN CASE
    CASE     WRK.MM GE 01 AND WRK.MM LE 03
        END.YY = WRK.YY - 1
        END.MM = 12
        END.DD = 31

    CASE     WRK.MM GE 04 AND WRK.MM LE 06
        END.YY = WRK.YY
        END.MM = 03
        END.DD = 31

    CASE     WRK.MM GE 07 AND WRK.MM LE 09
        END.YY = WRK.YY
        END.MM = 06
        END.DD = 30


    CASE     WRK.MM GE 10 AND WRK.MM LE 12
        END.YY = WRK.YY
        END.MM = 09
        END.DD = 30

    END CASE


    END.MM = FMT(END.MM,"R%2")
    END.DD = FMT(END.DD,"R%2")

    END.DATE = END.YY:END.MM:END.DD

*----------------------------------------------------------------------
    STR.YY = END.YY
    STR.MM = END.MM - 2
    STR.DD = "01"

    STR.MM = FMT(STR.MM,"R%2")
    STR.DD = FMT(STR.DD,"R%2")
    STR.DATE = STR.YY:STR.MM:STR.DD
*------------------------
***** CALL CDT('', STR.DATE, "-1C")
*------------------------
    P.DATE   = FMT(TODAY,"####/##/##")
    P.STR.DATE   = FMT(STR.DATE,"####/##/##")
    P.END.DATE   = FMT(END.DATE,"####/##/##")
*------------------------

    LOW.AMT  = WS.MAX.AMOUNT
    HIGH.AMT = 999999999999


    P.LOW.AMT = FMT(LOW.AMT,"15L,")
    P.HIGH.AMT = FMT(HIGH.AMT,"17L,")

*------------------------
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUSTOMER = ""
    Y.CUST.ID   = ""
*-------------------------------
    CALL OPF(FN.CUST,F.CUST)
*-------------------------------
    FN.SEC = "FBNK.SECTOR"
    F.SEC  = ""
    R.SECTOR = ""
    Y.SEC.ID   = ""
*-------------------------------
    CALL OPF(FN.SEC,F.SEC)
*-------------------------------
    FN.LGL = "F.SCB.CUS.LEGAL.FORM"
    F.LGL  = ""
    R.LEGAL = ""
    Y.LGL.ID   = ""
*-------------------------------
    CALL OPF(FN.LGL,F.LGL)
*-------------------------------

    T.DR.AMT   = 0
    T.CR.AMT   = 0





    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
    YYBRN = BRANCH


    T.DAY = P.DATE
    PR.HD ="'L'":SPACE(1):"��� ���� ������ "  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY:SPACE(86):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(11):SPACE(100):PGM.NAME
**    PR.HD :="'L'":SPACE(50):"������ ������ ������� �� ������� ���� ��� �������"
**    PR.HD :="'L'":SPACE(55):"������� �� �������":" ":CUST.TYPE
    PR.HD :="'L'":SPACE(55):"������ ������� ������� �� �������":" ":CUST.TYPE
    PR.HD :="'L'":SPACE(50):"�� ":P.LOW.AMT:" �� ":"  ��� ��� �� ":P.HIGH.AMT:" �� "
    PR.HD :="'L'":SPACE(57):"�� �� ������ �� ������� ��������"
**    PR.HD :="'L'":SPACE(55):"������ �� ":P.STR.DATE:"   ��� ":P.END.DATE

    PR.HD :="'L'":SPACE(50):STR('_',50)
    PR.HD :="'L'":" "
***********************************************************
    PR.HD :="'L'":SPACE(1)
*    PR.HD :="'L'":SPACE(0):" ��� ������" :SPACE(11):" ��� ������ " :SPACE(12):"����� ��������":SPACE(14):"������ ���������":SPACE(4):"������ ����������"
    PR.HD :="'L'":SPACE(0):" ��� ������" :SPACE(11):" ��� ������ " :SPACE(12):"����� ��������":SPACE(14):"���������"
    PR.HD :="'L'":SPACE(74):"�������             �������":SPACE(13):"������"


    PR.HD :="'L'":STR('_',132)
***********************
    HEADING PR.HD
    NO.OF.LINE = 0
    RETURN


END
