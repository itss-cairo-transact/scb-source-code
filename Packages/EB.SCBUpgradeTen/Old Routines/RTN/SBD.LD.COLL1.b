* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
************************************NI7OOOOOOOOOOOOO********************
*-----------------------------------------------------------------------------
* <Rating>-125</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.LD.COLL1
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LD.RENEW.METHOD
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS

*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*========================================================================
INITIATE:
******** REPORT.ID='SBD.LD.COLL1'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

*   YTEXT = "Enter LD.NO : "
*  CALL TXTINP(YTEXT, 8, 22, "16", "A")
* ID = COMI

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    DAT.H  = ''     ; V.DATE.H   = '' ; MAT.DATE.H = '' ; DESC = '' ; DESC.H = ''
    AMT.H  = ''     ; CATEG.H = ''    ; RATE.H = ''     ; RATE.AMT.H = '' ; KEYLIST2 = ''
    CUR.H  = ''     ; DAT1.H = ''     ; CATEG.ID.H = '' ; DESC.ID.H = ''
    DATT = '' ; DATF = '' ; NAME2 = ''
    RETURN
*========================================================================
PROCESS:

***** T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21096 AND PRODUCT.TYPE EQ FINAL AND OPERATION.CODE EQ 1401 AND STATUS EQ 'LIQ'"
    T.SEL = "SELECT ":FN.LD:" WITH CATEGORY EQ 21096 AND OPERATION.CODE EQ 1401 AND STATUS EQ 'LIQ' AND CO.CODE EQ " : COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,E1)
            ID1 = KEY.LIST<Z>
            CUST.ID1 = R.LD<LD.CUSTOMER.ID>
            AMT1=R.LD<LD.AMOUNT>
            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
            CUST.NAME1 = LOCAL.REF<1,CULR.ARABIC.NAME>
            CUST.NAME2 = LOCAL.REF<1,CULR.ARABIC.NAME.2>
            DAT1 = R.LD<LD.LOCAL.REF><1,LDLR.ISSUE.DATE>
            ISSUE.DATE  = DAT1[7,2]:'/':DAT1[5,2]:'/':DAT1[1,4]
            OLD.NO =R.LD<LD.LOCAL.REF><1,LDLR.OLD.NO>
            CURR.NO1 = R.LD<LD.CURR.NO>


            FIN.DAT=R.LD<LD.FIN.MAT.DATE>
            FIN.DATE  = FIN.DAT[7,2]:'/':FIN.DAT[5,2]:'/':FIN.DAT[1,4]

            CURR.NO = CURR.NO1 - 1

            XX1 = SPACE(132)
            XX1<1,1>[1,15]   = ID1
            XX1<1,1>[18,15]  = CUST.ID1
            XX1<1,1>[35,15]  = CUST.NAME1:' ':CUST.NAME2
            XX1<1,1>[72,15]  = AMT1
            XX1<1,1>[90,15]  = ISSUE.DATE
  **          XX1<1,1>[115,10] = OLD.NO
            XX1<1,1>[105,10] = FIN.DATE
            PRINT XX1<1,1>
        NEXT Z
    END
***************************************************************
*******T.SEL1 =  "SELECT ":FN.LD.H:" WITH  CATEGORY EQ 21096 AND PRODUCT.TYPE EQ FINAL AND OPERATION.CODE EQ 1401 AND STATUS EQ 'LIQ'"
    T.SEL1 =  "SELECT ":FN.LD.H:" WITH CATEGORY EQ 21096 AND OPERATION.CODE IN (1401 1402 1403) AND STATUS EQ 'LIQ' AND CO.CODE EQ " : COMP
    T.SEL1 := " BY @ID "
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
*******TEXT = "KEY.LIST1 :" : KEY.LIST1 ; CALL REM
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
*******TEXT = "SELECTED1 :" : SELECTED1 ; CALL REM
            CALL F.READ(FN.LD.H,KEY.LIST1<K>,R.LD.H,F.LD.H,E1)
            CALL F.READ(FN.LD.H,KEY.LIST1<K+1>,R.LD.H1,F.LD.H,E1)
            IF FIELD(KEY.LIST1<K>,';',1) # FIELD(KEY.LIST1<K+1>,';',1) THEN
                ID2 = KEY.LIST1<K>
                CUST.ID2 = R.LD.H<LD.CUSTOMER.ID>
                AMT2=R.LD.H<LD.AMOUNT>
                CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID2,LOCAL.REF)
                CUST.NAME2 = LOCAL.REF<1,CULR.ARABIC.NAME>
****  DAT2 = R.LD.H<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
**  DAT = R.LD.H<LD.DATE.TIME>
                DAT2= R.LD.H<LD.LOCAL.REF><1,LDLR.ISSUE.DATE>
                ISSUE.DATE2  = DAT2[7,2]:'/':DAT2[5,2]:'/':DAT2[1,4]
                OLD.NO2 =R.LD.H<LD.LOCAL.REF><1,LDLR.OLD.NO>

                FIN.DAT2=R.LD.H<LD.FIN.MAT.DATE>
                FIN.DATE2  = FIN.DAT2[7,2]:'/':FIN.DAT2[5,2]:'/':FIN.DAT2[1,4]

                XX2 = SPACE(132)
                XX2<1,1>[1,15]   = ID2
                XX2<1,1>[18,15]  = CUST.ID2
                XX2<1,1>[35,15]  = CUST.NAME2
                XX2<1,1>[72,15]  = AMT2
                XX2<1,1>[90,15]  = ISSUE.DATE2
**                XX2<1,1>[115,15] = OLD.NO2
                XX2<1,1>[105,15] = FIN.DATE2
                PRINT XX2<1,1>
            END
*** NEXT K+1

        NEXT K

    END
* NEXT Z
* END
    TEXT = "��� ������� "  ; CALL REM
*===============================================================
    PRINT STR('=',120)
*===============================================================
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    DATF1  = DATF[7,2]:'/':DATF[5,2]:"/":DATF[1,4]
    DATT1  = DATT[7,2]:'/':DATT[5,2]:"/":DATT[1,4]

    CALL F.READ(FN.LD,ID,R.LD,F.LD,E1)
    CUST.ID = R.LD<LD.CUSTOMER.ID>
    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOCAL.REF)
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD ="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(60):"������� �� ������ ������ ��������"
*PR.HD :="'L'":SPACE(55):"��":" ":DATF1:" ":"���":" ":DATT1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
*   PR.HD :="'L'":"���� �������� : ":CUST.ID:SPACE(10):"��� ���� ������ :":ID
    PR.HD :="'L'":" "
**PR.HD :="'L'":"���� �������� : ":CUST.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',120)
***********  PR.HD :="'L'":"��� ���� " :SPACE(5):"��� ������":SPACE(8):"��� ������":SPACE(25):"���� ��������" :SPACE(10):"":�:"����� ��������":SPACE (8)"����� ������"
    PR.HD :="'L'":"��� ���� " :SPACE(5):"��� ������":SPACE(8):"��� ������":SPACE(25):"���� ��������" :SPACE(10):"����� �������":SPACE(10):"����� ��������":SPACE(8):"����� ������"
    PR.HD :="'L'":"������ ������"
    PR.HD :="'L'":STR('_',120)
***    PRINT
    HEADING PR.HD
    RETURN
END
*==============================================================
