* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBM.BUS.LINE.AVRG.01

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE.AVRG
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE.AVRG.EGP
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE.AVRG.FCY
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS.ALL
    WS.BAL.RET = 0 ; WS.BAL.CORP = 0 ; WS.BAL.HQ = 0 ; WS.BAL.TRE = 0 ; WS.TOTAL = 0
    GOSUB PROCESS.EGP
    WS.BAL.RET = 0 ; WS.BAL.CORP = 0 ; WS.BAL.HQ = 0 ; WS.BAL.TRE = 0 ; WS.TOTAL = 0
    GOSUB PROCESS.FCY

    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========
    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT

*--------------------------------------
    FN.BU = 'F.SCB.BUS.LINE.AVRG' ; F.BU = ''
    CALL OPF(FN.BU,F.BU)

    FN.BUE = 'F.SCB.BUS.LINE.AVRG.EGP' ; F.BUE = ''
    CALL OPF(FN.BUE,F.BUE)

    FN.BUF = 'F.SCB.BUS.LINE.AVRG.FCY' ; F.BU.FCY = ''
    CALL OPF(FN.BUF,F.BUF)

    WS.BAL.RET = 0 ; WS.BAL.CORP = 0 ; WS.BAL.HQ = 0 ; WS.BAL.TRE = 0 ; WS.TOTAL = 0
*--------------------------------------

    WS.DAT.END.MONTH = TODAY[1,6]:'01'

    CALL CDT("",WS.DAT.END.MONTH,'-1C')

    WS.DAT.START.MONTH = WS.DAT.END.MONTH[1,6]:'01'

    WS.DAYS =  WS.DAT.END.MONTH[2]

    RETURN
*-----------------------------------------------------------------------------
PROCESS.ALL:
*=======
    T.SEL = "SELECT ":FN.BU:" WITH @ID GE ":WS.DAT.START.MONTH:" AND @ID LE ":WS.DAT.END.MONTH:" AND FILE.TYPE EQ 'AL' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BU,KEY.LIST<I>,R.BU,F.BU,E1)

            WS.DESC      = R.BU<BLA.DESCRIPTION>
            WS.BAL.CORP += R.BU<BLA.CORPORATE>
            WS.BAL.RET  += R.BU<BLA.RETAIL>
            WS.BAL.TRE  += R.BU<BLA.TREASURY>
            WS.BAL.HQ   += R.BU<BLA.HQ>
            WS.TOTAL    += R.BU<BLA.TOTAL>

            AVG.ID = WS.DAT.END.MONTH:'-TOTAL'
            CALL F.READ(FN.BU,AVG.ID,R.BU,F.BU,E4)

            R.BU<BLA.FILE.TYPE>     = 'AL'
            R.BU<BLA.DESCRIPTION> = WS.DESC
            R.BU<BLA.CORPORATE>   = WS.BAL.CORP
            R.BU<BLA.RETAIL>      = WS.BAL.RET
            R.BU<BLA.TREASURY>    = WS.BAL.TRE
            R.BU<BLA.HQ>          = WS.BAL.HQ
            R.BU<BLA.TOTAL>       = WS.TOTAL

            CALL F.WRITE(FN.BU,AVG.ID,R.BU)
            CALL JOURNAL.UPDATE(AVG.ID)

        NEXT I
    END
    RETURN
*====================================================

PROCESS.EGP:
*=======
    T.SEL = "SELECT ":FN.BUE:" WITH @ID GE ":WS.DAT.START.MONTH:" AND @ID LE ":WS.DAT.END.MONTH:" AND FILE.TYPE EQ 'AL' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BUE,KEY.LIST<I>,R.BUE,F.BUE,E1)

            WS.DESC      = R.BUE<BLA.DESCRIPTION>
            WS.BAL.CORP += R.BUE<BLA.CORPORATE>
            WS.BAL.RET  += R.BUE<BLA.RETAIL>
            WS.BAL.TRE  += R.BUE<BLA.TREASURY>
            WS.BAL.HQ   += R.BUE<BLA.HQ>
            WS.TOTAL    += R.BUE<BLA.TOTAL>

            AVG.ID = WS.DAT.END.MONTH:'-TOTAL'
            CALL F.READ(FN.BUE,AVG.ID,R.BUE,F.BUE,E4)

            R.BUE<BLA.FILE.TYPE>     = 'AL'
            R.BUE<BLA.DESCRIPTION> = WS.DESC
            R.BUE<BLA.CORPORATE>   = WS.BAL.CORP
            R.BUE<BLA.RETAIL>      = WS.BAL.RET
            R.BUE<BLA.TREASURY>    = WS.BAL.TRE
            R.BUE<BLA.HQ>          = WS.BAL.HQ
            R.BUE<BLA.TOTAL>       = WS.TOTAL

            CALL F.WRITE(FN.BUE,AVG.ID,R.BUE)
            CALL JOURNAL.UPDATE(AVG.ID)

        NEXT I
    END
    RETURN
*====================================================
PROCESS.FCY:
*=======
    T.SEL = "SELECT ":FN.BUF:" WITH @ID GE ":WS.DAT.START.MONTH:" AND @ID LE ":WS.DAT.END.MONTH:" AND FILE.TYPE EQ 'AL' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BUF,KEY.LIST<I>,R.BUF,F.BUF,E1)

            WS.DESC      = R.BUF<BLA.DESCRIPTION>
            WS.BAL.CORP += R.BUF<BLA.CORPORATE>
            WS.BAL.RET  += R.BUF<BLA.RETAIL>
            WS.BAL.TRE  += R.BUF<BLA.TREASURY>
            WS.BAL.HQ   += R.BUF<BLA.HQ>
            WS.TOTAL    += R.BUF<BLA.TOTAL>

            AVG.ID = WS.DAT.END.MONTH:'-TOTAL'
            CALL F.READ(FN.BUF,AVG.ID,R.BUF,F.BUF,E4)

            R.BUF<BLA.FILE.TYPE>     = 'AL'
            R.BUF<BLA.DESCRIPTION> = WS.DESC
            R.BUF<BLA.CORPORATE>   = WS.BAL.CORP
            R.BUF<BLA.RETAIL>      = WS.BAL.RET
            R.BUF<BLA.TREASURY>    = WS.BAL.TRE
            R.BUF<BLA.HQ>          = WS.BAL.HQ
            R.BUF<BLA.TOTAL>       = WS.TOTAL

            CALL F.WRITE(FN.BUF,AVG.ID,R.BUF)
            CALL JOURNAL.UPDATE(AVG.ID)

        NEXT I
    END
    RETURN
*====================================================
*====================================================
