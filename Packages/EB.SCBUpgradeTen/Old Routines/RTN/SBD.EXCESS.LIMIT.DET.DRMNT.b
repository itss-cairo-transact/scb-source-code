* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>3916</Rating>
*-----------------------------------------------------------------------------
***======================================
*** COPY FROM SBD.EXCESS.LIMIT.DET.NE ***
*** 2010/11/09 ***
***======================================

    SUBROUTINE SBD.EXCESS.LIMIT.DET.DRMNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EXCEPTION.LOG.FILE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 63 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.EXCESS.LIMIT.DET.DRMNT'
    CALL PRINTER.ON(REPORT.ID,'')

    LD.AC.NO    = 0    ; FT.AC.NO    = 0
    LD.CUS.NAME = ''   ; FT.CUS.NAME = ''
    LD.AMT  = 0        ; FT.AMT  = 0
    LD.TIME = ''       ; FT.TIME = ''
    LD.DATE = ''       ; FT.DATE = '' ; FT.OVER2 = '' ; FT.OVER3 = ''
    LD.AUTH = ''       ; FT.AUTH = '' ; LD.OVER2 = '' ; LD.OVER3 = ''

    TT.AC.NO    = 0    ; LC.AC.NO    = 0
    TT.CUS.NAME = ''   ; LC.CUS.NAME = ''

    TT.AMT  = 0        ; LC.AMT  = 0
    TT.TIME = ''       ; LC.TIME = ''
    TT.DATE = ''       ; LC.DATE = '' ; TT.OVER2 = '' ; TT.OVER3 = ''
    TT.AUTH = ''       ; LC.AUTH = '' ; LC.OVER2 = '' ; LC.OVER3 = ''

    DR.AC.NO    = 0
    DR.CUS.NAME = ''
    DR.AMT   = 0
    DR.TIME  = ''
    DR.DATE  = ''
    DR.AUTH  = ''
    DR.OVER2 = ''
    DR.OVER3 = ''
    SCB.CO   = ID.COMPANY

*************NESSMA********
    CUS.NO.VAL = 0
    EXC.DATE   = ''
***************************
    RETURN
*========================================================================
CALLDB:
    FN.EC = 'F.EXCEPTION.LOG.FILE'       ; F.EC = ''
    FN.FT = 'FBNK.FUNDS.TRANSFER'        ; F.FT = ''
    FN.TT = 'FBNK.TELLER'                ; F.TT = ''
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LC = 'FBNK.LETTER.OF.CREDIT'      ; F.LC = ''
    FN.DR = 'FBNK.DRAWINGS'              ; F.DR = ''
    FN.AC = 'FBNK.ACCOUNT'               ; F.AC = ''

    CALL OPF(FN.EC,F.EC)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.DR,F.DR)
    CALL OPF(FN.AC,F.AC)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG3=""
    KEY.LIST4="" ; SELECTED4="" ;  ER.MSG4=""
    KEY.LIST5="" ; SELECTED5="" ;  ER.MSG5=""
*---------------------------------------------
*-- edit by Nessma Mohammad on 2011/06/30 ----
    YTEXT = "Enter The Date : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    EXC.DATE = COMI
*---------------------------------------------
*    T.SEL = "SELECT ":FN.EC:" WITH COMP.CODE EQ ":SCB.CO:" BY REC.KEY"

    T.SEL = "SELECT ":FN.EC:" WITH COMP.CODE EQ ":SCB.CO:" AND RUN.DATE EQ ":EXC.DATE:" BY REC.KEY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.EC,KEY.LIST<I>,R.EC,F.EC,E1)
            EXC.KEY    = R.EC<EB.EXC.REC.KEY>
            EXC.KEY.ID = R.EC<EB.EXC.APPLICATION>
*            EXC.KEY.ID = EXC.KEY[1,2]
            EXC.KEY.LEN = LEN(EXC.KEY)
            IF EXC.KEY.ID EQ 'LD' THEN
*************************************************************************
                T.SEL1 = "SELECT ":FN.LD: " WITH @ID EQ ":EXC.KEY:" AND OVERRIDE LIKE ...DRMT..."
                CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
                IF SELECTED1 THEN
                    FOR J = 1 TO SELECTED1
                        CALL F.READ(FN.LD,KEY.LIST1<J>,R.LD,F.LD,E2)
                        LD.OVER = R.LD<LD.OVERRIDE>
                        LD.AMT = R.LD<LD.AMOUNT>
                        LD.CUR = R.LD<LD.CURRENCY>
                        LD.AC.NO = R.LD<LD.DRAWDOWN.ACCOUNT>

*** ACCOUNT BALANCE UPDATED BY KHALED 2010/10/17 ******
                        CALL F.READ(FN.AC,LD.AC.NO,R.AC,F.AC,E2)
                        CUR.BAL = R.AC<AC.WORKING.BALANCE>
                        OLD.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
*===============================================================
                        IF LD.AC.NO EQ '' THEN
                            LD.LOCAL = R.LD<LD.LOCAL.REF>
                            LD.AC.NO = LD.LOCAL<1,LDLR.DEBIT.ACCT>
                        END
*===============================================================
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,LD.AC.NO,LOCAL.REF)
*************************NESSMA*********************
                        IF(LD.AC.NO[1,1] EQ 0) THEN
                            CUS.NO.VAL = LD.AC.NO[2,7]
                        END ELSE
                            CUS.NO.VAL = LD.AC.NO[1,8]
                        END
                        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.NO.VAL,LOCAL.REF.NE)
                        LD.CUS.NAME = LOCAL.REF.NE<1,CULR.ARABIC.NAME>
****************************************************
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,LD.AC.NO,OLD.REF)
                        LD.OLD.NO = OLD.REF
                        LD.AUTH1 = R.LD<LD.OVERRIDE>
                        LD.AUTH2 = FIELD(LD.AUTH1,'SCB.',2)
                        IF LD.AUTH2 NE '' THEN
                            LD.AUTH3 = 'SCB.':LD.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,LD.AUTH3,AUTH.NAME)

                            IF AUTH.NAME EQ '' THEN
                                LD.AUTH = R.LD<LD.AUTHORISER>
                            END ELSE
                                LD.AUTH = AUTH.NAME
                            END

                        END ELSE
                            LD.AUTH1 = R.LD<LD.AUTHORISER>
                            LD.AUTH3 = FIELD(LD.AUTH1,"_",2)
                            LD.AUTH4 = LD.AUTH3[1,3]
                            IF LD.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,LD.AUTH3,AUTH.NAME)
                                LD.AUTH = AUTH.NAME
                            END ELSE
                                LD.AUTH = LD.AUTH1
                            END
                        END
                        LD.DD.TT = R.LD<LD.DATE.TIME>
*------------------------------------------------------------------------
                        LD.DATE1 = LD.DD.TT[1,2]
                        LD.DATE2 = LD.DD.TT[3,2]
                        LD.DATE3 = LD.DD.TT[5,2]
                        LD.DATE  = LD.DATE3:"/":LD.DATE2:"/":LD.DATE1
*------------------------------------------------------------------------
                        LD.TIME1 = LD.DD.TT[7,2]
                        LD.TIME2 = LD.DD.TT[9,2]
                        LD.TIME  = LD.TIME2:":":LD.TIME1
*------------------------------------------------------------------------
                        XX = SPACE(132)
                        XX4 = SPACE(132)
                        XXX4 = SPACE(132)
                        XX8 = SPACE(132)
                        XX<1,1>[1,20]    = LD.AC.NO
                        XX4<1,1>[1,20]   = LD.OLD.NO
                        XX<1,1>[20,35]   = LD.CUS.NAME
                        XX4<1,1>[22,35]  = EXC.KEY
                        XX<1,1>[57,10]   = LD.CUR
                        XX<1,1>[72,10]   = LD.AMT
                        XX4<1,1>[72,10]  = CUR.BAL
                        XX<1,1>[87,10]   = LD.TIME
                        XX4<1,1>[87,10]  = OLD.BAL
                        XX<1,1>[103,10]  = LD.DATE
                        XX<1,1>[118,35]  = LD.AUTH
                        XX4<1,1>[118,35] = LD.OVER2

                        PRINT XX<1,1>
                        PRINT XX4<1,1>
                        PRINT XXX4<1,1>
                        PRINT XX8<1,1>
                        PRINT STR('-',130)
                    NEXT J
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'FT' THEN
                T.SEL2 = "SELECT ":FN.FT: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...DRMT...)"
                CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)

                IF SELECTED2 THEN
                    FOR K = 1 TO SELECTED2
                        CALL F.READ(FN.FT,KEY.LIST2<K>,R.FT,F.FT,E3)
                        FT.AMT = R.FT<FT.AMOUNT.DEBITED>[4,20]
                        FT.CUR = R.FT<FT.DEBIT.CURRENCY>
                        FT.AC.NO = R.FT<FT.DEBIT.ACCT.NO>

*** ACCOUNT BALANCE UPDATED BY KHALED 2010/10/17 ******
                        CALL F.READ(FN.AC,FT.AC.NO,R.AC,F.AC,E2)
                        CUR.BAL = R.AC<AC.WORKING.BALANCE>
                        OLD.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
*******************************************************
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,FT.AC.NO,LOCAL.REF)
                        FT.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,FT.AC.NO,OLD.REF)
                        FT.OLD.NO = OLD.REF
                        FT.AUTH1 = R.FT<FT.OVERRIDE>
                        FT.OVER = R.FT<FT.OVERRIDE>

                        FT.AUTH2 = FIELD(FT.AUTH1,'SCB.',2)
                        IF FT.AUTH2 NE '' THEN
                            FT.AUTH3 = "SCB.":FT.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,FT.AUTH3,AUTH.NAME)

                            IF AUTH.NAME EQ '' THEN
                                FT.AUTH = R.FT<FT.AUTHORISER>
                            END ELSE
                                FT.AUTH = AUTH.NAME
                            END

                        END ELSE
                            FT.AUTH1 = R.FT<FT.AUTHORISER>
                            FT.AUTH3 = FIELD(FT.AUTH1,"_",2)
                            FT.AUTH4 = FT.AUTH3[1,3]
                            IF FT.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,FT.AUTH3,AUTH.NAME)
                                FT.AUTH = AUTH.NAME
                            END ELSE
                                FT.AUTH = FT.AUTH1
                            END
                        END
                        FT.DD.TT = R.FT<FT.DATE.TIME>
*------------------------------------------------------------------------
                        FT.DATE1 = FT.DD.TT[1,2]
                        FT.DATE2 = FT.DD.TT[3,2]
                        FT.DATE3 = FT.DD.TT[5,2]
                        FT.DATE  = FT.DATE3:"/":FT.DATE2:"/":FT.DATE1
*------------------------------------------------------------------------
                        FT.TIME1 = FT.DD.TT[7,2]
                        FT.TIME2 = FT.DD.TT[9,2]
                        FT.TIME  = FT.TIME2:":":FT.TIME1
*------------------------------------------------------------------------
                        XX1 = SPACE(132)
                        XX5 = SPACE(132)
                        XXX5 = SPACE(132)
                        XX9 = SPACE(132)
                        XX1<1,1>[1,20]   = FT.AC.NO
                        XX5<1,1>[1,20]   = FT.OLD.NO
                        XX1<1,1>[20,35]  = FT.CUS.NAME
                        XX5<1,1>[22,35]  = EXC.KEY
                        XX1<1,1>[57,10]  = FT.CUR
                        XX1<1,1>[72,10]  = FT.AMT
                        XX5<1,1>[72,10]  = CUR.BAL
                        XX1<1,1>[87,10]  = FT.TIME
                        XX5<1,1>[87,10]  = OLD.BAL
                        XX1<1,1>[103,10] = FT.DATE
                        XX1<1,1>[118,35] = FT.AUTH
                        XX5<1,1>[118,35] = FT.OVER2

                        PRINT XX1<1,1>
                        PRINT XX5<1,1>
                        PRINT XXX5<1,1>
                        PRINT XX9<1,1>
                        PRINT STR('-',130)
                    NEXT K
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'TT' THEN
                T.SEL3 = "SELECT ":FN.TT: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...DRMT...)"
                CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
                IF SELECTED3 THEN
                    FOR L = 1 TO SELECTED3
                        CALL F.READ(FN.TT,KEY.LIST3<L>,R.TT,F.TT,E4)
                        TT.CUR = R.TT<TT.TE.CURRENCY.1>
                        IF TT.CUR EQ 'EGP' THEN
                            TT.AMT = R.TT<TT.TE.AMOUNT.LOCAL.1>
                        END
                        IF TT.CUR NE 'EGP' THEN
                            TT.AMT = R.TT<TT.TE.AMOUNT.FCY.1>
                        END
                        TT.AC.NO = R.TT<TT.TE.ACCOUNT.1>

*** ACCOUNT BALANCE UPDATED BY KHALED 2010/10/17 ******
                        CALL F.READ(FN.AC,TT.AC.NO,R.AC,F.AC,E2)
                        CUR.BAL = R.AC<AC.WORKING.BALANCE>
                        OLD.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
*******************************************************
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,TT.AC.NO,LOCAL.REF)
                        TT.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,TT.AC.NO,OLD.REF)
                        TT.OLD.NO = OLD.REF
                        TT.AUTH1 = R.TT<TT.TE.OVERRIDE>
                        TT.OVER = R.TT<TT.TE.OVERRIDE>

                        TT.AUTH2 = FIELD(TT.AUTH1,'SCB.',2)
                        IF TT.AUTH2 NE '' THEN
                            TT.AUTH3 = 'SCB.':TT.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,TT.AUTH3,AUTH.NAME)

                            IF AUTH.NAME EQ '' THEN
                                TT.AUTH = R.TT<TT.TE.AUTHORISER>
                            END ELSE
                                TT.AUTH = AUTH.NAME
                            END

                        END ELSE
                            TT.AUTH1 = R.TT<TT.TE.AUTHORISER>
                            TT.AUTH3 = FIELD(TT.AUTH1,"_",2)
                            TT.AUTH4 = TT.AUTH3[1,3]
                            IF TT.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,TT.AUTH3,AUTH.NAME)
                                TT.AUTH = AUTH.NAME
                            END ELSE
                                TT.AUTH = TT.AUTH1
                            END
                        END
                        TT.DD.TT = R.TT<TT.TE.DATE.TIME>
*------------------------------------------------------------------------
                        TT.DATE1 = TT.DD.TT[1,2]
                        TT.DATE2 = TT.DD.TT[3,2]
                        TT.DATE3 = TT.DD.TT[5,2]
                        TT.DATE  = TT.DATE3:"/":TT.DATE2:"/":TT.DATE1
*------------------------------------------------------------------------
                        TT.TIME1 = TT.DD.TT[7,2]
                        TT.TIME2 = TT.DD.TT[9,2]
                        TT.TIME  = TT.TIME2:":":TT.TIME1
*------------------------------------------------------------------------
                        XX2  = SPACE(132)
                        XX6  = SPACE(132)
                        XXX6  = SPACE(132)
                        XX10 = SPACE(132)

                        XX2<1,1>[1,20]   = TT.AC.NO
                        XX6<1,1>[1,20]   = TT.OLD.NO
                        XX2<1,1>[20,35]  = TT.CUS.NAME
                        XX6<1,1>[22,35]  = EXC.KEY
                        XX2<1,1>[57,10]  = TT.CUR
                        XX2<1,1>[72,10]  = TT.AMT
                        XX5<1,1>[72,10]  = CUR.BAL
                        XX2<1,1>[87,10]  = TT.TIME
                        XX5<1,1>[87,10]  = OLD.BAL
                        XX2<1,1>[103,10] = TT.DATE
                        XX2<1,1>[118,35] = TT.AUTH
                        XX6<1,1>[118,35] = TT.OVER2

                        PRINT XX2<1,1>
                        PRINT XX6<1,1>
                        PRINT XXX6<1,1>
                        PRINT XX10<1,1>
                        PRINT STR('-',130)
                    NEXT L
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'TF' AND EXC.KEY.LEN EQ '12' THEN
                T.SEL4 = "SELECT ":FN.LC: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...DRMT...)"
                CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
                IF SELECTED4 THEN
                    FOR M = 1 TO SELECTED4
                        CALL F.READ(FN.LC,KEY.LIST4<M>,R.LC,F.LC,E5)
                        LC.AMT = R.LC<TF.LC.PROVIS.AMOUNT>
                        LC.CUR = R.LC<TF.LC.LC.CURRENCY>
                        LC.AC.NO = R.LC<TF.LC.APPLICANT.ACC>

*** ACCOUNT BALANCE UPDATED BY KHALED 2010/10/17 ******
                        CALL F.READ(FN.AC,LC.AC.NO,R.AC,F.AC,E2)
                        CUR.BAL = R.AC<AC.WORKING.BALANCE>
                        OLD.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
*******************************************************
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,LC.AC.NO,LOCAL.REF)
                        LC.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,LC.AC.NO,OLD.REF)
                        LC.OLD.NO = OLD.REF
                        LC.AUTH1 = R.LC<TF.LC.OVERRIDE>
                        LC.OVER = R.LC<TF.LC.OVERRIDE>

                        LC.AUTH2 = FIELD(LC.AUTH1,'SCB.',2)
                        IF LC.AUTH2 NE '' THEN
                            LC.AUTH3 = 'SCB.':LC.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,LC.AUTH3,AUTH.NAME)

                            IF AUTH.NAME EQ '' THEN
                                LC.AUTH = R.LC<TF.LC.AUTHORISER>
                            END ELSE
                                LC.AUTH = AUTH.NAME
                            END
                        END ELSE
                            LC.AUTH1 = R.LC<TF.LC.AUTHORISER>
                            LC.AUTH3 = FIELD(LC.AUTH1,"_",2)
                            LC.AUTH4 = LC.AUTH3[1,3]
                            IF LC.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,LC.AUTH3,AUTH.NAME)
                                LC.AUTH = AUTH.NAME
                            END ELSE
                                LC.AUTH = LC.AUTH1
                            END
                        END
                        LC.DD.TT = R.LC<TF.LC.DATE.TIME>
*------------------------------------------------------------------------
                        LC.DATE1 = LC.DD.TT[1,2]
                        LC.DATE2 = LC.DD.TT[3,2]
                        LC.DATE3 = LC.DD.TT[5,2]
                        LC.DATE  = LC.DATE3:"/":LC.DATE2:"/":LC.DATE1
*------------------------------------------------------------------------
                        LC.TIME1 = LC.DD.TT[7,2]
                        LC.TIME2 = LC.DD.TT[9,2]
                        LC.TIME  = LC.TIME2:":":LC.TIME1
*------------------------------------------------------------------------
                        XX3 = SPACE(132)
                        XX7 = SPACE(132)
                        XXX7 = SPACE(132)
                        XX11 = SPACE(132)
                        XX3<1,1>[1,20]    = LC.AC.NO
                        XX7<1,1>[1,20]    = LC.OLD.NO
                        XX3<1,1>[20,35]   = LC.CUS.NAME
                        XX7<1,1>[22,35]   = EXC.KEY
                        XX3<1,1>[57,10]   = LC.CUR
                        XX3<1,1>[72,10]   = LC.AMT
                        XX7<1,1>[72,10]   = CUR.BAL
                        XX3<1,1>[87,10]   = LC.TIME
                        XX7<1,1>[87,10]   = OLD.BAL
                        XX3<1,1>[103,10]  = LC.DATE
                        XX3<1,1>[118,35]  = LC.AUTH
                        XX7<1,1>[118,35]  = LC.OVER2

                        PRINT XX3<1,1>
                        PRINT XX7<1,1>
                        PRINT XXX7<1,1>
                        PRINT XX11<1,1>
                        PRINT STR('-',130)

                    NEXT M
                END
            END
*************************************************************************
            IF EXC.KEY.ID EQ 'TF' AND EXC.KEY.LEN EQ '14' THEN
                T.SEL5 = "SELECT ":FN.DR: " WITH @ID EQ ":EXC.KEY:" AND (OVERRIDE LIKE ...DRMT...)"
                CALL EB.READLIST(T.SEL5,KEY.LIST5,"",SELECTED5,ER.MSG5)
                IF SELECTED5 THEN
                    FOR KK = 1 TO SELECTED5
                        CALL F.READ(FN.DR,KEY.LIST5<KK>,R.DR,F.DR,E6)
                        DR.AMT = R.DR<TF.DR.DOCUMENT.AMOUNT>
                        DR.CUR = R.DR<TF.DR.DRAW.CURRENCY>
                        DR.AC.NO = R.DR<TF.DR.DRAWDOWN.ACCOUNT>

*** ACCOUNT BALANCE UPDATED BY KHALED 2010/10/17 ******
                        CALL F.READ(FN.AC,DR.AC.NO,R.AC,F.AC,E2)
                        CUR.BAL = R.AC<AC.WORKING.BALANCE>
                        OLD.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
*******************************************************
                        CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,DR.AC.NO,LOCAL.REF)
                        DR.CUS.NAME = LOCAL.REF
                        CALL DBR ('ACCOUNT':@FM:AC.ALT.ACCT.ID,DR.AC.NO,OLD.REF)
                        DR.OLD.NO = OLD.REF
                        DR.AUTH1 = R.DR<TF.DR.OVERRIDE>
                        DR.OVER = R.DR<TF.DR.OVERRIDE>

                        DR.AUTH2 = FIELD(DR.AUTH1,'SCB.',2)
                        IF DR.AUTH2 NE '' THEN
                            DR.AUTH3 = 'SCB.':DR.AUTH2
                            CALL DBR ('USER':@FM:EB.USE.USER.NAME,DR.AUTH3,AUTH.NAME)

                            IF AUTH.NAME EQ '' THEN
                                DR.AUTH = R.DR<TF.DR.AUTHORISER>
                            END ELSE
                                DR.AUTH = AUTH.NAME
                            END
                        END ELSE
                            DR.AUTH1 = R.DR<TF.DR.AUTHORISER>
                            DR.AUTH3 = FIELD(DR.AUTH1,"_",2)
                            DR.AUTH4 = DR.AUTH3[1,3]
                            IF DR.AUTH4 EQ 'SCB' THEN
                                CALL DBR ('USER':@FM:EB.USE.USER.NAME,DR.AUTH3,AUTH.NAME)
                                DR.AUTH = AUTH.NAME
                            END ELSE
                                DR.AUTH = DR.AUTH1
                            END
                        END
                        DR.DD.TT = R.DR<TF.DR.DATE.TIME>
*------------------------------------------------------------------------
                        DR.DATE1 = DR.DD.TT[1,2]
                        DR.DATE2 = DR.DD.TT[3,2]
                        DR.DATE3 = DR.DD.TT[5,2]
                        DR.DATE  = DR.DATE3:"/":DR.DATE2:"/":DR.DATE1
*------------------------------------------------------------------------
                        DR.TIME1 = DR.DD.TT[7,2]
                        DR.TIME2 = DR.DD.TT[9,2]
                        DR.TIME  = DR.TIME2:":":DR.TIME1
*------------------------------------------------------------------------
                        XX12 = SPACE(132)
                        XX13 = SPACE(132)
                        XXX13 = SPACE(132)
                        XX14 = SPACE(132)
                        XX12<1,1>[1,20]    = DR.AC.NO
                        XX13<1,1>[1,20]    = DR.OLD.NO
                        XX12<1,1>[20,35]   = DR.CUS.NAME
                        XX13<1,1>[22,35]   = EXC.KEY
                        XX12<1,1>[57,10]   = DR.CUR
                        XX12<1,1>[72,10]   = DR.AMT
                        XX13<1,1>[72,10]   = CUR.BAL
                        XX12<1,1>[87,10]   = DR.TIME
                        XX13<1,1>[87,10]   = OLD.BAL
                        XX12<1,1>[103,10]  = DR.DATE
                        XX12<1,1>[118,35]  = DR.AUTH
                        XX13<1,1>[118,35]  = DR.OVER2

                        PRINT XX12<1,1>
                        PRINT XX13<1,1>
                        PRINT XXX13<1,1>
                        PRINT XX14<1,1>
                        PRINT STR('-',130)
                    NEXT KK
                END
            END
        NEXT I
    END
*------------------------------------------------
    XX25 = SPACE(132)
    XX25<1,1>[50,35] = '***  ����� �������  ***'
    PRINT " "
    PRINT XX25<1,1>
    RETURN
*===============================================================
PRINT.HEAD:
*---------
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,SCB.CO,BRANCH)
    TD1 = TODAY[1,4]
    TD2 = TODAY[5,2]
    TD3 = TODAY[7,2]
    TD = TD1:"/":TD2:"/":TD3

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(1):'���� ��� ����� ��������'
    PR.HD :="'L'":" "
*    PR.HD :="'L'":SPACE(50):"���� �������� ���� ��� ��� ����� ��������  ":TD
    PR.HD :="'L'":SPACE(50): "���� �������� ���� ��� ��� ����� �������� �� �����  " :EXC.DATE
    PR.HD :="'L'":SPACE(48):STR('_',45)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� ������" :SPACE(10):"�����":SPACE(27):"������":SPACE(10):"������":SPACE(10):"�����":SPACE(10):"�������":SPACE(10):"���� �������"
    PR.HD :="'L'":"����� ������":SPACE(10):"������":SPACE(40):"����� �����":SPACE(5):"����� ���"
    PR.HD :="'L'":STR('_',130)
    HEADING PR.HD
    RETURN
*==============================================================
END
