* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*****************M.MOSTAFA 25/10/2017***********************************
* Create ofs to expand user fixed end date profile to Next Year     *
*********************************************************************
    PROGRAM SBD.USER.END.45

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
    KK1  = 0
    TDD  = TODAY
    TDYEAR = TDD[1,4]
    FIXED.END.D = TDYEAR:'0131'
    NXT.YEAR = TDYEAR + 1
    FIXED.NXT.END = NXT.YEAR:'0131'
    CUS.ID   = ''
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN

*------------------------------
INITIALISE:
    FN.USER = "F.USER" ; F.USER  = '' ; R.USER = '' ; ERR.USER = '' ; CALL OPF(FN.USER,F.USER)
    FN.CUS = "FBNK.CUSTOMER" ; F.CUS  = '' ; R.CUS = '' ; ERR.CUS = '' ; CALL OPF(FN.CUS,F.CUS)
    FN.OFS.SOURCE    ="F.OFS.SOURCE"
    F.OFS.SOURCE     = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "USER"
    OFS.OPTIONS      = "SCB"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""
    OFS.USER.INFO    = ""

    COMP = 'EG0010001'
    COM.CODE = COMP[2]
    OFS.USER.INFO = "INPUTT01":"/":"/" :COMP

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA    = ","
    KEY.LIST = "" ; SELECTED        = "" ;  ER.MSG = ""
    FN.COMP  = 'F.COMPANY' ; F.COMP = ''
    F.PATH  = FN.OFS.IN

***********************************
***********************************
    T.SEL  = "SELECT ":FN.USER:" WITH @ID LIKE SCB.... AND @ID UNLIKE ...AUTO... AND @ID UNLIKE ...OFS... AND @ID NE SCB.13"
    T.SEL := " AND  END.DATE.PROFILE GT ":TODAY:" AND USER.NAME NE '' "

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    CRT SELECTED
    LOOP
        REMOVE USER.ID FROM KEY.LIST SETTING POS.USR
    WHILE USER.ID:POS.USR
        KK1++
        CALL F.READ(FN.USER,USER.ID,R.USER,F.USER,E1)
        IF R.USER<EB.USE.DATE.LAST.SIGN.ON> THEN
            DIFF.DAYS = "C"
            CALL CDD("",R.USER<EB.USE.DATE.LAST.SIGN.ON>,TODAY,DIFF.DAYS)
        END

        IF DIFF.DAYS GE 45 THEN
*DEBUG
            USER.E.DAT = TODAY
            USER.NAME = R.USER<EB.USE.USER.NAME>
            CRT USER.ID:"--":USER.NAME
            GOSUB OFS.REC
        END
    REPEAT
    RETURN
****************************************************
OFS.REC:
*--------
*************************************
    OFS.MESSAGE.DATA  = "END.DATE.PROFILE=":USER.E.DAT
*************************************
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:USER.ID:COMMA:OFS.MESSAGE.DATA
    OFS.ID = "T":TNO:".USER.":USER.ID
    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    CRT USER.ID:"--":USER.NAME:"--":CUS.ID:"--":FIXED.NXT.END
    RETURN
*********************************************
END
*********************************************************
