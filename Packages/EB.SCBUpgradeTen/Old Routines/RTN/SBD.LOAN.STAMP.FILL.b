* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBD.LOAN.STAMP.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOAN.STAMP

*---------------------------------------
    FN.SLS = "F.SCB.LOAN.STAMP"  ; F.SLS  = ""
    CALL OPF (FN.SLS,F.SLS)

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"  ; F.LD  = ""
    CALL OPF (FN.LD,F.LD)

    FN.PD = "FBNK.PD.PAYMENT.DUE"  ; F.PD  = ""
    CALL OPF (FN.PD,F.PD)


    TD = TODAY[1,6]
    WS.99 = '99...'
    WS.90 = '90...'
*----------------------------------------
**** ACCOUNT SELECT *********

    T.SEL = "SELECT ":FN.AC:" WITH CUSTOMER NE '' AND @ID UNLIKE ":WS.99:" AND CATEGORY UNLIKE ":WS.90:" AND OPEN.ACTUAL.BAL LT 0 AND OPEN.ACTUAL.BAL NE '' AND WITHOUT CATEGORY IN (1002 1205 1206 1207 1208 1701) BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            WS.STAMP.ID = KEY.LIST<I>:'.':TD
            WS.AMT      = R.AC<AC.OPEN.ACTUAL.BAL>

            IF WS.AMT LT 0 THEN WS.AMT = WS.AMT * -1

            WS.CCY      = R.AC<AC.CURRENCY>
            WS.CUS      = R.AC<AC.CUSTOMER>
            WS.CATEG    = R.AC<AC.CATEGORY>
            WS.COMP     = R.AC<AC.CO.CODE>
            WS.LD.NO    = ''
            GOSUB WR.DATA
        NEXT I
    END
***************************************************
***** LOANS SELECT *****

    T.SEL1 = "SELECT ":FN.LD:" WITH CATEGORY GE 21050 AND CATEGORY LE 21074 AND STATUS NE 'LIQ'"

    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.LD,KEY.LIST1<K>,R.LD,F.LD,E2)
            WS.STAMP.ID = KEY.LIST1<K>:'.':TD
            WS.LD.AMT   = R.LD<LD.AMOUNT>
            WS.CCY      = R.LD<LD.CURRENCY>
            WS.CUS      = R.LD<LD.CUSTOMER.ID>
            WS.CATEG    = R.LD<LD.CATEGORY>
            WS.COMP     = R.LD<LD.CO.CODE>
            WS.LD.NO    = KEY.LIST1<K>

            WS.PD.ID  = 'PD':WS.LD.NO
            CALL F.READ(FN.PD,WS.PD.ID,R.PD,F.PD,ERR.PD)
            IF NOT(ERR.PD) THEN
                WS.PD.AMT = R.PD<PD.TOTAL.OVERDUE.AMT>
            END ELSE
                WS.PD.AMT = 0
            END

            WS.AMT = WS.LD.AMT + WS.PD.AMT

            GOSUB WR.DATA
        NEXT I
    END


*****************************
    RETURN
*------------------------------------------------------------------
WR.DATA:
*-------

    CALL F.READ(FN.SLS,WS.STAMP.ID,R.SLS,F.SLS,E4)

    R.SLS<SLS.CUSTOMER.ID> = WS.CUS
    R.SLS<SLS.CATEGORY>    = WS.CATEG
****
    AMT.CHK = R.SLS<SLS.AMOUNT>
    IF AMT.CHK GT WS.AMT THEN
        R.SLS<SLS.AMOUNT> = AMT.CHK
    END ELSE
****
        R.SLS<SLS.AMOUNT> = WS.AMT
    END

    R.SLS<SLS.CURRENCY>    = WS.CCY
    R.SLS<SLS.DATE>        = TD
    R.SLS<SLS.LD.NO>       = WS.LD.NO
    R.SLS<SLS.BRANCH>      = WS.COMP

    CALL F.WRITE(FN.SLS,WS.STAMP.ID,R.SLS)
    CALL JOURNAL.UPDATE(WS.STAMP.ID)

*----------------------------------
    RETURN

************************************************************
END
