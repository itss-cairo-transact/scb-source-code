* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>99</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.LG.LIQUIDITY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
    COMP = ID.COMPANY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 45 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.LG.LIQUIDITY'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:
    LG.AMT = ''
    CUR = ''
    MARG = ''
    LIM.REF = ''
    BB = ''
    WW = ''
    FF = ''
    RR = ''
    CUR.TOT = ''
    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    CUR.SEL = "SELECT FBNK.CURRENCY"
    CUR.KEY.LIST ="" ; CUR.SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(CUR.SEL,CUR.KEY.LIST,"",CUR.SELECTED,ER.MSG)

    FOR K = 1 TO CUR.SELECTED
        T.SEL= "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH CATEGORY EQ 21096 AND CURRENCY EQ ":CUR.KEY.LIST<K>:" AND CO.CODE EQ ":COMP:" AND STATUS NE 'LIQ'"
        KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED > 0 THEN GOSUB GET.RECORDS
    NEXT K

    PRINT " ����� ������ ������ ���� ������� = ":FF
    RETURN
*-----------------------------------
GET.RECORDS:

    IF KEY.LIST THEN
        LIM.LG = '' ; TOT.LG ='' ; TOT.MARG = ''

        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            LOCAL.REF = R.LD<LD.LOCAL.REF>
            LG.AMT<I> =R.LD<LD.AMOUNT>
            CUR<I>=R.LD<LD.CURRENCY>
            MARG<I>=LOCAL.REF<1,LDLR.MARGIN.AMT>
            LIM.REF<I>=R.LD<LD.LIMIT.REFERENCE>
            IF LIM.REF<I> = 7020.01 THEN LIM.LG = LIM.LG + LG.AMT<I>

            TOT.LG = TOT.LG + LG.AMT<I>
            TOT.MARG = TOT.MARG + MARG<I>
            DIFF = TOT.LG-TOT.MARG
        NEXT I

*********  To  GET  Equivalent For The Amount  ***********************************
        IF CUR<1> # LCCY THEN

            LAMT = "" ; RATE = "" ; DIF.AMT = "" ; DIF.RATE  = "" ; R.COUNT = ''
            AMT= LIM.LG
            CR   = CUR<1>
            MARKET = 1

            IF CR # LCCY THEN
                CALL MIDDLE.RATE.CONV.CHECK(AMT,CR,RATE,MARKET,LAMT,DIF.AMT,DIF.RATE)
            END
            CUR.TOT = CUR.TOT + LAMT
        END ELSE
            BB = BB + LIM.LG
            WW = WW + BB
            LAMT = WW
        END

*************************************************************************************
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END
    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 131 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE CUR<I> IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    MYRATE = R.CCY<RE.BCP.RATE,POS>

    HH = DIFF/2-LAMT/2
   * FF = FF + HH
    RR = TOT.LG * MYRATE
    FF = FF + RR
    XX = SPACE(80)
    XX<1,1>[1,3]   = CUR<1>
    XX<1,1>[10,3]   = SELECTED
    XX<1,1>[20,15]   = TOT.LG
    XX<1,1>[40,15]   = TOT.MARG
    XX<1,1>[50,15]   = DIFF
    XX<1,1>[70,15]   = DIFF/2
    XX<1,1>[90,15]   = LAMT/2
    XX<1,1>[105,15]   = RR

    PRINT XX<1,1>
    PRINT
*PRINT SPACE(1):HH:SPACE(5):LAMT/2:SPACE(5):DIFF/2:SPACE(5):DIFF:SPACE(5):TOT.MARG:SPACE(3):TOT.LG:SPACE(5):SELECTED:SPACE(10):CUR<1>

    RETURN
*===============================================================
PRINT.HEAD:
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*    YYBRN = FIELD(BRANCH,'.',2)

    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    YYBRN  = BRANCH
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(35):"��� " :YYBRN  :SPACE(35) :"SBD.LG.LIQUIDITY"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(30):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(25):" ��� ������ �������� � ��� ����� ������� "
    PR.HD :="'L'":SPACE(28):STR('_',30)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"������":SPACE(3):"�����":SPACE(5):"������":SPACE(10):"�������" :SPACE(5):" ��� ��� ����" :SPACE(5):" 50 %" :SPACE(10):" 50 % ����" :SPACE(10):" ������� "
    PR.HD :="'L'":SPACE(1):STR('_',102)

    HEADING PR.HD
    RETURN
*==============================================================
END
