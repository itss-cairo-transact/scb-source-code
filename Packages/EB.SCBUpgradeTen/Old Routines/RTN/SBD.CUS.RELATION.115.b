* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*--- CREATED BY NESSMA ON 2019/03/20
**    PROGRAM SBD.CUS.RELATION.115
    SUBROUTINE SBD.CUS.RELATION.115

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RELATION.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.POSTING.RESTRICT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.QUALITY
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RISK.RATING
*-----------------------------------------------
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    TEXT = "FINISHED"  ; CALL REM
    RETURN
*---------------------------------------
INITIAL:
*--------
    OPENSEQ "/home/signat" , "SBD.CUS.RELATION.115.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/home/signat":' ':"SBD.CUS.RELATION.115.CSV"
        HUSH OFF
    END
    OPENSEQ "/home/signat" , "SBD.CUS.RELATION.115.CSV" TO BB THEN
        CREATE BB THEN
            PRINT 'FILE SBD.CUS.RELATION.115.CSV CREATED IN /home/signat'
        END ELSE
            STOP 'Cannot create SBD.CUS.RELATION.115.CSV FILE File IN /home/signat'
        END
    END

    FN.CUS = "FBNK.CUSTOMER"  ; F.CUS = ""
    CALL OPF(FN.CUS, F.CUS)

    FN.CR = "FBNK.RELATION.CUSTOMER" ; F.CR = ""
    CALL OPF(FN.CR, F.CR)

    T.SEL    = "" ; SEL.LIST = "" ; NO.OF.REC = "" ; ERR.SEL = ""
    CUS.ID   = "" ; R.CUS    = "" ; ERR.CUS   = ""

    BB.DATA    = ""
    RETURN
*---------------------------------------
PRINT.HEAD:
*-----------
    BB.DATA   = "SBD.CUS.RELATION.115,,,,,"
    BB.DATA  := ",,,,,":" OUTPUT DATE : ":TODAY:",,,,,,,,,,,"
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA    = "ID":","
    BB.DATA   := "COMPANY.NAME":","
**    BB.DATA   := "CUSTOMER.CODE":","
    BB.DATA   := "CUSTOMER.NUMBER":","
    BB.DATA   := "CUSTOMER.NAME":",,,,"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*---------------------------------------
PROCESS:
*-------
    T.SEL  = "SELECT FBNK.RELATION.CUSTOMER WITH IS.RELATION EQ 115"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,SEL.LIST,"",NO.OF.REC,ERR.SEL)
    IF NO.OF.REC THEN
        FOR NN = 1 TO NO.OF.REC
            CALL F.READ(FN.CR,SEL.LIST<NN>,R.CR,F.CR,ERR.CR)
            FFF = DCOUNT(R.CR<EB.RCU.IS.RELATION>, @VM)

            BB.DATA  = SEL.LIST<NN>:","
            CALL F.READ(FN.CUS,SEL.LIST<NN>,R.CUS,F.CUS,ERR.CUS)
            BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:","
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            FOR XX = 1 TO FFF
                BB.DATA = ",,":R.CR<EB.RCU.OF.CUSTOMER><1,XX>:","
                CALL F.READ(FN.CUS,R.CR<EB.RCU.OF.CUSTOMER><1,XX>,R.CUS,F.CUS,ERR.CUS)
                BB.DATA := R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>:","
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            NEXT XX
        NEXT NN
    END
    RETURN
*-------------------------------------------------------
END
