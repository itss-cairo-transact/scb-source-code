* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBM.BUS.LINE.AVRG.02

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE.AVRG
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE.AVRG.EGP
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BUS.LINE.AVRG.FCY
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS.ALL
    WS.BAL.RET = 0 ; WS.BAL.CORP = 0 ; WS.BAL.HQ = 0 ; WS.BAL.TRE = 0 ; WS.TOTAL = 0
    GOSUB PROCESS.EGP
    WS.BAL.RET = 0 ; WS.BAL.CORP = 0 ; WS.BAL.HQ = 0 ; WS.BAL.TRE = 0 ; WS.TOTAL = 0
    GOSUB PROCESS.FCY

    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========

    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.AVRG.CSV" TO AA THEN
        CLOSESEQ AA
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"BUSINESS.LINE.AL.AVRG.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.AVRG.CSV" TO AA ELSE
        CREATE AA THEN
            PRINT 'FILE BUSINESS.LINE.AL.AVRG.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create BUSINESS.LINE.AL.AVRG.CSV File IN &SAVEDLISTS&'
        END
    END

*----------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.AVRG.EGP.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"BUSINESS.LINE.AL.AVRG.EGP.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.AVRG.EGP.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE BUSINESS.LINE.AL.AVRG.EGP.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create BUSINESS.LINE.AL.AVRG.EGP.CSV File IN &SAVEDLISTS&'
        END
    END

*---------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.AVRG.FCY.CSV" TO CC THEN
        CLOSESEQ CC
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"BUSINESS.LINE.AL.AVRG.FCY.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "BUSINESS.LINE.AL.AVRG.FCY.CSV" TO CC ELSE
        CREATE CC THEN
            PRINT 'FILE BUSINESS.LINE.AL.AVRG.FCY.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create BUSINESS.LINE.AL.AVRG.FCY.CSV File IN &SAVEDLISTS&'
        END
    END
*---------------------------------

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = TODAY[1,6]:'01'
    CALL CDT("",TD,'-1C')

    DAT.HED = FMT(TD,"####/##/##")

    HEAD.DESC  = DAT.HED:","
    HEAD.DESC := "CORPORATE":","
    HEAD.DESC := "RETAIL":","
    HEAD.DESC := "TREASURY":","
    HEAD.DESC := "HQ":","
    HEAD.DESC := "TOTAL":","

    AA.DATA = HEAD.DESC
    WRITESEQ AA.DATA TO AA ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    CC.DATA = HEAD.DESC
    WRITESEQ CC.DATA TO CC ELSE
        PRINT " ERROR WRITE FILE "
    END

*--------------------------------------
    FN.BU = 'F.SCB.BUS.LINE.AVRG' ; F.BU = ''
    CALL OPF(FN.BU,F.BU)

    FN.BUE = 'F.SCB.BUS.LINE.AVRG.EGP' ; F.BUE = ''
    CALL OPF(FN.BUE,F.BUE)

    FN.BUF = 'F.SCB.BUS.LINE.AVRG.FCY' ; F.BUF = ''
    CALL OPF(FN.BUF,F.BUF)

    WS.BAL.RET = 0 ; WS.BAL.CORP = 0 ; WS.BAL.HQ = 0 ; WS.BAL.TRE = 0 ; WS.TOTAL = 0
*--------------------------------------

    WS.DAT.END.MONTH = TODAY[1,6]:'01'

    CALL CDT("",WS.DAT.END.MONTH,'-1C')

    WS.DAT.START.MONTH = WS.DAT.END.MONTH[1,6]:'01'

    WS.DAYS =  WS.DAT.END.MONTH[2]

    WS.TOTAL.ID = WS.DAT.END.MONTH:'-TOTAL'

    RETURN
*-----------------------------------------------------------------------------
PROCESS.ALL:
*=======
    T.SEL = "SELECT ":FN.BU:" WITH @ID EQ ":WS.TOTAL.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BU,KEY.LIST<I>,R.BU,F.BU,E1)

*Line [ 159 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.COL = DCOUNT(R.BU<BLA.DESCRIPTION>,@VM)
            FOR X = 1 TO DECOUNT.COL
                WS.DESC     = R.BU<BLA.DESCRIPTION,X>
                WS.BAL.CORP = R.BU<BLA.CORPORATE,X> / WS.DAYS
                WS.BAL.RET  = R.BU<BLA.RETAIL,X> / WS.DAYS
                WS.BAL.TRE  = R.BU<BLA.TREASURY,X> / WS.DAYS
                WS.BAL.HQ   = R.BU<BLA.HQ,X> / WS.DAYS
                WS.TOTAL    = R.BU<BLA.TOTAL,X> / WS.DAYS


                AA.DATA  = WS.DESC:","
                AA.DATA := WS.BAL.CORP:","
                AA.DATA := WS.BAL.RET:","
                AA.DATA := WS.BAL.TRE:","
                AA.DATA := WS.BAL.HQ:","
                AA.DATA := WS.TOTAL:","

                WRITESEQ AA.DATA TO AA ELSE
                    PRINT " ERROR WRITE FILE "
                END
            NEXT X
        NEXT I
    END
    RETURN
*====================================================
PROCESS.EGP:
*=======
    T.SEL = "SELECT ":FN.BUE:" WITH @ID EQ ":WS.TOTAL.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BUE,KEY.LIST<I>,R.BUE,F.BUE,E1)

*Line [ 193 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.COL = DCOUNT(R.BUE<BLA.DESCRIPTION>,@VM)
            FOR X = 1 TO DECOUNT.COL
                WS.DESC     = R.BUE<BLA.DESCRIPTION,X>
                WS.BAL.CORP = R.BUE<BLA.CORPORATE,X> / WS.DAYS
                WS.BAL.RET  = R.BUE<BLA.RETAIL,X> / WS.DAYS
                WS.BAL.TRE  = R.BUE<BLA.TREASURY,X> / WS.DAYS
                WS.BAL.HQ   = R.BUE<BLA.HQ,X> / WS.DAYS
                WS.TOTAL    = R.BUE<BLA.TOTAL,X> / WS.DAYS


                BB.DATA  = WS.DESC:","
                BB.DATA := WS.BAL.CORP:","
                BB.DATA := WS.BAL.RET:","
                BB.DATA := WS.BAL.TRE:","
                BB.DATA := WS.BAL.HQ:","
                BB.DATA := WS.TOTAL:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            NEXT X
        NEXT I
    END
    RETURN
*====================================================
PROCESS.FCY:
*=======
    T.SEL = "SELECT ":FN.BUF:" WITH @ID EQ ":WS.TOTAL.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BUF,KEY.LIST<I>,R.BUF,F.BUF,E1)

*Line [ 227 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.COL = DCOUNT(R.BUF<BLA.DESCRIPTION>,@VM)
            FOR X = 1 TO DECOUNT.COL
                WS.DESC     = R.BUF<BLA.DESCRIPTION,X>
                WS.BAL.CORP = R.BUF<BLA.CORPORATE,X> / WS.DAYS
                WS.BAL.RET  = R.BUF<BLA.RETAIL,X> / WS.DAYS
                WS.BAL.TRE  = R.BUF<BLA.TREASURY,X> / WS.DAYS
                WS.BAL.HQ   = R.BUF<BLA.HQ,X> / WS.DAYS
                WS.TOTAL    = R.BUF<BLA.TOTAL,X> / WS.DAYS


                CC.DATA  = WS.DESC:","
                CC.DATA := WS.BAL.CORP:","
                CC.DATA := WS.BAL.RET:","
                CC.DATA := WS.BAL.TRE:","
                CC.DATA := WS.BAL.HQ:","
                CC.DATA := WS.TOTAL:","

                WRITESEQ CC.DATA TO CC ELSE
                    PRINT " ERROR WRITE FILE "
                END
            NEXT X
        NEXT I
    END
    RETURN
*====================================================
