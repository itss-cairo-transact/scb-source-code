* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.CRT.TRIAL.03
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS
***---------------------------------------------------
    FN.LC = "FBNK.LETTER.OF.CREDIT"
    F.LC  = ""

***---------------------------------------------------
    FN.CY = "FBNK.CURRENCY"
    F.CY  = ""
*------------------------------------------------------
    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

***----------------------------------------------------
    FN.CAT = "F.CATEG.MAS"
    F.CAT  = ""
***----------------------------------------------------
    WS.CY = ""
    WS.CATEG = ""
    WS.CATEG.KEY = ""
    WS.CY.CODE = ""
    WS.RATE = ""
    MSG.CY = ""
    MSG.AC = ""
    MSG.CAT = ""
    R.CAT = ""
    R.CY = ""
    WS.BAL = 0
    WS.COMN.AREA = 0
    WS.MLT.DIVD  = ""
    R.STAT = ""
    WS.TMP = 0
    A.ARY = ""
    CYPOS = ""
    WS.RATE.ARY = ""
    WS.MLT.DIVD.ARY = ""

    WS.KEY = "NZD"

***----------------------------------------------------
    CALL OPF (FN.LC,F.LC)
    CALL OPF (FN.CY,F.CY)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.CAT,F.CAT)

***----------------------------------------------------
    SEL.CMD = "SELECT ":FN.LC
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.LC.ID FROM SEL.LIST SETTING POS
    WHILE WS.LC.ID:POS
        CALL F.READ(FN.LC,WS.LC.ID,R.LC,F.LC,MSG.LC)

        IF R.LC<TF.LC.LIABILITY.AMT>  EQ 0 THEN
            GOTO AAAA
        END
***********************************************

        WS.CO.CODE =  R.LC<TF.LC.CO.CODE>
        WS.BR =  WS.CO.CODE[2]
        WS.CATEG = R.LC<TF.LC.CATEGORY.CODE>

        WS.BAL = R.LC<TF.LC.LIABILITY.AMT>
        WS.CY = R.LC<TF.LC.LC.CURRENCY>


*************************************************


        IF WS.CY EQ "EGP" THEN
            WS.CY.CODE = 10
            WS.RATE = 1
        END

        IF WS.CY NE "EGP" THEN
            GOSUB A.100.CY
        END
        IF WS.CY.CODE EQ 0 THEN
            GOTO AAAA
        END
        GOSUB A.200.CHK.REC
AAAA:

    REPEAT
BBBB:
    RETURN
*--------------------------------------------------------
***-------------------------------------------------------
A.100.CY:
    CALL F.READ(FN.CY,WS.CY,R.CY,F.CY,MSG.CY)
    IF MSG.CY EQ "" THEN
        WS.CY.CODE = R.CY<EB.CUR.NUMERIC.CCY.CODE>
        GOSUB A.110.CY
        RETURN
    END


    WS.RATE = 0
    WS.MLT.DIVD = ""
    WS.CY.CODE = 0
    RETURN
***-------------------------------------------------------
A.110.CY:
    WS.KEY = "NZD"
    CALL F.READ(FN.CCY,WS.KEY,R.CCY,F.CCY,MSG.CCY)
    IF MSG.CCY EQ "" THEN
        A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 137 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        LOCATE WS.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
        WS.RATE.ARY = R.CCY<RE.BCP.RATE>
        WS.RATE = WS.RATE.ARY<1,CYPOS>
        WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
        WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>
        RETURN
    END
    RETURN
*-------------------------------------------------
A.200.CHK.REC:
    MSG.CAT = ""
    WS.CATEG.KEY = WS.BR:"*":WS.CATEG:WS.CY.CODE
    CALL F.READ(FN.CAT,WS.CATEG.KEY,R.CAT,F.CAT,MSG.CAT)
    IF MSG.CAT EQ "" THEN
        GOSUB A.300.OLD.REC
        RETURN
    END
***   IF MSG.CAT NE "" THEN
    GOSUB A.400.NEW.REC
***  END

    RETURN
**---------------------------------------------------------
A.300.OLD.REC:
    WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.ACT.CY>
    WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
    R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.COMN.AREA1

    IF WS.CY EQ "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.COMN.AREA1
    END

    IF WS.CY NE "EGP" THEN
        GOSUB A.310.CALC.EQV
    END
***-------------------------------
**�������� ����� �������
***-------------------------------
    IF  WS.CATEG EQ 1001 OR  WS.CATEG EQ 1002  THEN
        GOSUB A.320.REV.CR
    END

    IF  WS.CATEG GE 6501 AND  WS.CATEG LE 6504  THEN
        GOSUB A.320.REV.CR
    END

    IF  WS.CATEG GE 1101 AND   WS.CATEG LE 1102  THEN
        GOSUB A.330.REV.DR
    END

    IF  WS.CATEG GE 1200 AND   WS.CATEG LE 1299 THEN
        GOSUB A.330.REV.DR
    END

    IF  WS.CATEG GE 1400 AND   WS.CATEG LE 1499 THEN
        GOSUB A.330.REV.DR
    END

    IF  WS.CATEG GE 1500 AND   WS.CATEG LE 1599 THEN
        GOSUB A.330.REV.DR
    END


    CALL F.WRITE(FN.CAT,WS.CATEG.KEY,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CATEG.KEY)
    RETURN

A.310.CALC.EQV:
    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.COMN.AREA = WS.BAL * WS.RATE
    END

    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.COMN.AREA = WS.BAL / WS.RATE
    END
    WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.LOCAL.CY>
    WS.COMN.AREA1 = WS.COMN.AREA1 + WS.COMN.AREA
    R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.COMN.AREA1
    RETURN
A.320.REV.CR:
    IF WS.BAL GT 0 THEN
        RETURN
    END
    IF WS.CY EQ "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.COMN.AREA1

        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> =  WS.COMN.AREA1
    END

    IF WS.CY NE "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.COMN.AREA1

        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.COMN.AREA
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> =  WS.COMN.AREA1

    END
    RETURN
A.330.REV.DR:
    IF WS.BAL LT 0 THEN
        RETURN
    END
    IF WS.CY EQ "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.COMN.AREA1

        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> =   WS.COMN.AREA1
    END

    IF WS.CY NE "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.COMN.AREA1

        WS.COMN.AREA1 = R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.COMN.AREA
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> =   WS.COMN.AREA1
    END
    RETURN
**---------------------------------------------------------
A.400.NEW.REC:
    R.CAT<CAT.CAT.CATEG.CODE> =  WS.CATEG
*    R.CAT<CAT.CAT.ASST.LIAB> =
    R.CAT<CAT.CAT.CY.NMRC.CODE> = WS.CY.CODE
    R.CAT<CAT.CAT.CY.ALPH.CODE> = WS.CY
    R.CAT<CAT.CAT.BR> =  WS.BR
    R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.BAL

    IF WS.CY EQ "EGP" THEN
        R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.BAL
    END

    IF WS.CY NE "EGP" THEN
        GOSUB A.410.CALC.EQV
    END
    R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  0
    R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> = 0
***-------------------------------
**�������� ����� �������
***-------------------------------
    IF  WS.CATEG EQ 1001 OR  WS.CATEG EQ 1002  THEN
        GOSUB A.420.REV.CR
    END

    IF  WS.CATEG GE 6501 AND  WS.CATEG LE 6504  THEN
        GOSUB A.420.REV.CR
    END

    IF  WS.CATEG GE 1101 AND   WS.CATEG LE 1102  THEN
        GOSUB A.430.REV.DR
    END

    IF  WS.CATEG GE 1200 AND   WS.CATEG LE 1299 THEN
        GOSUB A.430.REV.DR
    END

    IF  WS.CATEG GE 1400 AND   WS.CATEG LE 1499 THEN
        GOSUB A.430.REV.DR
    END

    IF  WS.CATEG GE 1500 AND   WS.CATEG LE 1599 THEN
        GOSUB A.430.REV.DR
    END

    CALL F.WRITE(FN.CAT,WS.CATEG.KEY,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CATEG.KEY)
    RETURN

A.410.CALC.EQV:
    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.COMN.AREA = WS.BAL * WS.RATE
    END

    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.COMN.AREA = WS.BAL / WS.RATE
    END
    R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.COMN.AREA
    RETURN

A.420.REV.CR:
    IF WS.BAL GT 0 THEN
        RETURN
    END
    IF WS.CY EQ "EGP" THEN
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> = WS.BAL
    END

    IF WS.CY NE "EGP" THEN
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> =  WS.COMN.AREA
    END
    RETURN
A.430.REV.DR:
    IF WS.BAL LT 0 THEN
        RETURN
    END
    IF WS.CY EQ "EGP" THEN
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> = WS.BAL
    END
    IF WS.CY NE "EGP" THEN
        R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  WS.BAL
        R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> =   WS.COMN.AREA
    END
    RETURN

*--------------------------------------------------------
END
