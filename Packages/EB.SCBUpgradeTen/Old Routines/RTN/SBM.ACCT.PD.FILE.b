* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBM.ACCT.PD.FILE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.PD
*----------------------------------------
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.APD = 'F.SCB.ACCT.PD' ; F.APD = ''
    CALL OPF(FN.APD,F.APD)

    END.DATE = TODAY[1,6]:'01'
    CALL CDT("",END.DATE,'-1C')

    FROM.DATE = END.DATE[1,6]:'01'

    DAYS    = 0
    WS.DATE = END.DATE[1,6]

    T.SEL = "SELECT ":FN.AC:" WITH CATEGORY IN (1236 1486 1487 1488 1507 1515 1516 1525 1526 1530 1531 1532 1568 1572 1574 1584) BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            ACC.ID.NO = KEY.LIST<I>
            CALL EB.ACCT.ENTRY.LIST(ACC.ID.NO,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)

            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,ER.AC)
            WS.CUST     = R.AC<AC.CUSTOMER>
            WS.CATEG    = R.AC<AC.CATEGORY>
            WS.OPEN.BAL = OPENING.BAL

            APD.ID = KEY.LIST<I>:'.':FROM.DATE[1,6]
            CALL F.READ(FN.APD,APD.ID,R.APD,F.APD,ER.APD)
            IF ER.APD THEN
                R.APD<APD.CUST.NO>  = WS.CUST
                R.APD<APD.CATEGORY> = WS.CATEG
                R.APD<APD.OPEN.BAL> = WS.OPEN.BAL
                R.APD<APD.DATE>     = WS.DATE

                CALL F.WRITE(FN.APD,APD.ID,R.APD)
                CALL JOURNAL.UPDATE(APD.ID)
            END
            K = 1
            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS.STE
            WHILE STE.ID:POS.STE
                IF STE.ID THEN

                    CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                    STE.AMT  = R.STE<AC.STE.AMOUNT.LCY>
                    STE.DATE = R.STE<AC.STE.VALUE.DATE>

                    R.APD<APD.TRN.DATE,K> = STE.DATE
                    R.APD<APD.TRN.BAL,K>  = STE.AMT

                    CALL F.WRITE(FN.APD,APD.ID,R.APD)
                    CALL JOURNAL.UPDATE(APD.ID)
                    K++

                END
            REPEAT
            K = 0

        NEXT I
    END

    RETURN
END
