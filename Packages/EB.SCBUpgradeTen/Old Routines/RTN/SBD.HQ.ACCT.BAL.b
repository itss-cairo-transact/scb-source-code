* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.HQ.ACCT.BAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
    $INCLUDE T24.BP I_F.USER         ;*EB.USE.
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
    $INCLUDE T24.BP I_F.ACCOUNT      ;*AC.
    $INCLUDE T24.BP I_F.DEPT.ACCT.OFFICER      ;*EB.DAO.
    $INCLUDE T24.BP I_F.RE.BASE.CCY.PARAM      ;*RE.BCP.
    $INCLUDE T24.BP I_F.RE.STAT.LINE.BAL       ;*RE.SLB.
    $INCLUDE T24.BP I_USER.ENV.COMMON   ;*EB.COM.
    COMP = ID.COMPANY

*    TDY = ''
*    CALL TXTINP("���� ����� �����  ", 8, 22, 14, TDY)
*    TD1 = COMI

    TD1 = TODAY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
*Line [ 40 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.HQ.ACCT.BAL'
    CALL PRINTER.ON(REPORT.ID,'')
    ACC.ID = ''
    STE.BR = ''
    STE.CNF.DR = ''
    STE.CNF.CR = ''
    RETURN
*===============================================================
CALLDB:

    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL  = '' ; R.BAL  = ''
    FN.BAL2= 'FBNK.RE.STAT.LINE.BAL' ; F.BAL2 = '' ; R.BAL2 = ''

    FN.CUR = 'FBNK.RE.BASE.CCY.PARAM' ; F.CUR = '' ; R.CUR = ''

    FN.ACC  = 'F.ACCOUNT' ; F.ACC  = '' ; R.ACC  = ''
    FN.ACC2 = 'F.ACCOUNT' ; F.ACC2 = '' ; R.ACC2 = ''

    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.BAL2,F.BAL2)
    CALL OPF(FN.CUR,F.CUR)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.ACC2,F.ACC2)
    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG =""
    KEY.LIST1 ="" ; SELECTED1 ="" ;  ER.MSG1 =""
    X = 0
    ACC.ID      = 0
    ACC.ID2     = 0
    TOT.IN      = 0
    TOT.OUT     = 0
    TOT.TR.DR   = 0
    TOT.TR.CR   = 0
    TOT.TR.DR2  = 0
    TOT.TR.CR2  = 0
    TOT.TR.DR3  = 0
    TOT.TR.CR3  = 0
    TOT.TR.DR4  = 0
    TOT.TR.CR4  = 0
    TOTAL.DEBITS    = 0
    TOTAL.CREDITS   = 0
    TOTAL.DEBITS2   = 0
    TOTAL.CREDITS2  = 0
    TOTAL.DEBITS3   = 0
    TOTAL.CREDITS3  = 0
    TOTAL.DEBITS4   = 0
    TOTAL.CREDITS4  = 0
    BR.OUT.DR   = 0
    BR.OUT.CR   = 0
    CUR.TYPE    = ''
    TOT.ALL.DR  = 0
    TOT.ALL.CR  = 0
    TOT.ALL.DR2 = 0
    TOT.ALL.CR2 = 0
    TOTAL.DR    = 0
    TOTAL.CR    = 0
    OP.BAL.DR   = 0
    OP.BAL.CR   = 0
    TOTAL.DR2   = 0
    TOTAL.CR2   = 0
    OP.BAL.DR2  = 0
    OP.BAL.CR2  = 0
    OP.BAL.LCL  = 0
    OP.BAL.LCL2 = 0
    CL.BAL.LCL  = 0
    CL.BAL.LCL2 = 0
    CL.BAL.DR   = 0
    CL.BAL.CR   = 0
    CLOSE.BAL   = 0
    CLOSE.BAL   = 0
    CLOSE.BAL.CRR   = 0
    CLOSE.BAL.DRR   = 0
    CL.BAL.LCL  = 0
    CL.BAL.LCL2 = 0
    CL.BAL.DR2  = 0
    CL.BAL.CR2  = 0
    CLOSE.BAL2  = 0
    CLOSE.BAL2  = 0
    CLOSE.BAL.CRR2   = 0
    CLOSE.BAL.DRR2   = 0
    AMT.IN.DR   = 0
    AMT.IN.CR   = 0
    AMT.OUT.DR  = 0
    AMT.OUT.CR  = 0
    STE.NRV1= ''
    XX1  = SPACE(120)
    XX2  = SPACE(120)
    XX11 = SPACE(120)
    XX12 = SPACE(120)
    XX13 = SPACE(120)
    XX14 = SPACE(120)
    XX15 = SPACE(120)
    XX21 = SPACE(120)
    XX22 = SPACE(120)
    XX23 = SPACE(120)
    XX24 = SPACE(120)
    XX25 = SPACE(120)
    XX31 = SPACE(120)
    XX32 = SPACE(120)

    K = 0
    CCC = ''
*    USER.COMPANY = COMP
    USER.COMPANY = COMP
    USER.CO = USER.COMPANY[4]
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*If the date required is end of month and not today
*and last day of month is holiday then
*make the date processed for LINE (line date) equal last day of month
*otherwise if date required equal day before holiday then
*make the line date equal the day before next working day
    TDD = ''
    TDX = TODAY
    TDL = TD1
    IF TD1 NE TDX THEN
        CALL CDT("",TDL,'+1W')
        TDR = TDL
        TDN = TDL
        CALL CDT("",TDR,'-1W')
        CALL CDT("",TDN,'-1C')
        TDLMN = TDL[5,2]
        TDRMN = TDR[5,2]
        IF TDLMN NE TDRMN THEN
            TDREM = TDL[1,4]:TDLMN:'01'
            CALL CDT("",TDREM,'-1C')
            TDD = TDREM
        END ELSE
            IF TDR NE TDN THEN
                TDD = TDN
            END ELSE
                TDD = TD1
            END
        END
    END
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*************************************************************************
    IF TD1 EQ TDX THEN
        ACC.ID = 'EGP115000099':USER.CO
        A.SEL = "SELECT ":FN.ACC:" WITH @ID EQ EGP115000099":USER.CO:" OR @ID EQ EGP115050099":USER.CO
        CALL EB.READLIST(A.SEL,KEY.LIST1,"",SELECTED1,ER.MSG1)
        IF SELECTED1 THEN
            FOR A = 1 TO SELECTED1
                CALL F.READ(FN.ACC,KEY.LIST1<A>,R.ACC,F.ACC,EA1)
                OP.BAL.LCL += R.ACC<AC.OPEN.ACTUAL.BAL>
            NEXT A
        END
    END ELSE
*-----------------------------------------------------------------
        BAL.ID = "GENLED-0620-LOCAL-":TDD:"*":USER.COMPANY
        CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,EM1)
        IF EM1 THEN
            BAL.ID = "GENLED-0111-LOCAL-":TDD:"*":USER.COMPANY
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,EMX1)
        END
        OP.BAL.LCL = R.BAL<RE.SLB.OPEN.BAL.LCL>
*-----------------------------------------------------------------
    END
    IF OP.BAL.LCL LE 0 THEN
        OP.BAL.DR = OP.BAL.LCL
    END ELSE
        OP.BAL.CR = OP.BAL.LCL
    END
    GOSUB PRINT13
*************************************************************************
*--------------------------------------------------------------------------------------
    IF TD1 EQ TDX THEN
        F.SEL  = "SELECT ":FN.ACC2:" WITH @ID LIKE ...115000099":USER.CO
        F.SEL := " AND @ID UNLIKE EGP..."
        CALL EB.READLIST(F.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    END ELSE
        B.SEL = "SELECT ":FN.BAL2:" WITH @ID LIKE GENLED-0820-...-":TDD:"*":USER.COMPANY
        B.SEL := " AND @ID UNLIKE ...LOCAL..."
        CALL EB.READLIST(B.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF NOT(SELECTED) THEN
            B.SEL = "SELECT ":FN.BAL2:" WITH @ID LIKE GENLED-0311-...-":TDD:"*":USER.COMPANY
            B.SEL := " AND @ID UNLIKE ...LOCAL..."
            CALL EB.READLIST(B.SEL,KEY.LIST,"",SELECTED,ER.MSGX)
        END
    END
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            IF TD1 EQ TDX THEN
                CALL F.READ(FN.ACC2,KEY.LIST<I>,R.ACC2,F.ACC2,EA2)
                ACC.ID2 = KEY.LIST<I>
                CUR.COD = ACC.ID2[1,3]
            END ELSE
                CALL F.READ(FN.BAL2,KEY.LIST<I>,R.BAL2,F.BAL2,EM2)
                BAL.ID2 = KEY.LIST<I>
                CUR.COD = FIELD(BAL.ID2,'-',3)
            END
            CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,EC2)
*Line [ 237 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CUR.COD IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            CUR.RATE  = R.CUR<RE.BCP.RATE,POS>
            RATE.TYPE = R.CUR<RE.BCP.RATE.TYPE,POS>
            IF RATE.TYPE EQ 'MULTIPLY' THEN
                IF TD1 EQ TDX THEN
                    OP.BAL.LCL2 += R.ACC2<AC.OPEN.ACTUAL.BAL> * CUR.RATE
                END ELSE
                    OP.BAL.LCL2 += R.BAL2<RE.SLB.OPEN.BAL> * CUR.RATE
                END
            END
            IF RATE.TYPE EQ 'DIVIDE' THEN
                IF TD1 EQ TDX THEN
                    OP.BAL.LCL2 += R.ACC2<AC.OPEN.ACTUAL.BAL> / CUR.RATE
                END ELSE
                    OP.BAL.LCL2 += R.BAL2<RE.SLB.OPEN.BAL> / CUR.RATE
                END
            END
        NEXT I
    END
*---------------------------------------------------------------------------------
    OP.BAL.LCL2 = DROUND(OP.BAL.LCL2,2)

    IF OP.BAL.LCL2 LE 0 THEN
        OP.BAL.DR2 = OP.BAL.LCL2
    END ELSE
        OP.BAL.CR2 = OP.BAL.LCL2
    END

    GOSUB PRINT23

*----------------------------------------------------------
*   OPEN LCY FILE BR
*----------------------------------------------------------
    SEQ.FILE.NAME = '&SAVEDLISTS&'
    RECORD.NAME = 'ACCT_HQ.':COMP
    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        TEXT = 'Unable to Locate ':SEQ.FILE.POINTER  ; CALL REM
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
*-----------------------------------------------------
            R.L.DATE = FIELD(Y.MSG,"|",2)
            IF R.L.DATE EQ TD1 THEN
                R.L.CODE = FIELD(Y.MSG,"|",1)
                TOTAL.DEBITS  = FIELD(Y.MSG,"|",3)
                TOTAL.CREDITS = FIELD(Y.MSG,"|",4)
                TOT.TR.DR += TOTAL.DEBITS
                TOT.TR.CR += TOTAL.CREDITS
                IF R.L.CODE EQ 11 THEN
                    GOSUB PRINT11
                END
                IF R.L.CODE EQ 12 THEN
                    GOSUB PRINT12
                END
            END
        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ SEQ.FILE.POINTER

*-----------------------------------------------------
*   OPEN FCY FILE   BR
*-----------------------------------------------------

    SEQ.FILE.NAME2 = '&SAVEDLISTS&'
    RECORD.NAME2 = 'ACCT_HQ2.':COMP
    OPENSEQ SEQ.FILE.NAME2,RECORD.NAME2 TO SEQ.FILE.POINTER2 ELSE
        TEXT = 'Unable to Locate ':SEQ.FILE.POINTER2  ; CALL REM
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG2 FROM SEQ.FILE.POINTER2 THEN
*-----------------------------------------------------
            R.L.DATE2 = FIELD(Y.MSG2,"|",2)
            IF R.L.DATE2 EQ TD1 THEN
                R.L.CODE2 = FIELD(Y.MSG2,"|",1)
                TOTAL.DEBITS2  = FIELD(Y.MSG2,"|",3)
                TOTAL.CREDITS2 = FIELD(Y.MSG2,"|",4)
                TOT.TR.DR2 += TOTAL.DEBITS2
                TOT.TR.CR2 += TOTAL.CREDITS2
                IF R.L.CODE2 EQ 21 THEN
                    GOSUB PRINT21
                END
                IF R.L.CODE2 EQ 22 THEN
                    GOSUB PRINT22
                END
            END
        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ SEQ.FILE.POINTER2

*-----------------------------------------------------
*   OPEN LCY FILE HQ
*-----------------------------------------------------
    SEQ.FILE.NAME3 = '&SAVEDLISTS&'
    RECORD.NAME3 = 'ACCT_HQ3.':COMP
    OPENSEQ SEQ.FILE.NAME3,RECORD.NAME3 TO SEQ.FILE.POINTER3 ELSE
        TEXT = 'Unable to Locate ':SEQ.FILE.POINTER3  ; CALL REM
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG3 FROM SEQ.FILE.POINTER3 THEN
*-----------------------------------------------------
            R.L.DATE3 = FIELD(Y.MSG3,"|",2)
            IF R.L.DATE3 EQ TD1 THEN
                R.L.CODE3 = FIELD(Y.MSG3,"|",1)
                TOTAL.DEBITS3  = FIELD(Y.MSG3,"|",3)
                TOTAL.CREDITS3 = FIELD(Y.MSG3,"|",4)
                TOT.TR.DR3 += TOTAL.DEBITS3
                TOT.TR.CR3 += TOTAL.CREDITS3
                GOSUB PRINT31
                GOSUB PRINT1
                GOSUB PRINT14
            END
        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ SEQ.FILE.POINTER3

*-----------------------------------------------------
*   OPEN FCY FILE HQ
*-----------------------------------------------------

    SEQ.FILE.NAME4 = '&SAVEDLISTS&'
    RECORD.NAME4 = 'ACCT_HQ4.':COMP
    OPENSEQ SEQ.FILE.NAME4,RECORD.NAME4 TO SEQ.FILE.POINTER4 ELSE
        TEXT = 'Unable to Locate ':SEQ.FILE.POINTER4  ; CALL REM
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG4 FROM SEQ.FILE.POINTER4 THEN
*-----------------------------------------------------
            R.L.DATE4 = FIELD(Y.MSG4,"|",2)
            IF R.L.DATE4 EQ TD1 THEN
                R.L.CODE4 = FIELD(Y.MSG4,"|",1)
                TOTAL.DEBITS4  = FIELD(Y.MSG4,"|",3)
                TOTAL.CREDITS4 = FIELD(Y.MSG4,"|",4)
                TOT.TR.DR4 += TOTAL.DEBITS4
                TOT.TR.CR4 += TOTAL.CREDITS4
*                IF R.L.CODE4 EQ 41 THEN
                GOSUB PRINT32
                GOSUB PRINT2
                GOSUB PRINT24
*                END
            END
        END ELSE
            EOF = 1
        END
    REPEAT

    CLOSESEQ SEQ.FILE.POINTER4

*===============================================

    CUR.TYPE = '(���� �����)'
    GOSUB PRINT.HEAD
    GOSUB PRINTALL1
    CUR.TYPE = '(���� ������)'
    GOSUB PRINT.HEAD
    GOSUB PRINTALL2
    RETURN
*===============================================
PRINT11:
    XX11 = SPACE(120)
    XX11<1,K>[1,20] = '����� ��������� ������� '
    XX11<1,K>[35,15]= TOTAL.DEBITS
    XX11<1,K>[55,15]= TOTAL.CREDITS
    TOTAL.DEBITS = 0
    TOTAL.CREDITS = 0
    RETURN
PRINT12:
    XX12 = SPACE(120)
    XX12<1,K>[1,20] = '����� ��������� �������'
    XX12<1,K>[35,15]= TOTAL.DEBITS
    XX12<1,K>[55,15]= TOTAL.CREDITS
    TOTAL.DEBITS = 0
    TOTAL.CREDITS = 0
    RETURN
PRINT31:
    XX31 = SPACE(120)
    XX31<1,K>[1,20] = '����� ����� (�) �'
    XX31<1,K>[35,15]= TOT.TR.DR3
    XX31<1,K>[55,15]= TOT.TR.CR3
    TOTAL.DEBITS = 0
    TOTAL.CREDITS = 0
    RETURN
PRINT1:
    XX1 = SPACE(120)
    XX1<1,K>[1,20] = '������ ������ �������'
    XX1<1,K>[35,15]= TOT.TR.DR + TOT.TR.DR3
    XX1<1,K>[55,15]= TOT.TR.CR + TOT.TR.CR3
    RETURN
PRINT13:
    XX13 = SPACE(120)
    XX13<1,K>[1,20] = '���� ��������'
    XX13<1,K>[35,15]= OP.BAL.DR
    XX13<1,K>[55,15]= OP.BAL.CR
    RETURN
PRINT14:
    CLOSE.BAL.DR = OP.BAL.DR + TOT.TR.DR + TOT.TR.DR3
    CLOSE.BAL.CR = OP.BAL.CR + TOT.TR.CR + TOT.TR.CR3
    CLOSE.BAL = CLOSE.BAL.DR + CLOSE.BAL.CR
    IF CLOSE.BAL LE 0 THEN
        CLOSE.BAL.DRR = CLOSE.BAL
        CLOSE.BAL.CRR = 0
    END ELSE
        CLOSE.BAL.CRR = CLOSE.BAL
        CLOSE.BAL.DRR = 0
    END
    XX14 = SPACE(120)
    XX14<1,K>[1,20] = '���� �������'
    XX14<1,K>[35,15]= CLOSE.BAL.DRR
    XX14<1,K>[55,15]= CLOSE.BAL.CRR
    RETURN
PRINT15:
    XX15 = SPACE(120)
    XX15<1,K>[1,20] = '��������'
    IF CLOSE.BAL.CRR NE 0 THEN
        XX15<1,K>[35,15]= CLOSE.BAL.CRR + (TOT.TR.DR*-1) + (TOT.TR.DR3*-1)
        XX15<1,K>[55,15]= OP.BAL.CR + TOT.TR.CR + TOT.TR.CR3
    END ELSE
        XX15<1,K>[35,15]= OP.BAL.DR + TOT.TR.DR + TOT.TR.DR3
        XX15<1,K>[55,15]= CLOSE.BAL.DRR + (TOT.TR.CR*-1) + (TOT.TR.CR3*-1)
    END
    RETURN
PRINT21:
    XX21 = SPACE(120)
    XX21<1,K>[1,20] = '����� ��������� ������� '
    XX21<1,K>[35,15]= TOTAL.DEBITS2
    XX21<1,K>[55,15]= TOTAL.CREDITS2
    TOTAL.DEBITS = 0
    TOTAL.CREDITS = 0
    RETURN
PRINT22:
    XX22 = SPACE(120)
    XX22<1,K>[1,20] = '����� ��������� �������'
    XX22<1,K>[35,15]= TOTAL.DEBITS2
    XX22<1,K>[55,15]= TOTAL.CREDITS2
    TOTAL.DEBITS = 0
    TOTAL.CREDITS = 0
    RETURN
PRINT32:
    XX32 = SPACE(120)
    XX32<1,K>[1,20] = '����� ����� (�) �'
    XX32<1,K>[35,15]= TOT.TR.DR4
    XX32<1,K>[55,15]= TOT.TR.CR4
    TOTAL.DEBITS4 = 0
    TOTAL.CREDITS4 = 0
    RETURN
PRINT2:
    XX2 = SPACE(120)
    XX2<1,K>[1,20] = '������ ������ �������'
    XX2<1,K>[35,15]= TOT.TR.DR2 + TOT.TR.DR4
    XX2<1,K>[55,15]= TOT.TR.CR2 + TOT.TR.CR4
    RETURN
PRINT23:
    XX23 = SPACE(120)
    XX23<1,K>[1,20] = '���� ��������'
    XX23<1,K>[35,15]= OP.BAL.DR2
    XX23<1,K>[55,15]= OP.BAL.CR2

    RETURN
PRINT24:
    CLOSE.BAL.DR2 = OP.BAL.DR2 + TOT.TR.DR2 + TOT.TR.DR4
    CLOSE.BAL.CR2 = OP.BAL.CR2 + TOT.TR.CR2 + TOT.TR.CR4
    CLOSE.BAL2 = CLOSE.BAL.DR2 + CLOSE.BAL.CR2
    IF CLOSE.BAL2 LE 0 THEN
        CLOSE.BAL.DRR2 = CLOSE.BAL2
        CLOSE.BAL.CRR2 = 0
    END ELSE
        CLOSE.BAL.CRR2 = CLOSE.BAL2
        CLOSE.BAL.DRR2 = 0
    END
    XX24 = SPACE(120)
    XX24<1,K>[1,20] = '���� �������'
    XX24<1,K>[35,15]= CLOSE.BAL.DRR2
    XX24<1,K>[55,15]= CLOSE.BAL.CRR2
    RETURN
PRINT25:
    XX25 = SPACE(120)
    XX25<1,K>[1,20] = '��������'
    IF CLOSE.BAL.CRR2 NE 0 THEN
        XX25<1,K>[35,15]= CLOSE.BAL.CRR2 + (TOT.TR.DR2*-1) + (TOT.TR.DR4*-1)
        XX25<1,K>[55,15]= OP.BAL.CR2 + TOT.TR.CR2 + TOT.TR.CR4
    END ELSE
        XX25<1,K>[35,15]= OP.BAL.DR2 + TOT.TR.DR2 + TOT.TR.DR4
        XX25<1,K>[55,15]= CLOSE.BAL.DRR2 + (TOT.TR.CR2*-1) + (TOT.TR.CR4*-1)
    END
    RETURN
*...................................
PRINTALL1:
    GOSUB PRINT15
    PRINT XX11<1,K>
    PRINT XX12<1,K>
    PRINT XX31<1,K>
    PRINT STR('-',60)
    PRINT XX1<1,K>
    PRINT XX13<1,K>
    PRINT XX14<1,K>
    PRINT XX15<1,K>
    RETURN
*...................................
PRINTALL2:
    GOSUB PRINT25
    PRINT XX21<1,K>
    PRINT XX22<1,K>
    PRINT XX32<1,K>
    PRINT STR('-',60)
    PRINT XX2<1,K>
    PRINT XX23<1,K>
    PRINT XX24<1,K>
    PRINT XX25<1,K>
    RETURN
*...................................
*===============================================================
PRINT.HEAD:
*---------
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)

    TD = TD1
*    CALL CDT("",TD,'-1W')
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH
    T.DAY1  = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(50):"���� ������ ������ �������":T.DAY1:SPACE(20):CUR.TYPE
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"������":SPACE(30):"����":SPACE(15):'����'
    PR.HD :="'L'":STR('_',120)
    HEADING PR.HD
    PRINT
    RETURN
*==============================================================
END
