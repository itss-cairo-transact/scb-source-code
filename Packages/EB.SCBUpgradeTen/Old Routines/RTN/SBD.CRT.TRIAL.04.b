* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.CRT.TRIAL.04
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*    $INSERT GLOBUS.BP  I_F.RE.BASE.CCY.PARAM
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS
***---------------------------------------------------
    FN.CONSOL = "FBNK.CONSOLIDATE.PRFT.LOSS"
    F.CONSOL  = ""

***---------------------------------------------------
    FN.CY = "FBNK.CURRENCY"
    F.CY  = ""
*------------------------------------------------------
*    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
*    F.CCY  = ""

***----------------------------------------------------
    FN.CAT = "F.CATEG.MAS"
    F.CAT  = ""
***----------------------------------------------------
    WS.CY = ""
    WS.CATEG = ""
    WS.CATEG.KEY = ""
    WS.CY.CODE = ""
    WS.RATE = ""
    MSG.CY = ""
    MSG.AC = ""
    MSG.CAT = ""
    R.CAT = ""
    R.CY = ""
    WS.BAL.LCY = 0
    WS.COMN.AREA = 0
    WS.MLT.DIVD  = ""
    R.STAT = ""
    WS.TMP = 0
    A.ARY = ""
    CYPOS = ""
    WS.RATE.ARY = ""
    WS.MLT.DIVD.ARY = ""
    WS.BAL.FCY = 0
    WS.KEY = "NZD"

***----------------------------------------------------
    CALL OPF (FN.CONSOL,F.CONSOL)
    CALL OPF (FN.CY,F.CY)
*   CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.CAT,F.CAT)

***----------------------------------------------------
*    SEL.CMD = "SELECT ":FN.CONSOL:" WITH @ID EQ PL.52151.1..1001.........EG0010001"
    SEL.CMD = "SELECT ":FN.CONSOL
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CON.ID FROM SEL.LIST SETTING POS
    WHILE WS.CON.ID:POS
        CALL F.READ(FN.CONSOL,WS.CON.ID,R.CONSOL,F.CONSOL,MSG.CONSOL)

***********************************************
*       WS.ID.BR = WS.CON.ID[2]
*       WS.CO.CODE =  WS.ID.BR
        WS.BR = WS.CON.ID[2]
        WS.CATEG = R.CONSOL<RE.PTL.VARIABLE.1>
        WS.REPT = DCOUNT(R.CONSOL<RE.PTL.CURRENCY>,@VM)
        WS.ARY.CY = R.CONSOL<RE.PTL.CURRENCY>
        WS.ARY.BAL = R.CONSOL<RE.PTL.BALANCE.YTD>
        WS.ARY.BAL.FCY = R.CONSOL<RE.PTL.CCY.BALANCE.YTD>

        GOSUB  A.050.COUNT
*?        WS.BAL = R.CONSOL<TF.CONSOL.LIABILITY.AMT>
*?        WS.CY = R.CONSOL<TF.CONSOL.LC.CURRENCY>


*************************************************


*?        IF WS.CY EQ "EGP" THEN
*?           WS.CY.CODE = 10
*?            WS.RATE = 1
*?        END

*?        IF WS.CY NE "EGP" THEN
*?            GOSUB A.100.CY
*?        END
*?        IF WS.CY.CODE EQ 0 THEN
*?            GOTO AAAA
*?        END
*?        GOSUB A.200.CHK.REC
AAAA:

    REPEAT
BBBB:
    RETURN
A.050.COUNT:
    FOR  WS.COUNT = 1 TO WS.REPT

        WS.CY =  WS.ARY.CY<1,WS.COUNT>
        GOSUB A.053.CHK
    NEXT WS.COUNT
    RETURN
*--------------------------------------------------------
A.053.CHK:
        IF WS.CY EQ "EGP" THEN
            WS.CY.CODE = 10
        END
    IF WS.CY EQ "EGP" THEN
        WS.BAL.LCY = WS.ARY.BAL<1,WS.COUNT>
        WS.BAL.FCY = WS.ARY.BAL<1,WS.COUNT>
        GOSUB A.200.CHK.REC
    END
    IF WS.CY NE "EGP" THEN
        WS.BAL.LCY = WS.ARY.BAL<1,WS.COUNT>
        WS.BAL.FCY = WS.ARY.BAL.FCY<1,WS.COUNT>
        GOSUB A.100.CY
        GOSUB A.200.CHK.REC
    END

    RETURN
***-------------------------------------------------------
A.100.CY:
    CALL F.READ(FN.CY,WS.CY,R.CY,F.CY,MSG.CY)
    IF MSG.CY EQ "" THEN
        WS.CY.CODE = R.CY<EB.CUR.NUMERIC.CCY.CODE>
*        GOSUB A.110.CY
        RETURN
    END


    WS.RATE = 0
    WS.MLT.DIVD = ""
    WS.CY.CODE = 0
    RETURN
***-------------------------------------------------------
*A.110.CY:
*    WS.KEY = "NZD"
*    CALL F.READ(FN.CCY,WS.KEY,R.CCY,F.CCY,MSG.CCY)
*    IF MSG.CCY EQ "" THEN
*        A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*        LOCATE WS.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
*        WS.RATE.ARY = R.CCY<RE.BCP.RATE>
*        WS.RATE = WS.RATE.ARY<1,CYPOS>
*        WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
*        WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>
*        RETURN
*    END
*    RETURN
*-------------------------------------------------
A.200.CHK.REC:
    MSG.CAT = ""
    WS.CATEG.KEY = WS.BR:"*":WS.CATEG:WS.CY.CODE
    CALL F.READ(FN.CAT,WS.CATEG.KEY,R.CAT,F.CAT,MSG.CAT)
    IF MSG.CAT EQ "" THEN
        GOSUB A.300.OLD.REC
        RETURN
    END

    GOSUB A.400.NEW.REC

    RETURN
**---------------------------------------------------------
A.300.OLD.REC:
*    WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.ACT.CY>
*    WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL.LCY
*    R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.COMN.AREA1

    IF WS.CY EQ "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL.LCY
        R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.COMN.AREA1
        WS.COMN.AREA1 =  R.CAT<CAT.CAT.BAL.IN.ACT.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL.LCY
        R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.COMN.AREA1
    END

    IF WS.CY NE "EGP" THEN
        WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.LOCAL.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL.LCY
        R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.COMN.AREA1
        WS.COMN.AREA1 =  R.CAT<CAT.CAT.BAL.IN.ACT.CY>
        WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL.FCY
        R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.COMN.AREA1
*        GOSUB A.310.CALC.EQV
    END


    CALL F.WRITE(FN.CAT,WS.CATEG.KEY,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CATEG.KEY)
    RETURN

*A.310.CALC.EQV:
*    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
*        WS.COMN.AREA = WS.BAL * WS.RATE
*    END

*    IF WS.MLT.DIVD NE "MULTIPLY" THEN
*        WS.COMN.AREA = WS.BAL / WS.RATE
*    END
*    WS.COMN.AREA1 = R.CAT<CAT.CAT.BAL.IN.LOCAL.CY>
*    WS.COMN.AREA1 = WS.COMN.AREA1 + WS.COMN.AREA
*     WS.COMN.AREA1 = WS.COMN.AREA1 + WS.BAL
*    R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.COMN.AREA1
*    RETURN
**---------------------------------------------------------
A.400.NEW.REC:
    R.CAT<CAT.CAT.CATEG.CODE> =  WS.CATEG
*    R.CAT<CAT.CAT.ASST.LIAB> =
    R.CAT<CAT.CAT.CY.NMRC.CODE> = WS.CY.CODE
    R.CAT<CAT.CAT.CY.ALPH.CODE> = WS.CY
    R.CAT<CAT.CAT.BR> =  WS.BR
*   R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.BAL.FCY

    IF WS.CY EQ "EGP" THEN
        R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.BAL.LCY
        R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.BAL.LCY
    END

    IF WS.CY NE "EGP" THEN
        R.CAT<CAT.CAT.BAL.IN.LOCAL.CY> = WS.BAL.LCY
        R.CAT<CAT.CAT.BAL.IN.ACT.CY> =  WS.BAL.FCY
*       GOSUB A.310.CALC.EQV
    END
    R.CAT<CAT.CAT.REV.BAL.IN.ACT.CY> =  0
    R.CAT<CAT.CAT.REV.BAL.IN.LOCAL.CY> = 0

    CALL F.WRITE(FN.CAT,WS.CATEG.KEY,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CATEG.KEY)
    RETURN


*--------------------------------------------------------
END
