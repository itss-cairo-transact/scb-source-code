* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBD.PRT.TRIAL.02.A
**    PROGRAM      SBD.PRT.TRIAL.02.ALL
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*---------------------------------------------------

    FN.CATMAS = "F.CATEG.MAS.D"
    F.CATMAS  = ""

    FN.CAT = "F.CATEGORY"
    F.CAT  = ""

    FN.REPL = "F.RE.STAT.REP.LINE"
    F.REPL = ""

    FN.DATE = "F.DATES"
    F.DATE  = ""


*----------------------------------------------------
    CALL OPF (FN.CATMAS,F.CATMAS)
    CALL OPF (FN.CAT,F.CAT)
    CALL OPF (FN.REPL,F.REPL)
    CALL OPF (FN.DATE,F.DATE)
*----------------------------WORKING-------------------
    WS.CAT = ""
    WS.LOCAL.TOT = 0
    WS.GRP.OF.F = 0
    WS.GRP.COMP = 0
    WS.CY.ALPH.CODE = 0
    MSG.CATMAS = ""
    MSG.CAT = ""
    WS.REV.TOT.BAL.LOCAL =  0
    WS.REV.BAL.LOCAL = 0
    WS.LINE.COUNT = 0
    WS.PAGE.COUNT = 0
    WS.TOTAL.LINE.FRN = 0
    WS.TOTAL.OF.EGP = 0
    WS.TOTAL.ASST.LIAB = 0
    WS.FRST = 0
*-----------------------------------------------
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
** --------------------------------------HEADER OF PRINTER
    WS.HD.T  = "CATEGORY OF TOTAL"
    WS.HD.T2A = "NO CATEG "
    WS.HD.T2B = "NAME CATEG "
    WS.HD.T2C = "BALANCE"
    WS.HD.T2D = "LOCAL IN BALANCE "
    WS.PRG.1 = "SBD.PRT.TRIAL.02.ALL"
*--------------------------- PROCEDURE ---------------------

    WS.AL.CODE = 0
    WS.AL.GRP  = 0
    WS.TOTAL.OF.EGP.COD =  0
    WS.TOTAL.LINE.FRN.COD = 0
    WS.LOCAL.TOT.COD = 0

    WS.TOTAL.LINE = 0
    WS.AL.CODE = 1
    GOSUB A.10.DATE
    GOSUB A.5000.PRT.HEAD
    GOSUB A.100.PROCESS
    GOSUB A.200.LINE.TOT

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN

*------------------------------------
A.10.DATE:
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.WRK.DAT = R.DATE<EB.DAT.LAST.WORKING.DAY>
    RETURN

*------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.CATMAS:" BY CATD.ASST.LIAB BY CATD.CATEG.CODE BY CATD.CY.ALPH.CODE"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CATMAS FROM SEL.LIST SETTING POS
    WHILE WS.CATMAS:POS
        CALL F.READ(FN.CATMAS,WS.CATMAS,R.CATMAS,F.CATMAS,MSG.CATMAS)

        WS.IN.ACT.CY = R.CATMAS<CAT.CATD.BAL.IN.ACT.CY>
        WS.IN.LCL.CY = R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
        WS.LINE = R.CATMAS<CAT.CATD.ASST.LIAB>
        WS.CATEG.CODE = R.CATMAS<CAT.CATD.CATEG.CODE>
        WS.ALPH.CODE  = R.CATMAS<CAT.CATD.CY.ALPH.CODE>

        IF WS.FRST = 0 THEN
            WS.LINE.COMP = WS.LINE
            WS.CATEG.CODE.COMP =  WS.CATEG.CODE
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.FRST = 1
            GOSUB  A.110.GET.REP.LINE.NAME
        END

        IF  WS.LINE.COMP NE WS.LINE  THEN
            GOSUB A.105.PRT.DTAL
            GOSUB A.200.LINE.TOT
            GOSUB A.150.CHK.HEAD
            WS.TOTAL.LINE = 0
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.LINE.COMP = WS.LINE
            WS.CATEG.CODE.COMP =  WS.CATEG.CODE
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
            GOSUB  A.110.GET.REP.LINE.NAME
        END

        IF  WS.CATEG.CODE.COMP NE   WS.CATEG.CODE  THEN
            GOSUB A.105.PRT.DTAL
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.CATEG.CODE.COMP =  WS.CATEG.CODE
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
        END

        IF  WS.ALPH.CODE.COMP  NE  WS.ALPH.CODE  THEN
            GOSUB A.105.PRT.DTAL
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
        END

        WS.GL.NO = FIELD(WS.CATMAS,'*',3)
        WS.GL.CY = FIELD(WS.CATMAS,'*',4)

        WS.IN.ACT.CY.TOT = WS.IN.ACT.CY.TOT + WS.IN.ACT.CY
        WS.IN.LCL.CY.TOT = WS.IN.LCL.CY.TOT + WS.IN.LCL.CY

A.100.A:
    REPEAT
A.100.EXIT:
    RETURN
*------------------------------------------
A.105.PRT.DTAL:
    CALL F.READ(FN.CAT,WS.CATEG.CODE.COMP,R.CAT,F.CAT,MSG.CAT)
    WS.GL.NAME = R.CAT<EB.CAT.DESCRIPTION,2>
    XX = SPACE(132)
    XX<1,1>[1,6] = ""
    XX<1,1>[10,5] = WS.CATEG.CODE.COMP
    XX<1,1>[20,3] = WS.ALPH.CODE.COMP
    XX<1,1>[25,35] = WS.GL.NAME
    XX<1,1>[65,20] = FMT(WS.IN.ACT.CY.TOT, "R2,")
    XX<1,1>[90,20] = FMT(WS.IN.LCL.CY.TOT, "R2,")
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    WS.TOTAL.LINE = WS.TOTAL.LINE + WS.IN.LCL.CY.TOT
    IF WS.LINE.COUNT GT 45  THEN
        GOSUB A.5000.PRT.HEAD
    END
    RETURN
*------------------------------------------
A.110.GET.REP.LINE.NAME:
    WS.REP.KEY = "GENLED.":WS.LINE.COMP
    CALL F.READ(FN.REPL,WS.REP.KEY,R.REPL,F.REPL,MSG.REPL)
    WS.LINE.NAM = R.REPL<RE.SRL.DESC,2,2>
    XX = SPACE(132)
    XX<1,1>[1,5] = WS.LINE.COMP
    XX<1,1>[7,1] = ""
    XX<1,1>[10,35] = WS.LINE.NAM
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,5] = "====="
    XX<1,1>[7,1] = ""
    XX<1,1>[10,35] = STR('=',35)
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 2
    IF WS.LINE.COUNT GT 45  THEN
        GOSUB A.5000.PRT.HEAD
    END


    RETURN
*------------------------------------------
A.150.CHK.HEAD:
*    IF WS.LINE EQ 230 THEN
    IF WS.LINE GT 200 AND WS.AL.CODE EQ 1 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 2
        GOSUB A.5000.PRT.HEAD
    END
*    IF WS.LINE EQ 510 THEN
    IF WS.LINE GT 500 AND WS.AL.CODE EQ 2 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 3
        GOSUB A.5000.PRT.HEAD
    END
*    IF WS.LINE EQ 710 THEN
    IF WS.LINE GT 700 AND WS.AL.CODE EQ 3 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 4
        GOSUB A.5000.PRT.HEAD
    END
*    IF WS.LINE EQ 1010 THEN
    IF WS.LINE GT 1000 AND WS.AL.CODE EQ 4 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 5
        GOSUB A.5000.PRT.HEAD
    END
    RETURN
*------------------------------------------
A.200.LINE.TOT:
    XX = SPACE(132)
    XX<1,1>[1,6] = ""
    XX<1,1>[10,5] = ""
    XX<1,1>[20,3] = ""
    XX<1,1>[25,35] = ""
    XX<1,1>[65,35] = ""
    XX<1,1>[101,20] = FMT(WS.TOTAL.LINE, "R2,")
    PRINT XX<1,1>
    WS.TOTAL.ASST.LIAB = WS.TOTAL.ASST.LIAB + WS.TOTAL.LINE
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    IF WS.LINE.COUNT GT 45  THEN
        GOSUB A.5000.PRT.HEAD
    END
    RETURN
*------------------------------------------
*����� ������ ������ � ������
A.300.PRT.TOT.ASST.LIAB:
    WS.TOT.COMNT = ""
    IF  WS.AL.CODE = 1  THEN
        WS.TOT.COMNT = "������ ���� ����"
    END
    IF  WS.AL.CODE = 2  THEN
        WS.TOT.COMNT = "������ ���� �����"
    END
    IF  WS.AL.CODE = 3  THEN
        WS.TOT.COMNT = "������ ���� ����"
    END
    IF  WS.AL.CODE = 4  THEN
        WS.TOT.COMNT = "������ ���� �����"
    END
    XX = SPACE(132)
    XX<1,1>[1,6] = ""
    XX<1,1>[10,5] = ""
    XX<1,1>[20,3] = ""
    XX<1,1>[25,35] = ""
    XX<1,1>[65,35] = WS.TOT.COMNT
    XX<1,1>[101,20] = FMT(WS.TOTAL.ASST.LIAB, "R2,")
    PRINT XX<1,1>
    WS.TOTAL.ASST.LIAB = 0
    RETURN

*-----------------------------------PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.LINE.COUNT = 0
    WS.PAGE.COUNT = WS.PAGE.COUNT + 1
    IF   WS.AL.CODE  EQ 1 THEN
        WS.AL.NAME = "������ �������"
    END
    IF   WS.AL.CODE  EQ 2 THEN
        WS.AL.NAME = "������ ��������"
    END
    IF   WS.AL.CODE  EQ 3 THEN
        WS.AL.NAME = "������ �������"
    END
    IF   WS.AL.CODE  EQ 4 THEN
        WS.AL.NAME = "������ ��������"
    END
    IF   WS.AL.CODE  EQ 5 THEN
        WS.AL.NAME = "�������� ��������"
    END
    WS.BR.NAME = "������ �����"
    DATY = WS.LAST.WRK.DAT
*    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":SPACE(3) :WS.BR.NAME:SPACE(34):WS.PRG.1:"   PAGE ":WS.PAGE.COUNT
    PR.HD :="'L'":SPACE(53):WS.AL.NAME
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD
    RETURN
END
