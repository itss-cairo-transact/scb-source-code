* @ValidationCode : MjotMTU0OTY4MjU1NDpDcDEyNTI6MTY0MDgxNjY4Mzk1MDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:24:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*------------------------
*** Create By Nessma ***
*------------------------
SUBROUTINE SBD.CALC.SAV.BAL.MILLION
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.RE.BASE.CCY.PARAM
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COMPANY
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CATEGORY
*Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.NUMERIC.CURRENCY
*---------------------------------------------------
    GOSUB INTIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS

*** CATEGORY via CURRENCY
    GOSUB PRINT.HEAD
    GOSUB CALC.TOTALS

*** CURRENCY
    GOSUB PRINT.HEAD.2
    GOSUB CALC.TOT

    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "DONE"  ; CALL REM
RETURN
*----------------------------------------------------------
LINE.END:
*--------
    PRINT " "
    PRINT SPACE(50) : "********* ����� �������  *********"
RETURN
*----------------------------------------------------------
CALC.TOTALS:
*------------
    CATEGORY.LIST<1,1> = '6501'
    CATEGORY.LIST<1,2> = '6502'
    CATEGORY.LIST<1,3> = '6503'
    CATEGORY.LIST<1,4> = '6504'
    CATEGORY.LIST<1,5> = '6511'

    YY         = ""
    NUM.CUR    = ""
    CUR.COD    = ""
    CUR.NAME   = ""
    CUR.NAMR   = ""
    CURR       = ""
    CURRR      = ""
    CATEGORY.N = ""
    CATEGORY.C = ""

    CUR.SEL  = "SELECT ":FN.CNUM:" BY @ID"
    CALL EB.READLIST(CUR.SEL,CUR.LIST,"",SELECTED.CUR,ER.MSG)
    IF SELECTED.CUR THEN
        FOR KK = 1 TO 5
            FOR QQ = 1 TO SELECTED.CUR
                CUR.INDX = CUR.LIST<QQ>
                CATEGORY.C = CATEGORY.LIST<1,KK>

*Line [ 94 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEGORY.C,CATEGORY.N)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEGORY.C,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEGORY.N=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
                YY<1,1>[22,10] = CATEGORY.N
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.INDX,CURR)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CUR.INDX,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CURR=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
*Line [ 109 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CURR,CURRR)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURRR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
                YY<1,1>[53,15]  = CURRR

                NUM.CUR         = TOT.NO.CATEG<CUR.INDX,CATEGORY.C>
                YY<1,1>[85,20] = TOT.NO.CATEG<CUR.INDX,CATEGORY.C>

                YY<1,1>[115,20] = TOT.BL.CATEG<CUR.INDX,CATEGORY.C>

                IF NUM.CUR THEN
                    PRINT YY<1,1>
                    YY<1,1>  = " "
                    PRINT " "
                END

                YY<1,1>  = ""
                NUM.CUR  = ""
                CUR.COD  = ""
                CUR.NAME = ""
                CUR.NAMR = ""
                CURR     = ""
                CURRR    = ""
                CATEGORY.N = ""
                CATEGORY.C = ""
            NEXT QQ
        NEXT KK
    END
RETURN
*-------------------------------------------------------------------
INTIAL:
*------
    REPORT.ID = 'SBM.AC.TOTALS'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC  = 'FBNK.ACCOUNT'        ;  F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER'       ;  F.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.COMP = 'F.COMPANY'          ;  F.COMP  = ''
    CALL OPF(FN.COMP,F.COMP)

    FN.CCC  = 'FBNK.CURRENCY'      ;  F.CCC  = ''
    CALL OPF(FN.CCC,F.CCC)

    FN.CNUM  = 'F.NUMERIC.CURRENCY'   ;  F.CNUM  = ''
    CALL OPF(FN.CNUM,F.CNUM)

    FN.CUR  = 'FBNK.CURRENCY'         ; F.CUR  = ''
    CALL OPF(FN.CUR,F.CUR)

    ENQ.LP   = 0
    CUR.RATE = ""
    COMP.OLD = ""
    COMP.NEW = ""
    CATEG    = ""
    CUR      = ""
    TOT.NO   = 0
    TOT.BAL  = 0
    BAL      = 0
    BREAK.2  = ""
    BREAK.1  = ""

    XX = ""
    YY = ""
    ZZ = ""
    NN = 1

    TOT.NO.CUR    = 0
    TOT.BL.CUR    = 0
    TOT.NO.CATEG  = 0
    TOT.BL.CATEG  = 0
    CATEGORY.LIST = ""

RETURN
*---------------------------------------------------
PROCESS:
*--------
    T.SEL   = "SELECT ":FN.AC:" WITH CATEGORY"
    T.SEL  := " IN ( 6501 6502 6503 6504 6511 )"
    T.SEL  := " BY CO.CODE BY CATEGORY BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E.AC)
            BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            CUR  = R.AC<AC.CURRENCY>
            CUS  = R.AC<AC.CUSTOMER>
            CATG = R.AC<AC.CATEGORY>
            CUR.CODE = KEY.LIST<I>[9,2]

            COMP.NEW  = R.AC<AC.CO.CODE>
            BREAK.2   = COMP.NEW :"*": CATG :"*": CUR

            IF I EQ 1 THEN
                BREAK.1 = BREAK.2
            END

            IF BREAK.1 EQ BREAK.2 THEN
                IF CUR EQ 'EGP' THEN
                    IF BAL GT 1000000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END ELSE
                    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ECAA)
                    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    BAL      = BAL * CUR.RATE

                    IF BAL GT 1000000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END
            END ELSE
*-------------------------------------
                GOSUB PRINT.LINE
*-------------------------------------
                IF CUR EQ 'EGP' THEN
                    IF BAL GT 1000000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END ELSE
                    CALL F.READ(FN.CUR,CUR,R.CUR,F.CUR,ECAA)
                    CUR.RATE = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                    BAL      = BAL * CUR.RATE

                    IF BAL GT 1000000 THEN
                        TOT.NO++
                        TOT.BAL = TOT.BAL + BAL
                        TOT.NO.CUR<1,CUR.CODE> = TOT.NO.CUR<1,CUR.CODE> + 1
                        TOT.BL.CUR<1,CUR.CODE> = TOT.BL.CUR<1,CUR.CODE> + BAL
                        TOT.NO.CATEG<CUR.CODE,CATG> = TOT.NO.CATEG<CUR.CODE,CATG> + 1
                        TOT.BL.CATEG<CUR.CODE,CATG> = TOT.BL.CATEG<CUR.CODE,CATG> + BAL
                    END
                END
            END

            BREAK.1 = BREAK.2
        NEXT I
    END ELSE
        PRINT "NO DATA"
    END
RETURN
*-------------------------------------------------------
PRINT.LINE:
*----------
    XX = ""

    COMP.ID = FIELD(BREAK.1 ,"*", 1)
*Line [ 279 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COMP.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    XX<1,1>[1,20] = COMP.NAME

    CATEG.CODE    = FIELD(BREAK.1 ,"*", 2)
*Line [ 289 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG.CODE,CATEG.CODE.2)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG.CODE,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.CODE.2=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
    XX<1,1>[22,6] = CATEG.CODE.2

    CUR.ID = FIELD(BREAK.1 ,"*", 3)
*Line [ 299 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.ID,CUR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.ID,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    XX<1,1>[53,15]  = CUR.NAME
    XX<1,1>[85,20]  = TOT.NO
    TOT.BFR = TOT.BAL
    TOT.BAL = FMT(TOT.BAL, "L2,")
    XX<1,1>[110,20] = TOT.BAL

    IF TOT.NO GT 0 THEN
        PRINT XX<1,1>
        PRINT " "
        XX<1,1> = ""
    END

    TOT.NO  = 0
    TOT.BAL = 0
RETURN
*-------------------------------------------------------
PRINT.HEAD:
*-----------
*Line [ 259 ] INITIALIZE "EB.USE.DEPARTMENT.CODE"  - ITSS - R21 Upgrade - 2021-12-23
    EB.USE.DEPARTMENT.CODE=""
*Line [ 326 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  = "'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD := "'L'":SPACE(1):"����� �������"
    PR.HD := T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD := "'L'":SPACE(109): "SBD.CALC.SAV.BAL.MILLION"
    PR.HD := "'L'"
    PR.HD := "'L'":SPACE(50):"������� ������ ������ ������ �������"
    PR.HD := "'L'":SPACE(60):"���� ��  1,000,000"
    PR.HD := "'L'":SPACE(45):STR('_',50)
    PR.HD := "'L'": " "
    PR.HD := "'L'":SPACE(1): "�����"
    PR.HD := SPACE(20): "��� �������"
    PR.HD := SPACE(17): "������"
    PR.HD := SPACE(22): "��� ��������"
    PR.HD := SPACE(17): "������ �������"
    PR.HD := "'L'":STR('_',120)
    PR.HD := "'L'"
    PRINT
    HEADING PR.HD
RETURN
*-------------------------------------------------------
PRINT.HEAD.2:
*------------
*Line [ 359 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  = "'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD := "'L'":SPACE(1):"����� �������"
    PR.HD := T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD := "'L'":SPACE(109):"SBD.CALC.SAV.BAL.MILLION"
    PR.HD := "'L'"
    PR.HD := "'L'":SPACE(50):"������� ������ ������ ������ �������"
    PR.HD := "'L'":SPACE(60):"���� ��  1,000,000"
    PR.HD := "'L'":SPACE(45):STR('_',50)
    PR.HD := "'L'": " "
    PR.HD := SPACE(1): "������"
    PR.HD := SPACE(22): "��� ��������"
    PR.HD := SPACE(17): "������ �������"
    PR.HD := "'L'":STR('_',100)
    PR.HD := "'L'"
    PRINT
    HEADING PR.HD
RETURN
*-----------------------------------------------------------------
CALC.TOT:
*--------
    YY       = ""
    NUM.CUR  = ""
    CUR.COD  = ""
    CUR.NAME = ""
    TOT.BLLL = ""

    CUR.SEL  = "SELECT ":FN.CNUM:" BY @ID"
    CALL EB.READLIST(CUR.SEL,CUR.LIST,"",SELECTED.CUR,ER.MSG)
    IF SELECTED.CUR THEN
        FOR QQ = 1 TO SELECTED.CUR
            CUR.INDX = CUR.LIST<QQ>
*Line [ 401 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,CUR.INDX,CUR.COD)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,CUR.INDX,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR.COD=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
*Line [ 408 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR.COD,CUR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.COD,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
            YY<1,1>[1,15]  = CUR.NAME
            NUM.CUR         = TOT.NO.CUR<1,CUR.INDX>
            YY<1,1>[35,20]  = TOT.NO.CUR<1,CUR.INDX>

            TOT.BLLL        = TOT.BL.CUR<1,CUR.INDX>
            TOT.BLLL        = FMT(TOT.BLLL , "L2,")
            YY<1,1>[70,20]  = TOT.BLLL

            IF NUM.CUR THEN
                PRINT YY<1,1>
                YY<1,1> = ""
            END
        NEXT QQ
    END
RETURN
*--------------------------------------------------------------
END
