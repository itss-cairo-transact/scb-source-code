* @ValidationCode : Mjo5NDc4NDI0MzM6Q3AxMjUyOjE2NDA4MzI4Mjk1MTI6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:53:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-12</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.TRANS.TODAY.03.R2(ENQ.DATA)
    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_ENQUIRY.COMMON
* $INSERT           I_F.SCB.TRANS.TODAY
    $INSERT  I_F.COMPANY
    $INSERT  I_USER.ENV.COMMON


**********  COMPANY.CO
*-----------------------------------
    WS.COMP = ID.COMPANY

    SYS.DATE   = TODAY
    WORK.DATE   = SYS.DATE
***    WORK.DATE   = 20090608
***    CALL CDT('', WORK.DATE, "-1W")
*------------------------
    FN.TRANS = "F.SCB.TRANS.TODAY"
    F.TRANS = ''
    R.TRANS=''
    Y.TRANS=''
    Y.TRANS.ERR=''

    CALL OPF(FN.TRANS,F.TRANS)

    SEL.CMD   = "SELECT ":FN.TRANS:" WITH @ID LIKE ...E..."
    SEL.CMD  := " AND BOOKING.DATE GE ":WORK.DATE
    SEL.CMD  := " AND AMOUNT.LCY LT 0 "
    SEL.CMD  := " AND COMPANY.CO EQ ":WS.COMP
    SEL.CMD  := " AND (((PL.CATEGORY GE  51000 AND PL.CATEGORY LE 54031)"
    SEL.CMD  := " OR (PL.CATEGORY GE 54033 AND PL.CATEGORY LE 59999)"
    SEL.CMD  := " OR (PL.CATEGORY GE 16511 AND PL.CATEGORY LE 16512))"
    SEL.CMD  := " OR ((PRODUCT.CATEGORY IN (10200 11050 11052 11057))"
    SEL.CMD  := " OR (PRODUCT.CATEGORY IN (16201 16205 16208 16250))"
    SEL.CMD  := " OR (PRODUCT.CATEGORY IN (16500 16504 16506 16508))"
    SEL.CMD  := " OR (PRODUCT.CATEGORY IN (16521 16054 11058 16110))"
    SEL.CMD  := " OR (PRODUCT.CATEGORY IN (16186 16520 16426 3050))"
    SEL.CMD  := " OR (PRODUCT.CATEGORY IN (16254 16109 16513 16374))))"




    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)

    IF NOREC >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
RETURN

END
