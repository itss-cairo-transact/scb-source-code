* @ValidationCode : MjoxMTY1NDA1NDk6Q3AxMjUyOjE2NDA4MjI3ODAzNDM6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:06:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
PROGRAM SBD.DEP.WEIGHT.AVG.RATE.FCY.2

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST.DATE
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.DATE
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] HASHING "$INCLUDE I_AC.LOCAL.REFS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY
*-------------------------------------------------------------------------
**** CCY SELECT ***

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    T.SEL1  = "SELECT ":FN.CCY:" WITH @ID NE 'EGP' BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.CCY,KEY.LIST1<K>,R.CCY,F.CCY,E1)
            CCY.ID   = KEY.LIST1<K>
            CCY.RATE = R.CCY<SBD.CURR.MID.RATE>

            GOSUB INITIATE
            GOSUB PROCESS
        NEXT K
    END
RETURN

*-------------------------------------------------------------------------
INITIATE:

    OPENSEQ "DEPO.BUCKET" , "DAILY.DEPOSITS.WEIGHT.AVG.RATE.":CCY.ID:".CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"DEPO.BUCKET":' ':"DAILY.DEPOSITS.WEIGHT.AVG.RATE.":CCY.ID:".CSV"
        HUSH OFF
    END
    OPENSEQ "DEPO.BUCKET" , "DAILY.DEPOSITS.WEIGHT.AVG.RATE.":CCY.ID:".CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DAILY.DEPOSITS.WEIGHT.AVG.RATE.':CCY.ID:'.CSV CREATED IN DEPO.BUCKET'
        END ELSE
            STOP 'Cannot create DAILY.DEPOSITS.WEIGHT.AVG.RATE.':CCY.ID:'.CSV File IN DEPO.BUCKET'
        END
    END

    WS.CCY        = '' ; WS.AMT         = 0  ; WS.MAT.DATE       = '' ; WS.RATE      = 0
    WS.SPREAD     = 0  ; CUST.SECTOR    = '' ; WS.RATE.TYPE.CODE = '' ; DAYS         = ''
    WS.LD.INV.AMT = '' ; WS.RATE.TOTAL  = 0  ; WS.NXT.INT.AMT    = 0  ; WS.INT.AMT   = 0
    WS.AMT.TOT    = 0  ; WS.WG.AVRG.TOT = 0  ; WS.AVG            = 0  ; WS.RATE.TYPE = ''
    WS.AMT.RESV   = 0  ; FLAG = ''           ; WS.AMT.LCY        = 0  ; WS.AMT.TOT.LCY = 0


    WS.AMT.1D = 0 ; WS.AMT.1W = 0 ; WS.AMT.1M   = 0 ; WS.AMT.2M   = 0 ; WS.AMT.3M   = 0 ; WS.AMT.6M = 0
    WS.AMT.9M = 0 ; WS.AMT.1Y = 0 ; WS.AMT.1.5Y = 0 ; WS.AMT.2Y   = 0 ; WS.AMT.3Y   = 0 ; WS.AMT.4Y = 0
    WS.AMT.5Y = 0 ; WS.AMT.6Y = 0 ; WS.AMT.7Y   = 0 ; WS.AMT.GT7Y = 0

    WS.AMT.1D.LCY = 0 ; WS.AMT.1W.LCY = 0 ; WS.AMT.1M.LCY   = 0 ; WS.AMT.2M.LCY = 0 ; WS.AMT.3M.LCY   = 0 ; WS.AMT.6M.LCY = 0
    WS.AMT.9M.LCY = 0 ; WS.AMT.1Y.LCY = 0 ; WS.AMT.1.5Y.LCY = 0 ; WS.AMT.2Y.LCY = 0 ; WS.AMT.3Y.LCY = 0 ; WS.AMT.4Y.LCY = 0
    WS.AMT.5Y.LCY = 0 ; WS.AMT.6Y.LCY = 0 ; WS.AMT.7Y.LCY   = 0 ; WS.AMT.GT7Y.LCY = 0

    WS.AMT.CD.1D = 0 ; WS.AMT.CD.1W = 0 ; WS.AMT.CD.1M   = 0 ; WS.AMT.CD.2M   = 0 ; WS.AMT.CD.3M = 0 ; WS.AMT.CD.6M = 0
    WS.AMT.CD.9M = 0 ; WS.AMT.CD.1Y = 0 ; WS.AMT.CD.1.5Y = 0 ; WS.AMT.CD.2Y   = 0 ; WS.AMT.CD.3Y = 0 ; WS.AMT.CD.4Y = 0
    WS.AMT.CD.5Y = 0 ; WS.AMT.CD.6Y = 0 ; WS.AMT.CD.7Y   = 0 ; WS.AMT.CD.GT7Y = 0

    WS.AMT.CD.1D.LCY = 0 ; WS.AMT.CD.1W.LCY = 0 ; WS.AMT.CD.1M.LCY   = 0 ; WS.AMT.CD.2M.LCY   = 0 ; WS.AMT.CD.3M.LCY = 0 ; WS.AMT.CD.6M.LCY = 0
    WS.AMT.CD.9M.LCY = 0 ; WS.AMT.CD.1Y.LCY = 0 ; WS.AMT.CD.1.5Y.LCY = 0 ; WS.AMT.CD.2Y.LCY   = 0 ; WS.AMT.CD.3Y.LCY = 0 ; WS.AMT.CD.4Y.LCY = 0
    WS.AMT.CD.5Y.LCY = 0 ; WS.AMT.CD.6Y.LCY = 0 ; WS.AMT.CD.7Y.LCY   = 0 ; WS.AMT.CD.GT7Y.LCY = 0


    WS.WG.AVRG.TOT.1D = 0 ; WS.WG.AVRG.TOT.1W = 0 ; WS.WG.AVRG.TOT.1M = 0 ; WS.WG.AVRG.TOT.2M = 0 ; WS.WG.AVRG.TOT.3M = 0
    WS.WG.AVRG.TOT.6M = 0 ; WS.WG.AVRG.TOT.9M = 0 ; WS.WG.AVRG.TOT.1Y = 0 ; WS.WG.AVRG.TOT.1.5Y = 0 ; WS.WG.AVRG.TOT.2Y = 0 ; WS.WG.AVRG.TOT.3Y = 0 ; WS.WG.AVRG.TOT.4Y = 0
    WS.WG.AVRG.TOT.5Y = 0 ; WS.WG.AVRG.TOT.6Y = 0 ; WS.WG.AVRG.TOT.7Y = 0 ; WS.WG.AVRG.TOT.GT7Y = 0

    WS.WG.AVRG.TOT.CD.1D   = 0 ; WS.WG.AVRG.TOT.CD.1W = 0 ; WS.WG.AVRG.TOT.CD.1M = 0 ; WS.WG.AVRG.TOT.CD.2M = 0 ; WS.WG.AVRG.TOT.CD.3M = 0
    WS.WG.AVRG.TOT.CD.6M = 0 ; WS.WG.AVRG.TOT.CD.9M = 0 ; WS.WG.AVRG.TOT.CD.1Y = 0 ; WS.WG.AVRG.TOT.CD.1.5Y = 0 ; WS.WG.AVRG.TOT.CD.2Y = 0 ; WS.WG.AVRG.TOT.CD.3Y = 0 ; WS.WG.AVRG.TOT.CD.4Y = 0
    WS.WG.AVRG.TOT.CD.5Y = 0 ; WS.WG.AVRG.TOT.CD.6Y = 0 ; WS.WG.AVRG.TOT.CD.7Y = 0 ; WS.WG.AVRG.TOT.CD.GT7Y = 0

    WS.AMT.MM.1          = 0 ; WS.AMT.TOT.MM.1 = 0 ; WS.WG.AVRG.TOT.MM.1  = 0 ; WS.AMT.1D.MM = 0
    WS.WG.AVRG.TOT.1D.MM = 0 ; WS.AMT.1W.MM    = 0 ; WS.WG.AVRG.TOT.1W.MM = 0 ; WS.AMT.1M.MM = 0
    WS.WG.AVRG.TOT.1M.MM = 0 ; WS.AMT.TOT.MM   = 0 ; WS.WG.AVRG.TOT.MM    = 0 ; WS.AVG.MM    = 0

    WS.AMT.MM.1.LCY  = 0 ; WS.AMT.TOT.MM.1.LCY = 0 ; WS.AMT.1D.MM.LCY = 0 ; WS.AMT.1W.MM.LCY = 0
    WS.AMT.1M.MM.LCY = 0 ; WS.AMT.TOT.MM.LCY   = 0

    WS.AMT.1M.MM = 0 ; WS.AMT.1M.MM.LCY = 0 ; WS.WG.AVRG.TOT.1M.MM = 0 ; WS.AMT.2M.MM = 0 ; WS.AMT.2M.MM.LCY = 0
    WS.WG.AVRG.TOT.2M.MM = 0 ; WS.AMT.3M.MM = 0 ; WS.AMT.3M.MM.LCY = 0 ; WS.WG.AVRG.TOT.3M.MM = 0
    WS.AMT.6M.MM  = 0 ; WS.AMT.6M.MM.LCY = 0 ; WS.WG.AVRG.TOT.6M.MM = 0
    WS.AMT.1Y.MM  = 0 ; WS.AMT.1Y.MM.LCY = 0 ; WS.WG.AVRG.TOT.1Y.MM = 0
    WS.AVRG.MM.1M = 0 ; WS.AVRG.MM.2M    = 0 ; WS.AVRG.MM.3M = 0 ; WS.AVRG.MM.6M = 0 ; WS.AVRG.MM.1Y = 0


    DAT.1 = TODAY[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')

    DAT.ID = 'EG0010001'
*Line [ 145 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,DAT.2)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT.2=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "DAILY.DEPOSITS.WEIGHTED.AVG.RATE"
    HEAD.DESC = HEAD1:",":DAT.HED:",":CCY.ID

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "Depo.Bucket":","
    HEAD.DESC := "Rate.Type":","
    HEAD.DESC := "Daily.Balance.(Equivalent)":","
    HEAD.DESC := "Daily.Balance.(Native)":","
    HEAD.DESC := "Weighted.Avg.Rate":","
    HEAD.DESC := "Interest.Expenses.Annually":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    HEAD.DESC  = "**** Customer.Deposits ****":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN

*========================================================================
PROCESS:
    FN.CBE = 'F.SCB.CUS.POS.LW' ; F.CBE = ''
    CALL OPF(FN.CBE,F.CBE)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = ''
    CALL OPF(FN.GCI,F.GCI)

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.BID = 'FBNK.BASIC.INTEREST.DATE' ; F.BID = ''
    CALL OPF(FN.BID,F.BID)

    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)

    FN.AD = 'FBNK.ACCOUNT.DATE' ; F.AD = ''
    CALL OPF(FN.AD,F.AD)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""

*-------------------------------------------
******* CURRENT ACCOUNTS SELECTION *************

    T.SEL  = "SELECT ":FN.CBE:" WITH (( CATEGORY IN (1001 1003 1005 1006 1007 1008 1009 1011 1059 1102 1201 1202 1205 1206 1207 1208 1211 1212 1214 1215 1216 1217 1301 1302 1303 1377 1390 1399 1401)"
    T.SEL := " OR CATEGORY IN (1402 1404 1405 1406 1407 1408 1413 1414 1415 1416 1417 1418 1419 1420 1421 1422 1445 1455 1477 1480 1481 1483 1484 1485 1493 1499 1501 1502 1503 1504)"
    T.SEL := " OR CATEGORY IN (1507 1508 1509 1510 1511 1512 1513 1514 1516 1518 1519 1523 1524 1525 1526 1534 1535 1544 1558 1559 1560 1566 1577 1579 1582 1585 1586 1587 1588 1595)"
    T.SEL := " OR CATEGORY IN (1596 1597 1599 2006 11232 11239 11240 11242 16700 16706))"
    T.SEL := " AND DEAL.CCY EQ ":CCY.ID:" AND LCY.AMOUNT GT 0)"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.CUR = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.CUR = DROUND(WS.AVRG.CUR,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Current.Accounts':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.CUR:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.TOT.LCY = 0

    END
*****************************************************

******* SAVING ACCOUNT SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN (6501 6502 6503 6504 6505 6506 6507 6508 6511 6513 6514 6515 6516 6517) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.SAV = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.SAV = DROUND(WS.AVRG.SAV,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0


        BB.DATA  = 'Saving.Accounts':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.SAV:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.TOT.LCY = 0

    END

**********************************************************
*** Floating CDs ***

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY EQ 21032 AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FLAG = 'CD'
            GOSUB GET.DETAIL
        NEXT I
***************************
*** 1 DAY   ***

        IF WS.AMT.CD.1D EQ 0 THEN WS.AMT.CD.1D = 1

        WS.AVRG.CD.1D  = ( WS.WG.AVRG.TOT.CD.1D / WS.AMT.CD.1D ) * 100
        WS.AVRG.CD.1D  = DROUND(WS.AVRG.CD.1D,'2')

        IF WS.AMT.CD.1D EQ 1 THEN WS.AMT.CD.1D = 0

        BB.DATA  = 'Floating CDs =1D (1 day)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.1D.LCY:","
        BB.DATA := WS.AMT.CD.1D:","
        BB.DATA := WS.AVRG.CD.1D:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1D:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1D = 0
        WS.AMT.CD.1D.LCY = 0
        WS.WG.AVRG.TOT.CD.1D = 0

*** 7 DAYS ***

        IF WS.AMT.CD.1W EQ 0 THEN WS.AMT.CD.1W = 1

        WS.AVRG.CD.1W  = ( WS.WG.AVRG.TOT.CD.1W / WS.AMT.CD.1W ) * 100
        WS.AVRG.CD.1W  = DROUND(WS.AVRG.CD.1W,'2')

        IF WS.AMT.CD.1W EQ 1 THEN WS.AMT.CD.1W = 0

        BB.DATA  = 'Floating CDs >1D & <= 1W (7 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.1W.LCY:","
        BB.DATA := WS.AMT.CD.1W:","
        BB.DATA := WS.AVRG.CD.1W:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1W:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1W = 0
        WS.AMT.CD.1W.LCY = 0
        WS.WG.AVRG.TOT.CD.1W = 0


*** 30 DAYS ***

        IF WS.AMT.CD.1M EQ 0 THEN WS.AMT.CD.1M = 1

        WS.AVRG.CD.1M  = ( WS.WG.AVRG.TOT.CD.1M / WS.AMT.CD.1M ) * 100
        WS.AVRG.CD.1M  = DROUND(WS.AVRG.CD.1M,'2')

        IF WS.AMT.CD.1M EQ 1 THEN WS.AMT.CD.1M = 0

        BB.DATA  = 'Floating CDs >1W & <= 1M (30 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.1M.LCY:","
        BB.DATA := WS.AMT.CD.1M:","
        BB.DATA := WS.AVRG.CD.1M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1M = 0
        WS.AMT.CD.1M.LCY = 0
        WS.WG.AVRG.TOT.CD.1M = 0


*** 60 DAYS ***

        IF WS.AMT.CD.2M EQ 0 THEN WS.AMT.CD.2M = 1

        WS.AVRG.CD.2M  = ( WS.WG.AVRG.TOT.CD.2M / WS.AMT.CD.2M ) * 100
        WS.AVRG.CD.2M  = DROUND(WS.AVRG.CD.2M,'2')

        IF WS.AMT.CD.2M EQ 1 THEN WS.AMT.CD.2M = 0

        BB.DATA  = 'Floating CDs >1M & <= 2M (60 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.2M.LCY:","
        BB.DATA := WS.AMT.CD.2M:","
        BB.DATA := WS.AVRG.CD.2M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.2M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.2M = 0
        WS.AMT.CD.2M.LCY = 0
        WS.WG.AVRG.TOT.CD.2M = 0


*** 90 DAYS ***

        IF WS.AMT.CD.3M EQ 0 THEN WS.AMT.CD.3M = 1

        WS.AVRG.CD.3M  = ( WS.WG.AVRG.TOT.CD.3M / WS.AMT.CD.3M ) * 100
        WS.AVRG.CD.3M  = DROUND(WS.AVRG.CD.3M,'2')

        IF WS.AMT.CD.3M EQ 1 THEN WS.AMT.CD.3M = 0

        BB.DATA  = 'Floating CDs >2M & <= 3M (90 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.3M.LCY:","
        BB.DATA := WS.AMT.CD.3M:","
        BB.DATA := WS.AVRG.CD.3M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.3M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.3M = 0
        WS.AMT.CD.3M.LCY = 0
        WS.WG.AVRG.TOT.CD.3M = 0
        WS.AMT.CD.3M = 0


*** 180 DAYS ***

        IF WS.AMT.CD.6M EQ 0 THEN WS.AMT.CD.6M = 1

        WS.AVRG.CD.6M  = ( WS.WG.AVRG.TOT.CD.6M / WS.AMT.CD.6M ) * 100
        WS.AVRG.CD.6M  = DROUND(WS.AVRG.CD.6M,'2')


        IF WS.AMT.CD.6M EQ 1 THEN WS.AMT.CD.6M = 0

        BB.DATA  = 'Floating CDs >3M & <= 6M (180 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.6M.LCY:","
        BB.DATA := WS.AMT.CD.6M:","
        BB.DATA := WS.AVRG.CD.6M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.6M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.6M = 0
        WS.AMT.CD.6M.LCY = 0
        WS.WG.AVRG.TOT.CD.6M = 0


*** 274 DAYS ***

        IF WS.AMT.CD.9M EQ 0 THEN WS.AMT.CD.9M = 1

        WS.AVRG.CD.9M  = ( WS.WG.AVRG.TOT.CD.9M / WS.AMT.CD.9M ) * 100
        WS.AVRG.CD.9M  = DROUND(WS.AVRG.CD.9M,'2')


        IF WS.AMT.CD.9M EQ 1 THEN WS.AMT.CD.9M = 0

        BB.DATA  = 'Floating CDs >6M & <= 9M (274 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.9M.LCY:","
        BB.DATA := WS.AMT.CD.9M:","
        BB.DATA := WS.AVRG.CD.9M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.9M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.9M = 0
        WS.AMT.CD.9M.LCY = 0
        WS.WG.AVRG.TOT.CD.9M = 0

*** 366 DAYS ***

        IF WS.AMT.CD.1Y EQ 0 THEN WS.AMT.CD.1Y = 1

        WS.AVRG.CD.1Y  = ( WS.WG.AVRG.TOT.CD.1Y / WS.AMT.CD.1Y ) * 100
        WS.AVRG.CD.1Y  = DROUND(WS.AVRG.CD.1Y,'2')

        IF WS.AMT.CD.1Y EQ 1 THEN WS.AMT.CD.1Y = 0

        BB.DATA  = 'Floating CDs >9M & <= 1Y (366 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.1Y.LCY:","
        BB.DATA := WS.AMT.CD.1Y:","
        BB.DATA := WS.AVRG.CD.1Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1Y = 0
        WS.AMT.CD.1Y.LCY = 0
        WS.WG.AVRG.TOT.CD.1Y = 0


*** 549 DAYS ***

        IF WS.AMT.CD.1.5Y EQ 0 THEN WS.AMT.CD.1.5Y = 1

        WS.AVRG.CD.1.5Y  = ( WS.WG.AVRG.TOT.CD.1.5Y / WS.AMT.CD.1.5Y ) * 100
        WS.AVRG.CD.1.5Y  = DROUND(WS.AVRG.CD.1.5Y,'2')

        IF WS.AMT.CD.1.5Y EQ 1 THEN WS.AMT.CD.1.5Y = 0

        BB.DATA  = 'Floating CDs >1Y & <= 1.5Y (549 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.1.5Y.LCY:","
        BB.DATA := WS.AMT.CD.1.5Y:","
        BB.DATA := WS.AVRG.CD.1.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1.5Y = 0
        WS.AMT.CD.1.5Y.LCY = 0
        WS.WG.AVRG.TOT.CD.1.5Y = 0

*** 730 DAYS ***

        IF WS.AMT.CD.2Y EQ 0 THEN WS.AMT.CD.2Y = 1

        WS.AVRG.CD.2Y  = ( WS.WG.AVRG.TOT.CD.2Y / WS.AMT.CD.2Y ) * 100
        WS.AVRG.CD.2Y  = DROUND(WS.AVRG.CD.2Y,'2')

        IF WS.AMT.CD.2Y EQ 1 THEN WS.AMT.CD.2Y = 0

        BB.DATA  = 'Floating CDs >1.5Y & <= 2Y (730 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.2Y.LCY:","
        BB.DATA := WS.AMT.CD.2Y:","
        BB.DATA := WS.AVRG.CD.2Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.2Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.2Y = 0
        WS.AMT.CD.2Y.LCY = 0
        WS.WG.AVRG.TOT.CD.2Y = 0


*** 1095 DAYS ***

        IF WS.AMT.CD.3Y EQ 0 THEN WS.AMT.CD.3Y = 1

        WS.AVRG.CD.3Y  = ( WS.WG.AVRG.TOT.CD.3Y / WS.AMT.CD.3Y ) * 100
        WS.AVRG.CD.3Y  = DROUND(WS.AVRG.CD.3Y,'2')


        IF WS.AMT.CD.3Y EQ 1 THEN WS.AMT.CD.3Y = 0

        BB.DATA  = 'Floating CDs >2Y & <= 3Y (1095 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.3Y.LCY:","
        BB.DATA := WS.AMT.CD.3Y:","
        BB.DATA := WS.AVRG.CD.3Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.3Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.3Y = 0
        WS.AMT.CD.3Y.LCY = 0
        WS.WG.AVRG.TOT.CD.3Y = 0

*************************************
*** 1460 DAYS ***

        IF WS.AMT.CD.4Y EQ 0 THEN WS.AMT.CD.4Y = 1

        WS.AVRG.CD.4Y  = ( WS.WG.AVRG.TOT.CD.4Y / WS.AMT.CD.4Y ) * 100
        WS.AVRG.CD.4Y  = DROUND(WS.AVRG.CD.4Y,'2')


        IF WS.AMT.CD.4Y EQ 1 THEN WS.AMT.CD.4Y = 0

        BB.DATA  = 'Floating CDs >3Y & <= 4Y (1460 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.4Y.LCY:","
        BB.DATA := WS.AMT.CD.4Y:","
        BB.DATA := WS.AVRG.CD.4Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.4Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.4Y = 0
        WS.AMT.CD.4Y.LCY = 0
        WS.WG.AVRG.TOT.CD.4Y = 0

*************************************
*** 1827 DAYS ***

        IF WS.AMT.CD.5Y EQ 0 THEN WS.AMT.CD.5Y = 1

        WS.AVRG.CD.5Y  = ( WS.WG.AVRG.TOT.CD.5Y / WS.AMT.CD.5Y ) * 100
        WS.AVRG.CD.5Y  = DROUND(WS.AVRG.CD.5Y,'2')

        IF WS.AMT.CD.5Y EQ 1 THEN WS.AMT.CD.5Y = 0

        BB.DATA  = 'Floating CDs >4Y & <= 5Y (1827 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.5Y.LCY:","
        BB.DATA := WS.AMT.CD.5Y:","
        BB.DATA := WS.AVRG.CD.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.5Y = 0
        WS.AMT.CD.5Y.LCY = 0
        WS.WG.AVRG.TOT.CD.5Y = 0

*************************************
*** 2190 DAYS ***

        IF WS.AMT.CD.6Y EQ 0 THEN WS.AMT.CD.6Y = 1

        WS.AVRG.CD.6Y  = ( WS.WG.AVRG.TOT.CD.6Y / WS.AMT.CD.6Y ) * 100
        WS.AVRG.CD.6Y  = DROUND(WS.AVRG.CD.6Y,'2')

        IF WS.AMT.CD.6Y EQ 1 THEN WS.AMT.CD.6Y = 0

        BB.DATA  = 'Floating CDs >5Y & <= 6Y (2190 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.6Y.LCY:","
        BB.DATA := WS.AMT.CD.6Y:","
        BB.DATA := WS.AVRG.CD.6Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.6Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.6Y = 0
        WS.AMT.CD.6Y.LCY = 0
        WS.WG.AVRG.TOT.CD.6Y = 0

*************************************
*** 2555 DAYS ***

        IF WS.AMT.CD.7Y EQ 0 THEN WS.AMT.CD.7Y = 1

        WS.AVRG.CD.7Y  = ( WS.WG.AVRG.TOT.CD.7Y / WS.AMT.CD.7Y ) * 100
        WS.AVRG.CD.7Y  = DROUND(WS.AVRG.CD.7Y,'2')

        IF WS.AMT.CD.7Y EQ 1 THEN WS.AMT.CD.7Y = 0

        BB.DATA  = 'Floating CDs >6Y & <= 7Y (2555 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.7Y.LCY:","
        BB.DATA := WS.AMT.CD.7Y:","
        BB.DATA := WS.AVRG.CD.7Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.7Y = 0
        WS.AMT.CD.7Y.LCY = 0
        WS.WG.AVRG.TOT.CD.7Y = 0
*************************************
*** GT 2555 DAYS ***

        IF WS.AMT.CD.GT7Y EQ 0 THEN WS.AMT.CD.GT7Y = 1

        WS.AVRG.CD.GT7Y  = ( WS.WG.AVRG.TOT.CD.GT7Y / WS.AMT.CD.GT7Y ) * 100
        WS.AVRG.CD.GT7Y  = DROUND(WS.AVRG.CD.GT7Y,'2')

        IF WS.AMT.CD.GT7Y EQ 1 THEN WS.AMT.CD.GT7Y = 0

        BB.DATA  = 'Floating CDs >7Y (above 2555 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.CD.GT7Y.LCY:","
        BB.DATA := WS.AMT.CD.GT7Y:","
        BB.DATA := WS.AVRG.CD.GT7Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.GT7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.GT7Y = 0
        WS.AMT.CD.GT7Y.LCY = 0
        WS.WG.AVRG.TOT.CD.GT7Y = 0


        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.RESV = 0

***************************
        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END
************************************************************
*** Floating TDs  ***

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY EQ 21012 AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I
**************************
*** 1 DAY ****

        IF WS.AMT.1D EQ 0 THEN WS.AMT.1D = 1

        WS.AVRG.TD.1D  = ( WS.WG.AVRG.TOT.1D / WS.AMT.1D ) * 100
        WS.AVRG.TD.1D  = DROUND(WS.AVRG.TD.1D,'2')

        IF WS.AMT.1D EQ 1 THEN WS.AMT.1D = 0

        BB.DATA  = 'Floating TD =1D (1 day)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.1D.LCY:","
        BB.DATA := WS.AMT.1D:","
        BB.DATA := WS.AVRG.TD.1D:","
        BB.DATA := WS.WG.AVRG.TOT.1D:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1D = 0
        WS.WG.AVRG.TOT.1D = 0
        WS.AMT.1D.LCY = 0


*** 7 DAYS ***

        IF WS.AMT.1W EQ 0 THEN WS.AMT.1W = 1

        WS.AVRG.TD.1W  = ( WS.WG.AVRG.TOT.1W / WS.AMT.1W ) * 100
        WS.AVRG.TD.1W  = DROUND(WS.AVRG.TD.1W,'2')

        IF WS.AMT.1W EQ 1 THEN WS.AMT.1W = 0

        BB.DATA  = 'Floating TD >1D & <= 1W (7 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.1W.LCY:","
        BB.DATA := WS.AMT.1W:","
        BB.DATA := WS.AVRG.TD.1W:","
        BB.DATA := WS.WG.AVRG.TOT.1W:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1W = 0
        WS.WG.AVRG.TOT.1W = 0
        WS.AMT.1W.LCY = 0


*** 30 DAYS ***

        IF WS.AMT.1M EQ 0 THEN WS.AMT.1M = 1

        WS.AVRG.TD.1M  = ( WS.WG.AVRG.TOT.1M / WS.AMT.1M ) * 100
        WS.AVRG.TD.1M  = DROUND(WS.AVRG.TD.1M,'2')

        IF WS.AMT.1M EQ 1 THEN WS.AMT.1M = 0

        BB.DATA  = 'Floating TD >1W & <= 1M (30 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.1M.LCY:","
        BB.DATA := WS.AMT.1M:","
        BB.DATA := WS.AVRG.TD.1M:","
        BB.DATA := WS.WG.AVRG.TOT.1M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1M = 0
        WS.WG.AVRG.TOT.1M = 0
        WS.AMT.1M.LCY = 0


*** 60 DAYS ***

        IF WS.AMT.2M EQ 0 THEN WS.AMT.2M = 1

        WS.AVRG.TD.2M  = ( WS.WG.AVRG.TOT.2M / WS.AMT.2M ) * 100
        WS.AVRG.TD.2M  = DROUND(WS.AVRG.TD.2M,'2')

        IF WS.AMT.2M EQ 1 THEN WS.AMT.2M = 0

        BB.DATA  = 'Floating TD >1M & <= 2M (60 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.2M.LCY:","
        BB.DATA := WS.AMT.2M:","
        BB.DATA := WS.AVRG.TD.2M:","
        BB.DATA := WS.WG.AVRG.TOT.2M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.2M = 0
        WS.WG.AVRG.TOT.2M = 0
        WS.AMT.2M.LCY = 0


*** 90 DAYS ***

        IF WS.AMT.3M EQ 0 THEN WS.AMT.3M = 1

        WS.AVRG.TD.3M  = ( WS.WG.AVRG.TOT.3M / WS.AMT.3M ) * 100
        WS.AVRG.TD.3M  = DROUND(WS.AVRG.TD.3M,'2')

        IF WS.AMT.3M EQ 1 THEN WS.AMT.3M = 0

        BB.DATA  = 'Floating TD >2M & <= 3M (90 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.3M.LCY:","
        BB.DATA := WS.AMT.3M:","
        BB.DATA := WS.AVRG.TD.3M:","
        BB.DATA := WS.WG.AVRG.TOT.3M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.3M = 0
        WS.WG.AVRG.TOT.3M = 0
        WS.AMT.3M.LCY = 0


*** 180 DAYS ***

        IF WS.AMT.6M EQ 0 THEN WS.AMT.6M = 1

        WS.AVRG.TD.6M  = ( WS.WG.AVRG.TOT.6M / WS.AMT.6M ) * 100
        WS.AVRG.TD.6M  = DROUND(WS.AVRG.TD.6M,'2')

        IF WS.AMT.6M EQ 1 THEN WS.AMT.6M = 0

        BB.DATA  = 'Floating TD >3M & <= 6M (180 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.6M.LCY:","
        BB.DATA := WS.AMT.6M:","
        BB.DATA := WS.AVRG.TD.6M:","
        BB.DATA := WS.WG.AVRG.TOT.6M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.6M = 0
        WS.WG.AVRG.TOT.6M = 0
        WS.AMT.6M.LCY = 0


*** 274 DAYS ***

        IF WS.AMT.9M EQ 0 THEN WS.AMT.9M = 1

        WS.AVRG.TD.9M  = ( WS.WG.AVRG.TOT.9M / WS.AMT.9M ) * 100
        WS.AVRG.TD.9M  = DROUND(WS.AVRG.TD.9M,'2')

        IF WS.AMT.9M EQ 1 THEN WS.AMT.9M = 0

        BB.DATA  = 'Floating TD >6M & <= 9M (274 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.9M.LCY:","
        BB.DATA := WS.AMT.9M:","
        BB.DATA := WS.AVRG.TD.9M:","
        BB.DATA := WS.WG.AVRG.TOT.9M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.9M = 0
        WS.WG.AVRG.TOT.9M = 0
        WS.AMT.9M.LCY = 0

*** 366 DAYS ***

        IF WS.AMT.1Y EQ 0 THEN WS.AMT.1Y = 1

        WS.AVRG.TD.1Y  = ( WS.WG.AVRG.TOT.1Y / WS.AMT.1Y ) * 100
        WS.AVRG.TD.1Y  = DROUND(WS.AVRG.TD.1Y,'2')

        IF WS.AMT.1Y EQ 1 THEN WS.AMT.1Y = 0

        BB.DATA  = 'Floating TD >9M & <= 1Y (366 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.1Y.LCY:","
        BB.DATA := WS.AMT.1Y:","
        BB.DATA := WS.AVRG.TD.1Y:","
        BB.DATA := WS.WG.AVRG.TOT.1Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1Y = 0
        WS.WG.AVRG.TOT.1Y = 0
        WS.AMT.1Y.LCY = 0


*** 549 DAYS ***

        IF WS.AMT.1.5Y EQ 0 THEN WS.AMT.1.5Y = 1

        WS.AVRG.TD.1.5Y  = ( WS.WG.AVRG.TOT.1.5Y / WS.AMT.1.5Y ) * 100
        WS.AVRG.TD.1.5Y  = DROUND(WS.AVRG.TD.1.5Y,'2')

        IF WS.AMT.1.5Y EQ 1 THEN WS.AMT.1.5Y = 0

        BB.DATA  = 'Floating TD >1Y & <= 1.5Y (549 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.1.5Y.LCY:","
        BB.DATA := WS.AMT.1.5Y:","
        BB.DATA := WS.AVRG.TD.1.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.1.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1.5Y = 0
        WS.WG.AVRG.TOT.1.5Y = 0
        WS.AMT.1.5Y.LCY = 0

*** 730 DAYS ***

        IF WS.AMT.2Y EQ 0 THEN WS.AMT.2Y = 1

        WS.AVRG.TD.2Y  = ( WS.WG.AVRG.TOT.2Y / WS.AMT.2Y ) * 100
        WS.AVRG.TD.2Y  = DROUND(WS.AVRG.TD.2Y,'2')

        IF WS.AMT.2Y EQ 1 THEN WS.AMT.2Y = 0

        BB.DATA  = 'Floating TD >1.5Y & <= 2Y (730 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.2Y.LCY:","
        BB.DATA := WS.AMT.2Y:","
        BB.DATA := WS.AVRG.TD.2Y:","
        BB.DATA := WS.WG.AVRG.TOT.2Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.2Y = 0
        WS.WG.AVRG.TOT.2Y = 0
        WS.AMT.RESV.2Y = 0


*** 1095 DAYS ***

        IF WS.AMT.3Y EQ 0 THEN WS.AMT.3Y = 1

        WS.AVRG.TD.3Y  = ( WS.WG.AVRG.TOT.3Y / WS.AMT.3Y ) * 100
        WS.AVRG.TD.3Y  = DROUND(WS.AVRG.TD.3Y,'2')

        IF WS.AMT.3Y EQ 1 THEN WS.AMT.3Y = 0

        BB.DATA  = 'Floating TD >2Y & <= 3Y (1095 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.3Y.LCY:","
        BB.DATA := WS.AMT.3Y:","
        BB.DATA := WS.AVRG.TD.3Y:","
        BB.DATA := WS.WG.AVRG.TOT.3Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.3Y = 0
        WS.WG.AVRG.TOT.3Y = 0
        WS.AMT.3Y.LCY = 0

*************************************
*** 1460 DAYS ***

        IF WS.AMT.4Y EQ 0 THEN WS.AMT.4Y = 1

        WS.AVRG.TD.4Y  = ( WS.WG.AVRG.TOT.4Y / WS.AMT.4Y ) * 100
        WS.AVRG.TD.4Y  = DROUND(WS.AVRG.TD.4Y,'2')

        IF WS.AMT.4Y EQ 1 THEN WS.AMT.4Y = 0

        BB.DATA  = 'Floating TD >3Y & <= 4Y (1460 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.4Y.LCY:","
        BB.DATA := WS.AMT.4Y:","
        BB.DATA := WS.AVRG.TD.4Y:","
        BB.DATA := WS.WG.AVRG.TOT.4Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.4Y = 0
        WS.WG.AVRG.TOT.4Y = 0
        WS.AMT.4Y.LCY = 0


*** 1827 DAYS ***

        IF WS.AMT.5Y EQ 0 THEN WS.AMT.5Y = 1

        WS.AVRG.TD.5Y  = ( WS.WG.AVRG.TOT.5Y / WS.AMT.5Y ) * 100
        WS.AVRG.TD.5Y  = DROUND(WS.AVRG.TD.5Y,'2')

        IF WS.AMT.5Y EQ 1 THEN WS.AMT.5Y = 0

        BB.DATA  = 'Floating TD >4Y & <= 5Y (1827 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.5Y.LCY:","
        BB.DATA := WS.AMT.5Y:","
        BB.DATA := WS.AVRG.TD.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.5Y = 0
        WS.WG.AVRG.TOT.5Y = 0
        WS.AMT.5Y.LCY = 0

*************************************
*** 2190 DAYS ***

        IF WS.AMT.6Y EQ 0 THEN WS.AMT.6Y = 1

        WS.AVRG.TD.6Y  = ( WS.WG.AVRG.TOT.6Y / WS.AMT.6Y ) * 100
        WS.AVRG.TD.6Y  = DROUND(WS.AVRG.TD.6Y,'2')

        IF WS.AMT.6Y EQ 1 THEN WS.AMT.6Y = 0

        BB.DATA  = 'Floating TD >5Y & <= 6Y (2190 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.6Y.LCY:","
        BB.DATA := WS.AMT.6Y:","
        BB.DATA := WS.AVRG.TD.6Y:","
        BB.DATA := WS.WG.AVRG.TOT.6Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.6Y = 0
        WS.WG.AVRG.TOT.6Y = 0
        WS.AMT.6Y.LCY = 0

*************************************
*** 2555 DAYS ***

        IF WS.AMT.7Y EQ 0 THEN WS.AMT.7Y = 1

        WS.AVRG.TD.7Y  = ( WS.WG.AVRG.TOT.7Y / WS.AMT.7Y ) * 100
        WS.AVRG.TD.7Y  = DROUND(WS.AVRG.TD.7Y,'2')

        IF WS.AMT.7Y EQ 1 THEN WS.AMT.7Y = 0

        BB.DATA  = 'Floating TD >6Y & <= 7Y (2555 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.7Y.LCY:","
        BB.DATA := WS.AMT.7Y:","
        BB.DATA := WS.AVRG.TD.7Y:","
        BB.DATA := WS.WG.AVRG.TOT.7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.7Y = 0
        WS.WG.AVRG.TOT.7Y = 0
        WS.AMT.7Y.LCY = 0

*************************************
*** GT 2555 DAYS ***

        IF WS.AMT.GT7Y EQ 0 THEN WS.AMT.GT7Y = 1

        WS.AVRG.TD.GT7Y  = ( WS.WG.AVRG.TOT.GT7Y / WS.AMT.GT7Y ) * 100
        WS.AVRG.TD.GT7Y  = DROUND(WS.AVRG.TD.GT7Y,'2')

        IF WS.AMT.GT7Y EQ 1 THEN WS.AMT.GT7Y = 0

        BB.DATA  = 'Floating TD >7Y (above 2555 days)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.GT7Y.LCY:","
        BB.DATA := WS.AMT.GT7Y:","
        BB.DATA := WS.AVRG.TD.GT7Y:","
        BB.DATA := WS.WG.AVRG.TOT.GT7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.GT7Y = 0
        WS.WG.AVRG.TOT.GT7Y = 0
        WS.AMT.GT7Y.LCY = 0


**************************

        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.TOT.LCY = 0

    END
*********************************************************
******* FIXED TIME DEPOSITS SELECTION *************

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21011) OR (CATEGORY EQ 21013 OR CATEGORY EQ 21014 OR CATEGORY EQ 21015 OR CATEGORY EQ 6512)) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FLAG = 'LD'
            GOSUB GET.DETAIL
        NEXT I

*** 1 DAY ****

        IF WS.AMT.1D EQ 0 THEN WS.AMT.1D = 1

        WS.AVRG.TD.1D  = ( WS.WG.AVRG.TOT.1D / WS.AMT.1D ) * 100
        WS.AVRG.TD.1D  = DROUND(WS.AVRG.TD.1D,'2')

        IF WS.AMT.1D EQ 1 THEN WS.AMT.1D = 0

        BB.DATA  = 'Fixed TD =1D (1 day)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.1D.LCY:","
        BB.DATA := WS.AMT.1D:","
        BB.DATA := WS.AVRG.TD.1D:","
        BB.DATA := WS.WG.AVRG.TOT.1D:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1D = 0
        WS.WG.AVRG.TOT.1D = 0
        WS.AMT.1D.LCY = 0


*** 7 DAYS ***

        IF WS.AMT.1W EQ 0 THEN WS.AMT.1W = 1

        WS.AVRG.TD.1W  = ( WS.WG.AVRG.TOT.1W / WS.AMT.1W ) * 100
        WS.AVRG.TD.1W  = DROUND(WS.AVRG.TD.1W,'2')

        IF WS.AMT.1W EQ 1 THEN WS.AMT.1W = 0

        BB.DATA  = 'Fixed TD >1D & <= 1W (7 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.1W.LCY:","
        BB.DATA := WS.AMT.1W:","
        BB.DATA := WS.AVRG.TD.1W:","
        BB.DATA := WS.WG.AVRG.TOT.1W:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1W = 0
        WS.WG.AVRG.TOT.1W = 0
        WS.AMT.1W.LCY = 0


*** 30 DAYS ***

        IF WS.AMT.1M EQ 0 THEN WS.AMT.1M = 1

        WS.AVRG.TD.1M  = ( WS.WG.AVRG.TOT.1M / WS.AMT.1M ) * 100
        WS.AVRG.TD.1M  = DROUND(WS.AVRG.TD.1M,'2')

        IF WS.AMT.1M EQ 1 THEN WS.AMT.1M = 0

        BB.DATA  = 'Fixed TD >1W & <= 1M (30 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.1M.LCY:","
        BB.DATA := WS.AMT.1M:","
        BB.DATA := WS.AVRG.TD.1M:","
        BB.DATA := WS.WG.AVRG.TOT.1M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1M = 0
        WS.WG.AVRG.TOT.1M = 0
        WS.AMT.1M.LCY = 0


*** 60 DAYS ***

        IF WS.AMT.2M EQ 0 THEN WS.AMT.2M = 1

        WS.AVRG.TD.2M  = ( WS.WG.AVRG.TOT.2M / WS.AMT.2M ) * 100
        WS.AVRG.TD.2M  = DROUND(WS.AVRG.TD.2M,'2')

        IF WS.AMT.2M EQ 1 THEN WS.AMT.2M = 0

        BB.DATA  = 'Fixed TD >1M & <= 2M (60 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.2M.LCY:","
        BB.DATA := WS.AMT.2M:","
        BB.DATA := WS.AVRG.TD.2M:","
        BB.DATA := WS.WG.AVRG.TOT.2M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.2M = 0
        WS.WG.AVRG.TOT.2M = 0
        WS.AMT.2M.LCY = 0


*** 90 DAYS ***

        IF WS.AMT.3M EQ 0 THEN WS.AMT.3M = 1

        WS.AVRG.TD.3M  = ( WS.WG.AVRG.TOT.3M / WS.AMT.3M ) * 100
        WS.AVRG.TD.3M  = DROUND(WS.AVRG.TD.3M,'2')

        IF WS.AMT.3M EQ 1 THEN WS.AMT.3M = 0

        BB.DATA  = 'Fixed TD >2M & <= 3M (90 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.3M.LCY:","
        BB.DATA := WS.AMT.3M:","
        BB.DATA := WS.AVRG.TD.3M:","
        BB.DATA := WS.WG.AVRG.TOT.3M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.3M = 0
        WS.WG.AVRG.TOT.3M = 0
        WS.AMT.3M.LCY = 0


*** 180 DAYS ***

        IF WS.AMT.6M EQ 0 THEN WS.AMT.6M = 1

        WS.AVRG.TD.6M  = ( WS.WG.AVRG.TOT.6M / WS.AMT.6M ) * 100
        WS.AVRG.TD.6M  = DROUND(WS.AVRG.TD.6M,'2')

        IF WS.AMT.6M EQ 1 THEN WS.AMT.6M = 0

        BB.DATA  = 'Fixed TD >3M & <= 6M (180 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.6M.LCY:","
        BB.DATA := WS.AMT.6M:","
        BB.DATA := WS.AVRG.TD.6M:","
        BB.DATA := WS.WG.AVRG.TOT.6M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.6M = 0
        WS.WG.AVRG.TOT.6M = 0
        WS.AMT.6M.LCY = 0


*** 274 DAYS ***

        IF WS.AMT.9M EQ 0 THEN WS.AMT.9M = 1

        WS.AVRG.TD.9M  = ( WS.WG.AVRG.TOT.9M / WS.AMT.9M ) * 100
        WS.AVRG.TD.9M  = DROUND(WS.AVRG.TD.9M,'2')

        IF WS.AMT.9M EQ 1 THEN WS.AMT.9M = 0

        BB.DATA  = 'Fixed TD >6M & <= 9M (274 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.9M.LCY:","
        BB.DATA := WS.AMT.9M:","
        BB.DATA := WS.AVRG.TD.9M:","
        BB.DATA := WS.WG.AVRG.TOT.9M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.9M = 0
        WS.WG.AVRG.TOT.9M = 0
        WS.AMT.9M.LCY = 0

*** 366 DAYS ***

        IF WS.AMT.1Y EQ 0 THEN WS.AMT.1Y = 1

        WS.AVRG.TD.1Y  = ( WS.WG.AVRG.TOT.1Y / WS.AMT.1Y ) * 100
        WS.AVRG.TD.1Y  = DROUND(WS.AVRG.TD.1Y,'2')

        IF WS.AMT.1Y EQ 1 THEN WS.AMT.1Y = 0

        BB.DATA  = 'Fixed TD >9M & <= 1Y (366 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.1Y.LCY:","
        BB.DATA := WS.AMT.1Y:","
        BB.DATA := WS.AVRG.TD.1Y:","
        BB.DATA := WS.WG.AVRG.TOT.1Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1Y = 0
        WS.WG.AVRG.TOT.1Y = 0
        WS.AMT.1Y.LCY = 0


*** 549 DAYS ***

        IF WS.AMT.1.5Y EQ 0 THEN WS.AMT.1.5Y = 1

        WS.AVRG.TD.1.5Y  = ( WS.WG.AVRG.TOT.1.5Y / WS.AMT.1.5Y ) * 100
        WS.AVRG.TD.1.5Y  = DROUND(WS.AVRG.TD.1.5Y,'2')

        IF WS.AMT.1.5Y EQ 1 THEN WS.AMT.1.5Y = 0

        BB.DATA  = 'Fixed TD >1Y & <= 1.5Y (549 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.1.5Y.LCY:","
        BB.DATA := WS.AMT.1.5Y:","
        BB.DATA := WS.AVRG.TD.1.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.1.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.1.5Y = 0
        WS.WG.AVRG.TOT.1.5Y = 0
        WS.AMT.1.5Y.LCY = 0

*** 730 DAYS ***

        IF WS.AMT.2Y EQ 0 THEN WS.AMT.2Y = 1

        WS.AVRG.TD.2Y  = ( WS.WG.AVRG.TOT.2Y / WS.AMT.2Y ) * 100
        WS.AVRG.TD.2Y  = DROUND(WS.AVRG.TD.2Y,'2')

        IF WS.AMT.2Y EQ 1 THEN WS.AMT.2Y = 0

        BB.DATA  = 'Fixed TD >1.5Y & <= 2Y (730 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.2Y.LCY:","
        BB.DATA := WS.AMT.2Y:","
        BB.DATA := WS.AVRG.TD.2Y:","
        BB.DATA := WS.WG.AVRG.TOT.2Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.2Y = 0
        WS.WG.AVRG.TOT.2Y = 0
        WS.AMT.RESV.2Y = 0


*** 1095 DAYS ***

        IF WS.AMT.3Y EQ 0 THEN WS.AMT.3Y = 1

        WS.AVRG.TD.3Y  = ( WS.WG.AVRG.TOT.3Y / WS.AMT.3Y ) * 100
        WS.AVRG.TD.3Y  = DROUND(WS.AVRG.TD.3Y,'2')

        IF WS.AMT.3Y EQ 1 THEN WS.AMT.3Y = 0

        BB.DATA  = 'Fixed TD >2Y & <= 3Y (1095 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.3Y.LCY:","
        BB.DATA := WS.AMT.3Y:","
        BB.DATA := WS.AVRG.TD.3Y:","
        BB.DATA := WS.WG.AVRG.TOT.3Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.3Y = 0
        WS.WG.AVRG.TOT.3Y = 0
        WS.AMT.3Y.LCY = 0

*************************************
*** 1460 DAYS ***

        IF WS.AMT.4Y EQ 0 THEN WS.AMT.4Y = 1

        WS.AVRG.TD.4Y  = ( WS.WG.AVRG.TOT.4Y / WS.AMT.4Y ) * 100
        WS.AVRG.TD.4Y  = DROUND(WS.AVRG.TD.4Y,'2')

        IF WS.AMT.4Y EQ 1 THEN WS.AMT.4Y = 0

        BB.DATA  = 'Fixed TD >3Y & <= 4Y (1460 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.4Y.LCY:","
        BB.DATA := WS.AMT.4Y:","
        BB.DATA := WS.AVRG.TD.4Y:","
        BB.DATA := WS.WG.AVRG.TOT.4Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.4Y = 0
        WS.WG.AVRG.TOT.4Y = 0
        WS.AMT.4Y.LCY = 0


*** 1827 DAYS ***

        IF WS.AMT.5Y EQ 0 THEN WS.AMT.5Y = 1

        WS.AVRG.TD.5Y  = ( WS.WG.AVRG.TOT.5Y / WS.AMT.5Y ) * 100
        WS.AVRG.TD.5Y  = DROUND(WS.AVRG.TD.5Y,'2')

        IF WS.AMT.5Y EQ 1 THEN WS.AMT.5Y = 0

        BB.DATA  = 'Fixed TD >4Y & <= 5Y (1827 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.5Y.LCY:","
        BB.DATA := WS.AMT.5Y:","
        BB.DATA := WS.AVRG.TD.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.5Y = 0
        WS.WG.AVRG.TOT.5Y = 0
        WS.AMT.5Y.LCY = 0

*************************************
*** 2190 DAYS ***

        IF WS.AMT.6Y EQ 0 THEN WS.AMT.6Y = 1

        WS.AVRG.TD.6Y  = ( WS.WG.AVRG.TOT.6Y / WS.AMT.6Y ) * 100
        WS.AVRG.TD.6Y  = DROUND(WS.AVRG.TD.6Y,'2')

        IF WS.AMT.6Y EQ 1 THEN WS.AMT.6Y = 0

        BB.DATA  = 'Fixed TD >5Y & <= 6Y (2190 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.6Y.LCY:","
        BB.DATA := WS.AMT.6Y:","
        BB.DATA := WS.AVRG.TD.6Y:","
        BB.DATA := WS.WG.AVRG.TOT.6Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.6Y = 0
        WS.WG.AVRG.TOT.6Y = 0
        WS.AMT.6Y.LCY = 0

*************************************
*** 2555 DAYS ***

        IF WS.AMT.7Y EQ 0 THEN WS.AMT.7Y = 1

        WS.AVRG.TD.7Y  = ( WS.WG.AVRG.TOT.7Y / WS.AMT.7Y ) * 100
        WS.AVRG.TD.7Y  = DROUND(WS.AVRG.TD.7Y,'2')

        IF WS.AMT.7Y EQ 1 THEN WS.AMT.7Y = 0

        BB.DATA  = 'Fixed TD >6Y & <= 7Y (2555 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.7Y.LCY:","
        BB.DATA := WS.AMT.7Y:","
        BB.DATA := WS.AVRG.TD.7Y:","
        BB.DATA := WS.WG.AVRG.TOT.7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.7Y = 0
        WS.WG.AVRG.TOT.7Y = 0
        WS.AMT.7Y.LCY = 0

*************************************
*** GT 2555 DAYS ***

        IF WS.AMT.GT7Y EQ 0 THEN WS.AMT.GT7Y = 1

        WS.AVRG.TD.GT7Y  = ( WS.WG.AVRG.TOT.GT7Y / WS.AMT.GT7Y ) * 100
        WS.AVRG.TD.GT7Y  = DROUND(WS.AVRG.TD.GT7Y,'2')

        IF WS.AMT.GT7Y EQ 1 THEN WS.AMT.GT7Y = 0

        BB.DATA  = 'Fixed TD >7Y (above 2555 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.GT7Y.LCY:","
        BB.DATA := WS.AMT.GT7Y:","
        BB.DATA := WS.AVRG.TD.GT7Y:","
        BB.DATA := WS.WG.AVRG.TOT.GT7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.GT7Y = 0
        WS.WG.AVRG.TOT.GT7Y = 0
        WS.AMT.GT7Y.LCY = 0

*************************************

    END
************************************************************
**** Fixed Accum CDs ****

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 21017 AND CATEGORY LE 21025) OR (CATEGORY EQ 21029 OR CATEGORY EQ 21036 OR CATEGORY EQ 21041 OR CATEGORY EQ 21042)) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FLAG = 'CD'
            GOSUB GET.DETAIL
        NEXT I
***************
*** 1 DAY   ***

        IF WS.AMT.CD.1D EQ 0 THEN WS.AMT.CD.1D = 1

        WS.AVRG.CD.1D  = ( WS.WG.AVRG.TOT.CD.1D / WS.AMT.CD.1D ) * 100
        WS.AVRG.CD.1D  = DROUND(WS.AVRG.CD.1D,'2')

        IF WS.AMT.CD.1D EQ 1 THEN WS.AMT.CD.1D = 0

        BB.DATA  = 'Fixed CDs =1D (1 day)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.1D.LCY:","
        BB.DATA := WS.AMT.CD.1D:","
        BB.DATA := WS.AVRG.CD.1D:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1D:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1D = 0
        WS.AMT.CD.1D.LCY = 0
        WS.WG.AVRG.TOT.CD.1D = 0

*** 7 DAYS ***

        IF WS.AMT.CD.1W EQ 0 THEN WS.AMT.CD.1W = 1

        WS.AVRG.CD.1W  = ( WS.WG.AVRG.TOT.CD.1W / WS.AMT.CD.1W ) * 100
        WS.AVRG.CD.1W  = DROUND(WS.AVRG.CD.1W,'2')

        IF WS.AMT.CD.1W EQ 1 THEN WS.AMT.CD.1W = 0

        BB.DATA  = 'Fixed CDs >1D & <= 1W (7 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.1W.LCY:","
        BB.DATA := WS.AMT.CD.1W:","
        BB.DATA := WS.AVRG.CD.1W:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1W:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1W = 0
        WS.AMT.CD.1W.LCY = 0
        WS.WG.AVRG.TOT.CD.1W = 0


*** 30 DAYS ***

        IF WS.AMT.CD.1M EQ 0 THEN WS.AMT.CD.1M = 1

        WS.AVRG.CD.1M  = ( WS.WG.AVRG.TOT.CD.1M / WS.AMT.CD.1M ) * 100
        WS.AVRG.CD.1M  = DROUND(WS.AVRG.CD.1M,'2')

        IF WS.AMT.CD.1M EQ 1 THEN WS.AMT.CD.1M = 0

        BB.DATA  = 'Fixed CDs >1W & <= 1M (30 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.1M.LCY:","
        BB.DATA := WS.AMT.CD.1M:","
        BB.DATA := WS.AVRG.CD.1M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1M = 0
        WS.AMT.CD.1M.LCY = 0
        WS.WG.AVRG.TOT.CD.1M = 0


*** 60 DAYS ***

        IF WS.AMT.CD.2M EQ 0 THEN WS.AMT.CD.2M = 1

        WS.AVRG.CD.2M  = ( WS.WG.AVRG.TOT.CD.2M / WS.AMT.CD.2M ) * 100
        WS.AVRG.CD.2M  = DROUND(WS.AVRG.CD.2M,'2')

        IF WS.AMT.CD.2M EQ 1 THEN WS.AMT.CD.2M = 0

        BB.DATA  = 'Fixed CDs >1M & <= 2M (60 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.2M.LCY:","
        BB.DATA := WS.AMT.CD.2M:","
        BB.DATA := WS.AVRG.CD.2M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.2M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.2M = 0
        WS.AMT.CD.2M.LCY = 0
        WS.WG.AVRG.TOT.CD.2M = 0


*** 90 DAYS ***

        IF WS.AMT.CD.3M EQ 0 THEN WS.AMT.CD.3M = 1

        WS.AVRG.CD.3M  = ( WS.WG.AVRG.TOT.CD.3M / WS.AMT.CD.3M ) * 100
        WS.AVRG.CD.3M  = DROUND(WS.AVRG.CD.3M,'2')

        IF WS.AMT.CD.3M EQ 1 THEN WS.AMT.CD.3M = 0

        BB.DATA  = 'Fixed CDs >2M & <= 3M (90 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.3M.LCY:","
        BB.DATA := WS.AMT.CD.3M:","
        BB.DATA := WS.AVRG.CD.3M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.3M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.3M = 0
        WS.AMT.CD.3M.LCY = 0
        WS.WG.AVRG.TOT.CD.3M = 0
        WS.AMT.CD.3M = 0


*** 180 DAYS ***

        IF WS.AMT.CD.6M EQ 0 THEN WS.AMT.CD.6M = 1

        WS.AVRG.CD.6M  = ( WS.WG.AVRG.TOT.CD.6M / WS.AMT.CD.6M ) * 100
        WS.AVRG.CD.6M  = DROUND(WS.AVRG.CD.6M,'2')


        IF WS.AMT.CD.6M EQ 1 THEN WS.AMT.CD.6M = 0

        BB.DATA  = 'Fixed CDs >3M & <= 6M (180 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.6M.LCY:","
        BB.DATA := WS.AMT.CD.6M:","
        BB.DATA := WS.AVRG.CD.6M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.6M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.6M = 0
        WS.AMT.CD.6M.LCY = 0
        WS.WG.AVRG.TOT.CD.6M = 0


*** 274 DAYS ***

        IF WS.AMT.CD.9M EQ 0 THEN WS.AMT.CD.9M = 1

        WS.AVRG.CD.9M  = ( WS.WG.AVRG.TOT.CD.9M / WS.AMT.CD.9M ) * 100
        WS.AVRG.CD.9M  = DROUND(WS.AVRG.CD.9M,'2')


        IF WS.AMT.CD.9M EQ 1 THEN WS.AMT.CD.9M = 0

        BB.DATA  = 'Fixed CDs >6M & <= 9M (274 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.9M.LCY:","
        BB.DATA := WS.AMT.CD.9M:","
        BB.DATA := WS.AVRG.CD.9M:","
        BB.DATA := WS.WG.AVRG.TOT.CD.9M:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.9M = 0
        WS.AMT.CD.9M.LCY = 0
        WS.WG.AVRG.TOT.CD.9M = 0

*** 366 DAYS ***

        IF WS.AMT.CD.1Y EQ 0 THEN WS.AMT.CD.1Y = 1

        WS.AVRG.CD.1Y  = ( WS.WG.AVRG.TOT.CD.1Y / WS.AMT.CD.1Y ) * 100
        WS.AVRG.CD.1Y  = DROUND(WS.AVRG.CD.1Y,'2')

        IF WS.AMT.CD.1Y EQ 1 THEN WS.AMT.CD.1Y = 0

        BB.DATA  = 'Fixed CDs >9M & <= 1Y (366 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.1Y.LCY:","
        BB.DATA := WS.AMT.CD.1Y:","
        BB.DATA := WS.AVRG.CD.1Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1Y = 0
        WS.AMT.CD.1Y.LCY = 0
        WS.WG.AVRG.TOT.CD.1Y = 0


*** 549 DAYS ***

        IF WS.AMT.CD.1.5Y EQ 0 THEN WS.AMT.CD.1.5Y = 1

        WS.AVRG.CD.1.5Y  = ( WS.WG.AVRG.TOT.CD.1.5Y / WS.AMT.CD.1.5Y ) * 100
        WS.AVRG.CD.1.5Y  = DROUND(WS.AVRG.CD.1.5Y,'2')

        IF WS.AMT.CD.1.5Y EQ 1 THEN WS.AMT.CD.1.5Y = 0

        BB.DATA  = 'Fixed CDs >1Y & <= 1.5Y (549 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.1.5Y.LCY:","
        BB.DATA := WS.AMT.CD.1.5Y:","
        BB.DATA := WS.AVRG.CD.1.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.1.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.1.5Y = 0
        WS.AMT.CD.1.5Y.LCY = 0
        WS.WG.AVRG.TOT.CD.1.5Y = 0

*** 730 DAYS ***

        IF WS.AMT.CD.2Y EQ 0 THEN WS.AMT.CD.2Y = 1

        WS.AVRG.CD.2Y  = ( WS.WG.AVRG.TOT.CD.2Y / WS.AMT.CD.2Y ) * 100
        WS.AVRG.CD.2Y  = DROUND(WS.AVRG.CD.2Y,'2')

        IF WS.AMT.CD.2Y EQ 1 THEN WS.AMT.CD.2Y = 0

        BB.DATA  = 'Fixed CDs >1.5Y & <= 2Y (730 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.2Y.LCY:","
        BB.DATA := WS.AMT.CD.2Y:","
        BB.DATA := WS.AVRG.CD.2Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.2Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.2Y = 0
        WS.AMT.CD.2Y.LCY = 0
        WS.WG.AVRG.TOT.CD.2Y = 0


*** 1095 DAYS ***

        IF WS.AMT.CD.3Y EQ 0 THEN WS.AMT.CD.3Y = 1

        WS.AVRG.CD.3Y  = ( WS.WG.AVRG.TOT.CD.3Y / WS.AMT.CD.3Y ) * 100
        WS.AVRG.CD.3Y  = DROUND(WS.AVRG.CD.3Y,'2')


        IF WS.AMT.CD.3Y EQ 1 THEN WS.AMT.CD.3Y = 0

        BB.DATA  = 'Fixed CDs >2Y & <= 3Y (1095 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.3Y.LCY:","
        BB.DATA := WS.AMT.CD.3Y:","
        BB.DATA := WS.AVRG.CD.3Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.3Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.3Y = 0
        WS.AMT.CD.3Y.LCY = 0
        WS.WG.AVRG.TOT.CD.3Y = 0

*************************************
*** 1460 DAYS ***

        IF WS.AMT.CD.4Y EQ 0 THEN WS.AMT.CD.4Y = 1

        WS.AVRG.CD.4Y  = ( WS.WG.AVRG.TOT.CD.4Y / WS.AMT.CD.4Y ) * 100
        WS.AVRG.CD.4Y  = DROUND(WS.AVRG.CD.4Y,'2')


        IF WS.AMT.CD.4Y EQ 1 THEN WS.AMT.CD.4Y = 0

        BB.DATA  = 'Fixed CDs >3Y & <= 4Y (1460 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.4Y.LCY:","
        BB.DATA := WS.AMT.CD.4Y:","
        BB.DATA := WS.AVRG.CD.4Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.4Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.4Y = 0
        WS.AMT.CD.4Y.LCY = 0
        WS.WG.AVRG.TOT.CD.4Y = 0

*************************************
*** 1827 DAYS ***

        IF WS.AMT.CD.5Y EQ 0 THEN WS.AMT.CD.5Y = 1

        WS.AVRG.CD.5Y  = ( WS.WG.AVRG.TOT.CD.5Y / WS.AMT.CD.5Y ) * 100
        WS.AVRG.CD.5Y  = DROUND(WS.AVRG.CD.5Y,'2')

        IF WS.AMT.CD.5Y EQ 1 THEN WS.AMT.CD.5Y = 0

        BB.DATA  = 'Fixed CDs >4Y & <= 5Y (1827 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.5Y.LCY:","
        BB.DATA := WS.AMT.CD.5Y:","
        BB.DATA := WS.AVRG.CD.5Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.5Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.5Y = 0
        WS.AMT.CD.5Y.LCY = 0
        WS.WG.AVRG.TOT.CD.5Y = 0

*************************************
*** 2190 DAYS ***

        IF WS.AMT.CD.6Y EQ 0 THEN WS.AMT.CD.6Y = 1

        WS.AVRG.CD.6Y  = ( WS.WG.AVRG.TOT.CD.6Y / WS.AMT.CD.6Y ) * 100
        WS.AVRG.CD.6Y  = DROUND(WS.AVRG.CD.6Y,'2')

        IF WS.AMT.CD.6Y EQ 1 THEN WS.AMT.CD.6Y = 0

        BB.DATA  = 'Fixed CDs >5Y & <= 6Y (2190 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.6Y.LCY:","
        BB.DATA := WS.AMT.CD.6Y:","
        BB.DATA := WS.AVRG.CD.6Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.6Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.6Y = 0
        WS.AMT.CD.6Y.LCY = 0
        WS.WG.AVRG.TOT.CD.6Y = 0

*************************************
*** 2555 DAYS ***

        IF WS.AMT.CD.7Y EQ 0 THEN WS.AMT.CD.7Y = 1

        WS.AVRG.CD.7Y  = ( WS.WG.AVRG.TOT.CD.7Y / WS.AMT.CD.7Y ) * 100
        WS.AVRG.CD.7Y  = DROUND(WS.AVRG.CD.7Y,'2')

        IF WS.AMT.CD.7Y EQ 1 THEN WS.AMT.CD.7Y = 0

        BB.DATA  = 'Fixed CDs >6Y & <= 7Y (2555 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.7Y.LCY:","
        BB.DATA := WS.AMT.CD.7Y:","
        BB.DATA := WS.AVRG.CD.7Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.7Y = 0
        WS.AMT.CD.7Y.LCY = 0
        WS.WG.AVRG.TOT.CD.7Y = 0
*************************************
*** GT 2555 DAYS ***

        IF WS.AMT.CD.GT7Y EQ 0 THEN WS.AMT.CD.GT7Y = 1

        WS.AVRG.CD.GT7Y  = ( WS.WG.AVRG.TOT.CD.GT7Y / WS.AMT.CD.GT7Y ) * 100
        WS.AVRG.CD.GT7Y  = DROUND(WS.AVRG.CD.GT7Y,'2')

        IF WS.AMT.CD.GT7Y EQ 1 THEN WS.AMT.CD.GT7Y = 0

        BB.DATA  = 'Fixed CDs >7Y (above 2555 days)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.CD.GT7Y.LCY:","
        BB.DATA := WS.AMT.CD.GT7Y:","
        BB.DATA := WS.AVRG.CD.GT7Y:","
        BB.DATA := WS.WG.AVRG.TOT.CD.GT7Y:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.CD.GT7Y = 0
        WS.AMT.CD.GT7Y.LCY = 0
        WS.WG.AVRG.TOT.CD.GT7Y = 0


        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.RESV = 0
*************************************
    END

    WS.AMT.TOT.LCY = 0
***************************************************************
**** LCs / LGs ****

    T.SEL = "SELECT ":FN.CBE:" WITH ((CATEGORY GE 3010 AND CATEGORY LE 3017) OR (CATEGORY EQ 3060 OR CATEGORY EQ 3065 OR CATEGORY EQ 3005)) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD  = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD  = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'LCs / LGs':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.TOT.LCY = 0

    END

***************************************************************
**** Payroll ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY EQ 1002 AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD  = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD  = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Payroll':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.TOT.LCY = 0

    END

***************************************************************
**** Others ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 16113 16151 16153 16188 16152 11380 16175 1012 1013 1014 1015 1016 1019 16170 3050 16198 16149 1017 3206 3208 ) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD  = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD  = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Others':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.WG.AVRG.TOT = 0
        WS.AMT.TOT.LCY = 0

    END

***************************************************************
    HEAD.DESC  = "**** Non-Customer.Deposits ****":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
***************************************************************
**** CIB loan ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 2004 ) AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'CIB.Loan':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END


***************************************************************
**** Social Fund loan ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 2007 2008 2011 ) AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Social.Fund.Development.Loan':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END


***************************************************************
**** Subordinated Loan  ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 21016 ) AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Subordinated.Loan':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END
***************************************************************
**** Repo ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 16144 21033 ) AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Repo':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END

***************************************************************
**** Equity ****

    T.SEL  = "SELECT ":FN.CBE:" WITH (( CATEGORY IN (18000 18001 18106 18105 18101 18110 18120 18145 18146 18151 18150 18121 18147 18148 18116 13300 18115)"
    T.SEL := " OR CATEGORY IN (18144 18149 18152 18153 18154 18155 18156 18157 18158 18159))"
    T.SEL := " AND DEAL.CCY EQ ":CCY.ID:")"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Equity':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END

***************************************************************
**** Due To Banks (Current Account) ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 2000 2001 5000 5001 5020 5021 ) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Due.To.Banks.(Current.Account)':","
        BB.DATA := 'Fixed':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END

***************************************************************
**** Due To Banks (CBE) ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 2002 2003 ) AND LCY.AMOUNT GT 0 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
        NEXT I

        IF WS.AMT.TOT EQ 0 THEN WS.AMT.TOT = 1

        WS.AVRG.TD = ( WS.WG.AVRG.TOT / WS.AMT.TOT ) * 100
        WS.AVRG.TD = DROUND(WS.AVRG.TD,'2')

        IF WS.AMT.TOT EQ 1 THEN WS.AMT.TOT = 0

        BB.DATA  = 'Due.To.Banks.(CBE)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        WS.AMT.TOT = 0
        WS.AMT.TOT.LCY = 0
        WS.WG.AVRG.TOT = 0

    END ELSE
        WS.AMT.TOT     = 0
        WS.AMT.TOT.LCY = 0
        WS.AVRG.TD     = 0
        WS.WG.AVRG.TOT = 0

        BB.DATA  = 'Due.To.Banks.(CBE)':","
        BB.DATA := 'Floating':","
        BB.DATA := WS.AMT.TOT.LCY:","
        BB.DATA := WS.AMT.TOT:","
        BB.DATA := WS.AVRG.TD:","
        BB.DATA := WS.WG.AVRG.TOT:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
    END
***************************************************************
**** Due to Banks (Money Market - Floating) ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY IN ( 21030 21031 ) AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            TRNS.ID = FIELD(KEY.LIST<I>,"*",5)

            CALL F.READ(FN.MM,TRNS.ID,R.MM,F.MM,E1)
            WS.RATE.TYPE.CODE = R.MM<MM.INT.RATE.TYPE>
            WS.CCY            = R.MM<MM.CURRENCY>

            IF WS.RATE.TYPE.CODE EQ 3 THEN

                WS.AMT.MM      = R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.TOT.MM += WS.AMT.MM

                WS.AMT.MM.LCY      = R.CBE<CUPOS.LCY.AMOUNT>
                WS.AMT.TOT.MM.LCY += WS.AMT.MM

                WS.MAT.DATE    = R.CBE<CUPOS.MATURITY.DATE>

                WS.MM.KEY = R.MM<MM.INTEREST.KEY>
                BI.ID.KEY = WS.MM.KEY:WS.CCY

                CALL F.READ(FN.BID,BI.ID.KEY,R.BID,F.BID,E4)
                BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
                BI.ID = BI.ID.KEY:BI.DATE

                CALL F.READ(FN.BI,BI.ID,R.BI,F.BI,E3)
                WS.RATE   = R.BI<EB.BIN.INTEREST.RATE>
                WS.SPREAD = R.MM<MM.INTEREST.SPREAD.1>

                WS.RATE.TOTAL     = WS.RATE + WS.SPREAD
                WS.AVG.MM         = ( WS.AMT.MM * WS.RATE.TOTAL ) / 100
                WS.WG.AVRG.TOT.MM += WS.AVG.MM
            END

            IF WS.RATE.TYPE.CODE EQ 1 THEN

                WS.AMT.MM.1      = R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.TOT.MM.1 += WS.AMT.MM.1

                WS.AMT.MM.1.LCY      = R.CBE<CUPOS.LCY.AMOUNT>
                WS.AMT.TOT.MM.1.LCY += WS.AMT.MM.1

                WS.MAT.DATE = R.CBE<CUPOS.MATURITY.DATE>
                WS.RATE     = R.MM<MM.INTEREST.RATE>
                WS.SPREAD   = R.MM<MM.INTEREST.SPREAD.1>

                WS.RATE.TOTAL        = WS.RATE + WS.SPREAD
                WS.AVG.MM.1          = ( WS.AMT.MM.1 * WS.RATE.TOTAL ) / 100
                WS.WG.AVRG.TOT.MM.1 += WS.AVG.MM.1

                DAYS = "C"
                IF WS.MAT.DATE NE '' THEN
                    CALL CDD("",DAT.2,WS.MAT.DATE,DAYS)
                END ELSE
                    DAYS = ''
                END

                IF DAYS EQ 1 THEN
                    WS.AMT.1D.MM         += WS.AMT.MM.1
                    WS.AMT.1D.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1D.MM += WS.AVG.MM.1
                END

                IF DAYS GE 2 AND DAYS LE 7 THEN
                    WS.AMT.1W.MM         += WS.AMT.MM.1
                    WS.AMT.1W.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1W.MM += WS.AVG.MM.1
                END

                IF DAYS GT 7 AND DAYS LE 30 THEN
                    WS.AMT.1M.MM         += WS.AMT.MM.1
                    WS.AMT.1M.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1M.MM += WS.AVG.MM.1
                END
            END
        NEXT I
    END

    IF WS.AMT.TOT.MM EQ 0 THEN WS.AMT.TOT.MM = 1

    WS.AVRG.MM = ( WS.WG.AVRG.TOT.MM / WS.AMT.TOT.MM ) * 100
    WS.AVRG.MM = DROUND(WS.AVRG.MM,'2')

    IF WS.AMT.TOT.MM EQ 1 THEN WS.AMT.TOT.MM = 0

    BB.DATA  = 'Due.To.Banks.(Money.Market)':","
    BB.DATA := 'Floating':","
    BB.DATA := WS.AMT.TOT.MM.LCY:","
    BB.DATA := WS.AMT.TOT.MM:","
    BB.DATA := WS.AVRG.MM:","
    BB.DATA := WS.WG.AVRG.TOT.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END



***************
*** 1 DAY   ***

    IF WS.AMT.1D.MM EQ 0 THEN WS.AMT.1D.MM = 1

    WS.AVRG.MM.1D  = ( WS.WG.AVRG.TOT.1D.MM / WS.AMT.1D.MM ) * 100
    WS.AVRG.MM.1D  = DROUND(WS.AVRG.MM.1D,'2')

    IF WS.AMT.1D.MM EQ 1 THEN WS.AMT.1D.MM = 0

    BB.DATA  = 'Fixed Due To Banks =1D (1 day)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1D.MM.LCY:","
    BB.DATA := WS.AMT.1D.MM:","
    BB.DATA := WS.AVRG.MM.1D:","
    BB.DATA := WS.WG.AVRG.TOT.1D.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 7 DAYS ***

    IF WS.AMT.1W.MM EQ 0 THEN WS.AMT.1W.MM = 1

    WS.AVRG.MM.1W  = ( WS.WG.AVRG.TOT.1W.MM / WS.AMT.1W.MM ) * 100
    WS.AVRG.MM.1W  = DROUND(WS.AVRG.MM.1W,'2')

    IF WS.AMT.1W.MM EQ 1 THEN WS.AMT.1W.MM = 0

    BB.DATA  = 'Fixed Due To Banks >1D & <= 1W (7 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1W.MM.LCY:","
    BB.DATA := WS.AMT.1W.MM:","
    BB.DATA := WS.AVRG.MM.1W:","
    BB.DATA := WS.WG.AVRG.TOT.1W.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 30 DAYS ***

    IF WS.AMT.1M.MM EQ 0 THEN WS.AMT.1M.MM = 1

    WS.AVRG.MM.1M  = ( WS.WG.AVRG.TOT.1M.MM / WS.AMT.1M.MM ) * 100
    WS.AVRG.MM.1M  = DROUND(WS.AVRG.MM.1M,'2')

    IF WS.AMT.1M.MM EQ 1 THEN WS.AMT.1M.MM = 0

    BB.DATA  = 'Fixed Due To Banks >1W & <= 1M (30 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1M.MM.LCY:","
    BB.DATA := WS.AMT.1M.MM:","
    BB.DATA := WS.AVRG.MM.1M:","
    BB.DATA := WS.WG.AVRG.TOT.1M.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
**************************************************
    WS.AMT.1M.MM = 0 ; WS.AMT.1M.MM.LCY = 0 ; WS.WG.AVRG.TOT.1M.MM = 0 ; WS.AMT.2M.MM = 0 ; WS.AMT.2M.MM.LCY = 0
    WS.WG.AVRG.TOT.2M.MM = 0 ; WS.AMT.3M.MM = 0 ; WS.AMT.3M.MM.LCY = 0 ; WS.WG.AVRG.TOT.3M.MM = 0
    WS.AMT.6M.MM  = 0 ; WS.AMT.6M.MM.LCY = 0 ; WS.WG.AVRG.TOT.6M.MM = 0
    WS.AMT.1Y.MM  = 0 ; WS.AMT.1Y.MM.LCY = 0 ; WS.WG.AVRG.TOT.1Y.MM = 0
    WS.AVRG.MM.1M = 0 ; WS.AVRG.MM.2M    = 0 ; WS.AVRG.MM.3M = 0 ; WS.AVRG.MM.6M = 0 ; WS.AVRG.MM.1Y = 0
************************************************
**** Arab Trade Finance Program (Money Market) ****

    T.SEL = "SELECT ":FN.CBE:" WITH CATEGORY EQ 21037 AND DEAL.CCY EQ ":CCY.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)
            TRNS.ID = FIELD(KEY.LIST<I>,"*",5)

            CALL F.READ(FN.MM,TRNS.ID,R.MM,F.MM,E1)
            WS.RATE.TYPE.CODE = R.MM<MM.INT.RATE.TYPE>
            WS.CCY            = R.MM<MM.CURRENCY>

            IF WS.RATE.TYPE.CODE EQ 3 THEN

                WS.AMT.MM      = R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.TOT.MM += WS.AMT.MM

                WS.AMT.MM.LCY      = R.CBE<CUPOS.LCY.AMOUNT>
                WS.AMT.TOT.MM.LCY += WS.AMT.MM

                WS.MAT.DATE    = R.CBE<CUPOS.MATURITY.DATE>

                WS.MM.KEY = R.MM<MM.INTEREST.KEY>
                BI.ID.KEY = WS.MM.KEY:WS.CCY

                CALL F.READ(FN.BID,BI.ID.KEY,R.BID,F.BID,E4)
                BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
                BI.ID = BI.ID.KEY:BI.DATE

                CALL F.READ(FN.BI,BI.ID,R.BI,F.BI,E3)
                WS.RATE   = R.BI<EB.BIN.INTEREST.RATE>
                WS.SPREAD = R.MM<MM.INTEREST.SPREAD.1>

                WS.RATE.TOTAL     = WS.RATE + WS.SPREAD
                WS.AVG.MM         = ( WS.AMT.MM * WS.RATE.TOTAL ) / 100
                WS.WG.AVRG.TOT.MM += WS.AVG.MM
            END

            IF WS.RATE.TYPE.CODE EQ 1 THEN

                WS.AMT.MM.1      = R.CBE<CUPOS.DEAL.AMOUNT>
                WS.AMT.TOT.MM.1 += WS.AMT.MM.1

                WS.AMT.MM.1.LCY      = R.CBE<CUPOS.LCY.AMOUNT>
                WS.AMT.TOT.MM.1.LCY += WS.AMT.MM.1

                WS.MAT.DATE = R.CBE<CUPOS.MATURITY.DATE>
                WS.RATE     = R.MM<MM.INTEREST.RATE>
                WS.SPREAD   = R.MM<MM.INTEREST.SPREAD.1>

                WS.RATE.TOTAL        = WS.RATE + WS.SPREAD
                WS.AVG.MM.1          = ( WS.AMT.MM.1 * WS.RATE.TOTAL ) / 100
                WS.WG.AVRG.TOT.MM.1 += WS.AVG.MM.1

                DAYS = "C"
                IF WS.MAT.DATE NE '' THEN
                    CALL CDD("",DAT.2,WS.MAT.DATE,DAYS)
                END ELSE
                    DAYS = ''
                END

                IF DAYS EQ 1 THEN
                    WS.AMT.1D.MM         += WS.AMT.MM.1
                    WS.AMT.1D.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1D.MM += WS.AVG.MM.1
                END

                IF DAYS GE 2 AND DAYS LE 7 THEN
                    WS.AMT.1W.MM         += WS.AMT.MM.1
                    WS.AMT.1W.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1W.MM += WS.AVG.MM.1
                END

                IF DAYS GT 7 AND DAYS LE 30 THEN
                    WS.AMT.1M.MM         += WS.AMT.MM.1
                    WS.AMT.1M.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1M.MM += WS.AVG.MM.1
                END

                IF DAYS GT 30 AND DAYS LE 60 THEN
                    WS.AMT.2M.MM         += WS.AMT.MM.1
                    WS.AMT.2M.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.2M.MM += WS.AVG.MM.1
                END

                IF DAYS GT 60 AND DAYS LE 91 THEN
                    WS.AMT.3M.MM         += WS.AMT.MM.1
                    WS.AMT.3M.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.3M.MM += WS.AVG.MM.1
                END

                IF DAYS GT 91 AND DAYS LE 182 THEN
                    WS.AMT.3M.MM         += WS.AMT.MM.1
                    WS.AMT.3M.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.3M.MM += WS.AVG.MM.1
                END

                IF DAYS GT 182 AND DAYS LE 364 THEN
                    WS.AMT.1Y.MM         += WS.AMT.MM.1
                    WS.AMT.1Y.MM.LCY     += WS.AMT.MM.1.LCY
                    WS.WG.AVRG.TOT.1Y.MM += WS.AVG.MM.1
                END

            END
        NEXT I
    END

***************
*** 1 DAY   ***

    IF WS.AMT.1D.MM EQ 0 THEN WS.AMT.1D.MM = 1

    WS.AVRG.MM.1D  = ( WS.WG.AVRG.TOT.1D.MM / WS.AMT.1D.MM ) * 100
    WS.AVRG.MM.1D  = DROUND(WS.AVRG.MM.1D,'2')

    IF WS.AMT.1D.MM EQ 1 THEN WS.AMT.1D.MM = 0

    BB.DATA  = 'Arab Trade Finance Program =1D (1 day)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1D.MM.LCY:","
    BB.DATA := WS.AMT.1D.MM:","
    BB.DATA := WS.AVRG.MM.1D:","
    BB.DATA := WS.WG.AVRG.TOT.1D.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 7 DAYS ***

    IF WS.AMT.1W.MM EQ 0 THEN WS.AMT.1W.MM = 1

    WS.AVRG.MM.1W  = ( WS.WG.AVRG.TOT.1W.MM / WS.AMT.1W.MM ) * 100
    WS.AVRG.MM.1W  = DROUND(WS.AVRG.MM.1W,'2')

    IF WS.AMT.1W.MM EQ 1 THEN WS.AMT.1W.MM = 0

    BB.DATA  = 'Arab Trade Finance Program >1D & <= 1W (7 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1W.MM.LCY:","
    BB.DATA := WS.AMT.1W.MM:","
    BB.DATA := WS.AVRG.MM.1W:","
    BB.DATA := WS.WG.AVRG.TOT.1W.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 30 DAYS ***

    IF WS.AMT.1M.MM EQ 0 THEN WS.AMT.1M.MM = 1

    WS.AVRG.MM.1M  = ( WS.WG.AVRG.TOT.1M.MM / WS.AMT.1M.MM ) * 100
    WS.AVRG.MM.1M  = DROUND(WS.AVRG.MM.1M,'2')

    IF WS.AMT.1M.MM EQ 1 THEN WS.AMT.1M.MM = 0

    BB.DATA  = 'Arab Trade Finance Program >1W & <= 1M (30 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1M.MM.LCY:","
    BB.DATA := WS.AMT.1M.MM:","
    BB.DATA := WS.AVRG.MM.1M:","
    BB.DATA := WS.WG.AVRG.TOT.1M.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 60 DAYS ***

    IF WS.AMT.2M.MM EQ 0 THEN WS.AMT.2M.MM = 1

    WS.AVRG.MM.2M  = ( WS.WG.AVRG.TOT.2M.MM / WS.AMT.2M.MM ) * 100
    WS.AVRG.MM.2M  = DROUND(WS.AVRG.MM.2M,'2')

    IF WS.AMT.2M.MM EQ 1 THEN WS.AMT.2M.MM = 0

    BB.DATA  = 'Arab Trade Finance Program >1M & <= 2M (60 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.2M.MM.LCY:","
    BB.DATA := WS.AMT.2M.MM:","
    BB.DATA := WS.AVRG.MM.2M:","
    BB.DATA := WS.WG.AVRG.TOT.2M.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 91 DAYS ***

    IF WS.AMT.3M.MM EQ 0 THEN WS.AMT.3M.MM = 1

    WS.AVRG.MM.3M  = ( WS.WG.AVRG.TOT.3M.MM / WS.AMT.3M.MM ) * 100
    WS.AVRG.MM.3M  = DROUND(WS.AVRG.MM.3M,'2')

    IF WS.AMT.3M.MM EQ 1 THEN WS.AMT.3M.MM = 0

    BB.DATA  = 'Arab Trade Finance Program >2M & <= 3M (91 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.3M.MM.LCY:","
    BB.DATA := WS.AMT.3M.MM:","
    BB.DATA := WS.AVRG.MM.3M:","
    BB.DATA := WS.WG.AVRG.TOT.3M.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 182 DAYS ***

    IF WS.AMT.6M.MM EQ 0 THEN WS.AMT.6M.MM = 1

    WS.AVRG.MM.6M  = ( WS.WG.AVRG.TOT.6M.MM / WS.AMT.6M.MM ) * 100
    WS.AVRG.MM.6M  = DROUND(WS.AVRG.MM.6M,'2')

    IF WS.AMT.6M.MM EQ 1 THEN WS.AMT.6M.MM = 0

    BB.DATA  = 'Arab Trade Finance Program >3M & <= 6M (182 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.6M.MM.LCY:","
    BB.DATA := WS.AMT.6M.MM:","
    BB.DATA := WS.AVRG.MM.6M:","
    BB.DATA := WS.WG.AVRG.TOT.6M.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


*** 364 DAYS ***

    IF WS.AMT.1Y.MM EQ 0 THEN WS.AMT.1Y.MM = 1

    WS.AVRG.MM.1Y  = ( WS.WG.AVRG.TOT.1Y.MM / WS.AMT.1Y.MM ) * 100
    WS.AVRG.MM.1Y  = DROUND(WS.AVRG.MM.1Y,'2')

    IF WS.AMT.1Y.MM EQ 1 THEN WS.AMT.1Y.MM = 0

    BB.DATA  = 'Arab Trade Finance Program >6M & <= 1Y (364 days)':","
    BB.DATA := 'Fixed':","
    BB.DATA := WS.AMT.1Y.MM.LCY:","
    BB.DATA := WS.AMT.1Y.MM:","
    BB.DATA := WS.AVRG.MM.1Y:","
    BB.DATA := WS.WG.AVRG.TOT.1Y.MM:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*==============================================================
GET.DETAIL:

    CALL F.READ(FN.CBE,KEY.LIST<I>,R.CBE,F.CBE,E1)

    WS.AMT     = R.CBE<CUPOS.DEAL.AMOUNT>
    WS.CCY     = R.CBE<CUPOS.DEAL.CCY>

    IF DAT.1 EQ DAT.2 THEN
        WS.AMT.LCY = WS.AMT * CCY.RATE
    END ELSE
        WS.AMT.LCY = R.CBE<CUPOS.LCY.AMOUNT>
    END

    WS.AMT.TOT     += WS.AMT
    WS.AMT.TOT.LCY += WS.AMT.LCY

    APP.REF.ID  = FIELD(KEY.LIST<I>,"*",2)
    TRNS.ID     = FIELD(KEY.LIST<I>,"*",5)
    WS.COMP     = R.CBE<CUPOS.CO.CODE>
    WS.CATEG    = R.CBE<CUPOS.CATEGORY>

    IF APP.REF.ID EQ 'LD' OR APP.REF.ID EQ 'CD' THEN
        WS.MAT.DATE = R.CBE<CUPOS.MATURITY.DATE>
        WS.VAL.DATE = R.CBE<CUPOS.VALUE.DATE>

        CALL F.READ(FN.LD,TRNS.ID,R.LD,F.LD,E1)
        WS.RATE           = R.LD<LD.INTEREST.RATE>
        WS.RATE.TYPE.CODE = R.LD<LD.INT.RATE.TYPE>
        WS.SPREAD         = R.LD<LD.INTEREST.SPREAD>
        WS.LD.RATE        = R.LD<LD.INTEREST.RATE>
        WS.LD.INV.AMT     = R.LD<LD.LOCAL.REF><1,LDLR.INVESTED.AMOUNT>
        WS.NXT.INT.AMT    = R.LD<LD.TOT.INTEREST.AMT>


        IF WS.LD.RATE EQ '' THEN
            WS.LD.KEY = R.LD<LD.INTEREST.KEY>
            BI.ID.KEY = WS.LD.KEY:WS.CCY

            CALL F.READ(FN.BID,BI.ID.KEY,R.BID,F.BID,E4)
            BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
            BI.ID = BI.ID.KEY:BI.DATE

            CALL F.READ(FN.BI,BI.ID,R.BI,F.BI,E3)
            WS.RATE = R.BI<EB.BIN.INTEREST.RATE>

        END
        IF WS.RATE.TYPE.CODE EQ 3 THEN
            WS.RATE.TYPE = 'FLOATING'
        END ELSE
            WS.RATE.TYPE = 'FIXED'
        END

        WS.RATE.T = WS.RATE + WS.SPREAD

********************************
        DAYS1 = "C"
        IF WS.MAT.DATE NE '' THEN
            CALL CDD("",WS.VAL.DATE,WS.MAT.DATE,DAYS1)
        END ELSE
            DAYS1 = ''
        END
    END
********************************
    IF APP.REF.ID EQ 'AC' THEN
        IF WS.CATEG NE '1002' THEN
            WS.RATE.TYPE = 'FIXED'
            CALL F.READ(FN.AC,TRNS.ID,R.AC,F.AC,E1)

            GROUP.ID    = R.AC<AC.CONDITION.GROUP>

            CALL F.READ(FN.GC,GROUP.ID,R.GC,F.GC,E1)
            WS.MAT.DATE  = R.GC<IC.GCP.CR.CAP.FREQUENCY>[1,8]

*********** SELECT FROM ACI *****

*Line [ 2851 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.AC.COUNT = DCOUNT(R.AC<AC.ACCT.CREDIT.INT>,@VM)
            AD.DATE     = R.AC<AC.ACCT.CREDIT.INT><1,WS.AC.COUNT>
            ACI.ID      = TRNS.ID:'-':AD.DATE

            CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 2857 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
            WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>
            IF WS.RATE EQ '' THEN

**** SELECT FROM BASIC.INTEREST ****

                ACI.BI.KEY    = R.ACI<IC.ACI.CR.BASIC.RATE>
                ACI.BI.ID.KEY = ACI.BI.KEY:WS.CCY
                ACI.OPER      = R.ACI<IC.ACI.CR.MARGIN.OPER>
                ACI.MRG.RATE  = R.ACI<IC.ACI.CR.MARGIN.RATE>

                CALL F.READ(FN.BID,ACI.BI.ID.KEY,R.BID,F.BID,E4)
                ACI.BI.DATE = R.BID<EB.BID.EFFECTIVE.DATE,1>
                ACI.BI.ID   = ACI.BI.ID.KEY:ACI.BI.DATE

                CALL F.READ(FN.BI,ACI.BI.ID,R.BI,F.BI,E3)

                WS.RATE = R.BI<EB.BIN.INTEREST.RATE>
                IF ACI.OPER EQ 'SUBTRACT' THEN
                    WS.RATE = WS.RATE - ACI.MRG.RATE
                END
                IF ACI.OPER EQ 'ADD' THEN
                    WS.RATE = WS.RATE + ACI.MRG.RATE
                END

                IF WS.RATE EQ '' THEN

**** SELECT RATE FROM GCI ****

                    GD.ID   = GROUP.ID:WS.CCY
                    CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
                    GD.DATE = R.GD<AC.GRD.CREDIT.GROUP.DATE>
                    IF GD.DATE EQ '' THEN
                        GD.DATE = R.GD<AC.GRD.CREDIT.DATES,1>
                    END
                    GCI.ID  = GD.ID:GD.DATE

                    CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
*Line [ 2896 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    WS.RATE.COUNT   = DCOUNT(R.GCI<IC.GCI.CR.INT.RATE>,@VM)

                    FOR KK = 1 TO WS.RATE.COUNT
                        WS.AMT.POS   = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK>
                        IF KK = 1 THEN
                            WS.AMT.POS.1 = 0
                        END ELSE
                            WS.AMT.POS.1 = R.GCI<IC.GCI.CR.LIMIT.AMT><1,KK-1>
                        END
                        IF WS.AMT GT WS.AMT.POS.1 AND WS.AMT LE WS.AMT.POS THEN
                            WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,KK>
                        END
                        WS.LAST.AMT = R.GCI<IC.GCI.CR.LIMIT.AMT><1,WS.RATE.COUNT-1>
                        IF WS.AMT GT WS.LAST.AMT THEN
                            WS.RATE = R.GCI<IC.GCI.CR.INT.RATE><1,WS.RATE.COUNT>
                        END
                    NEXT KK

                    WS.OPER   = R.GCI<IC.GCI.CR.MARGIN.OPER>
                    WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE,1>
                    IF WS.SPREAD = '' THEN
                        WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE,2>
                    END

                    IF WS.OPER EQ 'SUBTRACT' THEN
                        WS.SPREAD = '-':WS.SPREAD
                    END



                END
            END
        END
        IF WS.CATEG EQ '1002' THEN
            GCI.ID.1002 = '1EGP...'
            T.SEL3 = "SELECT ":FN.GCI:" WITH @ID LIKE ":GCI.ID.1002:" BY @ID"
            CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
            IF SELECTED3 THEN
                CALL F.READ(FN.GCI,KEY.LIST3<SELECTED3>,R.GCI,F.GCI,E3)
                WS.RATE = R.GCI<IC.GCI.CR.INT.RATE,1>
            END
        END
    END

    WS.RATE.TOTAL   = WS.RATE + WS.SPREAD
    WS.AVG          = ( WS.AMT * WS.RATE.TOTAL ) / 100
    WS.WG.AVRG.TOT += WS.AVG

    DAYS = "C"
    IF WS.MAT.DATE NE '' THEN
        CALL CDD("",DAT.2,WS.MAT.DATE,DAYS)
    END ELSE
        DAYS = ''
    END

    IF FLAG EQ 'LD' THEN
        IF DAYS EQ 1 THEN
            WS.AMT.1D         += WS.AMT
            WS.AMT.1D.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.1D += WS.AVG
        END

        IF DAYS GE 2 AND DAYS LE 7 THEN
            WS.AMT.1W         += WS.AMT
            WS.AMT.1W.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.1W += WS.AVG
        END

        IF DAYS GT 7 AND DAYS LE 30 THEN
            WS.AMT.1M         += WS.AMT
            WS.AMT.1M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.1M += WS.AVG
        END

        IF DAYS GT 30 AND DAYS LE 60 THEN
            WS.AMT.2M         += WS.AMT
            WS.AMT.2M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.2M += WS.AVG
        END

        IF DAYS GT 60 AND DAYS LE 90 THEN
            WS.AMT.3M         += WS.AMT
            WS.AMT.3M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.3M += WS.AVG
        END

        IF DAYS GT 90 AND DAYS LE 180 THEN
            WS.AMT.6M         += WS.AMT
            WS.AMT.6M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.6M += WS.AVG
        END

        IF DAYS GT 180 AND DAYS LE 274 THEN
            WS.AMT.9M         += WS.AMT
            WS.AMT.9M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.9M += WS.AVG
        END

        IF DAYS GT 274 AND DAYS LE 366 THEN
            WS.AMT.1Y         += WS.AMT
            WS.AMT.1Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.1Y += WS.AVG
        END

        IF DAYS GT 366 AND DAYS LE 549 THEN
            WS.AMT.1.5Y         += WS.AMT
            WS.AMT.1.5Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.1.5Y += WS.AVG
        END

        IF DAYS GT 549 AND DAYS LE 730 THEN
            WS.AMT.2Y         += WS.AMT
            WS.AMT.2Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.2Y += WS.AVG
        END

        IF DAYS GT 730 AND DAYS LE 1095 THEN
            WS.AMT.3Y         += WS.AMT
            WS.AMT.3Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.3Y += WS.AVG
        END

        IF DAYS GT 1095 AND DAYS LE 1460 THEN
            WS.AMT.4Y         += WS.AMT
            WS.AMT.4Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.4Y += WS.AVG
        END

        IF DAYS GT 1460 AND DAYS LE 1827 THEN
            WS.AMT.5Y         += WS.AMT
            WS.AMT.5Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.5Y += WS.AVG
        END

        IF DAYS GT 1827 AND DAYS LE 2190 THEN
            WS.AMT.6Y         += WS.AMT
            WS.AMT.6Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.6Y += WS.AVG
        END

        IF DAYS GT 2190 AND DAYS LE 2555 THEN
            WS.AMT.7Y         += WS.AMT
            WS.AMT.7Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.7Y += WS.AVG
        END

        IF DAYS GT 2555 THEN
            WS.AMT.GT7Y         += WS.AMT
            WS.AMT.GT7Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.GT7Y += WS.AVG
        END
    END

    IF FLAG = 'CD' THEN
        IF DAYS EQ 1 THEN
            WS.AMT.CD.1D         += WS.AMT
            WS.AMT.CD.1D.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.1D += WS.AVG
        END

        IF DAYS GE 2 AND DAYS LE 7 THEN
            WS.AMT.CD.1W         += WS.AMT
            WS.AMT.CD.1W.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.1W += WS.AVG
        END

        IF DAYS GT 7 AND DAYS LE 30 THEN
            WS.AMT.CD.1M         += WS.AMT
            WS.AMT.CD.1M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.1M += WS.AVG
        END

        IF DAYS GT 30 AND DAYS LE 60 THEN
            WS.AMT.CD.2M         += WS.AMT
            WS.AMT.CD.2M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.2M += WS.AVG
        END

        IF DAYS GT 60 AND DAYS LE 90 THEN
            WS.AMT.CD.3M         += WS.AMT
            WS.AMT.CD.3M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.3M += WS.AVG
        END

        IF DAYS GT 90 AND DAYS LE 180 THEN
            WS.AMT.CD.6M         += WS.AMT
            WS.AMT.CD.6M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.6M += WS.AVG
        END

        IF DAYS GT 180 AND DAYS LE 274 THEN
            WS.AMT.CD.9M         += WS.AMT
            WS.AMT.CD.9M.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.9M += WS.AVG
        END

        IF DAYS GT 274 AND DAYS LE 366 THEN
            WS.AMT.CD.1Y         += WS.AMT
            WS.AMT.CD.1Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.1Y += WS.AVG
        END

        IF DAYS GT 366 AND DAYS LE 549 THEN
            WS.AMT.CD.1.5Y         += WS.AMT
            WS.AMT.CD.1.5Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.1.5Y += WS.AVG
        END

        IF DAYS GT 549 AND DAYS LE 730 THEN
            WS.AMT.CD.2Y         += WS.AMT
            WS.AMT.CD.2Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.2Y += WS.AVG
        END

        IF DAYS GT 730 AND DAYS LE 1095 THEN
            WS.AMT.CD.3Y         += WS.AMT
            WS.AMT.CD.3Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.3Y += WS.AVG
        END

        IF DAYS GT 1095 AND DAYS LE 1460 THEN
            WS.AMT.CD.4Y         += WS.AMT
            WS.AMT.CD.4Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.4Y += WS.AVG
        END

        IF DAYS GT 1460 AND DAYS LE 1827 THEN
            WS.AMT.CD.5Y         += WS.AMT
            WS.AMT.CD.5Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.5Y += WS.AVG
        END

        IF DAYS GT 1827 AND DAYS LE 2190 THEN
            WS.AMT.CD.6Y         += WS.AMT
            WS.AMT.CD.6Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.6Y += WS.AVG
        END

        IF DAYS GT 2190 AND DAYS LE 2555 THEN
            WS.AMT.CD.7Y         += WS.AMT
            WS.AMT.CD.7Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.7Y += WS.AVG
        END

        IF DAYS GT 2555 THEN
            WS.AMT.CD.GT7Y         += WS.AMT
            WS.AMT.CD.GT7Y.LCY     += WS.AMT.LCY
            WS.WG.AVRG.TOT.CD.GT7Y += WS.AVG
        END

    END
********************************
RETURN
