* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-94</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE SAVING.LIST.TXT
*    PROGRAM SAVING.LIST.TXT
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LMM.ACCOUNT.BALANCES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

*****
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "SAV.LIST.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/":' ':"SAV.LIST.TXT"
        HUSH OFF
    END
    OPENSEQ "/IMPL2/TEST/NT24/bnk/bnk.run/&SAVEDLISTS&/" , "SAV.LIST.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SAV.LIST.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create SAV.LIST.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END
*****
    FN.LD  = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF( FN.LD,F.LD)
    FN.LM  = 'FBNK.LMM.ACCOUNT.BALANCES' ; F.LM = '' ; R.LM = ''
    CALL OPF( FN.LM,F.LM)

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH  STATUS NE 'FWD' AND (CATEGORY GE 21001 AND CATEGORY LE 21010) AND (( VALUE.DATE LE 20100930 AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT 20100930 AND AMOUNT EQ 0 )) AND CATEGORY NE '' BY CO.CODE BY CUSTOMER.ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*DEBUG
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>, R.LD, F.LD, ETEXT)
            BRN = R.LD<LD.CO.CODE>
            BRANCH = BRN[8,2]
            IF BRANCH[1,1] EQ 0 THEN
                BRANCH = BRANCH[2,1]
            END
*Line [ 70 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH,BRAN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRAN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            BRAN.NAME = FIELD(BRAN.NAME,'.',2)

            LD.ID  = KEY.LIST<I>
            CUS.ID = R.LD<LD.CUSTOMER.ID>
            CURR   = R.LD<LD.CURRENCY>

            IF R.LD<LD.AMOUNT> NE 0 THEN
                AMT = R.LD<LD.AMOUNT>
            END ELSE
                AMT = R.LD<LD.REIMBURSE.AMOUNT>
            END

            RATE   = R.LD<LD.INTEREST.RATE>
            SPREAD = R.LD<LD.INTEREST.SPREAD>
            TOT.INT.AMT   = R.LD<LD.TOT.INTEREST.AMT>
            CAT.ID        = R.LD<LD.CATEGORY>
            VALUE.DATE    = R.LD<LD.VALUE.DATE>
            FIN.MAT.DATE  = R.LD<LD.FIN.MAT.DATE>
            RENEW.IND     = R.LD<LD.LOCAL.REF><1,LDLR.RENEW.IND>
            RENEW.METHOD  = R.LD<LD.LOCAL.REF><1,LDLR.RENEW.METHOD>
            LD.NEW.ID     = LD.ID:'00'
            CALL F.READ(FN.LM,LD.NEW.ID, R.LM, F.LM, ETEXT)
            COMM.INT = R.LM<LD27.OUTS.CUR.ACC.I.PAY>

            BB.DATA  = BRAN.NAME:'|'
            BB.DATA := LD.ID:'|'
            BB.DATA := CUS.ID:'|'
            BB.DATA := CURR:'|'
            BB.DATA := AMT:'|'
            BB.DATA := RATE:'|'
            BB.DATA := SPREAD:'|'
            BB.DATA := TOT.INT.AMT:'|'
            BB.DATA := CAT.ID:'|'
            BB.DATA := VALUE.DATE:'|'
            BB.DATA := FIN.MAT.DATE:'|'
            BB.DATA := RENEW.IND:'|'
            BB.DATA := RENEW.METHOD:'|'
            BB.DATA := COMM.INT

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    CLOSESEQ BB

    PRINT "FINISHED"
*===================


END
