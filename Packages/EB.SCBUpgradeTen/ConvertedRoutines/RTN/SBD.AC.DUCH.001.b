* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBD.AC.DUCH.001

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SECTOR
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GROUP.DATE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COUNTRY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUST.FI.RATING

*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "DAILY.AC.DUCH.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DAILY.AC.DUCH.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DAILY.AC.DUCH.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DAILY.AC.DUCH.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create DAILY.AC.DUCH.CSV File IN &SAVEDLISTS&'
        END
    END

    CUS.ID      = ''  ; CUST.NAME  = '' ; WS.CCY    = '' ; WS.AMT = 0        ; WS.BUCKET.LABEL   = ''
    WS.MAT.DATE = ''  ; WS.RATE    = 0  ; WS.SPREAD = 0  ; CUST.SECTOR  = '' ; WS.RATE.TYPE.CODE = ''
    DAYS        = ''  ; INT.EXPENS = 0  ; WS.CATEG  = 0  ; WS.RATE.TYPE = 'FIXED'

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "DAILY.BANKS.CURRENT.ACCOUNTS"
    HEAD.DESC = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "CURRENCY":","
    HEAD.DESC := "AMOUNT.EQUIVALENT":","
    HEAD.DESC := "NATIVE.AMOUNT":","
    HEAD.DESC := "MATURITY.DATE":","
    HEAD.DESC := "RATE":","
    HEAD.DESC := "SPREAD.RATE":","
    HEAD.DESC := "TYPE.OF.RATE":","
    HEAD.DESC := "CUSTOMER.TYPE":","
    HEAD.DESC := "SECTOR.NAME":","
    HEAD.DESC := "DAYS.TO.MATURITY":","
    HEAD.DESC := "INTEREST.EXPENSES":","
    HEAD.DESC := "BUCKET.LABEL":","
    HEAD.DESC := "LEDGER":","
    HEAD.DESC := "PRODUCT.TYPE":","
    HEAD.DESC := "BRANCH":","
    HEAD.DESC := "NATIONALITY":","
    HEAD.DESC := "LONG.TERM.RATE":","
    HEAD.DESC := "SHORT.TERM.RATE":","
    HEAD.DESC := "FIRST.LONG.TERM.RATE":","
    HEAD.DESC := "FIRST.SHORT.TERM.RATE":","
    HEAD.DESC := "RATING.AGENCY":","
    HEAD.DESC := "RATING.DATE":","
    HEAD.DESC := "Segmentation.Desc":","
    HEAD.DESC := "Interest.Frequency":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN

*========================================================================
PROCESS:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.GA = 'F.CATEGORY' ; F.GA = ''
    CALL OPF(FN.GA,F.GA)

    FN.GCI = 'FBNK.GROUP.CREDIT.INT' ; F.GCI = ''
    CALL OPF(FN.GCI,F.GCI)

    FN.ACI = 'FBNK.ACCOUNT.CREDIT.INT' ; F.ACI = ''
    CALL OPF(FN.ACI,F.ACI)

    FN.GD = 'FBNK.GROUP.DATE' ; F.GD = ''
    CALL OPF(FN.GD,F.GD)

    FN.AD = 'FBNK.ACCOUNT.DATE' ; F.AD = ''
    CALL OPF(FN.AD,F.AD)

    FN.GC = 'FBNK.GROUP.CAPITALISATION' ; F.GC = ''
    CALL OPF(FN.GC,F.GC)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.NAT = 'F.COUNTRY' ; F.NAT = ''
    CALL OPF(FN.NAT,F.NAT)

    FN.RAT = 'F.SCB.CUST.FI.RATING' ; F.RAT = ''
    CALL OPF(FN.RAT,F.RAT)

    FN.RAT.H = 'F.SCB.CUST.FI.RATING$HIS' ; F.RAT.H = ''
    CALL OPF(FN.RAT.H,F.RAT.H)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* BANK'S CURRENT ACCOUNTS SELECTION *************

    T.SEL = "SELECT ":FN.AC:" WITH CATEGORY IN (5070 5094 5093 5080 5079 2000 5021 5000 2001 5001 5020 2002 2003) BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
    RETURN
*==============================================================
PRINT.DET:

    BB.DATA  = CUS.ID:","
    BB.DATA := CUST.NAME:","
    BB.DATA := WS.CCY:","
    BB.DATA := WS.AMT:","
    BB.DATA := WS.AMT.FCY:","
    BB.DATA := WS.MAT.DATE:","
    BB.DATA := WS.RATE:","
    BB.DATA := WS.SPREAD:","
    BB.DATA := WS.RATE.TYPE:","
    BB.DATA := CUST.SECTOR:","
    BB.DATA := SECTOR.NAME:","
    BB.DATA := DAYS:","
    BB.DATA := INT.EXPENS:","
    BB.DATA := WS.BUCKET.LABEL:","
    BB.DATA := WS.CATEG:","
    BB.DATA := DESC:","
    BB.DATA := WS.BRANCH.NAME:","
    BB.DATA := WS.NAT:","
    BB.DATA := WS.LONG.TRM.RATE:","
    BB.DATA := WS.SHORT.TRM.RATE:","
    BB.DATA := WS.LONG.TRM.RATE.H:","
    BB.DATA := WS.SHORT.TRM.RATE.H:","
    BB.DATA := WS.RATE.AGN:","
    BB.DATA := WS.RATE.DATE:","
    BB.DATA := WS.TARGET:","
    BB.DATA := WS.TARGET.NAME:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*===============================================================
GET.DETAIL:

    CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
    CUS.ID      = R.AC<AC.CUSTOMER>

    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
    CUST.NAME        = R.CU<EB.CUS.NAME.1>
    CUST.SECTOR.CODE = R.CU<EB.CUS.SECTOR>
    WS.CUS.NAT       = R.CU<EB.CUS.NATIONALITY>

    WS.TARGET      = R.CU<EB.CUS.TARGET>
    CALL F.READ(FN.TAR,WS.TARGET,R.TAR,F.TAR,E3)
    WS.TARGET.NAME = R.TAR<EB.TAR.DESCRIPTION>

    CALL F.READ(FN.RAT,CUS.ID,R.RAT,F.RAT,E4)
    WS.LONG.TRM.RATE  = R.RAT<CU.RAT.LONG.TRM.RAT>
    WS.SHORT.TRM.RATE = R.RAT<CU.RAT.SHORT.TRM.RAT>
    WS.RATE.AGN       = R.RAT<CU.RAT.RATING.AGENCY>
    WS.RATE.DATE      = R.RAT<CU.RAT.RATING.DATE>

    CUS.ID.H = CUS.ID:';1'
    CALL F.READ(FN.RAT.H,CUS.ID.H,R.RAT.H,F.RAT.H,E4)
    WS.LONG.TRM.RATE.H  = R.RAT.H<CU.RAT.LONG.TRM.RAT>
    WS.SHORT.TRM.RATE.H = R.RAT.H<CU.RAT.SHORT.TRM.RAT>

    CALL F.READ(FN.NAT,WS.CUS.NAT,R.NAT,F.NAT,E4)
    WS.NAT = R.NAT<EB.COU.COUNTRY.NAME,1>

    CALL F.READ(FN.SEC,CUST.SECTOR.CODE,R.SEC,F.SEC,E1)
    SECTOR.NAME = R.SEC<EB.SEC.DESCRIPTION,2>

    IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1300 OR CUST.SECTOR.CODE EQ 1400 OR CUST.SECTOR.CODE EQ 2000 THEN
        IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1300 THEN
            CUST.SECTOR = 'Staff'
        END

        IF CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1400 THEN
            CUST.SECTOR = 'Staff.Relatives'
        END
        IF CUST.SECTOR.CODE EQ 2000 THEN
            CUST.SECTOR = 'Retail'
        END
    END ELSE
        CUST.SECTOR = 'Corporate'
    END

    WS.CCY     = R.AC<AC.CURRENCY>
    WS.AMT.FCY = R.AC<AC.OPEN.ACTUAL.BAL>
    WS.BRANCH  = R.AC<AC.CO.CODE>

    CALL F.READ(FN.COM,WS.BRANCH,R.COM,F.COM,E4)
    WS.BRANCH.NAME = R.COM<EB.COM.COMPANY.NAME,1>

**  CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
**  LOCATE WS.CCY IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
**  AMT.RATE = R.CCY<RE.BCP.RATE,POS>

    CALL F.READ(FN.CCY,WS.CCY,R.CCY,F.CCY,E1)

    AMT.RATE = R.CCY<SBD.CURR.MID.RATE>

    IF WS.CCY EQ 'EGP' THEN
        AMT.RATE = 1
    END
    IF WS.CCY EQ 'JPY' THEN
        AMT.RATE = AMT.RATE / 100
    END

    WS.AMT   = WS.AMT.FCY * AMT.RATE
    WS.CATEG = R.AC<AC.CATEGORY>

    CALL F.READ(FN.GA,WS.CATEG,R.GA,F.GA,E1)
    DESC = R.GA<EB.CAT.DESCRIPTION,1>

    GROUP.ID  = R.AC<AC.CONDITION.GROUP>
    CALL F.READ(FN.GC,GROUP.ID,R.GC,F.GC,E1)
    WS.MAT.DATE  = R.GC<IC.GCP.CR.CAP.FREQUENCY>[1,8]

*********** SELECT FROM ACI *****

    CALL F.READ(FN.AD,KEY.LIST<I>,R.AD,F.AD,E2)
*Line [ 310 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.AC.COUNT = DCOUNT(R.AD<2>,@VM)
    AD.DATE     = R.AD<2><1,WS.AC.COUNT>
    ACI.ID      = KEY.LIST<I>:'-':AD.DATE

    CALL F.READ(FN.ACI,ACI.ID,R.ACI,F.ACI,E2)
*Line [ 316 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.RATE.COUNT   = DCOUNT(R.ACI<IC.ACI.CR.INT.RATE>,@VM)
    WS.RATE         = R.ACI<IC.ACI.CR.INT.RATE><1,WS.RATE.COUNT>
    IF WS.RATE EQ '' THEN

**** SELECT RATE FROM GCI ****

        GD.ID   = GROUP.ID:WS.CCY
        CALL F.READ(FN.GD,GD.ID,R.GD,F.GD,E2)
        GD.DATE = R.GD<AC.GRD.CREDIT.GROUP.DATE>
        GCI.ID  = GD.ID:GD.DATE

        CALL F.READ(FN.GCI,GCI.ID,R.GCI,F.GCI,E2)
        WS.RATE   = R.GCI<IC.GCI.CR.INT.RATE>
        WS.OPER   = R.GCI<IC.GCI.CR.MARGIN.OPER>
        WS.SPREAD = R.GCI<IC.GCI.CR.MARGIN.RATE>
        IF WS.OPER EQ 'SUBTRACT' THEN
            WS.SPREAD = '-':WS.SPREAD
        END
    END

********************************
    DAYS = "C"
    IF WS.MAT.DATE NE '' THEN
        CALL CDD("",DAT,WS.MAT.DATE,DAYS)
    END ELSE
        DAYS = ''
    END

    WS.RATE.ALL = WS.RATE + WS.SPREAD
    INT.EXPENS  = ( WS.AMT * WS.RATE.ALL * DAYS ) / 36500
    INT.EXPENS  = DROUND(INT.EXPENS,'0')

    IF DAYS EQ 1 THEN WS.BUCKET.LABEL = '1.DAY'
    IF DAYS GT 1 AND DAYS LE 7 THEN WS.BUCKET.LABEL = '1.WEEK'
    IF DAYS GT 7 AND DAYS LE 31 THEN WS.BUCKET.LABEL = '1.MONTH'
    IF DAYS GT 31 AND DAYS LE 93 THEN WS.BUCKET.LABEL = '3.MONTHS'
    IF DAYS GT 93 AND DAYS LE 186 THEN WS.BUCKET.LABEL = '6.MONTHS'
    IF DAYS GT 186 AND DAYS LE 370 THEN WS.BUCKET.LABEL = '1.YEAR'
    IF DAYS GT 370 AND DAYS LE 1090 THEN WS.BUCKET.LABEL = '3.YEAR'
    IF DAYS GT 1090 THEN WS.BUCKET.LABEL = 'MORE.THAN.3.YEAR'

    RETURN

*===============================================================
