* @ValidationCode : MjotMzkxMTE5NDMxOkNwMTI1MjoxNjQwODIxNDk4OTc2OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 15:44:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*** CREATED BY MOHAMMED SABRY 2014/4/16

PROGRAM SBD.DAILY.BATCH.02
*Line [ 19 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_COMMON
*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_EQUATE
**************************************
    EXECUTE "OFSADMINLOGIN"
**************************************
** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

***------------------------------------------------------------------------------------****
***����� ������� ����� �����
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"CONV.SAVE.TEXT.AA")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"CONV.CD.TEXT")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"DEPOSIT.R.TEXT.2")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"DEPOSIT.R.TEXT.3")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"DEPOSIT.R.TEXT.4")

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.BASIC.RATES.FILE")
*** ����� ����� ������ �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"TOT.CUST.POS.TODAY")
*** ����� ����� ������ D-ROOM
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBM.LD.DO")
*** ����� ��� ������� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"CONV.MM.MAST.TEXT")
*** ��� ������ - ������ ����������  --- NESSMA  2014/12/22
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"STMT.ENTRY.MAIL")
***  ����� ��� ���� �� ���� �����  --- NESSMA 2014/12/22
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"RTN.ACCT.STMT.MAIL")
*** ��� ������ ������� �� �����
***    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.ALL.GENLED.02")
*** ����� �������� ������� ��� SERVER
***    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"CPY.HOLD.RPT")
*** EDIT BY NESSMA ON 2018/12/17
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"EOD.REV.CARD.FEES")
***------------------------------------------------------------------------------------****
RETURN
***------------------------------------------------------------------------------------****
END
