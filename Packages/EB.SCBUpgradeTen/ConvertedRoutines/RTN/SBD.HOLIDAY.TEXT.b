* @ValidationCode : MjotMTcwODU4ODUxNzpDcDEyNTI6MTY0MDgyNTk4ODAxNDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:59:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
PROGRAM SBD.HOLIDAY.TEXT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.EMPLOYEE.HOLIDAY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "/hq/pyrl/user/pymrkz2" , "holiday.glbs" TO BB THEN
        CLOSESEQ BB

        HUSH ON
        EXECUTE 'DELETE ':"/hq/pyrl/user/pymrkz2":' ':"holiday.glbs"
        HUSH OFF
    END
    OPENSEQ "/hq/pyrl/user/pymrkz2" , "holiday.glbs" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE holiday.glbs CREATED IN /hq/pyrl/user/pymrkz2'
        END ELSE
            STOP 'Cannot create holiday.glbs File IN /hq/pyrl/user/pymrkz2'
        END
    END

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

**    DAT = '20120920'

RETURN

*========================================================================
PROCESS:
    FN.EH = 'F.SCB.EMPLOYEE.HOLIDAY' ; F.EH = ''
    CALL OPF(FN.EH,F.EH)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
*-------------------------------------------

    T.SEL  = "SELECT ":FN.EH:" WITH INPUT.DATE EQ ":DAT:" BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.EH,KEY.LIST<I>,R.EH,F.EH,E1)

*Line [ 74 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            INP.DAT = DCOUNT(R.EH<EH.INPUT.DATE>,@VM)
            FOR X = 1 TO INP.DAT

                IF R.EH<EH.INPUT.DATE,X> EQ DAT THEN

                    EMP.NO     = R.EH<EH.EMP.NO>
                    START.DAT  = R.EH<EH.START.DATE,X>
                    START.DATE = START.DAT[7,2]:START.DAT[5,2]:START.DAT[1,4]

                    END.DAT    = R.EH<EH.END.DATE,X>
                    END.DATE   = END.DAT[7,2]:END.DAT[5,2]:END.DAT[1,4]

                    IN.DAT    = R.EH<EH.INPUT.DATE,X>
                    IN.DATE   = IN.DAT[7,2]:IN.DAT[5,2]:IN.DAT[1,4]

                    DAYS.NO    = R.EH<EH.NO.OF.DAYS,X>
                    HOL.TYPE   = R.EH<EH.TYPE,X>

                    BB.DATA  = EMP.NO:"|"
                    BB.DATA := START.DATE:"|"
                    BB.DATA := END.DATE:"|"
                    BB.DATA := DAYS.NO:"|"
                    BB.DATA := HOL.TYPE:"|"
                    BB.DATA := IN.DATE:"|"

                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            NEXT X
        NEXT I
    END
RETURN
END
