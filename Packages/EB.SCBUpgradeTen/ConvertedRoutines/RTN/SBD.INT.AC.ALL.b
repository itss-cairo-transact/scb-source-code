* @ValidationCode : MjotMTU0NTE2NDk3MjpDcDEyNTI6MTY0MDgyNjU0NjIzMTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:09:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>272</Rating>
*-----------------------------------------------------------------------------
*    SUBROUTINE SBD.INT.AC.ALL
PROGRAM SBD.INT.AC.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 44 ] HASHING $INCLUDE I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB END.PROG

RETURN
*----------------------------
END.PROG:
    PRINT "DONE"
*    PRINT " "
*    PRINT " "
*    PRINT SPACE(55) : "*********** END.OF.REPORT **********"
*    CALL PRINTER.OFF
*    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*-----------------------------INITIALIZATIONS------------------------
INITIATE:
    REPORT.ID='SBD.INT.AC.ALL'
*    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)
*****
    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "INTERNAL.ACCOUNTS.ALL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/TEST/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"INTERNAL.ACCOUNTS.ALL.CSV"
        HUSH OFF
    END
    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "INTERNAL.ACCOUNTS.ALL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE INTERNAL.ACCOUNTS.ALL.CSV CREATED IN /TEST/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create File INTERNAL.ACCOUNTS.ALL.CSV IN /TEST/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END
*****
RETURN
*------------------------RED FORM TEXT FILE----------------------
PROCESS:
    T.SEL  = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ''"
    T.SEL := " AND POSTING.RESTRICT LT 90"
    T.SEL := " AND CURRENCY NE ''"
    T.SEL := " BY CO.CODE BY CATEGORY BY CURRENCY"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)
            ACCT.ID     = KEY.LIST<I>
            ACCT.TITLE  = R.AC<AC.ACCOUNT.TITLE.1>
            CATEG.ID    = R.AC<AC.CATEGORY>
*Line [ 102 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATEG.ID,CATEG.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>

            ONLINE.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
            BRANCH.NO   = R.AC<AC.CO.CODE>
            IF BRANCH.NO[8,1] EQ '0' THEN
                BRANCH.NO   = BRANCH.NO[9,1]
            END ELSE
                BRANCH.NO   = BRANCH.NO[8,2]
            END

*Line [ 118 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.NO,BRANCH.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.NO,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            YYBRN   = FIELD(BRANCH.NAME,'.',2)

            GOSUB WRITE.LINE
        NEXT I
    END
RETURN
*******************************************************
WRITE.LINE:
    XX.LINE = ""
*    ONLINE.BAL = FMT(ONLINE.BAL, "L2,")
    BRANCH.NAME = FIELD(BRANCH.NAME,'.',2)

*    XX.LINE<1,1>[1,15]   = ACCT.ID
*    XX.LINE<1,1>[20,30]  = ACCT.TITLE
*    XX.LINE<1,1>[55,10]  = CATEG.ID
*    XX.LINE<1,1>[65,20]  = CATEG.NAME
*    XX.LINE<1,1>[105,20] = ONLINE.BAL
*    XX.LINE<1,1>[130,15] = BRANCH.NAME
*    PRINT XX.LINE<1,1>
*    PRINT STR('-',130)

    XX.LINE  = ACCT.ID:","
    XX.LINE := ACCT.TITLE:","
    XX.LINE := CATEG.ID:","
    XX.LINE := CATEG.NAME:","
    XX.LINE := ONLINE.BAL:","
    XX.LINE := BRANCH.NAME:","

    WRITESEQ XX.LINE TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
*Line [ 159 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN   = FIELD(BRANCH,'.',2)
    DATY    = TODAY
    T.DAY   = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

*    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
*    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
*    PR.HD :="'L'":SPACE(90):"SBD.INT.AC.ALL"
*    PR.HD :="'L'"
*    PR.HD :="'L'":"ACCOUNT.NO"
*    PR.HD :=SPACE(10):"ACCOUNT.TITLE"
*    PR.HD :=SPACE(18):"CATEGORY"
*    PR.HD :=SPACE(13):"CATEGORY.NAME"
*    PR.HD :=SPACE(15):"ONLINE.BALANCE"
*    PR.HD :=SPACE(10):"BRANCH.NAME"
*    PR.HD :="'L'":STR('_',130)
*    PRINT
*    HEADING PR.HD

    XX.HD  = ""
    XX.HD  = "ACCOUNT.NO":","
    XX.HD := "ACCOUNT.TITLE":","
    XX.HD := "CATEGORY":","
    XX.HD := "CATEGORY.NAME":","
    XX.HD := "ONLINE.BALANCE":","
    XX.HD := "BRANCH.NAME":","

    WRITESEQ XX.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
END
