* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE   SBD.STMT.CHARGE.NO
***    PROGRAM  SBD.STMT.CHARGE.NO

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.CHARGE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------
    GOSUB PROCESS
*-------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='P.FUNCTION'
*    REPORT.ID='SBD.STMT.CHARGE.NO'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    PROGRAM.NAME = 'SBD.STMT.CHARGE.NO'

****************************************************

    DIM AR.DATE(50)
    DIM AR.STMT(50)
    DIM AR.POST(50)




*----------------------------------
    FN.COMP = "FBNK.COMPANY"
    F.COMP  = ""
    R.COMP  = ""
    Y.COMP.ID = ""

*----------------------------------
    FN.CHARGE = "F.SCB.STMT.CHARGE"
    F.CHARGE  = ""
    R.CHARGE = ""
    Y.CHARGE.ID = ""

*----------------------------------

*----------------------------------
    FN.DATE = "F.DATES"
    F.DATE  = ""
    R.DATE = ""
    Y.DATE.ID = ""

    CALL OPF (FN.DATE,F.DATE)
*----------------------------------

    SYS.DATE = TODAY

    P.DATE   = FMT(SYS.DATE,"####/##/##")
    P.WS.DATE = FMT(SYS.DATE,"####/##/##")


    TXT1 = ""
    TXT2 = ""
    TXT3 = ""
    TXT4 = ""

    HEAD.A1 = "������� ���� ������� ���� �� ��� ����"
    HEAD.B1 = "������ ��� ������ "
    HEAD.B2 = "�� ������ ��� ���������"




    RETURN
*========================================================================
PROCESS:

    L.AR.STMT = 0
    L.AR.POST = 0

    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010077' AND @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
*    T.SEL3 = "SELECT F.COMPANY WITH @ID EQ 'EG0010080'"

    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SEL.COMP,ER.MSG3)



    FOR I = 1 TO SEL.COMP

        COMP = KEY.LIST3<I>


*********************************************************************************************************

        GOSUB MOVE.ZEROS.TO.ARY
        GOSUB READ.STMT.CHARGE.FILE


*Line [ 123 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        YYBRN  = BRANCH

*********************************************************************************************************

        GOSUB SORT.ARY

*********************************************************************************************************
        T.AR.STMT = 0
        T.AR.POST = 0

*  --------------
        XX = SPACE(120)
        XX<1,1>[1,25] = YYBRN
        PRINT XX<1,1>
*  --------------

        FOR Y = 1 TO 50
            IF AR.DATE(Y) NE 0 THEN

                XX = SPACE(120)

                XX<1,1>[30,10]   = FMT(AR.DATE(Y),"####/##/##")
                XX<1,1>[55,10]   = AR.POST(Y)
                XX<1,1>[70,10]   = AR.STMT(Y)

                PRINT XX<1,1>

                T.AR.STMT += AR.STMT(Y)
                T.AR.POST += AR.POST(Y)

            END

        NEXT Y

        XX = SPACE(120)
        PRINT XX<1,1>


        XX<1,1>[1,20]    = "������ ��������"
        XX<1,1>[55,10]   = T.AR.POST
        XX<1,1>[70,10]   = T.AR.STMT
        PRINT XX<1,1>

        XX = STR('_',130)

        PRINT XX<1,1>


        L.AR.STMT   += T.AR.STMT
        L.AR.POST   += T.AR.POST


*------------------------------------------------

    NEXT I


*********************************************************************************************************

    XX = SPACE(130)
    XX<1,1>[1,20]    = "������ ���������"
    XX<1,1>[55,10]   = L.AR.POST
    XX<1,1>[70,10]   = L.AR.STMT
    PRINT XX<1,1>
    XX = STR('_',130)

    PRINT XX<1,1>
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)


*-------------------------------------------------

    RETURN
*========================================================================================================
*********************************************************************************************************
SORT.ARY:

REPEAT.SORT.ARY:

    SORT.FLAG = 0


    FOR L = 1 TO 50 - 1

        IF AR.DATE(L) GT  AR.DATE(L+1) THEN
            WS.DATE = AR.DATE(L)
            WS.STMT = AR.STMT(L)
            WS.POST = AR.POST(L)

            AR.DATE(L) = AR.DATE(L+1)
            AR.STMT(L) = AR.STMT(L+1)
            AR.POST(L) = AR.POST(L+1)

            AR.DATE(L+1) = WS.DATE
            AR.STMT(L+1) = WS.STMT
            AR.POST(L+1) = WS.POST

            SORT.FLAG = 1
        END

    NEXT  L

    IF SORT.FLAG EQ 1 THEN
        GOTO  REPEAT.SORT.ARY
    END

    RETURN
*********************************************************************************************************
READ.STMT.CHARGE.FILE:

    SEL.CHARGE  = "SELECT ":FN.CHARGE:" WITH CO.CODE EQ ":COMP

    CALL EB.READLIST(SEL.CHARGE,SEL.LIST.CHARGE,'',NO.OF.CHARGE,ERR.CHARGE)


    LOOP
        REMOVE Y.CHARGE.ID FROM SEL.LIST.CHARGE SETTING POS.CHARGE
    WHILE Y.CHARGE.ID:POS.CHARGE
        CALL F.READ(FN.CHARGE,Y.CHARGE.ID,R.CHARGE,F.CHARGE,ERR.CHARGE)

****************
        WS.DATE     = FIELD(Y.CHARGE.ID,'.',2,1)
        WS.STMT     = R.CHARGE<STM.STMT.FLG>
        WS.POST     = R.CHARGE<STM.POST.FLG>



        GOSUB WRITE.TO.ARY



    REPEAT

    RETURN
*********************************************************************************************************
WRITE.TO.ARY:


    STOP.FLAG = 0
    FOR J = 1 TO 50

        IF STOP.FLAG = 1 THEN
            RETURN
        END

        IF AR.DATE(J) EQ 0 THEN
            AR.DATE(J) = WS.DATE
        END



        IF AR.DATE(J) EQ WS.DATE   THEN
            STOP.FLAG = 1

            IF WS.STMT EQ 'YES' THEN
                AR.STMT(J) += 1
            END

            IF WS.POST EQ 'YES' THEN
                AR.POST(J) += 1
            END

        END

    NEXT  J

    RETURN
*********************************************************************************************************
MOVE.ZEROS.TO.ARY:


    FOR K = 1 TO 50

        AR.DATE(K) = 0
        AR.STMT(K) = 0
        AR.POST(K) = 0

    NEXT K


    RETURN
*********************************************************************************************************
PRINT.HEAD:
*---------

*    XX  = SPACE(120)


*   XX<1,1>[1,35]   =   YYBRN
*  XX<1,1>[40,6]   =   AR.STMT
* XX<1,1>[60,6]   =   AR.POST
*PRINT XX<1,1>

*Line [ 323 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,"99",BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,"99",R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    H.BRANCH  = BRANCH


    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(85):"��� :" :H.BRANCH
    PR.HD :="'L'":SPACE(1):" ������� : ":P.DATE:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):PROGRAM.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):HEAD.A1
    PR.HD :="'L'":SPACE(47):HEAD.B1:SPACE(2):HEAD.B2
    PR.HD :="'L'":SPACE(60):"�� ����� : ":P.WS.DATE
    PR.HD :="'L'":SPACE(50):STR('_',45)
    PR.HD :="'L'":" "
*    PR.HD :="'L'":STR('_',130)


*    PR.HD :="'L'":SPACE(13):"|":SPACE(5):"�������� �������":SPACE(7):"|":SPACE(6):"������ �������":SPACE (8):"|":SPACE(5):"������ ��������":SPACE(8):"|":SPACE(5):"������� ����":SPACE(11):"|":"������"


*    PR.HD :="'L'":"��� �����":SPACE(4):STR('_',28):STR('_',28):STR('_',28):STR('_',28)
     PR.HD :="'L'":SPACE(50):"��� �����":SPACE(5):"��� �����"

*    PR.HD :="'L'":"��� ����� ":SPACE(30):"����� ���������":SPACE(4):"�.��� �������":SPACE(5):"�.��� ����"
    PR.HD :="'L'":"��� ����� ":SPACE(20):"����� ���������":SPACE(4):"�.��� ����":SPACES(5):"�.��� �������"


    PR.HD :="'L'":STR('_',130)

    HEADING PR.HD

    RETURN
*==============================================================
END
