* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.TLR.IN.OUT1000.R1(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-----------------------------------
    SYS.DATE   = TODAY
    LAST.WEEK.DATE   = SYS.DATE
    CALL CDT('', LAST.WEEK.DATE, "-7C")
*------------------------
    FN.TLR = "FBNK.TELLER$HIS"
    F.TLR = ''
    R.TLR=''
    Y.TLR=''
    Y.TLR.ERR=''

    CALL OPF(FN.TLR,F.TLR)
    SEL.CMD ="SELECT " :FN.TLR:" WITH AUTH.DATE GE ":LAST.WEEK.DATE:" AND CUSTOMER.1 LIKE 13... AND (DR.CR.MARKER EQ 'DEBIT' OR EQ 'CREDIT') AND CO.CODE EQ ":COMP:" BY CUSTOMER.1 BY DR.CR.MARKER "
    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)

    IF NOREC >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
    RETURN

END

*------------------------
*    ENQ.DATA<2,1> = "DR.CR.MARKER"
*    ENQ.DATA<3,1> = "EQ"
*    ENQ.DATA<4,1> = "CREDIT"
*------------------------
*ENQ.DATA<2,1> = "AUTH.DATE"
*ENQ.DATA<3,1> = "GE"
*ENQ.DATA<4,1> = LAST.WEEK.DATE
*------------------------
*------------------------
*------------------------
*------------------------
*-----------------------------------
*RETURN
*END
