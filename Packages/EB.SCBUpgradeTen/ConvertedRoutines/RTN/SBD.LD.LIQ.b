* @ValidationCode : MjoxNzgxODg4NDc0OkNwMTI1MjoxNjQwODI2OTI0OTQ0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:15:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>283</Rating>
*-----------------------------------------------------------------------------
*** ���� ��������� ������� ***
*** CREATED BY REHAM ***
***=================================

*    SUBROUTINE SBD.LD.LIQ
PROGRAM SBD.LD.LIQ

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 52 ]  HASHING $INCLUDE I_F.SCB.LD.RENEW.METHOD - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.LD.RENEW.METHOD
*------------------------------------------------------------------------
    GOSUB INITIATE
*    GOSUB PRINT.HEAD
*    GOSUB PROCESS.H
    GOSUB PROCESS.L
*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
*    REPORT.ID='SBD.LD.LIQ'
*    CALL PRINTER.ON(REPORT.ID,'')

*    YTEXT = "Enter Date from : "
*    CALL TXTINP(YTEXT, 8, 22, "10", "A")
*    DATF = COMI
*    YTEXT = "Enter Date to : "
*    CALL TXTINP(YTEXT, 8, 22, "10", "A")


    OPENSEQ "&SAVEDLISTS&" , "SBD.LD.LIQ.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBD.LD.LIQ.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBD.LD.LIQ.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBD.LD.LIQ.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBD.LD.LIQ.CSV File IN &SAVEDLISTS&'
        END
    END

    HEAD.DESC  = "��� �������":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "����� ������":","
    HEAD.DESC := " ����� ��������� ������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "����� ���� ":","
    HEAD.DESC := "������ ������":","
    HEAD.DESC := "�������"
    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*    TD = TODAY
*    CALL CDT("",TD,-1)

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD.HP = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HP = ''
    CALL OPF(FN.LD.HP,F.LD.HP)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.TD = 'F.DATES' ; F.TD = ''
    CALL OPF(FN.TD,F.TD)

    FN.BASIC.INT = 'F.BASIC.INTEREST' ; F.BASIC.INT = ''
    CALL OPF(FN.BASIC.INT,F.BASIC.INT)


    COMP = 'EG0010001'
    CALL F.READ(FN.TD,COMP,R.TD,F.TD,ER.TD)
    LAST.WK = R.TD<EB.DAT.LAST.WORKING.DAY>

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    DAT.H  = ''     ; V.DATE.H   = '' ; MAT.DATE.H = '' ; DESC = '' ; DESC.H = ''
    AMT.H  = ''     ; CATEG.H = ''    ; RATE.H = ''     ; RATE.AMT.H = '' ; KEYLIST2 = '' ; AMT = '' ; CUR = ''
    CUR.H  = ''     ; DAT1.H = ''     ; CATEG.ID.H = '' ; DESC.ID.H = ''
    COMP = ID.COMPANY
RETURN
*========================================================================
PROCESS.L:
*DEBUG

*    T.SEL = "SELECT ":FN.LD: " WITH ( CATEGORY GE 21019 AND CATEGORY LE 21042 ) AND FIN.MAT.DATE EQ ":LAST.WK:" AND INPUTTER UNLIKE ...INPUTTCOB... AND INTEREST.RATE EQ '0'"
    T.SEL = "SELECT ":FN.LD: " WITH ( CATEGORY GE 21017 AND CATEGORY LE 21042 ) AND FIN.MAT.DATE EQ ":LAST.WK:" AND INPUTTER UNLIKE ...INPUTTCOB..."
    T.SEL := " BY CO.CODE BY FIN.MAT.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ.HISTORY(FN.LD.H,KEY.LIST<Z>,R.LD.HIS,F.LD.H,ER.LD.H)
            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,ER.LD)
            IF R.LD.HIS<LD.FIN.MAT.DATE> GT R.LD<LD.FIN.MAT.DATE> THEN
                DAT             = R.LD.HIS<LD.VALUE.DATE>
                V.DATE          = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
                DAT1            = R.LD<LD.FIN.MAT.DATE>
                CHR.AMT         = R.LD<LD.CHRG.AMOUNT>
                CATEG           = R.LD<LD.CATEGORY>
                MAT.DATE        = DAT1[1,4]:'/':DAT1[5,2]:'/':DAT1[7,2]

                AMT             = R.LD.HIS<LD.AMOUNT>
                AMT1            = FIELD(AMT,'.',1)
                CO.CODE         = R.LD.HIS<LD.CO.CODE>
*Line [ 160 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH1)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH1=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
                NAME.BRANCH     = BRANCH1
                NAME            = R.LD.HIS<LD.LOCAL.REF><1,LDLR.IN.RESPECT.OF>
                T.SEL3 = '' ; KEY.LIST3 = '' ; SELECTED3 = '' ; ER.MSG3 = ''
                IF NOT(R.LD.HIS<LD.INTEREST.RATE>) AND R.LD.HIS<LD.INTEREST.KEY> THEN
                    T.SEL3 = "SELECT FBNK.BASIC.INTEREST WITH @ID LIKE ":R.LD.HIS<LD.INTEREST.KEY>:R.LD.HIS<LD.CURRENCY>:"...":" BY @ID"
                    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
                    IF SELECTED3 THEN
                        FOR EE3 = 1 TO SELECTED3
                            OLD.DAT = KEY.LIST3<EE3-1>[8]
                            CUR.DAT = KEY.LIST3<EE3>[8]
                            LD.DATE = R.LD<LD.FIN.MAT.DATE>
                            IF ( R.LD<LD.FIN.MAT.DATE> GE OLD.DAT AND R.LD<LD.FIN.MAT.DATE> LE CUR.DAT ) THEN
                                CALL F.READ(FN.BASIC.INT,KEY.LIST3<EE3-1>,R.BASIC.INT3,F.BASIC.INT,E2)
                                RATE = R.BASIC.INT3<EB.BIN.INTEREST.RATE>
                                EE3 = SELECTED3
                            END
                        NEXT EE3
                    END
                END ELSE
                    RATE            = R.LD.HIS<LD.INTEREST.RATE>
                END
                RATE.AMT        = R.LD.HIS<LD.TOT.INTEREST.AMT>
                RATE.AMT1       = FIELD(RATE.AMT,'.',1)
                CUR             = R.LD.HIS<LD.CURRENCY>

                DAT.F          = R.LD.HIS<LD.FIN.MAT.DATE>
*      MAT.DATE.F     = DAT.F[7,2]:'/':DAT.F[5,2]:'/':DAT.F[1,4]
                MAT.DATE.F     = DAT.F[1,4]:'/':DAT.F[5,2]:'/':DAT.F[7,2]
                CR.AMT          = R.LD<LD.REIMBURSE.PL.AMT>
                BB.DATA  = KEY.LIST<Z>:','
                BB.DATA := NAME.BRANCH:','
                BB.DATA := MAT.DATE:','
                BB.DATA := MAT.DATE.F:','
                BB.DATA := RATE:','
                BB.DATA := AMT:','
                BB.DATA := RATE.AMT:','
                BB.DATA := CUR:','
                BB.DATA := NAME:','
                BB.DATA := CHR.AMT:','
                BB.DATA := CR.AMT:','
                BB.DATA := CATEG
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END

        NEXT Z

    END
RETURN
**********************************************************************************
END
