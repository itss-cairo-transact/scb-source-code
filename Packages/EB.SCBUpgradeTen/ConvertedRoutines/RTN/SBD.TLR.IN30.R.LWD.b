* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-50</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.TLR.IN30.R.LWD(ENQ.DATA)
**    PROGRAM SBD.TLR.IN30.R.LWD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

*-----------------------------------

    WS.COMP = ID.COMPANY
*-----------------------------------

    X.ID = ' 0 '
    X.NO = 0

*------------------------
**    WORK.DATE = 20130618
**    WS.COMP = "EG0010004"
**    ENQ.DATA = 0
*=======================================
*    SYS.DATE   = TODAY
*    WORK.DATE   = SYS.DATE
*    CALL CDT('', WORK.DATE, "-1W")
*------------------------
    YTEXT = "Enter Date. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    EXC.DATE = COMI
    WORK.DATE = EXC.DATE
*------------------------
*   WS.MARKER = 'DEBIT'
*------------------------
    WS.MARKER = 'CREDIT'
*------------------------
    WS.SECTOR = '4650'
*------------------------
    WS.AMOUNT = 30000
*=======================================
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST = ''
    R.CUST = ''
    Y.CUST.ID = ''
    Y.CUST.ERR = ''
    CALL OPF(FN.CUST,F.CUST)

*-------------
    FN.TLLR = "FBNK.TELLER$HIS"
    F.TLLR = ''
    R.TLLR = ''
    Y.TLLR.ID = ''
    Y.TLLR.ERR = ''
    CALL OPF(FN.TLLR,F.TLLR)

*-------------
    SEL.CMD   = "SELECT ":FN.TLLR:" BY CUSTOMER.1 WITH"
    SEL.CMD  := " AUTH.DATE EQ ":WORK.DATE
*    SEL.CMD  := " AND AMOUNT.LOCAL.1 GE ":WS.AMOUNT
    SEL.CMD  := " AND DR.CR.MARKER EQ ":WS.MARKER
    SEL.CMD  := " AND CO.CODE EQ ":WS.COMP
    SEL.CMD  := " AND CUSTOMER.1 NE ''"
    SEL.CMD  := " AND RECORD.STATUS EQ 'MAT'"


    CALL EB.READLIST(SEL.CMD,SEL.LIST,'',NOREC,RTNCD)


    LP = 0
    IF NOREC >= 1 THEN

        FOR XX = 1 TO NOREC

*----------
*        LOOP
*            REMOVE Y.TLLR.ID FROM SEL.LIST SETTING POS.TLLR
*        WHILE Y.TLLR.ID:POS.TLLR
*            CALL F.READ(FN.TLLR,Y.TLLR.ID,R.TLLR,F.TLLR,ERR.TLLR)
*----------

            CALL F.READ(FN.TLLR,SEL.LIST<XX>,R.TLLR,F.TLLR,ERR.TLLR)

            Y.CUST.ID = R.TLLR<TT.TE.CUSTOMER.1>
            Y.AMNT    = R.TLLR<TT.TE.AMOUNT.LOCAL.1>

*Line [ 114 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            N.SECTOR  = LOCAL.REF<1,CULR.NEW.SECTOR>

            IF N.SECTOR EQ WS.SECTOR THEN
                X.NO = X.NO + 1
                X.ID = X.ID:SEL.LIST<XX>:' '
            END



**        REPEAT

*----------
*                IF N.SECTOR NE WS.SECTOR THEN
*                    PRINT '**':XX:'-':Y.CUST.ID:'-':N.SECTOR:'-':Y.AMNT
*                END
*----------

        NEXT XX

    END

    SEL.ONE   = "SELECT ":FN.TLLR:" WITH @ID IN (":X.ID:")"

    CALL EB.READLIST(SEL.ONE,SEL.LIST.ONE,'',XXREC,RTNXX)

    IF XXREC > 0  THEN
        FOR DD = 1 TO XXREC
            ENQ.DATA<2,DD> = '@ID'
            ENQ.DATA<3,DD> = 'EQ'
            ENQ.DATA<4,DD> = SEL.LIST.ONE<DD>
        NEXT DD
    END ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUMMY'

    END


    RETURN
