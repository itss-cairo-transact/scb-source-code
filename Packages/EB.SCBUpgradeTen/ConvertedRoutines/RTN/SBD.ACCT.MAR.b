* @ValidationCode : MjotMTk3ODA0ODYwMjpDcDEyNTI6MTY0NTEyNzM3NDg1NTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 11:49:34
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
SUBROUTINE SBD.ACCT.MAR
*    PROGRAM SBD.ACCT.MAR

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.POSTING.RESTRICT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TRANSACTION
*Line [ 41 ] HASHING "$INCLUDE I_AC.LOCAL.REFS"  - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 43 ] HASHING "$INCLUDE I_F.SCB.NEW.SECTOR" - ITSS - R21 Upgrade - 2021-12-23
*  $INCLUDE I_F.SCB.NEW.SECTOR
*Line [ 45 ] HASHING "$INCLUDE I_F.SCB.RISK.RATING" - ITSS - R21 Upgrade - 2021-12-23
*  $INCLUDE I_F.SCB.RISK.RATING
*---------------------------------------
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
RETURN
*---------------------------------------
INITIAL:
*--------


    OPENSEQ "&SAVEDLISTS&" , "ACCOUNT.MAR.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"ACCOUNT.MAR.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "ACCOUNT.MAR.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ACCOUNT.MAR CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create ACCOUNT.MAR File IN &SAVEDLISTS&'
        END
    END
    CALL TXTINP('Enter Starting Date', 22, 23, '8', 'ANY')

    FROM.DATE = COMI

    CALL TXTINP('Enter Ending Date', 22, 23, '8', 'ANY')

    END.DATE = COMI
    COMI = ''
    CALL TXTINP('Enter Account Number ', 22, 23, '16', 'ANY')

    ACC.ID.NO = COMI

    FN.ACC = "FBNK.ACCOUNT"  ; F.ACC = ""
    CALL OPF(FN.ACC, F.ACC)

    CALL EB.ACCT.ENTRY.LIST(ACC.ID.NO,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    T.SEL    = "" ; SEL.LIST = "" ; NO.OF.REC = "" ; ERR.SEL = ""
    ACC.ID = "" ; R.ACC = ""  ; ERR.ACC = ""


    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)

    FN.COM = 'F.COMPANY'        ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.TR = 'FBNK.TRANSACTION'    ; F.TR = ''
    CALL OPF(FN.TR,F.TR)

    ACC.NAME = ""
*Line [ 100 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,ACC.ID.NO[1,16],ACC.NAME)
    F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
    FN.F.ITSS.ACCOUNT = ''
    CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
    CALL F.READ(F.ITSS.ACCOUNT,ACC.NAME,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
    ACC.NAME=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1,ACC.ID.NO[1,16]>

    FN.CUR = "FBNK.CURRENCY" ; F.CUR = ""
    CALL OPF(FN.CUR,F.CUR)
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,STE.ID,CURR1)
*    CURR = FIELD(CURR1,".",2)

RETURN

*---------------------------------------
PRINT.HEAD:
*-----------
    BB.DATA      = ""
    HEAD.DESC   = "��� ������":",":ACC.ID.NO[1,16]
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA      = ""
    HEAD.DESC    = "��� ������":",":ACC.NAME
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END



    BB.DATA      = ""
    HEAD.DESC   = "������ ��������� ":",":OPENING.BAL
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END



    HEAD.DESC    = "���� ����":","
    HEAD.DESC   := "���� ����":","
    HEAD.DESC   := "�������":","
    HEAD.DESC   := "DESCRIPTION":","
    HEAD.DESC   := "��� �������":","
    HEAD.DESC   := "�������":","
    HEAD.DESC   := "������ ":","
    HEAD.DESC   := "�����":","
    HEAD.DESC   := "�����"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
*---------------------------------------
PROCESS:
*-------
*DEBUG
    T.SEL = "SELECT FBNK.ACCOUNT WITH @ID EQ ":ACC.ID.NO[1,16]
    CALL EB.READLIST(T.SEL,SEL.LIST,'',SELECTED,ERR.SEL)
* CRT SELECTED
    LOOP
        REMOVE ACC.ID FROM SEL.LIST SETTING POS.AC
    WHILE ACC.ID:POS.AC
        CALL F.READ(FN.ACC,ACC.ID,R.ACC,F.ACC,ERR.ACC)
        AC.CUR         = R.ACC<AC.CURRENCY>
        AC.CAT         = R.ACC<AC.CATEGORY>
        AC.CAT1 = ""
*Line [ 170 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION,AC.CAT,AC.CAT1)
        F.ITSS.CATEGORY = 'F.CATEGORY'
        FN.F.ITSS.CATEGORY = ''
        CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
        CALL F.READ(F.ITSS.CATEGORY,AC.CAT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
        AC.CAT1=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
        AC.NAME        = R.ACC<AC.ACCOUNT.TITLE.1>
        AC.CODE         = R.ACC<AC.CO.CODE>
        CALL F.READ(FN.COM,AC.CODE,R.COM,F.COM,E3)
        AC.BRNAME   = R.COM<EB.COM.COMPANY.NAME,2>

        CALL EB.ACCT.ENTRY.LIST(ACC.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
        LOOP
            REMOVE STE.ID FROM ID.LIST SETTING POS.STE
        WHILE STE.ID:POS.STE
            IF STE.ID THEN
                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.CUR = R.STE<AC.STE.CURRENCY>
                CURR = ""
*Line [ 190 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME,STE.CUR,CURR)
                F.ITSS.CURRENCY = 'F.CURRENCY'
                FN.F.ITSS.CURRENCY = ''
                CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
                CALL F.READ(F.ITSS.CURRENCY,STE.CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
                CURR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>

                IF STE.CUR EQ LCCY THEN
                    STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
                END ELSE
                    STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
                END
                CREDIT.AMT = ""
                DEBIT.AMT = ""
                IF STE.AMT > 0 THEN
                    CREDIT.AMT = STE.AMT
                END

                IF STE.AMT < 0 THEN
                    DEBIT.AMT = STE.AMT
                END

                STE.REF   = R.STE<AC.STE.TRANS.REFERENCE>
                THEIR.REF = R.STE<AC.STE.THEIR.REFERENCE>
*************************************************************
                TRNS.CODE = R.STE<AC.STE.TRANSACTION.CODE>
                TR.ID = TRNS.CODE
*                CALL DBR ('TRANSACTION':@FM:AC.TRA.NARRATIVE,TRNS.CODE,TRNS.DESC)
                CALL F.READ(FN.TR,TR.ID,R.TR,F.TR,ER.TR)
                TRNS.DESC = R.TR<AC.TRA.NARRATIVE,2>

*************************************************************
                STE.VAL   = R.STE<AC.STE.BOOKING.DATE>
                IF STE.VAL THEN
                    STE.VAL = FMT(STE.VAL,"####/##/##")
                END
                STE.ACC = R.STE<AC.STE.ACCOUNT.NUMBER>
                GOSUB DATA.DISPLAY
            END
        REPEAT
    REPEAT
RETURN
**********************************************
DATA.DISPLAY:
    BB.DATA    = ""

    BB.DATA    = DEBIT.AMT:","
    BB.DATA   := CREDIT.AMT:","
    BB.DATA   := STE.REF:","
    BB.DATA   := THEIR.REF:","
    BB.DATA   := TRNS.DESC:","
    BB.DATA   := STE.VAL:","
    BB.DATA   := CURR:","
    BB.DATA   := AC.CAT1:","
    BB.DATA   := AC.BRNAME

    PRINT BB.DATA
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
*---------------------------------------
END
