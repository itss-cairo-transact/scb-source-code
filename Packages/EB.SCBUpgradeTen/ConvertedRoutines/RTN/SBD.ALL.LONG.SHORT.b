* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
**  MTAG
    SUBROUTINE SBD.ALL.LONG.SHORT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.SYSTEM.SUMMARY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*---------------------------------------------------
*DEBUG
    FN.SUM = "FBNK.EB.SYSTEM.SUMMARY"
    F.SUM  = ""

    FN.CUR = "FBNK.CURRENCY"
    F.CUR  = ""

    FN.DATE = "F.DATES"
    F.DATE  = ""

    FN.CURRH = "FBNK.CURRENCY$HIS"
    F.CURRH  = ''
*-----------------------------------------------------
    R.FIN = ""
    R.DATE = ""
    MSG.DATE = ""
    WS.LINE.NO = 0
    WS.LINE.NO.COMP = 0

    WS.CCY.EUR    =  0
    WS.LCL.EUR    =  0
    WS.LCL.EUR.P  =  0
    WS.CCY.USD    =  0
    WS.LCL.USD    =  0
    WS.LCL.USD.P  =  0
    WS.CCY.SAR    =  0
    WS.LCL.SAR    =  0
    WS.LCL.SAR.P  =  0
    WS.CCY.GBP    =  0
    WS.LCL.GBP    =  0
    WS.LCL.GBP.P  =  0
    WS.CCY.AED    =  0
    WS.LCL.AED    =  0
    WS.LCL.AED.P  =  0
    WS.CCY.AUD    =  0
    WS.LCL.AUD    =  0
    WS.LCL.AUD.P  =  0
    WS.CCY.CAD    =  0
    WS.LCL.CAD    =  0
    WS.LCL.CAD.P  =  0
    WS.CCY.CHF    =  0
    WS.LCL.CHF    =  0
    WS.LCL.CHF.P  =  0
    WS.CCY.DKK    =  0
    WS.LCL.DKK    =  0
    WS.LCL.DKK.P  =  0
    WS.CCY.JPY    =  0
    WS.LCL.JPY    =  0
    WS.LCL.JPY.P  =  0
    WS.CCY.KWD    =  0
    WS.LCL.KWD    =  0
    WS.LCL.KWD.P  =  0
    WS.CCY.NOK    =  0
    WS.LCL.NOK    =  0
    WS.LCL.NOK.P  =  0
    WS.CCY.SEK    =  0
    WS.LCL.SEK    =  0
    WS.LCL.SEK.P  =  0

    WS.TOTAL.LCL  =  0
    WS.TOTAL.L.S  =  0
    WS.HEAD.01.A = "CURRENT POSITION  IN : "

    WS.HEAD.02.A = "CURRENCY"
    WS.HEAD.02.B = "RATE"
    WS.HEAD.02.C = "LO. / SH."
    WS.HEAD.02.D = "CCY. AMOUNT "
    WS.HEAD.02.E = "*--- LOCAL CCY. EQUIVALENT ---*"
    WS.HEAD.02.F = "*----- PROFIT OR LOSS -----*"

    WS.HEAD.03.E = "LAST REV.       TODAY REV.     "
    WS.HEAD.03.F = "  TODAYS     TODAYS TOTALS "
    WS.PAGE.COUNT = 0
    WS.TOTAL = " TOTAL FOR CURRENCY MARKET : "



    WS.REP.HEADER = "SBD.ALL.LONG.SHORT"
*----------------------------PROCEDURE---------------------
    CALL OPF (FN.SUM,F.SUM)
    CALL OPF (FN.CUR,F.CUR)
    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.CURRH , F.CURRH)
*---------------------------------------------------------
**  REPORT.ID='SBD.ALL.LONG.SHORT'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
*------------------------PROCEDURE------------------------

    GOSUB GET.DATE
    GOSUB PRT.HEAD
    GOSUB GET.DATA.1
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*-----------------------------------------------
GET.DATE:
*--------
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.TODAY  = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.DAT.HD = R.DATE<EB.DAT.LAST.WORKING.DAY>
*    WS.DAT.MI = R.DATE<EB.DAT.BACK.VALUE.MINIMUM>
    WS.CUR    = WS.DAT.HD[3,6]
    WS.CUR1   = WS.DAT.HD[3,6]:"0100"
*    CRT WS.TODAY
    RETURN
*---------------------------------------------
GET.DATA.1:
*-----------
**    DEBUG
    SEL.CMD = "SELECT ":FN.SUM:" WITH @ID LIKE ...":WS.DAT.HD:"... BY @ID"
**    SEL.CMD = "SELECT ":FN.SUM:" WITH @ID LIKE ...EG0010099-":WS.DAT.HD:"... BY @ID"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.SUM.ID FROM SEL.LIST SETTING POS
    WHILE WS.SUM.ID:POS
        CALL F.READ(FN.SUM,WS.SUM.ID,R.SUM,F.SUM,MSG.LINE)
*Line [ 155 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        XX = DCOUNT(R.SUM<EB.SYSUM.AL.CCY>,@VM)
        CURR.B   =  R.SUM<EB.SYSUM.AL.CCY>
        WS.CCY.1 =  R.SUM<EB.SYSUM.AL.CCY.POSN>
        WS.LCL.1 =  R.SUM<EB.SYSUM.AL.LCL.POS>
        FOR POS1 = 1 TO XX
            CURR.BASE = CURR.B<1,POS1>
            BEGIN CASE
            CASE CURR.BASE = 'USD'
                WS.CCY.USD = WS.CCY.USD + WS.CCY.1<1,POS1>
                WS.LCL.USD = WS.LCL.USD + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'EUR'
                WS.CCY.EUR = WS.CCY.EUR + WS.CCY.1<1,POS1>
                WS.LCL.EUR = WS.LCL.EUR + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'SAR'
                WS.CCY.SAR = WS.CCY.SAR + WS.CCY.1<1,POS1>
                WS.LCL.SAR = WS.LCL.SAR + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'GBP'
                WS.CCY.GBP = WS.CCY.GBP + WS.CCY.1<1,POS1>
                WS.LCL.GBP = WS.LCL.GBP + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'AED'
                WS.CCY.AED = WS.CCY.AED + WS.CCY.1<1,POS1>
                WS.LCL.AED = WS.LCL.AED + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'AUD'
                WS.CCY.AUD = WS.CCY.AUD + WS.CCY.1<1,POS1>
                WS.LCL.AUD = WS.LCL.AUD + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'CAD'
                WS.CCY.CAD = WS.CCY.CAD + WS.CCY.1<1,POS1>
                WS.LCL.CAD = WS.LCL.CAD + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'CHF'
                WS.CCY.CHF = WS.CCY.CHF + WS.CCY.1<1,POS1>
                WS.LCL.CHF = WS.LCL.CHF + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'DKK'
                WS.CCY.DKK = WS.CCY.DKK + WS.CCY.1<1,POS1>
                WS.LCL.DKK = WS.LCL.DKK + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'JPY'
                WS.CCY.JPY = WS.CCY.JPY + WS.CCY.1<1,POS1>
                WS.LCL.JPY = WS.LCL.JPY + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'KWD'
                WS.CCY.KWD = WS.CCY.KWD + WS.CCY.1<1,POS1>
                WS.LCL.KWD = WS.LCL.KWD + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'NOK'
                WS.CCY.NOK = WS.CCY.NOK + WS.CCY.1<1,POS1>
                WS.LCL.NOK = WS.LCL.NOK + WS.LCL.1<1,POS1>
            CASE CURR.BASE = 'SEK'
                WS.CCY.SEK = WS.CCY.SEK + WS.CCY.1<1,POS1>
                WS.LCL.SEK = WS.LCL.SEK + WS.LCL.1<1,POS1>
            CASE OTHERWISE
                RATE = 1
            END CASE
        NEXT POS1
    REPEAT
    GOSUB PRINT.DATA
    RETURN
*----------------------------------------------------------------
PRINT.DATA:
*----------
    XX = SPACE(132)
**    DEBUG
    PRINT XX<1,1>
*---- "AED"
    CURR.BASE.N = 'AED...'
*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"AED",AED.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"AED",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
AED.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    AED.RAT = AED.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = AED.RAT<1,1>
    IF WS.CCY.AED < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.AED.P    =  WS.CCY.AED *  RATE.P
    WS.LCL.AED.P    = DROUND(WS.LCL.AED.P,2)
* MTAG TEST    WS.LCL.AED.DIF  = WS.LCL.AED - WS.LCL.AED.P
    WS.LCL.AED.DIF  = WS.LCL.AED.P - WS.LCL.AED
    IF WS.LCL.AED.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(AED.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.AED, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.AED.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.AED, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.AED.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.AED
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.AED.DIF

    XX = SPACE(132)
    PRINT XX<1,1>

*---- "AUD"
    CURR.BASE.N = 'AUD...'
*Line [ 262 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"AUD",AUD.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"AUD",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
AUD.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    AUD.RAT = AUD.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = AUD.RAT<1,1>
    IF WS.CCY.AUD < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.AUD.P    =  WS.CCY.AUD *  RATE.P
    WS.LCL.AUD.P    = DROUND(WS.LCL.AUD.P,2)
*MTAG TETS    WS.LCL.AUD.DIF  = WS.LCL.AUD - WS.LCL.AUD.P
    WS.LCL.AUD.DIF  = WS.LCL.AUD.P - WS.LCL.AUD
    IF WS.LCL.AUD.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(AUD.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.AUD, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.AUD.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.AUD, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.AUD.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.AUD
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.AUD.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "CAD"
    CURR.BASE.N = 'CAD...'
*Line [ 306 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"CAD",CAD.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"CAD",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CAD.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    CAD.RAT = CAD.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = CAD.RAT<1,1>
    IF WS.CCY.CAD < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.CAD.P    =  WS.CCY.CAD *  RATE.P
    WS.LCL.CAD.P    = DROUND(WS.LCL.CAD.P,2)
*MTAG TEST WS.LCL.CAD.DIF  = WS.LCL.CAD - WS.LCL.CAD.P
    WS.LCL.CAD.DIF  = WS.LCL.CAD.P - WS.LCL.CAD
    IF WS.LCL.CAD.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(CAD.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.CAD, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.CAD.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.CAD, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.CAD.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.CAD
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.CAD.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "CHF"
    CURR.BASE.N = 'CHF...'
*Line [ 350 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"CHF",CHF.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"CHF",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CHF.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    CHF.RAT = CHF.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = CHF.RAT<1,1>
    IF WS.CCY.CHF < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.CHF.P    =  WS.CCY.CHF *  RATE.P
    WS.LCL.CHF.P    = DROUND(WS.LCL.CHF.P,2)
*MTAG TEST    WS.LCL.CHF.DIF  = WS.LCL.CHF - WS.LCL.CHF.P
    WS.LCL.CHF.DIF  = WS.LCL.CHF.P - WS.LCL.CHF
    IF WS.LCL.CHF.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(CHF.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.CHF, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.CHF.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.CHF, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.CHF.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.CHF
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.CHF.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "DKK"
    CURR.BASE.N = 'DKK...'
*Line [ 394 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"DKK",DKK.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"DKK",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
DKK.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    DKK.RAT = DKK.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = DKK.RAT<1,1>
    IF WS.CCY.DKK < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.DKK.P    =  WS.CCY.DKK *  RATE.P
    WS.LCL.DKK.P    = DROUND(WS.LCL.DKK.P,2)
*MTAG TEST    WS.LCL.DKK.DIF  = WS.LCL.DKK - WS.LCL.DKK.P
    WS.LCL.DKK.DIF  = WS.LCL.DKK.P - WS.LCL.DKK
    IF WS.LCL.DKK.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(DKK.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.DKK, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.DKK.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.DKK, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.DKK.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.DKK
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.DKK.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "EUR"
    CURR.BASE.N = 'EUR...'
*Line [ 438 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"EUR",EUR.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"EUR",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
EUR.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    EUR.RAT = EUR.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = EUR.RAT<1,1>
    IF WS.CCY.EUR < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.EUR.P  = WS.CCY.EUR *  RATE.P
    WS.LCL.EUR.P = DROUND(WS.LCL.EUR.P,2)
*MTAG TEST    WS.LCL.EUR.DIF  = WS.LCL.EUR - WS.LCL.EUR.P
    WS.LCL.EUR.DIF  = WS.LCL.EUR.P - WS.LCL.EUR
    IF WS.LCL.EUR.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]     =  CURR.BASE.N
    XX<1,1>[15,5]    =  FMT(EUR.RAT, "R4,")
    XX<1,1>[31,5]    =  WS.TYPE
    XX<1,1>[45,10]   =  FMT(WS.CCY.EUR, "R2,")
    XX<1,1>[70,10]   = FMT(WS.LCL.EUR.P, "R2,")
    XX<1,1>[90,10]   = FMT(WS.LCL.EUR, "R2,")
    XX<1,1>[105,10]  = FMT(WS.LCL.EUR.DIF, "R2,")
    XX<1,1>[120,10]  = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.EUR
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.EUR.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "GBP"
    CURR.BASE.N = 'GBP...'
*Line [ 482 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"GBP",GBP.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"GBP",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
GBP.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    GBP.RAT = GBP.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = GBP.RAT<1,1>
    IF WS.CCY.GBP < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.GBP.P    =  WS.CCY.GBP *  RATE.P
    WS.LCL.GBP.P    = DROUND(WS.LCL.GBP.P,2)
*MTAG TEST    WS.LCL.GBP.DIF  = WS.LCL.GBP - WS.LCL.GBP.P
    WS.LCL.GBP.DIF  = WS.LCL.GBP.P - WS.LCL.GBP
    IF WS.LCL.GBP.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(GBP.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.GBP, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.GBP.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.GBP, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.GBP.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.GBP
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.GBP.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "JPY"
    CURR.BASE.N = 'JPY...'
*Line [ 526 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"JPY",JPY.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"JPY",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
JPY.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    JPY.RAT = JPY.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = JPY.RAT<1,1>
    IF WS.CCY.JPY < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.JPY.P    =  WS.CCY.JPY *  RATE.P
    WS.LCL.JPY.P    =  WS.LCL.JPY.P / 100
    WS.LCL.JPY.P    = DROUND(WS.LCL.JPY.P,2)
*MTAG TEST    WS.LCL.JPY.DIF  = WS.LCL.JPY - WS.LCL.JPY.P
    WS.LCL.JPY.DIF  = WS.LCL.JPY.P - WS.LCL.JPY
    IF WS.LCL.JPY.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(JPY.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.JPY, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.JPY.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.JPY, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.JPY.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.JPY
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.JPY.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "KWD"
    CURR.BASE.N = 'KWD...'
*Line [ 571 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"KWD",KWD.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"KWD",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
KWD.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    KWD.RAT = KWD.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = KWD.RAT<1,1>
    IF WS.CCY.KWD < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.KWD.P    =  WS.CCY.KWD *  RATE.P
    WS.LCL.KWD.P    = DROUND(WS.LCL.KWD.P,2)
*MTAG TEST    WS.LCL.KWD.DIF  = WS.LCL.KWD - WS.LCL.KWD.P
    WS.LCL.KWD.DIF  = WS.LCL.KWD.P - WS.LCL.KWD
    IF WS.LCL.KWD.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(KWD.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.KWD, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.KWD.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.KWD, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.KWD.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.KWD
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.KWD.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "NOK"
    CURR.BASE.N = 'NOK...'
*Line [ 615 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"NOK",NOK.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"NOK",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
NOK.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    NOK.RAT = NOK.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = NOK.RAT<1,1>
    IF WS.CCY.NOK < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.NOK.P    =  WS.CCY.NOK *  RATE.P
    WS.LCL.NOK.P    = DROUND(WS.LCL.NOK.P,2)
*MTAG TEST    WS.LCL.NOK.DIF  = WS.LCL.NOK - WS.LCL.NOK.P
    WS.LCL.NOK.DIF  = WS.LCL.NOK.P - WS.LCL.NOK
    IF WS.LCL.NOK.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(NOK.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.NOK, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.NOK.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.NOK, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.NOK.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.NOK
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.NOK.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "SAR"
    CURR.BASE.N = 'SAR...'
*Line [ 659 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"SAR",SAR.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"SAR",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
SAR.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    SAR.RAT = SAR.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = SAR.RAT<1,1>
    IF WS.CCY.SAR < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.SAR.P    =  WS.CCY.SAR *  RATE.P
    WS.LCL.SAR.P    = DROUND(WS.LCL.SAR.P,2)
*MTAG TEST    WS.LCL.SAR.DIF  = WS.LCL.SAR - WS.LCL.SAR.P
    WS.LCL.SAR.DIF  = WS.LCL.SAR.P - WS.LCL.SAR
    IF WS.LCL.SAR.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(SAR.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.SAR, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.SAR.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.SAR, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.SAR.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.SAR
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.SAR.DIF
    XX = SPACE(132)
    PRINT XX<1,1>
*---- "SEK"
    CURR.BASE.N = 'SEK...'
*Line [ 702 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"SEK",SEK.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"SEK",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
SEK.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    SEK.RAT = SEK.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = SEK.RAT<1,1>
    IF WS.CCY.SEK < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.SEK.P    =  WS.CCY.SEK *  RATE.P
    WS.LCL.SEK.P    = DROUND(WS.LCL.SEK.P,2)
*MTAG TEST    WS.LCL.SEK.DIF  = WS.LCL.SEK - WS.LCL.SEK.P
    WS.LCL.SEK.DIF  = WS.LCL.SEK.P - WS.LCL.SEK
    IF WS.LCL.SEK.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(SEK.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.SEK, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.SEK.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.SEK, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.SEK.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.SEK
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.SEK.DIF

    XX = SPACE(132)
    PRINT XX<1,1>
*---- "USD"
    CURR.BASE.N = 'USD...'
*Line [ 746 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,"USD",USD.RAT)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,"USD",R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
USD.RAT=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    T.SEL2  = "SELECT FBNK.CURRENCY$HIS WITH @ID LIKE "
    T.SEL2 := CURR.BASE.N:" AND DATE.TIME LT ":WS.CUR1:" BY DATE.TIME "
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    CALL F.READ(FN.CURRH,KEY.LIST2<SELECTED2>,R.CURRH,F.CURRH,E2)
    RATE.P = R.CURRH<EB.CUR.MID.REVAL.RATE><1,1>
    USD.RAT = USD.RAT<1,1>
    IF SELECTED2 EQ 0 THEN RATE.P = USD.RAT<1,1>
    IF WS.CCY.USD < 0 THEN
        WS.TYPE = 'LONG'
    END ELSE
        WS.TYPE = 'SHORT'
    END
    WS.LCL.USD.P    =  WS.CCY.USD *  RATE.P
    WS.LCL.USD.P    = DROUND(WS.LCL.USD.P,2)
* MTAG TEST     WS.LCL.USD.DIF  = WS.LCL.USD - WS.LCL.USD.P
    WS.LCL.USD.DIF = WS.LCL.USD.P - WS.LCL.USD
    IF WS.LCL.USD.DIF > 0 THEN
        WS.TYPE1 = 'CR'
    END ELSE
        WS.TYPE1 = 'DR'
    END
    XX<1,1>[1,5]    = CURR.BASE.N
    XX<1,1>[15,5]   = FMT(USD.RAT, "R4,")
    XX<1,1>[31,5]   = WS.TYPE
    XX<1,1>[45,10]  = FMT(WS.CCY.USD, "R2,")
    XX<1,1>[70,10]  = FMT(WS.LCL.USD.P, "R2,")
    XX<1,1>[90,10]  = FMT(WS.LCL.USD, "R2,")
    XX<1,1>[105,10] = FMT(WS.LCL.USD.DIF, "R2,")
    XX<1,1>[120,10] = WS.TYPE1
    PRINT XX<1,1>
    WS.TOTAL.LCL  =  WS.TOTAL.LCL +  WS.LCL.USD
    WS.TOTAL.L.S  =  WS.TOTAL.L.S +  WS.LCL.USD.DIF

    XX = SPACE(132)
    PRINT XX<1,1>

*-- PRINT TOTAL
    PRINT STR('-',132)
    XX = SPACE(132)
    PRINT XX<1,1>
    XX<1,1>[40,40]  = WS.TOTAL
    XX<1,1>[90,10]  = FMT(WS.TOTAL.LCL, "R2,")
    XX<1,1>[105,10] = FMT(WS.TOTAL.L.S, "R2,")
    PRINT XX<1,1>
    PRINT STR('-',132)
    RETURN
*---------------------------------------------
PRT.HEAD:
*--------
    WS.PAGE.COUNT = WS.PAGE.COUNT + 1
    DATY = WS.DAT.HD
    TT.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"SUEZ CANAL BANK":SPACE(20):WS.HEAD.01.A:"  ":TT.DAY:SPACE(40):WS.REP.HEADER
    PR.HD :="'L'":SPACE(1):WS.HEAD.02.A:SPACE(5):WS.HEAD.02.B:SPACE(10):WS.HEAD.02.C:SPACE(8):WS.HEAD.02.D:SPACE(10):WS.HEAD.02.E:SPACE(5):WS.HEAD.02.F
    PR.HD :="'L'":SPACE(70):WS.HEAD.03.E:SPACE(5):WS.HEAD.03.F
    PR.HD :="'L'":STR('=',132)
**    PRINT
    HEADING PR.HD
    RETURN
*------------------------------------------------
END
