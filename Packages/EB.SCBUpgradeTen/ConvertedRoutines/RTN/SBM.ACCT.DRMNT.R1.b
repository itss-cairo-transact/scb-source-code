* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-8</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.ACCT.DRMNT.R1(ENQ.DATA)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

*-----------------------------------
    SYS.DATE   = TODAY
    AGE.DATE   = SYS.DATE
    CALL CDT('', AGE.DATE, '-365C')
*------------------------
    ENQ.NAME   = ENQ.DATA<1.1>
    OPER.CODE  = "LE"
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,1> = "DATE.LAST.CR.CUST"
        ENQ.DATA<3,1> = OPER.CODE
        ENQ.DATA<4,1> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,2> = "DATE.LAST.DR.CUST"
        ENQ.DATA<3,2> = OPER.CODE
        ENQ.DATA<4,2> = AGE.DATE
    END
*------------------------
*    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
*        ENQ.DATA<2,3> = "CATEGORY"
*        ENQ.DATA<3,3> = "GE"
*        ENQ.DATA<4,3> = 1001
*    END
*------------------------
*    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
*        ENQ.DATA<2,4> = "CATEGORY"
*        ENQ.DATA<3,4> = "LE"
*        ENQ.DATA<4,4> = 1002
*    END
*------------------------
*-----------------------------------
    RETURN
END
