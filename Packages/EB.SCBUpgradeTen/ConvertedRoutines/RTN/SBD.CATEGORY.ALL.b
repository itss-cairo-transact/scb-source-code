* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* CREATE BY NESSMA
*-----------------------------------------------------------------------------
    PROGRAM SBD.CATEGORY.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY
*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB END.PROG

    RETURN
*----------------------------
END.PROG:
    PRINT "DONE"
    RETURN
*-----------------------------INITIALIZATIONS------------------------
INITIATE:
    REPORT.ID='SBD.INT.AC.ALL'

    FN.CATG = 'F.CATEGORY'     ; F.CATG = ''
    CALL OPF(FN.CATG , F.CATG)

    FN.CTP  = 'F.CATEG.ENTRY'  ; F.CTP  = ''
    CALL OPF(FN.CTP , F.CTP)
*--------------------------------------------------------------------
    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "CATEGORY.PL.ALL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"/TEST/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"CATEGORY.PL.ALL.CSV"
        HUSH OFF
    END

    OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "CATEGORY.PL.ALL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CATEGORY.PL.ALL.CSV CREATED IN /TEST/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create File CATEGORY.PL.ALL.CSV IN /TEST/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
        END
    END

    RETURN
*------------------------RED FORM TEXT FILE----------------------
PROCESS:
    N.SEL = "SELECT F.COMPANY"
    CALL EB.READLIST(N.SEL, COMP.LIST, "", COMP.SEL, ASD.C)
    PRINT COMP.SEL

    T.SEL  = "SELECT F.CATEGORY WITH @ID GE 50000"
    T.SEL := " AND @ID LE 69999"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    PRINT SELECTED

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.CATG,KEY.LIST<I>, R.CATG, F.CATG, ETEXT)
            OPENING.BAL = 0

            FOR NN = 1 TO COMP.SEL
                ID.COMPANY = COMP.LIST<NN>
                CAT.ID     = KEY.LIST<I>
                GOSUB GET.OPEN.BAL
            NEXT NN

            PRINT KEY.LIST<I> : "***" :  OPENING.BAL

            DESC.EN      = R.CATG<EB.CAT.DESCRIPTION,1>
            DESC.AR      = R.CATG<EB.CAT.DESCRIPTION,2>
            SHRT.EN      = R.CATG<EB.CAT.SHORT.NAME,1>
            SHRT.AR      = R.CATG<EB.CAT.SHORT.NAME,2>

            GOSUB WRITE.LINE
        NEXT I
    END
    RETURN
*--------------------------------------------------------------
GET.OPEN.BAL:
*-----------
    SS.DATE = TODAY[1,4]:"0101"
    EE.DATE = TODAY

    CALL GET.CATEG.MONTH.ENTRIES(SS.DATE,EE.DATE,CAT.ID,CAT.PRV.LIST)
    LOOP
        REMOVE CTP.ID FROM CAT.PRV.LIST SETTING POS.CTP
    WHILE CTP.ID:POS.CTP
        CALL F.READ(FN.CTP,CTP.ID,R.CTP,F.CTP,ER.CTP)
        OPENING.BAL += R.CTP<AC.CAT.AMOUNT.LCY>
    REPEAT

    RETURN
*---------------------------------------------------------------
WRITE.LINE:
*----------
    XX.LINE  = ""
    XX.LINE  = CAT.ID:","
    XX.LINE := DESC.EN:","
    XX.LINE := DESC.AR:","
    XX.LINE := SHRT.EN:","
    XX.LINE := SHRT.AR:","
    XX.LINE := OPENING.BAL:","

    WRITESEQ XX.LINE TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    PRINT XX.LINE
    RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:
    XX.HD  = ""
    XX.HD  = "CATEGORY.NO":","
    XX.HD := "Gb.Description":","
    XX.HD := "Ar.Description":","
    XX.HD := "Gb.Short.Name":","
    XX.HD := "Ar.Short.Name":","
    XX.HD := "Opening.Balance":","

    WRITESEQ XX.HD TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    RETURN
*--------------------------------------------------------
END
