* @ValidationCode : MjotMTk2MTgyNzkwNjpDcDEyNTI6MTY0MDg0MDg2NTIyOTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 21:07:45
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
PROGRAM SBM.ACCT.PD.DPD

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ACCT.PD
*----------------------------------------
    FN.APD = 'F.SCB.ACCT.PD' ; F.APD = ''
    CALL OPF(FN.APD,F.APD)

    END.DATE = TODAY[1,6]:'01'
    CALL CDT("",END.DATE,'-1C')

    FROM.DATE = END.DATE[1,6]:'01'

    WS.DATE = END.DATE[1,6]

    DAYS = 0
    WS.BAL.CR = 0

    T.SEL = "SELECT ":FN.APD:" WITH DATE EQ ":WS.DATE

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.APD,KEY.LIST<I>,R.APD,F.APD,ER.APD)
            WS.OPEN.BAL = R.APD<APD.OPEN.BAL>

*Line [ 51 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.TRN.COUNT  = DCOUNT(R.APD<APD.TRN.DATE>,@VM)

            FOR K = 1 TO WS.TRN.COUNT
                WS.TRN.DATE = R.APD<APD.TRN.DATE><1,K>
                WS.TRN.BAL  = R.APD<APD.TRN.BAL><1,K>

                IF WS.TRN.BAL GT 0 THEN
                    WS.BAL.CR += WS.TRN.BAL
                END
            NEXT K


            IF WS.OPEN.BAL LT 0 THEN
                WS.BAL.CR = WS.OPEN.BAL + WS.BAL.CR
                IF WS.BAL.CR GE '-1000' THEN
                    DAYS = 0
                END ELSE
                    IF DAYS = 0 THEN
                        DAYS = "C"
                        CALL CDD("",FROM.DATE,END.DATE,DAYS)
                        DAYS = DAYS + 1
                    END
                END
            END

            FOR X = 1 TO WS.TRN.COUNT

                WS.TRN.DATE.1 = R.APD<APD.TRN.DATE><1,X>
                WS.TRN.BAL.1  = R.APD<APD.TRN.BAL><1,X>

                IF WS.TRN.BAL.1 LT 0 THEN
                    WS.BAL = WS.TRN.BAL.1 + WS.BAL.CR
                    IF WS.BAL GE '-1000' THEN
                        DAYS = 0
                    END ELSE
                        IF DAYS = 0 THEN
                            DAYS = "C"
                            CALL CDD("",WS.TRN.DATE.1,END.DATE,DAYS)
                            DAYS = DAYS + 1
                        END
                    END
                    WS.BAL.CR += WS.TRN.BAL.1
                END

            NEXT X

            R.APD<APD.DPD> = DAYS

            CALL F.WRITE(FN.APD,KEY.LIST<I>,R.APD)
            CALL JOURNAL.UPDATE(KEY.LIST<I>)
            DAYS = 0
            WS.BAL.CR = 0

        NEXT I
    END

*****************************************************
RETURN
END
