* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****MOHSEN
    PROGRAM SBM.C.CRT.FIN.POS.05
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.BANK.FIN.POS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD.A
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS
*---------------------------------------------------
    FN.FIN = "F.CBE.BANK.FIN.POS"
    F.FIN  = ""
    FN.MASTACLD = "F.CBE.MAST.AC.LD.A"
    F.MASTACLD  = ""
    FN.CATMAS = "F.CATEG.MAS"
    F.CATMAS  = ""
*-----------------------------------------------------
    WS.FILE.AMT = 0
    WS.AMT.LCY = 0

    WS.AMT.FCY = 0
    WS.FIN.AMT.LCY =  0
    WS.FIN.AMT.FCY =  0
    WS.MOHSEN = 0
    WS.MOHSEN1 = 0
*-----------------------------------------------------------
* ARRAY SUB 1 = KEY OF FILE CBE.BANK.FIN.POS
* ARRAY SUB 2 = CATEGORY TO BE USED FOR READING CBE.MAST.AC.LD FILE

*    DIM ARRAY1(71,5)
     DIM ARRAY1(60,5)

    ARRAY1(1,1) = "2030000"
    ARRAY1(1,2) = "16151"
    ARRAY1(1,3) = "16152"
    ARRAY1(1,4) = "CR"
    ARRAY1(1,5) = "A"

    ARRAY1(2,1) = "2030000"
    ARRAY1(2,2) = "16188"
    ARRAY1(2,3) = "16188"
    ARRAY1(2,4) = "CR"
    ARRAY1(2,5) = "A"

    ARRAY1(3,1) = "1010100"
    ARRAY1(3,2) = "10000"
    ARRAY1(3,3) = "10001"
    ARRAY1(3,4) = "DR"
    ARRAY1(3,5) = "A"

    ARRAY1(4,1) = "1010100"
    ARRAY1(4,2) = "10010"
    ARRAY1(4,3) = "10011"
    ARRAY1(4,4) = "DR"
    ARRAY1(4,5) = "A"

    ARRAY1(5,1) = "1010100"
    ARRAY1(5,2) = "10020"
    ARRAY1(5,3) = "10020"
    ARRAY1(5,4) = "DR"
    ARRAY1(5,5) = "A"

    ARRAY1(6,1) = "1010100"
    ARRAY1(6,2) = "10200"
    ARRAY1(6,3) = "10200"
    ARRAY1(6,4) = "DR"
    ARRAY1(6,5) = "A"

    ARRAY1(7,1) = "1151400"
    ARRAY1(7,2) = "11212"
    ARRAY1(7,3) = "11218"
    ARRAY1(7,4) = "DR"
    ARRAY1(7,5) = "A"

    ARRAY1(8,1) = "1151400"
    ARRAY1(8,2) = "11274"
    ARRAY1(8,3) = "11281"
    ARRAY1(8,4) = "DR"
    ARRAY1(8,5) = "A"

    ARRAY1(9,1) = "1151400"
    ARRAY1(9,2) = "11352"
    ARRAY1(9,3) = "11360"
    ARRAY1(9,4) = "DR"
    ARRAY1(9,5) = "A"

    ARRAY1(10,1) = "1151400"
    ARRAY1(10,2) = "11369"
    ARRAY1(10,3) = "11378"
    ARRAY1(10,4) = "DR"
    ARRAY1(10,5) = "A"

    ARRAY1(11,1) = "1151400"
    ARRAY1(11,2) = "11251"
    ARRAY1(11,3) = "11251"
    ARRAY1(11,4) = "DR"
    ARRAY1(11,5) = "A"

    ARRAY1(12,1) = "1151400"
    ARRAY1(12,2) = "11019"
    ARRAY1(12,3) = "11019"
    ARRAY1(12,4) = "DR"
    ARRAY1(12,5) = "A"

    ARRAY1(13,1) = "1151400"
    ARRAY1(13,2) = "11020"
    ARRAY1(13,3) = "11020"
    ARRAY1(13,4) = "DR"
    ARRAY1(13,5) = "A"

    ARRAY1(14,1) = "1151400"
    ARRAY1(14,2) = "11022"
    ARRAY1(14,3) = "11023"
    ARRAY1(14,4) = "DR"
    ARRAY1(14,5) = "A"

*    ARRAY1(15,1) = "1151400"
*    ARRAY1(15,2) = "11035"
*    ARRAY1(15,3) = "11035"
*    ARRAY1(15,4) = "DR"
*    ARRAY1(15,5) = "A"

    ARRAY1(15,1) = "0"
    ARRAY1(15,2) = "0"
    ARRAY1(15,3) = "0"
    ARRAY1(15,4) = ""
    ARRAY1(15,5) = ""

    ARRAY1(16,1) = "1151400"
    ARRAY1(16,2) = "10300"
    ARRAY1(16,3) = "10300"
    ARRAY1(16,4) = "DR"
    ARRAY1(16,5) = "A"

***********************************************************
    ARRAY1(17,1) = "1151400"
    ARRAY1(17,2) = "11039"
    ARRAY1(17,3) = "11039"
    ARRAY1(17,4) = "DR"
    ARRAY1(17,5) = "A"

    ARRAY1(18,1) = "1151400"
    ARRAY1(18,2) = "11061"
    ARRAY1(18,3) = "11062"
    ARRAY1(18,4) = "DR"
    ARRAY1(18,5) = "A"

    ARRAY1(19,1) = "1151400"
    ARRAY1(19,2) = "11029"
    ARRAY1(19,3) = "11029"
    ARRAY1(19,4) = "DR"
    ARRAY1(19,5) = "A"

    ARRAY1(20,1) = "1151400"
    ARRAY1(20,2) = "11017"
    ARRAY1(20,3) = "11017"
    ARRAY1(20,4) = "DR"
    ARRAY1(20,5) = "A"

    ARRAY1(21,1) = "1151400"
    ARRAY1(21,2) = "11033"
    ARRAY1(21,3) = "11035"
    ARRAY1(21,4) = "DR"
    ARRAY1(21,5) = "A"

    ARRAY1(22,1) = "2030000"
    ARRAY1(22,2) = "16153"
    ARRAY1(22,3) = "16154"
    ARRAY1(22,4) = "CR"
    ARRAY1(22,5) = "A"

    ARRAY1(23,1) = "1151400"
    ARRAY1(23,2) = "11253"
    ARRAY1(23,3) = "11253"
    ARRAY1(23,4) = "DR"
    ARRAY1(23,5) = "A"

    ARRAY1(24,1) = "1151400"
    ARRAY1(24,2) = "11400"
    ARRAY1(24,3) = "11400"
    ARRAY1(24,4) = "DR"
    ARRAY1(24,5) = "A"

    ARRAY1(25,1) = "1151400"
    ARRAY1(25,2) = "11068"
    ARRAY1(25,3) = "11068"
    ARRAY1(25,4) = "DR"
    ARRAY1(25,5) = "A"

    ARRAY1(26,1) = "1551400"
    ARRAY1(26,2) = "11048"
    ARRAY1(26,3) = "11048"
    ARRAY1(26,4) = "DR"
    ARRAY1(26,5) = "A"

    ARRAY1(27,1) = "0"
    ARRAY1(27,2) = "0"
    ARRAY1(27,3) = "0"
    ARRAY1(27,4) = ""
    ARRAY1(27,5) = ""

    ARRAY1(28,1) = "0"
    ARRAY1(28,2) = "0"
    ARRAY1(28,3) = "0"
    ARRAY1(28,4) = ""
    ARRAY1(28,5) = ""

    ARRAY1(29,1) = "0"
    ARRAY1(29,2) = "0"
    ARRAY1(29,3) = "0"
    ARRAY1(29,4) = ""
    ARRAY1(29,5) = ""

    ARRAY1(30,1) = "0"
    ARRAY1(30,2) = "0"
    ARRAY1(30,3) = "0"
    ARRAY1(30,4) = ""
    ARRAY1(30,5) = ""

    ARRAY1(31,1) = "0"
    ARRAY1(31,2) = "0"
    ARRAY1(31,3) = "0"
    ARRAY1(31,4) = ""
    ARRAY1(31,5) = ""

    ARRAY1(32,1) = "0"
    ARRAY1(32,2) = "0"
    ARRAY1(32,3) = "0"
    ARRAY1(32,4) = ""
    ARRAY1(32,5) = ""

    ARRAY1(33,1) = "0"
    ARRAY1(33,2) = "0"
    ARRAY1(33,3) = "0"
    ARRAY1(33,4) = ""
    ARRAY1(33,5) = ""

    ARRAY1(34,1) = "0"
    ARRAY1(34,2) = "0"
    ARRAY1(34,3) = "0"
    ARRAY1(34,4) = ""
    ARRAY1(34,5) = ""

    ARRAY1(35,1) = "0"
    ARRAY1(35,2) = "0"
    ARRAY1(35,3) = "0"
    ARRAY1(35,4) = ""
    ARRAY1(35,5) = ""

    ARRAY1(36,1) = "0"
    ARRAY1(36,2) = "0"
    ARRAY1(36,3) = "0"
    ARRAY1(36,4) = ""
    ARRAY1(36,5) = ""

    ARRAY1(37,1) = "0"
    ARRAY1(37,2) = "0"
    ARRAY1(37,3) = "0"
    ARRAY1(37,4) = ""
    ARRAY1(37,5) = ""

    ARRAY1(38,1) = "0"
    ARRAY1(38,2) = "0"
    ARRAY1(38,3) = "0"
    ARRAY1(38,4) = ""
    ARRAY1(38,5) = ""

    ARRAY1(39,1) = "0"
    ARRAY1(39,2) = "0"
    ARRAY1(39,3) = "0"
    ARRAY1(39,4) = ""
    ARRAY1(39,5) = ""

    ARRAY1(40,1) = "0"
    ARRAY1(40,2) = "0"
    ARRAY1(40,3) = "0"
    ARRAY1(40,4) = ""
    ARRAY1(40,5) = ""

    ARRAY1(41,1) = "0"
    ARRAY1(41,2) = "0"
    ARRAY1(41,3) = "0"
    ARRAY1(41,4) = ""
    ARRAY1(41,5) = ""

    ARRAY1(42,1) = "0"
    ARRAY1(42,2) = "0"
    ARRAY1(42,3) = "0"
    ARRAY1(42,4) = ""
    ARRAY1(42,5) = ""

    ARRAY1(43,1) = "0"
    ARRAY1(43,2) = "0"
    ARRAY1(43,3) = "0"
    ARRAY1(43,4) = ""
    ARRAY1(43,5) = ""

    ARRAY1(44,1) = "0"
    ARRAY1(44,2) = "0"
    ARRAY1(44,3) = "0"
    ARRAY1(44,4) = ""
    ARRAY1(44,5) = ""

    ARRAY1(45,1) = "0"
    ARRAY1(45,2) = "0"
    ARRAY1(45,3) = "0"
    ARRAY1(45,4) = ""
    ARRAY1(45,5) = ""

    ARRAY1(46,1) = "0"
    ARRAY1(46,2) = "0"
    ARRAY1(46,3) = "0"
    ARRAY1(46,4) = ""
    ARRAY1(46,5) = ""

    ARRAY1(47,1) = "0"
    ARRAY1(47,2) = "0"
    ARRAY1(47,3) = "0"
    ARRAY1(47,4) = ""
    ARRAY1(47,5) = ""

    ARRAY1(48,1) = "0"
    ARRAY1(48,2) = "0"
    ARRAY1(48,3) = "0"
    ARRAY1(48,4) = ""
    ARRAY1(48,5) = ""

    ARRAY1(49,1) = "0"
    ARRAY1(49,2) = "0"
    ARRAY1(49,3) = "0"
    ARRAY1(49,4) = ""
    ARRAY1(49,5) = ""

    ARRAY1(50,1) = "0"
    ARRAY1(50,2) = "0"
    ARRAY1(50,3) = "0"
    ARRAY1(50,4) = ""
    ARRAY1(50,5) = ""

    ARRAY1(51,1) = "0"
    ARRAY1(51,2) = "0"
    ARRAY1(51,3) = "0"
    ARRAY1(51,4) = ""
    ARRAY1(51,5) = ""

    ARRAY1(52,1) = "0"
    ARRAY1(52,2) = "0"
    ARRAY1(52,3) = "0"
    ARRAY1(52,4) = ""
    ARRAY1(52,5) = ""

    ARRAY1(53,1) = "0"
    ARRAY1(53,2) = "0"
    ARRAY1(53,3) = "0"
    ARRAY1(53,4) = ""
    ARRAY1(53,5) = ""

    ARRAY1(54,1) = "0"
    ARRAY1(54,2) = "0"
    ARRAY1(54,3) = "0"
    ARRAY1(54,4) = ""
    ARRAY1(54,5) = ""

    ARRAY1(55,1) = "0"
    ARRAY1(55,2) = "0"
    ARRAY1(55,3) = "0"
    ARRAY1(55,4) = ""
    ARRAY1(55,5) = ""

    ARRAY1(56,1) = "0"
    ARRAY1(56,2) = "0"
    ARRAY1(56,3) = "0"
    ARRAY1(56,4) = ""
    ARRAY1(56,5) = ""

    ARRAY1(57,1) = "0"
    ARRAY1(57,2) = "0"
    ARRAY1(57,3) = "0"
    ARRAY1(57,4) = ""
    ARRAY1(57,5) = ""

    ARRAY1(58,1) = "0"
    ARRAY1(58,2) = "0"
    ARRAY1(58,3) = "0"
    ARRAY1(58,4) = ""
    ARRAY1(58,5) = ""

    ARRAY1(59,1) = "0"
    ARRAY1(59,2) = "0"
    ARRAY1(59,3) = "0"
    ARRAY1(59,4) = ""
    ARRAY1(59,5) = ""

    ARRAY1(60,1) = "0"
    ARRAY1(60,2) = "0"
    ARRAY1(60,3) = "0"
    ARRAY1(60,4) = ""
    ARRAY1(60,5) = ""


***********************************************************

*                                             LAST
*------------------------------------------------
    R.FIN = ""
*----------------------------PROCEDURE---------------------
    CALL OPF (FN.FIN,F.FIN)
    CALL OPF (FN.MASTACLD,F.MASTACLD)
    CALL OPF (FN.CATMAS,F.CATMAS)
*------------------------PROCEDURE------------------------
    GOSUB A.100.ARRAY1
    RETURN
*------------------------------------------------
A.100.ARRAY1:
     FOR AR1 = 1 TO 60
        WS.FIN.ID = ARRAY1(AR1,1)
        WS.CATEG.FRM =  ARRAY1(AR1,2)
        WS.CATEG.TO  =  ARRAY1(AR1,3)
        WS.SIGN  =  ARRAY1(AR1,4)
        WS.FILE =   ARRAY1(AR1,5)
        WS.AMT.LCY = 0
        WS.AMT.FCY = 0
        GOSUB A.200.ASIN.FILE
    NEXT AR1
    RETURN
*------------------------------------------------
A.200.ASIN.FILE:
    IF WS.CATEG.FRM EQ 0 THEN
        RETURN
    END
    IF WS.FILE EQ "A" THEN
        GOSUB A.400.GET.CATEG
*        GOSUB A.500.UPDAT
    END
    IF WS.FILE EQ "B" THEN
        GOSUB A.410.GET.CATEG
*       GOSUB A.500.UPDAT
    END
    RETURN
*------------------------------------------------
A.400.GET.CATEG:
*    SEL.CMD = "SELECT ":FN.MASTACLD:" WITH CBEM.CATEG EQ ":WS.CATEG
*    SEL.CMD = "SELECT ":FN.MASTACLD:" BY CBEM.CATEG"
    SEL.CMD = "SELECT ":FN.MASTACLD:" WITH CBEM.CATEG GE ":WS.CATEG.FRM:" AND CBEM.CATEG LE ":WS.CATEG.TO
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.MAST.ID FROM SEL.LIST SETTING POS
    WHILE WS.MAST.ID:POS
        CALL F.READ(FN.MASTACLD,WS.MAST.ID,R.MAST,F.MASTACLD,MSG.CAT)

        WS.FILE.AMT = R.MAST<CC.CBEM.IN.LCY>
        WS.FIL.CATG = R.MAST<CC.CBEM.CATEG>

        IF  WS.FIL.CATG LT WS.CATEG.FRM   THEN
            GOTO A.400.3
        END
        IF  WS.FIL.CATG GT WS.CATEG.TO   THEN
            GOTO A.400.4
        END

        IF WS.FILE.AMT  EQ 0 THEN
            GOTO  A.400.3
        END

        IF  WS.FILE.AMT LT 0 THEN
            WS.SS = "DR"
        END
        IF  WS.FILE.AMT GT 0 THEN
            WS.SS = "CR"
        END

        IF  WS.SIGN  EQ "ALL" THEN
            GOTO   A.400.1
        END

        IF  WS.SIGN EQ WS.SS THEN
            GOTO A.400.1
        END

        GOTO  A.400.3
A.400.1:
        WS.BR.F = R.MAST<CC.CBEM.BR>
        WS.BR = WS.BR.F
        IF WS.BR.F LT 10 THEN
            WS.BR = WS.BR.F[1]
        END
        WS.AMT.LCY = 0
        WS.AMT.FCY = 0

        IF R.MAST<CC.CBEM.CY> EQ "EGP" THEN

*           WS.AMT.LCY = WS.AMT.LCY + WS.FILE.AMT
            WS.AMT.LCY =  WS.FILE.AMT
        END

        IF R.MAST<CC.CBEM.CY> NE "EGP" THEN

*            WS.AMT.FCY = WS.AMT.FCY + WS.FILE.AMT
            WS.AMT.FCY =  WS.FILE.AMT
        END
        GOSUB A.500.UPDAT

A.400.3:
    REPEAT
A.400.4:
    RETURN
*--------------------------------------------------------------
A.410.GET.CATEG:
    SEL.CMD1 = "SELECT ":FN.CATMAS:" WITH CAT.CATEG.CODE  GE ":WS.CATEG.FRM:" AND CAT.CATEG.CODE LE ":WS.CATEG.TO
    CALL EB.READLIST(SEL.CMD1,SEL.LIST1,"",NO.OF.REC1,RET.CODE1)
    LOOP
        REMOVE WS.CATMAS.ID FROM SEL.LIST1 SETTING POS1
    WHILE WS.CATMAS.ID:POS1
        CALL F.READ(FN.CATMAS,WS.CATMAS.ID,R.CATMAS,F.CATMAS,MSG.CATMAS)

        WS.FILE.AMT = R.CATMAS<CAT.CAT.BAL.IN.LOCAL.CY>
        WS.FIL.CATG = R.CATMAS<CAT.CAT.CATEG.CODE>

        IF  WS.FIL.CATG LT WS.CATEG.FRM   THEN
            GOTO A.410.3
        END
        IF  WS.FIL.CATG GT WS.CATEG.TO   THEN
            GOTO A.410.4
        END

        IF WS.FILE.AMT  EQ 0 THEN
            GOTO  A.410.3
        END

        IF  WS.FILE.AMT LT 0 THEN
            WS.SS = "DR"
        END
        IF  WS.FILE.AMT GT 0 THEN
            WS.SS = "CR"
        END

        IF  WS.SIGN  EQ "ALL" THEN
            GOTO   A.410.1
        END

        IF  WS.SIGN EQ WS.SS THEN
            GOTO A.410.1
        END

        GOTO  A.410.3
A.410.1:
        WS.BR.F = R.CATMAS<CAT.CAT.BR>
        WS.BR = WS.BR.F
        IF WS.BR.F LT 10 THEN
            WS.BR = WS.BR.F[1]
        END
        WS.AMT.LCY = 0
        WS.AMT.FCY = 0
        IF R.CATMAS<CAT.CAT.CY.ALPH.CODE> EQ "EGP" THEN

*            WS.AMT.LCY = WS.AMT.LCY + WS.FILE.AMT
            WS.AMT.LCY =  WS.FILE.AMT
        END

        IF R.CATMAS<CAT.CAT.CY.ALPH.CODE> NE "EGP" THEN

*            WS.AMT.FCY = WS.AMT.FCY + WS.FILE.AMT
            WS.AMT.FCY =              WS.FILE.AMT
        END
        GOSUB A.500.UPDAT
A.410.3:
    REPEAT
A.410.4:
    RETURN
*--------------------------------------------------------------
A.500.UPDAT:
*    WS.FIN.AMT.LCY.FCY = WS.FIN.AMT.LCY + WS.FIN.AMT.FCY
*    IF  WS.FIN.ID EQ 1150100  AND WS.FIN.AMT.LCY.FCY GT 0 THEN
*        GOSUB A.550.UPDAT
*        RETURN
*    END
    WS.NEW.KEY = WS.FIN.ID:"*":WS.BR
*    CALL F.READ(FN.FIN,WS.FIN.ID,R.FIN,F.FIN,MSG.FIN)
    CALL F.READ(FN.FIN,WS.NEW.KEY,R.FIN,F.FIN,MSG.FIN)
    IF MSG.FIN NE "" THEN
        RETURN
    END

    WS.FIN.AMT.LCY =  R.FIN<CBE.FIN.POS.AMT.LCY>
    WS.FIN.AMT.FCY =  R.FIN<CBE.FIN.POS.AMT.FCY>

    WS.FIN.AMT.LCY =  WS.FIN.AMT.LCY + WS.AMT.LCY
    WS.FIN.AMT.FCY =  WS.FIN.AMT.FCY + WS.AMT.FCY
*    WS.MOHSEN = WS.MOHSEN + WS.AMT.LCY
*    WS.MOHSEN1 = WS.MOHSEN1 + WS.AMT.FCY
    R.FIN<CBE.FIN.POS.AMT.LCY>  =  WS.FIN.AMT.LCY
    R.FIN<CBE.FIN.POS.AMT.FCY>  =  WS.FIN.AMT.FCY
    CALL F.WRITE(FN.FIN,WS.NEW.KEY,R.FIN)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)

    RETURN
A.550.UPDAT:
*    IF  WS.FIN.ID EQ 1150100  THEN
*        WS.FIN.ID = 2060100
*    END
    WS.NEW.KEY = WS.FIN.ID:"*":WS.BR
*    CALL F.READ(FN.FIN,WS.FIN.ID,R.FIN,F.FIN,MSG.FIN)
    CALL F.READ(FN.FIN,WS.NEW.KEY,R.FIN,F.FIN,MSG.FIN)
    IF MSG.FIN NE "" THEN
        RETURN
    END

    WS.FIN.AMT.LCY =  R.FIN<CBE.FIN.POS.AMT.LCY>
    WS.FIN.AMT.FCY =  R.FIN<CBE.FIN.POS.AMT.FCY>

    WS.FIN.AMT.LCY =  WS.FIN.AMT.LCY + WS.AMT.LCY
    WS.FIN.AMT.FCY =  WS.FIN.AMT.FCY + WS.AMT.FCY

    R.FIN<CBE.FIN.POS.AMT.LCY>  =  WS.FIN.AMT.LCY
    R.FIN<CBE.FIN.POS.AMT.FCY>  =  WS.FIN.AMT.FCY
    CALL F.WRITE(FN.FIN,WS.NEW.KEY,R.FIN)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)

    RETURN
END
