* @ValidationCode : Mjo3MjMwMjgxNDE6Q3AxMjUyOjE2NDA4MjUwMzY3MTA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:43:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>636</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBD.FUND.DEP.FILL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] HASHING $INCLUDE I_AC.LOCAL.REFS  - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FUND.DEP
*-------------------------------------------------------------------------
    GOSUB INITIATE

***** CURRENCY SELECTION *************
    T.SEL2 = "SELECT FBNK.CURRENCY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
            CUR.ID = KEY.LIST2<NN>
            GOSUB PROCESS
        NEXT NN
    END
RETURN

*-------------------------------------------------------------------------
INITIATE:

    FN.CCBE = "F.SCB.FUND.DEP"
    F.CCBE = ''
    CALL OPF(FN.CCBE,F.CCBE)
*-- EDIT BY NESSMA
    CALL OPF(FN.CCBE,FILEVAR)
*    OPEN FN.CCBE TO FILEVAR ELSE ABORT 201, FN.CCBE
*-- END EDIT
    CLEARFILE FILEVAR
    PRINT 'FILE CLEARED'


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.LMM = 'FBNK.LMM.CUSTOMER' ; F.LMM = ''
    CALL OPF(FN.LMM,F.LMM)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.FD = 'F.SCB.FUND.DEP' ; F.FD = ''
    CALL OPF(FN.FD,F.FD)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
    AMT.CUR = 0 ; AMT.CD  = 0 ; AMT.DEP = 0 ; AMT.OTHERS = 0 ; CUS.ID1 = ''
    AMT.SAV = 0 ; AMT.INT = 0 ; AMT.LG  = 0 ; AMT.TOTAL  = 0 ; AMT.LC  = 0

    DAT = TODAY
    CALL CDT("",DAT,'-1C')

    TD = TODAY
    CALL CDT ('',TD,'-1W')

RETURN

*========================================================================
PROCESS:

*-------------------------------------------
******* ACCOUNTS SELECTION *************

    T.SEL  = "SELECT ":FN.AC:" WITH (( CATEGORY IN (1481 1499 1408 1582 1483 1595 1493 1558)"
    T.SEL := " OR CATEGORY IN (1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 1208 1211 1212 1216 1301 1302 1303 1377 1390)"
    T.SEL := " OR CATEGORY IN (1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513)"
    T.SEL := " OR CATEGORY IN (6501 6502 6503 6504 6511 6512 1012 1013 1014 1015 1016 1019 3011 3012 3013 3014 3017 3010 3005)"
    T.SEL := " OR CATEGORY IN (1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 1407 1413 1480))"
    T.SEL := " AND CURRENCY EQ ":CUR.ID:" AND OPEN.ACTUAL.BAL GE 0) BY CUSTOMER"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CATEG  = R.AC<AC.CATEGORY>
            CUS.ID = R.AC<AC.CUSTOMER>
            IF I EQ 1 THEN CUS.ID1 = CUS.ID

            IF CUS.ID NE CUS.ID1 THEN
                GOSUB LD.SEL
            END

            IF (CATEG GE 6501 AND CATEG LE 6504) OR (CATEG EQ 6511) THEN
                AMT.SAV += R.AC<AC.ONLINE.ACTUAL.BAL>
            END ELSE
                IF (CATEG GE 1012 AND CATEG LE 1016) OR (CATEG EQ 1019) THEN
                    AMT.INT += R.AC<AC.ONLINE.ACTUAL.BAL>
                END ELSE
                    IF (CATEG GE 3010 AND CATEG LE 3014) OR (CATEG EQ 3017) THEN
                        AMT.LC += R.AC<AC.ONLINE.ACTUAL.BAL>
                    END ELSE
                        IF CATEG EQ 3005 THEN
                            AMT.LG += R.AC<AC.ONLINE.ACTUAL.BAL>
                        END ELSE
                            IF CATEG EQ 6512 THEN
                                AMT.DEP += R.AC<AC.ONLINE.ACTUAL.BAL>
                            END ELSE
                                AMT.CUR += R.AC<AC.ONLINE.ACTUAL.BAL>
                            END
                        END
                    END
                END
            END

            AMT.TOTAL += R.AC<AC.ONLINE.ACTUAL.BAL>

            CUS.ID1 = CUS.ID
        NEXT I
        IF I EQ SELECTED THEN GOSUB LD.SEL
    END
RETURN
*---------------------------------------------
******* LDs SELECTION *************
LD.SEL:

    CALL F.READ(FN.LMM,CUS.ID1,R.LMM,F.LMM,ETEXT)
    LOOP
        REMOVE LD.ID FROM R.LMM SETTING POS.ACCT
    WHILE LD.ID:POS.ACCT
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,E1)
        CATEG.LD = R.LD<LD.CATEGORY>
        V.DATE   = R.LD<LD.VALUE.DATE>
        MAT.DATE = R.LD<LD.FIN.MAT.DATE>
        AMT.LD = R.LD<LD.AMOUNT>
        CUR.LD = R.LD<LD.CURRENCY>
        IF ((CATEG.LD GE 21001 AND CATEG.LD LE 21010) OR (CATEG.LD GE 21019 AND CATEG.LD LE 21029) OR (CATEG.LD EQ 21032 OR CATEG.LD EQ 21041 OR CATEG.LD EQ 21042)) AND (( V.DATE LE DAT AND AMT.LD NE 0 ) OR ( MAT.DATE GT DAT AND AMT.LD EQ 0 )) AND CUR.LD EQ CUR.ID THEN

            IF AMT.LD EQ 0 THEN
                AMT.LD = R.LD<LD.REIMBURSE.AMOUNT>
            END

            IF (CATEG.LD GE 21001 AND CATEG.LD LE 21010) THEN
                AMT.DEP += AMT.LD
            END ELSE
                AMT.CD += AMT.LD
            END

            AMT.TOTAL += AMT.LD
        END
    REPEAT

    IF AMT.TOTAL GT 0 THEN
        GOSUB PRINT.DET
    END

************************************************************
RETURN
*==============================================================
PRINT.DET:
    FD.ID = CUS.ID1:'-':CUR.ID
*Line [ 191 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID1,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID1,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

    CALL F.READ(FN.FD,FD.ID,R.FD,F.FD,E1)
    R.FD<FD.CUSTOMER.NO>   =  CUS.ID1
    R.FD<FD.CUSTOMER.NAME> =  CUST.NAME
    R.FD<FD.CURRENCY>      =  CUR.ID
    R.FD<FD.CURRENT.ACCT>  =  AMT.CUR
    R.FD<FD.SAVING.ACCT>   =  AMT.SAV
    R.FD<FD.TIME.DEP>      =  AMT.DEP
    R.FD<FD.CDS>           =  AMT.CD
    R.FD<FD.INT.ACCT>      =  AMT.INT
    R.FD<FD.CONT.LG>       =  AMT.LG
    R.FD<FD.CONT.LC>       =  AMT.LC
    R.FD<FD.OTHERS>        =  AMT.OTHERS
    R.FD<FD.TOTAL.AMT>     =  AMT.TOTAL
    R.FD<FD.WS.DATE>       =  TD

    CALL F.WRITE(FN.FD,FD.ID,R.FD)
    CALL  JOURNAL.UPDATE(FD.ID)

    AMT.CUR = 0 ; AMT.CD  = 0 ; AMT.DEP = 0 ; AMT.OTHERS = 0 ; CUS.ID1 = ''
    AMT.SAV = 0 ; AMT.INT = 0 ; AMT.LG  = 0 ; AMT.TOTAL  = 0 ; AMT.LC  = 0

RETURN

*===============================================================
END
