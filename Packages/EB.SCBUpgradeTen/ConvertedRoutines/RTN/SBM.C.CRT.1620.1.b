* @ValidationCode : Mjo0ODk5OTQ0MzA6Q3AxMjUyOjE2NDE3NTc1NjcwMTA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 11:46:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
* ValidationCode : MjotMTk2NTI3OTIzNTpDcDEyNTI6MTY0MDg0MTA2NTg3NTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* ValidationInfo : Timestamp         : 29 Dec 2021 21:11:05
* ValidationInfo : Encoding          : Cp1252
* ValidationInfo : User Name         : lap
* ValidationInfo : Nb tests success  : N/A
* ValidationInfo : Nb tests failure  : N/A
* ValidationInfo : Rating            : N/A
* ValidationInfo : Coverage          : N/A
* ValidationInfo : Strict flag       : N/A
* ValidationInfo : Bypass GateKeeper : false
* ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
*    SUBROUTINE  SBM.C.CRT.1620.1
PROGRAM  SBM.C.CRT.1620.1

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.STATIC.MAST.P1
*------------------------------------
    FN.LD = "F.CBE.MAST.AC.LD"
    F.LD  = ""

    FN.STAT = "F.CBE.STATIC.MAST.P1"
    F.STAT = ""
*------------------------------------------------
    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.STAT,F.STAT)
*------------------------------------------------CLEAR AREA
    FN.CLEAR = "F.CBE.STATIC.MAST.P1"
    FILEVAR  = ''
    CALL OPF(FN.CLEAR,FILEVAR)
    CLEARFILE FILEVAR
*-----------------------------------------------
    DIM ARRAYRNG(27,2)

*    ARRAY() = "����� ���� ��������                "
    ARRAYRNG(1,1) = "110"
    ARRAYRNG(1,2) = "01"

*     ARRAY() = "����� ���� ������� �����            "
    ARRAYRNG(2,1) = "120"
    ARRAYRNG(2,2) = "02"
*
*    ARRAY() = "����� ������ �����                 "
    ARRAYRNG(3,1) = "130"
    ARRAYRNG(3,2) = "03"
*
*    ARRAY()  = "����� ������� ����� ���������       "
    ARRAYRNG(4,1) = "210"
    ARRAYRNG(4,2) = "04"
*
*    ARRAY() = "����� ������� ������                 "
    ARRAYRNG(5,1) = "220"
    ARRAYRNG(5,2) = "05"
*
*    ARRAY() = "����� �������                       "
    ARRAYRNG(6,1) = "230"
    ARRAYRNG(6,2) = "06"
*
*    ARRAY() = "�������� ���������                 "
    ARRAYRNG(7,1) = "240"
    ARRAYRNG(7,2) = "07"
*
*    ARRAY() = "����� �����                        "
    ARRAYRNG(8,1) = "250"
    ARRAYRNG(8,2) = "08"
*
*    ARRAY() = "����� ���� ����� ������� �����     "
    ARRAYRNG(9,1) = "260"
    ARRAYRNG(9,2) = "09"
*
*    ARRAY()  = "������ � ������� �������� (1(      "
    ARRAYRNG(10,1) = "4500"
    ARRAYRNG(10,2) = "10"

    ARRAYRNG(11,1) = "8010"
    ARRAYRNG(11,2) = "10"

    ARRAYRNG(12,1) = "8011"
    ARRAYRNG(12,2) = "10"
*
*    ARRAY()  = "�������� �������                  "
    ARRAYRNG(13,1) = "4550"
    ARRAYRNG(13,2) = "10"
*
*    ARRAY()  = "����� �����                       "
    ARRAYRNG(14,1) = "4600"
    ARRAYRNG(14,2) = "10"
*
*    ARRAY()  = "����� �������                     "
    ARRAYRNG(15,1) = "4650"
    ARRAYRNG(15,2) = "11"
*
*    ARRAY()  = "����� ����� �� ���� �����          "
    ARRAYRNG(16,1) = "4700"
    ARRAYRNG(16,2) = "12"
*
*    ARRAY()  = "����� ������ ���� �� ���          "
    ARRAYRNG(17,1) = "4750"
    ARRAYRNG(17,2) = "13"
*
*    ARRAY()  = "������ �������(�����(             "
    ARRAYRNG(18,1) = "7000"
    ARRAYRNG(18,2) = "15"
*
*    ARRAY()  = " ������ ������� ������� ���������  "
    ARRAYRNG(19,1) = "8001"
    ARRAYRNG(19,2) = "16"

*    ARRAY()  = "  ���� ��� ���� ���������          "
    ARRAYRNG(20,1) = "8002"
    ARRAYRNG(20,2) = "16"
*
*    ARRAY()  = "������ ������� ������             "
    ARRAYRNG(21,1) = "8003"
    ARRAYRNG(21,2) = "16"
*
*    ARRAY()  = "������ ������� ������� ��� �������"
    ARRAYRNG(22,1) = "8004"
    ARRAYRNG(22,2) = "16"
*
*    ARRAY()  = "������ ���������                   "
    ARRAYRNG(23,1) = "8005"
    ARRAYRNG(23,2) = "16"
*
*    ARRAY()  = "������ ��������� ��� �������        "
    ARRAYRNG(24,1) = "8006"
    ARRAYRNG(24,2) = "16"
*
*    ARRAY()  = "����� ������� (����� ���������)    "
    ARRAYRNG(25,1) = "8007"
    ARRAYRNG(25,2) = "16"

*    ARRAY()  = "����� ������� ������               "
    ARRAYRNG(26,1) = "8008"
    ARRAYRNG(26,2) = "16"

*    ARRAY()  = "����� ������� ���������            "
    ARRAYRNG(27,1) = "6000"
    ARRAYRNG(27,2) = "14"
*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    GOSUB A.100.PROCESS:
*------------------------------------------START PROCESSING
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*----------------------------
A.100.PROCESS:
*------------------------------------------------------------------
    SEL.CMD = "SELECT ":FN.LD
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP

        REMOVE WS.LD.ID FROM SEL.LIST SETTING POS
    WHILE WS.LD.ID:POS
        CALL F.READ(FN.LD,WS.LD.ID,R.LD,F.LD,MSG.LD)
        WS.CO.CODE = "EG00100":R.LD<C.CBEM.BR>
        IF WS.CO.CODE = "EG0010088" THEN
            GOTO A.100.REP
        END
        WS.SECTOR = R.LD<C.CBEM.NEW.SECTOR>
        WS.CATEG    = R.LD<C.CBEM.CATEG>

        WS.AMT = R.LD<C.CBEM.IN.LCY>

        IF WS.CATEG GE 21001 AND WS.CATEG LE 21010 THEN
            GOTO A.100.B
        END
        IF WS.CATEG EQ 6512 THEN
            GOTO A.100.B
        END
        IF WS.CATEG GE 21020 AND WS.CATEG LE 21028 THEN
            GOTO A.100.B
        END
        IF WS.CATEG GE 1012 AND WS.CATEG LE 1016 THEN
            GOTO A.100.B
        END
        IF WS.CATEG EQ 1019 THEN
            GOTO A.100.B
        END
        IF WS.CATEG EQ 3001 THEN
            GOTO A.100.B
        END
        IF WS.CATEG GE 3010 AND WS.CATEG LE 3013 THEN
            GOTO A.100.B
        END
        IF WS.CATEG GE 3015 AND WS.CATEG LE 3017 THEN
            GOTO A.100.B
        END
        IF WS.CATEG EQ 3005 THEN
            GOTO A.100.B
        END
        IF WS.CATEG EQ 3014 THEN
            GOTO A.100.B
        END

        IF WS.CATEG GE 1001 AND WS.CATEG LE 1002 THEN
            GOTO  A.100.A
        END
        IF WS.CATEG GE 1100 AND WS.CATEG LE 1599 THEN
            GOTO  A.100.A
        END

        GOTO A.100.REP
A.100.A:
        IF WS.AMT GT 0  THEN
            GOTO A.100.B
        END
        GOTO A.100.REP
A.100.B:
        IF  WS.AMT  EQ ""  THEN
            GOTO A.100.REP
        END

        WS.SECTOR   = R.LD<C.CBEM.NEW.SECTOR>
        WS.SECTOR.N = R.LD<C.CBEM.NEW.SECTOR>
        WS.N.INDST  = R.LD<C.CBEM.NEW.INDUSTRY>
****������ ����� ������ ����� ���� �������
        IF  WS.SECTOR EQ 1130 AND  WS.N.INDST EQ 2070 THEN
            WS.SECTOR   = 8008
            WS.SECTOR.N = 8008
        END
        IF  WS.SECTOR EQ 2130 AND  WS.N.INDST EQ 2070 THEN
            WS.SECTOR   = 8008
            WS.SECTOR.N = 8008
        END
        IF  WS.SECTOR EQ 3130 AND  WS.N.INDST EQ 2070 THEN
            WS.SECTOR   = 8008
            WS.SECTOR.N = 8008
        END
        IF  WS.SECTOR EQ 4130 AND  WS.N.INDST EQ 2070 THEN
            WS.SECTOR = 8008
            WS.SECTOR.N = 8008
        END

        IF WS.SECTOR EQ 0 THEN
            GOTO A.100.REP
        END

        IF WS.SECTOR LT 4500   THEN
            WS.SECTOR.N  = WS.SECTOR[3]
        END

        WS.SUB = 1
        GOSUB A.200.CHK.INDSTRY
*-----------------------------------------------------
A.100.REP:
    REPEAT
A.100.EXIT:
RETURN
*-----------------------------------------------
A.200.CHK.INDSTRY:
    IF WS.SUB GT 27  THEN
        RETURN
    END
    IF  WS.SECTOR.N EQ  ARRAYRNG(WS.SUB,1) THEN
        WS.KEY2     =  ARRAYRNG(WS.SUB,2)
        GOSUB A.300.CRT
        RETURN
    END
    WS.SUB = WS.SUB + 1
    GOTO A.200.CHK.INDSTRY
RETURN
*----------------------------------------------------
A.300.CRT:
    MSG.STAT = ""
    WS.STAT.ID = WS.CO.CODE:"*":WS.KEY2
    CALL F.READ(FN.STAT,WS.STAT.ID,R.STAT,F.STAT,MSG.STAT)
    IF MSG.STAT EQ ""  THEN
        GOSUB A.330.EXIST
        RETURN
    END
    GOSUB A.350.NEW

RETURN
*----------------------------------------------------
A.330.EXIST:
    IF WS.AMT GE 0 AND WS.AMT LE 50000 THEN
        WS.TEMP = R.STAT<CBE.SM.COLUMN.01>
*Line [ 293 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.01>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 297 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.01> = WS.TEMP
*Line [ 299 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.01> = WS.COUNT
    END

    IF WS.AMT GT 50000 AND WS.AMT LE 100000 THEN
*Line [ 304 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.02>
*Line [ 306 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.02>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 310 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.02> = WS.TEMP
*Line [ 312 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.02> = WS.COUNT
    END

    IF WS.AMT GT 100000 AND WS.AMT LE 500000 THEN
*Line [ 317 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.03>
*Line [ 319 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.03>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 323 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.03> = WS.TEMP
*Line [ 325 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.03> = WS.COUNT
    END

    IF WS.AMT GT 500000 AND WS.AMT LE 1000000 THEN
*Line [ 330 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.04>
*Line [ 332 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.04>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 336 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.04> = WS.TEMP
*Line [ 338 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.04> = WS.COUNT
    END

    IF WS.AMT GT 1000000 AND WS.AMT LE 10000000 THEN
*Line [ 343 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.05>
*Line [ 345 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.05>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 349 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.05> = WS.TEMP
*Line [ 351 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.05> = WS.COUNT
    END

    IF WS.AMT GT 10000000 AND WS.AMT LE 25000000 THEN
*Line [ 356 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.06>
*Line [ 358 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.06>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 362 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.06> = WS.TEMP
*Line [ 364 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.06> = WS.COUNT
    END

    IF WS.AMT GT 25000000 AND WS.AMT LE 50000000 THEN
*Line [ 369 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.07>
*Line [ 371 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.07>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 375 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.07> = WS.TEMP
*Line [ 377 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.07> = WS.COUNT
    END

    IF WS.AMT GT 50000000 THEN
*Line [ 382 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.TEMP = R.STAT<CBE.SM.COLUMN.08>
*Line [ 384 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        WS.COUNT = R.STAT<CBE.SM.COUNT.08>
        WS.TEMP = WS.TEMP + WS.AMT
        WS.COUNT = WS.COUNT + 1
*Line [ 388 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.08> = WS.TEMP
*Line [ 390 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.08> = WS.COUNT
    END
    CALL F.WRITE(FN.STAT,WS.STAT.ID,R.STAT)
    CALL  JOURNAL.UPDATE(WS.STAT.ID)
RETURN
*----------------------------------------------------
A.350.NEW:
    IF WS.AMT GE 0 AND WS.AMT LE 50000 THEN
*Line [ 399 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.01> = WS.AMT
*Line [ 401 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.01> = 1
    END

    IF WS.AMT GT 50000 AND WS.AMT LE 100000 THEN
*Line [ 406 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.02> = WS.AMT
*Line [ 408 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.02> = 1
    END

    IF WS.AMT GT 100000 AND WS.AMT LE 500000 THEN
*Line [ 413 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.03>  = WS.AMT
*Line [ 415 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.03> = 1
    END

    IF WS.AMT GT 500000 AND WS.AMT LE 1000000 THEN
*Line [ 420 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.04>  = WS.AMT
*Line [ 422 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.04> = 1
    END

    IF WS.AMT GT 1000000 AND WS.AMT LE 10000000 THEN
*Line [ 427 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.05>  = WS.AMT
*Line [ 429 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.05> = 1
    END

    IF WS.AMT GT 10000000 AND WS.AMT LE 25000000 THEN
*Line [ 434 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.06>  = WS.AMT
*Line [ 436 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.06> = 1
    END

    IF WS.AMT GT 25000000 AND WS.AMT LE 50000000 THEN
*Line [ 441 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.07> = WS.AMT
*Line [ 443 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.07> = 1
    END

    IF WS.AMT GT 50000000 THEN
*Line [ 448 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COLUMN.08> = WS.AMT
*Line [ 450 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
        R.STAT<CBE.SM.COUNT.08>  = 1
    END

*Line [ 454 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    R.STAT<CBE.SM.RESERVED02> =  WS.CO.CODE
*Line [ 456 ] Add SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    R.STAT<CBE.SM.RESERVED03> =  WS.KEY2
    CALL F.WRITE(FN.STAT,WS.STAT.ID,R.STAT)
    CALL  JOURNAL.UPDATE(WS.STAT.ID)
RETURN
*----------------------------------------------------
END
