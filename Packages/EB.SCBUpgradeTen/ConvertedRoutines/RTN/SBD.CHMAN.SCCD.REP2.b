* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
***************************MAHMOUD 7/9/2014******************************
*-----------------------------------------------------------------------------
* <Rating>454</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.CHMAN.SCCD.REP2

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    SEL.AC10 = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ AC10"
    CALL EB.READLIST(SEL.AC10,FT.LIST,'',SELECTED.AC10,ERR.10)
    IF SELECTED.AC10 THEN
        RETURN
    END

    CURR.TIME = FMT(TIMEDATE(),'#####')
    HOUR.TIME = FIELD(CURR.TIME,":",1)
    MINT.TIME = FIELD(CURR.TIME,":",2)
    HOUR.MINUTE = HOUR.TIME:MINT.TIME
    IF HOUR.MINUTE LE '0830' OR HOUR.MINUTE GE '2330' THEN
        RETURN
    END
    GOSUB INITIATE
*Line [ 57 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 58 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
*========================================
INITIATE:
*-------
    KK = 0
    K2 = 0
***********************
    DYNO = 0
    TDD = TODAY
    TD1 = TODAY
    CRT TD1
***********************
    ARR.DATA = ''
    REC.QTY = 0
    REC.AMT = 0
    RETAIL.COUNT = 0
    RETAIL.AMOUNT = 0
    CORPORATE.COUNT = 0
    CORPORATE.AMOUNT = 0
    TOTAL.COUNT  = 0
    TOTAL.AMOUNT = 0
    COUNT.FORM = 0
    KEY.LIST   = '' ; SELECTED  = '' ; ERR.1 = ''
    KEY.LIST2  = '' ; SELECTED2 = '' ; ERR.2 = ''

    SCCD.GRP   = ' 21101 21102 21103 '

    RETURN
******************************************************
CLEAR.VAR:
*----------

    RETURN
*=======================================
CALLDB:
*-------
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = ''
    CALL OPF(FN.LD,F.LD)
    FN.CU = "FBNK.CUSTOMER" ; F.CU = "" ; R.CU = "" ; ER.CU = ""
    CALL OPF(FN.CU,F.CU)
    RETURN
*========================================
PROCESS:
*-------
***************************//LD//*****************************************
    SEL.CMD  = "SELECT ":FN.LD:" WITH CATEGORY IN(":SCCD.GRP:") AND STATUS EQ 'CUR' "
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.2)
    CRT SELECTED
    LOOP
        REMOVE LD.ID FROM KEY.LIST SETTING POSS2
    WHILE LD.ID:POSS2
        K2++
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.12)
        REC.COM  = R.LD<LD.CO.CODE>
        REC.CUS  = R.LD<LD.CUSTOMER.ID>
        REC.CAT  = R.LD<LD.CATEGORY>
        REC.CUR  = R.LD<LD.CURRENCY>
        REC.AMT  = R.LD<LD.AMOUNT>
        REC.VAL  = R.LD<LD.VALUE.DATE>
        REC.LCL  = R.LD<LD.LOCAL.REF>
        REC.FLG  = REC.LCL<1,LDLR.CHK.FLG>
        REC.QTY  = REC.LCL<1,LDLR.CD.QUANTITY>
        IF REC.QTY EQ '' THEN REC.QTY = 1
        REC.TYP  = REC.LCL<1,LDLR.CD.TYPE>
*Line [ 124 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR "." IN REC.TYP SETTING POS.TYP THEN REC.TYP = FIELD(REC.TYP,'.',1) ELSE NULL
        IF REC.TYP EQ "EGP-1000-60M-SUEZ" THEN
            REC.TYP = "EGP-1000-3M-60M-SUEZ"
        END
        CALL F.READ(FN.CU,REC.CUS,R.CU,F.CU,ERR.CU)
        CU.LCL = R.CU<EB.CUS.LOCAL.REF>
        REC.SEC = CU.LCL<1,CULR.NEW.SECTOR>
        IF REC.FLG EQ '' OR REC.FLG NE '�����' THEN
            IF REC.SEC EQ '4650' THEN
                REC.FLG = '�����'
            END ELSE
                REC.FLG = '���� ��������'
            END
        END
        REC.BNK = CU.LCL<1,CULR.CU.BANK>
        IF REC.BNK NE '0017' AND REC.BNK NE '' THEN
            REC.BNK = '9000'
        END ELSE
            REC.BNK = '0017'
        END

*===========================================
        IF REC.FLG EQ '�����' THEN
            RETAIL.COUNT++
            RETAIL.AMOUNT += REC.AMT
        END ELSE
            CORPORATE.COUNT++
            CORPORATE.AMOUNT += REC.AMT
        END
        COUNT.FORM++
        TOTAL.COUNT++
        TOTAL.AMOUNT += REC.AMT
*===========================================
    REPEAT
    GOSUB RET.REC
*****************************************************************
FILL.ARR:
*---------
    KK++

    RETURN
*****************************************************************
RET.REC:
********
    REPORT.ID='SBD.CHMAN.SCCD.REP'
    CALL PRINTER.ON(REPORT.ID,'')

    PR.HD  =REPORT.ID
    HEADING PR.HD
    PRINT "01000077656,01000068007"
    PRINT "Until Today at ":FMT(TIMEDATE(),'#####')
    PRINT "CDs.RTL: ":RETAIL.COUNT
    PRINT "RTL.AMT: ":FMT(RETAIL.AMOUNT,"L0,")
    PRINT "CDs.CORP: ":CORPORATE.COUNT
    PRINT "CORP.AMT: ":FMT(CORPORATE.AMOUNT,"L0,")
    PRINT "TOT.CDs: ":TOTAL.COUNT
    PRINT "TOT.AMT: ":FMT(TOTAL.AMOUNT,"L0,")

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*************************************************
END
