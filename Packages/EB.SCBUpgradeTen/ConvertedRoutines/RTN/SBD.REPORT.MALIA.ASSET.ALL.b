* @ValidationCode : MjoxNDA4OTU3ODE6Q3AxMjUyOjE2NDE3Nzg3NzYyODI6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:39:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*---------------------------------------NI7OOOOOOOOOOOOOOOOOO--------------------------------------
*-----------------------------------------------------------------------------
* <Rating>9226</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM SBD.REPORT.MALIA.ASSET.CUR.CO
SUBROUTINE SBD.REPORT.MALIA.ASSET.ALL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FOREX
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 65 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 67 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 69 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 71 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 73 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY

    COMP = ID.COMPANY
    IDDD="EG0010001"
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,IDDD,N.W.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
N.W.DAY=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    HHH = N.W.DAY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
RETURN
*========================================================================
INITIATE:

    OPENSEQ "&SAVEDLISTS&" , "SCB.MALIA.ASSET.ALLL.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.MALIA.ASSET.ALLL.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.MALIA.ASSET.ALLL.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.MALIA.ASSET.ALLL.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.MALIA.ASSET.ALLL.CSV File IN &SAVEDLISTS&'
        END
    END
    DAT.HED = TODAY
    HEAD1   = "SAMPLE ASSET REPORT "
    HEAD.DESC = HEAD1:" ":DAT.HED:",":COMP

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

** HEAD.DESC  = "CASH INFLOW":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*  HEAD.DESC  = "":","
    HEAD.DESC  = "":","
    HEAD.DESC := "INTERBANK":","
    HEAD.DESC := "OVERDRAFT":","
    HEAD.DESC := "CUSTOMER.LOAN":","
    HEAD.DESC := "CORPRATE.LOAN":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DEPT   = 'F.DEPT.ACCT.OFFICER' ; F.DEPT = ''
    FN.AC     = 'FBNK.ACCOUNT' ; F.AC = ''
    FN.CU     = 'FBNK.CUSTOMER' ; F.CU = ''
    FN.CURR   = 'FBNK.CURRENCY' ; F.CU = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE' ; F.IM = ''
    FN.FT     = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    FN.TT     = 'FBNK.TELLER$NAU' ; F.TT = ''
    FN.IN     = 'F.INF.MULTI.TXN' ; F.IN = ''
    FN.CLOSE  = 'FBNK.ACCOUNT.CLOSURE' ; F.CLOSE = ''
    FN.MM     = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    FN.FOR    = 'FBNK.FOREX' ; F.FOR = ''
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LETTER = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER = ''
    FN.DRAW   = 'FBNK.DRAWINGS' ; F.DRAW = ''
    FN.BT     = 'F.SCB.BT.BATCH' ; F.BT = ''
    FN.BR     = 'FBNK.BILL.REGISTER' ; F.BR = ''
    FN.CM     = 'F.COMPANY'  ; F.CM = ''
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''

    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.DEPT,F.DEPT)
    CALL OPF(FN.LETTER,F.LETTER)
    CALL OPF(FN.DRAW,F.DRAW)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.CURR,F.CURR)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.CLOSE,F.CLOSE)
    CALL OPF(FN.MM,F.MM)
    CALL OPF(FN.FOR,F.FOR)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.BT,F.BT)
    CALL OPF(FN.CM,F.CM)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG=""  ; R.DEP = ''
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG=""
    KEY.LIST4="" ; SELECTED4="" ;  ER.MSG=""

*Line [ 181 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD  = TODAY
***TEXT = COMP ; CALL REM
*------------------------------------------------------------------------
    XXX1  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX11 = SPACE(132) ; XXX12 = SPACE(132)
    XXX2  = SPACE(132)  ; XXX5  = SPACE(132)  ; XXX10 = SPACE(132) ; XXX13 = SPACE(132)
    XXX3  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX6  = SPACE(132) ; XXX7  = SPACE(132)
    XXX15 = SPACE(132)  ; XXX16 = SPACE(132)  ; XXX17 = SPACE(132) ; XXX14 = SPACE(132)
    XXX21 = SPACE(132)  ; XXX19 = SPACE(132)  ; XXX18 = SPACE(132) ; XXX20 = SPACE(132)
    XXX6  = SPACE(132)  ; XXX7  = SPACE(132)  ; XXX8  = SPACE(132) ; XXX9 = SPACE(132)

    T.SEL.CUR = "SELECT FBNK.CURRENCY BY @ID"
***   T.SEL.CUR = "SELECT FBNK.CURRENCY WITH @ID EQ EUR "
    CALL EB.READLIST(T.SEL.CUR,KEY.LIST.CUR,"",SELECTED.CUR,ER.MSG)
    FOR KKKK = 1 TO SELECTED.CUR
        CURR.ID = KEY.LIST.CUR<KKKK>

        AC.BAL.1M     = ''
        AC.BAL.6M     = ''
        AC.BAL.1Y     = '' ; XX2.LOAN1 = ''
        AC.BAL.2Y     = '' ; XX3.LOAN1 = ''
        AC.BAL.25Y    = '' ; XX4.LOAN1 = ''
        AC.BAL.50Y    = '' ; XX5.LOAN1 = ''
        AC.BAL.10Y    = '' ; XX6.LOAN1 = ''
        AC.BAL55.510Y = '' ; XX7.LOAN1 = ''
        AC.BAL55.25Y  = '' ; XX8.LOAN1 = ''
        AC.BAL55.1Y   = '' ; XX9.LOAN1 = ''
        AC.BAL55.2Y   = '' ; XX10.LOAN1 = ''
        AC.BAL55.1M   = '' ; XX11.LOAN1 = ''
        AC.BAL55.6M   = '' ; XX12.LOAN1 = ''
        AC.BAL33.MM   = '' ; XX13.LOAN1 = ''
        AC.BAL11.MM   = '' ; XX14.LOAN1 = ''
        AC.BAL33      = '' ; AC.BALL.MM = ''
        AC.BAL181.2Y  = '' ; XX1 = '' ; XX1.OVER = '' ; XX1.LOAN1 = '' ; XX1.LOAN2 = ''
        AC.BAL101.2Y  = '' ; XX2 = '' ; XX2.OVER = '' ; XX2.LOAN1 = '' ; XX2.LOAN2 = ''
        AC.BAL251.2Y  = '' ; XX3 = '' ; XX3.OVER = '' ; XX3.LOAN1 = '' ; XX3.LOAN2 = ''
        AC.BAL41.2Y   = '' ; XX4 = '' ; XX4.OVER = '' ; XX4.LOAN1 = '' ; XX4.LOAN2 = ''
        AC.BAL41.1Y   = '' ; XX5 = '' ; XX5.OVER = '' ; XX5.LOAN1 = '' ; XX5.LOAN2 = ''
        AC.BAL41.1M   = '' ; XX6 = '' ; XX6.OVER = '' ; XX6.LOAN1 = '' ; XX6.LOAN2 = ''
        AC.BAL41.6M   = '' ; XX7 = '' ; XX7.OVER = '' ; XX7.LOAN1 = '' ; XX7.LOAN2 = ''
        AC.BAL1       = '' ; XX8 = '' ; XX8.OVER = '' ; XX8.LOAN1 = '' ; XX8.LOAN2 = ''
        AC.BAL1.MM    = ''
        AC.BAL1.OVER  = ''
        AC.BAL1.LOAN1 = ''
        AC.BAL1.LOAN2 = ''
        AC.BAL2       = ''
        AC.BAL22.MM   = ''
        AC.BAL2.MM    = ''
        AC.BAL2.OVER  = ''
        AC.BAL2.LOAN1 = ''
        AC.BAL2.LOAN2 = ''
        AC.BAL3       = ''
        AC.BAL3.MM    = ''
        AC.BAL3.OVER  = ''
        AC.BAL3.LOAN1 = ''
        AC.BAL3.LOAN2 = ''
        AC.BAL4       = ''
        AC.BAL4.MM    = ''
        AC.BAL4.OVER  = ''
        AC.BAL4.LOAN1 = ''
        AC.BAL4.LOAN2 = ''
        AC.BAL5       = ''
        AC.BAL5.MM    = ''
        AC.BAL5.OVER  = ''
        AC.BAL5.LOAN1 = ''
        AC.BAL5.LOAN2 = ''
        AC.BAL6       = ''
        AC.BAL6.MM    = ''
        AC.BAL6.OVER  = ''
        AC.BAL6.LOAN1 = ''
        AC.BAL6.LOAN2= ''
        AC.BAL7      = '' ; AC.BAL.OVER = '' ; AC.BAL3.OVER = ''

        AC.BAL7.MM   = ''
        AC.BAL7.OVER = '' ; BB.DATA = ''
        AC.BAL7.LOAN1= ''
        AC.BAL7.LOAN2= ''
        AC.BAL8      = ''
        AC.BAL8.MM   = ''
        AC.BAL8.OVER = ''
        AC.BAL8.LOAN1= ''
        AC.BAL8.LOAN2= ''
        XX1.LOAN2 = ''
        XX2.LOAN2 = ''
        XX3.LOAN2 = ''
        XX4.LOAN2 = ''
        XX5.LOAN2 = ''
        XX6.LOAN2 = ''
        XX7.LOAN2 = ''
        XX8.LOAN2 = ''
        AC.BAL2      = ''
        XX2.OVER     = ''
        XX3.OVER     = ''
        XX4.OVER     = ''
        XX5.OVER     = ''
        XX6.OVER     = ''
        XX7.OVER     = ''
        XX8.OVER     = ''
        YDAYS        = ''
        DAYS         = ''
        AC.BAL11 = '' ; AC.BAL12 = ''  ; AC.BAL22 = '' ; AC.BAL44 = '' ; AC.BAL55 = '' ;
        AC.BALL = ''  ; AC.BAL66 = ''  ; AC.BAL77 = '' ; AC.BAL88 = ''
*=================BEGINING INTER BANK===================================================
        T.SEL= "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 2000 5000 5021 2001 5001 5020) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            CUS.LIM = R.AC<AC.LIMIT.REF>
            CUS.ID  = R.AC<AC.CUSTOMER>
            AC.BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                LMT.REF1    = FIELD(CUS.LIM,'.',1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    XX = CUS.ID:'.':'0000':CUS.LIM
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    XX = CUS.ID:'.':'000':CUS.LIM
                END
*TEXT = "XX: ":XX ; CALL REM
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD  = TODAY
                YDAYS = ''
                DAYS = 'C'
                IF AC.EXP GE AC.TOD THEN
                    CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                END
                AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                IF DAYS GE 1 AND DAYS LE 30 THEN

                    AC.BAL11 += R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL11 LT 0 THEN
                        AC.BAL1 = AC.BAL11 * (-1)
                    END

                END
****************6 MONTHS********************
                IF DAYS GT 30 AND DAYS LE 180 THEN
                    AC.BAL22 +=   R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL22 LT 0 THEN
                        AC.BAL2 = AC.BAL22 * (-1)
                    END
                END
**************UP 1 YEAR*********************
                IF DAYS GT 180 AND DAYS LE 365 THEN
                    AC.BAL44 += R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL44 LT 0 THEN
                        AC.BAL4 = AC.BAL44 * (-1)
                    END
                END
************UP 2 YEAR**************
                IF  DAYS GT 365 AND DAYS LE 730 THEN
                    AC.BAL55 += R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL55 LT 0 THEN
                        AC.BAL5 = AC.BAL55 * (-1)
                    END
                END
*TEXT = "AC.BAL5 ": AC.BAL5
***************FROM 2 TO 5 YEARS***********
                IF DAYS GT 730 AND DAYS LE 1825 THEN
                    AC.BAL66 += R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL66 LT 0 THEN
                        AC.BAL6 = AC.BAL66 * (-1)
                    END
                END
*TEXT = "AC.BAL6 ": AC.BAL6
****************FROM 5 TO 10 YEARS**************
                IF DAYS GT 1825 AND DAYS LE 3650 THEN
                    AC.BAL77 += R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL77 LT 0 THEN
                        AC.BAL7 = AC.BAL77 * (-1)
                    END
                END
*TEXT = "AC.BAL7 ": AC.BAL7
***************OVER 10 YEARS********************
                IF DAYS GT 3650 THEN
                    AC.BAL88 += R.AC<AC.OPEN.ACTUAL.BAL>
                    IF AC.BAL88 LT 0 THEN
                        AC.BAL8 = AC.BAL88 * (-1)
                    END
                END
*TEXT = "AC.BAL8 " : AC.BAL8 ; CALL REM
            END
***************************ON*************************
            IF  (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
                AC.BALL = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                AC.BAL33 += AC.BALL
                XX1.33 = FMT(AC.BAL33,"R2")
            END
        NEXT I
***************************************************
* T.SEL2 = "SELECT FBNK.MM.MONEY.MARKET WITH CATEGORY IN (21075 21076) AND CURRENCY EQ USD"
        T.SEL2  = "SELECT FBNK.MM.MONEY.MARKET WITH CATEGORY IN ( 21075 21076 ) AND MATURITY.DATE GE ":HHH:" AND CURRENCY EQ ":CURR.ID:" AND ( STATUS NE LIQ OR VALUE.DATE EQ ":HHH:") "
* T.SEL2 = "SELECT FBNK.MM.MONEY.MARKET WITH @ID IN ( MM1108100009 MM1019600004 )"
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
* TEXT = "SELECTED " : SELECTED2 ; CALL REM
        FOR J = 1 TO SELECTED2
            CALL F.READ(FN.MM,KEY.LIST2<J>,R.MM,F.MM,E2)
            CUS.ID.MM  = R.MM<MM.CUSTOMER.ID>
            MM.EXP     = R.MM<MM.MATURITY.DATE>
            MM.AMT     = R.MM<MM.PRINCIPAL>
*TEXT = "AC.BAL2 : " : AC.BAL2 ; CALL REM
*TEXT = " MM.AMT : " : MM.AMT ; CALL REM
            CALL F.READ(FN.CU,CUS.ID.MM,R.CU,F.CU,E2)
            SEC     = R.CU<EB.CUS.SECTOR>
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            AC.TOD = TODAY
            DAYS.MM =  'C'
* IF MM.EXP GE AC.TOD THEN
            CALL CDD ("",AC.TOD,MM.EXP,DAYS.MM)
* END
******************1 MONTHS*******************
            IF DAYS.MM GE 0 AND DAYS.MM LE 30 THEN
                AC.BAL11.MM += R.MM<MM.PRINCIPAL>
                IF  AC.BAL11.MM LT 0 THEN
                    AC.BAL1.MM = AC.BAL11.MM * (-1)
                END ELSE
                    AC.BAL1.MM = AC.BAL11.MM
                END
            END
            XX2 = AC.BAL1 + AC.BAL1.MM
            IF XX2 = '' THEN
                XX2 = 0
            END
****************6 MONTHS********************
            IF DAYS.MM GT 30 AND DAYS.MM LE 180 THEN
                AC.BAL22.MM += R.MM<MM.PRINCIPAL>
                IF AC.BAL22.MM LT 0 THEN
                    AC.BAL2.MM  = AC.BAL22.MM *(-1)
                END
            END
            XX3 = AC.BAL2 + AC.BAL2.MM
            IF XX3 = '' THEN
                XX3 = 0
            END
**************UP 1 YEAR*********************
            IF DAYS.MM GT 180 AND DAYS.MM LE 365 THEN
                AC.BAL44.MM += R.MM<MM.PRINCIPAL>
                IF  AC.BAL44.MM LT 0 THEN
                    AC.BAL4.MM = AC.BAL44.MM * (-1)
                END
            END
            XX4 = AC.BAL4 + AC.BAL4.MM
            IF XX4 = '' THEN
                XX4 = 0
            END
************UP 2 YEAR**************
            IF  DAYS.MM GT 365 AND DAYS.MM LE 730 THEN
                AC.BAL55.MM += R.MM<MM.PRINCIPAL>
                IF  AC.BAL55.MM LT 0 THEN
                    AC.BAL5.MM = AC.BAL55.MM * (-1)
                END

            END
            XX5 = AC.BAL5 + AC.BAL5.MM
            IF XX5 = '' THEN
                XX5 = 0
            END

***************FROM 2 TO 5 YEARS***********
            IF DAYS.MM GT 730 AND DAYS.MM LE 1825 THEN
                AC.BAL66.MM += R.MM<MM.PRINCIPAL>
                IF  AC.BAL66.MM LT 0 THEN
                    AC.BAL6.MM = AC.BAL66.MM * (-1)
                END

            END
            XX6 = AC.BAL6 + AC.BAL6.MM
            IF XX6 = '' THEN
                XX6 = 0
            END

****************FROM 5 TO 10 YEARS**************
            IF DAYS.MM GT 1825 AND DAYS.MM LE 3650 THEN
                AC.BAL77.MM += R.MM<MM.PRINCIPAL>
                IF  AC.BAL77.MM LT 0 THEN
                    AC.BAL7.MM = AC.BAL77.MM * (-1)
                END

            END
            XX7 = AC.BAL7   +  AC.BAL7.MM
            IF XX7 = '' THEN
                XX7 = 0
            END

***************OVER 10 YEARS********************
            IF DAYS.MM GT 3650 THEN
                AC.BAL88.MM += R.MM<MM.PRINCIPAL>
                IF  AC.BAL88.MM LT 0 THEN
                    AC.BAL8.MM = AC.BAL88.MM * (-1)
                END

            END
            XX8 = AC.BAL8 + AC.BAL8.MM

            IF XX8 = '' THEN
                XX8 = 0
            END
***************************ON*************************
            IF  MM.EXP EQ ''  THEN
                AC.BALL.MM = R.MM<MM.PRINCIPAL>
                IF  AC.BALL.MM LT 0 THEN
                    AC.BAL3.MM = AC.BAL33.MM * (-1)
                END
                AC.BAL33.MM += AC.BAL3.MM
            END
            XX1 =   AC.BAL33.MM + XX1.33
            IF XX1 = '' THEN
                XX1 = 0
            END
        NEXT J


**********************************************************
*==============================END INTER BANK=======================
*==============================BEGINING OVERDRAFT=====================
        T.SEL3 = "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1001 1002 1003 1059 1205 1207 1216 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1582 1407 1413 1408  6501 6502 6503 6504 6511 1558 1595 1223 1225 1221 1224 1222 1227 1220) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
**  T.SEL3 = "SELECT FBNK.ACCOUNT WITH @ID EQ 0130508210100101"
        CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG)
*TEXT = "SEL : ": SELECTED3 ; CALL REM
        FOR K = 1 TO SELECTED3
            CALL F.READ(FN.AC,KEY.LIST3<K>,R.AC,F.AC,ERR)
**TEXT = "KEY.LIST3 : " : KEY.LIST3<K> ; CALL REM
            CUS.ID3 = R.AC<AC.CUSTOMER>
            CUS.LIM3= R.AC<AC.LIMIT.REF>
**TEXT = "CUS.LIM3 : " : CUS.LIM3 ; CALL REM
            IF( CUS.LIM3 NE '' AND CUS.LIM3 NE '0' AND CUS.LIM3 NE 'NOSTRO' ) THEN
                LMT.REF3    = FIELD(CUS.LIM3,'.',1)
                IF LEN(LMT.REF3) EQ 3 THEN
                    XY = CUS.ID3:'.':'0000':CUS.LIM3
                END
                IF LEN(LMT.REF3) EQ 4 THEN
                    XY = CUS.ID3:'.':'000':CUS.LIM3
                END
**TEXT = "XY: ":XY ; CALL REM
                CALL F.READ(FN.LIM,XY,R.LIM,F.LIM,E2)
                AC.EXP3  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD3  = TODAY
                YDAYS    = ''
                DAYS.33 = 'C'
                IF AC.EXP3 GT AC.TOD3 THEN
                    CALL CDD ("",AC.TOD3,AC.EXP3,DAYS.33)
                END
                AC.BAL.OVER = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                IF DAYS.33 GE 1 AND DAYS.33 LE 30 THEN
                    AC.BAL.1M  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL1.OVER += AC.BAL.1M
                END

                XX2.OVER = AC.BAL1.OVER
                IF XX2.OVER = '' THEN
                    XX2.OVER = 0
                END
****************6 MONTHS********************
                IF DAYS.33 GT 30 AND DAYS.33 LE 180 THEN
                    AC.BAL.6M  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL2.OVER +=   AC.BAL.6M
                END
                XX3.OVER = AC.BAL2.OVER
                IF XX3.OVER = '' THEN
                    XX3.OVER = 0
                END
**************UP 1 YEAR*********************
                IF DAYS.33 GT 180 AND DAYS.33 LE 365 THEN
                    AC.BAL.1Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL4.OVER += AC.BAL.1Y
                END
                XX4.OVER = AC.BAL4.OVER
                IF XX4.OVER = '' THEN
                    XX4.OVER = 0
                END

************UP 2 YEAR**************
                IF  DAYS.33 GT 365 AND DAYS.33 LE 730 THEN
                    AC.BAL.2Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL5.OVER += AC.BAL.2Y
                END
                XX5.OVER = AC.BAL5.OVER
                IF XX5.OVER = '' THEN
                    XX5.OVER = 0
                END

***************FROM 2 TO 5 YEARS***********
                IF DAYS.33 GT 730 AND DAYS.33 LE 1825 THEN
                    AC.BAL.25Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL6.OVER += AC.BAL.25Y
                END
                XX6.OVER = AC.BAL6.OVER
                IF XX6.OVER = '' THEN
                    XX6.OVER = 0
                END

****************FROM 5 TO 10 YEARS**************
                IF DAYS.33 GT 1825 AND DAYS.33 LE 3650 THEN
                    AC.BAL.50Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL7.OVER += AC.BAL.50Y
                END
                XX7.OVER = AC.BAL7.OVER
                IF XX7.OVER = '' THEN
                    XX7.OVER = 0
                END

***************OVER 10 YEARS********************
                IF DAYS.33 GT 3650 THEN
                    AC.BAL.10Y = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL8.OVER += AC.BAL.10Y
                END
                XX8.OVER = AC.BAL8.OVER
                IF XX8.OVER = '' THEN
                    XX8.OVER = 0
                END
            END
***************************ON*************************
            IF  (CUS.LIM3 EQ '' OR CUS.LIM3 EQ 'NOSTRO') THEN
                AC.BAL.OVER = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                AC.BAL3.OVER += AC.BAL.OVER
            END
            XX1.OVER = FMT(AC.BAL3.OVER,"R2")
            IF XX1.OVER = '' THEN
                XX1.OVER = 0
            END
        NEXT K
*==============================END OVERDRAFT==========================
*=============================BEGINING CUSTOMER.LOANS=================
        T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1102 1404 1414 1206 1208 1401 1402 1405 1406 11240 11238 11232 11234 11239 11242 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
** T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN (1404)"
        CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ERR.MSG)
*TEXT = "SEL4 : " : SELECTED4 ; CALL REM
        FOR M = 1 TO SELECTED4
            CALL F.READ(FN.AC,KEY.LIST4<M>,R.AC,F.AC,ERR)
            CUS.ID4 = R.AC<AC.CUSTOMER>
            CUS.LIM4= R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID4,R.CU,F.CU,ERR)
            SEC.NEW=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF SEC.NEW EQ 4650 THEN
                IF( CUS.LIM4 NE '' AND CUS.LIM4 NE '0' AND CUS.LIM4 NE 'NOSTRO' ) THEN
                    LMT.REF4    = FIELD(CUS.LIM4,'.',1)
                    IF LEN(LMT.REF4) EQ 3 THEN
                        XY4 = CUS.ID4:'.':'0000':CUS.LIM4
                    END
                    IF LEN(LMT.REF4) EQ 4 THEN
                        XY4 = CUS.ID4:'.':'000':CUS.LIM4
                    END
*TEXT = "XY: ":XY ; CALL REM
                    CALL F.READ(FN.LIM,XY4,R.LIM,F.LIM,E2)
                    AC.EXP4  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD4  = TODAY
                    DAYS.44 = 'C'
                    IF AC.EXP4 GE AC.TOD4 THEN
                        CALL CDD ("",AC.TOD4,AC.EXP4,DAYS.44)
                    END
                    AC.BAL.LOAN1 = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                    IF DAYS.44 GE 1 AND DAYS.44 LE 30 THEN
                        AC.BAL41.1M  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL1.LOAN1 += AC.BAL41.1M
                    END

                    XX2.LOAN1 = AC.BAL1.LOAN1
                    IF XX2.LOAN1 = '' THEN
                        XX2.LOAN1 = 0
                    END

****************6 MONTHS********************
                    IF DAYS.44 GT 30 AND DAYS.44 LE 180 THEN
                        AC.BAL41.6M  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL2.LOAN1 +=   AC.BAL41.6M
                    END
                    XX3.LOAN1 = AC.BAL2.LOAN1
                    IF XX3.LOAN1 = '' THEN
                        XX3.LOAN1 = 0
                    END
**************UP 1 YEAR*********************
                    IF DAYS.44 GT 180 AND DAYS.44 LE 365 THEN
                        AC.BAL41.1Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL4.LOAN1 += AC.BAL41.1Y
                    END
                    XX4.LOAN1 = AC.BAL4.LOAN1
                    IF XX4.LOAN1 = '' THEN
                        XX4.LOAN1 = 0
                    END
************UP 2 YEAR**************
                    IF  DAYS.44 GT 365 AND DAYS.44 LE 730 THEN
                        AC.BAL41.2Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL5.LOAN1 += AC.BAL41.1Y
                    END
                    XX5.LOAN1 = AC.BAL5.LOAN1
                    IF XX5.LOAN1 = '' THEN
                        XX5.LOAN1 = 0
                    END
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.44 GT 730 AND DAYS.44 LE 1825 THEN
                        AC.BAL251.2Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL6.LOAN1 += AC.BAL251.2Y
                    END
                    XX6.LOAN1 = AC.BAL6.LOAN1
                    IF XX6.LOAN1 = '' THEN
                        XX6.LOAN1 = 0
                    END
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.44 GT 1825 AND DAYS.44 LE 3650 THEN
                        AC.BAL101.2Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL7.LOAN1 += AC.BAL101.2Y
                    END
                    XX7.LOAN1 = AC.BAL7.LOAN1
                    IF XX7.LOAN1 = '' THEN
                        XX7.LOAN1 = 0
                    END
***************OVER 10 YEARS********************
                    IF DAYS.44 GT 3650 THEN
                        AC.BAL181.2Y  = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL8.LOAN1 += AC.BAL181.2Y
                    END
                    XX8.LOAN1 = AC.BAL8.LOAN1
                    IF XX8.LOAN1 = '' THEN
                        XX8.LOAN1 = 0
                    END
                END
***************************ON*************************
                IF  (CUS.LIM4 EQ '' OR CUS.LIM4 EQ 'NOSTRO') THEN
                    AC.BAL.LOAN1 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL3.LOAN1 += AC.BAL.LOAN1
                END
**TEXT = "XX1.LOAN1 : " : AC.BAL3.LOAN1 ; CALL REM
                XX1.LOAN1 = AC.BAL3.LOAN1
                IF AC.BAL3.LOAN1 EQ '' THEN
                    AC.BAL3.LOAN1 = 0
                END
            END
        NEXT M
*=======================END CUSTOMER LOAN==============================
*=======================BEGINING CORPRATE LOANS=======================
        T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 1102 1404 1414 1206 1208 1401 1402 1405 1406 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
* T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN (1404)"
        CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ERR.MSG)
*TEXT = "SEL6 : " : SELECTED6 ; CALL REM
        FOR NN = 1 TO SELECTED6
            CALL F.READ(FN.AC,KEY.LIST6<NN>,R.AC,F.AC,ERR)
            CUS.ID6 = R.AC<AC.CUSTOMER>
            CUS.LIM6= R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID6,R.CU,F.CU,ERR)
            SEC.NEW=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF SEC.NEW NE 4650 AND SEC.NEW NE '' THEN
                IF( CUS.LIM6 NE '' AND CUS.LIM6 NE '0' AND CUS.LIM6 NE 'NOSTRO' ) THEN
                    LMT.REF6    = FIELD(CUS.LIM6,'.',1)
                    IF LEN(LMT.REF6) EQ 3 THEN
                        XY5 = CUS.ID6:'.':'0000':CUS.LIM6
                    END
                    IF LEN(LMT.REF6) EQ 4 THEN
                        XY5 = CUS.ID6:'.':'000':CUS.LIM6
                    END
*TEXT = "XY: ":XY ; CALL REM
                    CALL F.READ(FN.LIM,XY5,R.LIM,F.LIM,E2)
                    AC.EXP5  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD5  = TODAY
                    DAYS.55= 'C'
                    IF AC.EXP5 GE AC.TOD5 THEN
                        CALL CDD ("",AC.TOD5,AC.EXP5,DAYS.55)
                    END
                    AC.BAL.LOAN2 = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 MONTHS*******************
                    IF DAYS.55 GE 1 AND DAYS.55 LE 30 THEN
                        AC.BAL55.1M = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL1.LOAN2 += AC.BAL55.1M
                    END

                    XX2.LOAN2 = AC.BAL1.LOAN2
                    IF XX2.LOAN2 = '' THEN
                        XX2.LOAN2 = 0
                    END
****************6 MONTHS********************
                    IF DAYS.55 GT 30 AND DAYS.55 LE 180 THEN
                        AC.BAL55.6M = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL2.LOAN2 +=  AC.BAL55.6M
                    END
                    XX3.LOAN2 = AC.BAL2.LOAN2
                    IF XX3.LOAN2 = '' THEN
                        XX3.LOAN2 = 0
                    END

**************UP 1  YEAR*********************
                    IF DAYS.55 GT 180 AND DAYS.55 LE 365 THEN
                        AC.BAL55.1Y =      R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL4.LOAN2 += AC.BAL55.1Y
                    END
                    XX4.LOAN2 = AC.BAL4.LOAN2
                    IF XX4.LOAN2 = '' THEN
                        XX4.LOAN2 = 0
                    END
************UP 2 YEAR**************
                    IF  DAYS.55 GT 365 AND DAYS.55 LE 730 THEN
                        AC.BAL55.2Y =      R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL5.LOAN2 += AC.BAL55.2Y
                    END
                    XX5.LOAN2 = AC.BAL5.LOAN2
                    IF XX5.LOAN2 = '' THEN
                        XX5.LOAN2 = 0
                    END
***************FROM 2 TO 5 YEARS***********
                    IF DAYS.55 GT 730 AND DAYS.55 LE 1825 THEN
                        AC.BAL55.25Y = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL6.LOAN2 += AC.BAL55.25Y
                    END
                    XX6.LOAN2 = AC.BAL6.LOAN2
                    IF XX6.LOAN2 = '' THEN
                        XX6.LOAN2 = 0
                    END
****************FROM 5 TO 10 YEARS**************
                    IF DAYS.55 GT 1825 AND DAYS.55 LE 3650 THEN
                        AC.BAL55.510Y = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL7.LOAN2 += AC.BAL55.510Y
                    END
                    XX7.LOAN2 = AC.BAL7.LOAN2
                    IF XX7.LOAN2 = '' THEN
                        XX7.LOAN2 = 0
                    END
***************OVER 10 YEARS********************
                    IF DAYS.55 GT 3650 THEN
                        AC.BAL55.10Y = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL8.LOAN2 += AC.BAL55.10Y
                    END
                    XX8.LOAN2 = AC.BAL8.LOAN2
                    IF XX8.LOAN2 = '' THEN
                        XX8.LOAN2 = 0
                    END
                END
***************************ON*************************
                IF  (CUS.LIM6 EQ '' OR CUS.LIM6 EQ 'NOSTRO') THEN
                    AC.BAL.LOAN2 = R.AC<AC.OPEN.ACTUAL.BAL>  * (-1)
                    AC.BAL3.LOAN2 += AC.BAL.LOAN2
                END
                XX1.LOAN2 = AC.BAL3.LOAN2
                IF XX1.LOAN2 = '' THEN
                    XX1.LOAN2 = 0
                END
            END
        NEXT NN
*=============================END CORPRATE LOANS====================
        BB.DATA  = CURR.ID:",":COMP
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*****************************ON***********
        BB.DATA  = "ON" : ","
        BB.DATA := XX1:","
        BB.DATA := XX1.OVER :","
        BB.DATA := XX1.LOAN1:","
        BB.DATA := XX1.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************ON*******************
**************************1M ************
        BB.DATA  = "1M" : ","
        BB.DATA := XX2:","
        BB.DATA := XX2.OVER:","
        BB.DATA := XX2.LOAN1:","
        BB.DATA := XX2.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 M*****************
************************6M*******************
        BB.DATA  = "6M" : ","
        BB.DATA := XX3:","
        BB.DATA := XX3.OVER:","
        BB.DATA := XX3.LOAN1:","
        BB.DATA := XX3.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**********************6M************************
*********************1 YEAR********************
        BB.DATA  = "1 YEAR" : ","
        BB.DATA := XX4:","
        BB.DATA := XX4.OVER:","
        BB.DATA := XX4.LOAN1:","
        BB.DATA := XX4.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************1 YEAR*******************
***************************2 YEAR*************
        BB.DATA  = "2 YEAR" : ","
        BB.DATA := XX5:","
        BB.DATA :=  XX5.OVER:","
        BB.DATA :=  XX5.LOAN1:","
        BB.DATA :=  XX5.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************2 YEAR********************
*************************2 - 5 YEAR*************
        BB.DATA  = "2 - 5 YEAR" : ","
        BB.DATA := XX6:","
        BB.DATA :=  XX6.OVER:","
        BB.DATA :=  XX6.LOAN1:","
        BB.DATA :=  XX6.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************2 - 5 YEAR******************
************************10 -5 YEAR*******************8
        BB.DATA  = "5 - 10 YEAR" : ","
        BB.DATA := XX7:","
        BB.DATA :=  XX7.OVER:","
        BB.DATA :=  XX7.LOAN1:","
        BB.DATA :=  XX7.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************5 TO 10 YEAR***************
*************************OVER 10********************
        BB.DATA  = "OVER 10" : ","
        BB.DATA := XX8:","
        BB.DATA :=  XX8.OVER:","
        BB.DATA :=  XX8.LOAN1:","
        BB.DATA :=  XX8.LOAN2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************10 OVER*******************
    NEXT KKKK
*===============================================================
RETURN
END
