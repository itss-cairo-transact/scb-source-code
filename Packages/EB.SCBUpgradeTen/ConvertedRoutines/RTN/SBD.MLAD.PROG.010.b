* @ValidationCode : MjoxMzQ0MDYyMTI2OkNwMTI1MjoxNjQxNzc4NDU0MDg0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:34:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>2095</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.MLAD.PROG.010
***    PROGRAM SBD.MLAD.PROG.010
***                            ����� / ���� �����
***                            �������� ����� �� ��� ������
*   ------------------------------
    $INSERT    I_COMMON
    $INSERT    I_EQUATE
    $INSERT   I_F.USER
    $INSERT    I_F.CUSTOMER.ACCOUNT
    $INSERT    I_F.DEPT.ACCT.OFFICER
    $INSERT    I_F.CUSTOMER
    $INSERT    I_F.ACCOUNT
    $INSERT    I_F.LIMIT

    $INSERT    I_F.CATEGORY
    $INSERT    I_F.CURRENCY
    $INSERT    I_F.RE.STAT.LINE.CONT
*    $INSERT    I_F.RE.CONSOL.LOAN
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
****    $INCLUDE  I_F.LMM.SCHEDULES
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LMM.SCHEDULE.DATES
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FIN.OZON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FIN.SANAD

*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.INSTALL2
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.INSTALLMENT
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LOANS.INST

*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MLAD.FILE.010
    $INSERT   I_CU.LOCAL.REFS
    $INSERT   I_AC.LOCAL.REFS
    $INSERT  I_F.COMPANY
    $INSERT  I_USER.ENV.COMMON
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*-------------------------------------------------------------------------



    GOSUB INIT


    GOSUB PRE.PERIOD.2



    GOSUB OPENFILES

*-------------------------------
**  OPEN FN.MLAD010 TO FILEVAR ELSE ABORT 201, FN.MLAD010
    FILEVAR = ''

    CALL OPF(FN.MLAD010,FILEVAR)
    CLEARFILE FILEVAR
*-------------------------------


    GOSUB READ.LD.FILE

    GOSUB READ.MM.MONEY.MARKET

    GOSUB READ.FIN.OZON

*--------------------------------------------------------------------------------------------
*****                                                      ����� ������������
*****    GOSUB READ.FIN.SANAD
*--------------------------------------------------------------------------------------------

    GOSUB READ.SCB.INSTALL2

    GOSUB READ.SCB.INSTALLMENT

    GOSUB READ.SCB.LOANS.INST

    GOSUB READ.ACCOUNT.FILE


*-------------------------------
*                                                    ��� ������ ������ �������� �� ��� ��������
*                             1. READ.SCB.LOANS.INST
*                             2. LD.LOANS.AND.DEPOSITS  CATEGORY 21050 ... 21074
*                                                       CATEGORY 21081 ... 21095
*                                                ������ ����� ������ ��� �������� �� ������ ���

RETURN
*-------------------------------------------------------------------------
INIT:

    PROGRAM.ID = "SBD.MLAD.PROG.010"

    FN.MLAD010 = "F.SCB.MLAD.FILE.010"
    F.MLAD010  = ""
    R.MLAD010  = ""
    Y.MLAD010.ID  = ""

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC  = ""
    R.CUS.ACC = ""
    Y.CUS.ACC.ID   = ""

    FN.CUSTOMER = "FBNK.CUSTOMER"
    F.CUSTOMER  = ""
    R.CUSTOMER  = ""
    Y.CUST.ID   = ""

    FN.ACC = "FBNK.ACCOUNT"
    F.ACC  = ""
    R.ACCOUNT = ""
    Y.ACC.ID = ""

    FN.LIMIT = "FBNK.LIMIT"
    F.LIMIT  = ""
    R.LIMIT = ""
    Y.LIMIT.ID = ""

    FN.LCONT = "FBNK.RE.STAT.LINE.CONT"
    F.LCONT  = ""
    R.LCONT = ""
    Y.LCONT.ID = ""

    FN.CONS = "FBNK.RE.CONSOL.LOAN"
    F.CONS  = ""
    R.CONS = ""
    Y.CONS.ID = ""

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD  = ""
    R.LD = ""
    Y.LD.ID = ""

    FN.LC = "FBNK.LETTER.OF.CREDIT"
    F.LC  = ""
    R.LC = ""
    Y.LC.ID = ""

    FN.MON = "FBNK.MM.MONEY.MARKET"
    F.MON  = ""
    R.MON = ""
    Y.MON.ID = ""

    FN.LMM = "FBNK.LMM.SCHEDULES"
    F.LMM  = ""
    R.LMM = ""
    Y.LMM.ID = ""

    F.SCH.DATE = '' ; FN.SCH.DATE = 'FBNK.LMM.SCHEDULE.DATES'
    R.SCH.DATE = ''

    FN.OZON = "F.SCB.FIN.OZON"
    F.OZON  = ""
    R.OZON = ""
    Y.OZON.ID = ""


    FN.SANAD = "F.SCB.FIN.SANAD"
    F.SANAD  = ""
    R.SANAD = ""
    Y.SANAD.ID = ""

    FN.ALL2 = "F.SCB.INSTALL2"
    F.ALL2  = ""
    R.ALL2 = ""
    Y.ALL2.ID = ""

    FN.MENT = "F.SCB.INSTALLMENT"
    F.MENT  = ""
    R.MENT = ""
    Y.MENT.ID = ""

    FN.LINST = "F.SCB.LOANS.INST"
    F.LINST  = ""
    R.LINST = ""
    Y.LINST.ID = ""

*---------------------------------------
    SYS.DATE = TODAY

    WRK.DATE = SYS.DATE
    SEL.DATE = SYS.DATE

    CALL CDT('',SEL.DATE,'-1W')
*---------------------------------------

    WS.FLD0060     = 0
    WS.FLD0070     = 0
    WS.FLD0080     = 0
    WS.FLD0090     = 0
    WS.FLD0100     = 0

    DEPOSIT.AMOUNT = ""
    CER.AMOUNT     = ""
    LOANS.AMOUNT   = ""



    OK.FLAG = ""

    AC.GROUP = '1220 1221 1222 1223 1224 1225 1227 1501 1507 1508 1519 1599 1566'

RETURN
*-------------------------------------------------------------------------
PRE.PERIOD:

    DIM STR.PRD(10)
    DIM END.PRD(10)
    DIM PRD.COD(10)
*   ----------------                   NEXT DAY
    END.PRD(1) = WRK.DATE

    CALL CDT('',WRK.DATE,'-1W')
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(1) = WRK.DATE


    PRD.COD(1) = FMT("1010","R%4")
    PRD.COD(1) = 'DAY'
*   ----------------                   NEXT WEEK
    WRK.DATE = END.PRD(1)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(2) = WRK.DATE
    WRK.DATE = STR.PRD(1)
    CALL CDT('',WRK.DATE,'+6C')
    END.PRD(2) = WRK.DATE
    PRD.COD(2) = FMT("1020","R%4")
    PRD.COD(2) = 'WEEK'
*   ----------------                  NEXT MONTH
    WRK.DATE = END.PRD(2)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(3) = WRK.DATE

    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2] + 1
    WRK.DD   = WRK.DATE[7,2]

    IF WRK.MM GT 12 THEN
        WRK.MM = WRK.MM - 12
        WRK.YY = WRK.YY + 1
    END

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(3) = WRK.DATE
    PRD.COD(3) = FMT("1030","R%4")
    PRD.COD(3) = 'MONTH'
*   ----------------                    NEXT  3 MONTHES
    WRK.DATE = END.PRD(3)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(4) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2] + 3
    WRK.DD   = WRK.DATE[7,2]

    IF WRK.MM GT 12 THEN
        WRK.MM = WRK.MM - 12
        WRK.YY = WRK.YY + 1
    END

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(4) = WRK.DATE
    PRD.COD(4) = FMT("1040","R%4")
    PRD.COD(4) = '3MONTH'
*   ----------------                    NEXT 6 MONTHES
    WRK.DATE = END.PRD(4)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(5) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2] + 6
    WRK.DD   = WRK.DATE[7,2]

    IF WRK.MM GT 12 THEN
        WRK.MM = WRK.MM - 12
        WRK.YY = WRK.YY + 1
    END

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(5) = WRK.DATE
    PRD.COD(5) = FMT("1050","R%4")
    PRD.COD(5) = '6MONTH'

*   ----------------                      NEXT 1 YEAR
    WRK.DATE = END.PRD(5)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(6) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4] + 1
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]


    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(6) = WRK.DATE
    PRD.COD(6) = FMT("1060","R%4")
    PRD.COD(6) = 'YEAR'

*   ----------------                      NEXT 3 YEARS
    WRK.DATE = END.PRD(6)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(7) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4] + 3
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]


    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(7) = WRK.DATE
    PRD.COD(7) = FMT("1070","R%4")
    PRD.COD(7) = '3YEAR'
*   ----------------                      MORE THAN 3 YEARS
    WRK.DATE = END.PRD(7)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(8) = WRK.DATE


    WRK.DATE = STR.PRD(1)
***    WRK.YY   = WRK.DATE[1,4] + 999
    WRK.YY   = 9999
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]


    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(8) = WRK.DATE
    PRD.COD(8) = FMT("1080","R%4")
    PRD.COD(8) = 'MORE'



*----------------------------------------------
*    FOR I = 1 TO 8
*        PRINT STR.PRD(I):" ":END.PRD(I):" ":PRD.COD(I)
*    NEXT I


RETURN
*-------------------------------------------------------------------------
PRE.PERIOD.2:


    DIM STR.PRD(10)
    DIM END.PRD(10)
    DIM PRD.COD(10)
*   ----------------                   NEXT DAY
    CALL CDT('',WRK.DATE,'-1W')
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(1) = WRK.DATE

    CALL CDT('',WRK.DATE,'+1W')
***    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(1) = WRK.DATE


    PRD.COD(1) = FMT("1010","R%4")
    PRD.COD(1) = 'DAY'
*   ----------------                   NEXT WEEK
    WRK.DATE = END.PRD(1)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(2) = WRK.DATE
***    WRK.DATE = STR.PRD(1)
    WRK.DATE = END.PRD(1)
    CALL CDT('',WRK.DATE,'+6C')
    END.PRD(2) = WRK.DATE
    PRD.COD(2) = FMT("1020","R%4")
    PRD.COD(2) = 'WEEK'
*   ----------------                  NEXT MONTH
    WRK.DATE = END.PRD(2)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(3) = WRK.DATE

    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2] + 1
    WRK.DD   = WRK.DATE[7,2]

    IF WRK.MM GT 12 THEN
        WRK.MM = WRK.MM - 12
        WRK.YY = WRK.YY + 1
    END

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(3) = WRK.DATE
    PRD.COD(3) = FMT("1030","R%4")
    PRD.COD(3) = 'MONTH'
*   ----------------                    NEXT  3 MONTHES
    WRK.DATE = END.PRD(3)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(4) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2] + 3
    WRK.DD   = WRK.DATE[7,2]

    IF WRK.MM GT 12 THEN
        WRK.MM = WRK.MM - 12
        WRK.YY = WRK.YY + 1
    END

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(4) = WRK.DATE
    PRD.COD(4) = FMT("1040","R%4")
    PRD.COD(4) = '3MONTH'
*   ----------------                    NEXT 6 MONTHES
    WRK.DATE = END.PRD(4)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(5) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2] + 6
    WRK.DD   = WRK.DATE[7,2]

    IF WRK.MM GT 12 THEN
        WRK.MM = WRK.MM - 12
        WRK.YY = WRK.YY + 1
    END

    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(5) = WRK.DATE
    PRD.COD(5) = FMT("1050","R%4")
    PRD.COD(5) = '6MONTH'

*   ----------------                      NEXT 1 YEAR
    WRK.DATE = END.PRD(5)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(6) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4] + 1
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]


    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(6) = WRK.DATE
    PRD.COD(6) = FMT("1060","R%4")
    PRD.COD(6) = 'YEAR'

*   ----------------                      NEXT 3 YEARS
    WRK.DATE = END.PRD(6)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(7) = WRK.DATE


    WRK.DATE = STR.PRD(1)
    WRK.YY   = WRK.DATE[1,4] + 3
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]


    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(7) = WRK.DATE
    PRD.COD(7) = FMT("1070","R%4")
    PRD.COD(7) = '3YEAR'
*   ----------------                      MORE THAN 3 YEARS
    WRK.DATE = END.PRD(7)
    CALL CDT('',WRK.DATE,'+1C')
    STR.PRD(8) = WRK.DATE


    WRK.DATE = STR.PRD(1)
***    WRK.YY   = WRK.DATE[1,4] + 999
    WRK.YY   = 9999
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]


    WRK.MM = FMT(WRK.MM,"R%2")
    WRK.DD = FMT(WRK.DD,"R%2")
    WRK.DATE = WRK.YY:WRK.MM:WRK.DD
    CALL CDT('',WRK.DATE,'-1C')
    END.PRD(8) = WRK.DATE
    PRD.COD(8) = FMT("1080","R%4")
    PRD.COD(8) = 'MORE'



*----------------------------------------------
*    FOR I = 1 TO 8
*        PRINT STR.PRD(I):" ":END.PRD(I):" ":PRD.COD(I)
*    NEXT I
*    STOP
*----------------------------------------------


    T.DEPOSIT.AMT = 0

RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLAD010,F.MLAD010)
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)
    CALL OPF(FN.ACC,F.ACC)
    CALL OPF(FN.LIMIT,F.LIMIT)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LC,F.LC)
    CALL OPF(FN.LCONT,F.LCONT)
    CALL OPF(FN.CONS,F.CONS)
    CALL OPF(FN.MON,F.MON)
    CALL OPF(FN.LMM,F.LMM)
    CALL OPF(FN.OZON,F.OZON)
    CALL OPF(FN.SANAD,F.SANAD)
    CALL OPF(FN.ALL2,F.ALL2)
    CALL OPF(FN.MENT,F.MENT)
    CALL OPF(FN.SCH.DATE,F.SCH.DATE)
RETURN
*--------------------------------------------------------------------------
READ.LD.FILE:


    DAT.ID = 'EG0010001'
    WS.DATE.LPE = ''
*Line [ 592 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,WS.DATE.LPE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
WS.DATE.LPE=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>


    SEL.LD  = "SELECT ":FN.LD:" WITH STATUS NE 'FWD'"
    SEL.LD := " AND (( VALUE.DATE LE ":WS.DATE.LPE:" AND AMOUNT NE 0 )"
    SEL.LD := " OR ( FIN.MAT.DATE GT ":WS.DATE.LPE:" AND AMOUNT EQ 0 ))"


    CALL EB.READLIST(SEL.LD,SEL.LIST.LD,'',NO.OF,ERR.LD)

    LOOP
        REMOVE Y.LD.ID FROM SEL.LIST.LD SETTING POS.LD
    WHILE Y.LD.ID:POS.LD

        CALL F.READ(FN.LD,Y.LD.ID,R.LD,F.LD,ERR.LD)

****************
        WS.CATEG     = R.LD<LD.CATEGORY>
        WS.AMT       = R.LD<LD.AMOUNT>
        WS.REI.AMT   = R.LD<LD.REIMBURSE.AMOUNT>
        WS.CY        = R.LD<LD.CURRENCY>
        WS.MAT.DATE  = R.LD<LD.FIN.MAT.DATE>
        AGREE.DATE   = R.LD<LD.AGREEMENT.DATE>
        WS.RENEW.IND = R.LD<LD.LOCAL.REF,LDLR.RENEW.IND>
        WS.STATUS    = R.LD<LD.STATUS>
        WS.VALUE     = R.LD<LD.VALUE.DATE>
****************

        Y.CUST.ID = R.LD<LD.CUSTOMER.ID>
*Line [ 627 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
        NEW.SECTOR = LOCAL.REF<1,CULR.NEW.SECTOR>
*Line [ 635 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.SECTOR,Y.CUST.ID,O.SECTOR)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
O.SECTOR=R.ITSS.CUSTOMER<EB.CUS.SECTOR>
        OLD.SECTOR = O.SECTOR
****************
        IF WS.AMT NE 0 THEN
            WS.AMOUNT = WS.AMT
        END
* --------------
        IF WS.AMT EQ 0 THEN
            WS.AMOUNT = WS.REI.AMT
        END
*********************

        GOSUB RTN.MOVE.ZEROS

****************
        BEGIN CASE

*                                ��� ������� �/���� ��� ��� �� 7 ������ 2015
*                    ��� ������ �� ������� ������ ����� ��� ��� ����� �������
*                             ������ ������� ��� ����� ������� �� ����� ������
*   ----------------------------------------------
*        CASE WS.CATEG GE 21000 AND WS.CATEG LE 21013
*            IF OLD.SECTOR GE 3000 AND OLD.SECTOR LE 3223 THEN
*                WS.FLD0250 = WS.AMOUNT
*                OK.FLAG = 1
*            END ELSE
*                DEPOSIT.AMOUNT = WS.AMOUNT
*                T.DEPOSIT.AMT += WS.AMOUNT
*                OK.FLAG = 1
*            END
*   ----------------------------------------------
*   ----------------------------------------------
            CASE WS.CATEG GE 21000 AND WS.CATEG LE 21013
                DEPOSIT.AMOUNT = WS.AMOUNT
                T.DEPOSIT.AMT += WS.AMOUNT
                OK.FLAG = 1
*   ----------------------------------------------
            CASE WS.CATEG GE 21017 AND WS.CATEG LE 21029
                CER.AMOUNT = WS.AMOUNT
                OK.FLAG = 1
*   ----------------------------------------------
            CASE WS.CATEG EQ 21032
                CER.AMOUNT = WS.AMOUNT
                OK.FLAG = 1
*   ----------------------------------------------
            CASE WS.CATEG GE 21041 AND WS.CATEG LE 21042
                CER.AMOUNT = WS.AMOUNT
                OK.FLAG = 1
*  _______________________________________________
            CASE WS.CATEG GE 21050 AND WS.CATEG LE 21074
                GOSUB READ.LMM.SCHEDULES
*            WS.FLD0340 = WS.AMOUNT
                OK.FLAG = ""

*        CASE WS.CATEG GE 21081 AND WS.CATEG LE 21095
*            GOSUB READ.LMM.SCHEDULES
*            WS.FLD0340 = WS.AMOUNT
*            OK.FLAG = 1


*  _______________________________________________
*   ----------------------------------------------
        END CASE

****************
        IF OK.FLAG EQ "" THEN
            GOTO NEXT.LD.REC
        END
****************

        GOSUB GET.PERIOD

****************


NEXT.LD.REC:
    REPEAT

RETURN
*--------------------------------------------------------------------------
READ.LMM.SCHEDULES:
*    YY.LMM.ID = Y.LD.ID:"..."


    IDD  = Y.LD.ID:"00"
    CALL F.READ(FN.SCH.DATE, IDD, R.SCH.DATE, F.SCH.DATE, ETEXT)
*Line [ 709 ] Update VM to @VM - ITSS - R21 Upgrade - 2021-12-26
    DATE.LIST= CONVERT(@VM,"*", R.SCH.DATE)
    LOOP
        REMOVE ID.PAST FROM DATE.LIST  SETTING POS

    WHILE ID.PAST:POS

*DEBUG
        SCHEDULE.DATES     = FIELDS(ID.PAST,"*",1,1)
        SCHEDULE.PROCESSED = FIELDS(ID.PAST,"*",2,1)

        FLAG.D = SCHEDULE.PROCESSED
        IF FLAG.D EQ 'A' THEN

            YY.LMM.ID = Y.LD.ID:SCHEDULE.DATES:"00"
            Y.LMM.ID = Y.LD.ID:SCHEDULE.DATES:"00"



*            SEL.LMM  = "SELECT ":FN.LMM:" WITH @ID LIKE ":YY.LMM.ID
*            SEL.LMM := " BY @ID"

*            CALL EB.READLIST(SEL.LMM,SEL.LIST.LMM,'',NO.OF.LMM,ERR.LMM)

*            LOOP
*                REMOVE Y.LMM.ID FROM SEL.LIST.LMM SETTING POS.LMM
*            WHILE Y.LMM.ID:POS.LMM

            CALL F.READ(FN.LMM,Y.LMM.ID,R.LMM,F.LMM,ERR.LMM)


            WS.AMOUNT = R.LMM<3>

            IF WS.AMOUNT EQ 0 THEN
                GOTO NEXT.LMM.REC
            END

            X.GRE.DATE = ''
            X.JUL.DATE = Y.LMM.ID[13,7]
            CALL JULDATE(X.GRE.DATE,X.JUL.DATE)

            WS.MAT.DATE = X.GRE.DATE

**        PRINT Y.LMM.ID:"   ":WS.AMOUNT:"   ":WS.CY:"   ":Y.LMM.ID[13,7]:"   ":X.GRE.DATE

            WS.FLD0360 = WS.AMOUNT

            GOSUB GET.PERIOD


NEXT.LMM.REC:
*REPEAT
        END
    REPEAT

RETURN
*--------------------------------------------------------------------------
READ.MM.MONEY.MARKET:

    SEL.MON  = "SELECT ":FN.MON:" WITH PRINCIPAL NE 0"
    SEL.MON := " AND MATURITY.DATE GE ":SYS.DATE
    SEL.MON := " AND VALUE.DATE LT ":SYS.DATE



    CALL EB.READLIST(SEL.MON,SEL.MON.LD,'',NO.MON,ERR.MON)


    LOOP
        REMOVE Y.MON.ID FROM SEL.MON.LD SETTING POS.MON
    WHILE Y.MON.ID:POS.MON
        CALL F.READ(FN.MON,Y.MON.ID,R.MON,F.MON,ERR.MON)



        WS.CATEG     = R.MON<MM.CATEGORY>
        WS.AMOUNT    = R.MON<MM.PRINCIPAL>
        WS.CY        = R.MON<MM.CURRENCY>
        WS.MAT.DATE  = R.MON<MM.MATURITY.DATE>


        GOSUB RTN.MOVE.ZEROS


        OK.FLAG = ""

***     ================================================================================================================
        BEGIN CASE
***     -------------------------------------------------------                    ����� ������ ��� ������ �������
            CASE WS.CATEG EQ 21075
                WS.FLD0060 = WS.AMOUNT
                OK.FLAG = 1
***     -------------------------------------------------------                    ����� ������ ��� ������ ��������
            CASE WS.CATEG EQ 21076
                WS.FLD0070 = WS.AMOUNT
                OK.FLAG = 1
***     -------------------------------------------------------                       ����� ��� ����� �������
            CASE WS.CATEG EQ 21078
                WS.FLD0080 = WS.AMOUNT
                OK.FLAG = 1
***     -------------------------------------------------------                   %   ����� ��� ������� 10%
            CASE WS.CATEG EQ 21079
                WS.FLD0090 = WS.AMOUNT
                OK.FLAG = 1
***     -------------------------------------------------------                  ����� ��� ������� � �������
            CASE WS.CATEG EQ 21081
                WS.FLD0100 = WS.AMOUNT
                OK.FLAG = 1
        END CASE
***     ================================================================================================================
***                                                                                                   ������� �����
***     -------------------------------------------------------                       ����� ������ - ���� �����
        BEGIN CASE
            CASE WS.CATEG EQ 21030
                WS.FLD0260 = WS.AMOUNT
                OK.FLAG = 1
***     -------------------------------------------------------                       ����� ������ - ���� ������
            CASE WS.CATEG EQ 21031
                WS.FLD0270 = WS.AMOUNT
                OK.FLAG = 1
        END CASE
**    = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =                              ��� ������ �� �������
*===========================================================================================================================


*       ------------------------
        IF OK.FLAG EQ "" THEN
            GOTO NEXT.MM.REC
        END

****************

        GOSUB GET.PERIOD
*---------------

NEXT.MM.REC:



    REPEAT
RETURN
****************
*--------------------------------------------------------------------------
READ.FIN.OZON:

    SEL.OZON  = "SELECT ":FN.OZON


    CALL EB.READLIST(SEL.OZON,SEL.LIST.OZON,'',NO.OF.OZON,ERR.OZON)


    LOOP
        REMOVE Y.OZON.ID FROM SEL.LIST.OZON SETTING POS.OZON
    WHILE Y.OZON.ID:POS.OZON
        CALL F.READ(FN.OZON,Y.OZON.ID,R.OZON,F.OZON,ERR.OZON)

****************
        GOSUB RTN.MOVE.ZEROS


****        WS.FLD0300   = R.OZON<OZN.OFFICIAL.VALUE>
****                        ��� ������� ������� ���� ����� 2.11/04/12
        FILE.WS.FLD0300   = R.OZON<OZN.RECORDED.VALUE>
        FILE.WS.MAT.DATE  = R.OZON<OZN.MATURITY.DATE>
        FILE.WS.CY        = R.OZON<OZN.ACCOUNT.NO>[1,3]
***        WS.CY        = WS.ACC[1,3]
***        WS.CY        = 'EGP'

***  ----------------------------------------------------------------------------------
***                                    ��� 20% �� ������ ������ �� �����
***                                          ��� �����  �� �����


***

        WS.MAT.DATE = FILE.WS.MAT.DATE

        IF WS.MAT.DATE GT END.PRD(2) THEN
            WS.FLD0300   = FILE.WS.FLD0300 * 0.2
            FILE.WS.FLD0300 = FILE.WS.FLD0300 * 0.8
            WS.MAT.DATE  = END.PRD(2)
            WS.CY        = FILE.WS.CY
            GOSUB GET.PERIOD
        END

        WS.MAT.DATE = FILE.WS.MAT.DATE

        IF WS.MAT.DATE GE SYS.DATE THEN
            WS.FLD0300   = FILE.WS.FLD0300
            WS.MAT.DATE  = FILE.WS.MAT.DATE
            WS.CY        = FILE.WS.CY
            GOSUB GET.PERIOD
        END



*---------------
NEXT.OZON.REC:



    REPEAT
RETURN
****************
*--------------------------------------------------------------------------
*                                                                                     ����� ������������
READ.FIN.SANAD:

    SEL.SANAD  = "SELECT ":FN.SANAD

**     PRINT SEL.SANAD

    CALL EB.READLIST(SEL.SANAD,SEL.LIST.SANAD,'',NO.OF.SANAD,ERR.SANAD)


    LOOP
        REMOVE Y.SANAD.ID FROM SEL.LIST.SANAD SETTING POS.SANAD
    WHILE Y.SANAD.ID:POS.SANAD
        CALL F.READ(FN.SANAD,Y.SANAD.ID,R.SANAD,F.SANAD,ERR.SANAD)

****************
        GOSUB RTN.MOVE.ZEROS


        WS.FLD0310   = R.SANAD<SND.DISCOUNTED.VALUE>
        WS.MAT.DATE  = R.SANAD<SND.MATURITY.DATE>

        WS.CY        = 'EGP'



        IF WS.MAT.DATE GE SYS.DATE THEN
            GOSUB GET.PERIOD
        END



*---------------
NEXT.SANAD.REC:

    REPEAT
RETURN
****************
*--------------------------------------------------------------------------
READ.SCB.INSTALL2:

    SEL.ALL2  = "SELECT ":FN.ALL2:" WITH STATUS NE 'RANDOM'"

    CALL EB.READLIST(SEL.ALL2,SEL.LIST.ALL2,'',NO.OF.ALL2,ERR.ALL2)

    LOOP
        REMOVE Y.ALL2.ID FROM SEL.LIST.ALL2 SETTING POS.ALL2
    WHILE Y.ALL2.ID:POS.ALL2
        CALL F.READ(FN.ALL2,Y.ALL2.ID,R.ALL2,F.ALL2,ERR.ALL2)

****************
        GOSUB RTN.MOVE.ZEROS
****************
        NO.DATE        = R.ALL2<INS.INSTALLMENT.DATE>
        NO.OF.DATE     = DCOUNT(NO.DATE,@VM)
        WS.AC          = R.ALL2<INS.ACCOUNT.NO>
*Line [ 988 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,WS.AC,X.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,WS.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
X.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>

**        WS.CY          = 'EGP'
        WS.CY          = X.CURR

        IF WS.CY EQ '' THEN
            GO TO NEXT.ALL2
        END

**        WS.MAT.DATE    = R.ALL2<INS.INSTALLMENT.DATE,NO.OF.DATE>
**        WS.AC          = R.ALL2<INS.ACCOUNT.NO>
**        CALL DBR ('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,WS.AC,WS.OPEN.ACT.BAL)
**        WS.FLD0320   = WS.OPEN.ACT.BAL * -1
**        PRINT "INSTALL2  ":WS.AC:"   ":WS.MAT.DATE:"   ":WS.FLD0320:"  ":WS.CY

        FOR IX = 1 TO NO.OF.DATE
            WS.MAT.DATE    = R.ALL2<INS.INSTALLMENT.DATE,IX>
            WS.FLD0320     = R.ALL2<INS.INSTALLMENT.AMOUNT,IX>

            IF WS.MAT.DATE GE SYS.DATE THEN
**                PRINT "INSTALL2  ":WS.AC:"   ":WS.MAT.DATE:"   ":WS.FLD0320:"  ":WS.CY
                GOSUB GET.PERIOD
            END

        NEXT IX
*---------------
NEXT.ALL2:

    REPEAT

RETURN
****************
*--------------------------------------------------------------------------
READ.SCB.INSTALLMENT:

    SEL.MENT  = "SELECT ":FN.MENT

    CALL EB.READLIST(SEL.MENT,SEL.LIST.MENT,'',NO.OF.MENT,ERR.MENT)


    LOOP
        REMOVE Y.MENT.ID FROM SEL.LIST.MENT SETTING POS.MENT
    WHILE Y.MENT.ID:POS.MENT
        CALL F.READ(FN.MENT,Y.MENT.ID,R.MENT,F.MENT,ERR.MENT)

****************
        GOSUB RTN.MOVE.ZEROS
****************
        NO.DATE        = R.MENT<INST.INSTALLMENT.DATE>
        NO.OF.DATE     = DCOUNT(NO.DATE,@VM)
        WS.AC          = R.MENT<INST.ACCOUNT.NO>
*Line [ 1045 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,WS.AC,X.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,WS.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
X.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
**        WS.CY          = 'EGP'
        WS.CY          = X.CURR

        IF WS.CY EQ '' THEN
            GO TO NEXT.MENT
        END
**        WS.MAT.DATE    = R.MENT<INST.INSTALLMENT.DATE,NO.OF.DATE>
**        WS.AC          = R.MENT<INST.ACCOUNT.NO>
**        CALL DBR ('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,WS.AC,WS.OPEN.ACT.BAL)
**        WS.FLD0330   = WS.OPEN.ACT.BAL * -1
**        PRINT "INSTALLMENT  ":WS.AC:"  ":WS.MAT.DATE:"  ":WS.FLD0330:"  ":WS.CY

        FOR IX = 1 TO NO.OF.DATE
            WS.MAT.DATE    = R.MENT<INST.INSTALLMENT.DATE,IX>
            WS.FLD0330     = R.MENT<INST.INSTALLMENT.AMOUNT>

            IF WS.MAT.DATE GE SYS.DATE THEN
***                PRINT "INSTALLMENT  ":WS.AC:"  ":WS.MAT.DATE:"  ":WS.FLD0330:"  ":WS.CY
                GOSUB GET.PERIOD
            END

        NEXT IX
*---------------
NEXT.MENT:

    REPEAT

RETURN
****************

RETURN
*-------------------------------------------------------------------------------------------------------------------

READ.SCB.LOANS.INST:

    SEL.LINST  = "SELECT ":FN.LINST

    CALL EB.READLIST(SEL.LINST,SEL.LIST.LINST,'',NO.OF.LINST,ERR.LINST)
    LOOP
        REMOVE Y.LINST.ID FROM SEL.LIST.LINST SETTING POS.LINST
    WHILE Y.LINST.ID:POS.LINST
**        DEBUG
        CALL F.READ(FN.LINST,Y.LINST.ID,R.LINST,F.LINST,ERR.LINST)

****************
        GOSUB RTN.MOVE.ZEROS
****************
*** HARES
******************************************************************************************************
        NO.DATE        = R.LINST<LINST.INSTALLMENT.DATE>
        NO.OF.DATE     = DCOUNT(NO.DATE,@VM)
        WS.AC          = R.LINST<LINST.ACCOUNT.NO>
*Line [ 1104 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.CURRENCY,WS.AC,X.CURR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,WS.AC,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
X.CURR=R.ITSS.ACCOUNT<AC.CURRENCY>
**        WS.CY          = 'EGP'
        WS.CY          = X.CURR

        IF WS.CY EQ '' THEN
            GO TO NEXT.LINST
        END
******************************************************************************************************
**

        FOR IX = 1 TO NO.OF.DATE
            WS.MAT.DATE    = R.LINST<LINST.INSTALLMENT.DATE,IX>
            WS.FLD0340     = R.LINST<LINST.INST.AMOUNT,IX>

            IF WS.MAT.DATE GE SYS.DATE THEN
***                PRINT "LINST ":WS.AC:" ":" ":WS.CY:" ":WS.MAT.DATE:" ":WS.FLD0340
                GOSUB GET.PERIOD
            END

        NEXT IX
*---------------
NEXT.LINST:

    REPEAT


RETURN

*-------------------------------------------------------------------------------------------------------------------
READ.ACCOUNT.FILE:

    SEL.ACCOUNT  = "SELECT ":FN.ACC:" WITH CATEGORY GE 1100"
    SEL.ACCOUNT := " AND CATEGORY LE 1599"
    SEL.ACCOUNT := " AND CUSTOMER NE ''"
    SEL.ACCOUNT := " WITHOUT CATEGORY IN (":AC.GROUP:")"

    CALL EB.READLIST(SEL.ACCOUNT,SEL.LIST.ACCOUNT,'',NO.OF.ACCOUNT,ERR.ACCOUNT)
    LOOP
        REMOVE Y.ACC.ID FROM SEL.LIST.ACCOUNT SETTING POS.ACCOUNT
    WHILE Y.ACC.ID:POS.ACCOUNT

        CALL F.READ(FN.ACC,Y.ACC.ID,R.ACCOUNT,F.ACC,ERR.ACC)

**
        WS.CY          = R.ACCOUNT<AC.CURRENCY>
        WS.CUST        = R.ACCOUNT<AC.CUSTOMER>
        WS.LM.REF      = R.ACCOUNT<AC.LIMIT.REF>


        IF R.ACCOUNT<AC.WORKING.BALANCE> EQ '' THEN
            GOTO AC.LOOP
        END
        IF R.ACCOUNT<AC.WORKING.BALANCE> GT 0 THEN
            GOTO AC.LOOP
        END

        IF WS.LM.REF EQ '' THEN
            GOTO AC.LOOP
        END
        IF WS.LM.REF EQ 0 THEN
            GOTO AC.LOOP
        END


        IF LEN(WS.LM.REF) EQ 6 THEN
            WS.LMT.ID      = WS.CUST:'.':'0000':WS.LM.REF
        END

        IF LEN(WS.LM.REF) EQ 7 THEN
            WS.LMT.ID      = WS.CUST:'.':'000':WS.LM.REF
        END



****************
        GOSUB RTN.MOVE.ZEROS
****************


        WS.FLD0370     = R.ACCOUNT<AC.WORKING.BALANCE> * -1

        WS.MAT.DATE    = ''

* --------------------

        CALL F.READ(FN.LIMIT,WS.LMT.ID,R.LIMIT,F.LIMIT,E.LIMIT)
        WS.MAT.DATE = R.LIMIT<LI.EXPIRY.DATE>

* --------------------


        IF WS.MAT.DATE EQ '' THEN
            GOTO AC.LOOP
        END


        IF WS.MAT.DATE GE SYS.DATE THEN
            GOSUB GET.PERIOD
        END


AC.LOOP:

    REPEAT


RETURN
*-------------------------------------------------------------------------------------------------------------------
GET.PERIOD:



    WS.PERIOD = ''
    XX.END.PRD = ''

    FOR I = 1 TO 8

        IF WS.MAT.DATE GE STR.PRD(I) THEN
            IF WS.MAT.DATE LE END.PRD(I) THEN

                WS.PERIOD = PRD.COD(I)

                XX.END.PRD = END.PRD(I)

            END
        END

    NEXT I

    IF WS.PERIOD EQ '' THEN
        RETURN
    END

    Y.MLAD010.ID = WS.CY:"-":WS.PERIOD:"-":WS.MAT.DATE
    Y.MLAD010.ID = WS.CY:"-":WS.PERIOD


    GOSUB READ.MATUR.LADDER.1.FILE


RETURN
*-------------------------------------------------------------------------------------------------------------------
READ.MATUR.LADDER.1.FILE:
*-----------------------
    R.MLAD010 = ''

****    PRINT Y.MLAD010.ID

    CALL F.READ(FN.MLAD010,Y.MLAD010.ID,R.MLAD010,F.MLAD010,ERR.FI)

*   -------------------
    IF ERR.FI THEN

        R.MLAD010<ML010.FLD0060.AMT> = WS.FLD0060
        R.MLAD010<ML010.FLD0070.AMT> = WS.FLD0070
        R.MLAD010<ML010.FLD0080.AMT> = WS.FLD0080
        R.MLAD010<ML010.FLD0090.AMT> = WS.FLD0090
        R.MLAD010<ML010.FLD0100.AMT> = WS.FLD0100
        R.MLAD010<ML010.FLD0250.AMT> = WS.FLD0250
        R.MLAD010<ML010.FLD0260.AMT> = WS.FLD0260
        R.MLAD010<ML010.FLD0270.AMT> = WS.FLD0270
        R.MLAD010<ML010.FLD0300.AMT> = WS.FLD0300
        R.MLAD010<ML010.FLD0310.AMT> = WS.FLD0310
        R.MLAD010<ML010.FLD0320.AMT> = WS.FLD0320
        R.MLAD010<ML010.FLD0330.AMT> = WS.FLD0330
        R.MLAD010<ML010.FLD0340.AMT> = WS.FLD0340
        R.MLAD010<ML010.FLD0360.AMT> = WS.FLD0360
        R.MLAD010<ML010.FLD0370.AMT> = WS.FLD0370

        R.MLAD010<ML010.CERT.AMT>    = CER.AMOUNT
        R.MLAD010<ML010.DEPOSIT.AMT> = DEPOSIT.AMOUNT
        R.MLAD010<ML010.LOANS.AMT>   = LOANS.AMOUNT

    END ELSE

        R.MLAD010<ML010.FLD0060.AMT> += WS.FLD0060
        R.MLAD010<ML010.FLD0070.AMT> += WS.FLD0070
        R.MLAD010<ML010.FLD0080.AMT> += WS.FLD0080
        R.MLAD010<ML010.FLD0090.AMT> += WS.FLD0090
        R.MLAD010<ML010.FLD0100.AMT> += WS.FLD0100
        R.MLAD010<ML010.FLD0250.AMT> += WS.FLD0250
        R.MLAD010<ML010.FLD0260.AMT> += WS.FLD0260
        R.MLAD010<ML010.FLD0270.AMT> += WS.FLD0270
        R.MLAD010<ML010.FLD0300.AMT> += WS.FLD0300
        R.MLAD010<ML010.FLD0310.AMT> += WS.FLD0310
        R.MLAD010<ML010.FLD0320.AMT> += WS.FLD0320
        R.MLAD010<ML010.FLD0330.AMT> += WS.FLD0330
        R.MLAD010<ML010.FLD0340.AMT> += WS.FLD0340
        R.MLAD010<ML010.FLD0360.AMT> += WS.FLD0360
        R.MLAD010<ML010.FLD0370.AMT> += WS.FLD0370


        R.MLAD010<ML010.CERT.AMT>    += CER.AMOUNT
        R.MLAD010<ML010.DEPOSIT.AMT> += DEPOSIT.AMOUNT
        R.MLAD010<ML010.LOANS.AMT>   += LOANS.AMOUNT

    END
*   -------------------


    R.MLAD010<ML010.CURRENCY>   = WS.CY
****    R.MLAD010<ML010.MATUR.DATE> = WS.MAT.DATE
    R.MLAD010<ML010.MATUR.DATE> = XX.END.PRD
    R.MLAD010<ML010.PERIOD>     = WS.PERIOD

*------------------------
    CALL F.WRITE(FN.MLAD010,Y.MLAD010.ID,R.MLAD010)
    CALL JOURNAL.UPDATE(Y.MLAD010.ID)

RETURN



*--------------------------------------------------------------------------
RTN.MOVE.ZEROS:

    WS.FLD0010 = 0
    WS.FLD0020 = 0
    WS.FLD0030 = 0
    WS.FLD0040 = 0
    WS.FLD0050 = 0
    WS.FLD0060 = 0
    WS.FLD0070 = 0
    WS.FLD0080 = 0
    WS.FLD0090 = 0
    WS.FLD0100 = 0
    WS.FLD0110 = 0
    WS.FLD0120 = 0
    WS.FLD0130 = 0
    WS.FLD0140 = 0
    WS.FLD0150 = 0
    WS.FLD0160 = 0
    WS.FLD0170 = 0
    WS.FLD0180 = 0
    WS.FLD0190 = 0
    WS.FLD0200 = 0
    WS.FLD0210 = 0
    WS.FLD0220 = 0
    WS.FLD0230 = 0
    WS.FLD0240 = 0
    WS.FLD0250 = 0
    WS.FLD0260 = 0
    WS.FLD0270 = 0
    WS.FLD0290 = 0
    WS.FLD0300 = 0
    WS.FLD0310 = 0
    WS.FLD0320 = 0
    WS.FLD0330 = 0
    WS.FLD0340 = 0
    WS.FLD0350 = 0
    WS.FLD0360 = 0
    WS.FLD0370 = 0
    WS.FLD0380 = 0
    WS.FLD0390 = 0
    WS.FLD0400 = 0
    WS.FLD0410 = 0
    WS.FLD0420 = 0
    WS.FLD0430 = 0
    WS.FLD0440 = 0
    WS.FLD0450 = 0
    WS.FLD0460 = 0
    WS.FLD0470 = 0
    WS.FLD0480 = 0
    WS.FLD0490 = 0
    WS.FLD0500 = 0


    DEPOSIT.AMOUNT = 0
    CER.AMOUNT     = 0
    LOANS.AMOUNT   = 0
    OK.FLAG = ""





RETURN
*--------------------------------------------------------------------------

END
