* @ValidationCode : Mjo3NjU3OTI3NDU6Q3AxMjUyOjE2NDE3NzkxNDQwODM6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:45:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>897</Rating>
*-----------------------------------------------------------------------------
*PROGRAM  SBM.AC.CUTOMER.ALL
SUBROUTINE SBM.AC.CUTOMER.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TOPCUS.CR.TOT.LW
*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS.OPEN
    GOSUB PROCESS.CLOSE
    GOSUB PRINT.ARRAY
    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    PRINT "DONE"
RETURN
*----------------------------- INITIALIZATIONS ------------------------
PRINT.ARRAY:
    FOR ROW.ARR = 1 TO ZZ
        PRINT ARRAY.LINE<1,ROW.ARR>
    NEXT ROW.ARR
RETURN
*---------------------------------------------------------------------
LINE.END:
    WW = SPACE(120)
    WW<1,1> = SPACE(20):"********************  ����� �������   ******************"
    PRINT WW<1,1>
RETURN
*--------------------------------------------------------------------
INITIATE:
    ZZ = 1
    HH = 1
    ALL.BAL.NEW = 0
    ARRAY.LINE = SPACE(150)

*    REPORT.ID = 'SBM.AC.TOTALS'
    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CBE = "F.SCB.TOPCUS.CR.TOT.LW"  ; F.CBE = ""
    CALL OPF(FN.CBE,F.CBE)

    FN.ACC = 'FBNK.ACCOUNT'               ; F.ACC = ''
    CALL OPF( FN.ACC,F.ACC)

    FN.CUS = 'FBNK.CUSTOMER'              ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.CUS.H = 'FBNK.CUSTOMER$HIS'        ; F.CUS.H = ''
    CALL OPF(FN.CUS.H,F.CUS.H)

    FN.CUS.AC  = 'FBNK.CUSTOMER.ACCOUNT'  ; F.CUS.AC = '' ; R.CUS.AC = ''
    CALL OPF( FN.CUS.AC,F.CUS.AC)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'     ; F.CCY = '' ; R.CCY = ''
    CALL OPF( FN.CCY,F.CCY)

    FN.LD ='FBNK.LD.LOANS.AND.DEPOSITS'   ; R.LD='';F.LD=''
    CALL OPF(FN.LD,F.LD)

    FN.LMM ='FBNK.LMM.CUSTOMER' ;R.LMM='';F.LMM =''
    CALL OPF(FN.LMM,F.LMM)

    FN.LCC.AP ='FBNK.LC.APPLICANT' ;R.LCC.AP ='';F.LCC.AP =''
    CALL OPF(FN.LCC.AP,F.LCC.AP)

    FN.LCC ='FBNK.LETTER.OF.CREDIT' ;R.LCC ='';F.LCC =''
    CALL OPF(FN.LCC ,F.LCC )

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = ''
    CALL OPF (FN.CUR,F.CUR)


    INDV.COUNT.2 = 0
    COMP.COUNT.2 = 0

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    KEY.CCY   = 'NZD'
*----------------------
    X.DD   = "01"
    X.DD = FMT(X.DD,"R%2")
    DDD  = TODAY[1,6]:X.DD
*-----
    CALL CDT('',DDD,'-1C')
    END.DATE  = DDD
*-----
    SSS  = DDD[1,6]:X.DD
    START.DATE  = SSS
*-----

*    PRINT "AFTR ":START.DATE:" ":END.DATE:" ":TODAY
*    STOP
*----------------------
RETURN
*------------------------READ FORM TEXT FILE----------------------
PROCESS.OPEN:
    T.SEL  = " SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":START.DATE
    T.SEL := " AND CONTACT.DATE LE ":END.DATE
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
    T.SEL := " AND TEXT UNLIKE BR... BY COMPANY.BOOK"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    OLD.CO.CODE       = ''
    INDV.COUNT        = 0
    COMP.COUNT        = 0

    FOR I = 1 TO SELECTED-1
        CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
        CALL F.READ(FN.CUS,KEY.LIST<I+1>,R.CUS.1,F.CUS,CUS.ERR)
        COM.CODE    = R.CUS<EB.CUS.COMPANY.BOOK>
        COM.CODE.1  = R.CUS.1<EB.CUS.COMPANY.BOOK>

        IF COM.CODE EQ COM.CODE.1 THEN
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR    = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>

            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END

        END ELSE

            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR    = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END
*---------------
            ALL.COUNT    = COMP.COUNT + INDV.COUNT

*CALL EB.ROUND.AMOUNT('EGP',ALL.BAL.NEW,'',"2")
            OLDD      = COM.CODE[8,2]
            IF OLDD[1,1] EQ '0' THEN
                OLDD = OLDD[1]
            END

*Line [ 208 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,OLDD,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRAN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            BRANCH.NAME  = FIELD(BRAN.NAME,'.',2)
*--------------------------
            CBE.BR = COM.CODE[8,2]
*--------------------------
            GOSUB WRITE.LINE

            COMP.COUNT   = 0
            INDV.COUNT   = 0
        END
    NEXT I
RETURN
*******************************
PROCESS.CLOSE:
*-------------
****** DEBUG
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT GE 90 ) AND (SECTOR NE 5010 AND SECTOR NE 5020) AND CURR.NO GT 1"
    T.SEL := " AND WITHOUT TEXT LIKE BR... BY COMPANY.BOOK"


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        INDX = SELECTED

        FOR II = 1 TO INDX - 1
            CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERR)
            CALL F.READ(FN.CUS,KEY.LIST<II+1>,R.CUS.1,F.CUS,CUS.ERR)
*Line [ 237 ] Update COMPANY.BOOK to CO.CODE - ITSS - R21 Upgrade - 2021-12-26
            COM.CODE    = R.CUS<EB.CUS.CO.CODE>
*Line [ 239 ] Update COMPANY.BOOK to CO.CODE - ITSS - R21 Upgrade - 2021-12-26
            COM.CODE.1  = R.CUS.1<EB.CUS.CO.CODE>

            CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERRRR)
            CUS.ID  = KEY.LIST<II>
            CUR.NO  = R.CUS<EB.CUS.CURR.NO>
            RR = 1
            MM = CUR.NO - 1

            FOR NN = CUR.NO TO 1 STEP -1
                IF RR EQ 1 THEN
                    CUST.ID      = CUS.ID:";"
                    CUST.ID.NXT  = CUS.ID:";":MM
                END ELSE
                    CUST.ID      = CUS.ID:";":MM
                    MM           = MM -1
                    CUST.ID.NXT  = CUS.ID:";":MM
                END
                IF RR EQ 1 THEN
                    RR = 2
                    CALL F.READ(FN.CUS,CUS.ID,R.CUS.N,F.CUS,CUS.ER44)
                    CUS.DAT.2 = R.CUS.N<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.CUS.N<EB.CUS.POSTING.RESTRICT>
                END ELSE
                    CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                    CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                END

                CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                IF CUS.POST NE CUS.POST.NXT THEN
                    IF CUS.POST GE 90 THEN
                        IF CUS.DAT.2 GE START.DATE[3,6] AND CUS.DAT.2 LE END.DATE[3,6] THEN
*** COUNT *** -----------------
                            NEW.SECTOR  = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
                            IF NEW.SECTOR NE '' THEN
                                IF NEW.SECTOR NE '4650' THEN
                                    COMP.COUNT.2 = COMP.COUNT.2 + 1
                                END ELSE
                                    INDV.COUNT.2 = INDV.COUNT.2 + 1
                                END
                            END
                            NN       = 1
                        END
                    END
                END
            NEXT NN
            IF COM.CODE EQ COM.CODE.1 THEN
            END ELSE
* DIFF  --> SO SHOW ---------------------------------------------
                ALL.COUNT.2 = COMP.COUNT.2 + INDV.COUNT.2

                OLDD        = COM.CODE[8,2]
                OLDD        = OLDD + 0

*Line [ 300 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,OLDD,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRAN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                BRANCH.NAME.2  = FIELD(BRAN.NAME,'.',2)
                GOSUB WRITE.LINE.2

            END
        NEXT II
    END
RETURN
*-------------------------------------------------------------------*
WRITE.LINE:
    ARRAY.LINE<1,ZZ>[1,15]  = BRANCH.NAME
    ARRAY.LINE<1,ZZ>[16,10] = INDV.COUNT
    ARRAY.LINE<1,ZZ>[40,10] = COMP.COUNT
    ARRAY.LINE<1,ZZ>[52,10] = ALL.COUNT
    ARRAY.LINE<1,ZZ>[65,20] = ALL.BAL.NEW
    ZZ = ZZ + 1

    ALL.BAL.NEW = 0
RETURN
*------------------------------------------------------------------*
CALC.BALANCE:
    CUSTO = CUSTO:".ALL"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000

    ALL.BAL.NEW = ALL.BAL.NEW +  CBE.IN.LCY
RETURN
*----------------------------------------------------
WRITE.LINE.2:
    ARRAY.LINE<1,HH>[95,10]   = INDV.COUNT.2
    ARRAY.LINE<1,HH>[110,10]  = COMP.COUNT.2
    ARRAY.LINE<1,HH>[120,10]  = ALL.COUNT.2
    HH = HH + 1

    COMP.COUNT.2   = 0
    INDV.COUNT.2   = 0
    ALL.COUNT.2    = 0
    BRANCH.NAME.2  = ""
RETURN
************************ PRINT HEAD *******************************
PRINT.HEAD:
*Line [ 347 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(109):"SBM.AC.CUTOMER.ALL"
    PR.HD :="'L'":" "

**    FROM.DATE.2 = DDD
**    END.DATE.2  = TODAY
    FROM.DATE.2 = START.DATE
    END.DATE.2  = END.DATE

    FROM.DATE.2 = FMT(FROM.DATE.2, "####/##/##")
    END.DATE.2  = FMT(END.DATE.2 , "####/##/##")

    PR.HD :="'L'":SPACE(40):"�������� �������� � ������� ���� ����"
    PR.HD :="'L'":SPACE(40):" ��":SPACE(5):FROM.DATE.2 : "���" : SPACE(3):END.DATE.2

    PR.HD :="'L'":SPACE(30):STR('_',60)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(20): "�������� �������� "
    PR.HD :=SPACE(50): "�������� �������"

    PR.HD :="'L'":SPACE(15):"���"
    PR.HD :=SPACE(15):"���"
    PR.HD :=SPACE(10):"��������"
    PR.HD :=SPACE(10):"������� �������"
    PR.HD :=SPACE(10):"���"
    PR.HD :=SPACE(10):"���"
    PR.HD :=SPACE(10):"��������"

    PR.HD :="'L'":"�����":SPACE(10):"�������"
    PR.HD :=SPACE(10):"�������"

    PR.HD :=SPACE(50):"�������"
    PR.HD :=SPACE(5):"�������"


    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
RETURN
END
