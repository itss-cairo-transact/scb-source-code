* @ValidationCode : MjotMTY5MTU2NDk0NjpDcDEyNTI6MTY0MDgxODM4NTU5NzpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:53:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>535</Rating>
*-----------------------------------------------------------------------------
PROGRAM  SBD.CRT.TRIAL.02.D
*    SUBROUTINE SBD.CRT.TRIAL.02.D
*Line [ 21 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
*Line [ 23 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
*Line [ 25 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.CATEG.MAS.D
*Line [ 27 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.COMPANY
***---------------------------------------------------
    FN.CAT = "F.CATEG.MAS.D"
    F.CAT  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""

***----------------------------------------------------
    CALL OPF (FN.CAT,F.CAT)
    CALL OPF (FN.COMP,F.COMP)
***----------------------------------------------------
    GOSUB A.000.GET.ALL.BR
*----------------------------------
RETURN

*----------------------------------

A.000.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        IF WS.COMP.ID = "EG0010088" THEN
            GOTO A.000.REP
        END
*        IF WS.COMP.ID = "EG0010099" THEN
*            GOTO A.000.REP
*        END
        WS.TOT.LE = 0
        WS.TOT.FR = 0
        WS.BR = WS.COMP.ID
        GOSUB A.050.PROC
        GOSUB A.060.CHK
A.000.REP:
    REPEAT
RETURN

***----------------------------------------------------
A.050.PROC:
    SEL.CMD = "SELECT ":FN.CAT:" WITH @ID LIKE ":WS.BR:"..."
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

    LOOP
        REMOVE WS.CAT.ID FROM SEL.LIST SETTING POS
    WHILE WS.CAT.ID:POS

        CALL F.READ(FN.CAT,WS.CAT.ID,R.CAT,F.CAT,MSG.CAT)

        WS.LINE.NO = FIELD(WS.CAT.ID,'*',2)
        IF      WS.LINE.NO  EQ 0111 THEN
            GOTO A.050.A
        END
        IF      WS.LINE.NO  EQ 0311 THEN
            GOTO A.050.A
        END
        GOTO A.050.REP
*
A.050.A:
        WS.TEMP.AREA = R.CAT<CAT.CATD.BAL.IN.LOCAL.CY>
        IF   WS.LINE.NO EQ 0111 THEN
            WS.TOT.LE = WS.TOT.LE + WS.TEMP.AREA
        END
        IF   WS.LINE.NO EQ 0311 THEN
            WS.TOT.FR = WS.TOT.FR + WS.TEMP.AREA
        END
A.050.REP:
    REPEAT
A.050.EXIT:
RETURN
***-------------------------------------------------------
A.060.CHK:
    IF   WS.TOT.LE LT 0  AND WS.TOT.FR LT 0 THEN
        RETURN
    END
    GOSUB A.062.CHK
RETURN
***-------------------------------------------------------
A.062.CHK:
    SEL.CMD = "SELECT ":FN.CAT:" WITH @ID LIKE ":WS.BR:"..."
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

    LOOP
        REMOVE WS.CAT.ID FROM SEL.LIST SETTING POS
    WHILE WS.CAT.ID:POS

        CALL F.READ(FN.CAT,WS.CAT.ID,R.CAT,F.CAT,MSG.CAT)
        WS.CAT.ID.TEMP = WS.CAT.ID
        WS.LINE.NO = FIELD(WS.CAT.ID,'*',2)
        IF      WS.LINE.NO  EQ 0111 THEN
            GOTO A.062.A
        END
        IF      WS.LINE.NO  EQ 0311 THEN
            GOTO A.062.A
        END
        GOTO A.062.REP
*
A.062.A:
        WS.CAT.ID.TEMP = WS.CAT.ID
        WS.BAL = R.CAT<CAT.CATD.BAL.IN.ACT.CY>
        WS.BAL.EQV = R.CAT<CAT.CATD.BAL.IN.LOCAL.CY>

        IF   WS.LINE.NO EQ 0111 AND WS.TOT.LE GT 0 THEN
            GOSUB  A.065.DEL
            GOSUB  A.070.CRT

        END
        IF   WS.LINE.NO EQ 0311 AND WS.TOT.FR GT 0 THEN
            GOSUB  A.065.DEL
            GOSUB  A.070.CRT
        END
A.062.REP:
    REPEAT
A.062.EXIT:
RETURN
***-------------------------------------------------------
A.065.DEL:
    CALL F.DELETE(FN.CAT,WS.CAT.ID)
RETURN

***-------------------------------------------------------


A.070.CRT:
    IF WS.LINE.NO EQ 0111 THEN
        WS.CAT.ID.TEMPX = EREPLACE(WS.CAT.ID.TEMP,"*0111*","*0620*")
        R.CAT<CAT.CATD.ASST.LIAB>       =  "0620"
    END
    IF WS.LINE.NO EQ 0311 THEN
        WS.CAT.ID.TEMPX = EREPLACE(WS.CAT.ID.TEMP,"*0311*","*0820*")
        R.CAT<CAT.CATD.ASST.LIAB>       =  "0820"
    END
    R.CAT<CAT.CATD.BAL.IN.ACT.CY> = WS.BAL
    R.CAT<CAT.CATD.BAL.IN.LOCAL.CY> =  WS.BAL.EQV
    CALL F.WRITE(FN.CAT,WS.CAT.ID.TEMPX,R.CAT)
    CALL  JOURNAL.UPDATE(WS.CAT.ID.TEMPX)
RETURN

END
