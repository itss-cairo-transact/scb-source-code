* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBD.PRT.TRIAL.02.ALL.CY
*    PROGRAM      SBD.PRT.TRIAL.02.ALL.CY
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*---------------------------------------------------

    FN.CATMAS = "F.CATEG.MAS.D"
    F.CATMAS  = ""

    FN.CAT = "F.CATEGORY"
    F.CAT  = ""

    FN.REPL = "F.RE.STAT.REP.LINE"
    F.REPL = ""

    FN.DATE = "F.DATES"
    F.DATE  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""

*----------------------------------------------------
    CALL OPF (FN.CATMAS,F.CATMAS)
    CALL OPF (FN.CAT,F.CAT)
    CALL OPF (FN.REPL,F.REPL)
    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.COMP,F.COMP)
*----------------------------WORKING-------------------
    WS.CAT = ""
    WS.LOCAL.TOT = 0
    WS.GRP.OF.F = 0
    WS.GRP.COMP = 0
    WS.CY.ALPH.CODE = 0
    MSG.CATMAS = ""
    MSG.CAT = ""
    WS.LINE.COUNT = 0
    WS.PAGE.COUNT = 0
    WS.TOTAL.ASST.LIAB = 0
    WS.TOTAL.ASST.LIABFCY = 0
    WS.FRST = 0
    WS.NET =  0
    WS.TOT.AS.CY  = 0
    WS.TOT.AS.CYFCY  = 0
*-----------------------------------------------
    WS.OK = 0
    GOSUB A.001.ACCEPT.CY
    IF WS.OK EQ 1 THEN
        RETURN
    END
    WS.OK = 0
    GOSUB A.002.ACCEPT.BR
    IF WS.OK EQ 1 THEN
        RETURN
    END

    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
** --------------------------------------HEADER OF PRINTER
    WS.HD.T  = "CATEGORY OF TOTAL"
    WS.HD.T2A = "NO CATEG "
    WS.HD.T2B = "NAME CATEG "
    WS.HD.T2C = "BALANCE"
    WS.HD.T2D = "LOCAL IN BALANCE "
    WS.PRG.1 = "SBD.PRT.TRIAL.02.ALL.CY"
*--------------------------- PROCEDURE ---------------------

    WS.AL.CODE = 0
    WS.AL.GRP  = 0

    WS.TOTAL.LINE = 0
    WS.TOTAL.LINEFCY = 0
    WS.AL.CODE = 2
*    WS.CCY = "USD"
    GOSUB A.10.DATE
    GOSUB A.5000.PRT.HEAD
    GOSUB A.100.PROCESS
    GOSUB A.200.LINE.TOT

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN

*------------------------------------
A.001.ACCEPT.CY:
    YTEXT = "ENTER CURRENCY CODE OR XXX TO EXIT"
    CALL TXTINP(YTEXT, 8, 22, "3.3", "A")
    WS.CCY = COMI
    IF WS.CCY EQ "XXX" THEN
        WS.OK = 1
        RETURN
    END
    IF  WS.CCY EQ "USD" OR WS.CCY EQ "GBP" OR WS.CCY EQ "EUR"  THEN
        RETURN
    END
    IF  WS.CCY EQ "SAR" OR WS.CCY EQ "AED" OR WS.CCY EQ "KWD" THEN
        RETURN
    END
    IF WS.CCY EQ "JPY" OR WS.CCY EQ "CHF" OR WS.CCY EQ "CAD"  THEN
        RETURN
    END
    IF WS.CCY EQ "AUD"  OR WS.CCY EQ "NOK" OR WS.CCY EQ "SEK" THEN
        RETURN
    END
    IF WS.CCY EQ "DKK" OR WS.CCY EQ "INR" OR WS.CCY EQ "EGP"  THEN
        RETURN
    END

*    IF WS.OK EQ 1 THEN
    TEXT = "��� ���� ���" ; CALL REM
    GOTO  A.001.ACCEPT.CY
*    END
    RETURN
*------------------------------------
A.002.ACCEPT.BR:
    YTEXT = "ENTER BRANCH CODE EG00100XX OR EG0010000 FOR ALL BRANCHES OR XXX TO EXIT"
    CALL TXTINP(YTEXT, 8, 22, "9.9", "A")
    WS.BR = COMI
    IF WS.BR EQ "XXX" THEN
        WS.OK = 1
        RETURN
    END
    IF  WS.BR EQ "EG0010000" THEN
        WS.BR.NAME = "������ �����"
        RETURN
    END
    IF  WS.BR EQ "EG0010088" THEN
        GOTO A.002.ACCEPT.BR
    END
    MSG.COMP = ""
    CALL F.READ(FN.COMP,WS.BR,R.COMP,F.COMP,MSG.COMP)
    IF MSG.COMP NE "" THEN
        TEXT = "��� ����� ���"; CALL REM
        GOTO A.002.ACCEPT.BR
    END
    WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>

    RETURN
*------------------------------------
A.10.DATE:
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LAST.WRK.DAT = R.DATE<EB.DAT.LAST.WORKING.DAY>
    RETURN

*------------------------------------
A.100.PROCESS:
    IF  WS.BR EQ "EG0010000" THEN
        SEL.CMD = "SELECT ":FN.CATMAS:" WITH CATD.CY.ALPH.CODE EQ ":WS.CCY:" BY CATD.ASST.LIAB BY CATD.CATEG.CODE BY CATD.CY.ALPH.CODE"
    END
    IF  WS.BR NE "EG0010000" THEN
        SEL.CMD = "SELECT ":FN.CATMAS:" WITH CATD.CY.ALPH.CODE EQ ":WS.CCY:" AND CATD.BR EQ ":WS.BR:" BY CATD.ASST.LIAB BY CATD.CATEG.CODE BY CATD.CY.ALPH.CODE"
    END
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CATMAS FROM SEL.LIST SETTING POS
    WHILE WS.CATMAS:POS
        CALL F.READ(FN.CATMAS,WS.CATMAS,R.CATMAS,F.CATMAS,MSG.CATMAS)

        WS.IN.ACT.CY = R.CATMAS<CAT.CATD.BAL.IN.ACT.CY>
        WS.IN.LCL.CY = R.CATMAS<CAT.CATD.BAL.IN.LOCAL.CY>
        IF  WS.IN.ACT.CY EQ 0 AND WS.IN.LCL.CY EQ 0 THEN
            GOTO A.100.A
        END
        WS.LINE      = R.CATMAS<CAT.CATD.ASST.LIAB>
        IF  WS.LINE  EQ 120 OR WS.LINE EQ 630 THEN
            GOTO A.100.A
        END
        WS.CATEG.CODE = R.CATMAS<CAT.CATD.CATEG.CODE>
        WS.ALPH.CODE  = R.CATMAS<CAT.CATD.CY.ALPH.CODE>

        IF WS.FRST = 0 THEN
            WS.LINE.COMP = WS.LINE
            WS.CATEG.CODE.COMP =  WS.CATEG.CODE
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.FRST = 1
            GOSUB  A.110.GET.REP.LINE.NAME
        END

        IF  WS.LINE.COMP NE WS.LINE  THEN
            GOSUB A.105.PRT.DTAL
            GOSUB A.200.LINE.TOT
            GOSUB A.150.CHK.HEAD
            WS.TOTAL.LINE = 0
            WS.TOTAL.LINEFCY = 0
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.LINE.COMP = WS.LINE
            WS.CATEG.CODE.COMP =  WS.CATEG.CODE
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
            GOSUB  A.110.GET.REP.LINE.NAME
        END

        IF  WS.CATEG.CODE.COMP NE   WS.CATEG.CODE  THEN
            GOSUB A.105.PRT.DTAL
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.CATEG.CODE.COMP =  WS.CATEG.CODE
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
        END

        IF  WS.ALPH.CODE.COMP  NE  WS.ALPH.CODE  THEN
            GOSUB A.105.PRT.DTAL
            WS.IN.ACT.CY.TOT = 0
            WS.IN.LCL.CY.TOT = 0
            WS.ALPH.CODE.COMP  =  WS.ALPH.CODE
        END

        WS.GL.NO = FIELD(WS.CATMAS,'*',3)
        WS.GL.CY = FIELD(WS.CATMAS,'*',4)

        WS.IN.ACT.CY.TOT = WS.IN.ACT.CY.TOT + WS.IN.ACT.CY
        WS.IN.LCL.CY.TOT = WS.IN.LCL.CY.TOT + WS.IN.LCL.CY

*        IF WS.PAGE.COUNT GT 2  THEN
*            GOTO A.100.EXIT
*        END
A.100.A:
    REPEAT
A.100.EXIT:
    RETURN
*------------------------------------------
A.105.PRT.DTAL:
    CALL F.READ(FN.CAT,WS.CATEG.CODE.COMP,R.CAT,F.CAT,MSG.CAT)
    WS.GL.NAME = R.CAT<EB.CAT.DESCRIPTION,2>
    XX = SPACE(132)
    XX<1,1>[1,6] = ""
    XX<1,1>[10,5] = WS.CATEG.CODE.COMP
    XX<1,1>[20,3] = WS.ALPH.CODE.COMP
    XX<1,1>[25,35] = WS.GL.NAME
    XX<1,1>[65,20] = FMT(WS.IN.ACT.CY.TOT, "R2,")
    XX<1,1>[90,20] = FMT(WS.IN.LCL.CY.TOT, "R2,")
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    WS.TOTAL.LINE = WS.TOTAL.LINE + WS.IN.LCL.CY.TOT
    WS.TOTAL.LINEFCY = WS.TOTAL.LINEFCY + WS.IN.ACT.CY.TOT
    IF WS.LINE.COUNT GT 45  THEN
        GOSUB A.5000.PRT.HEAD
    END
    RETURN
*------------------------------------------
A.110.GET.REP.LINE.NAME:
    WS.REP.KEY = "GENLED.":WS.LINE.COMP
    CALL F.READ(FN.REPL,WS.REP.KEY,R.REPL,F.REPL,MSG.REPL)
    WS.LINE.NAM = R.REPL<RE.SRL.DESC,2,2>
    XX = SPACE(132)
    XX<1,1>[1,5] = WS.LINE.COMP
    XX<1,1>[7,1] = ""
    XX<1,1>[10,35] = WS.LINE.NAM
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,5] = "====="
    XX<1,1>[7,1] = ""
    XX<1,1>[10,35] = STR('=',35)
    PRINT XX<1,1>
    WS.LINE.COUNT = WS.LINE.COUNT + 2
    IF WS.LINE.COUNT GT 45  THEN
        GOSUB A.5000.PRT.HEAD
    END


    RETURN
*------------------------------------------
A.150.CHK.HEAD:
*    IF WS.LINE EQ 230 THEN
    IF WS.LINE GT 200 AND WS.AL.CODE EQ 1 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 2
        GOSUB A.5000.PRT.HEAD
    END
*    IF WS.LINE EQ 510 THEN
    IF WS.LINE GT 500 AND WS.AL.CODE EQ 2 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 4
        GOSUB A.5000.PRT.HEAD
    END
*    IF WS.LINE EQ 710 THEN
    IF WS.LINE GT 700 AND WS.AL.CODE EQ 3 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 4
        GOSUB A.5000.PRT.HEAD
    END
*    IF WS.LINE EQ 1010 THEN
    IF WS.LINE GT 1000 AND WS.AL.CODE EQ 4 THEN
        GOSUB A.300.PRT.TOT.ASST.LIAB
        WS.AL.CODE = 5
        GOSUB A.5000.PRT.HEAD
    END
    RETURN
*------------------------------------------
A.200.LINE.TOT:
    XX = SPACE(132)
    XX<1,1>[1,46] = ""
    XX<1,1>[47,11] = "������ �����"
    XX<1,1>[65,20] = FMT(WS.TOTAL.LINEFCY, "R2,")
    XX<1,1>[86,03] = ""
    XX<1,1>[90,14] = "������ ����"
    XX<1,1>[105,20] = FMT(WS.TOTAL.LINE, "R2,")
    PRINT XX<1,1>
    WS.TOTAL.ASST.LIAB = WS.TOTAL.ASST.LIAB + WS.TOTAL.LINE
    WS.TOTAL.ASST.LIABFCY = WS.TOTAL.ASST.LIABFCY + WS.TOTAL.LINEFCY
    WS.LINE.COUNT = WS.LINE.COUNT + 1
    IF WS.LINE.COUNT GT 45  THEN
        GOSUB A.5000.PRT.HEAD
    END
    RETURN
*------------------------------------------
*����� ������ ������ � ������
A.300.PRT.TOT.ASST.LIAB:
    WS.TOT.COMNT = ""
    IF  WS.AL.CODE = 1  THEN
        WS.TOT.COMNT = "������ ���� ����"
    END
    IF  WS.AL.CODE = 2  THEN
        WS.TOT.COMNT = "������ ���� �����"
        WS.TOT.AS.CY = WS.TOTAL.ASST.LIAB
        WS.TOT.AS.CYFCY = WS.TOTAL.ASST.LIABFCY
    END
    IF  WS.AL.CODE = 3  THEN
        WS.TOT.COMNT = "������ ���� ����"
    END
    IF  WS.AL.CODE = 4  THEN
        WS.TOT.COMNT = "������ ���� �����"
        WS.TOT.LIAB.CY = WS.TOTAL.ASST.LIAB
        WS.TOT.LIAB.CYFCY = WS.TOTAL.ASST.LIABFCY
    END
    XX = SPACE(132)
    XX<1,1>[35,35] = WS.TOT.COMNT
    XX<1,1>[75,20] = FMT(WS.TOTAL.ASST.LIABFCY, "R2,")
    XX<1,1>[101,20] = FMT(WS.TOTAL.ASST.LIAB, "R2,")
    PRINT XX<1,1>
    IF  WS.AL.CODE = 4  THEN
        WS.NET = WS.TOT.AS.CY + WS.TOT.LIAB.CY
        WS.NETFCY = WS.TOT.AS.CYFCY + WS.TOT.LIAB.CYFCY
        WS.TOT.COMNT = "��� ���� �����"
        XX = SPACE(132)
        XX<1,1>[35,35] = WS.TOT.COMNT
        XX<1,1>[75,20] = FMT(WS.NETFCY, "R2,")
        XX<1,1>[101,20] = FMT(WS.NET, "R2,")
        PRINT XX<1,1>
    END
    WS.TOTAL.ASST.LIAB = 0
    WS.TOTAL.ASST.LIABFCY = 0
    RETURN

*-----------------------------------PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.LINE.COUNT = 0
    WS.PAGE.COUNT = WS.PAGE.COUNT + 1
    IF   WS.AL.CODE  EQ 1 THEN
        WS.AL.NAME = "������ �������"
    END
    IF   WS.AL.CODE  EQ 2 THEN
        WS.AL.NAME = "������ ��������"
    END
    IF   WS.AL.CODE  EQ 3 THEN
        WS.AL.NAME = "������ �������"
    END
    IF   WS.AL.CODE  EQ 4 THEN
        WS.AL.NAME = "������ ��������"
    END
    IF   WS.AL.CODE  EQ 5 THEN
        WS.AL.NAME = "�������� ��������"
    END
    DATY = WS.LAST.WRK.DAT
*    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":SPACE(3) :WS.BR.NAME:SPACE(34):WS.PRG.1:"   PAGE ":WS.PAGE.COUNT
    PR.HD :="'L'":SPACE(53):WS.AL.NAME
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD
    RETURN
END
