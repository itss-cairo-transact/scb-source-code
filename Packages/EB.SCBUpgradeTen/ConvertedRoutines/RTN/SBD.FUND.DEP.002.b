* @ValidationCode : MjoxNjIzMjE5MjAwOkNwMTI1MjoxNjQwODI1MDM2OTcyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:43:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-304</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBD.FUND.DEP.002

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 41 ] HASHING $INCLUDE I_AC.LOCAL.REFS  - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*-------------------------------------------------------------------------

    GOSUB INITIATE

***** CURRENCY SELECTION *************
    T.SEL2 = "SELECT FBNK.CURRENCY BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR NN = 1 TO SELECTED2
            CUR.ID = KEY.LIST2<NN>
            GOSUB PROCESS
        NEXT NN
    END
RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "FUNDING.COMPOSITION.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"FUNDING.COMPOSITION.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "FUNDING.COMPOSITION.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE FUNDING.COMPOSITION.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create FUNDING.COMPOSITION.CSV File IN &SAVEDLISTS&'
        END
    END

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    DAT = TODAY
    CALL CDT ('',DAT,'-1C')

    TD = TODAY
    CALL CDT ('',TD,'-1W')
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "FUNDING.COMPOSITION"
    HEAD.DESC  = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "":","
    HEAD.DESC := "SIGHT & O/N":","
    HEAD.DESC := "1 WEEK":","
    HEAD.DESC := "TERM <= 3M":","
    HEAD.DESC := "TERM <= 6M":","
    HEAD.DESC := "TERM <= 1Y":","
    HEAD.DESC := "TERM >= 3Y":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN

*========================================================================
PROCESS:

    DEP.AMT = 0 ; BORW.AMT = 0
    DEP.WEEK  = 0 ; DEP.TERM3  = 0 ; DEP.TERM6  = 0 ; DEP.TERM12  = 0 ; DEP.SIGHT  = 0 ; DEP.TERM13  = 0
    BORW.WEEK = 0 ; BORW.TERM3 = 0 ; BORW.TERM6 = 0 ; BORW.TERM12 = 0 ; BORW.SIGHT = 0 ; BORW.TERM13 = 0

*-------------------------------------------
******* TERMS SELECTION *************

    T.SEL = "SELECT ":FN.LD:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21010) OR (CATEGORY GE 21019 AND CATEGORY LE 21029) OR (CATEGORY EQ 21032 OR CATEGORY EQ 21041 OR CATEGORY EQ 21042)) AND (( VALUE.DATE LE ":DAT:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":DAT:" AND AMOUNT EQ 0 )) AND CURRENCY EQ ":CUR.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            CATEG = R.LD<LD.CATEGORY>
            AMT = R.LD<LD.AMOUNT>
            IF AMT EQ 0 THEN
                AMT = R.LD<LD.REIMBURSE.AMOUNT>
            END

            IF CATEG EQ 21001 THEN
                DEP.WEEK  += AMT
            END

            IF CATEG GE 21002 AND CATEG LE 21005 THEN
                DEP.TERM3  += AMT
            END

            IF CATEG EQ 21006 THEN
                DEP.TERM6  += AMT
            END

            IF CATEG EQ 21007 THEN
                DEP.TERM12  += AMT
            END

            IF (CATEG GE 21008 AND CATEG LE 21010) OR (CATEG GE 21019 AND CATEG LE 21029) OR (CATEG EQ 21032 OR CATEG EQ 21041 OR CATEG EQ 21042) THEN
                DEP.TERM13  += AMT
            END

        NEXT I
    END
****************************************
******* MM SELECTION *************

    T.SEL = "SELECT ":FN.MM:" WITH (CATEGORY GE 21030 AND CATEGORY LE 21031) AND STATUS NE 'LIQ' AND CURRENCY EQ ":CUR.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.MM,KEY.LIST<I>,R.MM,F.MM,E1)
            CATEG = R.MM<MM.CATEGORY>
            AMT = R.MM<MM.PRINCIPAL>

            BORW.SIGHT += AMT

        NEXT I
    END
******* SIGHT SELECTION *************

    T.SEL  = "SELECT ":FN.AC:" WITH (( CATEGORY IN (1481 1499 1408 1582 1483 1595 1493 1558)"
    T.SEL := " OR CATEGORY IN (1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 11242 1208 1211 1212 1216 1301 1302 1303 1377 1390)"
    T.SEL := " OR CATEGORY IN (1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513)"
    T.SEL := " OR CATEGORY IN (6501 6502 6503 6504 6511 6512 16151 16153 16188 16152 11380 16175 1012 1013 1014 1015 1016 1019 16170)"
    T.SEL := " OR CATEGORY IN (3011 3012 3013 3014 3017 3010 3005 2002 2003 2000 5021 5000 2001 5001 5020)"
    T.SEL := " OR CATEGORY IN (1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 11238 1407 1413 11240 11232 11234 11239 1480))"
    T.SEL := " AND CURRENCY EQ ":CUR.ID:" AND OPEN.ACTUAL.BAL GT 0)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            CATEG = R.AC<AC.CATEGORY>
            AMT = R.AC<AC.OPEN.ACTUAL.BAL>
            IF CATEG EQ 2002 OR CATEG EQ 2003 OR CATEG EQ 2000 OR CATEG EQ 5021 OR CATEG EQ 5000 OR CATEG EQ 2001 OR CATEG EQ 5001 OR CATEG EQ 5020 THEN
                BORW.SIGHT  += AMT
            END ELSE
                DEP.SIGHT += AMT
            END
        NEXT I
    END

    BB.DATA  = 'BORROWINGS':","
    BB.DATA := BORW.SIGHT:","
    BB.DATA := BORW.WEEK:","
    BB.DATA := BORW.TERM3:","
    BB.DATA := BORW.TERM6:","
    BB.DATA := BORW.TERM12:","
    BB.DATA := BORW.TERM13:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA  = 'DEPOSITS':","
    BB.DATA := DEP.SIGHT:","
    BB.DATA := DEP.WEEK:","
    BB.DATA := DEP.TERM3:","
    BB.DATA := DEP.TERM6:","
    BB.DATA := DEP.TERM12:","
    BB.DATA := DEP.TERM13:","
    BB.DATA := CUR.ID:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    BB.DATA = '******':","
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*********************************
RETURN
END
