* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.TLR.IN200.NEW(ENQ.DATA)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*---------------------------------------
    WS.COMP   = ID.COMPANY
    YTEXT     = "Enter From.Date. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    FIRST.DATE = COMI

    YTEXT     = "Enter END.Date. : "
    CALL TXTINP(YTEXT, 8, 22, "16", "A")
    END.DATE  = COMI
    CHK.FLG   = 0
*---------------------------------------
    FN.TLLR    = "FBNK.TELLER$HIS"
    F.TLLR     = ''
    R.TELLER   = ''
    Y.TLLR     = ''
    Y.TLLR.ERR = ''

    CALL OPF(FN.TLLR,F.TLLR)

    SEL.CMD   = " SELECT ":FN.TLLR:" BY CUSTOMER.1 BY CO.CODE BY AUTH.DATE"
    SEL.CMD  := " WITH AUTH.DATE GE ":FIRST.DATE
    SEL.CMD  := " AND AUTH.DATE LE ":END.DATE
    SEL.CMD  := " AND DR.CR.MARKER EQ 'CREDIT'"
    SEL.CMD  := " AND RECORD.STATUS EQ 'MAT'"
    SEL.CMD  := " AND CUST.COMP EQ ": WS.COMP
    SEL.CMD  := " AND CUST.NEW.SECTOR NE '4650'"
    SEL.CMD  := " AND TRANSACTION.CODE EQ '73'"

    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)
    TOT.CUS        = 0
    CUSTOMER.ARRAY = ""
    KK             = 1

    IF NOREC THEN
        FOR NN =1 TO NOREC
            CALL F.READ(FN.TLLR,SELLIST<NN>,R.TLLR,F.TLLR,E.TXT)
            CUS.1 = R.TLLR<TT.TE.CUSTOMER.1>
            ACC.1 = R.TLLR<TT.TE.ACCOUNT.1>
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('ACCOUNT':@FM:AC.CATEGORY,ACC.1,CATG)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CATG=R.ITSS.ACCOUNT<AC.CATEGORY>

            CALL F.READ(FN.TLLR,SELLIST<NN+1>,R.TLLR2,F.TLLR,E.TXT2)
            CUS.2 = R.TLLR2<TT.TE.CUSTOMER.1>

            LCY.AMT  = R.TLLR<TT.TE.AMOUNT.LOCAL.1>
            IF CUS.1 EQ CUS.2 THEN
                TOT.CUS += LCY.AMT
            END ELSE
                TOT.CUS += LCY.AMT

                IF TOT.CUS GE '200000' THEN
                    CUSTOMER.ARRAY<1,KK>  = CUS.1
                    KK++
                    CHK.FLG = 1
                END
                TOT.CUS  = 0
            END
        NEXT NN
    END ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUMMY'
    END

    IF CHK.FLG EQ 0 THEN
        ENQ.DATA<2,2> = '@ID'
        ENQ.DATA<3,2> = 'EQ'
        ENQ.DATA<4,2> = 'DUMMY'
    END ELSE
        GOTO DISPLAY.CUSTOMER
    END

    RETURN
*-----------------------------------------------------
DISPLAY.CUSTOMER:
*----------------
    INDX.CUS = 1
    FOR HH = 1 TO KK

        CUST.ID = CUSTOMER.ARRAY<1,HH>

        SEL.CMD2   = "SELECT ":FN.TLLR:" WITH"
        SEL.CMD2  := " AUTH.DATE GE ":FIRST.DATE
        SEL.CMD2  := " AND AUTH.DATE LE ":END.DATE
        SEL.CMD2  := " AND DR.CR.MARKER EQ 'CREDIT'"
        SEL.CMD2  := " AND RECORD.STATUS EQ 'MAT'"
        SEL.CMD2  := " AND CUST.COMP EQ ": WS.COMP
        SEL.CMD2  := " AND CUST.NEW.SECTOR NE '4650'"
        SEL.CMD2  := " AND CUSTOMER.1 EQ ": CUST.ID
        SEL.CMD2  := " AND TRANSACTION.CODE EQ '73'"
        SEL.CMD2  := " BY CUSTOMER.1 BY CO.CODE BY AUTH.DATE"

        CALL EB.READLIST(SEL.CMD2,SELLIST2,'',NOREC2,RTNCD2)

        IF NOREC2 THEN
            FOR ENQ.LP = 1 TO NOREC2
                ENQ.DATA<2,INDX.CUS> = '@ID'
                ENQ.DATA<3,INDX.CUS> = 'EQ'
                ENQ.DATA<4,INDX.CUS> = SELLIST2<ENQ.LP>
                INDX.CUS++
            NEXT ENQ.LP
        END
    NEXT HH
    RETURN
*-----------------------------------------------------
END
