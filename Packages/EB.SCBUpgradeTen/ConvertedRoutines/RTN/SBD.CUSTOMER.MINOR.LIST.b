* @ValidationCode : MjoxNzg3MjkwODEwOkNwMTI1MjoxNjQwODIxMzkyNTg5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 15:43:12
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-164</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.CUSTOMER.MINOR.LIST
***    PROGRAM SBD.CUSTOMER.MINOR.LIST
*   ------------------------------
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_COMMON
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_EQUATE
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.USER
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.DEPT.ACCT.OFFICER
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.COUNTRY
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.CATEGORY
*Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.CURRENCY
*Line [ 42 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_F.POSTING.RESTRICT
*Line [ 44 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT             I_CU.LOCAL.REFS
*Line [ 46 ] HASHING "$INSERT I_AC.LOCAL.REFS" - ITSS - R21 Upgrade - 2021-12-23
*  $INSERT             I_AC.LOCAL.REFS
*Line [ 48 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COMPANY
*Line [ 50 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*================================================================
    GOSUB INIT
    GOSUB OPENFILES
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB PRINT.END
RETURN
*-------------------------
PRINT.END:
    PRINT ""
    PRINT STR('_',100)
    PRINT ""

    XX = SPACE(120)
    XX<1,1>[1,30]   = "**** ������ ��� �������  ****"
    XX<1,1>[40,20]  = FMT(SELECTED,"R0,")
    PRINT XX<1,1>

    XX = SPACE(120)
    PRINT XX<1,1>
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)

    TEXT = "��� �������" ; CALL REM
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*-----------------------------------------------------------------------------
INIT:
    WS.COMP     = ID.COMPANY
    PROGRAM.ID  = "SBD.CUSTOMER.MINOR.LIST"
*    REPORT.ID   = "SBD.CUSTOMER.MINOR.LIST"
    REPORT.ID   = 'P.FUNCTION'

    CALL PRINTER.ON(REPORT.ID,'')

    FN.CUS.ACC  = "FBNK.CUSTOMER.ACCOUNT" ; F.CUS.ACC  = ""
    FN.CUS      = "FBNK.CUSTOMER"         ; F.CUS      = ""

    FN.ACC = "FBNK.ACCOUNT"               ; F.ACC = ""

    SYS.DATE    = TODAY
    P.DATE      = FMT(SYS.DATE,"####/##/##")
RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
    CALL OPF(FN.CUS,F.CUS)
    CALL OPF(FN.ACC,F.ACC)

RETURN
*-----------------------------------------------------------
PROCESS:
    N.SEL  = "SELECT ":FN.CUS:" WITH POSTING.RESTRICT LE 89"
    N.SEL := " AND CUSTOMER.STATUS EQ 22"
    N.SEL := " AND COMPANY.BOOK EQ ":WS.COMP
    N.SEL := " BY BIRTH.INCORP.DATE"
    CALL EB.READLIST(N.SEL,K.LIST,'',SELECTED,ER.MSG)

    TEXT = "SELECTED = ":SELECTED   ; CALL REM
    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CUS.NO       = K.LIST<II>
            CALL F.READ(FN.CUS,CUS.NO,R.CUS, F.CUS, E.CUS)
            CUS.BD   = R.CUS<EB.CUS.BIRTH.INCORP.DATE>
            CUS.NAT  = R.CUS<EB.CUS.NATIONALITY>
*Line [ 117 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR("COUNTRY":@FM:EB.COU.COUNTRY.NAME,CUS.NAT,CUS.NATIONAL)
F.ITSS.COUNTRY = 'F.COUNTRY'
FN.F.ITSS.COUNTRY = ''
CALL OPF(F.ITSS.COUNTRY,FN.F.ITSS.COUNTRY)
CALL F.READ(F.ITSS.COUNTRY,CUS.NAT,R.ITSS.COUNTRY,FN.F.ITSS.COUNTRY,ERROR.COUNTRY)
CUS.NATIONAL=R.ITSS.COUNTRY<EB.COU.COUNTRY.NAME>

*Line [ 125 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR("CUSTOMER":@FM:EB.CUS.LOCAL.REF,CUS.NO,LOCAL)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.NO,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUST.NAME     = ""
            CUST.NAME     = LOCAL<1,CULR.ARABIC.NAME>

            CUST.ADDRESS  = LOCAL<1,CULR.ARABIC.ADDRESS,1>
            CUST.ADDRESS1 = LOCAL<1,CULR.ARABIC.ADDRESS,2>
            GOSUB LINE.CUS

            CALL F.READ(FN.CUS.ACC,K.LIST<II>, R.CUS.ACC, F.CUS.ACC ,ETEXT)
            LOOP
                REMOVE ACCOUNT.NUMBER FROM R.CUS.ACC SETTING POS.ACC
            WHILE ACCOUNT.NUMBER:POS.ACC
                CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACCT, F.ACC, E.ACC1)
                ACCT.ID = ACCOUNT.NUMBER
                ACCT.TITLE = R.ACCT<AC.ACCOUNT.TITLE.1>
                WORK.BAL   = R.ACCT<AC.WORKING.BALANCE>
*                GOSUB LINE.ACCT
            REPEAT
*            PRINT STR('_',100)
        NEXT II
    END
RETURN
*-----------------------------------------------------------
LINE.CUS:
    LINE  = ""
    LINE<1,1>[1,10]    = K.LIST<II>
    LINE<1,1>[25,30]   = CUST.NAME
    LINE<1,1>[60,30]   = CUST.ADDRESS
    LINE<1,2>[60,30]   = CUST.ADDRESS1
    LINE<1,1>[88,20]   = FMT(CUS.BD,"####/##/##")
    LINE<1,1>[110,20]  = CUS.NATIONAL

    PRINT LINE<1,1>
    PRINT LINE<1,2>
    PRINT ""

    LINE<1,1>   = ""
    LINE<1,2>   = ""
RETURN
*-----------------------------------------------------------
LINE.ACCT:
    LINE = ""

    LINE<1,1>[1,20]  = ACCT.ID
    LINE<1,1>[25,30] = ACCT.TITLE
    LINE<1,1>[60,20] = WORK.BAL

    PRINT LINE<1,1>
    LINE<1,1>  = ""

RETURN
*-------------------------------------------------------------
PRINT.HEAD:
*Line [ 184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH

    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):PROGRAM.ID
    PR.HD :="'L'":SPACE(35):"���� �������� ����� ������"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(10):" ��� ������ "
    PR.HD :=SPACE(25):"�������"
    PR.HD :=SPACE(20):"����� �������"
    PR.HD :=SPACE(10):"�������"
    PR.HD :="'L'":STR('_',110)
    PR.HD :="'L'":" "

    HEADING PR.HD
    LINE.NO = 0
RETURN
*===============================================================
END
