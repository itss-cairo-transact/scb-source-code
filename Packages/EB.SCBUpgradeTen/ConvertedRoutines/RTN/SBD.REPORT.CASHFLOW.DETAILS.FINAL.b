* @ValidationCode : MjotMTUwNzQ3OTk5NzpDcDEyNTI6MTY0MTc3ODY5MDE2NjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:38:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*---------------------------------------NI7OOOOOOOOOOOOOOOOOO--------------------------------------
*-----------------------------------------------------------------------------
* <Rating>73955</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBD.REPORT.CASHFLOW.DETAILS.FINAL

***    SUBROUTINE SBD.REPORT.CASHFLOW.CO.ALL.MAT

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FIN.OZON
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CLOSURE
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FOREX
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 64 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 66 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 68 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 70 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 72 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 74 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 76 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INF.MULTI.TXN

    FN.CM     = 'F.COMPANY'  ; F.CM = ''
    CALL OPF(FN.CM,F.CM)

    COMP = ID.COMPANY
*TEXT = "COMP :": COMP ; CALL REM
*Line [ 84 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    IDDD="EG0010001"
*Line [ 93 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.PERIOD.END,IDDD,N.W.DAY)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,IDDD,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
N.W.DAY=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    HHH = N.W.DAY

*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
**   GOSUB PROCESS2
RETURN

*========================================================================
INITIATE:

    OPENSEQ "&SAVEDLISTS&" , "SCB.MALIA.CASHFLOW.CO.EZZN.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SCB.MALIA.CASHFLOW.CO.EZZN.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SCB.MALIA.CASHFLOW.CO.EZZN.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SCB.MALIA.CASHFLOW.CO.EZZN.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SCB.MALIA.CASHFLOW.CO.EZZN.CSV File IN &SAVEDLISTS&'
        END
    END
    DAT.HED = TODAY
    HEAD1   = "SAMPLE CASH-FLOW REPORT "
    HEAD.DESC = HEAD1:" ":DAT.HED:",":COMP

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

** HEAD.DESC  = "CASH INFLOW":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*  HEAD.DESC  = "":","
    HEAD.DESC = "INTERBANK":","
    HEAD.DESC := "CURRENT ACCOUNT":","
    HEAD.DESC := "SAVING ACCOUNT":","
    HEAD.DESC := "CORPRATE DEPOSITS":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN
*========================================================================
PROCESS:
*---------------------
    FN.DEPT   = 'F.DEPT.ACCT.OFFICER' ; F.DEPT = ''
    FN.AC     = 'FBNK.ACCOUNT' ; F.AC = ''
    FN.EZN    = 'F.SCB.FIN.OZON' ; F.EZN = ''
    FN.CU     = 'FBNK.CUSTOMER' ; F.CU = ''
    FN.IM     = 'F.IM.DOCUMENT.IMAGE' ; F.IM = ''
    FN.FT     = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    FN.TT     = 'FBNK.TELLER$NAU' ; F.TT = ''
    FN.IN     = 'F.INF.MULTI.TXN' ; F.IN = ''
    FN.CLOSE  = 'FBNK.ACCOUNT.CLOSURE' ; F.CLOSE = ''
    FN.MM     = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    FN.FOR    = 'FBNK.FOREX' ; F.FOR = ''
    FN.LD     = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    FN.LETTER = 'FBNK.LETTER.OF.CREDIT' ; F.LETTER = ''
    FN.DRAW   = 'FBNK.DRAWINGS' ; F.DRAW = ''
    FN.BT     = 'F.SCB.BT.BATCH' ; F.BT = ''
    FN.BR     = 'FBNK.BILL.REGISTER' ; F.BR = ''
    FN.CM     = 'F.COMPANY'  ; F.CM = ''
    FN.LIM = 'FBNK.LIMIT' ; F.LIM = '' ; R.LIM = ''

    CALL OPF(FN.LIM,F.LIM)
    CALL OPF(FN.DEPT,F.DEPT)
    CALL OPF(FN.LETTER,F.LETTER)
    CALL OPF(FN.DRAW,F.DRAW)
    CALL OPF(FN.AC,F.AC)
    CALL OPF(FN.CU,F.CU)
    CALL OPF(FN.IM,F.IM)
    CALL OPF(FN.FT,F.FT)
    CALL OPF(FN.TT,F.TT)
    CALL OPF(FN.EZN,F.EZN)
    CALL OPF(FN.IN,F.IN)
    CALL OPF(FN.CLOSE,F.CLOSE)
    CALL OPF(FN.MM,F.MM)
    CALL OPF(FN.FOR,F.FOR)
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.BT,F.BT)
    CALL OPF(FN.CM,F.CM)

    KEY.LIST ="" ; SELECTED ="" ;  ER.MSG=""  ; R.DEP = ''
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG=""
    KEY.LIST3="" ; SELECTED3="" ;  ER.MSG=""
    KEY.LIST4="" ; SELECTED4="" ;  ER.MSG=""

*Line [ 197 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DATEE)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DATEE=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>
    TD  = TODAY
***TEXT = COMP ; CALL REM
****BB.DATA = ''
*------------------------------------------------------------------------


    XXX1  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX11 = SPACE(132) ; XXX12 = SPACE(132)
    XXX2  = SPACE(132)  ; XXX5  = SPACE(132)  ; XXX10 = SPACE(132) ; XXX13 = SPACE(132)
    XXX3  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX6  = SPACE(132) ; XXX7  = SPACE(132)
    XXX15 = SPACE(132)  ; XXX16 = SPACE(132)  ; XXX17 = SPACE(132) ; XXX14 = SPACE(132)
    XXX21 = SPACE(132)  ; XXX19 = SPACE(132)  ; XXX18 = SPACE(132) ; XXX20 = SPACE(132)
    XXX6  = SPACE(132)  ; XXX7  = SPACE(132)  ; XXX8  = SPACE(132) ; XXX9 = SPACE(132)
    BAL1.1.P = 0 ; BAL14.1.P  = ''  ; BAL1.1.C  = 0 ; BAL14.1.C  = ''
    BAL1.P  = 0 ; BAL1.C  = 0  ; BAL2.P  = 0 ; BAL2.C  = 0 ;BAL3.P  = 0 ; BAL3.C  = 0  ; BAL4.P  = 0 ; BAL4.C  = 0
    BAL5.P  = 0 ; BAL5.C  = 0  ; BAL6.P  = 0 ; BAL6.C  = 0 ;BAL7.P  = 0 ; BAL7.C  = 0  ; BAL8.P  = 0 ; BAL8.C  = 0
    BAL9.P  = 0 ; BAL9.C  = 0  ; BAL10.P = 0 ; BAL10.C = 0 ;BAL11.P = 0 ; BAL11.C = 0  ; BAL12.P = 0 ; BAL12.C = 0
    BAL13.P = 0 ; BAL13.C = 0  ; BAL14.P = 0 ; BAL14.C = 0 ;BAL15.P = 0 ; BAL15.C = 0  ; BAL16.P = 0 ; BAL16.C = 0
    BAL17.P = 0 ; BAL17.C = 0  ; BAL18.P = 0 ; BAL18.C = 0 ;BAL19.P = 0 ; BAL19.C = 0  ; BAL20.P = 0 ; BAL20.C = 0
    BAL21.P = 0 ; BAL21.C = 0  ; BAL22.P = 0 ; BAL22.C = 0 ;BAL23.P = 0 ; BAL23.C = 0  ; BAL24.P = 0 ; BAL24.C = 0
    BAL25.P = 0 ; BAL25.C = 0  ; BAL26.P = 0 ; BAL26.C = 0 ;BAL27.P = 0 ; BAL27.C = 0  ; BAL28.P = 0 ; BAL28.C = 0
    BAL29.P = 0 ; BAL29.C = 0  ; BAL30.P = 0 ; BAL30.C = 0 ;BAL31.P = 0 ; BAL31.C = 0  ; BAL32.P = 0 ; BAL32.C = 0
    BAL33.P = 0 ; BAL33.C = 0  ; BAL34.P = 0 ; BAL34.C = 0 ;BAL35.P = 0 ; BAL35.C = 0  ; BAL36.P = 0 ; BAL36.C = 0
    BAL37.P = 0 ; BAL37.C = 0  ; BAL38.P = 0 ; BAL38.C = 0 ;BAL39.P = 0 ; BAL39.C = 0  ; BAL40.P = 0 ; BAL40.C = 0
    BAL41.P = 0 ; BAL41.C = 0  ; BAL42.P = 0 ; BAL42.C = 0 ;BAL43.P = 0 ; BAL43.C = 0  ; BAL44.P = 0 ; BAL44.C = 0
    BAL45.P = 0 ; BAL45.C = 0  ; BAL46.P = 0 ; BAL46.C = 0 ;BAL47.P = 0 ; BAL47.C = 0  ; BAL48.P = 0 ; BAL48.C = 0
    BAL49.P = 0 ; BAL49.C = 0  ; BAL50.P = 0 ; BAL50.C = 0 ; BAL51.P = 0; BAL51.C = 0  ; BAL52.P = 0 ; BAL52.C = 0
    BAL53.P = 0 ; BAL53.C = 0  ; BAL54.P = 0 ; BAL54.C = 0 ; BAL55.P = 0; BAL55.C = 0
    XX2  = SPACE(132)
    BB.DATA = ''
    AC.BAL13.OVER.33.D = '' ; XX13.OVER.33.D = '' ; AC.BAL1.OVER.33.D = ''
    XX2.OVER.33.D = '' ; AC.BAL2.OVER.33.D = '' ; XX3.OVER.33.D = ''
    AC.BAL4.OVER.33.D = '' ; XX4.OVER.33.D = '' ; AC.BAL5.OVER.33.D = ''
    XX5.OVER.33.D = '' ;AC.BAL6.OVER.33.D = '' ;XX6.OVER.33.D = ''
    AC.BAL7.OVER.33.D = '' ; XX7.OVER.33.D = '' ;AC.BAL8.OVER.33.D = ''
    XX8.OVER.33.D = '' ; AC.BAL9.OVER.33.D = '' ; XX9.OVER.33.D = ''
    XX9.OVER.33.D.P.C = '' ; XX8.OVER.33.D.P.C = '' ; XX7.OVER.33.D.P.C = '' ; XX6.OVER.33.D.P.C = '' ; XX5.OVER.33.D.P.C = ' '
    XX4.OVER.33.D.P.C = '' ; XX3.OVER.33.D.P.C = '' ; XX2.OVER.33.D.P.C = '' ; XX1.OVER.33.D.P.C = '' ; XX10.OVER.33.D.P.C = ' '
    XX11.OVER.33.D.P.C = '' ; XX12.OVER.33.D.P.C = '' ; XX13.OVER.33.D.P.C = '' ; XX14.OVER.33.D.P.C = ''
    AC.BAL10.OVER.33.D = '' ; XX10.OVER.33.D = '' ;AC.BAL11.OVER.33.D = ''
    XX11.OVER.33.D = '' ; AC.BAL12.OVER.33.D = '' ; XX12.OVER.33.D = '' ; XX12.OVER.33.D.OV = ''
    AC.BAL.OVER.33.D = '' ; AC.BAL3.OVER.33.D = '' ;XX1.OVER.33.D = ''
    AC.BAL13.OVER.44 = '' ;XX13.OVER.44 = '' ; AC.BAL1.OVER.44 = '' ; XX2.OVER.44 = ''
    AC.BAL2.OVER.44 = '' ; XX3.OVER.44 = '' ; AC.BAL4.OVER.44 = '' ; XX4.OVER.44 = ''
    AC.BAL5.OVER.44 = '' ; XX5.OVER.44 = '' ; AC.BAL6.OVER.44 = '' ; XX6.OVER.44 = ''
    AC.BAL7.OVER.44 = '' ; XX7.OVER.44 = '' ; AC.BAL8.OVER.44 = '' ; XX8.OVER.44 = ''
    AC.BAL9.OVER.44 = '' ; XX9.OVER.44 = '' ; AC.BAL10.OVER.44 = '' ;XX10.OVER.44 = '' ; AC.BAL11.OVER.44 = '' ;XX11.OVER.44 = '' ; AC.BAL12.OVER.44 = '' ;XX12.OVER.44 = '' ; XX12.OVER.44.OV = ''
    AC.BAL.OVER.44 = '' ;AC.BAL3.OVER.44 = '' ; XX1.OVER.44 = ''
    XX7.OVER.44 = '' ; AC.BAL4.LOAN2.P = '' ; AC.BAL5.LOAN2.P = '' ; AC.BAL6.LOAN2.P = ''
    XX8.OVER.44 = '' ; AC.BAL7.LOAN2.P = '' ; AC.BAL8.LOAN2.P = '' ; AC.BAL9.LOAN2.P = ''  ; AC.BAL14.LOAN2.P = ''
    XX9.OVER.44 = '' ; AC.BAL10.LOAN2.P = '' ; AC.BAL11.LOAN2.P = '' ; AC.BAL12.LOAN2.P = ''  ; AC.BAL13.LOAN2.P = ''
    XX10.OVER.44= '' ; XX4.LOAN2.P  = '' ; XX3.LOAN2.P = '' ; XX2.LOAN2.P = ''
    XX4.LOAN2.P.L  = '' ; XX3.LOAN2.P.L = '' ; XX2.LOAN2.P.L = ''; XX11.OVER.44= '' ; XX7.LOAN2.P.L  = '' ; XX6.LOAN2.P.L = '' ; XX5.LOAN2.P.L = '' ; XX5.LOAN2.P.L = '' ; XX4.LOAN2.P.L = '' ; XX3.LOAN2.P.L = ''
    XX12.OVER.44= '' ; XX12.OVER.44.OV = '' ;XX10.LOAN2.P = '' ; XX9.LOAN2.P = '' ; XX8.LOAN2.P = ''
    XX13.OVER.44= '' ; XX13.LOAN2.P = '' ;XX12.LOAN2.P = '' ; XX11.LOAN2.P = '' ; XX13.LOAN2.P.L = '' ;XX12.LOAN2.P.L = '' ; XX11.LOAN2.P.L = ''
    AC.BAL14 = '' ; AC.BAL1 = '' ; AC.BAL2 = '' ; AC.BAL4 = ''  ; XX2.LOAN1.CHQ = ' '  ;  XX1.LOAN1.CHQ = ' ' ;  XX3.LOAN1.CHQ = ' ' ; XX4.LOAN1.CHQ = ' ' ; XX5.LOAN1.CHQ = ' '  ; XX6.LOAN1.CHQ = ' ' ;XX7.LOAN1.CHQ = ' ' ;XX8.LOAN1.CHQ = ' ' ;XX9.LOAN1.CHQ = ' '
    XX10.LOAN1.CHQ = ' ' ; XX11.LOAN1.CHQ = ' ' ; XX12.LOAN1.CHQ = ' ' ; XX13.LOAN1.CHQ = ' ' ; XX14.LOAN1.CHQ = ' '
    AC.BAL5 = '' ; AC.BAL6 = '' ; AC.BAL7 = '' ;AC.BAL8 = '' ; XX12.LOAN2.OV.P.L = ''
    AC.BAL10 = '' ; AC.BAL11 = '';AC.BAL12 = '' ; AC.BAL3 = ''
    AC.BAL130.LOAN2.P = '' ; AC.BAL130.LOAN2 = '' ; AC.BAL10.LOAN2.P = '' ; AC.BAL10.LOAN2 = '' ; AC.BAL20.LOAN2.P = '' ; AC.BAL20.LOAN2 = '' ;AC.BAL40.LOAN2.P = '' ; AC.BAL40.LOAN2 = '' ; AC.BAL50.LOAN2.P = '' ;AC.BAL50.LOAN2 = '' ; AC.BAL60.LOAN2.P = '' ;  AC.BAL60.LOAN2 = '' ; AC.BAL70.LOAN2 = '' ; AC.BAL80.LOAN2.P = '' ; AC.BAL80.LOAN2 = '' ; AC.BAL90.LOAN2.P = '' ; AC.BAL90.LOAN2 = '' ; AC.BAL100.LOAN2.P = '' ; AC.BAL100.LOAN2 = '' ; AC.BAL110.LOAN2.P = '' ;  AC.BAL110.LOAN2 = '' ; AC.BAL120.LOAN2.P = '' ; AC.BAL120.LOAN2 = '' ; AC.BAL120.LOAN2.OV.P = '' ; AC.BAL120.LOAN2.OV = '' ;AC.BAL30.LOAN2.P= '' ; AC.BAL30.LOAN2 = '' ; AC.BAL130.LOAN2.P.P.D= ''; AC.BAL130.LOAN2.P.D = '' ; AC.BAL10.LOAN2.P.P.D = '' ; AC.BAL10.LOAN2.P.D = '' ; AC.BAL20.LOAN2.P.P.D = '' ; AC.BAL20.LOAN2.P.D = '' ; AC.BAL40.LOAN2.P.P.D = '' ; AC.BAL40.LOAN2.P.D = '' ; AC.BAL50.LOAN2.P.P.D = '' ; AC.BAL50.LOAN2.P.D = '' ; AC.BAL60.LOAN2.P.P.D = '' ; AC.BAL60.LOAN2.P.D = '' ; AC.BAL70.LOAN2.P.D = '' ; AC.BAL80.LOAN2.P.P.D = '' ; AC.BAL80.LOAN2.P.D = '' ; AC.BAL90.LOAN2.P.P.D = '' ; AC.BAL90.LOAN2.P.D = '' ; AC.BAL100.LOAN2.P.P.D = '' ; AC.BAL100.LOAN2.P.D = '' ; AC.BAL110.LOAN2.P.P.D = '' ; AC.BAL110.LOAN2.P.D = '' ; AC.BAL120.LOAN2.P.P.D = '' ; AC.BAL120.LOAN2.P.D = '' ; AC.BAL120.LOAN2.OV.P.P.D = '' ; AC.BAL120.LOAN2.OV.P.D = '' ; AC.BAL30.LOAN2.P.P.D = '' ;AC.BAL30.LOAN2.P.D = '' ;AC.BAL13.LOAN2.DD = '' ; AC.BAL1.LOAN2.DD = '' ; AC.BAL2.LOAN2.DD = '' ; AC.BAL4.LOAN2.DD = '' ; AC.BAL5.LOAN2.DD = '' ; AC.BAL6.LOAN2.DD = '' ; AC.BAL7.LOAN2.DD = '' ; AC.BAL8.LOAN2.DD = '' ;AC.BAL9.LOAN2.DD = '' ; AC.BAL10.LOAN2.DD = '' ;AC.BAL12.LOAN2.DD.OV = '' ; AC.BAL11.LOAN2.DD = '';AC.BAL12.LOAN2.DD = '' ; AC.BAL3.LOAN2.DD = '' ; AC.BAL13.LOAN2.DD.P.D = ''; AC.BAL1.LOAN2.DD.P.D = '' ; AC.BAL2.LOAN2.DD.P.D = '' ;   AC.BAL4.LOAN2.DD.P.D = '' ; AC.BAL5.LOAN2.DD.P.D = '' ; AC.BAL6.LOAN2.DD.P.D = '' ; AC.BAL7.LOAN2.DD.P.D = '' ; AC.BAL8.LOAN2.DD.P.D = '' ; AC.BAL9.LOAN2.DD.P.D = '' ;AC.BAL10.LOAN2.DD.P.D = '' ; AC.BAL11.LOAN2.DD.P.D = ''; AC.BAL12.LOAN2.DD.P.D = '' ; AC.BAL12.LOAN2.DD.OV.P.D = '' ; AC.BAL3.LOAN2.DD.P.D = '' ; XX13.LOAN2.DD = '' ; XX2.LOAN2.DD = '' ;XX3.LOAN2.DD = '' ; XX4.LOAN2.DD = '' ;XX5.LOAN2.DD = '' ;XX6.LOAN2.DD = '' ;XX7.LOAN2.DD = '' ;XX8.LOAN2.DD = '' ;XX9.LOAN2.DD = '' ;XX10.LOAN2.DD = '' ;XX11.LOAN2.DD = '' ; XX12.LOAN2.DD = '' ; XX12.LOAN2.DD.OV = '' ; XX1.LOAN2.DD = '' ;XX13.LOAN2.DD.P.D = '' ;   XX1.LOAN2.DD.P.D = '' ; XX2.LOAN2.DD.P.D = '' ;  XX3.LOAN2.DD.P.D = '' ;  XX4.LOAN2.DD.P.D = '' ; XX5.LOAN2.DD.P.D = '' ;  XX6.LOAN2.DD.P.D = '' ; XX7.LOAN2.DD.P.D = '' ; XX8.LOAN2.DD.P.D = '' ;  XX9.LOAN2.DD.P.D = '' ; XX10.LOAN2.DD.P.D = '' ;  XX11.LOAN2.DD.P.D = '' ;  XX12.LOAN2.DD.P.D = '' ;  XX12.LOAN2.DD.OV.P.D = ''
    AC1 = '' ; AC2 = '' ; AC3 = '' ; AC4 = '' ; AC5 = '' ; AC6 = ''
    AC7 = '' ; AC8 = '' ; AC9 = '' ; AC10 = '' ; AC11 = '' ; AC12 = '' ; AC.BAL100.LOAN2.P.D = ''  ;AC.BAL13.LOAN2.DD.P.D = '' ;AC.BAL11.LOAN2.DD.P.D = '' ;AC.BAL12.LOAN2.DD.OV.P.D = '' ;AC.BAL12.LOAN2.DD.P.D = '' ;AC.BAL120.LOAN2.OV.P.D = '' ;XX12.OVER.33.D.OV = '' ;AC.BAL12.OVER.33.D.OV = ''
    AC13 = '' ; AC14 = '' ; AC15 = '' ; AC16 = '';AC.BAL.16 = ''
    AC.BAL14.MM = '' ; XX14 = '' ; AC.BAL1.MM = '' ; XX3 = '';
    AC.BAL2.MM = '' ; XX4 = '' ; AC.BAL4.MM = '' ; XX5 = ''
    AC.BAL5.MM = '' ; XX6 = '' ; AC.BAL6.MM = '' ; XX7 = ''
    AC.BAL7.MM = '' ; XX8 = '' ; AC.BAL8.MM = '' ; XX9 = ''
    AC.BAL9.MM = '' ; XX10 = '' ; AC.BAL10.MM = '' ; XX11 = ''
    AC.BAL11.MM = '' ; XX12 = '' ; AC.BAL12.MM = '' ; XX13 = ''
    AC.BAL20.MM = '' ; XX20 = '' ; AC.BAL3.MM = '' ; XX1.16 = '' ; AC.BAL6.LOAN2.DD.P.D = ''
    AC.BAL13.LOAN2 = '' ; AC13 = '' ; AC.BAL1.LOAN2 = '' ; AC14 = ''
    XX3.LOAN2 = '' ; XX2.LOAN2 = '' ; XX13.LOAN2 = '' ; AC.BAL2.LOAN2 = ''
    AC15 = '' ; AC.BAL4.LOAN2 = '' ; AC16 = '' ; XX4.LOAN2 = ''  ; XX4.LOAN2.P.L = ''
    AC17 = '' ; AC.BAL5.LOAN2 = '' ; XX5.LOAN2 = ''; AC18 = '' ; AC.BAL6.LOAN2 = '' ; XX6.LOAN2 = ''
    AC17.P.L = '' ; AC.BAL5.LOAN2.P.L = '' ; XX5.LOAN2.P.L = ''; AC18.P.L = '' ; AC.BAL6.LOAN2.P.L = '' ; XX6.LOAN2.P.L = ''
    AC19.P.L = '' ; AC.BAL7.LOAN2.P.L = '' ; XX7.LOAN2.P.L = ''; AC20 = '' ; AC.BAL8.LOAN2 = '' ; XX8.LOAN2 = ''
    AC19= '' ; AC.BAL7.LOAN2 = '' ; XX7.LOAN2 = ''
    AC.BAL100.LOAN2.P.D = 0 ;  AC.BAL12.LOAN2.DD.OV.P.D =  0 ; AC.BAL11.LOAN2.DD.P.D = 0 ; AC.BAL12.LOAN2.DD.P.D = 0 ;AC.BAL10.LOAN2.DD.P.D = '' ; AC.BAL1.LOAN2.DD.P.D = '' ; XX12.LOAN2.DD.OV.P.D = '' ; AC.BAL40.LOAN2.P.D = '' ; AC.BAL3.LOAN2.DD.P.D = '' ;XX13.LOAN2.DD.P.D = '' ; XX2.LOAN2.DD.P.D = '' ; XX3.LOAN2.DD.P.D = '' ; XX4.LOAN2.DD.P.D = '' ; XX5.LOAN2.DD.P.D = '' ; XX6.LOAN2.DD.P.D = '' ; XX7.LOAN2.DD.P.D = '' ;  XX8.LOAN2.DD.P.D = '' ; XX9.LOAN2.DD.P.D = '' ; XX10.LOAN2.DD.P.D = '' ; XX11.LOAN2.DD.P.D = '' ; XX12.LOAN2.DD.P.D = '' ;XX12.LOAN2.DD.OV.P.D = '' ; XX1.LOAN2.DD.P.D = '' ; AC.BAL5.LOAN2.DD.P.D = '' ; AC.BAL9.LOAN2.DD.P.D = '' ; AC.BAL8.LOAN2.DD.P.D = '' ; AC.BAL10.LOAN2.DD.P.D = '' ;AC.BAL130.LOAN2.P.D = '' ; AC.BAL1.LOAN2.DD.P.D = '' ; AC.BAL10.LOAN2.P.D = '' ; AC.BAL2.LOAN2.DD.P.D = '' ; AC.BAL20.LOAN2.P.D = ''; AC.BAL4.LOAN2.DD.P.D = '' ; AC.BAL40.LOAN2.P.D = '' ; AC.BAL50.LOAN2.P.D = '' ; AC.BAL60.LOAN2.P.D = '' ; AC.BAL70.LOAN2.P.D = '' ; AC.BAL80.LOAN2.P.D = '' ; AC.BAL90.LOAN2.P.D = '' ; AC.BAL110.LOAN2.P.D = '' ; AC.BAL120.LOAN2.P.D = '' ;AC.BAL3.LOAN2.DD.P.D = '' ; AC.BAL3.LOAN1.CHQ = ''
    AC21 = '' ; AC.BAL9.LOAN2 = '' ; XX9.LOAN2 = ''   ; AC.BAL7.LOAN2.DD.P.D = '' ; XX12.OVER.33.D.OV= '' ;AC.BAL12.OVER.33.D.OV.P.C = ''
    AC21.P.L = '' ; AC.BAL9.LOAN2.P.L = '' ; XX9.LOAN2.P.L = ''
    AC22 = '' ; AC.BAL10.LOAN2 = '' ; XX10.LOAN2 = '' ; XX2.LOAN1.CHQ = ' '  ;  XX1.LOAN1.CHQ = ' ' ;  XX3.LOAN1.CHQ = ' ' ; XX4.LOAN1.CHQ = ' ' ; XX5.LOAN1.CHQ = ' '  ; XX6.LOAN1.CHQ = ' ' ;XX7.LOAN1.CHQ = ' ' ;XX8.LOAN1.CHQ = ' ' ;XX9.LOAN1.CHQ = ' '
    AC13.P.L = '' ; AC.BAL13.LOAN2.P.L = ''; XX13.LOAN2.P.L = ''
    AC14.P.L = '' ; AC.BAL1.LOAN2.P.L = '' ; XX2.LOAN2.P.L = ''; AC15.P.L = '' ; AC.BAL2.LOAN2.P.L = '' ; XX3.LOAN2.P.L = ''; AC16.P.L = '' ; AC.BAL4.LOAN2.P.L = '' ; XX4.LOAN2.P.L = ''; AC17.P.L = '' ; AC.BAL5.LOAN2.P.L = '' ; XX5.LOAN2.P.L = ''; AC18.P.L = '' ; AC.BAL6.LOAN2.P.L = '' ; XX6.LOAN2.P.L = ''; AC19.P.L = '' ; AC.BAL7.LOAN2.P.L = '' ; XX7.LOAN2.P.L = ''; AC20.P.L = '' ; AC.BAL8.LOAN2.P.L = '' ; XX8.LOAN2.P.L = ''; AC21.P.L = '' ; AC.BAL9.LOAN2.P.L = '' ; XX9.LOAN2.P.L = ''; AC22.P.L = '' ; AC.BAL10.LOAN2.P.L = '' ; XX10.LOAN2.P.L = ''; AC23.P.L = '' ; AC.BAL11.LOAN2.P.L = '' ; XX11.LOAN2.P.L = ''; AC24.P.L = '' ; AC.BAL12.LOAN2.P.L = '' ; XX12.LOAN2.P.L = ''; AC25.P.L = '' ; AC.BAL13.LOAN2.P.L = '' ; XX13.LOAN2.P.L = ''; AC26.P.L = '' ; AC.BAL14.LOAN2.P.L = '' ; XX14.LOAN2.P.L = ''; AC27.P.L = '' ; AC.BAL15.LOAN2.P.L = '' ; XX15.LOAN2.P.L = ''; AC28.P.L = '' ; AC.BAL16.LOAN2.P.L = '' ; XX16.LOAN2.P.L = ''; AC29.P.L = '' ; AC.BAL17.LOAN2.P.L = '' ; XX17.LOAN2.P.L = ''; AC30.P.L = '' ; AC.BAL18.LOAN2.P.L = '' ; XX18.LOAN2.P.L = ''
    XX10.LOAN1.CHQ = ' ' ; XX11.LOAN1.CHQ = ' ' ; XX12.LOAN1.CHQ = ' ' ; XX13.LOAN1.CHQ = ' ' ; XX14.LOAN1.CHQ = ' '
    AC23 = '' ; AC.BAL11.LOAN2 = '' ; XX11.LOAN2 = ''
    AC24 = '' ; AC.BAL12.LOAN2 = '' ; XX12.LOAN2 = ''
    AC24.OV = '' ; AC.BAL12.LOAN2.OV = '' ; XX12.LOAN2.OV = '' ; AC.BAL12.LOAN2.OV.P.L = ''
    AC.BAL.LOAN2 = '' ; AC.BAL3.LOAN2 = ''; XX1.LOAN2 = '' ; XX1.LOAN2.P.L = '' ; AC.BAL3.LOAN2.P.L = ''
    AC.BAL13.LOAN2.P = '' ; XX13.LOAN2.P = '' ; AC.BAL1.LOAN2.P = '' ; XX2.LOAN2.P = ''
    AC.BAL2.LOAN2.P = '' ; XX3.LOAN2.P = '' ; AC.BAL4.LOAN2.P = '' ; XX4.LOAN2.P = ''
    AC.BAL5.LOAN2.P = '' ; XX5.LOAN2.P = '' ; AC.BAL6.LOAN2.P = '' ; XX6.LOAN2.P = ''
    AC.BAL7.LOAN2.P = '' ; XX7.LOAN2.P = '' ; AC.BAL8.LOAN2.P = '' ; XX8.LOAN2.P = ''
    AC.BAL9.LOAN2.P = '' ; XX9.LOAN2.P = '' ; AC.BAL10.LOAN2.P = '' ; XX10.LOAN2.P = '' ; AC.BAL11.LOAN2.P = '' ; XX11.LOAN2.P = ''
    AC.BAL12.LOAN2.P = '' ; XX12.LOAN2.P = '' ; AC.BAL.LOAN2.P = '' ; AC.BAL3.LOAN2.P = ''; XX1.LOAN2.P = '' ; XX1.LOAN2.P.L=''
    AC25 = '' ; AC.BAL13.LOAN1 = '' ; XX13.LOAN1 = '' ; AC26 = '' ; AC.BAL1.LOAN1 = ''  ; XX2.LOAN1 = ''
    AC25.EZN = '' ; AC.BAL13.LOAN1.EZN = '' ; XX13.LOAN1.EZN = '' ; AC26.EZN = '' ; AC.BAL1.LOAN1.EZN = ''  ; XX2.LOAN1.EZN = ''
    AC27 = '' ; AC.BAL2.LOAN1 = ''  ; XX3.LOAN1 = '' ; AC28 = '' ; AC.BAL4.LOAN1 = ''  ; XX4.LOAN1 = ''
    AC27.EZN = '' ; AC.BAL2.LOAN1.EZN = ''  ; XX3.LOAN1.EZN = '' ; AC28.EZN = '' ; AC.BAL4.LOAN1.EZN = ''  ; XX4.LOAN1.EZN = ''
    AC29 = '' ; AC.BAL5.LOAN1 = ''  ; XX5.LOAN1 = '' ; AC30 = '' ; AC.BAL6.LOAN1 = ''  ; XX6.LOAN1 = '' ;  AC31 = '' ; AC.BAL7.LOAN1 = ''  ; XX7.LOAN1 = ''
    AC29.EZN = '' ; AC.BAL5.LOAN1.EZN = ''  ; XX5.LOAN1.EZN = '' ; AC30.EZN = '' ; AC.BAL6.LOAN1.EZN = ''  ; XX6.LOAN1.EZN = '' ;  AC31.EZN = '' ; AC.BAL7.LOAN1.EZN = ''  ; XX7.LOAN1.EZN = ''
    AC32 = '' ; AC.BAL8.LOAN1 = ''  ; XX8.LOAN1 = '' ; AC33 = '' ; AC.BAL9.LOAN1 = ''  ; XX9.LOAN1 = ''; AC34 = '' ; AC.BAL10.LOAN1 = ''  ; XX10.LOAN1 = ''
    AC32.EZN = '' ; AC.BAL8.LOAN1.EZN = ''  ; XX8.LOAN1.EZN = '' ; AC33.EZN = '' ; AC.BAL9.LOAN1.EZN = ''  ; XX9.LOAN1.EZN = ''; AC34.EZN = '' ; AC.BAL10.LOAN1.EZN = ''  ; XX10.LOAN1.EZN = ''
    AC35 = '' ; AC.BAL11.LOAN1 = ''  ; XX11.LOAN1 = '' ; AC36 = '' ; AC.BAL12.LOAN1 = ''  ;AC.BAL12.LOAN1.OV = '' ; XX12.LOAN1 = ''
    AC35.EZN = '' ; AC.BAL11.LOAN1.EZN = ''  ; XX11.LOAN1.EZN = '' ; AC36.EZN = '' ; AC.BAL12.LOAN1.EZN = ''  ;AC.BAL12.LOAN1.EZN.OV = ''; XX12.LOAN1.EZN = ''
    XX12.LOAN1.OV = '' ;  XX12.LOAN1.OV = '' ; XX12.LOAN1.OV.EZN = ''
    XX1.LOAN1 = '' ; AC.BAL.LOAN1 = '' ; AC.BAL3.LOAN1 = ''
    AC.BAL13.D = '' ; AC.BAL1.D = '' ; AC.BAL2.D = ''; AC.BAL4.D = '' ;AC.BAL5.D = '' ; AC.BAL6.D = ''
    AC.BAL7.D = '' ;AC.BAL8.D = '' ; AC.BAL9.D = '' ; AC.BAL10.D = '' ;AC.BAL11.D = '' ; AC.BAL12.D = '' ;AC.BAL = '' ; AC.BAL3.D = '' ; AC.BAL13.MM.D = '' ; XX13.D = '' ; AC.BAL1.MM.D = ''
    XX2.D = '' ; AC.BAL2.MM.D = '' ; XX3.D = '' ; AC.BAL4.MM.D = '' ; XX4.D = '' ; AC.BAL5.MM.D = '' ; XX5.D = '' ; AC.BAL6.MM.D = '' ; XX6.D = ''
    AC.BAL7.MM.D = '' ; XX7.D = '' ;AC.BAL8.MM.D = '' ; XX8.D = '' ; AC.BAL9.MM.D = '' ; XX9.D = '' ; AC.BAL10.MM.D = '' ; XX10.D = ''
    AC.BAL11.MM.D = '' ; XX11.D = '' ;  AC.BAL12.MM.D = '' ; XX12.D = ''
    AC.BAL3.MM.D = '' ; XX1.D = '' ; AC.BAL130.LOAN2 = '' ; AC.BAL10.LOAN2 = ''; AC.BAL20.LOAN2 = '' ; AC.BAL40.LOAN2 = '' ; AC.BAL50.LOAN2 = '' ; AC.BAL60.LOAN2 = '' ; AC.BAL70.LOAN2 = '' ; AC.BAL80.LOAN2 = '' ; AC.BAL90.LOAN2 = '' ; AC.BAL100.LOAN2 = '' ; AC.BAL110.LOAN2 = ''
    AC.BAL120.LOAN2 = '' ; AC.BAL30.LOAN2 = '' ;AC.BAL30.LOAN2.P.D = '' ;AC.BAL13.LOAN2.DD = '' ;XX13.LOAN2.DD = '' ;AC.BAL1.LOAN2.DD = '' ; XX2.LOAN2.DD = '' ; AC.BAL2.LOAN2.DD = '' ;XX3.LOAN2.DD = ''
    XX1.OVER.33.D = ''
    XX2.OVER.33.D = '' ; XX3.OVER.33.D = ''; XX4.OVER.33.D = ''; XX5.OVER.33.D = '' ; XX6.OVER.33.D = ''; XX7.OVER.33.D = ''; XX8.OVER.33.D = ''
    XX9.OVER.33.D = '' ; XX10.OVER.33.D = ''; XX11.OVER.33.D = ''
    XX1.OVER.33.D.P.C = ''
    XX2.OVER.33.D.P.C = '' ; XX3.OVER.33.D.P.C = ''; XX4.OVER.33.D.P.C = ''; XX5.OVER.33.D.P.C = '' ; XX6.OVER.33.D.P.C = ''; XX7.OVER.33.D.P.C = ''; XX8.OVER.33.D.P.C = ''
    XX9.OVER.33.D.P.C = '' ; XX10.OVER.33.D.P.C = ''; XX11.OVER.33.D.P.C = ''
    XX12.OVER.33.D = '';XX12.OVER.33.D.OV  = '' ;XX13.OVER.33.D = '' ; XX1.LOAN2.DD = '' ; XX2.LOAN2.DD = '' ; XX1.LOAN2.P.L = ''
    XX3.LOAN2.DD = ''; XX4.LOAN2.DD = '' ; XX5.LOAN2.DD = ''; XX6.LOAN2.DD = ''; XX7.LOAN2.DD = '' ; XX8.LOAN2.DD = '' ; XX9.LOAN2.DD = '' ; XX10.LOAN2.DD = '' ; XX11.LOAN2.DD = '' ; XX12.LOAN2.DD = '' ; XX13.LOAN2.DD = ''
    XX1.D = '' ; XX2.D = '' ; XX3.D = '' ; XX4.D = '' ; XX5.D = '' ; XX6.D = ''
    XX7.D = '' ; XX8.D = '' ; XX9.D = '' ; XX10.D = '' ; XX11.D = '' ; XX12.D = '' ; XX13.D = ''
    AC.BAL2.D      = '' ; AC.BAL12.LOAN2.DD62.OV = '' ; XX11.OVER.33.D = '' ;AC.BAL120.LOAN102.OV = ''
    YDAYS.D        = '' ; XX4.LOAN2.DD62 = '' ; AC.BAL9.LOAN2 = ''
    DAYS.D         = ''
    AC.BAL4.LOAN2.DD = '' ; XX4.LOAN2.DD = '' ; AC.BAL5.LOAN2.DD = ''  XX5.LOAN2.DD = '' ; AC.BAL6.LOAN2.DD = '' ; XX6.LOAN2.DD = ''
    AC.BAL7.LOAN2.DD = '' ; XX7.LOAN2.DD = '' ;AC.BAL8.LOAN2.DD = ''
    XX8.LOAN2.DD = '' ;AC.BAL9.LOAN2.DD = '' ; XX9.LOAN2.DD = ''
    AC.BAL10.LOAN2.DD = '' ;XX10.LOAN2.DD = '' ; AC.BAL11.LOAN2.DD = '' ; XX11.LOAN2.DD = '' ;  AC.BAL12.LOAN2.DD = '' ; XX12.LOAN2.DD = ''
    AC.BAL12.LOAN2.DD.OV = '' ; XX12.LOAN2.DD.OV = '' ; AC.BAL.LOAN2.DD = '' ; AC.BAL3.LOAN2.DD = '' ; XX1.LOAN2.DD = '' ; XX1.LOAN2.P.L = ''
    AC.BAL130.LOAN102 = '' ; AC.BAL10.LOAN102 = '' ;AC.BAL120.LOAN102= ''
    AC.BAL20.LOAN102 = '' ; AC.BAL30.LOAN102 = '' ; AC.BAL40.LOAN102 = ''
    AC.BAL50.LOAN102 = '' ; AC.BAL60.LOAN102 = '' ; AC.BAL70.LOAN102 =''
    AC.BAL80.LOAN102 = '' ; AC.BAL90.LOAN102 = '' ; AC.BAL110.LOAN102 = ''
    AC.BAL13.LOAN2.DD62 = '' ; XX13.LOAN2.DD62 = ''; AC.BAL1.LOAN2.DD62 = '' ; XX2.LOAN2.DD62 = ''
    AC.BAL2.LOAN2.DD62 = '' ; XX3.LOAN2.DD62 = ''; AC.BAL4.LOAN2.DD62 = '' ; XX4.LOAN2.DD62 = ''
    AC.BAL5.LOAN2.DD62 = '' ; XX5.LOAN2.DD62 = ''; AC.BAL6.LOAN2.DD62 = '' ; XX6.LOAN2.DD62 = ''; AC.BAL7.LOAN2.DD62 = '' ; XX7.LOAN2.DD62 = ''
    AC.BAL8.LOAN2.DD62 = '' ; XX8.LOAN2.DD62 = ''; AC.BAL9.LOAN2.DD62 = '' ; XX9.LOAN2.DD62 = '' ; AC.BAL10.LOAN2.DD62 = '' ; XX10.LOAN2.DD62 = ''; AC.BAL11.LOAN2.DD62 = '' ; XX11.LOAN2.DD62 = ''
    AC.BAL12.LOAN2.DD62 = '' ; XX12.LOAN2.DD62 = '' ; AC.BAL12.LOAN2.DD62.OV = '' ; XX12.LOAN2.DD62.OV = ''
    AC.BAL.LOAN2.DD62 = '' ;AC.BAL3.LOAN2.DD62 = '' ;XX1.LOAN2.DD62 = '' ; XX1.LOAN2.P.L= ''

    T.SEL.CUR = "SELECT FBNK.CURRENCY BY @ID"
***    T.SEL.CUR = "SELECT FBNK.CURRENCY WITH @ID EQ EGP USD  "
    CALL EB.READLIST(T.SEL.CUR,KEY.LIST.CUR,"",SELECTED.CUR,ER.MSG)
    FOR KKKK = 1 TO SELECTED.CUR
        CURR.ID = KEY.LIST.CUR<KKKK>

        XXX1  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX11 = SPACE(132) ; XXX12 = SPACE(132)
        XXX2  = SPACE(132)  ; XXX5  = SPACE(132)  ; XXX10 = SPACE(132) ; XXX13 = SPACE(132)
        XXX3  = SPACE(132)  ; XXX4  = SPACE(132)  ; XXX6  = SPACE(132) ; XXX7  = SPACE(132)
        XXX15 = SPACE(132)  ; XXX16 = SPACE(132)  ; XXX17 = SPACE(132) ; XXX14 = SPACE(132)
        XXX21 = SPACE(132)  ; XXX19 = SPACE(132)  ; XXX18 = SPACE(132) ; XXX20 = SPACE(132)
        XXX6  = SPACE(132)  ; XXX7  = SPACE(132)  ; XXX8  = SPACE(132) ; XXX9 = SPACE(132)
        BAL1.1.P = 0 ; BAL14.1.P  = ''  ; BAL1.1.C  = 0 ; BAL14.1.C  = ''
        BAL1.P  = 0 ; BAL1.C  = 0  ; BAL2.P  = 0 ; BAL2.C  = 0 ;BAL3.P  = 0 ; BAL3.C  = 0  ; BAL4.P  = 0 ; BAL4.C  = 0
        BAL5.P  = 0 ; BAL5.C  = 0  ; BAL6.P  = 0 ; BAL6.C  = 0 ;BAL7.P  = 0 ; BAL7.C  = 0  ; BAL8.P  = 0 ; BAL8.C  = 0
        BAL9.P  = 0 ; BAL9.C  = 0  ; BAL10.P = 0 ; BAL10.C = 0 ;BAL11.P = 0 ; BAL11.C = 0  ; BAL12.P = 0 ; BAL12.C = 0
        BAL13.P = 0 ; BAL13.C = 0  ; BAL14.P = 0 ; BAL14.C = 0 ;BAL15.P = 0 ; BAL15.C = 0  ; BAL16.P = 0 ; BAL16.C = 0
        BAL17.P = 0 ; BAL17.C = 0  ; BAL18.P = 0 ; BAL18.C = 0 ;BAL19.P = 0 ; BAL19.C = 0  ; BAL20.P = 0 ; BAL20.C = 0
        BAL21.P = 0 ; BAL21.C = 0  ; BAL22.P = 0 ; BAL22.C = 0 ;BAL23.P = 0 ; BAL23.C = 0  ; BAL24.P = 0 ; BAL24.C = 0
        BAL25.P = 0 ; BAL25.C = 0  ; BAL26.P = 0 ; BAL26.C = 0 ;BAL27.P = 0 ; BAL27.C = 0  ; BAL28.P = 0 ; BAL28.C = 0
        BAL29.P = 0 ; BAL29.C = 0  ; BAL30.P = 0 ; BAL30.C = 0 ;BAL31.P = 0 ; BAL31.C = 0  ; BAL32.P = 0 ; BAL32.C = 0
        BAL33.P = 0 ; BAL33.C = 0  ; BAL34.P = 0 ; BAL34.C = 0 ;BAL35.P = 0 ; BAL35.C = 0  ; BAL36.P = 0 ; BAL36.C = 0
        BAL37.P = 0 ; BAL37.C = 0  ; BAL38.P = 0 ; BAL38.C = 0 ;BAL39.P = 0 ; BAL39.C = 0  ; BAL40.P = 0 ; BAL40.C = 0
        BAL41.P = 0 ; BAL41.C = 0  ; BAL42.P = 0 ; BAL42.C = 0 ;BAL43.P = 0 ; BAL43.C = 0  ; BAL44.P = 0 ; BAL44.C = 0
        BAL45.P = 0 ; BAL45.C = 0  ; BAL46.P = 0 ; BAL46.C = 0 ;BAL47.P = 0 ; BAL47.C = 0  ; BAL48.P = 0 ; BAL48.C = 0
        BAL49.P = 0 ; BAL49.C = 0  ; BAL50.P = 0 ; BAL50.C = 0 ; BAL51.P = 0; BAL51.C = 0  ; BAL52.P = 0 ; BAL52.C = 0
        BAL53.P = 0 ; BAL53.C = 0  ; BAL54.P = 0 ; BAL54.C = 0 ; BAL55.P = 0; BAL55.C = 0
        XX2  = SPACE(132)
        AC.BAL130.LOAN2.P = '' ; AC.BAL130.LOAN2 = '' ; AC.BAL10.LOAN2.P = '' ; AC.BAL10.LOAN2 = '' ; AC.BAL20.LOAN2.P = '' ; AC.BAL20.LOAN2 = '' ;AC.BAL40.LOAN2.P = '' ; AC.BAL40.LOAN2 = '' ; AC.BAL50.LOAN2.P = '' ;AC.BAL50.LOAN2 = '' ; AC.BAL60.LOAN2.P = '' ;  AC.BAL60.LOAN2 = '' ; AC.BAL70.LOAN2 = '' ; AC.BAL80.LOAN2.P = '' ; AC.BAL80.LOAN2 = '' ; AC.BAL90.LOAN2.P = '' ; AC.BAL90.LOAN2 = '' ; AC.BAL100.LOAN2.P = '' ; AC.BAL100.LOAN2 = '' ; AC.BAL110.LOAN2.P = '' ;  AC.BAL110.LOAN2 = '' ; AC.BAL120.LOAN2.P = '' ; AC.BAL120.LOAN2 = '' ; AC.BAL120.LOAN2.OV.P = '' ; AC.BAL120.LOAN2.OV = '' ;AC.BAL30.LOAN2.P= '' ; AC.BAL30.LOAN2 = '' ; AC.BAL130.LOAN2.P.P.D= ''; AC.BAL130.LOAN2.P.D = '' ; AC.BAL10.LOAN2.P.P.D = '' ; AC.BAL10.LOAN2.P.D = '' ; AC.BAL20.LOAN2.P.P.D = '' ; AC.BAL20.LOAN2.P.D = '' ; AC.BAL40.LOAN2.P.P.D = '' ; AC.BAL40.LOAN2.P.D = '' ; AC.BAL50.LOAN2.P.P.D = '' ; AC.BAL50.LOAN2.P.D = '' ; AC.BAL60.LOAN2.P.P.D = '' ; AC.BAL60.LOAN2.P.D = '' ; AC.BAL70.LOAN2.P.D = '' ; AC.BAL80.LOAN2.P.P.D = '' ; AC.BAL80.LOAN2.P.D = '' ; AC.BAL90.LOAN2.P.P.D = '' ; AC.BAL90.LOAN2.P.D = '' ; AC.BAL100.LOAN2.P.P.D = '' ; AC.BAL100.LOAN2.P.D = '' ; AC.BAL110.LOAN2.P.P.D = '' ; AC.BAL110.LOAN2.P.D = '' ; AC.BAL120.LOAN2.P.P.D = '' ; AC.BAL120.LOAN2.P.D = '' ; AC.BAL120.LOAN2.OV.P.P.D = '' ; AC.BAL120.LOAN2.OV.P.D = '' ; AC.BAL30.LOAN2.P.P.D = '' ;AC.BAL30.LOAN2.P.D = '' ;AC.BAL13.LOAN2.DD = '' ; AC.BAL1.LOAN2.DD = '' ; AC.BAL2.LOAN2.DD = '' ; AC.BAL4.LOAN2.DD = '' ; AC.BAL5.LOAN2.DD = '' ; AC.BAL6.LOAN2.DD = '' ; AC.BAL7.LOAN2.DD = '' ; AC.BAL8.LOAN2.DD = '' ;AC.BAL9.LOAN2.DD = '' ; AC.BAL10.LOAN2.DD = '' ;AC.BAL12.LOAN2.DD.OV = '' ; AC.BAL11.LOAN2.DD = '';AC.BAL12.LOAN2.DD = '' ; AC.BAL3.LOAN2.DD = '' ; AC.BAL13.LOAN2.DD.P.D = ''; AC.BAL1.LOAN2.DD.P.D = '' ; AC.BAL2.LOAN2.DD.P.D = '' ;   AC.BAL4.LOAN2.DD.P.D = '' ; AC.BAL5.LOAN2.DD.P.D = '' ; AC.BAL6.LOAN2.DD.P.D = '' ; AC.BAL7.LOAN2.DD.P.D = '' ; AC.BAL8.LOAN2.DD.P.D = '' ; AC.BAL9.LOAN2.DD.P.D = '' ;AC.BAL10.LOAN2.DD.P.D = '' ; AC.BAL11.LOAN2.DD.P.D = ''; AC.BAL12.LOAN2.DD.P.D = '' ; AC.BAL12.LOAN2.DD.OV.P.D = '' ; AC.BAL3.LOAN2.DD.P.D = '' ; XX13.LOAN2.DD = '' ; XX2.LOAN2.DD = '' ;XX3.LOAN2.DD = '' ; XX4.LOAN2.DD = '' ;XX5.LOAN2.DD = '' ;XX6.LOAN2.DD = '' ;XX7.LOAN2.DD = '' ;XX8.LOAN2.DD = '' ;XX9.LOAN2.DD = '' ;XX10.LOAN2.DD = '' ;XX11.LOAN2.DD = '' ; XX12.LOAN2.DD = '' ; XX12.LOAN2.DD.OV = '' ; XX1.LOAN2.DD = '' ;XX13.LOAN2.DD.P.D = '' ;   XX1.LOAN2.DD.P.D = '' ; XX2.LOAN2.DD.P.D = '' ;  XX3.LOAN2.DD.P.D = '' ;  XX4.LOAN2.DD.P.D = '' ; XX5.LOAN2.DD.P.D = '' ;  XX6.LOAN2.DD.P.D = '' ; XX7.LOAN2.DD.P.D = '' ; XX8.LOAN2.DD.P.D = '' ;  XX9.LOAN2.DD.P.D = '' ; XX10.LOAN2.DD.P.D = '' ;  XX11.LOAN2.DD.P.D = '' ;  XX12.LOAN2.DD.P.D = '' ;  XX12.LOAN2.DD.OV.P.D = ''
        BB.DATA = '' ; XX12.LOAN2.OV.P.L = ''
        AC.BAL100.LOAN2.P.D = 0 ;  AC.BAL12.LOAN2.DD.OV.P.D =  0 ; AC.BAL11.LOAN2.DD.P.D = 0 ; AC.BAL12.LOAN2.DD.P.D = 0 ;AC.BAL10.LOAN2.DD.P.D = '' ; AC.BAL1.LOAN2.DD.P.D = '' ; XX12.LOAN2.DD.OV.P.D = '' ; AC.BAL40.LOAN2.P.D = '' ; AC.BAL3.LOAN2.DD.P.D = '' ;XX13.LOAN2.DD.P.D = '' ; XX2.LOAN2.DD.P.D = '' ; XX3.LOAN2.DD.P.D = '' ; XX4.LOAN2.DD.P.D = '' ; XX5.LOAN2.DD.P.D = '' ; XX6.LOAN2.DD.P.D = '' ; XX7.LOAN2.DD.P.D = '' ;  XX8.LOAN2.DD.P.D = '' ; XX9.LOAN2.DD.P.D = '' ; XX10.LOAN2.DD.P.D = '' ; XX11.LOAN2.DD.P.D = '' ; XX12.LOAN2.DD.P.D = '' ;XX12.LOAN2.DD.OV.P.D = '' ; XX1.LOAN2.DD.P.D = '' ; AC.BAL5.LOAN2.DD.P.D = '' ; AC.BAL9.LOAN2.DD.P.D = '' ; AC.BAL8.LOAN2.DD.P.D = '' ; AC.BAL10.LOAN2.DD.P.D = '' ;AC.BAL130.LOAN2.P.D = '' ; AC.BAL1.LOAN2.DD.P.D = '' ; AC.BAL10.LOAN2.P.D = '' ; AC.BAL2.LOAN2.DD.P.D = '' ; AC.BAL20.LOAN2.P.D = ''; AC.BAL4.LOAN2.DD.P.D = '' ; AC.BAL40.LOAN2.P.D = '' ; AC.BAL50.LOAN2.P.D = '' ; AC.BAL60.LOAN2.P.D = '' ; AC.BAL70.LOAN2.P.D = '' ; AC.BAL80.LOAN2.P.D = '' ; AC.BAL90.LOAN2.P.D = '' ; AC.BAL110.LOAN2.P.D = '' ; AC.BAL120.LOAN2.P.D = '' ;AC.BAL3.LOAN2.DD.P.D = '' ; AC.BAL3.LOAN1.CHQ = ''
        AC.BAL13.OVER.33.D = '' ; XX13.OVER.33.D = '' ; AC.BAL1.OVER.33.D = '' ; AC.BAL6.LOAN2.DD.P.D = ''
        AC.BAL13.OVER.33.D.P.C = '' ; XX13.OVER.33.D.P.C = '' ; AC.BAL1.OVER.33.D.P.C = ''
        XX2.OVER.33.D = '' ; AC.BAL2.OVER.33.D = '' ; XX3.OVER.33.D = ''
        XX2.OVER.33.D.P.C = '' ; AC.BAL2.OVER.33.D.P.C = '' ; XX3.OVER.33.D.P.C = ''
        AC.BAL4.OVER.33.D = '' ; XX4.OVER.33.D = '' ; AC.BAL5.OVER.33.D = ''
        AC.BAL4.OVER.33.D.P.C = '' ; XX4.OVER.33.D.P.C = '' ; AC.BAL5.OVER.33.D.P.C = ''
        XX5.OVER.33.D = '' ;AC.BAL6.OVER.33.D = '' ;XX6.OVER.33.D = ''
        XX5.OVER.33.D.P.C = '' ;AC.BAL6.OVER.33.D.P.C = '' ;XX6.OVER.33.D.P.C = ''
        AC.BAL7.OVER.33.D = '' ; XX7.OVER.33.D = '' ;AC.BAL8.OVER.33.D = ''
        AC.BAL7.OVER.33.D.P.C = '' ; XX7.OVER.33.D.P.C = '' ;AC.BAL8.OVER.33.D.P.C = ''
        XX8.OVER.33.D = '' ; AC.BAL9.OVER.33.D = '' ; XX9.OVER.33.D = ''
        XX8.OVER.33.D.P.C = '' ; AC.BAL9.OVER.33.D.P.C = '' ; XX9.OVER.33.D.P.C = ''
        AC.BAL10.OVER.33.D = '' ; XX10.OVER.33.D = '' ;AC.BAL11.OVER.33.D = ''
        AC.BAL10.OVER.33.D.P.C = '' ; XX10.OVER.33.D.P.C = '' ;AC.BAL11.OVER.33.D.P.C = ''
        XX11.OVER.33.D = '' ; AC.BAL12.OVER.33.D = '' ; XX12.OVER.33.D = '' ; XX12.OVER.33.D.OV = ''
        XX11.OVER.33.D.P.C = '' ; AC.BAL12.OVER.33.D.P.C = '' ; XX12.OVER.33.D.P.C = '' ; XX12.OVER.33.D.OV.P.C = ''
        AC.BAL.OVER.33.D = '' ; AC.BAL3.OVER.33.D = '' ;XX1.OVER.33.D = ''
        AC13.P.L = '' ; AC.BAL13.LOAN2.P.L = ''; XX13.LOAN2.P.L = ''; AC14.P.L = '' ; AC.BAL1.LOAN2.P.L = '' ; XX2.LOAN2.P.L = ''; AC15.P.L = '' ; AC.BAL2.LOAN2.P.L = '' ; XX3.LOAN2.P.L = ''; AC16.P.L = '' ; AC.BAL4.LOAN2.P.L = '' ; XX4.LOAN2.P.L = ''; AC17.P.L = '' ; AC.BAL5.LOAN2.P.L = '' ; XX5.LOAN2.P.L = ''; AC18.P.L = '' ; AC.BAL6.LOAN2.P.L = '' ; XX6.LOAN2.P.L = ''; AC19.P.L = '' ; AC.BAL7.LOAN2.P.L = '' ; XX7.LOAN2.P.L = ''; AC20.P.L = '' ; AC.BAL8.LOAN2.P.L = '' ; XX8.LOAN2.P.L = ''; AC21.P.L = '' ; AC.BAL9.LOAN2.P.L = '' ; XX9.LOAN2.P.L = ''; AC22.P.L = '' ; AC.BAL10.LOAN2.P.L = '' ; XX10.LOAN2.P.L = ''; AC23.P.L = '' ; AC.BAL11.LOAN2.P.L = '' ; XX11.LOAN2.P.L = ''; AC24.P.L = '' ; AC.BAL12.LOAN2.P.L = '' ; XX12.LOAN2.P.L = ''; AC25.P.L = '' ; AC.BAL13.LOAN2.P.L = '' ; XX13.LOAN2.P.L = ''; AC26.P.L = '' ; AC.BAL14.LOAN2.P.L = '' ; XX14.LOAN2.P.L = ''; AC27.P.L = '' ; AC.BAL15.LOAN2.P.L = '' ; XX15.LOAN2.P.L = ''; AC28.P.L = '' ; AC.BAL16.LOAN2.P.L = '' ; XX16.LOAN2.P.L = ''; AC29.P.L = '' ; AC.BAL17.LOAN2.P.L = '' ; XX17.LOAN2.P.L = ''; AC30.P.L = '' ; AC.BAL18.LOAN2.P.L = '' ; XX18.LOAN2.P.L = ''
        AC.BAL100.LOAN2.P.D = ''  ;AC.BAL13.LOAN2.DD.P.D = '' ;AC.BAL11.LOAN2.DD.P.D = '' ;AC.BAL12.LOAN2.DD.OV.P.D = '' ;AC.BAL12.LOAN2.DD.P.D = '' ;AC.BAL120.LOAN2.OV.P.D = '' ;XX12.OVER.33.D.OV = '' ;AC.BAL12.OVER.33.D.OV = ''
        AC.BAL.OVER.33.D.P.C = '' ; AC.BAL3.OVER.33.D.P.C = '' ;XX1.OVER.33.D.P.C = ''
        AC.BAL13.OVER.44 = '' ;XX13.OVER.44 = '' ; AC.BAL1.OVER.44 = '' ; XX2.OVER.44 = ''
        AC.BAL2.OVER.44 = '' ; XX3.OVER.44 = '' ; AC.BAL4.OVER.44 = '' ; XX4.OVER.44 = ''
        AC.BAL5.OVER.44 = '' ; XX5.OVER.44 = '' ; AC.BAL6.OVER.44 = '' ; XX6.OVER.44 = ''
        AC.BAL7.OVER.44 = '' ; XX7.OVER.44 = '' ; AC.BAL8.OVER.44 = '' ; XX8.OVER.44 = ''
        AC.BAL9.OVER.44 = '' ; XX9.OVER.44 = '' ; AC.BAL10.OVER.44 = '' ;XX10.OVER.44 = '' ; AC.BAL11.OVER.44 = '' ;XX11.OVER.44 = ''
        AC.BAL12.OVER.44 = '' ;AC.BAL12.OVER.44.OV = ''  ; XX12.OVER.44 = '' ; XX12.OVER.44.OV = ''
        AC.BAL.OVER.44 = '' ;AC.BAL3.OVER.44 = '' ; XX1.OVER.44 = ''
        AC.BAL14 = '' ; AC.BAL1 = '' ; AC.BAL2 = '' ; AC.BAL4 = ''
        AC.BAL5 = '' ; AC.BAL6 = '' ; AC.BAL7 = '' ;AC.BAL8 = ''
        AC.BAL10 = '' ; AC.BAL11 = '';AC.BAL12 = '' ; AC.BAL3 = ''
        AC1 = '' ; AC2 = '' ; AC3 = '' ; AC4 = '' ; AC5 = '' ; AC6 = ''
        AC7 = '' ; AC8 = '' ; AC9 = '' ; AC10 = '' ; AC11 = '' ; AC12 = ''
        AC13 = '' ; AC14 = '' ; AC15 = '' ; AC16 = '';AC.BAL.16 = ''
        AC.BAL14.MM = '' ; XX14 = '' ; AC.BAL1.MM = '' ; XX3 = ''
        AC.BAL2.MM = '' ; XX4 = '' ; AC.BAL4.MM = '' ; XX5 = ''
        AC.BAL5.MM = '' ; XX6 = '' ; AC.BAL6.MM = '' ; XX7 = ''
        AC.BAL7.MM = '' ; XX8 = '' ; AC.BAL8.MM = '' ; XX9 = ''
        AC.BAL9.MM = '' ; XX10 = '' ; AC.BAL10.MM = '' ; XX11 = ''
        XX11.LOAN2.DD2 = ''
        AC.BAL11.MM = '' ; XX12 = '' ; AC.BAL12.MM = '' ; XX13 = ''
        AC.BAL20.MM = '' ; XX20 = '' ; AC.BAL3.MM = '' ; XX1.16 = ''
        AC.BAL13.LOAN2 = '' ; AC13 = '' ; AC.BAL1.LOAN2 = '' ; AC14 = ''
        XX3.LOAN2 = '' ; XX2.LOAN2 = '' ; XX13.LOAN2 = '' ; AC.BAL2.LOAN2 = ''
        AC15 = '' ; AC.BAL4.LOAN2 = '' ; AC16 = '' ; XX4.LOAN2 = ''
        AC17 = '' ; AC.BAL5.LOAN2 = '' ; XX5.LOAN2 = ''
        AC18 = '' ; AC.BAL6.LOAN2 = '' ; XX6.LOAN2 = ''
        AC19 = '' ; AC.BAL7.LOAN2 = '' ; XX7.LOAN2 = ''
        AC20 = '' ; AC.BAL8.LOAN2 = '' ; XX8.LOAN2 = ''
        AC21 = '' ; AC.BAL9.LOAN2 = '' ; XX9.LOAN2 = ''
        AC22 = '' ; AC.BAL10.LOAN2 = '' ; XX10.LOAN2 = ''
        AC23 = '' ; AC.BAL11.LOAN2 = '' ; XX11.LOAN2 = ''
        AC24 = '' ; AC.BAL12.LOAN2 = '' ; XX12.LOAN2 = ''
        AC24.OV = '' ; AC.BAL12.LOAN2.OV = '' ; XX12.LOAN2.OV = '' ; AC.BAL12.LOAN2.OV.P.L = ''
        AC.BAL.LOAN2 = '' ; AC.BAL3.LOAN2 = ''; XX1.LOAN2 = ''  ;XX1.LOAN2.P.L= '' ; AC.BAL3.LOAN2.P.L = '' ; XX1.LOAN2.P.L = ''
        AC.BAL13.LOAN2.P = '' ; XX13.LOAN2.P = ''
        AC.BAL1.LOAN2.P = '' ; XX2.LOAN2.P = ''
        AC.BAL2.LOAN2.P = '' ; XX3.LOAN2.P = ''
        AC.BAL4.LOAN2.P = '' ; XX4.LOAN2.P = ''
        AC.BAL5.LOAN2.P = '' ; XX5.LOAN2.P = ''
        AC.BAL6.LOAN2.P = '' ; XX6.LOAN2.P = ''
        AC.BAL7.LOAN2.P = '' ; XX7.LOAN2.P = ''
        AC.BAL8.LOAN2.P = '' ; XX8.LOAN2.P = ''
        AC.BAL9.LOAN2.P = '' ; XX9.LOAN2.P = ''
        AC.BAL10.LOAN2.P = '' ; XX10.LOAN2.P = ''
        AC.BAL11.LOAN2.P = '' ; XX11.LOAN2.P = ''
        AC.BAL12.LOAN2.P = '' ; XX12.LOAN2.P = ''
        AC.BAL.LOAN2.P = '' ; AC.BAL3.LOAN2.P = ''; XX1.LOAN2.P = '' ; XX1.LOAN2.P.L = ''
        AC25 = '' ; AC.BAL13.LOAN1 = '' ; XX13.LOAN1 = ''
        AC25.EZN = '' ; AC.BAL13.LOAN1.EZN = '' ; XX13.LOAN1.EZN = ''
        AC26 = '' ; AC.BAL1.LOAN1 = ''  ; XX2.LOAN1 = ''
        AC26.EZN = '' ; AC.BAL1.LOAN1.EZN = ''  ; XX2.LOAN1.EZN = ''
        AC27 = '' ; AC.BAL2.LOAN1 = ''  ; XX3.LOAN1 = ''
        AC27.EZN = '' ; AC.BAL2.LOAN1.EZN = ''  ; XX3.LOAN1.EZN = ''
        AC28 = '' ; AC.BAL4.LOAN1 = ''  ; XX4.LOAN1 = ''
        AC28.EZN = '' ; AC.BAL4.LOAN1.EZN = ''  ; XX4.LOAN1.EZN = ''
        AC29 = '' ; AC.BAL5.LOAN1 = ''  ; XX5.LOAN1 = ''
        AC29.EZN = '' ; AC.BAL5.LOAN1.EZN = ''  ; XX5.LOAN1.EZN = ''
        AC30 = '' ; AC.BAL6.LOAN1 = ''  ; XX6.LOAN1 = ''
        AC30.EZN = '' ; AC.BAL6.LOAN1.EZN = ''  ; XX6.LOAN1.EZN = ''
        AC31 = '' ; AC.BAL7.LOAN1 = ''  ; XX7.LOAN1 = ''
        AC31.EZN = '' ; AC.BAL7.LOAN1.EZN = ''  ; XX7.LOAN1.EZN = ''
        AC32 = '' ; AC.BAL8.LOAN1 = ''  ; XX8.LOAN1 = ''
        AC32.EZN = '' ; AC.BAL8.LOAN1.EZN = ''  ; XX8.LOAN1.EZN = ''
        AC33 = '' ; AC.BAL9.LOAN1 = ''  ; XX9.LOAN1 = ''
        AC33.EZN = '' ; AC.BAL9.LOAN1.EZN = ''  ; XX9.LOAN1.EZN = ''
        AC34 = '' ; AC.BAL10.LOAN1 = ''  ; XX10.LOAN1 = ''
        AC34.EZN = '' ; AC.BAL10.LOAN1.EZN = ''  ; XX10.LOAN1.EZN = ''
        AC35 = '' ; AC.BAL11.LOAN1 = ''  ; XX11.LOAN1 = ''
        AC35.EZN = '' ; AC.BAL11.LOAN1.EZN = ''  ; XX11.LOAN1.EZN = ''
        AC36 = '' ; AC.BAL12.LOAN1 = ''  ; XX12.LOAN1 = '' ; AC.BAL12.LOAN1.OV = ''
        AC36.EZN = '' ; AC.BAL12.LOAN1.EZN = ''  ; XX12.LOAN1.EZN = ''  ; AC.BAL12.LOAN1.EZN.OV = ''
        XX12.LOAN1.OV = '' ;XX12.LOAN1.OV.EZN = ''             ;  XX12.LOAN1.OV = ''
        XX1.LOAN1 = '' ; AC.BAL.LOAN1 = '' ; AC.BAL3.LOAN1 = ''
        AC.BAL13.D = '' ; AC.BAL1.D = '' ; AC.BAL2.D = ''
        AC.BAL4.D = '' ;AC.BAL5.D = '' ; AC.BAL6.D = ''
        AC.BAL7.D = '' ;AC.BAL8.D = '' ; AC.BAL9.D = ''
        AC.BAL10.D = '' ;AC.BAL11.D = '' ; AC.BAL12.D = '' ;AC.BAL = '' ; AC.BAL3.D = '' ; AC.BAL13.MM.D = '' ; XX13.D = '' ; AC.BAL1.MM.D = ''
        XX2.D = '' ; AC.BAL2.MM.D = '' ; XX3.D = '' ; AC.BAL4.MM.D = '' ; XX4.D = '' ; AC.BAL5.MM.D = '' ; XX5.D = '' ; AC.BAL6.MM.D = '' ; XX6.D = ''
        AC.BAL7.MM.D = '' ; XX7.D = '' ;AC.BAL8.MM.D = '' ; XX8.D = ''
        AC.BAL9.MM.D = '' ; XX9.D = '' ; AC.BAL10.MM.D = '' ; XX10.D = ''
        AC.BAL11.MM.D = '' ; XX11.D = '' ;  AC.BAL12.MM.D = '' ; XX12.D = ''
        AC.BAL3.MM.D = '' ; XX1.D = '' ; AC.BAL130.LOAN2 = '' ; AC.BAL10.LOAN2 = ''; AC.BAL20.LOAN2 = '' ; AC.BAL40.LOAN2 = '' ; AC.BAL50.LOAN2 = '' ; AC.BAL60.LOAN2 = '' ; AC.BAL70.LOAN2 = '' ; AC.BAL80.LOAN2 = '' ; AC.BAL90.LOAN2 = '' ; AC.BAL100.LOAN2 = '' ; AC.BAL110.LOAN2 = ''
        AC.BAL120.LOAN2 = '' ; AC.BAL30.LOAN2 = '' ; AC.BAL13.LOAN2.DD = '' ;XX13.LOAN2.DD = '' ;AC.BAL1.LOAN2.DD = '' ; XX2.LOAN2.DD = ''
        AC.BAL2.LOAN2.DD = '' ;XX3.LOAN2.DD = ''
        XX10.LOAN2.DD62 = '' ; XX4.LOAN2.DD = '' ; AC.BAL12.LOAN2 = ''
        XX12.LOAN2 = '' ; AC.BAL13.LOAN2 ='' ; AC.BAL120.LOAN102 = ''
        AC.BAL100.LOAN102 = '' ;AC.BAL10.LOAN102 = '' ;AC.BAL20.LOAN102 = ''
        AC.BAL40.LOAN102 = '' ; AC.BAL50.LOAN102 = '' ;AC.BAL60.LOAN102 = ''
        AC.BAL70.LOAN102 = '' ; AC.BAL80.LOAN102 = '' ;AC.BAL90.LOAN102 = ''
        XX11.OVER.33.D = '' ; XX2.OVER.33.D = '' ; XX3.OVER.33.D =''
        XX12.OVER.33.D = '' ; XX12.OVER.33.D.OV = '' ; XX4.OVER.33.D = '' ; XX5.OVER.33.D = '' ; AC.BAL12.LOAN2 = ''
        AC.BAL30.LOAN2 = '' ; AC.BAL20.LOAN2 = '' ; AC.BAL10.LOAN2 = ''
        AC.BAL40.LOAN2 = '' ; AC.BAL50.LOAN2 = '' ; AC.BAL60.LOAN2 = ''
        AC.BAL70.LOAN2  = '' ; AC.BAL12.LOAN2.DD.OV = '' ; AC.BAL.16 = ''
        AC.BAL80.LOAN2  = '' ; AC.BAL12.LOAN2.P.OV = '' ; AC.BAL30.LOAN102 = ''
        AC.BAL90.LOAN2  = '' ; AC.BAL120.LOAN2.OV = ''
        AC.BAL100.LOAN2 = '' ; AC.BAL3.LOAN2.DD62 = ''
        AC.BAL110.LOAN2 = '' ;AC.BAL13.LOAN2.DD = '' ; XX1.LOAN2.DD = ''
        XX13.LOAN2.DD ='' ; AC.BAL1.LOAN2.DD = ''; XX2.LOAN2.DD = '' ;
        AC.BAL2.LOAN2.DD = '' ; XX3.LOAN2.DD = '' ; AC.BAL4.LOAN2.DD = ''  XX4.LOAN2.DD = '' ; AC.BAL5.LOAN2.DD = '' ; XX5.LOAN2.DD = ''
        AC.BAL6.LOAN2.DD = '' ; XX6.LOAN2.DD = '' ; AC.BAL7.LOAN2.DD = ''
        XX7.LOAN2.DD = '' ; AC.BAL8.LOAN2.DD = '' ; XX8.LOAN2.DD = ''
        AC.BAL9.LOAN2.DD = '' ; XX9.LOAN2.DD = '' ; AC.BAL10.LOAN2.DD='' XX10.LOAN2.DD = '' ; AC.BAL11.LOAN2.DD = '' ;XX11.LOAN2.DD = ''
        AC.BAL12.LOAN2.DD = '' ; XX12.LOAN2.DD = '' ;AC.BAL12.LOAN2.DD.OV=''
        XX12.LOAN2.DD.OV ='' ; AC.BAL3.LOAN2.DD= '' ;AC.BAL.LOAN2.DD = ''
        AC.BAL120.LOAN2 = '' ; AC.BAL130.LOAN2 = '' ; AC.BAL3.OVER.44 = ''
        XX12.LOAN2  = '' ; XX13.LOAN2  = '' ; XX1.LOAN2   = ''; XX2.LOAN2   = ''
        XX3.LOAN2   = '' ; XX4.LOAN2   = '' ; XX5.LOAN2   = ''
        XX6.LOAN2   = ''; AC.BAL13.LOAN2.DD62 = '' ; XX1.LOAN2.DD62 = ''; XX13.LOAN2.DD62 ='' ; AC.BAL1.LOAN2.DD62 = ''; XX2.LOAN2.DD62 = '' ; AC.BAL2.LOAN2.DD62 = '' ; XX3.LOAN2.DD62 = '' ; AC.BAL4.LOAN2.DD62 = ''  XX4.LOAN2.DD62 = '' ; AC.BAL5.LOAN2.DD62 = '' ; XX5.LOAN2.DD62 = ''; AC.BAL6.LOAN2.DD62 = '' ; XX6.LOAN2.DD62 = '' ; AC.BAL7.LOAN2.DD62 = ''; XX7.LOAN2.DD62 = '' ; AC.BAL8.LOAN2.DD62 = '' ; XX8.LOAN2.DD62 = ''; AC.BAL9.LOAN2.DD62 = '' ; XX9.LOAN2.DD62 = '' ; AC.BAL10.LOAN2.DD62='' XX10.LOAN2.DD62 = '' ; AC.BAL11.LOAN2.DD62 = '' ;XX11.LOAN2.DD62 = ''; AC.BAL12.LOAN2.DD62 = '' ; XX12.LOAN2.DD62 = ''; AC.BAL12.LOAN2.DD.OV62=''; XX12.LOAN2.DD.OV62 ='' ; AC.BAL3.LOAN2.DD62 = '' ;AC.BAL.LOAN2.DD62 = ''
        XX7.LOAN2   = ''; XX8.LOAN2   = ''; XX9.LOAN2   = '' ; XX2.LOAN1.CHQ = ' '  ;  XX1.LOAN1.CHQ = ' ' ;  XX3.LOAN1.CHQ = ' ' ; XX4.LOAN1.CHQ = ' ' ; XX5.LOAN1.CHQ = ' '  ; XX6.LOAN1.CHQ = ' ' ;XX7.LOAN1.CHQ = ' ' ; XX8.LOAN1.CHQ = ' ' ;XX9.LOAN1.CHQ = ' '
        XX10.LOAN1.CHQ = ' ' ; XX11.LOAN1.CHQ = ' ' ; XX12.LOAN1.CHQ = ' ' ; XX13.LOAN1.CHQ = ' ' ; XX14.LOAN1.CHQ = ' ' XX10.LOAN2  = ''
        XX11.LOAN2  = ''; AC.BAL14.MM = ''
        AC.BAL4.LOAN2.DD = '' ; XX4.LOAN2.DD = '' ; AC.BAL5.LOAN2.DD = ''  XX5.LOAN2.DD = '' ; AC.BAL6.LOAN2.DD = '' ; XX6.LOAN2.DD = ''
        AC.BAL7.LOAN2.DD = '' ; XX7.LOAN2.DD = '' ;AC.BAL8.LOAN2.DD = ''
        XX8.LOAN2.DD = '' ;AC.BAL9.LOAN2.DD = '' ; XX9.LOAN2.DD = ''
        AC.BAL10.LOAN2.DD = '' ;XX10.LOAN2.DD = '' ; AC.BAL11.LOAN2.DD = ''
        XX11.LOAN2.DD = '' ;  AC.BAL12.LOAN2.DD = '' ; XX12.LOAN2.DD = ''
        AC.BAL12.LOAN2.DD.OV = '' ; XX12.LOAN2.DD.OV = ''
        AC.BAL.LOAN2.DD = '' ; AC.BAL3.LOAN2.DD = '' ; XX1.LOAN2.DD = ''
        AC.BAL130.LOAN102 = '' ; AC.BAL10.LOAN102 = '' ;AC.BAL120.LOAN102= ''
        AC.BAL20.LOAN102 = '' ; AC.BAL30.LOAN102 = '' ; AC.BAL40.LOAN102 = ''
        AC.BAL50.LOAN102 = '' ; AC.BAL60.LOAN102 = '' ; AC.BAL70.LOAN102 =''
        AC.BAL80.LOAN102 = '' ; AC.BAL90.LOAN102 = '' ; AC.BAL110.LOAN102 = ''
        AC.BAL13.LOAN2.DD62 = '' ; XX13.LOAN2.DD62 = ''
        AC.BAL1.LOAN2.DD62 = '' ; XX2.LOAN2.DD62 = ''
        AC.BAL2.LOAN2.DD62 = '' ; XX3.LOAN2.DD62 = ''
        AC.BAL4.LOAN2.DD62 = '' ; XX4.LOAN2.DD62 = ''
        AC.BAL5.LOAN2.DD62 = '' ; XX5.LOAN2.DD62 = ''
        AC.BAL6.LOAN2.DD62 = '' ; XX6.LOAN2.DD62 = ''
        AC.BAL7.LOAN2.DD62 = '' ; XX7.LOAN2.DD62 = ''
        AC.BAL8.LOAN2.DD62 = '' ; XX8.LOAN2.DD62 = ''
        AC.BAL9.LOAN2.DD62 = '' ; XX9.LOAN2.DD62 = ''
        AC.BAL10.LOAN2.DD62 = '' ; XX10.LOAN2.DD62 = ''
        AC.BAL11.LOAN2.DD62 = '' ; XX11.LOAN2.DD62 = ''
        AC.BAL12.LOAN2.DD62 = '' ; XX12.LOAN2.DD62 = ''
        AC.BAL12.LOAN2.DD62.OV = '' ; XX12.LOAN2.DD62.OV = ''
        AC.BAL.LOAN2.DD62 = '' ;AC.BAL3.LOAN2.DD62 = '' ;XX1.LOAN2.DD62 = ''
        AC.BAL14 = ''
        AC.BAL9 = ''
        AC.BAL9.MM =''
        AC.BAL10 =''
        AC.BAL10.MM = ''
        AC.BAL11 = ''
        AC.BAL11.MM = ''
        AC.BAL12 = ''
        AC.BAL12.MM = ''
        XX11.LOAN2 = ''
        XX12.LOAN2 = ''
        XX9.LOAN2 = ''
        AC.BAL11.LOAN2 = ''
        XX11.LOAN1 = ''
        XX12.LOAN1 = ''
        XX12.LOAN1.OV = '' ; XX12.LOAN1.OV.EZN = ''
        XX10.LOAN1 = ''
        XX13.LOAN1 = ''
        XX9.LOAN1 = ''
        AC.BAL13.D = ''
        AC.BAL13.MM.D = ''
        AC.BAL9.D = ''
        AC.BAL9.MM.D = ''
        AC.BAL10.D = ''
        AC.BAL10.MM.D = ''
        AC.BAL11.D = ''
        AC.BAL11.MM.D = ''
        AC.BAL12.D = ''
        AC.BAL12.MM.D = ''
        AC.BAL130.LOAN2 = ''
        AC.BAL20.LOAN2 = ''
        AC.BAL40.LOAN2 = ''
        AC.BAL50.LOAN2 = ''
        AC.BAL60.LOAN2 = ''
        AC.BAL70.LOAN2 = ''
        AC.BAL80.LOAN2 = ''
        AC.BAL90.LOAN2 = ''
        AC.BAL100.LOAN2 = ''
        AC.BAL110.LOAN2 = ''
        AC.BAL120.LOAN2 = ''
        XX3.OVER.44 = ''
        XX4.OVER.44 = ''
        XX5.OVER.44 = ''
        XX7.OVER.44 = ''
        XX2.OVER.44 = ''
        XX12.OVER.44 = '' ; XX12.OVER.44.OV = ''
        XX13.OVER.44 = ''
        XX8.OVER.44 = ''
        XX7.OVER.44 = ''
        XX6.OVER.44 = ''
        AC.BAL1      = ''
        AC.BAL1.MM   = ''
        AC.BAL1.OVER = ''
        AC.BAL1.LOAN1= ''
        AC.BAL1.LOAN2= ''
        AC.BAL2      = ''
        AC.BAL2.MM   = ''
        AC.BAL2.OVER = ''
        AC.BAL2.LOAN1= ''
        AC.BAL2.LOAN2= ''
        AC.BAL3      = ''
        AC.BAL3.MM   = ''
        AC.BAL3.OVER = ''
        AC.BAL3.LOAN1= ''
        AC.BAL3.LOAN2= ''
        AC.BAL4      = ''
        AC.BAL4.MM   = ''
        AC.BAL4.OVER = ''
        AC.BAL4.LOAN1= ''
        AC.BAL4.LOAN2= ''
        AC.BAL5      = ''
        AC.BAL5.MM   = ''
        AC.BAL5.OVER = ''
        AC.BAL5.LOAN1= ''
        AC.BAL5.LOAN2= ''
        AC.BAL6      = ''
        AC.BAL6.MM   = ''
        AC.BAL6.OVER = ''
        AC.BAL6.LOAN1= ''
        AC.BAL6.LOAN2= ''
        AC.BAL7      = ''
        AC.BAL7.MM   = ''
        AC.BAL7.OVER = ''
        AC.BAL7.LOAN1= ''
        AC.BAL7.LOAN2= ''
        AC.BAL8      = ''
        AC.BAL8.MM   = ''
        AC.BAL8.OVER = ''
        AC.BAL8.LOAN1= ''
        AC.BAL8.LOAN2= ''
        AC.BAL2      = ''
        XX2.OVER     = ''
        XX3.OVER     = ''
        XX4.OVER     = ''
        XX5.OVER     = ''
        XX6.OVER     = ''
        XX7.OVER     = ''
        XX8.OVER     = ''
        XX1.LOAN2    = ''
        XX2.LOAN2    = ''
        XX3.LOAN2    = ''
        XX4.LOAN2    = ''
        XX5.LOAN2    = ''
        XX6.LOAN2    = ''
        XX7.LOAN2    = ''
        XX8.LOAN2    = ''
        XX1.LOAN1    = ''
        XX2.LOAN1    = ''
        XX3.LOAN1    = ''
        XX4.LOAN1    = ''
        XX5.LOAN1    = ''
        XX6.LOAN1    = ''
        XX7.LOAN1    = ''
        XX8.LOAN1    = ''
        AC.BAL2      = ''
        YDAYS        = ''
        DAYS         = ''
        XX12.LOAN2  = ''
        XX13.LOAN2  = ''
        XX1.LOAN2   = ''
        XX2.LOAN2   = ''
        XX3.LOAN2   = ''
        XX4.LOAN2   = ''
        XX5.LOAN2   = ''
        XX6.LOAN2   = ''
        XX7.LOAN2   = ''
        XX8.LOAN2   = ''
        XX9.LOAN2   = ''
        XX10.LOAN2  = '' ; XX2.LOAN1.CHQ = ' '  ;  XX1.LOAN1.CHQ = ' ' ;  XX3.LOAN1.CHQ = ' ' ; XX4.LOAN1.CHQ = ' ' ; XX5.LOAN1.CHQ = ' '  ; XX6.LOAN1.CHQ = ' ' ;XX7.LOAN1.CHQ = ' ' ;XX8.LOAN1.CHQ = ' ' ;XX9.LOAN1.CHQ = ' '
        XX10.LOAN1.CHQ = ' ' ; XX11.LOAN1.CHQ = ' ' ; XX12.LOAN1.CHQ = ' ' ; XX13.LOAN1.CHQ = ' ' ; XX14.LOAN1.CHQ = ' '
        XX11.LOAN2  = ''
        AC.BAL14.MM = ''
        AC.BAL14 = ''
        AC.BAL9 = ''
        AC.BAL9.MM =''
        AC.BAL10 =''
        AC.BAL10.MM = ''
        AC.BAL11 = ''
        AC.BAL11.MM = ''
        AC.BAL12 = ''
        AC.BAL12.MM = ''
        XX11.LOAN2 = ''
        XX12.LOAN2 = ''
        XX9.LOAN2 = ''
        AC.BAL11.LOAN2 = ''
        XX11.LOAN1 = ''
        XX12.LOAN1 = ''
        XX12.LOAN1.OV = '' ; XX12.LOAN1.OV.EZN = ''
        XX10.LOAN1 = ''
        XX13.LOAN1 = ''
        XX9.LOAN1 = ''
        AC.BAL13.D = ''
        AC.BAL13.MM.D = ''
        AC.BAL9.D = ''
        AC.BAL9.MM.D = ''
        AC.BAL10.D = ''
        AC.BAL10.MM.D = ''
        AC.BAL11.D = ''
        AC.BAL11.MM.D = ''
        AC.BAL12.D = ''
        AC.BAL12.MM.D = ''
        AC.BAL130.LOAN2 = ''
        AC.BAL20.LOAN2 = ''
        AC.BAL40.LOAN2 = ''
        AC.BAL50.LOAN2 = ''
        AC.BAL60.LOAN2 = ''
        AC.BAL70.LOAN2 = ''
        AC.BAL80.LOAN2 = ''
        AC.BAL90.LOAN2 = ''
        AC.BAL100.LOAN2 = ''
        AC.BAL110.LOAN2 = ''
        AC.BAL120.LOAN2 = ''
        XX3.OVER.44 = ''
        XX4.OVER.44 = ''
        XX5.OVER.44 = ''
        XX7.OVER.44 = ''
        XX2.OVER.44 = ''
        XX12.OVER.44 = ''  ; XX12.OVER.44.OV = ''
        XX13.OVER.44 = ''
        XX8.OVER.44 = ''
        XX7.OVER.44 = ''
        XX6.OVER.44 = ''
        AC.BAL1.D      = ''
        AC.BAL1.MM.D   = ''
        AC.BAL1.OVER.D = ''
        AC.BAL1.LOAN1.D= ''
        AC.BAL1.LOAN2.D= ''
        AC.BAL2.D      = ''
        AC.BAL2.MM.D   = ''
        AC.BAL2.OVER.D = ''
        AC.BAL2.LOAN1.D= ''
        AC.BAL2.LOAN2.D= ''
        AC.BAL3.D      = ''
        AC.BAL3.MM.D   = ''
        AC.BAL3.OVER.D = ''
        AC.BAL3.LOAN1.D= ''
        AC.BAL3.LOAN2.D= ''
        AC.BAL4.D      = ''
        AC.BAL4.MM.D   = ''
        AC.BAL4.OVER.D = ''
        AC.BAL4.LOAN1.D= ''
        AC.BAL4.LOAN2.D= ''
        AC.BAL5.D      = ''
        AC.BAL5.MM.D   = ''
        AC.BAL5.OVER.D = ''
        AC.BAL5.LOAN1.D= ''
        AC.BAL5.LOAN2.D= ''
        AC.BAL6.D      = ''
        AC.BAL6.MM.D   = ''
        AC.BAL6.OVER.D = ''
        AC.BAL6.LOAN1.D= ''
        AC.BAL6.LOAN2.D= ''
        AC.BAL7.D      = ''
        AC.BAL7.MM.D   = ''
        AC.BAL7.OVER.D = ''
        AC.BAL7.LOAN1.D= ''
        AC.BAL7.LOAN2.D= ''
        AC.BAL8.D      = ''
        AC.BAL8.MM.D   = ''
        AC.BAL8.OVER.D = ''
        AC.BAL8.LOAN1.D= ''
        AC.BAL8.LOAN2.D= ''
        AC.BAL2.D      = ''
        XX2.OVER.D     = ''
        XX3.OVER.D     = ''
        XX4.OVER.D     = ''
        XX5.OVER.D     = ''
        XX6.OVER.D     = ''
        XX7.OVER.D     = '' ; XX12.LOAN2.OV = '' ; XX12.LOAN2.P.OV = ''
        XX8.OVER.D     = '' ; AC.BAL20.MM = '' ;
        AC.BAL13.LOAN2.DD = '' ; AC.BAL12.LOAN2.DD = ''
        AC.BAL11.LOAN2.DD = '' ; AC.BAL10.LOAN2.DD = '' ; AC.BAL9.LOAN2.DD = '' ; AC.BAL8.LOAN2.DD = ''
        AC.BAL7.LOAN2.DD = '' ; AC.BAL6.LOAN2.DD = '' ; AC.BAL5.LOAN2.DD = '' ; AC.BAL4.LOAN2.DD = ''
        AC.BAL3.LOAN2.DD = '' ; AC.BAL2.LOAN2.DD = '' ; AC.BAL1.LOAN2.DD = '' ; AC.BAL30.LOAN2.DD ='' ; AC.BAL10.LOAN2.DD =''
        AC.BAL20.LOAN2.DD ='' ; AC.BAL40.LOAN2.DD ='' ; AC.BAL50.LOAN2.DD ='' ; AC.BAL60.LOAN2.DD ='' ; AC.BAL70.LOAN2.DD =''
        AC.BAL80.LOAN2.DD ='' ; AC.BAL90.LOAN2.DD =''; AC.BAL100.LOAN2.DD ='' ; AC.BAL110.LOAN2.DD ='' ; AC.BAL120.LOAN2.DD ='' ; AC.BAL130.LOAN2.DD =''
        XX1.LOAN2.D    = '' ; XX2.LOAN2.D    = '' ; XX3.LOAN2.D    = '' ; XX4.LOAN2.D    = ''
        XX5.LOAN2.D    = '' ; XX6.LOAN2.D    = '' ; XX7.LOAN2.D    = '' ; XX8.LOAN2.D    = '' ; XX1.LOAN1.D    = '' ; XX2.LOAN1.D    = '' ; XX3.LOAN1.D    = ''; XX4.LOAN1.D    = ''
        XX5.LOAN1.D    = '' ; XX6.LOAN1.D    = '' ; XX7.LOAN1.D    = '' ; XX8.LOAN1.D    = ''
        XX1.OVER.44 = '' ; XX2.OVER.44 = '' ; XX3.OVER.44 = '' ; XX4.OVER.44 = ''
        XX5.OVER.44 = '' ; XX6.OVER.44 = '' ; AC.BAL2.LOAN2.P = '' ; AC.BAL1.LOAN2.P = '' ; AC.BAL3.LOAN2.P = ''
        XX7.OVER.44 = '' ; AC.BAL4.LOAN2.P = '' ; AC.BAL5.LOAN2.P = '' ; AC.BAL6.LOAN2.P = ''
        XX8.OVER.44 = '' ; AC.BAL7.LOAN2.P = '' ; AC.BAL8.LOAN2.P = '' ; AC.BAL9.LOAN2.P = ''  ; AC.BAL14.LOAN2.P = ''
        XX9.OVER.44 = '' ; AC.BAL10.LOAN2.P = '' ; AC.BAL11.LOAN2.P = '' ; AC.BAL12.LOAN2.P = ''  ; AC.BAL13.LOAN2.P = ''
        XX10.OVER.44= '' ; XX4.LOAN2.P  = '' ; XX3.LOAN2.P = '' ; XX2.LOAN2.P = ''
        XX11.OVER.44= '' ; XX7.LOAN2.P  = '' ; XX6.LOAN2.P = '' ; XX5.LOAN2.P = ''
        XX12.OVER.44= '' ; XX12.OVER.44.OV = '' ;XX10.LOAN2.P = '' ; XX9.LOAN2.P = '' ; XX8.LOAN2.P = ''
        XX13.OVER.44= '' ; XX13.LOAN2.P = '' ;XX12.LOAN2.P = '' ; XX11.LOAN2.P = ''

        XX1.OVER.33.D = ''; XX2.OVER.33.D = ''; XX3.OVER.33.D = '' ; XX4.OVER.33.D = ''; XX5.OVER.33.D = ''; XX6.OVER.33.D = ''; XX7.OVER.33.D = ''; XX8.OVER.33.D = '' ; XX9.OVER.33.D = '' ; XX10.OVER.33.D = '' ; XX11.OVER.33.D = ''
        XX1.OVER.33.D.P.C = ''; XX2.OVER.33.D.P.C = ''; XX3.OVER.33.D.P.C = '' ; XX4.OVER.33.D.P.C = ''; XX5.OVER.33.D.P.C = ''; XX6.OVER.33.D.P.C = ''; XX7.OVER.33.D.P.C = ''; XX8.OVER.33.D.P.C = '' ; XX9.OVER.33.D.P.C = '' ; XX10.OVER.33.D.P.C = '' ; XX11.OVER.33.D.P.C = ''
        XX12.OVER.33.D = '' ; XX12.OVER.33.D.OV = ''; XX13.OVER.33.D = ''
        XX12.OVER.33.D.P.C = '' ; XX12.OVER.33.D.OV.P.C = ''; XX13.OVER.33.D.P.C = ''
        XX1.LOAN2.DD = ''
        XX2.LOAN2.DD = ''
        XX3.LOAN2.DD = ''
        XX4.LOAN2.DD = ''
        XX5.LOAN2.DD = ''
        XX6.LOAN2.DD = ''
        XX7.LOAN2.DD = '' ; XX8.LOAN2.DD = '' ; XX9.LOAN2.DD = '' ; XX10.LOAN2.DD = '' ; XX11.LOAN2.DD = '' ; XX12.LOAN2.DD = '' ; XX13.LOAN2.DD = ''
        XX1.D = '' ; XX2.D = '' ; XX3.D = '' ; XX4.D = '' ; XX5.D = '' ; XX6.D = ''
        XX7.D = '' ; XX8.D = '' ; XX9.D = '' ; XX10.D = '' ; XX11.D = '' ; XX12.D = '' ; XX13.D = ''
        AC.BAL2.D      = '' ; AC.BAL12.LOAN2.DD62.OV = '' ; XX11.OVER.33.D = '' ;AC.BAL120.LOAN102.OV = ''
        YDAYS.D        = '' ; XX4.LOAN2.DD62 = '' ; AC.BAL9.LOAN2 = ''
        DAYS.D         = ''


*=================BEGINING INTER BANK LOANS===================================================
        T.SEL= "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 2000 5000 5021 2001 5001 5020 3200 5080 5079 ) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
***  T.SEL= "SELECT FBNK.ACCOUNT WITH CATEGORY IN (5001) AND CURRENCY EQ USD AND OPEN.ACTUAL.BAL LT 0"
***  T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 9940000510200003 9940080020200001 0121756710140501 0110023510140501 0110122810120501 0121214710120601 0120913210120601)"
***  T.SEL= "SELECT FBNK.ACCOUNT WITH @ID IN ( 0110122810120501 0120913210120601)"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* TEXT = "SEL1 INTER BANK : " : SELECTED ; CALL REM
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E2)
            CUS.LIM = R.AC<AC.LIMIT.REF>
            CUS.ID  = R.AC<AC.CUSTOMER>
            AC.BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                LMT.REF1    = FIELD(CUS.LIM,'.',1)
                IF LEN(LMT.REF1) EQ 3 THEN
                    XX = CUS.ID:'.':'0000':CUS.LIM
                END
                IF LEN(LMT.REF1) EQ 4 THEN
                    XX = CUS.ID:'.':'000':CUS.LIM
                END
*TEXT = "XX: ":XX ; CALL REM
                CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
                AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD  = TODAY
                YDAYS = ''
                DAYS = 'C'
                IF AC.EXP GE AC.TOD THEN
                    CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                END
                AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 DAYS*******************
                IF DAYS EQ 1 THEN
                    AC1 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL14 += AC1
                END
******************2 DAYS*******************
                IF DAYS EQ 2 THEN
                    AC2 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL1 += AC2
                END
****************3 DAYS********************
                IF DAYS EQ 3  THEN
                    AC3 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL2 +=   AC3
                END
**************4 DAYS*********************
                IF DAYS EQ 4 THEN
                    AC4 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL4 += AC4
                END
************5 DAYS**************
                IF  DAYS EQ 5 THEN
                    AC5 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL5 += AC5
                END
*TEXT = "AC.BAL5 ": AC.BAL5
***************2 WEEKS***********
                IF DAYS GE 5 AND DAYS LE 14 THEN
                    AC6 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL6 += AC6
                END
*TEXT = "AC.BAL6 ": AC.BAL6
****************3 WEEKS**************
                IF DAYS GE 14 AND DAYS LE 21 THEN
                    AC7 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL7 += AC7
                END
*TEXT = "AC.BAL7 ": AC.BAL7
***************4 WEEKS********************
                IF DAYS GE 21 AND DAYS LE 28 THEN
                    AC8 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL8 += AC8
                END
***************5 WEEKS********************
                IF DAYS GE 28 AND DAYS LE 35 THEN
                    AC9 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL9 += AC9
                END
***************6 WEEKS********************
                IF DAYS GE 35 AND DAYS LE 42 THEN
                    AC10 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL10 += AC10
                END
***************7 WEEKS********************
                IF DAYS GE 42 AND DAYS LE 49 THEN
                    AC11 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL11 += AC11
                END
***************8 WEEKS********************
                IF DAYS GE 49 AND DAYS LE 56 THEN
                    AC12 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL12 += AC12
                END

            END
***************************ON*************************
            IF  (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
                AC16 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                AC.BAL.16 += AC16
                AC.BAL3 = AC.BAL.16
            END
        NEXT I
***************************************************
        T.SEL2  = "SELECT FBNK.MM.MONEY.MARKET WITH CATEGORY IN ( 21075 21076 21078 21079 ) AND MATURITY.DATE GE ":HHH:" AND CURRENCY EQ ":CURR.ID:" AND ( STATUS NE LIQ OR VALUE.DATE EQ ":HHH:") "
*T.SEL2 = "SELECT FBNK.MM.MONEY.MARKET WITH @ID IN ( MM1108100009 MM1019600004 )"
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
*   TEXT = "SELECTED " : SELECTED2 ; CALL REM
        FOR J = 1 TO SELECTED2
            CALL F.READ(FN.MM,KEY.LIST2<J>,R.MM,F.MM,E2)
            CUS.ID.MM  = R.MM<MM.CUSTOMER.ID>
            MM.EXP     = R.MM<MM.MATURITY.DATE>
            MM.AMT     = R.MM<MM.PRINCIPAL>
*TEXT = "AC.BAL2 : " : AC.BAL2 ; CALL REM
*TEXT = " MM.AMT : " : MM.AMT ; CALL REM
            CALL F.READ(FN.CU,CUS.ID.MM,R.CU,F.CU,E2)
            SEC     = R.CU<EB.CUS.SECTOR>
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            AC.TOD  = TODAY
            DAYS.MM = 'C'
* IF MM.EXP GE AC.TOD THEN
            CALL CDD ("",AC.TOD,MM.EXP,DAYS.MM)
* END
******************1 DAYS*******************
            IF ( DAYS.MM EQ 0 OR DAYS.MM EQ 1 ) THEN
                AC.BAL14.MM += R.MM<MM.PRINCIPAL>
            END
            XX14 =  AC.BAL14.MM + AC.BAL14
            IF XX14 = '' THEN
                XX14 = 0
            END
****************2 DAYS********************
            IF DAYS.MM EQ 2 THEN
                AC.BAL1.MM += R.MM<MM.PRINCIPAL>
            END
            XX3 = AC.BAL1 + AC.BAL1.MM
            IF XX3 = '' THEN
                XX3 = 0
            END
**************3 DAYS*********************
            IF DAYS.MM EQ 3 THEN
                AC.BAL2.MM += R.MM<MM.PRINCIPAL>
            END
            XX4 = AC.BAL2 + AC.BAL2.MM
            IF XX4 = '' THEN
                XX4 = 0
            END
***************4 DAYS**************
            IF  DAYS.MM EQ 4 THEN
                AC.BAL4.MM += R.MM<MM.PRINCIPAL>
            END
            XX5 = AC.BAL4 + AC.BAL4.MM
            IF XX5 = '' THEN
                XX5 = 0
            END

***************5 DAYS***********
            IF DAYS.MM EQ 5 THEN
                AC.BAL5.MM += R.MM<MM.PRINCIPAL>
            END
            XX6 = AC.BAL5 + AC.BAL5.MM
            IF XX6 = '' THEN
                XX6 = 0
            END

****************2 WEEKS**************
            IF DAYS.MM GT 5 AND DAYS.MM LE 14 THEN
                AC.BAL6.MM += R.MM<MM.PRINCIPAL>
            END
            XX7 = AC.BAL6   +  AC.BAL6.MM
            IF XX7 = '' THEN
                XX7 = 0
            END

***************3 WEEKS********************
            IF DAYS.MM GT 14 AND DAYS.MM LE 21 THEN
                AC.BAL7.MM += R.MM<MM.PRINCIPAL>
            END

            XX8 = AC.BAL7 + AC.BAL7.MM

            IF XX8 = '' THEN
                XX8 = 0
            END
***************4 WEEKS********************
            IF DAYS.MM GT 21 AND DAYS.MM LE 28 THEN
                AC.BAL8.MM += R.MM<MM.PRINCIPAL>
            END
            XX9 = AC.BAL8 + AC.BAL8.MM

            IF XX9 = '' THEN
                XX9 = 0
            END
***************5 WEEKS********************
            IF DAYS.MM GT 28 AND DAYS.MM LE 35 THEN
                AC.BAL9.MM += R.MM<MM.PRINCIPAL>
            END
            XX10 = AC.BAL9 + AC.BAL9.MM

            IF XX10 = '' THEN
                XX10 = 0
            END

***************6 WEEKS********************
            IF DAYS.MM GT 35 AND DAYS.MM LE 42 THEN
                AC.BAL10.MM += R.MM<MM.PRINCIPAL>
            END
            XX11 = AC.BAL10 + AC.BAL10.MM

            IF XX11 = '' THEN
                XX11 = 0
            END

***************7 WEEKS********************
            IF DAYS.MM GT 42 AND DAYS.MM LE 49 THEN
                AC.BAL11.MM += R.MM<MM.PRINCIPAL>
            END
            XX12 = AC.BAL11 + AC.BAL11.MM

            IF XX12 = '' THEN
                XX12 = 0
            END
***************8 WEEKS********************
            IF DAYS.MM GE 49 AND DAYS.MM LE 56 THEN
                AC.BAL12.MM += R.MM<MM.PRINCIPAL>
            END
            XX13 = AC.BAL12 + AC.BAL12.MM

            IF XX13 = '' THEN
                XX13 = 0
            END

***************OVER8 WEEKS********************
            IF DAYS.MM GE 56 THEN
                AC.BAL20.MM += R.MM<MM.PRINCIPAL>
            END
            XX20 =  AC.BAL20.MM

            IF XX20 = '' THEN
                XX20 = 0
            END

***************************ON*************************
            IF  MM.EXP EQ ''  THEN
                AC.BAL3.MM += R.MM<MM.PRINCIPAL>
            END
            XX1.16 = FMT(AC.BAL3,"R2") + FMT(AC.BAL3.MM,"R2")
            IF XX1.16 = '' THEN
                XX1.16 = 0
            END
        NEXT J

**********************************************************
*==============================END INTER BANK=======================
*=======================BEGINING CORPRATE LOANS=======================
***        T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN (3201 11240 11242 11238 11232 11234 11239 1102 1404 1414 1206 1208 1401 1402 1405 1406 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591 1001 1002 1003 1059 1205 1207 1216 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1582 1407 1413 1408 6501 6502 6503 6504 6511 1558) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
        T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN (3201 1102 1404 1414 1206 1208 1401 1402 1405 1406 11240 11238 11232 11234 11239 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 11242 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591 1001 1002 1003 1059 1205 1207 1216 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1582 1407 1413 1408 6501 6502 6503 6504 6511 1558 1595 1223 1225 1221 1224 1222 1227 1220) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
* T.SEL6="SELECT FBNK.ACCOUNT WITH CATEGORY IN (1404)"
        CALL EB.READLIST(T.SEL6,KEY.LIST6,"",SELECTED6,ERR.MSG)
*TEXT = "SEL6 : " : SELECTED6 ; CALL REM
        FOR NN = 1 TO SELECTED6
            CALL F.READ(FN.AC,KEY.LIST6<NN>,R.AC,F.AC,ERR)
            CUS.ID6 = R.AC<AC.CUSTOMER>
            CUS.LIM6= R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID6,R.CU,F.CU,ERR)
            SEC.NEW=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF SEC.NEW NE 4650 AND SEC.NEW NE '' THEN
                IF( CUS.LIM6 NE '' AND CUS.LIM6 NE '0' AND CUS.LIM6 NE 'NOSTRO' ) THEN
*                IF SEC.NEW NE 4650 AND SEC.NEW NE '' THEN
                    LMT.REF6    = FIELD(CUS.LIM6,'.',1)
                    IF LEN(LMT.REF6) EQ 3 THEN
                        XY5 = CUS.ID6:'.':'0000':CUS.LIM6
                    END
                    IF LEN(LMT.REF6) EQ 4 THEN
                        XY5 = CUS.ID6:'.':'000':CUS.LIM6
                    END
*TEXT = "XY: ":XY ; CALL REM
                    CALL F.READ(FN.LIM,XY5,R.LIM,F.LIM,E2)
                    AC.EXP5  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD5  = TODAY
                    DAYS.55= 'C'
                    IF AC.EXP5 GE AC.TOD5 THEN
                        CALL CDD ("",AC.TOD5,AC.EXP5,DAYS.55)
                    END
                    AC.BAL.LOAN2 = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 DAYS*******************
                    IF ( DAYS.55 EQ 1 OR DAYS.55 EQ 0 ) THEN
                        AC13 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL13.LOAN2 += AC13
                    END

                    XX13.LOAN2 = AC.BAL13.LOAN2
                    IF XX13.LOAN2 = '' THEN
                        XX13.LOAN2 = 0
                    END
******************2 DAYS*******************
                    IF DAYS.55 EQ 2 THEN
                        AC14 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL1.LOAN2 += AC14
                    END

                    XX2.LOAN2 = AC.BAL1.LOAN2
                    IF XX2.LOAN2 = '' THEN
                        XX2.LOAN2 = 0
                    END
****************3 DAYS********************
                    IF DAYS.55 EQ 3 THEN
                        AC15 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL2.LOAN2 +=   AC15
                    END
                    XX3.LOAN2 = AC.BAL2.LOAN2
                    IF XX3.LOAN2 = '' THEN
                        XX3.LOAN2 = 0
                    END

**************4 DAYS*********************
                    IF DAYS.55 EQ 4 THEN
                        AC16 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL4.LOAN2 += AC16
                    END
                    XX4.LOAN2 = AC.BAL4.LOAN2
                    IF XX4.LOAN2 = '' THEN
                        XX4.LOAN2 = 0
                    END
************5 DAYS**************
                    IF  DAYS.55 EQ 5 THEN
                        AC17 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL5.LOAN2 += AC17
                    END
                    XX5.LOAN2 = AC.BAL5.LOAN2
                    IF XX5.LOAN2 = '' THEN
                        XX5.LOAN2 = 0
                    END
***************2 WEEKS***********
                    IF DAYS.55 GT 5 AND DAYS.55 LE 14  THEN
                        AC18 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL6.LOAN2 += AC18
                    END
                    XX6.LOAN2 = AC.BAL6.LOAN2
                    IF XX6.LOAN2 = '' THEN
                        XX6.LOAN2 = 0
                    END
****************3 WEEKS**************
                    IF DAYS.55 GT 14 AND DAYS.55 LE 21 THEN
                        AC19 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL7.LOAN2 += AC19
                    END
                    XX7.LOAN2 = AC.BAL7.LOAN2
                    IF XX7.LOAN2 = '' THEN
                        XX7.LOAN2 = 0
                    END
***************4 WEEKS********************
                    IF DAYS.55 GT 21 AND DAYS.55 LE 28 THEN
                        AC20 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL8.LOAN2 += AC20
                    END
                    XX8.LOAN2 = AC.BAL8.LOAN2
                    IF XX8.LOAN2 = '' THEN
                        XX8.LOAN2 = 0
                    END

***************5 WEEKS********************
                    IF DAYS.55 GT 28 AND DAYS.55 LE 35 THEN
                        AC21 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL9.LOAN2 += AC21
                    END
                    XX9.LOAN2 = AC.BAL9.LOAN2
                    IF XX9.LOAN2 = '' THEN
                        XX9.LOAN2 = 0
                    END
***************6 WEEKS********************
                    IF DAYS.55 GT 35 AND DAYS.55 LE 42 THEN
                        AC22 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL10.LOAN2 += AC22
                    END
                    XX10.LOAN2 = AC.BAL10.LOAN2
                    IF XX10.LOAN2 = '' THEN
                        XX10.LOAN2 = 0
                    END
***************7 WEEKS********************
                    IF DAYS.55 GT 42 AND DAYS.55 LE 49 THEN
                        AC23 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL11.LOAN2 += AC23
                    END
                    XX11.LOAN2 = AC.BAL11.LOAN2
                    IF XX11.LOAN2 = '' THEN
                        XX11.LOAN2 = 0
                    END
***************8 WEEKS********************
                    IF DAYS.55 GT 49 AND DAYS.55 LE 56 THEN
                        AC24 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL12.LOAN2 += AC24
                    END
                    XX12.LOAN2 = AC.BAL12.LOAN2
                    IF XX12.LOAN2 = '' THEN
                        XX12.LOAN2 = 0
                    END
*******************OVER 8 WEEKS********************************
                    IF DAYS.55 GT 56 THEN
                        AC24.OV = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL12.LOAN2.OV += AC24.OV
                    END
                    XX12.LOAN2.OV = AC.BAL12.LOAN2.OV
                    IF XX12.LOAN2.OV = '' THEN
                        XX12.LOAN2.OV = 0
                    END

                END


***************************ON*************************
                IF  (CUS.LIM6 EQ '' OR CUS.LIM6 EQ 'NOSTRO') THEN
                    AC.BAL.LOAN2 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL3.LOAN2 += AC.BAL.LOAN2
                END
                XX1.LOAN2 = AC.BAL3.LOAN2
                IF XX1.LOAN2 = '' THEN
                    XX1.LOAN2 = 0
                END
            END
        NEXT NN
*=============================END CORPRATE LOANS====================


*=============================BEGIN PERSONAL LOANS===================
        T.SEL.P.L="SELECT FBNK.ACCOUNT WITH CATEGORY IN (3201 1102 1404 1414 1206 1208 1401 1402 1405 1406 11240 11238 11232 11234 11239 1480 1481 1499 1483 1493 1202 1212 1301 1302 1303 1377 1390 1399 1502 1503 1416 1201 1211 11242 1477 3005 1501 3010 1560 3017 1566 3014 1577 3013 1588 3012 1599 3011 1591 1001 1002 1003 1059 1205 1207 1216 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1582 1407 1413 1408 6501 6502 6503 6504 6511 1558 1595 1223 1225 1221 1224 1222 1227 1220) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 "
        CALL EB.READLIST(T.SEL.P.L,KEY.LIST.P.L,"",SELECTED.P.L,ERR.MSG)
        FOR NNNNN = 1 TO SELECTED.P.L
            CALL F.READ(FN.AC,KEY.LIST.P.L<NNNNN>,R.AC,F.AC,ERR)
            CUS.ID.P.L  = R.AC<AC.CUSTOMER>
            CUS.LIM.P.L = R.AC<AC.LIMIT.REF>
            CALL F.READ(FN.CU,CUS.ID.P.L,R.CU,F.CU,ERR)
            SEC.NEW.P.L = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF SEC.NEW.P.L EQ 4650 THEN
                IF( CUS.LIM.P.L NE '' AND CUS.LIM.P.L NE '0' AND CUS.LIM.P.L NE 'NOSTRO' ) THEN
*                IF SEC.NEW.P.L EQ 4650 THEN
                    LMT.REF.P.L    = FIELD(CUS.LIM.P.L,'.',1)
                    IF LEN(LMT.REF.P.L) EQ 3 THEN
                        XY5 = CUS.ID.P.L:'.':'0000':CUS.LIM.P.L
                    END
                    IF LEN(LMT.REF.P.L) EQ 4 THEN
                        XY5 = CUS.ID.P.L:'.':'000':CUS.LIM.P.L
                    END
*TEXT = "XY: ":XY ; CALL REM
                    CALL F.READ(FN.LIM,XY5,R.LIM,F.LIM,E2)
*Line [ 1230 ] Update LI to LILR - ITSS - R21 Upgrade - 2021-12-26
                    AC.EXP.P.L  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD.P.L  = TODAY
                    DAYS.P.L= 'C'
                    IF AC.EXP.P.L GE AC.TOD.P.L THEN
                        CALL CDD ("",AC.TOD.P.L,AC.EXP.P.L,DAYS.P.L)
                    END
                    AC.BAL.LOAN2 = R.AC<AC.OPEN.ACTUAL.BAL>
******************1 DAYS*******************
                    IF ( DAYS.P.L EQ 1 OR DAYS.P.L EQ 0 ) THEN
                        AC13.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL13.LOAN2.P.L += AC13.P.L
                    END

                    XX13.LOAN2.P.L = AC.BAL13.LOAN2.P.L
                    IF XX13.LOAN2.P.L = '' THEN
                        XX13.LOAN2.P.L = 0
                    END
******************2 DAYS*******************
                    IF DAYS.P.L EQ 2 THEN
                        AC14.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL1.LOAN2.P.L += AC14.P.L
                    END

                    XX2.LOAN2.P.L = AC.BAL1.LOAN2.P.L
                    IF XX2.LOAN2.P.L = '' THEN
                        XX2.LOAN2.P.L = 0
                    END

****************3 DAYS********************
                    IF DAYS.P.L EQ 3 THEN
                        AC15.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL2.LOAN2.P.L +=   AC15.P.L
                    END
                    XX3.LOAN2.P.L = AC.BAL2.LOAN2.P.L
                    IF XX3.LOAN2.P.L = '' THEN
                        XX3.LOAN2.P.L = 0
                    END

**************4 DAYS*********************
                    IF DAYS.P.L EQ 4 THEN
                        AC16.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL4.LOAN2.P.L += AC16.P.L
                    END
                    XX4.LOAN2.P.L = AC.BAL4.LOAN2.P.L
                    IF XX4.LOAN2.P.L = '' THEN
                        XX4.LOAN2.P.L = 0
                    END
************5 DAYS**************
                    IF  DAYS.P.L EQ 5 THEN
                        AC17.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL5.LOAN2.P.L += AC17.P.L
                    END
                    XX5.LOAN2.P.L = AC.BAL5.LOAN2.P.L
                    IF XX5.LOAN2.P.L = '' THEN
                        XX5.LOAN2.P.L = 0
                    END
***************2 WEEKS***********
                    IF DAYS.P.L GT 5 AND DAYS.P.L LE 14  THEN
                        AC18.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL6.LOAN2.P.L += AC18.P.L
                    END
                    XX6.LOAN2.P.L = AC.BAL6.LOAN2.P.L
                    IF XX6.LOAN2.P.L = '' THEN
                        XX6.LOAN2.P.L = 0
                    END
****************3 WEEKS**************
                    IF DAYS.P.L GT 14 AND DAYS.P.L LE 21 THEN
                        AC19.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL7.LOAN2.P.L += AC19.P.L
                    END
                    XX7.LOAN2.P.L = AC.BAL7.LOAN2.P.L
                    IF XX7.LOAN2.P.L = '' THEN
                        XX7.LOAN2.P.L = 0
                    END
***************4 WEEKS********************
                    IF DAYS.P.L GT 21 AND DAYS.P.L LE 28 THEN
                        AC20.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL8.LOAN2.P.L += AC20.P.L
                    END
                    XX8.LOAN2.P.L = AC.BAL8.LOAN2.P.L
                    IF XX8.LOAN2.P.L = '' THEN
                        XX8.LOAN2.P.L = 0
                    END

***************5 WEEKS********************
                    IF DAYS.P.L GT 28 AND DAYS.P.L LE 35 THEN
                        AC21.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL9.LOAN2.P.L += AC21.P.L
                    END
                    XX9.LOAN2.P.L = AC.BAL9.LOAN2.P.L
                    IF XX9.LOAN2.P.L = '' THEN
                        XX9.LOAN2.P.L = 0
                    END
***************6 WEEKS********************
                    IF DAYS.P.L GT 35 AND DAYS.P.L LE 42 THEN
                        AC22.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL10.LOAN2.P.L += AC22.P.L
                    END
                    XX10.LOAN2.P.L = AC.BAL10.LOAN2.P.L
                    IF XX10.LOAN2.P.L = '' THEN
                        XX10.LOAN2.P.L = 0
                    END
***************7 WEEKS********************
                    IF DAYS.P.L GT 42 AND DAYS.P.L LE 49 THEN
                        AC23.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL11.LOAN2.P.L += AC23.P.L
                    END
                    XX11.LOAN2.P.L = AC.BAL11.LOAN2.P.L
                    IF XX11.LOAN2.P.L = '' THEN
                        XX11.LOAN2.P.L = 0
                    END
***************8 WEEKS********************
                    IF DAYS.P.L GT 49 AND DAYS.P.L LE 56 THEN
                        AC24.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL12.LOAN2.P.L += AC24.P.L
                    END
                    XX12.LOAN2.P.L = AC.BAL12.LOAN2.P.L
                    IF XX12.LOAN2.P.L = '' THEN
                        XX12.LOAN2.P.L = 0
                    END
*******************OVER 8 WEEKS********************************
                    IF DAYS.P.L GT 56 THEN
                        AC24.OV.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                        AC.BAL12.LOAN2.OV.P.L += AC24.OV.P.L
                    END
                    XX12.LOAN2.OV.P.L = AC.BAL12.LOAN2.OV.P.L
                    IF XX12.LOAN2.OV.P.L = '' THEN
                        XX12.LOAN2.OV.P.L = 0
                    END

                END


***************************ON*************************
                IF  (CUS.LIM.P.L EQ '' OR CUS.LIM.P.L EQ 'NOSTRO') THEN
                    AC.BAL.LOAN2.P.L = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL3.LOAN2.P.L += AC.BAL.LOAN2.P.L
                END
                XX1.LOAN2.P.L = AC.BAL3.LOAN2.P.L
                IF XX1.LOAN2.P.L = '' THEN
                    XX1.LOAN2.P.L = 0
                END
            END
        NEXT NNNNN
*=============================END PERSONAL LOANS=====================
*=============================BEGIN MATURING SECURITIES=============
*DEBUG
        T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 12009 12010 12008 12011 12025 12014 12028 12037 12036 12040 12031 12033 12043 12048 12041 12054) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 AND OPEN.ACTUAL.BAL NE '' "
********* T.SEL4="SELECT FBNK.ACCOUNT WITH CATEGORY IN (12008 ) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL LT 0 AND OPEN.ACTUAL.BAL NE '' "


* T.SEL4="SELECT FBNK.ACCOUNT WITH @ID EQ EGP1203300690099  EGP1203300840099  EGP1200900010099 "
        CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ERR.MSG)
* TEXT = "SEL4 BEGIN MATURING SECURITIES : " : SELECTED4 ; CALL REM
        FOR M = 1 TO SELECTED4
            CALL F.READ(FN.AC,KEY.LIST4<M>,R.AC,F.AC,ERR)
            CUS.ID4      = R.AC<AC.CUSTOMER>
            CUS.LIM4     = R.AC<AC.LIMIT.REF>
            MATUR        = R.AC<AC.LOCAL.REF><1,ACLR.MATUR.DATE>
            AC.BAL.LOAN1 = R.AC<AC.OPEN.ACTUAL.BAL>
* TEXT = "CUS.LIM4 : " : CUS.LIM4 ; CALL REM
* TEXT = "MATUR : " : MATUR ; CALL REM
**************UPPPPPPPPPPP**********
            AC.TOD4.MAT  = TODAY
            DAYS.44.MAT  = 'C'
            IF MATUR GE AC.TOD4.MAT THEN
                CALL CDD ("",AC.TOD4.MAT,MATUR,DAYS.44.MAT)
            END
*******************END UPPPPPPPPPPPPPDATE**************
            IF NOT(CUS.ID4) THEN CONTINUE ; * R15 BAU Fix
            CALL F.READ(FN.CU,CUS.ID4,R.CU,F.CU,ERR)
            SEC.NEW=R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
**IF SEC.NEW EQ 4650 THEN
            IF( CUS.LIM4 NE '' AND CUS.LIM4 NE '0' AND CUS.LIM4 NE 'NOSTRO' ) OR (MATUR NE '')  THEN
                LMT.REF4    = FIELD(CUS.LIM4,'.',1)
                IF LEN(LMT.REF4) EQ 3 THEN
                    XY4 = CUS.ID4:'.':'0000':CUS.LIM4
                END
                IF LEN(LMT.REF4) EQ 4 THEN
                    XY4 = CUS.ID4:'.':'000':CUS.LIM4
                END
*TEXT = "XY: ":XY ; CALL REM
                CALL F.READ(FN.LIM,XY4,R.LIM,F.LIM,E2)
*Line [ 1414 ] Update LI to LILR - ITSS - R21 Upgrade - 2021-12-26
                AC.EXP4  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD4  = TODAY
                DAYS.44 = 'C'
                IF AC.EXP4 GE AC.TOD4 THEN
                    CALL CDD ("",AC.TOD4,AC.EXP4,DAYS.44)
                END

******************1 DAYS*******************
                IF (DAYS.44 = 1 OR DAYS.44.MAT = 1 ) THEN
                    AC25 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL13.LOAN1 += AC25
                END
                XX13.LOAN1 = AC.BAL13.LOAN1
                IF XX13.LOAN1  = '' THEN
                    XX13.LOAN1 = 0
                END

******************2 DAYS*******************
                IF (DAYS.44 = 2 OR DAYS.44.MAT = 2 ) THEN
                    AC26 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL1.LOAN1 += AC26
                END
                XX2.LOAN1 = AC.BAL1.LOAN1
                IF XX2.LOAN1 = '' THEN
                    XX2.LOAN1 = 0
                END

****************3 DAYS********************
                IF (DAYS.44 = 3 OR DAYS.44.MAT = 3 ) THEN
                    AC27 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL2.LOAN1 +=   AC27
                END
                XX3.LOAN1 = AC.BAL2.LOAN1
                IF XX3.LOAN1 = '' THEN
                    XX3.LOAN1 = 0
                END
**************4 DAYS*********************
                IF (DAYS.44 = 4 OR DAYS.44.MAT = 4 ) THEN
                    AC28 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL4.LOAN1 += AC28
                END
                XX4.LOAN1 = AC.BAL4.LOAN1
                IF XX4.LOAN1 = '' THEN
                    XX4.LOAN1 = 0
                END
************5 DAYS**************
                IF  (DAYS.44 = 5 OR DAYS.44.MAT = 5 ) THEN
                    AC29 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL5.LOAN1 += AC29
                END
                XX5.LOAN1 = AC.BAL5.LOAN1
                IF XX5.LOAN1 = '' THEN
                    XX5.LOAN1 = 0
                END
***************2 WEEKS***********
                IF (DAYS.44 GT 5 AND DAYS.44 LE 14) OR (DAYS.44.MAT GT 5 AND DAYS.44.MAT LE 14) THEN
                    AC30 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL6.LOAN1 += AC30
                END
                XX6.LOAN1 = AC.BAL6.LOAN1
                IF XX6.LOAN1 = '' THEN
                    XX6.LOAN1 = 0
                END
****************3 WEEKS**************
                IF (DAYS.44 GT 14 AND DAYS.44 LE 21) OR (DAYS.44.MAT GT 14 AND DAYS.44.MAT LE 21) THEN
                    AC31 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL7.LOAN1 += AC31
                END
                XX7.LOAN1 = AC.BAL7.LOAN1
                IF XX7.LOAN1 = '' THEN
                    XX7.LOAN1 = 0
                END
***************4 WEEKS********************
                IF (DAYS.44 GT 21 AND DAYS.44 LE 28) OR (DAYS.44.MAT GT 21 AND DAYS.44.MAT LE 28) THEN
                    AC32 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL8.LOAN1 += AC32
                END
                XX8.LOAN1 = AC.BAL8.LOAN1
                IF XX8.LOAN1 = '' THEN
                    XX8.LOAN1 = 0
                END

***************5 WEEKS********************
                IF (DAYS.44 GT 28 AND DAYS.44 LE 35) OR (DAYS.44.MAT GT 28 AND DAYS.44.MAT LE 35) THEN
                    AC33 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL9.LOAN1 += AC33
                END
                XX9.LOAN1 = AC.BAL9.LOAN1
                IF XX9.LOAN1 = '' THEN
                    XX9.LOAN1 = 0
                END
***************6 WEEKS********************
                IF (DAYS.44 GT 35 AND DAYS.44 LE 42) OR (DAYS.44.MAT GT 35 AND DAYS.44.MAT LE 42) THEN
                    AC34 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL10.LOAN1 += AC34
                END
                XX10.LOAN1 = AC.BAL10.LOAN1
                IF XX10.LOAN1 = '' THEN
                    XX10.LOAN1 = 0
                END
***************7 WEEKS********************
                IF (DAYS.44 GT 42 AND DAYS.44 LE 49) OR (DAYS.44.MAT GT 42 AND DAYS.44.MAT LE 49) THEN
                    AC35 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL11.LOAN1 += AC35
                END
                XX11.LOAN1 = AC.BAL11.LOAN1
                IF XX11.LOAN1 = '' THEN
                    XX11.LOAN1 = 0
                END
***************8 WEEKS********************
                IF (DAYS.44 GT 49 AND DAYS.44 LE 56) OR (DAYS.44.MAT GT 49 AND DAYS.44.MAT LE 56) THEN
                    AC36 = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL12.LOAN1 += AC36
                END
                XX12.LOAN1 = AC.BAL12.LOAN1
                IF XX12.LOAN1 = '' THEN
                    XX12.LOAN1 = 0
                END
***************OVER 8 WEEKS********************
                IF (DAYS.44 GT 56) OR (DAYS.44.MAT GT 56) THEN
                    AC36.OV = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL12.LOAN1.OV += AC36.OV
                END
                XX12.LOAN1.OV = AC.BAL12.LOAN1.OV
                IF XX12.LOAN1.OV = '' THEN
                    XX12.LOAN1.OV = 0
                END
            END

***************************ON*************************
*DEBUG
            IF  (CUS.LIM4 EQ '' ) AND (MATUR EQ '')  THEN
                T.SEL.EZN = "SELECT F.SCB.FIN.OZON WITH ACCOUNT.NO EQ ":KEY.LIST4<M>:" AND MATURITY.DATE GE  ":HHH
*** T.SEL.EZN = "SELECT F.SCB.FIN.OZON WITH ACCOUNT.NO EQ EGP1200800010099 AND MATURITY.DATE EQ 20111004"
                CALL EB.READLIST(T.SEL.EZN,KEY.LIST4.EZN,"",SELECTED4.EZN,ERR.MSG)

                IF SELECTED4.EZN THEN
                    FOR JJJ  = 1 TO SELECTED4.EZN
                        CALL F.READ(FN.EZN,KEY.LIST4.EZN<JJJ>,R.EZN,F.EZN,ERR)
                        MAT.EZN = R.EZN<OZN.MATURITY.DATE>
                        OP.EZN  = R.EZN<OZN.RECORDED.VALUE>

                        AC.TOD4.EZN  = TODAY
                        DAYS.44.EZN  = 'C'
                        IF MAT.EZN GE AC.TOD4.EZN THEN
                            CALL CDD ("",AC.TOD4.EZN,MAT.EZN,DAYS.44.EZN)
                        END

*TEXT = MAT.EZN ; CALL REM
*TEXT = DAYS.44.EZN ; CALL REM
                        IF MAT.EZN NE '' THEN
******************1 DAYS*******************
                            IF (DAYS.44.EZN = 1 OR DAYS.44.EZN = 0) THEN
*TEXT = "IN 1 " ; CALL REM
                                AC25 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL13.LOAN1 += AC25

                            END
*TEXT = AC.BAL13.LOAN1 ; CALL REM
                            XX13.LOAN1 =  AC.BAL13.LOAN1
                            IF XX13.LOAN1 = '' THEN
                                XX13.LOAN1 = 0
                            END
******************2 DAYS*******************
                            IF (DAYS.44.EZN = 2 ) THEN
                                AC26 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL1.LOAN1 += AC26
                            END
                            XX2.LOAN1 =  AC.BAL1.LOAN1
                            IF XX2.LOAN1 = '' THEN
                                XX2.LOAN1 = 0
                            END

****************3 DAYS********************
                            IF (DAYS.44.EZN = 3 ) THEN
                                AC27 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL2.LOAN1 +=   AC27
                            END
                            XX3.LOAN1 =  AC.BAL2.LOAN1
                            IF XX3.LOAN1 = '' THEN
                                XX3.LOAN1 = 0
                            END
**************4 DAYS*********************
                            IF (DAYS.44.EZN = 4 ) THEN
                                AC28 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL4.LOAN1 += AC28
                            END
                            XX4.LOAN1 = AC.BAL4.LOAN1
                            IF XX4.LOAN1 = '' THEN
                                XX4.LOAN1 = 0
                            END
************5 DAYS********************
                            IF  (DAYS.44.EZN = 5 ) THEN
                                AC29 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL5.LOAN1 += AC29
                            END
                            XX5.LOAN1 = AC.BAL5.LOAN1
                            IF XX5.LOAN1 = '' THEN
                                XX5.LOAN1 = 0
                            END
***************2 WEEKS***********
                            IF (DAYS.44.EZN GT 5 AND DAYS.44.EZN LE 14)  THEN
                                AC30 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL6.LOAN1 += AC30
                            END
                            XX6.LOAN1 = AC.BAL6.LOAN1
                            IF XX6.LOAN1 = '' THEN
                                XX6.LOAN1 = 0
                            END
****************3 WEEKS**************
                            IF (DAYS.44.EZN GT 14 AND DAYS.44.EZN LE 21) THEN
                                AC31 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL7.LOAN1 += AC31
                            END
                            XX7.LOAN1 = AC.BAL7.LOAN1
                            IF XX7.LOAN1 = '' THEN
                                XX7.LOAN1 = 0
                            END
***************4 WEEKS********************
                            IF (DAYS.44.EZN GT 21 AND DAYS.44.EZN LE 28) THEN
                                AC32 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL8.LOAN1 += AC32
                            END
                            XX8.LOAN1 = AC.BAL8.LOAN1
                            IF XX8.LOAN1 = '' THEN
                                XX8.LOAN1 = 0
                            END

***************5 WEEKS********************
                            IF (DAYS.44.EZN GT 28 AND DAYS.44.EZN LE 35) THEN
                                AC33 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL9.LOAN1 += AC33
                            END
                            XX9.LOAN1 = AC.BAL9.LOAN1
                            IF XX9.LOAN1 = '' THEN
                                XX9.LOAN1 = 0
                            END
***************6 WEEKS********************
                            IF (DAYS.44.EZN GT 35 AND DAYS.44.EZN LE 42) THEN
                                AC34 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL10.LOAN1 += AC34
                            END
                            XX10.LOAN1 = AC.BAL10.LOAN1
                            IF XX10.LOAN1 = '' THEN
                                XX10.LOAN1 = 0
                            END

***************7 WEEKS********************
                            IF (DAYS.44.EZN GT 42 AND DAYS.44.EZN LE 49) THEN
                                AC35 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL11.LOAN1 += AC35
                            END
                            XX11.LOAN1 = AC.BAL11.LOAN1
                            IF XX11.LOAN1 = '' THEN
                                XX11.LOAN1 = 0
                            END

***************8 WEEKS********************
                            IF (DAYS.44.EZN GT 49 AND DAYS.44.EZN LE 56) THEN
                                AC36 = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL12.LOAN1 += AC36
                            END
                            XX12.LOAN1 = AC.BAL12.LOAN1
                            IF XX12.LOAN1 = '' THEN
                                XX12.LOAN1 = 0
                            END
***************OVER 8 WEEKS********************
                            IF (DAYS.44.EZN GT 56) THEN
                                AC36.OV = R.EZN<OZN.RECORDED.VALUE>
                                AC.BAL12.LOAN1.OV +=   AC36.OV
                            END
                            XX12.LOAN1.OV = AC.BAL12.LOAN1.OV

                            IF XX12.LOAN1.OV = '' THEN
                                XX12.LOAN1.OV = 0
                            END
                        END
                    NEXT JJJ
                END ELSE

                    AC.BAL.LOAN1   = R.AC<AC.OPEN.ACTUAL.BAL> * (-1)
                    AC.BAL3.LOAN1 += AC.BAL.LOAN1
                END
                XX1.LOAN1 = AC.BAL3.LOAN1
                IF AC.BAL3.LOAN1 EQ '' THEN
                    AC.BAL3.LOAN1 = 0
                END
            END
        NEXT M
*=============================END MATURING SECUTITIES===============
**********************deposits******************************
*=================BEGINING INTER BANK DEPOSITS ===================================================
* T.SEL12= "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 2002 2003 2000 5021 5000 2001 5001 5020 ) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
        T.SEL12= "SELECT FBNK.ACCOUNT WITH CATEGORY IN ( 2002 2003 2000 5021 5000 2001 5001 5020 ) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "

        CALL EB.READLIST(T.SEL12,KEY.LIST12,"",SELECTED12,ER.MSG)
**  TEXT = "OUTFLOW INTER BANK DEPOSITS:":SELECTED12 ; CALL REM
        FOR II = 1 TO SELECTED12
            CALL F.READ(FN.AC,KEY.LIST12<II>,R.AC,F.AC,E2)
            CUS.LIM.D = R.AC<AC.LIMIT.REF>
            CUS.ID.D  = R.AC<AC.CUSTOMER>
            AC.BAL.D  = R.AC<AC.OPEN.ACTUAL.BAL>
            IF( CUS.LIM.D NE '' AND CUS.LIM.D NE '0' AND CUS.LIM.D NE 'NOSTRO' ) THEN
                LMT.REF1.D    = FIELD(CUS.LIM.D,'.',1)
                IF LEN(LMT.REF1.D) EQ 3 THEN
                    XX.D = CUS.ID.D:'.':'0000':CUS.LIM.D
                END
                IF LEN(LMT.REF1.D) EQ 4 THEN
                    XX.D = CUS.ID.D:'.':'000':CUS.LIM.D
                END
*TEXT = "XX: ":XX ; CALL REM
                CALL F.READ(FN.LIM,XX.D,R.LIM,F.LIM,E2)
*Line [ 1727 ] Update LI to LILR - ITSS - R21 Upgrade - 2021-12-26
                AC.EXP.D  = R.LIM<LI.EXPIRY.DATE>
                AC.TOD.D  = TODAY
                YDAYS = ''
                DAYS.D = 'C'
                IF AC.EXP.D GE AC.TOD.D THEN
                    CALL CDD ("",AC.TOD.D,AC.EXP.D,DAYS.D)
                END
                AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
***************1 DAY********************
                IF DAYS.D = 1 THEN
                    AC.BAL13.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
******************2 DAYS*******************
                IF DAYS.D = 2 THEN
                    AC.BAL1.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
****************3 DAYS********************
                IF DAYS.D = 3 THEN
                    AC.BAL2.D +=   R.AC<AC.OPEN.ACTUAL.BAL>
                END
**************4 DAYS*********************
                IF DAYS.D = 4 THEN
                    AC.BAL4.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************ 5 DAYS**************
                IF  DAYS.D =  5 THEN
                    AC.BAL5.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
***************2 WEEKS***********
                IF DAYS.D GE 5 AND DAYS.D LE 14 THEN
                    AC.BAL6.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
****************3 WEEKS**************
                IF DAYS.D GE 14 AND DAYS.D LE 21 THEN
                    AC.BAL7.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************** 4 WEEKS********************
                IF DAYS.D GE 21 AND DAYS.D LE 28  THEN
                    AC.BAL8.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************** 5 WEEKS********************
                IF DAYS.D GE 28 AND DAYS.D LE 35  THEN
                    AC.BAL9.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************** 6 WEEKS********************
                IF DAYS.D GE 35 AND DAYS.D LE 42  THEN
                    AC.BAL10.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************** 7 WEEKS********************
                IF DAYS.D GE 42 AND DAYS.D LE 49  THEN
                    AC.BAL11.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
************** 8 WEEKS********************
                IF DAYS.D GE 49 AND DAYS.D LE 56  THEN
                    AC.BAL12.D += R.AC<AC.OPEN.ACTUAL.BAL>
                END
            END
***************************ON*************************
            IF  ( CUS.LIM.D EQ '' OR CUS.LIM.D EQ 'NOSTRO') THEN
                AC.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
                AC.BAL3.D += AC.BAL
            END
        NEXT II

*       TEXT = "AC.BAL3.D : " : AC.BAL3.D ; CALL REM
***************************************************
        T.SEL22 = "SELECT FBNK.MM.MONEY.MARKET WITH CATEGORY IN (21030 21031) AND MATURITY.DATE GE ":HHH:" AND CURRENCY EQ ":CURR.ID:" AND ( STATUS NE LIQ OR VALUE.DATE EQ ":HHH:") "
* T.SEL2 = "SELECT FBNK.MM.MONEY.MARKET WITH @ID IN ( MM1108100009 MM1019600004 )"
        CALL EB.READLIST(T.SEL22,KEY.LIST22,"",SELECTED22,ER.MSG22)
*TEXT = "OUTFLOW 2 INTERBANK " : SELECTED22 ; CALL REM
        FOR JJ = 1 TO SELECTED22
            CALL F.READ(FN.MM,KEY.LIST22<JJ>,R.MM,F.MM,E22)
            CUS.ID.MM.D  = R.MM<MM.CUSTOMER.ID>
            MM.EXP.D     = R.MM<MM.MATURITY.DATE>
            MM.AMT.D     = R.MM<MM.PRINCIPAL>
            CALL F.READ(FN.CU,CUS.ID.MM.D,R.CU,F.CU,E2)
            SEC.D     = R.CU<EB.CUS.SECTOR>
            NEW.SEC.D = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            AC.TOD.D = TODAY
            DAYS.MM.D =  'C'
            IF MM.EXP.D GE AC.TOD.D THEN
                CALL CDD ("",AC.TOD.D,MM.EXP.D,DAYS.MM.D)
            END
***************1 DAYS********************
            IF DAYS.MM.D = 1 THEN
                AC.BAL13.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX13.D = AC.BAL13.D + AC.BAL13.MM.D

            IF XX13.D = '' THEN
                XX13.D = 0
            END

******************2 DAYS*******************
            IF DAYS.MM.D = 2 THEN
                AC.BAL1.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX2.D = AC.BAL1.D + AC.BAL1.MM.D
            IF XX2.D = '' THEN
                XX2.D = 0
            END
****************3 DAYS********************
            IF DAYS.MM.D = 3 THEN
                AC.BAL2.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX3.D = AC.BAL2.D + AC.BAL2.MM.D
            IF XX3.D = '' THEN
                XX3.D = 0
            END
**************4 DAYS*********************
            IF DAYS.MM.D = 4 THEN
                AC.BAL4.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX4.D = AC.BAL4.D + AC.BAL4.MM.D
            IF XX4.D = '' THEN
                XX4.D = 0
            END
************5 DAYS**************
            IF  DAYS.MM.D = 5 THEN
                AC.BAL5.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX5.D = AC.BAL5.D + AC.BAL5.MM.D
            IF XX5.D = '' THEN
                XX5.D = 0
            END

***************2 WEEKS**********
            IF DAYS.MM.D GT 5 AND DAYS.MM.D LE 14 THEN
                AC.BAL6.MM.D += R.MM<MM.PRINCIPAL>

            END
            XX6.D = AC.BAL6.D + AC.BAL6.MM.D
            IF XX6.D = '' THEN
                XX6.D = 0
            END

****************3 WEEKS**************
            IF DAYS.MM.D GT 14 AND DAYS.MM.D LE 21 THEN
                AC.BAL7.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX7.D = AC.BAL7.D + AC.BAL7.MM.D
            IF XX7.D = '' THEN
                XX7.D = 0
            END

***************4 WEEKS********************
            IF DAYS.MM.D GT 21 AND DAYS.MM.D LE 28 THEN
                AC.BAL8.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX8.D = AC.BAL8.D + AC.BAL8.MM.D

            IF XX8.D = '' THEN
                XX8.D = 0
            END
***************5 WEEKS********************
            IF DAYS.MM.D GT 28 AND DAYS.MM.D LE 35 THEN
                AC.BAL9.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX9.D = AC.BAL9.D + AC.BAL9.MM.D

            IF XX9.D = '' THEN
                XX9.D = 0
            END
***************6 WEEKS********************
            IF DAYS.MM.D GT 35 AND DAYS.MM.D LE 42 THEN
                AC.BAL10.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX10.D = AC.BAL10.D + AC.BAL10.MM.D

            IF XX10.D = '' THEN
                XX10.D = 0
            END
***************7 WEEKS********************
            IF DAYS.MM.D GT 42 AND DAYS.MM.D LE 49 THEN
                AC.BAL11.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX11.D = AC.BAL11.D + AC.BAL11.MM.D

            IF XX11.D = '' THEN
                XX11.D = 0
            END
***************8 WEEKS********************
            IF DAYS.MM.D GT 49 AND DAYS.MM.D LE 56 THEN
                AC.BAL12.MM.D += R.MM<MM.PRINCIPAL>
            END
            XX12.D = AC.BAL12.D + AC.BAL12.MM.D

            IF XX12.D = '' THEN
                XX12.D = 0
            END

***************************ON*************************
            IF  MM.EXP.D EQ ''  THEN
                AC.BAL3.MM.D += R.MM<MM.PRINCIPAL>
            END
        NEXT JJ
        XX1.D = AC.BAL3.D
        IF XX1.D = '' THEN
            XX1.D = 0
        END
*TEXT = "XX1.D :": XX1.D ; CALL REM
*==============================END INTER BANK DEPOSITS=======================
******* LD  *************

        T.SEL = "SELECT ":FN.LD:" WITH ((CATEGORY GE 21001 AND CATEGORY LE 21010) OR (CATEGORY GE 21020 AND CATEGORY LE 21029) OR (CATEGORY EQ 21032)) AND (( VALUE.DATE LE ":HHH:" AND AMOUNT NE 0 ) OR ( FIN.MAT.DATE GT ":HHH:" AND AMOUNT EQ 0 )) AND CURRENCY EQ ":CURR.ID
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR G = 1 TO SELECTED
                CALL F.READ(FN.LD,KEY.LIST<G>,R.LD,F.LD,E1)
                CATEG    = R.LD<LD.CATEGORY>
                AMT      = R.LD<LD.AMOUNT>
                IF AMT EQ 0 THEN
                    AMT  = R.LD<LD.REIMBURSE.AMOUNT>
                END
                CUS.ID   = R.LD<LD.CUSTOMER.ID>
                EXP.DATE = R.LD<LD.FIN.MAT.DATE>
                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
                CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

***********GET NO OF DAYS*************
                TD.DATE = TODAY
                DAYS.55  = 'C'
                IF EXP.DATE GE TD.DATE THEN
                    CALL CDD ("",TD.DATE,EXP.DATE,DAYS.55)
                END
************END NO OF DAYS**************
****** OVER NIGHT **************
                IF EXP.DATE EQ '' THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL28.P  += AMT
                    END ELSE
                        BAL28.C  += AMT
                    END
                END
********END OVER NIGHT *********
                IF  EXP.DATE NE '' THEN
                    IF DAYS.55 GE 0 AND DAYS.55 LE 1 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL1.P += AMT
                        END ELSE
                            BAL1.C  += AMT
                        END
                    END

                    IF DAYS.55 = 2 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL2.P  += AMT
                        END ELSE
                            BAL2.C += AMT
                        END
                    END

                    IF DAYS.55 = 3 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL3.P  += AMT
                        END ELSE
                            BAL3.C += AMT
                        END
                    END

                    IF DAYS.55 = 4 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL4.P  += AMT
                        END ELSE
                            BAL4.C  += AMT
                        END
                    END
                    IF DAYS.55 = 5 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL5.P  += AMT
                        END ELSE
                            BAL5.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 5 AND DAYS.55 LE 14 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL6.P  += AMT
                        END ELSE
                            BAL6.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 14 AND DAYS.55 LE 21 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL7.P  += AMT
                        END ELSE
                            BAL7.C  += AMT
                        END
                    END

                    IF DAYS.55 GT 21 AND DAYS.55 LE 28 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL8.P  += AMT
                        END ELSE
                            BAL8.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 28 AND DAYS.55 LE 35 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL9.P  += AMT
                        END ELSE
                            BAL9.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 35 AND DAYS.55 LE 42 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL10.P  += AMT
                        END ELSE
                            BAL10.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 42 AND DAYS.55 LE 49 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL11.P  += AMT
                        END ELSE
                            BAL11.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 49 AND DAYS.55 LE 56 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL12.P  += AMT
                        END ELSE
                            BAL12.C  += AMT
                        END
                    END
                    IF DAYS.55 GT 56 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL13.P  += AMT
                        END ELSE
                            BAL13.C  += AMT
                        END
                    END
                END
            NEXT G
        END
****************************************
******* AC  *************

        T.SEL  = "SELECT ":FN.AC:" WITH  CATEGORY IN (6512 6501 6502 6503 6504 6511 1012 1013 1014 1015 1016 1019 16170) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0"
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR GG = 1 TO SELECTED
                CALL F.READ(FN.AC,KEY.LIST<GG>,R.AC,F.AC,E1)
                CATEG       = R.AC<AC.CATEGORY>
                AMT         = R.AC<AC.OPEN.ACTUAL.BAL>
                CUS.ID      = R.AC<AC.CUSTOMER>
                CUS.LIM     = R.AC<AC.LIMIT.REF>
                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
                CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
****** OVER NIGHT **************
                IF (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL27.P  += AMT
                    END ELSE
                        BAL27.C  += AMT
                    END
                END
*************************************
********BEGIN DAYS *****************
                IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                    LMT.REF1    = FIELD(CUS.LIM,'.',1)
                    IF LEN(LMT.REF1) EQ 3 THEN
                        XX = CUS.ID:'.':'0000':CUS.LIM
                    END
                    IF LEN(LMT.REF1) EQ 4 THEN
                        XX = CUS.ID:'.':'000':CUS.LIM
                    END
                    CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
*Line [ 2097 ] Update LI to LILR - ITSS - R21 Upgrade - 2021-12-26
                    AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD  = TODAY
                    YDAYS   = ''
                    DAYS    = 'C'
                    IF AC.EXP GE AC.TOD THEN
                        CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                    END

                    IF DAYS GE 0 AND DAYS LE 1 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL14.P  += AMT
                        END ELSE
                            BAL14.C  += AMT
                        END
                    END
                    IF DAYS = 2  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL15.P  += AMT
                        END ELSE
                            BAL15.C  += AMT
                        END
                    END
                    IF DAYS = 3  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL16.P  += AMT
                        END ELSE
                            BAL16.C  += AMT
                        END
                    END
                    IF DAYS = 4  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL17.P  += AMT
                        END ELSE
                            BAL17.C  += AMT
                        END
                    END
                    IF DAYS = 5  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL18.P  += AMT
                        END ELSE
                            BAL18.C  += AMT
                        END
                    END
                    IF DAYS GT 5 AND DAYS LE 14  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL19.P  += AMT
                        END ELSE
                            BAL19.C  += AMT
                        END
                    END
                    IF DAYS GT 14 AND DAYS LE 21  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL20.P  += AMT
                        END ELSE
                            BAL20.C  += AMT
                        END
                    END
                    IF DAYS GT 21 AND DAYS LE 28  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL21.P  += AMT
                        END ELSE
                            BAL21.C  += AMT
                        END
                    END
                    IF DAYS GT 28 AND DAYS LE 35  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL22.P  += AMT
                        END ELSE
                            BAL22.C  += AMT
                        END
                    END
                    IF DAYS GT 35 AND DAYS LE 42  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL23.P  += AMT
                        END ELSE
                            BAL23.C  += AMT
                        END
                    END
                    IF DAYS GT 42 AND DAYS LE 49  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL24.P  += AMT
                        END ELSE
                            BAL24.C  += AMT
                        END
                    END
                    IF DAYS GT 49 AND DAYS LE 56  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL25.P  += AMT
                        END ELSE
                            BAL25.C  += AMT
                        END
                    END
                    IF DAYS GE 56 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL26.P  += AMT
                        END ELSE
                            BAL26.C  += AMT
                        END
                    END
                END
            NEXT GG
        END

**************CURRECNT AC CORPRATE AND PERSONAL
        T.SEL  = "SELECT ":FN.AC:" WITH  CATEGORY IN (1001 1002 1003 1059 1102 1201 1202 1205 1206 1207 11242 1208 1211 1212 1216 1301 1302 1303 1377 1390 1399 1401 1402 1404 1405 1406 1414 1477 1501 1502 1503 1416 1504 1507 1508 1509 1510 1511 1512 1513 1514 1518 1519 1534 1544 1559 1560 1566 1577 1588 1599 11238 1407 1413 11240 11232 11239 1480 1481 1499 1408 1582 1483 1595 1493 1558 ) AND CURRENCY EQ ":CURR.ID:"  AND OPEN.ACTUAL.BAL GT 0 "
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR GGG = 1 TO SELECTED
                CALL F.READ(FN.AC,KEY.LIST<GGG>,R.AC,F.AC,E1)
                CATEG       = R.AC<AC.CATEGORY>
                AMT         = R.AC<AC.OPEN.ACTUAL.BAL>
                CUS.ID      = R.AC<AC.CUSTOMER>
                CUS.LIM     = R.AC<AC.LIMIT.REF>
                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
                CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
****** OVER NIGHT **************
                IF (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
                    IF CUST.SECTOR EQ 4650 THEN
                        BAL29.P  += AMT
                    END ELSE
                        BAL29.C  += AMT
                    END
                END
*********BEGIN DAYS
                IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                    LMT.REF1    = FIELD(CUS.LIM,'.',1)
                    IF LEN(LMT.REF1) EQ 3 THEN
                        XX = CUS.ID:'.':'0000':CUS.LIM
                    END
                    IF LEN(LMT.REF1) EQ 4 THEN
                        XX = CUS.ID:'.':'000':CUS.LIM
                    END
                    CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
*Line [ 2232 ] Update LI to LILR - ITSS - R21 Upgrade - 2021-12-26
                    AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD  = TODAY
                    YDAYS   = ''
                    DAYS    = 'C'
                    IF AC.EXP GE AC.TOD THEN
                        CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                    END
                    IF DAYS GE 0 AND DAYS LE 1 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL30.P  += AMT
                        END ELSE
                            BAL30.C  += AMT
                        END
                    END
                    IF DAYS = 2  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL31.P  += AMT
                        END ELSE
                            BAL31.C  += AMT
                        END
                    END
                    IF DAYS = 3  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL32.P  += AMT
                        END ELSE
                            BAL32.C  += AMT
                        END
                    END
                    IF DAYS = 4  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL33.P  += AMT
                        END ELSE
                            BAL33.C  += AMT
                        END
                    END
                    IF DAYS = 5  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL34.P  += AMT
                        END ELSE
                            BAL34.C  += AMT
                        END
                    END
                    IF DAYS GT 5 AND DAYS LE 14  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL35.P  += AMT
                        END ELSE
                            BAL35.C  += AMT
                        END
                    END
                    IF DAYS GT 14 AND DAYS LE 21  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL36.P  += AMT
                        END ELSE
                            BAL36.C  += AMT
                        END
                    END
                    IF DAYS GT 21 AND DAYS LE 28  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL37.P  += AMT
                        END ELSE
                            BAL37.C  += AMT
                        END
                    END
                    IF DAYS GT 28 AND DAYS LE 35  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL38.P  += AMT
                        END ELSE
                            BAL38.C  += AMT
                        END
                    END
                    IF DAYS GT 35 AND DAYS LE 42  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL39.P  += AMT
                        END ELSE
                            BAL39.C  += AMT
                        END
                    END
                    IF DAYS GT 42 AND DAYS LE 49  THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL39.P  += AMT
                        END ELSE
                            BAL39.C  += AMT
                        END
                    END
                    IF DAYS GT 49 AND DAYS LE 56 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL40.P  += AMT
                        END ELSE
                            BAL40.C  += AMT
                        END
                    END
                    IF DAYS GT 56 THEN
                        IF CUST.SECTOR EQ 4650 THEN
                            BAL41.P  += AMT
                        END ELSE
                            BAL41.C  += AMT
                        END
                    END
                END
            NEXT GGG
        END

***********BEGIN OTHER
        T.SEL  = "SELECT ":FN.AC:" WITH  CATEGORY IN (16151 16153 16188 16152 11380 16175 3011 3012 3013 3014 3017 3010 3005) AND CURRENCY EQ ":CURR.ID:" AND OPEN.ACTUAL.BAL GT 0 "
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR GGGG = 1 TO SELECTED
                CALL F.READ(FN.AC,KEY.LIST<GGGG>,R.AC,F.AC,E1)
                CATEG       = R.AC<AC.CATEGORY>
                AMT         = R.AC<AC.OPEN.ACTUAL.BAL>
                CUS.ID      = R.AC<AC.CUSTOMER>
                CUS.LIM     = R.AC<AC.LIMIT.REF>
                CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E1)
                CUST.SECTOR = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
****** OVER NIGHT **************
                IF (CUS.LIM EQ '' OR CUS.LIM EQ 'NOSTRO') THEN
* IF CUST.SECTOR EQ 4650 THEN
                    BAL42.P  += AMT
*END ELSE
*    BAL42.C  += AMT
* END
                END
*********BEGIN DAYS
                IF( CUS.LIM NE '' AND CUS.LIM NE '0' AND CUS.LIM NE 'NOSTRO' ) THEN
                    LMT.REF1    = FIELD(CUS.LIM,'.',1)
                    IF LEN(LMT.REF1) EQ 3 THEN
                        XX = CUS.ID:'.':'0000':CUS.LIM
                    END
                    IF LEN(LMT.REF1) EQ 4 THEN
                        XX = CUS.ID:'.':'000':CUS.LIM
                    END
                    CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,E2)
*Line [ 2366 ] Update LI to LILR - ITSS - R21 Upgrade - 2021-12-26
                    AC.EXP  = R.LIM<LI.EXPIRY.DATE>
                    AC.TOD  = TODAY
                    YDAYS   = ''
                    DAYS    = 'C'
                    IF AC.EXP GE AC.TOD THEN
                        CALL CDD ("",AC.TOD,AC.EXP,DAYS)
                    END
                    IF DAYS GE 0 AND DAYS LE 1 THEN
*  IF CUST.SECTOR EQ 4650 THEN
                        BAL43.P  += AMT
*  END ELSE
*      BAL43.C  += AMT
*  END
                    END
                    IF DAYS = 2  THEN
* IF CUST.SECTOR EQ 4650 THEN
                        BAL44.P  += AMT
* END ELSE
*     BAL44.C  += AMT
* END
                    END
                    IF DAYS = 3  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL45.P  += AMT
*END ELSE
*   BAL45.C  += AMT
* END
                    END
                    IF DAYS = 4  THEN
* IF CUST.SECTOR EQ 4650 THEN
                        BAL46.P  += AMT
* END ELSE
*     BAL46.C  += AMT
* END
                    END
                    IF DAYS = 5  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL47.P  += AMT
*END ELSE
*    BAL47.C  += AMT
*END
                    END
                    IF DAYS GT 5 AND DAYS LE 14  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL48.P  += AMT
*END ELSE
*    BAL48.C  += AMT
*END
                    END
                    IF DAYS GT 14 AND DAYS LE 21  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL49.P  += AMT
*END ELSE
*    BAL49.C  += AMT
*END
                    END
                    IF DAYS GT 21 AND DAYS LE 28  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL50.P  += AMT
*END ELSE
*    BAL50.C  += AMT
*END
                    END
                    IF DAYS GT 28 AND DAYS LE 35  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL51.P  += AMT
*END ELSE
*    BAL51.C  += AMT
*END
                    END
                    IF DAYS GT 35 AND DAYS LE 42  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL52.P  += AMT
*END ELSE
*    BAL52.C  += AMT
*END
                    END
                    IF DAYS GT 42 AND DAYS LE 49  THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL53.P  += AMT
*END ELSE
*    BAL53.C  += AMT
*END
                    END
                    IF DAYS GT 49 AND DAYS LE 56 THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL54.P  += AMT
*END ELSE
*    BAL54.C  += AMT
*END
                    END
                    IF DAYS GT 56 THEN
*IF CUST.SECTOR EQ 4650 THEN
                        BAL55.P  += AMT
*END ELSE
*    BAL55.C  += AMT
*END
                    END
                END
            NEXT GGGG
        END

        BB.DATA  = CURR.ID  : ',' : BRANCH
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*======================================WRITE CASH INFLOWS===============
        BB.DATA  = "CASH INFLOW":","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        BB.DATA := "INTERBANK.LOANS":","
        BB.DATA := " LOANS":","
        BB.DATA := "PERSONAL LOANS":","
        BB.DATA := "MATURING SECURITIES":","
        BB.DATA := "TOTAL INFLOW":","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*=========================================================================
        TOT.ON = XX1.16 + XX1.LOAN2 + XX1.LOAN1 + XX1.LOAN2.P
* BB.DATA  = XX1:","
* BB.DATA := XX1.LOAN2:","
* BB.DATA := XX1.LOAN2.P
* BB.DATA := XX1.LOAN1:","
* BB.DATA := TOT.ON:","

* WRITESEQ BB.DATA TO BB ELSE
*     PRINT " ERROR WRITE FILE "
* END
*************************ON*******************

**************************1 DAYS ************
        TOT.1DAYS = XX14 + XX13.LOAN2 + XX13.LOAN1 +XX13.LOAN1.EZN+ TOT.ON
        BB.DATA  = "1DAY":","
        BB.DATA := XX14 + XX1.16:","
        BB.DATA := XX13.LOAN2 + XX1.LOAN2:","
        BB.DATA := XX13.LOAN2.P.L + XX1.LOAN2.P.L:","
        BB.DATA := XX13.LOAN1.EZN + XX13.LOAN1 + XX1.LOAN1 :","
        BB.DATA := TOT.1DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 DAYS*****************
************************2 DAYS*******************
        TOT.2DAYS = XX3 + XX2.LOAN2 + XX2.LOAN2.P + XX2.LOAN1 + XX2.LOAN1.EZN
        BB.DATA  = "2DAYS":","
        BB.DATA := XX3:","
        BB.DATA := XX2.LOAN2:","
        BB.DATA := XX2.LOAN2.P.L:","
        BB.DATA := XX2.LOAN1 + XX2.LOAN1.EZN :","
        BB.DATA := TOT.2DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**********************2 DAYS************************
*********************3 DAYS********************
        TOT.3DAYS = XX4 + XX3.LOAN2 + XX3.LOAN2.P + XX3.LOAN1
        BB.DATA  = "3DAYS":","
        BB.DATA := XX4:","
        BB.DATA := XX3.LOAN2:","
        BB.DATA := XX3.LOAN2.P.L:","
        BB.DATA := XX3.LOAN1 + XX3.LOAN1.EZN :","
        BB.DATA := TOT.3DAYS:","

*TEXT = "BR.DATA : " : BB.DATA ; CALL REM
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************3 DAYS*******************
***************************4 DAYS*************
        TOT.4DAYS = XX5 + XX4.LOAN2 + XX4.LOAN2.P + XX4.LOAN1
        BB.DATA  = "4DAYS":","
        BB.DATA := XX5:","
        BB.DATA :=  XX4.LOAN2:","
        BB.DATA :=  XX4.LOAN2.P.L:","
        BB.DATA :=  XX4.LOAN1 + XX4.LOAN1.EZN :","
        BB.DATA :=  TOT.4DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 DAYS********************
*************************5 DAYS*************
        TOT.5DAYS = XX6 + XX5.LOAN2 + XX5.LOAN2.P + XX5.LOAN1
        BB.DATA  = "5DAYS":","
        BB.DATA := XX6:","
        BB.DATA :=  XX5.LOAN2:","
        BB.DATA :=  XX5.LOAN2.P.L:","
        BB.DATA :=  XX5.LOAN1 + XX5.LOAN1.EZN:","
        BB.DATA :=  TOT.5DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
************************5 DAYS******************
************************2 WEEKS*******************8
        TOT.2WEEKS = XX7 + XX6.LOAN2 + XX6.LOAN2.P + XX6.LOAN1
        BB.DATA  = "2WEEKS":","
        BB.DATA := XX7:","
        BB.DATA :=  XX6.LOAN2:","
        BB.DATA :=  XX6.LOAN2.P.L:","
        BB.DATA :=  XX6.LOAN1 + XX6.LOAN1.EZN:","
        BB.DATA := TOT.2WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************2 WEEKS***************
**************************3 WEEKS***************
        TOT.3WEEKS = XX8 + XX7.LOAN2 + XX7.LOAN2.P + XX7.LOAN1
        BB.DATA  = "3WEEKS":","
        BB.DATA := XX8:","
        BB.DATA :=  XX7.LOAN2:","
        BB.DATA :=  XX7.LOAN2.P.L:","
        BB.DATA :=  XX7.LOAN1 + XX7.LOAN1.EZN :","
        BB.DATA := TOT.3WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************3 WEEKS***************
*************************4 WEEKS********************
        TOT.4WEEKS = XX9 + XX8.LOAN2 + XX8.LOAN2.P + XX8.LOAN1
        BB.DATA  = "4WEEKS":","
        BB.DATA := XX9:","
        BB.DATA :=  XX8.LOAN2:","
        BB.DATA :=  XX8.LOAN2.P.L:","
        BB.DATA :=  XX8.LOAN1 + XX8.LOAN1.EZN:","
        BB.DATA := TOT.4WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 WEEKS*******************
*************************5 WEEKS********************
        TOT.5WEEKS = XX10 + XX9.LOAN2 + XX9.LOAN2.P + XX9.LOAN1
        BB.DATA  = "5WEEKS":","
        BB.DATA := XX10:","
        BB.DATA :=  XX9.LOAN2:","
        BB.DATA :=  XX9.LOAN2.P.L:","
        BB.DATA :=  XX9.LOAN1 + XX9.LOAN1.EZN:","
        BB.DATA := TOT.5WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************5 WEEKS*******************
*************************6 WEEKS********************
        TOT.6WEEKS = XX11 + XX10.LOAN2 + XX10.LOAN2.P + XX10.LOAN1
        BB.DATA  = "6WEEKS":","
        BB.DATA := XX11:","
        BB.DATA :=  XX10.LOAN2:","
        BB.DATA :=  XX10.LOAN2.P.L:","
        BB.DATA :=  XX10.LOAN1 + XX10.LOAN1.EZN :","
        BB.DATA :=  TOT.6WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************6 WEEKS*******************
*************************7 WEEKS********************
        TOT.7WEEKS = XX12 + XX11.LOAN2 + XX11.LOAN2.P + XX11.LOAN1
        BB.DATA  = "7WEEKS":","
        BB.DATA := XX12:","
        BB.DATA :=  XX11.LOAN2:","
        BB.DATA :=  XX11.LOAN2.P.L:","
        BB.DATA :=  XX11.LOAN1 + XX11.LOAN1.EZN:","
        BB.DATA :=  TOT.7WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************7 WEEKS*******************
*******************8 WEEKS*******************
        TOT.8WEEKS = XX13 + XX12.LOAN2 + XX12.LOAN2.P + XX12.LOAN1
        BB.DATA  = "8WEEKS":","
        BB.DATA := XX13:","
        BB.DATA :=  XX12.LOAN2:","
        BB.DATA :=  XX12.LOAN2.P.L:","
        BB.DATA :=  XX12.LOAN1 + XX12.LOAN1.EZN :","
        BB.DATA :=  TOT.8WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************8 WEEKS*****************************
******************OVER 8 WEEKS*******************
        TOT.8WEEKS.OV = AC.BAL20.MM + XX12.LOAN2.OV + XX12.LOAN2.P.OV
        BB.DATA = "OVER 8WEEKS":","
        BB.DATA := AC.BAL20.MM : ","
        BB.DATA := XX12.LOAN2.OV : ","
        BB.DATA := XX12.LOAN2.OV.P.L : ","
        BB.DATA := XX12.LOAN1.OV + XX12.LOAN1.OV.EZN: ","
        BB.DATA := TOT.8WEEKS.OV
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

**********************WRITE OUTFLOW********************
        BB.DATA  = "CASH OUTFLOW":","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        BB.DATA := "INTERBANK DEPOSITS":","
        BB.DATA := "DEPOSITS":","
        BB.DATA := "PERSONAL DEPOSITS":","
        BB.DATA := "CURRENT ACCOUNT":","
        BB.DATA := "PERSONAL CURRENT AC":","
        BB.DATA := "OTHER":","
        BB.DATA := "TOTAL OUTFLOW":","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************************************************
*****************************ON***********
        TOT.ON.D = XX1.D + XX1.LOAN2.DD + XX1.LOAN2.DD.P.D +  XX1.OVER.33.D + XX1.OVER.44 + XX1.LOAN1.CHQ
*  BB.DATA  = XX1.D:","
*  BB.DATA := XX1.LOAN2.DD :","
*  BB.DATA := XX1.OVER.33.D:","
*  BB.DATA := XX1.OVER.44:","
*  BB.DATA := TOT.ON.D

* WRITESEQ BB.DATA TO BB ELSE
*    PRINT " ERROR WRITE FILE "
* END
*************************ON*******************
**************************1 DAYS ************

        TOT.13.D = XX13.D + XX13.LOAN2.DD + XX13.LOAN2.DD.P.D +  XX13.OVER.33.D + XX13.OVER.33.D.P.C + TOT.ON.D
        BB.DATA  = "1DAY":","
        BB.DATA := XX13.D + XX1.D:","
        BB.DATA := BAL1.C  + BAL14.C + BAL27.C + BAL28.C:","
        BB.DATA := BAL1.P  + BAL14.P + BAL27.P + BAL28.P :","
        BB.DATA := BAL29.C :","
        BB.DATA := BAL29.P :","
        BB.DATA := BAL42.P + BAL43.P:","
        BB.DATA := TOT.13.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 DAYS*****************
************************2 DAYS*******************
        TOT.2.D  = XX2.D + XX2.LOAN2.DD +XX2.LOAN2.DD62 + XX2.OVER.33.D + XX2.OVER.33.D.P.C + XX3.LOAN1.CHQ

        BB.DATA  = "2DAYS":","
        BB.DATA := XX2.D:","
        BB.DATA := BAL2.C  + BAL15.C:","
        BB.DATA := BAL2.P  + BAL15.P:","
        BB.DATA := BAL30.C:","
        BB.DATA := BAL30.P:","
        BB.DATA := BAL44.P:","
        BB.DATA := TOT.2.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**********************2 DAYS************************
*********************3 DAYS********************
        TOT.3.D = XX3.D + XX3.LOAN2.DD + XX3.LOAN2.DD.P.D + XX3.OVER.33.D + XX3.OVER.33.D.P.C + XX4.LOAN1.CHQ
        BB.DATA  = "3DAYS":","
        BB.DATA := XX3.D:","
        BB.DATA := BAL3.C  + BAL16.C:","
        BB.DATA := BAL3.P  + BAL16.P:","
        BB.DATA := BAL31.C:","
        BB.DATA := BAL31.P:","
        BB.DATA := BAL45.P:","
        BB.DATA := TOT.3.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************3 DAYS*******************
***************************4 DAYS*************
        TOT.4.D = XX4.D + XX4.LOAN2.DD +XX4.LOAN2.DD.P.D + XX4.OVER.33.D + XX4.OVER.33.D.P.C + XX5.LOAN1.CHQ
        BB.DATA  = "4DAYS":","
        BB.DATA := XX4.D:","
        BB.DATA :=  BAL4.C  + BAL17.C:","
        BB.DATA :=  BAL4.P  + BAL17.P:","
        BB.DATA :=  BAL32.C:","
        BB.DATA :=  BAL32.P:","
        BB.DATA :=  BAL46.P:","
        BB.DATA :=  TOT.4.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 DAYS********************
************************* 5 DAYS*************
        TOT.5.D = XX5.D + XX5.LOAN2.DD +XX5.LOAN2.DD.P.D + XX5.OVER.33.D + XX5.OVER.33.D.P.C + XX6.LOAN1.CHQ
        BB.DATA  = "5DAYS":","
        BB.DATA := XX5.D:","
        BB.DATA :=  BAL5.C  + BAL18.C:","
        BB.DATA :=  BAL5.P  + BAL18.P:","
        BB.DATA :=  BAL33.C:","
        BB.DATA :=  BAL33.P:","
        BB.DATA :=  BAL47.P:","
        BB.DATA :=  TOT.5.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************5 DAYS******************
************************2 WEEKS*******************8
        TOT.6.D = XX6.D + XX6.LOAN2.DD +XX6.LOAN2.DD.P.D + XX6.OVER.33.D + XX6.OVER.33.D.P.C + XX7.LOAN1.CHQ
        BB.DATA  = "2WEEKS":","
        BB.DATA := XX6.D:","
        BB.DATA :=  BAL6.C  + BAL19.C:","
        BB.DATA :=  BAL6.P  + BAL19.P:","
        BB.DATA :=  BAL34.C:","
        BB.DATA :=  BAL34.P:","
        BB.DATA :=  BAL48.P:","
        BB.DATA :=  TOT.6.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************2 WEEKS***************
*************************3 WEEKS********************
        TOT.7.D = XX7.D + XX7.LOAN2.DD +XX7.LOAN2.DD.P.D + XX7.OVER.33.D +  XX7.OVER.33.D.P.C + XX8.LOAN1.CHQ
        BB.DATA  = "3WEEKS":","
        BB.DATA :=  XX7.D:","
        BB.DATA :=  BAL7.C  + BAL20.C:","
        BB.DATA :=  BAL7.P  + BAL20.P:","
        BB.DATA :=  BAL35.C:","
        BB.DATA :=  BAL35.P:","
        BB.DATA :=  BAL49.P:","
        BB.DATA :=  TOT.7.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************3 WEEKS*******************
*************************4 WEEKS********************
        TOT.8.D = XX8.D + XX8.LOAN2.DD +XX8.LOAN2.DD.P.D + XX8.OVER.33.D + XX8.OVER.33.D.P.C + XX9.LOAN1.CHQ
        BB.DATA  = "4WEEKS":","
        BB.DATA :=  XX8.D:","
        BB.DATA :=  BAL8.C  + BAL21.C:","
        BB.DATA :=  BAL8.P  + BAL21.P:","
        BB.DATA :=  BAL36.C:","
        BB.DATA :=  BAL36.P:","
        BB.DATA :=  BAL50.P:","
        BB.DATA :=  TOT.8.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 WEEKS*******************
*************************5 WEEKS********************
        TOT.9.D = XX9.D + XX9.LOAN2.DD +XX9.LOAN2.DD.P.D + XX9.OVER.33.D + XX9.OVER.33.D.P.C + XX10.LOAN1.CHQ
        BB.DATA  = "5WEEKS":","
        BB.DATA := XX9.D:","
        BB.DATA :=  BAL9.C  + BAL22.C:","
        BB.DATA :=  BAL9.P  + BAL22.P:","
        BB.DATA := BAL37.C:","
        BB.DATA := BAL37.P:","
        BB.DATA := BAL51.P:","
        BB.DATA := TOT.9.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************5 WEEKS*******************
*************************6 WEEKS********************
        TOT.10.D = XX10.D + XX10.LOAN2.DD + XX10.LOAN2.DD.P.D + XX10.OVER.33.D + XX10.OVER.33.D.P.C + XX11.LOAN1.CHQ
        BB.DATA  = "6WEEKS":","
        BB.DATA :=  XX10.D:","
        BB.DATA :=  BAL10.C  + BAL23.C:","
        BB.DATA :=  BAL10.P  + BAL23.P:","
        BB.DATA :=  BAL38.C:","
        BB.DATA :=  BAL38.P:","
        BB.DATA :=  BAL52.P:","
        BB.DATA :=  TOT.10.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************6 WEEKS*******************
***************7 WEEKS********************
        TOT.11.D = XX11.D + XX11.LOAN2.DD + XX11.LOAN2.DD.P.D +  XX11.OVER.33.D + XX11.OVER.33.D.P.C + XX12.LOAN1.CHQ
        BB.DATA  =  "7WEEKS":","
        BB.DATA := XX11.D:","
        BB.DATA := BAL11.C  + BAL24.C:","
        BB.DATA := BAL11.P  + BAL24.P:","
        BB.DATA := BAL39.C:","
        BB.DATA := BAL39.P:","
        BB.DATA := BAL53.P:","
        BB.DATA := TOT.11.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************7 WEEKS*******************
        TOT.12.D = XX12.D + XX12.LOAN2.DD +XX12.LOAN2.DD.P.D +  XX12.OVER.33.D + XX12.OVER.33.D.P.C + XX13.LOAN1.CHQ
        BB.DATA  = "8WEEKS":","
        BB.DATA := XX12.D:","
        BB.DATA := BAL12.C  + BAL25.C:","
        BB.DATA := BAL12.P  + BAL25.P:","
        BB.DATA := BAL40.C:","
        BB.DATA := BAL40.P:","
        BB.DATA := BAL54.P:","
        BB.DATA := TOT.12.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************8 WEEKS*******************
*************************OVER 8 WEEKS********************
        TOT.12.D.OV = XX12.LOAN2.DD.OV + XX12.LOAN2.DD.OV.P.D + XX12.OVER.33.D.OV + XX12.OVER.33.D.OV.P.C + XX14.LOAN1.CHQ
        BB.DATA  = "OVER 8WEEKS":","
        BB.DATA := "":","
        BB.DATA := BAL13.C  + BAL26.C:","
        BB.DATA := BAL13.P  + BAL26.P:","
        BB.DATA := BAL41.C:","
        BB.DATA := BAL41.P:","
        BB.DATA := BAL55.P:","
        BB.DATA := TOT.12.D.OV

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

********************************CUMMULATIVE**********
        BB.DATA  = "CASH INFLOW CUMMULATIVE ":","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        BB.DATA := "INTERBANK.LOANS":","
        BB.DATA := " LOANS":","
        BB.DATA := "MATURING SECURITIES":","
        BB.DATA := "CUMMULATIVE INFLOW":","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*********************************************
        TOT.ON = XX1.16 + XX1.LOAN2 + XX1.LOAN1  + XX1.LOAN2.P
* BB.DATA  = XX1:","
* BB.DATA := XX1.LOAN2:","
* BB.DATA := XX1.LOAN1:","
* BB.DATA := TOT.ON:","

* WRITESEQ BB.DATA TO BB ELSE
*     PRINT " ERROR WRITE FILE "
* END
*************************ON*******************
**************************1 DAYS ************
        TOT.1DAYS = XX14 + XX13.LOAN2 +XX13.LOAN2.P  + XX13.LOAN1.EZN+XX13.LOAN1 + TOT.ON
        BB.DATA  = "1DAY":","
        BB.DATA := XX14 + XX1.16:","
        BB.DATA := XX13.LOAN2 + XX1.LOAN2:","
* BB.DATA := XX13.LOAN2.P + XX1.LOAN2.P:","
        BB.DATA := XX13.LOAN1.EZN + XX13.LOAN1 + XX1.LOAN1:","
        BB.DATA := TOT.1DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 DAYS*****************
************************2 DAYS*******************
        TOT.2DAYS = XX3 + XX2.LOAN2 +XX2.LOAN2.P + XX2.LOAN1 + TOT.1DAYS
        BB.DATA  = "2DAYS":","
        BB.DATA := XX3:","
        BB.DATA := XX2.LOAN1:","
        BB.DATA := XX2.LOAN2:","
*  BB.DATA := XX2.LOAN2.P:","
        BB.DATA := TOT.2DAYS :","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**********************2 DAYS************************
*********************3 DAYS********************
        TOT.3DAYS = XX4 + XX3.LOAN2 +XX3.LOAN2.P + XX3.LOAN1 + TOT.2DAYS
        BB.DATA  = "3DAYS":","
        BB.DATA := XX4:","
        BB.DATA := XX3.LOAN2:","
*  BB.DATA := XX3.LOAN2.P:","
        BB.DATA := XX3.LOAN1:","
        BB.DATA := TOT.3DAYS:","

*TEXT = "BR.DATA : " : BB.DATA ; CALL REM
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************3 DAYS*******************
***************************4 DAYS*************
        TOT.4DAYS = XX5 + XX4.LOAN2 +XX4.LOAN2.P+ XX4.LOAN1 + TOT.3DAYS
        BB.DATA  = "4DAYS":","
        BB.DATA := XX5:","
        BB.DATA :=  XX4.LOAN2:","
* BB.DATA :=XX4.LOAN2.P:","
        BB.DATA :=  XX4.LOAN1:","
        BB.DATA :=  TOT.4DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 DAYS********************
*************************5 DAYS*************
        TOT.5DAYS = XX6 + XX5.LOAN2 +XX5.LOAN2.P + XX5.LOAN1 + TOT.4DAYS
        BB.DATA  = "5DAYS":","
        BB.DATA := XX6:","
        BB.DATA :=  XX5.LOAN2:","
* BB.DATA := XX5.LOAN2.P:","
        BB.DATA :=  XX5.LOAN1:","
        BB.DATA :=  TOT.5DAYS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
************************5 DAYS******************
************************2 WEEKS*******************8
        TOT.2WEEKS = XX7 + XX6.LOAN2 +XX6.LOAN2.P + XX6.LOAN1 + TOT.5DAYS
        BB.DATA  = "2WEEKS":","
        BB.DATA := XX7:","
        BB.DATA :=  XX6.LOAN2:","
        BB.DATA :=  XX6.LOAN2.P:","
        BB.DATA :=  XX6.LOAN1:","
        BB.DATA := TOT.2WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************2 WEEKS***************
**************************3 WEEKS***************
        TOT.3WEEKS = XX8 + XX7.LOAN2 +XX7.LOAN2.P+ XX7.LOAN1 + TOT.2WEEKS
        BB.DATA  = "3WEEKS":","
        BB.DATA := XX8:","
        BB.DATA :=  XX7.LOAN2:","
        BB.DATA :=  XX7.LOAN2.P:","
        BB.DATA :=  XX7.LOAN1:","
        BB.DATA := TOT.3WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************3 WEEKS***************
*************************4 WEEKS********************
        TOT.4WEEKS = XX9 + XX8.LOAN2 +XX8.LOAN2.P+ XX8.LOAN1 + TOT.3WEEKS
        BB.DATA  = "4WEEKS":","
        BB.DATA := XX9:","
        BB.DATA :=  XX8.LOAN2:","
        BB.DATA :=  XX8.LOAN2.P:","
        BB.DATA :=  XX8.LOAN1:","
        BB.DATA := TOT.4WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 WEEKS*******************
*************************5 WEEKS********************
        TOT.5WEEKS = XX10 + XX9.LOAN2 +XX9.LOAN2.P+ XX9.LOAN1 + TOT.4WEEKS
        BB.DATA  = "5WEEKS":","
        BB.DATA := XX10:","
        BB.DATA :=  XX9.LOAN2:","
        BB.DATA :=  XX9.LOAN2.P:","
        BB.DATA :=  XX9.LOAN1:","
        BB.DATA := TOT.5WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************5 WEEKS*******************
*************************6 WEEKS********************
        TOT.6WEEKS = XX11 + XX10.LOAN2 +XX10.LOAN2.P + XX10.LOAN1 + TOT.5WEEKS
        BB.DATA  = "6WEEKS":","
        BB.DATA := XX11:","
        BB.DATA :=  XX10.LOAN2:","
        BB.DATA :=  XX10.LOAN2.P:","
        BB.DATA :=  XX10.LOAN1:","
        BB.DATA :=  TOT.6WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************6 WEEKS*******************
*************************7 WEEKS********************
        TOT.7WEEKS = XX12 + XX11.LOAN2 +XX11.LOAN2.P+ XX11.LOAN1 + TOT.6WEEKS
        BB.DATA  = "7WEEKS":","
        BB.DATA := XX12:","
        BB.DATA :=  XX11.LOAN2:","
        BB.DATA :=  XX11.LOAN2.P:","
        BB.DATA :=  XX11.LOAN1:","
        BB.DATA :=  TOT.7WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************7 WEEKS*******************
*******************8 WEEKS*******************
        TOT.8WEEKS = XX13 + XX12.LOAN2 +XX12.LOAN2.P + XX12.LOAN1 + TOT.7WEEKS
        BB.DATA = "8WEEKS":","
        BB.DATA := XX13:","
        BB.DATA :=  XX12.LOAN2:","
        BB.DATA :=  XX12.LOAN2.P:","
        BB.DATA :=  XX12.LOAN1:","
        BB.DATA :=  TOT.8WEEKS:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************8 WEEKS*******************

******************OVER 8 WEEKS*******************
        TOT.8WEEKS.OV = AC.BAL20.MM + XX12.LOAN2.OV + XX12.LOAN2.P.OV + TOT.8WEEKS
        BB.DATA = "OVER 8WEEKS":","
        BB.DATA := AC.BAL20.MM : ","
        BB.DATA := XX12.LOAN2.OV : ","
        BB.DATA := XX12.LOAN2.P.OV : ","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
********************OVER 8WEEKS


**********************WRITE OUTFLOW********************
        BB.DATA  = "CASH OUTFLOW CUMMULATIVE":","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        BB.DATA := "INTERBANK DEPOSITS":","
        BB.DATA := "CORPRATE DEPOSITS":","
        BB.DATA := "PERSONAL DEPOSITS":","
        BB.DATA := "CORPRATE CURRENT AC":","
        BB.DATA := "PERSONAL CURRENT AC":","
        BB.DATA := "CUMMULATIVE OUTFLOW":","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END


*****************************ON***********
        TOT.ON.D = XX1.D + XX1.LOAN2.DD +XX1.LOAN2.DD62 + XX1.OVER.33.D + XX1.OVER.44
* BB.DATA  = XX1.D:","
* BB.DATA := XX1.LOAN2.DD :","
* BB.DATA := XX1.OVER.33.D:","
* BB.DATA := XX1.OVER.44:","
* BB.DATA := TOT.ON.D:","

*WRITESEQ BB.DATA TO BB ELSE
*    PRINT " ERROR WRITE FILE "
*END
*************************ON*******************
**************************1 DAYS ************
        TOT.13.D = TOT.ON.D +  XX13.D + XX13.LOAN2.DD + XX13.LOAN2.DD62+ XX13.OVER.33.D + XX13.OVER.33.D.P.C
        BB.DATA = "1 DAYS":","
        BB.DATA := XX13.D + XX1.D:","
        BB.DATA := XX13.LOAN2.DD + XX1.LOAN2.DD:","
        BB.DATA := XX13.LOAN2.DD62 + XX1.LOAN2.DD62:","
        BB.DATA := XX13.OVER.33.D + XX1.OVER.33.D:","
        BB.DATA := XX13.OVER.33.D.P.C + XX1.OVER.33.D.P.C:","
        BB.DATA := TOT.13.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 DAYS*****************
************************2 DAYS*******************
        TOT.2.D = XX2.D + XX2.LOAN2.DD +XX2.LOAN2.DD62+ XX2.OVER.33.D + XX2.OVER.44 + TOT.13.D
        BB.DATA = "2 DAYS":","
        BB.DATA := XX2.D:","
        BB.DATA := XX2.LOAN2.DD:","
        BB.DATA := XX2.LOAN2.DD62:","
        BB.DATA := XX2.OVER.33.D:","
        BB.DATA := XX2.OVER.33.D.P.C:","
        BB.DATA := TOT.2.D

        WRITESEQ BB.DATA TO BB ELSE

            PRINT " ERROR WRITE FILE "
        END
**********************2 DAYS************************
*********************3 DAYS********************
        TOT.3.D = XX3.D + XX3.LOAN2.DD +XX3.LOAN2.DD62+ XX3.OVER.33.D + XX3.OVER.44 + TOT.2.D
        BB.DATA = "3 DAYS":","
        BB.DATA := XX3.D:","
        BB.DATA := XX3.LOAN2.DD:","
        BB.DATA := XX3.LOAN2.DD62:","
        BB.DATA := XX3.OVER.33.D:","
        BB.DATA := XX3.OVER.44:","
        BB.DATA := TOT.3.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************3 DAYS*******************
***************************4 DAYS*************
        TOT.4.D = XX4.D + XX4.LOAN2.DD + XX4.LOAN2.DD62+ XX4.OVER.33.D + XX4.OVER.44 + TOT.3.D
        BB.DATA = "4 DAYS":","
        BB.DATA := XX4.D:","
        BB.DATA :=  XX4.LOAN2.DD:","
        BB.DATA :=  XX4.LOAN2.DD62:","
        BB.DATA :=  XX4.OVER.33.D:","
        BB.DATA :=  XX4.OVER.44:","
        BB.DATA :=  TOT.4.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 DAYS********************
************************* 5 DAYS*************
        TOT.5.D = XX5.D + XX5.LOAN2.DD +XX5.LOAN2.DD62 + XX5.OVER.33.D + XX5.OVER.44 + TOT.4.D
        BB.DATA = "5 DAYS":","
        BB.DATA := XX5.D:","
        BB.DATA :=  XX5.LOAN2.DD:","
        BB.DATA :=  XX5.LOAN2.DD62:","
        BB.DATA :=  XX5.OVER.33.D:","
        BB.DATA :=  XX5.OVER.44:","
        BB.DATA :=  TOT.5.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************5 DAYS******************
************************2 WEEKS*******************8
        TOT.6.D  =  XX6.D + XX6.LOAN2.DD +XX6.LOAN2.DD62+ XX6.OVER.33.D + XX6.OVER.44 + TOT.5.D
        BB.DATA = "2WEEKS":","
        BB.DATA :=  XX6.D:","
        BB.DATA :=  XX6.LOAN2.DD:","
        BB.DATA :=  XX6.LOAN2.DD62:","
        BB.DATA :=  XX6.OVER.33.D:","
        BB.DATA :=  XX6.OVER.44:","
        BB.DATA :=  TOT.6.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************2 WEEKS***************
*************************3 WEEKS********************
        TOT.7.D  =  XX7.D + XX7.LOAN2.DD +XX7.LOAN2.DD62+ XX7.OVER.33.D + XX7.OVER.44 + TOT.6.D
        BB.DATA = "3WEEKS":","
        BB.DATA :=  XX7.D:","
        BB.DATA :=  XX7.LOAN2.DD:","
        BB.DATA :=  XX7.LOAN2.DD62:","
        BB.DATA :=  XX7.OVER.33.D:","
        BB.DATA :=  XX7.OVER.44:","
        BB.DATA :=  TOT.7.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************3 WEEKS*******************
*************************4 WEEKS********************
        TOT.8.D  =  XX8.D + XX8.LOAN2.DD +XX8.LOAN2.DD62+ XX8.OVER.33.D + XX8.OVER.44 + TOT.7.D
        BB.DATA = "4WEEKS":","
        BB.DATA :=  XX8.D:","
        BB.DATA :=  XX8.LOAN2.DD:","
        BB.DATA :=  XX8.LOAN2.DD62:","
        BB.DATA :=  XX8.OVER.33.D:","
        BB.DATA :=  XX8.OVER.44:","
        BB.DATA :=  TOT.8.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 WEEKS*******************
*************************5 WEEKS********************
        TOT.9.D = XX9.D + XX9.LOAN2.DD +XX9.LOAN2.DD62+ XX9.OVER.33.D + XX9.OVER.44 + TOT.8.D
        BB.DATA  =  "5WEEKS":","
        BB.DATA :=  XX9.D:","
        BB.DATA :=  XX9.LOAN2.DD:","
        BB.DATA :=  XX9.LOAN2.DD62:","
        BB.DATA :=  XX9.OVER.33.D:","
        BB.DATA :=  XX9.OVER.44:","
        BB.DATA :=  TOT.9.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************5 WEEKS*******************
*************************6 WEEKS********************
        TOT.10.D = XX10.D + XX10.LOAN2.DD +XX10.LOAN2.DD62+ XX10.OVER.33.D + XX10.OVER.44 + TOT.9.D
        BB.DATA = "6WEEKS":","
        BB.DATA :=  XX10.D:","
        BB.DATA :=  XX10.LOAN2.DD:","
        BB.DATA :=  XX10.LOAN2.DD62:","
        BB.DATA :=  XX10.OVER.33.D:","
        BB.DATA :=  XX10.OVER.44:","
        BB.DATA :=  TOT.10.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************6 WEEKS*******************
***************7 WEEKS********************
        TOT.11.D = XX11.D + XX11.LOAN2.DD +XX11.LOAN2.DD62 + XX11.OVER.33.D + XX11.OVER.44 + TOT.10.D
        BB.DATA = "7WEEKS":","
        BB.DATA := XX11.D:","
        BB.DATA :=  XX11.LOAN2.DD:","
        BB.DATA :=  XX11.LOAN2.DD62:","
        BB.DATA :=  XX11.OVER.33.D:","
        BB.DATA :=  XX11.OVER.44:","
        BB.DATA :=  TOT.11.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************7 WEEKS*******************
*************************8 WEEKS********************
        TOT.12.D = XX12.D + XX12.LOAN2.DD +XX12.LOAN2.DD62+ XX12.OVER.33.D + XX12.OVER.44 + TOT.11.D

        BB.DATA  = "8WEEKS":","
        BB.DATA := XX12.D:","
        BB.DATA :=  XX12.LOAN2.DD:","
        BB.DATA :=  XX12.LOAN2.DD62:","
        BB.DATA :=  XX12.OVER.33.D:","
        BB.DATA :=  XX12.OVER.44:","
        BB.DATA :=  TOT.12.D

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************8 WEEKS*******************
*************************OVER 8 WEEKS********************
        TOT.12.D.OV = XX12.LOAN2.DD.OV + XX12.LOAN2.DD62.OV + TOT.12.D
        BB.DATA  = "OVER 8WEEKS":","
        BB.DATA := "":","
        BB.DATA := XX12.LOAN2.DD.OV:","
        BB.DATA := XX12.LOAN2.DD62.OV:","
        BB.DATA := "":","
        BB.DATA := "":","
        BB.DATA := TOT.12.D.OV

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*********************************END CUMMILATIVE**************************
        BB.DATA  = "CASH OUTFLOW NET":","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

        NET1  = TOT.1DAYS-TOT.13.D
        NET2  = TOT.2DAYS-TOT.2.D
        NET3  = TOT.3DAYS-TOT.3.D
        NET4  = TOT.4DAYS-TOT.4.D
        NET5  = TOT.5DAYS-TOT.5.D
        NET6  = TOT.2WEEKS-TOT.6.D
        NET7  = TOT.3WEEKS-TOT.7.D
        NET8  = TOT.4WEEKS-TOT.8.D
        NET9  = TOT.5WEEKS-TOT.9.D
        NET10 = TOT.6WEEKS-TOT.10.D
        NET11 = TOT.7WEEKS-TOT.11.D
        NET12 = TOT.8WEEKS-TOT.12.D
        NET13 = TOT.8WEEKS.OV-TOT.12.D.OV
*****************************ON***********
*  BB.DATA := XX1.OVER.44:","
* WRITESEQ BB.DATA TO BB ELSE
*    PRINT " ERROR WRITE FILE "
* END
*************************ON*******************
**************************1 DAYS ************
        BB.DATA  = " 1DAY ": ","
        BB.DATA  := NET1:","
        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************1 DAYS*****************
************************2 DAYS*******************
        BB.DATA  = " 2DAYS ": ","
        BB.DATA := NET2:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**********************2 DAYS************************
*********************3 DAYS********************
        BB.DATA  = " 3DAYS ": ","
        BB.DATA  := NET3:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
******************3 DAYS*******************
***************************4 DAYS*************
        BB.DATA  = " 4DAYS ": ","
        BB.DATA  := NET4:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 DAYS********************
************************* 5 DAYS*************
        BB.DATA  = " 5DAYS ": ","
        BB.DATA  := NET5:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*************************5 DAYS******************
************************2 WEEKS*******************8
        BB.DATA  = " 2WEEKS ": ","
        BB.DATA  := NET6:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
**************************2 WEEKS***************
*************************3 WEEKS********************
        BB.DATA  = " 3WEEKS ": ","
        BB.DATA  :=  NET7:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************3 WEEKS*******************
*************************4 WEEKS********************
        BB.DATA  = " 4WEEKS ": ","
        BB.DATA  :=  NET8:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************4 WEEKS*******************
*************************5 WEEKS********************
        BB.DATA  = " 5WEEKS ": ","
        BB.DATA  := NET9:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************5 WEEKS*******************
*************************6 WEEKS********************
        BB.DATA  = " 6WEEKS ": ","
        BB.DATA  := NET10:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************6 WEEKS*******************
***************7 WEEKS********************
        BB.DATA  = " 7WEEKS ":","
        BB.DATA  := NET11:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************7 WEEKS*******************
*************************8 WEEKS********************
        BB.DATA  = " 8WEEKS ": ","
        BB.DATA  := NET12:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************8 WEEKS*******************
*************************OVER 8 WEEKS********************
        BB.DATA  = "OVER 8 ": ","
        BB.DATA  := NET13:","

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
*******************OVER 8 WEEKS*******************
    NEXT KKKK
*===============================================================
RETURN
