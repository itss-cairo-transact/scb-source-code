* @ValidationCode : Mjo4MzMxOTM5MDE6Q3AxMjUyOjE2NDA4MTcwMTQzNjE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:30:14
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
**    PROGRAM SBD.CATEG.TODAY.FILL
SUBROUTINE SBD.CATEG.TODAY.FILL

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CATEG.TODAY
*----------------------------------------------------------------
* Modification History:
*
* Ref: SCBUPG20160728 : Date: 28-July-2016
* To fix fatal errors after R15 upgrade, below changes are made:
* (1) Used faster jBC CLEARFILE statement to clear F.SCB.CATEG.TODAY
* (2) Used batch mode of calling JOURNAL.UPDATE (JU) instead of calling JOURNAL.UPDATE
*     for each F.WRITE. One call to JU for every 500 writes.
*
*
*
*********************** OPENING FILES *****************************
    FN.CATG   = "FBNK.CATEG.ENTRY"          ; F.CATG     = "" ; R.CATG   = ""
    FN.CATGTD = "FBNK.CATEG.ENT.LWORK.DAY"  ; F.CATGTD   = "" ; R.CATGTD = ""
    FN.CAT    = "F.SCB.CATEG.TODAY"         ; F.CAT      = "" ; R.CAT    = ""


    CALL OPF (FN.CATG,F.CATG)
    CALL OPF (FN.CATGTD,F.CATGTD)
    CALL OPF (FN.CAT,F.CAT)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-----------------------------------------------------------------
********** DELETE RECORDS BEFORE **********
* SCBUPG20160728 - S
    CRT "Clearing ":FN.CAT
    CLEARFILE F.CAT
    CRT "Clearing ":FN.CAT:" Complete"
*    T.SEL2 = "SELECT ":FN.CAT
*    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
*    IF SELECTED2 THEN
*        FOR D = 1 TO SELECTED2
*            CALL F.READ(FN.CAT,KEY.LIST2<D>,R.CAT,F.CAT,E5)
**DELETE F.CAT , KEY.LIST2<D>
*            CALL F.DELETE (FN.CAT,KEY.LIST2<D>)
*
*        NEXT D
*    END
* SCBUPG20160728 - E
*-----------------------------------------------------------------
********* WRITE IN TEMP ************

    T.SEL = "SELECT ":FN.CATGTD
* SCBUPG20160728 - S/E
    WRITE.CNTR = 0
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CATGTD,KEY.LIST<I>,R.CATGTD,F.CATGTD,E1)

            LOOP
                REMOVE CATEG.ID FROM R.CATGTD SETTING POS
            WHILE CATEG.ID:POS
                CALL F.READ(FN.CATG,CATEG.ID,R.CATG,F.CATG,E2)
                CALL F.READ(FN.CAT,CATEG.ID,R.CAT,F.CAT,E3)
********************** COPY NEW DATA ******************************

                R.CAT<CATD.COMPANY.CODE>     = R.CATG<AC.CAT.COMPANY.CODE>
                R.CAT<CATD.AMOUNT.LCY>       = R.CATG<AC.CAT.AMOUNT.LCY>
                R.CAT<CATD.PL.CATEGORY>      = R.CATG<AC.CAT.PL.CATEGORY>
                R.CAT<CATD.CUSTOMER.ID>      = R.CATG<AC.CAT.CUSTOMER.ID>
                R.CAT<CATD.PRODUCT.CATEGORY> = R.CATG<AC.CAT.PRODUCT.CATEGORY>
                R.CAT<CATD.CURRENCY>         = R.CATG<AC.CAT.CURRENCY>
                R.CAT<CATD.OUR.REFERENCE>    = R.CATG<AC.CAT.OUR.REFERENCE>

* SCBUPG20160728 - S
                CALL F.WRITE(FN.CAT,CATEG.ID,R.CAT)
                WRITE.CNTR += 1
* Call JU for every 500 F.WRITE's.
                IF MOD(WRITE.CNTR,500) = 0 THEN
                    CRT "Flusing to disk after writing 500 records..."
                    CALL JOURNAL.UPDATE(CATEG.ID)
                    WRITE.CNTR = 0
                END
            REPEAT
        NEXT I
    END
* Any residual WRITE entries will be flushed.
    IF WRITE.CNTR THEN
        CALL JOURNAL.UPDATE("SBD.CATEG.TODAY.FILL")
    END
RETURN
* SCBUPG20160728 - E
END
