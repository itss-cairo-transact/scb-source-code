* @ValidationCode : MjoxNDY0MDkyNzkxOkNwMTI1MjoxNjQxNzc5MjU5MzI4OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:47:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*--- CREATE BY NESSMA ON 2011/12/18 ----
SUBROUTINE SBM.AC.TOTALS.CUS.ALL.RNG
*---------------------------------------

*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.USER
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.BILL.REGISTER
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DATES
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DEPT.ACCT.OFFICER
*Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.RE.BASE.CCY.PARAM
*Line [ 42 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.LD.LOANS.AND.DEPOSITS
*Line [ 44 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.LETTER.OF.CREDIT
*Line [ 46 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY
*Line [ 48 ] HASHING $INSERT I_AC.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*  $INSERT           I_AC.LOCAL.REFS
*Line [ 50 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT           I_CU.LOCAL.REFS
*Line [ 52 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT           I_F.SCB.TOPCUS.CR.TOT.LW
    
*---------------------------------------------------------------------
    VAR.TOD = TODAY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS.OPEN
    GOSUB PROCESS.CLOSE
    GOSUB PRINT.ARRAY
    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    PRINT "DONE"
RETURN
*----------------------------- INITIALIZATIONS ------------------------
PRINT.ARRAY:
    FOR ROW.ARR = 1 TO ZZ-1
        PRINT ARRAY.LINE<1,ROW.ARR>
        PRINT " "
    NEXT ROW.ARR
RETURN
*---------------------------------------------------------------------
LINE.END:
    PRINT " "
    TOT.4 = FMT(TOT.4, "L2,")
    TOT.5 = FMT(TOT.5, "L2,")

    LINE.HD = "�������� :"
    LINE.HD := SPACE(5) :  TOT.4 : SPACE(16) : TOT.5   : SPACE(20)
    LINE.HD := TOT.1    : SPACE(8) : TOT.2  : SPACE(8)
    LINE.HD := TOT.3    : SPACE(9)  : TOT.6  : SPACE(5)
    LINE.HD := TOT.7    : SPACE(6)  : TOT.8

    PRINT LINE.HD
*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    WW = SPACE(100)
    PRINT " "
    PRINT STR('_',125)
    PRINT " "
    XX.LINE= "**��� ������� ��� �� �� ����� ����� ��� ������ **"
    XX.LINE = "** ��� ������� �� ����� �������� �������� ��� �� �� ����� ����� ��� ������"
    XX.DD = FMT(VAR.TOD, "####/##/##")
    PRINT XX.LINE :SPACE(2): XX.DD

    PRINT " "
    PRINT " "
    WW<1,1> = SPACE(40):"********************  ����� �������   ******************"
    PRINT WW<1,1>
RETURN
*--------------------------------------------------------------------
INITIATE:
    TOT.1 = 0
    TOT.2 = 0
    TOT.3 = 0
    TOT.4 = 0
    TOT.5 = 0
    TOT.6 = 0
    TOT.7 = 0
    TOT.8 = 0

    ZZ = 1
    HH = 1
    ALL.BAL.NEW     = 0
    ALL.BAL.NEW.FCY = 0
    ARRAY.LINE = SPACE(150)

    REPORT.ID='SBM.AC.TOTALS'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.CBE = "F.SCB.TOPCUS.CR.TOT.LW"     ; F.CBE = ""
    CALL OPF(FN.CBE,F.CBE)

    FN.CUS = 'FBNK.CUSTOMER'              ; F.CUS = ""
    CALL OPF( FN.CUS,F.CUS)

    FN.CUS.H = 'FBNK.CUSTOMER$HIS'         ; F.CUS.H = ""
    CALL OPF(FN.CUS.H,F.CUS.H)

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    T.DATE    = TODAY
    KEY.CCY   = 'NZD'
    DDD       = TODAY[1,6]:'01'
*-----
    START.DATE   = "20120101"
    END.DATE     = "20120731"
    END.DATE1    = TODAY

    COMP.COUNT.2 = 0
    INDV.COUNT.2 = 0
RETURN
*------------------------ READ FORM TEXT FILE --------------------
PROCESS.OPEN:
    T.SEL  = " SELECT FBNK.CUSTOMER WITH CONTACT.DATE GE ":START.DATE
    T.SEL := " AND CONTACT.DATE LE ":END.DATE
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020)"
    T.SEL := " AND TEXT UNLIKE BR... BY COMPANY.BOOK"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    OLD.CO.CODE       = ''
    INDV.COUNT        = 0
    COMP.COUNT        = 0

    FOR I = 1 TO SELECTED-1
        CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
        CALL F.READ(FN.CUS,KEY.LIST<I+1>,R.CUS.1,F.CUS,CUS.ERR)
        COM.CODE    = R.CUS<EB.CUS.COMPANY.BOOK>
        COM.CODE.1  = R.CUS.1<EB.CUS.COMPANY.BOOK>

        IF COM.CODE EQ COM.CODE.1 THEN
            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR    = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>

            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END

        END ELSE

            CALL F.READ(FN.CUS,KEY.LIST<I>,R.CUS,F.CUS,CUS.ERR)
            NEW.SECTOR    = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
            IF NEW.SECTOR NE '' THEN
                IF NEW.SECTOR NE '4650' THEN
                    COMP.COUNT = COMP.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END ELSE
                    INDV.COUNT = INDV.COUNT + 1
                    CUSTO = KEY.LIST<I>
                    GOSUB CALC.BALANCE
                END
            END

            ALL.COUNT    = COMP.COUNT + INDV.COUNT

            OLDD      = COM.CODE[8,2]
            IF OLDD[1,1] EQ '0' THEN
                OLDD = OLDD[1]
            END

*Line [ 206 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,OLDD,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRAN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            BRANCH.NAME  = FIELD(BRAN.NAME,'.',2)
*--------------------------
            CBE.BR = COM.CODE[8,2]
*--------------------------
            GOSUB WRITE.LINE

            COMP.COUNT   = 0
            INDV.COUNT   = 0
        END
    NEXT I
RETURN
*----------------------------------------------------------------------
PROCESS.CLOSE:
*-------------
    T.SEL  = "SELECT FBNK.CUSTOMER WITH (POSTING.RESTRICT GE 90 )"
    T.SEL := " AND (SECTOR NE 5010 AND SECTOR NE 5020) AND CURR.NO GT 1"
    T.SEL := " AND WITHOUT TEXT LIKE BR... BY COMPANY.BOOK"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN

        FOR II = 1 TO SELECTED-1
            CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERR)
            CALL F.READ(FN.CUS,KEY.LIST<II+1>,R.CUS.1,F.CUS,CUS.ERR)
*Line [ 233 ] Update COMPANY.BOOK to CO.CODE - ITSS - R21 Upgrade - 2021-12-26
            COM.CODE    = R.CUS<EB.CUS.COMPANY.BOOK>
*Line [ 235 ] Update COMPANY.BOOK to CO.CODE - ITSS - R21 Upgrade - 2021-12-26
            COM.CODE.1  = R.CUS.1<EB.CUS.COMPANY.BOOK>

            CALL F.READ(FN.CUS,KEY.LIST<II>,R.CUS,F.CUS,CUS.ERRRR)
            CUS.ID  = KEY.LIST<II>
            CUR.NO  = R.CUS<EB.CUS.CURR.NO>
            RR = 1
            MM = CUR.NO - 1

            FOR NN = CUR.NO TO 1 STEP -1
                IF RR EQ 1 THEN
                    CUST.ID      = CUS.ID:";"
                    CUST.ID.NXT  = CUS.ID:";":MM
                END ELSE
                    CUST.ID      = CUS.ID:";":MM
                    MM           = MM -1
                    CUST.ID.NXT  = CUS.ID:";":MM
                END
                IF RR EQ 1 THEN
                    RR = 2
                    CALL F.READ(FN.CUS,CUS.ID,R.CUS.N,F.CUS,CUS.ER44)
                    CUS.DAT.2 = R.CUS.N<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.CUS.N<EB.CUS.POSTING.RESTRICT>
                END ELSE
                    CALL F.READ(FN.CUS.H,CUST.ID,R.CUS.H,F.CUS.H,CUS.ER2)
                    CUS.DAT.2 = R.CUS.H<EB.CUS.DATE.TIME><1,1>[1,6]
                    CUS.POST  = R.CUS.H<EB.CUS.POSTING.RESTRICT>
                END

                CALL F.READ(FN.CUS.H,CUST.ID.NXT,R.CUS.H.NXT,F.CUS.H,CUS.ER3)
                CUS.POST.NXT  = R.CUS.H.NXT<EB.CUS.POSTING.RESTRICT>

                IF CUS.POST NE CUS.POST.NXT THEN
                    IF CUS.POST GE 90 THEN
                        IF CUS.DAT.2 GE START.DATE[3,6] AND CUS.DAT.2 LE END.DATE1[3,6] THEN

                            NEW.SECTOR  = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
                            IF NEW.SECTOR NE '' THEN
                                IF NEW.SECTOR NE '4650' THEN
                                    COMP.COUNT.2 = COMP.COUNT.2 + 1
                                END ELSE
                                    INDV.COUNT.2 = INDV.COUNT.2 + 1
                                END
                            END
                            NN       = 1
                        END
                    END
                END
            NEXT NN
            IF COM.CODE EQ COM.CODE.1 THEN
            END ELSE
                ALL.COUNT.2 = COMP.COUNT.2 + INDV.COUNT.2

                OLDD        = COM.CODE[8,2]
                OLDD        = OLDD + 0

*Line [ 295 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,OLDD,BRAN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,OLDD,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRAN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
                BRANCH.NAME.2  = FIELD(BRAN.NAME,'.',2)
                GOSUB WRITE.LINE.2
            END
        NEXT II
    END
RETURN
*-------------------------------------------------------------------*
WRITE.LINE:
    ARRAY.LINE<1,ZZ>[1,15]   = BRANCH.NAME

    XX = ALL.BAL.NEW
    XX = FMT(XX, "L2,")
    ARRAY.LINE<1,ZZ>[16,20]  = XX

    YY = ALL.BAL.NEW.FCY
    YY = FMT(YY, "L2,")
    ARRAY.LINE<1,ZZ>[45,20]  = YY
*----
    ARRAY.LINE<1,ZZ>[80,10]  = INDV.COUNT
    ARRAY.LINE<1,ZZ>[92,10]  = COMP.COUNT
    ARRAY.LINE<1,ZZ>[102,10] = ALL.COUNT
*----
    ZZ = ZZ + 1
*%%%%%%% TOTALS %%%%%%%%%%%%%%%
    TOT.1 = TOT.1 + INDV.COUNT
    TOT.2 = TOT.2 + COMP.COUNT
    TOT.3 = TOT.3 + ALL.COUNT
    TOT.4 = TOT.4 + ALL.BAL.NEW
    TOT.5 = TOT.5 + ALL.BAL.NEW.FCY
*%%%%%%% TOTALS %%%%%%%%%%%%%%%
    ALL.BAL.NEW     = 0
    ALL.BAL.NEW.FCY = 0
RETURN
*------------------------------------------------------------------*
CALC.BALANCE:
    CUST.NUM = CUSTO
*-------------------
    CUSTO    = CUST.NUM : ".LCY"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000
    ALL.BAL.NEW = ALL.BAL.NEW +  CBE.IN.LCY
*----------
    CUSTO  = CUST.NUM : ".FCY"
    CALL F.READ(FN.CBE,CUSTO,R.CBE,F.CBE,ER.CBE)
    CBE.IN.LCY.FCY  = R.CBE<SCB.TOP.TOTAL.CUS> * 1000

    ALL.BAL.NEW.FCY = ALL.BAL.NEW.FCY +  CBE.IN.LCY.FCY
RETURN
*----------------------------------------------------
WRITE.LINE.2:

    ARRAY.LINE<1,HH>[115,10]  = INDV.COUNT.2
    ARRAY.LINE<1,HH>[125,10]  = COMP.COUNT.2
    ARRAY.LINE<1,HH>[135,10]  = ALL.COUNT.2
    HH = HH + 1

*%%%%%%%%%%% TOTALS %%%%%%%%%%%%
    TOT.6 = TOT.6 + INDV.COUNT.2
    TOT.7 = TOT.7 + COMP.COUNT.2
    TOT.8 = TOT.8 + ALL.COUNT.2
*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    COMP.COUNT.2   = 0
    INDV.COUNT.2   = 0
    ALL.COUNT.2    = 0
    BRANCH.NAME.2  = ""

RETURN
************************ PRINT HEAD *******************************
PRINT.HEAD:
*Line [ 372 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = VAR.TOD
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"����� �������"
    PR.HD :=T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(109):"SBM.AC.TOTALS.CUS.ALL.RNG"
    PR.HD :="'L'":" "

    FROM.DATE.2  = "20120101"
    END.DATE.2   = "20120731"
    FROM.DATE.2  = FMT(FROM.DATE.2, "####/##/##")
    END.DATE.2   = FMT(END.DATE.2 , "####/##/##")

    PR.HD :="'L'":SPACE(40): "�������� �������� �������� ���� ����"
    PR.HD :="'L'":SPACE(40):" ��":SPACE(5):FROM.DATE.2 : "���" : SPACE(3):END.DATE.2

    PR.HD :="'L'" :SPACE(30):STR('_',60)
    PR.HD :="'L'" :" "
    PR.HD :="'L'":SPACE(80)
    PR.HD :="������� ��������"
    PR.HD :=SPACE(20)
    PR.HD :="������� �������"

    PR.HD :="'L'":"�����"
    PR.HD :=SPACE(8): "** ������� ������� ������"
    PR.HD :=SPACE(4) : "** ����� �������� �����"

    PR.HD :=SPACE(10): "�����"
    PR.HD :=SPACE(5): "�����"
    PR.HD :=SPACE(5):"��������"

    PR.HD :=SPACE(10): "�����"
    PR.HD :=SPACE(5): "�����"
    PR.HD :=SPACE(3):"��������"

    PR.HD :="'L'":STR('_',130)
    PRINT
    HEADING PR.HD
RETURN
END
