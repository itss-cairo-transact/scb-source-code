* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.1620.2.BR
*    PROGRAM  SBM.C.PRT.1620.2.BR
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.MAST.AC.LD
*    $INSERT GLOBUS.BP  I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*------------------------------------
*����� ������� ���

* 1620.2
*------------------------------------
    FN.LD = "F.CBE.MAST.AC.LD"
    F.LD  = ""


*    FN.BR = "F.DEPT.ACCT.OFFICER"
*    F.BR = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*------------------------------------------------
    CALL OPF (FN.LD,F.LD)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.1.LE = "0"
    WS.COUNT = 0
    WS.1.EQV = "0"
    WS.NO.OF.AC.LE = 0
    WS.NO.OF.AC.EQV = 0
    WS.COL.AMT = 0
    WS.COMN = 0
    WS.COMN1 = 0
    WS.COMN1.TOT = 0
    WS.COMN.TOT = 0
    WS.COMN1.FINAL.TOT  = 0
    WS.COMN.FINAL.TOT = 0
    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.COUNTA = 0
    MSG.AC = ""
    MSG.CY = ""
    WS.CY = ""
    WS.RATE = 0
    WS.MLT.DIVD = 0
    WS.CODE = 0
*----------------------------------------
    WS.HD.T  = "����� ����� ������� ���� � ������ ����� ����� �������"
*-----------------------------------------

    WS.HD.TA = "����� ��� 1620"
*---------------------------------------------------
    WS.HD.T2A = "���� ��� "
    WS.PAGE.NO    = "1"
*----------------------------------------------------
    WS.PRG.1 = "SBM.C.PRT.1620.2.BR"
*------------------------------------------------------
*------------------ REPORT HEADERS --------------------
    HEAD.00   = " ������ �������"
    HEAD.00.A = " ������ ��������"

*------------------------------------------------------
    HEAD.01   = " ���"
*----------------------------------------------------
    HEAD.01.1 = "���� �� ���"
*---------------------------------------------------
    HEAD.01.2 = "���� �� 3 ����"
*---------------------------------------------------
    HEAD.01.3 = "���� �� 6 ����"
*--------------------------------------------------
    HEAD.01.4 = "���� �� ���"
*---------------------------------------------------
    HEAD.01.5 = "�������"
*---------------------------------------------------
    HEAD.02   = "����"
*----------------------------------------------------
    HEAD.02.1 = "��� 3 ����"
*---------------------------------------------------
    HEAD.02.2 = "��� 6 ����"
*---------------------------------------------------
    HEAD.02.3 = "��� ���"
*--------------------------------------------------
*---------------------------------------------------
********    ARRAY1 = ""
******                                 ARRAY
******                                                 1 ������
******
****** 2----   H = HEADER  D = DETAIL   T = TOTAL
******  3 AND 4 ----   RANGE  FROM  TO    FOR INDUSTRY
******  5  TOTAL  OF  FIRST DATA COLUMN   IN LE
******  6  TOTAL  OF  2     DATA COLUMN   IN LE
******* 7  TOTAL  OF  3     DATA COLUMN   IN LE
******* 8  TOTAL  OF  1     DATA COLUMN   IN EQVELENT
********9  TOTAL  OF  2     DATA COLUMN   IN EQVELENT
********10 TOTAL  OF  3     DATA COLUMN   IN EQVELENT
********11 THE ARRAY NO OF  TOTAL OF GROUP TO BE ACUMLATED


    DIM ARRAY1(23,13)

    ARRAY1(1,1) = "������ �����                       "
    ARRAY1(1,2) = "H"

    ARRAY1(2,1) = "����� ���� ��������                "
    ARRAY1(2,2) = "D"
    ARRAY1(2,3) = "0"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = "0"
    ARRAY1(2,7) = "0"
    ARRAY1(2,8) = "0"
    ARRAY1(2,9) = "0"
    ARRAY1(2,10) = "0"
    ARRAY1(2,11) = "0"
    ARRAY1(2,12) = "0"
    ARRAY1(2,13) = "5"

    ARRAY1(3,1) = "����� ���� ������� �����            "
    ARRAY1(3,2) = "D"
    ARRAY1(3,3) = "0"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"
    ARRAY1(3,8) = "0"
    ARRAY1(3,9) = "0"
    ARRAY1(3,10) = "0"
    ARRAY1(3,11) = "0"
    ARRAY1(3,12) = "0"
    ARRAY1(3,13) = "5"

    ARRAY1(4,1) = "����� ������ �����                 "
    ARRAY1(4,2) = "D"
    ARRAY1(4,3) = "0"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"
    ARRAY1(4,8) = "0"
    ARRAY1(4,9) = "0"
    ARRAY1(4,10) = "0"
    ARRAY1(4,11) = "0"
    ARRAY1(4,12) = "0"
    ARRAY1(4,13) = "5"

    ARRAY1(5,1) = "����� ������ �����                 "
    ARRAY1(5,2) = "T"
    ARRAY1(5,3) = "0"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = "0"
    ARRAY1(5,7) = "0"
    ARRAY1(5,8) = "0"
    ARRAY1(5,9) = "0"
    ARRAY1(5,10) = "0"
    ARRAY1(5,11) = "0"
    ARRAY1(5,12) = "0"

    ARRAY1(6,1) = "���� ������� �����                  "
    ARRAY1(6,2) = "H"

    ARRAY1(7,1)  = "����� ������� ����� ���������       "
    ARRAY1(7,2)  = "D"
    ARRAY1(7,3) = "0"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"
    ARRAY1(7,6) = "0"
    ARRAY1(7,7) = "0"
    ARRAY1(7,8) = "0"
    ARRAY1(7,9) = "0"
    ARRAY1(7,10) = "0"
    ARRAY1(7,11) = "0"
    ARRAY1(7,12) = "0"
    ARRAY1(7,13) = "13"

    ARRAY1(8,1) = "����� ������� ������                 "
    ARRAY1(8,2) = "D"
    ARRAY1(8,3) = "0"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = "0"
    ARRAY1(8,7) = "0"
    ARRAY1(8,8) = "0"
    ARRAY1(8,9) = "0"
    ARRAY1(8,10) = "0"
    ARRAY1(8,11) = "0"
    ARRAY1(8,12) = "0"
    ARRAY1(8,13) = "13"

    ARRAY1(9,1) = "����� �������                       "
    ARRAY1(9,2) = "D"
    ARRAY1(9,3) = "0"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = "0"
    ARRAY1(9,7) = "0"
    ARRAY1(9,8) = "0"
    ARRAY1(9,9) = "0"
    ARRAY1(9,10) = "0"
    ARRAY1(9,11) = "0"
    ARRAY1(9,12) = "0"
    ARRAY1(9,13) = "13"

    ARRAY1(10,1) = "�������� ���������                 "
    ARRAY1(10,2) = "D"
    ARRAY1(10,3) = "0"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = "0"
    ARRAY1(10,7) = "0"
    ARRAY1(10,8) = "0"
    ARRAY1(10,9) = "0"
    ARRAY1(10,10) = "0"
    ARRAY1(10,11) = "0"
    ARRAY1(10,12) = "0"
    ARRAY1(10,13) = "13"

    ARRAY1(11,1) = "����� �����                        "
    ARRAY1(11,2) = "D"
    ARRAY1(11,3) = "0"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = "0"
    ARRAY1(11,7) = "0"
    ARRAY1(11,8) = "0"
    ARRAY1(11,9) = "0"
    ARRAY1(11,10) = "0"
    ARRAY1(11,11) = "0"
    ARRAY1(11,12) = "0"
    ARRAY1(11,13) = "13"

    ARRAY1(12,1) = "����� ���� ����� ������� �����     "
    ARRAY1(12,2) = "D"
    ARRAY1(12,3) = "0"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = "0"
    ARRAY1(12,7) = "0"
    ARRAY1(12,8) = "0"
    ARRAY1(12,9) = "0"
    ARRAY1(12,10) = "0"
    ARRAY1(12,11) = "0"
    ARRAY1(12,12) = "0"
    ARRAY1(12,13) = "13"

    ARRAY1(13,1) = "����� ���� ������� �����           "
    ARRAY1(13,2) = "T"
    ARRAY1(13,3) = "0"
    ARRAY1(13,4) = "0"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = "0"
    ARRAY1(13,7) = "0"
    ARRAY1(13,8) = "0"
    ARRAY1(13,9) = "0"
    ARRAY1(13,10) = "0"
    ARRAY1(13,11) = "0"
    ARRAY1(13,12) = "0"


    ARRAY1(14,1) = "���� ������� �����                 "
    ARRAY1(14,2) = "D"
    ARRAY1(14,3) = "0"
    ARRAY1(14,4) = "0"
    ARRAY1(14,5) = "0"
    ARRAY1(14,6) = "0"
    ARRAY1(14,7) = "0"
    ARRAY1(14,8) = "0"
    ARRAY1(14,9) = "0"
    ARRAY1(14,10) = "0"
    ARRAY1(14,11) = "0"
    ARRAY1(14,12) = "0"
    ARRAY1(14,13) = "0"


    ARRAY1(15,1)  = "������ �������                    "
    ARRAY1(15,2)  = "H"

    ARRAY1(16,1)  = "����� �������                     "
    ARRAY1(16,2)  = "D"
    ARRAY1(16,3) = "0"
    ARRAY1(16,4) = "0"
    ARRAY1(16,5) = "0"
    ARRAY1(16,6) = "0"
    ARRAY1(16,7) = "0"
    ARRAY1(16,8) = "0"
    ARRAY1(16,9) = "0"
    ARRAY1(16,10) = "0"
    ARRAY1(16,11) = "0"
    ARRAY1(16,12) = "0"
    ARRAY1(16,13) = "19"

    ARRAY1(17,1)  = "����� ����� �� ���� �����          "
    ARRAY1(17,2)  = "D"
    ARRAY1(17,3) = "0"
    ARRAY1(17,4) = "0"
    ARRAY1(17,5) = "0"
    ARRAY1(17,6) = "0"
    ARRAY1(17,7) = "0"
    ARRAY1(17,8) = "0"
    ARRAY1(17,9) = "0"
    ARRAY1(17,10) = "0"
    ARRAY1(17,11) = "0"
    ARRAY1(17,12) = "0"
    ARRAY1(17,13) = "19"

    ARRAY1(18,1)  = "����� ������ ���� �� ���          "
    ARRAY1(18,2)  = "D"
    ARRAY1(18,3) = "0"
    ARRAY1(18,4) = "0"
    ARRAY1(18,5) = "0"
    ARRAY1(18,6) = "0"
    ARRAY1(18,7) = "0"
    ARRAY1(18,8) = "0"
    ARRAY1(18,9) = "0"
    ARRAY1(18,10) = "0"
    ARRAY1(18,11) = "0"
    ARRAY1(18,12) = "0"
    ARRAY1(18,13) = "19"

    ARRAY1(19,1)  = "����� ������ �������              "
    ARRAY1(19,2)  = "T"
    ARRAY1(19,3) = "0"
    ARRAY1(19,4) = "0"
    ARRAY1(19,5) = "0"
    ARRAY1(19,6) = "0"
    ARRAY1(19,7) = "0"
    ARRAY1(19,8) = "0"
    ARRAY1(19,9) = "0"
    ARRAY1(19,10) = "0"
    ARRAY1(19,11) = "0"
    ARRAY1(19,12) = "0"
*------------------------------------------------------------
    ARRAY1(20,1) =  "����� ������� ���������            "
    ARRAY1(20,2)  = "D"
    ARRAY1(20,3) = "0"
    ARRAY1(20,4) = "0"
    ARRAY1(20,5) = "0"
    ARRAY1(20,6) = "0"
    ARRAY1(20,7) = "0"
    ARRAY1(20,8) = "0"
    ARRAY1(20,9) = "0"
    ARRAY1(20,10) = "0"
    ARRAY1(20,11) = "0"
    ARRAY1(20,12) = "0"
    ARRAY1(20,13) = "0"
*------------------------------------------------------------
    ARRAY1(21,1)  = "������ �������(�����(             "
    ARRAY1(21,2)  = "D"
    ARRAY1(21,3) = "0"
    ARRAY1(21,4) = "0"
    ARRAY1(21,5) = "0"
    ARRAY1(21,6) = "0"
    ARRAY1(21,7) = "0"
    ARRAY1(21,8) = "0"
    ARRAY1(21,9) = "0"
    ARRAY1(21,10) = "0"
    ARRAY1(21,11) = "0"
    ARRAY1(21,12) = "0"
    ARRAY1(21,13) = "0"

    ARRAY1(22,1)  = "������� ��������                  "
    ARRAY1(22,2)  = "D"
    ARRAY1(22,3) = "0"
    ARRAY1(22,4) = "0"
    ARRAY1(22,5) = "0"
    ARRAY1(22,6) = "0"
    ARRAY1(22,7) = "0"
    ARRAY1(22,8) = "0"
    ARRAY1(22,9) = "0"
    ARRAY1(22,10) = "0"
    ARRAY1(22,11) = "0"
    ARRAY1(22,12) = "0"
    ARRAY1(22,13) = "0"


    ARRAY1(23,1)  = "��������                 "
    ARRAY1(23,2)  = "T"
    ARRAY1(23,3) = "0"
    ARRAY1(23,4) = "0"
    ARRAY1(23,5) = "0"
    ARRAY1(23,6) = "0"
    ARRAY1(23,7) = "0"
    ARRAY1(23,8) = "0"
    ARRAY1(23,9) = "0"
    ARRAY1(23,10) = "0"
    ARRAY1(23,11) = "0"
    ARRAY1(23,12) = "0"
********************** ********************************
***** SECOND ARRAY TO DEFINE THE RANGE FOR EVERY SECTOR
********************** ********************************
****����� ������� ��� ����� �� ����� �����
****������� ��� �� 5000 ���� ������� ��� ��� 3 ����� �� ������
****��� �� ������� ����� ���� ���� ����� ����
****����� ������ ���� ������ ���� ���� �������
****������� �� 5000  ���� ������� ��� ����� �������

    DIM ARRAYRNG(27,2)

*    ARRAY() = "����� ���� ��������                "
    ARRAYRNG(1,1) = "110"
    ARRAYRNG(1,2) = "2"

*     ARRAY() = "����� ���� ������� �����            "
    ARRAYRNG(2,1) = "120"
    ARRAYRNG(2,2) = "3"
*
*    ARRAY() = "����� ������ �����                 "
    ARRAYRNG(3,1) = "130"
    ARRAYRNG(3,2) = "4"
*
*    ARRAY()  = "����� ������� ����� ���������       "
    ARRAYRNG(4,1) = "210"
    ARRAYRNG(4,2) = "7"
*
*    ARRAY() = "����� ������� ������                 "
    ARRAYRNG(5,1) = "220"
    ARRAYRNG(5,2) = "8"
*
*    ARRAY() = "����� �������                       "
    ARRAYRNG(6,1) = "230"
    ARRAYRNG(6,2) = "9"
*
*    ARRAY() = "�������� ���������                 "
    ARRAYRNG(7,1) = "240"
    ARRAYRNG(7,2) = "10"
*
*    ARRAY() = "����� �����                        "
    ARRAYRNG(8,1) = "250"
    ARRAYRNG(8,2) = "11"
*
*    ARRAY() = "����� ���� ����� ������� �����     "
    ARRAYRNG(9,1) = "260"
    ARRAYRNG(9,2) = "12"
*
*    ARRAY()  = "������ � ������� �������� (1(      "
    ARRAYRNG(10,1) = "4500"
    ARRAYRNG(10,2) = "14"

    ARRAYRNG(11,1) = "8010"
    ARRAYRNG(11,2) = "14"

    ARRAYRNG(12,1) = "8011"
    ARRAYRNG(12,2) = "14"
*
*    ARRAY()  = "�������� �������                  "
    ARRAYRNG(13,1) = "4550"
    ARRAYRNG(13,2) = "14"
*
*    ARRAY()  = "����� �����                       "
    ARRAYRNG(14,1) = "4600"
    ARRAYRNG(14,2) = "14"
*
*    ARRAY()  = "����� �������                     "
    ARRAYRNG(15,1) = "4650"
    ARRAYRNG(15,2) = "16"
*
*    ARRAY()  = "����� ����� �� ���� �����          "
    ARRAYRNG(16,1) = "4700"
    ARRAYRNG(16,2) = "17"
*
*    ARRAY()  = "����� ������ ���� �� ���          "
    ARRAYRNG(17,1) = "4750"
    ARRAYRNG(17,2) = "18"
*
*    ARRAY()  = "������ �������(�����(             "
    ARRAYRNG(18,1) = "7000"
    ARRAYRNG(18,2) = "21"
*
*    ARRAY()  = " ������ ������� ������� ���������  "
    ARRAYRNG(19,1) = "8001"
    ARRAYRNG(19,2) = "22"

*    ARRAY()  = "  ���� ��� ���� ���������          "
    ARRAYRNG(20,1) = "8002"
    ARRAYRNG(20,2) = "22"
*
*    ARRAY()  = "������ ������� ������             "
    ARRAYRNG(21,1) = "8003"
    ARRAYRNG(21,2) = "22"
*
*    ARRAY()  = "������ ������� ������� ��� �������"
    ARRAYRNG(22,1) = "8004"
    ARRAYRNG(22,2) = "22"
*
*    ARRAY()  = "������ ���������                   "
    ARRAYRNG(23,1) = "8005"
    ARRAYRNG(23,2) = "22"
*
*    ARRAY()  = "������ ��������� ��� �������        "
    ARRAYRNG(24,1) = "8006"
    ARRAYRNG(24,2) = "22"
*
*    ARRAY()  = "����� ������� (����� ���������)    "
    ARRAYRNG(25,1) = "8007"
    ARRAYRNG(25,2) = "22"

*    ARRAY()  = "����� ������� ������               "
    ARRAYRNG(26,1) = "8008"
    ARRAYRNG(26,2) = "22"

*    ARRAY()  = "����� ������� ���������            "
    ARRAYRNG(27,1) = "6000"
    ARRAYRNG(27,2) = "20"
*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    WS.BRX = ID.COMPANY
    GOSUB A.050.GET.ALL.BR
*------------------------------------------START PROCESSING
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*----------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" WITH @ID EQ ":WS.BRX
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
*        IF WS.BR NE 1 THEN
*            GOTO A.050.A
*        END

*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        WS.ARRY.RAW = 1
        WS.ARRY.COL = 1
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0
            WS.CODE = 0
*            GOSUB A.5100.PRT.SPACE.PAGE
            GOSUB A.5000.PRT.HEAD
            GOSUB A.300.PRNT
            WS.CODE = 1
            GOSUB A.5000.PRT.HEAD
            GOSUB A.400.PRNT
        END
*
A.050.A:
    REPEAT
    RETURN
*----------------------------
A.100.PROCESS:


*------------------------------------------------------------------
*    SEL.CMD = "SELECT ":FN.LD:" WITH(CBEM.CATEG GE 21001 AND CBEM.CATEG LE 21010)"
    SEL.CMD = " SELECT ":FN.LD:" WITH CBEM.CATEG IN( 21001 21002 21003 21004 21005 21006 21007 21008 21009 21010)"
    SEL.CMD := " OR CBEM.CATEG IN( 21020 21021 21022 21023 21024 21025 21026 21027 21028 6512) AND CBEM.BR EQ ":WS.BR
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)

    LOOP

        REMOVE WS.LD.ID FROM SEL.LIST SETTING POS
    WHILE WS.LD.ID:POS
        CALL F.READ(FN.LD,WS.LD.ID,R.CBEM,F.LD,MSG.LD)
        WS.CO.CODE = R.CBEM<C.CBEM.BR>
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        IF WS.CO.CODE  NE WS.BR THEN
            GOTO BBB
        END
A.100.A:
        WS.INDSTRYA = R.CBEM<C.CBEM.NEW.SECTOR>
        WS.N.INDST  = R.CBEM<C.CBEM.NEW.INDUSTRY>
****������ ����� ������ ����� ���� �������
        IF  WS.INDSTRYA EQ 1130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END
        IF  WS.INDSTRYA EQ 2130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END
        IF  WS.INDSTRYA EQ 3130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END
        IF  WS.INDSTRYA EQ 4130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END

        IF WS.INDSTRYA EQ 0 THEN
            GOTO BBB
        END

        WS.INDSTRY = WS.INDSTRYA
        IF WS.INDSTRYA LT 4500   THEN
            WS.INDSTRY  = WS.INDSTRYA[3]
        END

        WS.1.LE = R.CBEM<C.CBEM.IN.LCY>
        WS.CATEG = R.CBEM<C.CBEM.CATEG>
        WS.CY   = R.CBEM<C.CBEM.CY>

        GOSUB A.200.ACUM
*-----------------------------------------------------
BBB:
    REPEAT
    RETURN
*-----------------------------------------------

A.200.ACUM:
    FOR WSRNG = 1 TO 27
        GOSUB A.205.CHK.INDSTRY
    NEXT WSRNG
    RETURN

A.205.CHK.INDSTRY:
    IF  WS.INDSTRY NE ARRAYRNG(WSRNG,1) THEN
        RETURN
    END
    WS = ARRAYRNG(WSRNG,2)
    GOSUB A.210.ACUM
    WS.FLAG.PRT = 1
    RETURN

****        ARRAY          ������� ��� ��
A.210.ACUM:
    WS.COMN = WS.1.LE

    IF WS.CATEG GE 21001 AND WS.CATEG LE 21003 THEN
        WS.COL.AMT = 3
    END


    IF WS.CATEG GE 21004 AND WS.CATEG LE 21005 THEN
        WS.COL.AMT = 4
    END

    IF WS.CATEG GE 21006 THEN
        WS.COL.AMT = 5
    END

    IF WS.CATEG GE 21007 THEN
        WS.COL.AMT = 6
    END

    IF WS.CATEG GE 21008 AND WS.CATEG LE 21010 THEN
        WS.COL.AMT = 7
    END

    IF WS.CATEG GE 21020 AND WS.CATEG LE 21024 THEN
        WS.COL.AMT = 7
    END


    IF WS.CY NE "EGP" THEN
        WS.COL.AMT = WS.COL.AMT + 5
    END

    WS.COMN = WS.1.LE / 1000
    ARRAY1(WS,WS.COL.AMT) = ARRAY1(WS,WS.COL.AMT) + WS.COMN

    WS.T  = ARRAY1(WS,13)
    IF WS.T GT 0  THEN
        ARRAY1(WS.T,WS.COL.AMT) = ARRAY1(WS.T,WS.COL.AMT) + WS.COMN
    END

    ARRAY1(23,WS.COL.AMT) = ARRAY1(23,WS.COL.AMT) + WS.COMN
    RETURN
*****                                     ARRAY   ������� ��

A.300.PRNT:
    FOR I = 1 TO 23
        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.310.PRT.HD
        END

        IF WS.H.D.T = "T" THEN
            GOSUB A.320.PRT.TOT
        END

        IF WS.H.D.T = "D" THEN
            GOSUB A.330.PRT.DTAL
        END

    NEXT I
    RETURN
A.310.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
****       XX<1,1>[1,35]   = "--------------------------"
    XX<1,1>[1,35]   =  STR('-',35)
    PRINT XX<1,1>
    RETURN

A.320.PRT.TOT:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[40,7]   = FMT(ARRAY1(I,3), "R0,")
    XX<1,1>[52,7]   = FMT(ARRAY1(I,4), "R0,")
    XX<1,1>[68,7]   = FMT(ARRAY1(I,5), "R0,")
    XX<1,1>[89,7]   = FMT(ARRAY1(I,6), "R0,")
    XX<1,1>[109,8]   = FMT(ARRAY1(I,7), "R0,")
    WS.COMN1 = ARRAY1(I,3) + ARRAY1(I,4) + ARRAY1(I,5) + ARRAY1(I,6) + ARRAY1(I,7)
    XX<1,1>[124,7]   = FMT(WS.COMN1, "R0,")

    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,132]   = STR('-',132)
    PRINT XX<1,1>
    RETURN
A.330.PRT.DTAL:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[40,7]   = FMT(ARRAY1(I,3), "R0,")
    XX<1,1>[52,7]   = FMT(ARRAY1(I,4), "R0,")
    XX<1,1>[68,7]   = FMT(ARRAY1(I,5), "R0,")
    XX<1,1>[89,7]   = FMT(ARRAY1(I,6), "R0,")
    XX<1,1>[109,8]  = FMT(ARRAY1(I,7), "R0,")
    WS.COMN1 = ARRAY1(I,3) + ARRAY1(I,4) + ARRAY1(I,5) + ARRAY1(I,6) + ARRAY1(I,7)
    XX<1,1>[124,7]   = FMT(WS.COMN1, "R0,")
    PRINT XX<1,1>
    RETURN

A.400.PRNT:
    FOR I = 1 TO 23
        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.410.PRT.HD
        END

        IF WS.H.D.T = "T" THEN
            GOSUB A.420.PRT.TOT
        END

        IF WS.H.D.T = "D" THEN
            GOSUB A.430.PRT.DTAL
        END

    NEXT I
    RETURN
A.410.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
****       XX<1,1>[1,35]   = "--------------------------"
    XX<1,1>[1,35]   =  STR('-',35)
    PRINT XX<1,1>
    RETURN

A.420.PRT.TOT:
    XX = SPACE(134)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[40,7]   = FMT(ARRAY1(I,8), "R0,")
    XX<1,1>[52,7]   = FMT(ARRAY1(I,9), "R0,")
    XX<1,1>[68,7]   = FMT(ARRAY1(I,10), "R0,")
    XX<1,1>[89,7]   = FMT(ARRAY1(I,11), "R0,")
    XX<1,1>[109,7]   = FMT(ARRAY1(I,12), "R0,")
    WS.COMN1 = ARRAY1(I,8) + ARRAY1(I,9) + ARRAY1(I,10) + ARRAY1(I,11) + ARRAY1(I,12)
    XX<1,1>[124,7]   = FMT(WS.COMN1, "R0,")

    PRINT XX<1,1>
*    WS.COMN1.TOT = 0
*    WS.COMN.TOT = 0

*    IF I = 23 THEN
*        WS.COMN1.FINAL.TOT = 0
*        WS.COMN.FINAL.TOT = 0
*    END
    XX = SPACE(132)
    XX<1,1>[1,132]   = STR('-',132)
    PRINT XX<1,1>
    WS.COMN1.TOT = 0
    WS.COMN.TOT = 0
    ARRAY1(I,3) = 0
    ARRAY1(I,4) = 0
    ARRAY1(I,5) = 0
    ARRAY1(I,6) = 0
    ARRAY1(I,7) = 0
    ARRAY1(I,8) = 0
    ARRAY1(I,9) = 0
    ARRAY1(I,10) = 0
    ARRAY1(I,11) = 0
    ARRAY1(I,12) = 0
    RETURN
*------------------------------------------
A.430.PRT.DTAL:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[40,7]   = FMT(ARRAY1(I,8), "R0,")
    XX<1,1>[52,7]   = FMT(ARRAY1(I,9), "R0,")
    XX<1,1>[68,7]   = FMT(ARRAY1(I,10), "R0,")
    XX<1,1>[89,8]   = FMT(ARRAY1(I,11), "R0,")
    XX<1,1>[109,7]  = FMT(ARRAY1(I,12), "R0,")

    WS.COMN1 = ARRAY1(I,8) + ARRAY1(I,9) + ARRAY1(I,10) + ARRAY1(I,11) + ARRAY1(I,12)
    XX<1,1>[124,7]   = FMT(WS.COMN1, "R0,")
    PRINT XX<1,1>

    ARRAY1(I,3) = 0
    ARRAY1(I,4) = 0
    ARRAY1(I,5) = 0
    ARRAY1(I,6) = 0
    ARRAY1(I,7) = 0
    ARRAY1(I,8) = 0
    ARRAY1(I,9) = 0
    ARRAY1(I,10) = 0
    ARRAY1(I,11) = 0
    ARRAY1(I,12) = 0
    RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

*    CALL F.READ(FN.BR,WS.BR.H,R.BR,F.BR,MSG.BR)
*    WS.BR.NAME = R.BR<EB.DAO.NAME>
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(20):WS.HD.TA
    PR.HD :="'L'":SPACE(107):WS.HD.T2A:SPACE(3):WS.PAGE.NO
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    IF WS.CODE = 0  THEN
        PR.HD :="'L'":SPACE(70):HEAD.00
    END
    IF WS.CODE = 1  THEN
        PR.HD :="'L'":SPACE(70):HEAD.00.A
    END
    PR.HD :="'L'":SPACE(40):HEAD.01:SPACE(6):HEAD.01.1:SPACE(6):HEAD.01.2:SPACE(6):HEAD.01.3:SPACE(6):HEAD.01.4:SPACE(6):HEAD.01.5
    PR.HD :="'L'":SPACE(40):HEAD.02:SPACE(6):HEAD.02.1:SPACE(7):HEAD.02.2:SPACE(11):HEAD.02.3
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
