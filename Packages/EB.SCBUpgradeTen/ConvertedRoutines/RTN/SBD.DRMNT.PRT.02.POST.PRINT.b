* @ValidationCode : Mjo4MTk4MTI1ODpDcDEyNTI6MTY0MDgyNDAwMDcwMDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:26:40
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-131</Rating>
*-----------------------------------------------------------------------------
***============================================
*** COPY FROM SBD.EXCESS.LIMIT.DET.DRMN.RNG ***
*** 2010/11/17 ***
***============================================
SUBROUTINE SBD.DRMNT.PRT.02.POST.PRINT
*----------------------------------------------
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DRMNT.FILE
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*----------------------------------------------
    GOSUB INITIATE
*----------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================
INITIATE:
    REPORT.ID = 'SBD.DRMNT.PRT.02.POST.PRINT'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.DR = "F.SCB.DRMNT.FILE"  ;  F.DR = ""
    CALL OPF(FN.DR,F.DR)

    FN.CUS = 'FBNK.CUSTOMER'    ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    CUST.ARR   = ""
    CUST.ARR.2 = ""
    CUST.ARR.3 = ""
    CUST.ARR.4 = ""
    CUST.ARR.5 = ""
    CODE       = TIMEDATE()
    WS.COMP       = ID.COMPANY
    FLG.PRT    = 0


    GOSUB GET.DATA


RETURN
*===============================================================
GET.DATA:
    N.SEL = "SELECT ":FN.DR:" WITH COMPANY.CODE EQ ":WS.COMP:" AND PRINT.UPDATE EQ 'P' BY @ID"
*    N.SEL = "SELECT ":FN.DR:" WITH COMPANY.CODE EQ ":WS.COMP:" AND FLAG.RUN EQ 'NO' AND PRINT.UPDATE EQ 'P' BY @ID"
    CALL EB.READLIST(N.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR NN = 1 TO SELECTED
            CALL F.READ(FN.DR,KEY.LIST<NN>,R.DR,F.DR,E.DR)
            RUN.DATE  = R.DR<DRMNT.RUN.DATE>
            TOD.DATE  = TODAY
            TOD.DATE3 = TODAY
            CALL CDT ('' , TOD.DATE3 , "-3W")


            IF RUN.DATE GE TOD.DATE3 THEN
                FLG.PRT = 1
                CUSTOMER.NO   = R.DR<DRMNT.CUSTOMER.NO>
                CUSTOMER.DATE = R.DR<DRMNT.LAST.TRANS.DATE>

                GOSUB PRINT.HEAD
                GOSUB PROCESS
            END
        NEXT NN
    END
    IF  FLG.PRT EQ 0 THEN
        TEXT = "NO RECORDS FOR PRINTING " ; CALL REM
    END
RETURN
*===============================================================
PROCESS:
    CUST.ARR   = ""
    CUST.ARR.2 = ""
    CUST.ARR.3 = ""
    CUST.ARR.4 = ""
    CUST.ARR.5 = ""

    CALL F.READ(FN.CUS,CUSTOMER.NO,R.CUS,F.CUS,ER.CUS)
    CUST.NAME    = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
    CUST.ADDRESS = R.CUS<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>

    CUST.ADD     = ""
*Line [ 116 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
    CUS.ADD.N    = DCOUNT(CUST.ADDRESS,@SM)

    CUST.ADD<1,1> = CUST.ADDRESS<1,1,1>
    CUST.ADD<1,2> = CUST.ADDRESS<1,1,2>
    CUST.ADD<1,3> = CUST.ADDRESS<1,1,3>

    CUST.ARR<1,1>[1,10]     = "�����"
    CUST.ARR<1,1>[15,30]    = CUST.NAME

    CUST.ARR.2<1,1>[1,10]   = "�������"
    CUST.ARR.2<1,1>[15,50]  =  CUST.ADD<1,1>
    CUST.ARR.2<1,2>[15,50]  =  CUST.ADD<1,2>
    CUST.ARR.2<1,3>[15,50]  =  CUST.ADD<1,3>

    CUST.ARR.3<1,1>[1,10]   = "������� "
    CUST.ARR.3<1,1>[15,30]  = "������  ���" :" ": CUSTOMER.NO

    CUST.ARR.4<1,1>[2,10]   = "�����"
    CUST.ARR.4<1,1>[15,20]  = FMT(CUSTOMER.DATE,"####/##/##")

    PRINT CUST.ARR
    PRINT CUST.ARR.2<1,1>
    PRINT CUST.ARR.2<1,2>
    PRINT CUST.ARR.2<1,3>
    PRINT CUST.ARR.3
    PRINT CUST.ARR.4

    GOSUB WRITE.FLAG.TO.DRMNT.FILE
RETURN
*===================================================================
WRITE.FLAG.TO.DRMNT.FILE:

    DRMNT.ID = ""
    DRMNT.ID = "P.":WS.COMP:".":CUSTOMER.NO

    R.DR<DRMNT.FLAG.RUN>        =  "YES"
****UPDATED BY NESSREEN AHMED 24/3/2016 for R15****
****    WRITE R.DR TO F.DR , DRMNT.ID ON ERROR
****    END
    CALL F.WRITE(FN.DR,DRMNT.ID,R.DR)
    CALL JOURNAL.UPDATE(DRMNT.ID)
****END OF UPDATE 24/3/2016*****************************
RETURN
*===================================================================
PRINT.HEAD:
*----------
    CUS.LEN = LEN(CUSTOMER.NO)

    IF CUS.LEN EQ 7 THEN
        BRANCH.ID = CUSTOMER.NO[1,1]
    END

    IF CUS.LEN EQ 8 THEN
        BRANCH.ID = CUSTOMER.NO[1,2]
    END

*Line [ 173 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRANCH.ID,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRANCH.ID,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[1,4]:'/':DATY[5,2]:"/":DATY[7,2]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : " : T.DAY : SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":SPACE(1):"CUST.POST"
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(55):"����� �������"
    PR.HD :="'L'":SPACE(50):STR('_',20)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PRINT
    HEADING PR.HD
RETURN
*=====================================================
END
