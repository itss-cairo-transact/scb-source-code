* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBD.LD.AUTO.RECOVER

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PD.PAYMENT.DUE
*-----------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-----------------------------------------------------------
INITIATE:

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    FN.SCH = 'FBNK.LD.SCHEDULE.DEFINE' ; F.SCH = ''
    CALL OPF(FN.SCH,F.SCH)

    FN.PD = 'FBNK.PD.PAYMENT.DUE' ; F.PD = ''
    CALL OPF(FN.PD,F.PD)

    FN.CA = 'FBNK.CUSTOMER.ACCOUNT' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    WS.DAT = TODAY

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    OPENSEQ "MECH" , "LOANS.AUTO.RECOVERY" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LOANS.AUTO.RECOVERY"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LOANS.AUTO.RECOVERY" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE LOANS.AUTO.RECOVERY CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LOANS.AUTO.RECOVERY File IN MECH'
        END
    END


    OPENSEQ "MECH" , "LOANS.AUTO.RECOVERY.ACCOUNTS.CSV" TO AA THEN
        CLOSESEQ AA
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"LOANS.AUTO.RECOVERY.ACCOUNTS.CSV"
        HUSH OFF
    END
    OPENSEQ "MECH" , "LOANS.AUTO.RECOVERY.ACCOUNTS.CSV" TO AA ELSE
        CREATE AA THEN
            PRINT 'FILE LOANS.AUTO.RECOVERY.ACCOUNTS.CSV CREATED IN MECH'
        END ELSE
            STOP 'Cannot create LOANS.AUTO.RECOVERY.ACCOUNTS.CSV File IN MECH'
        END
    END


    HEAD.DESC  = "Orginal.Account":","
    HEAD.DESC := "Recovery.Account":","
    HEAD.DESC := "Amount":","
    HEAD.DESC := "Past.Due.Amount":","

    AA.DATA = HEAD.DESC
    WRITESEQ AA.DATA TO AA ELSE
        PRINT " ERROR WRITE FILE "
    END

    DB.AMT = 0 ; WS.PD.AMT = 0

    RETURN
*------------------------------------------------------------
PROCESS:

    T.SEL = "SELECT ":FN.PD:" WITH CATEGORY GE 21063 AND CATEGORY LE 21066 AND TOTAL.OVERDUE.AMT NE '' AND CURRENCY EQ 'EGP' BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.PD,KEY.LIST<I>,R.PD,F.PD,E1)
            CALL F.READ(FN.PD,KEY.LIST<I+1>,R.PD1,F.PD,E2)
            WS.CUST.ID  = R.PD<PD.CUSTOMER>
            WS.CUST.ID1 = R.PD1<PD.CUSTOMER>

            LD.NO = KEY.LIST<I>[12]
            CALL F.READ(FN.LD,LD.NO,R.LD,F.LD,E4)
            WS.PD.ACCT = R.LD<LD.PRIN.LIQ.ACCT>

** WS.PD.ACCT  = R.PD<PD.ORIG.STLMNT.ACT>

            WS.PD.AMT  += R.PD<PD.TOTAL.OVERDUE.AMT>
            PD.NO       = KEY.LIST<I>
            IF WS.CUST.ID NE WS.CUST.ID1 THEN
                CALL F.READ(FN.AC,WS.PD.ACCT,R.AC,F.AC,ER3)
                PD.ACCT.BAL   = R.AC<AC.ONLINE.ACTUAL.BAL>
                PD.ACCT.COMP  = R.AC<AC.CO.CODE>
                PD.ACCT.CATEG = R.AC<AC.CATEGORY>

                IF PD.ACCT.BAL LT 0 THEN
                    PD.ACCT.BAL = PD.ACCT.BAL * -1
                    WS.PD.AMT   = WS.PD.AMT + PD.ACCT.BAL
                END ELSE
                    WS.PD.AMT   = WS.PD.AMT - PD.ACCT.BAL
                END

                IF PD.ACCT.BAL LT WS.PD.AMT THEN

                    CALL F.READ(FN.CA,WS.CUST.ID,R.CA,F.CA,E2)

                    LOOP
                        REMOVE ACC.NO FROM R.CA SETTING POS.ACC
                    WHILE ACC.NO:POS.ACC

                        CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,ER.AC)
                        AC.CATEG         = R.AC<AC.CATEGORY>
                        AC.CCY           = R.AC<AC.CURRENCY>
                        IF AC.CATEG NE PD.ACCT.CATEG THEN
                            IF ( AC.CATEG GE 6501 AND AC.CATEG LE 6517 ) THEN
                                AC.AVAL.BAL1 = R.AC<AC.WORKING.BALANCE>
                            END ELSE
                                AC.AVAL.BAL1 = R.AC<AC.ONLINE.ACTUAL.BAL>
                            END

*Line [ 160 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            LOCKED.BAL.COUNT = DCOUNT(R.AC<AC.LOCKED.AMOUNT>,@VM)
                            AC.LOCKED.BAL    = R.AC<AC.LOCKED.AMOUNT><1,LOCKED.BAL.COUNT>

                            AC.AVAL.BAL = AC.AVAL.BAL1 - AC.LOCKED.BAL

                            IF ( AC.CATEG EQ 1001 OR AC.CATEG EQ 1535 OR ( AC.CATEG GE 1003 AND AC.CATEG LE 1009 ) OR ( AC.CATEG GE 6501 AND AC.CATEG LE 6517 )) AND AC.CCY EQ 'EGP' THEN
                                IF AC.AVAL.BAL NE '' AND AC.AVAL.BAL GT 0 THEN
                                    IF AC.AVAL.BAL LE WS.PD.AMT THEN
                                        DB.AMT  = AC.AVAL.BAL
                                        GOSUB CREATE.FT
                                    END ELSE
                                        DB.AMT = WS.PD.AMT
                                        GOSUB CREATE.FT
                                    END
                                END
                            END
                        END
                    REPEAT
                END
                WS.PD.AMT = 0
            END
NEXT.CUST:
        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN LOANS.AUTO.RECOVERY'
    EXECUTE 'DELETE ':"MECH":' ':"LOANS.AUTO.RECOVERY"


    RETURN
*------------------------------------------------------------
CREATE.FT:
    IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//':PD.ACCT.COMP:','

    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":','
    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":AC.CCY:','
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":AC.CCY:','
    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":ACC.NO:','
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":WS.PD.ACCT:','
    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:','
    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":WS.DAT:','
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":WS.DAT:','
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"Auto.Recovery.PD":','
    OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":"Auto.Recovery.PD":','
    OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":','
    OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO"


    MSG.DATA = IDD:",":OFS.MESSAGE.DATA

    WRITESEQ MSG.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

************************************************************

    AA.DATA  = WS.PD.ACCT:","
    AA.DATA := ACC.NO:","
    AA.DATA := DB.AMT:","
    AA.DATA := WS.PD.AMT:","

    WRITESEQ AA.DATA TO AA ELSE
        PRINT " ERROR WRITE FILE "
    END

************************************************************

    WS.PD.AMT = WS.PD.AMT - DB.AMT

    IF WS.PD.AMT = 0 THEN
        GO TO NEXT.CUST
    END
    RETURN
*============================================================
