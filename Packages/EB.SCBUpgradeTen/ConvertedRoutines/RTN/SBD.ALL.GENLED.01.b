* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****MOHSEN
*    PROGRAM SBD.ALL.GENLED.01.01
    SUBROUTINE SBD.ALL.GENLED.01
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*---------------------------------------------------

    FN.LINE = "FBNK.RE.STAT.LINE.BAL"
    F.LINE  = ""

    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

    FN.DATE = "F.DATES"
    F.DATE  = ""

    FN.REP = "F.RE.STAT.REP.LINE"
    F.REP  = ""

*-----------------------------------------------------
    R.FIN = ""
    R.DATE = ""
    MSG.DATE = ""
    WS.LINE.NO = 0
    WS.LINE.NO.COMP = 0

    WS.OPEN.BAL.T =  0
    WS.CR.MOV.T   =  0
    WS.DR.MOV.T   =  0
    WS.CLOS.BAL.T =  0

    WS.OPEN.BAL.FT =  0
    WS.CR.MOV.FT   =  0
    WS.DR.MOV.FT   =  0
    WS.CLOS.BAL.FT =  0

    WS.OPEN.BAL.FT.AL =  0
    WS.CR.MOV.FT.AL   =  0
    WS.DR.MOV.FT.AL   =  0
    WS.CLOS.BAL.FT.AL =  0

    WS.OPEN.BAL.FT.AF =  0
    WS.CR.MOV.FT.AF   =  0
    WS.DR.MOV.FT.AF   =  0
    WS.CLOS.BAL.FT.AF =  0

    WS.OPEN.BAL.FT.LL =  0
    WS.CR.MOV.FT.LL   =  0
    WS.DR.MOV.FT.LL   =  0
    WS.CLOS.BAL.FT.LL =  0

    WS.OPEN.BAL.FT.LF =  0
    WS.CR.MOV.FT.LF   =  0
    WS.DR.MOV.FT.LF   =  0
    WS.CLOS.BAL.FT.LF =  0

    WS.OPEN.BAL.CONT.DEF  = 0
    WS.DR.MOV.CONT.DEF    = 0
    WS.CR.MOV.CONT.DEF    = 0
    WS.CLOS.BAL.CONT.DEF  = 0

    WS.HEAD.01.A = "���� �����"
    WS.HEAD.01.B = "������ ������ ������ �� "

    WS.HEAD.02.A = "��� �����"
    WS.HEAD.02.B = "������ ���������"
    WS.HEAD.02.C = "������� �������"
    WS.HEAD.02.D = "������� �������"
    WS.HEAD.02.E = "������ �������"

    WS.PAGE.COUNT = 0

*----------------------------PROCEDURE---------------------
    CALL OPF (FN.LINE,F.LINE)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.DATE,F.DATE)
    CALL OPF (FN.REP,F.REP)
*---------------------------------------------------------
    REPORT.ID='SBM.C.PRT.001'
*    REPORT.ID='SBM.C.PRT.003'
    CALL PRINTER.ON(REPORT.ID,'')

*------------------------PROCEDURE------------------------

    GOSUB A.050.DATE
    GOSUB A.500.PRT.HEAD
    XX = SPACE(132)
    XX<1,1>[1,20]   =  "������ �������"
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,20]   =  STR('=',20)
    PRINT XX<1,1>

    GOSUB A.150.READ.LINE.BAL
*??    GOSUB A.300.PRT
*??    GOSUB A.460.CONT.DEF

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*-----------------------------------------------
A.050.DATE:
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.TODAY = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.DAT.HD= R.DATE<EB.DAT.LAST.WORKING.DAY>
*    WS.TODAY  = TODAY
*    WS.TODAY  = 20100131
*    CRT WS.TODAY
    RETURN
*------------------------------------------------
A.150.READ.LINE.BAL:
    SEL.CMD = "SELECT ":FN.LINE:" WITH @ID LIKE GENLED-... AND @ID LIKE ...":WS.TODAY:"... BY @ID"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.LINE.ID FROM SEL.LIST SETTING POS
    WHILE WS.LINE.ID:POS
        CALL F.READ(FN.LINE,WS.LINE.ID,R.LINE,F.LINE,MSG.LINE)
        WS.COMN.ID = WS.LINE.ID
        WS.BR = WS.COMN.ID[2]
        IF WS.BR EQ 88 THEN
            GOTO A.150.REP
        END
*        IF WS.BR EQ 99 THEN
*            GOTO A.150.REP
*        END
*        GOTO A.150.REP
A.150.CONT:
        WS.COMN.CY = FIELD(WS.COMN.ID,"-", 3)
        WS.COMN.LINE.F = FIELD(WS.COMN.ID,"-", 2)
        IF WS.COMN.CY EQ "LOCAL" THEN
            GOTO A.150.REP
        END

        WS.LINE.NO = WS.COMN.LINE.F

        IF WS.LINE.NO.COMP EQ  0   THEN
            WS.LINE.NO.COMP = WS.LINE.NO
        END

        IF  WS.LINE.NO NE WS.LINE.NO.COMP  THEN
            GOSUB A.300.PRT
            WS.LINE.NO.COMP = WS.LINE.NO
        END
        IF WS.LINE.NO.COMP  LT 148  THEN
            GOSUB A.160.LCL
            GOSUB A.170.ACUM
        END
        IF WS.LINE.NO.COMP  GT 500  AND WS.LINE.NO.COMP LE 649 THEN
            GOSUB A.160.LCL
            GOSUB A.170.ACUM
        END
        IF WS.LINE.NO.COMP  GT 200  AND WS.LINE.NO.COMP LE 349 THEN
            GOSUB A.165.FCY
            GOSUB A.200.CALC.FCY
            GOSUB A.170.ACUM
        END
        IF WS.LINE.NO.COMP  GT 700  AND WS.LINE.NO.COMP LE 849 THEN
            GOSUB A.165.FCY
            GOSUB A.200.CALC.FCY
            GOSUB A.170.ACUM
        END

        IF WS.LINE.NO.COMP  GT 849 AND WS.LINE.NO.COMP LT 1450 THEN
            GOSUB A.250.CONT
        END

        IF WS.LINE.NO.COMP  GE 9990 THEN
            GOSUB A.250.CONT
        END

A.150.REP:
    REPEAT
A.150.EXIT:
    RETURN
*------------------------------------------------
*������� ���� � ���� ����

A.160.LCL:
    IF WS.COMN.CY EQ "EGP" THEN
        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL>
        WS.CR.MOV   = R.LINE<RE.SLB.CR.MOVEMENT>
        WS.DR.MOV   = R.LINE<RE.SLB.DB.MOVEMENT>
        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL>
        RETURN
    END
    IF WS.LINE.NO EQ 120 OR WS.LINE.NO EQ 630 THEN
        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL.LCL>
        WS.CR.MOV   = R.LINE<RE.SLB.CR.MVMT.LCL>
        WS.DR.MOV   = R.LINE<RE.SLB.DB.MVMT.LCL>
        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL.LCL>
        RETURN
    END
    IF WS.COMN.CY NE "EGP" THEN
*        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL.LCL>
*        WS.CR.MOV   = R.LINE<RE.SLB.CR.MVMT.LCL>
*        WS.DR.MOV   = R.LINE<RE.SLB.DB.MVMT.LCL>
*        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL.LCL>
        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL>
        WS.CR.MOV   = R.LINE<RE.SLB.CR.MOVEMENT>
        WS.DR.MOV   = R.LINE<RE.SLB.DB.MOVEMENT>
        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL>
        GOSUB A.200.CALC.FCY
    END
    RETURN

*-----------------------------------------------
*������� ���� � ���� �����

A.165.FCY:
    IF WS.COMN.CY EQ "EGP" THEN
        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL>
        WS.CR.MOV   = R.LINE<RE.SLB.CR.MOVEMENT>
        WS.DR.MOV   = R.LINE<RE.SLB.DB.MOVEMENT>
        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL>
        RETURN
    END
    IF WS.COMN.CY NE "EGP" THEN
        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL>
        WS.CR.MOV   = R.LINE<RE.SLB.CR.MOVEMENT>
        WS.DR.MOV   = R.LINE<RE.SLB.DB.MOVEMENT>
        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL>
    END
    RETURN

*------------------------------------------------
A.170.ACUM:
    WS.OPEN.BAL.T =  WS.OPEN.BAL.T + WS.OPEN.BAL
    WS.CR.MOV.T   =  WS.CR.MOV.T  + WS.CR.MOV
    WS.DR.MOV.T   =  WS.DR.MOV.T  + WS.DR.MOV
    WS.CLOS.BAL.T =  WS.CLOS.BAL.T + WS.CLOS.BAL

*����� ������ ����
    IF WS.LINE.NO.COMP  LT  148 THEN
        WS.OPEN.BAL.FT.AL =  WS.OPEN.BAL.FT.AL + WS.OPEN.BAL
        WS.CR.MOV.FT.AL   =  WS.CR.MOV.FT.AL  + WS.CR.MOV
        WS.DR.MOV.FT.AL   =  WS.DR.MOV.FT.AL  + WS.DR.MOV
        WS.CLOS.BAL.FT.AL =  WS.CLOS.BAL.FT.AL + WS.CLOS.BAL
    END

*����� ������ �����
    IF WS.LINE.NO.COMP  GT 200  AND WS.LINE.NO.COMP LE 349 THEN
        WS.OPEN.BAL.FT.AF =  WS.OPEN.BAL.FT.AF + WS.OPEN.BAL
        WS.CR.MOV.FT.AF   =  WS.CR.MOV.FT.AF  + WS.CR.MOV
        WS.DR.MOV.FT.AF   =  WS.DR.MOV.FT.AF  + WS.DR.MOV
        WS.CLOS.BAL.FT.AF =  WS.CLOS.BAL.FT.AF + WS.CLOS.BAL
    END

*������� ������ ������
    IF WS.LINE.NO.COMP  GT 500  AND WS.LINE.NO.COMP LE 649 THEN
        WS.OPEN.BAL.FT.LL =  WS.OPEN.BAL.FT.LL + WS.OPEN.BAL
        WS.CR.MOV.FT.LL   =  WS.CR.MOV.FT.LL  + WS.CR.MOV
        WS.DR.MOV.FT.LL   =  WS.DR.MOV.FT.LL  + WS.DR.MOV
        WS.CLOS.BAL.FT.LL =  WS.CLOS.BAL.FT.LL + WS.CLOS.BAL
    END

*����� ������ �������
    IF WS.LINE.NO.COMP  GT 700  AND WS.LINE.NO.COMP LE 849 THEN
        WS.OPEN.BAL.FT.LF =  WS.OPEN.BAL.FT.LF + WS.OPEN.BAL
        WS.CR.MOV.FT.LF   =  WS.CR.MOV.FT.LF  + WS.CR.MOV
        WS.DR.MOV.FT.LF   =  WS.DR.MOV.FT.LF  + WS.DR.MOV
        WS.CLOS.BAL.FT.LF =  WS.CLOS.BAL.FT.LF + WS.CLOS.BAL
    END

    RETURN

*-----------------------------------------------
A.200.CALC.FCY:
    WS.KEYCY = "NZD"
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    IF MSG.CY EQ "" THEN
        GOSUB A.210.CALC
    END

    RETURN

A.210.CALC:
    A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 307 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.COMN.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
    WS.RATE.ARY = R.CCY<RE.BCP.RATE>
    WS.RATE = WS.RATE.ARY<1,CYPOS>
    WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
    WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>


    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.OPEN.BAL * WS.RATE
        WS.OPEN.BAL = WS.AMT.LOCAL
        WS.AMT.LOCAL = WS.CR.MOV   * WS.RATE
        WS.CR.MOV = WS.AMT.LOCAL
        WS.AMT.LOCAL = WS.DR.MOV   * WS.RATE
        WS.DR.MOV = WS.AMT.LOCAL
        WS.AMT.LOCAL = WS.CLOS.BAL * WS.RATE
        WS.CLOS.BAL = WS.AMT.LOCAL

    END

    IF WS.MLT.DIVD NE "MULTIPLY" THEN

        WS.AMT.LOCAL = WS.OPEN.BAL / WS.RATE
        WS.OPEN.BAL = WS.AMT.LOCAL
        WS.AMT.LOCAL = WS.CR.MOV   / WS.RATE
        WS.CR.MOV = WS.AMT.LOCAL
        WS.AMT.LOCAL = WS.DR.MOV   / WS.RATE
        WS.DR.MOV = WS.AMT.LOCAL
        WS.AMT.LOCAL = WS.CLOS.BAL / WS.RATE
        WS.CLOS.BAL = WS.AMT.LOCAL
    END

    RETURN
*------------------------------------------------
A.250.CONT:
*    IF WS.COMN.CY EQ "EGP" THEN
    WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL>
    WS.CR.MOV   = R.LINE<RE.SLB.CR.MOVEMENT>
    WS.DR.MOV   = R.LINE<RE.SLB.DB.MOVEMENT>
    WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL>

    IF WS.LINE.NO.COMP  EQ 9993 THEN
        WS.OPEN.BAL = R.LINE<RE.SLB.OPEN.BAL.LCL>
        WS.CR.MOV   = R.LINE<RE.SLB.CR.MVMT.LCL>
        WS.DR.MOV   = R.LINE<RE.SLB.DB.MVMT.LCL>
        WS.CLOS.BAL = R.LINE<RE.SLB.CLOSING.BAL.LCL>
    END

    IF WS.COMN.CY NE "EGP" AND WS.LINE.NO.COMP NE 9993 THEN
        GOSUB A.165.FCY
        GOSUB A.200.CALC.FCY
    END
*    GOSUB A.170.ACUM

    WS.OPEN.BAL.T =  WS.OPEN.BAL.T + WS.OPEN.BAL
    WS.CR.MOV.T   =  WS.CR.MOV.T  + WS.CR.MOV
    WS.DR.MOV.T   =  WS.DR.MOV.T  + WS.DR.MOV
    WS.CLOS.BAL.T =  WS.CLOS.BAL.T + WS.CLOS.BAL

    WS.OPEN.BAL.CONT.DEF =  WS.OPEN.BAL.CONT.DEF + WS.OPEN.BAL
    WS.CR.MOV.CONT.DEF  =  WS.CR.MOV.CONT.DEF  + WS.CR.MOV
    WS.DR.MOV.CONT.DEF  =  WS.DR.MOV.CONT.DEF  + WS.DR.MOV
    WS.CLOS.BAL.CONT.DEF =  WS.CLOS.BAL.CONT.DEF + WS.CLOS.BAL
    RETURN
*------------------------------------------------
A.300.PRT:
*    CRT  WS.LINE.NO.COMP
*    CRT  WS.OPEN.BAL.T:" ":WS.CR.MOV.T:"  ":WS.DR.MOV.T:"  ":WS.CLOS.BAL.T
    WS.REP.ID = "GENLED.":WS.LINE.NO.COMP
    CALL F.READ(FN.REP,WS.REP.ID,R.REP,F.REP,MSG.REP)
    WS.LINE.TITL = R.REP<RE.SRL.DESC,2,2>
    IF  WS.LINE.TITL EQ ""  THEN
        WS.LINE.TITL = R.REP<RE.SRL.DESC,2,1>
    END
    IF  WS.LINE.TITL EQ ""  THEN
        WS.LINE.TITL = R.REP<RE.SRL.DESC,1,2>
    END
    XX = SPACE(132)
    PRINT XX<1,1>
    XX = SPACE(132)

    XX<1,1>[1,8]   =  ""
    XX<1,1>[9,35]   =  WS.LINE.TITL
    XX<1,1>[46,20]   = FMT(WS.OPEN.BAL.T, "R2,")
    XX<1,1>[68,20]   = FMT(WS.DR.MOV.T, "R2,")
    XX<1,1>[90,20]   = FMT(WS.CR.MOV.T, "R2,")
    XX<1,1>[112,20]   = FMT(WS.CLOS.BAL.T, "R2,")
    PRINT XX<1,1>
    IF WS.LINE.NO.COMP  EQ 145  THEN
        GOSUB A.400.PRT.TOT
        GOSUB A.500.PRT.HEAD
        XX = SPACE(132)
        XX<1,1>[1,20]   =  "������ ��������"
        PRINT XX<1,1>
        XX = SPACE(132)
        XX<1,1>[1,20]   =  STR('=',20)
        PRINT XX<1,1>
    END
    IF WS.LINE.NO.COMP  EQ 337  THEN
        GOSUB A.400.PRT.TOT
        GOSUB A.500.PRT.HEAD
        XX = SPACE(132)
        XX<1,1>[1,20]   =  "������ ������"
        PRINT XX<1,1>
        XX = SPACE(132)
        XX<1,1>[1,20]   =  STR('=',20)
        PRINT XX<1,1>
    END
    IF WS.LINE.NO.COMP  EQ 640  THEN
        GOSUB A.400.PRT.TOT
        GOSUB A.500.PRT.HEAD
        XX = SPACE(132)
        XX<1,1>[1,20]   =  "������ �������"
        PRINT XX<1,1>
        XX = SPACE(132)
        XX<1,1>[1,20]   =  STR('=',20)
        PRINT XX<1,1>
    END
    IF WS.LINE.NO.COMP  EQ 840  THEN
        GOSUB A.400.PRT.TOT
        GOSUB A.450.DEF.TOTALS
        GOSUB A.500.PRT.HEAD
    END
*???
    IF WS.LINE.NO.COMP  EQ 1427  THEN
        GOSUB A.460.CONT.DEF
    END
    WS.OPEN.BAL.T = 0
    WS.CR.MOV.T = 0
    WS.DR.MOV.T = 0
    WS.CLOS.BAL.T = 0
    RETURN
*---------------------------------------------
A.400.PRT.TOT:
    IF WS.LINE.NO.COMP  EQ 145  THEN
        WS.1 = WS.OPEN.BAL.FT.AL
        WS.2  = WS.DR.MOV.FT.AL
        WS.3 =  WS.CR.MOV.FT.AL
        WS.4  = WS.CLOS.BAL.FT.AL
        WS.LINE.TITL = "������ ������ ����"
    END
    IF WS.LINE.NO.COMP  EQ 337  THEN
        WS.1 = WS.OPEN.BAL.FT.AF
        WS.2  = WS.DR.MOV.FT.AF
        WS.3 =  WS.CR.MOV.FT.AF
        WS.4  = WS.CLOS.BAL.FT.AF
        WS.LINE.TITL = "������ ������ �����"
    END
    IF WS.LINE.NO.COMP  EQ 640  THEN
        WS.1 = WS.OPEN.BAL.FT.LL
        WS.2  = WS.DR.MOV.FT.LL
        WS.3 =  WS.CR.MOV.FT.LL
        WS.4  = WS.CLOS.BAL.FT.LL
        WS.LINE.TITL = "������ ������ ����"
    END
    IF WS.LINE.NO.COMP  EQ 840  THEN
        WS.1  = WS.OPEN.BAL.FT.LF
        WS.2  = WS.DR.MOV.FT.LF
        WS.3 =  WS.CR.MOV.FT.LF
        WS.4  = WS.CLOS.BAL.FT.LF
        WS.LINE.TITL = "������ ������ �����"
    END

    GOSUB A.490.DASH

    XX = SPACE(132)
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,8]   =  ""
    XX<1,1>[9,35]   =  WS.LINE.TITL
    XX<1,1>[46,20]   = FMT(WS.1, "R2,")
    XX<1,1>[68,20]   = FMT(WS.2, "R2,")
    XX<1,1>[90,20]   = FMT(WS.3, "R2,")
    XX<1,1>[112,20]   = FMT(WS.4, "R2,")
    PRINT XX<1,1>
    XX = SPACE(132)
    PRINT XX<1,1>

    GOSUB A.490.DASH
    RETURN
*--------------------------------------------------------
A.450.DEF.TOTALS:
    GOSUB A.490.DASH
    WS.1 = WS.OPEN.BAL.FT.AL + WS.OPEN.BAL.FT.LL
    WS.2 = WS.DR.MOV.FT.AL   + WS.DR.MOV.FT.LL
    WS.3 =  WS.CR.MOV.FT.AL  + WS.CR.MOV.FT.LL
    WS.4 = WS.CLOS.BAL.FT.AL + WS.CLOS.BAL.FT.LL

    XX = SPACE(132)
    XX<1,1>[1,8]   =  ""
    XX<1,1>[9,35]   =  "��� ���� � ���� ����"
    XX<1,1>[46,20]   = FMT(WS.1, "R2,")
    XX<1,1>[68,20]   = FMT(WS.2, "R2,")
    XX<1,1>[90,20]   = FMT(WS.3, "R2,")
    XX<1,1>[112,20]   = FMT(WS.4, "R2,")
    PRINT XX<1,1>


    WS.1 = WS.OPEN.BAL.FT.AF + WS.OPEN.BAL.FT.LF
    WS.2  = WS.DR.MOV.FT.AF +  WS.DR.MOV.FT.LF
    WS.3 =  WS.CR.MOV.FT.AF +  WS.CR.MOV.FT.LF
    WS.4  = WS.CLOS.BAL.FT.AF + WS.CLOS.BAL.FT.LF

    XX = SPACE(132)
    XX<1,1>[1,8]   =  ""
    XX<1,1>[9,35]   =  "��� ���� ����� �����"
    XX<1,1>[46,20]   = FMT(WS.1, "R2,")
    XX<1,1>[68,20]   = FMT(WS.2, "R2,")
    XX<1,1>[90,20]   = FMT(WS.3, "R2,")
    XX<1,1>[112,20]   = FMT(WS.4, "R2,")
    PRINT XX<1,1>
    GOSUB A.490.DASH

    RETURN

*---------------------------------------------
A.460.CONT.DEF:
    GOSUB A.490.DASH
    WS.1 = WS.OPEN.BAL.CONT.DEF
    WS.2  = WS.DR.MOV.CONT.DEF
    WS.3 =  WS.CR.MOV.CONT.DEF
    WS.4  = WS.CLOS.BAL.CONT.DEF

    XX = SPACE(132)
    XX<1,1>[1,8]   =  ""
    XX<1,1>[9,35]   =  "��� ������ ������"
    XX<1,1>[46,20]   = FMT(WS.1, "R2,")
    XX<1,1>[68,20]   = FMT(WS.2, "R2,")
    XX<1,1>[90,20]   = FMT(WS.3, "R2,")
    XX<1,1>[112,20]   = FMT(WS.4, "R2,")
    PRINT XX<1,1>

    GOSUB A.490.DASH
    RETURN
*---------------------------------------------
A.490.DASH:
    XX = SPACE(132)
    XX<1,1>[1,8]   =  ""
    XX<1,1>[9,35]   =  ""
    XX<1,1>[46,20]   = STR('=',20)
    XX<1,1>[68,20]   = STR('=',20)
    XX<1,1>[90,20]   = STR('=',20)
    XX<1,1>[112,20]  = STR('=',20)
    PRINT XX<1,1>
    RETURN
*---------------------------------------------
A.500.PRT.HEAD:
    WS.PAGE.COUNT = WS.PAGE.COUNT + 1
*    DATY = TODAY
    DATY = WS.DAT.HD
*    DATY = WS.TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������":SPACE(20):WS.HEAD.01.B:"  ":T.DAY:SPACE(20):WS.PAGE.COUNT:"   SBD.ALL.GENLED.01"
*    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":SPACE(20):WS.HEAD.02.A:SPACE(10):WS.HEAD.02.B:SPACE(10):WS.HEAD.02.C:SPACE(10):WS.HEAD.02.D:SPACE(10):WS.HEAD.02.E
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
    RETURN
END
