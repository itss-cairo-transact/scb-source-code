* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBD.CUST.TEL.ALL.TEXT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
*Line [ 35 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 36 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    RETURN
*==============================================================
INITIATE:

    OPENSEQ "&SAVEDLISTS&" , "CUSTOMER.TEL.txt" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CUSTOMER.TEL.txt"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CUSTOMER.TEL.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CUSTOMER.TEL.txt CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create CUSTOMER.TEL.txt File IN &SAVEDLISTS&'
        END
    END

    RETURN
*===============================================================
CALLDB:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*************************************************************************
    T.SEL  = "SELECT ":FN.CU: " WITH POSTING.RESTRICT LT '90' AND POSTING.RESTRICT NE '70' AND POSTING.RESTRICT NE '16' AND POSTING.RESTRICT NE '13'"
    T.SEL := " AND SECTOR UNLIKE '3...' AND SECTOR UNLIKE '5...' AND CREDIT.CODE NE 110 AND CREDIT.CODE NE 120"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            FOR J = 1 TO 3
                WS.TEL.NO = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,J>

                WS.TEL.NO.ID = WS.TEL.NO[1,2]
                IF WS.TEL.NO.ID EQ '01' THEN
                    WS.TEL.LEN = LEN(WS.TEL.NO)
                    IF WS.TEL.LEN EQ 11 THEN

                        BB.DATA = WS.TEL.NO

                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END

                    END

                END

            NEXT J

        NEXT I

    END

    RETURN
*==============================================================
END
