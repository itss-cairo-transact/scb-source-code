* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.1610.1.BR
*    PROGRAM     SBM.C.PRT.1610.1.BR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST
*    $INSERT GLOBUS.BP  I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*------------------------------------
*����� ������� ��� 1600
*����� ������� ���
* 1610
*------------------------------------
    FN.CBE = "F.CBE.STATIC.MAST"
    F.CBE  = ""
*    FN.BR = "F.DEPT.ACCT.OFFICER"
*    F.BR = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*---------------------------------
    CALL OPF (FN.CBE,F.CBE)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    REPORT.ID='SBM.C.PRT.001'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.1.LE = "0"

    WS.1.EQV = "0"
    WS.COL.NO = 0
    WS.COL.AMT = 0
    WS.COMN = 0
    WS.COMN1 = 0
    WS.COMN1.TOT = 0
    WS.COMN.TOT = 0
    WS.COMN1.FINAL.TOT  = 0
    WS.COMN.FINAL.TOT = 0

    WS.INDSTRYA = ""
    WS.INDSTRY = ""
    WS.COUNT = 0
    WS.COUNTA = 0
*----------------------------------------
    WS.HD.T  = "������� �������� ������ ��������� ����������"
*-----------------------------------------

    WS.HD.TA = "����� ��� 1610"
*---------------------------------------------------
    WS.HD.T2 = "� ������� �������� ��������"
*---------------------------------------------------
    WS.HD.T2A = "���� ��� "
    WS.PAGE.NO    = "1"
*----------------------------------------------------
    WS.PRG.1 = "SBM.C.PRT.1610.1.BR"
*------------------------------------------------------
*------------------ REPORT HEADERS --------------------
*-----------------------------------------------------
    HEAD.01 = "50000 ����"
*----------------------------------------------------
    HEAD.01.1 = "���� �� 50- 100"
*---------------------------------------------------
    HEAD.01.2 = "���� �� 100-500"
*---------------------------------------------------
    HEAD.01.3 = "���� �� 500 ���"
*--------------------------------------------------
    HEAD.01.4 = "���� �� �����  "
*---------------------------------------------------
    HEAD.01.5 = "���� �� 10 �����"
*---------------------------------------------------
    HEAD.01.6 = "���� �� 25 �����"
*---------------------------------------------------
    HEAD.01.7 = "���� �� 50 �����"
*--------------------------------------------------
    HEAD.01.8 = "�������         "
*--------------------------------------------------
    HEAD.NO = "��� "
    HEAD.NO.1 = "�����"
*--------------------------------------------------
    HEAD.AMT = "����"
*--------------------------------------------------

********    ARRAY1 = ""
******                                 ARRAY
******                                                 1 ������
******
****** 2----   H = HEADER  D = DETAIL   T = TOTAL
******  3 AND 4 ----   RANGE  FROM  TO    FOR INDUSTRY
******  5  TOTAL  OF  FIRST DATA COLUMN   IN LE
******  6  TOTAL  OF  2     DATA COLUMN   IN LE
******* 7  TOTAL  OF  3     DATA COLUMN   IN LE
******* 8  TOTAL  OF  1     DATA COLUMN   IN EQVELENT
********9  TOTAL  OF  2     DATA COLUMN   IN EQVELENT
********10 TOTAL  OF  3     DATA COLUMN   IN EQVELENT
********11 THE ARRAY NO OF  TOTAL OF GROUP TO BE ACUMLATED


    DIM ARRAY1(22,19)

    ARRAY1(1,1) = "������ �����                       "
    ARRAY1(1,2) = "H"

    ARRAY1(2,1) = "����� ���� ��������                "
    ARRAY1(2,2) = "D"
    ARRAY1(2,3) = "0"
    ARRAY1(2,4) = "0"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = "0"
    ARRAY1(2,7) = "0"
    ARRAY1(2,8) = "0"
    ARRAY1(2,9) = "0"
    ARRAY1(2,10) = "0"
    ARRAY1(2,11) = "0"
    ARRAY1(2,12) = "0"
    ARRAY1(2,13) = "0"
    ARRAY1(2,14) = "0"
    ARRAY1(2,15) = "0"
    ARRAY1(2,16) = "0"
    ARRAY1(2,17) = "0"
    ARRAY1(2,18) = "0"
    ARRAY1(2,19) = "5"

    ARRAY1(3,1) = "����� ���� ������� �����            "
    ARRAY1(3,2) = "D"
    ARRAY1(3,3) = "0"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"
    ARRAY1(3,8) = "0"
    ARRAY1(3,9) = "0"
    ARRAY1(3,10) = "0"
    ARRAY1(3,11) = "0"
    ARRAY1(3,12) = "0"
    ARRAY1(3,13) = "0"
    ARRAY1(3,14) = "0"
    ARRAY1(3,15) = "0"
    ARRAY1(3,16) = "0"
    ARRAY1(3,17) = "0"
    ARRAY1(3,18) = "0"
    ARRAY1(3,19) = "5"

    ARRAY1(4,1) = "����� ������ �����                 "
    ARRAY1(4,2) = "D"
    ARRAY1(4,3) = "0"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"
    ARRAY1(4,8) = "0"
    ARRAY1(4,9) = "0"
    ARRAY1(4,10) = "0"
    ARRAY1(4,11) = "0"
    ARRAY1(4,12) = "0"
    ARRAY1(4,13) = "0"
    ARRAY1(4,14) = "0"
    ARRAY1(4,15) = "0"
    ARRAY1(4,16) = "0"
    ARRAY1(4,17) = "0"
    ARRAY1(4,18) = "0"
    ARRAY1(4,19) = "5"

    ARRAY1(5,1) = "����� ������ �����                 "
    ARRAY1(5,2) = "T"
    ARRAY1(5,3) = "0"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = "0"
    ARRAY1(5,7) = "0"
    ARRAY1(5,8) = "0"
    ARRAY1(5,9) = "0"
    ARRAY1(5,10) = "0"
    ARRAY1(5,11) = "0"
    ARRAY1(5,12) = "0"
    ARRAY1(5,13) = "0"
    ARRAY1(5,14) = "0"
    ARRAY1(5,15) = "0"
    ARRAY1(5,16) = "0"
    ARRAY1(5,17) = "0"
    ARRAY1(5,18) = "0"

    ARRAY1(6,1) = "���� ������� �����                  "
    ARRAY1(6,2) = "H"

    ARRAY1(7,1)  = "����� ������� ����� ���������       "
    ARRAY1(7,2)  = "D"
    ARRAY1(7,3) = "0"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"
    ARRAY1(7,6) = "0"
    ARRAY1(7,7) = "0"
    ARRAY1(7,8) = "0"
    ARRAY1(7,9) = "0"
    ARRAY1(7,10) = "0"
    ARRAY1(7,11) = "0"
    ARRAY1(7,12) = "0"
    ARRAY1(7,13) = "0"
    ARRAY1(7,14) = "0"
    ARRAY1(7,15) = "0"
    ARRAY1(7,16) = "0"
    ARRAY1(7,17) = "0"
    ARRAY1(7,18) = "0"
    ARRAY1(7,19) = "13"

    ARRAY1(8,1) = "����� ������� ������                 "
    ARRAY1(8,2) = "D"
    ARRAY1(8,3) = "0"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = "0"
    ARRAY1(8,7) = "0"
    ARRAY1(8,8) = "0"
    ARRAY1(8,9) = "0"
    ARRAY1(8,10) = "0"
    ARRAY1(8,11) = "0"
    ARRAY1(8,12) = "0"
    ARRAY1(8,13) = "0"
    ARRAY1(8,14) = "0"
    ARRAY1(8,15) = "0"
    ARRAY1(8,16) = "0"
    ARRAY1(8,17) = "0"
    ARRAY1(8,18) = "0"
    ARRAY1(8,19) = "13"

    ARRAY1(9,1) = "����� �������                       "
    ARRAY1(9,2) = "D"
    ARRAY1(9,3) = "0"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = "0"
    ARRAY1(9,7) = "0"
    ARRAY1(9,8) = "0"
    ARRAY1(9,9) = "0"
    ARRAY1(9,10) = "0"
    ARRAY1(9,11) = "0"
    ARRAY1(9,12) = "0"
    ARRAY1(9,13) = "0"
    ARRAY1(9,14) = "0"
    ARRAY1(9,15) = "0"
    ARRAY1(9,16) = "0"
    ARRAY1(9,17) = "0"
    ARRAY1(9,18) = "0"
    ARRAY1(9,19) = "13"

    ARRAY1(10,1) = "�������� ���������                 "
    ARRAY1(10,2) = "D"
    ARRAY1(10,3) = "0"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = "0"
    ARRAY1(10,7) = "0"
    ARRAY1(10,8) = "0"
    ARRAY1(10,9) = "0"
    ARRAY1(10,10) = "0"
    ARRAY1(10,11) = "0"
    ARRAY1(10,12) = "0"
    ARRAY1(10,13) = "0"
    ARRAY1(10,14) = "0"
    ARRAY1(10,15) = "0"
    ARRAY1(10,16) = "0"
    ARRAY1(10,17) = "0"
    ARRAY1(10,18) = "0"
    ARRAY1(10,19) = "13"

    ARRAY1(11,1) = "����� �����                        "
    ARRAY1(11,2) = "D"
    ARRAY1(11,3) = "0"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = "0"
    ARRAY1(11,7) = "0"
    ARRAY1(11,8) = "0"
    ARRAY1(11,9) = "0"
    ARRAY1(11,10) = "0"
    ARRAY1(11,11) = "0"
    ARRAY1(11,12) = "0"
    ARRAY1(11,13) = "0"
    ARRAY1(11,14) = "0"
    ARRAY1(11,15) = "0"
    ARRAY1(11,16) = "0"
    ARRAY1(11,17) = "0"
    ARRAY1(11,18) = "0"
    ARRAY1(11,19) = "13"

    ARRAY1(12,1) = "����� ���� ����� ������� �����     "
    ARRAY1(12,2) = "D"
    ARRAY1(12,3) = "0"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = "0"
    ARRAY1(12,7) = "0"
    ARRAY1(12,8) = "0"
    ARRAY1(12,9) = "0"
    ARRAY1(12,10) = "0"
    ARRAY1(12,11) = "0"
    ARRAY1(12,12) = "0"
    ARRAY1(12,13) = "0"
    ARRAY1(12,14) = "0"
    ARRAY1(12,15) = "0"
    ARRAY1(12,16) = "0"
    ARRAY1(12,17) = "0"
    ARRAY1(12,18) = "0"
    ARRAY1(12,19) = "13"

    ARRAY1(13,1) = "����� ���� ������� �����           "
    ARRAY1(13,2) = "T"
    ARRAY1(13,3) = "0"
    ARRAY1(13,4) = "0"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = "0"
    ARRAY1(13,7) = "0"
    ARRAY1(13,8) = "0"
    ARRAY1(13,9) = "0"
    ARRAY1(13,10) = "0"
    ARRAY1(13,11) = "0"
    ARRAY1(13,12) = "0"
    ARRAY1(13,13) = "0"
    ARRAY1(13,14) = "0"
    ARRAY1(13,15) = "0"
    ARRAY1(13,16) = "0"
    ARRAY1(13,17) = "0"
    ARRAY1(13,18) = "0"


    ARRAY1(14,1) = "���� ������� �����                 "
    ARRAY1(14,2) = "D"
    ARRAY1(14,3) = "0"
    ARRAY1(14,4) = "0"
    ARRAY1(14,5) = "0"
    ARRAY1(14,6) = "0"
    ARRAY1(14,7) = "0"
    ARRAY1(14,8) = "0"
    ARRAY1(14,9) = "0"
    ARRAY1(14,10) = "0"
    ARRAY1(14,11) = "0"
    ARRAY1(14,12) = "0"
    ARRAY1(14,13) = "0"
    ARRAY1(14,14) = "0"
    ARRAY1(14,15) = "0"
    ARRAY1(14,16) = "0"
    ARRAY1(14,17) = "0"
    ARRAY1(14,18) = "0"
    ARRAY1(14,19) = "0"

*    ARRAY1(15,1)  = "������ � ������� �������� (1(      "
*    ARRAY1(15,2)  = "D"
*    ARRAY1(15,5) = "0"
*    ARRAY1(15,6) = "0"
*    ARRAY1(15,7) = "0"
*    ARRAY1(15,8) = "0"
*    ARRAY1(15,9) = "0"
*    ARRAY1(15,10) = "0"
*    ARRAY1(15,11) = "18"

*    ARRAY1(16,1)  = "�������� �������                  "
*    ARRAY1(16,2)  = "D"
*    ARRAY1(16,5) = "0"
*    ARRAY1(16,6) = "0"
*    ARRAY1(16,7) = "0"
*    ARRAY1(16,8) = "0"
*    ARRAY1(16,9) = "0"
*    ARRAY1(16,10) = "0"
*    ARRAY1(16,11) = "18"

*    ARRAY1(17,1)  = "����� �����                       "
*    ARRAY1(17,2)  = "D"
*    ARRAY1(17,5) = "0"
*    ARRAY1(17,6) = "0"
*    ARRAY1(17,7) = "0"
*    ARRAY1(17,8) = "0"
*    ARRAY1(17,9) = "0"
*    ARRAY1(17,10) = "0"
*    ARRAY1(17,11) = "18"

*    ARRAY1(18,1)  = "����� ���� ������� �����          "
*    ARRAY1(18,2)  = "T"
*    ARRAY1(18,5) = "0"
*    ARRAY1(18,6) = "0"
*    ARRAY1(18,7) = "0"
*    ARRAY1(18,8) = "0"
*    ARRAY1(18,9) = "0"
*    ARRAY1(18,10) = "0"

    ARRAY1(15,1)  = "������ �������                    "
    ARRAY1(15,2)  = "H"

    ARRAY1(16,1)  = "����� �������                     "
    ARRAY1(16,2)  = "D"
    ARRAY1(16,3) = "0"
    ARRAY1(16,4) = "0"
    ARRAY1(16,5) = "0"
    ARRAY1(16,6) = "0"
    ARRAY1(16,7) = "0"
    ARRAY1(16,8) = "0"
    ARRAY1(16,9) = "0"
    ARRAY1(16,10) = "0"
    ARRAY1(16,11) = "0"
    ARRAY1(16,12) = "0"
    ARRAY1(16,13) = "0"
    ARRAY1(16,14) = "0"
    ARRAY1(16,15) = "0"
    ARRAY1(16,16) = "0"
    ARRAY1(16,17) = "0"
    ARRAY1(16,18) = "0"
    ARRAY1(16,19) = "19"

    ARRAY1(17,1)  = "����� ����� �� ���� �����          "
    ARRAY1(17,2)  = "D"
    ARRAY1(17,3) = "0"
    ARRAY1(17,4) = "0"
    ARRAY1(17,5) = "0"
    ARRAY1(17,6) = "0"
    ARRAY1(17,7) = "0"
    ARRAY1(17,8) = "0"
    ARRAY1(17,9) = "0"
    ARRAY1(17,10) = "0"
    ARRAY1(17,11) = "0"
    ARRAY1(17,12) = "0"
    ARRAY1(17,13) = "0"
    ARRAY1(17,14) = "0"
    ARRAY1(17,15) = "0"
    ARRAY1(17,16) = "0"
    ARRAY1(17,17) = "0"
    ARRAY1(17,18) = "0"
    ARRAY1(17,19) = "19"

    ARRAY1(18,1)  = "����� ������ ���� �� ���          "
    ARRAY1(18,2)  = "D"
    ARRAY1(18,3) = "0"
    ARRAY1(18,4) = "0"
    ARRAY1(18,5) = "0"
    ARRAY1(18,6) = "0"
    ARRAY1(18,7) = "0"
    ARRAY1(18,8) = "0"
    ARRAY1(18,9) = "0"
    ARRAY1(18,10) = "0"
    ARRAY1(18,11) = "0"
    ARRAY1(18,12) = "0"
    ARRAY1(18,13) = "0"
    ARRAY1(18,14) = "0"
    ARRAY1(18,15) = "0"
    ARRAY1(18,16) = "0"
    ARRAY1(18,17) = "0"
    ARRAY1(18,18) = "0"
    ARRAY1(18,19) = "19"

    ARRAY1(19,1)  = "����� ������ �������              "
    ARRAY1(19,2)  = "T"
    ARRAY1(19,3) = "0"
    ARRAY1(19,4) = "0"
    ARRAY1(19,5) = "0"
    ARRAY1(19,6) = "0"
    ARRAY1(19,7) = "0"
    ARRAY1(19,8) = "0"
    ARRAY1(19,9) = "0"
    ARRAY1(19,10) = "0"
    ARRAY1(19,11) = "0"
    ARRAY1(19,12) = "0"
    ARRAY1(19,13) = "0"
    ARRAY1(19,14) = "0"
    ARRAY1(19,15) = "0"
    ARRAY1(19,16) = "0"
    ARRAY1(19,17) = "0"
    ARRAY1(19,18) = "0"


    ARRAY1(20,1)  = "������ �������(�����(             "
    ARRAY1(20,2)  = "D"
    ARRAY1(20,3) = "0"
    ARRAY1(20,4) = "0"
    ARRAY1(20,5) = "0"
    ARRAY1(20,6) = "0"
    ARRAY1(20,7) = "0"
    ARRAY1(20,8) = "0"
    ARRAY1(20,9) = "0"
    ARRAY1(20,10) = "0"
    ARRAY1(20,11) = "0"
    ARRAY1(20,12) = "0"
    ARRAY1(20,13) = "0"
    ARRAY1(20,14) = "0"
    ARRAY1(20,15) = "0"
    ARRAY1(20,16) = "0"
    ARRAY1(20,17) = "0"
    ARRAY1(20,18) = "0"
    ARRAY1(20,19) = "0"

    ARRAY1(21,1)  = "������� ��������                  "
    ARRAY1(21,2)  = "D"
    ARRAY1(21,3) = "0"
    ARRAY1(21,4) = "0"
    ARRAY1(21,5) = "0"
    ARRAY1(21,6) = "0"
    ARRAY1(21,7) = "0"
    ARRAY1(21,8) = "0"
    ARRAY1(21,9) = "0"
    ARRAY1(21,10) = "0"
    ARRAY1(21,11) = "0"
    ARRAY1(21,12) = "0"
    ARRAY1(21,13) = "0"
    ARRAY1(21,14) = "0"
    ARRAY1(21,15) = "0"
    ARRAY1(21,16) = "0"
    ARRAY1(21,17) = "0"
    ARRAY1(21,18) = "0"
    ARRAY1(21,19) = "0"

*    ARRAY1(27,1)  = " ������ ������� ������� ���������  "
*    ARRAY1(27,2)  = "D"
*    ARRAY1(27,5) = "0"
*    ARRAY1(27,6) = "0"
*    ARRAY1(27,7) = "0"
*    ARRAY1(27,8) = "0"
*    ARRAY1(27,9) = "0"
*    ARRAY1(27,10) = "0"
*    ARRAY1(27,11) = "35"

*    ARRAY1(28,1)  = "  ���� ��� ���� ���������          "
*    ARRAY1(28,2)  = "D"
*    ARRAY1(28,5) = "0"
*    ARRAY1(28,6) = "0"
*    ARRAY1(28,7) = "0"
*    ARRAY1(28,8) = "0"
*    ARRAY1(28,9) = "0"
*    ARRAY1(28,10) = "0"
*    ARRAY1(28,11) = "35"

*    ARRAY1(29,1)  = "������ ������� ������             "
*    ARRAY1(29,2)  = "D"
*    ARRAY1(29,5) = "0"
*    ARRAY1(29,6) = "0"
*    ARRAY1(29,7) = "0"
*    ARRAY1(29,8) = "0"
*    ARRAY1(29,9) = "0"
*    ARRAY1(29,10) = "0"
*    ARRAY1(29,11) = "35"

*    ARRAY1(30,1)  = "������ ������� ������� ��� �������"
*    ARRAY1(30,2)  = "D"
*    ARRAY1(30,5) = "0"
*    ARRAY1(30,6) = "0"
*    ARRAY1(30,7) = "0"
*    ARRAY1(30,8) = "0"
*    ARRAY1(30,9) = "0"
*    ARRAY1(30,10) = "0"
*    ARRAY1(30,11) = "35"

*    ARRAY1(31,1)  = "������ ���������                   "
*    ARRAY1(31,2)  = "D"
*    ARRAY1(31,5) = "0"
*    ARRAY1(31,6) = "0"
*    ARRAY1(31,7) = "0"
*    ARRAY1(31,8) = "0"
*    ARRAY1(31,9) = "0"
*    ARRAY1(31,10) = "0"
*    ARRAY1(31,11) = "35"

*    ARRAY1(32,1)  = "������ ��������� ��� �������        "
*    ARRAY1(32,2)  = "D"
*    ARRAY1(32,5) = "0"
*    ARRAY1(32,6) = "0"
*    ARRAY1(32,7) = "0"
*    ARRAY1(32,8) = "0"
*    ARRAY1(32,9) = "0"
*    ARRAY1(32,10) = "0"
*    ARRAY1(32,11) = "35"

*    ARRAY1(33,1)  = "����� ������� (����� ���������)    "
*    ARRAY1(33,2)  = "D"
*    ARRAY1(33,5) = "0"
*    ARRAY1(33,6) = "0"
*    ARRAY1(33,7) = "0"
*    ARRAY1(33,8) = "0"
*    ARRAY1(33,9) = "0"
*    ARRAY1(33,10) = "0"
*    ARRAY1(33,11) = "35"

*    ARRAY1(34,1)  = "����� ������� ������               "
*    ARRAY1(34,2)  = "D"
*    ARRAY1(34,5) = "0"
*    ARRAY1(34,6) = "0"
*    ARRAY1(34,7) = "0"
*    ARRAY1(34,8) = "0"
*    ARRAY1(34,9) = "0"
*    ARRAY1(34,10) = "0"
*    ARRAY1(34,11) = "35"

*    ARRAY1(35,1)  = "����� ������� ��������            "
*    ARRAY1(35,2)  = "T"
*    ARRAY1(35,5) = "0"
*    ARRAY1(35,6) = "0"
*    ARRAY1(35,7) = "0"
*    ARRAY1(35,8) = "0"
*    ARRAY1(35,9) = "0"
*    ARRAY1(35,10) = "0"

    ARRAY1(22,1)  = "��������                 "
    ARRAY1(22,2)  = "T"
    ARRAY1(22,3) = "0"
    ARRAY1(22,4) = "0"
    ARRAY1(22,5) = "0"
    ARRAY1(22,6) = "0"
    ARRAY1(22,7) = "0"
    ARRAY1(22,8) = "0"
    ARRAY1(22,9) = "0"
    ARRAY1(22,10) = "0"
    ARRAY1(22,11) = "0"
    ARRAY1(22,12) = "0"
    ARRAY1(22,13) = "0"
    ARRAY1(22,14) = "0"
    ARRAY1(22,15) = "0"
    ARRAY1(22,16) = "0"
    ARRAY1(22,17) = "0"
    ARRAY1(22,18) = "0"
********************** ********************************
***** SECOND ARRAY TO DEFINE THE RANGE FOR EVERY SECTOR
****** THIRD  ARRAY FOR LOCATION IN ARRAY1
********************** ********************************
****����� ������� ��� ����� �� ����� �����
****������� ��� �� 5000 ���� ������� ��� ��� 3 ����� �� ������
****��� �� ������� ����� ���� ���� ����� ����
****����� ������ ���� ������ ���� ���� �������
****������� �� 5000  ���� ������� ��� ����� �������
    DIM ARRAYRNG(26,2)
*    ARRAY1(2,1) = "����� ���� ��������                "
    ARRAYRNG(1,1) = "110"
    ARRAYRNG(1,2) = "2"

*    ARRAY1(3,1) = "����� ���� ������� �����            "
    ARRAYRNG(2,1) = "120"
    ARRAYRNG(2,2) = "3"

*
*    ARRAY1(4,1) = "����� ������ �����                 "
    ARRAYRNG(3,1) = "130"
    ARRAYRNG(3,2) = "4"
*
*    ARRAY1(7,1)  = "����� ������� ����� ���������       "
    ARRAYRNG(4,1) = "210"
    ARRAYRNG(4,2) = "7"

*
*    ARRAY1(8,1) = "����� ������� ������                 "
    ARRAYRNG(5,1) = "220"
    ARRAYRNG(5,2) = "8"
*
*    ARRAY1(9,1) = "����� �������                       "
    ARRAYRNG(6,1) = "230"
    ARRAYRNG(6,2) = "9"
*
*    ARRAY1(10,1) = "�������� ���������                 "
    ARRAYRNG(7,1) = "240"
    ARRAYRNG(7,2) = "10"
*
*    ARRAY1(11,1) = "����� �����                        "
    ARRAYRNG(8,1) = "250"
    ARRAYRNG(8,2) = "11"
*
*    ARRAY1(12,1) = "����� ���� ����� ������� �����     "
    ARRAYRNG(9,1) = "260"
    ARRAYRNG(9,2) = "12"
*
*
*    ARRAY1(15,1)  = "������ � ������� �������� (1(      "
    ARRAYRNG(10,1) = "4500"
    ARRAYRNG(10,2) = "14"

    ARRAYRNG(11,1) = "8010"
    ARRAYRNG(11,2) = "14"

    ARRAYRNG(12,1) = "8011"
    ARRAYRNG(12,2) = "14"

*
*    ARRAY1(16,1)  = "�������� �������                  "
    ARRAYRNG(13,1) = "4550"
    ARRAYRNG(13,2) = "14"
*
*    ARRAY1(17,1)  = "����� �����                       "
    ARRAYRNG(14,1) = "4600"
    ARRAYRNG(14,2) = "14"
*
*
*    ARRAY1(20,1)  = "����� �������                     "
    ARRAYRNG(15,1) = "4650"
    ARRAYRNG(15,2) = "16"
*
*    ARRAY1(21,1)  = "����� ����� �� ���� �����          "
    ARRAYRNG(16,1) = "4700"
    ARRAYRNG(16,2) = "17"
*
*    ARRAY1(22,1)  = "����� ������ ���� �� ���          "
    ARRAYRNG(17,1) = "4750"
    ARRAYRNG(17,2) = "18"
*
*
*    ARRAY1(24,1)  = "����� ������� ���������            "
*    ARRAYRNG(18,1) = "6000"
*    ARRAYRNG(18,2) = "24"

*
*    ARRAY1(25,1)  = "������ �������(�����(             "
    ARRAYRNG(18,1) = "7000"
    ARRAYRNG(18,2) = "20"

**
*
*    ARRAY1(27,1)  = " ������ ������� ������� ���������  "
    ARRAYRNG(19,1) = "8001"
    ARRAYRNG(19,2) = "21"

*    ARRAY1(28,1)  = "  ���� ��� ���� ���������          "
    ARRAYRNG(20,1) = "8002"
    ARRAYRNG(20,2) = "21"
*
*    ARRAY1(29,1)  = "������ ������� ������             "
    ARRAYRNG(21,1) = "8003"
    ARRAYRNG(21,2) = "21"
*
*    ARRAY1(30,1)  = "������ ������� ������� ��� �������"
    ARRAYRNG(22,1) = "8004"
    ARRAYRNG(22,2) = "21"
*
*    ARRAY1(31,1)  = "������ ���������                   "
    ARRAYRNG(23,1) = "8005"
    ARRAYRNG(23,2) = "21"
*
*    ARRAY1(32,1)  = "������ ��������� ��� �������        "
    ARRAYRNG(24,1) = "8006"
    ARRAYRNG(24,2) = "21"
*
*    ARRAY1(33,1)  = "����� ������� (����� ���������)    "
    ARRAYRNG(25,1) = "8007"
    ARRAYRNG(25,2) = "21"

*
*    ARRAY1(34,1)  = "����� ������� ������               "
    ARRAYRNG(26,1) = "8008"
    ARRAYRNG(26,2) = "21"
********************** ********************************
*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE
    WS.BRX = ID.COMPANY
    GOSUB A.050.GET.ALL.BR
*------------------------------------------START PROCESSING


    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" WITH @ID EQ ":WS.BRX
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        WS.ARRY.RAW = 1
        WS.ARRY.COL = 1
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0
*            GOSUB A.5200.PRT.SPACE.PAGE
            GOSUB A.5000.PRT.HEAD
            GOSUB A.300.PRNT
            GOSUB A.5100.PRT.HEAD
            GOSUB A.400.PRNT
        END
*
A.050.A:
    REPEAT
    RETURN
*------------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.CBE:" WITH CBE.BR EQ ":WS.BR

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS



        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.SCC)
        WS.TMP = R.CBE<C.CBE.BR>
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        IF WS.TMP NE WS.BR THEN
            GOTO AAA
        END
A.100.A:
        WS.INDSTRYA = R.CBE<C.CBE.NEW.SECTOR>
        WS.N.INDST  = R.CBE<C.CBE.NEW.INDUSTRY>
****������ ����� ������ ����� ���� �������
        IF  WS.INDSTRYA EQ 1130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END
        IF  WS.INDSTRYA EQ 2130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END
        IF  WS.INDSTRYA EQ 3130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END
        IF  WS.INDSTRYA EQ 4130 AND  WS.N.INDST EQ 2070 THEN
            WS.INDSTRYA = 8008
        END

        IF WS.INDSTRYA EQ 0 THEN
            GOTO AAA
        END
        WS.INDSTRY = WS.INDSTRYA
        IF WS.INDSTRYA LT 4500   THEN
            WS.INDSTRY  = WS.INDSTRYA[3]
        END
        WS.1.LE = 0

        WS.1.EQV = 0

*****       TEXT = " A..2  ":WS.INDSTRYA:"*":WS.INDSTRY ; CALL REM
        WS.1.LE = R.CBE<C.CBE.FACLTY.LE> + R.CBE<C.CBE.COMRCL.PAPER.LE>
        WS.1.LE = WS.1.LE + R.CBE<C.CBE.CUR.AC.LE.DR>

        WS.1.EQV = R.CBE<C.CBE.FACLTY.EQ> + R.CBE<C.CBE.COMRCL.PAPER.EQ>
        WS.1.EQV = WS.1.EQV + R.CBE<C.CBE.CUR.AC.EQ.DR>

        WS.1.LE = WS.1.LE + WS.1.EQV
        GOSUB A.200.ACUM
*-----------------------------------------------------
AAA:
    REPEAT
    RETURN

A.200.ACUM:
    FOR WSRNG = 1 TO 26

        GOSUB A.205.CHK.INDSTRY

    NEXT WSRNG
    RETURN

A.205.CHK.INDSTRY:
    IF  WS.INDSTRY NE ARRAYRNG(WSRNG,1) THEN

        RETURN
    END

    WS = ARRAYRNG(WSRNG,2)
    WS.FLAG.PRT = 1
    GOSUB A.210.ACUM
    RETURN

****        ARRAY          ������� ��� ��
A.210.ACUM:
    WS.COMN = WS.1.LE * -1

    IF WS.COMN GE 0 AND WS.COMN LE 50000 THEN
        WS.COL.NO = 3
        WS.COL.AMT = 4
    END

    IF WS.COMN GT 50000 AND WS.COMN LE 100000 THEN
        WS.COL.NO = 5
        WS.COL.AMT = 6
    END

    IF WS.COMN GT 100000 AND WS.COMN LE 500000 THEN
        WS.COL.NO = 7
        WS.COL.AMT = 8
    END

    IF WS.COMN GT 500000 AND WS.COMN LE 1000000 THEN
        WS.COL.NO = 9
        WS.COL.AMT = 10
    END

    IF WS.COMN GT 1000000 AND WS.COMN LE 10000000 THEN
        WS.COL.NO = 11
        WS.COL.AMT = 12
    END

    IF WS.COMN GT 10000000 AND WS.COMN LE 25000000 THEN
        WS.COL.NO = 13
        WS.COL.AMT = 14
    END

    IF WS.COMN GT 25000000 AND WS.COMN LE 50000000 THEN
        WS.COL.NO = 15
        WS.COL.AMT = 16
    END

    IF WS.COMN GT 50000000 THEN
        WS.COL.NO = 17
        WS.COL.AMT = 18
    END

    WS.COMN = WS.1.LE / 1000
    ARRAY1(WS,WS.COL.NO) = ARRAY1(WS,WS.COL.NO) + 1
    ARRAY1(WS,WS.COL.AMT) = ARRAY1(WS,WS.COL.AMT) + WS.COMN

    WS.T  = ARRAY1(WS,19)
    IF WS.T GT 0  THEN
        ARRAY1(WS.T,WS.COL.NO) = ARRAY1(WS.T,WS.COL.NO) + 1
        ARRAY1(WS.T,WS.COL.AMT) = ARRAY1(WS.T,WS.COL.AMT) + WS.COMN
    END
    ARRAY1(22,WS.COL.NO) = ARRAY1(22,WS.COL.NO) + 1
    ARRAY1(22,WS.COL.AMT) = ARRAY1(22,WS.COL.AMT) + WS.COMN
    RETURN
*****                                     ARRAY   ������� ��

A.300.PRNT:
    FOR I = 1 TO 22
        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.310.PRT.HD
        END

        IF WS.H.D.T = "T" THEN
            GOSUB A.320.PRT.TOT
        END

        IF WS.H.D.T = "D" THEN
            GOSUB A.330.PRT.DTAL
        END

    NEXT I
    RETURN
A.310.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
****       XX<1,1>[1,35]   = "--------------------------"
    XX<1,1>[1,35]   =  STR('-',35)
    PRINT XX<1,1>
    RETURN

A.320.PRT.TOT:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)

    XX<1,1>[37,5]   = ARRAY1(I,3)
    XX<1,1>[45,7]   = FMT(ARRAY1(I,4), "R0,")

    XX<1,1>[55,5]   = ARRAY1(I,5)
    XX<1,1>[63,7]   = FMT(ARRAY1(I,6), "R0,")

    XX<1,1>[73,5]   = ARRAY1(I,7)
    XX<1,1>[81,7]   = FMT(ARRAY1(I,8), "R0,")

    XX<1,1>[91,5]   = ARRAY1(I,9)
    XX<1,1>[99,7]   = FMT(ARRAY1(I,10), "R0,")

    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,132]   = STR('-',132)
    PRINT XX<1,1>

    RETURN
A.330.PRT.DTAL:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[37,5]   = ARRAY1(I,3)
    XX<1,1>[45,7]   = FMT(ARRAY1(I,4), "R0,")

    XX<1,1>[55,5]   = ARRAY1(I,5)
    XX<1,1>[63,7]   = FMT(ARRAY1(I,6), "R0,")

    XX<1,1>[73,5]   = ARRAY1(I,7)
    XX<1,1>[81,7]   = FMT(ARRAY1(I,8), "R0,")

    XX<1,1>[91,5]   = ARRAY1(I,9)
    XX<1,1>[99,7]   = FMT(ARRAY1(I,10), "R0,")
    PRINT XX<1,1>
    RETURN

A.400.PRNT:
    FOR I = 1 TO 22
        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.410.PRT.HD
        END

        IF WS.H.D.T = "T" THEN
            GOSUB A.420.PRT.TOT
        END

        IF WS.H.D.T = "D" THEN
            GOSUB A.430.PRT.DTAL
        END

    NEXT I
    RETURN
A.410.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
****       XX<1,1>[1,35]   = "--------------------------"
    XX<1,1>[1,35]   =  STR('-',35)
    PRINT XX<1,1>
    RETURN

A.420.PRT.TOT:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)

    XX<1,1>[37,5]   = ARRAY1(I,11)
    XX<1,1>[45,7]   = FMT(ARRAY1(I,12), "R0,")

    XX<1,1>[55,5]   = ARRAY1(I,13)
    XX<1,1>[63,7]   = FMT(ARRAY1(I,14), "R0,")

    XX<1,1>[73,5]   = ARRAY1(I,15)
    XX<1,1>[81,7]   = FMT(ARRAY1(I,16), "R0,")

    XX<1,1>[91,5]   = ARRAY1(I,17)
    XX<1,1>[99,8]   = FMT(ARRAY1(I,18), "R0,")

    WS.COMN = ARRAY1(I,4) + ARRAY1(I,6) + ARRAY1(I,8) + ARRAY1(I,10)
    WS.COMN = WS.COMN + ARRAY1(I,12) + ARRAY1(I,14) + ARRAY1(I,16) + ARRAY1(I,18)

    WS.COMN1    = ARRAY1(I,3) + ARRAY1(I,5) + ARRAY1(I,7) + ARRAY1(I,9)
    WS.COMN1    = WS.COMN1 + ARRAY1(I,11) + ARRAY1(I,13) + ARRAY1(I,15) + ARRAY1(I,17)
    XX<1,1>[110,5]   = WS.COMN1
    XX<1,1>[118,8]   = FMT(WS.COMN, "R0,")
*?    XX<1,1>[110,5]   = WS.COMN1.TOT
*?    XX<1,1>[118,8]   = FMT(WS.COMN.TOT, "R0,")

*?    IF I = 23 THEN
*?        XX<1,1>[110,5]   = WS.COMN1.FINAL.TOT
*?        XX<1,1>[118,8]   = FMT(WS.COMN.FINAL.TOT, "R0,")
*?    END
    WS.COMN1.TOT = 0
    WS.COMN.TOT = 0

    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,132]   = STR('-',132)
    PRINT XX<1,1>
    ARRAY1(I,3) = 0
    ARRAY1(I,4) = 0
    ARRAY1(I,5) = 0
    ARRAY1(I,6) = 0
    ARRAY1(I,7) = 0
    ARRAY1(I,8) = 0
    ARRAY1(I,9) = 0
    ARRAY1(I,10) = 0
    ARRAY1(I,11) = 0
    ARRAY1(I,12) = 0
    ARRAY1(I,13) = 0
    ARRAY1(I,14) = 0
    ARRAY1(I,15) = 0
    ARRAY1(I,16) = 0
    ARRAY1(I,17) = 0
    ARRAY1(I,18) = 0
    RETURN
A.430.PRT.DTAL:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[37,5]   = ARRAY1(I,11)
    XX<1,1>[45,7]   = FMT(ARRAY1(I,12), "R0,")

    XX<1,1>[55,5]   = ARRAY1(I,13)
    XX<1,1>[63,7]   = FMT(ARRAY1(I,14), "R0,")

    XX<1,1>[73,5]   = ARRAY1(I,15)
    XX<1,1>[81,7]   = FMT(ARRAY1(I,16), "R0,")

    XX<1,1>[91,5]   = ARRAY1(I,17)
    XX<1,1>[99,8]   = FMT(ARRAY1(I,18), "R0,")

    WS.COMN = ARRAY1(I,4) + ARRAY1(I,6) + ARRAY1(I,8) + ARRAY1(I,10)
    WS.COMN = WS.COMN + ARRAY1(I,12) + ARRAY1(I,14) + ARRAY1(I,16) + ARRAY1(I,18)

    WS.COMN1    = ARRAY1(I,3) + ARRAY1(I,5) + ARRAY1(I,7) + ARRAY1(I,9)
    WS.COMN1    = WS.COMN1 + ARRAY1(I,11) + ARRAY1(I,13) + ARRAY1(I,15) + ARRAY1(I,17)

*?   WS.COMN1.TOT = WS.COMN1.TOT + WS.COMN1
*?   WS.COMN1.FINAL.TOT = WS.COMN1.FINAL.TOT + WS.COMN1

*?   WS.COMN.TOT = WS.COMN.TOT + WS.COMN
*?   WS.COMN.FINAL.TOT = WS.COMN.FINAL.TOT + WS.COMN

    XX<1,1>[110,5]   = WS.COMN1
    XX<1,1>[118,8]   = FMT(WS.COMN, "R0,")
    PRINT XX<1,1>
    ARRAY1(I,3) = 0
    ARRAY1(I,4) = 0
    ARRAY1(I,5) = 0
    ARRAY1(I,6) = 0
    ARRAY1(I,7) = 0
    ARRAY1(I,8) = 0
    ARRAY1(I,9) = 0
    ARRAY1(I,10) = 0
    ARRAY1(I,11) = 0
    ARRAY1(I,12) = 0
    ARRAY1(I,13) = 0
    ARRAY1(I,14) = 0
    ARRAY1(I,15) = 0
    ARRAY1(I,16) = 0
    ARRAY1(I,17) = 0
    ARRAY1(I,18) = 0
    RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

*    CALL F.READ(FN.BR,WS.BR.H,R.BR,F.BR,MSG.BR)
*    WS.BR.NAME = R.BR<EB.DAO.NAME>
***    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
***    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ "
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(34):WS.HD.TA
    PR.HD :="'L'":SPACE(40):WS.HD.T2:SPACE(20):WS.HD.T2A:SPACE(3):WS.PAGE.NO
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(34):HEAD.01:SPACE(8):HEAD.01.1:SPACE(3):HEAD.01.2:SPACE(3):HEAD.01.3
    PR.HD :="'L'":SPACE(34):HEAD.NO:SPACE(6):HEAD.AMT:SPACE(6):HEAD.NO:SPACE(4):HEAD.AMT:SPACE(6):HEAD.NO:SPACE(4):HEAD.AMT:SPACE(7):HEAD.NO:SPACE(3):HEAD.AMT
    PR.HD :="'L'":SPACE(34):HEAD.NO.1:SPACE(15):HEAD.NO.1:SPACE(13):HEAD.NO.1:SPACE(13):HEAD.NO.1
****    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD
    PRINT
    RETURN
A.5100.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

*    CALL F.READ(FN.BR,WS.BR.H,R.BR,F.BR,MSG.BR)
*    WS.BR.NAME = R.BR<EB.DAO.NAME>

***    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
***    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ "
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(34):WS.HD.TA
    WS.PAGE.NO = WS.PAGE.NO + 1
    PR.HD :="'L'":SPACE(40):WS.HD.T2:SPACE(20):WS.HD.T2A:SPACE(3):WS.PAGE.NO
    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(34):HEAD.01.4:SPACE(5):HEAD.01.5:SPACE(2):HEAD.01.6:SPACE(2):HEAD.01.7:SPACE(3):HEAD.01.8
    PR.HD :="'L'":SPACE(36):HEAD.NO:SPACE(3):HEAD.AMT:SPACE(6):HEAD.NO:SPACE(5):HEAD.AMT:SPACE(6):HEAD.NO:SPACE(3):HEAD.AMT:SPACE(7):HEAD.NO:SPACE(3):HEAD.AMT
    PR.HD :="'L'":SPACE(36):HEAD.NO.1:SPACE(9):HEAD.NO.1:SPACE(11):HEAD.NO.1:SPACE(10):HEAD.NO.1
****    PR.HD :="'L'":SPACE(50):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
*    PRINT
    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
A.5200.PRT.SPACE.PAGE:
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
