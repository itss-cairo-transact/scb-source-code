* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>614</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE   SBD.MATCH.DEPOSIT
***    PROGRAM  SBD.MATCH.DEPOSIT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------
    GOSUB PROCESS
    GOSUB TOTAL.REPORT
*-------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
*    REPORT.ID='P.FUNCTION'
    REPORT.ID='SBD.MATCH.DEPOSIT'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    PROGRAM.NAME = 'SBD.MATCH.DEPOSIT'

*****ADDED BY MAHMOUD FOR E-MAIL PORPOSE************
*     PRINT "mahmoud.elhawary@scbank.com.eg"
****************************************************

    T.AGL.BAL = 0
    T.CER.BAL = 0
    T.SAV.BAL = 0
    T.CUR.BAL = 0
    T.OTR.BAL = 0


    T.AGL.BALX = 0
    T.CER.BALX = 0
    T.SAV.BALX = 0
    T.CUR.BALX = 0
    T.OTR.BALX = 0


    T.AGL.BALY = 0
    T.CER.BALY = 0
    T.SAV.BALY = 0
    T.CUR.BALY = 0
    T.OTR.BALY = 0

    T.TOT.BALY = 0

    TXT1 = "�����"
    TXT2 = "�����"
    TXT3 = "�����"
    TXT4 = "������"
    H.BRANCH  = "�� ���� �����"


*    HEAD.A1 = "������ ������ ������ �� ����� ������� ���������"
    HEAD.A1 = "���� �������� ������� �� ����� ������� ���������"

    HEAD.B1 = "������� ���������"
    HEAD.B2 = "���� ������"
*    HEAD.B2 = "���� ����� ���"


    S.HEAD1 = "�������� �������"
    S.HEAD2 = "������ �������"
    S.HEAD3 = "������ ��������"
    S.HEAD4 = "������� �������"
    S.HEAD5 = "������ ��������"

*----------------------------------
    FN.COMP = "FBNK.COMPANY"
    F.COMP  = ""
    R.COMP = ""
    Y.COMP.ID = ""

*----------------------------------

    FN.LINE = 'F.RE.STAT.LINE.BAL'
    F.LINE = ''

    CALL OPF(FN.LINE,F.LINE)

*----------------------------------
    FN.DATE = "F.DATES"
    F.DATE  = ""
    R.DATE = ""
    Y.DATE.ID = ""

    CALL OPF (FN.DATE,F.DATE)
*----------------------------------
    Y.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,Y.DATE.ID,R.DATE,F.DATE,ERR.DATE)
    DAT = R.DATE<EB.DAT.LAST.PERIOD.END>
    L.WS.DATE = R.DATE<EB.DAT.LAST.WORKING.DAY>
    SYS.DATE = TODAY


* -------  HARES M. MAHMOUD 22 MAR, 2010 ----------------
*    X.DAT = 20100307
*    DAT = X.DAT
*    CALL CDT('',DAT,'-1C')
*    L.WS.DATE = DAT
*    SYS.DATE = X.DAT
* -------  HARES M. MAHMOUD 22 MAR, 2010 ----------------


    P.DATE   = FMT(SYS.DATE,"####/##/##")
    P.WS.DATE = FMT(L.WS.DATE,"####/##/##")



    RETURN
*========================================================================
PROCESS:

    XCURR = 'EGP'

    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010077' AND @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SEL.COMP,ER.MSG3)



    FOR I = 1 TO SEL.COMP


        COMP = KEY.LIST3<I>

*********************************************************************************************************
        IDD1 = 'GENLED-0510':'-':XCURR:'-':DAT:'*':COMP
        CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
        CUR.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        CUR.BAL1X = R.LINE<RE.SLB.OPEN.BAL>

        IDD1 = 'GENLED-0550':'-':XCURR:'-':DAT:'*':COMP
        CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
        CUR.ADD1  = R.LINE<RE.SLB.CLOSING.BAL>
        CUR.ADD1X = R.LINE<RE.SLB.OPEN.BAL>

        CUR.BAL1  = CUR.BAL1  + CUR.ADD1
        CUR.BAL1X = CUR.BAL1X + CUR.ADD1X
*---------------
        IF COMP EQ 'EG0010011' THEN
            IDD2 = 'GENLED-0525':'-':XCURR:'-':DAT:'*':COMP
        END

        IF COMP NE 'EG0010011' THEN
            IDD2 = 'GENLED-0520':'-':XCURR:'-':DAT:'*':COMP
        END

        CALL F.READ(FN.LINE,IDD2,R.LINE,F.LINE,E)
        SAV.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        SAV.BAL1X = R.LINE<RE.SLB.OPEN.BAL>

*---------------
        IF COMP EQ 'EG0010011' THEN
            IDD3 = 'GENLED-0527':'-':XCURR:'-':DAT:'*':COMP
        END

        IF COMP NE 'EG0010011' THEN
            IDD3 = 'GENLED-0530':'-':XCURR:'-':DAT:'*':COMP
        END

        CALL F.READ(FN.LINE,IDD3,R.LINE,F.LINE,E)
        AGL.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        AGL.BAL1X = R.LINE<RE.SLB.OPEN.BAL>
*---------------

        IDD4 = 'GENLED-0570':'-':XCURR:'-':DAT:'*':COMP
        CALL F.READ(FN.LINE,IDD4,R.LINE,F.LINE,E)
        CER.BAL1  = R.LINE<RE.SLB.CLOSING.BAL>
        CER.BAL1X = R.LINE<RE.SLB.OPEN.BAL>



** ---------------------------------------------------
**   ����� ����
**      IDD9 = 'BAL-0090':'-':XCURR:'-':DAT:'*':COMP
**      CALL F.READ(FN.LINE,IDD9,R.LINE,F.LINE,E)
**      CLOSE.BAL9  = R.LINE<RE.SLB.CLOSING.BAL>
**      CLOSE.BAL9X = R.LINE<RE.SLB.OPEN.BAL>
**   ����� ����
**      IDD10 = 'BAL-0100':'-':XCURR:'-':DAT:'*':COMP
**      CALL F.READ(FN.LINE,IDD10,R.LINE,F.LINE,E)
**      CLOSE.BAL10  = R.LINE<RE.SLB.CLOSING.BAL>
**      CLOSE.BAL10X = R.LINE<RE.SLB.OPEN.BAL>

        OTR.BAL1  = 0
        OTR.BAL1X = 0
        OTR.BAL1Y = 0

*********************************************************************************************************
        AGL.BAL = FMT((AGL.BAL1)/1000,"L0")
        CER.BAL = FMT((CER.BAL1)/1000,"L0")
        SAV.BAL = FMT((SAV.BAL1)/1000,"L0")
        CUR.BAL = FMT((CUR.BAL1)/1000,"L0")
        OTR.BAL = FMT((OTR.BAL1)/1000,"L0")
**************************************************
        AGL.BALX = FMT((AGL.BAL1X)/1000,"L0")
        CER.BALX = FMT((CER.BAL1X)/1000,"L0")
        SAV.BALX = FMT((SAV.BAL1X)/1000,"L0")
        CUR.BALX = FMT((CUR.BAL1X)/1000,"L0")
        OTR.BALX = FMT((OTR.BAL1X)/1000,"L0")

*********************************************************************************************************
        AGL.BALY = AGL.BAL - AGL.BALX
        CER.BALY = CER.BAL - CER.BALX
        SAV.BALY = SAV.BAL - SAV.BALX
        CUR.BALY = CUR.BAL - CUR.BALX
        OTR.BALY = OTR.BAL - OTR.BALX
        TOT.BALY = AGL.BALY + CER.BALY + SAV.BALY + CUR.BALY + OTR.BALY
*********************************************************************************************************
        T.AGL.BAL += AGL.BAL
        T.CER.BAL += CER.BAL
        T.SAV.BAL += SAV.BAL
        T.CUR.BAL += CUR.BAL
        T.OTR.BAL += OTR.BAL


        T.AGL.BALX += AGL.BALX
        T.CER.BALX += CER.BALX
        T.SAV.BALX += SAV.BALX
        T.CUR.BALX += CUR.BALX
        T.OTR.BALX += OTR.BALX


        T.AGL.BALY += AGL.BALY
        T.CER.BALY += CER.BALY
        T.SAV.BALY += SAV.BALY
        T.CUR.BALY += CUR.BALY
        T.OTR.BALY += OTR.BALY

        T.TOT.BALY += TOT.BALY



*********************************************************************************************************


*Line [ 268 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        YYBRN  = BRANCH


*------------------------------------------------

        XX= SPACE(140)
        XX<1,1>[1,12]   = YYBRN

        XX<1,1>[14,1]   = '|'
**        XX<1,1>[15,10]  = CUR.BALX
        XX<1,1>[15,10]  = FMT(CUR.BALX,"L0,")
**        XX<1,1>[26,10]  = CUR.BAL
        XX<1,1>[26,10]  = FMT(CUR.BAL,"L0,")
**        XX<1,1>[36,8]   = CUR.BALY
        XX<1,1>[36,8]   = FMT(CUR.BALY,"L0,")

        XX<1,1>[43,1]   = '|'
**        XX<1,1>[44,10]  = SAV.BALX
        XX<1,1>[44,10]  = FMT(SAV.BALX,"L0,")
**        XX<1,1>[55,10]  = SAV.BAL
        XX<1,1>[55,10]  = FMT(SAV.BAL,"L0,")
**        XX<1,1>[65,8]   = SAV.BALY
        XX<1,1>[65,8]   = FMT(SAV.BALY,"L0,")

        XX<1,1>[72,1]   = '|'
**        XX<1,1>[73,10]  = CER.BALX
        XX<1,1>[73,10]  = FMT(CER.BALX,"L0,")
**        XX<1,1>[84,10]  = CER.BAL
        XX<1,1>[84,10]  = FMT(CER.BAL,"L0,")
**        XX<1,1>[94,8]   = CER.BALY
        XX<1,1>[94,8]   = FMT(CER.BALY,"L0,")

        XX<1,1>[101,1]   = '|'
**        XX<1,1>[102,10]  = AGL.BALX
        XX<1,1>[102,10]  = FMT(AGL.BALX,"L0,")
**        XX<1,1>[113,10]  = AGL.BAL
        XX<1,1>[113,10]  = FMT(AGL.BAL,"L0,")
**        XX<1,1>[123,8]   = AGL.BALY
        XX<1,1>[123,8]   = FMT(AGL.BALY,"L0,")

        XX<1,1>[130,1]   = '|'
**        XX<1,1>[131,8]   = TOT.BALY
        XX<1,1>[131,8]   = FMT(TOT.BALY,"L0,")

        PRINT XX<1,1>

*-------------------------------------------------
        XX= SPACE(140)


        XX<1,1>[1,13]    = '_____________'
        XX<1,1>[14,1]    = '|'
        XX<1,1>[15,10]   = '__________'
        XX<1,1>[25,10]   = '__________'
        XX<1,1>[35,8]    = '________'
        XX<1,1>[43,1]    = '|'
        XX<1,1>[44,10]   = '__________'
        XX<1,1>[54,10]   = '__________'
        XX<1,1>[64,8]    = '________'
        XX<1,1>[72,1]    = '|'
        XX<1,1>[73,10]   = '__________'
        XX<1,1>[83,10]   = '__________'
        XX<1,1>[93,8]    = '________'
        XX<1,1>[101,1]   = '|'
        XX<1,1>[102,10]  = '__________'
        XX<1,1>[112,10]  = '__________'
        XX<1,1>[122,8]   = '________'
        XX<1,1>[130,1]   = '|'
        XX<1,1>[131,8]   = '________'

        PRINT XX<1,1>

    NEXT I

*********************************************************************************************************
*    PRINT STR('=',140)

    XX= SPACE(140)

    XX<1,1>[1,13]  = "������ �����"
    XX<1,1>[14,1]   = '|'
**    XX<1,1>[15,10]  = T.CUR.BALX
    XX<1,1>[15,10]  = FMT(T.CUR.BALX,"L0,")
**    XX<1,1>[26,10]  = T.CUR.BAL
    XX<1,1>[26,10]  = FMT(T.CUR.BAL,"L0,")
**    XX<1,1>[36,8]   = T.CUR.BALY
    XX<1,1>[36,8]   = FMT(T.CUR.BALY,"L0,")

    XX<1,1>[43,1]   = '|'
**    XX<1,1>[44,10]  = T.SAV.BALX
    XX<1,1>[44,10]  = FMT(T.SAV.BALX,"L0,")
**    XX<1,1>[55,10]  = T.SAV.BAL
    XX<1,1>[55,10]  = FMT(T.SAV.BAL,"L0,")
**    XX<1,1>[66,8]   = T.SAV.BALY
    XX<1,1>[66,8]   = FMT(T.SAV.BALY,"L0,")

    XX<1,1>[72,1]   = '|'
**    XX<1,1>[73,10]  = T.CER.BALX
    XX<1,1>[73,10]  = FMT(T.CER.BALX,"L0,")
**    XX<1,1>[84,10]  = T.CER.BAL
    XX<1,1>[84,10]  = FMT(T.CER.BAL,"L0,")
**    XX<1,1>[94,8]   = T.CER.BALY
    XX<1,1>[94,8]   = FMT(T.CER.BALY,"L0,")

    XX<1,1>[101,1]   = '|'
**    XX<1,1>[102,10]  = T.AGL.BALX
    XX<1,1>[102,10]  = FMT(T.AGL.BALX,"L0,")
**    XX<1,1>[113,10]  = T.AGL.BAL
    XX<1,1>[113,10]  = FMT(T.AGL.BAL,"L0,")
**    XX<1,1>[123,8]   = T.AGL.BALY
    XX<1,1>[123,8]   = FMT(T.AGL.BALY,"L0,")

    XX<1,1>[130,1]   = '|'
**    XX<1,1>[131,8]   = T.TOT.BALY
    XX<1,1>[131,8]   = FMT(T.TOT.BALY,"L0,")

    PRINT XX<1,1>
    PRINT STR('_',138)
*-------------------------------------------------

    RETURN
*===============================================================
*********************************************************************************************************
TOTAL.REPORT:

    TT.BALX = T.CUR.BALX + T.SAV.BALX + T.CER.BALX + T.AGL.BALX
    TT.BAL  = T.CUR.BAL  + T.SAV.BAL  + T.CER.BAL  + T.AGL.BAL
    TT.BALY = T.CUR.BALY + T.SAV.BALY + T.CER.BALY + T.AGL.BALY

*-----------------------------------------------------
    XX= SPACE(140)
    PRINT XX<1,1>
    PRINT XX<1,1>
    PRINT XX<1,1>
    PRINT XX<1,1>
    PRINT XX<1,1>
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[30,20]  = '____________________'
    XX<1,1>[50,20]  = '____________________'
    XX<1,1>[70,20]  = '____________________'
    XX<1,1>[90,15]  = '_______________'
    PRINT XX<1,1>
*-----------------------------------------------------
    XX= SPACE(140)

    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = "������ ��������"
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = "����������"
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = "�����������"
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = "�����������"
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD1
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.CUR.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.CUR.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.CUR.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD2
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.SAV.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.SAV.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.SAV.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD3
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.CER.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.CER.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.CER.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD4
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.AGL.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.AGL.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.AGL.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD5
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(TT.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(TT.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(TT.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------


    RETURN
*********************************************************************************************************
R.MOVE.LINE:

    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = '___________________'
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = '___________________'
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = '___________________'
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = '______________'
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>

    RETURN
*********************************************************************************************************
PRINT.HEAD:
*---------

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(85):"��� :" :H.BRANCH
    PR.HD :="'L'":SPACE(1):" ������� : ":P.DATE:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):PROGRAM.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):HEAD.A1
    PR.HD :="'L'":SPACE(55):HEAD.B1:SPACE(2):HEAD.B2
    PR.HD :="'L'":SPACE(60):"�� ����� : ":P.WS.DATE
    PR.HD :="'L'":SPACE(50):STR('_',45)
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',138)


*    PR.HD :="'L'":SPACE(21):"�������� �������":SPACE(14):"������ �������":SPACE (14):"������ ��������":SPACE(14):"������� ����":SPACE(11):"�":"������"
*    PR.HD :="'L'":SPACE(15):"�":SPACE(5):"�������� �������":SPACE(14):"������ �������":SPACE (14):"������ ��������":SPACE(14):"������� ����":SPACE(11):"�":"������"
*    PR.HD :="'L'":SPACE(15):"|":SPACE(5):"�������� �������":SPACE(8):"|":SPACE(5):"������ �������":SPACE (8):"|":SPACE(5):"������ ��������":SPACE(8):"|":SPACE(5):"������� ����":SPACE(11):"�":"������"
    PR.HD :="'L'":SPACE(13):"|":SPACE(5):"�������� �������":SPACE(7):"|":SPACE(6):"������ �������":SPACE (8):"|":SPACE(5):"������ ��������":SPACE(8):"|":SPACE(5):"������� ����":SPACE(11):"|":"������"


*    PR.HD :="'L'":"��� �����":SPACE(6):"|":STR('-',28):"|":STR('-',28):"|":STR('-',28):"|":STR('-',28):"|"
    PR.HD :="'L'":"��� �����":SPACE(4):"|":STR('_',28):"|":STR('_',28):"|":STR('_',28):"|":STR('_',28):"|"



    PR.HD :="'L'":SPACE(13):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT4



    PR.HD :="'L'":STR('_',138)



    HEADING PR.HD

    RETURN
*==============================================================
END
