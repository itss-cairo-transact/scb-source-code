* @ValidationCode : MjoxNDIwNjEzNTc6Q3AxMjUyOjE2NDA4MjgxNjExMzM6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:36:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>763</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.MLAD.PROG.021
***    PROGRAM SBD.MLAD.PROG.021

***                            ����� / ���� �����
***                            �������� ������ ��� �������

*   ------------------------------
    $INSERT    I_COMMON
    $INSERT    I_EQUATE
    $INSERT    I_F.USER
    $INSERT    I_F.CATEGORY
    $INSERT    I_F.CURRENCY
*    $INCLUDE TEMENOS.BP I_F.CATEG.MAS.D
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEG.MAS.D.AL.NEW
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MLAD.FILE.020
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MLAD.FILE.PARAM
    $INSERT  I_F.COMPANY
    $INSERT  I_USER.ENV.COMMON
*-------------------------------------------------------------------------


    GOSUB INIT



    GOSUB OPENFILES


    GOSUB READ.MLAD.FILE.PARAM


    GOSUB LOAN.ROUTINE




*-------------------------------

RETURN
*-------------------------------------------------------------------------
INIT:


    PROGRAM.ID = "SBD.MLAD.PROG.021"


    FN.MLADPARAM = "F.SCB.MLAD.FILE.PARAM"
    F.MLADPARAM  = ""
    R.MLADPARAM  = ""
    Y.MLADPARAM.ID  = ""

    FN.MLAD020 = "F.SCB.MLAD.FILE.020"
    F.MLAD020  = ""
    R.MLAD020  = ""
    Y.MLAD020.ID  = ""


    FN.CATMAS = "F.CATEG.MAS.D.AL.NEW"
    F.CATMAS  = ""
    R.CATMAS = ""
    Y.CATMAS.ID = ""


    WS.PERIOD  = ''

    TWS.AMOUNT = 0
*---------------------------------------
    SYS.DATE = TODAY

    WRK.DATE = SYS.DATE

*---------------------------------------

    WS.TYPE    = 0
    WS.LINE    = 0
    WS.GENLED  = 0
    WS.CATEGFR = 0
    WS.CATEGTO = 0
    WS.MATDAT  = 0
    WS.DAY     = 0
    WS.WEEK    = 0
    WS.MONTH   = 0
    WS.3MONTH  = 0
    WS.6MONTH  = 0
    WS.YEAR    = 0
    WS.3YEAR   = 0
    WS.MORE    = 0


    WS.LOAN.AMOUNT = 0
    GOSUB RTN.MOVE.ZEROS
    GOSUB RTN.MOVE.ZEROS.2


***    LOAN.GROUP.1 = '0019 0020 0021 0030 0040 0050 0051 0052 0062 0230 0231 0250 0251 0252 0254 0262'
***    LOAN.GROUP.2 = '0060 0260'
***LOAN.GROUP.1 ='0230 0240 0245 0250 0251 0255 0260 0270 0271 0280 0290 0300 0301 0310 0311 0315 0320 0330 0340 0350 0351 0352 0353 0354'
    LOAN.GROUP.1 ='240 245 250 251 255 260 280 290 310 315 320 330 340 350 351 352 353 354 401'
    LOAN.GROUP.2 = '400'
RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLADPARAM,F.MLADPARAM)
    CALL OPF(FN.MLAD020,F.MLAD020)
    CALL OPF(FN.CATMAS,F.CATMAS)



RETURN
*--------------------------------------------------------------------------
READ.MLAD.FILE.PARAM:

    SEL.MLPARAM  = "SELECT ":FN.MLADPARAM:" BY MLAD.RECORD.NO"

    CALL EB.READLIST(SEL.MLPARAM,SEL.LIST.MLPARAM,'',NO.MLPARAM,ERR.MLPARAM)


    LOOP
        REMOVE Y.MLADPARAM.ID FROM SEL.LIST.MLPARAM SETTING POS.MLPARAM
    WHILE Y.MLADPARAM.ID:POS.MLPARAM
        CALL F.READ(FN.MLADPARAM,Y.MLADPARAM.ID,R.MLADPARAM,F.MLADPARAM,ERR.MLPARAM)

****************
        GOSUB RTN.MOVE.ZEROS.2
****************

        WS.TYPE    = R.MLADPARAM<MLPAR.MLAD.TYPE>
        WS.LINE    = R.MLADPARAM<MLPAR.MLAD.LINE.NO>
        WS.GENLED  = R.MLADPARAM<MLPAR.GENLED.LINE.NO>
        WS.CATEGFR = R.MLADPARAM<MLPAR.CATEGORY.FROM>
        WS.CATEGTO = R.MLADPARAM<MLPAR.CATEGORY.TO>
        WS.MATDAT  = R.MLADPARAM<MLPAR.MATURITY.DATE>
        WS.DAY     = R.MLADPARAM<MLPAR.NEXT.DAY>
        WS.WEEK    = R.MLADPARAM<MLPAR.NEXT.WEEK>
        WS.MONTH   = R.MLADPARAM<MLPAR.NEXT.MONTH>
        WS.3MONTH  = R.MLADPARAM<MLPAR.NEXT.3MONTH>
        WS.6MONTH  = R.MLADPARAM<MLPAR.NEXT.6MONTH>
        WS.YEAR    = R.MLADPARAM<MLPAR.NEXT.YEAR>
        WS.3YEAR   = R.MLADPARAM<MLPAR.NEXT.3YEAR>
        WS.MORE    = R.MLADPARAM<MLPAR.NEXT.MORE>

*--------------------

*--------------------
        BEGIN CASE
            CASE WS.MATDAT EQ 'Y'
                WS.DAY    = 0
                WS.WEEK   = 0
                WS.MONTH  = 0
                WS.3MONTH = 0
                WS.6MONTH = 0
                WS.YEAR   = 0
                WS.3YEAR  = 0
                WS.MORE   = 0
        END CASE
*--------------------
        BEGIN CASE
            CASE WS.GENLED EQ ''
                WS.GENLED = 0
        END CASE
*--------------------
        BEGIN CASE
            CASE WS.CATEGFR EQ ''
                WS.CATEGFR = 0
        END CASE
*--------------------
        BEGIN CASE
            CASE WS.CATEGTO EQ ''
                WS.CATEGTO = 0
        END CASE
*--------------------
        BEGIN CASE
            CASE WS.GENLED GT '0'
                WS.CATEGFR    = 0
                WS.CATEGTO    = 0
        END CASE
*--------------------
        BEGIN CASE
            CASE WS.CATEGFR GT '0' AND WS.CATEGTO EQ '0'
                WS.GENLED     = 0
                WS.CATEGTO    = WS.CATEGFR
        END CASE
*--------------------
        LOAN.FLAG     = 'N'
        BEGIN CASE
            CASE WS.CATEGFR GE '1100' AND WS.CATEGTO LE '1599'
                LOAN.FLAG     = 'Y'
        END CASE
*--------------------



        GOSUB READ.CATEG.MAS.D




L.RET:

    REPEAT

RETURN
*--------------------------------------------------------------------------
READ.CATEG.MAS.D:

    IF WS.GENLED NE '0' THEN
        SEL.CATMAS  = "SELECT ":FN.CATMAS
        SEL.CATMAS := " WITH CATD.ASST.LIAB EQ ":WS.GENLED
    END

    IF WS.CATEGFR NE '0' THEN
        SEL.CATMAS  = "SELECT ":FN.CATMAS
        SEL.CATMAS := " WITH (CATD.CATEG.CODE GE ":WS.CATEGFR
        SEL.CATMAS := " AND CATD.CATEG.CODE LE ":WS.CATEGTO:")"
    END



    CALL EB.READLIST(SEL.CATMAS,SEL.LIST.CATMAS,'',NO.CATMAS,ERR.CATMAS)


    LOOP
        REMOVE Y.CATMAS.ID FROM SEL.LIST.CATMAS SETTING POS.CATMAS
    WHILE Y.CATMAS.ID:POS.CATMAS
        CALL F.READ(FN.CATMAS,Y.CATMAS.ID,R.CATMAS,F.CATMAS,ERR.CATMAS)

****************

        WS.ASST   = R.CATMAS<CATAL.CATD.ASST.LIAB>
        WS.CATEG  = R.CATMAS<CATAL.CATD.CATEG.CODE>
        WS.CY     = R.CATMAS<CATAL.CATD.CY.ALPH.CODE>
        WS.AMOUNT = R.CATMAS<CATAL.CATD.BAL.IN.ACT.CY>
        WS.LCL    = R.CATMAS<CATAL.CATD.BAL.IN.LOCAL.CY>

***  ---------------------------------------
***  ---------------------------------------
***  ---------------------------------------
***                                                                              ��������� �� ���������
***                             ��� �������  ��� ���������� ������� �� ���� ����


        IF WS.ASST EQ '0630' OR WS.ASST EQ '0120' THEN
            WS.CY = 'EGP'
            WS.AMOUNT = WS.LCL
        END

*--------------------------------------------------------------------------------
*           ����� ���� ���� ����� ���� ������� �������
*           ��� ��� ��� ����� ��� �������
*              �������� ��� 9-6-2015

        IF WS.TYPE EQ 'X' THEN
            IF WS.LINE EQ '150' THEN
                IF WS.CY EQ 'USD' THEN
                    IF   WS.ASST EQ '0311' THEN
                        IF WS.AMOUNT  LT -100000000000.00 THEN

                            WS.AMOUNT = WS.AMOUNT + 283056415511286.78 - 28304.64

                        END
                    END
                END
            END
        END

*--------------------------------------------------------------------------------


*----- hares
*        IF WS.TYPE EQ 'X' THEN
*            IF WS.LINE EQ '010' THEN
*                IF WS.CY EQ 'EUR' THEN
*                    T.TT = T.TT + WS.AMOUNT
*                    PRINT WS.GENALL:" ":WS.CATEGFR:" ":WS.CATEGTO:" ":WS.CATEG:" ":WS.CY:" ":WS.AMOUNT:" ":" ":T.TT
*                    PRINT SEL.CATMAS
*                    PRINT WS.CY:"  ":WS.AMOUNT
*                END
*            END
*        END
*----- hares


        GOSUB RTN.MOVE.ZEROS
*     ----------------------------
***                                                  ALAA EZZAT
***

        IF WS.TYPE EQ 'X' THEN
            WS.AMOUNT = WS.AMOUNT * -1
        END
*     ------------------------------------------------------------
        IF LOAN.FLAG EQ 'Y' THEN
            TWS.AMOUNT += WS.AMOUNT
            GOTO LOAN.TAG
        END
****   -----------------------------------------------------------
****



        WS.DAY.AMT    = WS.AMOUNT * WS.DAY / 100
        WS.WEEK.AMT   = WS.AMOUNT * WS.WEEK / 100
        WS.MONTH.AMT  = WS.AMOUNT * WS.MONTH / 100
        WS.3MONTH.AMT = WS.AMOUNT * WS.3MONTH / 100
        WS.6MONTH.AMT = WS.AMOUNT * WS.6MONTH / 100
        WS.YEAR.AMT   = WS.AMOUNT * WS.YEAR / 100
        WS.3YEAR.AMT  = WS.AMOUNT * WS.3YEAR / 100
        WS.MORE.AMT   = WS.AMOUNT * WS.MORE / 100


        GOSUB READ.MLAD.FILE.020
        GOSUB RTN.MOVE.ZEROS

*---------------
LOAN.TAG:
    REPEAT

RETURN
*--------------------------------------------------------------------------
LOAN.ROUTINE:

    R.MLAD020 = ''
    FN.MLAD020 = "F.SCB.MLAD.FILE.020"
    F.MLAD020  = ""
    R.MLAD020  = ""
    Y.MLAD020.ID  = ""

    SEL.MLAD020  = "SELECT ":FN.MLAD020:" WITH @ID LIKE ...-X-090"

    CALL EB.READLIST(SEL.MLAD020,SEL.LIST.MLAD020,'',NO.MLAD020,ERR.MLAD020)

    LOOP
        REMOVE Y.MLAD020.ID FROM SEL.LIST.MLAD020 SETTING POS.MLAD020
    WHILE Y.MLAD020.ID:POS.MLAD020
        CALL F.READ(FN.MLAD020,Y.MLAD020.ID,R.MLAD020,F.MLAD020,ERR.MLAD020)

****************
        WS.CY020 = R.MLAD020<ML020.CURRENCY>
****************
        GOSUB RTN.MOVE.ZEROS
****************
        WS.DAY.AMT    = R.MLAD020<ML020.NEXT.DAY>
        WS.WEEK.AMT   = R.MLAD020<ML020.NEXT.WEEK>
        WS.MONTH.AMT  = R.MLAD020<ML020.NEXT.MONTH>
        WS.3MONTH.AMT = R.MLAD020<ML020.NEXT.3MONTH>
        WS.6MONTH.AMT = R.MLAD020<ML020.NEXT.6MONTH>
        WS.YEAR.AMT   = R.MLAD020<ML020.NEXT.YEAR>
        WS.3YEAR.AMT  = R.MLAD020<ML020.NEXT.3YEAR>
        WS.MORE.AMT   = R.MLAD020<ML020.NEXT.MORE>

        WS.TOT.020 = WS.DAY.AMT + WS.WEEK.AMT + WS.MONTH.AMT +  WS.3MONTH.AMT +  WS.6MONTH.AMT + WS.YEAR.AMT +  WS.3YEAR.AMT + WS.MORE.AMT


**********************************
        GOSUB  READ.CATEG.FOR.LOAN
**********************************
*        IF ABS(WS.TOT.020) NE ABS(WS.LOAN.AMOUNT) THEN
*            PRINT '-------------------------------------------------'
*            PRINT Y.MLAD020.ID
*            PRINT 'TOTAL1 ':ABS(WS.TOT.020)
*            PRINT 'TOTAL2 ':ABS(WS.LOAN.AMOUNT)
*            PRINT 'TOTAL3 ':ABS(WS.SUSP.AMOUNT)
*            PRINT '================================================='
*        END
**********************************

        WS.TOT.020 = ABS(WS.TOT.020)
        WS.LOAN.AMOUNT = ABS(WS.LOAN.AMOUNT)
        WS.SUSP.AMOUNT = ABS(WS.SUSP.AMOUNT)


        WS.TOT.020 = WS.LOAN.AMOUNT - WS.TOT.020


*--------------------
        WS.CY =  WS.CY020
        WS.TYPE = "X"
        WS.LINE = "090"

        IF WS.CY EQ 'EGP' THEN
            WS.DAY.AMT      = WS.TOT.020 * 0.20
            WS.WEEK.AMT     = WS.TOT.020 * 0
            WS.MONTH.AMT    = WS.TOT.020 * 0
            WS.3MONTH.AMT   = WS.TOT.020 * 0
            WS.6MONTH.AMT   = WS.TOT.020 * 0.80
            WS.YEAR.AMT     = WS.TOT.020 * 0
            WS.3YEAR.AMT    = WS.TOT.020 * 0
            WS.MORE.AMT     = WS.SUSP.AMOUNT
        END
        IF WS.CY NE 'EGP' THEN
            WS.DAY.AMT      = WS.TOT.020 * 0.50
            WS.WEEK.AMT     = WS.TOT.020 * 0
            WS.MONTH.AMT    = WS.TOT.020 * 0
            WS.3MONTH.AMT   = WS.TOT.020 * 0
            WS.6MONTH.AMT   = WS.TOT.020 * 0.50
            WS.YEAR.AMT     = WS.TOT.020 * 0
            WS.3YEAR.AMT    = WS.TOT.020 * 0
            WS.MORE.AMT     = WS.SUSP.AMOUNT
        END
*--------------------

        GOSUB READ.MLAD.FILE.020

*--------------------


    REPEAT

RETURN
*--------------------------------------------------------------------------
READ.CATEG.FOR.LOAN:

    WS.LOAN.AMOUNT = 0
    WS.SUSP.AMOUNT = 0
*--------------------
    SEL.CATMAS  = "SELECT ":FN.CATMAS:" WITH  CATD.CY.ALPH.CODE EQ ":WS.CY020
    SEL.CATMAS := " AND CATD.ASST.LIAB IN (":LOAN.GROUP.1:")"
*    SEL.CATMAS := " AND CATD.CATEG.CODE GE 1100"
*    SEL.CATMAS := " AND CATD.CATEG.CODE LE 1599"

    CALL EB.READLIST(SEL.CATMAS,SEL.LIST.CATMAS,'',NO.CATMAS,ERR.CATMAS)
    LOOP
        REMOVE Y.CATMAS.ID FROM SEL.LIST.CATMAS SETTING POS.CATMAS
    WHILE Y.CATMAS.ID:POS.CATMAS
        CALL F.READ(FN.CATMAS,Y.CATMAS.ID,R.CATMAS,F.CATMAS,ERR.CATMAS)
        WS.AMOUNT = R.CATMAS<CATAL.CATD.BAL.IN.ACT.CY>
        WS.CAT    = R.CATMAS<CATAL.CATD.CATEG.CODE>
        WS.ASST   = R.CATMAS<CATAL.CATD.ASST.LIAB>
*--------------------
*        IF R.CATMAS<CATAL.CATD.CY.ALPH.CODE> EQ 'EUR' THEN
*            PRINT '----------------------------------------'
*            PRINT WS.ASST:'  ':WS.CAT:'  ':WS.AMOUNT
*        END
*--------------------
        IF  WS.AMOUNT NE 0  THEN
            WS.LOAN.AMOUNT += WS.AMOUNT
        END
*--------------------
    REPEAT
*----------------------------------------------------------------------------------
    SEL.CATMAS  = "SELECT ":FN.CATMAS:" WITH  CATD.CY.ALPH.CODE EQ ":WS.CY020
    SEL.CATMAS := " AND CATD.ASST.LIAB IN (":LOAN.GROUP.2:")"
*    SEL.CATMAS := " AND CATD.CATEG.CODE GE 1100"
*    SEL.CATMAS := " AND CATD.CATEG.CODE LE 1599"

    CALL EB.READLIST(SEL.CATMAS,SEL.LIST.CATMAS,'',NO.CATMAS,ERR.CATMAS)
    LOOP
        REMOVE Y.CATMAS.ID FROM SEL.LIST.CATMAS SETTING POS.CATMAS
    WHILE Y.CATMAS.ID:POS.CATMAS
        CALL F.READ(FN.CATMAS,Y.CATMAS.ID,R.CATMAS,F.CATMAS,ERR.CATMAS)
        WS.AMOUNT = R.CATMAS<CATAL.CATD.BAL.IN.ACT.CY>
        WS.CAT    = R.CATMAS<CATAL.CATD.CATEG.CODE>
        WS.ASST   = R.CATMAS<CATAL.CATD.ASST.LIAB>
*--------------------
        IF  WS.AMOUNT NE 0  THEN
            WS.SUSP.AMOUNT += WS.AMOUNT
        END
*--------------------
    REPEAT

RETURN

*--------------------------------------------------------------------------
RTN.MOVE.ZEROS.2:


    WS.TYPE    = 0
    WS.LINE    = 0
    WS.GENLED  = 0
    WS.CATEGFR = 0
    WS.CATEGTO = 0
    WS.MATDAT  = 0
    WS.DAY     = 0
    WS.WEEK    = 0
    WS.MONTH   = 0
    WS.3MONTH  = 0
    WS.6MONTH  = 0
    WS.YEAR    = 0
    WS.3YEAR   = 0
    WS.MORE    = 0

RETURN
*--------------------------------------------------------------------------
RTN.MOVE.ZEROS:

    WS.DAY.AMT    = 0
    WS.WEEK.AMT   = 0
    WS.MONTH.AMT  = 0
    WS.3MONTH.AMT = 0
    WS.6MONTH.AMT = 0
    WS.YEAR.AMT   = 0
    WS.3YEAR.AMT  = 0
    WS.MORE.AMT   = 0


RETURN
*--------------------------------------------------------------------------
READ.MLAD.FILE.020:

    R.MLAD020 = ''

    Y.MLAD020.ID = WS.CY:"-":WS.TYPE:"-":WS.LINE

    CALL F.READ(FN.MLAD020,Y.MLAD020.ID,R.MLAD020,F.MLAD020,ERR.FI)

*   -------------------
    IF ERR.FI EQ '' THEN

        R.MLAD020<ML020.NEXT.DAY>      += WS.DAY.AMT
        R.MLAD020<ML020.NEXT.WEEK>     += WS.WEEK.AMT
        R.MLAD020<ML020.NEXT.MONTH>    += WS.MONTH.AMT
        R.MLAD020<ML020.NEXT.3MONTH>   += WS.3MONTH.AMT
        R.MLAD020<ML020.NEXT.6MONTH>   += WS.6MONTH.AMT
        R.MLAD020<ML020.NEXT.YEAR>     += WS.YEAR.AMT
        R.MLAD020<ML020.NEXT.3YEAR>    += WS.3YEAR.AMT
        R.MLAD020<ML020.NEXT.MORE>     += WS.MORE.AMT


*------------------------
        R.MLAD020<ML020.RESERVED1> = Y.MLAD020.ID[5,5]
        CALL F.WRITE(FN.MLAD020,Y.MLAD020.ID,R.MLAD020)
        CALL JOURNAL.UPDATE(Y.MLAD020.ID)

*------------------------

    END
RETURN
*===============================================================
END
