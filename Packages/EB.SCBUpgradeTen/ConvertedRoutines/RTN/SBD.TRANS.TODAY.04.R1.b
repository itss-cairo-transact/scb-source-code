* @ValidationCode : MjoxNDcxMTUwMzUyOkNwMTI1MjoxNjQwODMyOTAwNzE3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:55:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
SUBROUTINE SBD.TRANS.TODAY.04.R1(ENQ.DATA)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] HASHING "$INCLUDE I_F.SCB.TRANS.TODAY" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

*-----------------------------------
    SYS.DATE   = TODAY
    LAST.WORK.DATE   = SYS.DATE
*****    CALL CDT('', LAST.WORK.DATE, "-1W")
*------------------------
    FN.TRANS = "F.SCB.TRANS.TODAY"
    F.TRANS = ''
    R.TRANS=''
    Y.TRANS=''
    Y.TRANS.ERR=''

    CALL OPF(FN.TRANS,F.TRANS)

    SEL.CMD   = "SELECT ":FN.TRANS:" WITH @ID LIKE ...E... AND COMPANY.CO EQ ":COMP
    SEL.CMD  := " AND BOOKING.DATE GE ":LAST.WORK.DATE
    SEL.CMD  := " AND AMOUNT.LCY LT 0 "
    SEL.CMD  := " AND ((PL.CATEGORY GE  11213 AND PL.CATEGORY LE 11218)"
    SEL.CMD  := " OR (PL.CATEGORY GE 11274 AND PL.CATEGORY LE 11281)"
    SEL.CMD  := " OR (PL.CATEGORY GE 11352 AND PL.CATEGORY LE 11360)"
    SEL.CMD  := " OR (PL.CATEGORY GE 11369 AND PL.CATEGORY LE 11378)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11251)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11019)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11020)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11022)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11023)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11035)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11212)"
    SEL.CMD  := " OR (PL.CATEGORY EQ 11214))"




    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)

    IF NOREC >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
RETURN

END
