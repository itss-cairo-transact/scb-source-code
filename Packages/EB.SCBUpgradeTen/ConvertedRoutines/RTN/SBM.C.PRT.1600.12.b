* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1232</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE  SBM.C.PRT.1600.12
*    PROGRAM SBM.C.PRT.1600.12
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.STATIC.MAST.P
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

    FLG = 0
    YTEXT = " Enter (1-PRINT) OR (2-CBE)"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.NUM  = COMI

    IF WS.NUM EQ 1 THEN
        REPORT.ID='SBM.C.PRT.001'
        FLG = 1
        GOSUB INITIAL
    END
    IF WS.NUM EQ 2 THEN
        REPORT.ID='1600.12'
        FLG = 1
        GOSUB INITIAL
    END
    IF FLG EQ 0 THEN
        TEXT = "INVALID INPUT NUMBER"
        RETURN
    END
    TEXT = "DONE" ; CALL REM
    RETURN
*--------------------------------------------
INITIAL:

****����� ����� ����� �������
****������� ���
****1600
****����
**** 12
*-------------------------------------------
    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*------------------------------------------
    CALL OPF (FN.CBE,F.CBE)
    CALL OPF (FN.COMP,F.COMP)
*------------------------------------------------CLEAR AREA
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.ARRY.RAW = ""
    WS.ARRY.COL = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.1.LE = ""
    WS.2.LE = ""
    WS.3.LE = ""

    WS.1.EQV = ""
    WS.2.EQV = ""
    WS.3.EQV = ""
    WS.HD.T  = "����� ������ � ���������� ���� ������ ������� � ������ ������ ���������  "

    WS.HD.TA = " ����� ��� 1600 "

    WS.HD.T2 = "������� ������� �� �������� ���� ���� ������� "


    WS.HD.T2A = "12    ���� "

    WS.HD.T3  = "������ ����� ���  "

    WS.HD.1  = "���� �����"
    WS.HD.1A = "���� ������"

    WS.HD.2  = "���� ���������� "

    WS.HD.2A = "������ ���� "

    WS.HD.2B = "�������� "

    WS.HD.3  = "��������� "

    WS.PRG.1 = "SBM.C.PRT.1600.12"
********    ARRAY1 = ""
******                                 ARRAY
******                                                 1 ������
******
****** 2----   H = HEADER  D = DETAIL   T = TOTAL
******  3 AND 4 ----   RANGE  FROM  TO    FOR INDUSTRY
******  5  TOTAL  OF  FIRST DATA COLUMN   IN LE
******  6  TOTAL  OF  2     DATA COLUMN   IN LE
******* 7  TOTAL  OF  3     DATA COLUMN   IN LE
******* 8  TOTAL  OF  1     DATA COLUMN   IN EQVELENT
********9  TOTAL  OF  2     DATA COLUMN   IN EQVELENT
********10 TOTAL  OF  3     DATA COLUMN   IN EQVELENT
********11 THE ARRAY NO OF  TOTAL OF GROUP TO BE ACUMLATED


    DIM ARRAY1(36,11)

    ARRAY1(1,1) = "������ �����                       "
    ARRAY1(1,2) = "H"

    ARRAY1(2,1) = "����� ���� ��������                "
    ARRAY1(2,2) = "D"
    ARRAY1(2,5) = "0"
    ARRAY1(2,6) = "0"
    ARRAY1(2,7) = "0"
    ARRAY1(2,8) = "0"
    ARRAY1(2,9) = "0"
    ARRAY1(2,10) = "0"
    ARRAY1(2,11) = "5"

    ARRAY1(3,1) = "����� ���� ������� �����            "
    ARRAY1(3,2) = "D"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"
    ARRAY1(3,8) = "0"
    ARRAY1(3,9) = "0"
    ARRAY1(3,10) = "0"
    ARRAY1(3,11) = "5"

    ARRAY1(4,1) = "����� ������ �����                 "
    ARRAY1(4,2) = "D"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"
    ARRAY1(4 ,8) = "0"
    ARRAY1(4,9) = "0"
    ARRAY1(4,10) = "0"
    ARRAY1(4,11) = "5"

    ARRAY1(5,1) = "����� ������ �����                 "
    ARRAY1(5,2) = "T"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = "0"
    ARRAY1(5,7) = "0"
    ARRAY1(5,8) = "0"
    ARRAY1(5,9) = "0"
    ARRAY1(5,10) = "0"

    ARRAY1(6,1) = "���� ������� �����                  "
    ARRAY1(6,2) = "H"

    ARRAY1(7,1)  = "����� ������� ����� ���������       "
    ARRAY1(7,2)  = "D"
    ARRAY1(7,5) = "0"
    ARRAY1(7,6) = "0"
    ARRAY1(7,7) = "0"
    ARRAY1(7,8) = "0"
    ARRAY1(7,9) = "0"
    ARRAY1(7,10) = "0"
    ARRAY1(7,11) = "13"

    ARRAY1(8,1) = "����� ������� ������                 "
    ARRAY1(8,2) = "D"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = "0"
    ARRAY1(8,7) = "0"
    ARRAY1(8,8) = "0"
    ARRAY1(8,9) = "0"
    ARRAY1(8,10) = "0"
    ARRAY1(8,11) = "13"

    ARRAY1(9,1) = "����� �������                       "
    ARRAY1(9,2) = "D"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = "0"
    ARRAY1(9,7) = "0"
    ARRAY1(9,8) = "0"
    ARRAY1(9,9) = "0"
    ARRAY1(9,10) = "0"
    ARRAY1(9,11) = "13"

    ARRAY1(10,1) = "�������� ���������                 "
    ARRAY1(10,2) = "D"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = "0"
    ARRAY1(10,7) = "0"
    ARRAY1(10,8) = "0"
    ARRAY1(10,9) = "0"
    ARRAY1(10,10) = "0"
    ARRAY1(10,11) = "13"

    ARRAY1(11,1) = "����� �����                        "
    ARRAY1(11,2) = "D"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = "0"
    ARRAY1(11,7) = "0"
    ARRAY1(11,8) = "0"
    ARRAY1(11,9) = "0"
    ARRAY1(11,10) = "0"
    ARRAY1(11,11) = "13"

    ARRAY1(12,1) = "����� ���� ����� ������� �����     "
    ARRAY1(12,2) = "D"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = "0"
    ARRAY1(12,7) = "0"
    ARRAY1(12,8) = "0"
    ARRAY1(12,9) = "0"
    ARRAY1(12,10) = "0"
    ARRAY1(12,11) = "13"

    ARRAY1(13,1) = "����� ���� ������� �����           "
    ARRAY1(13,2) = "T"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = "0"
    ARRAY1(13,7) = "0"
    ARRAY1(13,8) = "0"
    ARRAY1(13,9) = "0"
    ARRAY1(13,10) = "0"


    ARRAY1(14,1) = "���� ������� �����                 "
    ARRAY1(14,2) = "H"

    ARRAY1(15,1)  = "������ � ������� �������� (1(      "
    ARRAY1(15,2)  = "D"
    ARRAY1(15,5) = "0"
    ARRAY1(15,6) = "0"
    ARRAY1(15,7) = "0"
    ARRAY1(15,8) = "0"
    ARRAY1(15,9) = "0"
    ARRAY1(15,10) = "0"
    ARRAY1(15,11) = "18"

    ARRAY1(16,1)  = "�������� �������                  "
    ARRAY1(16,2)  = "D"
    ARRAY1(16,5) = "0"
    ARRAY1(16,6) = "0"
    ARRAY1(16,7) = "0"
    ARRAY1(16,8) = "0"
    ARRAY1(16,9) = "0"
    ARRAY1(16,10) = "0"
    ARRAY1(16,11) = "18"

    ARRAY1(17,1)  = "����� �����                       "
    ARRAY1(17,2)  = "D"
    ARRAY1(17,5) = "0"
    ARRAY1(17,6) = "0"
    ARRAY1(17,7) = "0"
    ARRAY1(17,8) = "0"
    ARRAY1(17,9) = "0"
    ARRAY1(17,10) = "0"
    ARRAY1(17,11) = "18"

    ARRAY1(18,1)  = "����� ���� ������� �����          "
    ARRAY1(18,2)  = "T"
    ARRAY1(18,5) = "0"
    ARRAY1(18,6) = "0"
    ARRAY1(18,7) = "0"
    ARRAY1(18,8) = "0"
    ARRAY1(18,9) = "0"
    ARRAY1(18,10) = "0"

    ARRAY1(19,1)  = "������ �������                    "
    ARRAY1(19,2)  = "H"

    ARRAY1(20,1)  = "����� �������                     "
    ARRAY1(20,2)  = "D"
    ARRAY1(20,5) = "0"
    ARRAY1(20,6) = "0"
    ARRAY1(20,7) = "0"
    ARRAY1(20,8) = "0"
    ARRAY1(20,9) = "0"
    ARRAY1(20,10) = "0"
    ARRAY1(20,11) = "23"

    ARRAY1(21,1)  = "����� ����� �� ���� �����          "
    ARRAY1(21,2)  = "D"
    ARRAY1(21,5) = "0"
    ARRAY1(21,6) = "0"
    ARRAY1(21,7) = "0"
    ARRAY1(21,8) = "0"
    ARRAY1(21,9) = "0"
    ARRAY1(21,10) = "0"
    ARRAY1(21,11) = "23"

    ARRAY1(22,1)  = "����� ������ ���� �� ���          "
    ARRAY1(22,2)  = "D"
    ARRAY1(22,5) = "0"
    ARRAY1(22,6) = "0"
    ARRAY1(22,7) = "0"
    ARRAY1(22,8) = "0"
    ARRAY1(22,9) = "0"
    ARRAY1(22,10) = "0"
    ARRAY1(22,11) = "23"

    ARRAY1(23,1)  = "����� ������ �������              "
    ARRAY1(23,2)  = "T"
    ARRAY1(23,5) = "0"
    ARRAY1(23,6) = "0"
    ARRAY1(23,7) = "0"
    ARRAY1(23,8) = "0"
    ARRAY1(23,9) = "0"
    ARRAY1(23,10) = "0"

    ARRAY1(24,1)  = "����� ������� ���������            "
    ARRAY1(24,2)  = "D"
    ARRAY1(24,5) = "0"
    ARRAY1(24,6) = "0"
    ARRAY1(24,7) = "0"
    ARRAY1(24,8) = "0"
    ARRAY1(24,9) = "0"
    ARRAY1(24,10) = "0"
    ARRAY1(24,11) = "0"

    ARRAY1(25,1)  = "������ �������(�����(             "
    ARRAY1(25,2)  = "D"
    ARRAY1(25,5) = "0"
    ARRAY1(25,6) = "0"
    ARRAY1(25,7) = "0"
    ARRAY1(25,8) = "0"
    ARRAY1(25,9) = "0"
    ARRAY1(25,10) = "0"
    ARRAY1(25,11) = "0"

    ARRAY1(26,1)  = "������� ��������                  "
    ARRAY1(26,2)  = "H"

    ARRAY1(27,1)  = " ������ ������� ������� ���������  "
    ARRAY1(27,2)  = "D"
    ARRAY1(27,5) = "0"
    ARRAY1(27,6) = "0"
    ARRAY1(27,7) = "0"
    ARRAY1(27,8) = "0"
    ARRAY1(27,9) = "0"
    ARRAY1(27,10) = "0"
    ARRAY1(27,11) = "35"

    ARRAY1(28,1)  = "  ���� ��� ���� ���������          "
    ARRAY1(28,2)  = "D"
    ARRAY1(28,5) = "0"
    ARRAY1(28,6) = "0"
    ARRAY1(28,7) = "0"
    ARRAY1(28,8) = "0"
    ARRAY1(28,9) = "0"
    ARRAY1(28,10) = "0"
    ARRAY1(28,11) = "35"

    ARRAY1(29,1)  = "������ ������� ������             "
    ARRAY1(29,2)  = "D"
    ARRAY1(29,5) = "0"
    ARRAY1(29,6) = "0"
    ARRAY1(29,7) = "0"
    ARRAY1(29,8) = "0"
    ARRAY1(29,9) = "0"
    ARRAY1(29,10) = "0"
    ARRAY1(29,11) = "35"

    ARRAY1(30,1)  = "������ ������� ������� ��� �������"
    ARRAY1(30,2)  = "D"
    ARRAY1(30,5) = "0"
    ARRAY1(30,6) = "0"
    ARRAY1(30,7) = "0"
    ARRAY1(30,8) = "0"
    ARRAY1(30,9) = "0"
    ARRAY1(30,10) = "0"
    ARRAY1(30,11) = "35"

    ARRAY1(31,1)  = "������ ���������                   "
    ARRAY1(31,2)  = "D"
    ARRAY1(31,5) = "0"
    ARRAY1(31,6) = "0"
    ARRAY1(31,7) = "0"
    ARRAY1(31,8) = "0"
    ARRAY1(31,9) = "0"
    ARRAY1(31,10) = "0"
    ARRAY1(31,11) = "35"

    ARRAY1(32,1)  = "������ ��������� ��� �������        "
    ARRAY1(32,2)  = "D"
    ARRAY1(32,5) = "0"
    ARRAY1(32,6) = "0"
    ARRAY1(32,7) = "0"
    ARRAY1(32,8) = "0"
    ARRAY1(32,9) = "0"
    ARRAY1(32,10) = "0"
    ARRAY1(32,11) = "35"

    ARRAY1(33,1)  = "����� ������� (����� ���������)    "
    ARRAY1(33,2)  = "D"
    ARRAY1(33,5) = "0"
    ARRAY1(33,6) = "0"
    ARRAY1(33,7) = "0"
    ARRAY1(33,8) = "0"
    ARRAY1(33,9) = "0"
    ARRAY1(33,10) = "0"
    ARRAY1(33,11) = "35"

    ARRAY1(34,1)  = "����� ������� ������               "
    ARRAY1(34,2)  = "D"
    ARRAY1(34,5) = "0"
    ARRAY1(34,6) = "0"
    ARRAY1(34,7) = "0"
    ARRAY1(34,8) = "0"
    ARRAY1(34,9) = "0"
    ARRAY1(34,10) = "0"
    ARRAY1(34,11) = "35"

    ARRAY1(35,1)  = "����� ������� ��������            "
    ARRAY1(35,2)  = "T"
    ARRAY1(35,5) = "0"
    ARRAY1(35,6) = "0"
    ARRAY1(35,7) = "0"
    ARRAY1(35,8) = "0"
    ARRAY1(35,9) = "0"
    ARRAY1(35,10) = "0"

    ARRAY1(36,1)  = "������ ����� �������               "
    ARRAY1(36,2)  = "T"
    ARRAY1(36,5) = "0"
    ARRAY1(36,6) = "0"
    ARRAY1(36,7) = "0"
    ARRAY1(36,8) = "0"
    ARRAY1(36,9) = "0"
    ARRAY1(36,10) = "0"
**************************************************
**************************************************
    DIM ARRAYRNG(25,2)
*    ARRAY1(2,1) = "����� ���� ��������                "
    ARRAYRNG(1,1) = "110"
    ARRAYRNG(1,2) = "2"

*    ARRAY1(3,1) = "����� ���� ������� �����            "
    ARRAYRNG(2,1) = "120"
    ARRAYRNG(2,2) = "3"

*
*    ARRAY1(4,1) = "����� ������ �����                 "
    ARRAYRNG(3,1) = "130"
    ARRAYRNG(3,2) = "4"

*    ARRAY1(7,1)  = "����� ������� ����� ���������       "
    ARRAYRNG(4,1) = "210"
    ARRAYRNG(4,2) = "7"

*
*    ARRAY1(8,1) = "����� ������� ������                 "
    ARRAYRNG(5,1) = "220"
    ARRAYRNG(5,2) = "8"

*    ARRAY1(9,1) = "����� �������                       "
    ARRAYRNG(6,1) = "230"
    ARRAYRNG(6,2) = "9"

*    ARRAY1(10,1) = "�������� ���������                 "
    ARRAYRNG(7,1) = "240"
    ARRAYRNG(7,2) = "10"

*    ARRAY1(11,1) = "����� �����                        "
    ARRAYRNG(8,1) = "250"
    ARRAYRNG(8,2) = "11"

*    ARRAY1(12,1) = "����� ���� ����� ������� �����     "
    ARRAYRNG(9,1) = "260"
    ARRAYRNG(9,2) = "12"

*    ARRAY1(15,1)  = "������ � ������� �������� (1(      "
    ARRAYRNG(10,1) = "4500"
    ARRAYRNG(10,2) = "15"

*    ARRAY1(16,1)  = "�������� �������                  "
    ARRAYRNG(11,1) = "4550"
    ARRAYRNG(11,2) = "16"

*    ARRAY1(17,1)  = "����� �����                       "
    ARRAYRNG(12,1) = "4600"
    ARRAYRNG(12,2) = "17"

*    ARRAY1(20,1)  = "����� �������                     "
    ARRAYRNG(13,1) = "4650"
    ARRAYRNG(13,2) = "20"

*    ARRAY1(21,1)  = "����� ����� �� ���� �����          "
    ARRAYRNG(14,1) = "4700"
    ARRAYRNG(14,2) = "21"

*    ARRAY1(22,1)  = "����� ������ ���� �� ���          "
    ARRAYRNG(15,1) = "4750"
    ARRAYRNG(15,2) = "22"

*    ARRAY1(24,1)  = "����� ������� ���������            "
    ARRAYRNG(16,1) = "6000"
    ARRAYRNG(16,2) = "24"

*    ARRAY1(25,1)  = "������ �������(�����(             "
    ARRAYRNG(17,1) = "7000"
    ARRAYRNG(17,2) = "25"

*    ARRAY1(27,1)  = " ������ ������� ������� ���������  "
    ARRAYRNG(18,1) = "8001"
    ARRAYRNG(18,2) = "27"

*    ARRAY1(28,1)  = "  ���� ��� ���� ���������          "
    ARRAYRNG(19,1) = "8002"
    ARRAYRNG(19,2) = "28"

*    ARRAY1(29,1)  = "������ ������� ������             "
    ARRAYRNG(20,1) = "8003"
    ARRAYRNG(20,2) = "29"

*    ARRAY1(30,1)  = "������ ������� ������� ��� �������"
    ARRAYRNG(21,1) = "8004"
    ARRAYRNG(21,2) = "30"

*    ARRAY1(31,1)  = "������ ���������                   "
    ARRAYRNG(22,1) = "8005"
    ARRAYRNG(22,2) = "31"

*    ARRAY1(32,1)  = "������ ��������� ��� �������        "
    ARRAYRNG(23,1) = "8006"
    ARRAYRNG(23,2) = "32"

*    ARRAY1(33,1)  = "����� ������� (����� ���������)    "
    ARRAYRNG(24,1) = "8007"
    ARRAYRNG(24,2) = "33"

*    ARRAY1(34,1)  = "����� ������� ������               "
    ARRAYRNG(25,1) = "8008"
    ARRAYRNG(25,2) = "34"

********   PROCEDURE *****************************************
*************************************************
*-------------------------------------------PREPARE  VARIABLE

    WS.INDSTRY = ""
    WS.INDSTRYA = ""
    WS.CBE.ID.INSR  = ""
    GOSUB A.050.GET.ALL.BR
*    GOSUB A.5000.PRT.HEAD
*------------------------------------------START PROCESSING
*    GOSUB A.100.PROCESS

*    WS.ARRY.RAW = 1
*    WS.ARRY.COL = 1

*    GOSUB A.300.PRNT
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE  99 THEN
            GOTO A.050.A
        END
*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        WS.ARRY.RAW = 1
        WS.ARRY.COL = 1
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0
*            GOSUB A.5100.PRT.SPACE.PAGE
            GOSUB A.5000.PRT.HEAD
            GOSUB A.300.PRNT
        END
*
A.050.A:
    REPEAT
    RETURN
*----------------------------------------------
A.100.PROCESS:
    IF WS.BR NE 99  THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE EG001... AND CBE.BR EQ ":WS.BR
    END
    IF WS.BR EQ 99  THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE EG001..."
    END

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.CBE.ID FROM SEL.LIST SETTING POS
    WHILE WS.CBE.ID:POS

        CALL F.READ(FN.CBE,WS.CBE.ID,R.CBE,F.CBE,MSG.CBE)
        WS.TEMP = R.CBE<P.CBE.BR>
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        IF  WS.TEMP NE WS.BR THEN
            GOTO AAA
        END
*        IF WS.TEMP NE 1 THEN
*            GOTO AAA
*        END
A.100.A:
        WS.INDSTRYA = R.CBE<P.CBE.NEW.SECTOR>
*****����� ������ ������ �� ���� ����� ������ ����� ���� �������
*****������ ������� �������
        WS.CBE.ID.INSR  = WS.CBE.ID[1]
        IF WS.CBE.ID.INSR EQ "A"  THEN
            WS.INDSTRYA = 8008
        END


        IF WS.INDSTRYA EQ 0 THEN
            GOTO AAA
        END
        WS.INDSTRY = WS.INDSTRYA
        IF WS.INDSTRYA LT 4500   THEN
            WS.INDSTRY  = WS.INDSTRYA[3]
        END
        WS.1.LE = R.CBE<P.CBE.MARG.LC.LE>
        WS.2.LE = R.CBE<P.CBE.MARG.LG.LE> + R.CBE<P.CBE.BLOCK.AC.LE>
        WS.3.LE = R.CBE<P.CBE.MARG.LG.LE> + R.CBE<P.CBE.BLOCK.AC.LE> + R.CBE<P.CBE.MARG.LC.LE>

        WS.1.EQV = R.CBE<P.CBE.MARG.LC.EQ>
        WS.2.EQV = R.CBE<P.CBE.MARG.LG.EQ> + R.CBE<P.CBE.BLOCK.AC.EQ>
        WS.3.EQV = R.CBE<P.CBE.MARG.LG.EQ> + R.CBE<P.CBE.BLOCK.AC.EQ> + R.CBE<P.CBE.MARG.LC.EQ>
        GOSUB A.200.ACUM
*-----------------------------------------------------
AAA:
    REPEAT
BBB:
    RETURN
A.200.ACUM:
    FOR WSRNG = 1 TO 25

        GOSUB A.205.CHK.INDSTRY

    NEXT WSRNG
    RETURN
A.205.CHK.INDSTRY:
    IF  WS.INDSTRY NE ARRAYRNG(WSRNG,1) THEN
        RETURN
    END
    WS = ARRAYRNG(WSRNG,2)
    WS.FLAG.PRT = 1
    GOSUB A.210.ACUM
    RETURN

****        ARRAY          ������� ��� ��

A.210.ACUM:
****   TEXT = " ENETRING 210 " ; CALL REM
    ARRAY1(WS,5) = ARRAY1(WS,5) + WS.1.LE
    ARRAY1(WS,6) = ARRAY1(WS,6) + WS.2.LE
    ARRAY1(WS,7) = ARRAY1(WS,7) + WS.3.LE
    ARRAY1(WS,8) = ARRAY1(WS,8) + WS.1.EQV
    ARRAY1(WS,9) = ARRAY1(WS,9) + WS.2.EQV
    ARRAY1(WS,10) = ARRAY1(WS,10) + WS.3.EQV
    WS.T  = ARRAY1(WS,11)
    IF WS.T NE "0" THEN
        ARRAY1(WS.T,5) = ARRAY1(WS.T,5) + WS.1.LE
        ARRAY1(WS.T,6) = ARRAY1(WS.T,6) + WS.2.LE
        ARRAY1(WS.T,7) = ARRAY1(WS.T,7) + WS.3.LE
        ARRAY1(WS.T,8) = ARRAY1(WS.T,8) + WS.1.EQV
        ARRAY1(WS.T,9) = ARRAY1(WS.T,9) + WS.2.EQV
        ARRAY1(WS.T,10) = ARRAY1(WS.T,10) + WS.3.EQV
    END

    ARRAY1(36,5) = ARRAY1(36,5) + WS.1.LE
    ARRAY1(36,6) = ARRAY1(36,6) + WS.2.LE
    ARRAY1(36,7) = ARRAY1(36,7) + WS.3.LE
    ARRAY1(36,8) = ARRAY1(36,8) + WS.1.EQV
    ARRAY1(36,9) = ARRAY1(36,9) + WS.2.EQV
    ARRAY1(36,10) = ARRAY1(36,10) + WS.3.EQV

    RETURN
*****                                     ARRAY   ������� ��

A.300.PRNT:
    FOR I = 1 TO 36
        WS.H.D.T = ARRAY1(WS.ARRY.RAW,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.310.PRT.HD
        END
****        XX<1,1>[1,35]   = ARRAY1(WS.ARRY.RAW,WS.ARRY.COL)
        IF WS.H.D.T = "T" THEN
            GOSUB A.320.PRT.TOT
        END
        IF WS.H.D.T = "D" THEN
            GOSUB A.330.PRT.DTAL
        END
        WS.ARRY.RAW = WS.ARRY.RAW + 1
    NEXT I
    RETURN
A.310.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
*    XX<1,1>[1,35]   =  STR('-',35)
*    PRINT XX<1,1>
    RETURN

A.320.PRT.TOT:
    WS.A5 = ARRAY1(I,5) / 1000
    WS.A6 = ARRAY1(I,6) / 1000
    WS.A7 = ARRAY1(I,7) / 1000
    WS.A8 = ARRAY1(I,8) / 1000
    WS.A9 = ARRAY1(I,9) / 1000
    WS.A10 = ARRAY1(I,10) / 1000
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[37,15]   = FMT(WS.A5, "R0,")
    XX<1,1>[53,15]   = FMT(WS.A6, "R0,")
    XX<1,1>[69,15]   = FMT(WS.A7, "R0,")
    XX<1,1>[85,15]   = FMT(WS.A8, "R0,")
    XX<1,1>[101,15]  = FMT(WS.A9, "R0,")
    XX<1,1>[117,15]  = FMT(WS.A10, "R0,")
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,132]   = STR('-',132)
    PRINT XX<1,1>
    ARRAY1(I,5) =  0
    ARRAY1(I,6) =  0
    ARRAY1(I,7) =  0
    ARRAY1(I,8) =  0
    ARRAY1(I,9) =  0
    ARRAY1(I,10) = 0
    RETURN
A.330.PRT.DTAL:
    WS.A5 = ARRAY1(I,5) / 1000
    WS.A6 = ARRAY1(I,6) / 1000
    WS.A7 = ARRAY1(I,7) / 1000
    WS.A8 = ARRAY1(I,8) / 1000
    WS.A9 = ARRAY1(I,9) / 1000
    WS.A10 = ARRAY1(I,10) / 1000
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[37,15]   = FMT(WS.A5, "R0,")
    XX<1,1>[53,15]   = FMT(WS.A6, "R0,")
    XX<1,1>[69,15]   = FMT(WS.A7, "R0,")
    XX<1,1>[85,15]   = FMT(WS.A8, "R0,")
    XX<1,1>[101,15]  = FMT(WS.A9, "R0,")
    XX<1,1>[117,15]  = FMT(WS.A10, "R0,")
    PRINT XX<1,1>
    ARRAY1(I,5) =  0
    ARRAY1(I,6) =  0
    ARRAY1(I,7) =  0
    ARRAY1(I,8) =  0
    ARRAY1(I,9) =  0
    ARRAY1(I,10) = 0
    RETURN

**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

***    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������":SPACE(19):WS.HD.T:SPACE(5):WS.HD.TA
    PR.HD :="'L'":SPACE(1):WS.BR.NAME:SPACE(30):WS.HD.T2:SPACE(28):WS.HD.T2A
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(79):WS.PRG.1:SPACE(3):WS.HD.T3
    PR.HD :="'L'":SPACE(54):WS.HD.1:SPACE(40):WS.HD.1A
    PR.HD :="'L'":SPACE(35):WS.HD.2:SPACE(3):WS.HD.2A:SPACE(3):WS.HD.2B:SPACE(5):WS.HD.2:SPACE(3):WS.HD.2A:SPACE(3):WS.HD.2B
    PR.HD :="'L'":SPACE(35):WS.HD.3:SPACE(40):WS.HD.3
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
    RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
    RETURN

END
