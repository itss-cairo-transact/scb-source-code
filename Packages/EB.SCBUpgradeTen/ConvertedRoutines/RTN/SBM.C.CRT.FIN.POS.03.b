* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****  MOHSEN
    PROGRAM SBM.C.CRT.FIN.POS.03
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CBE.BANK.FIN.POS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*---------------------------------------------------
    FN.FIN = "F.CBE.BANK.FIN.POS"
    F.FIN  = ""
    FN.COMP = "F.COMPANY"
    F.COMP = ""

*
*-------------------------------------------------
*REC KEY  =  X               ������ � ������
*         =  XX              ����� ���� ������ � ������
*         =  XXXX             ����� ������� �� ����� ������

*REC NATURE -----   H  = HEAD
*                   SH = SUB HEAD
*                   D  = DETAIL
*                   ST = SUB TOTAL
*                   T  = TOTAL
*                   HD = HEAD AND DATAIL SAME TIME
*-------------------------------------------------
    WS.KEY = ""
    R.FIN = ""
*-------------------------------------------------
    CALL OPF (FN.FIN,F.FIN)
    CALL OPF (FN.COMP,F.COMP)
*------------------------PROCEDURE------------------------
    GOSUB A.050.GET.ALL.BR
*------------------------------------------------
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC
*
        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR LT 10 THEN
            WS.BR = WS.COMP.ID[1]
        END
*        IF  WS.BR EQ 1 THEN
*            GOSUB A.100.CRT
*        END
*        IF  WS.BR EQ 99 THEN
*            GOSUB A.100.CRT
*        END

        GOSUB A.100.CRT
A.050.A:
    REPEAT
    RETURN
*------------------------------------------------
A.100.CRT:
*...........................................................

    WS.KEY = "2010000"
    WS.DSCR = "����� ������ ������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "2010100"
    WS.DSCR = "����� �������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010101"
    WS.DSCR = "�����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010102"
    WS.DSCR = "����� ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010103"
    WS.DSCR = "����� ����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010104"
    WS.DSCR = "����� ����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010105"
    WS.DSCR = "������ ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010106"
    WS.DSCR = "���� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010107"
    WS.DSCR = "������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010108"
    WS.DSCR = "���� ������� ������� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010109"
    WS.DSCR = "����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010199"
    WS.DSCR = "����� ������� �������� ����� �������"
    WS.TYPE = "ST"
    GOSUB A.500.CRT




    WS.KEY = "2010200"
    WS.DSCR = "���� �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010201"
    WS.DSCR = "������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010202"
    WS.DSCR = "�����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010203"
    WS.DSCR = "����� ���� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010204"
    WS.DSCR = "����� ��� 3 ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010205"
    WS.DSCR = "������� �������� ����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010299"
    WS.DSCR = "����� ������� �������� ������ �������"
    WS.TYPE = "ST"
    GOSUB A.500.CRT





    WS.KEY = "2010300"
    WS.DSCR = "���� ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010301"
    WS.DSCR = "������ �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010302"
    WS.DSCR = "������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010303"
    WS.DSCR = "������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010304"
    WS.DSCR = "�����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010305"
    WS.DSCR = "������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010306"
    WS.DSCR = "������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT



    WS.KEY = "2010400"
    WS.DSCR = "������� �������� ����� �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2010401"
    WS.DSCR = "�� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010402"
    WS.DSCR = "�� ������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2010499"
    WS.DSCR = "����� ������� �������� ������ ��������"
    WS.TYPE = "ST"
    GOSUB A.500.CRT

    WS.KEY = "2019999"
    WS.DSCR = "������ ������� �������� ������"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "2020000"
    WS.DSCR = "������ ����� ������ ���� ���� �����"
    WS.TYPE = "HD"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "2030000"
    WS.DSCR = "����� � ������ � ������ ������ �����"
    WS.TYPE = "HD"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2040000"
    WS.DSCR = "����� �������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "2040100"
    WS.DSCR = "��� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040200"
    WS.DSCR = "����� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040300"
    WS.DSCR = "����� ���� � ������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2040301"
    WS.DSCR = "����� ���� ���"
    WS.TYPE = "D"
    GOSUB A.500.CRT
    WS.KEY = "2040302"
    WS.DSCR = "����� ��� ���"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040400"
    WS.DSCR = "����� ���� ����� ����� ����� ����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040500"
    WS.DSCR = "������ ����� � �����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2040501"
    WS.DSCR = "����� ������� ��� ��� ��� �� 3 �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040502"
    WS.DSCR = "����� ������� ��� ��� 3 ����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040503"
    WS.DSCR = "������ �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040600"
    WS.DSCR = "����� ����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2040601"
    WS.DSCR = "������ �������� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2040602"
    WS.DSCR = "����� ����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2049999"
    WS.DSCR = "������ ����� �������"
    WS.TYPE = "T"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2050000"
    WS.DSCR = "������ ����� �����"
    WS.TYPE = "HD"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2060000"
    WS.DSCR = "����� ����� � �������� ����"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "2060100"
    WS.DSCR = "������ ������� � ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060200"
    WS.DSCR = "�������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060300"
    WS.DSCR = "����� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060400"
    WS.DSCR = "������� ����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060500"
    WS.DSCR = "����� �����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060600"
    WS.DSCR = "������� ������ � �� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060700"
    WS.DSCR = "������� ��� ����� ����� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060800"
    WS.DSCR = "��� ����� ������� ��� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2060900"
    WS.DSCR = "��������� ��� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2061000"
    WS.DSCR = "����� ����� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2069999"
    WS.DSCR = "������ ������� ������� � ���������� ������"
    WS.TYPE = "T"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2070000"
    WS.DSCR = "����� �����"
    WS.TYPE = "HD"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2080000"
    WS.DSCR = "����� ������ ����� �����"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "2080100"
    WS.DSCR = "���� ����� ����� �� ���� ����"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2080101"
    WS.DSCR = "����� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2080102"
    WS.DSCR = "���� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2080200"
    WS.DSCR = "���� ����� ����� �� ���� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2089999"
    WS.DSCR = "������ ������� �������� ����� �����"
    WS.TYPE = "T"
    GOSUB A.500.CRT

*..........................................................
    WS.KEY = "2090000"
    WS.DSCR = "������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "2090100"
    WS.DSCR = "���� ������� �������� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2090200"
    WS.DSCR = "���� ������� �������� ��� ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2099999"
    WS.DSCR = "������ ��������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

*..........................................................

    WS.KEY = "2100000"
    WS.DSCR = "���� ���������"
    WS.TYPE = "H"
    GOSUB A.500.CRT

    WS.KEY = "2100100"
    WS.DSCR = "��� ����� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2100200"
    WS.DSCR = "�����������"
    WS.TYPE = "SH"
    GOSUB A.500.CRT

    WS.KEY = "2100201"
    WS.DSCR = "������ ������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2100202"
    WS.DSCR = "��������� ����"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2100300"
    WS.DSCR = "������� �������� ������� �������"
    WS.TYPE = "D"
    GOSUB A.500.CRT

    WS.KEY = "2109999"
    WS.DSCR = "������ ���� ���������"
    WS.TYPE = "T"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2110000"
    WS.DSCR = "���� ����� ����� ���� �� ����� ������� ���"
    WS.TYPE = "HD"
    GOSUB A.500.CRT
*..........................................................
    WS.KEY = "2129999"
    WS.DSCR = "����� ���������� � ���� ���������"
    WS.TYPE = "T"

    GOSUB A.500.CRT
    RETURN
*------------------------------------------------
A.500.CRT:
    R.FIN<CBE.FIN.POS.LIN.NO>  = WS.KEY
    R.FIN<CBE.FIN.POS.BR>  = WS.BR
    WS.NEW.KEY = WS.KEY:"*":WS.BR

    R.FIN<CBE.FIN.POS.DSCRPTION> = WS.DSCR
    R.FIN<CBE.FIN.POS.REC.TYPE> =  WS.TYPE
    R.FIN<CBE.FIN.POS.AMT.LCY>  =  0
    R.FIN<CBE.FIN.POS.AMT.FCY>  =  0
    CALL F.WRITE(FN.FIN,WS.NEW.KEY,R.FIN)
    CALL  JOURNAL.UPDATE(WS.NEW.KEY)
    RETURN
END
