* @ValidationCode : MjoxNTQ0NTk5ODQxOkNwMTI1MjoxNjQwODM5NzY1MTM3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 20:49:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>18</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBH.STMT.NT.STAFF(X.COMP,X.CURRENCY,X.PERIOD)
***    PROGRAM SBH.STMT.NT.STAFF(X.COMP,X.CURRENCY,X.PERIOD)
***    PROGRAM SBH.STMT.NT.STAFF

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SECTOR
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.LEGAL.FORM
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.INDUSTRY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

*-----------------------------------------------------------------------------
    GOSUB GET.PARAMETER
    GOSUB INIT



*-----------------------------------------------------------------------------
    REPORT.ID = 'SBH.STMT.NT.STAFF'
    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD

    GOSUB READ.CUSTOMER.FILE


    XX = SPACE(132)
    XX<1,1>[10,35]   = '����������� ������� ������� :  '
    XX<1,1>[50,20]   = NO.CUST
    PRINT XX<1,1>
    XX = SPACE(132)
    PRINT XX<1,1>


    XX<1,1>[50,40]   = "*****  �������������  ����������   *****"
    PRINT XX<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*----------------------------------------------------------------------------
GET.PARAMETER:



*    X.WS.CY     = '10'
    X.WS.CY     = X.CURRENCY
*    X.WS.PERIOD = 'H'
    X.WS.PERIOD = X.PERIOD
*    WS.COMP     = ID.COMPANY
*    WS.COMP     = "EG0010013"
    WS.COMP     = X.COMP
    WRK.DATE    = TODAY


******   WRK.DATE = 20100815
*-----------------------------------------
    BEGIN CASE
        CASE     X.WS.CY EQ '10'
            H.CY.DES = "����"
            P.CY.DES = "  ������� ������� - ���� ���� ���"
            LOW.AMT  =  200000
            HIGH.AMT =  99999999999

        CASE     X.WS.CY EQ '20'
            H.CY.DES = "�����"
            P.CY.DES = "������� �������� - ����� ������ ���"
            LOW.AMT  =  33300
            HIGH.AMT =  99999999999

    END CASE

*-----------------------



    WRK.YY     = WRK.DATE[1,4]
    WRK.MM     = WRK.DATE[5,2]
    WRK.DD     = WRK.DATE[7,2]

*------------------------
    IF  X.WS.PERIOD EQ 'Q' THEN
        BEGIN CASE
            CASE     WRK.MM GE 01 AND WRK.MM LE 03
                END.YY = WRK.YY - 1
                END.MM = 12
                END.DD = 31
            CASE     WRK.MM GE 04 AND WRK.MM LE 06
                END.YY = WRK.YY
                END.MM = 03
                END.DD = 31
            CASE     WRK.MM GE 07 AND WRK.MM LE 09
                END.YY = WRK.YY
                END.MM = 06
                END.DD = 30
            CASE     WRK.MM GE 10 AND WRK.MM LE 12
                END.YY = WRK.YY
                END.MM = 09
                END.DD = 30
        END CASE
*---------
        END.MM = FMT(END.MM,"R%2")
        END.DD = FMT(END.DD,"R%2")

        END.DATE = END.YY:END.MM:END.DD
        STR.YY = END.YY
        STR.MM = END.MM - 2
**    STR.MM = END.MM - 5

    END
*------------------------------------------------

    IF  X.WS.PERIOD EQ 'H' THEN
        BEGIN CASE
            CASE     WRK.MM GE 01 AND WRK.MM LE 06
                END.YY = WRK.YY - 1
                END.MM = 12
                END.DD = 31

            CASE     WRK.MM GE 07 AND WRK.MM LE 12
                END.YY = WRK.YY
                END.MM = 06
                END.DD = 30

        END CASE
*---------

        END.MM = FMT(END.MM,"R%2")
        END.DD = FMT(END.DD,"R%2")

        END.DATE = END.YY:END.MM:END.DD
        STR.YY = END.YY
***    STR.MM = END.MM - 2
        STR.MM = END.MM - 5

    END
*------------------------------------------------




    STR.DD = "01"
    STR.MM = FMT(STR.MM,"R%2")
    STR.DD = FMT(STR.DD,"R%2")
    STR.DATE = STR.YY:STR.MM:STR.DD
*------------------------
***** CALL CDT('', STR.DATE, "-1C")
*------------------------





RETURN
*----------------------------------------------------------------------------
READ.CUSTOMER.FILE:


    SEL.CUST = "SELECT ":FN.CUST:" WITH"
    SEL.CUST :=" COMPANY.BOOK EQ ":WS.COMP
    SEL.CUST :=" AND SECTOR IN (1100 1300)"
*HARES
*    SEL.CUST :=" AND @ID IN (13101049)"

    SEL.CUST :=" BY @ID"


    CALL EB.READLIST(SEL.CUST,CUST.LIST,'',NO.OF.CUST,ERR.CUST)

    LOOP
        REMOVE Y.CUST.ID FROM CUST.LIST SETTING CUST.POS
    WHILE Y.CUST.ID:CUST.POS
        CALL F.READ(FN.CUST,Y.CUST.ID,R.CUSTOMER,F.CUST,ERR.CUST.ERR)


        Y.SEC.ID  = R.CUSTOMER<EB.CUS.SECTOR>
        Y.IND.ID  = R.CUSTOMER<EB.CUS.INDUSTRY>

*Line [ 225 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,Y.CUST.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,Y.CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

        CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>
        Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>

        Y.CUS.ACC.ID = Y.CUST.ID

        GOSUB READ.CUSTOMER.ACCOUNT.FILE



    REPEAT

RETURN
*----------------------------------------------------------------------------
READ.CUSTOMER.ACCOUNT.FILE:

    DR.AMT    = 0
    CR.AMT    = 0
    T.DR.AMT  = 0
    T.CR.AMT  = 0
    WS.DR.AMT = 0
    WS.CR.AMT = 0



    CALL F.READ(FN.CUS.ACC,Y.CUS.ACC.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)

    NO.OF.ACC = DCOUNT(R.CUS.ACC,@FM)

*-------------------------------
    FOR  Z = 1 TO NO.OF.ACC
        Y.ACC.ID  = R.CUS.ACC<Z>
        Y.ACC.CY  = Y.ACC.ID[9,2]
        Y.ACC.CTG = Y.ACC.ID[11,4]
*---------------

        IF Y.ACC.CTG EQ X.WS.CATEG THEN
            IF Y.ACC.CY EQ X.WS.CY THEN
                GOSUB READ.STMT.ENTRY
            END
        END

*---------------
    NEXT Z


    GOSUB   SELECT.THE.CUST


RETURN
*----------------------------------------------------------------------------
READ.STMT.ENTRY:
    SEL.LIST = ''
    CALL EB.ACCT.ENTRY.LIST(Y.ACC.ID<1>,STR.DATE,END.DATE,SEL.LIST,OPENING.BAL,ER)

*--------------------------------------


    LOOP
        REMOVE REC.ID FROM SEL.LIST SETTING POS.CUST
    WHILE REC.ID:POS.CUST
        CALL F.READ(FN.STMT,REC.ID,R.STMT,F.STMT,ERR.STMT)
****************
        SS.ID = R.STMT<AC.STE.SYSTEM.ID>
        RR.ST = R.STMT<AC.STE.RECORD.STATUS>
****************
        PL.CATEG  = R.STMT<AC.STE.PL.CATEGORY>
        PRD.CATEG = R.STMT<AC.STE.PRODUCT.CATEGORY>
        SS.CY     = R.STMT<AC.STE.CURRENCY>
        CUST.ID   = R.STMT<AC.STE.CUSTOMER.ID>
*--------------------------
        SS.SYS.ID  = R.STMT<AC.STE.SYSTEM.ID>
        SS.OUR.REF = R.STMT<AC.STE.OUR.REFERENCE>:';1'

        FT.CR.CUSTOMER.ID = 0
        FT.DR.CUSTOMER.ID = 0

*-----------------
        IF SS.SYS.ID EQ 'IC2' THEN
            GOTO NEXT.STMT
        END
*-----------------

        IF SS.SYS.ID EQ 'FT' THEN
            CALL F.READ(FN.FT,SS.OUR.REF,R.FT,F.FT,ERR.FT)
            FT.CR.CUSTOMER.ID = R.FT<FT.CREDIT.CUSTOMER>
            FT.DR.CUSTOMER.ID = R.FT<FT.DEBIT.CUSTOMER>
            FT.DR.AMT   = R.FT<FT.AMOUNT.DEBITED>
            FT.CR.AMT   = R.FT<FT.AMOUNT.CREDITED>
            FT.DR.ACCT.NO = R.FT<FT.DEBIT.ACCT.NO>
*Line [ 322 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.TITLE.1,FT.DR.ACCT.NO,ACC.TITL.1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,FT.DR.ACCT.NO,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.TITL.1=R.ITSS.ACCOUNT<AC.ACCOUNT.TITLE.1>
            FT.DR.ACCT.DES = ACC.TITL.1
            FT.VAL.DATE   = R.FT<FT.CREDIT.VALUE.DATE>
*------------
*HARES
*            CALL DBR ('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,SS.OUR.REF,FT.LOCAL.REF)
            FTX.LOCAL.REF = R.FT<FT.LOCAL.REF>

            FT.NOTE.CRT = ''
            FT.NOTE.DRT = ''

            FT.NOTE.CRT  = FTX.LOCAL.REF<1,FTLR.NOTE.CREDIT>
            FT.NOTE.DRT  = FTX.LOCAL.REF<1,FTLR.NOTE.DEBITED>
****           PRINT " FT ":FT.NOTE.CRT:"   ":FT.NOTE.DRT

*------------
            IF  FT.CR.CUSTOMER.ID NE 0 THEN
                IF FT.CR.CUSTOMER.ID EQ FT.DR.CUSTOMER.ID THEN
                    GOTO NEXT.STMT
                END
            END
        END

*--------------------------
        IF SS.ID EQ 'LD' THEN
            GOTO NEXT.STMT
        END
        IF RR.ST EQ 'REVE' THEN
            GOTO NEXT.STMT
        END
****************

        DR.AMT  = 0
        CR.AMT  = 0
****************
*Line [ 351 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.STMT<AC.STE.AMOUNT.LCY>,@VM)

        FOR X = 1 TO YY


            IF X.WS.CY EQ '10' THEN
                AMT = R.STMT<AC.STE.AMOUNT.LCY,1,X>
*-----------------
*HARES
*                IF AMT GT 0 THEN
*                    IF SS.SYS.ID EQ 'FT' THEN
*                        PRINT SS.OUR.REF:"  ":SS.SYS.ID:"   ":"  ":FT.DR.ACCT.NO:"  ":FT.DR.ACCT.DES:"  ":FT.VAL.DATE:"     ":AMT
*                        PRINT FT.NOTE.CRT:"  ":FT.NOTE.DRT
*                        PRINT "_____________________________________________________________________"
*                    END
*                END
*-----------------
            END

            IF X.WS.CY EQ '20' THEN
                AMT = R.STMT<AC.STE.AMOUNT.FCY,1,X>
            END


            IF    AMT LT 0    THEN
                DR.AMT  = DR.AMT + AMT
            END
            IF    AMT GT 0    THEN
                CR.AMT  = CR.AMT + AMT
            END

        NEXT X
****************
        T.DR.AMT   += DR.AMT
        T.CR.AMT   += CR.AMT


NEXT.STMT:

    REPEAT



RETURN
*----------------------------------------------------------------------------
SELECT.THE.CUST:

    WS.DR.AMT = ABS(T.DR.AMT)
    WS.CR.AMT = ABS(T.CR.AMT)

    IF WS.DR.AMT GE LOW.AMT  THEN
        IF WS.DR.AMT LT HIGH.AMT THEN
            GOSUB PRINT.ONE.LINE
            GO TO END.SEL.CUST
        END
    END


    IF WS.CR.AMT GE LOW.AMT  THEN
        IF WS.CR.AMT LT HIGH.AMT THEN
            GOSUB PRINT.ONE.LINE
        END
    END

END.SEL.CUST:
RETURN
*-------------------------------------------------------------------------
PRINT.ONE.LINE:

    Y.LGL.ID = LOCAL.REF<1,CULR.LEGAL.FORM>

    CALL F.READ(FN.SEC,Y.SEC.ID,R.SECTOR,F.SEC,ERR.SEC)
    SEC.NAME = R.SECTOR<EB.SEC.DESCRIPTION,2>


    CALL F.READ(FN.IND,Y.IND.ID,R.INDUSTRY,F.IND,ERR.IND)
    IND.NAME = R.INDUSTRY<EB.IND.DESCRIPTION,2>

    CALL F.READ(FN.LGL,Y.LGL.ID,R.LEGAL,F.LGL,ERR.LGL)
    LGL.NAME = R.LEGAL<LEG.DESCRIPTION,2>
*---------------
    GOSUB WRITE.LINE.01

*---------------
RETURN

*-----------------------------------------------------------------------
INIT:
    PGM.NAME = "SBH.STMT.NT.STAFF"
    SAVE.CUST  = 0
*------------------------
    CUST.TYPE = "    �������� ����������  "

*------------

    X.WS.CATEG = '1001'

    H.FROM = "��"
    H.TO     = "  - ��� ��� �� "

* -----------
* -----------

    NO.CUST = 0

*------------------------

*----------------------------------------------------------------------
    P.DATE   = FMT(TODAY,"####/##/##")
    P.STR.DATE   = FMT(STR.DATE,"####/##/##")
    P.END.DATE   = FMT(END.DATE,"####/##/##")
*------------------------




    P.LOW.AMT = FMT(LOW.AMT,"15L,")
    P.HIGH.AMT = FMT(HIGH.AMT,"15L,")

*------------------------
    FN.STMT = "FBNK.STMT.ENTRY"
    F.STMT = ''
    R.STMT=''
    Y.STMT=''
    Y.STMT.ERR=''
*------------------------
    CALL OPF(FN.STMT,F.STMT)
*------------------------
    FN.FT = "FBNK.FUNDS.TRANSFER$HIS"
    F.FT = ''
    R.FT=''
    Y.FT=''
    Y.FT.ERR=''
*------------------------
    CALL OPF(FN.FT,F.FT)
*------------------------
    FN.CUST = "FBNK.CUSTOMER"
    F.CUST  = ""
    R.CUSTOMER = ""
    Y.CUST.ID   = ""
*-------------------------------
    CALL OPF(FN.CUST,F.CUST)
*-------------------------------
    FN.SEC = "FBNK.SECTOR"
    F.SEC  = ""
    R.SECTOR = ""
    Y.SEC.ID   = ""
*-------------------------------
    CALL OPF(FN.SEC,F.SEC)
*-------------------------------
    FN.LGL = "F.SCB.CUS.LEGAL.FORM"
    F.LGL  = ""
    R.LEGAL = ""
    Y.LGL.ID   = ""
*-------------------------------
    CALL OPF(FN.LGL,F.LGL)
*-------------------------------
    FN.IND = "FBNK.INDUSTRY"
    F.IND  = ""
    R.INDUSTRY = ""
    Y.IND.ID   = ""
*-------------------------------
    CALL OPF(FN.IND,F.IND)
*-------------------------------
    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT"
    F.CUS.ACC  = ""
    R.CUS.ACC = ""
    Y.CUS.ACC.ID   = ""
*-------------------------------
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)
*-------------------------------
    FN.ACC = "FBNK.ACCOUNT"
    F.ACC  = ""
    R.ACC = ""
    Y.ACC.ID   = ""
*-------------------------------
    CALL OPF(FN.ACC,F.ACC)
*-------------------------------

    T.DR.AMT   = 0
    T.CR.AMT   = 0





RETURN
*------------------------------------------------------------------------
WRITE.LINE.01:

    XX = SPACE(132)
* ---------------------------------------------
*                                         ����� �������
    Y.CUST.ID = (((Y.CUST.ID * 2) + 11100222) * 3)

    IF X.WS.CY EQ 10 THEN
        CUST.NAME = STR('�',30)
    END

    IF X.WS.CY EQ 20 THEN
        CUST.NAME = STR('E',30)
    END

    CUST.NAME = STR('X',30)
* ---------------------------------------------

    XX<1,1>[1,12]     = Y.CUST.ID
    XX<1,1>[15,30]   = CUST.NAME


*    XX<1,1>[50,20]   = SEC.NAME
*    XX<1,1>[50,20]   = LGL.NAME
    XX<1,1>[50,20]   = IND.NAME

    P.DR.AMT  = FMT(T.DR.AMT,"15L2,")
    P.CR.AMT  = FMT(T.CR.AMT,"15L2,")
    T.NT.AMT  = T.CR.AMT + T.DR.AMT
    P.NT.AMT  = FMT(T.NT.AMT,"15L2,")

    XX<1,1>[75,15]   = P.DR.AMT
    XX<1,1>[95,15]  = P.CR.AMT
    XX<1,1>[115,15]  = P.NT.AMT
    PRINT XX<1,1>

    NO.CUST = NO.CUST + 1

    PR.LINE = STR('_',132)
    PRINT PR.LINE

    NO.OF.LINE = NO.OF.LINE + 1
    IF NO.OF.LINE GT 39  THEN
        GOSUB PRINT.HEAD
    END

RETURN
*===============================================================
PRINT.HEAD:
*Line [ 601 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH


    T.DAY = P.DATE
    PR.HD ="'L'":SPACE(1):"��� ���� ������ "  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):"������� : ":T.DAY:SPACE(86):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(11):SPACE(100):PGM.NAME
    PR.HD :="'L'":SPACE(50):"������ ������ ������� �� ������� ���� ��� �������"
***    PR.HD :="'L'":SPACE(55):"������� �� �������":" ":CUST.TYPE
    PR.HD :="'L'":SPACE(65):CUST.TYPE
***    PR.HD :="'L'":SPACE(50):"�� ":P.LOW.AMT:" �� ":"  ��� ��� �� ":P.HIGH.AMT:" �� "
    PR.HD :="'L'":SPACE(50):H.FROM:" ":P.LOW.AMT:" ":H.CY.DES:" ":H.TO:" ":P.HIGH.AMT:" ":H.CY.DES

    PR.HD :="'L'":SPACE(57):P.CY.DES
    PR.HD :="'L'":SPACE(55):"������ �� ":P.STR.DATE:"   ��� ":P.END.DATE

    PR.HD :="'L'":SPACE(50):STR('_',50)
    PR.HD :="'L'":" "
***********************************************************
    PR.HD :="'L'":SPACE(1)
    PR.HD :="'L'":SPACE(0):" ��� ������" :SPACE(11):" ��� ������ " :SPACE(15):"����� ��������":SPACE(11):"������ ���������":SPACE(4):"������ ����������"
    PR.HD :="'L'":SPACE(74):"�������             �������":SPACE(13):"������"
    PR.HD :="'L'":STR('_',132)
***********************
    HEADING PR.HD
    NO.OF.LINE = 0
RETURN



END
