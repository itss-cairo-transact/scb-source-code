* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.ACCT.DRMNT.R(ENQ.DATA)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON

*-----------------------------------
    SYS.DATE   = TODAY
    CALL CDT('', SYS.DATE, '-365C')
    AGE.DATE   = SYS.DATE
*------------------------
    ENQ.NAME   = ENQ.DATA<1.1>
    OPER.CODE  = "LE"
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,1> = "DATE.LAST.UPDATE"
        ENQ.DATA<3,1> = OPER.CODE
        ENQ.DATA<4,1> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,2> = "DATE.LAST.CR.CUST"
        ENQ.DATA<3,2> = OPER.CODE
        ENQ.DATA<4,2> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,3> = "DATE.LAST.CR.AUTO"
        ENQ.DATA<3,3> = OPER.CODE
        ENQ.DATA<4,3> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,4> = "DATE.LAST.CR.BANK"
        ENQ.DATA<3,4> = OPER.CODE
        ENQ.DATA<4,4> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,5> = "DATE.LAST.DR.CUST"
        ENQ.DATA<3,5> = OPER.CODE
        ENQ.DATA<4,5> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,6> = "DATE.LAST.DR.AUTO"
        ENQ.DATA<3,6> = OPER.CODE
        ENQ.DATA<4,6> = AGE.DATE
    END
*------------------------
    IF  ENQ.NAME EQ "SBM.ACCT.DRMNT" THEN
        ENQ.DATA<2,7> = "DATE.LAST.DR.BANK"
        ENQ.DATA<3,7> = OPER.CODE
        ENQ.DATA<4,7> = AGE.DATE
    END
*-----------------------------------
    RETURN
END
