* @ValidationCode : MjotMTg0OTc3NDQzNDpDcDEyNTI6MTY0MDgxNjc2NzkyNjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 14:26:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*------------------------
*** Create By Nessma ***
*Line [ 19 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
SUBROUTINE SBD.CALC.SAV.BAL.NULL.ALL
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_COMMON
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_EQUATE
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_USER.ENV.COMMON
*Line [ 26 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.RE.BASE.CCY.PARAM
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CUSTOMER
*Line [ 34 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CURRENCY
*Line [ 36 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.COMPANY
*Line [ 38 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CATEGORY
*Line [ 40 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.NUMERIC.CURRENCY
*Line [ 42 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_CU.LOCAL.REFS
*---------------------------------------------------
    COMP.ID = ID.COMPANY
    GOSUB INTIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS

    GOSUB PRINT.TOT
    GOSUB LINE.END

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = "DONE"  ; CALL REM
RETURN
*----------------------------------------------------------
LINE.END:
*--------
    PRINT " "
    PRINT SPACE(50) : "********* ����� �������  *********"
RETURN
*----------------------------------------------------------
INTIAL:
*------
    REPORT.ID = 'SBM.AC.TOTALS'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.AC  = 'FBNK.ACCOUNT'        ;  F.AC  = ''
    CALL OPF(FN.AC,F.AC)

    FN.AC.H  = 'FBNK.ACCOUNT$HIS'        ;  F.AC.H  = ''
    CALL OPF(FN.AC.H,F.AC.H)

    FN.CUS = 'FBNK.CUSTOMER'       ;  F.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.COMP = 'F.COMPANY'          ;  F.COMP  = ''
    CALL OPF(FN.COMP,F.COMP)

    FN.CCC  = 'FBNK.CURRENCY'      ;  F.CCC  = ''
    CALL OPF(FN.CCC,F.CCC)

    FN.CNUM  = 'F.NUMERIC.CURRENCY'   ;  F.CNUM  = ''
    CALL OPF(FN.CNUM,F.CNUM)

    FN.CUR  = 'FBNK.CURRENCY'         ; F.CUR  = ''
    CALL OPF(FN.CUR,F.CUR)

    ENQ.LP   = 0
    CUR.RATE = ""
    COMP.OLD = ""
    COMP.NEW = ""
    CATEG    = ""
    CUR      = ""
    TOT.NO   = 0
    TOT.BAL  = 0
    BAL      = 0
    BREAK.2  = ""
    BREAK.1  = ""
    TOTAL.BANK = 0

    XX = ""
    YY = ""
    ZZ = ""
    NN = 1

    TOT.NO.CUR    = 0
    TOT.BL.CUR    = 0
    TOT.NO.CATEG  = 0
    TOT.BL.CATEG  = 0

    TOT.NO.BRN = 0
    CATEGORY.LIST = ""

RETURN
*---------------------------------------------------
PROCESS:
*--------
    T.SEL   = "SELECT ":FN.AC:" WITH CATEGORY"
    T.SEL  := " IN ( 6501 6502 6503 6504 6511 )"
    T.SEL  := " AND OPEN.ACTUAL.BAL EQ ''"
    T.SEL  := " AND CO.CODE EQ " : COMP.ID
    T.SEL  := " BY CO.CODE BY CATEGORY BY CURRENCY"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E.AC)
            BAL  = R.AC<AC.OPEN.ACTUAL.BAL>
            CUR  = R.AC<AC.CURRENCY>
            CUS  = R.AC<AC.CUSTOMER>
            CATG = R.AC<AC.CATEGORY>
            CUR.CODE = KEY.LIST<I>[9,2]

            COMP.NEW  = R.AC<AC.CO.CODE>
            BREAK.2   = COMP.NEW

            IF I EQ 1 THEN
                BREAK.1 = BREAK.2
*                PRINT "-------------------------"
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.NEW,COMP.NAME)
*                PRINT "���  " : COMP.NAME
*                PRINT "-------------------------"
            END

            GOSUB PRINT.LINE.ALL

            IF BREAK.1 EQ BREAK.2 THEN
                IF CUR EQ 'EGP' THEN
                    TOTAL.BANK ++
                    TOT.NO++
                END ELSE
                    TOTAL.BANK ++
                    TOT.NO++
                END
            END ELSE
*-------------------------------------
                GOSUB PRINT.LINE
*-------------------------------------
*               PRINT "-------------------------"
*               CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BREAK.2,COMP.NAME)
*               PRINT "���  " : COMP.NAME
*               PRINT "-------------------------"

                IF CUR EQ 'EGP' THEN
                    TOT.NO++
                    TOTAL.BANK ++
                END ELSE
                    TOTAL.BANK ++
                    TOT.NO++
                END
            END

            BREAK.1 = BREAK.2
        NEXT I
    END ELSE
        PRINT "NO DATA"
    END
RETURN
*-------------------------------------------------------
PRINT.LINE.ALL:
*--------------
    RR = ""
    FF = ""
    INPP.NAME = ""
    AUTH.NAME = ""
*Line [ 188 ] INITIALIZE "EB.USE.DEPARTMENT.CODE"  - ITSS - R21 Upgrade - 2021-12-23
    EB.USE.USER.NAME=""
    RR<1,1>[1,20] = KEY.LIST<I>
*Line [ 191 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    RR<1,1>[20,30] = LOCAL.REF<1,CULR.ARABIC.NAME>

*Line [ 200 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,CATG,CATEG.NN)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,CATG,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.NN=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
    RR<1,1>[55,20] = CATEG.NN

*Line [ 209 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,R.AC<AC.CURRENCY>,CUR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,R.AC<AC.CURRENCY>,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
    RR<1,1>[85,20] = CUR.NAME

    IF R.AC<AC.CURR.NO> EQ 1 THEN
        DAT.TIME = R.AC<AC.DATE.TIME><1,1>
        IF DAT.TIME THEN
            DAT.TIME = "20":DAT.TIME[1,6]
            DAT.TIME = FMT(DAT.TIME,"####/##/##")
        END

        INPP = R.AC<AC.INPUTTER><1,1>
        AUTH = R.AC<AC.AUTHORISER><1,1>
    END

    IF R.AC<AC.CURR.NO> GT 1 THEN
        ACCT.ID = KEY.LIST<I>:";1"
        CALL F.READ(FN.AC.H,ACCT.ID,R.AC.H,F.AC.H,E.AC.H)

        DAT.TIME = R.AC.H<AC.DATE.TIME><1,1>
        IF DAT.TIME THEN
            DAT.TIME = "20":DAT.TIME[1,6]
            DAT.TIME = FMT(DAT.TIME,"####/##/##")
        END

        INPP = R.AC.H<AC.INPUTTER><1,1>
        AUTH = R.AC.H<AC.AUTHORISER><1,1>
    END

    INPP.XX = FIELD(INPP, "OFS" ,2)
    AUTH.XX = FIELD(AUTH, "OFS" ,2)

    IF INPP.XX THEN
        INPP.NAME =  "����� ���"
    END ELSE
        INPPX = FIELD(INPP, '_' ,2)
*Line [ 250 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('USER':@FM:EB.USE.USER.NAME,INPPX,INPP.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,INPPX,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
INPP.NAME=R.ITSS.USER<EB.USE.USER.NAME>
    END

    IF AUTH.XX THEN
        AUTH.NAME =  "����� ���"
    END ELSE
        AUTH = FIELD(AUTH, '_' ,2)
*Line [ 263 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('USER':@FM:EB.USE.USER.NAME,AUTH,AUTH.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,AUTH,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
AUTH.NAME=R.ITSS.USER<EB.USE.USER.NAME>
    END

    RR<1,1>[105,15] = DAT.TIME
    RR<1,1>[118,10] = INPP.NAME
    FF<1,1>[118,10] = AUTH.NAME

    PRINT RR<1,1>
    PRINT FF<1,1>
    PRINT " "
    RR = ""
    FF = ""
RETURN
*-------------------------------------------------------
PRINT.LINE:
*----------
    LINE = ""

    COMP.ID = FIELD(BREAK.1 ,"*", 1)
*Line [ 288 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP.ID,COMP.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP.ID,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
COMP.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    LINE<1,1>[1,20]  = "������ ���  " :SPACE(5) : COMP.NAME
    LINE<1,1>[35,20] = TOT.NO

    IF TOT.NO GT 0 THEN
        PRINT STR('_',40)
        PRINT LINE<1,1>
        PRINT STR('_',40)
    END

    TOT.NO  = 0
    TOT.BAL = 0
RETURN
*-------------------------------------------------------
PRINT.HEAD:
*-----------
*Line [ 275 ] INITIALIZE "EB.USE.DEPARTMENT.CODE"  - ITSS - R21 Upgrade - 2021-12-23
    EB.USE.DEPARTMENT.CODE=""
*Line [ 313 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  = "'L'":SPACE(1):" ��� ���� ������"
    PR.HD := "'L'":SPACE(1):"����� �������"
    PR.HD := T.DAY:SPACE(85):"��� ������ : ":"'P'"
*Line [ 273 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    PR.HD := "'L'":SPACE(109):"SBD.CALC.SAV.BAL.NULL.ALL"
    PR.HD := "'L'"
    PR.HD := "'L'":SPACE(50):"������� ������ ������ ������ �������"
    PR.HD := "'L'":SPACE(60):"����� �������"
    PR.HD := "'L'":SPACE(45):STR('-',50)
    PR.HD := "'L'": " "

    XX.HD = ""
    YY.HD = ""

    XX.HD<1,1>[1,20]   = "���  ������"
    XX.HD<1,1>[20,30]  = "��� ������"
    XX.HD<1,1>[55,20]  = "��� ������"
    XX.HD<1,1>[85,20]  = "������"
    XX.HD<1,1>[105,15] = "����� �����"
    XX.HD<1,1>[122,10] = "����������"
    YY.HD<1,1>[122,10] = "����������"

    PR.HD := "'L'" : XX.HD<1,1>
    PR.HD := "'L'" : YY.HD<1,1>
    PR.HD := "'L'" : STR('-',120)
    PR.HD := "'L'": " "

    PRINT
    HEADING PR.HD
RETURN
*------------------------------------------------------------------------
PRINT.TOT:
*---------
    PRINT " "
    PRINT STR('_',120)
    PRINT SPACE(50): "������ ����� " : SPACE(10) : TOTAL.BANK
    PRINT STR('_',120)
    PRINT " "

RETURN
*-------------------------------------------------------
END
