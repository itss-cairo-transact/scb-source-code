* @ValidationCode : MjotMTY0MDY3NDQ2MDpDcDEyNTI6MTY0MDgyOTk2NzY4NDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:06:07
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>28</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.MLAD.PROG.102.EXCEL
*    PROGRAM    SBD.MLAD.PROG.102.EXCEL

*   ------------------------------
    $INSERT    I_COMMON
    $INSERT    I_EQUATE
    $INSERT    I_F.USER
    $INSERT    I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MLAD.FILE.020
    $INSERT  I_F.COMPANY
    $INSERT  I_USER.ENV.COMMON

    OPENSEQ "&SAVEDLISTS&" , "SBD.MLAD.PROG.102.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBD.MLAD.PROG.102.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBD.MLAD.PROG.102.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBD.MLAD.PROG.102 CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBD.MLAD.PROG.102 File IN &SAVEDLISTS&'
        END
    END

    GOSUB INIT

*    IF PROGRAM.ID EQ "SBD.MLAD.PROG.102"  THEN
    WS.COMP = ID.COMPANY
    REPORT.ID = 'P.FUNCTION'
*    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB OPENFILES
*-------------------------------
    GOSUB PRINT.HEAD

    GOSUB READ.MLAD.FILE.020
*-------------------------------
    GOSUB MOVE.LINE.TO.PRINT
    SAVE.ASST = FILE.ASST
    GOSUB END.PART.TWO
*-------------------------------
*    CALL PRINTER.OFF
*    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*    END
*-----------------------------------------------------------------------------


RETURN
*-------------------------------------------------------------------------
INIT:


    PROGRAM.ID = "SBD.MLAD.PROG.102"


    FN.MLAD.020 = "F.SCB.MLAD.FILE.020"
    F.MLAD.020  = ""
    R.MLAD.020  = ""
    Y.MLAD.020.ID   = ""

*----------------------------------
    FN.CURRENCY = 'FBNK.CURRENCY'
    F.CURRENCY  = ''
    R.CURRENCY = ''
    Y.CURRENCY.ID = ''
    CALL OPF(FN.CURRENCY,F.CURRENCY)



*---------------------------------------
    SYS.DATE = TODAY
    P.TIME    = TIMEDATE()[1,8]

    SYS.YYMM = SYS.DATE[1,6]

    WRK.DATE = SYS.DATE
    P.DATE   = FMT(SYS.DATE,"####/##/##")

*---------------------------------------
    WRK.YY   = WRK.DATE[1,4]
    WRK.MM   = WRK.DATE[5,2]
    WRK.DD   = WRK.DATE[7,2]
*---------------------------------------
    DIM ARY.X(12)
    ARY.X(1) = "������"
    ARY.X(2) = "������"
    ARY.X(3) = "������"
    ARY.X(4) = "������"
    ARY.X(5) = "������"
    ARY.X(6) = "������"
    ARY.X(7) = "������"
    ARY.X(8) = "������"
    ARY.X(9) = "������"
    ARY.X(10) = "������"
    ARY.X(11) = "������"
    ARY.X(12) = "������"
    MON = ARY.X(WRK.MM)

*-----------------------
    LINE.NO = 0
    MAX.LINE.NO = 30
*-----------------------
    X.CY = "EGP"


RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLAD.020,F.MLAD.020)

RETURN
*--------------------------------------------------------------------------
READ.MLAD.FILE.020:


    SAVE.CY = ''
    SAVE.AS = ''
    SAVE.ASST = ''

    SEL.MLAD.020  = "SELECT ":FN.MLAD.020
    SEL.MLAD.020 := " WITH CURRENCY NE ":X.CY
***    SEL.MLAD.020 := " AND  CURRENCY EQ 'USD'"
    SEL.MLAD.020 := " BY ASST.TYPE"


    CALL EB.READLIST(SEL.MLAD.020,SEL.LIST.MLAD.020,'',NO.OF.MLAD.020,ERR.MLAD.020)
    LOOP
        REMOVE Y.MLAD.020.ID FROM SEL.LIST.MLAD.020 SETTING POS.MLAD.020
    WHILE Y.MLAD.020.ID:POS.MLAD.020
        CALL F.READ(FN.MLAD.020,Y.MLAD.020.ID,R.MLAD.020,F.MLAD.020,ERR.MLAD.020)


        N.CURR   = R.MLAD.020<ML020.CURR.NAME>
        N.DESCR  = R.MLAD.020<ML020.LINE.DESCR>
        N.RATE   = R.MLAD.020<ML020.RATE>

        N.DAY    = R.MLAD.020<ML020.NEXT.DAY>       * N.RATE
        N.WEEK   = R.MLAD.020<ML020.NEXT.WEEK>      * N.RATE
        N.MONTH  = R.MLAD.020<ML020.NEXT.MONTH>     * N.RATE
        N.3MONTH = R.MLAD.020<ML020.NEXT.3MONTH>    * N.RATE
        N.6MONTH = R.MLAD.020<ML020.NEXT.6MONTH>    * N.RATE
        N.YEAR   = R.MLAD.020<ML020.NEXT.YEAR>      * N.RATE
        N.3YEAR  = R.MLAD.020<ML020.NEXT.3YEAR>     * N.RATE
        N.MORE   = R.MLAD.020<ML020.NEXT.MORE>      * N.RATE

        M.DAY    = FMT((N.DAY)/1000,"L0")
        M.WEEK   = FMT((N.WEEK)/1000,"L0")
        M.MONTH  = FMT((N.MONTH)/1000,"L0")
        M.3MONTH = FMT((N.3MONTH)/1000,"L0")
        M.6MONTH = FMT((N.6MONTH)/1000,"L0")
        M.YEAR   = FMT((N.YEAR)/1000,"L0")
        M.3YEAR  = FMT((N.3YEAR)/1000,"L0")
        M.MORE   = FMT((N.MORE)/1000,"L0")
        M.TOTAL  = M.DAY + M.WEEK + M.MONTH + M.3MONTH + M.6MONTH + M.YEAR + M.3YEAR + M.MORE

* -------------------------------------
        FILE.CY = FIELD(Y.MLAD.020.ID,'-',1,1)
        FILE.AS = FIELD(Y.MLAD.020.ID,'-',2,1)
        FILE.ASST = Y.MLAD.020.ID[5,5]

* -------------------------------------
        IF SAVE.ASST EQ '' THEN
            GOSUB MOVE.ZEROS
            SAVE.ASST = FILE.ASST
            SAVE.AS   = FILE.AS
        END
* -------------------------------------
        IF SAVE.AS NE FILE.AS THEN
            GOSUB MOVE.LINE.TO.PRINT
            SAVE.ASST = FILE.ASST
            GOSUB END.PART.ONE
            SAVE.AS = FILE.AS
        END
* -------------------------------------
        IF SAVE.ASST NE FILE.ASST THEN
            GOSUB MOVE.LINE.TO.PRINT
            SAVE.ASST = FILE.ASST
        END
* -------------------------------------
        TM.DAY    += M.DAY
        TM.WEEK   += M.WEEK
        TM.MONTH  += M.MONTH
        TM.3MONTH += M.3MONTH
        TM.6MONTH += M.6MONTH
        TM.YEAR   += M.YEAR
        TM.3YEAR  += M.3YEAR
        TM.MORE   += M.MORE
        TM.TOTAL  += M.TOTAL
        TM.DESCR   = N.DESCR
*---------------
        GOSUB CALC.TOTAL.X.Y:
*---------------
    REPEAT


RETURN
*--------------------------------------------------------------------------
MOVE.LINE.TO.PRINT:

    P.DAY    = TM.DAY
    P.WEEK   = TM.WEEK
    P.MONTH  = TM.MONTH
    P.3MONTH = TM.3MONTH
    P.6MONTH = TM.6MONTH
    P.YEAR   = TM.YEAR
    P.3YEAR  = TM.3YEAR
    P.MORE   = TM.MORE
    P.TOTAL  = TM.TOTAL
    P.DESCR  = TM.DESCR
    GOSUB PRINT.ONE.LINE
    GOSUB MOVE.ZEROS.TM
RETURN
*--------------------------------------------------------------------------
CALC.TOTAL.X.Y:
* -------------------------------------
    IF FILE.AS EQ 'X' THEN
        X.DAY    += M.DAY
        X.WEEK   += M.WEEK
        X.MONTH  += M.MONTH
        X.3MONTH += M.3MONTH
        X.6MONTH += M.6MONTH
        X.YEAR   += M.YEAR
        X.3YEAR  += M.3YEAR
        X.MORE   += M.MORE
        X.TOTAL  += M.TOTAL
    END
* -------------------------------------
    IF FILE.AS EQ 'Y' THEN
        Y.DAY    += M.DAY
        Y.WEEK   += M.WEEK
        Y.MONTH  += M.MONTH
        Y.3MONTH += M.3MONTH
        Y.6MONTH += M.6MONTH
        Y.YEAR   += M.YEAR
        Y.3YEAR  += M.3YEAR
        Y.MORE   += M.MORE
        Y.TOTAL  += M.TOTAL
    END

RETURN
*--------------------------------------------------------------------------
MOVE.ZEROS:
    X.DAY    = 0
    X.WEEK   = 0
    X.MONTH  = 0
    X.3MONTH = 0
    X.6MONTH = 0
    X.YEAR   = 0
    X.3YEAR  = 0
    X.MORE   = 0
    X.TOTAL  = 0

    Y.DAY    = 0
    Y.WEEK   = 0
    Y.MONTH  = 0
    Y.3MONTH = 0
    Y.6MONTH = 0
    Y.YEAR   = 0
    Y.3YEAR  = 0
    Y.MORE   = 0
    Y.TOTAL  = 0

    GOSUB MOVE.ZEROS.TM

RETURN
*--------------------------------------------------------------------------
MOVE.ZEROS.TM:

    TM.DAY    = 0
    TM.WEEK   = 0
    TM.MONTH  = 0
    TM.3MONTH = 0
    TM.6MONTH = 0
    TM.YEAR   = 0
    TM.3YEAR  = 0
    TM.MORE   = 0
    TM.TOTAL  = 0

RETURN
*--------------------------------------------------------------------------
END.PART.ONE:

    P.DAY    = X.DAY
    P.WEEK   = X.WEEK
    P.MONTH  = X.MONTH
    P.3MONTH = X.3MONTH
    P.6MONTH = X.6MONTH
    P.YEAR   = X.YEAR
    P.3YEAR  = X.3YEAR
    P.MORE   = X.MORE
    P.TOTAL  = X.TOTAL

    P.DESCR = "������ ������)1("

    GOSUB PRINT.ONE.LINE


    SAVE.AS = FILE.AS
RETURN
*--------------------------------------------------------------------------
END.PART.TWO:

    P.DAY    = Y.DAY
    P.WEEK   = Y.WEEK
    P.MONTH  = Y.MONTH
    P.3MONTH = Y.3MONTH
    P.6MONTH = Y.6MONTH
    P.YEAR   = Y.YEAR
    P.3YEAR  = Y.3YEAR
    P.MORE   = Y.MORE
    P.TOTAL  = Y.TOTAL

    P.DESCR = "������ ����������)2("

    GOSUB PRINT.ONE.LINE


* ------------------------------------------------------------------------

    P.DAY    = X.DAY      - Y.DAY
    P.WEEK   = X.WEEK     - Y.WEEK
    P.MONTH  = X.MONTH    - Y.MONTH
    P.3MONTH = X.3MONTH   - Y.3MONTH
    P.6MONTH = X.6MONTH   - Y.6MONTH
    P.YEAR   = X.YEAR     - Y.YEAR
    P.3YEAR  = X.3YEAR    - Y.3YEAR
    P.MORE   = X.MORE     - Y.MORE
    P.TOTAL  = X.TOTAL    - Y.TOTAL


* -----------------------
    GOSUB SAVE.OLD.VALUE
* -----------------------


    P.DESCR = "������ ������� )3(=)1( - )2("


* -----------------------
    GOSUB PRINT.ONE.LINE
* -----------------------

* ------------------------------------------------------------------------
**     ������ ���������

    J.DAY    = J.DAY
    J.WEEK   = J.WEEK     + J.DAY
    J.MONTH  = J.MONTH    + J.WEEK
    J.3MONTH = J.3MONTH   + J.MONTH
    J.6MONTH = J.6MONTH   + J.3MONTH
    J.YEAR   = J.YEAR     + J.6MONTH
    J.3YEAR  = J.3YEAR    + J.YEAR
    J.MORE   = J.MORE     + J.3YEAR
    J.TOTAL  = J.TOTAL    + J.MORE


    P.DAY    = J.DAY
    P.WEEK   = J.WEEK
    P.MONTH  = J.MONTH
    P.3MONTH = J.3MONTH
    P.6MONTH = J.6MONTH
    P.YEAR   = J.YEAR
    P.3YEAR  = J.3YEAR
    P.MORE   = J.MORE
    P.TOTAL  = J.TOTAL


    P.DESCR = "������ ���������"

* ----------------------
    GOSUB PRINT.ONE.LINE
* ----------------------



***    RETURN

* ------------------------------------------------------------------------
**     ���� ������ ������� ��� ������ ����������


    K.DAY    = K.DAY      / Y.TOTAL * 100
    K.WEEK   = K.WEEK     / Y.TOTAL * 100
    K.MONTH  = K.MONTH    / Y.TOTAL * 100
    K.3MONTH = K.3MONTH   / Y.TOTAL * 100
    K.6MONTH = K.6MONTH   / Y.TOTAL * 100
    K.YEAR   = K.YEAR     / Y.TOTAL * 100
    K.3YEAR  = K.3YEAR    / Y.TOTAL * 100
    K.MORE   = K.MORE     / Y.TOTAL * 100
    K.TOTAL  = K.TOTAL    / Y.TOTAL * 100


    P.DAY    = K.DAY
    P.WEEK   = K.WEEK
    P.MONTH  = K.MONTH
    P.3MONTH = K.3MONTH
    P.6MONTH = K.6MONTH
    P.YEAR   = K.YEAR
    P.3YEAR  = K.3YEAR
    P.MORE   = K.MORE
    P.TOTAL  = K.TOTAL

***    P.DESCR = "���� ������ ������� ��� �. ����������"
***    P.DESCR = "�.���� ����� �� �. ��������"


    P.DESCR = "���� ���� ����� ����������"
* ----------------------
    GOSUB PRINT.CENT.LINE

* ------------------------------------------------------------------------
**     ���� ������ ��������� ��� ������ ����������

    J.DAY    = J.DAY      / Y.TOTAL * 100
    J.WEEK   = J.WEEK     / Y.TOTAL * 100
    J.MONTH  = J.MONTH    / Y.TOTAL * 100
    J.3MONTH = J.3MONTH   / Y.TOTAL * 100
    J.6MONTH = J.6MONTH   / Y.TOTAL * 100
    J.YEAR   = J.YEAR     / Y.TOTAL * 100
    J.3YEAR  = J.3YEAR    / Y.TOTAL * 100
    J.MORE   = J.MORE     / Y.TOTAL * 100
    J.TOTAL  = J.TOTAL    / Y.TOTAL * 100


    P.DAY    = J.DAY
    P.WEEK   = J.WEEK
    P.MONTH  = J.MONTH
    P.3MONTH = J.3MONTH
    P.6MONTH = J.6MONTH
    P.YEAR   = J.YEAR
    P.3YEAR  = J.3YEAR
    P.MORE   = J.MORE
    P.TOTAL  = J.TOTAL

***    P.DESCR = "���� ������ ��������� ��� �.����������"
***    P.DESCR = "�.���� ������� �� �.��������"


    P.DESCR = "���� ���� ������� ����������"


* ----------------------
    GOSUB PRINT.CENT.LINE

* ------------------------------------------------------------------------

    SAVE.CY = FILE.CY
    SAVE.AS = FILE.AS

    GOSUB MOVE.ZEROS


RETURN
*--------------------------------------------------------------------------
SAVE.OLD.VALUE:


**                    �������� ���� ����� �� ���� ���
    J.DAY    = 0
    J.WEEK   = 0
    J.MONTH  = 0
    J.3MONTH = 0
    J.6MONTH = 0
    J.YEAR   = 0
    J.3YEAR  = 0
    J.MORE   = 0
    J.TOTAL  = 0

    K.DAY    = 0
    K.WEEK   = 0
    K.MONTH  = 0
    K.3MONTH = 0
    K.6MONTH = 0
    K.YEAR   = 0
    K.3YEAR  = 0
    K.MORE   = 0
    K.TOTAL  = 0

**                    �������� ���� ����� �� ���� ���
    J.DAY    = P.DAY
    J.WEEK   = P.WEEK
    J.MONTH  = P.MONTH
    J.3MONTH = P.3MONTH
    J.6MONTH = P.6MONTH
    J.YEAR   = P.YEAR
    J.3YEAR  = P.3YEAR
    J.MORE   = P.MORE
    J.TOTAL  = P.TOTAL

    K.DAY    = P.DAY
    K.WEEK   = P.WEEK
    K.MONTH  = P.MONTH
    K.3MONTH = P.3MONTH
    K.6MONTH = P.6MONTH
    K.YEAR   = P.YEAR
    K.3YEAR  = P.3YEAR
    K.MORE   = P.MORE
    K.TOTAL  = P.TOTAL


RETURN
*--------------------------------------------------------------------------
PRINT.ONE.LINE:

    BB.DATA = ""
    BB.DATA     = P.DESCR:","
    BB.DATA    := FMT(P.DAY,"LZ"):","
    BB.DATA    := FMT(P.WEEK,"LZ"):","
    BB.DATA    := FMT(P.MONTH,"LZ"):","
    BB.DATA    := FMT(P.3MONTH,"LZ"):","
    BB.DATA    := FMT(P.6MONTH,"LZ"):","
    BB.DATA    := FMT(P.YEAR,"LZ"):","
    BB.DATA    := FMT(P.3YEAR,"LZ"):","
    BB.DATA    := FMT(P.MORE,"LZ"):","
    BB.DATA    := FMT(P.TOTAL,"LZ")

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*--------------------------------------------------------------------------
PRINT.CENT.LINE:

    BB.DATA = ""
    BB.DATA    = P.DESCR:","
    BB.DATA    = '%':FMT(P.DAY,"R2,"):","
    BB.DATA    = '%':FMT(P.WEEK,"R2,"):","
    BB.DATA    = '%':FMT(P.MONTH,"R2,"):","
    BB.DATA    = '%':FMT(P.3MONTH,"R2,"):","
    BB.DATA    = '%':FMT(P.6MONTH,"R2,"):","
    BB.DATA    = '%':FMT(P.YEAR,"R2,"):","
    BB.DATA   = '%':FMT(P.3YEAR,"R2,"):","
    BB.DATA   = '%':FMT(P.MORE,"R2,"):","
    BB.DATA   = '%':FMT(P.TOTAL,"R2,")

	WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*--------------------------------------------------------------------------
PRINT.HEAD:


    BB.DATA      = ""
    HEAD.DESC   = ",,,,":"���� ��������� ������ ����������"
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    BB.DATA      = ""
    HEAD.DESC   = ",,,,":"�������":",":P.DATE
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
	
*Line [ 578 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH

***    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,X.CY,CURR.NAME)
***    CURR.NAME = "����� ������ ����� �������"
    CURR.NAME = "����� ������ ����� ������� �������"
    D.X = "����� ������"

	BB.DATA      = ""
    HEAD.DESC    = "������":",":CURR.NAME:" ":D.X
    WRITESEQ HEAD.DESC TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    GOSUB FILL.HEAD.1
	
    BB.DATA = ""
    BB.DATA    = D.A:","
    BB.DATA   := D.B:","
    BB.DATA   := D.C:","
    BB.DATA   := D.D:","
    BB.DATA   := D.E:","
    BB.DATA   := D.F:","
    BB.DATA   := D.G:","
    BB.DATA   := D.H:","
    BB.DATA   := D.I:","
    BB.DATA   := D.J

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    
RETURN
*===============================================================
FILL.HEAD.1:
***        1234567890123456789012345678901234567890
    D.A = "�����������������                   "
    D.B = " ����� ������"
    D.C = "����� ���� "
    D.D = "���� �� ����� � ��� ��� "
    D.E = "���� �� ��� � ��� 3 ���� "
    D.F = "���� �� 3 ���� � ��� 6 ���� "
    D.G = "���� �� 6 ���� � ��� ��� "
    D.H = "���� �� ��� � ��� 3 ����� "
    D.I = "���� �� 3 ����� "
    D.J = " �������� "

RETURN
*===============================================================
END
