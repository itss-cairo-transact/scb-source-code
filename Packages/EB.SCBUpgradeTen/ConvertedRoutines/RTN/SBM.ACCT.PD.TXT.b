* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    PROGRAM SBM.ACCT.PD.TXT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCT.PD
*-----------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-----------------------------------------------------------
INITIATE:
*========

    FN.APD = 'F.SCB.ACCT.PD' ; F.APD = ''
    CALL OPF(FN.APD,F.APD)

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""

    OPENSEQ "MECH" , "ACCOUNT.DPD.CSV" TO AA THEN
        CLOSESEQ AA
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"ACCOUNT.DPD.CSV"
        HUSH OFF
    END
    OPENSEQ "MECH" , "ACCOUNT.DPD.CSV" TO AA ELSE
        CREATE AA THEN
            PRINT 'FILE ACCOUNT.DPD.CSV CREATED IN MECH'
        END ELSE
            STOP 'Cannot create ACCOUNT.DPD.CSV File IN MECH'
        END
    END

    DAT.1 = TODAY[1,6]:'01'
    CALL CDT("",DAT.1,'-1C')

    TD    = DAT.1[1,6]
    DAT.2 = DAT.1

    CALL ADD.MONTHS(DAT.2,'-2')

    DAT.2    = DAT.2[1,6]
    DAT.HED  = FMT(TD,"####/##")
    DAT.HED1 = FMT(DAT.2,"####/##")

    HEAD1 = "Account.Days.Past.Due"
    HEAD.DESC = HEAD1:" ":DAT.HED:" - ":DAT.HED1:","

    AA.DATA = HEAD.DESC
    WRITESEQ AA.DATA TO AA ELSE
        PRINT " ERROR WRITE FILE "
    END


    HEAD.DESC  = "Account.No":","
    HEAD.DESC := "Date":","
    HEAD.DESC := "Days.Past.Due":","

    AA.DATA = HEAD.DESC
    WRITESEQ AA.DATA TO AA ELSE
        PRINT " ERROR WRITE FILE "
    END

    LW.DAT = TODAY[1,6]:'01'
    CALL CDT("",LW.DAT,'-1W')
    WS.DATE.1 = LW.DAT[1,6]

    LW.DAT.M1 = LW.DAT[1,6]:'01'
    CALL CDT("",LW.DAT.M1,'-1W')
    WS.DATE.2 = LW.DAT.M1[1,6]

    LW.DAT.M2 = LW.DAT.M1[1,6]:'01'
    CALL CDT("",LW.DAT.M2,'-1W')
    WS.DATE.3 = LW.DAT.M2[1,6]


    RETURN
*------------------------------------------------------------
PROCESS:

    T.SEL = "SELECT ":FN.APD:" WITH DPD GT 0 AND DATE GE ":WS.DATE.3:" AND DATE LE ":WS.DATE.1:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.APD,KEY.LIST<I>,R.APD,F.APD,E1)

            WS.ACCT.NO = FIELD(KEY.LIST<I>,".",1)
            WS.DPD     = R.APD<APD.DPD>
            WS.DATE    = R.APD<APD.DATE>

            AA.DATA  = "'":WS.ACCT.NO:"'":","
            AA.DATA := WS.DATE:","
            AA.DATA := WS.DPD:","

            WRITESEQ AA.DATA TO AA ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

    RETURN
END
*============================================================
