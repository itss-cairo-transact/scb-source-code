* @ValidationCode : MjoxMTE5Njg3Njk2OkNwMTI1MjoxNjQwNzQwODg4MjExOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 17:21:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>3704</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBD.BAL.MONTH.FILL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BAL.MONTH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM

*********************** OPENING FILES *****************************
    FN.LD  = "FBNK.LD.LOANS.AND.DEPOSITS" ; F.LD   = "" ; R.LD = ""
    FN.AC  = "FBNK.ACCOUNT"  ; F.AC   = ""  ; R.AC = ""
    FN.CU  = "FBNK.CUSTOMER" ; F.CU   = ""  ; R.CU = ""
    FN.BAL = "F.SCB.BAL.MONTH"  ; F.BAL  = ""

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''

    CALL OPF(FN.CCY,F.CCY)

    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.BAL,F.BAL)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""

    DAT = TODAY
    CALL CDT ('',DAT,'-1W')
*** LAST.PERIOD.END

    TD = DAT

    DEP.COM = 0
    DEP.PRV = 0
    DEP.COM.FCY = 0
    DEP.PRV.FCY = 0
    AMT1 = 0

    DEP.COM2 = 0
    DEP.PRV2 =0
    DEP.COM.FCY2 = 0
    DEP.PRV.FCY2 = 0
    AMT.DEP2 = 0


    CD.COM = 0
    CD.PRV = 0
    CD.COM.FCY = 0
    CD.PRV.FCY = 0
    AMT.CD = 0

    SAV.COM = 0
    SAV.PRV = 0
    SAV.COM.FCY = 0
    SAV.PRV.FCY = 0
    AMT.SAV = 0

    CUR.COM =0
    CUR.PRV =0
    CUR.COM.FCY = 0
    CUR.PRV.FCY = 0
    AMT.CUR = 0

    COMP1 = ''

*------------------------------------------------------------------
********* SELECT DEPOSITS *******************
    T.SEL  = "SELECT ":FN.LD:" WITH ( CATEGORY GE 21001 AND CATEGORY LE 21010 ) "
    T.SEL := " AND ((VALUE.DATE LE ":TD:" AND AMOUNT NE 0 ) OR (FIN.MAT.DATE GT ":TD:" AND AMOUNT EQ 0)) BY CO.CODE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,E1)
            CUS.ID = R.LD<LD.CUSTOMER.ID>
            COMP = R.LD<LD.CO.CODE>


            IF SELECTED EQ 1 THEN COMP1 = COMP

            IF COMP NE COMP1 THEN
                DEP.COM = 0
                DEP.PRV = 0
                DEP.COM.FCY = 0
                DEP.PRV.FCY = 0
                AMT1 = 0
            END
            CURR = R.LD<LD.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 128 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            AMT1 = R.LD<LD.REIMBURSE.AMOUNT> * RATE

*******************
**   CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
**   NEW.SEC = LOCAL.REF<1,CULR.NEW.SECTOR>
*******************

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF CURR EQ 'EGP' THEN
                IF NEW.SEC EQ 4650 THEN
                    DEP.PRV += AMT1
                END

                IF NEW.SEC NE 4650 THEN
                    DEP.COM += AMT1
                END
            END

            IF CURR NE 'EGP' THEN

                IF NEW.SEC EQ 4650 THEN
                    DEP.PRV.FCY += AMT1
                END

                IF NEW.SEC NE 4650 THEN
                    DEP.COM.FCY += AMT1
                END
            END


********************************************************************************

            BAL.ID = COMP:"-":TD

            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)

            R.BAL<BL.BR.CODE> = COMP
            R.BAL<BL.TD.TODAY> = TD
            R.BAL<BL.DEP.COM> = DEP.COM
            R.BAL<BL.DEP.PRV> = DEP.PRV
            R.BAL<BL.DEP.COM.FCY> = DEP.COM.FCY
            R.BAL<BL.DEP.PRV.FCY> = DEP.PRV.FCY
            CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
            CALL  JOURNAL.UPDATE(BAL.ID)

            COMP1 = COMP

        NEXT I
    END

***********************************************
*************DEP FOR DOKKI********************
    T.SEL11   = "SELECT ":FN.AC:" WITH CATEGORY EQ 6512"
    CALL EB.READLIST(T.SEL11,KEY.LIST11,"",SELECTED11,ER.MSG11)
    IF SELECTED11 THEN
        FOR G = 1 TO SELECTED11
            CALL F.READ(FN.AC,KEY.LIST11<G>,R.AC,F.AC,E3)
            CUS.ID = R.AC<AC.CUSTOMER>
            COMP = R.AC<AC.CO.CODE>
            IF SELECTED11 EQ 1 THEN COMP1 = COMP
            IF COMP NE COMP1 THEN
                DEP.COM2 = 0
                DEP.PRV2 = 0
                DEP.COM.FCY2 = 0
                DEP.PRV.FCY2 = 0
                AMT.DEP2= 0
            END
            CURR = R.AC<AC.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 202 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            AMT.DEP2 = R.AC<AC.OPEN.ACTUAL.BAL> * RATE

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF CURR EQ 'EGP' THEN
                IF NEW.SEC EQ 4650 THEN
                    DEP.PRV2 += AMT.DEP2
                END ELSE

*  NEW.SEC NE 4650 THEN
                    DEP.COM2 += AMT.DEP2
                END
            END

            IF CURR NE 'EGP' THEN

                IF NEW.SEC EQ 4650 THEN
                    DEP.PRV.FCY2 += AMT.DEP2
                END ELSE

*   NEW.SEC NE 4650 THEN
                    DEP.COM.FCY2 += AMT.DEP2
                END
            END

            BAL.ID = COMP:"-":TD
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)
            R.BAL<BL.BR.CODE> = COMP
            R.BAL<BL.TD.TODAY> = TD

            R.BAL<BL.DEP.COM> = DEP.COM2
            R.BAL<BL.DEP.PRV> = DEP.PRV2
            R.BAL<BL.DEP.COM.FCY> = DEP.COM.FCY2
            R.BAL<BL.DEP.PRV.FCY> = DEP.PRV.FCY2

            CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
            CALL  JOURNAL.UPDATE(BAL.ID)
            COMP1 = COMP
        NEXT G

    END


************************************************
***************  SELECT CD *********************
    T.SEL1  = "SELECT ":FN.LD:" WITH CATEGORY GE 21020 AND CATEGORY LE 21032"
    T.SEL1 := " AND ((VALUE.DATE LE ":TD:" AND AMOUNT NE 0 ) OR (FIN.MAT.DATE GT ":TD:" AND AMOUNT EQ 0 )) BY CO.CODE"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR X = 1 TO SELECTED1
            CALL F.READ(FN.LD,KEY.LIST1<X>,R.LD,F.LD,E1)
            CUS.ID = R.LD<LD.CUSTOMER.ID>
            COMP = R.LD<LD.CO.CODE>
            IF SELECTED1 EQ 1 THEN COMP1 = COMP
            IF COMP NE COMP1 THEN
                CD.COM = 0
                CD.PRV = 0
                CD.COM.FCY = 0
                CD.PRV.FCY = 0
                AMT.CD = 0
            END
            CURR = R.LD<LD.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 269 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
*   AMT.CD = R.LD<LD.AMOUNT> * RATE
            AMT.CD =R.LD<LD.REIMBURSE.AMOUNT> * RATE


            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>

            IF CURR EQ 'EGP' THEN
                IF NEW.SEC EQ 4650 THEN
                    CD.PRV += AMT.CD
                END ELSE

*   NEW.SEC NE 4650 THEN
                    CD.COM += AMT.CD
                END
            END

            IF CURR NE 'EGP' THEN

                IF NEW.SEC EQ 4650 THEN
                    CD.PRV.FCY += AMT.CD
                END ELSE

*    NEW.SEC NE 4650 THEN
                    CD.COM.FCY += AMT.CD
                END
            END

            BAL.ID = COMP:"-":TD
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)
            R.BAL<BL.BR.CODE> = COMP
            R.BAL<BL.TD.TODAY> = TD

            R.BAL<BL.CD.COM> = CD.COM
            R.BAL<BL.CD.PRV> = CD.PRV
            R.BAL<BL.CD.COM.FCY> = CD.COM.FCY
            R.BAL<BL.CD.PRV.FCY> = CD.PRV.FCY

            CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
            CALL  JOURNAL.UPDATE(BAL.ID)
            COMP1 = COMP
        NEXT X

    END
**********************************************************************
****************************SELECT SAVING****************
    T.SEL2  = "SELECT ":FN.AC:" WITH CATEGORY IN (6501 6502 6503 6504 6511) BY CO.CODE"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR Z = 1 TO SELECTED2
            CALL F.READ(FN.AC,KEY.LIST2<Z>,R.AC,F.AC,E2)
            CUS.ID = R.AC<AC.CUSTOMER>
            COMP = R.AC<AC.CO.CODE>
            IF SELECTED2 EQ 1 THEN COMP1 = COMP
            IF COMP NE COMP1 THEN
                SAV.COM = 0
                SAV.PRV = 0
                SAV.COM.FCY = 0
                SAV.PRV.FCY = 0
                AMT.SAV= 0
            END
            CURR = R.AC<AC.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 336 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            AMT.SAV = R.AC<AC.OPEN.ACTUAL.BAL> * RATE

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF CURR EQ 'EGP' THEN
                IF NEW.SEC EQ 4650 THEN
                    SAV.PRV += AMT.SAV
                END ELSE

*    NEW.SEC NE 4650 THEN
                    SAV.COM += AMT.SAV
                END
            END

            IF CURR NE 'EGP' THEN

                IF NEW.SEC EQ 4650 THEN
                    SAV.PRV.FCY += AMT.SAV
                END ELSE

*     NEW.SEC NE 4650 THEN
                    SAV.COM.FCY += AMT.SAV
                END
            END

            BAL.ID = COMP:"-":TD
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)
            R.BAL<BL.BR.CODE> = COMP
            R.BAL<BL.TD.TODAY> = TD

            R.BAL<BL.SAV.COM> = SAV.COM
            R.BAL<BL.SAV.PRV> = SAV.PRV
            R.BAL<BL.SAV.COM.FCY> = SAV.COM.FCY
            R.BAL<BL.SAV.PRV.FCY> = SAV.PRV.FCY

            CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
            CALL  JOURNAL.UPDATE(BAL.ID)
            COMP1 = COMP
        NEXT Z

    END
****************************SELECT CUR****************
    T.SEL3   = "SELECT ":FN.AC:" WITH ((CATEGORY GE 1001 AND CATEGORY LE 1003) OR (CATEGORY GE 1201 AND CATEGORY LE 1208)"
    T.SEL3  := " OR (CATEGORY GE 1300 AND CATEGORY LE 1599) OR (CATEGORY GE 1100 AND CATEGORY LE 1212)"
    T.SEL3  := " OR (CATEGORY GE 1401 AND CATEGORY LE 1437)"
    T.SEL3  := " OR (CATEGORY EQ 1290)) AND OPEN.ACTUAL.BAL GT 0 BY CO.CODE"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SELECTED3,ER.MSG3)
    IF SELECTED3 THEN
        FOR M = 1 TO SELECTED3
            CALL F.READ(FN.AC,KEY.LIST3<M>,R.AC,F.AC,E3)
            CUS.ID = R.AC<AC.CUSTOMER>
            COMP = R.AC<AC.CO.CODE>
            IF SELECTED3 EQ 1 THEN COMP1 = COMP
            IF COMP NE COMP1 THEN
                CUR.COM = 0
                CUR.PRV = 0
                CUR.COM.FCY = 0
                CUR.PRV.FCY = 0
                AMT.CUR= 0
            END
            CURR = R.AC<AC.CURRENCY>
            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 401 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            AMT.CUR = R.AC<AC.OPEN.ACTUAL.BAL> * RATE

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF CURR EQ 'EGP' THEN
                IF NEW.SEC EQ 4650 THEN
                    CUR.PRV += AMT.CUR
                END ELSE

*     IF NEW.SEC NE 4650 THEN
                    CUR.COM += AMT.CUR
                END
            END

            IF CURR NE 'EGP' THEN

                IF NEW.SEC EQ 4650 THEN
                    CUR.PRV.FCY += AMT.CUR
                END ELSE

*    NEW.SEC NE 4650 THEN
                    CUR.COM.FCY += AMT.CUR
                END
            END

            BAL.ID = COMP:"-":TD
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)
            R.BAL<BL.BR.CODE> = COMP
            R.BAL<BL.TD.TODAY> = TD

            R.BAL<BL.CUR.COM> = CUR.COM
            R.BAL<BL.CUR.PRV> = CUR.PRV
            R.BAL<BL.CUR.COM.FCY> = CUR.COM.FCY
            R.BAL<BL.CUR.PRV.FCY> = CUR.PRV.FCY

            CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
            CALL  JOURNAL.UPDATE(BAL.ID)
            COMP1 = COMP
        NEXT M

    END
**************************************************
*************** SELECTED OTHER **************

    T.SEL4   = "SELECT ":FN.AC:" WITH ((CATEGORY GE 1012 AND CATEGORY LE 1016) OR (CATEGORY GE 3010 AND CATEGORY LE 3013) OR (CATEGORY GE 3016 AND CATEGORY LE 3017)"
    T.SEL4  := " OR (CATEGORY GE 16151 AND CATEGORY LE 16154) OR (CATEGORY IN (3005 1019 16113 16188))) AND OPEN.ACTUAL.BAL GT 0 BY CO.CODE"
    CALL EB.READLIST(T.SEL4,KEY.LIST4,"",SELECTED4,ER.MSG4)
    IF SELECTED4 THEN
        FOR K = 1 TO SELECTED4
            CALL F.READ(FN.AC,KEY.LIST4<K>,R.AC,F.AC,E4)
            CUS.ID = R.AC<AC.CUSTOMER>
            COMP = R.AC<AC.CO.CODE>
            IF SELECTED4 EQ 1 THEN COMP1 = COMP
            IF COMP NE COMP1 THEN
                OTH.COM = 0
                OTH.PRV = 0
                OTH.COM.FCY = 0
                OTH.PRV.FCY = 0
                AMT.OTH= 0
            END
            CURR = R.AC<AC.CURRENCY>

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 467 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            LOCATE CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
            RATE = R.CCY<RE.BCP.RATE,POS>
            AMT.OTH = R.AC<AC.OPEN.ACTUAL.BAL> * RATE

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
            NEW.SEC = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            IF CURR EQ 'EGP' THEN
                IF NEW.SEC EQ 4650 THEN
                    OTH.PRV += AMT.OTH
                END

                IF  NEW.SEC NE 4650 THEN
                    OTH.COM += AMT.OTH
                END
            END

            IF CURR NE 'EGP' THEN

                IF NEW.SEC EQ 4650 THEN
                    OTH.PRV.FCY += AMT.OTH
                END

                IF NEW.SEC NE 4650 THEN
                    OTH.COM.FCY += AMT.OTH
                END
            END

            BAL.ID = COMP:"-":TD
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,E3)
            R.BAL<BL.BR.CODE> = COMP
            R.BAL<BL.TD.TODAY> = TD

            R.BAL<BL.OTH.COM> = OTH.COM
            R.BAL<BL.OTH.PRV> = OTH.PRV
            R.BAL<BL.OTH.COM.FCY> = OTH.COM.FCY
            R.BAL<BL.OTH.PRV.FCY> = OTH.PRV.FCY

            CALL F.WRITE(FN.BAL,BAL.ID,R.BAL)
            CALL  JOURNAL.UPDATE(BAL.ID)
            COMP1 = COMP
        NEXT K

    END

RETURN
END
************************************************
