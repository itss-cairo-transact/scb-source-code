* @ValidationCode : MjotMTYyNzk2NDMwOkNwMTI1MjoxNjQwODMyNDEwODkyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:46:50
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
SUBROUTINE SBD.TELLER.OVER.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.TELLER.OVER

*********************** OPENING FILES *****************************
    FN.ORG  = "FBNK.TELLER"        ; F.ORG  = ""
    FN.CPY  = "F.SCB.TELLER.OVER"  ; F.CPY  = ""

    CALL OPF (FN.ORG,F.ORG)
    CALL OPF (FN.CPY,F.CPY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

*------------------------------------------------------------------

    T.SEL = "SELECT ":FN.ORG:" WITH OVERRIDE LIKE '...CREDIT TILL CLOSING BALANCE...' AND ( VERSION.NAME UNLIKE ...CASH... AND VERSION.NAME UNLIKE ...DEPST... ) BY CO.CODE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ORG,KEY.LIST<I>,R.ORG,F.ORG,E1)
            CALL F.READ(FN.CPY,KEY.LIST<I>,R.CPY,F.CPY,E2)
            REF.ID = KEY.LIST<I>

            INP.ID  = R.ORG<TT.TE.INPUTTER>
            USER.ID = FIELD(INP.ID,"_",2)
            WS.TIME = R.ORG<TT.TE.DATE.TIME>[7,4]

*Line [ 54 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('USER':@FM:EB.USE.USER.NAME,USER.ID,USER.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,USER.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
USER.NAME=R.ITSS.USER<EB.USE.USER.NAME>

********************** COPY NEW DATA ******************************

            R.CPY<TOV.USER.ID>    = USER.ID
            R.CPY<TOV.USER.NAME>  = USER.NAME
            R.CPY<TOV.REF>        = REF.ID
            R.CPY<TOV.CURRENCY>   = R.ORG<TT.TE.CURRENCY.1>
            R.CPY<TOV.AMOUNT>     = R.ORG<TT.TE.NET.AMOUNT>
            R.CPY<TOV.AMOUNT.LCY> = R.ORG<TT.TE.AMOUNT.LOCAL.1>
            R.CPY<TOV.TIME>       = WS.TIME
            R.CPY<TOV.BRANCH>     = R.ORG<TT.TE.CO.CODE>
            R.CPY<TOV.DATE>       = TODAY

            CALL F.WRITE(FN.CPY,REF.ID,R.CPY)
**  CALL  JOURNAL.UPDATE(REF.ID)

        NEXT I
    END
END
