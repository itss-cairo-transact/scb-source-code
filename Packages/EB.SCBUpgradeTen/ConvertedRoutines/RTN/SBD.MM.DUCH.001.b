* @ValidationCode : MjoxNDkzOTk5OTY0OkNwMTI1MjoxNjQxNzc4NTQ4MTc5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:35:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
PROGRAM SBD.MM.DUCH.001

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.GROUP.CAPITALISATION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.MM.MONEY.MARKET
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SECTOR
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SBD.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BASIC.INTEREST
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COUNTRY
*Line [ 52 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TARGET
*Line [ 54 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 56 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 58 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 60 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 62 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUST.FI.RATING
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
RETURN
*-------------------------------------------------------------------------
INITIATE:
    OPENSEQ "&SAVEDLISTS&" , "DAILY.MM.DUCH.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"DAILY.MM.DUCH.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "DAILY.MM.DUCH.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE DAILY.MM.DUCH.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create DAILY.MM.DUCH.CSV File IN &SAVEDLISTS&'
        END
    END

    CUS.ID      = ''  ; CUST.NAME  = '' ; WS.CCY    = '' ; WS.AMT = 0        ; WS.BUCKET.LABEL   = ''
    WS.MAT.DATE = ''  ; WS.RATE    = 0  ; WS.SPREAD = 0  ; CUST.SECTOR  = '' ; WS.RATE.TYPE.CODE = ''
    DAYS        = ''  ; INT.EXPENS = 0  ; WS.CATEG  = 0  ; WS.RATE.TYPE = 'FIXED'

    DAT.ID = 'EG0010001'
*Line [ 89 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,DAT.2)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT.2=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TDD = TODAY

    TD = DAT.2
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "DAILY.MONEY.MARKET"
    HEAD.DESC = HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "CUSTOMER.NUMBER":","
    HEAD.DESC := "CUSTOMER.NAME":","
    HEAD.DESC := "CURRENCY":","
    HEAD.DESC := "AMOUNT.EQUIVALENT":","
    HEAD.DESC := "NATIVE.AMOUNT":","
    HEAD.DESC := "VALUE.DATE":","
    HEAD.DESC := "MATURITY.DATE":","
    HEAD.DESC := "RATE":","
    HEAD.DESC := "SPREAD.RATE":","
    HEAD.DESC := "TOTAL.RATE":","
    HEAD.DESC := "TYPE.OF.RATE":","
    HEAD.DESC := "CUSTOMER.TYPE":","
    HEAD.DESC := "SECTOR.NAME":","
    HEAD.DESC := "DAYS.TO.MATURITY":","
    HEAD.DESC := "INTEREST.EXPENSES":","
    HEAD.DESC := "BUCKET.LABEL":","
    HEAD.DESC := "LEDGER":","
    HEAD.DESC := "PRODUCT.TYPE":","
    HEAD.DESC := "LIMIT":","
    HEAD.DESC := "NATIONALITY":","
    HEAD.DESC := "ROLLOVER.DATE":","
    HEAD.DESC := "INTEREST.AMOUNT":","
    HEAD.DESC := "Weighted.Average.NATIVE.AMOUNT":","
    HEAD.DESC := "LONG.TERM.RATE":","
    HEAD.DESC := "SHORT.TERM.RATE":","
    HEAD.DESC := "FIRST.LONG.TERM.RATE":","
    HEAD.DESC := "FIRST.SHORT.TERM.RATE":","
    HEAD.DESC := "RATING.AGENCY":","
    HEAD.DESC := "RATING.DATE":","
    HEAD.DESC := "LAST.RENEWAL.DATE":","
    HEAD.DESC := "Segmentation":","
    HEAD.DESC := "Segmentation.Desc":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
RETURN

*========================================================================
PROCESS:
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.MM = 'FBNK.MM.MONEY.MARKET' ; F.MM = ''
    CALL OPF(FN.MM,F.MM)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.SEC = 'FBNK.SECTOR' ; F.SEC = ''
    CALL OPF(FN.SEC,F.SEC)

    FN.GA = 'F.CATEGORY' ; F.GA = ''
    CALL OPF(FN.GA,F.GA)

    FN.LI = 'F.LIMIT' ; F.LI = ''
    CALL OPF(FN.LI,F.LI)

    FN.BI = 'FBNK.BASIC.INTEREST' ; F.BI = ''
    CALL OPF(FN.BI,F.BI)

    FN.NAT = 'F.COUNTRY' ; F.NAT = ''
    CALL OPF(FN.NAT,F.NAT)

    FN.RAT = 'F.SCB.CUST.FI.RATING' ; F.RAT = ''
    CALL OPF(FN.RAT,F.RAT)

    FN.RAT.H = 'F.SCB.CUST.FI.RATING$HIS' ; F.RAT.H = ''
    CALL OPF(FN.RAT.H,F.RAT.H)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2="" ; SELECTED2="" ;  ER.MSG2=""
*-------------------------------------------
******* Money Market SELECTION *************

    T.SEL = "SELECT ":FN.MM:" WITH (STATUS NE 'LIQ') OR (VALUE.DATE EQ ":DAT:" AND MATURITY.DATE EQ ":TDD:") OR (MATURITY.DATE EQ ":TDD:") BY CUSTOMER.ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            GOSUB GET.DETAIL
            GOSUB PRINT.DET
        NEXT I
    END
RETURN
*==============================================================
PRINT.DET:

    BB.DATA  = CUS.ID:","
    BB.DATA := CUST.NAME:","
    BB.DATA := WS.CCY:","
    BB.DATA := WS.AMT:","
    BB.DATA := WS.AMT.FCY:","
    BB.DATA := WS.VAL.DATE:","
    BB.DATA := WS.MAT.DATE:","
    BB.DATA := WS.RATE:","
    BB.DATA := WS.SPREAD:","
    BB.DATA := WS.RATE.ALL:","
    BB.DATA := WS.RATE.TYPE:","
    BB.DATA := CUST.SECTOR:","
    BB.DATA := SECTOR.NAME:","
    BB.DATA := DAYS:","
    BB.DATA := INT.EXPENS:","
    BB.DATA := WS.BUCKET.LABEL:","
    BB.DATA := WS.CATEG:","
    BB.DATA := DESC:","
    BB.DATA := WS.LIMIT:","
    BB.DATA := WS.NAT:","
    BB.DATA := WS.ROL.DATE:","
    BB.DATA := WS.INT.AMT:","
    BB.DATA := WS.WA.AMT:","
    BB.DATA := WS.LONG.TRM.RATE:","
    BB.DATA := WS.SHORT.TRM.RATE:","
    BB.DATA := WS.LONG.TRM.RATE.H:","
    BB.DATA := WS.SHORT.TRM.RATE.H:","
    BB.DATA := WS.RATE.AGN:","
    BB.DATA := WS.RATE.DATE:","
    BB.DATA := WS.LAST.RENEWAL.DATE:","
    BB.DATA := WS.TARGET:","
    BB.DATA := WS.TARGET.NAME:","

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

RETURN
*===============================================================
GET.DETAIL:

    CALL F.READ(FN.MM,KEY.LIST<I>,R.MM,F.MM,E1)
    CUS.ID       = R.MM<MM.CUSTOMER.ID>
    WS.LIMIT.REF = R.MM<MM.LIMIT.REFERENCE>
    WS.LIMIT.ID = CUS.ID:'.000':WS.LIMIT.REF
    WS.VAL.DATE = R.MM<MM.VALUE.DATE>
    WS.ROL.DATE = R.MM<MM.ROLLOVER.DATE>
    WS.INT.DATE = R.MM<MM.INT.PERIOD.START>

    IF WS.ROL.DATE NE '' THEN
        WS.LAST.RENEWAL.DATE = WS.ROL.DATE
    END ELSE
        IF WS.INT.DATE NE '' THEN
            WS.LAST.RENEWAL.DATE = WS.INT.DATE
        END ELSE
            IF WS.VAL.DATE NE '' THEN
                WS.LAST.RENEWAL.DATE = WS.VAL.DATE
            END
        END
    END

    NEW.RATE    = R.MM<MM.NEW.INTEREST.RATE>
    OLD.INT.AMT = R.MM<MM.TOT.INTEREST.AMT>
    NEW.INT.AMT = R.MM<MM.NEXT.INT.AMOUNT>

    IF NEW.RATE NE '' THEN
        WS.INT.AMT = NEW.INT.AMT
    END ELSE
        WS.INT.AMT = OLD.INT.AMT
    END

    CALL F.READ(FN.RAT,CUS.ID,R.RAT,F.RAT,E4)
    WS.LONG.TRM.RATE  = R.RAT<CU.RAT.LONG.TRM.RAT>
    WS.SHORT.TRM.RATE = R.RAT<CU.RAT.SHORT.TRM.RAT>
    WS.RATE.AGN       = R.RAT<CU.RAT.RATING.AGENCY>
    WS.RATE.DATE      = R.RAT<CU.RAT.RATING.DATE>

    CUS.ID.H = CUS.ID:';1'
    CALL F.READ(FN.RAT.H,CUS.ID.H,R.RAT.H,F.RAT.H,E4)
    WS.LONG.TRM.RATE.H  = R.RAT.H<CU.RAT.LONG.TRM.RAT>
    WS.SHORT.TRM.RATE.H = R.RAT.H<CU.RAT.SHORT.TRM.RAT>


    CALL F.READ(FN.LI,WS.LIMIT.ID,R.LI,F.LI,E5)
    WS.LIMIT = R.LI<LI.INTERNAL.AMOUNT>

    CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)
    CUST.NAME        = R.CU<EB.CUS.NAME.1>
    CUST.SECTOR.CODE = R.CU<EB.CUS.SECTOR>
    WS.CUS.NAT       = R.CU<EB.CUS.NATIONALITY>

    WS.TARGET      = R.CU<EB.CUS.TARGET>
    CALL F.READ(FN.TAR,WS.TARGET,R.TAR,F.TAR,E3)
    WS.TARGET.NAME = R.TAR<EB.TAR.DESCRIPTION>


    CALL F.READ(FN.NAT,WS.CUS.NAT,R.NAT,F.NAT,E4)
    WS.NAT = R.NAT<EB.COU.COUNTRY.NAME,1>


    CALL F.READ(FN.SEC,CUST.SECTOR.CODE,R.SEC,F.SEC,E1)
    SECTOR.NAME = R.SEC<EB.SEC.DESCRIPTION,2>

    IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1300 OR CUST.SECTOR.CODE EQ 1400 OR CUST.SECTOR.CODE EQ 2000 THEN
        IF CUST.SECTOR.CODE EQ 1100 OR CUST.SECTOR.CODE EQ 1300 THEN
            CUST.SECTOR = 'Staff'
        END

        IF CUST.SECTOR.CODE EQ 1200 OR CUST.SECTOR.CODE EQ 1400 THEN
            CUST.SECTOR = 'Staff.Relatives'
        END
        IF CUST.SECTOR.CODE EQ 2000 THEN
            CUST.SECTOR = 'Retail'
        END
    END ELSE
        CUST.SECTOR = 'Corporate'
    END

    WS.CCY     = R.MM<MM.CURRENCY>
    WS.AMT.FCY = R.MM<MM.PRINCIPAL>

    CALL F.READ(FN.CCY,WS.CCY,R.CCY,F.CCY,E1)
    AMT.RATE = R.CCY<SBD.CURR.MID.RATE>

    IF WS.CCY EQ 'EGP' THEN
        AMT.RATE = 1
    END
    IF WS.CCY EQ 'JPY' THEN
        AMT.RATE = AMT.RATE / 100
    END

    WS.AMT   = WS.AMT.FCY * AMT.RATE
    WS.CATEG = R.MM<MM.CATEGORY>

    CALL F.READ(FN.GA,WS.CATEG,R.GA,F.GA,E1)
    DESC = R.GA<EB.CAT.DESCRIPTION,1>

    WS.MAT.DATE = R.MM<MM.MATURITY.DATE>
    WS.RATE     = R.MM<MM.INTEREST.RATE>

    WS.RATE.TYPE.CODE = R.MM<MM.INT.RATE.TYPE>
    WS.SPREAD         = R.MM<MM.INTEREST.SPREAD.1>

    IF WS.RATE.TYPE.CODE EQ 3 THEN
        WS.RATE.TYPE = 'FLOATING'
        WS.MM.KEY = R.MM<MM.INTEREST.KEY>
        BI.ID = WS.MM.KEY:WS.CCY:'...'
        T.SEL2 = "SELECT ":FN.BI:" WITH @ID LIKE ":BI.ID:" BY @ID"
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
        IF SELECTED2 THEN
            CALL F.READ(FN.BI,KEY.LIST2<SELECTED2>,R.BI,F.BI,E3)
            WS.RATE = R.BI<EB.BIN.INTEREST.RATE>
        END
    END ELSE
        WS.RATE.TYPE = 'FIXED'
    END
********************************
    DAYS = "C"
    IF WS.MAT.DATE NE '' THEN
        CALL CDD("",DAT.2,WS.MAT.DATE,DAYS)
    END ELSE
        DAYS = ''
    END

    WS.WA.AMT = WS.AMT.FCY * DAYS

    WS.RATE.ALL = WS.RATE + WS.SPREAD
    INT.EXPENS  = ( WS.AMT * WS.RATE.ALL * DAYS ) / 36000
    INT.EXPENS  = DROUND(INT.EXPENS,'0')

    MONTHS.NO = 1
    CALL EB.NO.OF.MONTHS(DAT.2,WS.MAT.DATE,MONTHS.NO)

    IF DAYS EQ 1 THEN
        WS.BUCKET.LABEL = '1.DAY'
    END ELSE
        IF DAYS GT 1 AND DAYS LE 7 THEN
            WS.BUCKET.LABEL = '1.WEEK'
        END ELSE
            IF MONTHS.NO EQ 1 THEN
                WS.BUCKET.LABEL = '1.MONTH'
            END
        END
    END

**IF DAYS GT 7 AND DAYS LE 31 THEN WS.BUCKET.LABEL = '1.MONTH'
**IF DAYS GT 31 AND DAYS LE 93 THEN WS.BUCKET.LABEL = '3.MONTHS'
**IF DAYS GT 93 AND DAYS LE 186 THEN WS.BUCKET.LABEL = '6.MONTHS'
**IF DAYS GT 186 AND DAYS LE 370 THEN WS.BUCKET.LABEL = '1.YEAR'
**IF DAYS GT 370 AND DAYS LE 1090 THEN WS.BUCKET.LABEL = '3.YEAR'
**IF DAYS GT 1090 THEN WS.BUCKET.LABEL = 'MORE.THAN.3.YEAR'

    IF MONTHS.NO GT 1 AND MONTHS.NO LE 3 THEN WS.BUCKET.LABEL = '3.MONTHS'
    IF MONTHS.NO GT 3 AND MONTHS.NO LE 6 THEN WS.BUCKET.LABEL = '6.MONTHS'
    IF MONTHS.NO GT 6 AND MONTHS.NO LE 9 THEN WS.BUCKET.LABEL = '9.MONTHS'
    IF MONTHS.NO GT 9 AND MONTHS.NO LE 12 THEN WS.BUCKET.LABEL = '1.YEAR'
    IF MONTHS.NO GT 12 AND MONTHS.NO LE 18 THEN WS.BUCKET.LABEL = '1.5.YEAR'
    IF MONTHS.NO GT 18 AND MONTHS.NO LE 24 THEN WS.BUCKET.LABEL = '2.YEAR'
    IF MONTHS.NO GT 24 AND MONTHS.NO LE 36 THEN WS.BUCKET.LABEL = '3.YEAR'
    IF MONTHS.NO GT 36 AND MONTHS.NO LE 48 THEN WS.BUCKET.LABEL = '4.YEAR'
    IF MONTHS.NO GT 48 AND MONTHS.NO LE 60 THEN WS.BUCKET.LABEL = '5.YEAR'
    IF MONTHS.NO GT 60 AND MONTHS.NO LE 72 THEN WS.BUCKET.LABEL = '6.YEAR'
    IF MONTHS.NO GT 72 AND MONTHS.NO LE 84 THEN WS.BUCKET.LABEL = '7.YEAR'
    IF MONTHS.NO GT 84 AND MONTHS.NO LE 96 THEN WS.BUCKET.LABEL = '8.YEAR'
    IF MONTHS.NO GT 96 AND MONTHS.NO LE 108 THEN WS.BUCKET.LABEL = '9.YEAR'
    IF MONTHS.NO GT 108 AND MONTHS.NO LE 120 THEN WS.BUCKET.LABEL = '10.YEAR'
    IF MONTHS.NO GT 120 AND MONTHS.NO LE 132 THEN WS.BUCKET.LABEL = '11.YEAR'
    IF MONTHS.NO GT 132 AND MONTHS.NO LE 144 THEN WS.BUCKET.LABEL = '12.YEAR'
    IF MONTHS.NO GT 144 AND MONTHS.NO LE 156 THEN WS.BUCKET.LABEL = '13.YEAR'
    IF MONTHS.NO GT 156 AND MONTHS.NO LE 168 THEN WS.BUCKET.LABEL = '14.YEAR'
    IF MONTHS.NO GT 168 AND MONTHS.NO LE 180 THEN WS.BUCKET.LABEL = '15.YEAR'
    IF MONTHS.NO GT 180 AND MONTHS.NO LE 192 THEN WS.BUCKET.LABEL = '16.YEAR'
    IF MONTHS.NO GT 192 AND MONTHS.NO LE 204 THEN WS.BUCKET.LABEL = '17.YEAR'
    IF MONTHS.NO GT 204 AND MONTHS.NO LE 216 THEN WS.BUCKET.LABEL = '18.YEAR'
    IF MONTHS.NO GT 216 AND MONTHS.NO LE 228 THEN WS.BUCKET.LABEL = '19.YEAR'
    IF MONTHS.NO GT 228 AND MONTHS.NO LE 240 THEN WS.BUCKET.LABEL = '20.YEAR'
    IF MONTHS.NO GT 240 THEN WS.BUCKET.LABEL = 'MORE.THAN.20.YEAR'

RETURN

*===============================================================
