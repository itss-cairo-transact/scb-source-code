* @ValidationCode : MjotNzE2ODkwNzU3OkNwMTI1MjoxNjQwODI3NTk1Nzk5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:26:35
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>807</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.MATCH.DEPOSIT.FR
***    PROGRAM SBD.MATCH.DEPOSIT.FR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*-------------------------------------------------------------------------
*                                            ��� ���
*                                          ��� �������
*                                            R A T E
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
    GOSUB PROCESS
    GOSUB TOTAL.REPORT
*-------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
*    REPORT.ID='P.FUNCTION'
    REPORT.ID='SBD.MATCH.DEPOSIT.FR'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY
    PROGRAM.NAME = 'SBD.MATCH.DEPOSIT.FR'

    T.AGL.BAL = 0
    T.CER.BAL = 0
    T.SAV.BAL = 0
    T.CUR.BAL = 0
    T.OTR.BAL = 0


    T.AGL.BALX = 0
    T.CER.BALX = 0
    T.SAV.BALX = 0
    T.CUR.BALX = 0
    T.OTR.BALX = 0


    T.AGL.BALY = 0
    T.CER.BALY = 0
    T.SAV.BALY = 0
    T.CUR.BALY = 0
    T.OTR.BALY = 0

    T.TOT.BALY = 0

    TXT1 = "�����"
    TXT2 = "�����"
    TXT3 = "�����"
    TXT4 = "������"

    H.BRANCH  = "�� ���� �����"


*    HEAD.A1 = "������ ������ ������ �� ����� ������� ���������"
    HEAD.A1 = "���� �������� ������� �� ����� ������� ���������"
*    HEAD.B1 = "������� ���������"
    HEAD.B1 = "�������� �������� ����� ����"
    HEAD.B2 = "���� ������"
*    HEAD.B2 = "���� ����� ���"

    S.HEAD1 = "�������� �������"
    S.HEAD2 = "������ �������"
    S.HEAD3 = "������ ��������"
    S.HEAD4 = "������� �������"
    S.HEAD5 = "������ ��������"

*----------------------------------
    FN.COMP = "FBNK.COMPANY"
    F.COMP  = ""
    R.COMP = ""
    Y.COMP.ID = ""

*----------------------------------
    FN.CCY = "FBNK.CURRENCY"
    F.CCY  = ""
    R.CCY = ""
    Y.CCY.ID = ""

    CALL OPF(FN.CCY,F.CCY)
*----------------------------------

    FN.LINE = 'F.RE.STAT.LINE.BAL'
    F.LINE = ''

    CALL OPF(FN.LINE,F.LINE)

*----------------------------------
    DIM AR.CURR(50)
    DIM AR.RATE(50)
    DIM AR.TYPE(50)

*----------------------------------
    T.SEL  = "SELECT FBNK.CURRENCY WITH @ID NE 'EGP'"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, CCY.ERR)

    NO.CURR = SELECTED
    FOR I = 1 TO NO.CURR

        CALL F.READ(FN.CCY,KEY.LIST<I>,R.CCY,F.CCY,CCY.ERR)
        Y.CCY.ID  = KEY.LIST<I>

*        CALL F.READ(FN.CCY,Y.CCY.ID,R.CCY,F.CCY,ERR.CCY)
        IF Y.CCY.ID EQ 'JPY' THEN
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE><1,1> / 100
        END ELSE
            RATE = R.CCY<EB.CUR.MID.REVAL.RATE><1,1>
        END

        AR.CURR(I) = Y.CCY.ID
        AR.RATE(I) = RATE
        AR.TYPE(I) = ''

*                                            ��� ���
*                                          ��� �������
*                                            R A T E
        AR.RATE(I) = 1

    NEXT I

*----------------------------------
*    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
*    CALL OPF(FN.CUR,F.CUR)
*    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*    CUR.COD = R.CUR<RE.BCP.ORIGINAL.CCY>
*    NO.CURR = DCOUNT(CUR.COD,VM)
*    FOR POS = 1 TO NO.CURR
*        AR.CURR(POS) = R.CUR<RE.BCP.ORIGINAL.CCY,POS>
*        AR.RATE(POS) = R.CUR<RE.BCP.RATE,POS>
*        AR.TYPE(POS) = R.CUR<RE.BCP.RATE.TYPE,POS>
*    NEXT POS
*----------------------------------
    FN.DATE = "F.DATES"
    F.DATE  = ""
    R.DATE = ""
    Y.DATE.ID = ""

    CALL OPF (FN.DATE,F.DATE)
*----------------------------------

    Y.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,Y.DATE.ID,R.DATE,F.DATE,ERR.DATE)
    DAT = R.DATE<EB.DAT.LAST.PERIOD.END>
    L.WS.DATE = R.DATE<EB.DAT.LAST.WORKING.DAY>

*----------------------
*    XXXX     = TODAY
*    CALL CDT('',XXXX,'-1W')
*    SYS.DATE = XXXX
*----------------------


    SYS.DATE = TODAY


* -------  HARES M. MAHMOUD 22 MAR, 2010 ----------------
*    X.DAT = 20120109
*    DAT = X.DAT
*    CALL CDT('',DAT,'-1C')
*    L.WS.DATE = DAT
*    SYS.DATE = X.DAT
* -------  HARES M. MAHMOUD 22 MAR, 2010 ----------------


    P.DATE   = FMT(SYS.DATE,"####/##/##")
    P.WS.DATE = FMT(L.WS.DATE,"####/##/##")


RETURN
*========================================================================
PROCESS:


    T.SEL3 = "SELECT F.COMPANY WITH @ID UNLIKE 'EG0010077' AND @ID UNLIKE 'EG0010088' AND @ID UNLIKE 'EG0010099' BY @ID"
    CALL EB.READLIST(T.SEL3,KEY.LIST3,"",SEL.COMP,ER.MSG3)



    FOR I = 1 TO SEL.COMP

        AGL.BAL = 0
        CER.BAL = 0
        SAV.BAL = 0
        CUR.BAL = 0
        OTR.BAL = 0
* ---------------------------
        AGL.BALX = 0
        CER.BALX = 0
        SAV.BALX = 0
        CUR.BALX = 0
        OTR.BALX = 0


        COMP = KEY.LIST3<I>

*********************************************************************************************************

        FOR X = 1 TO NO.CURR

            XCURR = AR.CURR(X)
            XRATE = AR.RATE(X)
            XTYPE = AR.TYPE(X)

            IDD1 = 'GENLED-0710':'-':XCURR:'-':DAT:'*':COMP
            CALL F.READ(FN.LINE,IDD1,R.LINE,F.LINE,E1)
            CUR.BAL1  = R.LINE<RE.SLB.CLOSING.BAL.LCL>
            CUR.BAL1X = R.LINE<RE.SLB.OPEN.BAL.LCL>

*----------------------
            IF COMP EQ 'EG0010011' THEN
                IDD2 = 'GENLED-0725':'-':XCURR:'-':DAT:'*':COMP
            END

            IF COMP NE 'EG0010011' THEN
                IDD2 = 'GENLED-0720':'-':XCURR:'-':DAT:'*':COMP
            END

            CALL F.READ(FN.LINE,IDD2,R.LINE,F.LINE,E)
            SAV.BAL1  = R.LINE<RE.SLB.CLOSING.BAL.LCL>
            SAV.BAL1X = R.LINE<RE.SLB.OPEN.BAL.LCL>

*----------------------
            IF COMP EQ 'EG0010011' THEN
                IDD3 = 'GENLED-0727':'-':XCURR:'-':DAT:'*':COMP
            END

            IF COMP NE 'EG0010011' THEN
                IDD3 = 'GENLED-0730':'-':XCURR:'-':DAT:'*':COMP
                IDD3X = 'GENLED-0735':'-':XCURR:'-':DAT:'*':COMP

            END

            CALL F.READ(FN.LINE,IDD3,R.LINE,F.LINE,E)
            AGL.BAL1  = R.LINE<RE.SLB.CLOSING.BAL.LCL>
            AGL.BAL1X = R.LINE<RE.SLB.OPEN.BAL.LCL>

            CALL F.READ(FN.LINE,IDD3X,R.LINE,F.LINE,E)
            AGL.BAL3X  = R.LINE<RE.SLB.CLOSING.BAL.LCL>
            AGL.BAL3Y  = R.LINE<RE.SLB.OPEN.BAL.LCL>

            AGL.BAL1   =  AGL.BAL1 + AGL.BAL3X
            AGL.BAL1X  =  AGL.BAL1X + AGL.BAL3Y

            AGL.BAL3X  = 0
            AGL.BAL3Y  = 0

*----------------------
            IDD3 = 'GENLED-0723':'-':XCURR:'-':DAT:'*':COMP

            CALL F.READ(FN.LINE,IDD3,R.LINE,F.LINE,E)
            CER.BAL1  = R.LINE<RE.SLB.CLOSING.BAL.LCL>
            CER.BAL1X = R.LINE<RE.SLB.OPEN.BAL.LCL>
*----------------------

*            CER.BAL1  = 0
*            CER.BAL1X = 0
*            CER.BAL1Y = 0

            OTR.BAL1  = 0
            OTR.BAL1X = 0
            OTR.BAL1Y = 0
*********************************************************************************************************
*********************************************************************************************************
*********************************************************************************************************
            AGL.BAL += (AGL.BAL1 * XRATE)
            CER.BAL += (CER.BAL1 * XRATE)
            SAV.BAL += (SAV.BAL1 * XRATE)
            CUR.BAL += (CUR.BAL1 * XRATE)
            OTR.BAL += (OTR.BAL1 * XRATE)
*************************************************
            AGL.BALX += (AGL.BAL1X * XRATE)
            CER.BALX += (CER.BAL1X * XRATE)
            SAV.BALX += (SAV.BAL1X * XRATE)
            CUR.BALX += (CUR.BAL1X * XRATE)
            OTR.BALX += (OTR.BAL1X * XRATE)

        NEXT X
*********************************************************************************************************
        AGL.BAL = FMT((AGL.BAL)/1000,"L0")
        CER.BAL = FMT((CER.BAL)/1000,"L0")
        SAV.BAL = FMT((SAV.BAL)/1000,"L0")
        CUR.BAL = FMT((CUR.BAL)/1000,"L0")
        OTR.BAL = FMT((OTR.BAL)/1000,"L0")
*************************************************
        AGL.BALX = FMT((AGL.BALX)/1000,"L0")
        CER.BALX = FMT((CER.BALX)/1000,"L0")
        SAV.BALX = FMT((SAV.BALX)/1000,"L0")
        CUR.BALX = FMT((CUR.BALX)/1000,"L0")
        OTR.BALX = FMT((OTR.BALX)/1000,"L0")
*********************************************************************************************************


        AGL.BALY = AGL.BAL - AGL.BALX
        CER.BALY = CER.BAL - CER.BALX
        SAV.BALY = SAV.BAL - SAV.BALX
        CUR.BALY = CUR.BAL - CUR.BALX
        OTR.BALY = OTR.BAL - OTR.BALX

        TOT.BALY = AGL.BALY + CER.BALY + SAV.BALY + CUR.BALY + OTR.BALY
*********************************************************************************************************
*        PRINT AGL.BAL:"    ":AGL.BALX:"    ":AGL.BALY
*        PRINT CUR.BAL:"    ":CUR.BALX:"    ":CUR.BALY
*        PRINT SAV.BAL:"    ":SAV.BALX:"    ":SAV.BALY

        T.AGL.BAL += AGL.BAL
        T.CER.BAL += CER.BAL
        T.SAV.BAL += SAV.BAL
        T.CUR.BAL += CUR.BAL
        T.OTR.BAL += OTR.BAL


        T.AGL.BALX += AGL.BALX
        T.CER.BALX += CER.BALX
        T.SAV.BALX += SAV.BALX
        T.CUR.BALX += CUR.BALX
        T.OTR.BALX += OTR.BALX


        T.AGL.BALY += AGL.BALY
        T.CER.BALY += CER.BALY
        T.SAV.BALY += SAV.BALY
        T.CUR.BALY += CUR.BALY
        T.OTR.BALY += OTR.BALY
        T.TOT.BALY += TOT.BALY

*********************************************************************************************************


*Line [ 364 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
        YYBRN  = BRANCH


*------------------------------------------------
        XX= SPACE(140)
        XX<1,1>[1,13]   = YYBRN

        XX<1,1>[14,1]   = '|'
**        XX<1,1>[15,10]  = CUR.BALX
        XX<1,1>[15,10]  = FMT(CUR.BALX,"L0,")
**        XX<1,1>[25,10]  = CUR.BAL
        XX<1,1>[25,10]  = FMT(CUR.BAL,"L0,")
**        XX<1,1>[35,8]   = CUR.BALY
        XX<1,1>[35,8]   = FMT(CUR.BALY,"L0,")

        XX<1,1>[43,1]   = '|'
**        XX<1,1>[44,10]  = SAV.BALX
        XX<1,1>[44,10]  = FMT(SAV.BALX,"L0,")
**        XX<1,1>[54,10]  = SAV.BAL
        XX<1,1>[54,10]  = FMT(SAV.BAL,"L0,")
**        XX<1,1>[64,8]   = SAV.BALY
        XX<1,1>[64,8]   = FMT(SAV.BALY,"L0,")

        XX<1,1>[72,1]   = '|'
**        XX<1,1>[73,10]  = CER.BALX
        XX<1,1>[73,10]  = FMT(CER.BALX,"L0,")
**        XX<1,1>[83,10]  = CER.BAL
        XX<1,1>[83,10]  = FMT(CER.BAL,"L0,")
**        XX<1,1>[93,8]   = CER.BALY
        XX<1,1>[93,8]   = FMT(CER.BALY,"L0,")

        XX<1,1>[101,1]   = '|'
**        XX<1,1>[102,10]  = AGL.BALX
        XX<1,1>[102,10]  = FMT(AGL.BALX,"L0,")
**        XX<1,1>[112,10]  = AGL.BAL
        XX<1,1>[112,10]  = FMT(AGL.BAL,"L0,")
**        XX<1,1>[122,8]   = AGL.BALY
        XX<1,1>[122,8]   = FMT(AGL.BALY,"L0,")

        XX<1,1>[130,1]   = '|'
**        XX<1,1>[131,8]   = TOT.BALY
        XX<1,1>[131,8]   = FMT(TOT.BALY,"L0,")
        PRINT XX<1,1>
*-------------------------------------------------
        XX= SPACE(140)

        XX<1,1>[1,13]    = '_____________'
        XX<1,1>[14,1]    = '|'
        XX<1,1>[15,10]   = '__________'
        XX<1,1>[25,10]   = '__________'
        XX<1,1>[35,8]    = '________'
        XX<1,1>[43,1]    = '|'
        XX<1,1>[44,10]   = '__________'
        XX<1,1>[54,10]   = '__________'
        XX<1,1>[64,8]    = '________'
        XX<1,1>[72,1]    = '|'
        XX<1,1>[73,10]   = '__________'
        XX<1,1>[83,10]   = '__________'
        XX<1,1>[93,8]    = '________'
        XX<1,1>[101,1]   = '|'
        XX<1,1>[102,10]  = '__________'
        XX<1,1>[112,10]  = '__________'
        XX<1,1>[122,8]   = '________'
        XX<1,1>[130,1]   = '|'
        XX<1,1>[131,8]   = '________'

        PRINT XX<1,1>

    NEXT I

*********************************************************************************************************
*    PRINT STR('=',140)

    XX= SPACE(140)

    XX<1,1>[1,13]  = "������ �����"

    XX<1,1>[14,1]   = '|'
**    XX<1,1>[15,10]  = T.CUR.BALX
    XX<1,1>[15,10]  = FMT(T.CUR.BALX,"L0,")
**    XX<1,1>[25,10]  = T.CUR.BAL
    XX<1,1>[25,10]  = FMT(T.CUR.BAL,"L0,")
**    XX<1,1>[35,8]   = T.CUR.BALY
    XX<1,1>[35,8]   = FMT(T.CUR.BALY,"L0,")

    XX<1,1>[43,1]   = '|'
**    XX<1,1>[44,10]  = T.SAV.BALX
    XX<1,1>[44,10]  = FMT(T.SAV.BALX,"L0,")
**    XX<1,1>[54,10]  = T.SAV.BAL
    XX<1,1>[54,10]  = FMT(T.SAV.BAL,"L0,")
**    XX<1,1>[65,8]   = T.SAV.BALY
    XX<1,1>[65,8]   = FMT(T.SAV.BALY,"L0,")

    XX<1,1>[72,1]   = '|'
**    XX<1,1>[73,10]  = T.CER.BALX
    XX<1,1>[73,10]  = FMT(T.CER.BALX,"L0,")
**    XX<1,1>[83,10]  = T.CER.BAL
    XX<1,1>[83,10]  = FMT(T.CER.BAL,"L0,")
**    XX<1,1>[93,8]   = T.CER.BALY
    XX<1,1>[93,8]   = FMT(T.CER.BALY,"L0,")

    XX<1,1>[101,1]   = '|'
**    XX<1,1>[102,10]  = T.AGL.BALX
    XX<1,1>[102,10]  = FMT(T.AGL.BALX,"L0,")
**    XX<1,1>[112,10]  = T.AGL.BAL
    XX<1,1>[112,10]  = FMT(T.AGL.BAL,"L0,")
**    XX<1,1>[122,8]   = T.AGL.BALY
    XX<1,1>[122,8]   = FMT(T.AGL.BALY,"L0,")

    XX<1,1>[130,1]   = '|'
**    XX<1,1>[131,8]   = T.TOT.BALY
    XX<1,1>[131,8]   = FMT(T.TOT.BALY,"L0,")

    PRINT XX<1,1>
    PRINT STR('_',138)


*-------------------------------------------------

RETURN
*********************************************************************************************************
TOTAL.REPORT:

    TT.BALX = T.CUR.BALX + T.SAV.BALX + T.CER.BALX + T.AGL.BALX
    TT.BAL  = T.CUR.BAL  + T.SAV.BAL  + T.CER.BAL  + T.AGL.BAL
    TT.BALY = T.CUR.BALY + T.SAV.BALY + T.CER.BALY + T.AGL.BALY

*-----------------------------------------------------
    XX= SPACE(140)
    PRINT XX<1,1>
    PRINT XX<1,1>
    PRINT XX<1,1>
    PRINT XX<1,1>
    PRINT XX<1,1>
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[30,20]  = '____________________'
    XX<1,1>[50,20]  = '____________________'
    XX<1,1>[70,20]  = '____________________'
    XX<1,1>[90,15]  = '_______________'
    PRINT XX<1,1>
*-----------------------------------------------------
    XX= SPACE(140)

    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = "������ ��������"
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = "����������"
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = "�����������"
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = "�����������"
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD1
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.CUR.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.CUR.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.CUR.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD2
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.SAV.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.SAV.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.SAV.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD3
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.CER.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.CER.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.CER.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD4
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(T.AGL.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(T.AGL.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(T.AGL.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------
    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = S.HEAD5
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = FMT(TT.BALX,"L0,")
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = FMT(TT.BAL,"L0,")
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = FMT(TT.BALY,"L0,")
    XX<1,1>[104,1]  = '|'
    PRINT XX<1,1>
*---------------------------
    GOSUB R.MOVE.LINE
*-----------------------------------------------------


RETURN

*===============================================================
R.MOVE.LINE:

    XX= SPACE(140)
    XX<1,1>[29,1]   = '|'
    XX<1,1>[30,19]  = '___________________'
    XX<1,1>[49,1]   = '|'
    XX<1,1>[50,19]  = '___________________'
    XX<1,1>[69,1]   = '|'
    XX<1,1>[70,19]  = '___________________'
    XX<1,1>[89,1]   = '|'
    XX<1,1>[90,14]  = '______________'
    XX<1,1>[104,1]  = '|'

    PRINT XX<1,1>

RETURN
*===============================================================

PRINT.HEAD:
*---------


    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :H.BRANCH
    PR.HD :="'L'":SPACE(1):" ������� : ":P.DATE:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):PROGRAM.NAME
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):HEAD.A1
    PR.HD :="'L'":SPACE(55):HEAD.B1:SPACE(2):HEAD.B2
    PR.HD :="'L'":SPACE(60):"�� ����� : ":P.WS.DATE
    PR.HD :="'L'":SPACE(50):STR('_',45)
    PR.HD :="'L'":" "


    PR.HD :="'L'":STR('_',138)
    PR.HD :="'L'":SPACE(13):"|":SPACE(5):"�������� �������":SPACE(7):"|":SPACE(6):"������ �������":SPACE (8):"|":SPACE(5):"������ ��������":SPACE(8):"|":SPACE(5):"������� ����":SPACE(11):"|":"������"
    PR.HD :="'L'":"��� �����":SPACE(4):"|":STR('_',28):"|":STR('_',28):"|":STR('_',28):"|":STR('_',28):"|"
    PR.HD :="'L'":SPACE(13):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT1:SPACE(5):TXT2:SPACE(5):TXT3:SPACE(3):"|":TXT4
    PR.HD :="'L'":STR('_',138)





    HEADING PR.HD
RETURN
*==============================================================
END
