* @ValidationCode : MjotMTIwMzQ5NjA2NTpDcDEyNTI6MTY0NTEyNzgwODIxMjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 17 Feb 2022 11:56:48
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>128</Rating>
*-----------------------------------------------------------------------------
*** ���� �������� ���� �� ����� �� ������� ������ �� ���� ***
*** CREATED BY KHALED ***
***=================================

SUBROUTINE SBD.LD.OLD.VALUE.DATE.1.LW
***    PROGRAM    SBD.LD.OLD.VALUE.DATE.1.LW

*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
*------------------------------------------------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*========================================================================
INITIATE:
    REPORT.ID='SBD.LD.OLD.VALUE.DATE.1.LW'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.LD.H = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.H = ''
    CALL OPF(FN.LD.H,F.LD.H)

    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    WS.DATE = TODAY
    CALL CDT('',WS.DATE,'-1W')

    TD  = WS.DATE[3,6]:"..."
    TDD = WS.DATE

    TT   = FMT(TDD,"####/##/##")
    COMP = ID.COMPANY


****    COMP = 'EG0010032'

    DAT.H  = ''     ; V.DATE.H   = '' ; MAT.DATE.H = '' ; DESC = '' ; DESC.H = ''
    AMT.H  = ''     ; CATEG.H = ''    ; RATE.H = ''     ; RATE.AMT.H = '' ; KEYLIST2 = ''
    CUR.H  = ''     ; DAT1.H = ''     ; CATEG.ID.H = '' ; DESC.ID.H = ''
    XX1 = SPACE(132) ; XX2 = SPACE(132) ; XX3 = SPACE(132) ; XX4 = SPACE(132) ; XX5 = SPACE(132)

RETURN
*========================================================================
PROCESS:

    T.SEL = "SELECT ":FN.LD: " WITH (CATEGORY GE 21001 AND CATEGORY LE 21010) AND STATUS NE 'LIQ' AND INPUTTER UNLIKE ...OFS... AND DATE.TIME LIKE ":TD:" AND VALUE.DATE LT ":TDD:" AND CO.CODE EQ ":COMP
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR Z = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<Z>,R.LD,F.LD,E1)

            FIRST.ID = R.LD<LD.LOCAL.REF><1,LDLR.FIRST.LD.ID>
            DAT      = R.LD<LD.VALUE.DATE>
            V.DATE   = DAT[7,2]:'/':DAT[5,2]:'/':DAT[1,4]
            DAT1     = R.LD<LD.FIN.MAT.DATE>
            MAT.DATE = DAT1[7,2]:'/':DAT1[5,2]:'/':DAT1[1,4]
            AMT      = R.LD<LD.AMOUNT>

            CATEG.ID = R.LD<LD.CATEGORY>
*  CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG1)
*Line [ 106 ] UPDATE "*CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID,CATEG1)" TO BE " CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG1)"  - ITSS - R21 Upgrade - 2021-12-23
*Line [ 107 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID,CATEG1)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG1=R.ITSS.CATEGORY<@FM:EB.CAT.DESCRIPTION,2>
            CATEG     = CATEG1[6,20]
            RATE      = R.LD<LD.INTEREST.RATE>
            RATE.AMT  = R.LD<LD.TOT.INTEREST.AMT>
            RATE.SPRD = R.LD<LD.INTEREST.SPREAD>
            CUR       = R.LD<LD.CURRENCY>
            CUS.ID    = R.LD<LD.CUSTOMER.ID>

*Line [ 121 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
            F.ITSS.CUSTOMER = 'F.CUSTOMER'
            FN.F.ITSS.CUSTOMER = ''
            CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
            CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
            LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUS.NAME = LOCAL.REF<1,CULR.ARABIC.NAME>

            XX2<1,1>[1,15]   = CUS.ID
            XX2<1,1>[15,15]  = CUS.NAME
            XX1<1,1>[15,15]  = KEY.LIST<Z>
            XX1<1,1>[30,15]  = V.DATE
            XX1<1,1>[45,15]  = MAT.DATE
            XX1<1,1>[65,15]  = AMT
            XX1<1,1>[80,15]  = CATEG
            XX2<1,1>[90,15]  = RATE
            XX1<1,1>[90,15]  = RATE.SPRD
            XX1<1,1>[100,10] = RATE.AMT
            XX1<1,1>[115,15] = CUR

**********************************************************************************
            IDHIS = KEY.LIST<Z>:";1"

            CALL F.READ(FN.LD.H,IDHIS,R.LD.H,F.LD.H,E1)

            INP        = R.LD.H<LD.INPUTTER>
            DAT.H      = R.LD.H<LD.VALUE.DATE>
            V.DATE.H   = DAT.H[7,2]:'/':DAT.H[5,2]:'/':DAT.H[1,4]
            DAT1.H     = R.LD.H<LD.FIN.MAT.DATE>
            MAT.DATE.H = DAT1.H[7,2]:'/':DAT1.H[5,2]:'/':DAT1.H[1,4]
            AMT.H      = R.LD.H<LD.AMOUNT>
            CATEG.ID.H = R.LD.H<LD.CATEGORY>
* CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID.H,CATEG1.H)
*Line [ 143 ] UPDATE "*CALL DBR ('CATEGORY':@FM:EB.CAT.DESCRIPTION<2,2>,CATEG.ID.H,CATEG1.H)" TO BE " CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID.H,CATEG1.H)"  - ITSS - R21 Upgrade - 2021-12-23
*Line [ 156 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR (('CATEGORY':@FM:EB.CAT.DESCRIPTION)<2,2>,CATEG.ID.H,CATEG1.H)
            F.ITSS.CATEGORY = 'F.CATEGORY'
            FN.F.ITSS.CATEGORY = ''
            CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
            CALL F.READ(F.ITSS.CATEGORY,CATEG.ID.H,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
            CATEG1.H=R.ITSS.CATEGORY<@FM:EB.CAT.DESCRIPTION,2>
            CATEG.H    = CATEG1.H[6,20]
            RATE.H     = R.LD.H<LD.INTEREST.RATE>
            RATE.AMT.H = R.LD.H<LD.TOT.INTEREST.AMT>
            CUR.H      = R.LD.H<LD.CURRENCY>

            XX3<1,1>[1,15]   = IDHIS
            XX3<1,1>[17,15]  = V.DATE.H
            XX3<1,1>[35,15]  = MAT.DATE.H
            XX3<1,1>[52,15]  = AMT.H
            XX3<1,1>[62,15]  = CATEG.H
            XX3<1,1>[73,15]  = RATE.H
            XX3<1,1>[85,10]  = RATE.AMT.H
            XX3<1,1>[100,15] = CUR.H

** FINDSTR 'OFS' IN INP SETTING FMS,VMS THEN
            PRINT XX2<1,1>
            PRINT XX1<1,1>
** END ELSE
** END
            XX1 = SPACE(132) ; XX2 = SPACE(132)

        NEXT Z
    END ELSE
        XX4<1,1>[50,35] = '***  �� ���� ������  ***'
        PRINT XX4<1,1>
    END
*===============================================================
    PRINT STR('=',120)
    XX5<1,1>[50,35] = '***  ����� �������  ***'
    PRINT XX5<1,1>

*===============================================================
RETURN
*===============================================================
PRINT.HEAD:
*---------
*Line [ 199 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
    F.ITSS.COMPANY = 'F.COMPANY'
    FN.F.ITSS.COMPANY = ''
    CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
    CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
    BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN  = BRANCH
    DATY   = WS.DATE
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"������� ���� �� ����� �� ������� ������ �� ����":SPACE(20):"���� ��� ����� ��������"
    PR.HD :="'L'":SPACE(60):"���� : ":TT
    PR.HD :="'L'":SPACE(48):STR('-',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('-',120)
    PR.HD :="'L'":"��� ������":SPACE(5):"��� �������" :SPACE(5):"����� ����":SPACE(5):"����� ���������":SPACE(5):"������" :SPACE(5):"�����":SPACE(5):"�����":SPACE(5):"���� ������":SPACE(5):"������":SPACE(5):"������"
    PR.HD :="'L'":"��� ������":SPACE(77):"�������"
    PR.HD :="'L'":STR('-',120)
**    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
