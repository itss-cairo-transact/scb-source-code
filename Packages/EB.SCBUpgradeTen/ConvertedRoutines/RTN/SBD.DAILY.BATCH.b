* @ValidationCode : Mjo2NTQ0MjM5Njk6Q3AxMjUyOjE2NDA4MjE3MTA1NjE6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 15:48:30
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-56</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMMED SABRY 2010/11/08

PROGRAM SBD.DAILY.BATCH
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_COMMON
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT    I_EQUATE
*Line [ 26 ] HASHING  "$INSERT I_F.SCB.TOPCUS.CR.TOT.LW" - ITSS - R21 Upgrade - 2021-12-23
*$INSERT  I_F.SCB.TOPCUS.CR.TOT.LW
*Line [ 28 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.SCB.CUS.POS.LW
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*************************************************
    EXECUTE "OFSADMINLOGIN"
*************************************************
**** UPDATED BY MOHAMED SABRY 2012/10/11

** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

***--------------- CLEAR FILE SCB.TOPCUS.CR.TOT.LW -------------------***
***---------------        Mohammed Sabry           -------------------***

*    FN.TOP.TOT = "F.SCB.TOPCUS.CR.TOT.LW"
*    F.TOP.TOT = ''
*    CALL OPF(FN.TOP.TOT,F.TOP.TOT)
*    OPEN FN.TOP.TOT TO FILEVAR ELSE ABORT 201, FN.TOP.TOT
*    CLEARFILE FILEVAR

*    FN.CUS.POS="F.SCB.CUS.POS.LW"
*    F.CUS.POS=''
*    R.CUS.POS=''
*    Y.CUS.POS.ID=''
*    Y.CUS.POS.ERR=''
*    CALL OPF(FN.CUS.POS,F.CUS.POS)
*    OPEN FN.CUS.POS TO FILEVAR ELSE ABORT 201, FN.CUS.POS
*    CLEARFILE FILEVAR

***---------------  ����� �� ����� �� ���� ������ FROM T24        -----------------****
***---------------             ��� ������� ��� ������             -----------------****
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBR.ENV.USER")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"TEST.COMMIT.MOD")
***---------------  ����� ����� ��� ������� ����� ��� ��� �������� -----------------****
***---------------                   2011/06/01                   -----------------****
***-------  ������ ��� ���� �������� �� ����� CRB -----
***------- MBAKRY  "2012/01/10------------------------------------
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"CHANGE.CRB.1002")
***------- MBAKRY  "2012/01/10------------------------------------
*********UPDATE NI7OOOO (20110725)******
***��� ����� ��������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.CATEG.TODAY.FILL")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"E.REPORT.CREATE.TODAY")
*********END UPDATE (20110725)*****
*-------------------------------------------------
*** STOPED 2011/06/02
***  ��� ����� ��������
***CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"E.REPORT.CREATE.TODAY")
***  ����� ��� ������ ��� ��������� � ���������
***CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.CATEG.TODAY.FILL")
*-------------------------------------------------
*** ����� ������ ����� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"OFS.UPDAT.COLL")
*** ������ ������� ��� ������ �� ����
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"VVR.TRNS.TODAY")
***����� ��� ������� ������� ��� �������
*    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.CUS.P.D.001")
*** ����� ������ ������� �������� ���������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"TOT.CUST.SUS.MRG")
* --------------------------------------------------------------
***����� ��� �� MID RATE NI7OOO
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.CUR.WRITE")
* --------------------------------------------------------------
*** ����� ����� ����� ������� ;***TWAZON.ALL  NI7OOOO
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.GL.CURR.CREAT.MARKZY")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.GL.CURR.CREATE.PL.MARKZY")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.GL.CURR.CREATE.PL2.MARKZY")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.GL.CURR.CREATE.PL2")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.GL.CURR.CREATE.PL")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SCB.GL.CURR.CREAT")

*** ������ ������� ���������
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.BAL.MONTH.FILL")
*** ����� ������ ������ ������ ������ ��������;SBD.CRT.TRIAL.ALL
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.CRT.TRIAL.01.D")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.CRT.TRIAL.02.D")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.CRT.TRIAL.01.D.AL.NEW")

*** ����� ������ ���� ������� ������� ������ ��������
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.CONV.LQDDTY.TEMP")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.LQDDTY.LG.TEMP")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.LQDDTY.LG.FCY.TEMP")
*** ����� ���� ����� ������� ����� �������
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"TOT.CBE.DEPOSITS.RESERVE")
***  ������ �� ����� ������� ������� ����
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBM.GET.DEPOSITS.LD.FCY")

***--------------- CREATE FILE SCB.TOPCUS.CR.TOT.LW  -------------------------------***
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"VVR.CUS.REC.H.LW")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"E.TOPCUS.ALL.LW")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"E.TOPCUS.LCY.LW")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"E.TOPCUS.FCY.LW")
*--------------------------------------
*** 2021/07/14 **** ELITE CUSTOMER ***

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.VIP.CUS.BAL.FILL")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.VIP.CUS.BAL.01")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.VIP.CUS.BAL.02")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.VIP.CUS.DATA")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.VIP.CUS.PAYROLL.01")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.VIP.CUS.PAYROLL.02")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.VIP.CUS.PAYROLL.03")


*----------------- CREATE SCB.LD BY COPY ALL LDS CREATED FROM 01 RO 31 OF MONTH ----***
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.GET.CREATED.LDS")
***---------------  ����� ����� ��� ������� ����� ��� ��� �������� -----------------****
***---------------                   2011/06/01                   -----------------****
*** KHALED IBRAHIM

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.FUND.DEP.001")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.FUND.DEP.002")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.FUND.DEP.FILL")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.FUND.DEP.003")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.HOLIDAY.TEXT")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.MM.DUCH.001")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.DUCH.001")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.AC.DUCH.001")

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.WEIGHT.AVG.RATE")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.WEIGHT.AVG.RATE.FCY")

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.WEIGHT.AVG.RATE.2")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.WEIGHT.AVG.RATE.FCY.2")

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.CRT.TRIAL.01.D.AL.TARGET")

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.GENLEDALL.SEGM.AL.AVRG.FILL")

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.CUR.RATE7.EGP")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.CUR.ALLRATE.EGP")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.DEP.CUR.ALLRATE.USD")

***----------------------FILE MALIA REPORT------------
*** NI7OOOOOO****
** EXECUTE "SBD.REPORT.CASHFLOW.CO.EZZN.CHQ"
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.REPORT.CASHFLOW.DETAILS.FINAL")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.REPORT.MALIA.FUND")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.REPORT.MALIA.ASSET.ALL")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.FUND.DEP.ALL")
***NI7OO*****20120129
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBM.GET.DEPOSITS.LD.FCY.AA.CAT")
*****END
***--------------- MAHMOUD 2/10/2011
***--------------- LOANS PROG. FOR FINANCIAL DEPT.

*** STOPED AT 20170928 BAKRY ***    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.CR.CUST.DET.ALL")
***********MAHMOUD MAGDY 2019/05/13
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.CR.CUST.DET.4650")
***********MAHMOUD MAGDY 2020/01/19
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.CR.CUST.DET.TARGET")

***--------------- MAHMOUD 5/12/2011
***--------------- FIX CATEG.ENTRY TO ACC CUSTOMER.ID

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"FIX.CUST.CATEG.DAILY")

***--------------- MAHMOUD 14/4/2014
***--------------- CREATE TXN HISTORY FILE FOR INTERNET BANKING CUSTOMERS

    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBR.AC.TXN.HIST")

* ------------------   H A R E S   M A H M O U D   --------------------------------------------
***                                 ����� ��� ��������� ������ ����������� - ������ ������������
*****    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.ALL")
* ---------------------------------------------------------------------------------------------
***---------------  ����� �������� �������� ������ ������� ������ ��������- -------------****
***---------------                MOHAMED SABRY 2011/06/30              ---------------****
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.GET.LIMIT.DATA")
***------------------------------------------------------------------------------------****
***---------------  ����� ���� ��������� ���� �� ������ -------------****
***---------------                BAKRY & REHAM 2013/02/10 ---------------****
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.LD.LIQ")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"FT.TRANS.ADD")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"FT.HAWLA.ADD.IN.USD")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"FT.HAWLA.ADD.OUT.USD")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.CD.AND.LD")

***------------------------------------------------------------------------------------****
*** ������ ����� ������� ��� ���������� ������ �� ����MSABRY 2013/05/23 --------------****

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.OFS.USR.FWD")

*** MSABRY 2014/02/06 ����� ��� ���������� ��� ��� SSL
***    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBR.ENV.USER")
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.ALL.GENLED.02")
*------------------------------------------------------
*** BY MOHAMED MOSSTAFA *****
*** USER GT 45 DAY ****

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBD.USER.END.45")
*-------------------------------------------------------
***--------------------MAHMOUD 10/9/2014-----------------------------------------------****
***--------------------CREATE .CSV FILE OF DETAILED SCCDs------------------------------****

    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"SBR.CBE.SCCD.DET.ALL")
***------------------------------------------------------------------------------------****
** ����� ������ ������ ���� LINE
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.PRT.TRIAL.03.NEW")
***-----------------------------------------------------------
************* NOHA PROGRAMS *****************
*--- NOHA PRFT LOSS
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"EOY.PRFT.LOSS")
    CALL FSCB.RUN.JOB(WS.S,WS.M,WS.A,"PRFT.LOSS.TXT")

***------------------------------------------------------------------------------------****
    EXECUTE "LO"
***------------------------------------------------------------------------------------****
RETURN
***------------------------------------------------------------------------------------****
END
