* @ValidationCode : Mjo3MTY3NzU1NTY6Q3AxMjUyOjE2NDA4NDE1MDQ0ODk6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 21:18:24
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBM.C.CRT.MAST.00.3

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.MAST.AC.LD.A

*---------------------------------------------------
    FN.AC = "FBNK.ACCOUNT"
    F.AC  = ""

    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

    FN.CBE = "F.CBE.MAST.AC.LD.A"
    F.CBE  = ""
*----------------------------------------------------
    WS.CUSTOMER = 0
    WS.CO.CODE =  0
    WS.BR =  0
    WS.CATEG = 0
    WS.AMT = 0
    WS.CY = ""
    WS.LCY = 0
    WS.FCY = 0
    WS.INDS = 0
    WS.OLD.SECTOR = 0
    WS.NEW.SECTOR = 0
    WS.NEW.INDST  = ""
    WS.LEGAL.FORM = 0
    WS.RATE = 0
    WS.MLT.DIVD = 0
    WS.KEY = ""
    R.CBE = ""
    WS.AMT.LOCAL = ""
*-------------------------------------------------
    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.CBE,F.CBE)

*------------------------------------------------
    FN.CLEAR = "F.CBE.MAST.AC.LD.A"
**OPEN FN.CLEAR TO FILEVAR ELSE ABORT 201, FN.CLEAR
    FILEVAR = ''
    CALL OPF(FN.CLEAR,FILEVAR)

    CLEARFILE FILEVAR
*------------------------------------------------
    GOSUB A.100.CRT.AC
RETURN
*------------------------------------------------
A.100.CRT.AC:
    SEL.CMD = "SELECT ":FN.AC
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.AC.ID FROM SEL.LIST SETTING POS
    WHILE WS.AC.ID:POS
        CALL F.READ(FN.AC,WS.AC.ID,R.AC,F.AC,MSG.AC)

        IF R.AC<AC.CUSTOMER> NE "" THEN
            GOTO A.100.A
        END
        WS.CUSTOMER = ""

        WS.CO.CODE =  R.AC<AC.CO.CODE>
        WS.BR =  WS.CO.CODE[2]
        WS.CATEG = 0
        WS.CATEG = R.AC<AC.CATEGORY>
        WS.AMT = R.AC<AC.OPEN.ACTUAL.BAL>
        WS.CY = R.AC<AC.CURRENCY>
        IF WS.CY EQ "NZD" THEN
            GOTO A.100.A
        END

        WS.KEY = WS.AC.ID
*      GOSUB A.300.GET.CUS

        IF WS.CY EQ "EGP" THEN
            WS.LCY = WS.AMT
            WS.FCY = WS.AMT
        END

        IF WS.CY NE "EGP" THEN
            GOSUB A.400.GET.CY
            WS.LCY = WS.AMT.LOCAL
            WS.FCY = WS.AMT

        END
        GOSUB  A.500.CRT.REC
A.100.A:
    REPEAT
RETURN
*------------------------------------------------
*A.300.GET.CUS:
*    CALL F.READ(FN.CUS,WS.CUSTOMER,R.CUS,F.CUS,MSG.CUS)
*    IF MSG.CUS EQ "" THEN
*        WS.INDS = R.CUS<EB.CUS.INDUSTRY>
*        WS.OLD.SECTOR = R.CUS<EB.CUS.SECTOR>
*        WS.NEW.SECTOR = R.CUS<EB.CUS.LOCAL.REF,CULR.NEW.SECTOR>
*        WS.LEGAL.FORM = R.CUS<EB.CUS.LOCAL.REF,CULR.LEGAL.FORM>
*        RETURN
*    END
*    WS.INDS = 0
*    WS.OLD.SECTOR = 0
*    WS.NEW.SECTOR = 0
*    WS.LEGAL.FORM = 0
*    RETURN

*------------------------------------------------
A.400.GET.CY:
    WS.KEYCY = "NZD"
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    IF MSG.CY EQ "" THEN
        GOSUB A.400.CALC
    END

RETURN

A.400.CALC:
    A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 147 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
    WS.RATE.ARY = R.CCY<RE.BCP.RATE>
    WS.RATE = WS.RATE.ARY<1,CYPOS>
    WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
    WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>
    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT * WS.RATE
    END
    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT / WS.RATE
    END
RETURN
*--------------------------------------------------------

A.500.CRT.REC:
    R.CBE<CC.CBEM.CUSTOMER.CODE> = WS.CUSTOMER
    R.CBE<CC.CBEM.BR> = WS.BR
    R.CBE<CC.CBEM.NEW.SECTOR> = 0

    R.CBE<CC.CBEM.INDUSTRY> =  0
    R.CBE<CC.CBEM.OLD.SECTOR> = 0
    R.CBE<CC.CBEM.LEGAL.FORM> =  0
    R.CBE<CC.CBEM.CATEG> =  WS.CATEG
    R.CBE<CC.CBEM.CY> =  WS.CY
    R.CBE<CC.CBEM.IN.LCY> =  WS.LCY
    R.CBE<CC.CBEM.IN.FCY> =  WS.FCY
    R.CBE<CC.CBEM.MRGN.LCY> = 0
    R.CBE<CC.CBEM.MRGN.FCY> = 0
    R.CBE<CC.CBEM.MRGN.PRCNT> = 0
    R.CBE<CC.CBEM.PRODCT.TYPE> = ""
    R.CBE<CC.CBEM.BENF.STAT>   = ""

    IF WS.CATEG EQ 16170 THEN
        R.CBE<CC.CBEM.CUSTOMER.CODE> = WS.CUSTOMER
        R.CBE<CC.CBEM.BR>            = WS.BR
        R.CBE<CC.CBEM.NEW.SECTOR>    = 4650
        R.CBE<CC.CBEM.INDUSTRY>      = 1100
        R.CBE<CC.CBEM.OLD.SECTOR>    = 1100
        R.CBE<CC.CBEM.LEGAL.FORM>    = 0
        R.CBE<CC.CBEM.CATEG>         = WS.CATEG
        R.CBE<CC.CBEM.CY>            = WS.CY
        R.CBE<CC.CBEM.IN.LCY>        = WS.LCY
        R.CBE<CC.CBEM.IN.FCY>        = WS.FCY
        R.CBE<CC.CBEM.MRGN.LCY>      = 0
        R.CBE<CC.CBEM.MRGN.FCY>      = 0
        R.CBE<CC.CBEM.MRGN.PRCNT>    = 0
        R.CBE<CC.CBEM.PRODCT.TYPE>   = ""
        R.CBE<CC.CBEM.BENF.STAT>     = ""
    END

    CALL F.WRITE(FN.CBE,WS.KEY,R.CBE)
    CALL  JOURNAL.UPDATE(WS.KEY)
RETURN
END
