* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-42</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.CD.SUEZ.BNK.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    GOSUB INITIATE
    GOSUB GET.DATA

    RETURN
*-----------------------------------------------------
INITIATE:

    WS.COMP = ID.COMPANY

    FN.CU  = "FBNK.CUSTOMER"            ; F.CU   = ""
    CALL OPF(FN.CU,F.CU)
    FN.LD  = 'FBNK.LD.LOANS.AND.DEPOSITS' ;F.LD=''
    CALL OPF(FN.LD,F.LD)

    RETURN
*-----------------------------------------------------
GET.DATA:
*************
    ID.DATE = TODAY
    CALL CDT('',ID.DATE,'-2W')


*   T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21101 AND CATEGORY LE 21103) AND VALUE.DATE EQ ": ID.DATE : " AND CORPT.FLG EQ 'NO' AND ( BANK NE '0017' AND BANK NE '0009' AND BANK NE '' ) "
    T.SEL  = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21101 AND CATEGORY LE 21103) AND VALUE.DATE EQ ": ID.DATE : " AND  CORPT.FLG EQ 'NO' AND ( BANK NE '0017' AND BANK NE '0009' AND BANK NE '' ) "

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED,SSS )


    FOR I = 1 TO SELECTED
*************
        CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,ERRLD)
        CALL F.READ(FN.CU,R.LD<LD.CUSTOMER.ID>,R.CU,F.CU,ERRCU)

        WS.MOB.TEL          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>


        GOSUB PRINT.SMS

    NEXT I
    RETURN
*-----------------------------------------------------
PRINT.SMS:

    REPORT.ID='SBD.CD.SUEZ.BNK.CUS'
    CALL PRINTER.ON(REPORT.ID,'')

    PR.HD  =REPORT.ID
   * HEADING PR.HD

    PRINT PR.HD:KEY.LIST<I>:',':WS.MOB.TEL<1,1,3>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*-----------------------------------------------------
