* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*** ���� ������ ������� ***
*** CREATED BY KHALED ***
***=================================
    SUBROUTINE SBD.LOAN.DEP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 47 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 48 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='SBD.LOAN.DEP'
    CALL PRINTER.ON(REPORT.ID,'')
    Y.DIFF.BAL1 = 0
    Y.OPEN.BAL = 0
    Y.CLOSE.BAL = 0
    Y.TOT.OPEN.BAL = 0
    Y.TOT.CLOSE.BAL = 0
    Y.TOT.DIFF.BAL = 0
    Y.TOT.OPEN.PER = 0
    Y.TOT.OPEN.BAL.PER = 0
    Y.TOT.OPEN.BAL.PER1 = 0
    Y.TOT.CLOSE.PER = 0
    Y.TOT.CLOSE.BAL.PER1 = 0
    Y.TOT.CLOSE.BAL.PER1 = 0
    COMP = ID.COMPANY
    RETURN
*===============================================================
CALLDB:
    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    FN.BAL = 'FBNK.RE.STAT.LINE.BAL' ; F.BAL = ''

    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.BAL,F.BAL)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""

    DAT.ID = COMP
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD1=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>

*************************************************************************
    T.SEL = "SELECT ":FN.LN: " WITH @ID EQ GENLED.0020 OR @ID EQ GENLED.0030 OR @ID EQ GENLED.0031 OR @ID EQ GENLED.0050 OR @ID EQ GENLED.0060 OR @ID EQ GENLED.0220 OR @ID EQ GENLED.0230 OR @ID EQ GENLED.0231 OR @ID EQ GENLED.0250 OR @ID EQ GENLED.0260 OR @ID EQ GENLED.0510 OR @ID EQ GENLED.0520 OR @ID EQ GENLED.0530 OR @ID EQ GENLED.0540 OR @ID EQ GENLED.0550 OR @ID EQ GENLED.0560 OR @ID EQ GENLED.0570 OR @ID EQ GENLED.0710 OR @ID EQ GENLED.0720 OR @ID EQ GENLED.0730 OR @ID EQ GENLED.0740 OR @ID EQ GENLED.0750 OR @ID EQ GENLED.0760"
    T.SEL := " BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E1)
            Y.DESC = R.LN<RE.SRL.DESC,2,2>
            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            BAL.ID = FIELD(KEY.LIST<I>,".",1):"-":DESC.ID:"-":"...":"-":TD1:"*":COMP
*************************************************************************
            T.SEL1 = "SELECT ":FN.BAL: " WITH @ID LIKE ":BAL.ID:"... AND @ID UNLIKE ...LOCAL..."
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 THEN
                FOR X = 1 TO SELECTED1
                    CALL F.READ(FN.BAL,KEY.LIST1<X>,R.BAL,F.BAL,E2)
                    CUR = KEY.LIST1<X>[13,3]
                    Y.OPEN.BAL.O  =    R.BAL<RE.SLB.OPEN.BAL>
                    Y.CLOSE.BAL.O =    R.BAL<RE.SLB.CLOSING.BAL>

                    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 109 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                    RATE = R.CCY<RE.BCP.RATE,POS>
                    Y.OPEN.BAL += Y.OPEN.BAL.O * RATE
                    Y.CLOSE.BAL += Y.CLOSE.BAL.O * RATE

                    Y.OPEN.BAL = DROUND(Y.OPEN.BAL,'2')
                    Y.CLOSE.BAL = DROUND(Y.CLOSE.BAL,'2')

                NEXT X
            END
*************************************************************************
            XX  = SPACE(120)

            Y.DIFF.BAL1     = Y.CLOSE.BAL     -  Y.OPEN.BAL
            Y.TOT.OPEN.BAL  = Y.TOT.OPEN.BAL  +  Y.OPEN.BAL
            Y.TOT.CLOSE.BAL = Y.TOT.CLOSE.BAL +  Y.CLOSE.BAL
            Y.TOT.DIFF.BAL  = Y.TOT.CLOSE.BAL -  Y.TOT.OPEN.BAL

            XX<1,1>[1,35]   = Y.DESC
            XX<1,1>[45,15]  = Y.OPEN.BAL
            XX<1,1>[75,15]  = Y.CLOSE.BAL
            XX<1,1>[105,15] = Y.DIFF.BAL1

            PRINT XX<1,1>

            Y.DESC = ''
            Y.OPEN.BAL  = 0
            Y.CLOSE.BAL = 0
            Y.DIFF.BAL1 = 0

            IF DESC.ID EQ "0260" THEN
                XX1 = SPACE(120)
                PRINT STR('-',120)
                XX1<1,1>[1,35]   = "������ ������"
                XX1<1,1>[45,15]  = Y.TOT.OPEN.BAL
                XX1<1,1>[75,15]  = Y.TOT.CLOSE.BAL
                XX1<1,1>[105,15] = Y.TOT.DIFF.BAL
                PRINT XX1
                PRINT STR('-',120)

                Y.TOT.OPEN.BAL.PER  = Y.TOT.OPEN.BAL
                Y.TOT.CLOSE.BAL.PER = Y.TOT.CLOSE.BAL

                Y.TOT.OPEN.BAL  = 0
                Y.TOT.CLOSE.BAL = 0
                Y.TOT.DIFF.BAL  = 0

            END ELSE

                IF DESC.ID EQ "0760" THEN
                    PRINT STR('-',120)
                    XX1 = SPACE(120)

                    XX1<1,1>[1,35]   = "������ �������"
                    XX1<1,1>[45,15]  = Y.TOT.OPEN.BAL
                    XX1<1,1>[75,15]  = Y.TOT.CLOSE.BAL
                    XX1<1,1>[105,15] = Y.TOT.DIFF.BAL

                    PRINT XX1
                    PRINT STR('-',120)
                    Y.TOT.OPEN.BAL.PER1 = Y.TOT.OPEN.BAL
                    Y.TOT.CLOSE.BAL.PER1 = Y.TOT.CLOSE.BAL

                    Y.TOT.OPEN.BAL = 0
                    Y.TOT.CLOSE.BAL = 0
                    Y.TOT.DIFF.BAL = 0

                END
            END

        NEXT I
        Y.TOT.OPEN.PER  = (Y.TOT.OPEN.BAL.PER  / Y.TOT.OPEN.BAL.PER1)  * 100
        Y.TOT.CLOSE.PER = (Y.TOT.CLOSE.BAL.PER / Y.TOT.CLOSE.BAL.PER1) * 100

        XX2 = SPACE(120)
        XX2<1,1>[1,35]   = "������"
        CALL EB.ROUND.AMOUNT ('EGP',Y.TOT.OPEN.PER,'',"")
        XX2<1,1>[45,15]  = Y.TOT.OPEN.PER:"%"
        CALL EB.ROUND.AMOUNT ('EGP',Y.TOT.CLOSE.PER,'',"")
        XX2<1,1>[75,15]  = Y.TOT.CLOSE.PER:"%"
        PRINT XX2

    END
    RETURN
*===============================================================
PRINT.HEAD:
*---------

*Line [ 205 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    TD = TODAY
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ���� ������ ������� �� :  ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":"����������" :SPACE(35):"����� ������":SPACE(20):"����� ������":SPACE(20):"��������"
    PR.HD :="'L'":STR('_',120)
**    PRINT
    HEADING PR.HD
    RETURN
*==============================================================
END
