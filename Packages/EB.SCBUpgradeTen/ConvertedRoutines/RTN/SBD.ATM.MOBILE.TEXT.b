* @ValidationCode : MjoxODg3NDM0NzY2OkNwMTI1MjoxNjQwNzQwNjYzNDA5OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 17:17:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
SUBROUTINE SBD.ATM.MOBILE.TEXT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.ATM.MOBILE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*--------------------------------------------
    EXECUTE "SELECT ATM.MOBILE"
    EXECUTE "DELETE ATM.MOBILE"


    PAY.TIME.1 = TIME()
    PAY.TIME.2 = OCONV(PAY.TIME.1,'MTS')
    PAY.TIME   = PAY.TIME.2[1,2]:PAY.TIME.2[4,2]:PAY.TIME.2[7,2]

    WS.FILE.ID = "SMS-SCB-":TODAY:PAY.TIME:".txt"

    OPENSEQ "ATM.MOBILE" , WS.FILE.ID TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"ATM.MOBILE":' ':WS.FILE.ID
        HUSH OFF
    END
    OPENSEQ "ATM.MOBILE" , WS.FILE.ID TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SMS-SCB CREATED IN ATM.MOBILE'
        END ELSE
            STOP 'Cannot create SMS-SCB File IN ATM.MOBILE'
        END
    END

    YTEXT = "Enter the Date And Time (yymmddhhmm) : "
    CALL TXTINP(YTEXT, 8, 22, "10", "")

    WS.DATE.TIME = COMI
    WS.BANK.NAME = 'SCB'

    ATM.DATA.H  = 'H'
    ATM.DATA.H := TODAY
    ATM.DATA.H := FMT(WS.BANK.NAME,"L#40")

    WRITESEQ ATM.DATA.H TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    FN.ATM = 'F.SCB.ATM.MOBILE' ; F.ATM = ''
    CALL OPF(FN.ATM,F.ATM)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    KEY.LIST="" ; SELECTED="" ; ER.MSG=""
    FLAG = 0 ; WS.COUNT = 0

******************************************************************
    T.SEL = "SELECT ":FN.ATM:" WITH DATE.TIME GE ":WS.DATE.TIME
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ATM,KEY.LIST<I>,R.ATM,F.ATM,E1)

            WS.PAN        = KEY.LIST<I>
            WS.MOBILE     = R.ATM<SAM.MOBILE.NO>
            WS.CARD.TYPE  = R.ATM<SAM.CARD.TYPE>
            WS.SMS.ONLINE = R.ATM<SAM.SMS.ONLINE>
            WS.SMS.STMT   = R.ATM<SAM.SMS.STATEMENT>
            WS.CARD       = WS.PAN:","

***** GET CUSTOMER.ID FROM TEXT FILE ****

            SEQ.FILE.NAME = 'MECH'
            RECORD.NAME = 'SMS.csv'
            OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
                PRINT 'Unable to Locate ':SEQ.FILE.POINTER
                STOP
                RETURN
            END
            EOF = ''

            LOOP WHILE NOT(EOF)

                READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN

                    IF INDEX(Y.MSG,WS.CARD,1) THEN
                        PROG.ID = FIELD(Y.MSG,WS.CARD,2)
                        WS.CUST.NO = PROG.ID
                        FLAG = 1
                        WS.COUNT++
                    END

                END ELSE
                    EOF = 1
                END
            REPEAT
            CLOSESEQ SEQ.FILE.POINTER

**************
            IF FLAG = 1 THEN
                ATM.DATA  = FMT(WS.CUST.NO,"L#20")
                ATM.DATA := FMT(WS.PAN,"L#19")
                ATM.DATA := FMT(WS.MOBILE,"L#14")
                ATM.DATA := WS.SMS.ONLINE
                ATM.DATA := WS.SMS.STMT
                ATM.DATA := WS.CARD.TYPE

                WRITESEQ ATM.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
            FLAG = 0
        NEXT I
    END

    ATM.DATA.T = 'T'
    ATM.DATA.T := FMT(WS.COUNT,"L#6")

    WRITESEQ ATM.DATA.T TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
*************************************************************
    TEXT = 'DONE' ; CALL REM

RETURN

END
