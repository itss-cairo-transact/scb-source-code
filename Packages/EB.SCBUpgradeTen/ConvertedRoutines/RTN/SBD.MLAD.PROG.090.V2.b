* @ValidationCode : MjotMTUzMjA2OTQ3NzpDcDEyNTI6MTY0MDgyODg1NzEzMTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 29 Dec 2021 17:47:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>165</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.MLAD.PROG.090.V2
***    PROGRAM SBD.MLAD.PROG.090.V2

***                            ����� / ���� �����
***                    ���� �������� ������  ����� �������
***      MID RATE                         ������ ��� �������� ����� �����
***
*   ------------------------------
    $INSERT    I_COMMON
    $INSERT    I_EQUATE

*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MLAD.FILE.020
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*-------------------------------------------------------------------------

    GOSUB INIT


    GOSUB OPENFILES

*-------------------------------
**OPEN FN.MLAD020 TO FILEVAR ELSE ABORT 201, FN.MLAD020
    FILEVAR = ''
    CALL OPF(FN.MLAD020,FILEVAR)

    CLEARFILE FILEVAR
*-------------------------------

*    GOSUB READ.RE.BASE.CCY.PARAM
    GOSUB READ.CURRENCY.FILE

    GOSUB PRODUCE.DUMMY.CURRENCY:

*-------------------------------

RETURN
*-------------------------------------------------------------------------
INIT:


    PROGRAM.ID = "SBD.MLAD.PROG.090.V2"


    FN.MLAD020 = "F.SCB.MLAD.FILE.020"
    F.MLAD020  = ""
    R.MLAD020  = ""
    Y.MLAD020.ID  = ""


    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'
    F.CUR   = ''
    R.CUR   = ''

    FN.CCY  = 'FBNK.CURRENCY'
    F.CCY   = ''
    R.CCY   = ''
    Y.CCY.ID = ''
*---------------------------------------
    DIM CY.PRT(8)

    CY.PRT(1) = "EGP"
    CY.PRT(2) = "USD"
    CY.PRT(3) = "EUR"
    CY.PRT(4) = "GBP"
    CY.PRT(5) = "CHF"
    CY.PRT(6) = "SAR"
    CY.PRT(7) = "JPY"

*               ������� �������� ���� ��� �� ���� �� ���� �������
*                     ���� ���� ���� �� ������� ���� ���� ��������

    CY.PRT(8) = "ZZZ"

*---------------------------------------
    DIM X.DES(25)

    X.DES(1) = "����� ������ �������� ��� �������"
    X.DES(2) = "����� ��� ������"
    X.DES(3) = "����� ������� ����� ������� ���������"
    X.DES(4) = "���� �����"
    X.DES(5) = "���� ������"
    X.DES(6) = "���� ������� � ����� ������ ����"
    X.DES(7) = "���� ����� ���� ��������"
    X.DES(8) = "���� �������� ������"
    X.DES(9) = "���� �������� �������"
    X.DES(10) = "����� ������ ������"
    X.DES(11) = "���� ����� ����� ������� �������"
    X.DES(12) = "��������� ����� ����� �����"
    X.DES(13) = "��������� ����� ����� ���"
    X.DES(14) = "��������� ����� �� ����� �����"
    X.DES(15) = "����� ����� ����� ����"


    X.DES(16) = " (1) ������ ������"
    X.DES(17) = ""
    X.DES(18) = ""
    X.DES(19) = ""
    X.DES(20) = ""
    X.DES(21) = ""
    X.DES(22) = ""
    X.DES(23) = ""
    X.DES(24) = ""
    X.DES(25) = ""




*---------------------------
    DIM Y.DES(25)

    Y.DES(1) = "����� ������ ������"
    Y.DES(2) = "����� ������� ������"
    Y.DES(3) = "���� �����"
    Y.DES(4) = "���� ������"
    Y.DES(5) = "����� �������"
    Y.DES(6) = "����� ������� ������� ������"
    Y.DES(7) = "������ ����� �����"
    Y.DES(8) = "�������� ����� ���� ��������"
    Y.DES(9) = "����� ��� �����"
    Y.DES(10) = "���� ���� ����� �������"
    Y.DES(11) = "�������� ����� ����� ������� �������"
    Y.DES(12) = "������"
    Y.DES(13) = "���� �������"
    Y.DES(14) = "����� ����� ��������� ����"

    Y.DES(15) = " (2) ������ ����������"
    Y.DES(16) = " (2-1)=(3) ������ �������"
    Y.DES(17) = "���� ������� ������ �������"
    Y.DES(18) = "������� �� ���� �������"
    Y.DES(19) = " (1) ������ ���������"
    Y.DES(20) = "���� ������� ������ ���������"
    Y.DES(21) = "������� �� ���� �������"
    Y.DES(22) = ""
    Y.DES(23) = ""
    Y.DES(24) = ""
    Y.DES(25) = ""



*---------------------------
    DIM X.RANGE(25)
    X.RANGE(1)  = FMT("010","R%3")
    X.RANGE(2)  = FMT("020","R%3")
    X.RANGE(3)  = FMT("030","R%3")
    X.RANGE(4)  = FMT("040","R%3")
    X.RANGE(5)  = FMT("050","R%3")
    X.RANGE(6)  = FMT("060","R%3")
    X.RANGE(7)  = FMT("070","R%3")
    X.RANGE(8)  = FMT("080","R%3")
    X.RANGE(9)  = FMT("090","R%3")
    X.RANGE(10) = FMT("100","R%3")
    X.RANGE(11) = FMT("110","R%3")
    X.RANGE(12) = FMT("120","R%3")
    X.RANGE(13) = FMT("130","R%3")
    X.RANGE(14) = FMT("140","R%3")
    X.RANGE(15) = FMT("150","R%3")
    X.RANGE(16) = FMT("160","R%3")
    X.RANGE(17) = FMT("170","R%3")
    X.RANGE(18) = FMT("180","R%3")
    X.RANGE(19) = FMT("190","R%3")
    X.RANGE(20) = FMT("200","R%3")
    X.RANGE(21) = FMT("210","R%3")
    X.RANGE(22) = FMT("220","R%3")
    X.RANGE(23) = FMT("230","R%3")
    X.RANGE(24) = FMT("240","R%3")
    X.RANGE(25) = FMT("250","R%3")



*---------------------------
    DIM Y.RANGE(25)
    Y.RANGE(1)  = FMT("010","R%3")
    Y.RANGE(2)  = FMT("020","R%3")
    Y.RANGE(3)  = FMT("030","R%3")
    Y.RANGE(4)  = FMT("040","R%3")
    Y.RANGE(5)  = FMT("050","R%3")
    Y.RANGE(6)  = FMT("060","R%3")
    Y.RANGE(7)  = FMT("070","R%3")
    Y.RANGE(8)  = FMT("080","R%3")
    Y.RANGE(9)  = FMT("090","R%3")
    Y.RANGE(10) = FMT("100","R%3")
    Y.RANGE(11) = FMT("110","R%3")
    Y.RANGE(12) = FMT("120","R%3")
    Y.RANGE(13) = FMT("130","R%3")
    Y.RANGE(14) = FMT("140","R%3")
    Y.RANGE(15) = FMT("150","R%3")
    Y.RANGE(16) = FMT("160","R%3")
    Y.RANGE(17) = FMT("170","R%3")
    Y.RANGE(18) = FMT("180","R%3")
    Y.RANGE(19) = FMT("190","R%3")
    Y.RANGE(20) = FMT("200","R%3")
    Y.RANGE(21) = FMT("210","R%3")
    Y.RANGE(22) = FMT("220","R%3")
    Y.RANGE(23) = FMT("230","R%3")
    Y.RANGE(24) = FMT("240","R%3")
    Y.RANGE(25) = FMT("250","R%3")

*---------------------------
    NO.X = 15
    NO.Y = 14

RETURN
*-------------------------------------------------------------------------
OPENFILES:
    CALL OPF(FN.MLAD020,F.MLAD020)
    CALL OPF(FN.CUR,F.CUR)
    CALL OPF(FN.CCY,F.CCY)


RETURN
*--------------------------------------------------------------------------
READ.CURRENCY.FILE:

    T.SEL  = "SELECT FBNK.CURRENCY WITH @ID NE 'NZD' AND @ID NE 'ITL'"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, CCY.ERR)

    NO.CURR = SELECTED
    FOR I = 1 TO NO.CURR

        CALL F.READ(FN.CCY,KEY.LIST<I>,R.CCY,F.CCY,CCY.ERR)
        CY.CURR = KEY.LIST<I>
*        CUR.COD = KEY.LIST<I>
*        --------------------------
        CY.NAME = R.CCY<EB.CUR.CCY.NAME><1,2>
*        --------------------------

        IF CY.CURR EQ 'JPY' THEN
            CY.RATE = R.CCY<EB.CUR.MID.REVAL.RATE><1,1> / 100
        END ELSE
            CY.RATE = R.CCY<EB.CUR.MID.REVAL.RATE><1,1>
        END
*        --------------------------
        IF CY.CURR EQ 'EGP' THEN
            CY.RATE = 1
        END

*       --------------------------
*                         �������� ����� ���� �������
        IF CY.CURR EQ 'USD' THEN
            USD.RATE = CY.RATE
        END
*  --------------------------
        FOR X = 1 TO NO.X

            Y.MLAD20.ID = "X-":X.RANGE(X):"-":CY.CURR
            Y.MLAD20.ID = CY.CURR:"-X-":X.RANGE(X)

            R.MLAD020<ML020.LINE.DESCR>    = X.DES(X)

*            PRINT Y.MLAD20.ID:"   ":X.DES(X)

            GOSUB WRITE.MLAD.FILE.020

        NEXT X
*  --------------------------
        FOR Y = 1 TO NO.Y

            Y.MLAD20.ID = "Y-":Y.RANGE(Y):"-":CY.CURR
            Y.MLAD20.ID = CY.CURR:"-Y-":Y.RANGE(Y)

            R.MLAD020<ML020.LINE.DESCR>    = Y.DES(Y)

*            PRINT Y.MLAD20.ID:"    ":X.DES(Y)

            GOSUB WRITE.MLAD.FILE.020

        NEXT Y
*  --------------------------
    NEXT I
RETURN
*------------------------------------------------------------------------------------------
PRODUCE.DUMMY.CURRENCY:


    CY.CURR = "ZZZ"
    CY.RATE = 1 / USD.RATE

*  --------------------------
    FOR X = 1 TO NO.X


        Y.MLAD20.ID = "X-":X.RANGE(X):"-":CY.CURR
        Y.MLAD20.ID = CY.CURR:"-X-":X.RANGE(X)

        R.MLAD020<ML020.LINE.DESCR>    = X.DES(X)


*            PRINT Y.MLAD20.ID:"   ":X.DES(X)



        GOSUB WRITE.MLAD.FILE.020

    NEXT X
*  --------------------------
    FOR Y = 1 TO NO.Y


        Y.MLAD20.ID = "Y-":Y.RANGE(Y):"-":CY.CURR
        Y.MLAD20.ID = CY.CURR:"-Y-":Y.RANGE(Y)

        R.MLAD020<ML020.LINE.DESCR>    = Y.DES(Y)


*            PRINT Y.MLAD20.ID:"    ":X.DES(Y)


        GOSUB WRITE.MLAD.FILE.020


    NEXT Y


RETURN
WRITE.MLAD.FILE.020:
*-----------------------


*    CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CY.CURR,CY.NAME)

*   ------------------------------
    IF CY.CURR EQ 'ZZZ' THEN
        CY.NAME = " ����� ���� ����� ��������  "
    END
*   ------------------------------


    PRINT.FLAG = 'N'
    FOR DDD = 1 TO 8
        IF CY.CURR = CY.PRT(DDD) THEN
            PRINT.FLAG = 'Y'
        END
    NEXT DDD



    R.MLAD020<ML020.RATE>           = CY.RATE
    R.MLAD020<ML020.CURRENCY>       = CY.CURR
    R.MLAD020<ML020.CURR.NAME>      = CY.NAME
    R.MLAD020<ML020.NEXT.DAY>       = 0
    R.MLAD020<ML020.NEXT.WEEK>      = 0
    R.MLAD020<ML020.NEXT.MONTH>     = 0
    R.MLAD020<ML020.NEXT.3MONTH>    = 0
    R.MLAD020<ML020.NEXT.6MONTH>    = 0
    R.MLAD020<ML020.NEXT.YEAR>      = 0
    R.MLAD020<ML020.NEXT.3YEAR>     = 0
    R.MLAD020<ML020.NEXT.MORE>      = 0
    R.MLAD020<ML020.PRINTABLE.CODE> = PRINT.FLAG
    R.MLAD020<ML020.RECORD.STATUS>  = 'N'


*------------------------
    R.MLAD020<ML020.RESERVED1> = Y.MLAD020.ID[5,5]
    CALL F.WRITE(FN.MLAD020,Y.MLAD20.ID,R.MLAD020)
    CALL JOURNAL.UPDATE(Y.MLAD20.ID)

RETURN
*===============================================================
END
