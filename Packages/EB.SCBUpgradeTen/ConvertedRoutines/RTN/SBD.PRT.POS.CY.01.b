* @ValidationCode : MjoxMzU3NjUzNjY2OkNwMTI1MjoxNjQwODMxMDkxODA1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:24:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>1222</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE  SBD.PRT.POS.CY.01
*    PROGRAM  SBD.PRT.POS.CY.01
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.D.DEF.POS.CY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*-----------------------------------------------
    FN.DEFPOS = "F.SCB.D.DEF.POS.CY"
    F.DEFPOS  = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""

    FN.DATE = "F.DATES"
    F.DATE  = ""

*--------------------------------------------
    CALL OPF (FN.DEFPOS,F.DEFPOS)
    CALL OPF (FN.COMP,F.COMP)
    CALL OPF (FN.DATE,F.DATE)
*------------------------------------------------CLEAR AREA
***    REPORT.ID='SBM.C.PRT.001'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""

    WS.FLAG.FRST = 0
    WS.BR = 0
    WS.FLAG = 0
    WS.PAG = 0
*                                                          PRINTER HEADER
    HEAD.01.TITL = "��� ������ ������ �������� �� ���"
    HEAD.01.PROG = "PROGRAM:-SBD.PRT.POS.CY.01"
    HEAD.01 = SPACE(50):HEAD.01.TITL:SPACE(25):HEAD.01.PROG
*.....................................................
    HEAD.02.PAG = "��� ����":" : "
    HEAD.02.TITL = ""
    HEAD.02 = SPACE(50):HEAD.02.TITL:SPACE(67):HEAD.02.PAG
*.....................................................
    HEAD.03.A = "������ ������"
    HEAD.03.B = "������� "
    HEAD.03.C = "��� �������� ��������"
    HEAD.03.D = "������� ��� �����"
    HEAD.03 = SPACE(09):HEAD.03.A:SPACE(08):HEAD.03.B:SPACE(10):HEAD.03.C:SPACE(08):HEAD.03.D

    HEAD.04.A = "��� ������ � ������"
    HEAD.04 = SPACE(15):HEAD.04.A
********************** ********************************
*ACUMLATE  ALL BRANCHES IN AREA
    DIM ARRAY1(20,5)

    ARRAY1(1,1) = ""
    ARRAY1(1,2) = "0"
    ARRAY1(1,3) = "0"
    ARRAY1(1,4) = "0"
    ARRAY1(1,5) = "0"

    ARRAY1(2,1) = ""
    ARRAY1(2,2) = "0"
    ARRAY1(2,3) = "0"
    ARRAY1(2,4) = "0"

    ARRAY1(2,5) = "0"

    ARRAY1(3,1) = ""
    ARRAY1(3,2) = "0"
    ARRAY1(3,3) = "0"
    ARRAY1(3,4) = "0"
    ARRAY1(3,5) = "0"

    ARRAY1(4,1) = ""
    ARRAY1(4,2) = "0"
    ARRAY1(4,3) = "0"
    ARRAY1(4,4) = "0"
    ARRAY1(4,5) = "0"

    ARRAY1(5,1) = ""
    ARRAY1(5,2) = "0"
    ARRAY1(5,3) = "0"
    ARRAY1(5,4) = "0"
    ARRAY1(5,5) = "0"

    ARRAY1(6,1) = ""
    ARRAY1(6,2) = "0"

    ARRAY1(6,3) = "0"
    ARRAY1(6,4) = "0"
    ARRAY1(6,5) = "0"

    ARRAY1(7,1) = ""
    ARRAY1(7,2) = "0"
    ARRAY1(7,3) = "0"
    ARRAY1(7,4) = "0"
    ARRAY1(7,5) = "0"

    ARRAY1(8,1) = ""
    ARRAY1(8,2) = "0"
    ARRAY1(8,3) = "0"
    ARRAY1(8,4) = "0"
    ARRAY1(8,5) = "0"

    ARRAY1(9,1) = ""
    ARRAY1(9,2) = "0"
    ARRAY1(9,3) = "0"
    ARRAY1(9,4) = "0"
    ARRAY1(9,5) = "0"

    ARRAY1(10,1) = ""
    ARRAY1(10,2) = "0"
    ARRAY1(10,3) = "0"
    ARRAY1(10,4) = "0"
    ARRAY1(10,5) = "0"

    ARRAY1(11,1) = ""
    ARRAY1(11,2) = "0"
    ARRAY1(11,3) = "0"
    ARRAY1(11,4) = "0"
    ARRAY1(11,5) = "0"

    ARRAY1(12,1) = ""
    ARRAY1(12,2) = "0"
    ARRAY1(12,3) = "0"
    ARRAY1(12,4) = "0"
    ARRAY1(12,5) = "0"

    ARRAY1(13,1) = ""
    ARRAY1(13,2) = "0"
    ARRAY1(13,3) = "0"
    ARRAY1(13,4) = "0"

    ARRAY1(13,5) = "0"

    ARRAY1(14,1) = ""
    ARRAY1(14,2) = "0"
    ARRAY1(14,3) = "0"
    ARRAY1(14,4) = "0"
    ARRAY1(14,5) = "0"

    ARRAY1(15,1) = ""
    ARRAY1(15,2) = "0"
    ARRAY1(15,3) = "0"
    ARRAY1(15,4) = "0"
    ARRAY1(15,5) = "0"

    ARRAY1(16,1) = ""
    ARRAY1(16,2) = "0"
    ARRAY1(16,3) = "0"
    ARRAY1(16,4) = "0"
    ARRAY1(16,5) = "0"

    ARRAY1(17,1) = ""
    ARRAY1(17,2) = "0"
    ARRAY1(17,3) = "0"
    ARRAY1(17,4) = "0"
    ARRAY1(17,5) = "0"

    ARRAY1(18,1) = ""
    ARRAY1(18,2) = "0"
    ARRAY1(18,3) = "0"
    ARRAY1(18,4) = "0"
    ARRAY1(18,5) = "0"

    ARRAY1(19,1) = ""
    ARRAY1(19,2) = "0"
    ARRAY1(19,3) = "0"
    ARRAY1(19,4) = "0"
    ARRAY1(19,5) = "0"

    ARRAY1(20,1) = ""
    ARRAY1(20,2) = "0"
    ARRAY1(20,3) = "0"
    ARRAY1(20,4) = "0"
    ARRAY1(20,5) = "0"


*******************  PROCEDURE *** ********************************
*-------------------------------------------PREPARE  VARIABLE

*------------------------------------------START PROCESSING
    WS.DATE.ID = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
*    WS.TODAY = R.DATE<EB.DAT.LAST.PERIOD.END>
    WS.TODAY = R.DATE<EB.DAT.LAST.WORKING.DAY>

    GOSUB A.100.PROCESS
    GOSUB A.5100.PRT.SPACE.PAGE
    WS.FLAG = 1
    GOSUB A.5000.PRT.HEAD
    WS.SUB = 1
    GOSUB A.400.PRT.TOTAL

A.050:
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*------------------------------------------------

*------------------------------------------------
A.100.PROCESS:
    SEL.CMD = "SELECT ":FN.DEFPOS:" BY @ID"
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.DEFPOS.ID FROM SEL.LIST SETTING POS
    WHILE WS.DEFPOS.ID:POS


        CALL F.READ(FN.DEFPOS,WS.DEFPOS.ID,R.DEFPOS,F.DEFPOS,MSG.DEFPOS)
        WS.BR =  WS.DEFPOS.ID[1,9]
*        IF WS.BR NE "EG0010001"  THEN
*            GOTO A.100.REP
*        END
        IF  WS.FLAG.FRST EQ 0  THEN
            GOSUB A.5000.PRT.HEAD
            WS.FLAG.FRST = 1
            WS.BR.COMP = WS.BR
        END
        IF WS.BR NE WS.BR.COMP THEN
            GOSUB A.5000.PRT.HEAD
            WS.BR.COMP = WS.BR
        END
        WS.CY.AREA = WS.DEFPOS.ID[3]
        GOSUB A.220.PRT
A.100.REP:
    REPEAT
RETURN
*----------------------------------------------------------
A.220.PRT:
    XX = SPACE(132)
    PRINT XX<1,1>

    WS.AMT1 = R.DEFPOS<SCBD.DEF.LE>
    WS.AMT2 = R.DEFPOS<SCBD.DEF.FCY>
    WS.AMT3 = R.DEFPOS<SCBD.DEF.CONT>
    WS.AMT4 = R.DEFPOS<SCBD.LIN.CLASS>

    XX = SPACE(132)
    XX<1,1>[1,5]   = WS.CY.AREA
    XX<1,1>[10,15]   = FMT(WS.AMT1, "R2,")
    XX<1,1>[30,15]   = FMT(WS.AMT2, "R2,")
    XX<1,1>[50,15]   = FMT(WS.AMT3, "R2,")
    XX<1,1>[80,15]   = FMT(WS.AMT4, "R2,")

    PRINT XX<1,1>
    WS.SUB = 1
    GOSUB A.300.TOTAL.BR

RETURN
*--------------------------------------------------------
A.300.TOTAL.BR:
    IF WS.SUB GT 20 THEN
        RETURN
    END
    IF  ARRAY1(WS.SUB,1) EQ WS.CY.AREA THEN
        WS.T1   = ARRAY1(WS.SUB,2)
        WS.T2   = ARRAY1(WS.SUB,3)
        WS.T3   = ARRAY1(WS.SUB,4)
        WS.T4   = ARRAY1(WS.SUB,5)
        WS.T1   = WS.T1 + WS.AMT1
        WS.T2   = WS.T2 + WS.AMT2
        WS.T3   = WS.T3 + WS.AMT3
        WS.T4   = WS.T4 + WS.AMT4
        ARRAY1(WS.SUB,2) = WS.T1
        ARRAY1(WS.SUB,3) = WS.T2
        ARRAY1(WS.SUB,4) = WS.T3
        ARRAY1(WS.SUB,5) = WS.T4
        RETURN
    END

    IF  ARRAY1(WS.SUB,1) EQ "" THEN
        ARRAY1(WS.SUB,1) = WS.CY.AREA
        ARRAY1(WS.SUB,2) = WS.AMT1
        ARRAY1(WS.SUB,3) = WS.AMT2
        ARRAY1(WS.SUB,4) = WS.AMT3
        ARRAY1(WS.SUB,5) = WS.AMT4
        RETURN
    END
    WS.SUB += 1
    GOTO A.300.TOTAL.BR
RETURN
*--------------------------------------------------------
A.400.PRT.TOTAL:
    IF WS.SUB GT  20  THEN
        RETURN
    END
    IF  ARRAY1(WS.SUB,1) EQ "" THEN
        WS.SUB += 1
        GOTO A.400.PRT.TOTAL
    END
*    XX = SPACE(132)
*    PRINT XX<1,1>
    WS.CY.AREA = ARRAY1(WS.SUB,1)
    WS.AMT1 =    ARRAY1(WS.SUB,2)
    WS.AMT2 =    ARRAY1(WS.SUB,3)
    WS.AMT3 =    ARRAY1(WS.SUB,4)
    WS.AMT4 =    ARRAY1(WS.SUB,5)

    XX = SPACE(132)
    XX<1,1>[1,5]   = WS.CY.AREA
    XX<1,1>[10,15]   = FMT(WS.AMT1, "R2,")
    XX<1,1>[30,15]   = FMT(WS.AMT2, "R2,")
    XX<1,1>[50,15]   = FMT(WS.AMT3, "R2,")
    XX<1,1>[80,15]   = FMT(WS.AMT4, "R2,")
    PRINT XX<1,1>

    WS.SUB += 1
    GOTO A.400.PRT.TOTAL
RETURN

**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    IF WS.FLAG EQ  0   THEN
        CALL F.READ(FN.COMP,WS.BR,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
    END
    IF WS.FLAG EQ  1   THEN
        WS.BR.NAME = "������  �����"
    END


    WS.PAG = WS.PAG + 1
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":HEAD.01

    DATY = WS.TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD :="'L'":SPACE(60):T.DAY
    PR.HD :="'L'":HEAD.02:WS.PAG
    PR.HD :="'L'":HEAD.04
    PR.HD :="'L'":HEAD.03
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
**    PRINT
    HEADING PR.HD
RETURN
*------------------------------
A.5100.PRT.SPACE.PAGE:
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
RETURN

END
