* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.LD.COM(ENQ)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.ENTRY

    COMP = ID.COMPANY
    KEY.LIST=""
    SELECTED=""
    ER.MSG=""

    LOCATE "BOOKING.DATE" IN ENQ<2,1> SETTING CUS.POS THEN
        VDATE = ENQ<4,CUS.POS>
        B.DATE = FIELD(VDATE,' ',1)
        C.DATE = FIELD(VDATE,' ',2)
    END

    LOCATE "CUSTOMER.ID" IN ENQ<2,1> SETTING CUS.POS1 THEN
        CUST.ID = ENQ<4,CUS.POS1>
    END

    TEXT = "CUS : " : CUST.ID ; CALL REM
    TEXT = "C.DATE : " : C.DATE ; CALL REM
    TEXT = "B.DATE : " : B.DATE ; CALL REM

**T.SEL = "SELECT FBNK.CATEG.ENTRY WITH PL.CATEGORY IN (52050 52052 52054 52057 25058) AND  BOOKING.DATE GE ":B.DATE: " AND BOOKING.DATE LE ":C.DATE:" AND CUSTOMER.ID EQ ":CUST.ID : " AND OUR.REFERENCE LK LD..."

**    T.SEL = "SELECT FBNK.CATEG.ENTRY WITH CUSTOMER.ID EQ 1301484 AND PL.CATEGORY IN (52050 52052 52054 52057 25058 ) AND OUR.REFERENCE LIKE LD... AND BOOKING.DATE GE 20090101 AND BOOKING.DATE LE 20110101 "

  T.SEL = "SELECT FBNK.CATEG.ENTRY WITH CUSTOMER.ID EQ ":CUST.ID:" AND PL.CATEGORY IN (52050 52052 52054 52057 25058 ) AND OUR.REFERENCE LIKE LD... AND BOOKING.DATE GE ":B.DATE:" AND BOOKING.DATE LE ":C.DATE:" AND COMPANY.CODE EQ ":COMP

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    TEXT = "SELECTED :" : SELECTED ; CALL REM

*==============================================================

    IF SELECTED THEN
        FOR KK = 1 TO SELECTED
            ENQ<2,KK> = '@ID'
            ENQ<3,KK> = 'EQ'
            ENQ<4,KK> = KEY.LIST<KK>
        NEXT KK
    END ELSE
        ENQ<2,KK> = '@ID'
        ENQ<3,KK> = 'EQ'
        ENQ<4,KK> = 'DUMMY'
    END
    RETURN
END
