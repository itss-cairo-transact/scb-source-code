* @ValidationCode : MjotOTY0OTQ2NTg1OkNwMTI1MjoxNjQwODI0MDIzMzM1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:27:03
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
***** CREATED BY NESSMA 2010/10/13 **************
*-----------------------------------------------------------------------------
* <Rating>415</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.DRMNT.PRT.02.TOTAL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 46 ] HASHING "$INCLUDE I_AC.LOCAL.REFS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*-----------------------------------------*
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB SORTING.ARR
    GOSUB PRINT.BODY
    GOSUB PRINT.END
RETURN
*-----------------------------------------*
INITIAL:
*-------
    GOSUB OPEN.FILES

    PROGRAM.ID = "SBD.DRMNT.PRT.02.TOTAL"
    REPORT.ID  = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    SYS.DATE = TODAY
    P.DATE   = FMT(SYS.DATE,"####/##/##")
    WS.COMP  = ID.COMPANY

    ARR.KEY      = ""         ;* CAT:CUR
    ARR.CAT      = ""
    ARR.CUR      = ""
    ARR.CCY.BAL  = ""
    ARR.LCY.BAL  = ""

    INDEX.KEY    = 1
    INDEX.SEARCH = 1

    ACC.FLAG     = 0
    ARR.LINE     = SPACE(120)

RETURN
*-----------------------------------------*
OPEN.FILES:
*----------
    FN.CUS = "FBNK.CUSTOMER"    ; F.CUS = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC = "FBNK.ACCOUNT"     ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT" ; F.CUS.ACC = ''
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CAT = "F.CATEGORY"                ; F.CAT     = ""
    CALL OPF(FN.CAT,F.CAT)

    FN.CUR = "F.CURRENCY"                ; F.CUR     = ""
    CALL OPF(FN.CUR,F.CUR)

    FN.NUM.CUR = "F.NUMERIC.CURRENCY"    ; F.NUM.CUR = ""
    CALL OPF(FN.NUM.CUR,F.NUM.CUR)

    FN.PARM  = "FBNK.RE.BASE.CCY.PARAM"  ; F.PARM    = ""
    CALL OPF(FN.PARM,F.PARM)

RETURN
*-----------------------------------------*
PROCESS:
*-------
    SEL.N  = "SELECT ":FN.CUS:" WITH POSTING.RESTRICT EQ 18 AND COMPANY.BOOK EQ ":WS.COMP:" AND DRMNT.CODE GT 0 BY @ID"
*    SEL.N  = "SELECT ":FN.CUS:" WITH POSTING.RESTRICT EQ 18 AND COMPANY.BOOK EQ 'EG0010001' AND DRMNT.CODE GT 0 AND @ID LIKE 11003..."

    CALL EB.READLIST(SEL.N,KEY.LIST,'',SELECTED,ERR.CUS)


    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.CUS.ACC,KEY.LIST<II>, R.CUS.ACC, F.CUS.ACC, E111)
            ACCOUNT.NUMBER = R.CUS.ACC<EB.CAC.ACCOUNT.NUMBER>

            FLG = 0
            NI  = 1
            LOOP WHILE FLG = 0
                IF R.CUS.ACC<NI,EB.CAC.ACCOUNT.NUMBER> = '' THEN
                    FLG = 1
                END
                ACCT.NO = R.CUS.ACC<NI,EB.CAC.ACCOUNT.NUMBER>
                NI = NI + 1

                ACC.CAT = ACCT.NO[11,4]
                ACC.CUR = ACCT.NO[9,2]
                ACC.KEY = ACC.CAT : ACC.CUR

                CALL F.READ(FN.ACC,ACCT.NO,R.ACC,F.ACC,E.ACC)
                CCY.AMT  = R.ACC<AC.WORKING.BALANCE>

                CUR.PARM = ACC.CUR
                CALL F.READ(FN.NUM.CUR,CUR.PARM,R.P, F.NUM.CUR, E.NUM.CUR)
                CUR.CODE = R.P<EB.NCN.CURRENCY.CODE>
                CALL F.READ(FN.PARM,'NZD',R.PARM,F.PARM,E.C)
*Line [ 141 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE CUR.CODE IN R.PARM<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                CUR.RATE   = R.PARM<RE.BCP.RATE,POS>
                LCY.AMT    = CCY.AMT * CUR.RATE
*************** SEARCHING *******************************
                ACC.FLAG   = 0
                FOR S.INDX = 1 TO INDEX.KEY
                    IF ARR.KEY<1,S.INDX> EQ ACC.KEY THEN
                        ACC.FLAG = 1

                        ARR.CAT<1,S.INDX>     = ACC.CAT
                        ARR.CUR<1,S.INDX>     = ACC.CUR
                        ARR.CCY.BAL<1,S.INDX> = ARR.CCY.BAL<1,S.INDX> + CCY.AMT
                        ARR.LCY.BAL<1,S.INDX> = ARR.LCY.BAL<1,S.INDX> + LCY.AMT
                    END
                NEXT S.INDX
                IF ACC.FLAG EQ 0 THEN
*************** ADDING NEW ONE**************************

                    ARR.KEY<1,INDEX.KEY>     = ACC.KEY
                    ARR.CAT<1,INDEX.KEY>     = ACC.CAT
                    ARR.CUR<1,INDEX.KEY>     = ACC.CUR
                    ARR.CCY.BAL<1,INDEX.KEY> = CCY.AMT
                    ARR.LCY.BAL<1,INDEX.KEY> = LCY.AMT

                    INDEX.KEY = INDEX.KEY + 1
                END
            REPEAT
        NEXT II
    END
RETURN
*------------------------------------------*
SORTING.ARR:
*-----------
****** BUBBLE SORTING
    SWAPPED = 1     ;*DO WHILE
    LOOP WHILE SWAPPED EQ 1
        SWAPPED  = 0
        INDEX.KEY.NEW = INDEX.KEY  - 2
        FOR HH.I = 1 TO INDEX.KEY.NEW
            NUM.1   = ARR.KEY<1,HH.I>
            NUM.2   = ARR.KEY<1,HH.I+1>

            IF NUM.1 GT NUM.2 THEN
                ARR.KEY<1,HH.I>   = NUM.2
                ARR.KEY<1,HH.I+1> = NUM.1
*** SWAP THE OTHERS ***
                CAT.1 = ARR.CAT<1,HH.I>
                CAT.2 = ARR.CAT<1,HH.I+1>
                ARR.CAT<1,HH.I>   = CAT.2
                ARR.CAT<1,HH.I+1> = CAT.1

                CUR.1 = ARR.CUR<1,HH.I>
                CUR.2 = ARR.CUR<1,HH.I+1>
                ARR.CUR<1,HH.I>   = CUR.2
                ARR.CUR<1,HH.I+1> = CUR.1

                CCY.BAL.1 = ARR.CCY.BAL<1,HH.I>
                CCY.BAL.2 = ARR.CCY.BAL<1,HH.I+1>
                ARR.CCY.BAL<1,HH.I>   = CCY.BAL.2
                ARR.CCY.BAL<1,HH.I+1> = CCY.BAL.1

                LCY.BAL.1 = ARR.LCY.BAL<1,HH.I>
                LCY.BAL.2 = ARR.LCY.BAL<1,HH.I+1>
                ARR.LCY.BAL<1,HH.I>   = LCY.BAL.2
                ARR.LCY.BAL<1,HH.I+1> = LCY.BAL.1

                SWAPPED = 1
            END
        NEXT HH.I
    REPEAT
RETURN
*------------------------------------------*
PRINT.BODY:
*----------
    LCY.AMT.TOT = 0


    FOR INDX = 1 TO INDEX.KEY-1
        ARR.LINE<1,1>[2,18]  = ARR.CAT<1,INDX>
        CAT.ID = ARR.CAT<1,INDX>
        CALL F.READ(FN.CAT,CAT.ID,R.CAT, F.CAT, E.CAT)
        DESC.CAT = R.CAT<EB.CAT.DESCRIPTION><1,2>
        ARR.LINE<1,1>[18,25] = DESC.CAT

        CUR.ID   = ARR.CUR<1,INDX>
        CALL F.READ(FN.NUM.CUR,CUR.ID,R.NUM.CUR, F.NUM.CUR, E.NUM.CUR)
        CUR.CODE = R.NUM.CUR<EB.NCN.CURRENCY.CODE>
        ARR.LINE<1,1>[46,11] = CUR.CODE

        ARR.LINE<1,1>[56,23] = FMT(ARR.CCY.BAL<1,INDX>,"R2,")
        ARR.LINE<1,1>[80,23] = FMT(ARR.LCY.BAL<1,INDX>,"R2,")

        LCY.AMT.TOT = LCY.AMT.TOT + ARR.LCY.BAL<1,INDX>

        PRINT ARR.LINE<1,1>
        ARR.LINE = ""
    NEXT INDX

    GOSUB PRINT.TOT

RETURN
*------------------------------------------*
PRINT.TOT:
*---------
    LINE.STR             = SPACE(120)
    LINE.TOT             = SPACE(120)
    LINE.TOT2            = SPACE(120)

    LINE.STR<1,1>[80,20] = STR('_',20)

    LINE.TOT<1,1>[56,16]  = "��� ������� = "
    LINE.TOT<1,1>[80,10]  = SELECTED

    LINE.TOT2<1,1>[56,16] = "������ ������ = "
    LINE.TOT2<1,1>[80,10] = FMT(LCY.AMT.TOT,"R2,")

    PRINT LINE.STR
    PRINT " "
    PRINT LINE.TOT2
    PRINT " "
    PRINT LINE.TOT
RETURN
*------------------------------------------*
PRINT.HEAD:
*----------
*Line [ 267 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH

    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):PROGRAM.ID
    PR.HD :="'L'":SPACE(35):"������ ������ ������� ������� ������"
    PR.HD :="'L'":" ":SPACE(33):STR('_',38)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):"�������":SPACE(10):"�����":SPACE(20):"������"
    PR.HD :=SPACE(5):"��������":SPACE(15):"�������� ����"
    PR.HD :=SPACE(1):STR('_',120)
    PR.HD :="'L'":" "
    HEADING PR.HD
    LINE.NO = 0
RETURN
*-----------------------------------------*
PRINT.END:
*---------
    EMPTY.LINE = " "
    PRINT EMPTY.LINE
    PRINT EMPTY.LINE
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)
    TEXT = "��� �������"  ; CALL REM
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*-----------------------------------------*
END
