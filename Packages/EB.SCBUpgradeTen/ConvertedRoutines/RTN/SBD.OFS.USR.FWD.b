* @ValidationCode : MjoyMDM0ODU2NzYxOkNwMTI1MjoxNjQwODMwOTcyODg3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:22:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
**** CREATED BY MOHAMED SABRY 2013/05/22
*-----------------------------------------------------------------------------
* <Rating>-51</Rating>
*-----------------------------------------------------------------------------
* SUBROUTINE SBD.OFS.USR.FWD
PROGRAM SBD.OFS.USR.FWD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.OFS.SOURCE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.MODIFY.USER.AUTHORITY
*---------------------------------------------------
*   EXECUTE "ITTRNSLOGING"
    EXECUTE "OFSADMINLOGIN"
*---------------------------------------------------
    FN.MUA  = "F.SCB.MODIFY.USER.AUTHORITY"    ; F.MUA = ""
*
    FN.DATE = "F.DATES"                 ; F.DATE  = ""
*----------------------------------------------------
    CALL OPF (FN.MUA,F.MUA)
    CALL OPF (FN.DATE,F.DATE)

    FN.USER = "F.USER"
    FV.USER = ""
    CALL OPF(FN.USER,FV.USER)

    FN.USER.SIGN.ON.NAME = "F.USER.SIGN.ON.NAME"
    FV.USER.SIGN.ON.NAME = ""
    CALL OPF(FN.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME)

*-------------------------------------------------
    GOSUB A.10.DATE
    GOSUB SEL.RUN.MUA
RETURN
*----------------------------------------
A.10.DATE:

    WS.DATE.ID      = "EG0010001"
    CALL F.READ(FN.DATE,WS.DATE.ID,R.DATE,F.DATE,MSG.DATE)
    WS.LWD          = R.DATE<EB.DAT.LAST.WORKING.DAY>

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "SCB.MODIFY.USER.AUTHORITY"
    SCB.VERSION  = "OFS.FWD"

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

RETURN
*-----------------------------------------

SEL.RUN.MUA:
*------------
    SEL.MUA = "SELECT ":FN.MUA:" WITH START.DATE.PROFILE GT ":WS.LWD:" AND START.DATE.PROFILE LE ":TODAY:" AND STATUS EQ 'FWD' BY @ID "
    CALL EB.READLIST(SEL.MUA,KEY.LIST,"",SELECTED.MUA,ER.MSG.MUA)
    PRINT SELECTED.MUA
    IF SELECTED.MUA THEN
        FOR I.MUA = 1 TO SELECTED.MUA
            CALL F.READ(FN.MUA,KEY.LIST<I.MUA>,R.MUA,F.MUA,ER.MUA)

            WS.MUA.ID      = KEY.LIST<I.MUA>
            WS.MUA.CO      = R.MUA<MUA.CO.CODE>
            WS.BR.CODE     = R.MUA<MUA.CO.CODE>[2]
            WS.MUA.AUTH.ID = R.MUA<MUA.AUTHORISER>
            WS.MUA.AUTH.ID = FIELD(WS.MUA.AUTH.ID,'_',2)
*Line [ 88 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('USER':@FM:EB.USE.SIGN.ON.NAME,WS.MUA.AUTH.ID,WS.SION.NAME)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,WS.MUA.AUTH.ID,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.SION.NAME=R.ITSS.USER<EB.USE.SIGN.ON.NAME>
            PRINT  "WS.MUA.AUTH.ID ":WS.MUA.AUTH.ID:" WS.SION.NAME " :WS.SION.NAME

*            GOSUB GEN.OFS.USER

*           SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT":WS.BR.CODE:"//EG00100":WS.BR.CODE:"," : OFS.MESSAGE.DATA
            SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS," : WS.SION.NAME : "//EG00100" :WS.BR.CODE: "," : WS.MUA.ID :",STATUS:1:1=CURR"
            PRINT SCB.OFS.MESSAGE


* SCB R15 UPG 20160717 - S
*            CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE,SCB.OFS.MESSAGE)
            CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE,SCB.OFS.MESSAGE)
* SCB R15 UPG 20160717 - E

            PRINT SCB.OFS.MESSAGE
        NEXT I.MUA
    END

RETURN
*-------------------------------------------------
GEN.OFS.USER:

    OPERATOR        = WS.SION.NAME
    CALL F.READ(FN.USER.SIGN.ON.NAME,OPERATOR,R.USER.SIGN.ON.NAME,FV.USER.SIGN.ON.NAME,"")
    CALL F.READ(FN.USER,R.USER.SIGN.ON.NAME,R.USER,FV.USER,"")

*Line [ 115 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.CO.CONT = DCOUNT(R.USER<EB.USE.COMPANY.RESTR>,@VM)
    WS.CO.CONT ++

    R.USER<EB.USE.COMPANY.RESTR,WS.CO.CONT> = 'ALL'
    R.USER<EB.USE.APPLICATION,WS.CO.CONT>   = 'SCB.MODIFY.USER.AUTHORITY'
    R.USER<EB.USE.VERSION,WS.CO.CONT>       = ',OFS.FWD'
    R.USER<EB.USE.FUNCTION,WS.CO.CONT>      = 'I'

RETURN
*-------------------------------------------------
END
