* @ValidationCode : MjotODA2NTIyNzY0OkNwMTI1MjoxNjQxNzc3ODgzODI3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:24:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>596</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.CUST.POS.VISA

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.ISSUE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CARD.TYPE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.AC.LOCKED.EVENTS
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.VISA.CARD.TYPE
*Line [ 53 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CI.LOCAL.REFS
*Line [ 55 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 57 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS
*Line [ 59 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUST.POS.TODAY
*Line [ 61 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 63 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
*-------------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
*-------------------------------------------------------------------------
*Line [ 70 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 71 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
*==============================================================
INITIATE:
*****UPDATED BY NESSREEN 22/10/2009***************
***    REPORT.ID='SBD.CUST.POS.VISA'
    REPORT.ID='P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')
RETURN
*===============================================================
CALLDB:
    FN.CUS = 'FBNK.CUSTOMER'         ; F.CUS = '' ; R.CUS = ''
    CALL OPF(FN.CUS,F.CUS)
    FN.ACC = 'FBNK.ACCOUNT'          ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.CRD = 'FBNK.CARD.ISSUE'
    F.CRD = '' ; R.CRD = ''
    CALL OPF(FN.CRD,F.CRD)
    FN.CRT = 'FBNK.CARD.TYPE'
    F.CRT = '' ; R.CRT = ''
    CALL OPF(FN.CRT,F.CRT)

    FN.POS = 'F.SCB.CUST.POS.TODAY'  ; F.POS = '' ; R.POS = ''
    CALL OPF(FN.POS,F.POS)
    FN.CAT = 'F.CATEGORY'            ; F.CAT = '' ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)
    FN.CUR = 'FBNK.CURRENCY'         ; F.CUR = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    FN.LIM = 'FBNK.LIMIT'            ; F.LIM = '' ; R.LIM = ''
    CALL OPF(FN.LIM,F.LIM)
    FN.COL = 'FBNK.COLLATERAL'       ; F.COL = '' ; R.COL = ''
    CALL OPF(FN.COL,F.COL)
    FN.EVT = 'FBNK.AC.LOCKED.EVENTS' ; F.EVT = '' ; R.EVT = ''
    CALL OPF(FN.EVT,F.EVT)

    KEY.LIST ="" ; SELECTED ="" ; ER.MSG =""
    KEY.LIST1="" ; SELECTED1="" ; ER.MSG1=""
    XNUMBER = 0
    TDD = TODAY

*=================================================================

    T.SEL = "SELECT ":FN.POS:" WITH @ID UNLIKE 99... AND CO.CODE EQ ":COMP:" AND AC.CATEG BETWEEN '1204' AND '1209' BY OLD.CUST.ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED  THEN

        XX = SPACE(132) ; XX1 = SPACE(132) ; XX2 = SPACE(132) ; XX3 = SPACE(132) ; XX4 = SPACE(132) ; XX5 = SPACE(132) ; XX6 = SPACE(132) ; XX7 = SPACE(132)

        K=0 ; K1=0 ; K2=0 ; K3=0 ; K4=0 ; K5=0 ; K6=0 ; K7=0

        POS.ID = '' ; CUS.ID = '' ; CUS.ZZ = '' ; CUS.NAME = ''

        AC.COD   = '' ; AC.ID         = ''
        AC.BAL   = '' ; AC.LIM.REF    = ''
        AC.CUR   = '' ; AC.CUR.NAME   = '' ; AC.LIM.REFS = ''
        AC.CAT   = '' ; AC.CAT.NAME   = ''
        AC.LIMIT = '' ; AC.LOCK       = ''
        AC.LIST  = '' ; SELECTED.AC   = '' ; ER.AC       = ''

*******************************************************************
        FOR I = 1 TO SELECTED
            K++
            CALL F.READ(FN.POS,KEY.LIST<I>,R.POS,F.POS,E1)
            POS.ID = KEY.LIST<I>
            CUS.ID = FIELD (POS.ID,'-',1)
            CUS.ZZ = CUS.ID:".0000"
*Line [ 141 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LCL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LCL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
            CUS.NAME = LCL.REF<1,CULR.ARABIC.NAME>
            OLD.CUST = LCL.REF<1,CULR.OLD.CUST.ID>
            XX<1,K>[1,10]   = CUS.ID
            XX<1,K>[20,35]  = CUS.NAME
            XX<1,K>[70,15]  = '( ����� ������  ':OLD.CUST:' )'
*            XX<1,K>[90,10]  = OLD.CUST
            XNUMBER++
            PRINT STR("-",120)
            PRINT XX<1,K>
*---------------------------*AC*--------------------
            ACC.NO1 = ''
            AC.COD = R.POS<CUST.AC>
*Line [ 153 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DAC = DCOUNT(AC.COD,@VM)
            IF DAC GT 0 THEN
                FOR VVV = 1 TO DAC
*************UPDATED BY NESSREEN AHMED 20/7/2011*********************************************************************************************************************
**** V.SEL  = "SELECT ":FN.CRD:" WITH CARD.STATUS EQ 90 AND CANCELLATION.DATE EQ '' AND CAN.REASON EQ '' AND ACCOUNT EQ ":R.POS<CUST.AC,VVV>
**** V.SEL := " AND CARD.CODE IN (101 102 104 105 201 202 204 205 501 502 504 505 601 602 604 605)"
                    V.SEL  = "SELECT ":FN.CRD:" WITH CARD.STATUS EQ 90 AND CANCELLATION.DATE EQ '' AND CAN.REASON EQ '' AND ACCOUNT EQ ":R.POS<CUST.AC,VVV>
                    V.SEL := " AND CARD.CODE IN (101 102 104 105 201 202 204 205 501 502 504 505 601 602 604 605 610 611 612 613)"
*************END OF UPDATE 20/7/2011*********************************************************************************************************************************
                    CALL EB.READLIST(V.SEL,KEY.LIST1,"",SELECTED1,ER.MSG1)
                    IF SELECTED1 THEN
                        FOR SSS = 1 TO SELECTED1
                            K3++
                            CALL F.READ(FN.CRD,KEY.LIST1<SSS>,R.CRD,F.CRD,EE1)
                            V.NO = FIELD(KEY.LIST1<SSS>,'.',2)
                            V.LREF = R.CRD<CARD.IS.LOCAL.REF>
                            V.CODE = V.LREF<1,LRCI.CARD.CODE>
*Line [ 178 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('SCB.VISA.CARD.TYPE':@FM:VICA.CODE.DESC,V.CODE,V.DESC)
F.ITSS.SCB.VISA.CARD.TYPE = 'F.SCB.VISA.CARD.TYPE'
FN.F.ITSS.SCB.VISA.CARD.TYPE = ''
CALL OPF(F.ITSS.SCB.VISA.CARD.TYPE,FN.F.ITSS.SCB.VISA.CARD.TYPE)
CALL F.READ(F.ITSS.SCB.VISA.CARD.TYPE,V.CODE,R.ITSS.SCB.VISA.CARD.TYPE,FN.F.ITSS.SCB.VISA.CARD.TYPE,ERROR.SCB.VISA.CARD.TYPE)
V.DESC=R.ITSS.SCB.VISA.CARD.TYPE<VICA.CODE.DESC>
                            XX3<1,K3>[1,15]   = V.NO
                            XX3<1,K3>[20,35]  = V.DESC
                            PRINT XX3<1,K3>
                        NEXT SSS
                    END
                NEXT VVV
                PRINT STR('-',60)
                FOR ACC = 1 TO DAC
                    K1++
                    AC.ID  = R.POS<CUST.AC,ACC>
                    AC.SER = AC.ID[15,2]
                    AC.CAT = R.POS<CUST.AC.CATEG,ACC>
                    AC.CUR = R.POS<CUST.AC.CURR,ACC>
                    IF AC.CAT NE '1002' THEN
                        CALL F.READ(FN.ACC,AC.ID,R.ACC,F.ACC,ER.ACC)
                        AC.LIM.REF  = R.ACC<AC.LIMIT.REF>
                        AC.LIM.REFS = CUS.ZZ:AC.LIM.REF
*Line [ 202 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,AC.CUR,AC.CUR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,AC.CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
AC.CUR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*Line [ 209 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('CATEGORY':@FM:EB.CAT.DESCRIPTION,AC.CAT,AC.CAT.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,AC.CAT,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
AC.CAT.NAME=R.ITSS.CATEGORY<EB.CAT.DESCRIPTION>
                        AC.BAL += R.ACC<AC.ONLINE.ACTUAL.BAL>
*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('LIMIT':@FM:LI.MAXIMUM.TOTAL,AC.LIM.REFS,AC.LIMIT)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,AC.LIM.REFS,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AC.LIMIT=R.ITSS.LIMIT<LI.MAXIMUM.TOTAL>
                        AC.LOCK += R.ACC<AC.LOCKED.AMOUNT,1>
                        GOSUB PRINTAC
                    END
                NEXT ACC
            END
********************************************************
********************************************************
*===============================================================
*            PRINT STR('-',120)
        NEXT I
        PRINT STR('=',120)
        PRINT '��� ������� : ':XNUMBER
*===============================================================
    END ELSE
        XNUMBER = 0
    END
RETURN
*===============================================================
**************************
PRINTAC:
    XX1<1,K1>[1,5]    = AC.CAT
    XX1<1,K1>[7,35]   = AC.CAT.NAME
    XX1<1,K1>[41,2]   = AC.SER
    XX1<1,K1>[47,20]  = AC.CUR.NAME
    XX1<1,K1>[72,15]  = AC.BAL
    XX1<1,K1>[92,15]  = AC.LIMIT
    XX1<1,K1>[108,15] = AC.LOCK
    PRINT XX1<1,K1>
    AC.CAT      = ''
    AC.CAT.NAME = ''
    AC.CUR.NAME = ''
    AC.BAL   = ''
    AC.LIMIT = ''
    AC.LOCK  = ''
RETURN
**************************
*===============================================================
PRINT.HEAD:
*---------
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
*Line [ 264 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN   = BRANCH
    DATY    = TODAY
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH

    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
*    PR.HD :="'L'":SPACE(50):"����� ������� �������"
    PR.HD :="'L'":SPACE(30):"������ ������� ������ �������� �� ����� :":T.DAY
    PR.HD :="'L'":SPACE(30):STR('_',52)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"�������" :SPACE(10):"��� �������" :SPACE(10):"�����":SPACE(5):"������" :SPACE(15):"������" :SPACE(15):"�� �����" :SPACE(10):"������"
    PR.HD :="'L'":STR('_',120)
    HEADING PR.HD
    PRINT
RETURN
*==============================================================
END
