* @ValidationCode : MjoxNzczNjUzODEyOkNwMTI1MjoxNjQwODMyNjQzNTU1OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:50:43
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.TRANS(ENQ)
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] HASHING "$INCLUDE I_F.SCB.TRANS.TODAY" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY
    FN.CUS='F.SCB.TRANS.TODAY'
    F.CUS=''
    CALL OPF(FN.CUS,F.CUS)

*TODAY = 20110429
    WORK.DATE   = TODAY
    CALL CDT('', WORK.DATE, "-1W")

    YY = '...':WORK.DATE:'...'
    XX = WORK.DATE[3,7]
    ZZ = XX:'0700'

    T.SEL="SELECT F.SCB.TRANS.TODAY WITH COMPANY.CO EQ ":COMP:" AND @ID LIKE " :YY: " AND DATE.TIME GT " :ZZ

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    ENQ.LP   = ''
    OPER.VAL = ''
    IF SELECTED THEN
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END ELSE
        ENQ.ERROR = "NO RECORDS FOUND"
    END

**********************
RETURN
END
