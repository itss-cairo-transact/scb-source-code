* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-21</Rating>
*-----------------------------------------------------------------------------
***    SUBROUTINE SBD.MLAD.PROG.ALL
    PROGRAM SBD.MLAD.PROG.ALL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    EXECUTE "OFSADMINLOGIN"

** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'

*----------------------------------------------------------------------------
**                                              ����� ����� ������� ������ �������
**                                              ����� ������ ������� ���������
**                                              ����� ����� ������� ������
**K CALL    "SBD.MLAD.PROG.010"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.010")

**    TEXT = "SBD MLAD PROG 010" ; CALL REM


*--------------------------------------------------------------------------
**                                            ��� ����� ���   ����� ��� �� TEMP
**     CALL "SBD.MLAD.PROG.091"
**     TEXT = "SBD MLAD PROG 091" ; CALL REM

**-------------------------------------------------------------------------
**                                                       ����� ��� ������� ��������
**                                       NZD             ������� ������
**    CALL "SBD.MLAD.PROG.090"
**                                   MID RATE            ������� ������
**K  CALL "SBD.MLAD.PROG.090.V2"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.090.V2")

**    TEXT = "SBD MLAD PROG 090.V2" ; CALL REM

**------------------------------------------------------------------------
*                                                               ������� �����  020
*                                                               ����� ���� �������
*                                                             ����� ������ ��������
*                                        ����� ��� ����� �������   (����� + ������ )
**K    CALL "SBD.MLAD.PROG.030"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.030")

**    TEXT = "SBD MLAD PROG 030" ; CALL REM

**-----------------------------------------------------------------------
**                                               ������� ��� ����� ��������
**K    CALL "SBD.MLAD.PROG.300"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.300")

**    TEXT = "SBD MLAD PROG 300" ; CALL REM

**----------------------------------------------------------------------
**                                                 ������� ����� ������
**K    CALL "SBD.MLAD.PROG.305"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.305")


**    TEXT = "SBD MLAD PROG 305" ; CALL REM

**----------------------------------------------------------------------
**                           ����� �������� ������� �������� ���� ����� ������
**    CALL "SBD.MLAD.PROG.310"
**K    CALL "SBD.MLAD.PROG.310.V2"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.310.V2")

**    TEXT = "SBD MLAD PROG 310" ; CALL REM
**    TEXT = "SBD MLAD PROG 310.V2" ; CALL REM

**----------------------------------------------------------------------
*                 PARAM File                         ������ ��� ������� ���� ���
*                                                               ������ ������

**K    CALL "SBD.MLAD.PROG.021"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.021")

**    TEXT = "SBD MLAD PROG 021" ; CALL REM

*-----------------------------------------------------------------------
**                                              ������ ��� ����� ���� ����� ��������
**K    CALL "SBD.MLAD.PROG.099"
    CALL FSCB.RUN.JOB(WS.S,WS.D,WS.A,"SBD.MLAD.PROG.099")

**    TEXT = "End Program 99" ; CALL REM

*-----------------------------------------------------------------------
**                           ����� ������� ������� ����� ��������� ������ �����������
**    CALL "SBD.MLAD.PROG.100"
**    CALL "SBD.MLAD.PROG.101"
**    CALL "SBD.MLAD.PROG.102"
*-----------------------------------------------------------------------
**    EX.CMD = 'LIST F.SCB.MLAD.FILE.020 NEXT.DAY NEXT.WEEK NEXT.MONTH NEXT.3MONTH NEXT.YEAR BY @ID WITH @ID LIKE USD...'
**    EXECUTE EX.CMD

*-----------------------------------------------------------------------
    RETURN
END
