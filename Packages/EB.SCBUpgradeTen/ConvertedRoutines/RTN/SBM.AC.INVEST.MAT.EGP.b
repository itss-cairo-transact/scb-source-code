* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>272</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBM.AC.INVEST.MAT.EGP
*    PROGRAM SBM.AC.INVEST.MAT.EGP

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

*---------------------------------------------------------------------
    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT.CREATED "   ; CALL REM
    RETURN

*-----------------------------INITIALIZATIONS------------------------
INITIATE:

    REPORT.ID='SBM.AC.INVEST.MAT.EGP'
    CALL PRINTER.ON(REPORT.ID,'')


    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = ''
    CALL OPF( FN.CUS,F.CUS)

    FN.STE = 'FBNK.STMT.ENTRY' ; F.STE = '' ; R.STE = '' ; ER.STE = ''
    CALL OPF(FN.STE,F.STE)

    EOF       = ''
    ETEXT     = ''
    ETEXT1    = ''
    I         = 1
********** TO GET FROM AND TO DATE ******************************

    T.DATE  = TODAY
    T.MONTH = T.DATE[5,2]

    IF T.MONTH EQ '01' OR T.MONTH EQ '04' OR T.MONTH EQ '07' OR T.MONTH EQ '10' THEN
        CALL ADD.MONTHS(T.DATE,'-1')
    END
    IF T.MONTH EQ '02' OR T.MONTH EQ '05' OR T.MONTH EQ '08' OR T.MONTH EQ '11' THEN
        CALL ADD.MONTHS(T.DATE,'-2')
    END
    IF T.MONTH EQ '03' OR T.MONTH EQ '06' OR T.MONTH EQ '09' OR T.MONTH EQ '12' THEN
        CALL ADD.MONTHS(T.DATE,'-3')
    END
    CORRECT.L.DATE = T.DATE
    CALL LAST.DAY(CORRECT.L.DATE)

    FIRST.DATE = CORRECT.L.DATE[1,6]:'01'
    CORRECT.F.DATE = FIRST.DATE
    CALL ADD.MONTHS(CORRECT.F.DATE,'-2')

****************** I MAKE CHANGE HERE *********************
    TODAY.YEAR = TODAY[1,4]
    CORRECT.F.DATE = TODAY.YEAR :'0101'
***********************************************************

    RETURN

*------------------------RED FORM TEXT FILE----------------------
PROCESS:

    T.SEL  = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ '' AND CATEGORY IN (12041 12043 12048) AND POSTING.RESTRICT LT 90 AND COST.BONDS NE '' BY CATEGORY"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    TOT.1 = 0; TOT.2 = 0; TOT.3 = 0; TOT.4 = 0; TOT.5 = 0; TOT.6 = 0
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( FN.AC,KEY.LIST<I>, R.AC, F.AC, ETEXT)

            ACCT.ID     = KEY.LIST<I>
            ACCT.TITLE  = R.AC<AC.ACCOUNT.TITLE.1>
            NO.OF.BONDS = R.AC<AC.LOCAL.REF,ACLR.NO.OF.BONDS>
            MATUR.DATE  = R.AC<AC.LOCAL.REF,ACLR.MATUR.DATE>
            COST.BONDS  = R.AC<AC.LOCAL.REF,ACLR.COST.BONDS>
*--------------------UPDATE BY MOHAMED MOSTAFA----------------
*            PREV.BAL    = R.AC<AC.LOCAL.REF,ACLR.PREV.BAL>
            CALL GET.ENQ.BALANCE(ACCT.ID,CORRECT.L.DATE,PREV.BAL)
*--------------------------------------------------------------
            PRICE.EVAL  = R.AC<AC.LOCAL.REF,ACLR.PRICE.EVAL>
            EVAL.BAL    = R.AC<AC.LOCAL.REF,ACLR.EVAL.BAL>
*            REVALUATION = R.AC<AC.LOCAL.REF,ACLR.REVALUATION>
            REVALUATION = EVAL.BAL + PREV.BAL


***************** TO GET PREV OPEN BAL ********************
            CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,CORRECT.F.DATE,CORRECT.L.DATE,ID.LIST,OPENING.BAL,ER)
            ADD.AMT = 0; SUB.AMT = 0

            LOOP
                REMOVE STE.ID FROM ID.LIST SETTING POS
            WHILE STE.ID:POS

                CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
                STE.TRANS = R.STE<AC.STE.TRANSACTION.CODE>

                IF STE.TRANS NE '' AND STE.TRANS NE '849' THEN
                    STE.AMT   = R.STE<AC.STE.AMOUNT.LCY>
                    IF STE.AMT LT 0 THEN
                        ADD.AMT = ADD.AMT + STE.AMT
                    END
                    ELSE
                        SUB.AMT = SUB.AMT + STE.AMT
                    END
                END
            REPEAT
************************************
            GOSUB WRITE

            TOT.1 = TOT.1 + COST.BONDS
            TOT.2 = TOT.2 + PREV.BAL
            TOT.3 = TOT.3 + EVAL.BAL
            TOT.4 = TOT.4 + REVALUATION
            TOT.5 = TOT.5 + ADD.AMT
            TOT.6 = TOT.6 + SUB.AMT

        NEXT I

        XX1 = SPACE(132)
        XX3 = SPACE(132)
        XX5 = SPACE(132)

        XX3<1,1>[1,132]  = ""
        XX1<1,1>[1,15]   = "�������"
        XX1<1,1>[41,15]  = DROUND(TOT.1,2)
        XX1<1,1>[55,15]  = DROUND(TOT.2,2)
        XX1<1,1>[90,15]  = DROUND(TOT.3,2)
        XX1<1,1>[110,15] = DROUND(TOT.4,2)
        XX1<1,1>[125,15] = DROUND(TOT.5,2)
        XX5<1,1>[125,15] = DROUND(TOT.6,2)

        PRINT XX3<1,1>
        PRINT XX3<1,1>
        PRINT XX1<1,1>
        PRINT XX5<1,1>
    END
    ELSE
        XX15 = SPACE(132)
        XX15<1,1>[60,132]  = "�� ���� ������"
        PRINT XX15<1,1>
    END
    RETURN
*******************************************************
WRITE:

    XX2 = SPACE(132)
    XX4 = SPACE(132)
    XX9 = SPACE(132)

    XX9<1,1>[1,135]  = STR('-',135)

    XX2<1,1>[1,15]   = ACCT.ID
    XX4<1,1>[1,30]   = ACCT.TITLE
    XX2<1,1>[26,15]  = NO.OF.BONDS
    XX4<1,1>[26,15]  = MATUR.DATE[7,2]:'/':MATUR.DATE[5,2]:'/':MATUR.DATE[1,4]
    XX2<1,1>[41,15]  = DROUND(COST.BONDS,2)
    XX2<1,1>[55,15]  = DROUND(PREV.BAL,2)
    XX2<1,1>[76,15]  = DROUND(PRICE.EVAL,2)
    XX2<1,1>[90,15]  = DROUND(EVAL.BAL,2)
    XX2<1,1>[110,15] = DROUND(REVALUATION,2)
    XX2<1,1>[125,15] = DROUND(ADD.AMT,2)
    XX4<1,1>[125,15] = DROUND(SUB.AMT,2)

    PRINT XX2<1,1>
    PRINT XX4<1,1>
    PRINT XX9<1,1>

    RETURN
*---------------------------PRINT HEAD-----------------
PRINT.HEAD:

*Line [ 217 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,R.USER<EB.USE.DEPARTMENT.CODE>,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRANCH=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
    YYBRN  = FIELD(BRANCH,'.',2)
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    L.DATE  = CORRECT.L.DATE
    L.DATE  = L.DATE[7,2]:'/':L.DATE[5,2]:"/":L.DATE[1,4]

    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):"��������� ����� ��� ����� ���������"
    PR.HD :="'L'":SPACE(62):"��":SPACE(2):L.DATE
    PR.HD :="'L'":SPACE(50):STR('_',33)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":"��� ������":SPACE(15):"�����":SPACE(10):"�������":SPACE(5):"������ ������":SPACE(5):"�. �������":SPACE(4):"�.���� �.�������":SPACE(4):"���� �������":SPACE(8):"��������"
    PR.HD :="'L'":"��� ������":SPACE(15):"�.���������":SPACE(88):"�����������"
    PR.HD :="'L'":STR('_',135)
    PRINT
    HEADING PR.HD
    RETURN
END
