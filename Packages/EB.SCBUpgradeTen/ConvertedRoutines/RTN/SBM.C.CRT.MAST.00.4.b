* @ValidationCode : MjotNDA3ODA0OTk3OkNwMTI1MjoxNjQwODQxNTM5MzkxOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 21:18:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
****MOHSEN
PROGRAM SBM.C.CRT.MAST.00.4

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE

*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL

*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.MAST.AC.LD
*    $INSERT GLOBUS.BP  I_F.RE.BASE.CCY.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CBE.CY.RATE
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*-----------------------------------------------------------------------
    FN.COL = "FBNK.COLLATERAL"
    F.COL  = ""

    FN.ACLD = "F.CBE.MAST.AC.LD"
    F.ACLD = ""

*    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
*    F.CCY  = ""
    FN.CCY = "F.SCB.CBE.CY.RATE"
    F.CCY  = ""


    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD  = ""

*--------------------------------------------------------
    CALL OPF (FN.COL,F.COL)
    CALL OPF (FN.ACLD,F.ACLD)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.LD,F.LD)
*---------------------------------------------------------
    WS.AMT = 0
    WS.COL.AMT.LCY = 0
    WS.COL.AMT.FCY = 0
*----------------------------------------------------------------------
    GOSUB A.100.READ.ACLD
RETURN
*----------------------------------------------------------------------
A.100.READ.ACLD:
    SEL.CMDAC = "SELECT ":FN.ACLD:" WITH CBEM.CATEG GE 21001 AND CBEM.CATEG LE 21010"
    CALL EB.READLIST(SEL.CMDAC,SEL.LISTAC,"",NO.OF.RECAC,RET.CODEAC)
    LOOP
        REMOVE WS.ACLD.ID FROM SEL.LISTAC SETTING POSACLD
    WHILE WS.ACLD.ID:POSACLD
        CALL F.READ(FN.ACLD,WS.ACLD.ID,R.ACLD,F.ACLD,MSG.ACLD)
        GOSUB A.200.LD
        R.ACLD<C.RESERVED4>  =    WS.COL.AMT.LCY
        R.ACLD<C.RESERVED3>  =    WS.COL.AMT.FCY
        GOSUB  A.600.UP.FINPOS
A.100.REPEAT:
    REPEAT
RETURN
*-------------------------------------------------------------------
A.200.LD:
    CALL F.READ(FN.LD,WS.ACLD.ID,R.LD,F.LD,MSG.LD)
    WS.COL.ID = R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
    IF  WS.COL.ID  EQ ""  THEN
        RETURN
    END
    IF  WS.COL.ID  EQ 0  THEN
        RETURN
    END
*    CRT WS.COL.ID:"  ":WS.ACLD.ID
    WS.COL.AMT.LCY = 0
    WS.COL.AMT.FCY = 0
    GOSUB A.300.READ.COL
RETURN
*-------------------------------------------------------------------
A.300.READ.COL:
    MSG.COL = ""
    CALL F.READ(FN.COL,WS.COL.ID,R.COL,F.COL,MSG.COL)
*        WS.ACLD.ID = R.COL<COLL.APPLICATION.ID>
    IF MSG.COL NE ""  THEN
        RETURN
    END

    WS.COL.AMT.LCY = R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
    IF  WS.COL.AMT.LCY EQ 0  THEN
        RETURN
    END
    IF  WS.COL.AMT.LCY EQ ""  THEN
        RETURN
    END
    WS.CY      = R.COL<COLL.CURRENCY>

    IF  WS.CY  EQ "EGP"  THEN
        WS.COL.AMT.FCY = WS.COL.AMT.LCY
    END

    IF  WS.CY  NE "EGP"  THEN
        WS.COL.AMT.FCY = WS.COL.AMT.LCY
        WS.COL.AMT.LCY = 0
        GOSUB  A.550.GET.CY
    END
A.300.REPET:
RETURN
*-----------------------------------------------------------
A.550.GET.CY:
*    WS.KEYCY = "NZD"
    WS.KEYCY = WS.CY
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)

    IF MSG.CY EQ "" THEN
        GOSUB A.551.CALC
    END

RETURN
*-------------------------------------------------------
A.551.CALC:
*    A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*    LOCATE WS.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
*    WS.RATE.ARY = R.CCY<RE.BCP.RATE>
*    WS.RATE = WS.RATE.ARY<1,CYPOS>
    WS.RATE =  R.CCY<CCR.CY.RATE>
*    WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
*    WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>
    WS.MLT.DIVD =  R.CCY<CCR.CY.CALC>
    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.COL.AMT.LCY = WS.COL.AMT.FCY * WS.RATE
    END
    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.COL.AMT.LCY = WS.COL.AMT.FCY / WS.RATE
    END
RETURN

*------------------------------------------------------------
A.600.UP.FINPOS:
    CALL F.WRITE(FN.ACLD,WS.ACLD.ID,R.ACLD)
    CALL  JOURNAL.UPDATE(WS.ACLD.ID)

RETURN
*------------------------------------------------------------

END
