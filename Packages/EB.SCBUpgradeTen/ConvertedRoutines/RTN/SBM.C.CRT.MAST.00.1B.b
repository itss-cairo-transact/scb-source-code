* @ValidationCode : Mjo1NzExNzgyOTE6Q3AxMjUyOjE2NDA4NDE0ODgzMzc6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 21:18:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>79</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBM.C.CRT.MAST.00.1B
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.LD.TEMP
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*---------------------------------------------------
    FN.REP = "FBNK.RE.STAT.LINE.CONT"
    F.REP  = ""

    FN.TEMP = "F.SCB.LD.TEMP"
    F.TEMP  = ""

    FN.RCL = "FBNK.RE.CONSOL.LOAN"
    F.RCL  = ""

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS"
    F.LD  = ""

    FN.CCY = "FBNK.RE.BASE.CCY.PARAM"
    F.CCY  = ""

*
*----------------------------------------------------
    CALL OPF (FN.REP,F.REP)
    CALL OPF (FN.TEMP,F.TEMP)
    CALL OPF (FN.RCL,F.RCL)
    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.CCY,F.CCY)

*----------------------------------------------------
    FN.CLEAR = "F.SCB.LD.TEMP"
**OPEN FN.CLEAR TO FILEVAR ELSE ABORT 201, FN.CLEAR
    FILEVAR = ''
    CALL OPF(FN.CLEAR,FILEVAR)

    CLEARFILE FILEVAR

    R.TEMP = ""
    WS.AREA.LIKE = "GENLED.0530"
    GOSUB A.100.PROC

    WS.AREA.LIKE = "GENLED.0730"
    GOSUB A.100.PROC

    WS.AREA.LIKE = "GENLED.0570"
    GOSUB A.100.PROC

RETURN
*------------------------------------------------
A.100.PROC:
    SEL.CMD = "SELECT ":FN.REP:" WITH @ID LIKE ":WS.AREA.LIKE:"..."
    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.REP.ID FROM SEL.LIST SETTING POS
    WHILE WS.REP.ID:POS
        CALL F.READ(FN.REP,WS.REP.ID,R.REP,F.REP,MSG.REP)

        WS.REPORT.NAME = WS.REP.ID
        WS.COUNT = DCOUNT(R.REP<RE.SLC.ASST.CONSOL.KEY>,@VM)
        GOSUB A.150.CON
A.100.REP:
    REPEAT
A.100.EXIT:
RETURN
*------------------------------------------------
A.150.CON:
    FOR APP = 1  TO WS.COUNT
        WS.CON.KEY = R.REP<RE.SLC.ASST.CONSOL.KEY,APP>
        WS.AST.TYP = R.REP<RE.SLC.ASSET.TYPE,APP>
        GOSUB A.500.WRITE
    NEXT APP
RETURN
*------------------------------------------------
A.500.WRITE:
*    WS.COUNT.AS.TYP = DCOUNT(R.REP<RE.SLC.ASSET.TYPE,APP>,@SM)
*    GOSUB A.530.AST.TYP
    SEL.CMDRCL = "SELECT ":FN.RCL:" WITH @ID LIKE ":WS.CON.KEY:"..."
    CALL EB.READLIST(SEL.CMDRCL,SEL.LISTRCL,"",NO.OF.RECRCL,RET.CODERCL)
    LOOP
        REMOVE WS.RCL.ID FROM SEL.LISTRCL SETTING POSRCL
    WHILE WS.RCL.ID:POSRCL
        MSG.RCL = ""
        CALL F.READ(FN.RCL,WS.RCL.ID,R.RCL,F.RCL,MSG.RCL)
        WS.DCOUNT.COUNTER = 1
        GOSUB A.510.LD
    REPEAT
RETURN
*------------------------------------------------------
A.510.LD:
    WS.ARAY.RCL = R.RCL<WS.DCOUNT.COUNTER>
    IF  WS.ARAY.RCL EQ "" THEN
        GOTO A.510.EXIT
    END
    WS.DCOUNT.COUNTER = WS.DCOUNT.COUNTER + 1
    GOSUB A.550.PREP.CRT

    GOTO A.510.LD
A.510.EXIT:
RETURN
*------------------------------------------------------
A.550.PREP.CRT:
    CALL F.READ(FN.LD,WS.ARAY.RCL,R.LD,F.LD,MSG.LD)
    IF R.LD<LD.CUSTOMER.ID> EQ "" THEN
        RETURN
    END
    WS.CUSTOMER =  R.LD<LD.CUSTOMER.ID>
    WS.CO.CODE  =  R.LD<LD.CO.CODE>
    WS.BR = WS.CO.CODE[2]

    WS.AMT = R.LD<LD.AMOUNT>
    WS.AMT.REIM = R.LD<LD.REIMBURSE.AMOUNT>

    WS.CATEG = R.LD<LD.CATEGORY>
    WS.CY = R.LD<LD.CURRENCY>
    WS.V.DATE = R.LD<LD.VALUE.DATE>
    WS.FIN.DATE = R.LD<LD.FIN.MAT.DATE>

    IF WS.CY EQ "EGP" THEN
        WS.LCY = WS.AMT
        WS.FCY = WS.AMT
        WS.LCY.REIM = WS.AMT.REIM
        WS.FCY.REIM = WS.AMT.REIM
*        WS.LCYM = WS.MRG.AMT
*        WS.FCYM = WS.MRG.AMT
    END

    IF WS.CY NE "EGP" THEN
        GOSUB A.570.GET.CY
        WS.LCY = WS.AMT.LOCAL
        WS.FCY = WS.AMT
        WS.LCY.REIM = WS.AMT.REIM.LCL
        WS.FCY.REIM = WS.AMT.REIM

*        WS.LCYM = WS.MRG.AMT.LOCAL
*        WS.FCYM = WS.MRG.AMT
    END
    GOSUB A.600.WRITE
RETURN
*--------------------------------------------------
A.570.GET.CY:
    WS.KEYCY = "NZD"
    CALL F.READ(FN.CCY,WS.KEYCY,R.CCY,F.CCY,MSG.CY)
    IF MSG.CY EQ "" THEN
        GOSUB A.580.CALC
    END

RETURN
A.580.CALC:
    A.ARY =  R.CCY<RE.BCP.ORIGINAL.CCY>
*Line [ 176 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.CY IN A.ARY<1,1> SETTING CYPOS ELSE NULL
    WS.RATE.ARY = R.CCY<RE.BCP.RATE>
    WS.RATE = WS.RATE.ARY<1,CYPOS>
    WS.MLT.DIVD.ARY = R.CCY<RE.BCP.RATE.TYPE>
    WS.MLT.DIVD = WS.MLT.DIVD.ARY<1,CYPOS>

    IF WS.MLT.DIVD EQ "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT * WS.RATE
        WS.AMT.REIM.LCL =  WS.AMT.REIM * WS.RATE
*        WS.MRG.AMT.LOCAL = WS.MRG.AMT * WS.RATE
    END

    IF WS.MLT.DIVD NE "MULTIPLY" THEN
        WS.AMT.LOCAL = WS.AMT / WS.RATE
        WS.AMT.REIM.LCL =  WS.AMT.REIM / WS.RATE
*        WS.MRG.AMT.LOCAL = WS.MRG.AMT / WS.RATE
    END

RETURN
*--------------------------------------------------
A.600.WRITE:
    R.TEMP<SCB.TMP.CUSTOMER> = WS.CUSTOMER
    R.TEMP<SCB.TMP.BRANCH>   = WS.BR
    R.TEMP<SCB.TMP.CURRENCY> = WS.CY
    R.TEMP<SCB.TMP.AMT.LCY>  = WS.LCY
    R.TEMP<SCB.TMP.AMT.FCY>  = WS.FCY
    R.TEMP<SCB.TMP.RESERVED10> = WS.LCY.REIM
    R.TEMP<SCB.TMP.RESERVED9> =  WS.FCY.REIM
    R.TEMP<SCB.TMP.RESERVED8> = WS.CATEG
    R.TEMP<SCB.TMP.RESERVED7> = WS.V.DATE
    R.TEMP<SCB.TMP.RESERVED6> = WS.FIN.DATE

    CALL F.WRITE(FN.TEMP,WS.ARAY.RCL,R.TEMP)
    CALL  JOURNAL.UPDATE(WS.ARAY.RCL)

RETURN
END
