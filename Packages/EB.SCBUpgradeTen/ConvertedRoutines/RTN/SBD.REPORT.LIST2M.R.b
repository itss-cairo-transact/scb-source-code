* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-48</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SBD.REPORT.LIST2M.R(ENQ.DATA)
***    PROGRAM    SBD.REPORT.LIST2M.R
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HOLD.CONTROL

*-----------------------------------

    WS.COMP = ID.COMPANY

    XXX.DATE = TODAY
*   WRK.YY = XXX.DATE[1,4]
*   WRK.MM = XXX.DATE[5,2]
*   WRK.DD = XXX.DATE[7,2]
*   WRK.MM = WRK.MM - 1
*   IF WRK.MM LT 1 THEN
*       WRK.MM = 12
*       WRK.YY = WRK.YY - 1
*   END
*   MON.MM = FMT(MON.MM,"R%2")
*   WRK.YYYYMM = WRK.YY:WRK.MM:"..."
*   WRK.YYYYMM   = 200911:'...'

    CC = ''

    INPTXT = '����� ��� ������� �������  ':'yyyymmdd'
    CALL TXTINP(INPTXT, 8, 22, 10,CC)
    LST.DATE = COMI



***    XTEXT = "DATE ":WRK.YYYYMM ;CALL REM
*****   CALL CDT('', WORK.DATE, "-1W")
*------------------------
    FN.HOLD = "F.HOLD.CONTROL"
    F.HOLD = ''
    R.HOLD.REC = ''
    Y.HOLD = ''
    Y.HOLD.ERR = ''

    CALL OPF(FN.HOLD,F.HOLD)

    SEL.CMD   = "SELECT ":FN.HOLD:" BY REPORT.NAME WITH"
    SEL.CMD  := " BANK.DATE LIKE ...":LST.DATE:"..."
    SEL.CMD  := " AND RUN.IN.BATCH EQ 'Y'"
    SEL.CMD  := " AND COMPANY.ID EQ ":WS.COMP
    SEL.CMD  := " AND REPORT.NAME LIKE SB..."
*    PRINT SEL.CMD
*    STOP
*    SEL.CMD  := " AND (REPORT.NAME LIKE SBM..."
*    SEL.CMD  := " OR REPORT.NAME LIKE SBQ..."
*    SEL.CMD  := " OR REPORT.NAME LIKE SBH..."
*    SEL.CMD  := " OR REPORT.NAME LIKE SBW..."
*    SEL.CMD  := " OR REPORT.NAME LIKE SBY...)"


    CALL EB.READLIST(SEL.CMD,SELLIST,'',NOREC,RTNCD)


    IF NOREC >= 1 THEN
        FOR ENQ.LP = 1 TO NOREC
            ENQ.DATA<2,ENQ.LP> = '@ID'
            ENQ.DATA<3,ENQ.LP> = 'EQ'
            ENQ.DATA<4,ENQ.LP> = SELLIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ.DATA<2> = '@ID'
        ENQ.DATA<3> = 'EQ'
        ENQ.DATA<4> = 'DUUMY'
    END
    RETURN

END
