* @ValidationCode : MjoxNDE0Nzc4Mjc3OkNwMTI1MjoxNjQwODMxNzA5MTE0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 18:35:09
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
SUBROUTINE SBD.STMT.FT.MISS
***    PROGRAM SBD.STMT.FT.MISS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
    $INCLUDE  I_F.STMT.ENTRY      ;*AC.STE.
*    $INCLUDE GLOBUS.BP I_F.STMT.PRINTED ;*STP.
*    $INCLUDE GLOBUS.BP I_F.ACCT.STMT.PRINT        ;*ASP.
*    $INCLUDE  I_F.ACCT.ENT.TODAY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
*Line [ 38 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
*Line [ 39 ] Adding EB.SCBUpgradeTen. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
    GOSUB CALLDB
    GOSUB PROCESS
    PRINT STR('-',50):"  ����� �������  ":STR('-',50)
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
*========================================
INITIATE:
*-------
    REPORT.ID  ='SBD.STMT.FT.MISS'
    REPORT.NAME='SBD.STMT.FT.MISS'
    CALL PRINTER.ON(REPORT.ID,'')
    K = 0
    TDD = TODAY
    FT.ID = ''
    KEY.LIST  = '' ; SELECTED  = ''
    KEY.LIST2 = '' ; SELECTED2 = ''
RETURN
*=======================================
CALLDB:
*-------
    FN.STE = 'F.STMT.ENTRY' ; F.STE = '' ; R.STE = ''
    CALL OPF(FN.STE,F.STE)
    FN.FTE = 'FBNK.FUNDS.TRANSFER' ; F.FTE = '' ; R.FTE = ''
    CALL OPF(FN.FTE,F.FTE)
    FN.ENT = 'FBNK.ACCT.ENT.TODAY' ; F.ENT = '' ; R.ENT = ''
    CALL OPF(FN.ENT,F.ENT)
RETURN
*========================================
PROCESS:
*-------
    SEL.CMD = "SELECT ":FN.FTE:" WITH TRANSACTION.TYPE LIKE AC3... "
    SEL.CMD := " AND PROCESSING.DATE EQ ":TDD
*    SEL.CMD := " AND CURR.NO EQ 1 "
    SEL.CMD := " BY @ID"
    CALL EB.READLIST(SEL.CMD,KEY.LIST,'',SELECTED,ERR.MSG)
    IF SELECTED THEN
        CRT SELECTED
        FOR I = 1 TO SELECTED
            K++
            CALL F.READ(FN.FTE,KEY.LIST<I>,R.FTE,F.FTE,FTE.ERR)
            DB.ACCT = R.FTE<FT.DEBIT.ACCT.NO>
            DB.AMT  = R.FTE<FT.AMOUNT.DEBITED>
*Line [ 81 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            SSST    = DCOUNT(R.FTE<FT.STMT.NOS>,@VM)
            STE.FLAG = 'FALSE'
            KEY.LIST2 = '' ; SELECTED2 = ''
            FOR KK = 1 TO SSST
                FT.STE  = R.FTE<FT.STMT.NOS,KK>
                FINDSTR '.' IN FT.STE SETTING MMM THEN
                    STE.LK.ID = FIELD(FT.STE,'.',1)
                    LEN.STE   = LEN(STE.LK.ID)
                    CALL F.READ(FN.ENT,DB.ACCT,R.ENT,F.ENT,ER.ENT)
                    LOOP
                        REMOVE STE.ID FROM R.ENT SETTING POS.STE
                    WHILE STE.ID:POS.STE
                        IF STE.ID[1,LEN.STE] EQ STE.LK.ID THEN
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('STMT.ENTRY':@FM:AC.STE.ACCOUNT.NUMBER,STE.ID,STE.ACC)
F.ITSS.STMT.ENTRY = 'FBNK.STMT.ENTRY'
FN.F.ITSS.STMT.ENTRY = ''
CALL OPF(F.ITSS.STMT.ENTRY,FN.F.ITSS.STMT.ENTRY)
CALL F.READ(F.ITSS.STMT.ENTRY,STE.ID,R.ITSS.STMT.ENTRY,FN.F.ITSS.STMT.ENTRY,ERROR.STMT.ENTRY)
STE.ACC=R.ITSS.STMT.ENTRY<AC.STE.ACCOUNT.NUMBER>
                            IF STE.ACC NE '' THEN
                                STE.FLAG = 'TRUE'
                            END
                        END
                    REPEAT
***                    CRT KEY.LIST<I>:" - ":FT.STE:" - ":STE.FLAG
                END
            NEXT KK
            IF STE.FLAG NE 'TRUE' THEN
                FT.ERR = KEY.LIST<I>
                CRT FT.ERR:"      ":I
                GOSUB PRINTFT
            END
        NEXT I
    END ELSE
*Line [ 111 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        NULL
    END
RETURN
*...................................
PRINTFT:
    XX1 = SPACE(120)
    XX1<1,K>[1,16]  = FT.ERR
    XX1<1,K>[20,16] = DB.ACCT
    XX1<1,K>[40,20] = FMT(DB.AMT,"L2,")
    PRINT XX1<1,K>[1,80]
RETURN
*...................................
*===============================================================
PRINT.HEAD:
*---------
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    TD = TDD
    TIMEE   = TIMEDATE()
    HHH     = FIELD(TIMEE, ":", 1)
    MIN     = FIELD(TIMEE,":", 2)
    PART3   = FIELD(TIMEE,":", 3)
    SEC     = PART3[1,2]
    TIMEFMT = MIN:":":HHH
    T.DAY1  = FMT(TD,"####/##/##")
    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = FMT(DATY,"####/##/##")
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:"  �����: ":TIMEFMT:SPACE(65):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.NAME
    PR.HD :="'L'":SPACE(50):"������ ATM ��� ����� ���� ������ ������ : ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":"��� �������":SPACE(10):"��� ������":SPACE(10):"���� �����"
    PR.HD :="'L'":STR('_',120)
***    PRINT
    HEADING PR.HD
RETURN
*==============================================================
END
