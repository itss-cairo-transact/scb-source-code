* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBM.BAL.1455

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*---------------------------------------
    YTEXT  = 'Y/N �� ��� ����� �� ����� ��������  '

    CALL TXTINP(YTEXT, 8, 22, "1", "A")
    FLG = COMI
    IF FLG = 'Y' OR FLG = 'y' THEN
        GOSUB INITIALISE
        GOSUB BUILD.RECORD
        TEXT = "�� �������� �� ��������" ; CALL REM

    END ELSE
        TEXT = '�� ������ �� �������� ' ; CALL REM
    END

    RETURN

*----------------------------------------------------------------
INITIALISE:
    OPENSEQ "MECH" , "HR.BAL.1455" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"HR.BAL.1455"
        HUSH OFF
    END
    OPENSEQ "MECH" , "HR.BAL.1455" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE HR.BAL.1455 CREATED IN MECH'
        END ELSE
            STOP 'Cannot create HR.BAL.1455 File IN MECH'
        END
    END

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    COMMA = ","
    V.DATE = TODAY
    RETURN
*----------------------------------------------------
BUILD.RECORD:

    T.SEL = "SELECT FBNK.ACCOUNT WITH CATEGORY EQ 1455 AND ONLINE.ACTUAL.BAL GT 0 BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.AC,KEY.LIST<I>,R.AC,F.AC,E1)
            DB.ACCT = KEY.LIST<I>
            ACCT.ID = DB.ACCT[1,10]
            CR.ACCT = ACCT.ID:'144501'
            WS.AMT  = R.AC<AC.ONLINE.ACTUAL.BAL>

            IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//EG0010099,'

            OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":"EGP":COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":"EGP":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":WS.AMT:COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
            OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
            OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"1455.BALANCE":','
            OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":"1455.BALANCE":','

            MSG.DATA = IDD:",":OFS.MESSAGE.DATA

            WRITESEQ MSG.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM MECH TO OFS.IN HR.BAL.1455'
    EXECUTE 'DELETE ':"MECH":' ':"HR.BAL.1455"

    RETURN
***********************************
