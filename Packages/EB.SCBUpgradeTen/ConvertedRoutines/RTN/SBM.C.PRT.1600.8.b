* @ValidationCode : Mjo2NDcxNjE4Njk6Q3AxMjUyOjE2NDA5MDE0OTM3MzY6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 30 Dec 2021 13:58:13
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
SUBROUTINE  SBM.C.PRT.1600.8
    
*Line [ 18 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_COMMON
*Line [ 20 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_EQUATE
*Line [ 22 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT  I_F.CBE.STATIC.MAST.P
*Line [ 24 ] Removed directory from $INSERT - ITSS - R21 Upgrade - 2021-12-23
    $INSERT   I_F.COMPANY

    FLG = 0
    YTEXT = " Enter (1-PRINT) OR (2-CBE)"
    CALL TXTINP(YTEXT, 8, 22, "17", "A")
    WS.NUM  = COMI

    IF WS.NUM EQ 1 THEN
        REPORT.ID='SBM.C.PRT.001'
        FLG = 1
        GOSUB INITIAL
    END
    IF WS.NUM EQ 2 THEN
        REPORT.ID='1600.8'
        FLG = 1
        GOSUB INITIAL
    END
    IF FLG EQ 0 THEN
        TEXT = "INVALID INPUT NUMBER"
        RETURN
    END
    TEXT = "DONE" ; CALL REM
RETURN
*--------------------------------------------
INITIAL:
*-------------------------------------------------
****����� ������ ������� ����� 1600 ������ 9
****����� ����� ���
****1600
****����
****8
*----------------------------------------
    FN.CBE = "F.CBE.STATIC.MAST.P"
    F.CBE  = ""
*    FN.BR = "F.DEPT.ACCT.OFFICER"
*    F.BR = ""

    FN.COMP = "F.COMPANY"
    F.COMP = ""
*------------------------------------------
    CALL OPF (FN.CBE,F.CBE)
*    CALL OPF (FN.BR,F.BR)
    CALL OPF (FN.COMP,F.COMP)

*------------------------------------------------CLEAR AREA
    CALL PRINTER.ON(REPORT.ID,'')
    ETEXT = ""
    FLAG.FRST = 0
    WS.ARRY.RAW = ""
    WS.ARRY.COL = ""
    WS.FROM = ""
    WS.TO = ""
    WS.H.D.T = ""
    WS = ""
    WSRNG = ""
    WS.T = ""
    WS.1.LE = ""
    WS.2.LE = ""
    WS.3.LE = ""

    WS.1.EQV = ""
    WS.2.EQV = ""
    WS.3.EQV = ""
    WS.HD.T  = "����� ������ � ���������� ���� ������ ������� � ������ ������ ���������  "

    WS.HD.TA = " ����� ��� 1600 "



    WS.HD.T2 = "��������� ���������� ���� ���� ��� ������ ��������� "

    WS.HD.T2A = "����         8 "


    WS.HD.T3  = "������ ����� ���  "

    WS.HD.1  = "���� �����"
    WS.HD.1A = "���� ������"


    WS.HD.2  =  "��� � ��� "

    WS.HD.2A = "���� �� ��� "





    WS.PRG.1 = "SBM.C.PRT.1600.8"
********    ARRAY1 = ""
******                                 ARRAY
******                                                 1 ������
******
****** 2----   H = HEADER  D = DETAIL   T = TOTAL
******  3 AND 4 ----   RANGE  FROM  TO    FOR INDUSTRY
******  5  TOTAL  OF  FIRST DATA COLUMN   IN LE
******  6  TOTAL  OF  2     DATA COLUMN   IN LE
******* 7  TOTAL  OF  3     DATA COLUMN   IN LE
******* 8  TOTAL  OF  1     DATA COLUMN   IN EQVELENT
********9  TOTAL  OF  2     DATA COLUMN   IN EQVELENT
********10 TOTAL  OF  3     DATA COLUMN   IN EQVELENT
********11 THE ARRAY NO OF  TOTAL OF GROUP TO BE ACUMLATED
******  12 THE SECTOR NO

    DIM ARRAY1(32,10)


    ARRAY1(1,1) = "����� : ���� �������               "

    ARRAY1(1,2) = "HH"
*------------------------------------------------------
    ARRAY1(2,1) = "������ �����                       "
    ARRAY1(2,2) = "H"
*------------------------------------------------------
    ARRAY1(3,1) = "����� ���� ��������                "
    ARRAY1(3,2) = "D"
    ARRAY1(3,5) = "0"
    ARRAY1(3,6) = "0"
    ARRAY1(3,7) = "0"
    ARRAY1(3,8) = "0"
    ARRAY1(3,9) = "6"
    ARRAY1(3,10) = "16"
*------------------------------------------------------
    ARRAY1(4,1) = "����� ���� ������� �����            "
    ARRAY1(4,2) = "D"
    ARRAY1(4,5) = "0"
    ARRAY1(4,6) = "0"
    ARRAY1(4,7) = "0"
    ARRAY1(4,8) = "0"
    ARRAY1(4,9) = "6"
    ARRAY1(4,10) = "16"
*-------------------------------------------------------
    ARRAY1(5,1) = "����� ������ �����                 "
    ARRAY1(5,2) = "D"
    ARRAY1(5,5) = "0"
    ARRAY1(5,6) = "0"
    ARRAY1(5,7) = "0"
    ARRAY1(5,8) = "0"
    ARRAY1(5,9) = "6"
    ARRAY1(5,10) = "16"
*--------------------------------------------------------
    ARRAY1(6,1) = "����� ������ �����                 "
    ARRAY1(6,2) = "T"
    ARRAY1(6,5) = "0"
    ARRAY1(6,6) = "0"
    ARRAY1(6,7) = "0"
    ARRAY1(6,8) = "0"
*----------------------------------------------------------
    ARRAY1(7,1) = "���� ������� �����                  "
    ARRAY1(7,2) = "H"
*----------------------------------------------------------
    ARRAY1(8,1)  = "����� ������� ����� ���������     "
    ARRAY1(8,2)  = "D"
    ARRAY1(8,5) = "0"
    ARRAY1(8,6) = "0"
    ARRAY1(8,7) = "0"
    ARRAY1(8,8) = "0"
    ARRAY1(8,9) = "14"
    ARRAY1(8,10) = "16"
*---------------------------------------------------------
    ARRAY1(9,1) = "����� ������� ������                 "
    ARRAY1(9,2) = "D"
    ARRAY1(9,5) = "0"
    ARRAY1(9,6) = "0"
    ARRAY1(9,7) = "0"
    ARRAY1(9,8) = "0"
    ARRAY1(9,9) = "14"
    ARRAY1(9,10) = "16"
*----------------------------------------------------------
    ARRAY1(10,1) = "����� �������                       "
    ARRAY1(10,2) = "D"
    ARRAY1(10,5) = "0"
    ARRAY1(10,6) = "0"
    ARRAY1(10,7) = "0"
    ARRAY1(10,8) = "0"
    ARRAY1(10,9) = "14"
    ARRAY1(10,10) = "16"
*-----------------------------------------------------------
    ARRAY1(11,1) = "�������� ���������                 "
    ARRAY1(11,2) = "D"
    ARRAY1(11,5) = "0"
    ARRAY1(11,6) = "0"
    ARRAY1(11,7) = "0"
    ARRAY1(11,8) = "0"
    ARRAY1(11,9) = "14"
    ARRAY1(11,10) = "16"
*---------------------------------------------------------
    ARRAY1(12,1) = "����� �����                        "
    ARRAY1(12,2) = "D"
    ARRAY1(12,5) = "0"
    ARRAY1(12,6) = "0"
    ARRAY1(12,7) = "0"
    ARRAY1(12,8) = "0"
    ARRAY1(12,9) = "14"
    ARRAY1(12,10) = "16"
*---------------------------------------------------------------------
    ARRAY1(13,1) = "����� ���� ����� ������� �����     "
    ARRAY1(13,2) = "D"
    ARRAY1(13,5) = "0"
    ARRAY1(13,6) = "0"
    ARRAY1(13,7) = "0"
    ARRAY1(13,8) = "0"
    ARRAY1(13,9) = "14"
    ARRAY1(13,10) = "16"
*-------------------------------------------------------------
    ARRAY1(14,1) = "����� ���� ������� �����          "

    ARRAY1(14,2) = "T"
    ARRAY1(14,5) = "0"
    ARRAY1(14,6) = "0"
    ARRAY1(14,7) = "0"
    ARRAY1(14,8) = "0"
**************************************************************
    ARRAY1(15,1)  =  "����� ����� (1)                "
    ARRAY1(15,2)  =  "E"
*************************************************************
    ARRAY1(16,1)  =  "����� ���� �������             "

    ARRAY1(16,2)  =  "GT"
    ARRAY1(16,5)  =  "0"
    ARRAY1(16,6)  =  "0"
    ARRAY1(16,7)  =  "0"
    ARRAY1(16,8)  =  "0"
****************************************************************
*****************************************************************


    ARRAY1(17,1) = "����� : ���� �������             "

    ARRAY1(17,2) = "HH"
*------------------------------------------------------
    ARRAY1(18,1) = "������ �����                       "
    ARRAY1(18,2) = "H"
*------------------------------------------------------
    ARRAY1(19,1) = "����� ���� ��������                "
    ARRAY1(19,2) = "D"
    ARRAY1(19,5) = "0"
    ARRAY1(19,6) = "0"
    ARRAY1(19,7) = "0"
    ARRAY1(19,8) = "0"
    ARRAY1(19,9) = "22"
    ARRAY1(19,10) = "32"
*------------------------------------------------------
    ARRAY1(20,1) = "����� ���� ������� �����            "
    ARRAY1(20,2) = "D"
    ARRAY1(20,5) = "0"
    ARRAY1(20,6) = "0"
    ARRAY1(20,7) = "0"
    ARRAY1(20,8) = "0"
    ARRAY1(20,9) = "22"
    ARRAY1(20,10) = "32"
*-------------------------------------------------------
    ARRAY1(21,1) = "����� ������ �����                 "
    ARRAY1(21,2) = "D"
    ARRAY1(21,5) = "0"
    ARRAY1(21,6) = "0"
    ARRAY1(21,7) = "0"
    ARRAY1(21,8) = "0"
    ARRAY1(21,9) = "22"
    ARRAY1(21,10) = "32"
*--------------------------------------------------------
    ARRAY1(22,1) = "����� ������ �����                 "
    ARRAY1(22,2) = "T"
    ARRAY1(22,5) = "0"
    ARRAY1(22,6) = "0"
    ARRAY1(22,7) = "0"
    ARRAY1(22,8) = "0"
*----------------------------------------------------------
    ARRAY1(23,1) = "���� ������� �����                  "
    ARRAY1(23,2) = "H"
*----------------------------------------------------------
    ARRAY1(24,1)  = "����� ������� ����� ���������     "
    ARRAY1(24,2)  = "D"
    ARRAY1(24,5) = "0"
    ARRAY1(24,6) = "0"
    ARRAY1(24,7) = "0"
    ARRAY1(24,8) = "0"
    ARRAY1(24,9) = "30"
    ARRAY1(24,10) = "32"
*---------------------------------------------------------
    ARRAY1(25,1) = "����� ������� ������                 "
    ARRAY1(25,2) = "D"
    ARRAY1(25,5) = "0"
    ARRAY1(25,6) = "0"
    ARRAY1(25,7) = "0"
    ARRAY1(25,8) = "0"
    ARRAY1(25,9) = "30"
    ARRAY1(25,10) = "32"
*----------------------------------------------------------
    ARRAY1(26,1) = "����� �������                       "
    ARRAY1(26,2) = "D"
    ARRAY1(26,5) = "0"
    ARRAY1(26,6) = "0"
    ARRAY1(26,7) = "0"
    ARRAY1(26,8) = "0"
    ARRAY1(26,9) = "30"
    ARRAY1(26,10) = "32"
*-----------------------------------------------------------
    ARRAY1(27,1) = "�������� ���������                 "
    ARRAY1(27,2) = "D"
    ARRAY1(27,5) = "0"
    ARRAY1(27,6) = "0"
    ARRAY1(27,7) = "0"
    ARRAY1(27,8) = "0"
    ARRAY1(27,9) = "30"
    ARRAY1(27,10) = "32"
*---------------------------------------------------------
    ARRAY1(28,1) = "����� �����                        "
    ARRAY1(28,2) = "D"
    ARRAY1(28,5) = "0"
    ARRAY1(28,6) = "0"
    ARRAY1(28,7) = "0"
    ARRAY1(28,8) = "0"
    ARRAY1(28,9) = "30"
    ARRAY1(28,10) = "32"
*---------------------------------------------------------------------
    ARRAY1(29,1) = "����� ���� ����� ������� �����     "
    ARRAY1(29,2) = "D"
    ARRAY1(29,5) = "0"
    ARRAY1(29,6) = "0"
    ARRAY1(29,7) = "0"
    ARRAY1(29,8) = "0"
    ARRAY1(29,9) = "30"
    ARRAY1(29,10) = "32"
*-------------------------------------------------------------
    ARRAY1(30,1) = "����� ���� ������� �����           "
    ARRAY1(30,2) = "T"
    ARRAY1(30,5) = "0"
    ARRAY1(30,6) = "0"
    ARRAY1(30,7) = "0"
    ARRAY1(30,8) = "0"
**************************************************************
    ARRAY1(31,1)  =  "����� ����� (1)                "
    ARRAY1(31,2)  =  "D"
    ARRAY1(31,5) = "0"
    ARRAY1(31,6) = "0"
    ARRAY1(31,7) = "0"
    ARRAY1(31,8) = "0"
    ARRAY1(31,9) = "0"
    ARRAY1(31,10) = "32"
*************************************************************
    ARRAY1(32,1)  =  "����� ���� �������             "


    ARRAY1(32,2)  =  "GT"
    ARRAY1(32,5)  =  "0"
    ARRAY1(32,6)  =  "0"
    ARRAY1(32,7)  =  "0"
    ARRAY1(32,8)  =  "0"
*****************************************************************
*****************************************************************
***********   THE RANGE FOR INDUSTRY *************************
*FIRST   RANGE FROM
*SECOND RANGE  TO
*THIRD  NO OF ACUMULATION ON ARRAY1
*****************************************************************
*****************************************************************
*
    DIM ARRAYRNG(19,2)
*
*    ARRAY1(1) =  "����� : ���� �������              "
*
*------------------------------------------------------
*    ARRAY1(2) = "������ �����                       "
*------------------------------------------------------
*    ARRAY1(3) = "����� ���� ��������                "
    ARRAYRNG(1,1) = "3110"
    ARRAYRNG(1,2) = "3"

*------------------------------------------------------
*    ARRAY1(4) = "����� ���� ������� �����            "
    ARRAYRNG(2,1) = "3120"
    ARRAYRNG(2,2) = "4"
*
*-------------------------------------------------------
*    ARRAY1(5) = "����� ������ �����                 "
    ARRAYRNG(3,1) = "3130"
    ARRAYRNG(3,2) = "5"

*    ARRAY1(7) = "���� ������� �����                  "
*----------------------------------------------------------
*    ARRAY1(8)  = "����� ������� ����� ���������     "
    ARRAYRNG(4,1)  = "3210"
    ARRAYRNG(4,2)  = "8"
*
*
*---------------------------------------------------------
*    ARRAY1(9) = "����� ������� ������                 "
    ARRAYRNG(5,1) = "3220"
    ARRAYRNG(5,2) = "9"

*----------------------------------------------------------
*    ARRAY1(10) = "����� �������                       "
    ARRAYRNG(6,1) = "3230"
    ARRAYRNG(6,2) = "10"

*    ARRAY1(11) = "�������� ���������                 "
    ARRAYRNG(7,1) = "3240"
    ARRAYRNG(7,2) = "11"

*---------------------------------------------------------
*    ARRAY1(12) = "����� �����                        "
    ARRAYRNG(8,1) = "3250"
    ARRAYRNG(8,2) = "12"

*---------------------------------------------------------------------
*    ARRAY1(13) = "����� ���� ����� ������� �����     "
    ARRAYRNG(9,1) = "3260"
    ARRAYRNG(9,2) = "13"

****************************************************************
*    ARRAY1(15)  =  "����� ����� (1)                "
*************************************************************
*
*    ARRAY1(17) = "����� : ���� �������             "
*------------------------------------------------------
*    ARRAY1(18) = "������ �����                       "
*------------------------------------------------------
*    ARRAY1(19) = "����� ���� ��������                "
    ARRAYRNG(10,1) = "4110"
    ARRAYRNG(10,2) = "19"

*------------------------------------------------------
*    ARRAY1(20) = "����� ���� ������� �����            "
    ARRAYRNG(11,1) = "4120"
    ARRAYRNG(11,2) = "20"
*
*-------------------------------------------------------
*    ARRAY1(21) = "����� ������ �����                 "
    ARRAYRNG(12,1) = "4130"
    ARRAYRNG(12,2) = "21"

*--------------------------------------------------------
*    ARRAY1(23) = "���� ������� �����                  "

*    ARRAY1(24)  = "����� ������� ����� ���������     "
    ARRAYRNG(13,1)  = "4210"
    ARRAYRNG(13,2)  = "24"
*
*---------------------------------------------------------
*    ARRAY1(25) = "����� ������� ������                 "
    ARRAYRNG(14,1) = "4220"
    ARRAYRNG(14,2) = "25"

*----------------------------------------------------------
*    ARRAY1(26) = "����� �������                       "
    ARRAYRNG(15,1) = "4230"
    ARRAYRNG(15,2) = "26"

*-----------------------------------------------------------
*    ARRAY1(27) = "�������� ���������                 "
    ARRAYRNG(16,1) = "4240"
    ARRAYRNG(16,2) = "27"

*---------------------------------------------------------
*    ARRAY1(28) = "����� �����                        "
    ARRAYRNG(17,1) = "4250"
    ARRAYRNG(17,2) = "28"
*---------------------------------------------------------------------
*    ARRAY1(29) = "����� ���� ����� ������� �����     "
    ARRAYRNG(18,1) = "4260"
    ARRAYRNG(18,2) = "29"

*-------------------------------------------------------------
*    ARRAY1(31)  =  "����� ����� (1)                "
    ARRAYRNG(19,1) = "4600"
    ARRAYRNG(19,2) = "31"

****************  PROCEDURE *****************
*-------------------------------------------PREPARE  VARIABLE
    WS.CBE.ID.INSR  = ""
    GOSUB A.050.GET.ALL.BR
*    WS.INDSTRY = ""
*    GOSUB A.5000.PRT.HEAD
*    GOSUB A.100.PROCESS
*    GOSUB A.300.PRNT
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
RETURN
A.050.GET.ALL.BR:
    SEL.CMDC = "SELECT ":FN.COMP:" BY @ID"
    CALL EB.READLIST(SEL.CMDC,SEL.LISTC,"",NO.OF.RECC,RET.CODEC)
    LOOP
        REMOVE WS.COMP.ID FROM SEL.LISTC SETTING POSC
    WHILE WS.COMP.ID:POSC

        CALL F.READ(FN.COMP,WS.COMP.ID,R.COMP,F.COMP,MSG.COMP)
        WS.BR.NAME = R.COMP<EB.COM.COMPANY.NAME,2>
        WS.BR = WS.COMP.ID[2]
        IF WS.BR = 88 THEN
            GOTO A.050.A
        END
        IF WS.BR NE 99 THEN
            GOTO A.050.A
        END
*        IF WS.BR LT 10 THEN
*            WS.BR = WS.COMP.ID[1]
*        END
        WS.FLAG.PRT = 0
        GOSUB A.100.PROCESS
        WS.ARRY.RAW = 1
        WS.ARRY.COL = 1
        IF WS.FLAG.PRT = 1 THEN
*CRT "--------------------> ":WS.BR:" ":WS.FLAG.PRT
            WS.FLAG.PRT = 0
*            GOSUB A.5100.PRT.SPACE.PAGE
            GOSUB A.5000.PRT.HEAD
            GOSUB A.300.PRNT
        END
*
A.050.A:
    REPEAT
RETURN
A.100.PROCESS:
    IF WS.BR NE 99  THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE EG001... AND CBE.BR EQ ":WS.BR
    END
    IF WS.BR EQ 99  THEN
        SEL.CMD = "SELECT ":FN.CBE:" WITH @ID LIKE EG001..."
    END

    CALL EB.READLIST(SEL.CMD,SEL.LIST,"",NO.OF.REC,RET.CODE)
    LOOP
        REMOVE WS.SCC.ID FROM SEL.LIST SETTING POS
    WHILE WS.SCC.ID:POS

        CALL F.READ(FN.CBE,WS.SCC.ID,R.CBE,F.CBE,MSG.CBE)
        WS.TMP = R.CBE<P.CBE.BR>
        IF WS.BR = 99 THEN
            GOTO A.100.A
        END
        IF WS.TMP NE WS.BR  THEN
            GOTO AAAA
        END
A.100.A:
        WS.INDSTRY = R.CBE<P.CBE.NEW.SECTOR>
*****����� ������ ������ �� ���� ����� ������ ����� ���� �������
*****������ ������� �������
        WS.CBE.ID.INSR  = WS.SCC.ID[1]
        IF WS.CBE.ID.INSR EQ "A"  THEN
            GOTO AAAA
        END


        WS.1.LE = R.CBE<P.CBE.FACLTY.LE> + R.CBE<P.CBE.CUR.AC.LE.DR>
*MSABRY 2014/1/30
*        WS.2.LE = "0"
        WS.2.LE = R.CBE<P.CBE.LOANS.LE.L>
        WS.1.EQV = R.CBE<P.CBE.FACLTY.EQ> + R.CBE<P.CBE.CUR.AC.EQ.DR>
*MSABRY 2014/1/30
*       WS.2.EQV = "0"
        WS.2.EQV = R.CBE<P.CBE.LOANS.EQ.L>

        GOSUB A.200.ACUM
*-----------------------------------------------------
AAAA:
    REPEAT
BBB:
RETURN
A.200.ACUM:
    FOR WSRNG = 1 TO 19

        GOSUB A.205.CHK.INDSTRY

    NEXT WSRNG
RETURN
A.205.CHK.INDSTRY:
    IF  WS.INDSTRY NE ARRAYRNG(WSRNG,1) THEN
        RETURN
    END

    WS = ARRAYRNG(WSRNG,2)
    WS.FLAG.PRT = 1
    GOSUB A.210.ACUM
RETURN

****        ARRAY          ������� ��� ��

A.210.ACUM:
    ARRAY1(WS,5) = ARRAY1(WS,5) + WS.1.LE
    ARRAY1(WS,6) = ARRAY1(WS,6) + WS.2.LE
    ARRAY1(WS,7) = ARRAY1(WS,7) + WS.1.EQV
    ARRAY1(WS,8) = ARRAY1(WS,8) + WS.2.EQV
    WS.T  = ARRAY1(WS,9)
    IF WS.T NE 0 THEN
        ARRAY1(WS.T,5) = ARRAY1(WS.T,5) + WS.1.LE
        ARRAY1(WS.T,6) = ARRAY1(WS.T,6) + WS.2.LE
        ARRAY1(WS.T,7) = ARRAY1(WS.T,7) + WS.1.EQV
        ARRAY1(WS.T,8) = ARRAY1(WS.T,8) + WS.2.EQV
    END
    WS.TO = ARRAY1(WS,10)
    ARRAY1(WS.TO,5) = ARRAY1(WS.TO,5) + WS.1.LE
    ARRAY1(WS.TO,6) = ARRAY1(WS.TO,6) + WS.2.LE
    ARRAY1(WS.TO,7) = ARRAY1(WS.TO,7) + WS.1.EQV
    ARRAY1(WS.TO,8) = ARRAY1(WS.TO,8) + WS.2.EQV

RETURN
*****                                     ARRAY   ������� ��

A.300.PRNT:
    FOR I = 1 TO 32
        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "HH" THEN
            GOSUB A.310.PRT.HD
        END

        WS.H.D.T = ARRAY1(I,2)
        IF WS.H.D.T = "H" THEN
            GOSUB A.310.PRT.HD
        END

****        XX<1,1>[1,35]   = ARRAY1(WS.ARRY.RAW,WS.ARRY.COL)
        IF WS.H.D.T = "T" THEN
            GOSUB A.320.PRT.TOT
        END

        IF WS.H.D.T = "D" THEN
            GOSUB A.330.PRT.DTAL
        END

        IF WS.H.D.T = "E" THEN
            GOSUB A.340.PRT.E
        END

        IF WS.H.D.T = "GT" THEN
            GOSUB A.320.PRT.TOT
        END

*******       WS.ARRY.RAW = WS.ARRY.RAW + 1
    NEXT I
RETURN
A.310.PRT.HD:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
****       XX<1,1>[1,35]   = "--------------------------"
*    XX<1,1>[1,35]   =  STR('-',35)
*    PRINT XX<1,1>
RETURN

A.320.PRT.TOT:
    WS.A5 = ARRAY1(I,5) / 1000
    WS.A6 = ARRAY1(I,6) / 1000
    WS.A7 = ARRAY1(I,7) / 1000
    WS.A8 = ARRAY1(I,8) / 1000
    XX = SPACE(132)
    XX<1,1>[1,35]    = ARRAY1(I,1)
    XX<1,1>[37,15]   = FMT(WS.A5, "R0,")
    XX<1,1>[53,15]   = FMT(WS.A6, "R0,")
    XX<1,1>[69,15]   = FMT(WS.A7, "R0,")
    XX<1,1>[85,15]   = FMT(WS.A8, "R0,")
    PRINT XX<1,1>
    XX = SPACE(132)
    XX<1,1>[1,132]   = STR('-',132)
    PRINT XX<1,1>
    ARRAY1(I,5) = 0
    ARRAY1(I,6) = 0
    ARRAY1(I,7) = 0
    ARRAY1(I,8) = 0
RETURN
A.330.PRT.DTAL:
    WS.A5 = ARRAY1(I,5) / 1000
    WS.A6 = ARRAY1(I,6) / 1000
    WS.A7 = ARRAY1(I,7) / 1000
    WS.A8 = ARRAY1(I,8) / 1000
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    XX<1,1>[37,15]   = FMT(WS.A5, "R0,")
    XX<1,1>[53,15]   = FMT(WS.A6, "R0,")
    XX<1,1>[69,15]   = FMT(WS.A7, "R0,")
    XX<1,1>[85,15]   = FMT(WS.A8, "R0,")
    PRINT XX<1,1>
    ARRAY1(I,5) = 0
    ARRAY1(I,6) = 0
    ARRAY1(I,7) = 0
    ARRAY1(I,8) = 0
RETURN

A.340.PRT.E:
    XX = SPACE(132)
    XX<1,1>[1,35]   = ARRAY1(I,1)
    PRINT XX<1,1>
RETURN
**************PRINT HEADER OF REPORT
A.5000.PRT.HEAD:
    WS.BR.H = WS.BR
    IF WS.BR LT 10 THEN
        WS.BR.H = WS.COMP.ID[1]
    END

*    CALL F.READ(FN.BR,WS.BR.H,R.BR,F.BR,MSG.BR)
*    WS.BR.NAME = R.BR<EB.DAO.NAME>
***    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):"��� ���� ������"
    PR.HD :="'L'":SPACE(1):WS.BR.NAME
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(34):WS.HD.T:SPACE(5):WS.HD.TA
    PR.HD :="'L'":SPACE(48):WS.HD.T2:SPACE(18):WS.HD.T2A
    PR.HD :="'L'":SPACE(110):WS.HD.T3

    PR.HD :="'L'":SPACE(110):WS.PRG.1
    PR.HD :="'L'":SPACE(42):WS.HD.1:SPACE(20):WS.HD.1A
    PR.HD :="'L'":SPACE(35):WS.HD.2:SPACE(3):WS.HD.2A:SPACE(9):WS.HD.2:SPACE(3):WS.HD.2A
    PR.HD :="'L'":" "
    PR.HD :="'L'":STR('_',132)
    HEADING PR.HD
    PRINT
RETURN
*-----------------------------------------------------------------
A.5100.PRT.SPACE.PAGE:
    IF FLAG.FRST EQ 0 THEN
        FLAG.FRST = 1
        RETURN
    END
    PR.HD ="'L'":SPACE(132)
    PRINT
    HEADING PR.HD
RETURN

END
