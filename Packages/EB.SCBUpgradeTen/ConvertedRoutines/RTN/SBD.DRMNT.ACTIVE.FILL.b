* @ValidationCode : MjotODQzODc3NzE5OkNwMTI1MjoxNjQxNzc4MTQ4MTgyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 17:29:08
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
PROGRAM SBD.DRMNT.ACTIVE.FILL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.IM.DOCUMENT.IMAGE
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DRMNT.FILES
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CU.DRMNT.INDEX

*----------------------------------------------------
    FN.CU   = "FBNK.CUSTOMER"         ; F.CU  = ""
    FN.SDF  = "F.SCB.DRMNT.FILES"     ; F.SDF = ""
    FN.DRI  = "F.SCB.CU.DRMNT.INDEX"  ; F.DRI = ""
    FN.IM   = "F.IM.DOCUMENT.IMAGE"   ; F.IM  = ""

    CALL OPF (FN.CU,F.CU)
    CALL OPF (FN.IM,F.IM)
    CALL OPF (FN.SDF,F.SDF)
    CALL OPF (FN.DRI,F.DRI)

    KEY.LIST=""  ; SELECTED=""   ;  ER.MSG=""
    KEY.LIST1="" ; SELECTED1=""  ;  ER.MSG1=""
    TD = TODAY

*------------------------------------------------------------------

    T.SEL = "SELECT ":FN.DRI:" WITH DRMNT.CODE.DATE EQ '' AND POSTING.RESTRICT LT 89 AND CODE.DATE EQ ":TD

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CU,KEY.LIST<I>,R.CU,F.CU,E1)
            CUS.ID        = KEY.LIST<I>
            CUS.NAME      = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            BRANCH.NO     = R.CU<EB.CUS.COMPANY.BOOK>
            WS.DRMNT.DATE = R.CU<EB.CUS.LOCAL.REF><1,CULR.DRMNT.DATE>

            T.SEL1 = "SELECT ":FN.IM:" WITH IMAGE.REFERENCE EQ ":CUS.ID
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            SIGN.COUNT = SELECTED1

            SDF.ID = CUS.ID:'-A-':TD
            CALL F.READ(FN.SDF,SDF.ID,R.SDF,F.SDF,E2)
            IF E2 THEN
                R.SDF<SDF.CUST.NO>         = CUS.ID
                R.SDF<SDF.CUST.NAME>       = CUS.NAME
                R.SDF<SDF.BRANCH.NO>       = BRANCH.NO
                R.SDF<SDF.SIGN.COUNT.SYS>  = SIGN.COUNT
                R.SDF<SDF.STATUS.DATE>     = TD
                R.SDF<SDF.DRMNT.DATE>      = WS.DRMNT.DATE

                CALL F.WRITE(FN.SDF,SDF.ID,R.SDF)
                CALL  JOURNAL.UPDATE(SDF.ID)
            END
        NEXT I
    END
