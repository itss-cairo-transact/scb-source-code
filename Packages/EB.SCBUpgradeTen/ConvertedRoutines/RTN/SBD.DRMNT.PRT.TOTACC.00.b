* @ValidationCode : Mjo4NjM0NjQxMDY6Q3AxMjUyOjE2NDA4MjUwOTIzNjY6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:44:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
*-----------------------------------------------------------------------------
* <Rating>-152</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SBD.DRMNT.PRT.TOTACC.00

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.POSTING.RESTRICT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 43 ]  HASHING "$INCLUDE I_AC.LOCAL.REFS" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_AC.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*--------------------------------------
    GOSUB INITIAL
    GOSUB PRINT.HEAD
    GOSUB PROCESS
    GOSUB PRINT.END
RETURN
*--------------------------------------
PRINT.END:
*---------
    XX = SPACE(120)
    PRINT XX<1,1>
    XX<1,1>[1,50]   = "**** ������ ��� �������  ****"
    XX<1,1>[70,20]  = FMT(CUS.NO,"R0,")
    PRINT XX<1,1>
    XX = SPACE(120)
    PRINT XX<1,1>
    PRINT SPACE(30):STR('_',20):"  ����� ����������  ":STR('_',20)
*-------------------------------
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

RETURN
*--------------------------------------
INITIAL:
*-------
    PROGRAM.ID = "SBD.DRMNT.PRT.TOTACC.00"

    FN.CUS.ACC = "FBNK.CUSTOMER.ACCOUNT" ;  F.CUS.ACC  = ""
    CALL OPF(FN.CUS.ACC,F.CUS.ACC)

    FN.CUSTOMER = "FBNK.CUSTOMER" ; F.CUSTOMER  = ""
    CALL OPF(FN.CUSTOMER , F.CUSTOMER)

    FN.ACC = "FBNK.ACCOUNT" ; F.ACC  = ""
    CALL OPF(FN.ACC,F.ACC)

    SYS.DATE = TODAY
    WS.COMP  = ID.COMPANY
    P.DATE   = FMT(SYS.DATE,"####/##/##")

    REPORT.ID = 'P.FUNCTION'
    CALL PRINTER.ON(REPORT.ID,'')

    CUS.NO    = 0
    LAST.DATE = ''
RETURN
*--------------------------------------
PROCESS:
*-------
    CUSTOMER.SEL  = "SELECT FBNK.CUSTOMER WITH POSTING.RESTRICT EQ '18'"
    CUSTOMER.SEL := " AND DRMNT.CODE EQ 1"
    CUSTOMER.SEL := " AND COMPANY.BOOK EQ ": WS.COMP

    CALL EB.READLIST(CUSTOMER.SEL,KEY.LIST,'',SELECTED,ERR)
    TEXT = SELECTED ; CALL REM

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            ACC.TOT = 0
            CUS.ID  = KEY.LIST<II>
            CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)
            LOOP
                REMOVE ACCOUNT.NUMBER FROM R.CUS.ACC SETTING POS.ACC
            WHILE ACCOUNT.NUMBER:POS.ACC
                CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACCT, F.ACC, E.ACC1)
                W.BAL = R.ACCT<AC.WORKING.BALANCE>
                ACC.TOT = ACC.TOT + W.BAL

            REPEAT

            IF ACC.TOT EQ 0 THEN
                GOSUB PRINT.CUSTOMER
                GOSUB PRINT.ACCOUNT
                GOSUB PRINT.LINE
            END

        NEXT II

        IF CUS.NO EQ 0 THEN
            GOSUB PRINT.NO.DATA
        END
    END ELSE
        GOSUB PRINT.NO.DATA
    END
RETURN
*--------------------------------------
PRINT.LINE:
*----------
    XX = STR('_',120)
    PRINT XX<1,1>
RETURN
*--------------------------------------
PRINT.CUSTOMER:
*--------------
    CUS.NO = CUS.NO + 1
    XX     = SPACE(120)
    XX<1,1>[1,10]   = CUS.ID
*Line [ 146 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUS.ID,LOCAL.REF)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOCAL.REF=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>
    CUST.NAME  = LOCAL.REF<1,CULR.ARABIC.NAME>

    XX<1,1>[12,40]  = CUST.NAME

    PRINT XX<1,1>

RETURN
*--------------------------------------
PRINT.ACCOUNT:
*-------------
    CALL F.READ(FN.CUS.ACC,CUS.ID,R.CUS.ACC,F.CUS.ACC,ERR.C.A)
    LOOP
        REMOVE ACCOUNT.NUMBER FROM R.CUS.ACC SETTING POS.ACC
    WHILE ACCOUNT.NUMBER:POS.ACC
        CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACCT, F.ACC, E.ACC1)

        W.BAL      = R.ACCT<AC.WORKING.BALANCE>
        Y.CATEG.ID = R.ACCT<AC.CATEGORY>
*Line [ 171 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CATEGORY':@FM:EB.CAT.SHORT.NAME,Y.CATEG.ID,CATEG.NAME)
F.ITSS.CATEGORY = 'F.CATEGORY'
FN.F.ITSS.CATEGORY = ''
CALL OPF(F.ITSS.CATEGORY,FN.F.ITSS.CATEGORY)
CALL F.READ(F.ITSS.CATEGORY,Y.CATEG.ID,R.ITSS.CATEGORY,FN.F.ITSS.CATEGORY,ERROR.CATEGORY)
CATEG.NAME=R.ITSS.CATEGORY<EB.CAT.SHORT.NAME>
        WS.CY      = R.ACCT<AC.CURRENCY>
*Line [ 179 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,WS.CY,CURR.NAME)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,WS.CY,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CURR.NAME=R.ITSS.CURRENCY<EB.CUR.CCY.NAME>
*-----
        CUST.DATE.CR    = R.ACCT<AC.DATE.LAST.CR.CUST>
        CUST.DATE.DR    = R.ACCT<AC.DATE.LAST.DR.CUST>
        OPENING.DATE    = R.ACCT<AC.OPENING.DATE>

        IF CUST.DATE.CR EQ ''   THEN
            IF CUST.DATE.DR EQ ''   THEN

                LAST.DATE = OPENING.DATE
            END
        END
*-----
        IF CUST.DATE.CR GT LAST.DATE THEN
            LAST.DATE = CUST.DATE.CR
        END

        IF CUST.DATE.DR GT LAST.DATE THEN
            LAST.DATE   = CUST.DATE.DR
        END
*-----
        XX = SPACE(120)
        XX<1,1>[1,20]   = ACCOUNT.NUMBER
        XX<1,1>[33,6]   = Y.CATEG.ID
        XX<1,1>[40,12]  = CATEG.NAME
        XX<1,1>[55,3]   = WS.CY
        XX<1,1>[60,15]  = CURR.NAME
        XX<1,1>[80,10]  = FMT(LAST.DATE,"####/##/##")
        XX<1,1>[110,20] = FMT(W.BAL,"R2,")

        PRINT XX<1,1>

    REPEAT
RETURN
*--------------------------------------
PRINT.NO.DATA:
*-------------
    XX = "********* �� ���� ����� **********"
    PRINT XX<1,1>
RETURN
*--------------------------------------
PRINT.HEAD:
*Line [ 227 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
    YYBRN = BRANCH

    PR.HD ="'L'":SPACE(1):"��� ���� ������ ":SPACE(90):"��� :":YYBRN
    PR.HD :="'L'":SPACE(1):" �������:":P.DATE:SPACE(85):"��� ������ :":"'P'"
    PR.HD :="'L'":SPACE(110):'SBD.DRMNT.PRT.TOTACC.00'
    PR.HD :="'L'":SPACE(35):"����� ��� ��� ����"
    PR.HD :=SPACE(1):"����� ������ ������ ���"
    PR.HD :="'L'":SPACE(30):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(05):" ��� ������ "
    PR.HD :="'L'":SPACE(1):" ��� ������":SPACE(25):" �������":SPACE(15):"������":SPACE(14):"��� ����":SPACE(17):"������"
    PR.HD :="'L'":SPACE(1):STR('_',120)

    HEADING PR.HD
    LINE.NO = 0
RETURN
*===============================================================

RETURN
END
