* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
    SUBROUTINE SBD.SOLFA.INST.CHECK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

*---------------------------------------
    OPENSEQ "MECH" , "SOLFA.BALANCE.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"SOLFA.BALANCE.CSV"
        HUSH OFF
    END
    OPENSEQ "MECH" , "SOLFA.BALANCE.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SOLFA.BALANCE.CSV CREATED IN MECH'
        END ELSE
            STOP 'Cannot create SOLFA.BALANCE.CSV File IN MECH'
        END
    END

    HEAD.DESC  = "��� ������":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "���� �����":","
    HEAD.DESC := "���� 1407":","
    HEAD.DESC := "���� 1408":","
    HEAD.DESC := "���� 1413":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*---------------------------------------
    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    SEQ.FILE.NAME = 'MECH'
    RECORD.NAME = 'SOLFA.csv'

    OPENSEQ SEQ.FILE.NAME,RECORD.NAME TO SEQ.FILE.POINTER ELSE
        PRINT 'Unable to Locate ':SEQ.FILE.POINTER
        STOP
        RETURN
    END
    EOF = ''
    LOOP WHILE NOT(EOF)
        READSEQ Y.MSG FROM SEQ.FILE.POINTER THEN
            EMP.NO   = FIELD(Y.MSG,",",1)
            AMT      = FIELD(Y.MSG,",",2)
            CUS.NO   = FMT(EMP.NO,'R%8')

            ACCT.NO.1407  = CUS.NO:'10140701'
            ACCT.NO.1408  = CUS.NO:'10140801'
            ACCT.NO.1413  = CUS.NO:'10141301'

            CALL F.READ(FN.AC,ACCT.NO.1407,R.AC,F.AC,E5)
            AMT.1407 = R.AC<AC.ONLINE.ACTUAL.BAL>
            IF AMT.1407 LT 0 THEN AMT.1407 = AMT.1407 * -1

            CUST.NAME = R.AC<AC.ACCOUNT.TITLE.1>

            CALL F.READ(FN.AC,ACCT.NO.1408,R.AC,F.AC,E5)
            AMT.1408 = R.AC<AC.ONLINE.ACTUAL.BAL>
            IF AMT.1408 LT 0 THEN AMT.1408 = AMT.1408 * -1

            CALL F.READ(FN.AC,ACCT.NO.1413,R.AC,F.AC,E5)
            AMT.1413 = R.AC<AC.ONLINE.ACTUAL.BAL>
            IF AMT.1413 LT 0 THEN AMT.1413 = AMT.1413 * -1

            TOTAL.AMT = AMT.1407 + AMT.1408 + AMT.1413

            IF AMT GT TOTAL.AMT THEN

                BB.DATA  = EMP.NO:","
                BB.DATA := CUST.NAME:","
                BB.DATA := AMT:","
                BB.DATA := AMT.1407:","
                BB.DATA := AMT.1408:","
                BB.DATA := AMT.1413:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

            END
*----------------------------------
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ SEQ.FILE.POINTER

    TEXT = '�� �������� �� ��������' ; CALL REM

    RETURN
************************************************************
END
