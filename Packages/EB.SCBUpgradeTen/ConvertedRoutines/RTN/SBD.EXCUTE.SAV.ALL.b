* @ValidationCode : MjoxNzYzMTUzNTU5OkNwMTI1MjoxNjQwODI0NDUwOTEwOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Dec 2021 16:34:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTen  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTen
*DONE
***----------------------------
***---- CREATE BY NESSMA ------
***----------------------------
SUBROUTINE SBD.EXCUTE.SAV.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] HASHING "$INCLUDE I_F.SCB.CALC.SAV.BAL.ALL" - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.CALC.SAV.BAL.ALL
*-----------------------------------
*Line [ 28 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    EXECUTE "SBR.CALC.SAV.BAL.NULL"
    EXECUTE "SBR.CALC.SAV.BAL.ZERO"
    EXECUTE "SBR.CALC.SAV.BAL.0.500"
    EXECUTE "SBR.CALC.SAV.BAL.500.1000"
    EXECUTE "SBR.CALC.SAV.BAL.1000.50000"
    EXECUTE "SBR.CALC.SAV.BAL.50000.100000"
    EXECUTE "SBR.CALC.SAV.BAL.100000.150000"
    EXECUTE "SBR.CALC.SAV.BAL.150000.250000"
    EXECUTE "SBR.CALC.SAV.BAL.250000.500000"
    EXECUTE "SBR.CALC.SAV.BAL.500000.1000000"
    EXECUTE "SBR.CALC.SAV.BAL.1000000"
    EXECUTE "SBR.CALC.SAV.BAL.TOTALS"

    TEXT = "�� �����  ��������" ; CALL REM
*-----------------------------------
RETURN
END
