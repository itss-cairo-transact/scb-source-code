* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-- CREATE BY NESSMA
    SUBROUTINE CNV.GET.INTER.BNK

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*----------------------------------------
    FN.CU =  "FBNK.CUSTOMER"  ; F.CU = ""
    CALL OPF(FN.CU, F.CU)

    N.SEL  = "SELECT FBNK.CUSTOMER WITH INTER.BNK.DATE GE 20140326"
    N.SEL := " AND SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300"
    N.SEL := " AND SECTOR NE 1400"
    N.SEL := " AND COMPANY.BOOK EQ ":O.DATA

    CALL EB.READLIST(N.SEL,KEY.LIST,'',SELECTED,ER.MSG)
    O.DATA = SELECTED

    RETURN
END
