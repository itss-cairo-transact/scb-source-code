* @ValidationCode : MjotMTY1NDg4NDM4NjpDcDEyNTI6MTY0MDYyMzEzOTE0NDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 18:38:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CNV.LIMIT.COLL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS

    XX = O.DATA

    FN.LIM = "FBNK.LIMIT"
    F.LIM  = ''
    R.LIM  = ''
    CALL OPF(FN.LIM,F.LIM)

    FN.COL = "F.COLLATERAL"
    F.COL  = ''
    R.COL  = ''
    COLL = ''
    CALL OPF(FN.COL,F.COL)

*****************************************************************

    CALL F.READ(FN.LIM,XX,R.LIM,F.LIM,ER)
*Line [ 47 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YY = DCOUNT(R.LIM<LI.COLLAT.RIGHT>,@VM)
    FOR H = 1 TO YY
        SS = R.LIM<LI.COLLAT.RIGHT,1,H>
        CL.SEL = "SELECT ":FN.COL:" WITH @ID LIKE ":SS:"..."
        CALL EB.READLIST(CL.SEL,KK,"",SELECTED,ERR)
        IF SELECTED THEN
            FOR AA = 1 TO SELECTED
                CALL F.READ(FN.COL,KK<AA>,R.COL,F.COL,EER)
                COLL += R.COL<COLL.LOCAL.REF><1,COLR.COLL.AMOUNT>
            NEXT AA
        END
    NEXT H
    O.DATA = COLL

RETURN
END
