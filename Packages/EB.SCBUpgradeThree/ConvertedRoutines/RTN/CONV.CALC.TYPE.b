* @ValidationCode : Mjo0MTYxOTc2MjU6Q3AxMjUyOjE2NDA2OTEyOTQyODk6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 13:34:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* BY NESSMA MOHAMMAD
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CALC.TYPE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_COMMON
    $INSERT I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_EQUATE
    $INSERT I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT TEMENOS.BP I_F.SCB.PASSWORD.UPD
    $INSERT I_F.SCB.PASSWORD.UPD
*-------------------------------------------
    R.PWR = ""   ; E11 = ""
    FN.PWR = "F.SCB.PASSWORD.UPD"  ; F.PWR = ""
    CALL OPF(FN.PWR , F.PWR)

    ID.TMP = O.DATA
    CALL F.READ(FN.PWR,ID.TMP, R.PWR, F.PWR, E11)
    RES    = R.PWR<PWR.USER.RESET>
    USR.ID = R.PWR<PWR.USER.ID>

    IF RES EQ '' THEN
        O.DATA = "A"
    END ELSE
        O.DATA = "B"
    END
*-------------------------------------------
RETURN
END
