* @ValidationCode : MjotMTMwNjk3MjQ1NDpDcDEyNTI6MTY0MDg3MjU0OTg0NDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 15:55:49
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*--- CREATE BY NESSMA MOHAMMED
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.VAL.DAT
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_COMMON
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_EQUATE
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.STMT.ENTRY
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.BILL.REGISTER
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_ENQUIRY.COMMON
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.FUNDS.TRANSFER
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.TELLER
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.LD.LOANS.AND.DEPOSITS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_LD.LOCAL.REFS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_BR.LOCAL.REFS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_FT.LOCAL.REFS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_TT.LOCAL.REFS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INSERT I_F.SCB.CUS.BR.V.DATE
*--------------------------------------------------------
    FN.CUS.V ='F.SCB.CUS.BR.V.DATE' ; F.CUS.V = ''
    CALL OPF(FN.CUS.V,F.CUS.V)

    CUS.BR     = O.DATA
    CALL F.READ(FN.CUS.V,CUS.BR, R.CUS.V, F.CUS.V, ETEXT.V)
    CUS.V.DATE = R.CUS.V<BR.V.NO.VALUE.DAY>

    NEW.VALUE.DATE = TODAY

    IF CUS.V.DATE EQ '' THEN
        CALL CDT('', NEW.VALUE.DATE , '+1W')
    END ELSE
        HH = '+':CUS.V.DATE:'W'
        CALL CDT('', NEW.VALUE.DATE , HH)
    END

    O.DATA = NEW.VALUE.DATE
*---------------------------------------------------------
RETURN
END
