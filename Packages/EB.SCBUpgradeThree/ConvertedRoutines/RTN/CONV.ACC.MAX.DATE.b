* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.ACC.MAX.DATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
    COMP = ID.COMPANY

*----------------------------------------------------------------------**
    ACCT.ID  = ''
    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC    = ''
    CALL OPF(FN.ACC,F.ACC)
    XX       = SPACE(130)
    ACCT.ID  = O.DATA
    MAX.DATE = ''
***--------------------------------------------------------------****
    CALL F.READ(FN.ACC,ACCT.ID,R.ACCT, F.ACC ,E11)
    CUST.DR  = R.ACCT<AC.DATE.LAST.DR.CUST>
    CUST.CR  = R.ACCT<AC.DATE.LAST.CR.CUST>
    BANK.DR  = R.ACCT<AC.DATE.LAST.DR.BANK>
    BANK.CR  = R.ACCT<AC.DATE.LAST.CR.BANK>
    AUTO.DR  = R.ACCT<AC.DATE.LAST.DR.AUTO>
    AUTO.CR  = R.ACCT<AC.DATE.LAST.CR.AUTO>

*    XX       = SPACE(130)
    XX<1,1>  = CUST.DR
    XX<1,2>  = CUST.CR
    XX<1,3>  = BANK.DR
    XX<1,4>  = BANK.CR
    XX<1,5>  = AUTO.DR
    XX<1,6>  = AUTO.CR
    MAX.DATE = CUST.DR
    FOR NN = 2 TO 6
        IF XX<1,NN> GT MAX.DATE THEN
            MAX.DATE = XX<1,NN>
        END
    NEXT NN
    O.DATA   = MAX.DATE
    RETURN
*==============================================================********
END
