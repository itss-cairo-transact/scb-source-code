* @ValidationCode : MjotMjExNzQzOTIyOkNwMTI1MjoxNjQwNjE1Nzg5OTg4OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 16:36:29
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*--------------------------NI7OOOOOOOOOOOOOOOO---------------------------------------------------
SUBROUTINE CNV.DEB
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RETIREMENTS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.HIS = ''
    CALL OPF(FN.FT.HIS,F.FT.HIS)

    XX = O.DATA
    IF O.DATA[1,2] = 'FT' THEN
        T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH @ID EQ ": XX
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            CALL F.READ(FN.FT,KEY.LIST,R.FT,F.FT,E2)
            ACC.DEB = R.FT<FT.DEBIT.ACCT.NO>
            ACC.DEB.NOTE = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
            ACC.CR.NOTE  = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>

            O.DATA = ACC.DEB
        END
        IF SELECTED = 0 THEN
            YY = O.DATA:';1'
            CALL F.READ(FN.FT.HIS,YY,R.FT.HIS,F.FT.HIS,E2)
            ACC.DEB.HIS = R.FT.HIS<FT.DEBIT.ACCT.NO>
            ACC.DEB.NOTE.HIS = R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
            ACC.CR.NOTE.HIS  = R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>

            O.DATA = ACC.DEB.HIS
        END
    END
RETURN
END
