* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.BASIC.INT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BASIC.INTEREST


    XX = ''
    XX = O.DATA
    IF O.DATA NE '' THEN
        BAS.LK   = XX:'EGP'
        BAS.RATE = ''
        FN.BAS  = 'FBNK.BASIC.INTEREST'  ; F.BAS  = '' ; R.BAS = ''
        CALL OPF(FN.BAS,F.BAS)
        BB.SEL  = "SELECT ":FN.BAS:" WITH @ID LIKE ":BAS.LK:"..."
        BB.SEL := " BY @ID"
        CALL EB.READLIST(BB.SEL,K.LIST,'',SELECTED,ER.MS)
        BAS.ID = K.LIST<SELECTED>

        CALL F.READ(FN.BAS,BAS.ID,R.BAS,F.BAS,EAA)
        BAS.RATE = R.BAS<EB.BIN.INTEREST.RATE>
        O.DATA = BAS.RATE
    END
    RETURN
