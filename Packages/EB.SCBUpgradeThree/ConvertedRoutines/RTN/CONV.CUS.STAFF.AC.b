* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CUS.STAFF.AC

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

**---------------------------------------------------------------------**
    CU = O.DATA
    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = '';F.CUS.AC=''
    FN.ACC ='FBNK.ACCOUNT' ;R.ACC = '';F.ACC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)
    CALL OPF(FN.ACC,F.ACC)


*************************************************************
    CALL F.READ(FN.CUS.AC,CU, R.CUS.AC, F.CUS.AC,ETEXT1)
    LOOP

        REMOVE ACC FROM R.CUS.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CURR    = R.ACC<AC.CURRENCY>
        CATEGORY   = R.ACC<AC.CATEGORY>
        AMT.CUR    = R.ACC<AC.OPEN.ACTUAL.BAL>

        IF (((CATEGORY GE 1001 AND CATEGORY LE 1003) OR (CATEGORY GE 1201 AND CATEGORY LE 1208)OR (CATEGORY GE 1300 AND CATEGORY LE 1599) OR (CATEGORY GE 1101 AND CATEGORY LE 1212)OR (CATEGORY GE 1401 AND CATEGORY LE 1437) OR CATEGORY EQ 1290 ) AND AC.CURR EQ 'EGP' AND AMT.CUR GT 0)  THEN
            AMT.CUR1     += AMT.CUR

        END
    REPEAT
    O.DATA = AMT.CUR1
**--------------------------------------------------------------------**

    RETURN
END
