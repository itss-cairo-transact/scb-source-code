* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------*
*-------CREATED BY RIHAM YOUSSEF---2020/11/16-------*
*    PROGRAM COM.CR.ADD
    SUBROUTINE  COM.CR.ADD
*-----------------------------------------*
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COM.CR
******************************************
    GOSUB CHECKLIST
    IF SW = 0  THEN
        GOSUB BUILD.RECORD
    END
    RETURN

*-----------
CHECKLIST:
*---------
    F.POSS    = "F.SCB.COM.CR" ;  FVAR.POSS = '' ; R.POSS = ''
    CALL OPF(F.POSS,FVAR.POSS)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''  ;R.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    T.DATE    = TODAY
    KEY.LIST = "" ; SELECTED = "" ;  ER.FT = "" ; SW = 0
    T.SEL = "SELECT F.SCB.COM.CR WITH DATE.LOAD EQ ":T.DATE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)

    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS RUN BEFORE  " ; CALL REM
        SW = 1
    END ELSE
        SW = 0
    END
    RETURN
*-----------
BUILD.RECORD:
*---------------

    Path.1 = "COM.CR/COM.CR.csv"

    OPENSEQ Path.1 TO MyPath.1 ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    R.LINE = '' ; R.POSS = ''; T.SEL='';KEY.LIST=''; SELECTED=0
    UPDATE.FLAG = ''; ERR=''; ER=''; L.LIST=''; SELECT.NO=0; Y.SEL=''
    ZEROS=''; NO.OF.RECORDS=0; NO.OF.REC=0; Z=''



    EOF = ''
    I   = 1
    POSS.ID = ''
*****************************************************************


    LOOP WHILE NOT(EOF)
        READSEQ R.LINE FROM MyPath.1 THEN

            CUST.CODE    = TRIM(FIELD(R.LINE,",",1))
            R.POSS<AB.CUST.CODE>    = CUST.CODE


            NAME = TRIM(FIELD(R.LINE,",",2))[1,35]
            R.POSS<AB.CUST.NAME>    = NAME

            DATE.NO = TRIM(FIELD(R.LINE,",",3))

            R.POSS<AB.DATE.LOAD>    = DATE.NO

            FINAL.BAL = TRIM(FIELD(R.LINE,",",4))
            R.POSS<AB.FINAL.BAL>      =  FINAL.BAL

            VALUE = TRIM(FIELD(R.LINE,",",5))
            R.POSS<AB.PERCENT>          = VALUE

            TOTAL.BAL = TRIM(FIELD(R.LINE,",",6))
            R.POSS<AB.TOTAL.BAL>      = TOTAL.BAL


            TAX = TRIM(FIELD(R.LINE,",",7))
            R.POSS<AB.TAX>            = TAX

            COMMISSION = TRIM(FIELD(R.LINE,",",8))
            R.POSS<AB.COMMISSION>     = COMMISSION

            NET.BAL   = TRIM(FIELD(R.LINE,",",9))
            R.POSS<AB.NET.BAL>        = NET.BAL
*--------------------------------------------
            ACCOUNT.NO.EQ = TRIM(FIELD(R.LINE,",",10))
            ACCT.NO = LEN(ACCOUNT.NO.EQ)
            IF ACCT.NO LT '16' THEN
                ACCOUNT.NO = '0' : ACCOUNT.NO.EQ
            END ELSE
                ACCOUNT.NO= ACCOUNT.NO.EQ
            END

            CALL F.READ(FN.ACCT,ACCOUNT.NO,R.ACCT,F.ACCT,E1)
            IF NOT(E1) THEN
                R.POSS<AB.ACCOUNT.NO>        = ACCOUNT.NO
            END ELSE
                R.POSS<AB.WRONG.ACCT.NO>     = ACCOUNT.NO
            END

            PAPER.CODE   =  TRIM(FIELD(R.LINE,",",11))
            R.POSS<AB.PAPER.CODE>      = PAPER.CODE

            CHEQUE =TRIM(FIELD(R.LINE,",",12))
            R.POSS<AB.CHEQUE>           = CHEQUE


            POSS.ID   = CUST.CODE:'-':PAPER.CODE:'-'::DATE.NO
            CALL F.WRITE(F.POSS,POSS.ID,R.POSS)
            CALL JOURNAL.UPDATE(POSS.ID)
            R.POSS<AB.ACCOUNT.NO> = ''
            R.POSS<AB.WRONG.ACCT.NO> = ''
        END ELSE
            EOF = 1
        END

    REPEAT
    CLOSESEQ MyPath.1
    RETURN

END
