* @ValidationCode : MjoyMTAzNTY2NzM6Q3AxMjUyOjE2NDE3MzUwMDUwNjc6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:30:05
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*****MADE BY NESSMA @ 6/2/2010 ***************
****************************************************
PROGRAM COB.PROTOCOL
*****    SUBROUTINE COB.PROTOCOL
****************************************************
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.PROTOCOL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.PROTOCOL
**************************************************
* DEBUG
**************************************************
    COUNT.N  = 0
    OLD.TIME = ''
    NEW.TIME = ''
    KEY.LIST = ""                  ; SELECTED = "" ; ER.MSG  = ""
    FN.PWR   = 'F.PROTOCOL'        ; F.PWR    = '' ; R.PWR   = '' ; RETRY1   = '' ; E1   = ''
    FN.PWR.H = 'F.SCB.PROTOCOL'    ; F.PWR.H  = '' ; R.PWR.H = '' ; RETRY1.H = '' ; E1.H = ''
    CALL OPF(FN.PWR,F.PWR)
    CALL OPF(FN.PWR.H,F.PWR.H)
**************************************************
    T.SEL = "SELECT F.PROTOCOL"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
**PRINT "SELECTED  = ":SELECTED
    FOR NN = 1 TO SELECTED
        CALL F.READ(FN.PWR,KEY.LIST<NN>, R.PWR, F.PWR, E1)
        ID.PWR.H = KEY.LIST<NN>
        TOD.DATE = TODAY
        CALL F.READ(FN.PWR.H,ID.PWR.H , R.PWR.H, F.PWR.H, E11)
        IF E11 THEN
            R.PWR.H<PRTC.PROCESS.DATE>      = R.PWR<EB.PTL.PROCESS.DATE>
            R.PWR.H<PRTC.VERSION>           = R.PWR<EB.PTL.DATE.VERSION>
*OLD.TIME = R.PWR<EB.PTL.TIME>
*NEW.TIME = OLD.TIME[1,2] : ":" : OLD.TIME[3,2] : ":" : OLD.TIME[2]
            R.PWR.H<PRTC.TIME>              = R.PWR<EB.PTL.TIME>
            R.PWR.H<PRTC.TERMINAL.ID>       = R.PWR<EB.PTL.TERMINAL.ID>
            R.PWR.H<PRTC.PHANTOM.ID>        = R.PWR<EB.PTL.PHANTOM.ID>
            R.PWR.H<PRTC.COMPANY.ID>        = R.PWR<EB.PTL.COMPANY.ID>
            R.PWR.H<PRTC.USER>              = R.PWR<EB.PTL.USER>
            R.PWR.H<PRTC.APPLICATION>       = R.PWR<EB.PTL.APPLICATION>
            R.PWR.H<PRTC.LEVEL.FUNCTION>    = R.PWR<EB.PTL.LEVEL.FUNCTION>
            R.PWR.H<PRTC.ID>                = R.PWR<EB.PTL.ID>
            R.PWR.H<PRTC.REMARK>            = R.PWR<EB.PTL.REMARK>
            R.PWR.H<PRTC.CLIENT.IP.ADDRESS> = R.PWR<EB.PTL.CLIENT.IP.ADDRESS>
*---------------------------------------------------------------------
            R.PWR.H<PRTC.BOOKING.DATE>      = TOD.DATE
*---------------------------------------------------------------------
*Line [ 68 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.RECORD.STATUS>     = R.PWR<EB.PTL.RECORD.STATUS>
            R.PWR.H<PRTC.RECORD.STATUS>     = ''
*Line [ 71 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.CURR.NO>           = R.PWR<EB.PTL.CURR.NO>
            R.PWR.H<PRTC.CURR.NO>           = ''
*Line [ 74 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.INPUTTER>          = R.PWR<EB.PTL.INPUTTER>
            R.PWR.H<PRTC.INPUTTER>          = ''
*Line [ 77 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.DATE.TIME>         = R.PWR<EB.PTL.DATE.TIME>
            R.PWR.H<PRTC.DATE.TIME>         = ''
*Line [ 80 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.AUTHORISER>        = R.PWR<EB.PTL.AUTHORISER>
            R.PWR.H<PRTC.AUTHORISER>        = ''
*Line [ 83 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.CO.CODE>           = R.PWR<EB.PTL.CO.CODE>
            R.PWR.H<PRTC.CO.CODE>           = ''
*Line [ 86 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.DEPT.CODE>         = R.PWR<EB.PTL.DEPT.CODE>
            R.PWR.H<PRTC.DEPT.CODE>         = ''
*Line [ 89 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.AUDITOR.CODE>      = R.PWR<EB.PTL.AUDITOR.CODE>
            R.PWR.H<PRTC.AUDITOR.CODE>      = ''
*Line [ 92 ] PROTOCOL is a life table, doesn't have audit fields - 2021-12-26
*R.PWR.H<PRTC.AUDIT.DATE.TIME>   = R.PWR<EB.PTL.AUDIT.DATE.TIME>
            R.PWR.H<PRTC.AUDIT.DATE.TIME>   = ''
*---------------------------------------------------------------------
* WRITE  R.PWR.H TO F.PWR.H , ID.PWR.H ON ERROR
****PRINT "CAN NOT WRITE RECORD":ID.PWR.H:"TO" :FN.PWR.H
* END

            CALL F.WRITE(FN.PWR.H,ID.PWR.H,R.PWR.H)
            CALL JOURNAL.UPDATE(ID.PWR.H)

            COUNT.N   =   COUNT.N    +    1
        END
    NEXT NN
***PRINT "MOVED  = ":COUNT.N
RETURN
END
