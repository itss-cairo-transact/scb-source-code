* @ValidationCode : MjoyMjgyOTYyMzE6Q3AxMjUyOjE2NDA2OTQ0ODIyNzI6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:28:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CONTENT.COUNT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DRMNT.FILES
*--------------------------------------------------------------------------
    FN.CA = 'F.SCB.DRMNT.FILES' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    CALL F.READ(FN.CA,O.DATA,R.CA,F.CA,E1)

*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    WS.FILE.DET.COUNT = DCOUNT(R.CA<SDF.FILE.CONTENT>,@VM)
    O.DATA    = WS.FILE.DET.COUNT

RETURN
END
