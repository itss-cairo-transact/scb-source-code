* @ValidationCode : MjotMTk3MDg4NzQ3ODpDcDEyNTI6MTY0NDg1MTA1MDQ3MTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 14 Feb 2022 17:04:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
***************************NI7OOOOOOOOOOO***********************
SUBROUTINE CNV.TF.AMT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER.SIGN.ON.NAME
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BT.BATCH
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 47 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 49 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 51 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LC.ACCOUNT.BALANCES


    FN.ACC = 'FBNK.LC.ACCOUNT.BALANCES' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
**TEXT = "O.DATA : " : O.DATA ; CALL REM
    CALL F.READ(FN.ACC,O.DATA,R.ACC,F.ACC,E1)

*Line [ 61 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DECOUNT.CHARGE = DCOUNT(R.ACC<LCAC.CHRG.CODE>,@VM)
**TEXT = "DEC : " : DECOUNT.CHARGE ; CALL REM
    FOR I = 1 TO DECOUNT.CHARGE
        CHARGE.CODE      = R.ACC<LCAC.CHRG.CODE><1,I>
        CHARGE.AMOUNT    = R.ACC<LCAC.AMT.REC><1,I>
        CHARGE.CURR      = R.ACC<LCAC.CHRG.CCY><1,I>
*Line [ 68 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('FT.CHARGE.TYPE':@FM:FT5.DESCRIPTION,CHARGE.CODE,CHARGE.NAME)
        F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
        FN.F.ITSS.FT.CHARGE.TYPE = ''
        CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
        CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHARGE.CODE,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
        CHARGE.NAME=R.ITSS.FT.CHARGE.TYPE<FT5.DESCRIPTION>
        IF ETEXT THEN
**TEXT = "ETEXT = " : ETEXT ; CALL REM
*Line [ 77 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('FT.COMMISSION.TYPE':@FM:FT4.SHORT.DESCR,CHARGE.CODE,CHARGE.NAME)
            F.ITSS.FT.COMMISSION.TYPE = 'F.FT.COMMISSION.TYPE'
            FN.F.ITSS.FT.COMMISSION.TYPE = ''
            CALL OPF(F.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE)
            CALL F.READ(F.ITSS.FT.COMMISSION.TYPE,CHARGE.CODE,R.ITSS.FT.COMMISSION.TYPE,FN.F.ITSS.FT.COMMISSION.TYPE,ERROR.FT.COMMISSION.TYPE)
            CHARGE.NAME=R.ITSS.FT.COMMISSION.TYPE<FT4.SHORT.DESCR>
        END
*Line [ 74 ] Update DBR - ITSS - R21 Upgrade - 2021-12-26
*CALL DBR ('CURRENCY':@FM:EB.CUR.CCY.NAME<2,2>,CHARGE.CURR,CUR)
*Line [ 87 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR (('CURRENCY':@FM:EB.CUR.CCY.NAME)<2,2>,CHARGE.CURR,CUR)
        F.ITSS.CURRENCY = 'F.CURRENCY'
        FN.F.ITSS.CURRENCY = ''
        CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
        CALL F.READ(F.ITSS.CURRENCY,CHARGE.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
        CUR=R.ITSS.CURRENCY<EB.CUR.CCY.NAME,2>
        TOT +=   CHARGE.AMOUNT
    NEXT I
    O.DATA = TOT
RETURN
END
