* @ValidationCode : MjotMzYxMDgxMjE6Q3AxMjUyOjE2NDA2MTQzMTg0NjI6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Dec 2021 16:11:58
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*************************NI7OOOOOOOOOOOOO**************
SUBROUTINE CNV.CUS.THIRD

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 28 ] Hashing $INCLUDE I_F.SCB.RETIREMENTS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS

    XX = O.DATA
    TEXT = "XX:": XX ; CALL REM

    FN.LD    ='FBNK.LD.LOANS.AND.DEPOSITS'     ; F.LD = ''
    FN.LD.HIS='FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LD.HIS = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.HIS,F.LD.HIS)

    CALL F.READ(FN.LD,XX,R.LD,F.LD,E2)
    CUS.ID    = R.LD<LD.CUSTOMER.ID>
    CUS.THIRD = R.LD<LD.LOCAL.REF><1,LDLR.THIRD.NUMBER>
    TEXT = "CUS.ID :": CUS.ID ; CALL REM
    TEXT = "CUS.THIRD :": CUS.THIRD ; CALL REM
    IF CUS.ID = CUS.THIRD THEN
        O.DATA = CUS.ID
    END ELSE
        O.DATA = CUS.THIRD
    END

RETURN
END
