* @ValidationCode : MjotMTE5MzYwOTM0NzpDcDEyNTI6MTY0MDYxMzkwNjY1ODpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 16:05:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
********************************MAHMOUD 30/9/2014*******************************
*-----------------------------------------------------------------------------
* <Rating>289</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.CU.LD.AMT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS

    CU.ID = ''
    CU.ID = O.DATA
    LD.AMT = 0
    SCCD.GRP   = ' 21101 21102 21103 '

    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD = '' ; ERR.LD = ''
    CALL OPF(FN.LD,F.LD)

    CALL F.READ(FN.IND.LD,CU.ID,R.IND.LD,F.IND.LD,ER.IND.LD)
    LOOP
        REMOVE LD.ID FROM R.IND.LD SETTING POSS.LD.1
    WHILE LD.ID:POSS.LD.1
        CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,ERR.LD)
        LD.CAT  = R.LD<LD.CATEGORY>

        FINDSTR LD.CAT:" " IN SCCD.GRP SETTING POS.SCCD THEN NULL ELSE GOTO NXT.REC
        LD.AMT  += R.LD<LD.AMOUNT>
    
NXT.REC:
    REPEAT

    O.DATA = LD.AMT
RETURN
END
