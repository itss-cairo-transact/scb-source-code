* @ValidationCode : MjoxMTM3MjU2NDI2OkNwMTI1MjoxNjQwNjkxMDUzMDM0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 13:30:53
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* BY NESSMA MOHAMMAD
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CALC.DEPT
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_COMMON
    $INSERT I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_EQUATE
    $INSERT I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.USER
    $INSERT I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INSERT TEMENOS.BP I_F.USR.LOCAL.REF
    $INSERT I_F.USR.LOCAL.REF
*-------------------------------------------
    R.PWR  = ""   ; E11 = ""
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF(FN.USR, F.USR)

    USR.ID = FIELD(O.DATA,'_',2)

    CALL F.READ(FN.USR,USR.ID, R.USR, F.USR, E11)
    DEPT.VAL = R.USR<EB.USE.LOCAL.REF><USER.SCB.DEPT.CODE>

*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DEPT.ACCT.OFFICER':@FM:2,DEPT.VAL,DEPT.COMP)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,DEPT.VAL,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
DEPT.COMP=R.ITSS.DEPT.ACCT.OFFICER<2>
    O.DATA   = DEPT.COMP
*-------------------------------------------
RETURN
END
