* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CU.USER
*    PROGRAM CONV.CU.USER

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
**********************************
*DEBUG
    WS.CU.ID = O.DATA
*    WS.CU.ID = '13100926;12'
*TEXT = WS.CU.ID ; CALL REM
    IF WS.CU.ID EQ '' THEN
        RETURN
    END

    FN.CUHIS  = "FBNK.CUSTOMER$HIS" ; F.CUHIS   = "" ; R.CUHIS  = ""
    CALL OPF(FN.CUHIS,F.CUHIS)

    FN.USR  = "F.USER" ; F.USR   = "" ; R.USR  = ""
    CALL OPF(FN.USR,F.USR)

    FN.CU  = "FBNK.CUSTOMER" ; F.CU   = "" ; R.CU  = ""
    CALL OPF(FN.CU,F.CU)

    CALL F.READ(FN.CUHIS,WS.CU.ID,R.CUHIS,F.CUHIS,ER.CUHIS)
    IF ER.CUHIS THEN
        WS.CU.ID = FIELD(WS.CU.ID, ";", 1)
        CALL F.READ(FN.CU,WS.CU.ID,R.CU,F.CU,ER.CU)
        WS.INP = FIELD(R.CU<EB.CUS.INPUTTER><1,1>,"_",2)
        CALL F.READ(FN.USR,WS.INP,R.USR,F.USR,ER.USRI)
        IF NOT(ER.USRI) THEN
            WS.INP = R.USR<EB.USE.USER.NAME>
        END
        WS.ATH = FIELD(R.CU<EB.CUS.AUTHORISER>,"_",2)
        CALL F.READ(FN.USR,WS.ATH,R.USR,F.USR,ER.USRA)
        IF NOT(ER.USRA) THEN
            WS.ATH = R.USR<EB.USE.USER.NAME>
        END
        O.DATA = WS.INP:'*':WS.ATH
        RETURN
    END
    WS.INP = FIELD(R.CUHIS<EB.CUS.INPUTTER><1,1>,"_",2)
    CALL F.READ(FN.USR,WS.INP,R.USR,F.USR,ER.USRI)
    IF NOT(ER.USRI) THEN
        WS.INP = R.USR<EB.USE.USER.NAME>
    END
    WS.ATH = FIELD(R.CUHIS<EB.CUS.AUTHORISER>,"_",2)
    CALL F.READ(FN.USR,WS.ATH,R.USR,F.USR,ER.USRA)
    IF NOT(ER.USRA) THEN
        WS.ATH = R.USR<EB.USE.USER.NAME>
    END
    O.DATA = WS.INP:'*':WS.ATH
    RETURN
