* @ValidationCode : MjotMTcyNjIyMDkyOTpDcDEyNTI6MTY0MTY0NjQ2MTU4NzpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 08 Jan 2022 14:54:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CONV.ATM.STMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*    $INCLUDE T24.BP I_F.ACCT.STMT.PRINT
*    $INCLUDE T24.BP I_F.STMT.PRINTED
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TRANSACTION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_AC.LOCAL.REFS

*-------------------------------------------------------------------------------------------------*
    FN.ACC   = 'FBNK.ACCOUNT'
    F.ACC    = ''        ; R.ACC   = '' ; READ.ERR   = '' ; RETRY   = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.ACCT  = 'FBNK.ACCT.STMT.PRINT'
    F.ACCT   = '' ; R.ACCT         = '' ; READ.ERR.T = '' ; RETRY.T = ''
    CALL OPF(FN.ACCT,F.ACCT)

    FN.ACC.P = 'FBNK.STMT.PRINTED'
    F.ACC.P  = ''        ; R.ACC.P = '' ; READ.ERR.P = '' ; RETRY.P = ''
    CALL OPF(FN.ACC.P,F.ACC.P)

    FN.STMT = 'FBNK.STMT.ENTRY'
    F.STMT  = ''        ; R.STMT = '' ; READ.ERR.ST = '' ; RETRY.ST = ''
    CALL OPF(FN.STMT,F.STMT)

    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""

*-------------------------------------------------------------------------------------------------*
    ACCT.ID  = O.DATA
*Line [ 56 ] The variable 'STMT.NO' initialised - 2021-12-26
    STMT.NO = ''
*Line [ 58 ] The variable 'STMT.NO.P' initialised - 2021-12-26
    STMT.NO.P = ''
*Line [ 60 ] The variable 'STMT.NO.P1 ' initialised - 2021-12-26
    STMT.NO.P1 = ''

    CALL F.READ(FN.ACCT,ACCT.ID,R.ACCT,F.ACCT,READ.ERR.T)
    LOOP
        REMOVE ACCT.ID  FROM R.ACCT SETTING POS
    WHILE ACCT.ID:POS
        STMT.NO ++
    REPEAT
    ACC.P    = R.ACCT<STMT.NO>
    ACC.P.ID = O.DATA:"-":FIELD(ACC.P,'/',1)
    CALL F.READ(FN.ACC.P,ACC.P.ID,R.ACC.P,F.ACC.P,READ.ERR.P)

    LOOP
        REMOVE ACC.P.ID  FROM R.ACC.P SETTING POS.P
    WHILE ACC.P.ID:POS.P
        STMT.NO.P ++
    REPEAT

    IF STMT.NO.P LT 5 THEN

        ACC.P1     = R.ACCT<STMT.NO -1>
        ACC.P.ID1  = O.DATA:"-":FIELD(ACC.P1,'/',1)
        CALL F.READ(FN.ACC.P,ACC.P.ID1,R.ACC.P1,F.ACC.P,READ.ERR.P)
        ST.1 = ( 5 - STMT.NO.P ) + 1

        LOOP
            REMOVE ACC.P.ID1  FROM R.ACC.P1 SETTING POS.P1
        WHILE ACC.P.ID1:POS.P1
            STMT.NO.P1 ++
        REPEAT
        ST.2 =  STMT.NO.P1 - ST.1

        FOR I = ST.2 TO STMT.NO.P1
            CALL F.READ(FN.STMT,R.ACC.P1<I>,R.STMT,F.STMT,READ.ERR.ST)
            TRNS.COD = R.STMT<AC.STE.TRANSACTION.CODE>
*Line [ 96 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,TRNS.COD,T.DESC)
F.ITSS.TRANSACTION = 'F.TRANSACTION'
FN.F.ITSS.TRANSACTION = ''
CALL OPF(F.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION)
CALL F.READ(F.ITSS.TRANSACTION,TRNS.COD,R.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION,ERROR.TRANSACTION)
T.DESC=R.ITSS.TRANSACTION<AC.TRA.NARRATIVE>
            DESC = T.DESC
            HH1  = ",":R.STMT<AC.STE.AMOUNT.LCY>:"/":R.STMT<AC.STE.BOOKING.DATE>:"/":DESC
            HH2   = HH1 : HH2
        NEXT I

        FOR II = 1 TO STMT.NO.P
            CALL F.READ(FN.STMT,R.ACC.P<II>,R.STMT,F.STMT,READ.ERR.ST)
            TRNS.COD = R.STMT<AC.STE.TRANSACTION.CODE>
*Line [ 111 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,TRNS.COD,T.DESC)
F.ITSS.TRANSACTION = 'F.TRANSACTION'
FN.F.ITSS.TRANSACTION = ''
CALL OPF(F.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION)
CALL F.READ(F.ITSS.TRANSACTION,TRNS.COD,R.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION,ERROR.TRANSACTION)
T.DESC=R.ITSS.TRANSACTION<AC.TRA.NARRATIVE>
            DESC = T.DESC
            HHH  = ",":R.STMT<AC.STE.AMOUNT.LCY>:"/":R.STMT<AC.STE.BOOKING.DATE>:"/":DESC
            HH   = HHH : HH
            TEXT = HH ; CALL REM
        NEXT II
        STMT.T  = HH2 : HH
*---------------------------------------------------------------------*
    END ELSE
        IF STMT.NO.P = 5 THEN
            H  = 1
        END ELSE
            H = (STMT.NO.P - 5 ) + 1
        END
        FOR I = H TO STMT.NO.P
            CALL F.READ(FN.STMT,R.ACC.P<I>,R.STMT,F.STMT,READ.ERR.ST)
            TRNS.COD = R.STMT<AC.STE.TRANSACTION.CODE>
*Line [ 134 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,TRNS.COD,T.DESC)
F.ITSS.TRANSACTION = 'F.TRANSACTION'
FN.F.ITSS.TRANSACTION = ''
CALL OPF(F.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION)
CALL F.READ(F.ITSS.TRANSACTION,TRNS.COD,R.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION,ERROR.TRANSACTION)
T.DESC=R.ITSS.TRANSACTION<AC.TRA.NARRATIVE>
            DESC = T.DESC
            HHH  = ",":R.STMT<AC.STE.AMOUNT.LCY>:"/":R.STMT<AC.STE.BOOKING.DATE>:"/":DESC
            HH   = HHH : HH
        NEXT I
        STMT.T = HH
    END

    O.DATA = STMT.T

RETURN
END
