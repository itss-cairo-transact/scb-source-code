* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CUS.NO.ALL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS


    C.CODE = O.DATA

    FN.LD  = "FBNK.LD.LOANS.AND.DEPOSITS"            ; F.LD   = ""
    CALL OPF (FN.LD,F.LD)

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21101 AND CATEGORY LE 21103) AND STATUS EQ 'CUR' AND CUSTOMER.ID NE 90000009 BY CUSTOMER.ID "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CUS.NO = 0
        FOR I = 1  TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,ER.LD)
            CALL F.READ(FN.LD,KEY.LIST<I+1>,R.LD1,F.LD,ER.LD)
*****************
            CUS.ID       = R.LD<LD.CUSTOMER.ID>
            CUS.ID1      = R.LD1<LD.CUSTOMER.ID>
            IF  CUS.ID NE CUS.ID1 THEN
                CUS.NO =  CUS.NO+1
            END
**************
        NEXT I
    END
*************************************
    *T.SEL1 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH (CATEGORY GE 21101 AND CATEGORY LE 21103) AND STATUS EQ 'CUR' AND CUSTOMER.ID EQ 90000009 "
    *CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
*************************************
    *CUS.ID.NONE = SELECTED1

   * O.DATA      = CUS.ID.NONE +   CUS.NO
   O.DATA      =  CUS.NO

    RETURN
END
