* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>80</Rating>
*-----------------------------------------------------------------------------
*** CREATED BY MOHAMMED SABRY 2010/08/09 ***
********************************************
    SUBROUTINE CONV.AC.SUS.BAL

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    FN.CU.AC='FBNK.CUSTOMER.ACCOUNT' ; F.CUS.AC=''
    FN.AC='FBNK.ACCOUNT' ; F.AC=''
    CALL OPF( FN.AC,F.AC)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY  = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)

    WS.CATEGORY      = 0
    WS.DE.SUS.BAL    = 0
    WS.CURRENCY      = ''
    WS.OPEN.ACT.BAL  = 0
    WS.SUS.AMT.NO    = 0
    WS.SUS.DATE      = ''
    WS.TOTAL.CUS.BAL = 0
    WS.ACCOUNT.ID = O.DATA

    GOSUB GET.ACCOUNT.BAL

    RETURN
*---------------------------------------------------------------------------
GET.ACCOUNT.BAL:

    CALL F.READ(FN.AC,WS.ACCOUNT.ID,R.AC, F.AC,AC.ERR)

    WS.CATEGORY = R.AC<AC.CATEGORY>
    WS.CURRENCY = R.AC<AC.CURRENCY>

    GOSUB GET.NZD.RATE

    IF WS.CATEGORY EQ 1710 OR WS.CATEGORY EQ 1711 THEN
        WS.OPEN.ACT.BAL   = R.AC<AC.OPEN.ACTUAL.BAL>
        WS.TOTAL.CUS.BAL += WS.OPEN.ACT.BAL * WS.RATE
    END

    IF R.AC<AC.ACCR.DR.SUSP> # '' THEN
        WS.SUS.AMT.NO = DCOUNT(R.AC<AC.ACCR.DR.SUSP>,@VM)
        FOR I = 1 TO WS.SUS.AMT.NO
            WS.DE.SUS.BAL = R.AC<AC.ACCR.DR.SUSP,I>
            WS.TOTAL.CUS.BAL += WS.DE.SUS.BAL * WS.RATE
        NEXT I
    END
    O.DATA = WS.TOTAL.CUS.BAL
    RETURN
*------------------------------------------------------------------------------------------------------------*
GET.NZD.RATE:
    WS.RATE = 0
    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 85 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE WS.CURRENCY IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    WS.RATE = R.CCY<RE.BCP.RATE,POS>

    RETURN
*------------------------------------------------------------------------------------------------------------*
