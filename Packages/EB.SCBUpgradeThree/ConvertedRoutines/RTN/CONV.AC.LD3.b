* @ValidationCode : MjoxNDQyNTk1NTIwOkNwMTI1MjoxNjQxNzM1NTg0Nzg0OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:39:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CONV.AC.LD3
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
************************************************************
    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
RETURN
************************************************************
INIT:
    XX = O.DATA
    AMT = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LD = ''
    R.LD = ''
RETURN
************************************************************
OPEN:
    CALL OPF(FN.LD,F.LD)
RETURN
PROCESS:
************************************************************
    T.SEL = "SELECT ":FN.LD: " WITH CUSTOMER.ID EQ ":XX:" AND CATEGORY GE 21000 AND CATEGORY LE 21010 AND CURRENCY NE 'EGP'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,EER)
            AMT += R.LD<LD.AMOUNT>
        NEXT I
    END
    O.DATA = AMT
RETURN
END
