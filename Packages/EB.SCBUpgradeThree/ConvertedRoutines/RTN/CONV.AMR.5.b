* @ValidationCode : MjotODQyOTkwNzg5OkNwMTI1MjoxNjQxNzM1ODY4NTkyOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:44:28
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CONV.AMR.5

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CBE.MAST.AC.LD
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM

    XX = O.DATA ; TOT.MAR = '' ; TOT.DEF = '' ; TOT = ''

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'
    F.CCY = '' ; R.CCY = ''
    KEY.CCY = 'NZD'
    CALL OPF(FN.CCY,F.CCY)
************************************************************


    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = ''
    CALL OPF(FN.LD,F.LD)


    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""

    CALL F.READ(FN.LD,XX,R.LD,F.LD,E1)
    CUR = R.LD<LD.CURRENCY>

    REF1 = R.LD<LD.LIMIT.REFERENCE>
    REF = REF1[1,4]
    PROD = R.LD<LD.LOCAL.REF><1,LDLR.PRODUCT.TYPE>
    AMT  = R.LD<LD.AMOUNT>
    MARG = R.LD<LD.LOCAL.REF><1,LDLR.MARGIN.AMT>

    CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 64 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    RATE = R.CCY<RE.BCP.RATE,POS>


    IF PROD EQ 'FINAL' OR PROD EQ 'ADVANCE' OR PROD EQ 'ADVANCED' THEN
        IF REF EQ '5740' THEN
            TOT.MAR = AMT * RATE
        END
    END


    TOT = TOT.MAR + TOT.DEF
    O.DATA = TOT


RETURN
END
