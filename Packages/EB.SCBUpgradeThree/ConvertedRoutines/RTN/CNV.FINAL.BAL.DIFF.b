* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.FINAL.BAL.DIFF

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.DIFF
*****
    XX1 = O.DATA
    ACCT.ID   = FIELD(XX1,'*',1)
    AC.ST.BAL = FIELD(XX1,'*',2)
    END.P.BAL = FIELD(XX1,'*',3)

    PATHNAME1 = "STMT.DIFF"
    FILENAME1 = ACCT.ID
    TTDD11 = TODAY

    CALL CDT("",TTDD11,'-1W')
*------------------------------------------------------------
    ST.DIFF.ID = ACCT.ID:".":TTDD11
    FN.ST.DIFF = "F.SCB.STMT.DIFF" ; F.ST.DIFF = "" ; R.ST.DIFF = "" ; CALL OPF(FN.ST.DIFF,F.ST.DIFF)

    FN.AC  = "FBNK.ACCOUNT" ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)

    CALL F.READ( FN.AC,ACCT.ID, R.AC, F.AC, ETEXT)
    ACCT.BAL = R.AC<AC.OPEN.ACTUAL.BAL>
    ACCT.COM = R.AC<AC.CO.CODE>

    IF (ACCT.BAL NE AC.ST.BAL) OR (ACCT.BAL NE END.P.BAL) THEN
        GOSUB SV.REC
    END

    RETURN
******************************************************************
SV.REC:
*------
    OPENSEQ PATHNAME1 , FILENAME1 TO BB1 ELSE
        CREATE BB1 THEN
            PRINT "FILE ":FILENAME1:" CREATED IN ":PATHNAME1
        END ELSE
            STOP "Cannot create ":FILENAME1:" File IN ":PATHNAME1
        END
    END

    XX.DATA  = ACCT.ID:"|"
    XX.DATA := ACCT.COM:"|"
    XX.DATA := TTDD11:"|"
    XX.DATA := ACCT.BAL:"|"
    XX.DATA := AC.ST.BAL:"|"
    XX.DATA := END.P.BAL:"|"

    WRITESEQ XX.DATA TO BB1 ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
**********************************************************
WRITE.REC:
    CALL F.READ(FN.ST.DIFF,ST.DIFF.ID,R.ST.DIFF,F.ST.DIFF,ER.DF)
    R.ST.DIFF<DIF.ACCOUNT.NUMBER>  = ACCT.ID
    R.ST.DIFF<DIF.ACCOUNT.COMPANY> = ACCT.COM
    R.ST.DIFF<DIF.STMT.DATE>       = TTDD11
    R.ST.DIFF<DIF.ACCOUNT.BALANCE> = ACCT.BAL
    R.ST.DIFF<DIF.ACCT.STMT.BAL>   = AC.ST.BAL
    R.ST.DIFF<DIF.END.PERIOD.BAL>  = END.P.BAL

*WRITE R.ST.DIFF TO F.ST.DIFF , ST.DIFF.ID ON ERROR
*    PRINT "CAN NOT WRITE RECORD ":ST.DIFF.ID:" TO ":FN.ST.DIFF
*END

    CALL F.WRITE(FN.ST.DIFF,ST.DIFF.ID,R.ST)
    CALL JOURNAL.UPDATE(ST.DIFF.ID)
    RETURN
*------------------------------------------------------
END
