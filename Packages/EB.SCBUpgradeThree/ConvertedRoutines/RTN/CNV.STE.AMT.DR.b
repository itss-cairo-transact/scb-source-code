* @ValidationCode : MjotMTI0OTMyMjk4MzpDcDEyNTI6MTY0MTczNDA0MjI1ODpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:14:02
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CNV.STE.AMT.DR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCT.ENT.TODAY

    XX = O.DATA
    AMT     = 0
    AMT.DR  = 0
    AMT.CR  = 0

    FN.STE = "F.STMT.ENTRY"
    F.STE  = ''
    R.STE  = ''
    CALL OPF(FN.STE,F.STE)
    FN.ENT = "F.ACCT.ENT.TODAY"
    F.ENT  = ''
    R.ENT  = ''
    CALL OPF(FN.ENT,F.ENT)

    CALL F.READ(FN.ENT,XX,R.ENT,F.ENT,ER.ENT)
    LOOP
        REMOVE STE.ID FROM R.ENT SETTING POSS
    WHILE STE.ID:POSS
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.LCY = R.STE<AC.STE.AMOUNT.LCY>
        STE.FCY = R.STE<AC.STE.AMOUNT.FCY>
        STE.CUR = R.STE<AC.STE.CURRENCY>
        IF STE.CUR EQ 'EGP' THEN
            AMT = STE.LCY
        END ELSE
            AMT = STE.FCY
        END
        IF AMT LT 0 THEN
            AMT.DR += AMT
        END ELSE
            AMT.CR += AMT
        END
    REPEAT
    O.DATA = AMT.DR
RETURN
END
