* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.AMT.CD

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.AC.LD.BAL

    TOT.LCY.OPNE  = 0
    TOT.LCY.ONLN  = 0
    TOT.LCY.DIF   = 0
    TOT.FCY.OPNE  = 0
    TOT.FCY.ONLN  = 0
    TOT.FCY.DIF   = 0

    T.TYP   = FIELD(O.DATA,"*",1)
    DAT     = FIELD(O.DATA,"*",2)

    FN.SCB = 'F.SCB.AC.LD.BAL' ; F.SCB = '' ; R.SCB = ''
    CALL OPF (FN.SCB,F.SCB)

    T.SEL  = "SELECT F.SCB.AC.LD.BAL WITH K.TYPE EQ 'CD' AND DATE EQ ":DAT : " AND @ID LIKE ...": T.TYP

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
    FOR I = 1 TO SELECTED

        CALL F.READ(FN.SCB,KEY.LIST<I>,R.SCB,F.SCB,SCB.ERR)

        TOT.LCY.OPNE  += R.SCB<SCB.LCY.AMT.OPEN>
        TOT.LCY.ONLN  += R.SCB<SCB.LCY.AMT.ONLINE>
        TOT.LCY.DIF   += R.SCB<SCB.LCY.AMT.DIF>

        TOT.FCY.OPNE  += R.SCB<SCB.FCY.AMT.OPEN>
        TOT.FCY.ONLN  += R.SCB<SCB.FCY.AMT.ONLINE>
        TOT.FCY.DIF   += R.SCB<SCB.FCY.AMT.DIF>

    NEXT I

*   O.DATA = "������ �������" :'*':TOT.LCY :'*':TOT.FCY
    O.DATA = "������ �������" :'*':TOT.LCY.OPNE :'*':TOT.LCY.ONLN:'*':TOT.LCY.DIF:'*':TOT.FCY.OPNE :'*':TOT.FCY.ONLN:'*':TOT.FCY.DIF

    RETURN
END
