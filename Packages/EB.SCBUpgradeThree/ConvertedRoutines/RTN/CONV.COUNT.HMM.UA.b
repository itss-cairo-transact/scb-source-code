* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.COUNT.HMM.UA

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
    XX = O.DATA
    WS.COUNT = 0
    WS.ENQ.NAME = 'ENQ ':O.DATA
    FN.US  = 'F.USER'
    FN.UA = 'F.USER.ABBREVIATION'
    F.US  = '' ; R.US  = ''
    F.UA = '' ; R.UA = ''
    CALL OPF(FN.US,F.US)
    CALL OPF(FN.UA,F.UA)
************************************************************
* T.SEL  = "SELECT ":FN.US:" WITH INIT.APPLICATION EQ ":XX
* CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
* IF SELECTED THEN
*     O.DATA = 'YES'
* END ELSE
    T.SEL2  = "SELECT ":FN.UA:" WITH ORIGINAL.TEXT EQ ":XX
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED1,ER.MSG)
    FOR I = 1 TO SELECTED1
*Line [ 46 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('USER':@FM:EB.USE.SIGN.ON.NAME,KEY.LIST2<I>,WS.USER.LIVE)
F.ITSS.USER = 'F.USER'
FN.F.ITSS.USER = ''
CALL OPF(F.ITSS.USER,FN.F.ITSS.USER)
CALL F.READ(F.ITSS.USER,KEY.LIST2<I>,R.ITSS.USER,FN.F.ITSS.USER,ERROR.USER)
WS.USER.LIVE=R.ITSS.USER<EB.USE.SIGN.ON.NAME>
        IF WS.USER.LIVE NE '' THEN
            WS.COUNT ++
        END
    NEXT I
* IF SELECTED1 THEN
*     O.DATA = 'YES'
* END  ELSE
*     O.DATA = ' * '
* END
* END
    O.DATA = WS.COUNT
    RETURN
