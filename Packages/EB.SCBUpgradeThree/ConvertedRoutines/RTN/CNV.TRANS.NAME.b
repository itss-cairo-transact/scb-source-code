* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.TRANS.NAME

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TRANSACTION

    XX1   = ''
    TR.NN = ''
*****
    XX1 = O.DATA
    COMP1     = FIELD(XX1,'*',1)
    TR.CODE   = FIELD(XX1,'*',2)

*Line [ 37 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('TRANSACTION':@FM:AC.TRA.NARRATIVE,TR.CODE,TR.NN)
F.ITSS.TRANSACTION = 'F.TRANSACTION'
FN.F.ITSS.TRANSACTION = ''
CALL OPF(F.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION)
CALL F.READ(F.ITSS.TRANSACTION,TR.CODE,R.ITSS.TRANSACTION,FN.F.ITSS.TRANSACTION,ERROR.TRANSACTION)
TR.NN=R.ITSS.TRANSACTION<AC.TRA.NARRATIVE>
    IF COMP1 EQ 'EG0010011' THEN
        IF TR.CODE EQ '391' THEN
            TR.NN = '������� ������'
        END
    END
    O.DATA = TR.NN
    RETURN
*------------------------------------------------------
END
