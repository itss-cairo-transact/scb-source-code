* @ValidationCode : MjoxMzgyMzY2NDk6Q3AxMjUyOjE2NDA2MTYyMDE5MTQ6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Dec 2021 16:43:21
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*************************NI7OOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.DEPT.AUTH

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.RETIREMENTS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1

    FN.SAMP = 'F.SCB.DEPT.SAMPLE1' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)

    XX = O.DATA
    CALL F.READ(FN.SAMP,O.DATA,R.SAMP,F.SAMP,E11)
    APP1 = R.SAMP<DEPT.SAMP.DEPT.NO.HWALA>
    APP2 = R.SAMP<DEPT.SAMP.DEPT.NO.LG11>
    APP3 = R.SAMP<DEPT.SAMP.DEPT.NO.TEL>
    APP4 = R.SAMP<DEPT.SAMP.DEPT.NO.TF1>
    APP5 = R.SAMP<DEPT.SAMP.DEPT.NO.BR>
    APP6 = R.SAMP<DEPT.SAMP.DEPT.NO.AC>
    APP7 = R.SAMP<DEPT.SAMP.DEPT.NO.WH>

    IF APP1 NE '' THEN
        O.DATA = APP1
    END
    IF APP2 NE '' THEN
        O.DATA = APP2
    END
    IF APP3 NE '' THEN
        O.DATA = APP3
    END
    IF APP4 NE '' THEN
        O.DATA = APP4
    END
    IF APP5 NE '' THEN
        O.DATA = APP5
    END
    IF APP6 NE '' THEN
        O.DATA = APP6
    END
    IF APP7 NE '' THEN
        O.DATA = APP7
    END


RETURN
END
