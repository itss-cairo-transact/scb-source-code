* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.AMT.COLL.NEW

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    CU.LD    = FIELD(O.DATA,"*",1)
    CATG.LD  = FIELD(O.DATA,"*",2)
    CURRR.LD = FIELD(O.DATA,"*",3)
    AMT      = 0

    IF CATG.LD GE 21001 AND CATG.LD LE 21035 THEN
        FN.LD ='FBNK.LD.LOANS.AND.DEPOSITS' ;R.LD='';F.LD=''
        CALL OPF(FN.LD,F.LD)

        FN.LMM ='FBNK.LMM.CUSTOMER' ;R.LMM='';F.LMM =''
        CALL OPF(FN.LMM,F.LMM)

        FN.COLL = 'FBNK.COLLATERAL' ; R.COLL = '' ; F.COLL = ''
        CALL OPF(FN.COLL,F.COLL)

**---------------------------------


        CALL F.READ( FN.LMM,CU.LD, R.LMM, F.LMM,ETEXTLM)
        LOOP

            REMOVE LDD FROM R.LMM  SETTING POS111
        WHILE LDD:POS111
            CALL F.READ(FN.LD,LDD, R.LD, F.LD,E)

            LD.AMT     = R.LD<LD.AMOUNT>
            LD.CURR    = R.LD<LD.CURRENCY>
            LD.CATEG   = R.LD<LD.CATEGORY>

            IF LD.AMT NE 0 THEN
                IF (CURRR.LD EQ LD.CURR AND CATG.LD EQ LD.CATEG ) THEN
                    COL.ID = R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
                    CALL F.READ(FN.COLL,COL.ID, R.COLL, F.COLL,E1)
                    COLL.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
                    AMT = COLL.AMT + AMT
                END
            END

        REPEAT
    END
    IF AMT NE 0 THEN
        O.DATA = AMT
    END ELSE
        O.DATA = ''
    END

**-------------------------------------------------------------------**
    RETURN
END
