* @ValidationCode : MjoyMDcwNDc3NzA6Q3AxMjUyOjE2NDA2MjU0ODE0MzA6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 27 Dec 2021 19:18:01
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************************MAHMOUD 16/7/2013*************************
*-----------------------------------------------------------------------------
* <Rating>190</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.ORG.CUR

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS

    SS = ''
    Y.ORG.CUR = ''
    Y.ORG.AMT = ''
    AA = O.DATA
    IF AA[1,2] EQ 'FT' THEN
*Line [ 38 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR '\' IN AA SETTING Y.XX THEN AA = FIELD(AA,'\',1) ELSE NULL
*Line [ 40 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,AA,FT.LCL)
F.ITSS.FUNDS.TRANSFER = 'FBNK.FUNDS.TRANSFER'
FN.F.ITSS.FUNDS.TRANSFER = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER)
CALL F.READ(F.ITSS.FUNDS.TRANSFER,AA,R.ITSS.FUNDS.TRANSFER,FN.F.ITSS.FUNDS.TRANSFER,ERROR.FUNDS.TRANSFER)
FT.LCL=R.ITSS.FUNDS.TRANSFER<FT.LOCAL.REF>
        IF NOT(FT.LCL) THEN
            AA = AA:";1"
*Line [ 49 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.LOCAL.REF,AA,FT.LCL)
F.ITSS.FUNDS.TRANSFER$HIS = 'F.FUNDS.TRANSFER$HIS'
FN.F.ITSS.FUNDS.TRANSFER$HIS = ''
CALL OPF(F.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS)
CALL F.READ(F.ITSS.FUNDS.TRANSFER$HIS,AA,R.ITSS.FUNDS.TRANSFER$HIS,FN.F.ITSS.FUNDS.TRANSFER$HIS,ERROR.FUNDS.TRANSFER$HIS)
FT.LCL=R.ITSS.FUNDS.TRANSFER$HIS<FT.LOCAL.REF>
        END
        Y.ORG.CUR = FT.LCL<1,FTLR.TRANSACTION.CUR>
        Y.ORG.AMT = FMT(FT.LCL<1,FTLR.TRANSACTION.AMT>,"L2")
    END
    IF Y.ORG.CUR NE '' AND Y.ORG.CUR NE 'EGP' THEN
        O.DATA = Y.ORG.CUR:" ":Y.ORG.AMT
    END ELSE
        O.DATA = ''
    END
RETURN
END
