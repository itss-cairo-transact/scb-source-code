* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.BANK.CODE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-----------------------------------------------
    BANK.CODE = O.DATA

    IF BANK.CODE EQ '0057 ' THEN
        NEW.CR.ACCT = '9940008710200501'
    END
    IF BANK.CODE EQ '0029' THEN
        NEW.CR.ACCT = '9940011910200501'
    END
    IF BANK.CODE EQ '0009' THEN
        NEW.CR.ACCT = '9940029510200501'
    END
    IF BANK.CODE EQ '0005' THEN
        NEW.CR.ACCT = '9940030010200501'
    END
    IF BANK.CODE EQ '0031' THEN
        NEW.CR.ACCT = '9940094010200501'
    END
    IF BANK.CODE EQ '0036' THEN
        NEW.CR.ACCT = '9949110110200501'
    END
    O.DATA  =  NEW.CR.ACCT
*-----------------------------------------------
    RETURN
END
