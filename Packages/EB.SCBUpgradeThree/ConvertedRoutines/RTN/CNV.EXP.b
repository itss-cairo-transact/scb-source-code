* @ValidationCode : Mjo4NjE3Njk3NDc6Q3AxMjUyOjE2NDE3MzM0NzYyNDc6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:04:36
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*---------------------------------NI7OOOOOOOOOOO-------------------------------------------
SUBROUTINE CNV.EXP

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS


    FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    CALL OPF( FN.LC,F.LC)

    FN.DR   = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
    CALL OPF( FN.DR,F.DR)

    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD= ''
    CALL OPF( FN.LD,F.LD)


* CALL F.READ( FN.DR,O.DATA, R.DR, F.DR,ETEXT)
    IF O.DATA[1,2] EQ 'LD' THEN
        CALL F.READ( FN.LD,O.DATA, R.LD, F.LD,ETEXT)
        CATE.LD = R.LD<LD.CATEGORY>
        IF CATE.LD NE 21096 THEN
            MAT.DAT = R.LD<LD.FIN.MAT.DATE>
            O.DATA = MAT.DAT[7,2]:"/":MAT.DAT[5,2]:"/":MAT.DAT[1,4]
        END
        IF CATE.LD EQ 21096 THEN
            ACT.DAT = R.LD<LD.LOCAL.REF><1,LDLR.ACTUAL.EXP.DATE>
            O.DATA = ACT.DAT[7,2]:"/":ACT.DAT[5,2]:"/":ACT.DAT[1,4]
        END
    END ELSE
        O.DATA = ''
    END
RETURN
END
