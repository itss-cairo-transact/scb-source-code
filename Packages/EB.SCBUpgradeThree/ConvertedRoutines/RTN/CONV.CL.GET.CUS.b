* @ValidationCode : MjotMjI0OTQ2NTQ5OkNwMTI1MjoxNjQwNjk0MjY2NTgxOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:24:26
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CL.GET.CUS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.DEPT.SAMPLE1

*TEXT = "HI"; CALL REM
    FN.CL  = 'F.SCB.DEPT.SAMPLE1$NAU'  ; F.CL = ''; R.CL = ''
    CALL OPF(FN.CL,F.CL)

    CL.NO = O.DATA

*IF CL.NO THEN
    CALL F.READ(FN.CL,CL.NO, R.CL, F.CL, ETEXT)
    CUS1 = R.CL<DEPT.SAMP.CUS.HWALA>
    CUS2 = R.CL<DEPT.SAMP.CUS.LG11>
    CUS3 = R.CL<DEPT.SAMP.CUS.LG22>
    CUS4 = R.CL<DEPT.SAMP.CUS.TEL>
    CUS5 = R.CL<DEPT.SAMP.CUS.TF1>
    CUS6 = R.CL<DEPT.SAMP.CUS.TF2>
    CUS7 = R.CL<DEPT.SAMP.CUS.TF3>
    CUS8 = R.CL<DEPT.SAMP.CUS.BR>
    CUS9 = R.CL<DEPT.SAMP.CUS.AC>
    CUS10 = R.CL<DEPT.SAMP.CUS.WH>

    BEGIN CASE
        CASE CUS1
            CUS.NO = CUS1
        CASE CUS2
            CUS.NO = CUS2
        CASE CUS3
            CUS.NO = CUS3
        CASE CUS4
            CUS.NO = CUS4
        CASE CUS5
            CUS.NO = CUS5
        CASE CUS6
            CUS.NO = CUS6
        CASE CUS7
            CUS.NO = CUS7
        CASE CUS8
            CUS.NO = CUS8
        CASE CUS9
            CUS.NO = CUS9
        CASE CUS10
            CUS.NO = CUS10

    END CASE
    O.DATA = CUS.NO
*END

RETURN
END
