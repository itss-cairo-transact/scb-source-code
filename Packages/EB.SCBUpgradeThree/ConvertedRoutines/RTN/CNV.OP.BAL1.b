* @ValidationCode : MjotMTIxMjcxNDYzNjpDcDEyNTI6MTY0MTczMzkzNTQzNDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:12:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CNV.OP.BAL1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT      ;*AC.
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM      ;*RE.BCP.
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.BAL       ;*RE.SLB.
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON          ;*EB.COM.
    COMP = ID.COMPANY

    XX = O.DATA
    CR.TYPE = FIELD(XX,'*',1)
    TDD     = FIELD(XX,'*',2)
    TD1     = FIELD(XX,'*',3)
    TDX = TODAY
*-------------------------------------------------------------------------
    GOSUB INITIATE
*Line [ 43 ] Adding EB.SCBUpgradeThree. in order to call another routine - ITSS - R21 Upgrade - 2022-02-09
*Line [ 44 ] Adding EB.SCBUpgradeThree. in order to call another routine - ITSS - R21 Upgrade - 2022-02-15
    GOSUB CALLDB
    GOSUB PROCESS
RETURN
*==============================================================
INITIATE:
*-------
    ACC.ID = ''
    STE.BR = ''
    STE.CNF.DR = ''
    STE.CNF.CR = ''
    BAL.ID = ''
    X = 0
    ACC.ID      = 0
    CUR.TYPE    = ''
    OP.BAL.LCL  = 0
    CLOSE.BAL   = 0
    K = 0
    CCC = ''
    USER.COMPANY = COMP
    USER.CO = USER.COMPANY[4]
RETURN
*===============================================================
CALLDB:
*-------
    FN.BAL  = 'FBNK.RE.STAT.LINE.BAL'   ; F.BAL  = '' ; R.BAL = ''
    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    FN.ACC  = 'F.ACCOUNT' ; F.ACC  = '' ; R.ACC  = ''
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CUR,F.CUR)
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST  ="" ; SELECTED  ="" ;  ER.MSG  =""
    KEY.LIST1 ="" ; SELECTED1 ="" ;  ER.MSG1 =""
    KEY.LIST2 ="" ; SELECTED2 ="" ;  ER.MSG2 =""
RETURN
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*************************************************************************
PROCESS:
*-------
    IF CR.TYPE EQ 'EGP' THEN
        IF TD1 EQ TDX THEN
            ACC.ID = 'EGP115000099':USER.CO
            A.SEL = "SELECT ":FN.ACC:" WITH @ID EQ EGP115000099":USER.CO:" OR @ID EQ EGP115050099":USER.CO
            CALL EB.READLIST(A.SEL,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 THEN
                FOR A = 1 TO SELECTED1
                    CALL F.READ(FN.ACC,KEY.LIST1<A>,R.ACC,F.ACC,EA1)
                    OP.BAL.LCL += R.ACC<AC.OPEN.ACTUAL.BAL>
                NEXT A
            END
        END ELSE
*-----------------------------------------------------------------
            BAL.ID = "GENLED-0620-LOCAL-":TDD:"*":USER.COMPANY
            CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,EM1)
            IF EM1 THEN
                BAL.ID = "GENLED-0111-LOCAL-":TDD:"*":USER.COMPANY
                CALL F.READ(FN.BAL,BAL.ID,R.BAL,F.BAL,EMX1)
            END
            OP.BAL.LCL = R.BAL<RE.SLB.OPEN.BAL.LCL>

*-----------------------------------------------------------------
        END
*************************************************************************
    END ELSE
*--------------------------------------------------------------------------------------
        CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
        CUR.COD     = R.CUR<RE.BCP.ORIGINAL.CCY>
*Line [ 109 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        TTT = DCOUNT(CUR.COD,@VM)
        FOR POS = 1 TO TTT
            CURR        = R.CUR<RE.BCP.ORIGINAL.CCY,POS>
            IF CURR NE 'EGP' THEN
                CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
                RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
                ACCC.ID     = CURR:"115000099":USER.CO
                BALL.ID     = "GENLED-0820-":CURR:"-":TDD:"*":USER.COMPANY
                BALL.ID2    = "GENLED-0311-":CURR:"-":TDD:"*":USER.COMPANY
                IF TD1 EQ TDX THEN
                    CALL F.READ(FN.ACC,ACCC.ID,R.ACC,F.ACC,EAA)
                END ELSE
                    CALL F.READ(FN.BAL,BALL.ID,R.BAL,F.BAL,EMA)
                    OPP.BAL = R.BAL<RE.SLB.OPEN.BAL>
                    IF OPP.BAL EQ '' THEN
                        CALL F.READ(FN.BAL,BALL.ID2,R.BAL,F.BAL,EMA2)
                        OPP.BAL = R.BAL<RE.SLB.OPEN.BAL>
                    END
                END
                IF RATE.TYPE EQ 'MULTIPLY' THEN
                    IF TD1 EQ TDX THEN
                        OP.BAL.LCL += R.ACC<AC.OPEN.ACTUAL.BAL> * CUR.RATE
                    END ELSE
                        OP.BAL.LCL += R.BAL<RE.SLB.OPEN.BAL> * CUR.RATE
                    END
                END
                IF RATE.TYPE EQ 'DIVIDE' THEN
                    IF TD1 EQ TDX THEN
                        OP.BAL.LCL += R.ACC<AC.OPEN.ACTUAL.BAL> / CUR.RATE
                    END ELSE
                        OP.BAL.LCL += R.BAL<RE.SLB.OPEN.BAL> / CUR.RATE
                    END
                END
            END
        NEXT POS
*---------------------------------------------------------------------------------
    END
    O.DATA = OP.BAL.LCL
RETURN
*================================================================
