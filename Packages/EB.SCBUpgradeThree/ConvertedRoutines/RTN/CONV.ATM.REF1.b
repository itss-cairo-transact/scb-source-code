* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.ATM.REF1(ENQUIRY.DATA)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS

    LOCATE "@ID" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
        IDD = ENQUIRY.DATA<4,CUS.POS>
    END

    LOCATE "EBC.AUTH.NO" IN ENQUIRY.DATA<2,1> SETTING CUS.POS THEN
        ATM.REF = ENQUIRY.DATA<4,CUS.POS>
    END
    FN.ACC = 'FBNK.ACCOUNT' ;F.ACC = '' ; R.ACC = '' ; READ.ERR = '' ; RETRY = ''
    CALL OPF(FN.ACC,F.ACC)
    KEY.LIST = "" ; SELECTED = "" ; ER.MSG = ""

    CALL F.READ(FN.ACC,IDD,R.ACC,F.ACC,READ.ERR)

    IF NOT(READ.ERR) THEN

        R.ACC<AC.LOCAL.REF,ACLR.EBC.AUTH.NO> = ATM.REF
        WRITE  R.ACC TO F.ACC , IDD ON ERROR
            PRINT "CAN NOT WRITE RECORD":KEY.LIST:"TO" :FN.ACC
        END
    END

    RETURN
END
