* @ValidationCode : MjotMTM2NTUyNzkwNjpDcDEyNTI6MTY0MTczNjgyMzA0OTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 16:00:23
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
PROGRAM CONV.BRANCH.DETAIL.LOAN.COMP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23

    $INCLUDE I_F.SCB.LINE.GENDETALL

    COMP = ID.COMPANY
*-----------------------------------------------------------------------
    OPENSEQ "&SAVEDLISTS&" , "BRANCH.DETAIL.LOAN.COMP.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"BRANCH.DETAIL.LOAN.COMP.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "BRANCH.DETAIL.LOAN.COMP.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE BRANCH.DETAIL.LOAN.COMP.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create BRANCH.DETAIL.LOAN.COMP.CSV File IN &SAVEDLISTS&'
        END
    END
*----------------------------------------------------------------

    FN.LN   = 'F.RE.STAT.REP.LINE'      ; F.LN   = '' ; R.LN   = ''
    FN.CONT = 'F.RE.STAT.LINE.CONT'     ; F.CONT = '' ; R.CONT = ''
    FN.CCY  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CCY  = '' ; R.CCY  = ''
    FN.CONS = 'F.CONSOLIDATE.ASST.LIAB' ; F.CONS = ''

    CALL OPF(FN.LN,F.LN) ; CALL OPF(FN.CONT,F.CONT) ; CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.CONS,F.CONS)

    DAT.ID = COMP
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,DAT.ID,R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
TD1=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

    KK = 'EG00100'
    NK = ''
    NK<1,1>='01'  ; NK<1,2>='02'  ; NK<1,3>='03'  ; NK<1,4>='04'  ; NK<1,5>='05'
    NK<1,6>='06'  ; NK<1,7>='07'  ; NK<1,8>='09'  ; NK<1,9>='10'  ; NK<1,10>='11'
    NK<1,11>='12' ; NK<1,12>='13' ; NK<1,13>='14' ; NK<1,14>='15' ; NK<1,15>='20'
    NK<1,16>='21' ; NK<1,17>='22' ; NK<1,18>='23' ; NK<1,19>='30' ; NK<1,20>='31'
    NK<1,21>='32' ; NK<1,22>='35' ; NK<1,23>='40' ; NK<1,24>='50' ; NK<1,25>='60'
    NK<1,26>='70' ; NK<1,27>='80' ; NK<1,28>='81' ; NK<1,29>='88' ; NK<1,30>='90' ; NK<1,31>='99'
    NK<1,32>='17' ; NK<1,33>='19' ; NK<1,34>='51' ; NK<1,35>='52'

    LINE.PRINT = ''
    LINE.BR    = ''

    HEAD.DESC  = ",":"���� ������� ����� ������ ��������� - ������� - ����"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "�������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "�.�����":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "�.�������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "���������":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "�.���":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "����� ����":","
    HEAD.DESC := "�.6 ������":","
    HEAD.DESC := "����� 6 ������":","
    HEAD.DESC := "��������":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "��������":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "����� �����":","
    HEAD.DESC := "��������":","
    HEAD.DESC := "��� �����":","
    HEAD.DESC := "���������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "����":","
    HEAD.DESC := "������ �� �����":","
    HEAD.DESC := "������":","
    HEAD.DESC := "�����":","
    HEAD.DESC := "�������":","
    HEAD.DESC := "������":"|"
    HEAD.DESC := "������":"|"
    HEAD.DESC := "����� ������":","
    HEAD.DESC := "��� �����":"|"
    HEAD.DESC := "������ ������":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    NEW.LINE = ''
    TOT.LINE = ''
    GOSUB PROCESS.ASST
RETURN
*--------------------------------------------------------------------
PROCESS.ASST:
    T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENDETALL.0119 AND @ID LE GENDETALL.0282 ) OR ( @ID GE GENDETALL.4000 AND @ID LE GENDETALL.4011 ) OR ( @ID GE GENDETALL.3065 AND @ID LE GENDETALL.3067) OR ( @ID GE GENDETALL.3074 AND @ID LE GENDETALL.3075) OR ( @ID EQ GENDETALL.3086 OR @ID EQ GENDETALL.0117 OR @ID EQ GENDETALL.3093 OR @ID EQ GENDETALL.3130 OR @ID EQ GENDETALL.3131 OR @ID EQ GENDETALL.3136 ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC = R.LN<RE.SRL.DESC><1,1>
            LINE.NO = R.LN<RE.SRL.DESC><1,2>
            ASST.TYPE       = R.LN<RE.SRL.ASSET.TYPE>
*Line [ 153 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            ASST.TYPE.COUNT = DCOUNT(ASST.TYPE,@VM)

            IF I EQ 1 THEN NEW.LINE = LINE.NO
            IF NEW.LINE NE LINE.NO THEN
                NN.DATA  = "******":","
                NN.DATA := "������ ":NEW.LINE:","
                NN.DATA := TOT.LINE<1,1>:","
                NN.DATA := TOT.LINE<1,2>:","
                NN.DATA := TOT.LINE<1,3>:","
                NN.DATA := TOT.LINE<1,4>:","
                NN.DATA := TOT.LINE<1,5>:","
                NN.DATA := TOT.LINE<1,6>:","
                NN.DATA := TOT.LINE<1,7>:","
                NN.DATA := TOT.LINE<1,8>:","
                NN.DATA := TOT.LINE<1,9>:","
                NN.DATA := TOT.LINE<1,10>:","
                NN.DATA := TOT.LINE<1,11>:","
                NN.DATA := TOT.LINE<1,12>:","
                NN.DATA := TOT.LINE<1,13>:","
                NN.DATA := TOT.LINE<1,14>:","
                NN.DATA := TOT.LINE<1,15>:","
                NN.DATA := TOT.LINE<1,16>:","
                NN.DATA := TOT.LINE<1,17>:","
                NN.DATA := TOT.LINE<1,18>:","
                NN.DATA := TOT.LINE<1,19>:","
                NN.DATA := TOT.LINE<1,20>:","
                NN.DATA := TOT.LINE<1,21>:","
                NN.DATA := TOT.LINE<1,22>:","
                NN.DATA := TOT.LINE<1,23>:","
                NN.DATA := TOT.LINE<1,24>:","
                NN.DATA := TOT.LINE<1,25>:","
                NN.DATA := TOT.LINE<1,26>:","
                NN.DATA := TOT.LINE<1,27>:","
                NN.DATA := TOT.LINE<1,28>:","
                NN.DATA := TOT.LINE<1,29>:","
                NN.DATA := TOT.LINE<1,30>:","
                NN.DATA := TOT.LINE<1,31>:","
                NN.DATA := TOT.LINE<1,32>:","
                NN.DATA := TOT.LINE<1,33>:","
                NN.DATA := TOT.LINE<1,34>:","
                NN.DATA := TOT.LINE<1,35>:","

                WRITESEQ NN.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOT.LINE = ''
            END

            DESC.ID = FIELD(KEY.LIST<I>,".",2)

            PROD.CATEG = R.LN<RE.SRL.ASSET1,1>
            PL.CATEG   = R.LN<RE.SRL.PROFIT1,1>

            IF PROD.CATEG EQ '' THEN
                CATEG = PL.CATEG
            END ELSE
                CATEG = PROD.CATEG
            END

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
            CLOSE.BAL.LCL  = 0
            CONT.BAL  = 0
            OLD.BRN = ''
            NEW.BRN = ''
*************************************************************************
            CONT.ID = KEY.LIST<I>:"...."
            T.SEL1 = "SELECT ":FN.CONT:" WITH @ID LIKE ":CONT.ID:" BY @ID"
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 THEN
                FOR KM = 1 TO SELECTED1
                    CALL F.READ(FN.CONT,KEY.LIST1<KM>,R.CONT,F.CONT,E5)
                    NEW.BRN = FIELD(KEY.LIST1<KM>,".",3)

*Line [ 227 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
                    FOR KM1 = 1 TO DECOUNT.CONT
                        CONS.ID  = R.CONT<RE.SLC.ASST.CONSOL.KEY,KM1>
                        CALL F.READ(FN.CONS,CONS.ID,R.CONS,F.CONS,E2)
                        CONT.SEC = R.CONS<RE.ASL.VARIABLE.11>
                        IF (CONT.SEC LT '3000' OR CONT.SEC GT '3999') AND (CONT.SEC NE '1100' AND CONT.SEC NE '1200' AND CONT.SEC NE '1300' AND CONT.SEC NE '1400' AND CONT.SEC NE '2000') THEN
*Line [ 234 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                            CONT.TYPE.COUNT =  DCOUNT(R.CONS<RE.ASL.TYPE>,@VM)
                            CONT.CUR = R.CONS<RE.ASL.CURRENCY>

                            IF KEY.LIST<I> GE 'GENDETALL.4000' AND KEY.LIST<I> LE 'GENDETALL.4011' THEN
                                ASST.TYPE.COUNT = 1
                            END

                            FOR II = 1 TO ASST.TYPE.COUNT
                                ASST.TYPE1 =  R.LN<RE.SRL.ASSET.TYPE,II>
*Line [ 244 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                                LOCATE ASST.TYPE1 IN R.CONS<RE.ASL.TYPE,1> SETTING TYP ELSE NULL

                                CONT.BAL = R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
*Line [ 248 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                                LOCATE CONT.CUR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                                RATE           = R.CCY<RE.BCP.RATE,POS>
                                CLOSE.BAL.LCL += CONT.BAL * RATE
                            NEXT II

                        END
                    NEXT KM1

                    IF NEW.BRN EQ 'EG0010099' THEN
                        LINE.BR<1,1>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010001' THEN
                        LINE.BR<1,2>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010002' THEN
                        LINE.BR<1,3>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010003' THEN
                        LINE.BR<1,4>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010004' THEN
                        LINE.BR<1,5>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010005' THEN
                        LINE.BR<1,6>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010006' THEN
                        LINE.BR<1,7>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010007' THEN
                        LINE.BR<1,8>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010009' THEN
                        LINE.BR<1,9>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010010' THEN
                        LINE.BR<1,10>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010011' THEN
                        LINE.BR<1,11>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010012' THEN
                        LINE.BR<1,12>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010013' THEN
                        LINE.BR<1,13>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010014' THEN
                        LINE.BR<1,14>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010015' THEN
                        LINE.BR<1,15>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010020' THEN
                        LINE.BR<1,16>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010021' THEN
                        LINE.BR<1,17>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010022' THEN
                        LINE.BR<1,18>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010023' THEN
                        LINE.BR<1,19>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010030' THEN
                        LINE.BR<1,20>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010031' THEN
                        LINE.BR<1,21>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010032' THEN
                        LINE.BR<1,22>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010035' THEN
                        LINE.BR<1,23>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010040' THEN
                        LINE.BR<1,24>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010050' THEN
                        LINE.BR<1,25>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010060' THEN
                        LINE.BR<1,26>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010070' THEN
                        LINE.BR<1,27>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010080' THEN
                        LINE.BR<1,28>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010081' THEN
                        LINE.BR<1,29>   = CLOSE.BAL.LCL
                    END

                    IF NEW.BRN EQ 'EG0010090' THEN
                        LINE.BR<1,30>   = CLOSE.BAL.LCL
                    END
                    IF NEW.BRN EQ 'EG0010017' THEN
                        LINE.BR<1,31>   = CLOSE.BAL.LCL
                    END
                    IF NEW.BRN EQ 'EG0010019' THEN
                        LINE.BR<1,32>   = CLOSE.BAL.LCL
                    END
                    IF NEW.BRN EQ 'EG0010051' THEN
                        LINE.BR<1,33>   = CLOSE.BAL.LCL
                    END
                    IF NEW.BRN EQ 'EG0010052' THEN
                        LINE.BR<1,34>   = CLOSE.BAL.LCL
                    END

                    CLOSE.BAL.LCL = 0
*------------------------------------------------------------------
                    IF NEW.BRN EQ 'EG0010099' THEN
                        TOT.LINE<1,1> += LINE.BR<1,1>
                    END

                    IF NEW.BRN EQ 'EG0010001' THEN
                        TOT.LINE<1,2> += LINE.BR<1,2>
                    END
                    IF NEW.BRN EQ 'EG0010002' THEN
                        TOT.LINE<1,3> += LINE.BR<1,3>
                    END

                    IF NEW.BRN EQ 'EG0010003' THEN
                        TOT.LINE<1,4> +=  LINE.BR<1,4>
                    END

                    IF NEW.BRN EQ 'EG0010004' THEN
                        TOT.LINE<1,5> +=   LINE.BR<1,5>
                    END

                    IF NEW.BRN EQ 'EG0010005' THEN
                        TOT.LINE<1,6> +=  LINE.BR<1,6>
                    END

                    IF NEW.BRN EQ 'EG0010006' THEN
                        TOT.LINE<1,7> +=  LINE.BR<1,7>
                    END

                    IF NEW.BRN EQ 'EG0010007' THEN
                        TOT.LINE<1,8> +=  LINE.BR<1,8>
                    END

                    IF NEW.BRN EQ 'EG0010009' THEN
                        TOT.LINE<1,9> +=  LINE.BR<1,9>
                    END

                    IF NEW.BRN EQ 'EG0010010' THEN
                        TOT.LINE<1,10> +=  LINE.BR<1,10>
                    END

                    IF NEW.BRN EQ 'EG0010011' THEN
                        TOT.LINE<1,11> +=  LINE.BR<1,11>
                    END

                    IF NEW.BRN EQ 'EG0010012' THEN
                        TOT.LINE<1,12> +=  LINE.BR<1,12>
                    END

                    IF NEW.BRN EQ 'EG0010013' THEN
                        TOT.LINE<1,13> +=  LINE.BR<1,13>
                    END

                    IF NEW.BRN EQ 'EG0010014' THEN
                        TOT.LINE<1,14> +=  LINE.BR<1,14>
                    END

                    IF NEW.BRN EQ 'EG0010015' THEN
                        TOT.LINE<1,15> +=  LINE.BR<1,15>
                    END

                    IF NEW.BRN EQ 'EG0010020' THEN
                        TOT.LINE<1,16> +=  LINE.BR<1,16>
                    END

                    IF NEW.BRN EQ 'EG0010021' THEN
                        TOT.LINE<1,17> +=  LINE.BR<1,17>
                    END

                    IF NEW.BRN EQ 'EG0010022' THEN
                        TOT.LINE<1,18> +=  LINE.BR<1,18>
                    END

                    IF NEW.BRN EQ 'EG0010023' THEN
                        TOT.LINE<1,19> +=  LINE.BR<1,19>
                    END

                    IF NEW.BRN EQ 'EG0010030' THEN
                        TOT.LINE<1,20> +=  LINE.BR<1,20>
                    END

                    IF NEW.BRN EQ 'EG0010031' THEN
                        TOT.LINE<1,21> +=  LINE.BR<1,21>
                    END

                    IF NEW.BRN EQ 'EG0010032' THEN
                        TOT.LINE<1,22> +=  LINE.BR<1,22>
                    END

                    IF NEW.BRN EQ 'EG0010035' THEN
                        TOT.LINE<1,23> +=  LINE.BR<1,23>
                    END

                    IF NEW.BRN EQ 'EG0010040' THEN
                        TOT.LINE<1,24> +=  LINE.BR<1,24>
                    END

                    IF NEW.BRN EQ 'EG0010050' THEN
                        TOT.LINE<1,25> +=  LINE.BR<1,25>
                    END

                    IF NEW.BRN EQ 'EG0010060' THEN
                        TOT.LINE<1,26> +=  LINE.BR<1,26>
                    END

                    IF NEW.BRN EQ 'EG0010070' THEN
                        TOT.LINE<1,27> +=  LINE.BR<1,27>
                    END

                    IF NEW.BRN EQ 'EG0010080' THEN
                        TOT.LINE<1,28> +=  LINE.BR<1,28>
                    END

                    IF NEW.BRN EQ 'EG0010081' THEN
                        TOT.LINE<1,29> +=  LINE.BR<1,29>
                    END

                    IF NEW.BRN EQ 'EG0010090' THEN
                        TOT.LINE<1,30> +=  LINE.BR<1,30>
                    END
                    IF NEW.BRN EQ 'EG0010017' THEN
                        TOT.LINE<1,31> +=  LINE.BR<1,31>
                    END
                    IF NEW.BRN EQ 'EG0010019' THEN
                        TOT.LINE<1,32> +=  LINE.BR<1,32>
                    END
                    IF NEW.BRN EQ 'EG0010051' THEN
                        TOT.LINE<1,33> +=  LINE.BR<1,33>
                    END
                    IF NEW.BRN EQ 'EG0010052' THEN
                        TOT.LINE<1,34> +=  LINE.BR<1,34>
                    END

                NEXT KM
            END
            BB.DATA  = CATEG:","
            BB.DATA := DESC:","
            BB.DATA := LINE.BR<1,1>:","
            BB.DATA := LINE.BR<1,2>:","
            BB.DATA := LINE.BR<1,3>:","
            BB.DATA := LINE.BR<1,4>:","
            BB.DATA := LINE.BR<1,5>:","
            BB.DATA := LINE.BR<1,6>:","
            BB.DATA := LINE.BR<1,7>:","
            BB.DATA := LINE.BR<1,8>:","
            BB.DATA := LINE.BR<1,9>:","
            BB.DATA := LINE.BR<1,10>:","
            BB.DATA := LINE.BR<1,11>:","
            BB.DATA := LINE.BR<1,12>:","
            BB.DATA := LINE.BR<1,13>:","
            BB.DATA := LINE.BR<1,14>:","
            BB.DATA := LINE.BR<1,15>:","
            BB.DATA := LINE.BR<1,16>:","
            BB.DATA := LINE.BR<1,17>:","
            BB.DATA := LINE.BR<1,18>:","
            BB.DATA := LINE.BR<1,19>:","
            BB.DATA := LINE.BR<1,20>:","
            BB.DATA := LINE.BR<1,21>:","
            BB.DATA := LINE.BR<1,22>:","
            BB.DATA := LINE.BR<1,23>:","
            BB.DATA := LINE.BR<1,24>:","
            BB.DATA := LINE.BR<1,25>:","
            BB.DATA := LINE.BR<1,26>:","
            BB.DATA := LINE.BR<1,27>:","
            BB.DATA := LINE.BR<1,28>:","
            BB.DATA := LINE.BR<1,29>:","
            BB.DATA := LINE.BR<1,30>:","
            BB.DATA := LINE.BR<1,31>:","
            BB.DATA := LINE.BR<1,32>:","
            BB.DATA := LINE.BR<1,33>:","
            BB.DATA := LINE.BR<1,34>:","

            FOR AK = 1 TO 34
                LINE.BR<1,35> += LINE.BR<1,AK>
            NEXT AK
            BB.DATA := LINE.BR<1,35>:","

            TOT.LINE<1,35> += LINE.BR<1,35>

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            LINE.BR<1,35> = 0
            NEW.LINE = LINE.NO

        NEXT I
        IF I EQ SELECTED THEN
            NN.DATA  = "******":","
            NN.DATA := "������ ":NEW.LINE:","
            NN.DATA := TOT.LINE<1,1>:","
            NN.DATA := TOT.LINE<1,2>:","
            NN.DATA := TOT.LINE<1,3>:","
            NN.DATA := TOT.LINE<1,4>:","
            NN.DATA := TOT.LINE<1,5>:","
            NN.DATA := TOT.LINE<1,6>:","
            NN.DATA := TOT.LINE<1,7>:","
            NN.DATA := TOT.LINE<1,8>:","
            NN.DATA := TOT.LINE<1,9>:","
            NN.DATA := TOT.LINE<1,10>:","
            NN.DATA := TOT.LINE<1,11>:","
            NN.DATA := TOT.LINE<1,12>:","
            NN.DATA := TOT.LINE<1,13>:","
            NN.DATA := TOT.LINE<1,14>:","
            NN.DATA := TOT.LINE<1,15>:","
            NN.DATA := TOT.LINE<1,16>:","
            NN.DATA := TOT.LINE<1,17>:","
            NN.DATA := TOT.LINE<1,18>:","
            NN.DATA := TOT.LINE<1,19>:","
            NN.DATA := TOT.LINE<1,20>:","
            NN.DATA := TOT.LINE<1,21>:","
            NN.DATA := TOT.LINE<1,22>:","
            NN.DATA := TOT.LINE<1,23>:","
            NN.DATA := TOT.LINE<1,24>:","
            NN.DATA := TOT.LINE<1,25>:","
            NN.DATA := TOT.LINE<1,26>:","
            NN.DATA := TOT.LINE<1,27>:","
            NN.DATA := TOT.LINE<1,28>:","
            NN.DATA := TOT.LINE<1,29>:","
            NN.DATA := TOT.LINE<1,30>:","
            NN.DATA := TOT.LINE<1,31>:","
            NN.DATA := TOT.LINE<1,32>:","
            NN.DATA := TOT.LINE<1,33>:","
            NN.DATA := TOT.LINE<1,34>:","
            NN.DATA := TOT.LINE<1,35>:","

            WRITESEQ NN.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            TOT.LINE = ''
        END

    END
RETURN
*------------------------------------------
