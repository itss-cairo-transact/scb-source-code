* @ValidationCode : MjotNDY4NjQ5Mjg2OkNwMTI1MjoxNjQwNjEzMjE5MjM3OmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 27 Dec 2021 15:53:39
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CNV.COST.CEN.CODE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_BR.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 33 ] Hashing $INCLUDE I_FT.LOCAL.REFS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_FT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 36 ] Hashing $INCLUDE I_F.INF.MULTI.TXN - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.INF.MULTI.TXN

    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.IN = "F.INF.MULTI.TXN" ; F.IN = ''
    CALL OPF(FN.IN,F.IN)
    FN.INH = "F.INF.MULTI.TXN$HIS" ; F.INH = ''
    CALL OPF(FN.INH,F.INH)


    XX  = O.DATA
    REF = ';1'
*   CALL F.READ(FN.INH,XX:REF,R.INH,F.INH,E5)
    IF XX[1,2] EQ 'FT' THEN
        CALL F.READ(FN.FT,XX,R.FT,F.FT,E2)
        COST.CENT1 = R.FT<FT.PROFIT.CENTRE.DEPT>
        O.DATA = COST.CENT1

        IF COST.CENT1 EQ '' THEN
            CALL F.READ(FN.FT.HIS,XX:REF,R.FT.HIS,F.FT.HIS,E3)
            COST.CENT = R.FT.HIS<FT.PROFIT.CENTRE.DEPT>
            O.DATA = COST.CENT
        END

    END ELSE

        O.DATA = ''
    END
*    IF XX[1,2] EQ 'IN' THEN
*          CALL F.READ(FN.IN,XX,R.IN,F.IN,E4)

*    END

RETURN
END
