* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE

*-----------------------------------------------------------------------------
* <Rating>-2</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.AC.SAVING
 
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ACCOUNT.OPEN.BAL


*   CALL DBR("SCB.AC.OPEN.BAL":@FM:1,O.DATA,ID)
 *  IF ID THEN
       XXX = TODAY[5,2]
         IF XXX < 10 THEN
            MNTH = TRIM(XXX, '0', 'L')
         END ELSE
            MNTH = XXX
         END

*Line [ 47 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*         CALL DBR("SCB.ACCOUNT.OPEN.BAL":@FM:MNTH,O.DATA,BAL)
F.ITSS.SCB.ACCOUNT.OPEN.BAL = 'F.SCB.ACCOUNT.OPEN.BAL'
FN.F.ITSS.SCB.ACCOUNT.OPEN.BAL = ''
CALL OPF(F.ITSS.SCB.ACCOUNT.OPEN.BAL,FN.F.ITSS.SCB.ACCOUNT.OPEN.BAL)
CALL F.READ(F.ITSS.SCB.ACCOUNT.OPEN.BAL,O.DATA,R.ITSS.SCB.ACCOUNT.OPEN.BAL,FN.F.ITSS.SCB.ACCOUNT.OPEN.BAL,ERROR.SCB.ACCOUNT.OPEN.BAL)
BAL=R.ITSS.SCB.ACCOUNT.OPEN.BAL<MNTH>
  * END 
      O.DATA = BAL

 
RETURN
END
