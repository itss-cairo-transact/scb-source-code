* @ValidationCode : MjotOTY5NjAzOTAzOkNwMTI1MjoxNjQwNjk0MTcyMjMxOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:22:52
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>164</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CHQ

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.TELLER
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_TT.LOCAL.REFS


    FN.CR='FBNK.STMT.ENTRY';F.CR=''
    CALL OPF(FN.CR,F.CR)
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS';F.LD=''
    CALL OPF(FN.LD,F.LD)
    FN.LDH='FBNK.LD.LOANS.AND.DEPOSITS$HIS';F.LDH=''
    CALL OPF(FN.LDH,F.LDH)
    FN.TT='FBNK.TELLER';F.TT=''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H='FBNK.TELLER$HIS';F.TT.H=''
    CALL OPF(FN.TT.H,F.TT.H)

    XX  = O.DATA
    CALL F.READ(FN.CR,XX,R.CR,F.CR,E2)
    REF   = R.CR<AC.STE.OUR.REFERENCE>
******************ADDED BY MAHMOUD 10/12/2009***************
    IF REF EQ '' THEN
        REF   = R.CR<AC.STE.TRANS.REFERENCE>
*Line [ 68 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR '\B' IN REF SETTING Y.XX THEN REF = FIELD(REF,'\',1) ELSE NULL
    END
************************************************************
    ACS   = R.CR<AC.STE.SYSTEM.ID>
    THIER = R.CR<AC.STE.THEIR.REFERENCE>

    IF REF[1,2] EQ 'BR' THEN
*Line [ 76 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,REF,LOCAL)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,REF,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
LOCAL=R.ITSS.BILL.REGISTER<EB.BILL.REG.LOCAL.REF>
        CHQNO  = LOCAL<1,BRLR.BILL.CHQ.NO>
        O.DATA = CHQNO
    END

    IF REF[1,2] EQ 'FT' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.FT.HIS,REF2,R.FT.HIS,F.FT.HIS,E2)
****************UPDATED BY MAHMOUD 15/1/2013*********************
        IF R.FT.HIS<FT.ORDERING.BANK> EQ 'ATM' THEN
            ATM.REF = R.FT.HIS<FT.LOCAL.REF,FTLR.TERMINAL.LOC>
            ATM.REF = CHANGE(ATM.REF,'-',' ')
            ATM.REF = TRIM(ATM.REF)
            CHQNO2 = CHANGE(ATM.REF,' ','-')
        END ELSE
            CHQNO2 = R.FT.HIS<FT.LOCAL.REF><1,FTLR.CHEQUE.NO>
        END
        O.DATA = CHQNO2
**************** END OF UPDATE **********************************

        IF O.DATA EQ '' THEN
            CALL F.READ(FN.FT,REF,R.FT,F.FT,E2)
****************UPDATED BY MAHMOUD 15/1/2013*********************
            IF R.FT<FT.ORDERING.BANK> EQ 'ATM' THEN
                ATM.REF = R.FT<FT.LOCAL.REF,FTLR.TERMINAL.LOC>
                ATM.REF = CHANGE(ATM.REF,'-',' ')
                ATM.REF = TRIM(ATM.REF)
                CHQNO3 = CHANGE(ATM.REF,' ','-')
            END ELSE
                CHQNO3 = R.FT<FT.LOCAL.REF><1,FTLR.CHEQUE.NO>
            END
            O.DATA = CHQNO3
**************** END OF UPDATE **********************************
        END
    END
******************ADDED BY MAHMOUD 10/12/2009***************
    IF REF[1,2] EQ 'LD' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.LDH,REF2,R.LDH,F.LDH,E22)
        LGNO4 = R.LDH<LD.LOCAL.REF><1,LDLR.OLD.NO>
        O.DATA = LGNO4
        IF LGNO4 EQ '' THEN
            CALL F.READ(FN.LD,REF,R.LD,F.LD,E22)
            LGNO4 = R.LD<LD.LOCAL.REF><1,LDLR.OLD.NO>
            O.DATA = LGNO4
        END
    END

    IF REF[1,2] EQ 'TT' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.TT.H,REF2,R.TT.H,F.TT.H,E223)
        CHQNO2T = R.TT.H<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
        O.DATA = CHQNO2T
        IF CHQNO2T EQ '' THEN
            CALL F.READ(FN.TT,REF,R.TT,F.TT,E232)
            CHQNO3T = R.TT<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
            O.DATA = CHQNO3T
        END
    END
*************************************************************
    IF REF[1,2] NE 'BR' AND REF[1,2] NE 'FT' AND REF[1,2] NE 'LD' AND REF[1,2] NE 'TT' THEN
        O.DATA = THIER
    END
    O.DATA = O.DATA[1,40]
RETURN
END
