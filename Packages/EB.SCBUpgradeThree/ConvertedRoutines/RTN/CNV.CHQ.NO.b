* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.CHQ.NO

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS

    FN.CR='FBNK.STMT.ENTRY';F.CR=''
    CALL OPF(FN.CR,F.CR)
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.LD='FBNK.LD.LOANS.AND.DEPOSITS';F.LD=''
    CALL OPF(FN.LD,F.LD)
    FN.LDH='FBNK.LD.LOANS.AND.DEPOSITS';F.LDH=''
    CALL OPF(FN.LDH,F.LDH)
    FN.TT='FBNK.TELLER';F.TT=''
    CALL OPF(FN.TT,F.TT)
    FN.TT.H='FBNK.TELLER$HIS';F.TT.H=''
    CALL OPF(FN.TT.H,F.TT.H)


    XX  = O.DATA
    CALL F.READ(FN.CR,XX,R.CR,F.CR,E2)
    REF   = R.CR<AC.STE.OUR.REFERENCE>
******************ADDED BY MAHMOUD 10/12/2009***************
    IF REF EQ '' THEN
        REF   = R.CR<AC.STE.TRANS.REFERENCE>
*Line [ 65 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR '\B' IN REF SETTING Y.XX THEN REF = FIELD(REF,'\',1) ELSE NULL
    END
************************************************************
    ACS   = R.CR<AC.STE.SYSTEM.ID>
    THIER = R.CR<AC.STE.THEIR.REFERENCE>

    IF REF[1,2] EQ 'BR' THEN
*Line [ 73 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('BILL.REGISTER':@FM:EB.BILL.REG.LOCAL.REF,REF,LOCAL)
F.ITSS.BILL.REGISTER = 'FBNK.BILL.REGISTER'
FN.F.ITSS.BILL.REGISTER = ''
CALL OPF(F.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER)
CALL F.READ(F.ITSS.BILL.REGISTER,REF,R.ITSS.BILL.REGISTER,FN.F.ITSS.BILL.REGISTER,ERROR.BILL.REGISTER)
LOCAL=R.ITSS.BILL.REGISTER<EB.BILL.REG.LOCAL.REF>
        CHQNO  = LOCAL<1,BRLR.BILL.CHQ.NO>
        O.DATA = CHQNO
    END

    IF REF[1,2] EQ 'FT' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.FT.HIS,REF2,R.FT.HIS,F.FT.HIS,E2)
        CHQNO2 = R.FT.HIS<FT.LOCAL.REF><1,FTLR.CHEQUE.NO>
        O.DATA = CHQNO2
        IF CHQNO2 EQ '' THEN
            CALL F.READ(FN.FT,REF,R.FT,F.FT,E2)
            CHQNO3 = R.FT<FT.LOCAL.REF><1,FTLR.CHEQUE.NO>
            O.DATA = CHQNO3
        END
    END
******************ADDED BY MAHMOUD 10/12/2009***************
    IF REF[1,2] EQ 'LD' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.LDH,REF2,R.LDH,F.LDH,E22)
        LGNO4 = R.LDH<LD.LOCAL.REF><1,LDLR.OLD.NO>
        O.DATA = LGNO4
        IF LGNO4 EQ '' THEN
            CALL F.READ(FN.LD,REF,R.LD,F.LD,E22)
            LGNO4 = R.LD<LD.LOCAL.REF><1,LDLR.OLD.NO>
            O.DATA = LGNO4
        END
    END

    IF REF[1,2] EQ 'TT' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.TT.H,REF2,R.TT.H,F.TT.H,E223)
        CHQNO2T = R.TT.H<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
        O.DATA = CHQNO2T
        IF CHQNO2T EQ '' THEN
            CALL F.READ(FN.TT,REF,R.TT,F.TT,E232)
            CHQNO3T = R.TT<TT.TE.LOCAL.REF,TTLR.CHEQUE.NO>
            O.DATA = CHQNO3T
        END
    END
    IF REF[1,2] NE 'BR' AND REF[1,2] NE 'FT' AND REF[1,2] NE 'LD' AND REF[1,2] NE 'TT' THEN
**    IF REF[1,2] NE 'BR' AND REF[1,2] NE 'FT' THEN
*************************************************************
        O.DATA = THIER
    END

    RETURN
END
