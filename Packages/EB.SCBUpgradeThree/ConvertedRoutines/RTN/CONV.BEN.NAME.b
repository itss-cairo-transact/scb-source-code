* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.BEN.NAME
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT

    XX = O.DATA
    FN.SCB = 'FBNK.LETTER.OF.CREDIT'
    F.SCB = '' ; R.SCB = ''
    CALL OPF(FN.SCB,F.SCB)
    ID = O.DATA[1,12]
**********************************************************
*|  T.SEL  = "SELECT FBNK.LETTER.OF.CREDIT WITH @ID EQ ":ID
*   CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED, ETEXT)
*   IF SELECTED NE '' THEN
*       FOR I = 1 TO SELECTED

    CALL F.READ(FN.SCB,ID,R.SCB,F.SCB,E1)
    BEN1 = R.SCB<TF.LC.BENEFICIARY.CUSTNO>
    BEN2 = R.SCB<TF.LC.BENEFICIARY,1>
    IF BEN1 EQ  '' THEN
        CUST.NO = BEN2
    END ELSE
        CUST.NO = BEN1
    END
    O.DATA = CUST.NO
*   NEXT I
    RETURN
END
