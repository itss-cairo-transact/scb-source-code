* @ValidationCode : MjotMTQzODU4MDIwMTpDcDEyNTI6MTY0MDY4Mjk2NjQ1MDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 11:16:06
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.AMT.COLL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER.POSITION
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS

    CU   = FIELD(O.DATA,"*",1)
    CATG = FIELD(O.DATA,"*",2)
    CURRR = FIELD(O.DATA,"*",3)

    IF CATG GE 21001 AND CATG LE 21030 THEN
        FN.LD ='FBNK.LD.LOANS.AND.DEPOSITS' ;R.LD='';F.LD=''
        CALL OPF(FN.LD,F.LD)

        FN.COLL = 'FBNK.COLLATERAL' ; R.COLL = '' ; F.COLL = ''
        CALL OPF(FN.COLL,F.COLL)

        T.SEL    = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT GT 0 AND COLLATERAL.ID NE '' AND  CUSTOMER.ID EQ ":CU:" AND CATEGORY EQ ":CATG :" AND CURRENCY EQ ": CURRR
        KEY.LIST = ""
        SELECTED = ""
        ER.MSG   = ""
        HHH      = '0'
***     AMT      = '0'
        CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
***TEXT = "SEL== " : SELECTED ; CALL REM

        IF SELECTED = 0 THEN O.DATA = ""
        COLL.AMT = ""
        O.DATA = ""
        FOR I = 1 TO SELECTED
***  TEXT = "KEY=  " : KEY.LIST<I> ; CALL REM
            CALL F.READ(FN.LD,KEY.LIST<I>, R.LD, F.LD,E)

            COL.ID = R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>

***   TEXT = "COL.ID=  " : COL.ID ; CALL REM
            CURR = R.LD<LD.CURRENCY>
            CALL F.READ(FN.COLL,COL.ID, R.COLL, F.COLL,E1)
            COLL.AMT = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
*Line [ 81 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            IF CURR = "EGP" THEN MID.RATE = 1
            IF CURR = "JPY" THEN MID.RATE = MID.RATE<1,1> / 100
***TEXT = "COL.AMT=  " : COLL.AMT ; CALL REM
            HHH  =  (MID.RATE<1> * COLL.AMT)
            AMT  = AMT + HHH
        NEXT I
        O.DATA = AMT
***TEXT = "O.DATA" : O.DATA ; CALL REM
    END
RETURN
END
