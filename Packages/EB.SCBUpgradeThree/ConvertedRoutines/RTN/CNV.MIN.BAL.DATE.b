* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************************MAHMOUD 18/3/2013*************************
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.MIN.BAL.DATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    XX1 = ''
    XX1 = O.DATA
    ACCT.MIN.BAL = ''
    ACCT.DAY.NO  = ''
    EXC.RATE    = 1
    MIN.LCY.AMT = 2000
    MIN.USD.AMT = 500
    ACCT.ID = FIELD(XX1,'-',1)
    MNTH    = FIELD(XX1,'-',2)


    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = '' ; ERR.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.ACCT = 'FBNK.ACCT.ACTIVITY' ; F.ACCT = '' ; R.ACCT = '' ; ERR.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    FN.CURR = 'FBNK.CURRENCY' ; F.CURR = '' ; R.CURR = '' ; ERR.CURR = ''
    CALL OPF(FN.CURR,F.CURR)


*Line [ 53 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT.ID,ACC.CUR)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.CUR=R.ITSS.ACCOUNT<AC.CURRENCY>
*Line [ 60 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,'USD',USD.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,'USD',R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
USD.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
    USD.RATE = USD.RATE<1,1>

    GOSUB GET.CCY.MIN.BAL

    CALL F.READ(FN.ACCT,XX1,R.ACCT,F.ACCT,ERR1)
*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CNT.ACCT = DCOUNT(R.ACCT<IC.ACT.BALANCE>,@VM)
    FOR AA1 = 1 TO CNT.ACCT
        ACCT.BAL.DATE = R.ACCT<IC.ACT.DAY.NO,AA1>
        ACCT.BAL.AMT  = R.ACCT<IC.ACT.BALANCE,AA1>
        IF ACCT.BAL.AMT LT MIN.BAL.AMT THEN
            MLT.LOC = AA1
            AA1 = CNT.ACCT
        END
    NEXT AA1
    ACCT.MIN.BAL = ACCT.BAL.AMT
    ACCT.DAY.NO  = MNTH:ACCT.BAL.DATE
    IF ACCT.BAL.AMT EQ '' THEN
*Line [ 85 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.ONLINE.ACTUAL.BAL,ACCT.ID,ACC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.BAL=R.ITSS.ACCOUNT<AC.ONLINE.ACTUAL.BAL>
*Line [ 92 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('ACCOUNT':@FM:AC.OPENING.DATE,ACCT.ID,ACC.OPEN)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACCT.ID,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACC.OPEN=R.ITSS.ACCOUNT<AC.OPENING.DATE>
        ACCT.BAL.AMT = ACC.BAL
        ACCT.DAY.NO  = ACC.OPEN
    END
    O.DATA = ACCT.BAL.AMT:"*":ACCT.DAY.NO
    RETURN
****************************************************
GET.CCY.MIN.BAL:
*-----------
    IF ACC.CUR EQ 'EGP' THEN
        MIN.BAL.AMT = MIN.LCY.AMT
    END ELSE
        IF ACC.CUR EQ 'USD' THEN
            MIN.BAL.AMT = MIN.USD.AMT
        END ELSE
            CALL F.READ(FN.CURR,ACC.CUR,R.CURR,F.CURR,ERR.CURR)
            CURR.RATE = R.CURR<EB.CUR.MID.REVAL.RATE,1>
            IF ACC.CUR EQ 'JPY' THEN
                CURR.RATE = CURR.RATE /100
            END
            IF CURR.RATE NE 0 AND CURR.RATE NE '' THEN
                EXC.RATE = USD.RATE / CURR.RATE
            END
            MIN.BAL.AMT = MIN.USD.AMT * EXC.RATE
        END
    END
    RETURN
****************************************************
END
