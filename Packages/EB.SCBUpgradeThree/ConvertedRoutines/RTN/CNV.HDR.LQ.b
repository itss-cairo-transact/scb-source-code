* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*********************MAHMOUD 13/6/2016***************************
*-----------------------------------------------------------------------------
* <Rating>-80</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.HDR.LQ

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY

    MIN.DR.BAL = 0
    MIN.BAL.AC = 0
    Y.MIN.BAL  = ''
    Y.ACC.ID = O.DATA

    FN.ACC.ACT = 'FBNK.ACCT.ACTIVITY' ; F.ACC.ACT = ''
    CALL OPF(FN.ACC.ACT,F.ACC.ACT)
*------------------------------------------
    TD1  = TODAY
    IF TD1[5,2] = '01' OR TD1[5,2] = '02' OR TD1[5,2] = '03' THEN
        TDLY = TD1[1,4] - 1
        FRDT = TDLY:'1001'
        TODT = TDLY:'1231'
        CALL LAST.WDAY(TODT)
    END
    IF TD1[5,2] = '04' OR TD1[5,2] = '05' OR TD1[5,2] = '06' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0101'
        TODT = TDCY:'0331'
        CALL LAST.WDAY(TODT)
    END
    IF TD1[5,2] = '07' OR TD1[5,2] = '08' OR TD1[5,2] = '09' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0401'
        TODT = TDCY:'0630'
        CALL LAST.WDAY(TODT)
    END
    IF TD1[5,2] = '10' OR TD1[5,2] = '11' OR TD1[5,2] = '12' THEN
        TDCY = TD1[1,4]
        FRDT = TDCY:'0701'
        TODT = TDCY:'0930'
        CALL LAST.WDAY(TODT)
    END
*------------------------------------------

    KK1 = 0
    CALL GET.ENQ.BALANCE(Y.ACC.ID,FRDT,Y.OPEN.BAL)
    KK1++
    Y.MIN.BAL<KK1> = Y.OPEN.BAL
    ACT.DATE = FRDT
    LOOP
    WHILE ACT.DATE LE TODT
        ACT.MNTH = ACT.DATE[1,6]
        ACC.ACT.ID = Y.ACC.ID:"-":ACT.MNTH
        CALL F.READ(FN.ACC.ACT,ACC.ACT.ID,R.ACC.ACT,F.ACC.ACT,Y.ERR1)
        IF NOT(Y.ERR1) THEN
            MIN.BAL.AC = MINIMUM(R.ACC.ACT<IC.ACT.BALANCE>)
            IF MIN.BAL.AC NE '' AND MIN.BAL.AC LT 0 THEN
                KK1++
                Y.MIN.BAL<KK1> = MIN.BAL.AC
            END
        END
        CALL ADD.MONTHS(ACT.DATE,'1')
    REPEAT
    MIN.DR.BAL = MINIMUM(Y.MIN.BAL)
    IF  MIN.DR.BAL  LT 0 THEN
        O.DATA = MIN.DR.BAL
    END ELSE
        O.DATA = ''
    END
    RETURN
END
