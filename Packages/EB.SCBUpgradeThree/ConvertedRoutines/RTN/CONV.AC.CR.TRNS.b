* @ValidationCode : MjotMTk1MDM3MjEwNjpDcDEyNTI6MTY0MTczNTQ3OTU0OTpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 09 Jan 2022 15:37:59
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.AC.CR.TRNS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.STMT.ENTRY
*$INCLUDE I_F.ACCT.ENT.TODAY
*$INCLUDE I_F.STMT.PRINTED


    FN.STMT.P  = 'FBNK.STMT.PRINTED' ; F.STMT.P = '' ; R.STMT.P = ''
    CALL OPF(FN.STMT.P,F.STMT.P)
    ST.AMT   = 0
    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)


    CALL F.READ( FN.STMT.P,O.DATA, R.STMT.P,F.STMT.P, ETEXT11)
    IF NOT(ETEXT11) THEN
        LOOP

            REMOVE TRNS.NO FROM R.STMT.P  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)

            IF R.STMT<AC.STE.AMOUNT.LCY> GT 0 THEN
                ST.AMT += R.STMT<AC.STE.AMOUNT.LCY>
            END
        REPEAT
    END
    O.DATA = ST.AMT
RETURN
END
