* @ValidationCode : MjotMTIxOTgzODg5MDpDcDEyNTI6MTY0MDYxMzQ4MDE1MjpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 27 Dec 2021 15:58:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************************NI7OOOOOOOOOOOOOO************************
SUBROUTINE CNV.CU

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
*Line [ 28 ] Hashing $INCLUDE I_F.SCB.RETIREMENTS - ITSS - R21 Upgrade - 2021-12-23
*$INCLUDE I_F.SCB.RETIREMENTS


    FN.AC = 'FBNK.ACCOUNT' ;  F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.AC.HIS = 'FBNK.ACCOUNT$HIS' ;  F.AC.HIS = ''
    CALL OPF(FN.AC.HIS,F.AC.HIS)

    CALL F.READ(FN.AC,O.DATA,R.AC,F.AC,ETEXT)
    IF NOT(ETEXT) THEN
        CUS.NO = R.AC<AC.CUSTOMER>
        PL.NO  = R.AC<AC.CATEGORY>

        IF CUS.NO NE '' THEN
            O.DATA = CUS.NO
        END
        IF CUS.NO EQ '' THEN
            O.DATA = PL.NO
        END

    END
    IF ETEXT THEN
        YY = O.DATA:';1'
        CALL F.READ(FN.AC.HIS,YY,R.AC.HIS,F.AC.HIS,ETEXT)
        CUS.NO.HIS = R.AC.HIS<AC.CUSTOMER>
        PL.NO.HIS  = R.AC.HIS<AC.CATEGORY>

        IF CUS.NO.HIS NE '' THEN
            O.DATA = CUS.NO.HIS
        END
        IF PL.NO.HIS EQ '' THEN
            O.DATA = PL.NO.HIS
        END

    END
RETURN
END
