* @ValidationCode : Mjo4NDY2MzEzNzI6Q3AxMjUyOjE2NDA2OTUwMzU0MDk6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:37:15
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CUST.NAME
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.POSSESS

    XX = O.DATA
    FN.SCB = 'F.SCB.POSSESS'
    F.SCB = '' ; R.SCB = ''
    CALL OPF(FN.SCB,F.SCB)
    POSSESS.ID = "...":O.DATA
**********************************************************
    T.SEL  = "SELECT F.SCB.POSSESS WITH CUST.CODE EQ ":O.DATA
    CALL EB.READLIST(T.SEL,KEY.LIST, "", SELECTED, ETEXT)
    IF SELECTED NE '' THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.SCB,KEY.LIST<I>,R.SCB,F.SCB,E1)
            CUST.NAME = R.SCB<POSS.CUST.NAME>
            O.DATA = CUST.NAME
        NEXT I
    END
RETURN
END
