* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.NOTE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    FN.CR='FBNK.STMT.ENTRY';F.CR=''
    CALL OPF(FN.CR,F.CR)
    FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
    CALL OPF(FN.FT,F.FT)
    FN.FT.HIS='FBNK.FUNDS.TRANSFER$HIS';F.FT.HIS=''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.IN = "F.INF.MULTI.TXN" ; F.IN = ''
    CALL OPF(FN.IN,F.IN)
    FN.INH = "F.INF.MULTI.TXN$HIS" ; F.INH = ''
    CALL OPF(FN.INH,F.INH)


    XX  = O.DATA
    CALL F.READ(FN.CR,XX,R.CR,F.CR,E2)
    REF   = R.CR<AC.STE.OUR.REFERENCE>
******************ADDED BY MAHMOUD 10/12/2009***************
    IF REF EQ '' THEN
        REF   = R.CR<AC.STE.TRANS.REFERENCE>
*Line [ 53 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR '\B' IN REF SETTING Y.XX THEN REF = FIELD(REF,'\',1) ELSE NULL
    END
************************************************************
    ACS   = R.CR<AC.STE.SYSTEM.ID>
    THIER = R.CR<AC.STE.THEIR.REFERENCE>

    CALL F.READ(FN.FT.HIS,REF2,R.FT.HIS,F.FT.HIS,E2)
    CALL F.READ(FN.FT,REF,R.FT,F.FT,E2)

    IF REF[1,2] EQ 'FT' THEN
        REF2 = REF:';1'


        CALL F.READ(FN.FT.HIS,REF2,R.FT.HIS,F.FT.HIS,E2)
        NOTE  = R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT,1>
        NOTE := R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT,2>

        O.DATA = NOTE
*        BEGIN CASE
*        CASE NOTE EQ ''
*            NOTE.1  = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT,1>
*            NOTE.1 := R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT,2>
*            O.DATA = NOTE.1
*        CASE NOTE EQ '' AND NOTE.1 EQ ''
*            NOTE.2  = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,1>
*            NOTE.2 := R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,2>
*            O.DATA = NOTE.2
*        CASE NOTE EQ '' AND NOTE.1 EQ '' AND NOTE.2 EQ ''
*            NOTE.3   = R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,1>
*            NOTE.3  := R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,2>
*            O.DATA = NOTE.3
*        END CASE


        IF NOTE EQ '' THEN
            CALL F.READ(FN.FT,REF,R.FT,F.FT,E2)
            NOTE.1  = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT,1>
            NOTE.1 := R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT,2>
            O.DATA = NOTE.1
        END
        IF NOTE EQ '' AND NOTE.1 EQ '' THEN
            CALL F.READ(FN.FT,REF,R.FT,F.FT,E2)
            NOTE.2  = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,1>
            NOTE.2 := R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,2>
            O.DATA = NOTE.2
        END
        IF NOTE EQ '' AND NOTE.1 EQ '' AND NOTE.2 EQ '' THEN
            NOTE.3   = R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,1>
            NOTE.3  := R.FT.HIS<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED,2>
            O.DATA = NOTE.3
        END

    END
    IF REF[1,2] EQ 'IN' THEN
        REF2 = REF:';1'
        CALL F.READ(FN.IN,REF,R.IN,F.IN,E000)
        CALL F.READ(FN.INH,REF2,R.INH,F.INH,E001)

        NOTE.4 = R.IN<INF.MLT.THEIR.REFERENCE>
        O.DATA = NOTE.4
        IF NOTE.4 EQ '' THEN
            NOTE.5 = R.INH<INF.MLT.THEIR.REFERENCE>
            O.DATA = NOTE.5
        END
    END
    IF REF[1,2] NE 'IN' AND REF[1,2] NE 'FT' THEN
        O.DATA = THIER
    END

    RETURN
END
