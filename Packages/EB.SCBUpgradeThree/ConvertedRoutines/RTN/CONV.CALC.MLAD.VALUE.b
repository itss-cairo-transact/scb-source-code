* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CALC.MLAD.VALUE

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D
*----------------------------------------------
    RECIEVE.ID = O.DATA
    FN.TMP     = "F.SCB.MLAD.FILE.PARAM"   ; F.TMP = ""
    CALL OPF(FN.TMP,F.TMP)

    FN.MAS = "F.CATEG.MAS.D"  ; F.MAS = ""
    CALL OPF(FN.MAS,F.MAS)
*----------------------------------------------
    PERC.VAL  = FIELD(RECIEVE.ID,'*',1)
    TMP.ID    = FIELD(RECIEVE.ID,'*',2)
    MLAD.CUR  = FIELD(RECIEVE.ID,'*',3)

*    TEXT = RECIEVE.ID  ; CALL REM

    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,EER.TMP)

    G.LINE  = R.TMP<MLPAR.GENLED.LINE.NO>
    CAT.FRM = R.TMP<MLPAR.CATEGORY.FROM>
    CAT.TO  = R.TMP<MLPAR.CATEGORY.TO>
*----
    NXT.DY = R.TMP<MLPAR.NEXT.DAY>
    NXT.WK = R.TMP<MLPAR.NEXT.WEEK>
    NXT.M  = R.TMP<MLPAR.NEXT.MONTH>
    NXT.M3 = R.TMP<MLPAR.NEXT.3MONTH>
    NXT.M6 = R.TMP<MLPAR.NEXT.6MONTH>
    NXT.Y  = R.TMP<MLPAR.NEXT.YEAR>
    NXT.Y3 = R.TMP<MLPAR.NEXT.3YEAR>
    NXT.MR = R.TMP<MLPAR.NEXT.MORE>
*----
    G.VALUE = 0
    IF G.LINE NE '' THEN
        N.SEL  = "SELECT ":FN.MAS:" WITH CATD.ASST.LIAB EQ ":G.LINE
        N.SEL := " AND CATD.CY.ALPH.CODE EQ ":MLAD.CUR
        CALL EB.READLIST(N.SEL,KEY.LIST,"",SELECTED,ER.MSG)

        IF SELECTED THEN
            FOR II = 1 TO SELECTED
                CALL F.READ(FN.MAS,KEY.LIST<II>,R.MAS,F.MAS,EER.MAS)
                G.VALUE += R.MAS<CAT.CATD.BAL.IN.ACT.CY>
            NEXT II
        END

    END ELSE
        N.SEL  = "SELECT ":FN.MAS:" WITH CATD.CATEG.CODE GE ":CAT.FRM
        N.SEL := " AND CATD.CATEG.CODE LE ":CAT.TO
        N.SEL := " AND CATD.CY.ALPH.CODE EQ ":MLAD.CUR
        CALL EB.READLIST(N.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR II = 1 TO SELECTED
                CALL F.READ(FN.MAS,KEY.LIST<II>,R.MAS,F.MAS,EER.MAS)
                G.VALUE += R.MAS<CAT.CATD.BAL.IN.ACT.CY>
            NEXT II
        END
    END
*=============================================================
*** EDIT BY NESSMA ON 2012/01/18 ***
    IF G.LINE EQ '0120' OR G.LINE EQ '0630' THEN
        N.SEL  = "SELECT ":FN.MAS:" WITH CATD.ASST.LIAB EQ ":G.LINE
*        N.SEL := " AND CATD.CY.ALPH.CODE EQ ":MLAD.CUR
        CALL EB.READLIST(N.SEL,KEY.LIST,"",SELECTED,ER.MSG)
        IF SELECTED THEN
            FOR II = 1 TO SELECTED
                CALL F.READ(FN.MAS,KEY.LIST<II>,R.MAS,F.MAS,EER.MAS)
                G.VALUE += R.MAS<CAT.CATD.BAL.IN.LOCAL.CY>
            NEXT II
        END
    END
*** END EDIT ON 2012/01/18 ***
*=============================================================

    G.VALUE = ( G.VALUE * PERC.VAL ) / 100
    O.DATA  = G.VALUE
    RETURN
