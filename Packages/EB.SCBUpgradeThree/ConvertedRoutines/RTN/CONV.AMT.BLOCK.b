* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.AMT.BLOCK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY

    CU    = FIELD(O.DATA,"*",1)
    CATG  = FIELD(O.DATA,"*",2)
    CURRR = FIELD(O.DATA,"*",3)
    FN.ACC ='FBNK.ACCOUNT' ;R.ACC='';F.ACC=''
    CALL OPF(FN.ACC,F.ACC)

    T.SEL  = "SELECT FBNK.ACCOUNT WITH CUSTOMER EQ ":CU:" AND CATEGORY EQ ":CATG:" AND CURRENCY EQ ":CURRR
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED = 0 THEN O.DATA = ""
    BLOCK.AMT = ""
    LOCK.AMT = ""
    O.DATA = ""
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.ACC,KEY.LIST<I>, R.ACC, F.ACC,E)
        LOCK.AMT = ""
        BLOCK.AMT = ""
        LOK = KEY.LIST<I>


*        CALL DBR( 'ACCOUNT':@FM:AC.LOCKED.AMOUNT,LOK,LOCK.AMT)
*        CALL DBR( 'ACCOUNT':@FM:AC.CURRENCY,LOK,CURR)

        LOCK.AMT = R.ACC<AC.LOCKED.AMOUNT>
        CURR = R.ACC<AC.CURRENCY>
        IF LOCK.AMT THEN
*Line [ 71 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            LOCK.NO=DCOUNT(LOCK.AMT,@VM)
            BLOCK.AMT  = BLOCK.AMT + LOCK.AMT<1,LOCK.NO>
*Line [ 74 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR( 'CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            IF CURR = "EGP" THEN MID.RATE = 1

*             HHH    = (MID.RATE<1> * BLOCK.AMT)
*            DDD    =( HHH + O.DATA)
*            O.DATA = DROUND(DDD,2)

            HHH  =  (MID.RATE<1> * BLOCK.AMT)
            O.DATA =   O.DATA + HHH

        END
    NEXT I

    RETURN
END
