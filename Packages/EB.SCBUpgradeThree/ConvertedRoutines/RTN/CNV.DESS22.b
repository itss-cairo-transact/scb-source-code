* @ValidationCode : Mjo1MTA2NTEyNjA6Q3AxMjUyOjE2NDE3MzMxOTQ1OTg6bGFwOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIwX1NQMy4wOi0xOi0x
* @ValidationInfo : Timestamp         : 09 Jan 2022 14:59:54
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*---------------------------------NI7OOOOOOOOOOO-------------------------------------------
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE CNV.DESS22

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.CUS.POS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CATEGORY
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_LD.LOCAL.REFS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DRAWINGS


    FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
    CALL OPF( FN.LC,F.LC)

    FN.DR   = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
    CALL OPF( FN.DR,F.DR)

    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS' ; F.LD = '' ; R.LD= ''
    CALL OPF( FN.LD,F.LD)

    FN.AC   = 'FBNK.ACCOUNT' ; F.AC = '' ; R.AC = ''
    CALL OPF( FN.AC,F.AC)

    FN.CU   = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF( FN.CU,F.CU)


    FN.IND.LD = 'FBNK.LMM.CUSTOMER' ; F.IND.LD = '' ; R.IND.LD = '' ; ER.IND.LD = ''
    CALL OPF(FN.IND.LD,F.IND.LD)

    YY = O.DATA
    XX = FIELD(YY,'*',3)
    ZZ = FIELD(YY,'*',2)
    O.DATA = ''
    IF XX GE 21021 AND XX LE 21032 THEN
*Line [ 72 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,ZZ,CUS.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,ZZ,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
        CALL F.READ(FN.IND.LD,ZZ,R.IND.LD,F.IND.LD,ER.IND.LD)
        LOOP
            REMOVE LD.ID FROM R.IND.LD SETTING POS.LD
        WHILE LD.ID:POS.LD
            IF LD.ID[1,2] EQ 'LD' THEN
                CALL F.READ(FN.LD,LD.ID,R.LD,F.LD,E2)

                ACLD    = R.LD<LD.PRIN.LIQ.ACCT>
                CATE.LD = R.LD<LD.CATEGORY>
                CO.LD   = R.LD<LD.CO.CODE>
                IF CATE.LD GE 21021 AND CATE.LD LE 21032 THEN
*Line [ 90 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('ACCOUNT':@FM:AC.CUSTOMER,ACLD,CUS.LD)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACLD,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
CUS.LD=R.ITSS.ACCOUNT<AC.CUSTOMER>
*Line [ 97 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR ('CUSTOMER':@FM:EB.CUS.COMPANY.BOOK,CUS.LD,CUS.BOOK)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUS.LD,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
CUS.BOOK=R.ITSS.CUSTOMER<EB.CUS.COMPANY.BOOK>
                    IF CUS.BOOK EQ 'EG0010011' AND CO.LD NE 'EG0010011' THEN
*Line [ 105 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,BRN.CODE,YYBRN1)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,BRN.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
YYBRN1=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
                        O.DATA = " ������ ����� �� ���� ����"
* END ELSE
*    O.DATA = ''
                    END
* IF CUS.BOOK NE 'EG0010011' THEN
*    O.DATA = ''
* END
                END
*END ELSE
*   O.DATA = ''
            END
        REPEAT
* END ELSE
*    O.DATA = ''
    END

RETURN
END
