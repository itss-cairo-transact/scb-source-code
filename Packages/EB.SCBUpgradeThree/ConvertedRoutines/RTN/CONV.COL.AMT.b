* @ValidationCode : MjotMTEwODYyMDAyNjpDcDEyNTI6MTY0MDY5NDMyMDE5MDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 28 Dec 2021 14:25:20
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CONV.COL.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COLLATERAL
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LIMIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CO.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CURRENCY

    FN.CCY = 'FBNK.CURRENCY'
    F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
RETURN
INIT:
    COL.R.ID = O.DATA
*TEXT =COL.R.ID ; CALL REM
    FN.COL = "FBNK.COLLATERAL"
    F.COL  = '' ; R.COL  = '' ; COLL = 0 ; X.COLL = ''
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.LIM = 'FBNK.LIMIT'
    F.LIM = '' ; R.LIM = ''
RETURN
OPEN:
    CALL OPF(FN.COL,F.COL)
*  CALL OPF(FN.LIM,F.LIM)
RETURN
PROCESS:
************************************************************
    T.SEL  = "SELECT FBNK.COLLATERAL WITH @ID LIKE ":COL.R.ID:"..."
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.COL,KEY.LIST<I>,R.COL,F.COL,EER)
            CY.CODE =  R.COL<COLL.CURRENCY>
            COL.AMT =  R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
            IF CY.CODE NE 'EGP' THEN
                CALL F.READ(FN.CCY,CY.CODE,R.CCY,F.CCY,E1)
                RATE = R.CCY<EB.CUR.MID.REVAL.RATE,1>
                WS.COL.AMT = RATE * COL.AMT
            END ELSE
                WS.COL.AMT = 1 * COL.AMT
            END
            COL.AMT.P += WS.COL.AMT
        NEXT I
    END
    O.DATA = COL.AMT.P
RETURN
END
