* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.ACC.CUS

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
    COMP = ID.COMPANY

*----------------------------------------------------------------------**
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    CUS.ID = O.DATA
    IF CUS.ID[1,1] EQ 0 THEN
        CUS.ID = CUS.ID[2,7]
    END
***--------------------------------------------------------------****
    FN.CUS.AC ='FBNK.CUSTOMER.ACCOUNT' ;R.CUS.AC = ''; F.CUS.AC=''
    CALL OPF(FN.CUS.AC,F.CUS.AC)
    CALL F.READ( FN.CUS.AC,CUS.ID, R.CUS.AC, F.CUS.AC,ETEXT1)
    LOOP
        REMOVE ACC FROM R.CUS.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CATEG   = R.ACC<AC.CATEGORY>
        IF AC.CATEG EQ '1560'  THEN
            XXX = ACC
        END ELSE
            XXX = "0"
        END
    REPEAT
    O.DATA = XXX
    RETURN
*==============================================================********
END
