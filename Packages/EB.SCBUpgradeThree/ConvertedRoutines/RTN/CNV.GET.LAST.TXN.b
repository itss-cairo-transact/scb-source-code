* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
********************MAHMOUD 24/3/2013**************************
    SUBROUTINE CNV.GET.LAST.TXN

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    XX1 = ''
    ARR1 = ''
    TXN.LAST.DATE = ''
    XX1 = O.DATA
    ACCT.ID = XX1
    FN.ACC = "FBNK.ACCOUNT" ; F.ACC = "" ; R.ACC = "" ; ER.ACC1 = ""
    CALL OPF(FN.ACC,F.ACC)

    CALL F.READ(FN.ACC,ACCT.ID,R.ACC,F.ACC,ER.ACC1)
    ARR1<1> = R.ACC<AC.DATE.LAST.CR.CUST>
    ARR1<2> = R.ACC<AC.DATE.LAST.DR.CUST>
    ARR1<3> = R.ACC<AC.DATE.LAST.CR.BANK>
    ARR1<4> = R.ACC<AC.DATE.LAST.DR.BANK>
    ARR1<5> = R.ACC<AC.DATE.LAST.CR.AUTO>
    ARR1<6> = R.ACC<AC.DATE.LAST.DR.AUTO>

    TXN.LAST.DATE = MAXIMUM(ARR1)

    O.DATA = TXN.LAST.DATE

    RETURN
END
