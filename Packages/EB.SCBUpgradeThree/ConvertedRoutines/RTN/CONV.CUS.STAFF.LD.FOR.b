* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CUS.STAFF.LD.FOR
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
********************************************************************
    CU6 = O.DATA

    FN.LD ='FBNK.LD.LOANS.AND.DEPOSITS' ;R.LD='';F.LD=''
    CALL OPF(FN.LD,F.LD)

    FN.LMM ='FBNK.LMM.CUSTOMER' ; F.LMM='' ; R.LMM=''
    CALL OPF(FN.LMM,F.LMM)

    FN.CCY = 'FBNK.RE.BASE.CCY.PARAM'; F.CCY = '' ; R.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    DAT = TODAY

*************************************************************
    CALL F.READ(FN.LMM,CU6,R.LMM,F.LMM,ETEXT1)

    LOOP

        REMOVE ACC FROM R.LMM  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.LD,ACC,R.LD,F.LD,ERR1)
        LD.CURR    = R.LD<LD.CURRENCY>
        CATEGORY   = R.LD<LD.CATEGORY>
        VALUE.DATE = R.LD<LD.VALUE.DATE>
        FIN.MAT.DATE = R.LD<LD.FIN.MAT.DATE>
        AMT.CUR = R.LD<LD.REIMBURSE.AMOUNT>
        IF LD.CURR NE 'EGP' THEN
            IF (CATEGORY GE 21001 AND CATEGORY LE 21010) AND (( VALUE.DATE LE ":DAT:" AND AMT.CUR NE 0 ) OR ( FIN.MAT.DATE GT ":DAT:" AND AMT.CUR EQ 0 )) THEN

                CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)
*Line [ 73 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                LOCATE LD.CURR IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                RATE = R.CCY<RE.BCP.RATE,POS>
                AMT.CUR = R.LD<LD.REIMBURSE.AMOUNT> * RATE
                AMT.CUR1     += AMT.CUR

            END
        END
    REPEAT
    O.DATA = AMT.CUR1
**--------------------------------------------------------------------**

    RETURN
END
