* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*************************MAHMOUD 26/9/2013 *********************
    SUBROUTINE CNV.TERMINAL.LOC

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    ST.ID = O.DATA

    FN.ST = 'FBNK.STMT.ENTRY' ; F.ST = '' ; R.ST = ''
    CALL OPF( FN.ST,F.ST)
    CALL F.READ( FN.ST,ST.ID, R.ST, F.ST, ETEXT)
    REF   = R.ST<AC.STE.OUR.REFERENCE>
    IF REF EQ '' THEN
        REF   = R.ST<AC.STE.TRANS.REFERENCE>
*Line [ 40 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR '\B' IN REF SETTING Y.XX THEN REF = FIELD(REF,'\',1) ELSE NULL
    END
    IF REF[1,2] EQ 'FT' THEN
        FN.FT='FBNK.FUNDS.TRANSFER';F.FT=''
        CALL OPF(FN.FT,F.FT)
        FT.ID = REF
        CALL F.READ(FN.FT,FT.ID, R.FT, F.FT, ETEXT)
        IF NOT(ETEXT) THEN
            IF R.FT<FT.ORDERING.BANK> EQ 'ATM' THEN
                ATM.REF = R.FT< FT.LOCAL.REF,FTLR.TERMINAL.LOC>
                ATM.REF = CHANGE(ATM.REF,'-',' ')
                ATM.REF = TRIM(ATM.REF)
                ATM.REF = CHANGE(ATM.REF,' ','-')
                O.DATA = ATM.REF
            END
        END ELSE
            FN.FT.H='FBNK.FUNDS.TRANSFER$HIS';F.FT.H=''
            CALL OPF(FN.FT.H,F.FT.H)
            FT.ID.H =  FT.ID:';1'
            CALL F.READ( FN.FT.H,FT.ID.H, R.FT.H, F.FT.H, ETEXT2)
            IF R.FT.H<FT.ORDERING.BANK> EQ 'ATM' THEN
                ATM.REF = R.FT.H< FT.LOCAL.REF,FTLR.TERMINAL.LOC>
                ATM.REF = CHANGE(ATM.REF,'-',' ')
                ATM.REF = TRIM(ATM.REF)
                ATM.REF = CHANGE(ATM.REF,' ','-')
                O.DATA = ATM.REF
            END
        END
        O.DATA = O.DATA[1,20]
    END ELSE
        O.DATA = ''
    END
**************** END OF UPDATE **********************************
    RETURN
END
