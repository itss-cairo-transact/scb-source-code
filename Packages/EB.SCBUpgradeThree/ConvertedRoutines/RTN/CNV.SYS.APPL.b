* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.SYS.APPL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.EB.SYSTEM.ID

    APPL   = ''
    SYS.ID = FIELD(O.DATA,'-',1)
    REF    = FIELD(O.DATA,'-',2)
*Line [ 30 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('EB.SYSTEM.ID':@FM:SID.APPLICATION,SYS.ID,APPL)
F.ITSS.EB.SYSTEM.ID = 'F.EB.SYSTEM.ID'
FN.F.ITSS.EB.SYSTEM.ID = ''
CALL OPF(F.ITSS.EB.SYSTEM.ID,FN.F.ITSS.EB.SYSTEM.ID)
CALL F.READ(F.ITSS.EB.SYSTEM.ID,SYS.ID,R.ITSS.EB.SYSTEM.ID,FN.F.ITSS.EB.SYSTEM.ID,ERROR.EB.SYSTEM.ID)
APPL=R.ITSS.EB.SYSTEM.ID<SID.APPLICATION>
    IF APPL EQ '' THEN
        IF REF[1,2] EQ 'BR' THEN
            APPL = 'BILL.REGISTER'
        END
        IF REF[1,2] EQ 'IN' THEN
            APPL = 'INF.MULTI.TXN'
        END
        IF REF[1,2] EQ 'BT' THEN
            APPL = 'SCB.BT.BATCH'
        END
    END
    IF SYS.ID EQ 'CQ' THEN
        IF REF[1,3] EQ 'SCB' THEN
            APPL = 'CHEQUE.ISSUE'
        END
    END
    O.DATA = APPL
    RETURN
END
