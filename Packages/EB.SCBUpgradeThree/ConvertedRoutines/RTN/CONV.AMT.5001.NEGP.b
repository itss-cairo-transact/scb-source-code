* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.AMT.5001.NEGP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*----------------------------------------------

    ONLINE.AMT = 0 ; CALC.AMT = 0 ; WS.AMT = ""

    CUS  = FIELD(O.DATA,"*",1)
    CUR  = FIELD(O.DATA,"*",2)
    CATG = "5001"
    CURR = "EGP"

    FN.ACC = 'FBNK.ACCOUNT'  ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.AC = ''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    CALL F.READ( FN.CUS.AC,CUS, R.CUS.AC, F.CUS.AC,ETEXT1)
    LOOP
        REMOVE ACC FROM R.CUS.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CURR    = R.ACC<AC.CURRENCY>
        AC.CATEG   = R.ACC<AC.CATEGORY>

        IF ( AC.CURR NE CURR AND CATG EQ AC.CATEG ) THEN
            ONLINE.AMT = R.ACC<AC.ONLINE.CLEARED.BAL>
*Line [ 63 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,AC.CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,AC.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
            CALC.AMT  = ONLINE.AMT * MID.RATE<1,1>
            WS.AMT   += CALC.AMT
            CALC.AMT  = 0
        END
    REPEAT

    IF WS.AMT THEN
        USD.CURR = 'USD'
*Line [ 78 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,USD.CURR,MID.RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,USD.CURR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
MID.RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
        WS.AMT = WS.AMT / MID.RATE<1,1>
        O.DATA = FMT(WS.AMT,"L2,")
    END ELSE
*Line [ 76 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        O.DATA = "NULL"
    END
*-----------------------------------------------------
    RETURN
END
