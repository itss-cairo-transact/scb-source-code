* @ValidationCode : MjotMTIzMjAyNzA2NzpDcDEyNTI6MTY0MDg3MjczMTE4NDpsYXA6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 30 Dec 2021 15:58:51
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
SUBROUTINE CNV.LCY.AMT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.RE.BASE.CCY.PARAM      ;*RE.BCP.
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON          ;*EB.COM.
    COMP = ID.COMPANY

    XX = O.DATA
    CUR.COD  = FIELD(XX,'*',1)
    AMT      = FIELD(XX,'*',2)

    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    CALL OPF(FN.CUR,F.CUR)

    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 40 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE CUR.COD IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
    RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
    IF RATE.TYPE EQ 'MULTIPLY' THEN
        TTT = AMT * CUR.RATE
    END
    IF RATE.TYPE EQ 'DIVIDE' THEN
        TTT = AMT / CUR.RATE
    END
    O.DATA = TTT
RETURN
