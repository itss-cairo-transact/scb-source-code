* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.ADI.HI

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.DEBIT.INT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.GENERAL.CHARGE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.HIGHEST.DEBIT

    XX = O.DATA:"...."
    FN.GEN = 'FBNK.GENERAL.CHARGE' ;  F.GEN = '' ; R.GEN = ''
    CALL OPF(FN.GEN,F.GEN)

    FN.HI = 'FBNK.HIGHEST.DEBIT' ;  F.HI = '' ; R.HI = ''
    CALL OPF(FN.HI,F.HI)

************************************************************
    T.SEL = "SELECT ":FN.GEN:" WITH @ID LIKE ":XX:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        CALL F.READ(FN.GEN,KEY.LIST<SELECTED>,R.GEN,F.GEN,E1)
        HI.DEBIT = R.GEN<IC.GCH.HIGHEST.DEBIT>:"...."
    END

    T.SEL1 = "SELECT ":FN.HI:" WITH @ID LIKE ":HI.DEBIT:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        CALL F.READ(FN.HI,KEY.LIST1<SELECTED1>,R.HI,F.HI,E2)
        HI.PER = R.HI<IC.HDB.PERCENTAGE>
    END
    IF HI.PER LT 1 THEN
        IF HI.PER NE '' THEN
            HI.PER  ="0" : HI.PER
        END
    END
    O.DATA = HI.PER
    RETURN
