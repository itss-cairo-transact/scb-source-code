* @ValidationCode : MjoyMDU1OTc5MDY1OkNwMTI1MjoxNjQxNzM2OTc1MzkzOmxhcDotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMF9TUDMuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 09 Jan 2022 16:02:55
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : lap
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* BY NESSMA MOHAMMAD
*-----------------------------------------------------------------------------
SUBROUTINE CONV.CALC.INP
*Line [ 21 ] Removed directory T24.BP - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_COMMON
    $INSERT I_COMMON
*Line [ 24 ] Removed directory T24.BP - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_EQUATE
    $INSERT I_EQUATE
*Line [ 27 ] Removed directory T24.BP - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT I_ENQUIRY.COMMON
*Line [ 30 ] Removed directory T24.BP - ITSS - R21 Upgrade - 2021-12-23
*$INSERT T24.BP I_F.USER
    $INSERT I_F.USER
*Line [ 33 ] Removed directory T24.BP - ITSS - R21 Upgrade - 2021-12-23
*$INSERT TEMENOS.BP I_F.SCB.PASSWORD.UPD
    $INSERT I_F.SCB.PASSWORD.UPD
*-------------------------------------------
    R.PWR = ""   ; E11 = ""
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF(FN.USR, F.USR)

    USR.ID = FIELD(O.DATA,'_',2)

    CALL F.READ(FN.USR,USR.ID, R.USR, F.USR, E11)
    INP.VALUE = R.USR<EB.USE.USER.NAME>
    O.DATA    = INP.VALUE
*-------------------------------------------
RETURN
END
