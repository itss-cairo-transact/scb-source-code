* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE COMM.EXP.BILL(CUSTOMER, DEAL.AMOUNT, DEAL.CURRENCY, CURRENCY.MARKET, CROSS.RATE, CROSS.CURRENCY, DRAWDOWN.CCY, T.DATA, CUST.COND, CHARGE.AMOUNT)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.COMMISSION.TYPE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LC.COMMON
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY


    MESSAGE    = ''
    E          = ''
    ETEXT      = ''
    ERR        = ''
    SHIFT      =  '' ; ROUND  = ''
    DEFFUN SHIFT.DATE(START.DATE, SHIFT, ROUND)

    NEW.AMT    = ''
    DR.CURR    = R.NEW(TF.DR.DRAW.CURRENCY)

    CHRG.ACCT  = R.NEW(TF.DR.CHARGE.ACCOUNT)<1,1>
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,CHRG.ACCT,CHRG.CURR)

    W.EXP.DATE = R.NEW(TF.DR.MATURITY.REVIEW)
    DR.AMT     = R.NEW(TF.DR.DOCUMENT.AMOUNT)
    START.DATE = TODAY
    TMP.NO     = 0
    ALL.COM    = 0
    NEW.COM    = 0

    FN.FT.COM  = 'FBNK.FT.COMMISSION.TYPE' ; F.FT.COM = ''
    CALL OPF(FN.FT.COM,F.FT.COM)

    FN.FT.CHRG = 'FBNK.FT.CHARGE.TYPE' ; F.FT.CHRG = ''
    CALL OPF(FN.FT.CHRG,F.FT.CHRG)

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = '' ; R.CUR = '' ; E11 = ''
    CALL OPF(FN.CUR,F.CUR)

    CALL F.READ(FN.CUR,DR.CURR, R.CUR, F.CUR ,E11)

***--------------------------------------------------------------***

    IF R.NEW(TF.DR.CHARGE.CODE)<1,1>  EQ 'TFIMCOLAVV'  THEN

        COM.CODE =  R.NEW(TF.DR.CHARGE.CODE)<1,1>
        CALL F.READ(FN.FT.COM,COM.CODE,R.FT.COM,F.FT.COM,E.COM)

        COM.MIN  = R.FT.COM<FT4.MINIMUM.AMT><1,1>

        FOR I = 1 TO 100

            START.DATE = SHIFT.DATE( START.DATE, '3M', 'UP')
            TMP.DATE   = START.DATE
            IF TMP.DATE > W.EXP.DATE THEN
                COMM.NO = I - TMP.NO ; I = 100
            END

        NEXT I

        FIRST.PIROD  = ( DR.AMT * 0.75 ) / 100
        COMM.NO.OTHR =  COMM.NO - 1
        ATHR.PIROD   = ((( DR.AMT * 0.5 ) / 100 )  *  COMM.NO.OTHR )

        ALL.COM    =  FIRST.PIROD +  ATHR.PIROD

        IF DR.CURR NE CHRG.CURR THEN

            CUR.RATE.DR  = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

            IF DR.CURR EQ 'EGP'  THEN
                CUR.RATE.CHRG  = 1
            END

            NEW.AMT1     = ALL.COM * CUR.RATE.DR
            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")

            COM.MIN1     = COM.MIN * CUR.RATE.DR
            CALL EB.ROUND.AMOUNT ('USD',COM.MIN1,'',"2")

            IF DR.CURR EQ "JPY" THEN
                NEW.AMT1  = ALL.COM / 100
                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")

                COM.MIN1     = COM.MIN / 100
                CALL EB.ROUND.AMOUNT ('USD',COM.MIN1,'',"2")
            END

            CALL F.READ(FN.CUR,CHRG.CURR, R.CUR.CHRG, F.CUR ,E11)
            CUR.RATE.CHRG  = R.CUR.CHRG<EB.CUR.MID.REVAL.RATE><1,1>

            IF CHRG.CURR EQ 'EGP'  THEN
                CUR.RATE.CHRG  = 1
            END

            NEW.AMT2       = NEW.AMT1 / CUR.RATE.CHRG
            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")

            COM.MIN2      = COM.MIN1 / CUR.RATE.CHRG
            CALL EB.ROUND.AMOUNT ('USD',COM.MIN2,'',"2")

            IF CHRG.CURR EQ "JPY" THEN
                NEW.AMT2  = NEW.AMT1 * 100
                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")

                COM.MIN2      = COM.MIN1 * 100
                CALL EB.ROUND.AMOUNT ('USD',COM.MIN2,'',"2")
            END

            NEW.AMT = NEW.AMT2
****
            BEGIN CASE
            CASE NEW.AMT2 < COM.MIN2
                NEW.AMT = COM.MIN2
            CASE OTHERWISE
                NEW.AMT = NEW.AMT2
            END CASE

***
        END ELSE
            NEW.AMT = ALL.COM

            BEGIN CASE
            CASE  NEW.AMT < COM.MIN
                NEW.AMT = COM.MIN
            CASE OTHERWISE
                NEW.AMT = ALL.COM
            END CASE

        END

        CALL EB.ROUND.AMOUNT ('USD',NEW.AMT,'',"2")

        R.NEW(TF.DR.CHARGE.AMOUNT)<1,1>          = NEW.AMT

        CALL REBUILD.SCREEN

        MESSAGE = ''
        E       = ''
        ETEXT   = ''
        ERR     = ''
    END

**------------          OTHER COMM & CHRG           ---------------**

*Line [ 175 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    CHRG.NO = DCOUNT(R.NEW(TF.DR.CHARGE.CODE),@VM)
    FOR XX =  2 TO CHRG.NO
**--------------            COMM             ------------------------**

        E.COM    = ''
        COM.CODE =  R.NEW(TF.DR.CHARGE.CODE)<1,XX>
        CALL F.READ(FN.FT.COM,COM.CODE,R.FT.COM,F.FT.COM,E.COM)
        IF NOT(E.COM) THEN

            MESSAGE      = ''
            E            = ''
            ETEXT        = ''
            ERR          = ''
            COM.MIN2     = ''
            COM.MAX2     = ''
            COM.MIN1     = ''
            COM.MAX1     = ''
            NEW.AMT      = ''
            NEW.AMT1     = ''
            NEW.AMT2     = ''
            COM.PER      = R.FT.COM<FT4.PERCENTAGE><1,1>
            COM.MIN      = R.FT.COM<FT4.MINIMUM.AMT><1,1>
            COM.MAX      = R.FT.COM<FT4.MAXIMUM.AMT><1,1>
            FLG.MIN      = ''
            FLG.MAX      = ''
            FLG.MAX.1    = ''
            FLG.MIN.1    = ''
            MIN.AMT      = ''
            MIN.AMT.1    = ''
            MIN.AMT.2    = ''
            MAX.AMT      = ''
            MAX.AMT.1    = ''
            MAX.AMT.2    = ''

            ALL.COM      = (( DR.AMT * COM.PER ) / 100 )

            IF DR.CURR NE CHRG.CURR THEN

                CUR.RATE.DR  = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
                IF DR.CURR EQ 'EGP'  THEN
                    CUR.RATE.CHRG  = 1
                END

                NEW.AMT1     = ALL.COM * CUR.RATE.DR
                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")

                COM.MIN1     = COM.MIN * CUR.RATE.DR
                CALL EB.ROUND.AMOUNT ('USD',COM.MIN1,'',"2")

                COM.MAX1     = COM.MAX * CUR.RATE.DR
                CALL EB.ROUND.AMOUNT ('USD',COM.MAX1,'',"2")

                IF DR.CURR EQ "JPY" THEN
                    NEW.AMT1  = ALL.COM / 100
                    CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")

                    COM.MIN1     = COM.MIN / 100
                    CALL EB.ROUND.AMOUNT ('USD',COM.MIN1,'',"2")

                    COM.MAX1     = COM.MAX / 100
                    CALL EB.ROUND.AMOUNT ('USD',COM.MAX1,'',"2")

                END

                CALL F.READ(FN.CUR,CHRG.CURR, R.CUR.CHRG, F.CUR ,E11)
                CUR.RATE.CHRG  = R.CUR.CHRG<EB.CUR.MID.REVAL.RATE><1,1>

                IF CHRG.CURR EQ 'EGP' THEN
                    CUR.RATE.CHRG  = 1
                END

                NEW.AMT2       = NEW.AMT1 / CUR.RATE.CHRG
                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")

                COM.MIN2      = COM.MIN1 / CUR.RATE.CHRG
                CALL EB.ROUND.AMOUNT ('USD',COM.MIN2,'',"2")

                COM.MAX2      = COM.MAX1 / CUR.RATE.CHRG
                CALL EB.ROUND.AMOUNT ('USD',COM.MAX2,'',"2")

                IF CHRG.CURR EQ "JPY" THEN
                    NEW.AMT2  = NEW.AMT1 * 100
                    CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")
                    COM.MIN2      = COM.MIN1 * 100
                    CALL EB.ROUND.AMOUNT ('USD',COM.MIN2,'',"2")
                    COM.MAX2      = COM.MAX1 * 100
                    CALL EB.ROUND.AMOUNT ('USD',COM.MAX2,'',"2")
                END

                NEW.AMT = NEW.AMT2

                BEGIN CASE

                CASE NEW.AMT2 < COM.MIN2
                    NEW.AMT = COM.MIN2
                CASE ( NEW.AMT2 > COM.MAX2 AND COM.MAX NE '' )
                    NEW.AMT = COM.MAX2
                CASE OTHERWISE
                    NEW.AMT = NEW.AMT2
                END CASE
            END ELSE

***---------------------  20120508   -----------------------
                NEW.AMT = ALL.COM

                BEGIN CASE
                CASE  NEW.AMT < COM.MIN
                    NEW.AMT = COM.MIN
                CASE ( NEW.AMT > COM.MAX AND COM.MAX NE '' )
                    NEW.AMT = COM.MAX
                CASE OTHERWISE
                    NEW.AMT = ALL.COM
                END CASE

            END

            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT,'',"2")

            R.NEW(TF.DR.CHARGE.AMOUNT)<1,XX>   = NEW.AMT
        END
**-----------------------------------------------------------------**

**------------              CHRG          -------------------------**

        MESSAGE      = ''
        E            = ''
        ETEXT        = ''
        ERR          = ''
        DR.CURR      = ''
        CUR.RATE.DR  = ''
        MIN.AMT      = ''
        MIN.AMT.1    = ''
        MIN.AMT.2    = ''
        MAX.AMT      = ''
        MAX.AMT.1    = ''
        MAX.AMT.2    = ''
        COM.MIN2     = ''
        COM.MAX2     = ''
        COM.MIN1     = ''
        COM.MAX1     = ''
        NEW.AMT      = ''
        NEW.AMT1     = ''
        NEW.AMT2     = ''

        CHRG.CODE =  R.NEW(TF.DR.CHARGE.CODE)<1,XX>
        CALL F.READ(FN.FT.CHRG,CHRG.CODE,R.FT.CHRG,F.FT.CHRG,E.CHRG)

        IF NOT(E.CHRG) THEN

            DR.CURR   = R.FT.CHRG<FT5.CURRENCY>
            ALL.COM   = R.FT.CHRG<FT5.FLAT.AMT>

            CALL F.READ(FN.CUR, DR.CURR, R.CUR, F.CUR ,E11)

            IF DR.CURR NE CHRG.CURR THEN

                CUR.RATE.CHRG  = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>

                IF DR.CURR EQ 'EGP'  THEN
                    CUR.RATE.CHRG = 1
                END

                NEW.AMT1     = ALL.COM * CUR.RATE.CHRG

                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")
                IF DR.CURR EQ "JPY" THEN
                    NEW.AMT1  = ALL.COM / 100
                    CALL EB.ROUND.AMOUNT ('USD',NEW.AMT1,'',"2")
                END

                CALL F.READ(FN.CUR,CHRG.CURR, R.CUR.CHRG, F.CUR ,E11)
                CUR.RATE.CHRG  = R.CUR.CHRG<EB.CUR.MID.REVAL.RATE><1,1>

                IF CHRG.CURR EQ 'EGP'  THEN
                    CUR.RATE.CHRG  = '1'
                END

                NEW.AMT2       = NEW.AMT1 / CUR.RATE.CHRG
                CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")


                IF CHRG.CURR EQ "JPY" THEN
                    NEW.AMT2  = NEW.AMT1 * 100
                    CALL EB.ROUND.AMOUNT ('USD',NEW.AMT2,'',"2")
                END

                NEW.AMT = NEW.AMT2
            END ELSE
                NEW.AMT = ALL.COM
            END
*TEXT = "NEW.AMT= " : NEW.AMT ; CALL REM
            CALL EB.ROUND.AMOUNT ('USD',NEW.AMT,'',"2")
            R.NEW(TF.DR.CHARGE.AMOUNT)<1,XX>   = NEW.AMT
        END

**-----------------------------------------------------------------**
    NEXT XX

**------------                OTHER COMM & CHRG    ---------------**

    RETURN
END
