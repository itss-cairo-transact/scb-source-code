* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
****************************NI7OOOOOOOOOOO*******
*-----------------------------------------------------------------------------
* <Rating>100</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.CCY.NZD

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY

    FN.CCY      = 'FBNK.CURRENCY'           ; F.CCY  = '' ; R.CCY = '' ; ETEXT.EX1 = ''
    FN.CCY.HIS  = 'FBNK.CURRENCY$HIS'       ; F.CCY.HIS  = '' ; R.CCY.HIS = '' ; ETEXT.EX1 = ''
    FN.CUR      = 'FBNK.RE.BASE.CCY.PARAM'      ; F.CUR  = '' ; R.CUR = ''
    FN.LD       = 'FBNK.LD.LOANS.AND.DEPOSITS'  ; F.LD   = '' ; R.LD  = ''
    FN.MID      = 'F.SBD.CURRENCY'              ; F.MID   = '' ; R.MID = ''

    CALL OPF (FN.CUR,F.CUR)
    CALL OPF (FN.LD,F.LD)
    CALL OPF (FN.CCY,F.CCY)
    CALL OPF (FN.CCY.HIS,F.CCY.HIS)
    CALL OPF (FN.MID,F.MID)

    XX = O.DATA
    IF XX NE 'EGP' THEN
        CALL F.READ(FN.MID,XX,R.MID,F.MID,E1)
        CUR.RATE = R.MID<SBD.CURR.MID.RATE>
        IF XX EQ 'JPY' THEN
            CUR.RATE = ( CUR.RATE / 100 )
        END
    END ELSE
        CUR.RATE = 1
    END
    O.DATA = CUR.RATE

    RETURN
END
