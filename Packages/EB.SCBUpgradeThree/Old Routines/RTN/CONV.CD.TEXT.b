* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-55</Rating>
*-----------------------------------------------------------------------------
    PROGRAM CONV.CD.TEXT

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CD.TYPES
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*-----------------------------------------------------------------------

**** OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "certicate2" TO BB THEN
    OPENSEQ "/home/signat" , "certicate2" TO BB THEN
        CLOSESEQ BB
        HUSH ON
***  EXECUTE 'DELETE ':"/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&":' ':"certicate2"
        EXECUTE 'DELETE ':"/home/signat":' ':"certicate2"
        HUSH OFF
    END
****  OPENSEQ "/life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&" , "certicate2" TO BB ELSE
    OPENSEQ "/home/signat" , "certicate2" TO BB ELSE
        CREATE BB THEN
***** PRINT 'FILE CONV.SAVE.TXT CREATED IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
            PRINT 'FILE CONV.SAVE.TXT CREATED IN /home/signat'
        END ELSE
******STOP 'Cannot create CONV.SAVE.TXT File IN /life/CAIRO/NT24/bnk/bnk.run/&SAVEDLISTS&'
            STOP 'Cannot create CONV.SAVE.TXT File IN /home/signat'
        END
    END
*----------------------------------------------------------------
    FN.ACC = 'F.SCB.CD.TYPES' ; F.ACC = '' ; R.ACC = ''

    CALL OPF(FN.ACC,F.ACC)

    FN.CATEG = 'F.CATEGORY' ; F.CATEG = '' ; R.CATEG = ''

    CALL OPF(FN.ACC,F.ACC)

*----------------------------------------------------------------
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.ACC:" WITH @ID EQ 'EGP-1000-1M-36M' OR @ID EQ 'EGP-1000-3M-36M' OR @ID EQ 'EGP-10000-36M' OR @ID EQ 'EGP-1000-60M' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E1)
            SCB.CAT = R.ACC<CD.TYPES.CATEGORY>
            CALL F.READ(FN.CATEG,SCB.CAT,R.CATEG,F.CATEG,E2)
            R1=R.ACC<CD.TYPES.DESCRIPTION,2>
            R2=R.ACC<CD.TYPES.CD.RATE>
            R3 = R.CATEG<EB.CAT.DESCRIPTION>
*Line [ 78 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            ZZ = DCOUNT(R3,@VM)
            R4 = ''
            IF KEY.LIST<I> = 'EGP-10000-36M' THEN
                R2=R2*3
                R2=DROUND(R2,2)
            END
            IF KEY.LIST<I> = 'EGP-1000-60M' THEN
                R2=R2*5
            END

            FOR II = 1 TO ZZ
                R4 := R3<1,II>:"|"
            NEXT II
*--------------------------------------------------------
*BB.DATA  = R1:","
            BB.DATA  = R2:"|":R4
*PRINT KEY.LIST<I>:" ":BB.DATA
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END
*************************************************************
*----------------------------------------------------------------
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    T.SEL = "SELECT ":FN.ACC:" WITH @ID EQ 'EGP-1000-6M-36M' OR @ID EQ 'EGP-1000-12M-36M' BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E1)
            SCB.CAT = R.ACC<CD.TYPES.CATEGORY>
            CALL F.READ(FN.CATEG,SCB.CAT,R.CATEG,F.CATEG,E2)
            R1=R.ACC<CD.TYPES.DESCRIPTION,2>
            R2=R.ACC<CD.TYPES.CD.RATE>
            R3 = R.CATEG<EB.CAT.DESCRIPTION>
*Line [ 114 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            ZZ = DCOUNT(R3,@VM)
            R4 = ''
            IF KEY.LIST<I> = 'EGP-10000-36M' THEN
                R2=R2*3
                R2=DROUND(R2,2)
            END
            IF KEY.LIST<I> = 'EGP-1000-60M' THEN
                R2=R2*5
            END

            FOR II = 1 TO ZZ
                R4 := R3<1,II>:"|"
            NEXT II
*--------------------------------------------------------
*BB.DATA  = R1:","
            BB.DATA  = R2:"|":R4
*PRINT KEY.LIST<I>:" ":BB.DATA
            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
        NEXT I
    END
*************************************************************
END
