* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.ACCT.ACTIVE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ACTIVITY

    FN.DR='FBNK.ACCT.ACTIVITY';F.DR=''
    CALL OPF(FN.DR,F.DR)
    BAL = '' ; BAL2 = '' ; AC.ID = ''

    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    ID = O.DATA
*  DAT = TODAY[1,6]:'01'
*  CALL CDT("",DAT,'-1C')
*  DAT1 = DAT[1,6]
    DAT = TODAY[5,2]
    Y = DAT - '2'
    DAT2 = TODAY[1,4] : Y
    AC.ID = ID : '-' : DAT2
    CALL F.READ(FN.DR,AC.ID,R.DR,F.DR,E2)
    BAL = R.DR<IC.ACT.BK.BALANCE>
*Line [ 46 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    DB = DCOUNT(BAL,@VM)
    FOR X = 1 TO DB
        BAL2 =BAL<1,DB>
    NEXT X
    O.DATA = BAL2
    RETURN
END
