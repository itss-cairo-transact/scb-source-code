* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>300</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CBE.DOCTP

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PAY.CBE.CODES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MONTHLY.PAY.CBE

    FN.CBE = 'F.SCB.PAY.CBE.CODES'; F.CBE = ''; R.CBE = ''; CBE.ERR = ''

    XX = O.DATA

    FUNTP    = FIELD(XX,'.',1)
    DOC.CODE = FIELD(XX,'.',2)

    IF FUNTP EQ 1 THEN
        FUN.CODE = '02'
    END
    ELSE IF FUNTP EQ 2 THEN
        FUN.CODE = '03'
    END
    ELSE IF FUNTP EQ 3 THEN
        FUN.CODE = '04'
    END
    ELSE IF FUNTP EQ 4 THEN
        FUN.CODE = '05'
    END


    T.SEL = "SELECT F.SCB.PAY.CBE.CODES WITH TABLE.FLAG EQ ":FUN.CODE:" AND CODE EQ ":DOC.CODE

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    CALL F.READ(FN.CBE,KEY.LIST<1>,R.CBE,F.CBE,CBE.ERR)
    DESC = R.CBE<CBE.CODE.DESCRIPTION>
    O.DATA = DESC

    RETURN
END
