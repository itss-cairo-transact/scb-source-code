* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
****************************************************
*-----------------------------------------------------------------------------
* <Rating>278</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE COB.PWD.WRT.HIS
****************************************************
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.PASSWORD.RESET
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PASSWORD.UPD
**************************************************
* Modification History
*
* 20-Jun-2016 SCBUPG20160620 Used correct file name for F.DELETE
*
*
*
*----------------------------------------------------
    COUNT.N  = 0
    FN.PWR   = 'F.PASSWORD.RESET'    ; F.PWR = ''
    FN.PWR.H = 'F.SCB.PASSWORD.UPD'  ; F.PWR.H  = ''
    CALL OPF(FN.PWR,F.PWR)
    CALL OPF(FN.PWR.H,F.PWR.H)
**************************************************
    T.SEL = "SELECT F.PASSWORD.RESET"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    FOR NN = 1 TO SELECTED
        CALL F.READ(FN.PWR,KEY.LIST<NN>, R.PWR, F.PWR, E1)

        TOD.DATE  = TODAY
        ID.PWR.H  = KEY.LIST<NN>:".":TOD.DATE
        CALL F.READ(FN.PWR.H,ID.PWR.H , R.PWR.H, F.PWR.H, E11)

        R.PWR.H<PWR.USER.PW.ATTEMPT> = R.PWR<EB.PWR.USER.PW.ATTEMPT>
        R.PWR.H<PWR.USER.ATTEMPT>    = R.PWR<EB.PWR.USER.ATTEMPT>
        R.PWR.H<PWR.NO.OF.ATTEMPTS>  = R.PWR<EB.PWR.NO.OF.ATTEMPTS>
        R.PWR.H<PWR.USER.DEACT.PERD> = R.PWR<EB.PWR.USER.DEACT.PERD>
        R.PWR.H<PWR.DEACTIV.PERIOD>  = R.PWR<EB.PWR.DEACTIV.PERIOD>
        R.PWR.H<PWR.USER.RESET>      = R.PWR<EB.PWR.USER.RESET>
        R.PWR.H<PWR.USER.PASSWORD>   = R.PWR<EB.PWR.USER.PASSWORD>
        R.PWR.H<PWR.USER.PWD>        = R.PWR<EB.PWR.USER.PWD>
        R.PWR.H<PWR.RECORD.STATUS>   = R.PWR<EB.PWR.RECORD.STATUS>
        R.PWR.H<PWR.CURR.NO>         = R.PWR<EB.PWR.CURR.NO>

        R.PWR.H<PWR.INPUTTER>        = R.PWR<EB.PWR.INPUTTER>
        R.PWR.H<PWR.DATE.TIME>       = R.PWR<EB.PWR.DATE.TIME>
        R.PWR.H<PWR.AUTHORISER>      = R.PWR<EB.PWR.AUTHORISER>
        R.PWR.H<PWR.CO.CODE>         = R.PWR<EB.PWR.CO.CODE>
        R.PWR.H<PWR.DEPT.CODE>       = R.PWR<EB.PWR.DEPT.CODE>
        R.PWR.H<PWR.AUDITOR.CODE>    = R.PWR<EB.PWR.AUDITOR.CODE>
        R.PWR.H<PWR.AUDIT.DATE.TIME> = R.PWR<EB.PWR.AUDIT.DATE.TIME>

        R.PWR.H<PWR.USER.ID>         = KEY.LIST<NN>
        R.PWR.H<PWR.BOOKING.DATE>    = TODAY

        CALL DBR ('USER':@FM:EB.USE.DEPARTMENT.CODE,KEY.LIST<NN>,DBT.COMP)
        IF LEN(DBT.COMP) EQ 1 THEN
            R.PWR.H<PWR.COMPANY.ID> = "EG001000":DBT.COMP
        END ELSE
            R.PWR.H<PWR.COMPANY.ID> = "EG00100" :DBT.COMP
        END
*------------------------------------
        FLAG     = 0
        COUNT.N  = COUNT.N + 1
*WRITE  R.PWR.H TO F.PWR.H , ID.PWR.H ON ERROR
*    FALG = 1
*END
        CALL F.WRITE(FN.PWR.H,ID.PWR.H,R.PWR.H)
*   CALL JOURNAL.UPDATE(ID.PWR.H)

        IF FLAG EQ 0 THEN
            COUNT.N = COUNT.N + 1
*DELETE F.PWR , KEY.LIST<NN>
* SCBUPG20160620 - S
            CALL F.DELETE (FN.PWR,KEY.LIST<NN>)
* SCBUPG20160620 - E
            COUNT.D = COUNT.D + 1
        END

    NEXT NN
    RETURN
END
