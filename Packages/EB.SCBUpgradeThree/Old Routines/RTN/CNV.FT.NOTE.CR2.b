* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.FT.NOTE.CR2
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    TR.ID = O.DATA
    TR.ID.H = TR.ID:";1"

    FN.FT    = 'FBNK.FUNDS.TRANSFER'     ; F.FT   = '' ; R.FT   = ''
    FN.FT.H  = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.H = '' ; R.FT.H = ''
    FN.TT    = 'FBNK.TELLER'     ; F.TT   = '' ; R.TT   = ''
    FN.TT.H  = 'FBNK.TELLER$HIS' ; F.TT.H = '' ; R.TT.H = ''
    FN.IN    = 'F.INF.MULTI.TXN'     ; F.IN   = '' ; R.IN   = ''
    FN.IN.H  = 'F.INF.MULTI.TXN$HIS' ; F.IN.H = '' ; R.IN.H = ''

    CALL OPF( FN.FT,F.FT)
    CALL OPF( FN.FT.H,F.FT.H)
    CALL OPF( FN.TT,F.TT)
    CALL OPF( FN.TT.H,F.TT.H)
    CALL OPF( FN.IN,F.IN)
    CALL OPF( FN.IN.H,F.IN.H)

    IF TR.ID[1,2] EQ 'FT' THEN
        CALL F.READ( FN.FT,TR.ID, R.FT, F.FT, ETEXT)
        IF NOT(ETEXT) THEN
            CR.NOTE = R.FT<FT.LOCAL.REF,FTLR.NOTE.CREDIT,2>
            O.DATA = CR.NOTE
        END ELSE
            CALL F.READ( FN.FT.H,TR.ID.H,R.FT.H, F.FT.H, ETEXT2)
            CR.NOTE = R.FT.H<FT.LOCAL.REF,FTLR.NOTE.CREDIT,2>
            O.DATA = CR.NOTE
        END
    END
    IF TR.ID[1,2] EQ 'TT' THEN
        CALL F.READ( FN.TT,TR.ID,R.TT, F.TT, ETEXT)
        IF NOT(ETEXT) THEN
            CR.NOTE = R.TT<TT.TE.NARRATIVE.2>
            O.DATA = CR.NOTE
        END ELSE
            CALL F.READ( FN.TT.H,TR.ID.H, R.TT.H, F.TT.H, ETEXT2)
            CR.NOTE = R.TT.H<TT.TE.NARRATIVE.2>
            O.DATA = CR.NOTE
        END
    END
    IF TR.ID[1,2] EQ 'IN' THEN
        CALL F.READ( FN.IN,TR.ID,R.IN, F.IN, ETEXT)
        IF NOT(ETEXT) THEN
            CR.NOTE = R.IN<INF.MLT.THEIR.REFERENCE><1,2>
            O.DATA = CR.NOTE
        END
    END

    RETURN
