* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
********************************NI7OOOOOOOOOOO*************
    SUBROUTINE CNV.SWIFT
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS


    FN.BR    ='FBNK.BILL.REGISTER' ; F.BR = ''
    FN.BR.NAU='FBNK.BILL.REGISTER$NAU' ; F.BR.NAU = ''
    CALL OPF(FN.BR,F.BR)
    CALL OPF(FN.BR.NAU,F.BR.NAU)

    CALL F.READ(FN.BR.NAU,O.DATA,R.BR.NAU,F.BR.NAU,E2)

    COCODE = R.BR.NAU<EB.BILL.REG.CO.CODE>
   ** TEXT = "CO.CODE : " : COCODE ; CALL REM
    IF COCODE EQ 'EG0010013' THEN
        O.DATA = 'SUCAEGCXGRD'
    END
    IF COCODE NE 'EG0010013' THEN
        O.DATA = 'SUCAEGCX'
    END
    RETURN
END
