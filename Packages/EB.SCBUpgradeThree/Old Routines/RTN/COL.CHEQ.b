* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE COL.CHEQ

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COL.CHEQ
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY


    GOSUB INITIATE
**  GOSUB PRINT.HEAD
*Line [ 42 ] Adding EB.SCBUpgradeThree. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    TEXT = " REPORT SUCCESFULLY COMPLETED " ; CALL REM

    RETURN
*==============================================================
INITIATE:
    REPORT.ID='COL.CHEQ'
    CALL PRINTER.ON(REPORT.ID,'')

    FN.COL = 'F.SCB.COL.CHEQ'             ; F.COL = ''
    CALL OPF(FN.COL,F.COL)


    FN.COM = 'F.COMPANY'  ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.ACC = 'FBNK.ACCOUNT' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)


    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    XX = SPACE(120)
    XX1 = SPACE(120)
    XX2 = SPACE(120)
    XX3 = SPACE(120)
    XX4 = SPACE(120)
    XX5 = SPACE(120)
    XX6 = SPACE(120)
    XX7 = SPACE(120)
    XX8 = SPACE(120)
    XX9 = SPACE(120)
    XX10 = SPACE(120)
    XX11 = SPACE(120)
    XX12 = SPACE(120)
    XX13 = SPACE(120)
    RETURN
*=============================================================
CALLDB:
*    YTEXT = "Enter Customer No.  : "
*    CALL TXTINP(YTEXT, 8, 22, "16", "A")
*    CUST.ID = COMI

    T.SEL = "SELECT F.SCB.COL.CHEQ "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    IF SELECTED THEN
        CRT 'KEY LIST : ':KEY.LIST
*        FOR I = 1 TO SELECTED
        CALL F.READ(FN.COL,KEY.LIST<SELECTED>,R.COL,F.COL,E1)
        CUST.ID   = R.NEW(COL.CUST.ID)
        BRANCH.ID = R.NEW(COL.BRANCH.ID)
        BR.SER = "00":BRANCH.ID
        COMP.ID = 'EG00100':BRANCH.ID
        CALL F.READ(FN.COM,COMP.ID,R.COM,F.COM,E2)
        COMP.NAME = R.COM<EB.COM.COMPANY.NAME,2>

        ACCOUNT.NUMBER = R.NEW(COL.ACCOUNT.NUMBER)
        CUST.NAME.1 = R.NEW(COL.CUST.NAME.1)
        CUST.NAME.2 = R.NEW(COL.CUST.NAME.2)
        NO.OF.CHEQS = R.NEW(COL.NO.OF.CHEQ)
        CHEQ.START.DATE = R.COL<COL.CHEQ.START.DATE>
        CHEQ.AMOUNT = R.NEW(COL.CHEQ.AMOUNT)
        START.SER = R.NEW(COL.START.SER)
        CUST = FIELD(START.SER,".",1)
        CUST = TRIM(CUST)
        SERIAL = FIELD(START.SER,".",2)
        END.SER = R.NEW(COL.END.SER)
        CHEQ.LETTERS = ""
************************
        CALL F.READ(FN.ACC,ACCOUNT.NUMBER,R.ACC,F.ACC,E.ACC)
        CUR = R.ACC<AC.CURRENCY>

************************

*            CUR = R.NEW(TT.TE.CURRENCY.1)

        CALL DBR('CURRENCY':@FM:EB.CUR.CCY.NAME,CUR,CURR)
        IN.AMOUNT = CHEQ.AMOUNT
        CALL WORDS.ARABIC(IN.AMOUNT,OUT.AMOUNT,78,NO.OF.LINES,ER.MSG)
        CHEQ.LETTERS = OUT.AMOUNT:" ":CURR:" ":"�� ���"

        T.DATE = CHEQ.START.DATE[7,2]:'/':CHEQ.START.DATE[5,2]:"/":CHEQ.START.DATE[1,4]

        AMT = FMT(CHEQ.AMOUNT, "L2,")
        KEY.1 = "SCB.COL.CHEQ"


        XX1<1,1>[1,100] = KEY.1
        XX<1,1>[1,100]  = CUST
        XX2<1,1>[1,100] = COMP.NAME
        XX3<1,1>[1,100] = CUST.NAME.1:' ':CUST.NAME.2
        XX4<1,1>[1,100] = SERIAL
        XX5<1,1>[1,100] = "0017":'   ':CUR
        XX6<1,1>[1,100] = AMT
        XX11<1,1>[1,100] = CHEQ.LETTERS
        XX7<1,1>[1,100] = BR.SER
        XX8<1,1>[1,100] = CUST
        XX9<1,1>[1,100] = "��� ���� ������"
        XX10<1,1>[1,100] = NO.OF.CHEQS
        XX12<1,1>[1,100] = ACCOUNT.NUMBER
*      XX13<1,1>[1,100] = CURR
        PRINT ''
        PRINT XX1<1,1>

        K = 0
        FOR X = 1 TO NO.OF.CHEQS
            IF X NE 1 THEN
                XX4<1,1> = XX4<1,1> + 1
                XX4<1,1> = FMT(XX4<1,1>, "R%4")
            END

            PRINT XX12<1,1>
            PRINT XX2<1,1>
            PRINT XX3<1,1>
            PRINT TRIM(XX8<1,1>) :".": XX4<1,1>
            PRINT XX6<1,1>
            PRINT XX11<1,1>
            PRINT TRIM(XX10<1,1>) :"     ": XX9<1,1>
            PRINT XX<1,1>
            PRINT XX5<1,1>
            PRINT XX7<1,1>

            K++
            IF K = 4 THEN
                GOSUB PRINT.HEAD
                K = 0
            END
        NEXT X
**        NEXT I
  *  END
    RETURN
*===============================================================
PRINT.HEAD:

    PR.HD  ="'L'"
    PR.HD :=" "

    HEADING PR.HD
    RETURN
*==============================================================
END
