* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*****************************NI7OOOOOOOOOOOOOO***********************
    SUBROUTINE CNV.ID
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS


    FN.CUS = "FBNK.CUSTOMER" ; F.CUS = ''
    FN.AC  = "FBNK.ACCOUNT"  ; F.AC  = ''
    FN.LC  = "FBNK.LETTER.OF.CREDIT"  ; F.LC  = ''
    CALL OPF (FN.CUS,F.CUS)
    CALL OPF (FN.AC,F.AC)
    CALL OPF (FN.LC,F.LC)

    XX = O.DATA
    CALL F.READ(FN.LC,XX,R.LC,F.LC,E2)
    CUSBEN = R.LC<TF.LC.ADVISING.BK.CUSTNO>
    ACBEN  = R.LC<TF.LC.ADVISING.BK.ACC>

    IF ACBEN EQ '' THEN
        O.DATA = CUSBEN
    END
    IF CUSBEN EQ '' THEN
        CALL F.READ(FN.AC,ACBEN,R.AC,F.AC,E3)
        CUSID = R.AC<AC.CUSTOMER>
        O.DATA = CUSID
    END
    IF ACBEN NE '' AND CUSBEN NE '' THEN
        O.DATA = CUSBEN
    END

    RETURN
END
