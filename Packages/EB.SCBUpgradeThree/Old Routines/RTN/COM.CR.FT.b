* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*--------------------Created by Riham youssef----20201116-----
*    PROGRAM COM.CR.FT
    SUBROUTINE COM.CR.FT
*----------------------------------------------------
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.COM.CR
*---------------------------------------
    GOSUB CHECK
    IF SF = 0 THEN
        GOSUB CHECKLIST
        IF SW = 0  THEN
            GOSUB INITIALISE
            GOSUB BUILD.RECORD
            GOSUB PROGRAM.END
        END
    END
    RETURN

*-----------
CHECK:
*---------
    FN.COM = 'F.SCB.COM.CR'
    F.COM  = ''
    R.COM  = ''
    CALL OPF(FN.COM,F.COM)

    DAT.Y = TODAY
    KEY.LIST11 = "" ; SELECTED11 = "" ;  ER.COM = "" ; SF = 0
    T.SEL11 = "SELECT F.SCB.COM.CR WITH WRONG.ACCT.NO NE ''"
    CALL EB.READLIST(T.SEL11,KEY.LIST11,"",SELECTED11,ER.COM)
    IF SELECTED11 THEN
        TEXT = "MUST CHECK FILE, NO WRONG ACCOUNT ALLOWED" ; CALL REM
        SF = 1
    END ELSE
        SF = 0
    END
    RETURN
*-----------
CHECKLIST:
*---------
    FN.FT = 'FBNK.FUNDS.TRANSFER'
    F.FT  = ''
    R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    DAT.Y = TODAY
    KEY.LIST = "" ; SELECTED = "" ;  ER.FT = "" ; SW = 0
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER WITH TRANSACTION.TYPE EQ 'AC64' AND NOTE.DEBITED EQ 'COUPON' AND DEBIT.VALUE.DATE EQ ":TODAY
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.FT)
    IF SELECTED THEN
        TEXT = "THIS PROGRAM IS DONE BEFORE " ; CALL REM
        SW = 1
    END ELSE
        SW = 0
    END
    RETURN
*----------------------------------------
INITIALISE:
*----------
    SCB.OFS.SOURCE  = "OFS.FOREX"
    SCB.APPL        = "FUNDS.TRANSFER"
    SCB.VERSION     = "POS"

    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION : "/I/PROCESS,,,"
    OPENSEQ "OFS.MNGR.IN" , "FT.OFS.COM.CR" TO BB.IN THEN
        CLOSESEQ BB.IN
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.IN":' ':"FT.OFS.COM.CR"
        HUSH OFF
    END

    OPENSEQ "OFS.MNGR.IN" , "FT.OFS.COM.CR" TO BB.IN ELSE
        CREATE BB.IN THEN
        END ELSE
            STOP 'Cannot create FT.OFS.COM.CR File IN OFS.MNGR.IN'
        END
    END
    OPENSEQ "OFS.MNGR.OUT" , "FT.OFS.COM.CR" TO BB.OUT THEN
        CLOSESEQ BB.OUT
        HUSH ON
        EXECUTE 'DELETE ':"OFS.MNGR.OUT":' ':"FT.OFS.COM.CR"
        HUSH OFF
    END
    OPENSEQ "OFS.MNGR.OUT" , "FT.OFS.COM.CR" TO BB.OUT ELSE
        CREATE BB.OUT THEN
        END ELSE
            STOP 'Cannot create FT.OFS.COM.CR File IN OFS.MNGR.OUT'
        END
    END

    WS.COUNT = 1
    RETURN
*-----------
BUILD.RECORD:
*------------

    FN.ATFX = 'F.SCB.COM.CR' ; F.ATFX = ''
    CALL OPF(FN.ATFX,F.ATFX)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)
    KEY.LIST1 = "" ; SELECTED1 = "" ;  ER.FT1 = "" ; DEB.AMT = 0 ; DB.ACCT = 0 ;
    DB.AMT.ALL = 0
    T.SEL1 = "SELECT F.SCB.COM.CR WITH WRONG.ACCT.NO EQ '' AND FT.FLAG NE 'YES'"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.FT1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.ATFX,KEY.LIST1<I>,R.ATFX,F.ATFX,E1)
            DB.AMT.ALL    += R.ATFX<AB.COMMISSION>
            SET.DATE     = R.ATFX<AB.DATE.LOAD>
            DEB.AMT      = R.ATFX<AB.NET.BAL>
            DB.ACCT      = 'EGP1653900010099'
            CUR          = 'EGP'
            CR.ACCT      = R.ATFX<AB.ACCOUNT.NO>
            GOSUB OFS.MSG
            DEB.AMT   = ''
            CR.ACCT   = ''
            CUR       = ''
            DB.ACCT   = ''
            WS.COUNT++
        NEXT I
**********************************
        DEB.AMT  =  DB.AMT.ALL
        DB.ACCT = 'EGP1653900010099'
        CR.ACCT = 'PL52513'
        CUR     = 'EGP'
        GOSUB OFS.MSG
    END
    RETURN
********
OFS.MSG:
*-------
*DEBUG
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE::=":"AC64"
    OFS.MESSAGE.DATA :=  ",DEBIT.CURRENCY::=":CUR
    OFS.MESSAGE.DATA :=  ",DEBIT.ACCT.NO::=":DB.ACCT
    OFS.MESSAGE.DATA :=  ",CREDIT.ACCT.NO::=":CR.ACCT
    OFS.MESSAGE.DATA :=  ",DEBIT.AMOUNT::=":DEB.AMT
    OFS.MESSAGE.DATA :=  ",DEBIT.VALUE.DATE::=":TODAY
    OFS.MESSAGE.DATA :=  ",CREDIT.VALUE.DATE::=":TODAY
    OFS.MESSAGE.DATA :=  ",LOCAL.REF:42:1=":"COUPON"
*  OFS.MESSAGE.DATA :=  ",LOCAL.REF:45:1=":SET.DATE
    OFS.MESSAGE.DATA :=  ",COMMISSION.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",CHARGE.CODE=":"WAIVE"
    OFS.MESSAGE.DATA :=  ",PROFIT.CENTRE.DEPT::=":"99"
    OFS.MESSAGE.DATA :=  ",DEBIT.THEIR.REF::=":WS.COUNT
    OFS.MESSAGE.DATA :=  ",ORDERING.BANK::=SCB"


    SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA
    BB.IN.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.IN.DATA TO BB.IN ELSE
    END

    SCB.OFS.SOURCE = "SCBONLINE"
    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

    BB.OUT.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.OUT.DATA TO BB.OUT ELSE
    END

    GOSUB UPD.REC

    RETURN
************************************************************
UPD.REC:
*-------
    CHKVAL = FIELD (SCB.OFS.MESSAGE,'/', 3)

    IF CHKVAL [1,1] = 1 THEN
        R.ATFX<AB.FT.FLAG> = "YES"
        CHKVAL2 = FIELD (SCB.OFS.MESSAGE,'/', 1)
        R.ATFX<AB.FT.ID>  = CHKVAL2
        CALL F.WRITE(FN.ATFX,KEY.LIST1<I>,R.ATFX)
        CALL JOURNAL.UPDATE(KEY.LIST1<I>)

        CALL F.RELEASE(FN.ATFX,KEY.LIST1<I>,F.ATFX)
        CLOSE F.ATFX
    END
    RETURN
*-----------
PROGRAM.END:
*-----------

    TEXT = "�� ����� ����� �����";CALL REM
    RETURN

*-------------------------------------------------------------
END
