* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.COMP.1

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.PASSWORD.UPD
*----------------------------------------------------------
    FN.PASS  = 'F.SCB.PASSWORD.UPD'  ; F.PASS = ''
    CALL OPF(FN.PASS,F.PASS)

    COMP.ID = O.DATA
*-------------------
    DATE.TOD  = TODAY
    CALL ADD.MONTHS(DATE.TOD, '-3')

    FROM.DATE = DATE.TOD[1,6] : "01"
    END.DATE  = FROM.DATE
    CALL LAST.DAY(END.DATE)

    T.SEL  = "SELECT ":FN.PASS :" WITH COMPANY.ID EQ ":COMP.ID
    T.SEL := " AND BOOKING.DATE GE " : FROM.DATE
    T.SEL := " AND BOOKING.DATE LE " : END.DATE
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    O.DATA = SELECTED:"*":FROM.DATE[1,4]:"/":FROM.DATE[5,2]
    RETURN
END
