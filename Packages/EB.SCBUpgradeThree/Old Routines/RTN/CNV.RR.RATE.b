* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.RR.RATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.CREDIT.INT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ACCT.CR
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
    COMP = ID.COMPANY

    FN.SCR = 'FBNK.STMT.ACCT.CR' ; F.SCR = ''
    CALL OPF(FN.SCR,F.SCR)

    XX = ''
    XX = O.DATA
    SCR.ID   = FIELD(XX,'*',1)
    SCR.RATE = FIELD(XX,'*',2)
    SCR.TOT  = FIELD(XX,'*',3)
    IF ( SCR.RATE EQ 0 OR SCR.RATE EQ '' ) AND SCR.TOT GT 0 THEN
        CALL F.READ(FN.SCR,SCR.ID,R.SCR,F.SCR,ERRX)
        INT.RATE = R.SCR<IC.STMCR.CR.INT.RATE>
*Line [ 45 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        DA1 = DCOUNT(INT.RATE,@VM)
        FOR AA = 1 TO DA1
            SCR.INT = R.SCR<IC.STMCR.CR.INT.RATE,AA>
            IF SCR.INT NE '' AND SCR.INT NE 0 THEN
                SCR.RATE = SCR.INT
                AA = DA1
            END
        NEXT AA
    END
    O.DATA = SCR.RATE
    RETURN
