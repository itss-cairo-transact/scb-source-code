* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    PROGRAM CONV.CUR.DETAIL.LOAN.EMP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LINE.GENDETALL

    COMP = ID.COMPANY
*-----------------------------------------------------------------------

    OPENSEQ "&SAVEDLISTS&" , "CUR.DETAIL.LOAN.EMP.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"CUR.DETAIL.LOAN.EMP.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "CUR.DETAIL.LOAN.EMP.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CUR.DETAIL.LOAN.EMP.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create CUR.DETAIL.LOAN.EMP.CSV File IN &SAVEDLISTS&'
        END
    END

*----------------------------------------------------------------

    FN.LN   = 'F.RE.STAT.REP.LINE'  ; F.LN   = '' ; R.LN = ''
    FN.CONT = 'F.RE.STAT.LINE.CONT' ; F.CONT = '' ; R.CONT = ''
    FN.CCY  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CCY  = '' ; R.CCY = ''
    FN.SLN  = 'F.SCB.LINE.GENDETALL'    ; F.SLN  = '' ; R.SLN = ''
    FN.CONS = 'F.CONSOLIDATE.ASST.LIAB' ; F.CONS = ''

    CALL OPF(FN.LN,F.LN) ; CALL OPF(FN.CONT,F.CONT) ; CALL OPF(FN.CCY,F.CCY)
    CALL OPF(FN.CONS,F.CONS)

    DAT.ID = COMP
    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,DAT.ID,TD1)
    TDD = TODAY
    CALL CDT("",TDD,'-1W')

    KK = 'EG00100'
    NK = ''
    NK<1,1>='01'  ; NK<1,2>='02'  ; NK<1,3>='03'  ; NK<1,4>='04'  ; NK<1,5>='05'
    NK<1,6>='06'  ; NK<1,7>='07'  ; NK<1,8>='09'  ; NK<1,9>='10'  ; NK<1,10>='11'
    NK<1,11>='12' ; NK<1,12>='13' ; NK<1,13>='14' ; NK<1,14>='15' ; NK<1,15>='20'
    NK<1,16>='21' ; NK<1,17>='22' ; NK<1,18>='23' ; NK<1,19>='30' ; NK<1,20>='31'
    NK<1,21>='32' ; NK<1,22>='35' ; NK<1,23>='40' ; NK<1,24>='50' ; NK<1,25>='60'
    NK<1,26>='70' ; NK<1,27>='80' ; NK<1,28>='81' ; NK<1,29>='88' ; NK<1,30>='90' ; NK<1,31>='99'

    LINE.PRINT = ''
    LINE.BR    = ''
    HEAD.DESC  = ",":"���� ������� ����� ������ ��������� - �������� - �����"

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "�������":","
    HEAD.DESC := "������":","
    HEAD.DESC := "��������":","
    HEAD.DESC := "���� ����":","
    HEAD.DESC := "����� ������":","
    HEAD.DESC := "���� ��������":","
    HEAD.DESC := "�� ������":","
    HEAD.DESC := "���� ������":","
    HEAD.DESC := "���� �����":","
    HEAD.DESC := "����":","
    HEAD.DESC := "����� ����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    NEW.LINE = ''
    TOT.LINE = ''
    GOSUB PROCESS.ASST
    RETURN
*--------------------------------------------------------------------
PROCESS.ASST:

    T.SEL = "SELECT ":FN.LN:" WITH ( @ID GE GENDETALL.0119 AND @ID LE GENDETALL.0282 ) OR ( @ID GE GENDETALL.4000 AND @ID LE GENDETALL.4011 ) OR ( @ID GE GENDETALL.3065 AND @ID LE GENDETALL.3067) OR ( @ID GE GENDETALL.3074 AND @ID LE GENDETALL.3075) OR ( @ID EQ GENDETALL.3086 OR @ID EQ GENDETALL.0117 OR @ID EQ GENDETALL.3093 OR @ID EQ GENDETALL.3130 OR @ID EQ GENDETALL.3131 OR @ID EQ GENDETALL.3136 ) BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LN,KEY.LIST<I>,R.LN,F.LN,E4)
            DESC    = R.LN<RE.SRL.DESC><1,1>
            LINE.NO = R.LN<RE.SRL.DESC><1,2>

            ASST.TYPE       = R.LN<RE.SRL.ASSET.TYPE>
*Line [ 129 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            ASST.TYPE.COUNT = DCOUNT(ASST.TYPE,@VM)

            IF I EQ 1  THEN NEW.LINE = LINE.NO

            IF NEW.LINE NE LINE.NO THEN
                NN.DATA  = "******":","
                NN.DATA := "������ ":NEW.LINE:","

                NN.DATA := TOT.LINE<1,1>:","
                NN.DATA := TOT.LINE<1,2>:","
                NN.DATA := TOT.LINE<1,3>:","
                NN.DATA := TOT.LINE<1,4>:","
                NN.DATA := TOT.LINE<1,5>:","
                NN.DATA := TOT.LINE<1,6>:","
                NN.DATA := TOT.LINE<1,7>:","
                NN.DATA := TOT.LINE<1,8>:","
                NN.DATA := TOT.LINE<1,9>:","

                WRITESEQ NN.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
                TOT.LINE = ''
            END

            DESC.ID = FIELD(KEY.LIST<I>,".",2)
            PROD.CATEG = R.LN<RE.SRL.ASSET1,1>
            PL.CATEG   = R.LN<RE.SRL.PROFIT1,1>

            IF PROD.CATEG EQ '' THEN
                CATEG = PL.CATEG
            END ELSE
                CATEG = PROD.CATEG
            END

            CALL F.READ(FN.CCY,'NZD',R.CCY,F.CCY,E1)

            CLOSE.BAL.LCL  = 0
            CLOSE.BAL.LCL2 = 0
            CLOSE.BAL.LCY  = 0
            NEW.CUR = ''

*************************************************************************
            CONT.ID = KEY.LIST<I>:"...."
            T.SEL1 = "SELECT ":FN.CONT:" WITH @ID LIKE ":CONT.ID:" BY @ID"
            CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
            IF SELECTED1 THEN
                FOR KM = 1 TO SELECTED1
                    CALL F.READ(FN.CONT,KEY.LIST1<KM>,R.CONT,F.CONT,E5)

*Line [ 179 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
                    FOR KM1 = 1 TO DECOUNT.CONT
                        CONS.ID  = R.CONT<RE.SLC.ASST.CONSOL.KEY,KM1>
                        CALL F.READ(FN.CONS,CONS.ID,R.CONS,F.CONS,E2)
                        CONT.SEC = R.CONS<RE.ASL.VARIABLE.11>
                        IF CONT.SEC EQ '1100' OR CONT.SEC EQ '1200' OR CONT.SEC EQ '1300' OR CONT.SEC EQ '1400' THEN

                            NEW.CUR = R.CONS<RE.ASL.CURRENCY>

                            IF KEY.LIST<I> GE 'GENDETALL.4000' AND KEY.LIST<I> LE 'GENDETALL.4011' THEN
                                ASST.TYPE.COUNT = 1
                            END

                            FOR II = 1 TO ASST.TYPE.COUNT
                                ASST.TYPE1 =  R.LN<RE.SRL.ASSET.TYPE,II>
*Line [ 195 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                                LOCATE ASST.TYPE1 IN R.CONS<RE.ASL.TYPE,1> SETTING TYP ELSE NULL

                                CONT.BAL = R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
*Line [ 199 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                                LOCATE R.CONS<RE.ASL.CURRENCY> IN R.CCY<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
                                RATE           = R.CCY<RE.BCP.RATE,POS>
                                CLOSE.BAL.LCL += CONT.BAL * RATE
                                CLOSE.BAL.LCY += CONT.BAL * RATE

                                FLAG = 0
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'EGP' THEN
                                    LINE.BR<1,2> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'USD' THEN
                                    LINE.BR<1,3> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'GBP' THEN
                                    LINE.BR<1,4> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'JPY' THEN
                                    LINE.BR<1,5> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'CHF' THEN
                                    LINE.BR<1,6> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'SAR' THEN
                                    LINE.BR<1,7> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF R.CONS<RE.ASL.CURRENCY> EQ 'EUR' THEN
                                    LINE.BR<1,8> += CLOSE.BAL.LCL
                                    FLAG = 1
                                END
                                IF FLAG = 0 THEN
                                    LINE.BR<1,9> += CLOSE.BAL.LCL
                                END
                                CLOSE.BAL.LCL = 0
                            NEXT II
                        END
                    NEXT KM1
                NEXT KM
            END
            LINE.BR<1,1>   = CLOSE.BAL.LCY

            TOT.LINE<1,1> += LINE.BR<1,1>
            TOT.LINE<1,2> += LINE.BR<1,2>
            TOT.LINE<1,3> += LINE.BR<1,3>
            TOT.LINE<1,4> += LINE.BR<1,4>
            TOT.LINE<1,5> += LINE.BR<1,5>
            TOT.LINE<1,6> += LINE.BR<1,6>
            TOT.LINE<1,7> += LINE.BR<1,7>
            TOT.LINE<1,8> += LINE.BR<1,8>
            TOT.LINE<1,9> += LINE.BR<1,9>

            BB.DATA  = CATEG:","
            BB.DATA := DESC:","
            BB.DATA := LINE.BR<1,1>:","
            BB.DATA := LINE.BR<1,2>:","
            BB.DATA := LINE.BR<1,3>:","
            BB.DATA := LINE.BR<1,4>:","
            BB.DATA := LINE.BR<1,5>:","
            BB.DATA := LINE.BR<1,6>:","
            BB.DATA := LINE.BR<1,7>:","
            BB.DATA := LINE.BR<1,8>:","
            BB.DATA := LINE.BR<1,9>:","

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END

            LINE.BR = 0
            NEW.LINE = LINE.NO

        NEXT I
        IF I EQ SELECTED THEN
            NN.DATA  = "******":","
            NN.DATA := "������ ":NEW.LINE:","
            NN.DATA := TOT.LINE<1,1>:","
            NN.DATA := TOT.LINE<1,2>:","
            NN.DATA := TOT.LINE<1,3>:","
            NN.DATA := TOT.LINE<1,4>:","
            NN.DATA := TOT.LINE<1,5>:","
            NN.DATA := TOT.LINE<1,6>:","
            NN.DATA := TOT.LINE<1,7>:","
            NN.DATA := TOT.LINE<1,8>:","
            NN.DATA := TOT.LINE<1,9>:","

            WRITESEQ NN.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END
            TOT.LINE = ''
        END

    END
    RETURN
