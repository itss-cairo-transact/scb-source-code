* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
******************************MAHMOUD 24/5/2015**************************
    SUBROUTINE CNV.TT.NRR2

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER

    TR.ID = FIELD(O.DATA,'*',1)
    NRR1  = FIELD(O.DATA,'*',2)
    NRR2  = ''
    FN.TT    = 'FBNK.TELLER'     ; F.TT   = '' ; R.TT   = ''
    FN.TT.H  = 'FBNK.TELLER$HIS' ; F.TT.H = '' ; R.TT.H = ''

    CALL OPF( FN.TT,F.TT)
    CALL OPF( FN.TT.H,F.TT.H)
*Line [ 36 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    FINDSTR '\B' IN TR.ID SETTING Y.XX THEN TR.ID = FIELD(TR.ID,'\',1) ELSE NULL
    IF TR.ID[1,2] EQ 'TT' THEN
        CALL F.READ( FN.TT,TR.ID,R.TT, F.TT, ETEXT)
        IF NOT(ETEXT) THEN
            NRR2 = R.TT<TT.TE.NARRATIVE.2><1,1>
        END ELSE
            CALL F.READ.HISTORY(FN.TT.H,TR.ID, R.TT.H, F.TT.H, ETEXT2)
            NRR2 = R.TT.H<TT.TE.NARRATIVE.2><1,1>
        END
        O.DATA = NRR2
    END ELSE
        O.DATA = NRR1
    END
    RETURN
