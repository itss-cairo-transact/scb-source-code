* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.CU.CR.NOR

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    FN.CU   = 'FBNK.CUSTOMER' ; F.CU = '' ; R.CU = ''
    CALL OPF( FN.CU,F.CU)

    WS.CREDIT.NUM = 0
    WS.NORMAL.NUM = 0
    WS.COMPANY = O.DATA
    O.DATA = ''
    T.SEL  ="SELECT FBNK.CUSTOMER WITH SCCD.CUSTOMER NE YES AND SCB.POSTING EQ 29 AND POSTING.RESTRICT LT 89 "
    T.SEL :="AND CREDIT.CODE LE 100 AND COMPANY.BOOK EQ ":WS.COMPANY:" AND DRMNT.CODE EQ '' WITHOUT SECTOR IN (5010 5020)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR ICU = 1 TO SELECTED
            CALL F.READ(FN.LI,KEY.LIST<ICU>,R.CU,F.CU,ER.CU)
            WS.CU.LCK.REF  = R.CU<EB.CUS.LOCAL.REF>
            WS.CREDIT.CODE = WS.CU.LCK.REF<1,CULR.CREDIT.CODE>

            IF WS.CREDIT.CODE EQ 100 THEN
                WS.CREDIT.NUM ++
            END

            IF WS.CREDIT.CODE EQ '' THEN
                WS.NORMAL.NUM ++
            END
        NEXT ICU
        O.DATA = WS.CREDIT.NUM :'*':WS.NORMAL.NUM
    END

    RETURN
END
