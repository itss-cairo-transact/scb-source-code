* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************************************NI7OOOOOOOOOOOOOOOOOOOOO************
    SUBROUTINE CNV.PHONE

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS

    XX = O.DATA
    TEXT = XX ; CALL REM
    FN.CRD = "FBNK.CARD.ISSUE"
    F.CRD  = ''
    R.CRD  = ''
    FN.CUS = "FBNK.CUSTOMER"
    F.CUS  = ''
    R.CUS  = ''

    CALL OPF(FN.CRD,F.CRD)
    CALL OPF(FN.CUS,F.CUS)

    CRD.NO  = ''
    T.SEL   = ''
    KEY.LIST= ''
    SELECTED = ''
****************************************************************
    CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,XX,MYLOCAL)
    ADR = MYLOCAL<1,CULR.TELEPHONE,1>
    IF ADR[1,2] EQ '01' THEN
        O.DATA = ADR
    END
    IF ADR[1,2] NE '01' THEN
        O.DATA = ''
    END


*****************************************************************

    RETURN
END
