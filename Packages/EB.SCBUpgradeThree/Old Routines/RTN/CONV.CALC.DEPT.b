* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* BY NESSMA MOHAMMAD
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CALC.DEPT

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT T24.BP I_F.USER
    $INSERT T24.BP I_F.DEPT.ACCT.OFFICER
    $INSERT TEMENOS.BP I_F.USR.LOCAL.REF
*-------------------------------------------
    R.PWR  = ""   ; E11 = ""
    FN.USR = "F.USER"  ; F.USR = ""
    CALL OPF(FN.USR, F.USR)

    USR.ID = FIELD(O.DATA,'_',2)

    CALL F.READ(FN.USR,USR.ID, R.USR, F.USR, E11)
    DEPT.VAL = R.USR<EB.USE.LOCAL.REF><USER.SCB.DEPT.CODE>

    CALL DBR ('DEPT.ACCT.OFFICER':@FM:2,DEPT.VAL,DEPT.COMP)
    O.DATA   = DEPT.COMP
*-------------------------------------------
    RETURN
END
