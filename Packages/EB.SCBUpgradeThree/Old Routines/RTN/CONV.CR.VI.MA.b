* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
**** CREATED BY MOHAMED SABRY 2010/05/9
**** UPDATE BY MOHAMED SABRY 2011/10/31 TO USE INDEX CARD.ISSUE.ACCOUNT ****
**** UPDATE BY MOHAMED SABRY 2011/12/19 TO USER Original cards Only

    SUBROUTINE CONV.CR.VI.MA
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CI.LOCAL.REFS
    FN.CRD = 'FBNK.CARD.ISSUE' ;F.CRD = '' ; R.CRD = ''
    CALL OPF(FN.CRD,F.CRD)

    FN.CID = "FBNK.CARD.ISSUE.ACCOUNT"   ; F.CID = ""
    CALL OPF(FN.CID,F.CID)

************************************************************
    WS.AC     = O.DATA
    CALL F.READ(FN.CID,WS.AC,R.CID,F.CID,ER.CID)
    LOOP
        REMOVE CRD.ID FROM R.CID SETTING POS.CID
    WHILE CRD.ID:POS.CID
        CALL F.READ(FN.CRD,CRD.ID,R.CRD,F.CRD,READ.ERR)
        IF NOT (READ.ERR) THEN
            WS.CRD.LOCAL = R.CRD<CARD.IS.LOCAL.REF>
            WS.CRD.CODE  = WS.CRD.LOCAL<1,LRCI.CARD.CODE>
            IF ( WS.CRD.CODE GE 101 AND WS.CRD.CODE LE 105 ) OR ( WS.CRD.CODE GE 201 AND WS.CRD.CODE LE 205 ) OR ( WS.CRD.CODE GE 501 AND WS.CRD.CODE LE 505 ) OR ( WS.CRD.CODE GE 601 AND WS.CRD.CODE LE 605 ) THEN
                IF R.CRD<CARD.IS.CANCELLATION.DATE> EQ '' THEN
                    O.DATA = CRD.ID
                    RETURN
                END  ELSE
                    O.DATA = '������.������'
                END
            END  ELSE
                O.DATA = '������.������'
            END
        END  ELSE
            O.DATA = '������.������'
        END

    REPEAT
************************************************************
    RETURN
