* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>500</Rating>      M.ELSAYED
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.GET.RET
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY

    FT.ID = O.DATA
    FN.FT.HIS = 'FBNK.FUNDS.TRANSFER$HIS' ; F.FT.HIS = ''
    CALL OPF(FN.FT.HIS,F.FT.HIS)
    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

*  FT.ID = R.STE<AC.STE.OUR.REFERENCE>
    CALL F.READ(FN.FT,FT.ID,R.FT,F.FT,ER.FT)
    IF (ER.FT) THEN
****READ HISTORY
        CALL F.READ.HISTORY(FN.FT.HIS,FT.ID,R.FT.HIS,F.FT.HIS,ER.FT.HIS)
        RET.REF  = R.FT.HIS<FT.LOCAL.REF><1,FTLR.RETRIVAL.REF.NO>
        O.DATA = RET.REF
    END ELSE
**** READ LIVE
        RET.REF = R.FT<FT.LOCAL.REF><1,FTLR.RETRIVAL.REF.NO>

        O.DATA = RET.REF
        RETURN
    END
