* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CALC.TOT.VALUES

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.MLAD.FILE.PARAM
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEG.MAS.D
*----------------------------------------------
    XX.TOT     = 0
    RECIEVE.ID = O.DATA

    XX1 = FIELD(RECIEVE.ID,'*',1)
    XX2 = FIELD(RECIEVE.ID,'*',2)
    XX3 = FIELD(RECIEVE.ID,'*',3)
    XX4 = FIELD(RECIEVE.ID,'*',4)
    XX5 = FIELD(RECIEVE.ID,'*',5)
    XX6 = FIELD(RECIEVE.ID,'*',6)
    XX7 = FIELD(RECIEVE.ID,'*',7)
    XX8 = FIELD(RECIEVE.ID,'*',8)

    XX.TOT  = XX1 + XX2 + XX3 + XX4 + XX5 + XX6 + XX7 + XX8
    O.DATA  = XX.TOT
*-----------------------------------------------
    RETURN
