* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*************************NI7OOOOOOOOOOOOO**************
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.DEPT.NOTES.MAR.AUTH

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.DEPT.SAMPLE1

    FN.SAMP = 'F.SCB.DEPT.SAMPLE1' ; F.SAMP = ''
    CALL OPF(FN.SAMP,F.SAMP)

    XX = O.DATA

    CALL F.READ(FN.SAMP,O.DATA,R.SAMP,F.SAMP,E11)

    APP1     = R.SAMP<DEPT.SAMP.NOTES.HWALA>
    APP1.MAR = R.SAMP<DEPT.SAMP.NOTES.HWALA.MAR>
    APP2     = R.SAMP<DEPT.SAMP.NOTES.LG11>
    APP2.MAR = R.SAMP<DEPT.SAMP.NOTES.LG11.MAR>
    APP3     = R.SAMP<DEPT.SAMP.NOTES.LG22>
    APP3.MAR = R.SAMP<DEPT.SAMP.NOTES.LG22.MAR>
    APP4     = R.SAMP<DEPT.SAMP.NOTES.TEL>
    APP4.MAR = R.SAMP< DEPT.SAMP.NOTES.TEL.MAR>
    APP5     = R.SAMP<DEPT.SAMP.NOTES.TF1>
* APP5.MAR = R.SAMP<>
    APP6     = R.SAMP<DEPT.SAMP.NOTES.TF2>
    APP6.MAR = R.SAMP<DEPT.SAMP.NOTES.TF2.MAR>
    APP7     = R.SAMP<DEPT.SAMP.NOTES.TF3>
    APP7.MAR = R.SAMP<DEPT.SAMP.NOTES.TF3.MAR>
    APP8     = R.SAMP<DEPT.SAMP.NOTES.BR>
    APP8.MAR = R.SAMP<DEPT.SAMP.NOTES.BR.MAR>
    APP9     = R.SAMP<DEPT.SAMP.NOTES.AC>
    APP9.MAR = R.SAMP<DEPT.SAMP.NOTES.AC.MAR>
    APP10     = R.SAMP<DEPT.SAMP.NOTES.WH>
    APP10.MAR = R.SAMP<DEPT.SAMP.NOTES.WH.MAR>


    IF APP1.MAR NE '' THEN
        O.DATA = APP1.MAR
    END
    IF APP2.MAR NE '' THEN
        O.DATA = APP2.MAR
    END
    IF APP3.MAR NE '' THEN
        O.DATA = APP3.MAR
    END
    IF APP4.MAR NE '' THEN
        O.DATA = APP4.MAR
    END
    IF APP5.MAR NE '' THEN
        O.DATA = APP5.MAR
    END
    IF APP6.MAR NE '' THEN
        O.DATA = APP6.MAR
    END
    IF APP7.MAR NE '' THEN
        O.DATA = APP7.MAR
    END
    IF APP8.MAR NE '' THEN
        O.DATA = APP8.MAR
    END
    IF APP9.MAR NE '' THEN
        O.DATA = APP9.MAR
    END
    IF APP10.MAR NE '' THEN
        O.DATA = APP10.MAR
    END


    RETURN
END
