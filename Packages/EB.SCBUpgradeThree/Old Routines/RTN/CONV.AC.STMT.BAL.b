* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-82</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.AC.STMT.BAL

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*    $INCLUDE T24.BP I_F.ACCT.ENT.TODAY


    FN.TRNS  = 'FBNK.ACCT.ENT.TODAY' ; F.TRNS = '' ; R.TRNS = ''
    CALL OPF(FN.TRNS,F.TRNS)

    FN.STMT  = 'FBNK.STMT.ENTRY' ; F.STMT = '' ; R.STMT = ''
    CALL OPF(FN.STMT,F.STMT)

    FN.ACC   = 'FBNK.ACCOUNT' ; F.ACC = '' ; R.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    CALL F.READ( FN.ACC,O.DATA, R.ACC,F.ACC, ETEXT1)
    IF NOT(ETEXT1) THEN

        CURRR     = R.ACC<AC.CURRENCY>
        OPEN.BAL  = R.ACC<AC.OPEN.ACTUAL.BAL>
        ONLIN.BAL = R.ACC<AC.ONLINE.ACTUAL.BAL>

        CALL F.READ( FN.TRNS,O.DATA, R.TRNS,F.TRNS, ETEXT)

        LOOP

            REMOVE TRNS.NO FROM R.TRNS  SETTING POS1
        WHILE TRNS.NO:POS1
            CALL F.READ(FN.STMT,TRNS.NO,R.STMT,F.STMT,ERR1)
            IF CURRR EQ 'EGP' THEN
                ST.AMT += R.STMT<AC.STE.AMOUNT.LCY>
            END ELSE
                ST.AMT += R.STMT<AC.STE.AMOUNT.FCY>
            END

        REPEAT
        BALL = OPEN.BAL + ST.AMT

        IF BALL NE ONLIN.BAL THEN
            O.DATA = O.DATA :" ** " : ONLIN.BAL : " ** " :  BALL

        END ELSE
            O.DATA = ''
****  O.DATA = O.DATA :" ** " : ONLIN.BAL : " ** " :  BALL
        END
    END ELSE

        O.DATA = ''
    END
    RETURN
END
