* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
****************************NI7OOOOOOOOOOO*******
    SUBROUTINE CNV.CCY.USD
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS

    FN.CUR  = 'FBNK.RE.BASE.CCY.PARAM'  ; F.CUR  = '' ; R.CUR = ''
    FN.LD   = 'FBNK.LD.LOANS.AND.DEPOSITS'  ; F.LD   = '' ; R.LD  = ''
    CALL OPF (FN.CUR,F.CUR)
    CALL OPF (FN.LD,F.LD)

    XX = O.DATA

    CALL F.READ(FN.LD,XX,R.LD,F.LD,ECAA1)
    AMOUNT = R.LD<LD.AMOUNT>
    CURR   = R.LD<LD.CURRENCY>

    CALL F.READ(FN.CUR,'NZD',R.CUR,F.CUR,ECAA)
*Line [ 51 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
    LOCATE 'USD' IN R.CUR<RE.BCP.ORIGINAL.CCY,1> SETTING POS ELSE NULL
    CUR.RATE    = R.CUR<RE.BCP.RATE,POS>
    RATE.TYPE   = R.CUR<RE.BCP.RATE.TYPE,POS>
    IF CURR EQ 'USD' THEN
        O.DATA11 = AMOUNT  / (1000)
        O.DATA =  FMT(O.DATA11, "L,2")
    END
    IF CURR NE 'USD' THEN
        AMT = AMOUNT * CUR.RATE
        O.DATA11 = AMT / (1000)
        O.DATA =  FMT(O.DATA11, "L,2")
    END
    RETURN
END
