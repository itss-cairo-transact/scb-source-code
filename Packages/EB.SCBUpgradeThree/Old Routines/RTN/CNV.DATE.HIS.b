* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.DATE.HIS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_DR.LOCAL.REFS

    XX = O.DATA
  *  TEXT = O.DATA ; CALL REM
    FN.LC = "FBNK.LETTER.OF.CREDIT"
    F.LC  = ''
    R.LC  = ''
    CALL OPF(FN.LC,F.LC)

    FN.DRW = "FBNK.DRAWINGS"
    F.DRW  = ''
    R.DRW  = ''
    CALL OPF(FN.DRW,F.DRW)

    FN.DRH = "FBNK.DRAWINGS$HIS"
    F.DRH  = ''
    R.DRH  = ''
    CALL OPF(FN.DRH,F.DRH)

    IF LEN(O.DATA) EQ 14  THEN
        DR.ID  = XX :";1"
        CALL F.READ(FN.DRH,DR.ID, R.DRH, F.DRH,ETEXT)
        ISSUE.DATE = R.DRH<TF.DR.LOCAL.REF,DRLR.REG.DATE>
    END
    IF ETEXT THEN
        DR.ID = XX
        CALL F.READ(FN.DRW,XX, R.DRW, F.DRW,ETEXT)
        *TEXT = XX ; CALL REM
        ISSUE.DATE = R.DRW<TF.DR.LOCAL.REF,DRLR.REG.DATE>
        *TEXT = ISSUE.DATE ; CALL REM
*END
    END
    IF LEN(XX) EQ 12 THEN
        CALL F.READ(FN.LC,XX,R.LC,F.LC,ER.LC)
        ISSUE.DATE =  R.LC<TF.LC.ISSUE.DATE>
    END

    O.DATA = ISSUE.DATE

    RETURN
END
