* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>195</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.AMT.COLL.H

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS

    CU   = FIELD(O.DATA,"*",1)
    CATG = FIELD(O.DATA,"*",2)
    CURR = FIELD(O.DATA,"*",3)
    FN.LD ='FBNK.LD.LOANS.AND.DEPOSITS' ;R.LD='';F.LD=''
    CALL OPF(FN.LD,F.LD)

    FN.COLL = 'FBNK.COLLATERAL' ; R.COLL = '' ; F.COLL = ''
    CALL OPF(FN.COLL,F.COLL)

    T.SEL    = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH AMOUNT GT 0 AND COLLATERAL.ID NE '' AND CUSTOMER.ID EQ ":CU:" AND CATEGORY EQ ":CATG:" AND CURRENCY EQ ":CURR
    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""
    HHH      = '0'
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED = 0 THEN O.DATA = ""
    COLL.AMT = ""
    O.DATA = ""
    FOR I = 1 TO SELECTED
        CALL F.READ(FN.LD,KEY.LIST<I>, R.LD, F.LD,E)
        COL.ID = R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>
        CURR = R.LD<LD.CURRENCY>
        CALL F.READ(FN.COLL,COL.ID, R.COLL, F.COLL,E1)
        COLL.AMT + = R.COLL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
    NEXT I
    O.DATA = COLL.AMT

    RETURN
END
