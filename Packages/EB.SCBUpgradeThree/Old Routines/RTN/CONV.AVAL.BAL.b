* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.AVAL.BAL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY

*------------------------------------------------
    FN.CA = 'FBNK.CUSTOMER.ACCOUNT' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)


    CUST.ID = O.DATA
    AC.AMT  = 0
*------------------------------------------------
    CALL F.READ(FN.CA,CUST.ID,R.CA,F.CA,E2)

    LOOP
        REMOVE ACC.NO FROM R.CA SETTING POS.ACC
    WHILE ACC.NO:POS.ACC

        CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,ER.AC)
        AC.CATEG = R.AC<AC.CATEGORY>

        IF AC.CATEG EQ 1001 OR ( AC.CATEG GE 1003 AND AC.CATEG LE 1009 ) OR ( AC.CATEG GE 6501 AND AC.CATEG LE 6517 ) THEN
            AC.CCY   = R.AC<AC.CURRENCY>

            CALL F.READ(FN.CCY,AC.CCY,R.CCY,F.CCY,E1)

            IF AC.CCY EQ 'EGP' THEN
                WS.RATE = 1
            END ELSE
                IF AC.CCY EQ 'JPY' THEN
                    WS.RATE = R.CCY<SBD.CURR.MID.RATE> / 100
                END ELSE
                    WS.RATE = R.CCY<SBD.CURR.MID.RATE>
                END
            END

            AC.AMT  +=  R.AC<AC.ONLINE.ACTUAL.BAL> * WS.RATE
        END

    REPEAT

    AC.AMT = DROUND(AC.AMT,'2')
    O.DATA = AC.AMT

    RETURN
END
