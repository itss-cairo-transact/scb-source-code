* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.LD.RATE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

*-------------------------------------------------------------------------
    FN.LD  = 'FBNK.LD.LOANS.AND.DEPOSITS'     ; F.LD  = '' ; R.LD  = ''
    FN.LDH = 'FBNK.LD.LOANS.AND.DEPOSITS$HIS' ; F.LDH = '' ; R.LDH = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LDH,F.LDH)
    XX = O.DATA
    CALL F.READ(FN.LD,XX,R.LD,F.LD,E1)
*    INT.RATE = R.LD<LD.INTEREST.RATE>
    INT.SPRD = R.LD<LD.INTEREST.SPREAD>
    IF INT.SPRD EQ '' THEN
        CALL F.READ(FN.LDH,XX:';1',R.LDH,F.LDH,EH1)
*        INT.RATE = R.LDH<LD.INTEREST.RATE>
        INT.SPRD = R.LDH<LD.INTEREST.SPREAD>
    END
    RATE = INT.SPRD
    O.DATA = RATE
    RETURN
*==============================================================
END
