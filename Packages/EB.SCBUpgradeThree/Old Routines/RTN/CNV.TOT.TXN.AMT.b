* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************MAHMOUD 17/1/2012*****************
    SUBROUTINE CNV.TOT.TXN.AMT

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT

    GOSUB INITIATE
*Line [ 31 ] Adding EB.SCBUpgradeThree. in order to call another routine - ITSS - R21 Upgrade - 2021-12-26
    GOSUB CALLDB
    GOSUB PROCESS
    RETURN
**********************************************
INITIATE:
*--------
    ACCT.ID = O.DATA
    FROM.DATE = TODAY
    CALL ADD.MONTHS(FROM.DATE,'-6')
    END.DATE  = TODAY
    CRN = 0
    DBN = 0
    CR.AMT = 0
    DB.AMT = 0
    L.CR.AMT = 0
    L.CR.DATE = ''
    L.DR.AMT = 0
    L.DR.DATE = ''
    RETURN
**********************************************
CALLDB:
*--------
    FN.ACC = "FBNK.ACCOUNT"    ; F.ACC  = '' ; R.ACC  = '' ; CALL OPF(FN.ACC,F.ACC)
    FN.STE = "FBNK.STMT.ENTRY" ; F.STE  = '' ; R.STE  = '' ; CALL OPF(FN.STE,F.STE)
    RETURN
**********************************************
PROCESS:
*-------
    CALL DBR('ACCOUNT':@FM:AC.CURRENCY,ACCT.ID,ACC.CUR)
    CALL EB.ACCT.ENTRY.LIST(ACCT.ID<1>,FROM.DATE,END.DATE,ID.LIST,OPENING.BAL,ER)
    TXN.COUNT  = DCOUNT(ID.LIST,@FM)
    LOOP
        REMOVE STE.ID FROM ID.LIST SETTING POS.STE
    WHILE STE.ID:POS.STE
        CALL F.READ(FN.STE,STE.ID,R.STE,F.STE,ER.STE)
        STE.B.DATE = R.STE<AC.STE.BOOKING.DATE>
        IF ACC.CUR EQ LCCY THEN
            STE.AMT = R.STE<AC.STE.AMOUNT.LCY>
        END ELSE
            STE.AMT = R.STE<AC.STE.AMOUNT.FCY>
        END
        IF STE.AMT GT 0 THEN
            CRN++
            CR.AMT += STE.AMT
            L.CR.AMT  = STE.AMT
            L.CR.DATE = STE.B.DATE
        END ELSE
            DBN++
            DB.AMT += STE.AMT
            L.DR.AMT  = STE.AMT
            L.DR.DATE = STE.B.DATE
        END
    REPEAT

    O.DATA = DBN:"*":DB.AMT:"*":CRN:"*":CR.AMT:"*":L.CR.AMT:"*":L.CR.DATE:"*":L.DR.AMT:"*":L.DR.DATE:"*":OPENING.BAL

    RETURN
****************************************
END
