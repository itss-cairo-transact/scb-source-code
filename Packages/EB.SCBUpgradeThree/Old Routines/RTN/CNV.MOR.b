* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
********************************NI7OOOOOOOOOOOO******************
    SUBROUTINE CNV.MOR

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS

    FN.LD = "FBNK.LD.LOANS.AND.DEPOSITS" ; F.LD = '' ; R.LD = ''
    FN.LD.NAU='FBNK.LD.LOANS.AND.DEPOSITS$NAU' ; F.LD.NAU = ''
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.LD.NAU,F.LD.NAU)

    XX = O.DATA
    YY = FIELD(XX,'-',1)

    T.SEL = "SELECT FBNK.LD.LOANS.AND.DEPOSITS WITH @ID EQ " : YY
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED NE 0 THEN
        CALL F.READ(FN.LD,KEY.LIST,R.LD,F.LD,E2)
        CUSID = R.LD<LD.CUSTOMER.ID>
        OLDNO = R.LD<LD.LOCAL.REF><1,LDLR.OLD.NO>
        DAC   = R.LD<LD.LOCAL.REF><1,LDLR.DEBIT.ACCT>

        O.DATA = CUSID
    END ELSE
        T.SEL2 = "SELECT FBNK.LD.LOANS.AND.DEPOSITS$NAU WITH @ID EQ " : YY
        CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG)
        IF SELECTED2 NE 0 THEN
            CALL F.READ(FN.LD.NAU,KEY.LIST2,R.LD.NAU,F.LD.NAU,E2)
            CUSID = R.LD.NAU<LD.CUSTOMER.ID>
            OLDNO = R.LD.NAU<LD.LOCAL.REF><1,LDLR.OLD.NO>
            DAC   = R.LD.NAU<LD.LOCAL.REF><1,LDLR.DEBIT.ACCT>
            O.DATA = CUSID
        END
    END
    RETURN
END
