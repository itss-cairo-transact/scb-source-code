* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*--- CREATE BY NESSMA MOHAMMED
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CNV.VAL.DAT

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
    $INSERT T24.BP  I_F.STMT.ENTRY
    $INSERT T24.BP  I_F.BILL.REGISTER
    $INSERT T24.BP  I_ENQUIRY.COMMON
    $INSERT T24.BP  I_F.FUNDS.TRANSFER
    $INSERT T24.BP  I_F.TELLER
    $INSERT T24.BP  I_F.LD.LOANS.AND.DEPOSITS
    $INSERT TEMENOS.BP I_LD.LOCAL.REFS
    $INSERT TEMENOS.BP I_BR.LOCAL.REFS
    $INSERT TEMENOS.BP I_FT.LOCAL.REFS
    $INSERT TEMENOS.BP I_TT.LOCAL.REFS
    $INSERT TEMENOS.BP I_F.SCB.CUS.BR.V.DATE
*--------------------------------------------------------
    FN.CUS.V ='F.SCB.CUS.BR.V.DATE' ; F.CUS.V = ''
    CALL OPF(FN.CUS.V,F.CUS.V)

    CUS.BR     = O.DATA
    CALL F.READ(FN.CUS.V,CUS.BR, R.CUS.V, F.CUS.V, ETEXT.V)
    CUS.V.DATE = R.CUS.V<BR.V.NO.VALUE.DAY>

    NEW.VALUE.DATE = TODAY

    IF CUS.V.DATE EQ '' THEN
        CALL CDT('', NEW.VALUE.DATE , '+1W')
    END ELSE
        HH = '+':CUS.V.DATE:'W'
        CALL CDT('', NEW.VALUE.DATE , HH)
    END

    O.DATA = NEW.VALUE.DATE
*---------------------------------------------------------
    RETURN
END
