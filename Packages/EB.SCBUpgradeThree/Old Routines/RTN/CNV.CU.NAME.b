* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************************NI7OOOOOOOOOOOOOO************************
    SUBROUTINE CNV.CU.NAME

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.RETIREMENTS


    FN.AC = 'FBNK.ACCOUNT' ;  F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.AC.HIS = 'FBNK.ACCOUNT$HIS' ;  F.AC.HIS = ''
    CALL OPF(FN.AC.HIS,F.AC.HIS)

    FN.CU = 'FBNK.CUSTOMER' ;  F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CU.HIS = 'FBNK.CUSTOMER$HIS' ;  F.CU.HIS = ''
    CALL OPF(FN.CU.HIS,F.CU.HIS)

    FN.CAT = 'F.CATEGORY' ;  F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    CALL F.READ(FN.AC,O.DATA,R.AC,F.AC,ETEXT)
    IF NOT(ETEXT) THEN
        CUS.NO = R.AC<AC.CUSTOMER>
        PL.NO  = R.AC<AC.CATEGORY>

        IF CUS.NO NE '' THEN
            WW = CUS.NO
            CALL F.READ(FN.CU,WW,R.CU,F.CU,ETEXT2)
            NAME1  = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            O.DATA = NAME1
        END
        IF CUS.NO EQ '' THEN
            ZZ = PL.NO
            CALL F.READ(FN.CAT,ZZ,R.CAT,F.CAT,ETEXT3)
            NAME2 = R.CAT<EB.CAT.DESCRIPTION,1>
            O.DATA = NAME2
        END

    END
    IF ETEXT THEN
        YY = O.DATA:';1'
        CALL F.READ(FN.AC.HIS,YY,R.AC.HIS,F.AC.HIS,ETEXT)
        CUS.NO.HIS = R.AC.HIS<AC.CUSTOMER>
        PL.NO.HIS  = R.AC.HIS<AC.CATEGORY>

        IF CUS.NO.HIS NE '' THEN
            WW1 = CUS.NO.HIS:';1'
            CALL F.READ(FN.CU,WW1,R.CU,F.CU,ETEXT4)
            NAME3 = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            O.DATA = NAME3
        END
        IF PL.NO.HIS EQ '' THEN
            ZZ1 = PL.NO.HIS:';1'
            CALL F.READ(FN.CAT,ZZ1,R.CAT,F.CAT,ETEXT5)
            NAME4 = R.CAT<EB.CAT.DESCRIPTION,1>
            O.DATA = NAME4

        END

    END
    RETURN
END
