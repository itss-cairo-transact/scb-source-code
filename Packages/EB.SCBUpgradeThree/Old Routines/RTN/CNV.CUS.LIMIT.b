* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.CUS.LIMIT
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT

    XX = O.DATA
    FN.LIM = "FBNK.LIMIT"
    F.LIM  = ''
    R.LIM  = ''
    LIMIT = 0

    CALL OPF(FN.LIM,F.LIM)

    T.SEL  = "SELECT ":FN.LIM:" WITH LIM.CUS EQ ":XX
    CALL EB.READLIST(T.SEL,K.LIST,"",SELECTED,ER.MSG)
    IF SELECTED  THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LIM,KEY.LIST<I>,R.LIM,F.LIM,E1)
            LIMIT += R.LIM<LI.MAXIMUM.TOTAL>
        NEXT I
        O.DATA = LIMIT
    END ELSE
        O.DATA = 0
    RETURN
END
