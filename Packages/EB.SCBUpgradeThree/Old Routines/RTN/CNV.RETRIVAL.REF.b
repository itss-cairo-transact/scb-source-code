* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
************************MAHMOUD 10/11/2013*************************
    SUBROUTINE CNV.RETRIVAL.REF

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    SS = ''
    Y.RET.REF = ''
    AA = O.DATA
    IF AA[1,2] EQ 'FT' THEN
*Line [ 34 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
        FINDSTR '\' IN AA SETTING Y.XX THEN AA = FIELD(AA,'\',1) ELSE NULL
        CALL DBR('FUNDS.TRANSFER':@FM:FT.LOCAL.REF,AA,FT.LCL)
        IF NOT(FT.LCL) THEN
            AA = AA:";1"
            CALL DBR('FUNDS.TRANSFER$HIS':@FM:FT.LOCAL.REF,AA,FT.LCL)
        END
        Y.RET.REF = FT.LCL<1,FTLR.RETRIVAL.REF.NO>
    END
    IF Y.RET.REF NE '' THEN
        O.DATA = Y.RET.REF
    END ELSE
        O.DATA = ''
    END
    RETURN
END
