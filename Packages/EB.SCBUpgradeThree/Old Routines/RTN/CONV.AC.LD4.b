* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CONV.AC.LD4
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COLLATERAL
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_LD.LOCAL.REFS
************************************************************
    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
    RETURN
************************************************************
INIT:
    XX = O.DATA
    AMT = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.LD = 'FBNK.LD.LOANS.AND.DEPOSITS'
    F.LD = ''
    R.LD = ''

    FN.COL = 'FBNK.COLLATERAL'
    F.COL = ''
    R.COL = ''
    RETURN
************************************************************
OPEN:
    CALL OPF(FN.LD,F.LD)
    CALL OPF(FN.COL,F.COL)
    RETURN
PROCESS:
************************************************************
    T.SEL = "SELECT ":FN.LD: " WITH CUSTOMER.ID EQ ":XX:" AND CATEGORY GE 21000 AND CATEGORY LE 21010 AND CURRENCY NE 'EGP'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.LD,KEY.LIST<I>,R.LD,F.LD,EER)
            COL.ID = R.LD<LD.LOCAL.REF,LDLR.COLLATERAL.ID>

            CALL F.READ(FN.COL,COL.ID,R.COL,F.COL,EER1)
            AMT +=  R.COL<COLL.LOCAL.REF,COLR.COLL.AMOUNT>
        NEXT I
    END

    O.DATA = AMT
    RETURN
END
