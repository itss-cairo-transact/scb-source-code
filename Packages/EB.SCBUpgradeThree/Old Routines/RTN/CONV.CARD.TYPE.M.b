* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.CARD.TYPE.M

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CARD.ISSUE

    IF O.DATA THEN
        C.CODE = O.DATA
        IF (C.CODE EQ 501 OR C.CODE EQ 504 OR C.CODE EQ 510 OR C.CODE EQ 512) THEN
            NNN  = '1'
        END
        IF (C.CODE EQ 502 OR C.CODE EQ 505 OR C.CODE EQ 511 OR C.CODE EQ 513) THEN
            NNN  = '2'
        END
        IF (C.CODE EQ 601 OR C.CODE EQ 604 OR C.CODE EQ 610 OR C.CODE EQ 612) THEN
            NNN  = '3'
        END
        IF (C.CODE EQ 602 OR C.CODE EQ 605 OR C.CODE EQ 611 OR C.CODE EQ 613) THEN
            NNN  = '4'
        END

        O.DATA = NNN

    END
    RETURN
END
