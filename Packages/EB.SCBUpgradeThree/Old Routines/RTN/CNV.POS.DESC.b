* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : ITSSGlobal-Egypt 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeThree
*DONE
    SUBROUTINE CNV.POS.DESC

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCT.ENTRY.STMT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LETTER.OF.CREDIT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DRAWINGS

    ETEXT = ''
    YYY = FIELD(O.DATA,'*',5)
    IF YYY[1,2] EQ 'TF' THEN
        FN.DR   = 'FBNK.DRAWINGS' ; F.DR = '' ; R.DR = ''
        CALL OPF(FN.DR,F.DR)
        CALL F.READ(FN.DR,YYY, R.DR, F.DR,ETEXT)
        IF NOT(ETEXT) THEN
            FN.LC   = 'FBNK.LETTER.OF.CREDIT' ; F.LC = '' ; R.LC = ''
            CALL OPF(FN.LC,F.LC)
            CALL F.READ(FN.LC,YYY[1,12],R.LC,F.LC,ETEXT1)
            CATT = R.LC<TF.LC.CATEGORY.CODE>
            IF CATT EQ '23100' OR CATT EQ '23102' OR CATT EQ '23152' OR CATT EQ '23155' OR CATT EQ '23150' THEN
                O.DATA = "������� ��� ����"
            END ELSE
                O.DATA = ""
            END
        END ELSE
            O.DATA = ""
        END
        END ELSE IF YYY[1,2] EQ 'PD' THEN
            O.DATA = "������� ����"
        END ELSE
            O.DATA = ""
        END
        RETURN
    END
