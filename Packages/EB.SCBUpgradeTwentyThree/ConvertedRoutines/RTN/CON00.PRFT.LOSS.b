* @ValidationCode : MjotOTM5NDUwMDM0OkNwMTI1MjoxNjQ4NTQ2ODcwMTg0Ok1vdW5pcjotMTotMTowOjA6ZmFsc2U6Ti9BOlIyMV9BTVIuMDotMTotMQ==
* @ValidationInfo : Timestamp         : 29 Mar 2022 11:41:10
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Mounir
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*CHANGE THE NAME OF ROUTINE FROM CON%00.PRFT.LOSS INTO CON00.PRFT.LOSS
SUBROUTINE CON00.PRFT.LOSS(ENQ)

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CONSOLIDATE.PRFT.LOSS
**************************************************

    T.SEL = "SELECT FBNK.CONSOLIDATE.PRFT.LOSS WITH BALANCE NE '' OR DEBIT.MOVEMENT NE '' OR CREDIT.MOVEMENT NE '' OR CCY.BALANCE NE '' OR CCY.DEBIT.MVE NE '' OR CCY.CREDT.MVE NE '' OR BALANCE.YTD NE '' OR CCY.BALANCE.YTD NE ''"
    ENQ.LP  = '' ; OPER.VAL = ''

*Line [ 39 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    FOR ENQ.LP = 1 TO DCOUNT(ENQ<2>,@VM)
        IF ENQ<3,ENQ.LP> = 'LK' THEN
            OPER.VAL = 'LIKE'
        END
        IF ENQ<3,ENQ.LP> = 'UL' THEN
            OPER.VAL = 'UNLIKE'
        END
        IF ENQ<3,ENQ.LP> # 'LK' AND  ENQ<3,ENQ.LP> # 'UL' THEN
            OPER.VAL = ENQ<3,ENQ.LP>
        END
        T.SEL := ' AND ':ENQ<2,ENQ.LP>:' ':OPER.VAL:' ':ENQ<4,ENQ.LP>

    NEXT ENQ.LP
*    T.SEL := ' AND VERSION.NAME EQ ':VER.NAME
    KEY.LIST = '' ; SELECTED = '' ; ER.MSG = ''
    CALL EB.READLIST(T.SEL , KEY.LIST,'',SELECTED,ER.MSG)
    IF SELECTED >= 1 THEN
        ENQ.LP = 0
        FOR ENQ.LP = 1 TO SELECTED
            ENQ<2,ENQ.LP> = '@ID'
            ENQ<3,ENQ.LP> = 'EQ'
            ENQ<4,ENQ.LP> = KEY.LIST<ENQ.LP>
        NEXT ENQ.LP
    END
    ELSE
        ENQ<2> = '@ID'
        ENQ<3> = 'EQ'
        ENQ<4> = 'DUUMY'
    END

RETURN
END
