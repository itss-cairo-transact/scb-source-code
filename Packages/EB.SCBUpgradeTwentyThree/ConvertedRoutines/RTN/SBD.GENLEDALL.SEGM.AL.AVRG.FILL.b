* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBD.GENLEDALL.SEGM.AL.AVRG.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CONSOLIDATE.ASST.LIAB
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENLEDALL.SEGM.AVRG
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========

    DAT = TODAY
    CALL CDT("",DAT,'-1W')
    AVG.ID1 = DAT:'.AL'

*--------------------------------------

    FN.CONT = 'F.RE.STAT.LINE.CONT' ; F.CONT = ''
    CALL OPF(FN.CONT,F.CONT)

    FN.CONS = 'F.CONSOLIDATE.ASST.LIAB' ; F.CONS = ''
    CALL OPF(FN.CONS,F.CONS)

    FN.LN = 'F.RE.STAT.REP.LINE' ; F.LN = ''
    CALL OPF(FN.LN,F.LN)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.AVG = 'F.SCB.GENLEDALL.SEGM.AVRG' ; F.AVG = ''
    CALL OPF(FN.AVG,F.AVG)

    WS.CATEG    = '' ; WS.CATEG.DESC = '' ; WS.CCY       = '' ; WS.AMT.LCY = 0
    WS.AMT.FCY  = 0  ; WS.LINE.NO    = 0  ; WS.LINE.DESC = '' ; WS.COMP = ''
    WS.BRN.NAME = '' ; WS.PRODUCT    = 0  ; WS.TARGET    = 0  ; WS.TARGET.NAME = 0

    RETURN
*-----------------------------------------------------------------------------
PROCESS:
*=======
    CONT.ID = 'GENLEDALL...'
    BRN.88  = '...EG0010088'

    T.SEL1 = "SELECT ":FN.CONT:" WITH @ID LIKE ":CONT.ID:" AND @ID UNLIKE ":BRN.88:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF SELECTED1 THEN
        FOR KM = 1 TO SELECTED1
            CALL F.READ(FN.CONT,KEY.LIST1<KM>,R.CONT,F.CONT,E5)
*Line [ 93 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            DECOUNT.CONT = DCOUNT(R.CONT<RE.SLC.ASST.CONSOL.KEY>,@VM)
            FOR KM1 = 1 TO DECOUNT.CONT
                ASST.CONS.ID    = R.CONT<RE.SLC.ASST.CONSOL.KEY,KM1>
                ASST.TYPE       = R.CONT<RE.SLC.ASSET.TYPE,KM1>
*Line [ 98 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                ASST.TYPE.COUNT = DCOUNT(ASST.TYPE,@SM)

                CALL F.READ(FN.CONS,ASST.CONS.ID,R.CONS,F.CONS,E2)

                WS.TARGET = R.CONS<RE.ASL.VARIABLE.9>
                WS.CATEG  = R.CONS<RE.ASL.VARIABLE.1>
                WS.CCY    = R.CONS<RE.ASL.CURRENCY>

                CALL F.READ(FN.CAT,WS.CATEG,R.CAT,F.CAT,E4)
                WS.CATEG.DESC = R.CAT<EB.CAT.DESCRIPTION,1>

                CALL F.READ(FN.TAR,WS.TARGET,R.TAR,F.TAR,E3)
                WS.TARGET.NAME = R.TAR<EB.TAR.DESCRIPTION,1>

                WS.LINE.NO   = FIELD(KEY.LIST1<KM>,".",2)
                WS.COMP      = FIELD(KEY.LIST1<KM>,".",3)

                LN.ID = 'GENLEDALL.':WS.LINE.NO
                CALL F.READ(FN.LN,LN.ID,R.LN,F.FN,E5)
                WS.LINE.DESC = R.LN<RE.SRL.DESC><1,1>

*Line [ 120 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,WS.COMP,WS.BRN.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,WS.COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
WS.BRN.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

                CALL F.READ(FN.CCY,WS.CCY,R.CCY,F.CCY,E1)
                IF WS.CCY EQ 'EGP' THEN
                    RATE = 1
                END ELSE
                    IF WS.CCY EQ 'JPY' THEN
                        RATE = R.CCY<SBD.CURR.MID.RATE> / 100
                    END ELSE
                        RATE = R.CCY<SBD.CURR.MID.RATE>
                    END
                END

                FOR II = 1 TO ASST.TYPE.COUNT
                    ASST.TYPE1 =  R.CONT<RE.SLC.ASSET.TYPE,KM1,II>
*Line [ 135 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
                    LOCATE ASST.TYPE1 IN R.CONS<RE.ASL.TYPE,1> SETTING TYP ELSE NULL

                    IF WS.CCY EQ 'EGP' THEN
                        WS.AMT.FCY += R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
                        WS.AMT.LCY  = WS.AMT.FCY
                    END ELSE
                        WS.AMT.FCY += R.CONS<RE.ASL.BALANCE,TYP> + R.CONS<RE.ASL.DEBIT.MOVEMENT,TYP> + R.CONS<RE.ASL.CREDIT.MOVEMENT,TYP>
                        WS.AMT.LCY += R.CONS<RE.ASL.LOCAL.BALANCE,TYP> + R.CONS<RE.ASL.LOCAL.DEBIT.MVE,TYP> + R.CONS<RE.ASL.LOCAL.CREDT.MVE,TYP>
                    END
                NEXT II

                IF WS.CATEG NE '' AND WS.CATEG.DESC NE '' THEN
                    AVG.ID = ASST.CONS.ID:'.':WS.LINE.NO:'.':AVG.ID1
                    CALL F.READ(FN.AVG,AVG.ID,R.AVG,F.AVG,E5)

                    R.AVG<SEGM.FILE.TYPE>         = 'AL'
                    R.AVG<SEGM.FILE.DATE>         = DAT
                    R.AVG<SEGM.CATEGORY>          = WS.CATEG
                    R.AVG<SEGM.CATEGORY.DESC>     = WS.CATEG.DESC
                    R.AVG<SEGM.CURRENCY>          = WS.CCY
                    R.AVG<SEGM.AMOUNT.EQUIVALENT> = WS.AMT.LCY
                    R.AVG<SEGM.NATIVE.AMOUNT>     = WS.AMT.FCY
                    R.AVG<SEGM.LINE>              = WS.LINE.NO
                    R.AVG<SEGM.LINE.DESC>         = WS.LINE.DESC
                    R.AVG<SEGM.BRANCH.CODE>       = WS.COMP
                    R.AVG<SEGM.BRANCH.NAME>       = WS.BRN.NAME
                    R.AVG<SEGM.PRODUCT.TYPE>      = WS.PRODUCT
                    R.AVG<SEGM.SEGM.CODE>         = WS.TARGET
                    R.AVG<SEGM.SEGM.DESC>         = WS.TARGET.NAME
                    R.AVG<SEGM.CONSOL.KEY>        = ASST.CONS.ID

                    CALL F.WRITE(FN.AVG,AVG.ID,R.AVG)
                    CALL JOURNAL.UPDATE(AVG.ID)

********** CHECK HOLIDAY ******
                    TD = DAT
                    CALL CDT("",TD,'+1C')

                    CALL AWD("EG00",TD,AA)
                    IF AA EQ 'H' THEN
                        LOOP WHILE AA = 'H'

                            AVG.ID.2 = ASST.CONS.ID:'.':WS.LINE.NO:'.':TD:'.AL'
                            EXECUTE 'COPY FROM F.SCB.GENLEDALL.SEGM.AVRG TO F.SCB.GENLEDALL.SEGM.AVRG':' ':AVG.ID:',':AVG.ID.2

                            CALL CDT("",TD,'+1C')
                            CALL AWD("EG00",TD,AA)
                        REPEAT
                    END
*********************

                END
                WS.AMT.FCY = 0
                WS.AMT.LCY = 0

            NEXT KM1

            WS.CATEG    = '' ; WS.CATEG.DESC = '' ; WS.CCY       = '' ; WS.AMT.LCY = 0
            WS.AMT.FCY  = 0  ; WS.LINE.NO    = 0  ; WS.LINE.DESC = '' ; WS.COMP = ''
            WS.BRN.NAME = '' ; WS.PRODUCT    = 0  ; WS.TARGET    = 0  ; WS.TARGET.NAME = 0

        NEXT KM

    END

    RETURN
*====================================================
