* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    SUBROUTINE VNC.CHEQ.APPL.CHK

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CHEQ.APPL

*-------------------------------------------------
    IF V$FUNCTION = 'A' THEN

        FN.FT = 'FBNK.FUNDS.TRANSFER$NAU' ; F.FT = ''
        CALL OPF(FN.FT,F.FT)

        FN.CHQ = 'F.SCB.CHEQ.APPL' ; F.CHQ = ''
        CALL OPF(FN.CHQ,F.CHQ)

        CALL F.READ(FN.FT,COMI,R.FT,F.FT,ERR)
        CHEQ.APPL.ID = R.FT<FT.DEBIT.THEIR.REF>

        CALL F.READ(FN.CHQ,CHEQ.APPL.ID,R.CHQ,F.CHQ,ERR.CHQ)

        IF ERR.CHQ THEN
            E = '��� ������� ��� ���� - ��� ����� ��� ������� ����'
            CALL STORE.END.ERROR
        END

    END

    RETURN
END
