* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    SUBROUTINE MKSA.VNC.DEFAULT.X.FCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN

    AMT.1 = 0
*ACC.1 = '9943330010508199'
*ACC.2 = '9943330010508001'
    ACC.1 = '9943330020507199'
    ACC.2 = '9943330020507099'

    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,ERR1)
    RATE    = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
**************************************************************
    IF V$FUNCTION = 'I' OR V$FUNCTION = 'A' THEN
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,1> = ACC.1
        R.NEW(INF.MLT.SIGN)<1,1> = 'DEBIT'
        R.NEW(INF.MLT.TXN.CODE)<1,1> = '78'
*R.NEW(INF.MLT.CURRENCY)<1,1> = 'EGP'
        R.NEW(INF.MLT.CURRENCY)<1,1> = 'USD'
*Line [ 50 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*        CALL DBR ('ACCOUNT':@FM:AC.OPEN.ACTUAL.BAL,ACC.1,AMT.1)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACC.1,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
AMT.1=R.ITSS.ACCOUNT<AC.OPEN.ACTUAL.BAL>
        R.NEW(INF.MLT.AMOUNT.LCY)<1,1> = AMT.1 * RATE
        R.NEW(INF.MLT.AMOUNT.FCY)<1,1> = AMT.1
        R.NEW(INF.MLT.EXCHANGE.RATE)<1,1> = RATE
*----------------------------------------
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,2> = ACC.2
        R.NEW(INF.MLT.SIGN)<1,2> = 'DEBIT'
        R.NEW(INF.MLT.TXN.CODE)<1,2> = '78'
*R.NEW(INF.MLT.CURRENCY)<1,2> = 'EGP'
        R.NEW(INF.MLT.CURRENCY)<1,2> = 'USD'
*----------------------------------------
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,3> = ACC.2
        R.NEW(INF.MLT.SIGN)<1,3> = 'CREDIT'
        R.NEW(INF.MLT.TXN.CODE)<1,3> = '79'
*R.NEW(INF.MLT.CURRENCY)<1,3> = 'EGP'
        R.NEW(INF.MLT.CURRENCY)<1,3> = 'USD'
    END

    RETURN

END
