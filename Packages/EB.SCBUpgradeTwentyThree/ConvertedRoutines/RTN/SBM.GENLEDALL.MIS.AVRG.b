* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>39</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBM.GENLEDALL.MIS.AVRG
*    SUBROUTINE SBM.GENLEDALL.MIS.AVRG

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENLEDALL.MIS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENLEDALL.MIS.AVRG

*Line [ 45 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT=R.ITSS.DATES<EB.DAT.LAST.PERIOD.END>
*Line [ 52 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DAT.LW)
F.ITSS.DATES = 'F.DATES'
FN.F.ITSS.DATES = ''
CALL OPF(F.ITSS.DATES,FN.F.ITSS.DATES)
CALL F.READ(F.ITSS.DATES,'EG0010001',R.ITSS.DATES,FN.F.ITSS.DATES,ERROR.DATES)
DAT.LW=R.ITSS.DATES<EB.DAT.LAST.WORKING.DAY>

*============================= Check No. of Days ===============================
    STRT.DAT = DAT[1,6]:"01"
    END.DAT = DAT
    DAYS = "C"
    CALL CDD("",STRT.DAT,END.DAT,DAYS)
    AVRG.DAYS = DAYS + 1
    PRINT "AVRG.DAYS = ":AVRG.DAYS
*==============================================================================

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN
*------------------------------
INITIALISE:
*----------
*    FN.CONT      = 'F.RE.STAT.LINE.CONT'      ; F.CONT = ''
    FN.BAL       = 'F.RE.STAT.LINE.BAL'       ; F.BAL  = '' ; R.BAL = ''
*    FN.CONS      = 'F.CONSOLIDATE.PRFT.LOSS'  ; F.CONS = ''
*    FN.LN        = 'F.RE.STAT.REP.LINE'       ; F.LN   = ''
*    FN.COM       = 'F.COMPANY'  ; F.COM = ''  ; R.COM = ''
*    FN.BASE      = 'FBNK.RE.BASE.CCY.PARAM'   ; F.BASE = ''      ; R.BASE = ''
    FN.GEN.MIS   = 'F.SCB.GENLEDALL.MIS'      ; F.GEN.MIS = ''   ; R.GEN.MIS = ''
    FN.GEN.MIS.1 = 'F.SCB.GENLEDALL.MIS'      ; F.GEN.MIS.1 = '' ; R.GEN.MIS.1 = ''
    FN.GEN.MIS.AVRG = 'F.SCB.GENLEDALL.MIS.AVRG'      ; F.GEN.MIS.AVRG = '' ; R.GEN.MIS.AVRG = ''
*    CALL OPF(FN.CONT,F.CONT)
    CALL OPF(FN.BAL,F.BAL)
*    CALL OPF(FN.CONS,F.CONS)
*    CALL OPF(FN.LN,F.LN)
*    CALL OPF(FN.COM,F.COM)
*    CALL OPF(FN.BASE,F.BASE)
    CALL OPF(FN.GEN.MIS.AVRG,F.GEN.MIS.AVRG)
    CALL OPF(FN.GEN.MIS,F.GEN.MIS)
    CALL OPF(FN.GEN.MIS.1,F.GEN.MIS.1)

    T.SEL.LN = "" ; KEY.LIST.LN = "" ; SELECTED.LN = "" ;  ER.MSG.LN = ""
    NET.AMT     = 0

    AVRG.AMT.FCY = 0 ; TOT.AMT.FCY = 0
    AVRG.AMT.LCY = 0 ; TOT.AMT.LCY = 0

    RETURN
*----------------------------------------------------------------**
BUILD.RECORD:
*------------
*------------ CLEAR FILE --------------------------------------**
*Line [ 94 ] Comment DEBUG - ITSS - R21 Upgrade - 2021-12-26
*DEBUG
    OPEN FN.GEN.MIS.AVRG TO FILEVAR ELSE ABORT 201, FN.GEN.MIS.AVRG
    CLEARFILE FILEVAR

*---------------------------------------------------------------**
    T.SEL1 = "" ; KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
*----------------------------------------------------------------**
    T.SEL = "SELECT ":FN.GEN.MIS:" WITH BOOKING.DATE GE ":STRT.DAT:" AND BOOKING.DATE LE ":END.DAT
    T.SEL := " BY COMPANY.CODE BY LINE.NO BY CURRENCY BY BOOKING.DATE"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG2)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.GEN.MIS,KEY.LIST<I>,R.GEN.MIS,F.GEN.MIS,E1)
            CALL F.READ(FN.GEN.MIS.1,KEY.LIST<I+1>,R.GEN.MIS.1,F.GEN.MIS.1,E2)

            TOT.AMT.FCY += R.GEN.MIS<SGM.AMOUNT.FCY>
            TOT.AMT.LCY += R.GEN.MIS<SGM.AMOUNT.LCY>

*=================================================================
            CHK.ID = R.GEN.MIS<SGM.COMPANY.CODE>:R.GEN.MIS<SGM.LINE.NO>:R.GEN.MIS<SGM.CURRENCY>
            CHK.ID.1 = R.GEN.MIS.1<SGM.COMPANY.CODE>:R.GEN.MIS.1<SGM.LINE.NO>:R.GEN.MIS.1<SGM.CURRENCY>
            IF CHK.ID # CHK.ID.1 THEN
*=================================================================
                AVRG.AMT.FCY = TOT.AMT.FCY / AVRG.DAYS
                AVRG.AMT.LCY = TOT.AMT.LCY / AVRG.DAYS
                IF R.GEN.MIS<SGM.CURRENCY> = 'JPY' THEN
                    AVRG.AMT.FCY = DROUND(AVRG.AMT.FCY,0)
                END ELSE
                    AVRG.AMT.FCY = DROUND(AVRG.AMT.FCY,2)
                END
                AVRG.AMT.LCY = DROUND(AVRG.AMT.LCY,2)
*=================================================================
                GOSUB GET.MNTH.MVMT
                GOSUB CREATE.REC
            END
        NEXT I
    END
    RETURN
**----------------------------------------------------------------------**
CREATE.REC:
*----------
    R.GEN.MIS.AVRG<SGMV.LINE.NO>        = R.GEN.MIS<SGM.LINE.NO>
    R.GEN.MIS.AVRG<SGMV.MNTH.DATE>      = STRT.DAT[1,6]
    R.GEN.MIS.AVRG<SGMV.COMPANY.CODE>   = R.GEN.MIS<SGM.COMPANY.CODE>
    R.GEN.MIS.AVRG<SGMV.CURRENCY>       = R.GEN.MIS<SGM.CURRENCY>
    R.GEN.MIS.AVRG<SGMV.NO.OF.DAYS>     = AVRG.DAYS
    R.GEN.MIS.AVRG<SGMV.AMOUNT.FCY>     = AVRG.AMT.FCY
    R.GEN.MIS.AVRG<SGMV.AMOUNT.LCY>     = AVRG.AMT.LCY
    ID.BAL1 = FIELD(KEY.LIST<I>,"-",1):'-':FIELD(KEY.LIST<I>,"-",2):'-':R.GEN.MIS<SGM.CURRENCY>:'-':END.DAT:'*':FIELD(KEY.LIST<I>,"*",2)
    CALL F.WRITE(FN.GEN.MIS.AVRG,ID.BAL1,R.GEN.MIS.AVRG)
    CALL JOURNAL.UPDATE(ID.BAL1)
    TOT.AMT.FCY = 0
    TOT.AMT.LCY = 0
    AVRG.AMT.FCY = 0
    AVRG.AMT.LCY = 0

    RETURN
*---------------------------------------------------------------------**
GET.MNTH.MVMT:
    CALL F.READ(FN.BAL,KEY.LIST<I>,R.BAL,F.BAL,E1.BAL)
    R.GEN.MIS.AVRG<SGMV.CR.MVMT.MTH.LCL> = R.BAL<RE.SLB.CR.MVMT.MTH.LCL>
    R.GEN.MIS.AVRG<SGMV.DB.MVMT.MTH.LCL> = R.BAL<RE.SLB.DB.MVMT.MTH.LCL>
    R.GEN.MIS.AVRG<SGMV.NT.MVMT.MTH.LCL> = R.GEN.MIS.AVRG<SGMV.CR.MVMT.MTH.LCL> + R.GEN.MIS.AVRG<SGMV.DB.MVMT.MTH.LCL>
    RETURN
