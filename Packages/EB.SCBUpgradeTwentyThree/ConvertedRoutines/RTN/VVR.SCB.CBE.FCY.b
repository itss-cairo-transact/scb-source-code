* @ValidationCode : MjotMTExNDM3NDEyOTpDcDEyNTI6MTY0MjMzMzExNzY4Nzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Jan 2022 13:38:37
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE VVR.SCB.CBE.FCY

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_F.BILL.REGISTER
    $INSERT  I_F.FT.COMMISSION.TYPE
    $INSERT  I_F.CUSTOMER
    $INSERT  I_F.CURRENCY
    $INSERT           I_F.SCB.BR.CUS
    $INSERT           I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BR.SLIPS
*------------- 2011/ 11/ 15 -ADD AS VIR on VERSION  ---------------
    CUR      = R.NEW(EB.BILL.REG.CURRENCY)
*Line [ 34 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR,CUR1)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CUR1=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>
    TEXT = CUR:'*':CUR1 ;CALL REM
    IF PGM.VERSION NE ',SCB.CHQ.LCY.REG.0017' THEN
        IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'NO' ) THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>  = '99433300':CUR1:'507099'
        END ELSE
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>  = '99433300':CUR1:'5075':ID.COMPANY[8,2]
        END
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> EQ '' THEN
*Line [ 43 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            E='ACCOUNT CBE IS '' ';CALL ERR;MESSAGE='REPEAT'
        END
    END
*------------------------------------------------------------------
RETURN
END
