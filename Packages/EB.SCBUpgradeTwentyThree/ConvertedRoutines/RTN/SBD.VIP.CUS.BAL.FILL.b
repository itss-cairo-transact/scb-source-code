* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBD.VIP.CUS.BAL.FILL

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VIP.CUS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VIP.CUS.BAL
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW

*---------------------------------------
    FN.VIP = "F.SCB.VIP.CUS.BAL"  ; F.VIP  = ""
    CALL OPF (FN.VIP,F.VIP)

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CA = 'F.SCB.VIP.CUS' ; F.CP = ''
    CALL OPF(FN.CA,F.CA)

    FN.CP = 'F.SCB.CUS.POS.LW' ; F.CP = ''
    CALL OPF(FN.CP,F.CP)

    TD = TODAY
    CALL CDT("",TD,'-1W')

    DAT = TD[1,6]

    WS.CATEG.LINE = ''
    WS.AMT  = 0
    VIP.BAL = 0
*----------------------------------------
**** SELECT VIP CATEGORY ****

    T.SEL1 = "SELECT ":FN.CA:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.CA,KEY.LIST1<K>,R.CA,F.CA,E3)
*Line [ 64 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.CATEG.COUNT = DCOUNT(R.CA<VIP.CATEGORY>,@VM)
            FOR X = 1 TO WS.CATEG.COUNT
                WS.CATEG1      = R.CA<VIP.CATEGORY><1,X>
                WS.CATEG.LINE := WS.CATEG1:' '
            NEXT X
        NEXT K
    END

    WS.CATEG = '(':WS.CATEG.LINE:')'

****** SELECT VIP CUSTOMER ******

    T.SEL = "SELECT ":FN.CP:" WITH CATEGORY IN ":WS.CATEG:" BY CUSTOMER"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CP,KEY.LIST<I>,R.CP,F.CP,E1)
            CALL F.READ(FN.CP,KEY.LIST<I+1>,R.CP1,F.CP,E2)

            WS.CUST.ID  = R.CP<CUPOS.CUSTOMER>
            WS.CUST.ID1 = R.CP1<CUPOS.CUSTOMER>

            CALL F.READ(FN.CU,WS.CUST.ID,R.CU,F.CU,E5)
            WS.TARGET = R.CU<EB.CUS.TARGET>

            IF WS.TARGET EQ 5850 OR WS.TARGET EQ 5860 OR WS.TARGET EQ 5870 THEN
                WS.AMT += R.CP<CUPOS.LCY.AMOUNT>

                IF WS.CUST.ID NE WS.CUST.ID1 THEN
                    WS.AMT     = DROUND(WS.AMT,'0')
                    VIP.BAL.ID = WS.CUST.ID:'.':DAT

                    CALL F.READ(FN.VIP,VIP.BAL.ID,R.VIP,F.VIP,E6)

                    IF NOT(E6) THEN
                        VIP.AMT = R.VIP<VIPBL.AMOUNT>
                        IF WS.AMT LT VIP.AMT THEN
                            R.VIP<VIPBL.AMOUNT>   = WS.AMT
                            R.VIP<VIPBL.BAL.DATE> = DAT
                            R.VIP<VIPBL.CUSTOMER> = WS.CUST.ID
                            R.VIP<VIPBL.TARGET>   = WS.TARGET

                            CALL F.WRITE(FN.VIP,VIP.BAL.ID,R.VIP)
                            CALL JOURNAL.UPDATE(VIP.BAL.ID)
                            WS.AMT = 0
                        END
                    END ELSE
                        R.VIP<VIPBL.AMOUNT>   = WS.AMT
                        R.VIP<VIPBL.BAL.DATE> = DAT
                        R.VIP<VIPBL.CUSTOMER> = WS.CUST.ID
                        R.VIP<VIPBL.TARGET>   = WS.TARGET

                        CALL F.WRITE(FN.VIP,VIP.BAL.ID,R.VIP)
                        CALL JOURNAL.UPDATE(VIP.BAL.ID)
                        WS.AMT = 0
                    END
                END
            END
        NEXT I
    END
***************************************************
    RETURN
END
