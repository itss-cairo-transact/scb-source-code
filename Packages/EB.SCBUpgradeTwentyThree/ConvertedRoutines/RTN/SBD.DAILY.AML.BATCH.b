* @ValidationCode : MjotMTQ4NzU4MjA4NzpDcDEyNTI6MTY0MjMzMDM3Njk1Mzp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Jan 2022 12:52:56
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*** CREATED BY MAHMOUD MAGDY 2020 / 12 / 13

PROGRAM SBD.DAILY.AML.BATCH
*Line [19,20] Remove T24_BP
    $INSERT   I_COMMON
    $INSERT   I_EQUATE
**************************************
    EXECUTE "OFSADMINLOGIN"
**************************************
** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'
***------------------------------------------------------------------------------------****
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"GET.PROG.FILE")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"GET.PROG.FILE.2")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"GET.PROG.FILE.SAN")
***------------------------------------------------------------------------------------****
RETURN
***------------------------------------------------------------------------------------****
END
