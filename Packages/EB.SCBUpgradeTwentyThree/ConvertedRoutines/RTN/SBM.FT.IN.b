* @ValidationCode : MjoxOTUzNjI1ODAwOkNwMTI1MjoxNjQyMzMwOTA0Mzc4OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 16 Jan 2022 13:01:44
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* CREATED BY REHAM YOUSSEF 2021/11/10
*-----------------------------------------------------------------------------
*    PROGRAM SBM.FT.IN
SUBROUTINE  SBM.FT.IN

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.COMPANY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_CU.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.FT.INT
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB MATT
RETURN
**************************************
INITIATE:
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    PRINT  "ST.DATE=":ST.DATE

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    PRINT "EN.DATE=":EN.DATE


    YTEXT = "���� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    AMT = COMI
    PRINT "AMOUNT=":AMT

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.FT   = 'FBNK.FUNDS.TRANSFER$HIS'    ; F.FT  = '' ; R.FT  = ''
*    FN.FT   = 'FBNK.FUNDS.TRANSFER'    ; F.FT  = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    KEY.LIST= ""   ; SELECTED  = "" ; ER.MSG = ""
    KEY.LIST2= ""  ; SELECTED2 = "" ; ER.MSG2 = ""
    DEB.AMT = ""    ; SERIAL1 = 0 ; SERIAL2 = 0
    SERIAL3 = 0 ;SERIAL4=0
    OPENSEQ "&SAVEDLISTS&" , "SBM.FT.IN.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBM.FT.IN.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBM.FT.IN.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBM.FT.IN.TXT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBM.FT.IN.TXT File IN &SAVEDLISTS&'
        END
    END

    HEAD.DESC  = "ID.NO.":"|"
    HEAD.DESC := "Branch.":"|"
    HEAD.DESC := "Cust Name.":"|"
    HEAD.DESC := "Cust.Acct":"|"
    HEAD.DESC := "Purpose for Transfer.":"|"
    HEAD.DESC := "Currency.":"|"
    HEAD.DESC := "Amount.":"|"
    HEAD.DESC := "Amt.Local":"|"
    HEAD.DESC := "Processing date .":"|"
    HEAD.DESC := "Transaction.type .":"|"
    HEAD.DESC := "BEN.CUST.":"|"
    HEAD.DESC := "NOTE.DEBIT.":"|"
    HEAD.DESC := "NOTE.CREDIT."


    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


RETURN
*************************DEBIT *******************************
MATT:
****
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH (TRANSACTION.TYPE LIKE IT...) AND (DEBIT.VALUE.DATE GE ":ST.DATE :" AND DEBIT.VALUE.DATE LE ":EN.DATE :") AND LOC.AMT.DEBITED GE ":AMT:" AND CREDIT.ACCT.NO UNLIKE 994... BY TRANSACTION.TYPE BY CREDIT.CURRENCY"
*    T.SEL = "GET-LIST FT.IN"


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT "SEL=":SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,ETEXT)
            ID.NO     = KEY.LIST<I>
            DR.CUS    = R.FT<FT.CREDIT.CUSTOMER>
            CALL F.READ(FN.CUS,DR.CUS,R.CUS,F.CUS,ETEXT)
            LOCAL.REF  = R.CUS<EB.CUS.LOCAL.REF>
            NAME       = LOCAL.REF<1,CULR.ARABIC.NAME>
            SEC        = R.CUS<EB.CUS.SECTOR>
            IF SEC EQ '1100' OR SEC EQ '1300' THEN
                CO.CODE    = R.FT<FT.CO.CODE>
*Line [ 131 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH.NAME)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,CO.CODE,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH.NAME=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>
*    BEN.CUS    = R.FT<FT.BEN.CUSTOMER>
*    PURPOSE    = R.FT<FT.PAYMENT.DETAILS>[1,35]
                BEN.CUS    = R.FT<FT.LOCAL.REF><1,FTLR.SEND.INSTIT>
                PURPOSE    = R.FT<FT.LOCAL.REF><1,FTLR.FOR.BK.CHR.COM,1>
                NOTE.DEBIT = R.FT<FT.LOCAL.REF><1,FTLR.NOTE.DEBITED>
                NOTE.CREDIT= R.FT<FT.LOCAL.REF><1,FTLR.NOTE.CREDIT>
                DR.ACCT    = R.FT<FT.CREDIT.ACCT.NO>
                DR.CUR     = R.FT<FT.CREDIT.CURRENCY>
                DR.AMT     = R.FT<FT.DEBIT.AMOUNT>
                DR.LOC.AMT = R.FT<FT.LOC.AMT.CREDITED>
                DR.DATE    = R.FT<FT.PROCESSING.DATE>
                TRN.TYPE   = R.FT<FT.TRANSACTION.TYPE>
*DEBUG
*Line [ 151 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('FT.TXN.TYPE.CONDITION':@FM:FT6.DESCRIPTION,TRN.TYPE,TRN.NAME)
F.ITSS.FT.TXN.TYPE.CONDITION = 'F.FT.TXN.TYPE.CONDITION'
FN.F.ITSS.FT.TXN.TYPE.CONDITION = ''
CALL OPF(F.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION)
CALL F.READ(F.ITSS.FT.TXN.TYPE.CONDITION,TRN.TYPE,R.ITSS.FT.TXN.TYPE.CONDITION,FN.F.ITSS.FT.TXN.TYPE.CONDITION,ERROR.FT.TXN.TYPE.CONDITION)
TRN.NAME=R.ITSS.FT.TXN.TYPE.CONDITION<FT6.DESCRIPTION>

                BB.DATA  = ID.NO:'|'
                BB.DATA := BRANCH.NAME:'|'
                BB.DATA := NAME:'|'
                BB.DATA := DR.ACCT:'|'
                BB.DATA := PURPOSE:'|'
                BB.DATA := DR.CUR:'|'
                BB.DATA := DR.AMT:'|'
                BB.DATA := DR.LOC.AMT:'|'
                BB.DATA := DR.DATE:'|'
                BB.DATA := TRN.NAME:'|'
                BB.DATA := BEN.CUS :'|'
                BB.DATA := NOTE.DEBIT :'|'
                BB.DATA := NOTE.CREDIT
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I
    END
RETURN
***************************************************************************
END
