* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    SUBROUTINE MKSA.VNC.DEFAULT.ACC.AMT.NEW.FCY
*    PROGRAM    VNC.DEFAULT.ACC.AMT.NEW

*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.INF.MULTI.TXN
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.FCY.REF.CBE

*ACC.MAIN = '9943330010508499'
    ACC.MAIN = '9943330020507499'
    KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
    FN.ACC = 'F.SCB.BR.FCY.REF.CBE' ; F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)
    FN.CUR = 'FBNK.CURRENCY' ; F.CUR = ''
    CALL OPF(FN.CUR,F.CUR)
    CALL F.READ(FN.CUR,'USD',R.CUR,F.CUR,ERR1)
    RATE   = R.CUR<EB.CUR.MID.REVAL.RATE><1,1>
    TOD    = R.DATES(EB.DAT.TODAY)
    TEXT = "DATE=":TOD ; CALL REM
*T.SEL = "SELECT F.SCB.BR.FCY.REF.CBE WITH ACP.REJ EQ 220 AND T.DATE EQ ": TODAY:" BY BRN.NO"
    T.SEL = "SELECT F.SCB.BR.FCY.REF.CBE WITH ACP.REJ EQ 220 AND T.DATE EQ ": TOD :" BY BRN.NO"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    TEXT = "DATE=":TOD ; CALL REM
    TEXT = "SELo : " : SELECTED ; CALL REM
    TOT.AMT = 0
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.ACC,KEY.LIST<I>,R.ACC,F.ACC,E1)
            CALL F.READ(FN.ACC,KEY.LIST<I+1>,R.ACC.1,F.ACC,E1)
            IF R.ACC<LCY.REF.BRN.NO> EQ R.ACC.1<LCY.REF.BRN.NO> THEN
                AMT       = R.ACC<LCY.REF.AMT>
                TOT.AMT = TOT.AMT + AMT
            END
            ELSE
                TOT.AMT = TOT.AMT +  R.ACC<LCY.REF.AMT>
                BRN       = R.ACC<LCY.REF.BRN.NO>
                TEXT = BRN :"*":  TOT.AMT ; CALL REM
                GOSUB ADD.BRAN
                TOT.AMT = 0
            END
        NEXT I
        GOSUB LAST.ACC
        RETURN
ADD.BRAN:
        XX = LEN(BRN)
        IF XX LT 2 THEN
            BRN = "0": BRN
        END
*ACC.NO = "99433300105085" : BRN
        ACC.NO = "99433300205075" : BRN
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        CUR.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
        NEXT.M = CUR.M + 1
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,NEXT.M> = ACC.NO
        R.NEW(INF.MLT.SIGN)<1,NEXT.M> = 'CREDIT'
        R.NEW(INF.MLT.TXN.CODE)<1,NEXT.M> = '79'
*R.NEW(INF.MLT.CURRENCY)<1,NEXT.M> = 'EGP'
*R.NEW(INF.MLT.AMOUNT.LCY)<1,NEXT.M> = TOT.AMT
        R.NEW(INF.MLT.CURRENCY)<1,NEXT.M> = 'USD'
        R.NEW(INF.MLT.AMOUNT.LCY)<1,NEXT.M> = TOT.AMT * RATE
        R.NEW(INF.MLT.AMOUNT.FCY)<1,NEXT.M> = TOT.AMT
        R.NEW(INF.MLT.EXCHANGE.RATE)<1,NEXT.M> = RATE

        RETURN
***********************************************************
LAST.ACC:
*Line [ 96 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        LAST.M = DCOUNT(R.NEW(INF.MLT.ACCOUNT.NUMBER),@VM)
        FOR X = 1 TO LAST.M
*TOTAL.AMT = TOTAL.AMT + R.NEW(INF.MLT.AMOUNT.LCY)<1,X>
            TOTAL.AMT = TOTAL.AMT + R.NEW(INF.MLT.AMOUNT.FCY)<1,X>
        NEXT X
****************************************************************

        LAST.M.NEW = LAST.M + 1
        R.NEW(INF.MLT.ACCOUNT.NUMBER)<1,LAST.M.NEW> = ACC.MAIN
        R.NEW(INF.MLT.SIGN)<1,LAST.M.NEW> = 'DEBIT'
        R.NEW(INF.MLT.TXN.CODE)<1,LAST.M.NEW> = '78'
*R.NEW(INF.MLT.CURRENCY)<1,LAST.M.NEW> = 'EGP'
*R.NEW(INF.MLT.AMOUNT.LCY)<1,LAST.M.NEW> = TOTAL.AMT
        R.NEW(INF.MLT.CURRENCY)<1,LAST.M.NEW> = 'USD'
        R.NEW(INF.MLT.AMOUNT.LCY)<1,LAST.M.NEW> = TOTAL.AMT * RATE
        R.NEW(INF.MLT.AMOUNT.FCY)<1,LAST.M.NEW> = TOTAL.AMT
        R.NEW(INF.MLT.EXCHANGE.RATE)<1,LAST.M.NEW> = RATE
        RETURN
    END
