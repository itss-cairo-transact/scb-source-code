* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* CREATED BY REHAM.YOUSSEF 2021/09/21
*-----------------------------------------------------------------------------
*    PROGRAM SBD.CUS.FT.INT.PRIV
    SUBROUTINE SBM.FT.INTERNAL

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.INT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY

    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB HIS
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
**************************************
INITIATE:
    REPORT.ID ='SBM.FT.INTERNAL'
    CALL PRINTER.ON(REPORT.ID,'')
    COMP = ID.COMPANY

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
*    PRINT  "ST.DATE=":ST.DATE

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
 *   PRINT "EN.DATE=":EN.DATE
*   OPENSEQ "&SAVEDLISTS&" , "FT.INTERNAL.CSV" TO BB THEN
*       CLOSESEQ BB
*       HUSH ON
*       EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"FT.INTERNAL.CSV"
*       HUSH OFF
*   END
*   OPENSEQ "&SAVEDLISTS&" , "FT.INTERNAL.CSV" TO BB ELSE
*       CREATE BB THEN
*           PRINT 'FILE FT.INTERNAL CREATED IN &SAVEDLISTS&'
*       END ELSE
*           STOP 'Cannot create FT.INTERNAL File IN &SAVEDLISTS&'
*       END
*   END



    FN.FT   = 'FBNK.FUNDS.TRANSFER$HIS'    ; F.FT  = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    KEY.LIST = ""   ; SELECTED  = "" ; ER.MSG = ""
    KEY.LIST2= ""   ; SELECTED2 = "" ; ER.MSG2 = ""
    DR.AMT1  = ""    ; SERIAL1 = 0 ; SERIAL2 = 0
    DR.AMT2  = ""     ; DR.AMT2 = ''
    RETURN
******************************************************************
PRINT.HEAD:


*Line [ 91 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,COMP,BRANCH)
F.ITSS.COMPANY = 'F.COMPANY'
FN.F.ITSS.COMPANY = ''
CALL OPF(F.ITSS.COMPANY,FN.F.ITSS.COMPANY)
CALL F.READ(F.ITSS.COMPANY,COMP,R.ITSS.COMPANY,FN.F.ITSS.COMPANY,ERROR.COMPANY)
BRANCH=R.ITSS.COMPANY<EB.COM.COMPANY.NAME>

    TD = TODAY
    CALL CDT("",TD,'-1W')
    T.DAY1  = TD[7,2]:'/':TD[5,2]:"/":TD[1,4]

    YYBRN  = BRANCH
    DATY   = TODAY
    T.DAY  = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD  ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(90):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(85):"��� ������ : ":"'P'"
    PR.HD :="'L'":SPACE(1):REPORT.ID
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(50):" ��������� �������� ������� :  ":T.DAY1
    PR.HD :="'L'":SPACE(48):STR('_',40)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(35):"�������� �������":SPACE(30):"�������� �������"
    PR.HD :="'L'":STR('_',120)
    PR.HD :="'L'":"�����" :SPACE(35):"������":SPACE(40):"�����":SPACE(35):"������"
    PR.HD :="'L'":STR('_',120)
    PRINT
    HEADING PR.HD
    RETURN
*    HEAD.DESC    = "WITHOUT REVERSAL TRANSACTION":","
*    HEAD.DESC   := "TOTAL AMOUNT":","
*    HEAD.DESC   := "TOTAL TRANSACTION":","
*    HEAD.DESC   := "REVERSAL TRANSACTION":","
*    HEAD.DESC   := "TOTAL AMOUNT":","
*    HEAD.DESC   := "TOTAL TRANSACTION":","

*    BB.DATA = HEAD.DESC
*    WRITESEQ BB.DATA TO BB ELSE
*        PRINT " ERROR WRITE FILE "
*    END
*    RETURN
**************************HIS ONLY *******************************
HIS:
*Line [ 135 ] Comment DEBUG - ITSS - R21 Upgrade - 2022-02-09
 **DEBUG
    T.SEL  = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH DEBIT.CUSTOMER NE '' AND CREDIT.CUSTOMER NE '' AND (DEBIT.CUSTOMER NE CREDIT.CUSTOMER) AND TRANSACTION.TYPE UNLIKE O... AND TRANSACTION.TYPE UNLIKE I... AND CREDIT.CURRENCY EQ 'EGP' AND DEBIT.CURRENCY EQ 'EGP' AND (PROCESSING.DATE GE ":ST.DATE:" AND PROCESSING.DATE LE ":EN.DATE:") AND CREDIT.ACCT.NO UNLIKE 994... AND DEBIT.ACCT.NO UNLIKE 994... AND (INPUTTER UNLIKE ...EIB... OR INPUTTER UNLIKE ...FAWRY...)"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*    PRINT "������ ��������=":SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT1,F.FT,ETEXT)
            STAT = R.FT1<FT.RECORD.STATUS>
            IF STAT NE 'REVE' THEN
                SERIAL1 = SERIAL1 + 1
                DR.AMT1 += R.FT1<FT.LOC.AMT.DEBITED>
            END ELSE
                SERIAL2 = SERIAL2 + 1
                DR.AMT2 += R.FT1<FT.LOC.AMT.DEBITED>
            END
        NEXT I
    END

    XX  = SPACE(120)

    XX<1,1>[1,35]   = DR.AMT2
    XX<1,1>[45,15]  = SERIAL2
    XX<1,1>[75,15]  = DR.AMT1
    XX<1,1>[105,15] = SERIAL1

    PRINT XX<1,1>
    RETURN
* HEAD.DESC    = "":","
* HEAD.DESC   := DR.AMT1:","
* HEAD.DESC   := SERIAL1:","
* HEAD.DESC   := "":","
* HEAD.DESC   := DR.AMT2:","
* HEAD.DESC   := SERIAL2:","

* BB.DATA = HEAD.DESC
* WRITESEQ BB.DATA TO BB ELSE
*     PRINT " ERROR WRITE FILE "
* END

**********************************************************************
END
