* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    SUBROUTINE VIR.GET.INDEX.FCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.FCY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCCB.BT.PARTIAL.FCY
*----------------------------------------------
    FN.TMP = "F.SCCB.BT.PARTIAL.FCY" ; F.TMP = ""
    CALL OPF(FN.TMP, F.TMP)

    TMP.ID = ID.COMPANY:".":TODAY
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,READ.ER)

    R.TMP<SCCB.BT.NEXT.BT.INDEX> = R.NEW(SCB.BT.CBE.REF)
*WRITE  R.TMP TO F.TMP , TMP.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":TMP.ID:"TO" :FN.TMP
*END
    CALL F.WRITE (FN.TMP,TMP.ID,R.TMP)
*------------------------------------------------
END
