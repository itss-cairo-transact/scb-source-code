* @ValidationCode : MjotNTA3MTQ0NDU0OkNwMTI1MjoxNjQyMzMyMTQyNTU0OnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 16 Jan 2022 13:22:22
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
SUBROUTINE VAR.WRT.FLG.FCY

    $INSERT  I_COMMON
    $INSERT  I_EQUATE
    $INSERT  I_F.BILL.REGISTER
    $INSERT  I_USER.ENV.COMMON
    $INSERT  I_F.ACCOUNT
    $INSERT  I_F.COMPANY
    $INSERT  I_F.CURRENCY
    $INSERT        I_F.SCB.BT.BATCH.FCY
    $INSERT        I_BR.LOCAL.REFS
    $INSERT        I_F.SCCB.BT.PARTIAL.FCY
*----------------------------------------------
    IF V$FUNCTION = 'A' AND  R.NEW(SCB.BT.RECORD.STATUS)='INAU' THEN
        FN.TMP = "F.SCCB.BT.PARTIAL.FCY" ; F.TMP = ""
        CALL OPF(FN.TMP , F.TMP)
        TMP.ID = ID.COMPANY:".":TODAY
        CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ER.TMP)
        R.TMP<SCCB.BT.FLG.AUTH> = "YES"

*WRITE  R.TMP TO F.TMP , TMP.ID ON ERROR
*   PRINT "CAN NOT WRITE RECORD":TMP.ID:"TO" :FN.TMP
*END
        CALL F.WRITE (FN.TMP,TMP.ID,R.TMP)
    END
*----------------------------------------------
RETURN
END
