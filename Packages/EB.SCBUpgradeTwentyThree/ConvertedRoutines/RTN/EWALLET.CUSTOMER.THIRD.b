* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*****MAI 24/06/2021***************
*-----------------------------------------------------------------------------
* <Rating>240</Rating>
*-----------------------------------------------------------------------------
   SUBROUTINE EWALLET.CUSTOMER.THIRD
**    PROGRAM EWALLET.CUSTOMER.THIRD

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.TITLE
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.GOVERNORATE
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.REGION
*   $INCLUDE TEMENOS.BP I_F.COMP.LOCAL.REFS

    TEXT = 'HELLO' ; CALL REM

    FN.CUSTOMER = 'FBNK.CUSTOMER' ; F.CUSTOMER = '' ; R.CUSTOMER = '' ; RETRY = '' ; ERR.CUST = ''
    CALL OPF(FN.CUSTOMER,F.CUSTOMER)

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''  ; R.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

**********OPEN FILE********************************
    OPENSEQ "&SAVEDLISTS&" , "EWALLET.CUST.THIRD.txt" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"EWALLET.CUST.THIRD.txt"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "EWALLET.CUST.THIRD.txt" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE EWALLET.CUST.THIRD.txt CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create EWALLET.CUST.THIRD.txt File IN &SAVEDLISTS&'
        END
    END
*****************************************************************
**EDITED 08 JAN 2021
    BB.DATA = "msisdn":",":"organization":",":"firstName":",":"middleName":",":"lastName":",":"address":",":"emailAddress":",":"officialId":",":"officialIdType":",":"nationality":",":"monthlySalary":",":"birthDateString":",":"birthPlaceCity":",":"birthPlaceCountry":",":"kycStatus":",":"archivingCode":",":"accountServiceCode" :",":"cardNumber":",":"cardAlias":",":"expiryStr":",":"martialStatus":",":"memo":",":"otherNationality" :",":"contactOperator":",":"companyName":",":"companyStreet":",":"companyArea":",":"companyGovernorate":",":"jobTitle":",":"gender":",":"bankCustomerId":",":"hasBankAcct":",":"extraParam":",":"accountType":",":"subType":",":"CategoryCode":",":"bankBranch"

    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END
    YTEXT = "���� ������� �������:"
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI

    YTEXT = "���� ����� �������:"
    CALL TXTINP(YTEXT, 8, 22, "2", "A")
    ST.TIME = COMI

    SEL.DATE = ST.DATE[3,6]
    IF LEN(ST.TIME) < 2 THEN
        STIME = '0':ST.TIME
    END ELSE
        STIME = ST.TIME
    END
    FMT.TIME = STIME:'00'
    DATETIME = SEL.DATE:FMT.TIME
    TEXT = '����� � ����� ��=' :DATETIME ; CALL REM

**    S.SEL = "SELECT FBNK.CUSTOMER WITH IS.EWALLET EQ 'YES' AND SECTOR EQ '5010' AND WALLET.REQ.DATE GE ":ST.DATE:" AND DATE.TIME GE ":DATETIME:" BY WALLET.REQ.DATE BY DATE.TIME"
    S.SEL = "SELECT FBNK.CUSTOMER WITH IS.EWALLET EQ 'YES' AND SECTOR EQ '5010' AND WALLET.REQ.DATE GE ":ST.DATE:" AND DATE.TIME GE ":DATETIME:" BY WALLET.REQ.DATE BY DATE.TIME"
    KEY.LIST.SS = '' ; SELECTED.SS = '' ; ER.MSG.SS = ''
    CALL EB.READLIST(S.SEL , KEY.LIST.SS,'',SELECTED.SS,ER.MSG.SS)

*PRINT 'SELECTED':SELECTED.SS
TEXT = 'SELECTED.SS=':SELECTED.SS ; CALL REM
    IF SELECTED.SS THEN
        FOR I = 1 TO SELECTED.SS
            AR.NAME = '' ; CU.GENDER = '' ; CUS.COMP = '' ; COMP.NAME = '' ; CU.BIRTH.D = '' ; CU.CONT.D = '' ; CUS.AGE = ''
            MOB.NO = '' ; FIRST.NAME = '' ;  MIDDLE.NAME = '' ;LAST.NAME = '' ; CU.ADDRESS = ''; CU.EMAIL = '' ; CU.NSN = '' ; CU.BIRTH.D = '' ; CU.COUNTRY = '' ;CU.ACCOUNT = '' ; CATEG1 = '' ; AC.DESC = '' ; CU.STREET = ''
            YYYY = '' ; MM= '' ; DD= '' ; BIRTH.D.F = ''
            CALL F.READ(FN.CUSTOMER,KEY.LIST.SS<I>, R.CUSTOMER, F.CUSTOMER ,E3)
            LOCAL.REF.C = R.CUSTOMER<EB.CUS.LOCAL.REF>
            AR.NAME    = LOCAL.REF.C<1,CULR.ARABIC.NAME>
            CU.GENDER  = LOCAL.REF.C<1,CULR.GENDER>
****
            MOB.NO<I>    = R.CUSTOMER<EB.CUS.SMS.1>
            FIRST.NAME<I>  = LOCAL.REF.C<1,CULR.FNAME>
            MIDDLE.NAME<I> = LOCAL.REF.C<1,CULR.MNAME>
            LAST.NAME<I>   = LOCAL.REF.C<1,CULR.LNAME>
**     CU.ADDRESS<I>  = LOCAL.REF.C<1,CULR.ARABIC.ADDRESS>
**     CU.STREET<I>   = R.CUSTOMER<EB.CUS.STREET>
*--------------EDITED BY MAI 14 FEB
**STREET = R.CUSTOMER<EB.CUS.STREET>
            STREET = R.CUSTOMER<EB.CUS.STREET><1,1>
            CU.STREET<I>   =  TRIM(STREET, ',', 'A')
*  CU.EMAIL<I>    = LOCAL.REF.C<1,CULR.EMAIL.ADDRESS>
*****UPDATED BY NESSREEN AHMED 22/3/2020***************************
*****       CU.EMAIL<I>    = R.CUSTOMER<EB.CUS.EMAIL.1>
            CU.EMAIL<I>    = ''
*****END OF UPDATE 22/3/2020***************************************
            CU.NSN<I>      = LOCAL.REF.C<1,CULR.NSN.NO>
            CU.BIRTH.D<I>  = R.CUSTOMER<EB.CUS.BIRTH.INCORP.DATE>
            YYYY = CU.BIRTH.D<I>[1,4]
            MM =  CU.BIRTH.D<I>[5,2]
            DD =  CU.BIRTH.D<I>[7,2]
            BIRTH.D.F = YYYY:'-':MM:'-':DD
****UPDATE BY MAI 31/3/2020
            CU.COUNTRY<I>  = R.CUSTOMER<EB.CUS.TOWN.COUNTRY><1,1>
            CU.COUNTRY<I> =  TRIM(CU.COUNTRY<I>, ',', 'A')
****eND OF UPDATE
* CU.ACCOUNT<I>  = LOCAL.REF.C<1,CULR.EWALLET.ACC>
* CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,CU.ACCOUNT<I> , CATEG1)
* CALL F.READ(FN.CAT,CATEG1,R.CAT,F.CAT,ERR)
* EN.AC.DESC<I> = R.CAT<EB.CAT.DESCRIPTION><1,1>
            CU.ACCOUNT<I> ='9999999999999999'
            CUS.COMP   = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
            CU.BRD.YEAR = CU.BIRTH.D[1,4]
            TODAY.D =  TODAY
            TODAY.YEAR = TODAY.D[1,4]
            CUS.AGE = TODAY.YEAR - CU.BRD.YEAR
            CU.CONT.D  = R.CUSTOMER<EB.CUS.CONTACT.DATE>
***ADDED 08 JAN 2021
            CU.BRANCH  = R.CUSTOMER<EB.CUS.COMPANY.BOOK>
****
*------------------EDITED BY MAI 17 FEB
*           EXPIRY.DT = '1909'
            EXPIRY.DT = ''

            BB.DATA = MOB.NO<I>:",":"SCB-ORG":",":FIRST.NAME<I>:",":MIDDLE.NAME<I>:",":LAST.NAME<I>:",":CU.STREET<I> :",":CU.EMAIL<I>:",":CU.NSN<I>:",":"NAT":",":"EGY":",":"500":",":BIRTH.D.F:",":CU.COUNTRY<I>:",":"EGY":",":"KYCED":",":"123455":",":"SCB" :",":CU.ACCOUNT<I> :",": "Bank Account" :",":EXPIRY.DT:",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"":",":"ACA":",":"ACA":",":"NORM":",":CU.BRANCH
****UPDATE BY MAI 06 APRIL 2020
*DEBUG
            IF FIRST.NAME<I> NE '' AND MIDDLE.NAME<I> NE '' AND LAST.NAME<I> NE '' THEN
******END OF UPDATE
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I
    END   ;***End of main selection from Customer***
    TEXT = '�� �������� �� �������� ' ;CALL REM
    RETURN
END
