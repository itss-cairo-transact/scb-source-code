* @ValidationCode : MjotMTg2MjU5ODc3NTpDcDEyNTI6MTY0MjMyOTYyNTM4Njp1c2VyOi0xOi0xOjA6MDpmYWxzZTpOL0E6UjIxX0FNUi4wOi0xOi0x
* @ValidationInfo : Timestamp         : 16 Jan 2022 12:40:25
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
PROGRAM CORRECT.BILL.1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.SCB.BR.PAY.PLACE
    $INSERT   I_BR.LOCAL.REFS
*-----------------------------------------------
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BRRR = '' ; ETEXT = ''
    CALL OPF( FN.BR,F.BR)

    T.SEL  = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ 20210503"
    T.SEL := " AND CURRENCY EQ EGP AND ( BILL.CHQ.STA NE 7 AND BILL.CHQ.STA NE 8 )"
    T.SEL := " AND PAY.PLACE NE '' AND COLL.DATE NE '' "
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ER.MSG)
    CRT "SELECTED = " : SELECTED

    FOR I =1 TO SELECTED
        CALL F.READ(FN.BR,KEY.LIST<I>,R.BRRR,F.BR,ETEXT)

**-----------------------------------------
        R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT> = '20210504'
        R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>    = '20210505'
**-----------------------------------------
        CALL F.WRITE(FN.BR,KEY.LIST<I>,R.BRRR)
        CALL JOURNAL.UPDATE(KEY.LIST<I>)

    NEXT I

RETURN
END
