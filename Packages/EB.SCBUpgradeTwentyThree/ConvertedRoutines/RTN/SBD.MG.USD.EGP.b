* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBD.MG.USD.EGP

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*---------------------------------------
    GOSUB INITIALISE
    GOSUB BUILD.RECORD
    RETURN

INITIALISE:
    OPENSEQ "MECH" , "MONEYGRAM.USD.TO.EGP" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"MECH":' ':"MONEYGRAM.USD.TO.EGP"
        HUSH OFF
    END
    OPENSEQ "MECH" , "MONEYGRAM.USD.TO.EGP" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE MONEYGRAM.USD.TO.EGP CREATED IN MECH'
        END ELSE
            STOP 'Cannot create MONEYGRAM.USD.TO.EGP File IN MECH'
        END
    END

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    DB.ACCT = '9940064920200101'
    CR.ACCT = '9940064910200101'

    V.DATE = TODAY
    COMMA  = ','

    RETURN
*----------------------------------------------------
BUILD.RECORD:

    CALL F.READ(FN.AC,DB.ACCT,R.AC,F.AC,E1)
    DB.AMT = R.AC<AC.ONLINE.ACTUAL.BAL> - 50

    IF DB.AMT GE 1 THEN
        DB.CCY = 'USD'
        CR.CCY = 'EGP'

        IDD = 'FUNDS.TRANSFER,MGFX,AUTO.CHRGE//EG0010013,'

        OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC80":COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":DB.CCY:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CR.CCY:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DB.ACCT:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
        OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
        OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
        OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"MONEYGRAM.FX":COMMA
        OFS.MESSAGE.DATA :=  "CREDIT.THEIR.REF=":"MONEYGRAM.FX":COMMA
        OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
        OFS.MESSAGE.DATA :=  "COMMISSION.CODE=":"WAIVE":COMMA
        OFS.MESSAGE.DATA :=  "CHARGE.CODE=":"WAIVE":COMMA
        OFS.MESSAGE.DATA :=  "LOCAL.REF:4:1=":"15 - OTHER":COMMA

        MSG.DATA = IDD:",":OFS.MESSAGE.DATA

        WRITESEQ MSG.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END

*** COPY TO OFS ***

        EXECUTE 'COPY FROM MECH TO OFS.IN MONEYGRAM.USD.TO.EGP'
        EXECUTE 'DELETE ':"MECH":' ':"MONEYGRAM.USD.TO.EGP"

    END ELSE
        PRINT 'PROGRAM NOT RUN - NO BALANCE'
    END

    RETURN
END
