* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-13</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.FT.BRN.CUR.1.W.FCY
** TO PL CREATE ACCOUNT MASK ACCORDING TO CURRENCY
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF MESSAGE # "VAL" THEN


        IF V$FUNCTION = "I" THEN
            CO.CODE = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
            R.NEW(FT.DEBIT.ACCT.NO)= COMI:'161510001':CO.CODE
******************************************************************

            R.NEW(FT.CREDIT.CURRENCY) = COMI


            CR = COMI
*Line [ 48 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CR,CRNO)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CR,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
CRNO=R.ITSS.CURRENCY<EB.CUR.NUMERIC.CCY.CODE>

            ACC.NO = '99433300': CRNO :'507199'
            R.NEW(FT.CREDIT.ACCT.NO) = ACC.NO

            CALL REBUILD.SCREEN
        END
    END
    RETURN
END
