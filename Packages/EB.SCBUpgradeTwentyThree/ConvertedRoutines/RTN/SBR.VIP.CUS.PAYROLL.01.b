* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBR.VIP.CUS.PAYROLL.01

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*-----------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-----------------------------------------------------------
INITIATE:

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.MCH = 'F.SCB.SUEZ.MECH.SAL' ; F.MCH = ''
    CALL OPF(FN.MCH,F.MCH)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)


    WS.AMT = 0 ; AMT.FLAG = 0 ; AMT.FLAG1 = 0

    KEY.LIST  ="" ; SELECTED  ="" ;  ER.MSG =""
    KEY.LIST1 ="" ; SELECTED1 ="" ;  ER.MSG1=""
    KEY.LIST2 ="" ; SELECTED2 ="" ;  ER.MSG2=""

    OPENSEQ "VIP.CUS" , "VIP.CUSTOMER.PAYROLL.GE.50000.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"VIP.CUS":' ':"VIP.CUSTOMER.PAYROLL.GE.50000.CSV"
        HUSH OFF
    END
    OPENSEQ "VIP.CUS" , "VIP.CUSTOMER.PAYROLL.GE.50000.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE VIP.CUSTOMER.PAYROLL.GE.50000.CSV CREATED IN VIP.CUS'
        END ELSE
            STOP 'Cannot create VIP.CUSTOMER.PAYROLL.GE.50000.CSV File IN VIP.CUS'
        END
    END

    DAT = TODAY

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "����� �������� ������� �������� ���� �� 50000 ���� 3 ����"
    HEAD.DESC = ",":HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "��� ����":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    MONTH.1 = TODAY[1,6]:'01'
    CALL CDT("",MONTH.1,'-1C')
    FIRST.MONTH = MONTH.1[1,6]

    MONTH.2 = MONTH.1[1,6]:'01'
    CALL CDT("",MONTH.2,'-1C')
    SECOND.MONTH = MONTH.2[1,6]

    MONTH.3 = MONTH.2[1,6]:'01'
    CALL CDT("",MONTH.3,'-1C')
    THIRD.MONTH = MONTH.3[1,6]

    RETURN
*------------------------------------------------------------
PROCESS:
*=======
    T.SEL1 = "SELECT F.SCB.SUEZ.MECH.SAL WITH SALARY.DATE GE ":THIRD.MONTH:" AND SALARY.DATE LE ":FIRST.MONTH:" BY CO.CODE BY CUS.NO"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1

            CALL F.READ(FN.MCH,KEY.LIST1<I>,R.MCH,F.MCH,E4)
            CALL F.READ(FN.MCH,KEY.LIST1<I+1>,R.MCH1,F.MCH,E2)

            CUS.ID  = R.MCH<MCH.CUS.NO>
            CUS.ID1 = R.MCH1<MCH.CUS.NO>

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E5)
            WS.TARGET = R.CU<EB.CUS.TARGET>
            IF WS.TARGET EQ 5800 THEN
                AC.CCY = R.MCH<MCH.CURRENCY>
                CALL F.READ(FN.CCY,AC.CCY,R.CCY,F.CCY,E1)
                IF AC.CCY EQ 'EGP' THEN
                    RATE = 1
                END ELSE
                    IF AC.CCY EQ 'JPY' THEN
                        RATE = R.CCY<SBD.CURR.MID.RATE> / 100
                    END ELSE
                        RATE = R.CCY<SBD.CURR.MID.RATE>
                    END
                END

                WS.AMT = R.MCH<MCH.AMOUNT> * RATE
                WS.AMT = DROUND(WS.AMT,'0')

                IF WS.AMT GE 50000 THEN
                    AMT.FLAG  = 1
                END ELSE
                    AMT.FLAG1 = 1
                END

                IF CUS.ID NE CUS.ID1 THEN
                    WS.CUS.NAME = R.MCH<MCH.EMP.NAME>
                    WS.BRN.ID   = R.MCH<MCH.CO.CODE>

                    CALL F.READ(FN.COM,WS.BRN.ID,R.COM,F.COM,E3)
                    WS.BRN.NAME = R.COM<EB.COM.COMPANY.NAME,2>

                    IF AMT.FLAG EQ 1 THEN
                        BB.DATA  = CUS.ID:','
                        BB.DATA := WS.CUS.NAME:','
                        BB.DATA := WS.AMT:','
                        BB.DATA := WS.BRN.NAME

                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END
                    END
                    WS.AMT    = 0
                    AMT.FLAG  = 0
                    AMT.FLAG1 = 0
                END
            END
        NEXT I
    END

    RETURN
*============================================================
