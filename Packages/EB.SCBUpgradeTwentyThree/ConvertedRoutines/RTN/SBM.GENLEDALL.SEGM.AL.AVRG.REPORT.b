* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBM.GENLEDALL.SEGM.AL.AVRG.REPORT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENLEDALL.SEGM.AVRG
*--------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS
    RETURN
*-------------------------------------------------------------------------
INITIATE:
*========

    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.SEGMENT.AL.AVRG.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"GENLEDALL.SEGMENT.AL.AVRG.CSV"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "GENLEDALL.SEGMENT.AL.AVRG.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE GENLEDALL.SEGMENT.AL.AVRG.CSV CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create GENLEDALL.SEGMENT.AL.AVRG.CSV File IN &SAVEDLISTS&'
        END
    END
*---------------------------------
    WS.DAT.END.MONTH = TODAY[1,6]:'01'
    CALL CDT("",WS.DAT.END.MONTH,'-1C')

    WS.DAYS  = WS.DAT.END.MONTH[2]
    WS.MONTH = '...':WS.DAT.END.MONTH[1,6]:'...':'.AL'

    DAT = TODAY
    CALL CDT("",DAT,'-1W')

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD.DESC   = "CATEGORY":","
    HEAD.DESC  := "CATEGORY.DESC":","
    HEAD.DESC  := "CURRENCY":","
    HEAD.DESC  := "AMOUNT.EQUIVALENT":","
    HEAD.DESC  := "NATIVE.AMOUNT":","
    HEAD.DESC  := "LINE":","
    HEAD.DESC  := "LINE.DESC":","
    HEAD.DESC  := "BRANCH Code":","
    HEAD.DESC  := "BRANCH.NAME":","
    HEAD.DESC  := "Product type":","
    HEAD.DESC  := "Segmentation Code":","
    HEAD.DESC  := "Segmentation Desc":","
    HEAD.DESC  := DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

*--------------------------------------

    FN.AVRG = 'F.SCB.GENLEDALL.SEGM.AVRG' ; F.AVRG = ''
    CALL OPF(FN.AVRG,F.AVRG)

    FN.CAT = 'F.CATEGORY' ; F.CAT = ''
    CALL OPF(FN.CAT,F.CAT)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    WS.CATEG    = '' ; WS.CATEG.DESC = '' ; WS.CCY       = '' ; WS.AMT.LCY = 0
    WS.AMT.FCY  = 0  ; WS.LINE.NO    = 0  ; WS.LINE.DESC = '' ; WS.COMP = ''
    WS.BRN.NAME = '' ; WS.PRODUCT    = 0  ; WS.TARGET    = 0  ; WS.TARGET.NAME = 0
    WS.AMT.FCY.AVRG = 0 ; WS.AMT.LCY.AVRG = 0

    RETURN
*-----------------------------------------------------------------------------
PROCESS:
*=======
    T.SEL = "SELECT ":FN.AVRG:" WITH @ID LIKE ":WS.MONTH:" BY @ID"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.AVRG,KEY.LIST<I>,R.AVRG,F.AVRG,E5)
            CALL F.READ(FN.AVRG,KEY.LIST<I+1>,R.AVRG1,F.AVRG1,E6)

            WS.LINE.NO = R.AVRG<SEGM.LINE>
            WS.CONSOL  = R.AVRG<SEGM.CONSOL.KEY>

            WS.LINE.NO1 = R.AVRG1<SEGM.LINE>
            WS.CONSOL1  = R.AVRG1<SEGM.CONSOL.KEY>

            WS.BK  = WS.CONSOL:'.':WS.LINE.NO
            WS.BK1 = WS.CONSOL1:'.':WS.LINE.NO1

            WS.AMT.FCY += R.AVRG<SEGM.NATIVE.AMOUNT>
            WS.AMT.LCY += R.AVRG<SEGM.AMOUNT.EQUIVALENT>

            WS.CATEG       = R.AVRG<SEGM.CATEGORY>
            WS.CATEG.DESC  = R.AVRG<SEGM.CATEGORY.DESC>
            WS.CCY         = R.AVRG<SEGM.CURRENCY>
            WS.LINE.DESC   = R.AVRG<SEGM.LINE.DESC>
            WS.COMP        = R.AVRG<SEGM.BRANCH.CODE>
            WS.BRN.NAME    = R.AVRG<SEGM.BRANCH.NAME>
            WS.PRODUCT     = R.AVRG<SEGM.PRODUCT.TYPE>
            WS.TARGET      = R.AVRG<SEGM.SEGM.CODE>
            WS.TARGET.NAME = R.AVRG<SEGM.SEGM.DESC>

            IF WS.BK NE WS.BK1 THEN
                WS.AMT.FCY.AVRG = WS.AMT.FCY / WS.DAYS
                WS.AMT.LCY.AVRG = WS.AMT.LCY / WS.DAYS

                WS.AMT.FCY.AVRG = DROUND(WS.AMT.FCY.AVRG,'2')
                WS.AMT.LCY.AVRG = DROUND(WS.AMT.LCY.AVRG,'2')

                BB.DATA  = WS.CATEG:","
                BB.DATA := WS.CATEG.DESC:","
                BB.DATA := WS.CCY:","
                BB.DATA := WS.AMT.LCY.AVRG:","
                BB.DATA := WS.AMT.FCY.AVRG:","
                BB.DATA := WS.LINE.NO:","
                BB.DATA := WS.LINE.DESC:","
                BB.DATA := WS.COMP:","
                BB.DATA := WS.BRN.NAME:","
                BB.DATA := WS.PRODUCT:","
                BB.DATA := WS.TARGET:","
                BB.DATA := WS.TARGET.NAME:","

                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END

                WS.AMT.FCY = 0
                WS.AMT.LCY = 0
            END

        NEXT I
    END

    RETURN
*====================================================
