* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    SUBROUTINE VVR.ATM.DRMNT.OLD

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_GTS.COMMON
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*---------------------------------------------
    IF MESSAGE EQ 'VAL' THEN
        FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
        CALL OPF(FN.CU,F.CU)

        CUS.ID = COMI

        CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,E2)

        WS.DRMNT.CODE   = R.CU<EB.CUS.LOCAL.REF><1,CULR.DRMNT.CODE>
        WS.POSTING.CODE = R.CU<EB.CUS.POSTING.RESTRICT>

        IF WS.DRMNT.CODE EQ '1' THEN

            E = 'RESPONS.CODE=12' :",ATM.123.REF.NO=" : R.NEW(FT.LOCAL.REF)<1,FTLR.ATM.123.REF.NO> :",EBC.AUTH.NO=" : R.NEW(FT.LOCAL.REF)<1, FTLR.EBC.AUTH.NO.NO>  :  ",TERMINAL.ID=" :  R.NEW(FT.LOCAL.REF)<1,FTLR.TERMINAL.ID> ;  CALL ERR; MESSAGE='REPEAT'

        END

    END



    RETURN
END
