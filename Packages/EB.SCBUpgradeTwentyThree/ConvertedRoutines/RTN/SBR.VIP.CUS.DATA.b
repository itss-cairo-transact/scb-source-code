* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBR.VIP.CUS.DATA

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TARGET
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*-----------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-----------------------------------------------------------
INITIATE:

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.TAR = 'FBNK.TARGET' ; F.TAR = ''
    CALL OPF(FN.TAR,F.TAR)

    KEY.LIST2  ="" ; SELECTED2  ="" ;  ER.MSG2 =""

    OPENSEQ "VIP.CUS" , "VIP.CUSTOMER.DATA.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"VIP.CUS":' ':"VIP.CUSTOMER.DATA.CSV"
        HUSH OFF
    END
    OPENSEQ "VIP.CUS" , "VIP.CUSTOMER.DATA.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE VIP.CUSTOMER.DATA.CSV CREATED IN VIP.CUS'
        END ELSE
            STOP 'Cannot create VIP.CUSTOMER.DATA.CSV File IN VIP.CUS'
        END
    END

    DAT = TODAY

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "����� ������ ����� �����"
    HEAD.DESC = ",":HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "����� ������":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*------------------------------------------------------------
PROCESS:

    T.SEL2 = "SELECT ":FN.CU:" WITH TARGET IN (5800 5850 5860 5870 5900) AND SECTOR EQ 2000 AND POSTING.RESTRICT LT 80 AND DRMNT.CODE NE 1 AND POSTING.RESTRICT NE 70 BY COMPANY.BOOK BY @ID"
    CALL EB.READLIST(T.SEL2,KEY.LIST2,"",SELECTED2,ER.MSG2)
    IF SELECTED2 THEN
        FOR I = 1 TO SELECTED2
            CALL F.READ(FN.CU,KEY.LIST2<I>,R.CU,F.CU,E5)
            CUS.ID = KEY.LIST2<I>

            WS.CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            WS.BRN.ID   = R.CU<EB.CUS.COMPANY.BOOK>
            WS.TARGET   = R.CU<EB.CUS.TARGET>

            CALL F.READ(FN.TAR,WS.TARGET,R.TAR,F.TAR,E3)
            WS.TARGET.NAME = R.TAR<EB.TAR.DESCRIPTION,1>

            CALL F.READ(FN.COM,WS.BRN.ID,R.COM,F.COM,E3)
            WS.BRN.NAME = R.COM<EB.COM.COMPANY.NAME,2>

            BB.DATA  = CUS.ID:','
            BB.DATA := WS.CUS.NAME:','
            BB.DATA := WS.TARGET.NAME:','
            BB.DATA := WS.BRN.NAME

            WRITESEQ BB.DATA TO BB ELSE
                PRINT " ERROR WRITE FILE "
            END


        NEXT I
    END

    RETURN
*============================================================
