* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>-682</Rating>
*-----------------------------------------------------------------------------
*    PROGRAM BR.EMP.TXT
    SUBROUTINE BR.EMP.TXT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*==================== create cf and cs text files ========

    OPENSEQ "&SAVEDLISTS&" , "BR.EMP.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"BR.EMP.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "BR.EMP.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE BR.EMP.TXT CREATED IN &SAVEDLISTS&'
        END
        ELSE
            STOP 'Cannot create BR.EMP.TXT File IN &SAVEDLISTS&'
        END
    END
*****
*========================= DEFINE VARIABLES =========================
    COMP   = ID.COMPANY
    TT.DATE = TODAY
*============================ OPEN TABLES =========================
    YTEXT = "Enter From Date::"
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    FF.DATE = COMI
******************************
    YTEXT = "Enter To Date::"
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    TT.DATE = COMI

    FN.CU = 'FBNK.CUSTOMER'; F.CU = ''; R.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.BR = 'FBNK.BILL.REGISTER'; F.BR = ''; R.BR = ''
    CALL OPF(FN.BR,F.BR)
    NO.REC = 0
    T.SEL  = "SELECT FBNK.BILL.REGISTER WITH COLL.DATE GE ":FF.DATE:" AND COLL.DATE LE ":TT.DATE:" AND AMOUNT GE 50000 AND BILL.CHQ.STA EQ 8 BY COLL.DATE"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

*======================== BB.DATA ===========================

    IF SELECTED THEN
        BB.DATA  = '��� ������':'|'
        BB.DATA := '�����':'|'
        BB.DATA := '��� ������':'|'
        BB.DATA := '��� �������':'|'
        BB.DATA := '��� �������':'|'
        BB.DATA := '��� ������':'|'
        BB.DATA := '������':'|'
        BB.DATA := '������':'|'
        BB.DATA := '����� �������'

        WRITESEQ BB.DATA TO BB ELSE
            PRINT " ERROR WRITE FILE "
        END
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.BR,KEY.LIST<I>,R.BR,F.BR,ETEXT)
            CUS.ID = R.BR<EB.BILL.REG.DRAWER>

            CALL F.READ(FN.CU,CUS.ID,R.CU,F.CU,ETEXT)
            CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
            SECTOR   = R.CU<EB.CUS.SECTOR>
            BRAN.NO  = R.CU<EB.CUS.ACCOUNT.OFFICER>
*Line [ 106 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*            CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,BRAN.NO,BRAN.NAME)
F.ITSS.DEPT.ACCT.OFFICER = 'F.DEPT.ACCT.OFFICER'
FN.F.ITSS.DEPT.ACCT.OFFICER = ''
CALL OPF(F.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER)
CALL F.READ(F.ITSS.DEPT.ACCT.OFFICER,BRAN.NO,R.ITSS.DEPT.ACCT.OFFICER,FN.F.ITSS.DEPT.ACCT.OFFICER,ERROR.DEPT.ACCT.OFFICER)
BRAN.NAME=R.ITSS.DEPT.ACCT.OFFICER<EB.DAO.NAME>
            BRAN.NAME = FIELD(BRAN.NAME,'.',2)
            IF SECTOR EQ 1100 OR SECTOR EQ 1300 THEN
                NO.REC   = NO.REC + 1
                LD.NO    = KEY.LIST<I>
                AMT      = R.BR<EB.BILL.REG.AMOUNT>
                CURR     = R.BR<EB.BILL.REG.CURRENCY>
                ACC.NO   = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.LIQ.ACCT>
                TRANS.DATE = R.BR<EB.BILL.REG.LOCAL.REF><1,BRLR.COLL.DATE>


                BB.DATA  = CUS.ID:'|'
                BB.DATA := BRAN.NAME:'|'
                BB.DATA := CUS.NAME:'|'
                BB.DATA := LD.NO:'|'
                BB.DATA := '����� �����':'|'
                BB.DATA := ACC.NO:'|'
                BB.DATA := CURR:'|'
                BB.DATA := AMT:'|'
                BB.DATA := TRANS.DATE
                WRITESEQ BB.DATA TO BB ELSE
                    PRINT " ERROR WRITE FILE "
                END
            END
        NEXT I
    END


    CLOSESEQ BB

    TEXT = "No of Records= ":NO.REC; CALL REM
END
