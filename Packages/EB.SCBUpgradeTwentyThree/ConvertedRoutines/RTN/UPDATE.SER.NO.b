* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
** ----- NESSREEN AHMED 01/06/2010 -----
*-----------------------------------------------------------------------------
* <Rating>0</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE UPDATE.SER.NO

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.P.SER.NO

    FN.P.SER = 'F.SCB.P.SER.NO' ; F.P.SER = '' ; R.P.SER = ''
**    FN.TELLER = 'F.TELLER$HIS' ; F.TELLER = '' ; R.TELLER = ''
    FN.TELLER = 'F.TELLER'     ; F.TELLER = '' ; R.TELLER = ''
    ETEXT = '' ; ETEXT.T = '' ; ETEXT.FT = '' ; ZZ= '' ; XX = ''

    CALL OPF( FN.P.SER,F.P.SER)
    CALL OPF( FN.TELLER,F.TELLER)
****UPDATED BY NESSREEN AHMED 25/7/2010**************************************
**** TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(2 20 97 98) AND RECORD.STATUS NE 'REVE' AND SER.NO NE '' AND AUTH.DATE EQ 20100520 BY AUTH.DATE "
****UPDATED BY NESSREEN AHMED 2/9/2010**************************************************************************
****UPDATED BY NESSREEN AHMED 24/8/2010************************************************************************************************************************
****** TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(2 20 97 98) AND RECORD.STATUS NE 'REVE' AND SER.NO NE '' AND (AUTH.DATE GE 20081123 AND AUTH.DATE LE 20100630) BY AUTH.DATE "
****TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(2 20 97 98) AND RECORD.STATUS NE 'REVE' AND SER.NO NE '' AND AUTH.DATE GE 20100701 BY AUTH.DATE "
*** TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(41 21 105 106) AND RECORD.STATUS NE 'REVE' AND SER.NO NE '' AND (AUTH.DATE GE 20081123 AND AUTH.DATE LE 20100831) BY AUTH.DATE "
**  TT.SEL = "SELECT FBNK.TELLER$HIS WITH TRANSACTION.CODE IN(41 21 105 106) AND RECORD.STATUS NE 'REVE' AND SER.NO NE '' AND (AUTH.DATE GE 20100901 AND AUTH.DATE LE 20100902) BY AUTH.DATE "
    TT.SEL = "SELECT FBNK.TELLER WITH TRANSACTION.CODE IN(41 21 105 106) AND SER.NO NE '' AND AUTH.DATE EQ 20100902 "
***********************
    KEY.LIST.TT=""
    SELECTED.TT=""
    ER.MSG.TT=""

    CALL EB.READLIST(TT.SEL,KEY.LIST.TT,"",SELECTED.TT,ER.MSG.TT)
    TEXT = 'SELECTED.TT=':SELECTED.TT ; CALL REM
    IF SELECTED.TT THEN
        FOR TTI = 1 TO SELECTED.TT
            TRN.DAT = '' ; OUR.REF = ''
            SER.NO = '' ; COMP.CODE = '' ; DEP.CO = '' ;  KEY.TO.USE = '' ; ACCT.NO = '' ; REF = '' ; TT.REF = ''  ; TT.R = ''
            ETEXT.R = '' ; CURR = ''  ; SER.AMT = ''

            CALL F.READ( FN.TELLER,KEY.LIST.TT<TTI>, R.TELLER, F.TELLER, ETEXT.TT)
            SER.NO    = R.TELLER<TT.TE.LOCAL.REF,TTLR.SER.NO>
            CURR      = R.TELLER<TT.TE.CURRENCY.1>
            SER.AMT   = R.TELLER<TT.TE.NET.AMOUNT>
            TRN.DAT   = R.TELLER<TT.TE.AUTH.DATE>
            ACCT.NO   = R.TELLER<TT.TE.ACCOUNT.1>
            COMP.CODE = R.TELLER<TT.TE.CO.CODE>
            DEP.CO    = R.TELLER<TT.TE.DEPT.CODE>
**   CALL DBR ('ACCOUNT':@FM:AC.ACCOUNT.OFFICER,ACCT.NO,DEP.CO)
            REF = KEY.LIST.TT<TTI>
            TT.REF  = FIELD(REF, ";" ,1)
            TT.R = TT.REF:";2"

            CALL F.READ( FN.TELLER,TT.R, R.TELLER, F.TELLER, ETEXT.R)
            IF ETEXT.R THEN
**  KEY.TO.USE = SER.NO:".":DEP.CO
                KEY.TO.USE = SER.NO
                CALL F.READ( FN.P.SER,KEY.TO.USE, R.P.SER, F.P.SER, ETEXT)
                IF ETEXT THEN
                    R.P.SER<PSN.REF.NO>     = TT.REF
                    R.P.SER<PSN.CURR>       = CURR
                    R.P.SER<PSN.TRN.DAT>    = TRN.DAT
                    R.P.SER<PSN.ACCT.NO>    = ACCT.NO
                    R.P.SER<PSN.AMOUNT.NO>  = SER.AMT
                    R.P.SER<PSN.SER.NO>     = SER.NO
                    R.P.SER<PSN.COMPANY.CO> = COMP.CODE
                    R.P.SER<PSN.SER.STATUS> = "PAID"

                    CALL F.WRITE(FN.P.SER,KEY.TO.USE,R.P.SER)
                    CALL JOURNAL.UPDATE(KEY.TO.USE)
                END
            END
        NEXT TTI
    END
    RETURN
END
