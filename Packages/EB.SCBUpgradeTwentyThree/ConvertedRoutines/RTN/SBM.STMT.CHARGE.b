* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBM.STMT.CHARGE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.STMT.CHARGE
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 46 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 48 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.HOLD.POST.CHECK
*Line [ 50 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD


    PRINT "THE CHARGE IS DONE"

    RETURN
*------------------------------
INITIALISE:
*----------
    FN.OFS.SOURCE ="F.OFS.SOURCE"
    F.OFS.SOURCE = ""

    CALL OPF(FN.OFS.SOURCE,F.OFS.SOURCE)
    CALL F.READ(FN.OFS.SOURCE,"OFS.CONV.PROCESS",OFS.SOURCE.REC,F.OFS.SOURCE,'')
    FN.OFS.IN        = OFS.SOURCE.REC<OFS.SRC.IN.QUEUE.DIR>
    FN.OFS.BK        = OFS.SOURCE.REC<OFS.SRC.LOCAL.REF,OFSSRCLR.OFS.BACKUP>
    F.OFS.IN         = 0
    F.OFS.BK         = 0
    OFS.REC          = ""
    OFS.OPERATION    = "FUNDS.TRANSFER"
    OFS.OPTIONS      = "SCB1"
    OFS.USER.INFO    = "/"
    OFS.TRANS.ID     = ""
    OFS.MESSAGE.DATA = ""

    CHG.ID = "ACPOSTD"
*Line [ 80 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*    CALL DBR('FT.CHARGE.TYPE':@FM:FT5.CATEGORY.ACCOUNT,CHG.ID,CR.ACT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,CHG.ID,R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
CR.ACT=R.ITSS.FT.CHARGE.TYPE<FT5.CATEGORY.ACCOUNT>

    V.DAT = TODAY

    RETURN
*----------------------------------------------------
BUILD.RECORD:
    COMMA = ","
    KEY.LIST1="" ; SELECTED1="" ;  ER.MSG1=""
    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CB = 'FBNK.CUSTOMER.ACCOUNT' ; F.CB = ''
    CALL OPF(FN.CB,F.CB)

    FN.ACCT = 'FBNK.ACCOUNT' ; F.ACCT = ''
    CALL OPF(FN.ACCT,F.ACCT)

*--------------------------
    T.SEL1  = "SELECT FBNK.CUSTOMER WITH SECTOR NE 1100 AND SECTOR NE 1200 AND SECTOR NE 1300 AND SECTOR NE 1400"
    T.SEL1 := " AND POSTING.RESTRICT NE 99 AND POSTING.RESTRICT NE 90 AND POSTING.RESTRICT NE 70 AND CREDIT.CODE NE 110 AND CREDIT.CODE NE 120"
    T.SEL1 := " AND STMT.CHARGE.EXP NE 'Y' AND STMT.CHARGE.FRQ NE '' AND STMT.CHARGE.FRQ NE 'QUARTER' BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)

    IF SELECTED1 THEN
        FOR I = 1 TO SELECTED1
            CALL F.READ(FN.CU,KEY.LIST1<I>,R.CU,F.CU,E1)
            CUST.ID = KEY.LIST1<I>
            CUFLG  = 0
            CUFLG1 = 0
            FLG1 = 0 ; FLG2 = 0

            WS.STMT.FRQ = R.CU<EB.CUS.LOCAL.REF><1,CULR.STMT.CHARGE.FRQ>

            COMP = R.CU<EB.CUS.COMPANY.BOOK>
            COM.CODE = COMP[8,2]

            OFS.USER.INFO = "AUTO.CHRGE":"/":"/" :COMP
***********************************************
            CALL F.READ(FN.CB,CUST.ID,R.CB,F.CB,E1)
            IF NOT(E1) THEN
***************************************************************
*Line [ 128 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.ID,LOC.REF1)
F.ITSS.CUSTOMER = 'F.CUSTOMER'
FN.F.ITSS.CUSTOMER = ''
CALL OPF(F.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER)
CALL F.READ(F.ITSS.CUSTOMER,CUST.ID,R.ITSS.CUSTOMER,FN.F.ITSS.CUSTOMER,ERROR.CUSTOMER)
LOC.REF1=R.ITSS.CUSTOMER<EB.CUS.LOCAL.REF>

***************************************************************
                FLG = 0
                H = 1
                LOOP WHILE FLG = 0
                    IF R.CB<H,EB.CAC.ACCOUNT.NUMBER> = '' THEN
                        FLG = 1
                    END
                    ACT.CB = R.CB<H,EB.CAC.ACCOUNT.NUMBER>
*Line [ 144 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('ACCOUNT':@FM:AC.WORKING.BALANCE,ACT.CB,W.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
W.BAL=R.ITSS.ACCOUNT<AC.WORKING.BALANCE>
                    ACT.CUR = ACT.CB[9,2]
*Line [ 152 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                    CALL DBR('NUMERIC.CURRENCY':@FM:EB.NCN.CURRENCY.CODE,ACT.CUR,CUR.CODE)
F.ITSS.NUMERIC.CURRENCY = 'F.NUMERIC.CURRENCY'
FN.F.ITSS.NUMERIC.CURRENCY = ''
CALL OPF(F.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY)
CALL F.READ(F.ITSS.NUMERIC.CURRENCY,ACT.CUR,R.ITSS.NUMERIC.CURRENCY,FN.F.ITSS.NUMERIC.CURRENCY,ERROR.NUMERIC.CURRENCY)
CUR.CODE=R.ITSS.NUMERIC.CURRENCY<EB.NCN.CURRENCY.CODE>
                    ACT.CATEG = ACT.CB[11,4]
                    IF ACT.CATEG EQ 1001 OR ACT.CATEG EQ 1002 OR ACT.CATEG EQ 6512 OR ACT.CATEG EQ 6511 OR ACT.CATEG EQ 1006 OR (ACT.CATEG GE 6514 AND ACT.CATEG LE 6517) OR (ACT.CATEG GE 6501 AND ACT.CATEG LE 6504) OR (ACT.CATEG GE 1101 AND ACT.CATEG LE 1204 OR ACT.CATEG GE 1290 AND ACT.CATEG LE 1599) THEN

*Line [ 162 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,CUR.CODE,RATE)
F.ITSS.CURRENCY = 'F.CURRENCY'
FN.F.ITSS.CURRENCY = ''
CALL OPF(F.ITSS.CURRENCY,FN.F.ITSS.CURRENCY)
CALL F.READ(F.ITSS.CURRENCY,CUR.CODE,R.ITSS.CURRENCY,FN.F.ITSS.CURRENCY,ERROR.CURRENCY)
RATE=R.ITSS.CURRENCY<EB.CUR.MID.REVAL.RATE>
*Line [ 169 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('ACCOUNT':@FM:AC.LIMIT.REF,ACT.CB,LIM.REF)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LIM.REF=R.ITSS.ACCOUNT<AC.LIMIT.REF>
*Line [ 176 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('ACCOUNT':@FM:AC.CUSTOMER,ACT.CB,ACT.CUS)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
ACT.CUS=R.ITSS.ACCOUNT<AC.CUSTOMER>
                        LIM.ID = ACT.CUS:".":"0000":LIM.REF
*Line [ 184 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('LIMIT':@FM:LI.AVAIL.AMT,LIM.ID,AVIL.BAL)
F.ITSS.LIMIT = 'F.LIMIT'
FN.F.ITSS.LIMIT = ''
CALL OPF(F.ITSS.LIMIT,FN.F.ITSS.LIMIT)
CALL F.READ(F.ITSS.LIMIT,LIM.ID,R.ITSS.LIMIT,FN.F.ITSS.LIMIT,ERROR.LIMIT)
AVIL.BAL=R.ITSS.LIMIT<LI.AVAIL.AMT>
                        END.BAL.L22 = W.BAL + AVIL.BAL

************************ UPDATED BY KHALED *** 2009/03/11 ***************************
*Line [ 194 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                        CALL DBR('ACCOUNT':@FM:AC.LOCKED.AMOUNT,ACT.CB,LOC.BAL)
F.ITSS.ACCOUNT = 'FBNK.ACCOUNT'
FN.F.ITSS.ACCOUNT = ''
CALL OPF(F.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT)
CALL F.READ(F.ITSS.ACCOUNT,ACT.CB,R.ITSS.ACCOUNT,FN.F.ITSS.ACCOUNT,ERROR.ACCOUNT)
LOC.BAL=R.ITSS.ACCOUNT<AC.LOCKED.AMOUNT>
*Line [ 147 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                        BALC = DCOUNT(LOC.BAL,@VM)
                        BALCC = LOC.BAL<1,BALC>
                        END.BAL.L = END.BAL.L22 - BALCC
*************************************************************************************

                        IF CUR.CODE EQ "EGP" THEN
                            RATE = 1
                        END

                        IF WS.STMT.FRQ EQ 'DAILY' THEN
*Line [ 212 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,'ACPOSTD',DB.AMT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,'ACPOSTD',R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DB.AMT=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>
                        END

                        IF WS.STMT.FRQ EQ 'WEEKLY' THEN
*Line [ 222 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,'ACPOSTW',DB.AMT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,'ACPOSTW',R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DB.AMT=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>
                        END

                        IF WS.STMT.FRQ EQ 'MONTHLY' THEN
*Line [ 232 ] Change CALL DBR function to F.READ function - ITSS - R21 Upgrade - 2022-02-09
*                            CALL DBR('FT.CHARGE.TYPE':@FM:FT5.FLAT.AMT,'ACPOSTM',DB.AMT)
F.ITSS.FT.CHARGE.TYPE = 'F.FT.CHARGE.TYPE'
FN.F.ITSS.FT.CHARGE.TYPE = ''
CALL OPF(F.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE)
CALL F.READ(F.ITSS.FT.CHARGE.TYPE,'ACPOSTM',R.ITSS.FT.CHARGE.TYPE,FN.F.ITSS.FT.CHARGE.TYPE,ERROR.FT.CHARGE.TYPE)
DB.AMT=R.ITSS.FT.CHARGE.TYPE<FT5.FLAT.AMT>
                        END

*******************************

                        IF CUR.CODE NE "EGP" THEN
                            DB.AMT = (DB.AMT / RATE)
                            DB.AMTE = FIELD(DB.AMT,".",2)
                            DB.AMT  = FIELD(DB.AMT,".",1):".":DB.AMTE[1,2]
                        END
                        LST.BAL = END.BAL.L
*******************************************************************
                        IF CUFLG NE 1 THEN
                            IF LST.BAL GE DB.AMT THEN
                                DEBIT.ACCT = ACT.CB
                                CUS = ACT.CB[1,8]:".":TODAY
                                GOSUB CR.FT.OFS
                                LST.BAL = LST.BAL - DB.AMT
                                CUFLG = 1
                            END ELSE
                                CUFLG = 3
                            END
                        END
*******************************************************************
                        IF CUFLG = 1 AND LOC.REF1<1,CULR.GOVERNORATE> NE '98' THEN
                            FLG = 1
                        END ELSE IF CUFLG1 = 2 THEN FLG = 1
*******************************************************************
                    END
                    H = H + 1
                REPEAT
            END

        NEXT I
    END
    RETURN
****************************************************************

CR.FT.OFS:
*********
    DR.ACCT = DEBIT.ACCT
    CURR = DEBIT.ACCT[9,2]
    CR.ACCT = "PL":CR.ACT
    DATEE = TODAY
**************************************************CRAETE FT BY OFS**********************************************
    OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC50":COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":CURR:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":CURR:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":DR.ACCT:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":CR.ACCT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":DB.AMT:COMMA

    OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":DATEE:COMMA
    OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":DATEE:COMMA

    OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
    OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":CUS[1,7]

    F.PATH = FN.OFS.IN
    OFS.REC = OFS.OPERATION:COMMA:OFS.OPTIONS:COMMA:OFS.USER.INFO:COMMA:COMMA:OFS.MESSAGE.DATA
    TER.NO.P = COM.CODE + 1000
    OFS.ID = "T":TRIM(TER.NO.P, '0', 'L'):".P.":CUS

    OPENPATH FN.OFS.IN TO F.OFS.IN ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.IN, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM
    OPENPATH FN.OFS.BK TO F.OFS.BK ELSE OFS.ERR = 1
    WRITE OFS.REC ON F.OFS.BK, OFS.ID:'-':COMP: ON ERROR  TEXT = " ERROR ";CALL REM ; STOP
    RETURN
************************************************************

END
