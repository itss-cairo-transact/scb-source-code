* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    SUBROUTINE VNC.GET.RETURN.NEW.FCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.FCY
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCCB.BT.PARTIAL.FCY
*----------------------------------------------
    IF V$FUNCTION ='A' THEN RETURN
    IF V$FUNCTION ='R' THEN RETURN
    IF V$FUNCTION ='D' THEN RETURN

    LINE.FLG = 0
    FLG.EOF  = 0
    TOD = R.DATES(EB.DAT.TODAY)
    TXT.ID = TOD[1,4]:"-":TOD[5,2]:"-":TOD[7,2]:'-credit.fcy.new.txt'
    TEXT = TXT.ID; CALL REM
    Path   = "/life/CAIRO/NT24/bnk/bnk.run/FCY.CLEARING/":TXT.ID
    OPENSEQ Path TO MyPath ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    EOF = ''  ;  I = 1  ;   LINE.NO = 0

    FN.TMP = "F.SCCB.BT.PARTIAL.FCY"    ;   F.TMP = "" ; R.TMP = ""
    CALL OPF(FN.TMP , F.TMP)

    TMP.ID = ID.COMPANY:".":TOD
    CALL F.READ(FN.TMP,TMP.ID,R.TMP,F.TMP,ERR)
    FLG = ""

    IF ERR THEN
        INDX = 1
    END ELSE
        INDX = R.TMP<SCCB.BT.NEXT.BT.INDEX>
    END

    LOOP WHILE NOT(EOF)
        READSEQ Line FROM MyPath THEN
            OUR.ID            = FIELD(Line,",",3)
            RETURN.REASON     = FIELD(Line,",",13)[1,3]
            RESULT.CODE       = FIELD(Line,",",1)
            DEPT.CODE         = FIELD(Line,",",8)

            LINE.NO ++
            IF LINE.NO EQ INDX THEN
                LINE.FLG = 1
            END
            DEPT.USR  = TRIM(ID.COMPANY[8,2],'0',"L")

            IF ( DEPT.USR EQ DEPT.CODE ) AND ( LINE.FLG EQ 1 ) THEN
                IF I LE 400 THEN
                    IF RESULT.CODE EQ "220" THEN
                        R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = OUR.ID
                        I++
                    END
                    IF RESULT.CODE EQ "820" THEN
                        R.NEW(SCB.BT.OUR.REFERENCE)<1,I> = OUR.ID
                        RET.RESON = TRIM(RETURN.REASON,' ', "R")
                        RET.RESON = TRIM(RETURN.REASON,' ', "L")
                        R.NEW(SCB.BT.RETURN.REASON)<1,I> = RET.RESON
                        I++
                    END
                END
                IF I GT 400 THEN
                    FLG.EOF = 1
                    EOF     = 1
                END
            END
        END ELSE
            EOF = 1
        END
    REPEAT
    CLOSESEQ MyPath

    IF EOF EQ 1 THEN
        R.NEW(SCB.BT.CBE.REF) = LINE.NO + 1
        CALL REBUILD.SCREEN
    END

    IF FLG.EOF EQ 1 THEN
        TEXT = "���� ���� �� ������" ;  CALL REM
    END

    IF FLG.EOF EQ 0 THEN
        TEXT =  " �� ���� ���� ��  ������"; CALL REM
    END

    RETURN
*------------------------------------------------
END
