* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
    PROGRAM SBM.VIP.CUS.CHARGE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.CHARGE.TYPE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VIP.CUS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VIP.CUS.BAL
*-----------------------------------------------------------
    GOSUB CHECK.FT
    IF FLAG EQ 0 THEN
        GOSUB INITIATE
        GOSUB PROCESS
    END
    RETURN
*-----------------------------------------------------------
CHECK.FT:

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    T.SEL = "SELECT ":FN.FT:" WITH TRANSACTION.TYPE EQ 'AC71' AND DEBIT.THEIR.REF EQ 'VIP.MIN.CHARGE'"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        PRINT 'THIS PROGRAM DONE BEFORE'
        FLAG = 1
    END ELSE
        FLAG = 0
    END

    RETURN
*-----------------------------------------------------------
INITIATE:

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    FN.CP = 'F.SCB.VIP.CUS' ; F.CP = ''
    CALL OPF(FN.CP,F.CP)

    FN.VI = 'F.SCB.VIP.CUS.BAL' ; F.VI = ''
    CALL OPF(FN.VI,F.VI)

    FN.CHG = 'FBNK.FT.CHARGE.TYPE' ; F.CHG = ''
    CALL OPF(FN.CHG,F.CHG)

    FN.CA = 'FBNK.CUSTOMER.ACCOUNT' ; F.CA = ''
    CALL OPF(FN.CA,F.CA)

    WS.DAT = TODAY       ; WS.AMT = 0     ; WS.CATEG.LINE = ''
    CHG.ID = "ACVIPCHRG" ; V.DATE = TODAY ; COMMA  = ','

    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    KEY.LIST1 ="" ; SELECTED1="" ;  ER.MSG1=""
    KEY.LIST2 ="" ; SELECTED2="" ;  ER.MSG2=""

    OPENSEQ "VIP.CUS" , "VIP.CUSTOMER.CHARGE" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"VIP.CUS":' ':"VIP.CUSTOMER.CHARGE"
        HUSH OFF
    END
    OPENSEQ "VIP.CUS" , "VIP.CUSTOMER.CHARGE" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE VIP.CUSTOMER.CHARGE CREATED IN VIP.CUS'
        END ELSE
            STOP 'Cannot create VIP.CUSTOMER.CHARGE File IN VIP.CUS'
        END
    END

    WS.END.MONTH = TODAY[1,6]:'01'
    CALL CDT("",WS.END.MONTH,'-1C')
    WS.MONTH = WS.END.MONTH[1,6]

    RETURN
*------------------------------------------------------------
PROCESS:
*=======
    CALL F.READ(FN.CHG,CHG.ID,R.CHG,F.CHG,ER.CHG)
    WS.DB.AMT  = R.CHG<FT5.FLAT.AMT>
    WS.PL.ACCT = R.CHG<FT5.CATEGORY.ACCOUNT>
    WS.CR.ACCT = 'PL':WS.PL.ACCT

    T.SEL1 = "SELECT ":FN.CP:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.CP,KEY.LIST1<K>,R.CP,F.CP,E3)
*Line [ 121 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.CATEG.COUNT = DCOUNT(R.CP<VIP.CATEGORY>,@VM)
            FOR X = 1 TO WS.CATEG.COUNT
                WS.CATEG1      = R.CP<VIP.CATEGORY><1,X>
                WS.CATEG.LINE := WS.CATEG1:' '
            NEXT X

        NEXT K
    END

    WS.CATEG = '(':WS.CATEG.LINE:')'

    T.SEL = "SELECT ":FN.VI:" WITH BAL.DATE EQ ":WS.MONTH
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.VI,KEY.LIST<I>,R.VI,F.VI,E1)
            WS.AMT = R.VI<VIPBL.AMOUNT>

            IF WS.AMT LT 1000000 THEN
                CUS.ID = R.VI<VIPBL.CUSTOMER>
                CALL F.READ(FN.CA,CUS.ID,R.CA,F.CA,E2)
                LOOP
                    REMOVE ACC.NO FROM R.CA SETTING POS.ACC
                WHILE ACC.NO:POS.ACC
                    CALL F.READ(FN.AC,ACC.NO,R.AC,F.AC,ER.AC)
                    AC.CATEG = R.AC<AC.CATEGORY>
                    AC.CCY   = R.AC<AC.CURRENCY>
                    FINDSTR AC.CATEG IN WS.CATEG SETTING POS THEN
                        IF AC.CCY EQ 'EGP' THEN
                            WS.DB.ACCT = ACC.NO
                            WS.AC.BAL  = R.AC<AC.ONLINE.ACTUAL.BAL>
                            WS.COMP    = R.AC<AC.CO.CODE>
                            IF WS.AC.BAL GT WS.DB.AMT THEN
                                IDD = 'FUNDS.TRANSFER,MECH,AUTO.CHRGE//':WS.COMP:','
                                OFS.MESSAGE.DATA  =  "TRANSACTION.TYPE=":"AC71":COMMA
                                OFS.MESSAGE.DATA :=  "DEBIT.CURRENCY=":AC.CCY:COMMA
                                OFS.MESSAGE.DATA :=  "CREDIT.CURRENCY=":AC.CCY:COMMA
                                OFS.MESSAGE.DATA :=  "DEBIT.ACCT.NO=":WS.DB.ACCT:COMMA
                                OFS.MESSAGE.DATA :=  "CREDIT.ACCT.NO=":WS.CR.ACCT:COMMA
                                OFS.MESSAGE.DATA :=  "DEBIT.AMOUNT=":WS.DB.AMT:COMMA
                                OFS.MESSAGE.DATA :=  "DEBIT.VALUE.DATE=":V.DATE:COMMA
                                OFS.MESSAGE.DATA :=  "CREDIT.VALUE.DATE=":V.DATE:COMMA
                                OFS.MESSAGE.DATA :=  "DR.ADVICE.REQD.Y.N=":"NO":COMMA
                                OFS.MESSAGE.DATA :=  "CR.ADVICE.REQD.Y.N=":"NO":COMMA
                                OFS.MESSAGE.DATA :=  "ORDERING.BANK=":"SCB":COMMA
                                OFS.MESSAGE.DATA :=  "DEBIT.THEIR.REF=":"VIP.MIN.CHARGE"

                                MSG.DATA = IDD:",":OFS.MESSAGE.DATA
                                WRITESEQ MSG.DATA TO BB ELSE
                                    PRINT " ERROR WRITE FILE "
                                END
                                WS.AMT = 0
                                GO TO EXIT.LOOP
                            END
                        END
                    END
                REPEAT
            END
EXIT.LOOP:
        NEXT I
    END

*** COPY TO OFS ***

    EXECUTE 'COPY FROM VIP.CUS TO OFS.IN VIP.CUSTOMER.CHARGE'
    EXECUTE 'DELETE ':"VIP.CUS":' ':"VIP.CUSTOMER.CHARGE"

    RETURN
*============================================================
