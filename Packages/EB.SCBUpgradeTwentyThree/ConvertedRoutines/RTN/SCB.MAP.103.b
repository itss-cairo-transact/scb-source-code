* @ValidationCode : MjotNjUzMzUwMzc2OkNwMTI1MjoxNjQyMzMxNzYwODAwOnVzZXI6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjFfQU1SLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 16 Jan 2022 13:16:00
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : user
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R21_AMR.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThree  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThree
*DONE
*-----------------------------------------------------------------------------
* <Rating>25638</Rating>
*-----------------------------------------------------------------------------
SUBROUTINE SCB.MAP.103(MAT.EXAMPLE.REC, ERR.MSG)

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
    $INCLUDE I_FT.LOCAL.REFS
*-----------------------------------------------------------------------
    ERR.MSG = ""
    IF R.NEW(FT.DEBIT.CURRENCY) = 'EGP' THEN

*        TEXT = "= ":I:" - ":EXAMPLE.REC(1)<1,I> ; CALL REM
*        TEXT = "= ":I:" - ":EXAMPLE.REC(2)<1,I> ; CALL REM

        EXAMPLE.REC(2)<83> = "PEG"
    END

RETURN
END
