* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*----------------------------------------------------------*
* <Rating>-11</Rating>
*----------------------------------------------------------*
    SUBROUTINE VNC.CU.DEFAULTS
**-------------------------------**
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CATEGORY
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SEIZED.CUST

*TO DEFAULT FEILD ACCOUNT.TITLE,SHORT.TITLE,ARABIC.TITLE WITH ARABIC CUSTOMER NAME AND CATEGORY NAME
    IF V$FUNCTION = 'I' THEN 
        CUST.NO = ID.NEW
*--------------------------
        CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,MYLOCAL)

        ARNAME      = MYLOCAL<1,CULR.ARABIC.NAME>
        PROF        =  MYLOCAL<1,CULR.PROFESSION>
        NATIONAL.ID = MYLOCAL<1,CULR.NSN.NO>
        PASSPORT.NO =  MYLOCAL<1,CULR.ID.NUMBER>
        REG.NO      = MYLOCAL<1,CULR.COM.REG.NO>
        TAX.NO      = MYLOCAL<1,CULR.TAX.NO> 

        R.NEW(SCB.CUS.ID)       = CUST.NO
        R.NEW(SCB.CUS.NAME)     = ARNAME
        R.NEW(SCB.PROFISSION)   = PROF
        R.NEW(SCB.NATIONAL.ID)  = NATIONAL.ID
        R.NEW(SCB.ID.NUMBER)    = PASSPORT.NO
        R.NEW(SCB.COM.REG.NO)   = REG.NO
        R.NEW(SCB.COM.TAX.NO)   = TAX.NO

***  R.NEW(AC.LOCAL.REF)<1,ACLR.ARABIC.TITLE> = ARNAME
        CALL REBUILD.SCREEN

    END
    RETURN
END
