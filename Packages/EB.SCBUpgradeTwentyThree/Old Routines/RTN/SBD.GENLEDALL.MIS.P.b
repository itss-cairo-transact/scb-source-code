* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>97</Rating>
*-----------------------------------------------------------------------------
    PROGRAM SBD.GENLEDALL.MIS.P

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.CONT
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.LINE.BAL
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.STAT.REP.LINE
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.GENLEDALL.MIS

    CALL DBR ('DATES':@FM:EB.DAT.LAST.PERIOD.END,'EG0010001',DAT)
    CALL DBR ('DATES':@FM:EB.DAT.LAST.WORKING.DAY,'EG0010001',DAT.LW)
*DAT = '20180102'
*DAT.LW = '20180102'
PROMPT "ENTER LAST PERIOD DATE  : " ; INPUT DAT
PROMPT "ENTER LAST WORKING DATE : " ; INPUT DAT.LW

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN
*------------------------------
INITIALISE:
*----------
    FN.CONT    = 'F.RE.STAT.LINE.CONT'      ; F.CONT = ''
    FN.BAL     = 'F.RE.STAT.LINE.BAL'       ; F.BAL  = ''
    FN.CONS    = 'F.CONSOLIDATE.PRFT.LOSS'  ; F.CONS = ''
    FN.LN      = 'F.RE.STAT.REP.LINE'       ; F.LN   = ''
    FN.COM     = 'F.COMPANY'  ; F.COM = ''  ; R.COM = ''
    FN.BASE    = 'FBNK.RE.BASE.CCY.PARAM'   ; F.BASE = ''    ; R.BASE = ''
    FN.GEN.MIS = 'F.SCB.GENLEDALL.MIS'      ; F.GEN.MIS = '' ; R.GEN.MIS = ''


    CALL OPF(FN.CONT,F.CONT)
    CALL OPF(FN.BAL,F.BAL)
    CALL OPF(FN.CONS,F.CONS)
    CALL OPF(FN.LN,F.LN)
    CALL OPF(FN.COM,F.COM)
    CALL OPF(FN.BASE,F.BASE)

    T.SEL.LN = "" ; KEY.LIST.LN = "" ; SELECTED.LN = "" ;  ER.MSG.LN = ""
    NET.AMT     = 0

    RETURN
*----------------------------------------------------------------**
BUILD.RECORD:
*------------
    T.SEL1 = "" ; KEY.LIST1 = "" ; SELECTED1 = "" ;  ER.MSG1 = ""
*----------------------------------------------------------------**
    T.SEL1 = "SELECT ":FN.COM:" WITH @ID NE EG0010088"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG2)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.COM,KEY.LIST1<K>,R.COM,F.COM,E1)
            COMP.CODE = KEY.LIST1<K>[2]

*            T.SEL.LN = "SELECT ":FN.LN:" WITH ( @ID LIKE GENDETALL.... OR @ID LIKE GENDETALL... ) AND PROFT.APPLIC.IDNE NE 'PL' BY @ID"
            T.SEL.LN = "SELECT ":FN.LN:" WITH ( @ID LIKE GENDETALL.... OR @ID LIKE GENLEDALL... ) BY @ID"
            CALL EB.READLIST(T.SEL.LN,KEY.LIST.LN,"",SELECTED.LN,ER.MSG2)

            IF SELECTED.LN THEN

                FOR RR = 1 TO SELECTED.LN

                    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
                    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
*Line [ 98 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                    CURR.CNT = DCOUNT(CURR.BASE,@VM)
                    FOR XX = 1 TO CURR.CNT
                        ID.BAL1 = FIELD(KEY.LIST.LN<RR>,".",1):'-':FIELD(KEY.LIST.LN<RR>,".",2):'-':CURR.BASE<1,XX>:'-':DAT.LW:'*':KEY.LIST1<K>
                        ID.BAL = FIELD(KEY.LIST.LN<RR>,".",1):'-':FIELD(KEY.LIST.LN<RR>,".",2):'-':CURR.BASE<1,XX>:'-':DAT:'*':KEY.LIST1<K>

*                        IF CURR.BASE<1,XX> EQ 'CHF' THEN
*                        END
                        CALL F.READ(FN.BAL,ID.BAL,R.BAL,F.BAL,E4)
                        IF R.BAL<RE.SLB.CLOSING.BAL.LCL> OR R.BAL<RE.SLB.CR.MVMT.LCL> OR R.BAL<RE.SLB.DB.MVMT.LCL> THEN
*                        IF NOT(E4) THEN
                            GOSUB CREATE.REC

                        END
                    NEXT XX
                NEXT RR
            END
        NEXT K
    END
    RETURN
**----------------------------------------------------------------------**
CREATE.REC:
*----------
    R.GEN.MIS<SGM.LINE.NO>        = KEY.LIST.LN<RR>
    R.GEN.MIS<SGM.BOOKING.DATE>   = DAT.LW
    R.GEN.MIS<SGM.COMPANY.CODE>   = KEY.LIST1<K>
    R.GEN.MIS<SGM.CURRENCY>       = CURR.BASE<1,XX>
    R.GEN.MIS<SGM.AMOUNT.FCY>     = R.BAL<RE.SLB.CLOSING.BAL>
    R.GEN.MIS<SGM.AMOUNT.LCY>     = R.BAL<RE.SLB.CLOSING.BAL.LCL>
    CALL F.WRITE(FN.GEN.MIS,ID.BAL1,R.GEN.MIS)
    CALL JOURNAL.UPDATE(ID.BAL)
*================== CHECK DATE IF HOLIDAY ===========================
    TD = DAT.LW
    CALL CDT("",TD,'+1C')
    CALL AWD("EG00",TD,AA)
    IF AA EQ 'H' THEN
        LOOP WHILE AA = 'H'
            ID.BAL1 = FIELD(KEY.LIST.LN<RR>,".",1):'-':FIELD(KEY.LIST.LN<RR>,".",2):'-':CURR.BASE<1,XX>:'-':TD:'*':KEY.LIST1<K>
            R.GEN.MIS<SGM.BOOKING.DATE>   = TD
            CALL F.WRITE(FN.GEN.MIS,ID.BAL1,R.GEN.MIS)
            CALL JOURNAL.UPDATE(ID.BAL1)
            CALL CDT("",TD,'+1C')
            CALL AWD("EG00",TD,AA)
        REPEAT
    END
    RETURN
*---------------------------------------------------------------------**
