* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>-1</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.SCB.CBE.FCY

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_F.BILL.REGISTER
    $INSERT T24.BP I_F.FT.COMMISSION.TYPE
    $INSERT T24.BP I_F.CUSTOMER
    $INSERT T24.BP I_F.CURRENCY
    $INSERT           I_F.SCB.BR.CUS
    $INSERT           I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BR.PAY.PLACE
    $INSERT            I_BR.LOCAL.REFS
    $INSERT           I_F.SCB.BR.SLIPS
*------------- 2011/ 11/ 15 -ADD AS VIR on VERSION  ---------------
    CUR      = R.NEW(EB.BILL.REG.CURRENCY)
    CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR,CUR1)
    TEXT = CUR:'*':CUR1 ;CALL REM
    IF PGM.VERSION NE ',SCB.CHQ.LCY.REG.0017' THEN
        IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'NO' ) THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>  = '99433300':CUR1:'507099'
        END ELSE
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>  = '99433300':CUR1:'5075':ID.COMPANY[8,2]
        END
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> EQ '' THEN
*Line [ 43 ] Adding '' instead of NULL - ITSS - R21 Upgrade - 2021-12-26
            E='ACCOUNT CBE IS '' ';CALL ERR;MESSAGE='REPEAT'
        END
    END
*------------------------------------------------------------------
    RETURN
END
