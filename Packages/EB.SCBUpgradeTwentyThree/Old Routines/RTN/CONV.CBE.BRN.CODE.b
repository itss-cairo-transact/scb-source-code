* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE CONV.CBE.BRN.CODE

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*-------------------------------------

    COMP = O.DATA

    IF COMP EQ "EG0010001" THEN
        BRN.CODE = '0101'
    END

    IF COMP EQ "EG0010006" THEN
        BRN.CODE = '0213'
    END

    IF COMP EQ "EG0010013" THEN
        BRN.CODE = '0178'
    END

    IF COMP EQ "EG0010021" THEN
        BRN.CODE = '1053'
    END

    IF COMP EQ "EG0010023" THEN
        BRN.CODE = '0252'
    END

    IF COMP EQ "EG0010032" THEN
        BRN.CODE = '0506'
    END

    IF COMP EQ "EG0010070" THEN
        BRN.CODE = '4514'
    END

    IF COMP EQ "EG0010007" THEN
        BRN.CODE = '0196'
    END

    IF COMP EQ "EG0010002" THEN
        BRN.CODE = '0147'
    END

    IF COMP EQ "EG0010005" THEN
        BRN.CODE = '0180'
    END

    IF COMP EQ "EG0010020" THEN
        BRN.CODE = '1001'
    END

    IF COMP EQ "EG0010040" THEN
        BRN.CODE = '1601'
    END

    IF COMP EQ "EG0010060" THEN
        BRN.CODE = '3001'
    END

    IF COMP EQ "EG0010004" THEN
        BRN.CODE = '5515'
    END

    IF COMP EQ "EG0010014" THEN
        BRN.CODE = '5531'
    END

    IF COMP EQ "EG0010015" THEN
        BRN.CODE = '5557'
    END

    IF COMP EQ "EG0010010" THEN
        BRN.CODE = '0143'
    END

    IF COMP EQ "EG0010009" THEN
        BRN.CODE = '0246'
    END

    IF COMP EQ "EG0010022" THEN
        BRN.CODE = '1029'
    END

    IF COMP EQ "EG0010030" THEN
        BRN.CODE = '1501'
    END

    IF COMP EQ "EG0010050" THEN
        BRN.CODE = '1801'
    END

    IF COMP EQ "EG0010090" THEN
        BRN.CODE = '3514'
    END

    IF COMP EQ "EG0010003" THEN
        BRN.CODE = '5501'
    END

    IF COMP EQ "EG0010011" THEN
        BRN.CODE = '5524'
    END

    IF COMP EQ "EG0010012" THEN
        BRN.CODE = '5537'
    END

    IF COMP EQ "EG0010035" THEN
        BRN.CODE = '9952'
    END

    IF COMP EQ "EG0010080" THEN
        BRN.CODE = '0508'
    END

    IF COMP EQ "EG0010081" THEN
        BRN.CODE = '0507'
    END

    IF COMP EQ "EG0010031" THEN
        BRN.CODE = '0117'
    END

    IF COMP EQ "EG0010051" THEN
        BRN.CODE = '0107'
    END

    IF COMP EQ "EG0010016" THEN
        BRN.CODE = '0102'
    END

    IF COMP EQ "EG0010017" THEN
        BRN.CODE = '0108'
    END

    IF COMP EQ "EG0010019" THEN
        BRN.CODE = '0103'
    END

    IF COMP EQ "EG0010052" THEN
        BRN.CODE = '0110'
    END

    IF COMP EQ "EG0010053" THEN
        BRN.CODE = '0510'
    END

    IF COMP EQ "EG0010018" THEN
        BRN.CODE = '0109'
    END

    IF COMP EQ "EG0010024" THEN
        BRN.CODE = '0513'
    END

    IF COMP EQ "EG0010025" THEN
        BRN.CODE = '0511'
    END

    IF COMP EQ "EG0010041" THEN
        BRN.CODE = '0522'
    END

    IF COMP EQ "EG0010026" THEN
        BRN.CODE = '0514'
    END

    IF COMP EQ "EG0010027" THEN
        BRN.CODE = '0512'
    END

    IF COMP EQ "EG0010029" THEN
        BRN.CODE = '0515'
    END

    IF COMP EQ "EG0010033" THEN
        BRN.CODE = '0516'
    END

    IF COMP EQ "EG0010083" THEN
        BRN.CODE = '0517'
    END

    IF COMP EQ "EG0010034" THEN
        BRN.CODE = '0521'
    END

    IF COMP EQ "EG0010082" THEN
        BRN.CODE = '0518'
    END

    IF COMP EQ "EG0010036" THEN
        BRN.CODE = '0520'
    END

    IF COMP EQ "EG0010045" THEN
        BRN.CODE = '0523'
    END

    O.DATA = BRN.CODE

    RETURN
