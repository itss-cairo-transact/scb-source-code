* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>1591</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY NESSREEN AHMED 9/3/2021****
*********************************************
    SUBROUTINE VIR.TT.MAX.WITHDRAW.NEW

*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.DR.CHQ
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_TT.LOCAL.REFS
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.AMOUNT.RANGE


    IF (V$FUNCTION # 'R') AND (V$FUNCTION # 'D' ) THEN

        KEY.LIST = "" ; SELECTED = "" ;  ER.MSG = ""
        KEY.LIST.N = "" ; SELECTED.N = "" ;  ER.MSG.N = ""
        KEY.LIST.H = "" ; SELECTED.H = "" ;  ER.MSG.H = ""
        KEY.LIST.AC = "" ; SELECTED.AC = "" ; ER.MSG.AC = ""
        MAXAMT = 0   ; AMT.ID = "MAXWITH"  ; AMTRG = 0 ; CATEG1 = ''

        FN.TT   = 'FBNK.TELLER' ; F.TT = ''
        CALL OPF(FN.TT,F.TT)

        FN.TT.N   = 'FBNK.TELLER$NAU' ; F.TT.N = ''
        CALL OPF(FN.TT.N,F.TT.N)


        FN.CUS = 'FBNK.CUSTOMER' ; F.CUS = '' ; R.CUS = '' ; ER.CUS = ''
        CALL OPF(FN.CUS,F.CUS)

        FN.AC = 'FBNK.ACCOUNT' ; F.AC= '' ; R.AC = '' ; ERR.AC = ''
        CALL OPF(FN.AC,F.AC)

        ACCT = R.NEW(TT.TE.ACCOUNT.1)
        CURR.1    = R.NEW(TT.TE.CURRENCY.1)
        CONT.GRP.1= R.NEW(TT.TE.CONTRACT.GRP)
        IF CONT.GRP.1 NE '' THEN
            IF CURR.1 = 'EGP'  THEN
                IF ACCT[1,3] # '994' THEN
                    CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACCT , CATEG1)
                    IF CATEG1 NE '1002' THEN
                        TOT.AMT = 0 ; AMT.EQV.USD = ''  ; AMT.EQV.USD.AC = '' ; AMT.LCY.FCY = ''
                        SELL.RATE = '' ; SELL.RATE.H = '' ; SELL.RATE.FCY = ''
                        AC.BAL = '' ; CATEG = ''  ; TOT.AMT.AC = '' ;  AMT.AC = '' ; ACT = ''  ; AC.CURR = ''
                        TEXT = '' ; ON.AC.BAL1008 = 0  ; TOT.AMT = 0  ; TOT.AMTLV = 0 ;  TOT.AMTNAU = 0

                        CUST      = R.NEW(TT.TE.CUSTOMER.1)
                        CURR.1    = R.NEW(TT.TE.CURRENCY.1)
                        AMT.LCY.1 = R.NEW(TT.TE.AMOUNT.LOCAL.1)
                        AMT.FCY.1 = R.NEW(TT.TE.AMOUNT.FCY.1)
                        CONT.GRP.1= R.NEW(TT.TE.CONTRACT.GRP)
                        CALL F.READ(FN.AC, ACCT,R.AC,F.AC,ERR.AC)
                        CATEG = R.AC<AC.CATEGORY>
                        TOT.AMT = TOT.AMT + AMT.LCY.1
                        *TEXT = 'TRANS.AMT=':TOT.AMT ; CALL REM
********Search unauthorized records********************
****Updated by NESSREEN AHMED 7/7/2021************************************************************
*****Updated by NESSREEN AHMED 22/6/2021************************
*****S.SEL = "SELECT FBNK.TELLER$NAU WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 EQ ":CUST :" AND CURRENCY.1 EQ 'EGP' AND @ID NE ": ID.NEW
****S.SEL = "SELECT FBNK.TELLER$NAU WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 EQ ":CUST :" AND CURRENCY.1 EQ 'EGP' AND @ID NE ": ID.NEW :" AND CATEG.1 NE '1002' "
S.SEL = "SELECT FBNK.TELLER$NAU WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 EQ ":CUST :" AND CURRENCY.1 EQ 'EGP' AND @ID NE ": ID.NEW :" AND CATEG.1 NE '1002' AND CHEQUE.NO EQ '' "
*****End of update 22/6/2021*******************************************
                        CALL EB.READLIST(S.SEL, KEY.LIST.S, "", SELECTED.S, ASD.S)
                        *TEXT = '��� ������ ��� ��� ����� ������=':SELECTED.S ; CALL REM
                        IF SELECTED.S THEN
                            FOR SS = 1 TO SELECTED.S
                                CATEG1 = ''
                                CALL F.READ( FN.TT.N,KEY.LIST.S<SS>, R.TT.N,F.TT.N, ERR.TT.N)
                                CUST<SS>         = R.TT.N<TT.TE.CUSTOMER.1>
                                TT.CURR<SS>      = R.TT.N<TT.TE.CURRENCY.1>
                                AMT.FCY<SS>      = R.TT.N<TT.TE.AMOUNT.FCY.1>
                                AMT.LCY<SS>      = R.TT.N<TT.TE.AMOUNT.LOCAL.1>
                                CONT.GRP<SS>     = R.TT.N<TT.TE.CONTRACT.GRP>
                                ACCT<SS>         = R.NEW(TT.TE.ACCOUNT.1)
                                CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY,ACCT<SS> , CATEG1)
                                IF CATEG1 NE '1002' THEN
                                    TOT.AMT = TOT.AMT + AMT.LCY<SS>
                                    TOT.AMTNAU = TOT.AMTNAU + AMT.LCY<SS>
                                END
                            NEXT SS
                            *TEXT = '������ ���� ��� ����=':TOT.AMTNAU ; CALL REM
                        END   ;*end of SELECTED.S
********Search authorized records********************
****Updated by NESSREEN AHMED 7/7/2021************************************************************
*****Updated by NESSREEN AHMED 22/6/2021************************
*****N.SEL = "SELECT FBNK.TELLER WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 EQ ":CUST:" AND CURRENCY.1 EQ 'EGP' "
****N.SEL = "SELECT FBNK.TELLER WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 EQ ":CUST:" AND CURRENCY.1 EQ 'EGP' AND CATEG.1 NE '1002' "
    N.SEL = "SELECT FBNK.TELLER WITH DR.CR.MARKER EQ 'DEBIT' AND CUSTOMER.1 EQ ":CUST:" AND CURRENCY.1 EQ 'EGP' AND CATEG.1 NE '1002' AND CHEQUE.NO EQ '' "
*****End of update 22/6/2021*******************************************
****End of update 7/7/2021**********************************************************************
                        CALL EB.READLIST(N.SEL, KEY.LIST.N, "", SELECTED.N, ASD.N)
                        *TEXT = '��� ������ ����� ������� ������=':SELECTED.N ; CALL REM
                        IF SELECTED.N THEN
                            FOR I = 1 TO SELECTED.N
                                CATEG1.N = ''
                                CALL F.READ( FN.TT,KEY.LIST.N<I>, R.TT,F.TT, ERR.TT)
                                CUST.L<I>       = R.TT<TT.TE.CUSTOMER.1>
                                TT.CURR.N<I>      = R.TT<TT.TE.CURRENCY.1>
                                AMT.FCY.N<I>      = R.TT<TT.TE.AMOUNT.FCY.1>
                                AMT.LCY.N<I>      = R.TT<TT.TE.AMOUNT.LOCAL.1>
                                CONT.GRP.N<I>     = R.TT<TT.TE.CONTRACT.GRP>
                                ACCT.N<I>         = R.NEW(TT.TE.ACCOUNT.1)
                                CALL DBR( 'ACCOUNT':@FM:AC.CATEGORY, ACCT.N<I>  , CATEG1.N)
                                IF CATEG1.N NE '1002' THEN
                                    TOT.AMTLV = TOT.AMTLV + AMT.LCY.N<I>
                                    TOT.AMT = TOT.AMT + AMT.LCY.N<I>
                                END
                            NEXT I
                           *TEXT = '������ ���� ����=' :TOT.AMTLV ; CALL REM
                        END   ;* IF SELECTED.N
******************************************************
                        *TEXT = 'Cu.Total Amount=' :TOT.AMT ; CALL REM
*****Case of Private Customers************************************************************************
                        CALL DBR( 'SCB.AMOUNT.RANGE':@FM:AMT.RG.RG.AMT,AMT.ID , AMTRG)
                        IF TOT.AMT GT AMTRG THEN
                            TEXT = ' ����� ������ �� ����� ������'
                            CALL STORE.OVERRIDE(CURR.NO)
                        END   ;*end of IF TOT.AMT GT 50000
                    END       ;*IF CATEG1 NE '1002'
                END ;*IF ACCT[1,3] # '994' THEN
            END     ;*IF CURR.1 EQ EGP
        END         ;*IF IF CONT.GRP.1 NE ''
    END   ;*IF V$FUNCTION # 'R'

    RETURN
END
