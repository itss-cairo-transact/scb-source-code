* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>-682</Rating>
*-----------------------------------------------------------------------------
    PROGRAM ISCO.SME.FIX.APP.AMT
*  SUBROUTINE ISCO.SME.FIX.APP.AMT

*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.NUMERIC.CURRENCY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CS
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CF

*========================= DEFINE VARIABLES =========================
    EOF  = ''
    R.CF = ''
    COMP = ID.COMPANY
    TODAY.DATE = TODAY
    MONTH      = TODAY.DATE[5,2]
    YEAR       = TODAY.DATE[1,4]

    IF MONTH EQ 1 THEN
        YEAR      = YEAR - 1
        NEW.MONTH = 12
    END
    ELSE
        NEW.MONTH = MONTH - 1
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END

    T.DATE = YEAR:NEW.MONTH
*============================ OPEN TABLES =========================
    FN.CF  = 'F.SCB.ISCO.SME.CF' ; F.CF = '' ; R.CF = ''
    CALL OPF(FN.CF,F.CF)
    FN.CS  = 'F.SCB.ISCO.SME.CS' ; F.CS = '' ; R.CS = ''
    CALL OPF(FN.CS,F.CS)
    FN.CUR= 'FBNK.CURRENCY'      ; F.CUR=''  ; R.CUR = '' ; E11=''
    CALL OPF(FN.CUR,F.CUR)
    FN.N.CUR= 'F.NUMERIC.CURRENCY'  ; F.N.CUR=''  ; R.N.CUR = '' ; E10=''
    CALL OPF(FN.N.CUR,F.N.CUR)
    FN.AC = 'FBNK.ACCOUNT'       ; F.AC = '' ; R.AC = ''
    CALL OPF(FN.AC,F.AC)
    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>
**********************************************
    T.SEL  = "SELECT F.SCB.ISCO.SME.CF WITH @ID LIKE ...":YEAR:NEW.MONTH:" BY CF.ACC.NO"

    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)
*======================== BB.DATA ===========================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            FLAG = 'Y'
            CALL F.READ(FN.CF,KEY.LIST<I>,R.CF,F.CF,ETEXT)
            PRIME.ACCT = KEY.LIST<I>
            PRIME.AC   = PRIME.ACCT[1,16]
            CALL F.READ(FN.AC,PRIME.AC,R.AC,F.AC,ER)
            PRIME.CURR = R.AC<AC.CURRENCY>
            ACC.NO     = R.CF<ISCO.CF.CF.ACC.NO>

            LOCATE PRIME.CURR IN CURR.BASE<1,1> SETTING POS THEN
                PRIME.RATE = R.BASE<RE.BCP.RATE,POS>
            END

            CHK.ACC     = ACC.NO[1,14]
            CALL F.READ( FN.AC,ACC.NO,R.AC1,F.AC,ER1)
            CUS.NO      = R.AC1<AC.CUSTOMER>
            *IF ACC.NO EQ '0120071510122001' THEN DEBUG
            CATEG       = R.AC1<AC.CATEGORY>
            PRIME.APP.AMT = R.CF<ISCO.CF.APPROVAL.AMOUNT>
            PRIME.LMT   = R.AC1<AC.LIMIT.REF>
            CHK.ID      = "...":CUS.NO:"...":CATEG:"...":YEAR:NEW.MONTH:"..."
            TOT.APP.AMT = 0
**********************************************************
            Q.SEL   = "SELECT F.SCB.ISCO.SME.CF WITH @ID LIKE ":CHK.ID:" BY CF.ACC.NO"
            CALL EB.READLIST(Q.SEL, KEY.LIST1, "", SELECTED1, ASD1)
            IF SELECTED1 LT 2 THEN
                FLAG = 'N'
*I    = I + 1
            END
            IF FLAG EQ 'Y' THEN
                FOR J = 2 TO SELECTED1
                    ACCT.NO = ''; AC.NO = ''
                    CALL F.READ( FN.CF,KEY.LIST1<J>,R.CF1,F.CF,ETEXT)
                    ACCT.NO     = KEY.LIST1<J>
                    AC.NO       = ACCT.NO[1,16]
                    CALL F.READ( FN.AC,AC.NO,R.AC,F.AC,ETEXT3)
                    ACCT.CURR   = R.CF1<ISCO.CF.CURRENCY>
                    AC.CURR     = R.AC<AC.CURRENCY>
                    AC.LMT      = R.AC<AC.LIMIT.REF>

                    LOCATE AC.CURR IN CURR.BASE<1,1> SETTING POS THEN
                        ACCT.RATE = R.BASE<RE.BCP.RATE,POS>
                    END

                    ACC.BAL = R.CF1<ISCO.CF.CF.ACCT.BALANCE>
                    APP.AMT = ACC.BAL

                    IF PRIME.LMT EQ AC.LMT AND AC.LMT NE '' THEN
                        R.CF1<ISCO.CF.APPROVAL.AMOUNT> = APP.AMT
                        CURR.APP.AMT = APP.AMT * ACCT.RATE
                        TOT.APP.AMT += CURR.APP.AMT
                        CALL F.WRITE(FN.CF,ACCT.NO,R.CF1)
                        CALL JOURNAL.UPDATE(ACCT.NO)
                    END

                NEXT J

                I = I + SELECTED1
                IF PRIME.LMT NE '' THEN
                TOT.APP.AMT = TOT.APP.AMT / PRIME.RATE
                PRIME.APP.AMT = PRIME.APP.AMT - TOT.APP.AMT
                R.CF<ISCO.CF.APPROVAL.AMOUNT> = PRIME.APP.AMT
                CALL F.WRITE(FN.CF,PRIME.ACCT,R.CF)
                CALL JOURNAL.UPDATE(PRIME.ACCT)
                END
            END
        NEXT I

    END

    RETURN

*=======================

END
