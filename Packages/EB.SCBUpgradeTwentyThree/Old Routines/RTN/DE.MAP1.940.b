* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE DE.MAP1.940(MAT YHANDOFF.REC, ERR.MSG)
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*
    YCOUNT = 0
*Line [ 25 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
    YCOUNT = DCOUNT(YHANDOFF.REC(1)<10>, @VM)
    YY6 = 0
    FOR YY6 = 1 TO YCOUNT


        YFTIDL = ''
        YFTIDH = ''
        YFTIDL = YHANDOFF.REC(1)<10,YY6>
        YFTIDH = YHANDOFF.REC(1)<10,YY6> : ";1"
        FTPAYDET = ''

        CALL DBR("FUNDS.TRANSFER":@FM:FT.PAYMENT.DETAILS,YFTIDL,FTPAYDET)
        IF NOT(ETEXT) THEN

*Line [ 40 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            FPDCOUNT = DCOUNT(FTPAYDET,@VM)
            IFPD = 0
            FOR IFPD = 1 TO FPDCOUNT
                YHANDOFF.REC(4)<1,YY6,IFPD> = FTPAYDET<1,IFPD>
            NEXT IFPD
            CALL DBR("FUNDS.TRANSFER":@FM:FT.COMMISSION.AMT,YFTIDH,FTCHG6)
            CHGVAR1 = FPDCOUNT + 1
            LENVAR1 = LEN(FTCHG6<1,1>)
            FTCHGAMT6 = FTCHG6<1,1>[4,LENVAR1]
            YHANDOFF.REC(4)<1,YY6,CHGVAR1> = "CHARGES: " : FTCHGAMT6

        END
        ELSE

            FTPAYDET = ''
            CALL DBR("FUNDS.TRANSFER$HIS":@FM:FT.PAYMENT.DETAILS,YFTIDH,FTPAYDET)

            IF NOT(ETEXT) THEN

*Line [ 60 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
                FPDCOUNT = DCOUNT(FTPAYDET,@VM)
                IFPD = 0
                FOR IFPD = 1 TO FPDCOUNT
                    YHANDOFF.REC(4)<1,YY6,IFPD> = FTPAYDET<1,IFPD>
                NEXT IFPD
                CALL DBR("FUNDS.TRANSFER$HIS":@FM:FT.COMMISSION.AMT,YFTIDH,FTCHG6)
                CHGVAR1 = FPDCOUNT + 1
                LENVAR1 = LEN(FTCHG6<1,1>)
                FTCHGAMT6 = FTCHG6<1,1>[4,LENVAR1]
                YHANDOFF.REC(4)<1,YY6,CHGVAR1> = "CHARGES: " : FTCHGAMT6
            END
        END
    NEXT YY6
    RETURN
END
