* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* EDIT BY Noha
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.FCY.MATURITY.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*--------------------------------------------
    MAT.DATE      = COMI
    NEXT.MAT.DATE = TODAY
    RECIVE.DATE   = TODAY
    CALL CDT('',NEXT.MAT.DATE,"+1W")

    COMP          = ID.COMPANY
    USR.DEP       = "1700":COMP[8,2]
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)
    R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = 2

    IF MAT.DATE LE NEXT.MAT.DATE THEN
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 6

* CHECK NEXT DATE *
******************************
        CALL CDT('',RECIVE.DATE,"+1W")
        REC.DATE.1 = OCONV(RECIVE.DATE,"DI")
        REC.DATE.DAY.1 = OCONV(REC.DATE.1,"DW")
        IF REC.DATE.DAY.1 EQ 7 THEN
            CALL CDT('',RECIVE.DATE,"+1W")
        END
        MAT.DATE.1 = RECIVE.DATE

* LOOP 2 WORKING DAYS *
*************************
        FOR I = 1 TO 2
            CALL CDT('',RECIVE.DATE,"+1W")
            REC.DATE.2 = OCONV(RECIVE.DATE,"DI")
            REC.DATE.DAY.2 = OCONV(REC.DATE.2,"DW")

            IF REC.DATE.DAY.2 EQ 7 THEN
                CALL CDT('',RECIVE.DATE,"+1W")
            END
        NEXT I

        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE.1
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = RECIVE.DATE

    END
    ELSE
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BIL.CHQ.TYPE> = 7

        CALL CDT('EG00', MAT.DATE , '+1W')
        MAT.DATE.I = OCONV(MAT.DATE,"DI")
        MAT.DATE.DAY = OCONV(MAT.DATE.I,"DW")

        IF MAT.DATE.DAY EQ 7 THEN
            CALL CDT('',MAT.DATE,"+1W")
        END
        MAT.DATE.1 = MAT.DATE

* LOOP 2 WORKING DAYS *
*************************
        FOR J = 1 TO 2
            CALL CDT('',MAT.DATE,"+1W")
            MAT.DATE.3 = OCONV(MAT.DATE,"DI")
            MAT.DATE.DAY.3 = OCONV(MAT.DATE.3,"DW")
            IF MAT.DATE.DAY.3 EQ 7 THEN
                CALL CDT('',MAT.DATE,"+1W")
            END
        NEXT J

        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = MAT.DATE.1
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = MAT.DATE
    END

    CALL REBUILD.SCREEN
    RETURN
END
