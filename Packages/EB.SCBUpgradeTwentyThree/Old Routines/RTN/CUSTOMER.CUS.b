* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*    SUBROUTINE CUSTOMER.CUS
    PROGRAM CUSTOMER.CUS
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Comment DEBUG - ITSS - R21 Upgrade - 2021-12-26
    *DEBUG
    FN.CUS="FBNK.CUSTOMER"
    F.CUS=""
    Y.CUS.ID=O.DATA
    CALL OPF(FN.CUS,F.CUS)
    T.SEL="SELECT FBNK.CUSTOMER WITH INDUSTRY LT 999 AND INDUSTRY LIKE ...00"
    CALL EB.READLIST(T.SEL,SEL.LIST,'',NO.OF.REC,RET.CODE)
    LOOP
        REMOVE Y.CUS.ID FROM SEL.LIST SETTING POS
    WHILE Y.CUS.ID:POS
        CALL F.READ(FN.CUS,Y.CUS.ID,R.CUSTOMER,F.CUS,CUS.ERR1)
        Y.INDUSTRY = R.CUSTOMER<EB.CUS.INDUSTRY>
        O.DATA=Y.INDUSTRY
        CRT "INDUSTRY ":Y.CUS.ID
    REPEAT
    RETURN
END
