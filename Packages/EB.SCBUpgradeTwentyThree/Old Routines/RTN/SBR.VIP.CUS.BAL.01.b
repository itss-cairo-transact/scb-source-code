* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    PROGRAM SBR.VIP.CUS.BAL.01

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VIP.CUS
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CUS.POS.LW
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SBD.CURRENCY
*-----------------------------------------------------------
    GOSUB INITIATE
    GOSUB PROCESS

    RETURN
*-----------------------------------------------------------
INITIATE:

    FN.CU = 'FBNK.CUSTOMER' ; F.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.CA = 'F.SCB.VIP.CUS' ; F.CP = ''
    CALL OPF(FN.CA,F.CA)

    FN.CP = 'F.SCB.CUS.POS.LW' ; F.CP = ''
    CALL OPF(FN.CP,F.CP)

    FN.COM = 'F.COMPANY' ; F.COM = ''
    CALL OPF(FN.COM,F.COM)

    FN.CCY = 'F.SBD.CURRENCY' ; F.CCY = ''
    CALL OPF(FN.CCY,F.CCY)


    WS.AMT = 0 ; WS.CATEG.LINE = ''

    KEY.LIST  ="" ; SELECTED  ="" ;  ER.MSG =""
    KEY.LIST1 ="" ; SELECTED1 ="" ;  ER.MSG1=""
    KEY.LIST2 ="" ; SELECTED2 ="" ;  ER.MSG2=""

    OPENSEQ "VIP.CUS" , "CUSTOMER.BALANCE.GE.1000000.CSV" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"VIP.CUS":' ':"CUSTOMER.BALANCE.GE.1000000.CSV"
        HUSH OFF
    END
    OPENSEQ "VIP.CUS" , "CUSTOMER.BALANCE.GE.1000000.CSV" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE CUSTOMER.BALANCE.GE.1000000.CSV CREATED IN VIP.CUS'
        END ELSE
            STOP 'Cannot create CUSTOMER.BALANCE.GE.1000000.CSV File IN VIP.CUS'
        END
    END

    DAT = TODAY

    TD = DAT
    DAT.HED = FMT(TD,"####/##/##")

    HEAD1 = "����� ������ ������� �������� ���� �� �����"
    HEAD.DESC = ",":HEAD1:" ":DAT.HED:","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    HEAD.DESC  = "��� ������":","
    HEAD.DESC := "��� ������":","
    HEAD.DESC := "������ ������� ����":","
    HEAD.DESC := "�����":","

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END

    RETURN
*------------------------------------------------------------
PROCESS:
**** SELECT VIP CATEGORY ****

    T.SEL1 = "SELECT ":FN.CA:" BY @ID"
    CALL EB.READLIST(T.SEL1,KEY.LIST1,"",SELECTED1,ER.MSG1)
    IF SELECTED1 THEN
        FOR K = 1 TO SELECTED1
            CALL F.READ(FN.CA,KEY.LIST1<K>,R.CA,F.CA,E3)
*Line [ 115 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.CATEG.COUNT = DCOUNT(R.CA<VIP.CATEGORY>,@VM)
            FOR X = 1 TO WS.CATEG.COUNT
                WS.CATEG1      = R.CA<VIP.CATEGORY><1,X>
                WS.CATEG.LINE := WS.CATEG1:' '
            NEXT X
        NEXT K
    END

    WS.CATEG = '(':WS.CATEG.LINE:')'

****** SELECT VIP CUSTOMER ******
    T.SEL = "SELECT ":FN.CP:" WITH CATEGORY IN ":WS.CATEG:" BY CUSTOMER"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.CP,KEY.LIST<I>,R.CP,F.CP,E1)
            CALL F.READ(FN.CP,KEY.LIST<I+1>,R.CP1,F.CP,E2)

            WS.CUST.ID  = R.CP<CUPOS.CUSTOMER>
            WS.CUST.ID1 = R.CP1<CUPOS.CUSTOMER>

            CALL F.READ(FN.CU,WS.CUST.ID,R.CU,F.CU,E5)

            WS.TARGET = R.CU<EB.CUS.TARGET>
            WS.SECTOR = R.CU<EB.CUS.SECTOR>

            IF WS.TARGET NE 5850 AND WS.TARGET NE 5860 AND WS.TARGET NE 5870 AND WS.SECTOR EQ 2000 THEN
                WS.AMT += R.CP<CUPOS.LCY.AMOUNT>
                IF WS.CUST.ID NE WS.CUST.ID1 THEN
                    WS.CUS.NAME = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                    WS.BRN.ID   = R.CU<EB.CUS.COMPANY.BOOK>

                    CALL F.READ(FN.COM,WS.BRN.ID,R.COM,F.COM,E3)
                    WS.BRN.NAME = R.COM<EB.COM.COMPANY.NAME,2>

                    WS.AMT = DROUND(WS.AMT,'0')
                    IF WS.AMT GE 1000000 THEN
                        BB.DATA  = WS.CUST.ID:','
                        BB.DATA := WS.CUS.NAME:','
                        BB.DATA := WS.AMT:','
                        BB.DATA := WS.BRN.NAME

                        WRITESEQ BB.DATA TO BB ELSE
                            PRINT " ERROR WRITE FILE "
                        END
                    END
                    WS.AMT = 0
                END
            END

        NEXT I
    END

    RETURN
*============================================================
