* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*** CREATED BY MAHMOUD MAGDY 2020 / 12 / 13

    PROGRAM SBD.DAILY.AML.BATCH

    $INSERT T24.BP  I_COMMON
    $INSERT T24.BP  I_EQUATE
**************************************
     EXECUTE "OFSADMINLOGIN"
**************************************
** FREQUENCY EQ D / M / Y / Q ...etc
    WS.D= 'D' ; WS.M = 'M' ; WS.Y = 'Y' ; WS.Q = 'Q'
** STAGE EQ B (Before) / A (After)
    WS.B = 'B' ; WS.A = 'A'
** TYPE EQ P (Program) / S ( Subroutine)
    WS.P = 'P' ; WS.S = 'S'
***------------------------------------------------------------------------------------****
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"GET.PROG.FILE")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"GET.PROG.FILE.2")
    CALL FSCB.RUN.JOB(WS.P,WS.D,WS.A,"GET.PROG.FILE.SAN")
***------------------------------------------------------------------------------------****
    RETURN
***------------------------------------------------------------------------------------****
END
