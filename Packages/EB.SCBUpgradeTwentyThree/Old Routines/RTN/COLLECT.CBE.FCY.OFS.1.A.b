* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE COLLECT.CBE.FCY.OFS.1.A
*PROGRAM COLLECT.CBE.FCY.OFS.1.A
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.REQ
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SUEZ.MECH.SAL
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*---------------------------------------
************ OPEN FILES ****************

    FN.FT = "FBNK.FUNDS.TRANSFER"  ; F.FT  = ""
    CALL OPF (FN.FT,F.FT)

    FN.FTN = "FBNK.FUNDS.TRANSFER$NAU"  ; F.FTN  = ""
    CALL OPF (FN.FTN,F.FTN)

    FN.AC = "FBNK.ACCOUNT"  ; F.AC  = ""
    CALL OPF (FN.AC,F.AC)

    FN.BR    = 'FBNK.BILL.REGISTER'  ; F.BR =''  ; R.BR = '' ; ERR2=''
    CALL OPF(FN.BR,F.BR)

    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "FUNDS.TRANSFER"
    SCB.VERSION = "MECH1"

    FOLDER.NAME = "&SAVEDLISTS&"
    FILE.NAME   = "FT.FCY1"

    OPENSEQ FOLDER.NAME , FILE.NAME TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':FOLDER.NAME:' ':FILE.NAME
        HUSH OFF
    END
    OPENSEQ FOLDER.NAME,FILE.NAME TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE ':FILE.NAME:' CREATED IN ':FOLDER.NAME
        END ELSE
            STOP 'Cannot create ':FILE.NAME:' File IN ':FOLDER.NAME
        END
    END

    WS.FILE.COMP = ID.COMPANY[2]
    COMP = ID.COMPANY

    WS.AMT = 0
    YTEXT = "Enter Today's Date:"
    CALL TXTINP(YTEXT, 8, 22, "8", "D")
    COLL.DATE = COMI
    YTEXT = "Enter Currecy:"
    CALL TXTINP(YTEXT, 8, 22, "3", "A")
    CURR1 = COMI
*--------------------------------------------------------------------

******* CHECK  *****
    T.SEL = "SELECT FBNK.FUNDS.TRANSFER$NAU WITH DEBIT.THEIR.REF EQ CBE.FCY AND PROCESSING.DATE EQ ":COLL.DATE:" AND DEBIT.CURRENCY EQ ":CURR1
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.FTN,KEY.LIST<I>,R.FTN,F.FTN,E4)
            FT.ID = KEY.LIST<I>
            GOSUB CREATE.OFS
        NEXT I
        TEXT = '�� �������' ; CALL REM
    END
    ELSE
        TEXT = '������� �� �� ���' ; CALL REM
    END

    RETURN
**********************************************
CREATE.OFS:
    SCB.OFS.HEADER = SCB.APPL: "," : SCB.VERSION : "/A/PROCESS,":",":FT.ID:","
    OFS.MESSAGE.DATA = ""
    SCB.OFS.MESSAGE = SCB.OFS.HEADER : OFS.MESSAGE.DATA

    BB.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.DATA TO BB ELSE
    END

    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)

    BB.DATA  = SCB.OFS.MESSAGE
    WRITESEQ BB.DATA TO BB ELSE
    END

    RETURN
************************************************************
END
