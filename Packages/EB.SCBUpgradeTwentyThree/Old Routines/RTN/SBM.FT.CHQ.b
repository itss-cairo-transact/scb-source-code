* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* CREATED BY REHAM YOUSSEF 2021/11/10
*-----------------------------------------------------------------------------
* PROGRAM SBM.FT.CHQ
    SUBROUTINE  SBM.FT.CHQ
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.TXN.TYPE.CONDITION
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.FT.INT
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIATE
    GOSUB MAT
    RETURN
**************************************
INITIATE:
    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    ST.DATE = COMI
    PRINT  "ST.DATE=":ST.DATE

    YTEXT = "���� ����� ����� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    EN.DATE = COMI
    PRINT "EN.DATE=":EN.DATE


    YTEXT = "���� ������ : "
    CALL TXTINP(YTEXT, 8, 22, "12", "A")
    AMT = COMI
    PRINT "AMOUNT=":AMT

    FN.CUS   = 'FBNK.CUSTOMER'    ; F.CUS   = '' ; R.CUS  = ''
    CALL OPF(FN.CUS,F.CUS)

    FN.ACC   = 'FBNK.ACCOUNT'    ; F.ACC   = '' ; R.ACC  = ''
    CALL OPF(FN.ACC,F.ACC)


    FN.FT   = 'FBNK.FUNDS.TRANSFER$HIS'    ; F.FT  = '' ; R.FT  = ''
    CALL OPF(FN.FT,F.FT)

    KEY.LIST= ""   ; SELECTED  = "" ; ER.MSG = ""
    KEY.LIST2= ""  ; SELECTED2 = "" ; ER.MSG2 = ""
    DEB.AMT = ""    ; SERIAL1 = 0 ; SERIAL2 = 0
    SERIAL3 = 0 ;SERIAL4=0
    OPENSEQ "&SAVEDLISTS&" , "SBM.FT.CHQ.TXT" TO BB THEN
        CLOSESEQ BB
        HUSH ON
        EXECUTE 'DELETE ':"&SAVEDLISTS&":' ':"SBM.FT.CHQ.TXT"
        HUSH OFF
    END
    OPENSEQ "&SAVEDLISTS&" , "SBM.FT.CHQ.TXT" TO BB ELSE
        CREATE BB THEN
            PRINT 'FILE SBM.FT.CHQ.TXT CREATED IN &SAVEDLISTS&'
        END ELSE
            STOP 'Cannot create SBM.FT.CHQ.TXT File IN &SAVEDLISTS&'
        END
    END

    HEAD.DESC  = "ID.NO.":"|"
    HEAD.DESC := "Branch.":"|"
    HEAD.DESC := "Cust Name.":"|"
    HEAD.DESC := "Cust.Acct":"|"
    HEAD.DESC := "Purpose for Transfer.":"|"
    HEAD.DESC := "Currency.":"|"
    HEAD.DESC := "Amount.":"|"
    HEAD.DESC := "Amt.Local":"|"
    HEAD.DESC := "Processing date .":"|"
    HEAD.DESC := "Transaction.type .":"|"
    HEAD.DESC := "BEN.CUST.":"|"
    HEAD.DESC := "CHEQUE.NO."

    BB.DATA = HEAD.DESC
    WRITESEQ BB.DATA TO BB ELSE
        PRINT " ERROR WRITE FILE "
    END


    RETURN
*************************DEBIT *******************************
MAT:
****
  T.SEL = "SELECT FBNK.FUNDS.TRANSFER$HIS WITH (TRANSACTION.TYPE EQ AC08 OR TRANSACTION.TYPE EQ AC09 OR TRANSACTION.TYPE EQ AC11) AND (DEBIT.VALUE.DATE GE ":ST.DATE :" AND DEBIT.VALUE.DATE LE ":EN.DATE :") AND LOC.AMT.CREDITED GE ":AMT:" AND DEBIT.ACCT.NO UNLIKE 994... BY TRANSACTION.TYPE BY DEBIT.CURRENCY"
*    T.SEL = "GET-LIST FT.CHQ"


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    PRINT "SEL=":SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED

            CALL F.READ(FN.FT,KEY.LIST<I>,R.FT,F.FT,ETEXT)
            ID.NO     = KEY.LIST<I>
            DR.ACCT    = R.FT<FT.DEBIT.ACCT.NO>
            LOC.REF = R.FT<FT.LOCAL.REF>
            CHEQ.NO      = LOC.REF<1,FTLR.CHEQUE.NO>
            CALL F.READ(FN.ACC,DR.ACCT,R.ACC,F.ACC,ETEXT1)
            CATEG  = R.ACC<AC.CATEGORY>
            IF CATEG NE '1002'  THEN

                DR.CUS    = R.FT<FT.DEBIT.CUSTOMER>
                CALL F.READ(FN.CUS,DR.CUS,R.CUS,F.CUS,ETEXT)
                LOCAL.REF  = R.CUS<EB.CUS.LOCAL.REF>
                NAME       = LOCAL.REF<1,CULR.ARABIC.NAME>
                SEC        = R.CUS<EB.CUS.SECTOR>
                IF SEC EQ '1100' OR SEC EQ '1300' THEN
                    CO.CODE    = R.FT<FT.CO.CODE>
                    CALL DBR('COMPANY':@FM:EB.COM.COMPANY.NAME,CO.CODE,BRANCH.NAME)
                    BEN.CUS    = R.FT<FT.BEN.CUSTOMER>
                    PURPOSE    = R.FT<FT.PAYMENT.DETAILS>[1,35]
                    DR.ACCT    = R.FT<FT.DEBIT.ACCT.NO>
                    DR.CUR     = R.FT<FT.DEBIT.CURRENCY>
                    DR.AMT     = R.FT<FT.DEBIT.AMOUNT>
                    DR.LOC.AMT = R.FT<FT.LOC.AMT.DEBITED>
                    DR.DATE    = R.FT<FT.PROCESSING.DATE>
                    TRN.TYPE   = R.FT<FT.TRANSACTION.TYPE>
                    CALL DBR('FT.TXN.TYPE.CONDITION':@FM:FT6.DESCRIPTION,TRN.TYPE,TRN.NAME)
                    BB.DATA  = ID.NO:'|'
                    BB.DATA := BRANCH.NAME:'|'
                    BB.DATA := NAME:'|'
                    BB.DATA := DR.ACCT:'|'
                    BB.DATA := PURPOSE:'|'
                    BB.DATA := DR.CUR:'|'
                    BB.DATA := DR.AMT:'|'
                    BB.DATA := DR.LOC.AMT:'|'
                    BB.DATA := DR.DATE:'|'
                    BB.DATA := TRN.NAME:'|'
                    BB.DATA := BEN.CUS:'|'
                    BB.DATA := CHEQ.NO:'|'
                    WRITESEQ BB.DATA TO BB ELSE
                        PRINT " ERROR WRITE FILE "
                    END
                END
            END
        NEXT I
    END
    RETURN
***************************************************************************
END
