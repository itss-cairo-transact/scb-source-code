* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
****************
* WAGDY MOUNIR *
****************
    SUBROUTINE SCB.CONV.MONS

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.LOANS.GOV

    ALL.F =  O.DATA
    L.NO = O.DATA[1,3]
    CUS = O.DATA[4,8]

*    TEXT = "LOAN = ": L.NO : " CUS = ":CUS ; CALL REM
    COMP =  ID.COMPANY
    EMP.CON = 0

    TAB   = 'F.SCB.LOANS.GOV'
    F.CF  = ''
    ETEXT = ''

    OLD.DAT = ""
    NEW.DAT = ""

    CALL OPF(TAB,F.CF)
    T.SEL = "SELECT F.SCB.LOANS.GOV WITH PAY.CODE EQ 13 AND LOAN.NO EQ ":L.NO:" AND CUSTOMER.NO EQ ":CUS:" AND CO.CODE EQ ":COMP: "  BY INSTALLEMENT.DATE"

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ( TAB,KEY.LIST<I>, R.CF, F.CF, ETEXT)
            NEW.DAT = R.CF<GOV.INSTALLEMENT.DATE>

            IF NEW.DAT NE OLD.DAT THEN
                EMP.CON = EMP.CON + 1
            END

            OLD.DAT = NEW.DAT
        NEXT I
    END
    O.DATA = EMP.CON

    RETURN
END
