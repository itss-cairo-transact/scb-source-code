* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>-18</Rating>
*-----------------------------------------------------------------------------
**** CREATED BY MOHAMED SABRY 2011/07/14 ****
*********************************************

    PROGRAM SBR.SW.COMPANY.USER

*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER.ABBREVIATION
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_OFS.SOURCE.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON

    GOSUB INITIALISE
    GOSUB BUILD.RECORD

    RETURN

INITIALISE:
*----------
    SCB.OFS.SOURCE = "TESTOFS"
    SCB.APPL = "USER"
    SCB.VERSION = "COMPANY"
    SCB.OFS.HEADER = SCB.APPL : "," : SCB.VERSION

    RETURN

BUILD.RECORD:

    COMMA = ","
    KEY.LIST=""  ; SELECTED=""  ;  ER.MSG=""
    FN.UA  = 'F.USER' ; F.UA = '' ; R.UA=''
    CALL OPF(FN.UA,F.UA)
****UPDATED BY NESSREEN AHMED 5/11/2015*********************
****  T.SEL = "SELECT ":FN.UA:" WITH DEPARTMENT.CODE EQ 99 "
    T.SEL = "SELECT ":FN.UA:" WITH (DEPARTMENT.CODE EQ 99 OR DEPARTMENT.CODE EQ 88 ) "
****END OF UPDATE 5/11/2015**********************************
*    T.SEL = "SELECT ":FN.UA:" WITH @ID EQ 'AUTHORISER'  "
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG1)
    PRINT 'SELECTED = ':SELECTED
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.UA,KEY.LIST<I>,R.UA,F.UA,E1)
            WS.USER.ID = KEY.LIST<I>
            WS.USER.INT = FIELD((R.UA<EB.USE.INIT.APPLICATION>),"?",2)

* IF  WS.USER.INT EQ 300 THEN
*     WS.HMM.NO   =  499
*     GOSUB REPLACE.HMM
* END
            WS.OTH.BOOK    = R.UA<EB.USE.OTH.BOOK.ACCESS>
            WS.COMPANY.CO  = R.UA<EB.USE.COMPANY.CODE>
*Line [ 80 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            WS.TEXT.UA.NO = DCOUNT(WS.COMPANY.CO,@VM)
            IF WS.TEXT.UA.NO GT 10 THEN
                PRINT WS.TEXT.UA.NO
                GOSUB EX.OVER.GROUP


*NEXT I
                IF OFS.MESSAGE.DATA NE '' THEN
                    PRINT  OFS.MESSAGE.DATA
                    SCB.OFS.MESSAGE = SCB.OFS.HEADER : "/I/PROCESS,INPUTT01//EG0010001," : WS.USER.ID : OFS.MESSAGE.DATA
                    PRINT SCB.OFS.MESSAGE
*                    CALL OFS.GLOBUS.MANAGER(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                    CALL SCB.OFS.ONLINE(SCB.OFS.SOURCE, SCB.OFS.MESSAGE)
                    PRINT SCB.OFS.MESSAGE
                END
                OFS.MESSAGE.DATA = ''
                WS.USER.ID = ''
            END
        NEXT I
    END
    RETURN
**************************************************************
REPLACE.HMM:
*    OFS.MESSAGE.DATA  =  ",INIT.APPLICATION:":X:":1=":WS.HMM.NO
*    OFS.MESSAGE.DATA  =  ",INIT.APPLICATION:1:1=":WS.HMM.NO
    RETURN
**************************************************************
EX.OVER.GROUP:
    WS.TEXT.UA.NO ++
*****UPDATED BY NESSREEN AHMED 5/11/2015***************************

    OFS.MESSAGE.DATA  :=  ",COMPANY.CODE:":WS.TEXT.UA.NO:":1=":'EG0010037'

*****END OF UPDATE ***********************************************
*    OFS.MESSAGE.DATA  :=  ",ABBREVIATION:":WS.TEXT.UA.NO:":1=":WS.HMM.SUB
*    OFS.MESSAGE.DATA  :=  ",ORIGINAL.TEXT:":WS.TEXT.UA.NO:":1=":"?":WS.HMM.SUB
    WS.HMM.SUB = ''
    RETURN
