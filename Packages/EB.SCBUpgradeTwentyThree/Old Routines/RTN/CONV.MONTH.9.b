* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>-10</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.MONTH.9

    $INSERT T24.BP I_COMMON
    $INSERT T24.BP I_EQUATE
    $INSERT T24.BP I_ENQUIRY.COMMON
    $INSERT T24.BP I_F.USER
    $INSERT TEMENOS.BP I_F.SCB.PASSWORD.UPD
*----------------------------------------------------------
    FN.PASS  = 'F.SCB.PASSWORD.UPD'  ; F.PASS = ''
    CALL OPF(FN.PASS,F.PASS)

    GETT.IDD  = O.DATA
    USER.ID   = FIELD(GETT.IDD,'*',1)
    COMP.ID   = FIELD(GETT.IDD,'*',2)
    SEL.COUNT = 0
*-----------------------------------------------
    DATE.TOD  = TODAY
    CALL ADD.MONTHS(DATE.TOD, '-4')

    FROM.DATE = DATE.TOD[1,6] : "01"
    END.DATE  = FROM.DATE
    CALL LAST.DAY(END.DATE)

    T.SEL  = "SELECT ":FN.PASS :" WITH USER.ID EQ ":USER.ID
    T.SEL := " AND BOOKING.DATE GE " : FROM.DATE
    T.SEL := " AND BOOKING.DATE LE " : END.DATE
    T.SEL := " AND COMPANY.ID EQ "   : COMP.ID
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    IF SELECTED THEN
        FOR II = 1 TO SELECTED
            CALL F.READ(FN.PASS,KEY.LIST<II>,R.PASS,F.PASS,ER.PASS)
            CURR.NO    = R.PASS<PWR.CURR.NO>
            SEL.COUNT += CURR.NO
        NEXT II
    END

    O.DATA = SEL.COUNT:"*":FROM.DATE[1,4]:"/":FROM.DATE[5,2]
    RETURN
END
