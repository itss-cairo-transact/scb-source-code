* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>729</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VISA.DEBIT.CUST
 
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.OFS.SOURCE
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 39 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.APP
*Line [ 41 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.TRANS
*Line [ 43 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.VISA.CODES
*Line [ 45 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

* TO DEBIT THE TOTAL USAGES FROM CUSTOMERS VISA ACCOUNT AND PRINTOUT A REPORT CONTANING
* THE CUSTOMERS AND TOTAL AMOUNT TO BE DEBITED


    GOSUB INITIATE
    GOSUB PRINT.HEAD
    GOSUB CALLDB
    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')
    RETURN
*==============================================================
INITIATE:
    REPORT.ID='VISA.DEBIT.CUST'
    CALL PRINTER.ON(REPORT.ID,'')
    RETURN
*===============================================================
CALLDB:

    F.VISA.TRANS = '' ; FN.VISA.TRANS = 'F.SCB.VISA.TRANS' ; R.VISA.TRANS = '' ; E1 = '' ; RETRY1 = ''
    CALL OPF(FN.VISA.TRANS,F.VISA.TRANS)

    T.SEL = "SELECT F.SCB.VISA.TRANS WITH @ID LIKE ...":TODAY : " BY BRANCH.NUMBER BY CUST.NO"
    KEY.LIST ="" ; SELECTED="" ;  ER.MSG=""
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
*================================================================
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
****************************************
            CALL F.READU(FN.VISA.TRANS,KEY.LIST<I>,R.VISA.TRANS,F.VISA.TRANS,E2,RETRY2)
            DEP.CODE=R.VISA.TRANS<SCB.TRANS.BRANCH.NUMBER>
            IF DEP.CODE # TEMP.CODE THEN
                TEMP.CODE = DEP.CODE

                CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,DEP.CODE,DEP.CODEE)
                DEP.CODEE=FIELD(DEP.CODEE,'.',2)
                XX<1,ZZ>="_________________":"   ���    ": DEP.CODEE:"   _____________________"
                PRINT XX<1,ZZ>
                ZZ=ZZ+1
            END
*****************************************
            XX=''
            TOT.USE='';TOT.INT='';TOT.COMM='';TOT.FEE='';TOT.ACT='';TOT.REV=''
            VISA.NO = KEY.LIST<I>[1,16]
            CUST.NO = R.VISA.TRANS<SCB.TRANS.CUST.NO>
            TEXT = 'CUS.NO=':CUST.NO ; CALL REM
            CALL DBR('CUSTOMER':@FM:EB.CUS.LOCAL.REF,CUST.NO,LOC.REF)
            CUST.NAME=LOC.REF<1,CULR.ARABIC.NAME>
            TEXT = 'CUST.NAME=':CUST.NAME ; CALL REM
            ACCT.NO = R.VISA.TRANS<SCB.TRANS.CUST.ACCT>
            TRNS.CODE=R.VISA.TRANS<SCB.TRANS.TRANS.CODE>
*Line [ 98 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
            TRNS.COUNT=DCOUNT(TRNS.CODE,@VM)
            FOR J= 1 TO TRNS.COUNT
                T.CODE=TRNS.CODE<1,J>
                IF T.CODE EQ '1' THEN
                    TOT.USE=TOT.USE+R.VISA.TRANS<SCB.TRANS.TRANS.AMT><1,J>
                END
                IF T.CODE EQ '2' THEN
                    TOT.INT=TOT.INT+R.VISA.TRANS<SCB.TRANS.TRANS.AMT><1,J>
                END
                IF T.CODE EQ '3' THEN
                    TOT.COMM=TOT.COMM+R.VISA.TRANS<SCB.TRANS.TRANS.AMT><1,J>
                END

                IF T.CODE EQ '4' THEN
                    TOT.FEE=TOT.FEE+R.VISA.TRANS<SCB.TRANS.TRANS.AMT><1,J>
                END

                IF T.CODE EQ '5' THEN
                    TOT.ACT=TOT.ACT+R.VISA.TRANS<SCB.TRANS.TRANS.AMT><1,J>

                END
                IF T.CODE EQ '6' THEN
                    TOT.REV=TOT.REV+R.VISA.TRANS<SCB.TRANS.TRANS.AMT><1,J>
                END
            NEXT J

            XX<1,ZZ>[1,30]= CUST.NAME
           * TEXT = 'CUST=':CUST.NAME ; CALL REM
            XX<1,ZZ>[50,16]= VISA.NO
           * TEXT = 'VISA=' :VISA.NO ; CALL REM
            XX<1,ZZ>[80,16]= ACCT.NO
            TOT.ALL=TOT.USE+TOT.INT+TOT.COMM+TOT.FEE-TOT.ACT-TOT.REV
           * TEXT = 'TOT=':TOT.ALL ; CALL REM
            XX<1,ZZ>[110,15]=TOT.ALL

            PRINT XX<1,ZZ>
*******NESRO*************************************************************
            DIM R.FT(FT.AUDIT.DATE.TIME)
            MAT R.FT = ""
            ER.MSG = ""
            ID.FT = ""
            W.STATUS = ""
           *** FT.LOCAL.REF
           *** FTLR.VISA.NO

            CURR = R.VISA.TRANS<SCB.TRANS.TRANS.CURR><1,J>
           * TEXT = 'CURR=':CURR ; CALL REM

            IF LEN(DEP.CODE) < 2 THEN
               BR = '0':DEP.CODE
           *    TEXT = 'BR=':BR ; CALL REM
            END ELSE
               BR = DEP.CODE
           *    TEXT = 'BR=' :BR ; CALL REM
            END
            ACC.CURR = ACCT.NO[9,2]
           * TEXT = 'ACC.CURR' ; CALL REM
            ACC.CATEG = ACCT.NO[11,4]
           * TEXT = 'ACC.CATE=':ACC.CATEG ; CALL REM
            BR.ACC = '914999':BR:ACC.CURR:ACC.CATEG:'01'
           * TEXT = 'BR.ACC=':BR.ACC ; CALL REM

            R.FT(FT.TRANSACTION.TYPE) = "ACVU"
            R.FT(FT.DEBIT.CUSTOMER)= CUST.NO
            R.FT(FT.DEBIT.CURRENCY) = CURR
            R.FT(FT.DEBIT.ACCT.NO) = ACCT.NO
            R.FT(FT.DEBIT.AMOUNT) = TOT.ALL
            R.FT(FT.DEBIT.VALUE.DATE) = TODAY
            R.FT(FT.CREDIT.CURRENCY) = CURR
            R.FT(FT.CREDIT.ACCT.NO) = BR.ACC
            R.FT(FT.CREDIT.VALUE.DATE) = TODAY
            R.FT(FT.COMMISSION.CODE)= "W"
            R.FT(FT.CHARGE.CODE)= "W"
            R.FT(FT.AUTHORISER) = TNO:'_':OPERATOR

        CALL INFORMER.CREATE.FT(MAT R.FT,"Y","","","",ID.FT,ER.MSG,W.STATUS)
        IF W.STATUS # "L" THEN
            IF NOT(ER.MSG) THEN
                ER.MSG = "ERROR CREATING FT"
            END
        END
        IF ER.MSG THEN
            TEXT = ER.MSG
            CALL FATAL.ERROR("VISA.DEBIT.CUST")
        END

*******END NESSRO*********************************************************
            ZZ=ZZ+1
************************************************************************************

**************************************************************************
        NEXT I
    END
    RETURN
*===============================================================
PRINT.HEAD:
    CALL DBR('DEPT.ACCT.OFFICER':@FM:EB.DAO.NAME,R.USER<EB.USE.DEPARTMENT.CODE>,BRANCH)
    YYBRN = FIELD(BRANCH,'.',2)
    DATY = TODAY
    T.DAY = DATY[7,2]:'/':DATY[5,2]:"/":DATY[1,4]
    PR.HD ="'L'":SPACE(1):" ��� ���� ������"  : SPACE(95):"��� :" :YYBRN
    PR.HD :="'L'":SPACE(1):" ������� : ":T.DAY:SPACE(90):"��� ������ : ":"'P'"
    PR.HD :="'L'":" "
    PR.HD :="'L'":SPACE(40):"����� ������ ��� ��������� ������ ��� ���� ������"
    PR.HD :="'L'":SPACE(38):STR('_',50)
    PR.HD :="'L'":" "
    PR.HD :="'L'":" "

    PR.HD :="'L'":SPACE(1):"����� ��������":SPACE(35):"��� ������":SPACE(20):"��� ���� �����":SPACE(12):"����������"
    PR.HD :="'L'":SPACE(1):STR('_',14):SPACE(33):STR('_',12):SPACE(18):STR('_',20):SPACE(10):STR('_',8)
    HEADING PR.HD
    RETURN
*==============================================================

END
