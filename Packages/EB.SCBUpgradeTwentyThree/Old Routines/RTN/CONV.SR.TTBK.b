* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>-16</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE CONV.SR.TTBK

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.SR.SCRIPT
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FT.BULK.CREDIT.AC


   * ID.NO=FIELD(O.DATA,".",1)
   * ID.NO=ID.NO:'...'
    ID.NO = R.NEW(SR.SCRIPT.TT.REFERENCE)
    TEXT = "ID.NO":ID.NO ; CALL REM

    T.SEL= "SELECT F.SCB.SR.SCRIPT WITH ":ID.NO:" LIKE 'TT...'"

*T.SEL    = "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK EQ ": O.DATA
*T.SEL    = "SELECT FBNK.FUNDS.TRANSFER WITH ORDERING.BANK LIKE ": O.DATA :"..."

    KEY.LIST = ""
    SELECTED = ""
    ER.MSG   = ""

    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

   * FN.FUNDS ='FBNK.FUNDS.TRANSFER' ;R.FUNDS='';F.FUNDS=''
   * CALL OPF(FN.FUNDS,F.FUNDS)


  *  FOR I = 1 TO SELECTED


  *  NEXT I


    RETURN
END
