* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE VVR.FT.CR.CERT.DR.W.FCY

** TO PL CREATE ACCOUNT MASK ACCORDING TO CURRENCY

*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_FT.LOCAL.REFS

    IF MESSAGE NE 'VAL' THEN
        IF V$FUNCTION = "I" THEN
            IF COMI THEN
                CUR1   = R.NEW(FT.LOCAL.REF)<1,FTLR.CURRENCY>
                CALL DBR('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR1,CUR)
                BRN.NO = R.NEW(FT.LOCAL.REF)<1,FTLR.TELLER.REF>
                CURR   = R.NEW(FT.DEBIT.CURRENCY)
********UPDATED BY NESSREEN ON 07/07/2008************************
**  R.NEW(FT.DEBIT.ACCT.NO) = COMI:'1615200010001'
*--- EDIT BY NESSMA -- *
* CO.CODE     = R.USER<EB.USE.COMPANY.CODE,1>[6,4]
                CO.CODE = ID.COMPANY[2]
                R.NEW(FT.DEBIT.ACCT.NO)= COMI:'161520001':CO.CODE

                CR = COMI
                R.NEW(FT.CREDIT.CURRENCY) = CR
                CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CR,CRNO)

                CO     = ID.COMPANY
                CO.NO  = CO[8,2]
                ACC.NO = "99433300": CRNO :"507199"
                R.NEW(FT.CREDIT.ACCT.NO) = ACC.NO
                CALL REBUILD.SCREEN

* CALL REBUILD.SCREEN
            END
        END
    END
    RETURN
END
