* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>77</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VVR.BR.DUBLICATE.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
    $INSERT            I_BR.LOCAL.REFS
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH.FCY
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK

    BB=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR>
**************************NI7OOOOOOOOOO***************
    FN.BANK = 'F.SCB.BANK.BRANCH'  ;  F.BANK  = ''
    CALL OPF (FN.BANK,F.BANK)

    BB=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR>
    VV=R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>

    CALL F.READ(FN.BANK,COMI,R.BANK,F.BANK,E1)

    BANKNO =  R.BANK<SCB.BAB.BANK.NO>
***-------------HYTHAM--------------20111221------------
    IF R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP' THEN
***-------------HYTHAM--------------20111221------------
        IF BANKNO NE VV THEN
            ETEXT = "��� �� ���� ��� �� ���� �����" ; CALL STORE.END.ERROR
        END
    END
*********************************************************************

    VV = COMI[5,2]
    IF R.NEW(EB.BILL.REG.CURRENCY) EQ 'EGP' THEN
        IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK> EQ 0017 AND COMI[1,4] EQ '1700' THEN
            FN.ACC = 'FBNK.ACCOUNT';F.ACC='';R.ACC = '';E1=''
            CALL OPF(FN.ACC,F.ACC)
            ACCC = "EGP12007000100":VV
            CALL F.READ(FN.ACC,ACCC, R.ACC, F.ACC ,E1)
            IF E1 THEN
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = 994999 : VV : 10 : 501001
            END ELSE
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = "EGP12007000100":VV
            END
        END ELSE
            BNKK = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>
            CUR      = R.NEW(EB.BILL.REG.CURRENCY)
            CALL DBR ('CURRENCY':@FM:EB.CUR.NUMERIC.CCY.CODE,CUR,CUR1)
* IF ( BNKK EQ '9901'  OR BNKK EQ '9902' ) THEN
            IF ( R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.IN.OUT.BILL> EQ 'NO' ) THEN
*R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '9943330010508001'
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT>  = '99433300':CUR1:'507099'
            END ELSE
*R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '99433300105085':ID.COMPANY[8,2]
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.CUST.ACCT> = '99433300':CUR1:'5075':ID.COMPANY[8,2]
            END

        END
    END
    IF MESSAGE NE "VAL" THEN

        GOSUB DUBLICATE.CHQ.NAU
        IF FLAG NE 1 THEN GOSUB GET.PAY.PLACE
        RETURN
*---------------------------------
DUBLICATE.CHQ.NAU:
        IF COMI THEN

            T.SEL1= '';KEY.LIST1 = '';SELECTED1 = '';ERR.MSG1 = ''

            CHQ.NO    = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BILL.CHQ.NO>
            BANK.NO   = R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK>
            BRANCH.NO = COMI

            T.SEL = "SSELECT FBNK.BILL.REGISTER$NAU WITH BILL.CHQ.NO EQ ": CHQ.NO :" AND BANK EQ ": BANK.NO :" AND BANK.BR EQ ": BRANCH.NO
            CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ERR.MSG1 )
            IF SELECTED THEN
                FLAG = 1
                ETEXT = "����� ���� �� ��� � ��� ����" ; CALL STORE.END.ERROR
            END
        END
        RETURN
*-------------------------------
GET.PAY.PLACE:

        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = ''

        FN.BRANCH = 'F.SCB.BANK.BRANCH';F.BRANCH='';R.BRANCH = '';E=''
        CALL OPF(FN.BRANCH,F.BRANCH)
        FN.BRANCH2 = 'F.SCB.BANK.BRANCH.FCY';F.BRANCH2 ='';R.BRANCH2 = '';E2 =''
        CALL OPF(FN.BRANCH2,F.BRANCH2)

        CALL F.READ(FN.BRANCH2,BANK.NO, R.BRANCH2, F.BRANCH2 ,E2)
        CALL F.READ(FN.BRANCH,COMI, R.BRANCH, F.BRANCH ,E)

        IF NOT (E2) THEN
            R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = 1
        END ELSE
            IF (E)  THEN
                ETEXT="�� ���� ���� ����� ��" ;RETURN
            END ELSE
*R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = R.BRANCH<SCB.BAB.LOCATION>
                R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.PAY.PLACE> = 2
            END
        END
        CALL REBUILD.SCREEN
    END
*********************************
****************************NI7OOOOOOOOOOOO****************8
    IF R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.BANK.BR> # COMI THEN
*  R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.RECEIVE.DATE> = ''
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MATURITY.EXT> = ''
* R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.STATUS.DATE>  = ''
        R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.COLL.DATE>    = ''
* R.NEW(EB.BILL.REG.LOCAL.REF)<1,BRLR.MAT.DATE>     = ''
        CALL REBUILD.SCREEN
    END
***********************************************************

*  END

    RETURN
*--------------------------------
END
