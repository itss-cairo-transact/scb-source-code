* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE VNC.LD.SCH.AUTO.DEFULT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.LOANS.AND.DEPOSITS
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LD.SCHEDULE.DEFINE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------
    WS.MAT.DATE = LINK.DATA<9,7>
    WS.VALUE.DATE = LINK.DATA<9,6>
    WS.INT.RATE = LINK.DATA<9,16> / 100
    WS.INST = WS.VALUE.DATE
    CALL ADD.MONTHS(WS.INST,'+1')
    WS.AMT      = LINK.DATA<9,4>
    WS.NO.YEAR = WS.MAT.DATE[1,4] - WS.VALUE.DATE[1,4]
    WS.NO.MONTHS = 0
    CALL MONTHS.BETWEEN(WS.MAT.DATE,WS.VALUE.DATE,WS.NO.MONTHS)

    IF V$FUNCTION EQ 'I' THEN
*        R.NEW(LD.SD.FORWARD.BACKWARD) = 5
*        R.NEW(LD.SD.BASE.DATE.KEY)    = 1

        R.NEW(LD.SD.SCH.TYPE)<1,1> = 'P'
        R.NEW(LD.SD.DATE)<1,1>     = WS.INST
        R.NEW(LD.SD.AMOUNT)<1,1>   = DROUND(WS.AMT / WS.NO.MONTHS,2)
        R.NEW(LD.SD.FREQUENCY)<1,1> = "1M"

        R.NEW(LD.SD.SCH.TYPE)<1,2> = 'I'
        R.NEW(LD.SD.DATE)<1,2>     = WS.INST
        R.NEW(LD.SD.AMOUNT)<1,2>   = DROUND(WS.AMT * WS.INT.RATE * WS.NO.YEAR / WS.NO.MONTHS,2)
        R.NEW(LD.SD.FREQUENCY)<1,2> = "1M"

    END

    RETURN
END
