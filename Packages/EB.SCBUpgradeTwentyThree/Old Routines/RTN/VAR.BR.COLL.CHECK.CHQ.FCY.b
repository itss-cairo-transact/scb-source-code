* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE VAR.BR.COLL.CHECK.CHQ.FCY

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.TELLER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BT.BATCH.FCY

    IF V$FUNCTION = 'A' THEN
        FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BR = ''
        CALL OPF( FN.BR,F.BR)

*Line [ 35 ] Add @VM Instead Of VM - ITSS - R21 Upgrade - 2021-12-26
        YY = DCOUNT(R.NEW(SCB.BT.OUR.REFERENCE),@VM)
        FOR H = 1 TO YY
            BR.ID = R.NEW(SCB.BT.OUR.REFERENCE)<1,H>
            CALL F.READ(FN.BR,BR.ID,R.BR,F.BR,E11)
            RET.REASON = R.NEW(SCB.BT.RETURN.REASON)<1,AV>

            BEGIN CASE
            CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> = 8
                E = "�����"  ; CALL ERR  ; MESSAGE = 'REPEAT'
            *CASE R.BR<EB.BILL.REG.CURRENCY> # "EGP"
             CASE R.BR<EB.BILL.REG.CURRENCY> # "USD"
                *ETEXT = "��� �� ���� ������ ����"
                 ETEXT = "��� �� ���� ������ �����"

            CASE  R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> = 7
                E = "�����"  ; CALL ERR  ; MESSAGE = 'REPEAT'
            CASE R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 2 OR R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 6
                E = "��� ������� �� ����������"  ; CALL ERR  ; MESSAGE = 'REPEAT'
            CASE ( R.BR<EB.BILL.REG.LOCAL.REF,BRLR.CUST.ACCT> EQ ''  AND RET.REASON EQ '' )
                E = "CHECK CUST.ACCT"  ; CALL ERR  ; MESSAGE = 'REPEAT'
            CASE NOT(R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 1 OR R.BR<EB.BILL.REG.LOCAL.REF,BRLR.BILL.CHQ.STA> EQ 5)
                E = "��� �� ���� ����� ����� �������"  ; CALL ERR  ; MESSAGE = 'REPEAT'
            CASE OTHERWISE
                RETURN
            END CASE
        NEXT H

    END
    RETURN
END
