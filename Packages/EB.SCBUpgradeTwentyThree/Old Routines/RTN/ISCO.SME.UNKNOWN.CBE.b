* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    PROGRAM ISCO.SME.UNKNOWN.CBE
*========================================================
*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.LIMIT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.RE.BASE.CCY.PARAM
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_AC.LOCAL.REFS
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CU.LOCAL.REFS
*Line [ 38 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.UNKNOWN.CASES
*Line [ 40 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CF
*Line [ 42 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.ISCO.SME.CS
*Line [ 44 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.CREDIT.CBE
*========================================================
    FN.UN   = 'F.SCB.ISCO.UNKNOWN.CASES'
    F.UN    = ''
    CALL OPF(FN.UN,F.UN)

    FN.CF   = 'F.SCB.ISCO.SME.CF'
    FVAR.CF= "" ; F.CF    = ''
    CALL OPF(FN.CF,F.CF)

    FN.CS = "F.SCB.ISCO.SME.CS"
    FVAR.CS=''; R.CS = ''; F.CS = ''
    CALL OPF(FN.CS,F.CS)

    FN.CBE  = 'F.SCB.CREDIT.CBE'
    F.CBE = ''     ; R.CBE = ''
    CALL OPF( FN.CBE ,F.CBE)

    FN.CU = 'FBNK.CUSTOMER'
    F.CU  = ''     ; R.CU = ''
    CALL OPF(FN.CU,F.CU)

    FN.LMT  = 'FBNK.LIMIT'
    F.LMT = ''     ; R.LMT = ''
    CALL OPF( FN.LMT ,F.LMT)

    FN.BASE = 'FBNK.RE.BASE.CCY.PARAM'
    F.BASE = ''
    R.BASE = ''
    CALL OPF(FN.BASE,F.BASE)

    CALL F.READ(FN.BASE,'NZD',R.BASE,F.BASE,E3)
    CURR.BASE = R.BASE<RE.BCP.ORIGINAL.CCY>

    OPEN FN.CF TO FVAR.CF ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END
    OPEN FN.CS TO FVAR.CS ELSE
        TEXT = "ERROR OPEN FILE" ; CALL REM
        RETURN
    END

    SELECTED = ''; SELECTED2 = ''; SELECTED3 = ''; ASD = ''
    R.FILL.CF    = ''; R.FILL.CS    = ''

    COMP   = ID.COMPANY
    T.DATE = TODAY
*============
    *T.DATE = "20200510"
*============
    MONTH  = T.DATE[5,2]
    MONTH.B4   = MONTH
    YEAR   = T.DATE[1,4]
    YEAR.B4 = YEAR
    IF MONTH EQ 1 THEN

        NEW.MONTH     = 12
        NEW.MONTH.B4  = 11
        YEAR          = YEAR - 1
        YEAR.B4       = YEAR.B4 - 1
    END
    ELSE
        NEW.MONTH     = MONTH - 1
        NEW.MONTH.B4  = MONTH.B4 - 2
    END
    IF LEN(NEW.MONTH) EQ 1 THEN
        NEW.MONTH = '0':NEW.MONTH
    END
    IF LEN(NEW.MONTH.B4) EQ 1 THEN
        NEW.MONTH.B4 = '0':NEW.MONTH.B4
    END

    CLOSED.ACCT.DATE = YEAR:NEW.MONTH.B4
    FINAL.DATE = YEAR: NEW.MONTH

*================= LAST WORKING DATE ===================================
    LST.WRK.D = ''
*CALL DBR('DATES':@FM:EB.DAT.LAST.WORKING.DAY,"EG0010099",LST.WRK.D)
    LST.WRK.D = TODAY[1,6]:"01"
    CALL CDT("",LST.WRK.D,'-1W')
**********************CREDIT.CBE FOR LD**************************

    Y.SEL  = "SELECT F.SCB.CREDIT.CBE WITH (TOT.US.12 NE 0 OR TOT.CR.12 NE 0)"
    Y.SEL := " AND (TOT.US.12 NE '' AND TOT.CR.12 NE '')"
    CALL EB.READLIST(Y.SEL, KEY.LIST2, "", SELECTED2, ASD2)

    IF SELECTED2 THEN
        FOR J = 1 TO SELECTED2
            R.FILL.CF = ''; R.FILL.CS = ''
            HIGH = 0
            TYPE = 'LD'
            R.CU = ''
            OLD.ACCT.NEW = ''
            NDPD = 0
*====================== NEW
            LD.CUS.ID   = KEY.LIST2<J>
            CALL F.READ( FN.CU,LD.CUS.ID, R.CU, F.CU, ETEXT)
* IF LD.CUS.ID EQ '81300154' THEN
*     DEBUG
* END

            SECTOR   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            POS.REST = R.CU<EB.CUS.POSTING.RESTRICT>
            ACTIVE   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ACTIVE.CUST>
            B.ID     = R.CU<EB.CUS.ACCOUNT.OFFICER>
            TURNOVER = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER>
            PAID.CAPITAL = R.CU<EB.CUS.LOCAL.REF><1,CULR.PAID.CAPITAL>
*==================================================================
*    IF (SECTOR NE '4650') AND (POS.REST NE 90 AND POS.REST NE 99) AND (ACTIVE EQ 'YES') AND ((TURNOVER GT 0 AND TURNOVER LT 200000000)) THEN
            IF (SECTOR NE '4650') AND (POS.REST NE 90 AND POS.REST NE 99) AND (ACTIVE EQ 'YES') AND (TURNOVER LT 200000000) THEN
                CRT @(30,3) KEY.LIST2<J>:' ':J:' OF ':SELECTED2
                CALL F.READ(FN.CBE,KEY.LIST2<J>, R.CBE, F.CBE, ETEXT3)
                LD.CUS.ID   = KEY.LIST2<J>
                MAX.TOTAL   = R.CBE<SCB.C.CBE.TOT.CR.12,1> * 1000
                OPN.BALANCE = R.CBE<SCB.C.CBE.TOT.US.12,1> * 1000
                CBE.NO      = R.CBE<SCB.C.CBE.CBE.NO,1>
                LMT.REF     = LD.CUS.ID:'.0002500.01'
                LD.ACCT     = LD.CUS.ID:'102109601'

                FOR LM.R = 1 TO 9

                    LMT.REF     = LD.CUS.ID:'.0002500.0':LM.R
                    CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT4)

                    IF R.LMT<LI.APPROVAL.DATE> NE '' THEN
                        LM.R = 10
                    END
                NEXT LM.R


*        CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT4)
*        IF ETEXT4 EQ '' AND R.LMT<LI.MAXIMUM.TOTAL> EQ '' THEN
*            GOTO END.LMT
*        END
                IF MAX.TOTAL EQ '' THEN
                    R.LMT<LI.EXPIRY.DATE> = "20001010"
                END
*============================================================
                IF OPN.BALANCE EQ '' THEN
                    OPN.BALANCE = 0
                END

*============================================================
                AMT_OVER       = 0
                IF OPN.BALANCE GT MAX.TOTAL THEN
                    AMT_OVER   = OPN.BALANCE - MAX.TOTAL
                END
                CALL F.READ( FN.CU,LD.CUS.ID, R.CU, F.CU, ETEXT5)
                WOMEN.NO    = R.CU<EB.CUS.LOCAL.REF><1,CULR.OWNERSHIP.PER>
                WOMEN.NO    = WOMEN.NO + 0
* =============== ???? ??? ???????   ========================
                DAYS = 'C'
                DDD = R.LMT<LI.EXPIRY.DATE>[1,8]
                IF DDD EQ '' THEN
                    DDD = "20100101"
                END

*=========================== ????? ??? ??? ??????? =====================
                ASSET_CLASS = 401

*=============== REPAYMENT AND TERM OF LOAN VALUES===========
                LD.ACC.CAT   = LD.ACCT[10,4]
                REPAY.TYPE   = '007'
                TERM.OF.LOAN = '12'
                HIGH         = ''
******************************* NOHA 27/9/2016 **********************
                CREDIT.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
                IF CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120 THEN
                    ASSET_CLASS = 404
                    NDPD = 999
                    IF AMT_OVER EQ 0 THEN AMT_OVER = 1
                END
*====================creat erecord to fil in the CF table==========

                IF NDPD EQ 0 THEN
                    AMT_OVER = 0
                END
                IF AMT_OVER EQ 0 THEN   ;*AHM
                    NDPD = 0
                END
                IF AMT_OVER LT 0 THEN
                    AMT_OVER = AMT_OVER * -1
                END

                AMT_OVER = DROUND(AMT_OVER,0)

                IF OPN.BALANCE LT 0 THEN
                    BAL.AMOUNT = OPN.BALANCE * -1
                END
                ELSE
                    BAL.AMOUNT = OPN.BALANCE
                END

                IF LEN(LD.CUS.ID) EQ 7 THEN
                    LD.ACCT.NEW = '0':LD.ACCT
                END ELSE
                    LD.ACCT.NEW = LD.ACCT
                END

                APP.DATE = R.LMT<LI.APPROVAL.DATE>

*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
                COMP.CODE = R.CU<EB.CUS.COMPANY.BOOK>[8,2]
                IF COMP.CODE EQ '11' THEN
                    ISLAMIC.IND = '001'
                END
                ELSE
                    ISLAMIC.IND = '002'
                END

                R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = COMP.CODE
*************************************************
                IF OPN.BALANCE EQ '' OR OPN.BALANCE EQ 0 THEN
                    SECURED.IND = '001'
                END
                ELSE
                    SECURED.IND = '003'
                END

*------- EDIT BY NESSMA ON 15/4/2013 --------------------
                IF LEN(LD.ACCT.NEW) EQ 16 THEN
                    LD.ACCT.NEW = "0":LD.ACCT.NEW
                END
*--------------------------------------------------------

                R.FILL.CF<ISCO.CF.CF.ACC.NO>           = LD.ACCT.NEW
                R.FILL.CF<ISCO.CF.APPROVAL.DATE>       = APP.DATE
                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(MAX.TOTAL,0)
                R.FILL.CF<ISCO.CF.AMT.INSTALL>         = ''
                R.FILL.CF<ISCO.CF.CURRENCY>            = '10'
                R.FILL.CF<ISCO.CF.CF.TYPE>             = '21096'
                R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = TERM.OF.LOAN
                R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = REPAY.TYPE
                R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = BAL.AMOUNT
                R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = AMT_OVER
                R.FILL.CF<ISCO.CF.NDPD>                = NDPD
                R.FILL.CF<ISCO.CF.ASSET.CLASS>         = ASSET_CLASS
                R.FILL.CF<ISCO.CF.REASON.AMT.FORGIVEN> = '002'
                R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = 0
                R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
                R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
                R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
                R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
                R.FILL.CF<ISCO.CF.ACC.STATUS>          = '3'
                R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = SECURED.IND
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND

                IF LEN(LD.ACCT) EQ 16 THEN
                    LD.ACCT = '0':LD.ACCT
                END

                ID.CF = LD.ACCT:YEAR:NEW.MONTH:"LD"

                WRITE R.FILL.CF TO FVAR.CF , ID.CF  ON ERROR
                    STOP 'CAN NOT WRITE RECORD ':ID.CF:' TO FILE ':F.CF
                END

*=============== creat erecord to fil in the CS table================

                IF TYPE EQ 'LD'  THEN

                    R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = COMP.CODE
                    R.FILL.CS<ISCO.CS.CF.ACC.NO>           = LD.ACCT
                    R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= LD.CUS.ID
                    ID.CS = LD.ACCT:YEAR:NEW.MONTH:"LD"

                END

                IDENT.CODE                             = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>

                R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = FMT(IDENT.CODE,"R%12")
                R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
*  R.FILL.CS<ISCO.CS.IDENT.1.EXPIRE.DATE> = '0'
                R.FILL.CS<ISCO.CS.IDENT.1.EXPIRE.DATE> = WOMEN.NO
                R.FILL.CS<ISCO.CS.A.SUBJ.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                R.FILL.CS<ISCO.CS.ADD.1.TYPE.E>        = '001'
                R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
                R.FILL.CS<ISCO.CS.ADD.1.GOV.E>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                R.FILL.CS<ISCO.CS.COUNTRY>             = R.CU<EB.CUS.RESIDENCE>
                LOCAL.REF1                             = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 328 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
                IF NO_OF_TEL EQ 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                IF NO_OF_TEL GT 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                R.FILL.CS<ISCO.CS.LEGAL.CONDITION>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.LEGAL.FORM>
                R.FILL.CS<ISCO.CS.SME.CATEGORY>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.SME.CATEGORY>
                R.FILL.CS<ISCO.CS.PAID.CAPITAL>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.PAID.CAPITAL>
                R.FILL.CS<ISCO.CS.TURNOVER>              = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER>
                R.FILL.CS<ISCO.CS.TOTAL.ASSETS>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TOTAL.ASSETS>
                R.FILL.CS<ISCO.CS.NO.OF.EMPLOYEES>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.NO.OF.EMP>
                R.FILL.CS<ISCO.CS.ESTABLISH.DATE>        = R.CU<EB.CUS.BIRTH.INCORP.DATE>
                R.FILL.CS<ISCO.CS.BUSINESS.START.DATE>   = R.CU<EB.CUS.LEGAL.ISS.DATE>
                R.FILL.CS<ISCO.CS.FIN.STATEMENT.DATE>    = R.CU<EB.CUS.LOCAL.REF><1,CULR.FIN.STAT.DATE>
                R.FILL.CS<ISCO.CS.MAX.TURNOVER.AMT.DATE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER.DATE>
                R.FILL.CS<ISCO.CS.BUSINESS.END.DATE>     = R.CU<EB.CUS.LEGAL.EXP.DATE>
                R.FILL.CS<ISCO.CS.BAC1>                  = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.ACTIVITY>
*R.FILL.CS<ISCO.CS.BAC3>                  = "003"
                IF WOMEN.NO GT 0 THEN
                    R.FILL.CS<ISCO.CS.BAC3>              = "001"
                END
                ELSE
                    R.FILL.CS<ISCO.CS.BAC3>              = "003"
                END
           R.FILL.CS<ISCO.CS.RECORD.STATUS>         = 'INAU'
                R.FILL.CS<ISCO.CS.CURR.NO>               = '1'
                R.FILL.CS<ISCO.CS.INPUTTER>              = 'ISCORE'
                R.FILL.CS<ISCO.CS.CO.CODE>               = COMP

                WRITE R.FILL.CS TO FVAR.CS , ID.CS  ON ERROR
                    STOP 'CAN NOT WRITE RECORD ':ID.CS:' TO FILE ':F.CS
                END

            END
*END.LMT:
        NEXT J
    END
**********************CREDIT.CBE FOR LC**************************

    Z.SEL  = "SELECT F.SCB.CREDIT.CBE WITH (TOT.US.11 NE 0 OR TOT.CR.11 NE 0)"
    Z.SEL := " AND (TOT.US.11 NE '' AND TOT.CR.11 NE '')"
    CALL EB.READLIST(Z.SEL, KEY.LIST3, "", SELECTED3, ASD3)

    IF SELECTED3 THEN
        FOR Z = 1 TO SELECTED3
            R.FILL.CF = '';R.FILL.CS = ''
            HIGH = 0
            TYPE = 'LC'
            R.CU = ''
            LC.ACCT = ''
            NDPD = 0
*==================================================================
            LC.CUS.ID   = KEY.LIST3<Z>
*IF LC.CUS.ID EQ "13300337" THEN
*    DEBUG
*END
            CALL F.READ( FN.CU,LC.CUS.ID, R.CU, F.CU, ETEXT)
            SECTOR   = R.CU<EB.CUS.LOCAL.REF><1,CULR.NEW.SECTOR>
            POS.REST = R.CU<EB.CUS.POSTING.RESTRICT>
            ACTIVE   = R.CU<EB.CUS.LOCAL.REF><1,CULR.ACTIVE.CUST>
            B.LC.ID  = R.CU<EB.CUS.ACCOUNT.OFFICER>
*==================================================================
            TURNOVER = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER>
            PAID.CAPITAL = R.CU<EB.CUS.LOCAL.REF><1,CULR.PAID.CAPITAL>

*==================================================================

            IF (SECTOR NE '4650') AND (POS.REST NE 90 AND POS.REST NE 99) AND (ACTIVE EQ 'YES') AND ((TURNOVER GT 0 AND TURNOVER LT 200000000) OR (TURNOVER EQ 0 AND PAID.CAPITAL GT 0)) THEN
                CRT @(30,3) KEY.LIST3<Z>:' ':Z:' OF ':SELECTED3
                CALL F.READ( FN.CBE,KEY.LIST3<Z>, R.CBE2, F.CBE, ETEXT6)
                LC.CUS.ID   = KEY.LIST3<Z>
                MAX.TOTAL   = R.CBE2<SCB.C.CBE.TOT.CR.11,1> * 1000
                OPN.BALANCE = R.CBE2<SCB.C.CBE.TOT.US.11,1> * 1000
                CBE.NO      = R.CBE2<SCB.C.CBE.CBE.NO,1>
                LMT.REF     = LC.CUS.ID:'.0020000.01'
                LMT.REF2    = LC.CUS.ID:'.0030000.01'
                LC.ACCT     = LC.CUS.ID:'102310101'
* ==================================================================
                CALL F.READ( FN.LMT,LMT.REF, R.LMT, F.LMT, ETEXT7)
                IF MAX.TOTAL EQ '' THEN
                    R.LMT<LI.EXPIRY.DATE> = "20001010"
                END
*============================================================
                IF OPN.BALANCE EQ '' THEN
                    OPN.BALANCE = 0
                END

*============================================================
                AMT_OVER       = 0
                IF OPN.BALANCE GT MAX.TOTAL THEN
                    AMT_OVER   = OPN.BALANCE - MAX.TOTAL
                END
                CALL F.READ( FN.CU,LC.CUS.ID, R.CU, F.CU, ETEXT5)
                WOMEN.NO    = R.CU<EB.CUS.LOCAL.REF><1,CULR.OWNERSHIP.PER>
                WOMEN.NO    = WOMEN.NO + 0
* ===========================================================

                DAYS = 'C'
                DDD = R.LMT<LI.EXPIRY.DATE>[1,8]
                IF DDD EQ '' THEN
                    DDD = "20100101"
                END

                IF R.LMT<LI.EXPIRY.DATE>[1,8] LT TODAY THEN

                    CALL CDD("",DDD,TODAY,DAYS)
                    NDPD = DAYS
                    IF NDPD GT 999 THEN NDPD = 999
                END ELSE
                    CALL CDT("",DDD,'-365C')
                    R.LMT<LI.APPROVAL.DATE> = DDD
                    NDPD = 0
                END
*====================================== =====================
                ASSET_CLASS = 0
                IF NDPD LE 30 THEN ASSET_CLASS = 401
                IF NDPD GE 31 AND NDPD LE 90 THEN ASSET_CLASS = 402
                IF NDPD GE 91 AND NDPD LE 120 THEN ASSET_CLASS = 403
                IF NDPD GE 121 THEN ASSET_CLASS = 404

*=============== REPAYMENT AND TERM OF LOAN VALUES===========
                LC.ACC.CAT   = LC.ACCT[10,4]
                REPAY.TYPE   = '007'
                TERM.OF.LOAN = '12'
                HIGH = ''
******************************* NOHA 27/9/2016 **********************
                CREDIT.CODE = R.CU<EB.CUS.LOCAL.REF><1,CULR.CREDIT.CODE>
                IF CREDIT.CODE EQ 110 OR CREDIT.CODE EQ 120 THEN
                    ASSET_CLASS = 404
                    NDPD = 999
                    IF AMT_OVER EQ 0 THEN AMT_OVER = 1
                END
                ELSE
                    NDPD = 0
                END
                IF ASSET_CLASS EQ 404 THEN
                    NDPD = 999
                    IF AMT_OVER EQ 0 THEN AMT_OVER = 1
                END
*====================creat erecord to fil in the CF table==========
                DATE.TEST = YEAR:NEW.MONTH:'30'

                IF NDPD EQ 0 THEN
                    AMT_OVER = 0
                END
                IF AMT_OVER EQ 0 THEN   ;*AHM
                    NDPD = 0
                END
                IF AMT_OVER LT 0 THEN
                    AMT_OVER = AMT_OVER * -1
                END

                IF R.LMT<LI.APPROVAL.DATE> GT DATE.TEST THEN
                    R.LMT<LI.APPROVAL.DATE> =  YEAR:NEW.MONTH:'29'
                END

                AMT_OVER = DROUND(AMT_OVER,0)

                IF OPN.BALANCE LT 0 THEN
                    BAL.AMOUNT = OPN.BALANCE * -1
                END
                ELSE
                    BAL.AMOUNT = OPN.BALANCE
                END
                IF LEN(B.LC.ID) EQ 1 AND LC.ACCT[1,1] NE '0' THEN
                    LC.ACCT.NEW = '0':LC.ACCT
                END
                ELSE
                    LC.ACCT.NEW = LC.ACCT
                END

                IF R.LMT<LI.APPROVAL.DATE> EQ YEAR:'0229' THEN
                    APP.DATE = YEAR:'0228'
                END
                ELSE
                    APP.DATE = R.LMT<LI.APPROVAL.DATE>
                END
                COMP.CODE = R.CU<EB.CUS.COMPANY.BOOK>[8,2]

*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
                IF COMP.CODE EQ '11' THEN
                    ISLAMIC.IND = '001'
                END
                ELSE
                    ISLAMIC.IND = '002'
                END

*************************************************
                IF OPN.BALANCE EQ '' OR OPN.BALANCE EQ 0 THEN
                    SECURED.IND = '001'
                END
                ELSE
                    SECURED.IND = '003'
                END
*************************************************
                R.FILL.CF<ISCO.CF.DATA.PROV.BRANCH.ID> = COMP.CODE
                R.FILL.CF<ISCO.CF.CF.ACC.NO>           = LC.ACCT.NEW
                R.FILL.CF<ISCO.CF.APPROVAL.DATE>       = APP.DATE
                R.FILL.CF<ISCO.CF.APPROVAL.AMOUNT>     = DROUND(MAX.TOTAL,0)
                R.FILL.CF<ISCO.CF.AMT.INSTALL>         = ''
                R.FILL.CF<ISCO.CF.CURRENCY>            = '10'
                R.FILL.CF<ISCO.CF.CF.TYPE>             = '23101'
                R.FILL.CF<ISCO.CF.TERM.OF.LOAN>        = TERM.OF.LOAN
                R.FILL.CF<ISCO.CF.REPAYMENT.TYPE>      = REPAY.TYPE
                R.FILL.CF<ISCO.CF.CF.ACCT.BALANCE>     = BAL.AMOUNT
                R.FILL.CF<ISCO.CF.AMT.OVERDUE>         = AMT_OVER
                R.FILL.CF<ISCO.CF.NDPD>                = NDPD
                R.FILL.CF<ISCO.CF.ASSET.CLASS>         = ASSET_CLASS
                R.FILL.CF<ISCO.CF.REASON.AMT.FORGIVEN> = '002'
                R.FILL.CF<ISCO.CF.RECORD.STATUS>       = 'INAU'
                R.FILL.CF<ISCO.CF.CURR.NO>             = '1'
                R.FILL.CF<ISCO.CF.INPUTTER>            = 'ISCORE'
                R.FILL.CF<ISCO.CF.CO.CODE>             = COMP
                R.FILL.CF<ISCO.CF.DATE.TIME>           = TODAY
                R.FILL.CF<ISCO.CF.ACC.STATUS>          = '3'
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY>      = SECURED.IND
                R.FILL.CF<ISCO.CF.PROTEST.NOTIFY.DATE> = ISLAMIC.IND
                R.FILL.CF<ISCO.CF.CLOSURE.REASON>      = 0
                IF LEN(LC.ACCT) EQ 16 THEN
                    LC.ACCT = '0':LC.ACCT

                END

                ID.CF = LC.ACCT:YEAR:NEW.MONTH:"LC"

                WRITE R.FILL.CF TO FVAR.CF , ID.CF  ON ERROR
                    STOP 'CAN NOT WRITE RECORD ':ID.CF:' TO FILE ':F.CF
                END

*=============== creat erecord to fil in the CS table================

                IF TYPE EQ 'LC' THEN
                    R.FILL.CS<ISCO.CS.DATA.PROV.BRANCH.ID> = COMP.CODE
                    R.FILL.CS<ISCO.CS.CF.ACC.NO>           = LC.ACCT
                    R.FILL.CS<ISCO.CS.DATA.PROV.IDENT.CODE>= LC.CUS.ID
                    ID.CS = LC.ACCT:YEAR:NEW.MONTH:"LC"
                END

                IDENT.CODE                             = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.NO>
                R.FILL.CS<ISCO.CS.IDENT.CODE.1>        = FMT(IDENT.CODE,"R%12")
                R.FILL.CS<ISCO.CS.ISSUE.AUTHORITY.1>   = ''
                R.FILL.CS<ISCO.CS.IDENT.1.EXPIRE.DATE> = WOMEN.NO
* R.FILL.CS<ISCO.CS.IDENT.1.EXPIRE.DATE> = '0'
                R.FILL.CS<ISCO.CS.A.SUBJ.NAME>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.NAME>
                R.FILL.CS<ISCO.CS.ADD.1.TYPE.E>        = '001'
                R.FILL.CS<ISCO.CS.ADD.1.LINE.1.A>      = R.CU<EB.CUS.LOCAL.REF><1,CULR.ARABIC.ADDRESS>
                R.FILL.CS<ISCO.CS.ADD.1.GOV.E>         = R.CU<EB.CUS.LOCAL.REF><1,CULR.GOVERNORATE>
                R.FILL.CS<ISCO.CS.COUNTRY>             = R.CU<EB.CUS.RESIDENCE>
                LOCAL.REF1                             = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE>

*Line [ 581 ] Add @SM Instead Of SM - ITSS - R21 Upgrade - 2021-12-26
                NO_OF_TEL =DCOUNT(LOCAL.REF1,@SM)
                IF NO_OF_TEL EQ 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                IF NO_OF_TEL GT 1 THEN
                    R.FILL.CS<ISCO.CS.PHONE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TELEPHONE,1>
                END
                R.FILL.CS<ISCO.CS.LEGAL.CONDITION>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.LEGAL.FORM>
                R.FILL.CS<ISCO.CS.SME.CATEGORY>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.SME.CATEGORY>
                R.FILL.CS<ISCO.CS.PAID.CAPITAL>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.PAID.CAPITAL>
                R.FILL.CS<ISCO.CS.TURNOVER>              = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER>
                R.FILL.CS<ISCO.CS.TOTAL.ASSETS>          = R.CU<EB.CUS.LOCAL.REF><1,CULR.TOTAL.ASSETS>
                R.FILL.CS<ISCO.CS.NO.OF.EMPLOYEES>       = R.CU<EB.CUS.LOCAL.REF><1,CULR.NO.OF.EMP>
                R.FILL.CS<ISCO.CS.ESTABLISH.DATE>        = R.CU<EB.CUS.BIRTH.INCORP.DATE>
                R.FILL.CS<ISCO.CS.BUSINESS.START.DATE>   = R.CU<EB.CUS.LEGAL.ISS.DATE>
                R.FILL.CS<ISCO.CS.FIN.STATEMENT.DATE>    = R.CU<EB.CUS.LOCAL.REF><1,CULR.FIN.STAT.DATE>
                R.FILL.CS<ISCO.CS.MAX.TURNOVER.AMT.DATE> = R.CU<EB.CUS.LOCAL.REF><1,CULR.TURNOVER.DATE>
                R.FILL.CS<ISCO.CS.BUSINESS.END.DATE>     = R.CU<EB.CUS.LEGAL.EXP.DATE>
                R.FILL.CS<ISCO.CS.BAC1>                  = R.CU<EB.CUS.LOCAL.REF><1,CULR.CBE.ACTIVITY>
                IF WOMEN.NO GT 0 THEN
                    R.FILL.CS<ISCO.CS.BAC3>              = "001"
                END
                ELSE
                    R.FILL.CS<ISCO.CS.BAC3>              = "003"
                END
* R.FILL.CS<ISCO.CS.BAC3>                  = "003"
                R.FILL.CS<ISCO.CS.RECORD.STATUS>         = 'INAU'
                R.FILL.CS<ISCO.CS.CURR.NO>               = '1'
                R.FILL.CS<ISCO.CS.INPUTTER>              = 'ISCORE'
                R.FILL.CS<ISCO.CS.CO.CODE>               = COMP

                WRITE R.FILL.CS TO FVAR.CS , ID.CS  ON ERROR
                    STOP 'CAN NOT WRITE RECORD ':ID.CS:' TO FILE ':F.CS
                END

            END
        NEXT Z
    END
**********************UNKNOWN CASESE**********************

    T.SEL  = "SELECT F.SCB.ISCO.UNKNOWN.CASES"
    CALL EB.READLIST(T.SEL, KEY.LIST, "", SELECTED, ASD)

    FOR I = 1 TO SELECTED

        R.UN  = '';  R.CF1 = ''; R.CF2 = ''; R.CF3 = ''; R.CF4 = '';
        ETEXT = '';  ER1   = ''; ER2 = ''; ER3 = ''; ER4 = ''
        NDPD  = 999; DAYS  = 'C'; FLAG = 'N'

        CALL F.READ( FN.UN,KEY.LIST<I>, R.UN, F.UN, ETEXT)
        UNKNOWN.ACCT = KEY.LIST<I>
        ACCT.ID  = KEY.LIST<I>:FINAL.DATE
        ACCT.ID1 = ACCT.ID
        ACCT.ID2 = ACCT.ID:'LD'
        ACCT.ID3 = ACCT.ID:'LC'

        APP.DATE = R.UN<ISCO.UN.APPROVAL.DATE>
        APP.AMT  = R.UN<ISCO.UN.APPROVAL.AMT>

        CALL CDD("",APP.DATE,TODAY,DAYS)

        IF DAYS LE 365 THEN
            NDPD = 120
        END
        CALL F.READ( FN.CF ,ACCT.ID1, R.CF1, F.CF, ER1)
        CALL F.READ( FN.CF ,ACCT.ID2, R.CF2, F.CF, ER2)
        CALL F.READ( FN.CF ,ACCT.ID3, R.CF3, F.CF, ER3)

        IF ER1 EQ '' THEN
            BAL = R.CF1<ISCO.CF.CF.ACCT.BALANCE> - APP.AMT

            IF R.CF1<ISCO.CF.APPROVAL.DATE> EQ '' THEN
                R.CF1<ISCO.CF.APPROVAL.DATE>   = APP.DATE
                FLAG = 'Y'
            END
            IF R.CF1<ISCO.CF.APPROVAL.AMOUNT> EQ '' OR R.CF1<ISCO.CF.APPROVAL.AMOUNT> EQ 0 THEN
                R.CF1<ISCO.CF.APPROVAL.AMOUNT> = DROUND(APP.AMT,0)
                FLAG = 'Y'
            END
            IF FLAG EQ 'Y' THEN
                IF BAL GT 1 THEN
                    R.CF1<ISCO.CF.AMT.OVERDUE> = BAL
                    R.CF1<ISCO.CF.NDPD> = NDPD
                END
                ELSE
                    R.CF1<ISCO.CF.AMT.OVERDUE> = 0
                    R.CF1<ISCO.CF.NDPD> = 0
                    R.CF1<ISCO.CF.ASSET.CLASS> = R.CF1<ISCO.CF.ASSET.CLASS>[1,2]:'1'
                END
            END
*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
            COMP.CODE = R.CF1<ISCO.CF.DATA.PROV.BRANCH.ID>
            IF COMP.CODE EQ '11' THEN
                R.CF1<ISCO.CF.PROTEST.NOTIFY.DATE> = '001'
            END
            ELSE
                R.CF1<ISCO.CF.PROTEST.NOTIFY.DATE> = '002'
            END
*************************************************
            CATT = R.CF1<ISCO.CF.CF.TYPE>
            IF(CATT EQ 1206 OR CATT EQ 1208 OR CATT EQ 1417 OR CATT EQ 1419 OR CATT EQ 1421 OR CATT EQ 1484 OR CATT EQ 1485 OR CATT EQ 1401 OR CATT EQ 1402 OR CATT EQ 1405 OR CATT EQ 1406 OR CATT EQ 1415 OR CATT EQ 1480 OR CATT EQ 1481 OR CATT EQ 1483 OR CATT EQ 1493 OR CATT EQ 1499 OR CATT EQ 1218 OR CATT EQ 21066) THEN
                R.CF1<ISCO.CF.PROTEST.NOTIFY> = '001'
            END
            ELSE
                R.CF1<ISCO.CF.PROTEST.NOTIFY> = '003'
            END
            R.CF1<ISCO.CF.CLOSURE.REASON>     = 0
**************************************************
            CALL F.WRITE(FN.CF,ACCT.ID1,R.CF1)
            CALL JOURNAL.UPDATE(ACCT.ID1)
        END
        IF ER2 EQ '' THEN
            BAL = R.CF2<ISCO.CF.CF.ACCT.BALANCE> - APP.AMT
            IF R.CF2<ISCO.CF.APPROVAL.DATE> EQ '' THEN
                R.CF2<ISCO.CF.APPROVAL.DATE>   = APP.DATE
                FLAG = 'Y'
            END
            IF R.CF2<ISCO.CF.APPROVAL.AMOUNT> EQ '' OR R.CF2<ISCO.CF.APPROVAL.AMOUNT> EQ 0 THEN
                R.CF2<ISCO.CF.APPROVAL.AMOUNT> = DROUND(APP.AMT,0)
                FLAG = 'Y'
            END
            IF FLAG EQ 'Y' THEN
                IF BAL GT 1 THEN
                    R.CF2<ISCO.CF.AMT.OVERDUE> = BAL
                END
                ELSE
                    R.CF2<ISCO.CF.AMT.OVERDUE> = 0
                    R.CF2<ISCO.CF.ASSET.CLASS> = R.CF2<ISCO.CF.ASSET.CLASS>[1,2]:'1'
                END
            END
*************************************************

            CALL F.WRITE(FN.CF,ACCT.ID2,R.CF2)
            CALL JOURNAL.UPDATE(ACCT.ID2)
        END
        IF ER3 EQ '' THEN
            BAL = R.CF3<ISCO.CF.CF.ACCT.BALANCE> - APP.AMT
            IF R.CF3<ISCO.CF.APPROVAL.DATE> EQ '' THEN
                R.CF3<ISCO.CF.APPROVAL.DATE>   = APP.DATE
                FLAG = 'Y'
            END
            IF R.CF3<ISCO.CF.APPROVAL.AMOUNT> EQ '' OR R.CF3<ISCO.CF.APPROVAL.AMOUNT> EQ 0 THEN
                R.CF3<ISCO.CF.APPROVAL.AMOUNT> = DROUND(APP.AMT,0)
                FLAG = 'Y'
            END
            IF FLAG EQ 'Y' THEN
                IF BAL GT 1 THEN
                    R.CF3<ISCO.CF.AMT.OVERDUE> = BAL
                END
                ELSE
                    R.CF3<ISCO.CF.AMT.OVERDUE> = 0
                    R.CF3<ISCO.CF.ASSET.CLASS> = R.CF3<ISCO.CF.ASSET.CLASS>[1,2]:'1'
                END
            END
*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
            COMP.CODE = R.CF3<ISCO.CF.DATA.PROV.BRANCH.ID>
            IF COMP.CODE EQ '11' THEN
                R.CF3<ISCO.CF.PROTEST.NOTIFY.DATE> = '001'
            END
            ELSE
                R.CF3<ISCO.CF.PROTEST.NOTIFY.DATE> = '002'
            END
*************************************************
            CATT = R.CF3<ISCO.CF.CF.TYPE>
            IF(CATT EQ 1206 OR CATT EQ 1208 OR CATT EQ 1417 OR CATT EQ 1419 OR CATT EQ 1421 OR CATT EQ 1484 OR CATT EQ 1485 OR CATT EQ 1401 OR CATT EQ 1402 OR CATT EQ 1405 OR CATT EQ 1406 OR CATT EQ 1415 OR CATT EQ 1480 OR CATT EQ 1481 OR CATT EQ 1483 OR CATT EQ 1493 OR CATT EQ 1499 OR CATT EQ 1218 OR CATT EQ 21066) THEN
                R.CF3<ISCO.CF.PROTEST.NOTIFY> = '001'
            END
            ELSE
                R.CF3<ISCO.CF.PROTEST.NOTIFY> = '003'
            END
**************************************************

            CALL F.WRITE(FN.CF,ACCT.ID3,R.CF3)
            CALL JOURNAL.UPDATE(ACCT.ID3)
        END

    NEXT I
*========================================================
    Q.SEL  = "SELECT F.SCB.ISCO.SME.CF WITH ACC.STATUS NE 90 AND @ID LIKE ...":CLOSED.ACCT.DATE:"... AND @ID UNLIKE ...A"
*   Q.SEL  = "SELECT F.SCB.ISCO.SME.CF WITH @ID LIKE 01303310102109601201502..."
    CALL EB.READLIST(Q.SEL, KEY.LIST13, "", SELECTED13, ASD13)
    FOR Q = 1 TO SELECTED13
        CALL F.READ( FN.CF,KEY.LIST13<Q>, R.CFF, F.CF, ETEXT)
        DATE.B4      = YEAR:NEW.MONTH
        NEW.DATE     = YEAR.B4:NEW.MONTH.B4
        KEY.ID       = KEY.LIST13<Q>
        LAST.CH      = KEY.ID[1]
        LEN.CH       = LEN(KEY.ID)
        LAST.3.CH    = KEY.ID[3]
        IF LAST.CH EQ 'B' THEN

            IF LEN.CH EQ 23 THEN
                ACCT.B4    = KEY.ID[1,16]:DATE.B4
                ACCT.B4.CS = KEY.ID[1,16]:DATE.B4

                ACCT.T     = KEY.ID[1,16]:NEW.DATE:'B'
                ACCT.T.CS  = KEY.ID[1,16]:NEW.DATE

                CS.ACCT.NEW= KEY.ID[1,16]:DATE.B4:'.A'

            END ELSE
                ACCT.B4     = KEY.ID[2,16]:DATE.B4
                ACCT.B4.CS  = KEY.ID[2,16]:DATE.B4
                ACCT.T      = KEY.ID[1,17]:NEW.DATE:'B'
                ACCT.T.CS   = KEY.ID[1,17]:NEW.DATE


                CS.ACCT.NEW = KEY.ID[1,17]:DATE.B4:'.A'
            END
        END
        IF LAST.CH EQ 'D'  AND LAST.3.CH NE 'OLD' THEN
            IF LEN.CH EQ 24 THEN
                ACCT.B4     = KEY.ID[1,16]:DATE.B4:'LD'
                ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4:'LD'
                ACCT.T      = KEY.ID[1,16]:NEW.DATE:'LD'
                ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE:'LD'

                CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'
            END ELSE
                ACCT.B4     = KEY.ID[1,17]:DATE.B4:'LD'
                ACCT.B4.CS  = KEY.ID[1,17]:DATE.B4:'LD'
                ACCT.T      = KEY.ID[1,17]:NEW.DATE:'LD'
                ACCT.T.CS   = KEY.ID[1,17]:NEW.DATE:'LD'

                CS.ACCT.NEW = KEY.ID[1,17]:DATE.B4:'.A'
            END
        END

        IF LAST.CH EQ 'L' THEN
            ACCT.B4     = KEY.ID[1,18]:DATE.B4:'L'
            ACCT.B4.CS  = KEY.ID[1,18]:DATE.B4:'L'
            ACCT.T      = KEY.ID[1,18]:NEW.DATE:'L'
            ACCT.T.CS   = KEY.ID[1,18]:NEW.DATE:'L'

            CS.ACCT.NEW = KEY.ID[1,18]:DATE.B4:'.A'

        END

        IF LAST.CH EQ 'C'  THEN
            IF LEN.CH EQ 24 THEN
                ACCT.B4     = KEY.ID[1,16]:DATE.B4:'LC'
                ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4:'LC'
                ACCT.T      = KEY.ID[1,16]:NEW.DATE:'LC'
                ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE:'LC'

                CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'

            END ELSE
                ACCT.B4    = KEY.ID[1,17]:DATE.B4:'LC'
                ACCT.B4.CS = KEY.ID[1,17]:DATE.B4:'LC'
                ACCT.T     = KEY.ID[1,17]:NEW.DATE:'LC'
                ACCT.T.CS  = KEY.ID[1,17]:NEW.DATE:'LC'

                CS.ACCT.NEW = KEY.ID[1,17]:DATE.B4:'.A'
            END
        END
        IF LAST.CH NE 'C' AND LAST.CH NE 'B' AND LAST.CH NE 'D' AND LAST.CH NE 'L' THEN
            ACCT.B4     = KEY.ID[1,16]:DATE.B4
            ACCT.B4.CS  = KEY.ID[1,16]:DATE.B4
            ACCT.T      = KEY.ID[1,16]:NEW.DATE
            ACCT.T.CS   = KEY.ID[1,16]:NEW.DATE

            CS.ACCT.NEW = KEY.ID[1,16]:DATE.B4:'.A'
        END
        CALL F.READ(FN.CF,ACCT.B4, R.CFT, F.CF,CF.ERR)
        CALL F.READ(FN.CS,ACCT.B4.CS,R.CSS,F.CS,CS.ERR)

        R.FILL.CLOSE = ''
        R.FILL.CL.S  = ''

        IF R.CFT EQ '' THEN
            CALL F.READ(FN.CF,ACCT.T, R.CFT, F.CF,CF.ERR)
            CALL F.READ(FN.CS,ACCT.T.CS,R.CSS,F.CS,CS.ERR)

            R.FILL.CLOSE<ISCO.CF.DATA.PROV.BRANCH.ID> = R.CFF<ISCO.CF.DATA.PROV.BRANCH.ID>
            R.FILL.CLOSE<ISCO.CF.CF.ACC.NO>           = R.CFF<ISCO.CF.CF.ACC.NO>

            ACC.NOOO = R.CFF<ISCO.CF.CF.ACC.NO>
            NN.SEL   = "SELECT F.SCB.ISCO.SME.CF WITH @ID LIKE ":ACC.NOOO:"..."
            NN.SEL  := " AND APPROVAL.DATE NE '' BY @ID"
            CALL EB.READLIST(NN.SEL, NN.KEY.LIST, "", NN.SELECTED, NN.ASD)

            IF NN.SELECTED THEN
                CALL F.READ(FN.CF,NN.KEY.LIST<NN.SELECTED>, R.CFT.NN, F.CF,CF.ERR)
                R.FILL.CLOSE<ISCO.CF.APPROVAL.DATE>  = R.CFT.NN<ISCO.CF.APPROVAL.DATE>

                IF R.CFT.NN<ISCO.CF.APPROVAL.AMOUNT> LE 0 THEN
                    R.FILL.CLOSE<ISCO.CF.APPROVAL.AMOUNT>  = "1"
                END ELSE
                    R.FILL.CLOSE<ISCO.CF.APPROVAL.AMOUNT>  = DROUND(R.CFT.NN<ISCO.CF.APPROVAL.AMOUNT>,0)
                END
            END

*********GET ISLAMIC INDICATOR AND SECURED INDICATOR********************
            COMP.CODE = R.CFF<ISCO.CF.DATA.PROV.BRANCH.ID>
            IF COMP.CODE EQ '11' THEN
                R.FILL.CLOSE<ISCO.CF.PROTEST.NOTIFY.DATE> = '001'
            END ELSE
                R.FILL.CLOSE<ISCO.CF.PROTEST.NOTIFY.DATE> = '002'
            END
*************************************************
            CATT = R.CFF<ISCO.CF.CF.TYPE>
            IF(CATT EQ 1206 OR CATT EQ 1208 OR CATT EQ 1417 OR CATT EQ 1419 OR CATT EQ 1421 OR CATT EQ 1484 OR CATT EQ 1485 OR CATT EQ 1401 OR CATT EQ 1402 OR CATT EQ 1405 OR CATT EQ 1406 OR CATT EQ 1415 OR CATT EQ 1480 OR CATT EQ 1481 OR CATT EQ 1483 OR CATT EQ 1493 OR CATT EQ 1499 OR CATT EQ 1218 OR CATT EQ 21066) THEN
                R.FILL.CLOSE<ISCO.CF.PROTEST.NOTIFY>  = '001'
            END ELSE
                R.FILL.CLOSE<ISCO.CF.PROTEST.NOTIFY>  = '003'
            END
            R.FILL.CLOSE<ISCO.CF.CLOSURE.REASON>      = 0
            R.FILL.CLOSE<ISCO.CF.CURRENCY>            = R.CFF<ISCO.CF.CURRENCY>
            R.FILL.CLOSE<ISCO.CF.CF.TYPE>             = R.CFF<ISCO.CF.CF.TYPE>
            R.FILL.CLOSE<ISCO.CF.TERM.OF.LOAN>        = R.CFF<ISCO.CF.TERM.OF.LOAN>
            R.FILL.CLOSE<ISCO.CF.REPAYMENT.TYPE>      = R.CFF<ISCO.CF.REPAYMENT.TYPE>
            R.FILL.CLOSE<ISCO.CF.CF.ACCT.BALANCE>     = 0
            R.FILL.CLOSE<ISCO.CF.AMT.OVERDUE>         = 0
            R.FILL.CLOSE<ISCO.CF.NDPD>                = 0
            R.FILL.CLOSE<ISCO.CF.ASSET.CLASS>         = R.CFF<ISCO.CF.ASSET.CLASS>
            R.FILL.CLOSE<ISCO.CF.ACCT.CLOSING.DATE>   = LST.WRK.D
            R.FILL.CLOSE<ISCO.CF.REASON.AMT.FORGIVEN> = '002'
            R.FILL.CLOSE<ISCO.CF.RECORD.STATUS>       = 'INAU'
            R.FILL.CLOSE<ISCO.CF.CURR.NO>             = '1'
            R.FILL.CLOSE<ISCO.CF.INPUTTER>            = 'ISCORE'
            R.FILL.CLOSE<ISCO.CF.CO.CODE>             = COMP
            R.FILL.CLOSE<ISCO.CF.ACC.STATUS>          = '90'
            R.FILL.CLOSE<ISCO.CF.DATE.TIME>           = TODAY

            WRITE R.FILL.CLOSE TO FVAR.CF , CS.ACCT.NEW  ON ERROR
                STOP 'CAN NOT WRITE RECORD ':CS.ACCT.NEW:' TO FILE ':F.CF
            END
*===================== WRITE CS ==============

            R.FILL.CL.S<ISCO.CS.DATA.PROV.BRANCH.ID>   = R.CSS<ISCO.CS.DATA.PROV.BRANCH.ID>
            R.FILL.CL.S<ISCO.CS.CF.ACC.NO>             = R.CSS<ISCO.CS.CF.ACC.NO>
            R.FILL.CL.S<ISCO.CS.DATA.PROV.IDENT.CODE>  = R.CSS<ISCO.CS.DATA.PROV.IDENT.CODE>
            R.FILL.CL.S<ISCO.CS.IDENT.CODE.1>          = R.CSS<ISCO.CS.IDENT.CODE.1>
            R.FILL.CL.S<ISCO.CS.ISSUE.AUTHORITY.1>     = ''
            R.FILL.CL.S<ISCO.CS.IDENT.1.EXPIRE.DATE>   = R.CSS<ISCO.CS.IDENT.1.EXPIRE.DATE>
            R.FILL.CL.S<ISCO.CS.A.SUBJ.NAME>           = R.CSS<ISCO.CS.A.SUBJ.NAME>
            R.FILL.CL.S<ISCO.CS.ADD.1.TYPE.E>          = R.CSS<ISCO.CS.ADD.1.TYPE.E>
            R.FILL.CL.S<ISCO.CS.ADD.1.LINE.1.A>        = R.CSS<ISCO.CS.ADD.1.LINE.1.A>
            R.FILL.CL.S<ISCO.CS.ADD.1.GOV.E>           = R.CSS<ISCO.CS.ADD.1.GOV.E>
            R.FILL.CL.S<ISCO.CS.COUNTRY>               = R.CSS<ISCO.CS.COUNTRY>
            R.FILL.CL.S<ISCO.CS.PHONE>                 = R.CSS<ISCO.CS.PHONE>
            R.FILL.CL.S<ISCO.CS.LEGAL.CONDITION>       = R.CSS<ISCO.CS.LEGAL.CONDITION>
            R.FILL.CL.S<ISCO.CS.SME.CATEGORY>          = R.CSS<ISCO.CS.SME.CATEGORY>
            R.FILL.CL.S<ISCO.CS.PAID.CAPITAL>          = R.CSS<ISCO.CS.PAID.CAPITAL>
            R.FILL.CL.S<ISCO.CS.TURNOVER>              = R.CSS<ISCO.CS.TURNOVER>
            R.FILL.CL.S<ISCO.CS.TOTAL.ASSETS>          = R.CSS<ISCO.CS.TOTAL.ASSETS>
            R.FILL.CL.S<ISCO.CS.NO.OF.EMPLOYEES>       = R.CSS<ISCO.CS.NO.OF.EMPLOYEES>
            R.FILL.CL.S<ISCO.CS.ESTABLISH.DATE>        = R.CSS<ISCO.CS.ESTABLISH.DATE>
            R.FILL.CL.S<ISCO.CS.BUSINESS.START.DATE>   = R.CSS<ISCO.CS.BUSINESS.START.DATE>
            R.FILL.CL.S<ISCO.CS.FIN.STATEMENT.DATE>    = R.CSS<ISCO.CS.FIN.STATEMENT.DATE>
            R.FILL.CL.S<ISCO.CS.MAX.TURNOVER.AMT.DATE> = R.CSS<ISCO.CS.MAX.TURNOVER.AMT.DATE>
            R.FILL.CL.S<ISCO.CS.BUSINESS.END.DATE>     = R.CSS<ISCO.CS.BUSINESS.END.DATE>
            R.FILL.CL.S<ISCO.CS.BAC1>                  = R.CSS<ISCO.CS.BAC1>
            R.FILL.CL.S<ISCO.CS.BAC3>                  = R.CSS<ISCO.CS.BAC3>
            R.FILL.CL.S<ISCO.CS.RECORD.STATUS>         = 'INAU'
            R.FILL.CL.S<ISCO.CS.CURR.NO>               = '1'
            R.FILL.CL.S<ISCO.CS.INPUTTER>              = 'ISCORE'
            R.FILL.CL.S<ISCO.CS.CO.CODE>               = COMP

            WRITE R.FILL.CL.S TO FVAR.CS , CS.ACCT.NEW  ON ERROR
                STOP 'CAN NOT WRITE RECORD ':CS.ACCT.NEW:' TO FILE ':F.CS
            END

        END
    NEXT Q
END
