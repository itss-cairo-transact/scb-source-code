* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>-30</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE SCB.EXPORT.MESSAGE(MSG.NO, R.MSG, GENERIC.DATA, ERROR.MSG)


*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DE.INTERFACE

    FXXX.SCBSWIFT.OUT = "SCBSWIFT"
    CALL OPF(FXXX.SCBSWIFT.OUT,SCBSWIFT.OUT)


    FXXX.DE.INTERFACE = "F.DE.INTERFACE"
    CALL OPF(FXXX.DE.INTERFACE,F.DE.INTERFACE)
    INTERFACE.ID = "SWIFT.GEN"
    R.INTERFACE = ""
    READ.FAILED = ""
    F.DE.INTERFACE = ""
    DE.O.MSG.ALLIANCE = ""

    CALL F.READ(FXXX.DE.INTERFACE, INTERFACE.ID, R.INTERFACE, F.DE.INTERFACE, READ.FAILED)
    IF READ.FAILED = "" THEN
        GOSUB PROCESS.MSG
    END
    ELSE
        ERROR.MSG = "IMPOSSIBLE TO READ DE.INTERFACE ALLIANCE"
        RETURN
    END
    RETURN
***********************************************************
PROCESS.MSG:

    MSG.TYPE = ""
    YPOS = INDEX(R.MSG,"{2",1) + 4
    MSG.TYPE = R.MSG[YPOS,3]

    GOSUB PROCESS.MSG.NO

*    MODULO = MOD(LEN(R.MSG), 512)
*    IF MODULO > 0 THEN
*        R.MSG = R.MSG:SPACE(512-MODULO)
*
*        R.MSG = R.MSG:SPACE(512-MODULO)
*    END

    R.MSG = CHAR(1):R.MSG
    R.MSG = R.MSG:CHAR(3)

    ERROR.MSG = ''
    WRITE R.MSG TO SCBSWIFT.OUT,"ex":MSG.NO:".glb" ON ERROR
        ERROR.MSG = "IMPOSSIBLE TO WRITE FORMATED MESSAGE TO SCBSWIFT.OUT"
    END

    RETURN
***********************************************************

PROCESS.MSG.NO:

    V$BEFORE = "{108:xxxxx}"
    AFTER = "{108:":MSG.NO:"}"
    R.MSG = CHANGE(R.MSG, V$BEFORE, AFTER)

    RETURN

***********************************************************

END
