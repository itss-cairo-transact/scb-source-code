* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
*-----------------------------------------------------------------------------
* <Rating>187</Rating>
*-----------------------------------------------------------------------------
    SUBROUTINE VNC.BL.BATCH.FCY

*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 27 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 29 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BR.LOCAL.REFS
*Line [ 31 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BL.BATCH
*Line [ 33 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 35 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*Line [ 37 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BANK.BRANCH

    IF V$FUNCTION = 'A' THEN RETURN
    IF V$FUNCTION = 'R' THEN RETURN

    FN.BATCH = 'FBNK.BILL.REGISTER' ; F.BATCH = '' ; R.BATCH = ''
    CALL OPF(FN.BATCH,F.BATCH)

*(EB.BILL.REG.LOCAL.REF)< 1,BRLR.COLL.DATE>
*(EB.BILL.REG.LOCAL.REF)< 1,BRLR.BILL.CHQ.STA>

    KEY.LIST=""; SELECTED="" ; ER.MSG=""
    SAM1 = TODAY

*****************
    COMP       = ID.COMPANY
    USR.DEP    = "1700":COMP[8,2]
    CALL DBR('SCB.BANK.BRANCH':@FM:SCB.BAB.LOCATION,USR.DEP,LOCT)

    IF LOCT EQ '1'  THEN
        CALL CDT('',SAM1,"+1W")
    END
    IF LOCT EQ '2'  THEN
        CALL CDT('',SAM1,"+2W")
    END


    TEXT = SAM1 ; CALL REM
*****************

**    SAM1 = "20080109"
**    CALL CDT('',SAM1,"+1W")
**    T.SEL  = "SSELECT FBNK.BILL.REGISTER WITH ( BANK NE 0017 AND BANK NE 9902 ) AND  MATURITY.EXT EQ ": SAM1 :" AND BILL.CHQ.STA EQ 1 AND DEPT.CODE EQ ":R.USER<EB.USE.DEPARTMENT.CODE>:" AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BANK UNLIKE 99... AND CURRENCY EQ EGP "

*T.SEL  = "SELECT FBNK.BILL.REGISTER WITH ( BANK NE 0017 AND BANK NE 9902 ) AND  MATURITY.EXT LE ": SAM1 :" AND BILL.CHQ.STA EQ 1  AND CO.CODE EQ " : COMP:" AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BANK UNLIKE 99... AND CURRENCY EQ EGP "
*T.SEL  = "SELECT FBNK.BILL.REGISTER WITH  IN.OUT.BILL EQ 'YES' AND  MATURITY.EXT LE ": SAM1 :" AND BILL.CHQ.STA EQ 1  AND CO.CODE EQ " : COMP:" AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BANK UNLIKE 99... AND CURRENCY EQ EGP "
    T.SEL  = "SELECT FBNK.BILL.REGISTER WITH  IN.OUT.BILL EQ 'YES' AND  MATURITY.EXT LE ": SAM1 :" AND BILL.CHQ.STA EQ 1  AND CO.CODE EQ " : COMP:" AND (BIL.CHQ.TYPE EQ 6 OR BIL.CHQ.TYPE EQ 7 ) AND BANK UNLIKE 99... AND CURRENCY EQ USD "


    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)

    CALL F.READ(FN.BATCH, KEY.LIST<I>, R.BATCH, F.BATCH, ETEXT)
    IF SELECTED THEN
        TEXT = "SELECT : " : SELECTED ; CALL REM
        R.NEW(SCB.BLB.DESCRIPTION)   = "CREATE BATCH TO SEND IT TO COLLECTION"

        FOR I = 1 TO SELECTED
            R.NEW(SCB.BLB.CHQ.REG.ID)<1,I>    = KEY.LIST<I>
        NEXT I

        R.NEW(SCB.BLB.NO.OF.CHQS)    = SELECTED

        FOR I = 1 TO SELECTED
            CALL F.READ(FN.BATCH, KEY.LIST<I>, R.BATCH, F.BATCH, ETEXT)
            R.NEW(SCB.BLB.AMOUNT)   += R.BATCH<EB.BILL.REG.AMOUNT>
        NEXT I

        R.NEW(SCB.BLB.USR.STATUS) = 5
*R.NEW(SCB.BLB.CURRENCY)   = "EGP"
        R.NEW(SCB.BLB.CURRENCY)   = "USD"
        R.NEW(SCB.BLB.BATCH.DATE) = TODAY
    END ELSE
        E = "�� ���� ����� ��� ������� �����"  ; CALL ERR; MESSAGE = 'REPEAT'
    END

    RETURN
END
