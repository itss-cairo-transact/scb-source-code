* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE VIR.MG.REPORT

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_USER.ENV.COMMON
*-------------------------------------------------------------------------
    GOSUB INITIATE
*-------------------------------------------------------------------------
    GOSUB CALLDB
    RETURN
*==============================================================
INITIATE:
    REPORT.ID = 'MG.REPORT'
    XX = ''

    FN.FT = 'FBNK.FUNDS.TRANSFER' ; F.FT = ''
    CALL OPF(FN.FT,F.FT)

    FN.AC = 'FBNK.ACCOUNT' ; F.AC = ''
    CALL OPF(FN.AC,F.AC)

    RETURN
*===============================================================
CALLDB:

    WS.ACCT.NO     = R.NEW(FT.DEBIT.ACCT.NO)
    WS.USD.AMT     = R.NEW(FT.AMOUNT.DEBITED)
    WS.EGP.AMT     = R.NEW(FT.AMOUNT.CREDITED)
    WS.VALUE.DATE1 = R.NEW(FT.DEBIT.VALUE.DATE)
    WS.RATE        = R.NEW(FT.TREASURY.RATE)
    WS.VALUE.DATE  = FMT(WS.VALUE.DATE1,"####/##/##")

    CALL F.READ(FN.AC,WS.ACCT.NO,R.AC,F.AC,ER.AC)
    WS.CUST.NAME  = R.AC<AC.ACCOUNT.TITLE.1>

    XX = ''
    XX<1,1>[1,65] = ',TreasuryGroup@scbank.com.eg,TreasuryOperation@scbank.com.eg,'

    CALL PRINTER.ON(REPORT.ID,'')
    GOSUB PRINT.HEAD
    PRINT XX<1,1>

    XX = ''
    XX<1,1>[1,40]   = WS.CUST.NAME
    XX<1,1>[45,20]  = WS.USD.AMT
    XX<1,1>[70,20]  = WS.EGP.AMT
    XX<1,1>[95,10]  = WS.RATE
    XX<1,1>[110,10] = WS.VALUE.DATE

    PRINT XX<1,1>

    CALL PRINTER.OFF
    CALL PRINTER.CLOSE(REPORT.ID,0,'')

    RETURN
*===============================================================
PRINT.HEAD:
*---------
    PR.HD  ="'L'":" "
    HEADING PR.HD
    RETURN
*==============================================================
END
