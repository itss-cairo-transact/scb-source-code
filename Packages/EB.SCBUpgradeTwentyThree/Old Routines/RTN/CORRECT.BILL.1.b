* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    PROGRAM CORRECT.BILL.1

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.BILL.REGISTER
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.USER
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DEPT.ACCT.OFFICER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.BR.PAY.PLACE
    $INSERT  TEMENOS.BP I_BR.LOCAL.REFS
*-----------------------------------------------
    KEY.LIST="" ; SELECTED="" ;  ER.MSG=""
    FN.BR = 'FBNK.BILL.REGISTER' ; F.BR = '' ; R.BRRR = '' ; ETEXT = ''
    CALL OPF( FN.BR,F.BR)

    T.SEL  = "SELECT FBNK.BILL.REGISTER WITH MATURITY.EXT EQ 20210503"
    T.SEL := " AND CURRENCY EQ EGP AND ( BILL.CHQ.STA NE 7 AND BILL.CHQ.STA NE 8 )"
    T.SEL := " AND PAY.PLACE NE '' AND COLL.DATE NE '' "
    CALL EB.READLIST(T.SEL,KEY.LIST,'',SELECTED,ER.MSG)
    CRT "SELECTED = " : SELECTED

    FOR I =1 TO SELECTED
        CALL F.READ(FN.BR,KEY.LIST<I>,R.BRRR,F.BR,ETEXT)

**-----------------------------------------
         R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.MATURITY.EXT> = '20210504'
         R.BRRR<EB.BILL.REG.LOCAL.REF,BRLR.COLL.DATE>    = '20210505'
**-----------------------------------------
        CALL F.WRITE(FN.BR,KEY.LIST<I>,R.BRRR)
        CALL JOURNAL.UPDATE(KEY.LIST<I>)

    NEXT I

    RETURN
END
