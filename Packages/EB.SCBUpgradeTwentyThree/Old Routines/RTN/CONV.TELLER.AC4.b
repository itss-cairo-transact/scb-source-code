* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE CONV.TELLER.AC4
*Line [ 17 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 19 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 21 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 23 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.SCB.TRANS.TODAY
*Line [ 25 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_CO.LOCAL.REFS
************************************************************
    GOSUB INIT
    GOSUB OPEN
    GOSUB PROCESS
    RETURN
************************************************************
INIT:
    XX = O.DATA
    AMT = 0
    T.SEL = ''  ; KEY.LIST = ''  ;  SELECTED = ''  ;  ER.MSG = ''
    FN.TEL = 'F.SCB.TRANS.TODAY'
    F.TEL = ''
    R.TEL = ''
    AMTLCY = 0
    AMTFCY = 0

    RETURN
************************************************************
OPEN:
    CALL OPF(FN.TEL,F.TEL)
    RETURN
PROCESS:
************************************************************
    T.SEL = "SELECT ":FN.TEL: " WITH CURRENCY EQ ":XX:" AND AMOUNT.LCY GT 0 AND ACCOUNT.NUMBER LIKE ...990001"
    CALL EB.READLIST(T.SEL,KEY.LIST,"",SELECTED,ER.MSG)
    IF SELECTED THEN
        FOR I = 1 TO SELECTED
            CALL F.READ(FN.TEL,KEY.LIST<I>,R.TEL,F.TEL,EER)
            IF XX EQ "EGP" THEN
                AMTLCY = R.TEL<TRANS.AMOUNT.LCY>
                AMT += AMTLCY
            END ELSE
                AMTFCY = R.TEL<TRANS.AMOUNT.FCY>
                AMT += AMTFCY
            END
        NEXT I
    END
    O.DATA = AMT
    RETURN
END
