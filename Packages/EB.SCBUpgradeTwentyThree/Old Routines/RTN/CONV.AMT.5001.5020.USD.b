* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE CONV.AMT.5001.5020.USD

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.COMPANY
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_ENQUIRY.COMMON
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.FUNDS.TRANSFER
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.ACCOUNT
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER
*Line [ 32 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CUSTOMER.POSITION
*Line [ 34 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 36 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.CURRENCY
*----------------------------------------------

    ONLINE.AMT = 0 ; CALC.AMT = 0 ; WS.AMT = ""

    CUS  = FIELD(O.DATA,"*",1)
    CUR  = FIELD(O.DATA,"*",2)
    CATG = "5001"
    CURR = "EGP"

    FN.ACC = 'FBNK.ACCOUNT'  ;  F.ACC = ''
    CALL OPF(FN.ACC,F.ACC)

    FN.CUS.AC = 'FBNK.CUSTOMER.ACCOUNT' ; F.CUS.AC = ''
    CALL OPF(FN.CUS.AC,F.CUS.AC)

    CALL F.READ( FN.CUS.AC,CUS, R.CUS.AC, F.CUS.AC,ETEXT1)
    LOOP
        REMOVE ACC FROM R.CUS.AC  SETTING POS1
    WHILE ACC:POS1
        CALL F.READ(FN.ACC,ACC,R.ACC,F.ACC,ERR1)
        AC.CURR    = R.ACC<AC.CURRENCY>
        AC.CATEG   = R.ACC<AC.CATEGORY>
        AC.BAL     = R.ACC<AC.WORKING.BALANCE>

        IF AC.CURR EQ 'EGP' AND AC.BAL LT '0' AND (AC.CATEG EQ '5001' OR AC.CATEG EQ '5020') THEN
            ONLINE.AMT = R.ACC<AC.WORKING.BALANCE>
            WS.AMT += ONLINE.AMT
            ONLINE.AMT = 0
        END

        IF AC.CURR NE 'EGP' AND AC.BAL LT '0' AND (AC.CATEG EQ '5001' OR AC.CATEG EQ '5020') THEN
            ONLINE.AMT = R.ACC<AC.WORKING.BALANCE>
            CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,AC.CURR,MID.RATE)
            CALC.AMT  = ONLINE.AMT * MID.RATE<1,1>
            WS.AMT   += CALC.AMT
            CALC.AMT  = 0
        END
    REPEAT

    IF WS.AMT THEN
        USD.CURR = 'USD'
        CALL DBR('CURRENCY':@FM:EB.CUR.MID.REVAL.RATE,USD.CURR,MID.RATE)
        WS.AMT = WS.AMT / MID.RATE<1,1>
        O.DATA = DROUND(WS.AMT,2)
    END ELSE
        O.DATA = 0
    END
*-----------------------------------------------------
    RETURN
END
