* @ValidationCode : Mjo5MDg2NjA5MDA6Q3AxMjUyOjE2MzgyNzIxMTg2MzE6S2FyZWVtIE1vcnRhZGE6LTE6LTE6MDowOmZhbHNlOk4vQTpSMjBfU1AzLjA6LTE6LTE=
* @ValidationInfo : Timestamp         : 
* @ValidationInfo : Encoding          : Cp1252
* @ValidationInfo : User Name         : Amr AlArgawy 
* @ValidationInfo : Nb tests success  : N/A
* @ValidationInfo : Nb tests failure  : N/A
* @ValidationInfo : Rating            : N/A
* @ValidationInfo : Coverage          : N/A
* @ValidationInfo : Strict flag       : N/A
* @ValidationInfo : Bypass GateKeeper : false
* @ValidationInfo : Compiler Version  : R20_SP3.0
*Line [ 13 ] Add Package EB.SCBUpgradeTwentyThreeCR  - ITSS - R21 Upgrade - 2021-12-23
$PACKAGE EB.SCBUpgradeTwentyThreeCR
*DONE
    SUBROUTINE REBUILD.ASP.DATES

*Line [ 18 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_COMMON
*Line [ 20 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_EQUATE
*Line [ 22 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_BATCH.FILES
*Line [ 24 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT
*Line [ 26 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.ACCOUNT.STATEMENT
*Line [ 28 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.STMT.ENTRY
*Line [ 30 ] Removed directory from $INCLUDE - ITSS - R21 Upgrade - 2021-12-23
$INCLUDE I_F.DATES

    FN.ACC = "F.ACCOUNT"
    F.ACC = ""
    CALL OPF(FN.ACC,F.ACC)

    FN.SP = "F.STMT.PRINTED"
    F.SP = ""
    CALL OPF(FN.SP,F.SP)

    FN.ASP = "F.ACCT.STMT.PRINT"
    F.ASP = ""
    CALL OPF(FN.ASP,F.ASP)

 OPEN '&SAVEDLISTS&' TO SAVE.LIST ELSE SAVE.LIST = ""
   SEL.LIST1 = ''


   READ SEL.LIST1 FROM SAVE.LIST,'ASP.COR' ELSE
       SEL.LIST1=''
   END

     *SEL.LIST1<-1> = '800231902690910'

     LOOP
        REMOVE ACCT.ID FROM SEL.LIST1 SETTING POS

    WHILE ACCT.ID:POS
    
    SEL.CMD = "SELECT ":FN.SP:" WITH @ID LIKE ":ACCT.ID:"..."
     CALL EB.READLIST(SEL.CMD,SEL.LIST,"","","")

     STMT.DATES = FIELDS(SEL.LIST, "-", 2, 1)

     TOT.NO.OF.DATES = DCOUNT(STMT.DATES,@FM)
     STMT.DATES = SORT(STMT.DATES)
     FOR SP.I = 1 TO TOT.NO.OF.DATES
         R.ASP<-1> = STMT.DATES<SP.I>:'/0'
     NEXT SP.I

     WRITE R.ASP TO F.ASP,ACCT.ID
     CRT "Wring ASP record ":R.ASP:" for the account ":ACCT.ID
     
     REPEAT
     
     
     RETURN
 END
